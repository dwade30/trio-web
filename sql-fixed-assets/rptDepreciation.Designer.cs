﻿namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptDepreciation.
	/// </summary>
	partial class rptDepreciation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDepreciation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepreciationDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMake = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtModel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUnitsDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmountSum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtUnitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblRecordNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJournalNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.subCostSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepreciationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitsDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountSum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRecordNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournalNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDescription,
				this.txtYear,
				this.txtMake,
				this.txtModel,
				this.txtAmount,
				this.txtUnits,
				this.txtUnitsDesc,
				this.fldTag
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.SubReport1,
				this.Field10,
				this.Field11,
				this.txtJournalNumber,
				this.txtPeriod,
				this.subCostSummary
			});
			this.ReportFooter.Height = 1.447917F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtDate,
				this.txtPage,
				this.txtMuniName,
				this.DLG1,
				this.txtTime,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Line1,
				this.Field7,
				this.Field8,
				this.Field9,
				this.txtDepreciationDate,
				this.Field12
			});
			this.PageHeader.Height = 0.8854167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAmountSum,
				this.Label1,
				this.txtUnitsTotal,
				this.Line2,
				this.lblRecordNumber
			});
			this.GroupFooter1.Height = 0.2604167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 0.0625F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 6.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.375F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.15625F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.40625F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 1.125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtMuniName.Text = "t";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.0625F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3333333F;
			this.DLG1.Left = 0.0625F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0.04166667F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.3333333F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 0.03125F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field1.Text = "Description";
			this.Field1.Top = 0.7083333F;
			this.Field1.Width = 2.65625F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 1.75F;
			this.Field2.MultiLine = false;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field2.Text = "Year";
			this.Field2.Top = 0.7083333F;
			this.Field2.Width = 0.5F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1666667F;
			this.Field3.Left = 2.25F;
			this.Field3.MultiLine = false;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field3.Text = "Make";
			this.Field3.Top = 0.7083333F;
			this.Field3.Width = 0.59375F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1666667F;
			this.Field4.Left = 3.34375F;
			this.Field4.MultiLine = false;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field4.Text = "Model";
			this.Field4.Top = 0.7083333F;
			this.Field4.Width = 0.625F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.19F;
			this.Field5.Left = 5.28125F;
			this.Field5.MultiLine = false;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field5.Text = "Amount";
			this.Field5.Top = 0.71875F;
			this.Field5.Width = 0.71875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.19F;
			this.Field6.Left = 6.09375F;
			this.Field6.MultiLine = false;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field6.Text = "Units";
			this.Field6.Top = 0.71875F;
			this.Field6.Width = 0.6875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 2F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.875F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.875F;
			this.Line1.Y2 = 0.875F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1666667F;
			this.Field7.Left = 0.15625F;
			this.Field7.MultiLine = false;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field7.Text = "Account";
			this.Field7.Top = 0.7083333F;
			this.Field7.Visible = false;
			this.Field7.Width = 1.0625F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1666667F;
			this.Field8.Left = 1.96875F;
			this.Field8.MultiLine = false;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.Field8.Text = "Amount";
			this.Field8.Top = 0.7083333F;
			this.Field8.Visible = false;
			this.Field8.Width = 1.0625F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1666667F;
			this.Field9.Left = 3.21875F;
			this.Field9.MultiLine = false;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Field9.Text = "Description";
			this.Field9.Top = 0.7083333F;
			this.Field9.Visible = false;
			this.Field9.Width = 1.0625F;
			// 
			// txtDepreciationDate
			// 
			this.txtDepreciationDate.Height = 0.2083333F;
			this.txtDepreciationDate.Left = 0.0625F;
			this.txtDepreciationDate.MultiLine = false;
			this.txtDepreciationDate.Name = "txtDepreciationDate";
			this.txtDepreciationDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
			this.txtDepreciationDate.Text = null;
			this.txtDepreciationDate.Top = 0.2916667F;
			this.txtDepreciationDate.Width = 6.21875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.19F;
			this.Field12.Left = 4.375F;
			this.Field12.MultiLine = false;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: left";
			this.Field12.Text = "Tag #";
			this.Field12.Top = 0.71875F;
			this.Field12.Width = 0.71875F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.2083333F;
			this.txtDescription.Left = 0F;
			this.txtDescription.MultiLine = false;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.71875F;
			// 
			// txtYear
			// 
			this.txtYear.Height = 0.2083333F;
			this.txtYear.Left = 1.75F;
			this.txtYear.MultiLine = false;
			this.txtYear.Name = "txtYear";
			this.txtYear.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtYear.Text = null;
			this.txtYear.Top = 0F;
			this.txtYear.Width = 0.46875F;
			// 
			// txtMake
			// 
			this.txtMake.Height = 0.2083333F;
			this.txtMake.Left = 2.25F;
			this.txtMake.MultiLine = false;
			this.txtMake.Name = "txtMake";
			this.txtMake.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtMake.Text = null;
			this.txtMake.Top = 0F;
			this.txtMake.Width = 1.0625F;
			// 
			// txtModel
			// 
			this.txtModel.Height = 0.2083333F;
			this.txtModel.Left = 3.34375F;
			this.txtModel.MultiLine = false;
			this.txtModel.Name = "txtModel";
			this.txtModel.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtModel.Text = null;
			this.txtModel.Top = 0F;
			this.txtModel.Width = 0.84375F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.21875F;
			this.txtAmount.Left = 5.125F;
			this.txtAmount.MultiLine = false;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.OutputFormat = resources.GetString("txtAmount.OutputFormat");
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 0.90625F;
			// 
			// txtUnits
			// 
			this.txtUnits.Height = 0.21875F;
			this.txtUnits.Left = 6.09375F;
			this.txtUnits.MultiLine = false;
			this.txtUnits.Name = "txtUnits";
			this.txtUnits.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.txtUnits.Text = null;
			this.txtUnits.Top = 0F;
			this.txtUnits.Width = 0.6875F;
			// 
			// txtUnitsDesc
			// 
			this.txtUnitsDesc.Height = 0.21875F;
			this.txtUnitsDesc.Left = 6.84375F;
			this.txtUnitsDesc.MultiLine = false;
			this.txtUnitsDesc.Name = "txtUnitsDesc";
			this.txtUnitsDesc.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left";
			this.txtUnitsDesc.Text = null;
			this.txtUnitsDesc.Top = 0F;
			this.txtUnitsDesc.Width = 0.78125F;
			// 
			// fldTag
			// 
			this.fldTag.Height = 0.21875F;
			this.fldTag.Left = 4.375F;
			this.fldTag.MultiLine = false;
			this.fldTag.Name = "fldTag";
			this.fldTag.OutputFormat = resources.GetString("fldTag.OutputFormat");
			this.fldTag.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.fldTag.Text = null;
			this.fldTag.Top = 0F;
			this.fldTag.Width = 0.71875F;
			// 
			// txtAmountSum
			// 
			this.txtAmountSum.Height = 0.19F;
			this.txtAmountSum.Left = 5.125F;
			this.txtAmountSum.Name = "txtAmountSum";
			this.txtAmountSum.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtAmountSum.Text = null;
			this.txtAmountSum.Top = 0.09375F;
			this.txtAmountSum.Width = 0.90625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.19F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 4.75F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.Label1.Text = "Total";
			this.Label1.Top = 0.09375F;
			this.Label1.Width = 0.34375F;
			// 
			// txtUnitsTotal
			// 
			this.txtUnitsTotal.Height = 0.19F;
			this.txtUnitsTotal.Left = 6.09375F;
			this.txtUnitsTotal.Name = "txtUnitsTotal";
			this.txtUnitsTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtUnitsTotal.Text = null;
			this.txtUnitsTotal.Top = 0.09375F;
			this.txtUnitsTotal.Width = 0.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.75F;
			this.Line2.LineWeight = 2F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.09375F;
			this.Line2.Width = 2.6875F;
			this.Line2.X1 = 4.75F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.09375F;
			this.Line2.Y2 = 0.09375F;
			// 
			// lblRecordNumber
			// 
			this.lblRecordNumber.Height = 0.1666667F;
			this.lblRecordNumber.HyperLink = null;
			this.lblRecordNumber.Left = 1.21875F;
			this.lblRecordNumber.Name = "lblRecordNumber";
			this.lblRecordNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblRecordNumber.Text = null;
			this.lblRecordNumber.Top = 0.08333334F;
			this.lblRecordNumber.Width = 2.78125F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.375F;
			this.SubReport1.Left = 0.0625F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 0.04166667F;
			this.SubReport1.Width = 6.28125F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.2083333F;
			this.Field10.Left = 2.25F;
			this.Field10.MultiLine = false;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.Field10.Text = "Journal Number:";
			this.Field10.Top = 0.6666667F;
			this.Field10.Width = 0.875F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.2083333F;
			this.Field11.Left = 2.15625F;
			this.Field11.MultiLine = false;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.Field11.Text = "Accounting Period:";
			this.Field11.Top = 0.875F;
			this.Field11.Width = 0.96875F;
			// 
			// txtJournalNumber
			// 
			this.txtJournalNumber.Height = 0.2083333F;
			this.txtJournalNumber.Left = 3.125F;
			this.txtJournalNumber.MultiLine = false;
			this.txtJournalNumber.Name = "txtJournalNumber";
			this.txtJournalNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtJournalNumber.Text = null;
			this.txtJournalNumber.Top = 0.6666667F;
			this.txtJournalNumber.Width = 2.125F;
			// 
			// txtPeriod
			// 
			this.txtPeriod.Height = 0.2083333F;
			this.txtPeriod.Left = 3.125F;
			this.txtPeriod.MultiLine = false;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtPeriod.Text = null;
			this.txtPeriod.Top = 0.875F;
			this.txtPeriod.Width = 1.0625F;
			// 
			// subCostSummary
			// 
			this.subCostSummary.CloseBorder = false;
			this.subCostSummary.Height = 0.125F;
			this.subCostSummary.Left = 0F;
			this.subCostSummary.Name = "subCostSummary";
			this.subCostSummary.Report = null;
			this.subCostSummary.Top = 1.3125F;
			this.subCostSummary.Width = 6.59375F;
			// 
			// rptDepreciation
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.MirrorMargins = true;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.645833F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepreciationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMake)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtModel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitsDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmountSum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUnitsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRecordNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournalNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMake;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtModel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnitsDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTag;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJournalNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport subCostSummary;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepreciationDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmountSum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUnitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRecordNumber;
	}
}
