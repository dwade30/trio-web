﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmDepreciation.
	/// </summary>
	partial class frmDepreciation : BaseForm
	{
		public fecherFoundation.FCTextBox txtTotal;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCMaskedTextBox mskDepreciationDate;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCLabel lblDateLastDepreciation;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuIgnore;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.txtTotal = new fecherFoundation.FCTextBox();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.mskDepreciationDate = new fecherFoundation.FCMaskedTextBox();
			this.lblTotal = new fecherFoundation.FCLabel();
			this.lblDateLastDepreciation = new fecherFoundation.FCLabel();
			this.cmdIgnore = new fecherFoundation.FCButton();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuIgnore = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdPrintPreview = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdIgnore)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 624);
			this.BottomPanel.Size = new System.Drawing.Size(1006, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtTotal);
			this.ClientArea.Controls.Add(this.vsGrid);
			this.ClientArea.Controls.Add(this.mskDepreciationDate);
			this.ClientArea.Controls.Add(this.lblTotal);
			this.ClientArea.Controls.Add(this.lblDateLastDepreciation);
			this.ClientArea.Size = new System.Drawing.Size(1006, 564);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdPrintPreview);
			this.TopPanel.Controls.Add(this.cmdIgnore);
			this.TopPanel.Size = new System.Drawing.Size(1006, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdIgnore, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(151, 30);
			this.HeaderText.Text = "Depreciation";
			// 
			// txtTotal
			// 
			this.txtTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.txtTotal.AutoSize = false;
			this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.CharacterCasing = CharacterCasing.Upper;
            this.txtTotal.Enabled = false;
			this.txtTotal.LinkItem = null;
			this.txtTotal.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotal.LinkTopic = null;
			this.txtTotal.Location = new System.Drawing.Point(836, 30);
			this.txtTotal.LockedOriginal = true;
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.ReadOnly = true;
			this.txtTotal.Size = new System.Drawing.Size(140, 40);
			this.txtTotal.TabIndex = 4;
			this.txtTotal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// vsGrid
			// 
			this.vsGrid.AllowSelection = false;
			this.vsGrid.AllowUserToResizeColumns = false;
			this.vsGrid.AllowUserToResizeRows = false;
			this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrid.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsGrid.ColumnHeadersHeight = 30;
			this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsGrid.DragIcon = null;
			this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.GridColor = System.Drawing.Color.Empty;
			this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.Location = new System.Drawing.Point(30, 85);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.ReadOnly = true;
			this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrid.RowHeightMin = 0;
			this.vsGrid.Rows = 50;
			this.vsGrid.ScrollTipText = null;
			this.vsGrid.ShowColumnVisibilityMenu = false;
			this.vsGrid.Size = new System.Drawing.Size(946, 450);
			this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsGrid.TabIndex = 2;
			this.vsGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsGrid_BeforeEdit);
			this.vsGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsGrid_AfterEdit);
			this.vsGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsGrid_ValidateEdit);
			this.vsGrid.CurrentCellChanged += new System.EventHandler(this.vsGrid_RowColChange);
			this.vsGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.vsGrid_KeyDownEvent);
			this.vsGrid.Enter += new System.EventHandler(this.vsGrid_Enter);
			this.vsGrid.Click += new System.EventHandler(this.vsGrid_ClickEvent);
			// 
			// mskDepreciationDate
			// 
			this.mskDepreciationDate.Location = new System.Drawing.Point(234, 30);
			this.mskDepreciationDate.Mask = "##/##/####";
			this.mskDepreciationDate.MaxLength = 10;
			this.mskDepreciationDate.Name = "mskDepreciationDate";
			this.mskDepreciationDate.Size = new System.Drawing.Size(134, 40);
			this.mskDepreciationDate.TabIndex = 1;
			this.mskDepreciationDate.Text = "  /  /";
			this.mskDepreciationDate.Enter += new System.EventHandler(this.mskDepreciationDate_Enter);
			// 
			// lblTotal
			// 
			this.lblTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.lblTotal.Location = new System.Drawing.Point(731, 44);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Size = new System.Drawing.Size(70, 18);
			this.lblTotal.TabIndex = 3;
			this.lblTotal.Text = "TOTAL";
			this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblDateLastDepreciation
			// 
			this.lblDateLastDepreciation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
			this.lblDateLastDepreciation.Location = new System.Drawing.Point(30, 44);
			this.lblDateLastDepreciation.Name = "lblDateLastDepreciation";
			this.lblDateLastDepreciation.Size = new System.Drawing.Size(143, 20);
			this.lblDateLastDepreciation.TabIndex = 0;
			this.lblDateLastDepreciation.Text = "DEPRECIATION DATE";
			this.lblDateLastDepreciation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdIgnore
			// 
			this.cmdIgnore.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdIgnore.AppearanceKey = "toolbarButton";
			this.cmdIgnore.Location = new System.Drawing.Point(920, 29);
			this.cmdIgnore.Name = "cmdIgnore";
			this.cmdIgnore.Size = new System.Drawing.Size(56, 24);
			this.cmdIgnore.TabIndex = 13;
			this.cmdIgnore.Text = "Ignore";
			this.cmdIgnore.Click += new System.EventHandler(this.cmdIgnore_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(465, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(116, 48);
			this.cmdProcess.TabIndex = 3;
			this.cmdProcess.Text = "Calculate";
			this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuNew,
				this.mnuDelete,
				this.mnuIgnore,
				this.mnuProcess,
				this.mnuSP2,
				this.mnuPrint,
				this.mnuPrintPreview,
				this.mnuSave,
				this.mnuSaveExit,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuNew.Text = "New";
			this.mnuNew.Visible = false;
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Visible = false;
			// 
			// mnuIgnore
			// 
			this.mnuIgnore.Index = 2;
			this.mnuIgnore.Name = "mnuIgnore";
			this.mnuIgnore.Text = "Ignore All Items";
			this.mnuIgnore.Click += new System.EventHandler(this.mnuIgnore_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 3;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcess.Text = "Calculate";
			this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 4;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			this.mnuSP2.Visible = false;
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 5;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuPrintPreview
			// 
			this.mnuPrintPreview.Index = 6;
			this.mnuPrintPreview.Name = "mnuPrintPreview";
			this.mnuPrintPreview.Text = "Print Preview";
			this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 7;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                      ";
			this.mnuSave.Visible = false;
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 8;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.CtrlF2;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Visible = false;
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 9;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 10;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdPrintPreview
			// 
			this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintPreview.AppearanceKey = "toolbarButton";
			this.cmdPrintPreview.Location = new System.Drawing.Point(819, 29);
			this.cmdPrintPreview.Name = "cmdPrintPreview";
			this.cmdPrintPreview.Size = new System.Drawing.Size(97, 24);
			this.cmdPrintPreview.TabIndex = 15;
			this.cmdPrintPreview.Text = "Print Preview";
			this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(771, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(44, 24);
			this.cmdPrint.TabIndex = 16;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// frmDepreciation
			// 
			this.ClientSize = new System.Drawing.Size(1006, 720);
			this.KeyPreview = true;
			this.Name = "frmDepreciation";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Depreciation";
			this.Load += new System.EventHandler(this.frmDepreciation_Load);
			this.Activated += new System.EventHandler(this.frmDepreciation_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDepreciation_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDepreciation_KeyPress);
			this.Resize += new System.EventHandler(this.frmDepreciation_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdIgnore)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdIgnore;
		private FCButton cmdProcess;
		private FCButton cmdPrint;
		private FCButton cmdPrintPreview;
	}
}
