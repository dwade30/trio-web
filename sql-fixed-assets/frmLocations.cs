﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmLocations.
	/// </summary>
	public partial class frmLocations : BaseForm
	{
		public frmLocations()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLocations InstancePtr
		{
			get
			{
				return (frmLocations)Sys.GetInstance(typeof(frmLocations));
			}
		}

		protected frmLocations _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/16/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		string strSort = "";
		bool boolDataChanged;
		int intType;
		const string GRIDCOLUMN0 = "ID";
		const string GRIDCOLUMN1 = "Changed";
		const string GRIDCOLUMN2 = "TypeID";
		// Location 1 or Location 2 or Location 3 or Location 4
		const string GRIDCOLUMN3 = "Code";
		const string GRIDCOLUMN4 = "Description";

		private void SetGridProperties()
		{
			LocationGrid.Cols = 5;
			LocationGrid.FixedCols = 0;
			LocationGrid.FixedRows = 1;
			LocationGrid.Rows = 1;
			LocationGrid.ColHidden(0, true);
			LocationGrid.ColHidden(1, true);
			LocationGrid.ColHidden(2, true);
			LocationGrid.ColWidth(3, 1300);
			LocationGrid.ColWidth(4, 420);
			//LocationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, LocationGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			LocationGrid.TextMatrix(0, 0, GRIDCOLUMN0);
			LocationGrid.TextMatrix(0, 1, GRIDCOLUMN1);
			LocationGrid.TextMatrix(0, 2, GRIDCOLUMN2);
			LocationGrid.TextMatrix(0, 3, GRIDCOLUMN3);
			LocationGrid.TextMatrix(0, 4, GRIDCOLUMN4);
			LocationGrid.Select(0, 0);
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboType.SelectedIndex >= 0)
			{
				intType = cboType.ItemData(cboType.SelectedIndex);
				LoadRecord();
			}
			else
			{
				LocationGrid.Rows = 1;
			}
		}

		private void cboType_DropDown(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							if (SaveRecord())
							{
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							Close();
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							break;
						}
				}
				//end switch
			}
		}

		private void frmLocations_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
			if (cboType.Items.Count == 0)
			{
				MessageBox.Show("No types have been set up. Enter location types in the customize screen on the file maintenance menu.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Close();
			}
		}

		private void frmLocations_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
				mnuNew_Click();
		}

		private void frmLocations_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl.GetName() != "LocationGrid")
				{
					KeyAscii = (Keys)0;
					Support.SendKeys("{TAB}", false);
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmLocations_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLocations.ScaleWidth	= 5880;
			//frmLocations.ScaleHeight	= 4245;
			//frmLocations.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetGridProperties();
			LoadTypes();
			if (cboType.Items.Count > 0)
				cboType.SelectedIndex = 0;
			LoadRecord();
			LocationGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			LocationGrid.Select(0, 0);
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							modVariables.Statics.gboolCancelSelected = false;
							if (SaveRecord())
							{
								e.Cancel = false;
							}
							else
							{
								e.Cancel = true;
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							modVariables.Statics.gboolCancelSelected = false;
							Close();
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							modVariables.Statics.gboolCancelSelected = true;
							e.Cancel = true;
							break;
						}
				}
				//end switch
			}
			else
			{
				Close();
			}
		}

		private void frmLocations_Resize(object sender, System.EventArgs e)
		{
			LocationGrid.ColWidth(3, FCConvert.ToInt32(LocationGrid.WidthOriginal * 0.13));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void LocationGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			LocationGrid.TextMatrix(LocationGrid.Row, 1, FCConvert.ToString(1));
			boolDataChanged = true;
		}

		private void LocationGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (LocationGrid.MouseRow == 0)
			{
				// sort this grid
				if (LocationGrid.Rows > 1)
				{
					LocationGrid.Select(1, LocationGrid.Col);
					if (strSort == "flexSortGenericAscending")
					{
						LocationGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						strSort = "flexSortGenericDescending";
					}
					else
					{
						LocationGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						strSort = "flexSortGenericAscending";
					}
				}
			}
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					(obj as MaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (FCConvert.ToBoolean(boolClear))
						(obj as FCComboBox).Clear();
				}
			}
			// obj
			LocationGrid.Rows = 1;
			tmpString = new object[3 + 1];
		}

		private bool SaveRecord()
		{
			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCounter = 1; intCounter <= LocationGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(LocationGrid.TextMatrix(intCounter, 0)) != 0)
					{
						if (Strings.Trim(LocationGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a location code before you may proceed.", "Invalid Location Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							LocationGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(LocationGrid.TextMatrix(intCounter, 4)) == "")
						{
							MessageBox.Show("You must enter a location description before you may proceed.", "Invalid Location Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							LocationGrid.Select(intCounter, 4);
							SaveRecord = false;
							return SaveRecord;
						}
					}
					else
					{
						if (Strings.Trim(LocationGrid.TextMatrix(intCounter, 3)) == "" && Strings.Trim(LocationGrid.TextMatrix(intCounter, 4)) == "")
						{
							// do nothing
						}
						else if (Strings.Trim(LocationGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a location code before you may proceed.", "Invalid Location Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							LocationGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(LocationGrid.TextMatrix(intCounter, 4)) == "")
						{
							MessageBox.Show("You must enter a location description before you may proceed.", "Invalid Location Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							LocationGrid.Select(intCounter, 4);
							SaveRecord = false;
							return SaveRecord;
						}
					}
				}
				for (intCounter = 1; intCounter <= LocationGrid.Rows - 1; intCounter++)
				{
					if (Conversion.Val(LocationGrid.TextMatrix(intCounter, 1)) == 1)
					{
						// then this record has changed
						if (FCConvert.ToDouble(LocationGrid.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							if (Strings.Trim(LocationGrid.TextMatrix(intCounter, 3)) == "" && Strings.Trim(LocationGrid.TextMatrix(intCounter, 4)) == "")
							{
								// do nothing
							}
							else
							{
								rsData.Execute("Insert into tblLocations (TypeID, Code,Description) VALUES (" + FCConvert.ToString(intType) + ",'" + modGlobalRoutines.FixQuote(LocationGrid.TextMatrix(intCounter, 3)) + "','" + modGlobalRoutines.FixQuote(LocationGrid.TextMatrix(intCounter, 4)) + "')", modGlobal.DatabaseNamePath);
							}
						}
						else
						{
							// this is an edit record
							rsData.OpenRecordset("Select * from tblLocations where ID = " + LocationGrid.TextMatrix(intCounter, 0), modGlobal.DatabaseNamePath);
							if (rsData.EndOfFile())
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
							rsData.Set_Fields("TypeID", intType);
							rsData.Set_Fields("Code", Strings.Trim(modGlobalRoutines.FixQuote(LocationGrid.TextMatrix(intCounter, 3))));
							rsData.Set_Fields("Description", Strings.Trim(modGlobalRoutines.FixQuote(LocationGrid.TextMatrix(intCounter, 4))));
							rsData.Update();
						}
					}
				}
				if (boolDataChanged)
					LoadRecord();
				SaveRecord = true;
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveRecord;
		}

		private void LoadRecord()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			LocationGrid.Rows = 1;
			// this is an edit record
			rsData.OpenRecordset("Select * from tblLocations where TypeID = " + FCConvert.ToString(intType) + " Order by Code", modGlobal.DatabaseNamePath);
			if (rsData.EndOfFile())
			{
				LocationGrid.Rows = 1;
			}
			else
			{
				LocationGrid.Rows = rsData.RecordCount() + 1;
				for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
				{
					LocationGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					LocationGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
					LocationGrid.TextMatrix(intCounter, 2, FCConvert.ToString(intType));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					LocationGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))));
					LocationGrid.TextMatrix(intCounter, 4, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Description"))));
					rsData.MoveNext();
				}
			}
			if (LocationGrid.Rows > 1)
			{
				LocationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, LocationGrid.Rows - 1, LocationGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			boolDataChanged = false;
		}

		private void LocationGrid_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (LocationGrid.Row == LocationGrid.Rows - 1 && LocationGrid.Col == LocationGrid.Cols - 1)
				{
					keyAscii = 0;
					mnuNew_Click();
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void LocationGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (LocationGrid.Row == LocationGrid.Rows - 1 && LocationGrid.Col == LocationGrid.Cols - 1)
				{
					keyAscii = 0;
					mnuNew_Click();
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void LocationGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (LocationGrid.Row == LocationGrid.Rows - 1 && LocationGrid.Col == LocationGrid.Cols - 1)
			{
				LocationGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				LocationGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			// If LocationGrid.Col = 3 Then
			// LocationGrid.EditMaxLength = 5
			// ElseIf LocationGrid.Col = 4 Then
			// LocationGrid.EditMaxLength = 35
			// Else
			// LocationGrid.EditMaxLength = 0
			// End If
		}

		private void LocationGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = LocationGrid.GetFlexRowIndex(e.RowIndex);
			int col = LocationGrid.GetFlexColIndex(e.ColumnIndex);
			if (modGlobalRoutines.DuplicateFound(3, row, LocationGrid, Strings.Trim(LocationGrid.EditText)))
			{
				MessageBox.Show("This location code already exists.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				LocationGrid.TextMatrix(row, 3, string.Empty);
				e.Cancel = true;
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			int intRow = 0;
			int intCol = 0;
			if (LocationGrid.Row > 0)
			{
				intRow = LocationGrid.Row;
				intCol = LocationGrid.Col;
				if (MessageBox.Show("Are you sure you wish to delete this item?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (Conversion.Val(LocationGrid.TextMatrix(LocationGrid.Row, 0)) == 0)
					{
						LocationGrid.RemoveItem(LocationGrid.Row);
					}
					else
					{
						rsData.Execute("Delete from tblLocations where ID = " + LocationGrid.TextMatrix(LocationGrid.Row, 0), modGlobal.DatabaseNamePath);
						LocationGrid.RemoveItem(LocationGrid.Row);
					}
					if (intRow <= LocationGrid.Rows - 1)
					{
						LocationGrid.Select(intRow, intCol);
					}
					else if (LocationGrid.Rows > 1)
					{
						LocationGrid.Select(intRow - 1, intCol);
					}
					MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			LocationGrid.Rows += 1;
			LocationGrid.TextMatrix(LocationGrid.Rows - 1, 0, FCConvert.ToString(0));
			LocationGrid.TextMatrix(LocationGrid.Rows - 1, 1, FCConvert.ToString(1));
			LocationGrid.Select(LocationGrid.Rows - 1, 3);
			LocationGrid.Focus();
			LocationGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, LocationGrid.Rows - 1, 0, LocationGrid.Rows - 1, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			LocationGrid.Select(0, 0);
			SaveRecord();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			LocationGrid.Select(0, 0);
			if (SaveRecord())
				Close();
		}

		private void LoadTypes()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblSettings", modGlobal.DatabaseNamePath);
			if (!rsData.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location1Caption"))) == string.Empty)
				{
					// do nothing
				}
				else
				{
					cboType.AddItem(rsData.Get_Fields_String("Location1Caption"));
					cboType.ItemData(cboType.NewIndex, 1);
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location2Caption"))) == string.Empty)
				{
					// do nothing
				}
				else
				{
					cboType.AddItem(rsData.Get_Fields_String("Location2Caption"));
					cboType.ItemData(cboType.NewIndex, 2);
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location3Caption"))) == string.Empty)
				{
					// do nothing
				}
				else
				{
					cboType.AddItem(rsData.Get_Fields_String("Location3Caption"));
					cboType.ItemData(cboType.NewIndex, 3);
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location4Caption"))) == string.Empty)
				{
					// do nothing
				}
				else
				{
					cboType.AddItem(rsData.Get_Fields_String("Location4Caption"));
					cboType.ItemData(cboType.NewIndex, 4);
				}
			}
			else
			{
				cboType.AddItem("Location 1");
				cboType.ItemData(cboType.NewIndex, 1);
				cboType.AddItem("Location 2");
				cboType.ItemData(cboType.NewIndex, 2);
				cboType.AddItem("Location 3");
				cboType.ItemData(cboType.NewIndex, 3);
				cboType.AddItem("Location 4");
				cboType.ItemData(cboType.NewIndex, 4);
			}
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			mnuDelete_Click(sender, e);
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuNew_Click();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(sender, e);
		}
	}
}
