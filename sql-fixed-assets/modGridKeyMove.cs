﻿//Fecher vbPorter - Version 1.0.0.27
using Wisej.Web;
using Global;
using fecherFoundation;
using System.Drawing;
using System;

namespace TWFA0000
{
	public class modGridKeyMove
	{
		// not working yet
		// vbPorter upgrade warning: GridName As object	OnWrite(VSFlex7DAOCtl.VSFlexGrid)
		// vbPorter upgrade warning: KeyCode As short	OnWrite(short, Keys)
		public static void MoveGridProsition(FCGrid GridName, int intStartCol, int intEndCol, Keys KeyCode = 0, int KeyAsii = 9999, bool AddNewRecord = false)
		{
			Statics.gboolSkipKeyMove = false;
			if (KeyAsii == 9999)
			{
				if (KeyCode == Keys.Tab || KeyCode == Keys.Return)
				{
					KeyCode = Keys.Right;
				}
				Statics.intKeyValue = FCConvert.ToInt32(KeyCode);
			}
			else
			{
				Statics.intKeyValue = KeyAsii;
			}
			if (intEndCol > GridName.Cols - 1)
			{
				intEndCol = GridName.Cols - 1;
			}
			switch (Statics.intKeyValue)
			{
				case 37:
					{
						MoveLeftAgain:
						;
						// left key
						if (GridName.Col == intStartCol)
						{
							if (GridName.Row > GridName.FixedRows)
							{
								if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row - 1, intEndCol) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
								{
									// the next column we want to be disabled
									if (intEndCol == intStartCol)
									{
										// there is no more columns to go to so
										// stay where you are
										KeyCode = 0;
									}
									else
									{
										Statics.gboolSkipKeyMove = true;
										GridName.Row -= 1;
										GridName.Col = intEndCol;
										goto MoveLeftAgain;
									}
								}
								else
								{
									GridName.Row -= 1;
									GridName.Col = intEndCol;
									KeyCode = 0;
								}
							}
						}
						else
						{
							if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row, GridName.Col - 1) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
							{
								// the next column we want to be disabled
								if (GridName.Col - 1 < intStartCol)
								{
									// there is no more columns to go to so
									// stay where you are
									KeyCode = 0;
								}
								else
								{
									Statics.gboolSkipKeyMove = true;
									GridName.Col -= 1;
									goto MoveLeftAgain;
								}
							}
						}
						break;
					}
				case 38:
					{
						MoveUpAgain:
						;
						// Up key
						if (GridName.Row > 0)
						{
							if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row - 1, GridName.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
							{
								// the previous row we want to be disabled
								if (GridName.Row - 1 < GridName.FixedRows)
								{
									// there is no more rows to go to so
									// stay where you are
									KeyCode = 0;
								}
								else
								{
									Statics.gboolSkipKeyMove = true;
									GridName.Row -= 1;
									goto MoveUpAgain;
								}
							}
						}
						break;
					}
				case 39:
					{
						MoveRightAgain:
						;
						// right key
						if (GridName.Col == intEndCol)
						{
							if (GridName.Row < GridName.Rows - 1)
							{
								if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row + 1, intStartCol) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
								{
									// the next column we want to be disabled
									if (intEndCol == intStartCol)
									{
										// there is no more columns to go to so
										// stay where you are
										KeyCode = 0;
									}
									else
									{
										Statics.gboolSkipKeyMove = true;
										GridName.Row += 1;
										GridName.Col = intStartCol;
										goto MoveRightAgain;
									}
								}
								else
								{
									GridName.Row += 1;
									GridName.Col = intStartCol;
									KeyCode = 0;
								}
							}
							else
							{
								if (AddNewRecord)
								{
									FCUtils.CallByName(GridName.FindForm(), "AddNewGridRecord", CallType.Method);
									KeyCode = 0;
								}
								else
								{
									PreviousRow:
									;
									// find the last row that was legal
									for (Statics.intCounter = GridName.Col; Statics.intCounter >= intStartCol; Statics.intCounter--)
									{
										if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row, Statics.intCounter) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
										{
											GridName.Col = Statics.intCounter - 1;
											break;
										}
									}
									if (Statics.intCounter == 1)
									{
										if (GridName.Row > GridName.FixedRows)
										{
											GridName.Row -= 1;
											GridName.Col = intEndCol;
											goto PreviousRow;
										}
									}
								}
							}
						}
						else
						{
							if (GridName.ColHidden(GridName.Col + 1))
							{
								Statics.gboolSkipKeyMove = true;
								GridName.Col += 1;
								goto MoveRightAgain;
							}
							else
							{
								if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row, GridName.Col + 1) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
								{
									// the next column we want to be disabled
									if (GridName.Col + 1 > intEndCol)
									{
										// there is no more columns to go to so
										// stay where you are
										KeyCode = 0;
									}
									else
									{
										Statics.gboolSkipKeyMove = true;
										GridName.Col += 1;
										goto MoveRightAgain;
									}
								}
							}
						}
						break;
					}
				case 40:
					{
						MoveDownAgain:
						;
						// down key
						if (GridName.Row + 1 >= GridName.Rows)
						{
							if (AddNewRecord)
							{
								FCUtils.CallByName(GridName.FindForm(), "AddNewGridRecord", CallType.Method);
							}
							else
							{
								// find the last row that was legal
								for (Statics.intCounter = GridName.Row; Statics.intCounter >= GridName.FixedRows; Statics.intCounter--)
								{
									if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Statics.intCounter, GridName.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
									{
										GridName.Row = Statics.intCounter - 1;
										break;
									}
								}
							}
						}
					//FC:FINAL:MSH - can't apply '==' operator to int and Color (same with i.issue #1442)
					else if (GridName.Cell(FCGrid.CellPropertySettings.flexcpBackColor, GridName.Row + 1, GridName.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							// the next row we want to be disabled
							if (GridName.Row + 1 >= GridName.Rows)
							{
								// there is no more rows to go to so
								// stay where you are
								KeyCode = 0;
							}
							else
							{
								Statics.gboolSkipKeyMove = true;
								GridName.Row += 1;
								goto MoveDownAgain;
							}
						}
						break;
					}
			}
			//end switch
		}

		public class StaticVariables
		{
			//=========================================================
			public int intCounter;
			public int intKeyValue;
			public bool gboolSkipKeyMove;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
