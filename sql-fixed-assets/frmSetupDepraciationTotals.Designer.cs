﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using GrapeCity.ActiveReports;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSetupDepraciationTotals.
	/// </summary>
	partial class frmSetupDepraciationTotals : BaseForm
	{
		public fecherFoundation.FCLabel lblAllDate;
		public fecherFoundation.FCComboBox cmbAllDate;
		public fecherFoundation.FCLabel lblAll;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCComboBox cmbDescription;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCCheckBox chkExcludeDiscarded;
		public fecherFoundation.FCCheckBox chkZeroCurDep;
		public fecherFoundation.FCCheckBox chkDep;
		public fecherFoundation.FCFrame fraDiscardedDateRange;
		public fecherFoundation.FCButton cmdDiscardedStartDate;
		public fecherFoundation.FCButton cmdDiscardedEndDate;
		public Global.T2KDateBox txtStartDiscardedDate;
		public Global.T2KDateBox txtEndDiscardedDate;
		public fecherFoundation.FCLabel lblToDiscarded;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCButton cmdStartDate;
		public fecherFoundation.FCButton cmdEndDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCFrame fraPurchaseDateRange;
		public fecherFoundation.FCButton cmdPurchaseEndDate;
		public fecherFoundation.FCButton cmdPurchaseStartDate;
		public Global.T2KDateBox txtStartPurchaseDate;
		public Global.T2KDateBox txtEndPurchaseDate;
		public fecherFoundation.FCLabel lblToPurchase;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame fraSelected;
		public fecherFoundation.FCGrid vsClass;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupDepraciationTotals));
            this.lblAllDate = new fecherFoundation.FCLabel();
            this.cmbAllDate = new fecherFoundation.FCComboBox();
            this.lblAll = new fecherFoundation.FCLabel();
            this.cmbAll = new fecherFoundation.FCComboBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.cmbDescription = new fecherFoundation.FCComboBox();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.chkExcludeDiscarded = new fecherFoundation.FCCheckBox();
            this.fraDiscardedDateRange = new fecherFoundation.FCFrame();
            this.cmdDiscardedStartDate = new fecherFoundation.FCButton();
            this.cmdDiscardedEndDate = new fecherFoundation.FCButton();
            this.txtStartDiscardedDate = new Global.T2KDateBox();
            this.txtEndDiscardedDate = new Global.T2KDateBox();
            this.lblToDiscarded = new fecherFoundation.FCLabel();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.cmdStartDate = new fecherFoundation.FCButton();
            this.cmdEndDate = new fecherFoundation.FCButton();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.chkZeroCurDep = new fecherFoundation.FCCheckBox();
            this.fraPurchaseDateRange = new fecherFoundation.FCFrame();
            this.cmdPurchaseEndDate = new fecherFoundation.FCButton();
            this.cmdPurchaseStartDate = new fecherFoundation.FCButton();
            this.txtStartPurchaseDate = new Global.T2KDateBox();
            this.txtEndPurchaseDate = new Global.T2KDateBox();
            this.lblToPurchase = new fecherFoundation.FCLabel();
            this.chkDep = new fecherFoundation.FCCheckBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.fraSelected = new fecherFoundation.FCFrame();
            this.vsClass = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeDiscarded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDiscardedDateRange)).BeginInit();
            this.fraDiscardedDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDiscardedStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDiscardedEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDiscardedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDiscardedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkZeroCurDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPurchaseDateRange)).BeginInit();
            this.fraPurchaseDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurchaseEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurchaseStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartPurchaseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndPurchaseDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelected)).BeginInit();
            this.fraSelected.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 650);
            this.BottomPanel.Size = new System.Drawing.Size(998, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbDescription);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Size = new System.Drawing.Size(998, 590);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(225, 30);
            this.HeaderText.Text = "Depreciation Totals";
            // 
            // lblAllDate
            // 
            this.lblAllDate.AutoSize = true;
            this.lblAllDate.Location = new System.Drawing.Point(20, 44);
            this.lblAllDate.Name = "lblAllDate";
            this.lblAllDate.Size = new System.Drawing.Size(51, 15);
            this.lblAllDate.TabIndex = 10;
            this.lblAllDate.Text = "RANGE";
            this.lblAllDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbAllDate
            // 
            this.cmbAllDate.Items.AddRange(new object[] {
            "All",
            "Depreciation Date Range",
            "Purchase Date Range",
            "Discarded Date Range"});
            this.cmbAllDate.Location = new System.Drawing.Point(126, 30);
            this.cmbAllDate.Name = "cmbAllDate";
            this.cmbAllDate.Size = new System.Drawing.Size(237, 40);
            this.cmbAllDate.TabIndex = 1;
            this.cmbAllDate.SelectedIndexChanged += new System.EventHandler(this.cmbAllDate_SelectedIndexChanged);
            // 
            // lblAll
            // 
            this.lblAll.AutoSize = true;
            this.lblAll.Location = new System.Drawing.Point(20, 44);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(51, 15);
            this.lblAll.TabIndex = 3;
            this.lblAll.Text = "RANGE";
            this.lblAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbAll
            // 
            this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Selected"});
            this.cmbAll.Location = new System.Drawing.Point(129, 30);
            this.cmbAll.Name = "cmbAll";
            this.cmbAll.Size = new System.Drawing.Size(201, 40);
            this.cmbAll.TabIndex = 1;
            this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(503, 44);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(72, 15);
            this.lblDescription.TabIndex = 4;
            this.lblDescription.Text = "ORDER BY";
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbDescription
            // 
            this.cmbDescription.Items.AddRange(new object[] {
            "Description",
            "Class",
            "Dept./Div"});
            this.cmbDescription.Location = new System.Drawing.Point(613, 30);
            this.cmbDescription.Name = "cmbDescription";
            this.cmbDescription.Size = new System.Drawing.Size(201, 40);
            this.cmbDescription.TabIndex = 1;
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.chkExcludeDiscarded);
            this.Frame4.Controls.Add(this.cmbAllDate);
            this.Frame4.Controls.Add(this.lblAllDate);
            this.Frame4.Controls.Add(this.fraDiscardedDateRange);
            this.Frame4.Controls.Add(this.fraDateRange);
            this.Frame4.Controls.Add(this.chkZeroCurDep);
            this.Frame4.Controls.Add(this.fraPurchaseDateRange);
            this.Frame4.Controls.Add(this.chkDep);
            this.Frame4.Location = new System.Drawing.Point(30, 30);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(394, 524);
            this.Frame4.TabIndex = 2;
            this.Frame4.Text = "Date Range";
            // 
            // chkExcludeDiscarded
            // 
            this.chkExcludeDiscarded.Location = new System.Drawing.Point(20, 476);
            this.chkExcludeDiscarded.Name = "chkExcludeDiscarded";
            this.chkExcludeDiscarded.Size = new System.Drawing.Size(213, 27);
            this.chkExcludeDiscarded.TabIndex = 9;
            this.chkExcludeDiscarded.Text = "Exclude discarded assets";
            // 
            // fraDiscardedDateRange
            // 
            this.fraDiscardedDateRange.AppearanceKey = "groupBoxNoBorders";
            this.fraDiscardedDateRange.Controls.Add(this.cmdDiscardedStartDate);
            this.fraDiscardedDateRange.Controls.Add(this.cmdDiscardedEndDate);
            this.fraDiscardedDateRange.Controls.Add(this.txtStartDiscardedDate);
            this.fraDiscardedDateRange.Controls.Add(this.txtEndDiscardedDate);
            this.fraDiscardedDateRange.Controls.Add(this.lblToDiscarded);
            this.fraDiscardedDateRange.Location = new System.Drawing.Point(1, 295);
            this.fraDiscardedDateRange.Name = "fraDiscardedDateRange";
            this.fraDiscardedDateRange.Size = new System.Drawing.Size(373, 86);
            this.fraDiscardedDateRange.TabIndex = 4;
            // 
            // cmdDiscardedStartDate
            // 
            this.cmdDiscardedStartDate.AppearanceKey = "imageButton";
            this.cmdDiscardedStartDate.Enabled = false;
            this.cmdDiscardedStartDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdDiscardedStartDate.Location = new System.Drawing.Point(139, 30);
            this.cmdDiscardedStartDate.Name = "cmdDiscardedStartDate";
            this.cmdDiscardedStartDate.Size = new System.Drawing.Size(40, 40);
            this.cmdDiscardedStartDate.TabIndex = 1;
            this.cmdDiscardedStartDate.Click += new System.EventHandler(this.cmdDiscardedStartDate_Click);
            // 
            // cmdDiscardedEndDate
            // 
            this.cmdDiscardedEndDate.AppearanceKey = "imageButton";
            this.cmdDiscardedEndDate.Enabled = false;
            this.cmdDiscardedEndDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdDiscardedEndDate.Location = new System.Drawing.Point(333, 30);
            this.cmdDiscardedEndDate.Name = "cmdDiscardedEndDate";
            this.cmdDiscardedEndDate.Size = new System.Drawing.Size(40, 40);
            this.cmdDiscardedEndDate.TabIndex = 4;
            this.cmdDiscardedEndDate.Click += new System.EventHandler(this.cmdDiscardedEndDate_Click);
            // 
            // txtStartDiscardedDate
            // 
            this.txtStartDiscardedDate.Enabled = false;
            this.txtStartDiscardedDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartDiscardedDate.Mask = "##/##/####";
            this.txtStartDiscardedDate.MaxLength = 10;
            this.txtStartDiscardedDate.Name = "txtStartDiscardedDate";
            this.txtStartDiscardedDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDiscardedDate.TabIndex = 5;
            // 
            // txtEndDiscardedDate
            // 
            this.txtEndDiscardedDate.Enabled = false;
            this.txtEndDiscardedDate.Location = new System.Drawing.Point(219, 30);
            this.txtEndDiscardedDate.Mask = "##/##/####";
            this.txtEndDiscardedDate.MaxLength = 10;
            this.txtEndDiscardedDate.Name = "txtEndDiscardedDate";
            this.txtEndDiscardedDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDiscardedDate.TabIndex = 3;
            // 
            // lblToDiscarded
            // 
            this.lblToDiscarded.Enabled = false;
            this.lblToDiscarded.Location = new System.Drawing.Point(184, 44);
            this.lblToDiscarded.Name = "lblToDiscarded";
            this.lblToDiscarded.Size = new System.Drawing.Size(23, 20);
            this.lblToDiscarded.TabIndex = 2;
            this.lblToDiscarded.Text = "TO";
            this.lblToDiscarded.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraDateRange
            // 
            this.fraDateRange.AppearanceKey = "groupBoxNoBorders";
            this.fraDateRange.Controls.Add(this.cmdStartDate);
            this.fraDateRange.Controls.Add(this.cmdEndDate);
            this.fraDateRange.Controls.Add(this.txtStartDate);
            this.fraDateRange.Controls.Add(this.txtEndDate);
            this.fraDateRange.Controls.Add(this.lblTo);
            this.fraDateRange.Location = new System.Drawing.Point(1, 85);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(373, 90);
            this.fraDateRange.TabIndex = 2;
            // 
            // cmdStartDate
            // 
            this.cmdStartDate.AppearanceKey = "imageButton";
            this.cmdStartDate.Enabled = false;
            this.cmdStartDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdStartDate.Location = new System.Drawing.Point(139, 30);
            this.cmdStartDate.Name = "cmdStartDate";
            this.cmdStartDate.Size = new System.Drawing.Size(40, 40);
            this.cmdStartDate.TabIndex = 1;
            this.cmdStartDate.Click += new System.EventHandler(this.cmdStartDate_Click);
            // 
            // cmdEndDate
            // 
            this.cmdEndDate.AppearanceKey = "imageButton";
            this.cmdEndDate.Enabled = false;
            this.cmdEndDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdEndDate.Location = new System.Drawing.Point(333, 30);
            this.cmdEndDate.Name = "cmdEndDate";
            this.cmdEndDate.Size = new System.Drawing.Size(40, 40);
            this.cmdEndDate.TabIndex = 4;
            this.cmdEndDate.Click += new System.EventHandler(this.cmdEndDate_Click);
            // 
            // txtStartDate
            // 
            this.txtStartDate.Enabled = false;
            this.txtStartDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartDate.Mask = "##/##/####";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate.TabIndex = 5;
            // 
            // txtEndDate
            // 
            this.txtEndDate.Enabled = false;
            this.txtEndDate.Location = new System.Drawing.Point(219, 30);
            this.txtEndDate.Mask = "##/##/####";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate.TabIndex = 3;
            // 
            // lblTo
            // 
            this.lblTo.Enabled = false;
            this.lblTo.Location = new System.Drawing.Point(184, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(23, 20);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "TO";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkZeroCurDep
            // 
            this.chkZeroCurDep.Location = new System.Drawing.Point(20, 436);
            this.chkZeroCurDep.Name = "chkZeroCurDep";
            this.chkZeroCurDep.Size = new System.Drawing.Size(339, 27);
            this.chkZeroCurDep.TabIndex = 7;
            this.chkZeroCurDep.Text = "Include assets with no current depreciation";
            // 
            // fraPurchaseDateRange
            // 
            this.fraPurchaseDateRange.AppearanceKey = "groupBoxNoBorders";
            this.fraPurchaseDateRange.Controls.Add(this.cmdPurchaseEndDate);
            this.fraPurchaseDateRange.Controls.Add(this.cmdPurchaseStartDate);
            this.fraPurchaseDateRange.Controls.Add(this.txtStartPurchaseDate);
            this.fraPurchaseDateRange.Controls.Add(this.txtEndPurchaseDate);
            this.fraPurchaseDateRange.Controls.Add(this.lblToPurchase);
            this.fraPurchaseDateRange.Location = new System.Drawing.Point(1, 190);
            this.fraPurchaseDateRange.Name = "fraPurchaseDateRange";
            this.fraPurchaseDateRange.Size = new System.Drawing.Size(373, 90);
            this.fraPurchaseDateRange.TabIndex = 3;
            // 
            // cmdPurchaseEndDate
            // 
            this.cmdPurchaseEndDate.AppearanceKey = "imageButton";
            this.cmdPurchaseEndDate.Enabled = false;
            this.cmdPurchaseEndDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdPurchaseEndDate.Location = new System.Drawing.Point(333, 30);
            this.cmdPurchaseEndDate.Name = "cmdPurchaseEndDate";
            this.cmdPurchaseEndDate.Size = new System.Drawing.Size(40, 40);
            this.cmdPurchaseEndDate.TabIndex = 4;
            this.cmdPurchaseEndDate.Click += new System.EventHandler(this.cmdPurchaseEndDate_Click);
            // 
            // cmdPurchaseStartDate
            // 
            this.cmdPurchaseStartDate.AppearanceKey = "imageButton";
            this.cmdPurchaseStartDate.Enabled = false;
            this.cmdPurchaseStartDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdPurchaseStartDate.Location = new System.Drawing.Point(139, 30);
            this.cmdPurchaseStartDate.Name = "cmdPurchaseStartDate";
            this.cmdPurchaseStartDate.Size = new System.Drawing.Size(40, 40);
            this.cmdPurchaseStartDate.TabIndex = 1;
            this.cmdPurchaseStartDate.Click += new System.EventHandler(this.cmdPurchaseStartDate_Click);
            // 
            // txtStartPurchaseDate
            // 
            this.txtStartPurchaseDate.Enabled = false;
            this.txtStartPurchaseDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartPurchaseDate.Mask = "##/##/####";
            this.txtStartPurchaseDate.MaxLength = 10;
            this.txtStartPurchaseDate.Name = "txtStartPurchaseDate";
            this.txtStartPurchaseDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartPurchaseDate.TabIndex = 5;
            // 
            // txtEndPurchaseDate
            // 
            this.txtEndPurchaseDate.Enabled = false;
            this.txtEndPurchaseDate.Location = new System.Drawing.Point(219, 30);
            this.txtEndPurchaseDate.Mask = "##/##/####";
            this.txtEndPurchaseDate.MaxLength = 10;
            this.txtEndPurchaseDate.Name = "txtEndPurchaseDate";
            this.txtEndPurchaseDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndPurchaseDate.TabIndex = 3;
            // 
            // lblToPurchase
            // 
            this.lblToPurchase.Enabled = false;
            this.lblToPurchase.Location = new System.Drawing.Point(184, 44);
            this.lblToPurchase.Name = "lblToPurchase";
            this.lblToPurchase.Size = new System.Drawing.Size(23, 20);
            this.lblToPurchase.TabIndex = 2;
            this.lblToPurchase.Text = "TO";
            this.lblToPurchase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkDep
            // 
            this.chkDep.Location = new System.Drawing.Point(20, 396);
            this.chkDep.Name = "chkDep";
            this.chkDep.Size = new System.Drawing.Size(306, 27);
            this.chkDep.TabIndex = 5;
            this.chkDep.Text = "Include completely depreciated assets";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.fraSelected);
            this.Frame2.Controls.Add(this.cmbAll);
            this.Frame2.Controls.Add(this.lblAll);
            this.Frame2.Location = new System.Drawing.Point(484, 87);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(598, 426);
            this.Frame2.TabIndex = 3;
            this.Frame2.Text = "Classes To Report";
            // 
            // fraSelected
            // 
            this.fraSelected.AppearanceKey = "groupBoxNoBorders";
            this.fraSelected.Controls.Add(this.vsClass);
            this.fraSelected.Enabled = false;
            this.fraSelected.Location = new System.Drawing.Point(0, 79);
            this.fraSelected.Name = "fraSelected";
            this.fraSelected.Size = new System.Drawing.Size(570, 328);
            this.fraSelected.TabIndex = 2;
            // 
            // vsClass
            // 
            this.vsClass.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsClass.Cols = 3;
            this.vsClass.FixedCols = 0;
            this.vsClass.Location = new System.Drawing.Point(20, 20);
            this.vsClass.Name = "vsClass";
            this.vsClass.RowHeadersVisible = false;
            this.vsClass.Rows = 1;
            this.vsClass.Size = new System.Drawing.Size(550, 302);
            this.vsClass.Click += new System.EventHandler(this.vsClass_ClickEvent);
            this.vsClass.KeyDown += new Wisej.Web.KeyEventHandler(this.vsClass_KeyDownEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 1;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(444, 24);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(125, 48);
            this.cmdPrintPreview.TabIndex = 5;
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(921, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdPrint.Size = new System.Drawing.Size(55, 24);
            this.cmdPrint.TabIndex = 6;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // frmSetupDepraciationTotals
            // 
            this.ClientSize = new System.Drawing.Size(998, 746);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSetupDepraciationTotals";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Depreciation Totals";
            this.Load += new System.EventHandler(this.frmSetupDepraciationTotals_Load);
            this.Activated += new System.EventHandler(this.frmSetupDepraciationTotals_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupDepraciationTotals_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeDiscarded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDiscardedDateRange)).EndInit();
            this.fraDiscardedDateRange.ResumeLayout(false);
            this.fraDiscardedDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDiscardedStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDiscardedEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDiscardedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDiscardedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkZeroCurDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPurchaseDateRange)).EndInit();
            this.fraPurchaseDateRange.ResumeLayout(false);
            this.fraPurchaseDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurchaseEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPurchaseStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartPurchaseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndPurchaseDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelected)).EndInit();
            this.fraSelected.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrintPreview;
		private FCButton cmdPrint;
	}
}
