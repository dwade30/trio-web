﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmDepreciation.
	/// </summary>
	public partial class frmDepreciation : BaseForm
	{
		public frmDepreciation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			// FC:FINAL:VGE - #368 Added proper SortMode for all columns
			foreach (DataGridViewColumn column in vsGrid.Columns)
			{
				column.SortMode = DataGridViewColumnSortMode.Automatic;
			}
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDepreciation InstancePtr
		{
			get
			{
				return (frmDepreciation)Sys.GetInstance(typeof(frmDepreciation));
			}
		}

		protected frmDepreciation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   01/28/2003                          *
		// *
		// MODIFIED BY    :   Dave Wade                           *
		// Last Updated   :   03/03/2003                          *
		// ********************************************************
		string strSort = "";
		bool boolDataChanged;
		const string GRIDCOLUMN0 = "ID";
		const string GRIDCOLUMN1 = "Changed";
		const string GRIDCOLUMN2 = "Ignore";
		const string GRIDCOLUMN3 = "Description";
		const string GRIDCOLUMN4 = "Year";
		const string GRIDCOLUMN5 = "Make";
		const string GRIDCOLUMN6 = "Model";
		const string GRIDCOLUMN7 = "Date Last Dep";
		const string GRIDCOLUMN8 = "Remaining";
		const string GRIDCOLUMN9 = "Current Units";
		const string GRIDCOLUMN10 = "Basis Cost";
		const string GRIDCOLUMN11 = "Unit Type";
		const string GRIDCOLUMN12 = "Life";
		const string GRIDCOLUMN13 = "Basic Reduction";
		const string GRIDCOLUMN14 = "Total Used";
		const string GRIDCOLUMN15 = "Salvage Value";
		public bool blnSingleDepreciation;
		public bool blnPreview;
		int intBYear;

		private void SetGridProperties()
		{
			vsGrid.FixedCols = 0;
			vsGrid.FixedRows = 1;
			vsGrid.Rows = 1;
			vsGrid.Cols = 16;
			vsGrid.ColHidden(0, true);
			vsGrid.ColHidden(1, true);
			vsGrid.ColHidden(10, true);
			vsGrid.ColHidden(12, true);
			vsGrid.ColHidden(13, true);
			vsGrid.ColHidden(14, true);
			vsGrid.ColHidden(15, true);
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.07));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
			vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.05));
			vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
			vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.08));
			vsGrid.ColWidth(7, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.ColWidth(8, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.ColWidth(9, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.ColWidth(11, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.FrozenCols = 4;
			//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsGrid.ColDataType(9, FCGrid.DataTypeSettings.flexDTCurrency);
			vsGrid.TextMatrix(0, 0, GRIDCOLUMN0);
			vsGrid.TextMatrix(0, 1, GRIDCOLUMN1);
			vsGrid.TextMatrix(0, 2, GRIDCOLUMN2);
			vsGrid.TextMatrix(0, 3, GRIDCOLUMN3);
			vsGrid.TextMatrix(0, 4, GRIDCOLUMN4);
			vsGrid.TextMatrix(0, 5, GRIDCOLUMN5);
			vsGrid.TextMatrix(0, 6, GRIDCOLUMN6);
			vsGrid.TextMatrix(0, 7, GRIDCOLUMN7);
			vsGrid.TextMatrix(0, 8, GRIDCOLUMN8);
			vsGrid.TextMatrix(0, 9, GRIDCOLUMN9);
			vsGrid.TextMatrix(0, 10, GRIDCOLUMN10);
			vsGrid.TextMatrix(0, 11, GRIDCOLUMN11);
			vsGrid.TextMatrix(0, 12, GRIDCOLUMN12);
			vsGrid.TextMatrix(0, 13, GRIDCOLUMN13);
			vsGrid.TextMatrix(0, 14, GRIDCOLUMN14);
			vsGrid.TextMatrix(0, 15, GRIDCOLUMN15);
			//FC:FINAL:AM: column data type shouldn't be boolean
			//vsGrid.ColDataType(2, FCGrid.DataTypeSettings.flexDTBoolean);
			vsGrid.Select(0, 0);
			// HIDE THE PRINT BUTTONS IF THE RESULTS
			mnuPrint.Visible = false;
			mnuPrintPreview.Visible = false;
			mnuIgnore.Enabled = true;
			cmdIgnore.Enabled = true;
			cmdIgnore.Visible = true;
			cmdPrint.Visible = false;
			cmdPrintPreview.Visible = false;
			txtTotal.Visible = false;
			lblTotal.Visible = false;
		}

		private void SetResultGridProperties()
		{
			vsGrid.FixedRows = 1;
			vsGrid.Rows = 1;
			vsGrid.FrozenCols = 0;
			vsGrid.Cols = 10;
			vsGrid.ColHidden(1, true);
			vsGrid.ColHidden(9, true);
			vsGrid.ColDataType(2, FCGrid.DataTypeSettings.flexDTString);
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.23));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.07));
			vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
			vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
			vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
			vsGrid.ColWidth(7, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
			vsGrid.ColWidth(8, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.3));
			// ALIGN THE HEADINGS
			//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsGrid.TextMatrix(0, 0, "Item ID");
			vsGrid.TextMatrix(0, 1, "");
			vsGrid.TextMatrix(0, 2, "Description");
			vsGrid.TextMatrix(0, 3, "Year");
			vsGrid.TextMatrix(0, 4, "Make");
			vsGrid.TextMatrix(0, 5, "Model");
			vsGrid.TextMatrix(0, 6, "Amount");
            vsGrid.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrid.TextMatrix(0, 7, "Units");
			vsGrid.TextMatrix(0, 8, "Units Desc");
			vsGrid.TextMatrix(0, 9, "Total Units");
			vsGrid.Select(0, 0);
			this.mnuIgnore.Enabled = false;
			this.cmdIgnore.Enabled = false;
			//Application.DoEvents();
			mnuPrint.Visible = false;
			mnuPrintPreview.Visible = false;
			cmdPrint.Visible = false;
			cmdPrintPreview.Visible = false;
			txtTotal.Visible = true;
			lblTotal.Visible = true;
			//this.txtTotal.Left = vsGrid.Left + vsGrid.ColWidth(0) + vsGrid.ColWidth(1) + vsGrid.ColWidth(2) + vsGrid.ColWidth(3) + vsGrid.ColWidth(4) - 500;
			//this.lblTotal.Left = this.txtTotal.Left - this.lblTotal.Width;
			//this.txtTotal.Width = vsGrid.ColWidth(6);
		}

		private void frmDepreciation_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmDepreciation_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmDepreciation_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			// TAKE CARE OF THE ENTER KEY
			if (KeyAscii == Keys.Return)
				Support.SendKeys("{TAB}", false);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDepreciation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDepreciation.ScaleWidth	= 9045;
			//frmDepreciation.ScaleHeight	= 7350;
			//frmDepreciation.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetGridProperties();
			vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			LoadRecord();
			// SET THE DEPRECIATION DATE TO TODAY
			this.mskDepreciationDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			clsDRWrapper rsData = new clsDRWrapper();
			clsDRWrapper rsMasterData = new clsDRWrapper();
			// vbPorter upgrade warning: curAmount As Decimal	OnWrite(short, Decimal)
			Decimal curAmount;
			vsGrid.Select(0, 0);
			// CHECK TO SEE IF THERE ARE ANY PENDING RECORDS IN THE DEPRECIATION TABLE.
			rsData.OpenRecordset("Select * from tblDepreciation where Pending = 1", modGlobal.DEFAULTDATABASENAME);
			if (rsData.EndOfFile())
			{
			}
			else
			{
				curAmount = 0;
				for (int row = 1; row <= vsGrid.Rows - 1; row++)
				{
					decimal amount = 0;
					decimal.TryParse(vsGrid.TextMatrix(row, 6), out amount);
					curAmount += amount;
				}
				if (curAmount != 0)
				{
					intResponse = MessageBox.Show("You have not saved the results of this depreciation run. Save now?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					switch (intResponse)
					{
						case DialogResult.Yes:
							{
								// YES
								mnuProcess_Click();
								break;
							}
						case DialogResult.No:
							{
								// NO
								// Dave 3/3/03
								// ******************************************************
								// check to see if any assets were changed from decling depreciation to straight line depreciation
								rsData.OpenRecordset("SELECT * FROM tblMaster WHERE ChangedToSL = 'P'", "TWFA0000.vb1");
								if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
								{
									// change master record back to using declining depreciation method and set the depreciation date back to what is was originally
									do
									{
										rsMasterData.OpenRecordset("SELECT * FROM tblMaster WHERE ID = " + rsData.Get_Fields_Int32("ID"), "TWFA0000.vb1");
										if (rsMasterData.EndOfFile() != true && rsMasterData.BeginningOfFile() != true)
										{
											rsMasterData.Edit();
											rsMasterData.Set_Fields("DepreciationMethod", 1);
											rsMasterData.Set_Fields("DateLastDepreciation", rsData.Get_Fields_DateTime("OldDepreciationDate"));
											rsMasterData.Set_Fields("RemainingLife", 0);
											rsMasterData.Set_Fields("AdjustedBasisCost", 0);
											rsMasterData.Set_Fields("ChangedToSL", "N");
											rsMasterData.Set_Fields("OldDepreciationDate", null);
											rsMasterData.Update(true);
										}
										rsData.MoveNext();
									}
									while (rsData.EndOfFile() != true);
								}
								// ******************************************************
								// delete all pending depreciation activity
								rsData.Execute("Delete from tblDepreciation where Pending = 1", modGlobal.DEFAULTDATABASENAME);
								Close();
								break;
							}
						case DialogResult.Cancel:
							{
								modVariables.Statics.gboolCancelSelected = true;
								e.Cancel = true;
								break;
							}
					}
					//end switch
				}
			}
			if (!e.Cancel)
			{
				blnSingleDepreciation = false;
			}
		}

		private void frmDepreciation_Resize(object sender, System.EventArgs e)
		{
			if (this.cmdIgnore.Enabled == false)
			{
				vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.23));
				vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.07));
				vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
				vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
				vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.15));
				vsGrid.ColWidth(7, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
				vsGrid.ColWidth(8, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.3));
			}
			else
			{
				vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.07));
				vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.2));
				vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.05));
				vsGrid.ColWidth(5, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.1));
				vsGrid.ColWidth(6, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.08));
				vsGrid.ColWidth(7, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
				vsGrid.ColWidth(8, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
				vsGrid.ColWidth(9, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuIgnore_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
			{
				vsGrid.TextMatrix(intCounter, 2, FCConvert.ToString(-1));
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// PRINT THE RECORD WITHOUT SHOWING
			modDuplexPrinting.DuplexPrintReport(rptDepreciation.InstancePtr);
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptDepreciation.InstancePtr);
		}

		public void mnuPrintPreview_Click()
		{
			mnuPrintPreview_Click(mnuPrintPreview, new System.EventArgs());
		}

		private void ProcessResultsToDatabase()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsData = new clsDRWrapper();
				int lngReportID = 0;
				int intCounter;
				string strAccount = "";
				// vbPorter upgrade warning: dblValue As double	OnWrite(double, string, short)
				double dblValue = 0;
				int intArrayID = 0;
				clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
				clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
				// ff.CopyFile "TWFA0000.vb1", "TWFA0000.bak"
				rsData.Execute("Insert into tblDepreciationMaster (UserID, DepreciationType, CreatedDate) VALUES ('" + modGlobalConstants.Statics.gstrUserID + "','R','" + FCConvert.ToString(DateTime.Now) + "')", modGlobal.DEFAULTDATABASENAME);
				rsData.OpenRecordset("Select Max(ID) as NewID from tblDepreciationMaster", modGlobal.DEFAULTDATABASENAME);
				if (rsData.EndOfFile())
				{
					lngReportID = 0;
				}
				else
				{
					// TODO Get_Fields: Field [NewID] not found!! (maybe it is an alias?)
					lngReportID = FCConvert.ToInt32(rsData.Get_Fields("NewID"));
				}
				for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
				{
					// update the master table with the last depreciation date and Total Used
					rsData.OpenRecordset("Select ChangedToSL, DepreciationMethod from tblMaster where ID = " + vsGrid.TextMatrix(intCounter, 0), modGlobal.DEFAULTDATABASENAME);
					if (rsData.EndOfFile())
					{
					}
					else
					{
						if (FCConvert.ToString(rsData.Get_Fields_String("ChangedToSL")) == "P")
						{
							rsData.Execute("Update tblMaster Set TotalValue = TotalValue - " + vsGrid.TextMatrix(intCounter, 6) + ", DateLastDepreciation = '" + this.mskDepreciationDate.Text + "', TotalUsed = " + vsGrid.TextMatrix(intCounter, 9) + ", ChangedToSL = 'Y' where ID = " + vsGrid.TextMatrix(intCounter, 0), modGlobal.DEFAULTDATABASENAME);
						}
						else
						{
							if (FCConvert.ToInt32(rsData.Get_Fields_Int16("DepreciationMethod")) == 3)
							{
								if (FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 6)) < 0)
								{
									rsData.Execute("Update tblMaster Set TotalValue = TotalValue - " + vsGrid.TextMatrix(intCounter, 6) + ", Improvements = Improvements - " + vsGrid.TextMatrix(intCounter, 6) + ", DateLastDepreciation = '" + this.mskDepreciationDate.Text + "', TotalUsed = " + vsGrid.TextMatrix(intCounter, 9) + " where ID = " + vsGrid.TextMatrix(intCounter, 0), modGlobal.DEFAULTDATABASENAME);
								}
								else
								{
									rsData.Execute("Update tblMaster Set TotalValue = TotalValue - " + vsGrid.TextMatrix(intCounter, 6) + ", TotalDepreciated = TotalDepreciated + " + vsGrid.TextMatrix(intCounter, 6) + ", DateLastDepreciation = '" + this.mskDepreciationDate.Text + "', TotalUsed = " + vsGrid.TextMatrix(intCounter, 9) + " where ID = " + vsGrid.TextMatrix(intCounter, 0), modGlobal.DEFAULTDATABASENAME);
								}
							}
							else
							{
								rsData.Execute("Update tblMaster Set TotalValue = TotalValue - " + vsGrid.TextMatrix(intCounter, 6) + ", TotalDepreciated = TotalDepreciated + " + vsGrid.TextMatrix(intCounter, 6) + ", DateLastDepreciation = '" + this.mskDepreciationDate.Text + "', TotalUsed = " + vsGrid.TextMatrix(intCounter, 9) + " where ID = " + vsGrid.TextMatrix(intCounter, 0), modGlobal.DEFAULTDATABASENAME);
							}
						}
					}
				}
				rsData.Execute("Update tblDepreciation Set Pending = 0, ReportID = " + FCConvert.ToString(lngReportID) + " where Pending = 1", modGlobal.DEFAULTDATABASENAME);
				mnuPrint.Visible = false;
				mnuPrintPreview.Visible = false;
				mnuProcess.Enabled = false;
				cmdProcess.Enabled = false;
				//cmdProcess.Visible = false;
				cmdPrint.Visible = false;
				cmdPrintPreview.Visible = false;
				modVariables.Statics.glngDepReportID = lngReportID;
				// NOW ADD THE DATA TO BUDGETARY
				// ONLY WANT EXPENSE AND DEPRECIATION
				if (modVariables.Statics.boolBudgetaryExists)
				{
					//FC:FINAL:JEI:IIT807 adjust indey to avoid IndexOutOfRangeException
					//modVariables.AccountStrut = new modBudgetaryAccounting.FundType[0];
					modVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[1];
					intArrayID = 1;
					modAccountTitle.SetAccountFormats();
					rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where ReportID = " + FCConvert.ToString(modVariables.Statics.glngDepReportID) + " order by Acct1", modGlobal.DatabaseNamePath);
					if (!rsData.EndOfFile())
					{
						strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct1"));
						for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
						{
							if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("Acct1")))
							{
								dblValue += FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
							}
							else
							{
								Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
								modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
								modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
								modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
								modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
								modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
								intArrayID += 1;
								dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
								strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct1"));
							}
							rsData.MoveNext();
						}
						// save the last record
						Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
						modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
						modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
						modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
						modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
						modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
						intArrayID += 1;
					}
					dblValue = 0;
					rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where ReportID = " + FCConvert.ToString(modVariables.Statics.glngDepReportID) + " order by Acct3", modGlobal.DatabaseNamePath);
					if (!rsData.EndOfFile())
					{
						strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct3"));
						for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
						{
							if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("Acct3")))
							{
								dblValue += FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
							}
							else
							{
								Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
								modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
								modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
								modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
								modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
								modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
								intArrayID += 1;
								dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
								strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct3"));
							}
							rsData.MoveNext();
						}
						// save the last record
						Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
						modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
						modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
						modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
						modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
						modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
					}
					modVariables.Statics.glngJournalNumber = modBudgetaryAccounting.AddToJournal(ref modVariables.Statics.AccountStrut, "FA", 0, Convert.ToDateTime(Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy")), Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy") + " - Depreciation");
				}
				modVariables.Statics.gintPeriodNumber = FCConvert.ToInt32(Strings.Left(Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy"), 2));
				modVariables.Statics.gdatDepreciationDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
				if (modVariables.Statics.boolBudgetaryExists)
				{
					if (modSecurity.ValidPermissions_8(null, modStartup.AUTOPOSTDEPRECIATIONJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptFADepreciation))
					{
						if (MessageBox.Show("Update of Depreciation run has been completed successfully.  Your entries have been saved in journal " + modValidateAccount.GetFormat_6(Strings.Trim(modVariables.Statics.glngJournalNumber.ToString()), 4) + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							clsJournalInfo = new clsPostingJournalInfo();
							clsJournalInfo.JournalNumber = modVariables.Statics.glngJournalNumber;
							clsJournalInfo.JournalType = "GJ";
							clsJournalInfo.Period = FCConvert.ToString(DateAndTime.DateValue(mskDepreciationDate.Text).Month);
							clsJournalInfo.CheckDate = "";
							clsPostInfo.AddJournal(clsJournalInfo);
							clsPostInfo.AllowPreview = true;
							clsPostInfo.WaitForReportToEnd = true;
							clsPostInfo.PostJournals();
						}
					}
					else
					{
						MessageBox.Show("Update of Depreciation run has been completed successfully.  Your entries have been saved in journal " + modValidateAccount.GetFormat_6(Strings.Trim(modVariables.Statics.glngJournalNumber.ToString()), 4) + ".", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("Update of Depreciation run has been completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				blnPreview = false;
				mnuPrintPreview_Click();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: Ans As short, int --> As DialogResult
			DialogResult Ans;
			// vbPorter upgrade warning: curAmount As Decimal	OnWrite(short, Decimal)
			Decimal curAmount;
			int intArrayID;
			try
			{
				// On Error GoTo ErrorHandler
				if (this.cmdIgnore.Enabled == false)
				{
					curAmount = 0;
					for (int row = 1; row <= vsGrid.Rows - 1; row++)
					{
						decimal amount = 0;
						decimal.TryParse(vsGrid.TextMatrix(row, 6), out amount);
						curAmount += amount;
					}
					if (curAmount != 0)
					{
						Ans = MessageBox.Show("You are about to post a depreciation run for this / these assets.  Do you wish to continue?", "Post Depreciation?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
						if (Ans == DialogResult.Yes)
						{
							ProcessResultsToDatabase();
						}
					}
					else
					{
						MessageBox.Show("You may only save depreciation infromation if actual depreciation has occured.", "Invalid Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					return;
				}
				int intCount;
				int intCounter;
				double dblDepreciation = 0;
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsMaster = new clsDRWrapper();
				// vbPorter upgrade warning: intDateDifference As int --> As long	OnWrite(long, short)
				int intDateDifference;
				string strDescription = "";
				string strAccount = "";
				// vbPorter upgrade warning: dblValue As double	OnWrite(double, string, short)
				double dblValue = 0;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				vsGrid.Select(0, 0);
				if (ValidData())
				{
					// PROCESS ALL UN'S FROM THE GRID
					for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
					{
						// CHECK TO SEE IF WE IGNORE THIS ITEM
						if (FCConvert.ToInt32(vsGrid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) == -1)
						{
						}
						else
						{
							if (Conversion.Val(vsGrid.TextMatrix(intCounter, 12)) != 0)
							{
								dblDepreciation = ((Conversion.Val(vsGrid.TextMatrix(intCounter, 10)) - Conversion.Val(vsGrid.TextMatrix(intCounter, 13))) / Conversion.Val(vsGrid.TextMatrix(intCounter, 12))) * (Conversion.Val(vsGrid.TextMatrix(intCounter, 8)) - Conversion.Val(Strings.Trim(vsGrid.TextMatrix(intCounter, 9))));
								// need to check to make sure total dep is not greater then
								// the BasisCost (10) - Salvage Value (15)
								if (dblDepreciation > (Conversion.Val(vsGrid.TextMatrix(intCounter, 10)) - Conversion.Val(vsGrid.TextMatrix(intCounter, 15))))
								{
									dblDepreciation = (Conversion.Val(vsGrid.TextMatrix(intCounter, 10)) - Conversion.Val(vsGrid.TextMatrix(intCounter, 15)));
								}
								rsData.Execute("Insert into tblDepreciation (ItemID,DepreciationDate,DepreciationAmount,DepreciationUnits,DepreciationDesc,Pending) VALUES (" + vsGrid.TextMatrix(intCounter, 0) + ",'" + mskDepreciationDate.Text + "','" + FCConvert.ToString(modGlobal.Round_6(dblDepreciation, 2)) + "','" + FCConvert.ToString(Conversion.Val(vsGrid.TextMatrix(intCounter, 8)) - Conversion.Val(Strings.Trim(vsGrid.TextMatrix(intCounter, 9)))) + "','" + vsGrid.TextMatrix(intCounter, 11) + "(UN)" + "',1)", modGlobal.DEFAULTDATABASENAME);
							}
							else
							{
								// CANNOT DIVIDE BY ZERO
							}
						}
					}
					// PROCESS ALL DECLINING BALANCE ITEMS
					string datPurchaseDate = "";
					string datLastDepDate = "";
					string datCurrentDepDate = "";
					string datLowDate = "";
					string datHighDate = "";
					string datTempPurchaseDate = "";
					double dblBasisCost = 0;
					int intYearDif = 0;
					bool boolFirstDep = false;
					bool boolLastYear = false;
					int intDaysTotal = 0;
					int intPassCount;
					bool blnChangedToSL = false;
					// vbPorter upgrade warning: datOldDepDate As DateTime	OnWriteFCConvert.ToInt16(
					DateTime datOldDepDate = DateTime.FromOADate(0);
					bool boolEndOfLife = false;
					if (!blnSingleDepreciation)
					{
						rsMaster.OpenRecordset("Select * from tblMaster where IsNull(Discarded, 0) = 0 AND DateLastDepreciation < '" + mskDepreciationDate.Text + "' AND (isDate(OutOfServiceDate) = 0 or OutOfServiceDate > DateLastDepreciation) AND DepreciationMethod = 1", modGlobal.DatabaseNamePath);
					}
					else
					{
						rsMaster.OpenRecordset("Select * from tblMaster where DateLastDepreciation < '" + mskDepreciationDate.Text + "' AND (isDate(OutOfServiceDate) = 0 or OutOfServiceDate > DateLastDepreciation) AND ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationMethod = 1", modGlobal.DatabaseNamePath);
					}
					while (!rsMaster.EndOfFile())
					{
						blnChangedToSL = false;
						datPurchaseDate = Strings.Format(rsMaster.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy");
						if (Information.IsDate(datPurchaseDate))
						{
						}
						else
						{
							MessageBox.Show("Invalid Purchase Date for the item:" + rsMaster.Get_Fields_String("Description"), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						datLastDepDate = Strings.Format(rsMaster.Get_Fields_DateTime("DateLastDepreciation"), "MM/dd/yyyy");
						if (Information.IsDate(datLastDepDate))
						{
						}
						else
						{
							MessageBox.Show("Invalid Date Last Depreciated for the item:" + rsMaster.Get_Fields_String("Description"), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						if (Information.IsDate(rsMaster.Get_Fields("OutOfServiceDate")))
						{
							if (rsMaster.Get_Fields_DateTime("OutOfServiceDate").ToOADate() < DateAndTime.DateValue(mskDepreciationDate.Text).ToOADate())
							{
								datCurrentDepDate = Strings.Format(rsMaster.Get_Fields_DateTime("OutOfServiceDate"), "MM/dd/yyyy");
							}
							else
							{
								datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
							}
						}
						else
						{
							datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
						}
						if (Information.IsDate(datCurrentDepDate))
						{
						}
						else
						{
							MessageBox.Show("Invalid Current Depreciation Date for the item:" + rsMaster.Get_Fields_String("Description"), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						// Matthew 03-04-03
						int intTemp = 0;
						intTemp = GetYearDifference(ref rsMaster, datLastDepDate, datCurrentDepDate);
						if (intTemp == 0)
						{
							// THIS DEP IS ALL WITHING THE SAME DEP YEAR
							dblBasisCost = FCConvert.ToDouble(CalculateBasisCost(ref rsMaster, datLastDepDate, datCurrentDepDate));
							boolLastYear = false;
							if (datLastDepDate == datPurchaseDate)
							{
								// this is the first dep for this item
								boolFirstDep = true;
							}
							else
							{
								boolFirstDep = false;
							}
							intDateDifference = Math.Abs(DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMaster.Get_Fields_DateTime("DateLastDepreciation"), "MM/dd/yyyy")), FCConvert.ToDateTime(datCurrentDepDate)));
							if (boolFirstDep)
							{
								// this is the first dep for this item
								intDateDifference += 1;
							}
							strDescription = "Days (DB) ";
							// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
							dblDepreciation = (dblBasisCost * (rsMaster.Get_Fields_Double("DBRate") / rsMaster.Get_Fields("Life")) / 365) * intDateDifference;
							if (dblDepreciation < 0)
								dblDepreciation = 0;
							if (dblDepreciation > 0)
							{
								rsData.Execute("Insert into tblDepreciation (ItemID,DepreciationDate,DepreciationAmount,DepreciationUnits,DepreciationDesc,Pending) VALUES (" + rsMaster.Get_Fields_Int32("ID") + ",'" + datCurrentDepDate + "','" + FCConvert.ToString(modGlobal.Round_6(dblDepreciation, 2)) + "','" + FCConvert.ToString(intDateDifference) + "','" + strDescription + "',1)", modGlobal.DEFAULTDATABASENAME);
							}
						}
						else
						{
							// get the basis cost for the number of years that this depreciation covers
							dblBasisCost = FCConvert.ToDouble(CalculateBasisCost(ref rsMaster, datLastDepDate, datCurrentDepDate));
							// get the number of years difference between the datLastDepDate date and
							// the current depreciation date
							intYearDif = GetYearDifference(ref rsMaster, datLastDepDate, datCurrentDepDate);
							// set temp variables
							dblDepreciation = 0;
							boolLastYear = false;
							intDaysTotal = 0;
							if (datLastDepDate == datPurchaseDate)
							{
								// this is the first dep for this item
								boolFirstDep = true;
							}
							else
							{
								boolFirstDep = false;
							}
							datLowDate = datLastDepDate;
							intPassCount = 0;
							// TODO Get_Fields: Check the table for the column [LIFE] and replace with corresponding Get_Field method
							for (intCounter = 1; intCounter <= FCConvert.ToInt32(rsMaster.Get_Fields("LIFE")); intCounter++)
							{
								// get the end date for the current year being calculated
								datHighDate = Strings.Format(DateAndTime.DateAdd("yyyy", intCounter, DateAndTime.DateValue(datPurchaseDate)), "MM/dd/yyyy");
								if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(datLowDate), FCConvert.ToDateTime(datHighDate)) > 0)
								{
									break;
								}
							}
							// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
							if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(datHighDate), FCConvert.ToDateTime(Strings.Format(DateAndTime.DateAdd("yyyy", Conversion.Val(rsMaster.Get_Fields("Life")), DateAndTime.DateValue(datPurchaseDate)), "MM/dd/yyyy"))) <= 0)
							{
								// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
								datHighDate = Strings.Format(DateAndTime.DateAdd("d", -1, DateAndTime.DateAdd("yyyy", FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("Life")))), DateAndTime.DateValue(datPurchaseDate))), "MM/dd/yyyy");
								intYearDif -= 1;
								boolEndOfLife = true;
							}
							else
							{
								boolEndOfLife = false;
							}
							for (intCounter = 1; intCounter <= intYearDif + 1; intCounter++)
							{
								if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(datHighDate), FCConvert.ToDateTime(datCurrentDepDate)) < 0)
								{
									boolLastYear = true;
								}
								else
								{
									if (boolEndOfLife == false)
									{
										boolLastYear = false;
									}
									else
									{
										boolLastYear = true;
									}
								}
								if (!boolEndOfLife)
								{
									if (boolLastYear)
									{
										datHighDate = datCurrentDepDate;
									}
									else
									{
										datHighDate = Strings.Format(DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(datHighDate)), "MM/dd/yyyy");
									}
								}
								// find the diff
								intDateDifference = DateAndTime.DateDiff("d", FCConvert.ToDateTime(datLowDate), FCConvert.ToDateTime(datHighDate));
								if (intCounter == 1)
								{
									if (boolFirstDep)
									{
										if (boolLastYear)
										{
											intDateDifference += 1;
										}
									}
									else
									{
										if (boolLastYear)
										{
										}
										else
										{
											intDateDifference -= 1;
										}
									}
								}
								intDaysTotal += intDateDifference;
								if (intCounter == 1)
								{
									strDescription = "(" + FCConvert.ToString(intDateDifference) + " / ";
								}
								else
								{
									strDescription += FCConvert.ToString(intDateDifference) + " / ";
								}
								// calculate the depreciation for this year and add it to any
								// previous years depreciation
								// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
								dblDepreciation += (dblBasisCost * (rsMaster.Get_Fields_Double("DBRate") / rsMaster.Get_Fields("Life")) / 365) * intDateDifference;
								// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
								dblBasisCost -= (dblBasisCost * (rsMaster.Get_Fields_Double("DBRate") / rsMaster.Get_Fields("Life")));
								datLowDate = datHighDate;
								intBYear += 1;
								// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
								if (intBYear == Conversion.Val(rsMaster.Get_Fields("Life")))
								{
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
									if (CompareDBToSL_20(rsMaster.Get_Fields("Life"), dblBasisCost, rsMaster.Get_Fields_Double("DBRate")) == "SL")
									{
										blnChangedToSL = true;
										datOldDepDate = (DateTime)rsMaster.Get_Fields_DateTime("DateLastDepreciation");
										rsMaster.Edit();
										rsMaster.Set_Fields("DepreciationMethod", 0);
										// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
										rsMaster.Set_Fields("RemainingLife", Conversion.Val(rsMaster.Get_Fields("Life")) - intBYear);
										rsMaster.Set_Fields("AdjustedBasisCost", dblBasisCost);
										rsMaster.Set_Fields("ChangedToSL", "P");
										rsMaster.Set_Fields("DateLastDepreciation", datLowDate);
										rsMaster.Set_Fields("OldDepreciationDate", datOldDepDate);
										rsMaster.Update(true);
										break;
									}
								}
								// get the end date for the current year being calculated
								datHighDate = Strings.Format(DateAndTime.DateAdd("yyyy", 1, DateAndTime.DateValue(datHighDate)), "MM/dd/yyyy");
							}
							strDescription = "Days (DB) " + Strings.Mid(Strings.Trim(strDescription), 1, Strings.Trim(strDescription).Length - 1) + ")";
							if (dblDepreciation < 0)
								dblDepreciation = 0;
							// add this depreciation amount to the database
							if (!blnChangedToSL)
							{
								datOldDepDate = DateTime.FromOADate(0);
							}
							// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
							if (dblDepreciation == 0 && (rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") != rsMaster.Get_Fields_Decimal("SalvageValue")) && (DateAndTime.DateValue(datCurrentDepDate).ToOADate() == DateAndTime.DateAdd("d", -1, DateAndTime.DateAdd("yyyy", Conversion.Val(rsMaster.Get_Fields("Life")), (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate"))).ToOADate()))
							{
								rsData.OpenRecordset("SELECT * FROM tblDepreciation WHERE ItemID = " + rsMaster.Get_Fields_Int32("ID") + " ORDER BY DepreciationDate DESC");
								if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
								{
									if (String.Compare(FCConvert.ToString(rsData.Get_Fields_DateTime("DepreciationDate")), datCurrentDepDate) >= 0)
									{
										datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
										dblDepreciation = FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") - rsMaster.Get_Fields_Decimal("SalvageValue"));
										intDaysTotal = 0;
									}
									else
									{
										dblDepreciation = FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") - rsMaster.Get_Fields_Decimal("SalvageValue"));
									}
								}
								else
								{
									dblDepreciation = FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") - rsMaster.Get_Fields_Decimal("SalvageValue"));
								}
							}
							if (dblDepreciation > 0)
							{
								rsData.Execute("Insert into tblDepreciation (ItemID,DepreciationDate,DepreciationAmount,DepreciationUnits,DepreciationDesc,Pending,ChangedToSL,OldDepreciationDate) VALUES (" + rsMaster.Get_Fields_Int32("ID") + ",'" + datCurrentDepDate + "','" + FCConvert.ToString(modGlobal.Round_6(dblDepreciation, 2)) + "','" + FCConvert.ToString(intDaysTotal) + "','" + strDescription + "',1," + FCConvert.ToString(blnChangedToSL) + ",'" + FCConvert.ToString(datOldDepDate) + "')", modGlobal.DEFAULTDATABASENAME);
							}
						}
						rsMaster.MoveNext();
					}
					// PROCESS ALL STRAIGHT LINE ITEMS
					if (!blnSingleDepreciation)
					{
						rsMaster.OpenRecordset("Select * from tblMaster where IsNull(Discarded, 0) = 0 AND DateLastDepreciation < '" + mskDepreciationDate.Text + "' AND (isDate(OutOfServiceDate) = 0 or OutOfServiceDate > DateLastDepreciation) AND ((convert(money, BasisCost) - convert(money, TotalDepreciated)) > convert(money, SalvageValue)) AND DepreciationMethod = 0 AND ClassCode <> '0'", modGlobal.DatabaseNamePath);
						// Dave 090706 Left(ManualClassCode, 1) <> '0'
					}
					else
					{
						rsMaster.OpenRecordset("Select * from tblMaster where DateLastDepreciation < '" + mskDepreciationDate.Text + "' AND (isDate(OutOfServiceDate) = 0 or OutOfServiceDate > DateLastDepreciation) AND ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationMethod = 0", modGlobal.DatabaseNamePath);
					}
					double dblLifeValue = 0;
					double dblBasisValue = 0;
					datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
					if (Information.IsDate(datCurrentDepDate))
					{
					}
					else
					{
						MessageBox.Show("Invalid Current Depreciation Date for the item:" + rsMaster.Get_Fields_String("Description"), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					while (!rsMaster.EndOfFile())
					{
						if (Information.IsDate(rsMaster.Get_Fields("OutOfServiceDate")))
						{
							if (rsMaster.Get_Fields_DateTime("OutOfServiceDate").ToOADate() < DateAndTime.DateValue(mskDepreciationDate.Text).ToOADate())
							{
								datCurrentDepDate = Strings.Format(rsMaster.Get_Fields_DateTime("OutOfServiceDate"), "MM/dd/yyyy");
							}
							else
							{
								datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
							}
						}
						else
						{
							datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
						}
						if (FCConvert.ToString(rsMaster.Get_Fields_String("ChangedToSL")) == "P" || FCConvert.ToString(rsMaster.Get_Fields_String("ChangedToSL")) == "Y")
						{
							dblLifeValue = rsMaster.Get_Fields_Int16("RemainingLife");
							dblBasisValue = rsMaster.Get_Fields_Double("AdjustedBasisCost");
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
							dblLifeValue = Conversion.Val(rsMaster.Get_Fields("Life"));
							dblBasisValue = Conversion.Val(rsMaster.Get_Fields_Decimal("BasisCost")) - Conversion.Val(rsMaster.Get_Fields_Decimal("SalvageValue"));
						}
						if (Conversion.Val(dblLifeValue) != 0)
						{
							// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
							if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(datCurrentDepDate), FCConvert.ToDateTime(Strings.Format(DateAndTime.DateAdd("yyyy", rsMaster.Get_Fields("Life"), (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate")), "MM/dd/yyyy"))) < 0)
							{
								// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
								datCurrentDepDate = Strings.Format(DateAndTime.DateAdd("d", -1, DateAndTime.DateAdd("yyyy", FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields("Life")))), (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate"))), "MM/dd/yyyy");
							}
							if (FCConvert.ToString(rsMaster.Get_Fields_DateTime("DateLastDepreciation")) == string.Empty)
							{
								intDateDifference = (DateAndTime.DateDiff("d", (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate"), FCConvert.ToDateTime(datCurrentDepDate)));
								// this is the first dep for this item
								intDateDifference += 1;
							}
							else
							{
								intDateDifference = (DateAndTime.DateDiff("d", (DateTime)rsMaster.Get_Fields_DateTime("DateLastDepreciation"), FCConvert.ToDateTime(datCurrentDepDate)));
								if (DateAndTime.DateDiff("d", (DateTime)rsMaster.Get_Fields_DateTime("DateLastDepreciation"), (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate")) == 0)
								{
									// this is the first dep for this item
									intDateDifference += 1;
								}
							}
							dblDepreciation = (((dblBasisValue / dblLifeValue) / 365) * intDateDifference);
							if (FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated")) - dblDepreciation < 0)
							{
								dblDepreciation = dblBasisValue - FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("TotalDepreciated"));
							}
							if (dblDepreciation < 0)
								dblDepreciation = 0;
							// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
							if ((rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") != rsMaster.Get_Fields_Decimal("SalvageValue")) && (DateAndTime.DateValue(datCurrentDepDate).ToOADate() == DateAndTime.DateAdd("d", -1, DateAndTime.DateAdd("yyyy", Conversion.Val(rsMaster.Get_Fields("Life")), (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate"))).ToOADate()))
							{
								rsData.OpenRecordset("SELECT * FROM tblDepreciation WHERE ItemID = " + rsMaster.Get_Fields_Int32("ID") + " ORDER BY DepreciationDate DESC", "TWFA0000.vb1");
								if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
								{
									if (rsData.Get_Fields_DateTime("DepreciationDate").ToOADate() >= DateAndTime.DateValue(datCurrentDepDate).ToOADate())
									{
										datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
										dblDepreciation = FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") - rsMaster.Get_Fields_Decimal("SalvageValue"));
										intDaysTotal = 0;
									}
									else
									{
										dblDepreciation = FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") - rsMaster.Get_Fields_Decimal("SalvageValue"));
									}
								}
								else
								{
									dblDepreciation = FCConvert.ToDouble(rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("TotalDepreciated") - rsMaster.Get_Fields_Decimal("SalvageValue"));
								}
							}
							strDescription = "Days (SL)";
							if (dblDepreciation > 0)
							{
								rsData.Execute("Insert into tblDepreciation (ItemID,DepreciationDate,DepreciationAmount,DepreciationUnits,DepreciationDesc,Pending) VALUES (" + rsMaster.Get_Fields_Int32("ID") + ",'" + datCurrentDepDate + "','" + FCConvert.ToString(modGlobal.Round_6(dblDepreciation, 2)) + "','" + FCConvert.ToString(intDateDifference) + "','" + strDescription + "',1)", modGlobal.DEFAULTDATABASENAME);
							}
						}
						rsMaster.MoveNext();
					}
					// PROCESS ALL CONDITION BASED ITEMS
					if (!blnSingleDepreciation)
					{
						rsMaster.OpenRecordset("Select * from tblMaster where IsNull(Discarded, 0) = 0 AND DateLastDepreciation < '" + mskDepreciationDate.Text + "' AND (isDate(OutOfServiceDate) = 0 or OutOfServiceDate > DateLastDepreciation) AND ((convert(money, BasisCost) - convert(money, TotalDepreciated) + convert(money, Improvements)) > convert(money, SalvageValue)) AND DepreciationMethod = 3", modGlobal.DatabaseNamePath);
					}
					else
					{
						rsMaster.OpenRecordset("Select * from tblMaster where DateLastDepreciation < '" + mskDepreciationDate.Text + "' AND (isDate(OutOfServiceDate) = 0 or OutOfServiceDate > DateLastDepreciation) AND ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationMethod = 3", modGlobal.DatabaseNamePath);
					}
					double dblOldValue = 0;
					double dblNewValue = 0;
					clsDRWrapper rsCondition = new clsDRWrapper();
					datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
					if (Information.IsDate(datCurrentDepDate))
					{
					}
					else
					{
						MessageBox.Show("Invalid Current Depreciation Date for the item:" + rsMaster.Get_Fields_String("Description"), "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					while (!rsMaster.EndOfFile())
					{
						if (Information.IsDate(rsMaster.Get_Fields("OutOfServiceDate")))
						{
							if (rsMaster.Get_Fields_DateTime("OutOfServiceDate").ToOADate() < DateAndTime.DateValue(mskDepreciationDate.Text).ToOADate())
							{
								datCurrentDepDate = Strings.Format(rsMaster.Get_Fields_DateTime("OutOfServiceDate"), "MM/dd/yyyy");
							}
							else
							{
								datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
							}
						}
						else
						{
							datCurrentDepDate = Strings.Format(mskDepreciationDate.Text, "MM/dd/yyyy");
						}
						dblOldValue = Conversion.Val(rsMaster.Get_Fields_Decimal("BasisCost")) - Conversion.Val(rsMaster.Get_Fields_Decimal("TotalDepreciated")) + Conversion.Val(rsMaster.Get_Fields_Double("Improvements"));
						rsCondition.OpenRecordset("SELECT * FROM Condition WHERE Condition = '" + rsMaster.Get_Fields_String("Condition") + "'", "TWFA0000.vb1");
						dblDepreciation = 0;
						if (rsCondition.EndOfFile() != true && rsCondition.BeginningOfFile() != true)
						{
							dblNewValue = Conversion.Val(rsMaster.Get_Fields_Decimal("BasisCost")) - (Conversion.Val(rsMaster.Get_Fields_Decimal("BasisCost")) * rsCondition.Get_Fields_Double("DepreciationPercentage"));
							dblDepreciation = dblOldValue - dblNewValue;
						}
						if (FCConvert.ToString(rsMaster.Get_Fields_DateTime("DateLastDepreciation")) == string.Empty)
						{
							intDateDifference = (DateAndTime.DateDiff("d", (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate"), FCConvert.ToDateTime(datCurrentDepDate)));
							// this is the first dep for this item
							intDateDifference += 1;
						}
						else
						{
							intDateDifference = (DateAndTime.DateDiff("d", (DateTime)rsMaster.Get_Fields_DateTime("DateLastDepreciation"), FCConvert.ToDateTime(datCurrentDepDate)));
							if (DateAndTime.DateDiff("d", (DateTime)rsMaster.Get_Fields_DateTime("DateLastDepreciation"), (DateTime)rsMaster.Get_Fields_DateTime("PurchaseDate")) == 0)
							{
								// this is the first dep for this item
								intDateDifference += 1;
							}
						}
						strDescription = "Days (CB)";
						rsData.Execute("Insert into tblDepreciation (ItemID,DepreciationDate,DepreciationAmount,DepreciationUnits,DepreciationDesc,Pending) VALUES (" + rsMaster.Get_Fields_Int32("ID") + ",'" + datCurrentDepDate + "','" + FCConvert.ToString(modGlobal.Round_6(dblDepreciation, 2)) + "','" + FCConvert.ToString(intDateDifference) + "','" + strDescription + "',1)", modGlobal.DEFAULTDATABASENAME);
						rsMaster.MoveNext();
					}
					// no show the grid with the depreciated values.
					SetResultGridProperties();
					LoadResultRecords();
				}
				rsMaster.OpenRecordset("Select sum(DepreciationAmount) as TotalAmount from tblDepreciation where Pending = 1", modGlobal.DEFAULTDATABASENAME);
				txtTotal.Text = Strings.Format(rsMaster.Get_Fields_Decimal("TotalAmount"), "0.00");
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				mskDepreciationDate.Enabled = false;
				vsGrid.Focus();
				mnuProcess.Text = "Save & Print";
				cmdProcess.Text = "Save & Print";
				modVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0 + 1];
				intArrayID = 1;
				modAccountTitle.SetAccountFormats();
				rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where Pending = 1 order by Acct1", modGlobal.DatabaseNamePath);
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct1"));
					for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("Acct1")))
						{
							dblValue += FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
						}
						else
						{
							Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
							modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
							modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
							modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
							modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
							modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
							intArrayID += 1;
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct1"));
						}
						rsData.MoveNext();
					}
					// save the last record
					Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
					modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
					modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
					modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
					modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue, "0.00"));
					modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
					intArrayID += 1;
				}
				dblValue = 0;
				rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where ReportID = " + FCConvert.ToString(modVariables.Statics.glngDepReportID) + " order by Acct3", modGlobal.DatabaseNamePath);
				if (!rsData.EndOfFile())
				{
					strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct3"));
					for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
					{
						if (strAccount == FCConvert.ToString(rsData.Get_Fields_String("Acct3")))
						{
							dblValue += FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
						}
						else
						{
							Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
							modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
							modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
							modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
							modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
							modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
							intArrayID += 1;
							dblValue = FCConvert.ToDouble(Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
							strAccount = FCConvert.ToString(rsData.Get_Fields_String("Acct3"));
						}
						rsData.MoveNext();
					}
					// save the last record
					Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
					modVariables.Statics.AccountStrut[intArrayID] = new modBudgetaryAccounting.FundType();
					modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
					modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
					modVariables.Statics.AccountStrut[intArrayID].Amount = FCConvert.ToDecimal(Strings.Format(dblValue * -1, "0.00"));
					modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(mskDepreciationDate.Text, "MM/dd/yy") + " Dep";
				}
				mnuPrint.Visible = true;
				mnuPrintPreview.Visible = true;
				cmdPrint.Visible = true;
				cmdPrintPreview.Visible = true;
				blnPreview = true;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
		}

		public void mnuProcess_Click()
		{
			mnuProcess_Click(cmdProcess, new System.EventArgs());
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetYearDifference(ref clsDRWrapper rsMaster, string datLastDepDate, string datCurrentDepDate)
		{
			short GetYearDifference = 0;
			int intNumEndYear;
			int intNumBeginYear;
			// vbPorter upgrade warning: datDiff As int --> As long	OnWrite(long, short)
			long datDiff;
			datDiff = DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMaster.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy")), FCConvert.ToDateTime(datLastDepDate));
			// really want the day after the last depreciation date
			datDiff += 1;
			intNumBeginYear = 0;
			intNumEndYear = 0;
			NextLoop:
			;
			if (datDiff > 365)
			{
				// then the years are different
				datDiff -= 365;
				intNumBeginYear += 1;
				goto NextLoop;
			}
			intNumBeginYear += 1;
			datDiff = DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMaster.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy")), FCConvert.ToDateTime(datCurrentDepDate));
			NextLoop2:
			;
			if (datDiff > 365)
			{
				intNumEndYear += 1;
				datDiff -= 365;
				goto NextLoop2;
			}
			intNumEndYear += 1;
			// get the year difference
			GetYearDifference = FCConvert.ToInt16(intNumEndYear - intNumBeginYear);
			return GetYearDifference;
		}

		private object CalculateBasisCost(ref clsDRWrapper rsMaster, string datLastDepDate, string datCurrentDepDate)
		{
			object CalculateBasisCost = null;
			int intCounter;
			// vbPorter upgrade warning: datDiff As int --> As long	OnWrite(long, short)
			long datDiff;
			CalculateBasisCost = rsMaster.Get_Fields_Decimal("BasisCost") - rsMaster.Get_Fields_Decimal("SalvageValue");
			datDiff = DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMaster.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy")), FCConvert.ToDateTime(datLastDepDate));
			// really want the day after the last depreciation date
			datDiff += 1;
			intBYear = 0;
			NextLoop:
			;
			if (datDiff > 365)
			{
				// then the years are different
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				CalculateBasisCost -= (CalculateBasisCost * (rsMaster.Get_Fields_Double("DBRate") / rsMaster.Get_Fields("Life")));
				intBYear += 1;
				datDiff -= 365;
				goto NextLoop;
			}
			return CalculateBasisCost;
		}

		private void LoadResultRecords()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where Pending = 1 ORDER BY tblMaster.Description", modGlobal.DatabaseNamePath);
			// DepreciationDate = #" & Me.mskDepreciationDate & "# AND
			if (!rsData.EndOfFile())
			{
				vsGrid.Rows += rsData.RecordCount();
				for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
				{
					vsGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					vsGrid.TextMatrix(intCounter, 1, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					vsGrid.TextMatrix(intCounter, 2, FCConvert.ToString(rsData.Get_Fields_String("Description")));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					vsGrid.TextMatrix(intCounter, 3, FCConvert.ToString(rsData.Get_Fields("Year")));
					vsGrid.TextMatrix(intCounter, 4, FCConvert.ToString(rsData.Get_Fields_String("Make")));
					vsGrid.TextMatrix(intCounter, 5, FCConvert.ToString(rsData.Get_Fields_String("Model")));
					vsGrid.TextMatrix(intCounter, 6, Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00"));
					vsGrid.TextMatrix(intCounter, 7, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DepreciationUnits"))));
					vsGrid.TextMatrix(intCounter, 8, FCConvert.ToString(rsData.Get_Fields_String("DepreciationDesc")));
					vsGrid.TextMatrix(intCounter, 9, FCConvert.ToString(Conversion.Val(rsData.Get_Fields_String("DepreciationUnits")) + Conversion.Val(rsData.Get_Fields_Int32("TotalUsed"))));
					rsData.MoveNext();
				}
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsGrid.Rows - 1, 8, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 6, vsGrid.Rows - 1, 7, FCGrid.AlignmentSettings.flexAlignRightCenter);
			}
			vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
		}

		private void mskDepreciationDate_Enter(object sender, System.EventArgs e)
		{
			//this.mskDepreciationDate.SelectionStart = 0;
			//this.mskDepreciationDate.SelectionLength = Marshal.SizeOf(this.mskDepreciationDate.Text);
			this.mskDepreciationDate.SelectAll();
		}

		private void vsGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsGrid.TextMatrix(vsGrid.Row, 1, FCConvert.ToString(1));
			boolDataChanged = true;
			vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsGrid.Row, 9, FCGrid.AlignmentSettings.flexAlignRightCenter);
		}

		private void vsGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsGrid.Col != 9)
				vsGrid.EditMask = string.Empty;
		}

		private void vsGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsGrid.MouseRow == 0)
			{
				// sort this grid
				if (vsGrid.Rows < 2)
					return;
				vsGrid.Select(1, vsGrid.Col);
				if (strSort == "flexSortGenericAscending")
				{
					vsGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					strSort = "flexSortGenericDescending";
				}
				else
				{
					vsGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					strSort = "flexSortGenericAscending";
				}
			}
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					(obj as MaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (FCConvert.ToBoolean(boolClear))
						(obj as FCComboBox).Clear();
				}
			}
			// obj
			vsGrid.Rows = 1;
			tmpString = new object[3 + 1];
		}

		private bool SaveRecord()
		{
			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 1)) == 1)
					{
						// then this record has changed
						if (FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
						}
					}
				}
				if (boolDataChanged)
					LoadRecord();
				SaveRecord = true;
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveRecord;
		}

		private void LoadRecord()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			vsGrid.Rows = 1;
			if (!blnSingleDepreciation)
			{
				rsData.OpenRecordset("Select * from tblMaster where Discarded = 0 AND DepreciationMethod = 2 ORDER BY Description", modGlobal.DatabaseNamePath);
			}
			else
			{
				rsData.OpenRecordset("Select * from tblMaster where ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationMethod = 2", modGlobal.DatabaseNamePath);
			}
			vsGrid.Rows = rsData.RecordCount() + 1;
			if (rsData.EndOfFile())
				return;
			for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
			{
				vsGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				vsGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
				vsGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Description"))));
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				vsGrid.TextMatrix(intCounter, 4, Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Year"))));
				vsGrid.TextMatrix(intCounter, 5, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Make"))));
				vsGrid.TextMatrix(intCounter, 6, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Model"))));
				vsGrid.TextMatrix(intCounter, 7, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_DateTime("DateLastDepreciation"))));
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				vsGrid.TextMatrix(intCounter, 8, FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Life")))) - Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("TotalUsed"))))));
				vsGrid.TextMatrix(intCounter, 9, string.Empty);
				vsGrid.TextMatrix(intCounter, 10, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Decimal("BasisCost"))));
				vsGrid.TextMatrix(intCounter, 11, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("UNRate"))));
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				vsGrid.TextMatrix(intCounter, 12, FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Life"))))));
				vsGrid.TextMatrix(intCounter, 13, FCConvert.ToString(0));
				// Val(Trim(rsData.Fields("BasisReduction")))
				vsGrid.TextMatrix(intCounter, 14, FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("TotalUsed"))))));
				vsGrid.TextMatrix(intCounter, 15, FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Decimal("SalvageValue"))))));
				rsData.MoveNext();
			}
			if (vsGrid.Rows > 1)
			{
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 3, vsGrid.Rows - 1, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 3, vsGrid.Rows - 1, 8, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 11, vsGrid.Rows - 1, 11, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			boolDataChanged = false;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			vsGrid.Select(0, 0);
			SaveRecord();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			vsGrid.Select(0, 0);
			if (SaveRecord())
				Close();
		}

		private void vsGrid_Enter(object sender, System.EventArgs e)
		{
			if (vsGrid.Rows > 1)
				vsGrid.Select(1, 2);
		}

		private void vsGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys code = e.KeyCode;
			modGridKeyMove.MoveGridProsition(vsGrid, 2, 13, code);
		}

		private void vsGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (vsGrid.Col == 2 || vsGrid.Col == 9)
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool ValidData()
		{
			bool ValidData = false;
			int intCounter;
			if (Information.IsDate(this.mskDepreciationDate.Text))
			{
			}
			else
			{
				MessageBox.Show("Invalid Depreciation Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				mskDepreciationDate.Focus();
				mskDepreciationDate.SelectionStart = 0;
				mskDepreciationDate.SelectionLength = this.mskDepreciationDate.Text.Length;
				return ValidData;
			}
			for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
			{
				if (FCConvert.ToInt32(vsGrid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) == -1)
				{
				}
				else
				{
					if (vsGrid.TextMatrix(intCounter, 9) == string.Empty)
					{
						MessageBox.Show("Current Units must be completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsGrid.Select(intCounter, 9);
						vsGrid.Focus();
						return ValidData;
					}
					else
					{
						if (Conversion.Val(vsGrid.TextMatrix(intCounter, 8)) < Conversion.Val(vsGrid.TextMatrix(intCounter, 9)))
						{
							MessageBox.Show("Current Units must be less then the remaining units.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							vsGrid.Select(intCounter, 9);
							vsGrid.Focus();
							return ValidData;
						}
					}
				}
			}
			ValidData = true;
			return ValidData;
		}

		private void vsGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsGrid.GetFlexRowIndex(e.RowIndex);
			int col = vsGrid.GetFlexColIndex(e.ColumnIndex);
			if (!Information.IsNumeric(vsGrid.EditText))
			{
				if (col == 9)
				{
					if (vsGrid.EditText != string.Empty)
					{
						MessageBox.Show("Invalid current units.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						vsGrid.Select(row, col);
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private string CompareDBToSL_20(double dblLife, double dblBasis, double dblDBRate)
		{
			return CompareDBToSL(ref dblLife, ref dblBasis, ref dblDBRate);
		}

		private string CompareDBToSL(ref double dblLife, ref double dblBasis, ref double dblDBRate)
		{
			string CompareDBToSL = "";
			double dblSLDep;
			double dblDBDep;
			dblSLDep = dblBasis / (dblLife - intBYear);
			dblDBDep = dblBasis * (dblDBRate / dblLife);
			if (dblSLDep > dblDBDep)
			{
				CompareDBToSL = "SL";
			}
			else
			{
				CompareDBToSL = "DB";
			}
			return CompareDBToSL;
		}

		private void cmdIgnore_Click(object sender, EventArgs e)
		{
			mnuIgnore_Click(sender, e);
		}

		private void cmdCalculate_Click(object sender, EventArgs e)
		{
			mnuProcess_Click();
		}

		private void cmdSavePrint_Click(object sender, EventArgs e)
		{
			mnuSaveExit_Click(sender, e);
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuPrint_Click(sender, e);
		}

		private void cmdPrintPreview_Click(object sender, EventArgs e)
		{
			mnuPrintPreview_Click(sender, e);
		}
	}
}
