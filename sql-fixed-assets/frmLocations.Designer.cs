﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmLocations.
	/// </summary>
	partial class frmLocations : BaseForm
	{
		public fecherFoundation.FCComboBox cboType;
		public fecherFoundation.FCGrid LocationGrid;
		public fecherFoundation.FCLabel lblLocationType;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cboType = new fecherFoundation.FCComboBox();
			this.LocationGrid = new fecherFoundation.FCGrid();
			this.lblLocationType = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.LocationGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 373);
			this.BottomPanel.Size = new System.Drawing.Size(582, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboType);
			this.ClientArea.Controls.Add(this.LocationGrid);
			this.ClientArea.Controls.Add(this.lblLocationType);
			this.ClientArea.Size = new System.Drawing.Size(582, 313);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(582, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(183, 30);
			this.HeaderText.Text = "Location Codes";
			// 
			// cboType
			// 
			this.cboType.AutoSize = false;
			this.cboType.BackColor = System.Drawing.SystemColors.Window;
			this.cboType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboType.FormattingEnabled = true;
			this.cboType.Location = new System.Drawing.Point(189, 30);
			this.cboType.Name = "cboType";
			this.cboType.Size = new System.Drawing.Size(176, 40);
			this.cboType.TabIndex = 1;
			this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
			this.cboType.DropDown += new System.EventHandler(this.cboType_DropDown);
			// 
			// LocationGrid
			// 
			this.LocationGrid.AllowSelection = false;
			this.LocationGrid.AllowUserToResizeColumns = false;
			this.LocationGrid.AllowUserToResizeRows = false;
			this.LocationGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.LocationGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.LocationGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.LocationGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.LocationGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.LocationGrid.BackColorSel = System.Drawing.Color.Empty;
			this.LocationGrid.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.LocationGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.LocationGrid.ColumnHeadersHeight = 30;
			this.LocationGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.LocationGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.LocationGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.LocationGrid.ExtendLastCol = true;
			this.LocationGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.LocationGrid.FrozenCols = 0;
			this.LocationGrid.GridColor = System.Drawing.Color.Empty;
			this.LocationGrid.Location = new System.Drawing.Point(30, 90);
			this.LocationGrid.Name = "LocationGrid";
			this.LocationGrid.ReadOnly = true;
			this.LocationGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.LocationGrid.RowHeightMin = 0;
			this.LocationGrid.Rows = 50;
			this.LocationGrid.ShowColumnVisibilityMenu = false;
			this.LocationGrid.Size = new System.Drawing.Size(522, 205);
			this.LocationGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.LocationGrid.TabIndex = 2;
			this.LocationGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.LocationGrid_KeyPressEdit);
			this.LocationGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.LocationGrid_AfterEdit);
			this.LocationGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.LocationGrid_ValidateEdit);
			this.LocationGrid.CurrentCellChanged += new System.EventHandler(this.LocationGrid_RowColChange);
			this.LocationGrid.KeyPress += new Wisej.Web.KeyPressEventHandler(this.LocationGrid_KeyPressEvent);
			this.LocationGrid.Click += new System.EventHandler(this.LocationGrid_ClickEvent);
			// 
			// lblLocationType
			// 
			this.lblLocationType.Location = new System.Drawing.Point(30, 44);
			this.lblLocationType.Name = "lblLocationType";
			this.lblLocationType.Size = new System.Drawing.Size(106, 21);
			this.lblLocationType.TabIndex = 0;
			this.lblLocationType.Text = "LOCATION TYPE";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuNew,
				this.mnuDelete,
				this.mnuSP2,
				this.mnuSave,
				this.mnuSaveExit,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 2;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                      ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 4;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 5;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(446, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdNew.Size = new System.Drawing.Size(45, 24);
			this.cmdNew.TabIndex = 17;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(495, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.cmdDelete.Size = new System.Drawing.Size(57, 24);
			this.cmdDelete.TabIndex = 16;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(253, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 5;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmLocations
			// 
			this.ClientSize = new System.Drawing.Size(582, 469);
			this.KeyPreview = true;
			this.Name = "frmLocations";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Location Codes";
			this.Load += new System.EventHandler(this.frmLocations_Load);
			this.Activated += new System.EventHandler(this.frmLocations_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLocations_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLocations_KeyPress);
			this.Resize += new System.EventHandler(this.frmLocations_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.LocationGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdNew;
		private FCButton cmdDelete;
		private FCButton cmdSave;
	}
}
