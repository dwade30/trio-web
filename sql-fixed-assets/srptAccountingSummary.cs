﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for srptAccountingSummary.
	/// </summary>
	public partial class srptAccountingSummary : BaseSectionReport
	{
		public static srptAccountingSummary InstancePtr
		{
			get
			{
				return (srptAccountingSummary)Sys.GetInstance(typeof(srptAccountingSummary));
			}
		}

		protected srptAccountingSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOriginalCostSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		//clsDRWrapper rsData = new clsDRWrapper();
		int intPageNumber;
		int intArrayID;

		public srptAccountingSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ErrorHandler
				// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
				if (!modVariables.Statics.boolBudgetaryExists)
				{
					eArgs.EOF = true;
					return;
				}
				if (intArrayID == Information.UBound(modVariables.Statics.AccountStrut, 1) + 1)
				{
					eArgs.EOF = true;
					return;
				}
				txtAccount.Text = modVariables.Statics.AccountStrut[intArrayID].Account;
				if (modVariables.Statics.AccountStrut[intArrayID].Amount < 0)
				{
					txtAmount.Text = Strings.Format(modVariables.Statics.AccountStrut[intArrayID].Amount * -1, "0.00") + " CR";
				}
				else
				{
					txtAmount.Text = FCConvert.ToString(modVariables.Statics.AccountStrut[intArrayID].Amount) + " DB";
				}
				txtDescription.Text = modAccountTitle.ReturnAccountDescription(modVariables.Statics.AccountStrut[intArrayID].Account);
				intArrayID += 1;
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Unable to build Account Summary report (Fetch Data).", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			intArrayID = 1;
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
		}
		
	}
}
