﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Global;
using SharedApplication;
using SharedApplication.Extensions;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmMaster.
	/// </summary>
	public partial class frmMaster : BaseForm
	{
		public frmMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblUnits = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblUnits.AddControlArrayElement(lblUnits_1, 1);
			this.lblUnits.AddControlArrayElement(lblUnits_0, 0);
            cmbLocationType.SelectedIndexChanged += CmbLocationType_SelectedIndexChanged;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            txtYear.AllowOnlyNumericInput();
		}

        private void CmbLocationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLocationType.SelectedIndex >= 0)
            {
                LoadLocations(cmbLocationType.ItemData(cmbLocationType.SelectedIndex));
            }
        }


        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmMaster InstancePtr
		{
			get
			{
				return (frmMaster)Sys.GetInstance(typeof(frmMaster));
			}
		}

		protected frmMaster _InstancePtr = null;

		string[] strSplit = null;
		string strSort = "";
		public bool boolDataChanged;
		bool boolLoading;
		bool blnRunInitialDepreciation;
		string[] DivDescriptions = new string[5000 + 1];
		string[] DeptDescriptions = new string[5000 + 1];
		clsGridAccount vsPurchase = new clsGridAccount();
		clsGridAccount vsExpense = new clsGridAccount();
		clsGridAccount vsAsset = new clsGridAccount();
		clsGridAccount vsDepreciation = new clsGridAccount();
		public bool blnFromSearch;
		bool blnDepreciate;

		private void CheckCustomizeSettings()
		{
			if (modVariables.Statics.boolVendorsFromBud)
			{
				cboVendor.BackColor = Color.White;
			}
			else
			{
				cboVendor.BackColor = Color.White;
			}
			if (modVariables.Statics.boolDeptDivFromBud)
			{
				if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) != 0)
				{
					modVariables.Statics.boolUseDivision = true;
				}
				else
				{
					// if div value = 0 then they do not use division
					modVariables.Statics.boolUseDivision = false;
				}
			}
		}

		private void cboClassCode_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void cboClassCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			if (this.cboClassCode.SelectedIndex < 0)
				return;
			fraDepreciationMethod.Visible = cboClassCode.SelectedIndex != 1;
			rsData.OpenRecordset("Select * from tblClassCodes where ID = " + FCConvert.ToString(this.cboClassCode.ItemData(this.cboClassCode.SelectedIndex)), modGlobal.DEFAULTDATABASENAME);
			if (rsData.EndOfFile())
			{
				// do nothing
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				if (Conversion.Val(rsData.Get_Fields("Life")) != 0)
				{
					// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
					txtLife.Text = FCConvert.ToString(rsData.Get_Fields("Life"));
				}
			}
			rsData = null;
		}

		private void cboClassCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(cboClassCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cboDept_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboDept.SelectedIndex >= 0)
			{
				LoadDivCombos(ref cboDiv);
			}
			if (cboDept.SelectedIndex < 0)
			{
			}
			else if (cboDiv.SelectedIndex < 0)
			{
				txtDeptDivDescription.Text = DeptDescriptions[cboDept.SelectedIndex];
			}
			else
			{
				txtDeptDivDescription.Text = DeptDescriptions[cboDept.SelectedIndex] + " - " + DivDescriptions[cboDiv.SelectedIndex];
			}
			if (Strings.Trim(txtDeptDivDescription.Text) == "-")
				txtDeptDivDescription.Text = string.Empty;
			if (Strings.Right(Strings.Trim(txtDeptDivDescription.Text), 1) == "-")
				txtDeptDivDescription.Text = Strings.Left(Strings.Trim(txtDeptDivDescription.Text), Strings.Trim(txtDeptDivDescription.Text).Length - 1);
			boolDataChanged = true;
		}

		private void cboDiv_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboDept.SelectedIndex < 0)
			{
			}
			else if (cboDiv.SelectedIndex < 0)
			{
				txtDeptDivDescription.Text = DeptDescriptions[cboDept.SelectedIndex];
			}
			else
			{
				txtDeptDivDescription.Text = DeptDescriptions[cboDept.SelectedIndex] + " - " + DivDescriptions[cboDiv.SelectedIndex];
			}
			boolDataChanged = true;
		}

		private void cboVendor_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (boolLoading)
				return;
			if (cboVendor.SelectedIndex < 0)
			{
			}
			else
			{
				if (cboVendor.SelectedIndex == 1)
				{
					string strReturn = "";
					strReturn = Interaction.InputBox("Enter name for vendor", "Enter outdated vendor name", null);
					strReturn = Strings.Trim(strReturn);
					if (strReturn == string.Empty)
					{
						MessageBox.Show("Invalid vendor name.", "Invalid Vendor Name", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						//FC:FINAL:AM:#i379 - setting the item will trigger SelectedIndexChanged again
						boolLoading = true;
						cboVendor.Items[1] = strReturn;
						boolLoading = false;
					}
				}
			}
			boolDataChanged = true;
		}

		private void chkDiscarded_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDiscarded.CheckState == Wisej.Web.CheckState.Checked)
			{
				//fraDiscardedDate.Enabled = true;
				txtDiscardedDate.Enabled = true;
			}
			else
			{
				txtDiscardedDate.Text = "";
				//fraDiscardedDate.Enabled = false;
				txtDiscardedDate.Enabled = false;
			}
		}

		private void chkExpensedFirstYear_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void chkFederalFunds_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void frmMaster_Activated(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM: disable the menu items here
			mnuVendors.Enabled = !modVariables.Statics.boolVendorsFromBud;
			mnuDeptDiv.Enabled = !modVariables.Statics.boolDeptDivFromBud;
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			if (KeyAscii == Keys.Return)
			{
				// If Me.ActiveControl.Name <> "txtWarrantyDescription" Then
				Support.SendKeys("{TAB}", false);
				// End If
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMaster_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				boolLoading = true;
				blnDepreciate = false;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
				txtAcct1.Row = 0;
				txtAcct1.Col = 0;
				txtAcct2.Row = 0;
				txtAcct2.Col = 0;
				txtAcct3.Row = 0;
				txtAcct3.Col = 0;
				txtAcct4.Row = 0;
				txtAcct4.Col = 0;
				vsPurchase.GRID7Light = txtAcct2;
				vsAsset.GRID7Light = txtAcct4;
				vsExpense.GRID7Light = txtAcct1;
				vsDepreciation.GRID7Light = txtAcct3;
				vsPurchase.AccountCol = -1;
				vsExpense.AccountCol = -1;
				vsAsset.AccountCol = -1;
				vsDepreciation.AccountCol = -1;
                vsPurchase.DefaultAccountType = "E";
                vsExpense.DefaultAccountType = "E";
                vsAsset.DefaultAccountType = "G";
                vsDepreciation.DefaultAccountType = "G";
				if (modVariables.Statics.boolDeptDivFromBud)
				{
					mnuDeptDiv.Enabled = false;
				}
				if (modVariables.Statics.boolVendorsFromBud)
				{
					mnuVendors.Enabled = false;
				}
				modAccountTitle.SetAccountFormats();
				SetGridProperties();
				LoadConditionCombo();
				//Application.DoEvents();
				optDepreciation_Click(0);
				
				CheckCustomizeSettings();
				SetLocationCaptions();
                LoadRecord(modVariables.Statics.glngMasterIDNumber);
                modGlobalFunctions.SetFixedSize(this, 0);
				modGlobalFunctions.SetTRIOColors(this, false);
				txtDeptDivDescription.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				txtDeptDivDescription.Font = FCUtils.FontChangeSize(txtDeptDivDescription.Font, 7);
				cboDiv.Visible = modVariables.Statics.boolUseDivision;
				lblDash.Visible = modVariables.Statics.boolUseDivision;
				cboDiv.Locked = !modVariables.Statics.boolUseDivision;
				vsExpense.Validation = modVariables.Statics.boolDeptDivFromBud;
				vsAsset.Validation = modVariables.Statics.boolDeptDivFromBud;
				vsPurchase.Validation = modVariables.Statics.boolDeptDivFromBud;
				vsDepreciation.Validation = modVariables.Statics.boolDeptDivFromBud;
				//Application.DoEvents();
				if (modVariables.Statics.boolBudgetaryExists)
				{
					modGlobalRoutines.GetBudFormats();
				}
				//Application.DoEvents();
				modVariables.Statics.gboolMasterOpen = true;
				boolDataChanged = false;
				boolLoading = false;
				if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.ADDASSET)) != "N")
				{
					mnuNew.Enabled = true;
				}
				else
				{
					mnuNew.Enabled = false;
				}
				if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.DELETEASSET)) != "N")
				{
					mnuDelete.Enabled = true;
				}
				else
				{
					mnuDelete.Enabled = false;
				}
				if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.ADDVENDORS)) != "N")
				{
					mnuVendors.Enabled = true;
				}
				else
				{
					mnuVendors.Enabled = false;
				}
				if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.ADDDEPT)) != "N")
				{
					mnuDeptDiv.Enabled = true;
				}
				else
				{
					mnuDeptDiv.Enabled = false;
				}
				if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.ADDLOCATIONS)) != "N")
				{
					mnuLocations.Enabled = true;
				}
				else
				{
					mnuLocations.Enabled = false;
				}
				if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.CUSTOMIZE)) != "N")
				{
					mnuLocationCaptions.Enabled = true;
				}
				else
				{
					mnuLocationCaptions.Enabled = false;
				}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
		}

		public void SetLocationCaptions()
		{
            cmbLocationType.Clear();

			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblSettings", modGlobal.DatabaseNamePath);
            int index = 0;
            int firstType = 0;
            if (!rsData.EndOfFile())
			{
                if (!string.IsNullOrWhiteSpace(rsData.Get_Fields_String("Location1Caption")))
                {
                    cmbLocationType.AddItem(rsData.Get_Fields_String("Location1Caption"));
                    cmbLocationType.ItemData(index, 1);
                    firstType = 1;
                    index++;
                }

                if (!string.IsNullOrWhiteSpace(rsData.Get_Fields_String("Location2Caption")))
                {
                    cmbLocationType.AddItem(rsData.Get_Fields_String("Location2Caption"));
                    cmbLocationType.ItemData(index, 2);
                    if (firstType == 0)
                    {
                        firstType = 2;
                    }
                    index++;
                }
                
                if (!string.IsNullOrWhiteSpace(rsData.Get_Fields_String("Location3Caption")))
                {
                    cmbLocationType.AddItem(rsData.Get_Fields_String("Location3Caption"));
                    if (firstType == 0)
                    {
                        firstType = 3;
                    }
                    cmbLocationType.ItemData(index, 3);
                    index++;
                }

                if (!string.IsNullOrWhiteSpace(rsData.Get_Fields_String("Location4Caption")))
                {
                    cmbLocationType.AddItem(rsData.Get_Fields_String("Location4Caption"));
                    if (firstType == 0)
                    {
                        firstType = 4;
                    }
                    cmbLocationType.ItemData(index, 4);
                }

                if (firstType > 0)
                {
                    //fix a bug that clears the first item data once a second item is added to the combo
                    cmbLocationType.ItemData(0, firstType);
                }
			}
			
		}

		public void SetGridProperties()
		{
			vsDepreciationsPosted.FixedCols = 0;
			vsDepreciationsPosted.FixedRows = 1;
			vsDepreciationsPosted.Rows = 1;
			vsDepreciationsPosted.Cols = 6;
			vsDepreciationsPosted.ColHidden(0, true);
			vsDepreciationsPosted.ColHidden(1, true);

            //FC:FINAL:BSE: #2094 align header
            vsDepreciationsPosted.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsDepreciationsPosted.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsDepreciationsPosted.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsDepreciationsPosted.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsDepreciationsPosted.ColDataType(2, FCGrid.DataTypeSettings.flexDTDate);
			//vsDepreciationsPosted.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsDepreciationsPosted.TextMatrix(0, 0, "ID");
			vsDepreciationsPosted.TextMatrix(0, 1, "Changed");
			vsDepreciationsPosted.TextMatrix(0, 2, "Date");
			vsDepreciationsPosted.TextMatrix(0, 3, "Amount");
			vsDepreciationsPosted.TextMatrix(0, 4, "Units");
			vsDepreciationsPosted.TextMatrix(0, 5, "Description");
			vsDepreciationsPosted.Select(0, 0);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							modVariables.Statics.gboolCancelSelected = false;
							if (SaveRecord(modVariables.Statics.glngMasterIDNumber))
							{
								Close();
								if (blnFromSearch && !blnDepreciate)
								{
									frmSearch.InstancePtr.Show(App.MainForm);
								}
							}
							else
							{
								e.Cancel = true;
								this.Focus();
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							modVariables.Statics.gboolCancelSelected = false;
							Close();
							if (blnFromSearch && !blnDepreciate)
							{
								frmSearch.InstancePtr.Show(App.MainForm);
							}
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							modVariables.Statics.gboolCancelSelected = true;
							e.Cancel = true;
							this.Focus();
							break;
						}
				}
				//end switch
			}
			else
			{
				if (modVariables.Statics.gboolCancelSelected)
				{
					e.Cancel = true;
				}
				else
				{
					// Unload Me
					if (blnFromSearch && !blnDepreciate)
					{
						frmSearch.InstancePtr.Show(App.MainForm);
					}
				}
			}
            //FC:FINAL:AM:#4523 - close search form too
            if (blnDepreciate)
            {
                frmSearch.InstancePtr.Close();
            }
			modVariables.Statics.gboolMasterOpen = false;
		}

		private void frmMaster_Resize(object sender, System.EventArgs e)
		{
            //vsDepreciationsPosted.ColWidth(0, FCConvert.ToInt32(vsDepreciationsPosted.WidthOriginal * 0.04));
            //vsDepreciationsPosted.ColWidth(1, FCConvert.ToInt32(vsDepreciationsPosted.WidthOriginal * 0.04));
            var gridWidth = vsDepreciationsPosted.WidthOriginal;

            vsDepreciationsPosted.ColWidth(2, FCConvert.ToInt32(gridWidth * 0.25));
			vsDepreciationsPosted.ColWidth(3, FCConvert.ToInt32(gridWidth * 0.24));
			vsDepreciationsPosted.ColWidth(4, FCConvert.ToInt32(gridWidth * 0.2));
			vsDepreciationsPosted.ColWidth(5, FCConvert.ToInt32(gridWidth * 0.23));
            //FC:FINAL:BSE: #2094 align header 
            vsDepreciationsPosted.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsDepreciationsPosted.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsDepreciationsPosted.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsDepreciationsPosted.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        }

		public void clrForm(bool boolClear = true)
		{
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					if (obj.Name != "txtDB" && obj.Name != "txtLife")
					{
						(obj as MaskedTextBox).Mask = "##/##/####";
					}
				}
				if (obj is FCComboBox)
				{
					if ((obj as FCComboBox).ListCount > 0)
					{
						(obj as FCComboBox).ListIndex = 0;
					}
				}
			}
			// obj
			vsDepreciationsPosted.Rows = 1;
			txtAcct1.TextMatrix(0, 0, "");
			txtAcct2.TextMatrix(0, 0, "");
			txtAcct3.TextMatrix(0, 0, "");
			txtAcct4.TextMatrix(0, 0, "");
			mnuFileComments.Enabled = false;
			Image1.Visible = false;
		}

		private bool SaveRecord(int lngID = 0)
		{
			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				int lngTempRecordNumber = 0;
				// vbPorter upgrade warning: dblCurrentValue As double	OnWriteFCConvert.ToDecimal(
				double dblCurrentValue;
				// ADD VALIDATION ROUTINE   DAVE
				dblCurrentValue = FCConvert.ToDouble(CalcCurrentValue());
				if (dblCurrentValue < 0)
				{
					MessageBox.Show("Current Value cannot be less then 0.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtBasisCost.Focus();
					txtBasisCost.Text = "0.00";
					txtBasisCost.SelectionStart = 0;
					txtBasisCost.SelectionLength = 1;
					SaveRecord = false;
					return SaveRecord;
				}
				else
				{
					txtValue.Text = Strings.Format(dblCurrentValue, "#,##0.00");
				}
				if (Strings.Trim(txtDescription.Text) == "")
				{
					MessageBox.Show("You must enter a description for this asset before you may proceed.", "Enter Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtDescription.Focus();
					SaveRecord = false;
					return SaveRecord;
				}
				if (chkDiscarded.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (!Information.IsDate(txtDiscardedDate.Text))
					{
						MessageBox.Show("You must enter a discarded date for this asset before you may proceed.", "Enter Discarded Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtDiscardedDate.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
				}
				if (cboClassCode.SelectedIndex != 1)
				{
					if (Strings.Trim(txtLife.Text) == "")
					{
						MessageBox.Show("You must enter a life for this asset before you may proceed.", "Enter Life", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtLife.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					if (Strings.Trim(txtBasisCost.Text) == "")
					{
						MessageBox.Show("You must enter a basis cost for this asset before you may proceed.", "Enter Basis Cost", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtBasisCost.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					else if (FCConvert.ToDecimal(txtBasisCost.Text) == 0)
					{
						MessageBox.Show("You must enter a basis cost for this asset before you may proceed.", "Enter Basis Cost", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtBasisCost.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					//FC:FINAL:AM:#i376 - compare correct text
					//if (Strings.InStr(1, mskDateLastDepreciation.Text, "_", CompareConstants.vbBinaryCompare) != 0)
					if (mskDateLastDepreciation.Text.Replace(" ", "") == "//")
					{
						MessageBox.Show("You must enter a valid last depreciation date for this asset before you may proceed.", "Enter Last Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						mskDateLastDepreciation.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					else if (!Information.IsDate(mskDateLastDepreciation.Text))
					{
						MessageBox.Show("You must enter a valid last depreciation date for this asset before you may proceed.", "Enter Last Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						mskDateLastDepreciation.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					//FC:FINAL:AM:#i376 - compare correct text
					//if (Strings.InStr(1, mskPurchaseDate.Text, "_", CompareConstants.vbBinaryCompare) != 0)
					if (mskPurchaseDate.Text.Replace(" ", "") == "//")
					{
						MessageBox.Show("You must enter a valid purchase date for this asset before you may proceed.", "Enter Purchase Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						mskPurchaseDate.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					else if (!Information.IsDate(mskPurchaseDate.Text))
					{
						MessageBox.Show("You must enter a valid purchase date for this asset before you may proceed.", "Enter Purchase Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						mskPurchaseDate.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					if (!modValidateAccount.AccountValidate(txtAcct1.TextMatrix(0, 0)))
					{
						MessageBox.Show("You must enter a valid expense account for this asset before you may proceed.", "Enter Expense Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtAcct1.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
					if (!modValidateAccount.AccountValidate(txtAcct3.TextMatrix(0, 0)))
					{
						MessageBox.Show("You must enter a valid depreciation account for this asset before you may proceed.", "Enter Depreciation Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtAcct3.Focus();
						SaveRecord = false;
						return SaveRecord;
					}
				}
				rsData.OpenRecordset("Select * from tblMaster where ID = " + FCConvert.ToString(lngID), modGlobal.DatabaseNamePath);
				if (rsData.EndOfFile())
				{
					// check security permissions before letting the user add an asset
					if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.ADDASSET)) != "N")
					{
						rsData.AddNew();
					}
					else
					{
						MessageBox.Show("You do not have permissions to perform this action.", "No Perissions", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						SaveRecord = false;
						return SaveRecord;
					}
				}
				else
				{
					// check security permissions before letting the user edit an asset
					if (FCConvert.ToString(modGlobal.Statics.clsFASecurity.Check_Permissions(modStartup.EDITASSET)) != "N")
					{
						rsData.Edit();
					}
					else
					{
						MessageBox.Show("You do not have permissions to perform this action", "No Perissions", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						SaveRecord = false;
						return SaveRecord;
					}
				}
				rsData.Set_Fields("Description", txtDescription.Text);
				rsData.Set_Fields("Year", txtYear.Text);
				rsData.Set_Fields("Make", txtMake.Text);
				rsData.Set_Fields("Model", txtModel.Text);
				rsData.Set_Fields("SerialNumber", txtSerialNumber.Text);
				rsData.Set_Fields("TagNumber", txtTagNumber.Text);
				rsData.Set_Fields("WarrantyDescription", txtWarrantyDescription.Text);
				rsData.Set_Fields("Condition", cboCondition.Text);
				rsData.Set_Fields("Discarded", chkDiscarded.CheckState == Wisej.Web.CheckState.Checked);
				if (Information.IsDate(txtDiscardedDate.Text))
				{
					rsData.Set_Fields("DiscardedDate", DateAndTime.DateValue(txtDiscardedDate.Text));
				}
				else
				{
					rsData.Set_Fields("DiscardedDate", null);
				}
				if (mskPurchaseDate.ClipText == string.Empty)
				{
					rsData.Set_Fields("PurchaseDate", null);
				}
				else
				{
					rsData.Set_Fields("PurchaseDate", mskPurchaseDate.Text);
				}
				if (mskWarrantyDate.ClipText == string.Empty)
				{
					rsData.Set_Fields("WarrantyExpDate", null);
				}
				else
				{
					rsData.Set_Fields("WarrantyExpDate", mskWarrantyDate.Text);
				}

                var locationType = GetLocationType();
                var locationCode = GetLocationCode();
                var location1Id = 0;
                var location2Id = 0;
                var location3Id = 0;
                var location4Id = 0;
                switch (locationType)
                {
                    case 1:
                        location1Id = locationCode;
                        break;
                    case 2:
                        location2Id = locationCode;
                        break;
                    case 3:
                        location3Id = locationCode;
                        break;
                    case 4:
                        location4Id = locationCode;
                        break;
                    default:
                        break;
                }
                rsData.Set_Fields("Location1Code", location1Id);
                rsData.Set_Fields("Location2Code",location2Id);
                rsData.Set_Fields("Location3Code", location3Id);
                rsData.Set_Fields("Location4Code", location4Id);
                
				if (cboDept.SelectedIndex < 0)
				{
					// do nothing
				}
				else
				{
					rsData.Set_Fields("Dept", cboDept.Text);
				}
				if (cboDiv.SelectedIndex < 0)
				{
					// do nothing
				}
				else
				{
					rsData.Set_Fields("Div", cboDiv.Text);
				}
				if (cboVendor.SelectedIndex < 0)
				{
					// do nothing
				}
				else
				{
					if (cboVendor.ItemData(cboVendor.SelectedIndex) == 0)
					{
						rsData.Set_Fields("ManualVendor", cboVendor.Items[cboVendor.SelectedIndex].ToString());
						rsData.Set_Fields("VendorCode", 0);
					}
					else
					{
						rsData.Set_Fields("ManualVendor", string.Empty);
						rsData.Set_Fields("VendorCode", cboVendor.ItemData(cboVendor.SelectedIndex));
					}
				}
				if (cboClassCode.SelectedIndex < 0)
				{
					// do nothing
				}
				else
				{
					if (cboClassCode.ItemData(cboClassCode.SelectedIndex) == 0)
					{
						rsData.Set_Fields("ManualClassCode", cboClassCode.Items[cboClassCode.SelectedIndex].ToString());
						rsData.Set_Fields("ClassCode", 0);
					}
					else
					{
						rsData.Set_Fields("ManualClassCode", string.Empty);
						rsData.Set_Fields("ClassCode", cboClassCode.ItemData(cboClassCode.SelectedIndex));
					}
				}
				rsData.Set_Fields("FederalFunds", (0 != (chkFederalFunds.CheckState) ? 1 : 0));
				rsData.Set_Fields("ExpensedFirstYear", (0 != (chkExpensedFirstYear.CheckState) ? 1 : 0));
				rsData.Set_Fields("TotalDepreciated", FCConvert.ToDecimal(txtTotalDepreciated.Text));
				rsData.Set_Fields("BasisCost", FCConvert.ToDecimal(txtBasisCost.Text));
				if (Strings.Trim(txtReplacementCost.Text) != "")
				{
					rsData.Set_Fields("ReplacementCost", FCConvert.ToDecimal(txtReplacementCost.Text));
				}
				else
				{
					rsData.Set_Fields("ReplacementCost", 0);
				}
				rsData.Set_Fields("SalvageValue", FCConvert.ToDecimal(txtSalvageValue.Text));
				if (Strings.Trim(txtImprovements.Text) != "")
				{
					rsData.Set_Fields("Improvements", FCConvert.ToDecimal(txtImprovements.Text));
				}
				else
				{
					rsData.Set_Fields("Improvements", 0);
				}
				rsData.Set_Fields("Acct1", txtAcct1.TextMatrix(0, 0));
				rsData.Set_Fields("Acct2", txtAcct2.TextMatrix(0, 0));
				rsData.Set_Fields("Acct3", txtAcct3.TextMatrix(0, 0));
				rsData.Set_Fields("Acct4", txtAcct4.TextMatrix(0, 0));
				if (cboClassCode.SelectedIndex != 1)
				{
					if (Strings.Trim(txtValue.Text) == "")
					{
						rsData.Set_Fields("TotalValue", 0);
					}
					else
					{
						rsData.Set_Fields("TotalValue", FCConvert.ToDecimal(txtValue.Text));
					}
				}
				else
				{
					rsData.Set_Fields("TotalValue", FCConvert.ToDecimal(txtBasisCost.Text));
				}
				if (txtTotalUsed.Text == "")
				{
					rsData.Set_Fields("TotalUsed", 0);
				}
				else
				{
					rsData.Set_Fields("TotalUsed", FCConvert.ToInt32(FCConvert.ToDouble(txtTotalUsed.Text)));
				}
				if (txtLife.Text == "")
				{
					rsData.Set_Fields("Life", 0);
				}
				else
				{
					rsData.Set_Fields("Life", FCConvert.ToInt32(FCConvert.ToDouble(txtLife.Text)));
				}
				rsData.Set_Fields("TotalPosted", txtTotalPosted.Text);
				if (mskDateLastDepreciation.ClipText == "")
				{
					rsData.Set_Fields("DateLastDepreciation", null);
				}
				else
				{
					rsData.Set_Fields("DateLastDepreciation", mskDateLastDepreciation.Text);
				}
				if (this.mebOutOfServiceDate.ClipText == "")
				{
					rsData.Set_Fields("OutOfServiceDate", null);
				}
				else
				{
					rsData.Set_Fields("OutOfServiceDate", mebOutOfServiceDate.Text);
				}
				rsData.Set_Fields("DBRate", cboRate.Text);
				rsData.Set_Fields("UNRate", txtUN.Text);
				if (cmbDepreciation.SelectedIndex == 0)
				{
					rsData.Set_Fields("DepreciationMethod", 0);
				}
				else if (cmbDepreciation.SelectedIndex == 1)
				{
					rsData.Set_Fields("DepreciationMethod", 1);
				}
				else if (cmbDepreciation.SelectedIndex == 2)
				{
					rsData.Set_Fields("DepreciationMethod", 2);
				}
				else if (cmbDepreciation.SelectedIndex == 3)
				{
					rsData.Set_Fields("DepreciationMethod", 3);
				}
				rsData.Update();
				lngTempRecordNumber = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
				SaveGrid(lngID);
				fraDepreciationMethod.Visible = cboClassCode.SelectedIndex != 1;
				SaveRecord = true;
				MessageBox.Show("Save completed successfully.", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
				boolDataChanged = false;
				modVariables.Statics.glngMasterIDNumber = lngTempRecordNumber;
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveRecord;
		}

        private int GetLocationCode()
        {
            if (cmbLocation.SelectedItem == null)
            {
                return 0;
            }

            return ((DescriptionIDCodePair) cmbLocation.SelectedItem).ID;
        }

        private int GetLocationType()
        {
            if (cmbLocationType.SelectedIndex < 0)
            {
                return 0;
            }

            return cmbLocationType.ItemData(cmbLocationType.SelectedIndex);
        }

        

        private void SetLocationCombo(int locationId)
        {
            foreach (DescriptionIDCodePair item in cmbLocation.Items)
            {
                if (item.ID == locationId)
                {
                    cmbLocation.SelectedItem = item;
                    return;
                }
            }

            cmbLocation.SelectedItem = null;
        }

        private void SetLocationType(int id)
        {
            for (int x = 0; x < cmbLocationType.Items.Count; x++)
            {
                if (cmbLocationType.ItemData(x) == id)
                {
                    cmbLocationType.SelectedIndex = x;
                    return;
                }
            }
        }

        public void LoadGrid(int lngID)
		{
			double dblTotal = 0;
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			double dblImprovements = 0;
			rsData.OpenRecordset("Select * from tblDepreciation where ItemID = " + FCConvert.ToString(lngID), modGlobal.DatabaseNamePath);
			vsDepreciationsPosted.Rows = rsData.RecordCount() + 1;
			for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
			{
				vsDepreciationsPosted.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				vsDepreciationsPosted.TextMatrix(intCounter, 1, FCConvert.ToString(0));
				vsDepreciationsPosted.TextMatrix(intCounter, 2, FCConvert.ToString(modDateRoutines.LongDate(rsData.Get_Fields_DateTime("DepreciationDate"))));
				vsDepreciationsPosted.TextMatrix(intCounter, 3, FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Decimal("DepreciationAmount"))));
				if (Conversion.Val(rsData.Get_Fields_Decimal("DepreciationAmount")) < 0)
				{
					dblImprovements -= Conversion.Val(rsData.Get_Fields_Decimal("DepreciationAmount"));
				}
				else
				{
					dblTotal += Conversion.Val(rsData.Get_Fields_Decimal("DepreciationAmount"));
				}
				vsDepreciationsPosted.TextMatrix(intCounter, 4, FCConvert.ToString(Conversion.Val(rsData.Get_Fields_String("DepreciationUnits"))));
				vsDepreciationsPosted.TextMatrix(intCounter, 5, FCConvert.ToString(rsData.Get_Fields_String("DepreciationDesc")));
				rsData.MoveNext();
			}
			txtTotalDepreciated.Text = Strings.Format(dblTotal, "#,##0.00");
			txtImprovements.Text = Strings.Format(dblImprovements, "#,##0.00");
			vsDepreciationsPosted.ColFormat(3, "#,##0.00");
			vsDepreciationsPosted.ColFormat(4, "#,##0.00");
		}

		private void Image1_Click(object sender, System.EventArgs e)
		{
			mnuFileComments_Click();
		}

		private void mebOutOfServiceDate_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.mebOutOfServiceDate.SelectAll();
		}

		private void mebOutOfServiceDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (mebOutOfServiceDate.Text.Replace(" ", "") == "//")
				return;
			if (Information.IsDate(mebOutOfServiceDate.Text.Replace("_", "")))
			{
				this.mebOutOfServiceDate.Text = FCConvert.ToString(modDateRoutines.LongDate(mebOutOfServiceDate.Text.Replace("_", "")));
			}
			else
			{
				MessageBox.Show("Invalid Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.Cancel = true;
			}
		}

		private void mnuClass_Click(object sender, System.EventArgs e)
		{
			frmClassCodes.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
			LoadClassCodeCombos(ref cboClassCode);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			if (modVariables.Statics.glngMasterIDNumber > 0)
			{
				if (MessageBox.Show("Are you sure you wish to delete this item?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsData.Execute("Delete from tblMaster where ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber), modGlobal.DatabaseNamePath);
					clrForm();
					MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolDataChanged = false;
					mnuExit_Click();
				}
			}
		}

		private void mnuDeletePosted_Click()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			if (vsDepreciationsPosted.Row > 1)
			{
				if (MessageBox.Show("Are you sure you wish to delete this item?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsData.Execute("Delete from tblDepreciation where ID = " + vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Row, 0), modGlobal.DatabaseNamePath);
					vsDepreciationsPosted.RemoveItem(vsDepreciationsPosted.Row);
					MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuDepreciate_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("SELECT * from tblMaster where ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber), modGlobal.DatabaseNamePath);
			if (rsData.Get_Fields_Boolean("Discarded") == false)
			{
				blnDepreciate = true;
				frmDepreciation.InstancePtr.blnSingleDepreciation = true;
				frmDepreciation.InstancePtr.Show(App.MainForm);
				Close();
			}
			else
			{
				MessageBox.Show("You may not post a depreciation for a discarded item.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
		}

		private void mnuDeptDiv_Click(object sender, System.EventArgs e)
		{
			int intDeptID = 0;
			int intDivID = 0;
			if (cboDept.SelectedIndex < 0)
			{
				intDeptID = 0;
			}
			else
			{
				intDeptID = cboDept.ItemData(cboDept.SelectedIndex);
			}
			if (cboDiv.SelectedIndex < 0)
			{
				intDivID = 0;
			}
			else
			{
				intDivID = cboDiv.ItemData(cboDiv.SelectedIndex);
			}
			frmDeptDiv.InstancePtr.blnFromMaster = true;
			frmDeptDiv.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
			LoadDeptCombos(ref cboDept);
			SearchComboBoxes(ref cboDept, intDeptID);
			SearchComboBoxes(ref cboDiv, intDivID);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
			//MDIParent.InstancePtr.Focus();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			mnuVendors.Enabled = !modVariables.Statics.boolVendorsFromBud;
			mnuDeptDiv.Enabled = !modVariables.Statics.boolDeptDivFromBud;
		}

		private void mnuFileComments_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsComm = new clsDRWrapper();
			frmCommentOld.InstancePtr.Init("FA", "tblMaster", "Comments", "ID", modVariables.Statics.glngMasterIDNumber, "Comment", "", 0, false, true);
			rsComm.OpenRecordset("SELECT * FROM tblMaster WHERE ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber), "TWFA0000.vb1");
			if (rsComm.EndOfFile() != true && rsComm.BeginningOfFile() != true)
			{
				if (Strings.Trim(FCConvert.ToString(rsComm.Get_Fields_String("Comments"))) != "")
				{
					Image1.Visible = true;
				}
				else
				{
					Image1.Visible = false;
				}
			}
			else
			{
				Image1.Visible = false;
			}
		}

		public void mnuFileComments_Click()
		{
			mnuFileComments_Click(mnuFileComments, new System.EventArgs());
		}

		private void mnuFileInitialDepreciation_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: Ans As short, int --> As DialogResult
			DialogResult Ans;
			object curNewAmount;
			if (boolDataChanged)
			{
				Ans = MessageBox.Show("You must save your changes before you may continue.  Do you wish to save now?", "Save and Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (Ans == DialogResult.Yes)
				{
					if (SaveRecord(modVariables.Statics.glngMasterIDNumber))
					{
						if (InitialDepreciationAlreadyRun())
						{
							curNewAmount = GetInitialDepreciationAmount();
							frmInput.InstancePtr.Init(ref curNewAmount, "Please enter the new initial depreciation amount.", "Enter Initial Depreciation", 1500, false, modGlobalConstants.InputDTypes.idtNumber);
							Decimal curNewAmountDecimal = FCConvert.ToDecimal(curNewAmount);
							if (curNewAmountDecimal > 0 && curNewAmountDecimal != GetInitialDepreciationAmount())
							{
								if (FCConvert.ToDecimal(txtBasisCost.Text) - FCConvert.ToDecimal(txtTotalDepreciated.Text) - FCConvert.ToDecimal(txtSalvageValue.Text) + GetInitialDepreciationAmount() - curNewAmountDecimal < 0)
								{
									MessageBox.Show("You may not change the initial amount to this amount because the item would then be depreciated below the salvage value.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
									return;
								}
								else
								{
									SetInitialDepreciationAmount(ref curNewAmountDecimal);
									LoadGrid(modVariables.Statics.glngMasterIDNumber);
									txtValue.Text = Strings.Format(FCConvert.ToDecimal(txtBasisCost.Text) - FCConvert.ToDecimal(txtTotalDepreciated.Text), "#,##0.00");
									SaveRecord(modVariables.Statics.glngMasterIDNumber);
									LoadRecord(modVariables.Statics.glngMasterIDNumber);
								}
							}
						}
						else
						{
							frmInitialDepreciation.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
						}
					}
				}
				else
				{
					return;
				}
			}
			else
			{
				if (InitialDepreciationAlreadyRun())
				{
					curNewAmount = GetInitialDepreciationAmount();
					frmInput.InstancePtr.Init(ref curNewAmount, "Please enter the new initial depreciation amount.", "Enter Initial Depreciation", 1500, false, modGlobalConstants.InputDTypes.idtNumber);
					Decimal curNewAmountDecimal = FCConvert.ToDecimal(curNewAmount);
					if (curNewAmountDecimal > 0 && curNewAmountDecimal != GetInitialDepreciationAmount())
					{
						if (FCConvert.ToDecimal(txtBasisCost.Text) - FCConvert.ToDecimal(txtTotalDepreciated.Text) - FCConvert.ToDecimal(txtSalvageValue.Text) + GetInitialDepreciationAmount() - curNewAmountDecimal < 0)
						{
							MessageBox.Show("You may not change the initial amount to this amount because the item would then be depreciated below the salvage value.", "Invalid Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
						else
						{
							SetInitialDepreciationAmount(ref curNewAmountDecimal);
							LoadGrid(modVariables.Statics.glngMasterIDNumber);
							txtValue.Text = Strings.Format(FCConvert.ToDecimal(txtBasisCost.Text) - FCConvert.ToDecimal(txtTotalDepreciated.Text), "#,##0.00");
							SaveRecord(modVariables.Statics.glngMasterIDNumber);
							LoadRecord(modVariables.Statics.glngMasterIDNumber);
						}
					}
				}
				else
				{
					frmInitialDepreciation.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
				}
			}
		}

		private void mnuFileRemoveDepreciation_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
			// vbPorter upgrade warning: Ans As short, int --> As DialogResult
			DialogResult Ans;
			Decimal curCurrentValue;
			clsDRWrapper rsAsset = new clsDRWrapper();
			int intArrayID = 0;
			string strAccount = "";
			clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
			clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
			if (vsDepreciationsPosted.Rows > 1)
			{
				Ans = MessageBox.Show("Are you sure you wish to remove the last depreciation posted for this asset?", "Remove Last Depreciation?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (Ans == DialogResult.Yes)
				{
					rsDelete.OpenRecordset("SELECT * FROM tblDepreciation WHERE ID = " + vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 0), "twfa0000.vb1");
					if (rsDelete.EndOfFile() != true && rsDelete.BeginningOfFile() != true)
					{
						if (rsDelete.Get_Fields_Decimal("DepreciationAmount") < 0)
						{
							txtImprovements.Text = Strings.Format(FCConvert.ToDecimal(txtImprovements.Text) + rsDelete.Get_Fields_Decimal("DepreciationAmount"), "#,##0.00");
						}
						else
						{
							txtTotalDepreciated.Text = Strings.Format(FCConvert.ToDecimal(txtTotalDepreciated.Text) - rsDelete.Get_Fields_Decimal("DepreciationAmount"), "#,##0.00");
						}
						curCurrentValue = CalcCurrentValue();
						txtValue.Text = Strings.Format(curCurrentValue, "#,##0.00");
						rsAsset.OpenRecordset("Select * from tblMaster where ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber), modGlobal.DatabaseNamePath);
						if (rsAsset.EndOfFile() != true && rsAsset.BeginningOfFile() != true)
						{
							rsAsset.Edit();
							rsAsset.Set_Fields("TotalValue", curCurrentValue);
							if (vsDepreciationsPosted.Rows - 1 > 1)
							{
								rsAsset.Set_Fields("DateLastDepreciation", vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 2, 2));
								mskDateLastDepreciation.Text = vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 2, 2);
							}
							else
							{
								rsAsset.Set_Fields("DateLastDepreciation", mskPurchaseDate.Text);
								mskDateLastDepreciation.Text = mskPurchaseDate.Text;
							}
							rsAsset.Update();
						}
						if (Conversion.Val(rsDelete.Get_Fields_Decimal("DepreciationAmount")) != 0 && modVariables.Statics.boolBudgetaryExists)
						{
							modVariables.Statics.AccountStrut = new modBudgetaryAccounting.FundType[0];
							intArrayID = 0;
							modAccountTitle.SetAccountFormats();
							strAccount = FCConvert.ToString(rsAsset.Get_Fields_String("Acct1"));
							Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
							modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
							modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
							modVariables.Statics.AccountStrut[intArrayID].Amount = rsDelete.Get_Fields_Decimal("DepreciationAmount") * -1;
							modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 2), "MM/dd/yy") + " Dep Removal";
							intArrayID += 1;
							strAccount = FCConvert.ToString(rsAsset.Get_Fields_String("Acct3"));
							Array.Resize(ref modVariables.Statics.AccountStrut, Information.UBound(modVariables.Statics.AccountStrut, 1) + 1 + 1);
							modVariables.Statics.AccountStrut[intArrayID].Account = strAccount;
							modVariables.Statics.AccountStrut[intArrayID].AcctType = "FA";
							modVariables.Statics.AccountStrut[intArrayID].Amount = rsDelete.Get_Fields_Decimal("DepreciationAmount");
							modVariables.Statics.AccountStrut[intArrayID].Description = Strings.Format(vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 2), "MM/dd/yy") + " Dep Removal";
							// If vsDepreciationsPosted.Rows - 1 > 1 Then
							modVariables.Statics.glngJournalNumber = modBudgetaryAccounting.AddToJournal(ref modVariables.Statics.AccountStrut, "FA", 0, DateTime.ParseExact(Strings.Format(vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 2), "MM/dd/yyyy"), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture), Strings.Format(vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 2), "MM/dd/yyyy") + " - Dep Removal");
							// Else
							// glngJournalNumber = AddToJournal(AccountStrut, "FA", 0, Format(mskPurchaseDate.Text, "MM/dd/yyyy"), Format(mskPurchaseDate.Text, "MM/dd/yyyy") & " - Dep Removal")
							// End If
						}
						modGlobalFunctions.AddCYAEntry_242("FA", "Removal of Depreciation Run Executed", "Asset: " + FCConvert.ToString(rsAsset.Get_Fields_Int32("ID")), "Depreciation Date Removed: " + vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 2), "Depreciation Amount Removed: " + FCConvert.ToString(Conversion.Val(rsDelete.Get_Fields_Decimal("DepreciationAmount"))));
						rsDelete.Delete();
						vsDepreciationsPosted.RemoveItem(vsDepreciationsPosted.Rows - 1);
						if (modVariables.Statics.boolBudgetaryExists && modVariables.Statics.glngJournalNumber > 0)
						{
							if (modSecurity.ValidPermissions_8(null, modStartup.AUTOPOSTDEPRECIATIONJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptFADepreciation))
							{
								if (MessageBox.Show("Removal of Depreciation run has been completed successfully.  Your entries have been saved in journal " + modValidateAccount.GetFormat_6(Strings.Trim(modVariables.Statics.glngJournalNumber.ToString()), 4) + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									clsJournalInfo = new clsPostingJournalInfo();
									clsJournalInfo.JournalNumber = modVariables.Statics.glngJournalNumber;
									clsJournalInfo.JournalType = "GJ";
									clsJournalInfo.Period = FCConvert.ToString(DateAndTime.DateValue(vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Rows - 1, 2)).Month);
									clsJournalInfo.CheckDate = "";
									clsPostInfo.AddJournal(clsJournalInfo);
									clsPostInfo.AllowPreview = true;
									clsPostInfo.PostJournals();
								}
							}
							else
							{
								MessageBox.Show("Removal of Depreciation run has been completed successfully.  Your entries have been saved in journal " + modValidateAccount.GetFormat_6(Strings.Trim(modVariables.Statics.glngJournalNumber.ToString()), 4) + ".", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						else
						{
							MessageBox.Show("Removal of Depreciation run has been completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
			}
		}

		private void mnuLocationCaptions_Click(object sender, System.EventArgs e)
		{
            int locationType = 0;
            if (cmbLocationType.SelectedIndex >= 0)
            {
                locationType = cmbLocationType.ItemData(cmbLocationType.SelectedIndex);
            }

            int locationId = 0;
            if (cmbLocation.SelectedItem != null)
            {
                locationId = ((DescriptionIDCodePair)cmbLocation.SelectedItem).ID;
            }
            frmSettings.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
            SetLocationCaptions();
            SetLocationType(locationType);
            if (cmbLocationType.SelectedIndex >= 0)
            {
                SetLocationCombo(locationId);
            }            
		}

		private void mnuLocations_Click(object sender, System.EventArgs e)
        {
            int locationId = 0;
            if (cmbLocation.SelectedItem != null)
            {
                locationId = ((DescriptionIDCodePair) cmbLocation.SelectedItem).ID;
            }
			frmLocations.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);           
            if (cmbLocationType.SelectedIndex >= 0)
            {
                LoadLocations(cmbLocationType.ItemData(cmbLocationType.SelectedIndex));
                SetLocationCombo(locationId);
            }
        }

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							modVariables.Statics.gboolCancelSelected = false;
							if (SaveRecord(modVariables.Statics.glngMasterIDNumber))
							{
								clrForm();
								SetDefaults();
								modVariables.Statics.glngMasterIDNumber = 0;
								txtDescription.Focus();
							}
							else
							{
								// do nothing
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							clrForm();
							SetDefaults();
							modVariables.Statics.glngMasterIDNumber = 0;
							txtDescription.Focus();
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							// do nothing
							break;
						}
				}
				//end switch
			}
			else
			{
				clrForm();
				SetDefaults();
				modVariables.Statics.glngMasterIDNumber = 0;
				txtDescription.Focus();
			}
		}

		public void SetDefaults()
		{
			this.cmbDepreciation.SelectedIndex = 0;
			this.cboRate.SelectedIndex = 1;
			this.txtBasisCost.Text = "0.00";
			this.txtSalvageValue.Text = "0.00";
			boolDataChanged = false;
		}

		private void mnuNewPosted_Click()
		{
			vsDepreciationsPosted.Rows += 1;
			vsDepreciationsPosted.Focus();
			vsDepreciationsPosted.Select(vsDepreciationsPosted.Rows - 1, 2);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDepData = new clsDRWrapper();
			SaveRecord(modVariables.Statics.glngMasterIDNumber);
			mnuFileComments.Enabled = true;
			if (cboClassCode.SelectedIndex != 1)
			{
				mnuFileInitialDepreciation.Enabled = SetInitialDepreciationMenuEnabledProperty(ref modVariables.Statics.glngMasterIDNumber);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveRecord(modVariables.Statics.glngMasterIDNumber))
				Close();
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(double, int)

		private object SetDeptDivComboBoxes(ref FCComboBox ControlName, string strID, string strType = "LOCATION", int intTypeID = 1)
		{
			object SetDeptDivComboBoxes = null;
			int intCounter;
			if (Conversion.Val(strID) == 0)
			{
				// do nothing
			}
			else
			{
				for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
				{
					if (ControlName.Items[intCounter].ToString() == strID)
					{
						ControlName.SelectedIndex = intCounter;
						return SetDeptDivComboBoxes;
					}
				}
			}
			return SetDeptDivComboBoxes;
		}
		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		private void SearchClassCodeComboBoxes(ref FCComboBox ControlName, int lngID)
		{
			int intCounter;
			if (lngID == 0)
			{
				ControlName.SelectedIndex = 0;
			}
			else
			{
				for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
				{
					if (ControlName.ItemData(intCounter) == lngID)
					{
						ControlName.SelectedIndex = intCounter;
						return;
					}
				}
			}
		}
		// vbPorter upgrade warning: strDeptID As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: strDivID As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		private string SearchDeptDivComboBoxes(ref FCComboBox DeptControlName, ref FCComboBox DivControlName, int strDeptID, int strDivID)
		{
			string SearchDeptDivComboBoxes = "";
			int intCounter;
			for (intCounter = 1; intCounter <= DeptControlName.Items.Count - 1; intCounter++)
			{
				if (FCConvert.ToDouble(DeptControlName.Items[intCounter].ToString()) == strDeptID)
				{
					DeptControlName.SelectedIndex = intCounter;
					SearchDeptDivComboBoxes = txtDeptDivDescription.Text;
					break;
				}
			}
			for (intCounter = 1; intCounter <= DivControlName.Items.Count - 1; intCounter++)
			{
				if (FCConvert.ToDouble(DivControlName.Items[intCounter].ToString()) == strDivID)
				{
					DivControlName.SelectedIndex = intCounter;
					SearchDeptDivComboBoxes = txtDeptDivDescription.Text;
					break;
				}
			}
			return SearchDeptDivComboBoxes;
		}

        private void LoadLocations(int intTypeID)
        {
            var rsData = new clsDRWrapper();
            cmbLocation.Items.Clear();
            rsData.OpenRecordset("Select * from tblLocations where TypeID = " + intTypeID + " order by Code",
                "FixedAssets");
            var index = 0;
            while (!rsData.EndOfFile())
            {
                cmbLocation.Items.Add(new DescriptionIDCodePair()
                {
                    Description = rsData.Get_Fields_String("Code") + "  -  " + rsData.Get_Fields_String("Description"),
                    Code = rsData.Get_Fields_String("Code"),
                    ID = rsData.Get_Fields_Int32("ID")

                });

                rsData.MoveNext();
            }
        }
		//private void LoadLocationCombos(ref FCComboBox ControlName, int intTypeID)
		//{
		//	int intCounter;
		//	clsDRWrapper rsData = new clsDRWrapper();
		//	ControlName.Clear();
		//	ControlName.AddItem(string.Empty);
		//	ControlName.ItemData(ControlName.NewIndex, 0);
		//	rsData.OpenRecordset("Select * from tblLocations where TypeID = " + FCConvert.ToString(intTypeID) + " Order by Code", modGlobal.DatabaseNamePath);
		//	if (rsData.EndOfFile())
		//	{
		//		// do nothing
		//	}
		//	else
		//	{
		//		for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
		//		{
		//			// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//			if (FCConvert.ToString(rsData.Get_Fields("Code")).Length > 10)
		//			{
		//				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//				ControlName.AddItem(rsData.Get_Fields("Code") + "  -  " + rsData.Get_Fields_String("Description"));
		//			}
		//			else
		//			{
		//				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
		//				ControlName.AddItem(rsData.Get_Fields("Code") + Strings.StrDup(10 - FCConvert.ToString(rsData.Get_Fields("Code")).Length, " ") + " -  " + rsData.Get_Fields_String("Description"));
		//			}
		//			ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")));
		//			rsData.MoveNext();
		//		}
		//	}
		//}

		private void LoadDeptCombos(ref FCComboBox ControlName)
		{
			int intCounter;
			int intIndexCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			string strOld = "";
			ControlName.Clear();
			ControlName.AddItem(string.Empty);
			ControlName.ItemData(ControlName.NewIndex, 0);
			intIndexCounter = 1;
			if (modVariables.Statics.boolDeptDivFromBud)
			{
				rsData.OpenRecordset("Select * from DeptDivTitles Order by Department,Division", "TWBD0000.vb1");
				for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
				{
					if (strOld != Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Department"))))
					{
						strOld = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Department")));
						ControlName.AddItem(rsData.Get_Fields_String("Department"));
						DeptDescriptions[intIndexCounter] = FCConvert.ToString(rsData.Get_Fields_String("ShortDescription"));
						intIndexCounter += 1;
					}
					rsData.MoveNext();
				}
			}
			else
			{
				rsData.OpenRecordset("Select distinct tblDeptDiv.* from tblDeptDiv Order by Dept,Div", modGlobal.DatabaseNamePath);
				for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
				{
					if (strOld != Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Dept"))))
					{
						strOld = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Dept")));
						ControlName.AddItem(rsData.Get_Fields_String("Dept"));
						DeptDescriptions[intIndexCounter] = FCConvert.ToString(rsData.Get_Fields_String("Description"));
						intIndexCounter += 1;
					}
					rsData.MoveNext();
				}
			}
		}

		private void LoadClassCodeCombos(ref FCComboBox ControlName)
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			string strOld = "";
			ControlName.Clear();
			ControlName.AddItem(string.Empty);
			ControlName.ItemData(ControlName.NewIndex, 0);
			ControlName.AddItem("0     - INVENTORY");
			ControlName.ItemData(ControlName.NewIndex, 0);
			rsData.OpenRecordset("Select tblClassCodes.* from tblClassCodes Order by Code", modGlobal.DatabaseNamePath);
			for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
			{
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsData.Get_Fields("Code")).Length > 4)
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					ControlName.AddItem(Strings.Left(FCConvert.ToString(rsData.Get_Fields("Code")), 9) + " -  " + rsData.Get_Fields_String("Description"));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					ControlName.AddItem(rsData.Get_Fields("Code") + Strings.StrDup(4 - FCConvert.ToString(rsData.Get_Fields("Code")).Length, " ") + " -  " + rsData.Get_Fields_String("Description"));
				}
				ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")));
				rsData.MoveNext();
			}
		}

		private void LoadDivCombos(ref FCComboBox ControlName)
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			ControlName.Clear();
			ControlName.AddItem(string.Empty);
			ControlName.ItemData(ControlName.NewIndex, 0);
			if (modVariables.Statics.boolDeptDivFromBud)
			{
				rsData.OpenRecordset("Select * from DeptDivTitles where Department = '" + cboDept.Text + "' and convert(int, Division) <> 0 Order by Division", "TWBD0000.vb1");
				if (rsData.EndOfFile())
				{
					// do nothing
				}
				else
				{
					for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
					{
						ControlName.AddItem(rsData.Get_Fields_String("Division"));
						DivDescriptions[intCounter] = FCConvert.ToString(rsData.Get_Fields_String("ShortDescription"));
						rsData.MoveNext();
					}
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblDeptDiv where Dept = '" + cboDept.Text + "' and convert(int, Div) <> 0 Order by Div", modGlobal.DatabaseNamePath);
				if (rsData.EndOfFile())
				{
					// do nothing
				}
				else
				{
					for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
					{
						ControlName.AddItem(rsData.Get_Fields_String("Div"));
						DivDescriptions[intCounter + 1] = FCConvert.ToString(rsData.Get_Fields_String("Description"));
						rsData.MoveNext();
					}
				}
			}
			cboDiv.SelectedIndex = 0;
			txtDeptDivDescription.Text = string.Empty;
		}

		private void LoadVendorCombos(ref FCComboBox ControlName)
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			ControlName.Clear();
			ControlName.AddItem(string.Empty);
			ControlName.ItemData(ControlName.NewIndex, 0);
			ControlName.AddItem("Temporary Vendor");
			ControlName.ItemData(ControlName.NewIndex, 0);
			if (modVariables.Statics.boolVendorsFromBud)
			{
				// need to change this database to be twdb0000.vb1
				rsData.OpenRecordset("Select * from VendorMaster WHERE Status <> 'D' Order by CheckName", "TWBD0000.vb1");
				for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
				{
					ControlName.AddItem(rsData.Get_Fields_String("CheckName"));
					ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsData.Get_Fields_Int32("VendorNumber")));
					rsData.MoveNext();
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblVendors Order by Description", modGlobal.DatabaseNamePath);
				for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
				{
					ControlName.AddItem(rsData.Get_Fields_String("Description"));
					ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsData.Get_Fields_Int32("ID")));
					rsData.MoveNext();
				}
			}
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			Close();
			frmSearch.InstancePtr.Show(App.MainForm);
		}

		private void mnuVendors_Click(object sender, System.EventArgs e)
		{
			int intVendorID = 0;
			if (cboVendor.SelectedIndex < 0)
			{
				intVendorID = 0;
			}
			else
			{
				intVendorID = cboVendor.ItemData(cboVendor.SelectedIndex);
			}
			frmVendors.InstancePtr.Show(FormShowEnum.Modal, App.MainForm);
			LoadVendorCombos(ref cboVendor);
			SearchComboBoxes(ref cboVendor, intVendorID, "VENDOR");
		}

		private void mskDateLastDepreciation_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.mskDateLastDepreciation.SelectAll();
		}

		private void mskDateLastDepreciation_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (mskDateLastDepreciation.Text.Replace(" ", "") == "//")
				return;
			if (Information.IsDate(mskDateLastDepreciation.Text.Replace("_", "")))
			{
				this.mskDateLastDepreciation.Text = FCConvert.ToString(modDateRoutines.LongDate(mskDateLastDepreciation.Text.Replace("_", "")));
			}
			else
			{
				MessageBox.Show("Invalid Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.Cancel = true;
			}
		}

		private void mskPurchaseDate_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void mskPurchaseDate_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.mskPurchaseDate.SelectAll();
		}

		private void mskPurchaseDate_Leave(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#i376 - compare correct text
			//if (Strings.InStr(1, mskDateLastDepreciation.Text, "_", CompareConstants.vbBinaryCompare) != 0)
			if (mskDateLastDepreciation.Text.Replace(" ", "") == "//")
			{
				this.mskDateLastDepreciation.Text = this.mskPurchaseDate.Text;
			}
			else if (!Information.IsDate(DateAndTime.DateValue(mskDateLastDepreciation.Text)))
			{
				this.mskDateLastDepreciation.Text = this.mskPurchaseDate.Text;
			}
		}

		private void mskPurchaseDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (mskPurchaseDate.Text.Replace(" ", "") == "//")
				return;
			if (Information.IsDate(mskPurchaseDate.Text.Replace("_", "")))
			{
				this.mskPurchaseDate.Text = FCConvert.ToString(modDateRoutines.LongDate(mskPurchaseDate.Text.Replace("_", "")));
			}
			else
			{
				MessageBox.Show("Invalid Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.Cancel = true;
			}
			if (Information.IsDate(mskPurchaseDate.Text))
			{
				if (DateAndTime.DateDiff("d", DateTime.Today, FCConvert.ToDateTime(mskPurchaseDate.Text)) > 0)
				{
					MessageBox.Show("Purchase date cannot be greater than Today's Date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					mskPurchaseDate.Mask = string.Empty;
					mskPurchaseDate.Text = string.Empty;
					mskPurchaseDate.Mask = "##/##/####";
					//mskPurchaseDate.SelStart = 0;
					//mskPurchaseDate.SelLength = Marshal.SizeOf(mskPurchaseDate.Text);
					mskPurchaseDate.SelectAll();
					e.Cancel = true;
					return;
				}
			}
			if (Information.IsDate(mskWarrantyDate.Text) && Information.IsDate(mskPurchaseDate.Text))
			{
				if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(mskWarrantyDate.Text), FCConvert.ToDateTime(mskPurchaseDate.Text)) >= 0)
				{
					MessageBox.Show("Expiration Date must be greater than Purchase Date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					mskPurchaseDate.Mask = string.Empty;
					mskPurchaseDate.Text = string.Empty;
					mskPurchaseDate.Mask = "##/##/####";
					//mskPurchaseDate.SelStart = 0;
					//mskPurchaseDate.SelLength = Marshal.SizeOf(mskPurchaseDate.Text);
					mskPurchaseDate.SelectAll();
					e.Cancel = true;
				}
			}
		}

		private void mskWarrantyDate_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void mskWarrantyDate_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.mskWarrantyDate.SelectAll();
		}

		private void mskWarrantyDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (mskWarrantyDate.Text.Replace(" ", "") == "//")
				return;
			if (Information.IsDate(mskWarrantyDate.Text.Replace("_", "")))
			{
				this.mskWarrantyDate.Text = FCConvert.ToString(modDateRoutines.LongDate(mskWarrantyDate.Text.Replace("_", "")));
			}
			else
			{
				MessageBox.Show("Invalid Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.Cancel = true;
				return;
			}
			if (Information.IsDate(mskWarrantyDate.Text) && Information.IsDate(mskPurchaseDate.Text))
			{
				if (DateAndTime.DateDiff("d", FCConvert.ToDateTime(mskWarrantyDate.Text), FCConvert.ToDateTime(mskPurchaseDate.Text)) >= 0)
				{
					MessageBox.Show("Expiration Date must be greater than Purchase Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					mskWarrantyDate.Mask = string.Empty;
					mskWarrantyDate.Text = string.Empty;
					mskWarrantyDate.Mask = "##/##/####";
					//mskWarrantyDate.SelStart = 0;
					//mskWarrantyDate.SelLength = Marshal.SizeOf(mskWarrantyDate.Text);
					mskWarrantyDate.SelectAll();
					e.Cancel = true;
				}
			}
		}

		private void optDepreciation_CheckedChanged_2(int Index)
		{
			optDepreciation_CheckedChanged(Index, cmbDepreciation, EventArgs.Empty);
		}

		private void optDepreciation_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			lblTotalUsed.Visible = Index == 2;
			txtTotalUsed.Visible = Index == 2;
			lblUnits[0].Visible = true;
			lblUnits[1].Visible = false;
			cboRate.Visible = Index == 1;
			lblRate.Visible = Index == 1;
			if (Index == 2 || Index == 3)
			{
				txtUN.Visible = true;
				lblUM.Visible = true;
				lblUnits[1].Visible = true;
			}
			else
			{
				txtUN.Visible = false;
				lblUM.Visible = false;
				lblUnits[1].Visible = false;
			}
			if (Index == 3)
			{
				lblCondition.Visible = true;
				cboCondition.Visible = true;
				lblLife.Text = "Units";
				cboCondition.SelectedIndex = 0;
			}
			else
			{
				lblCondition.Visible = false;
				cboCondition.Visible = false;
				lblLife.Text = "Life";
			}
			if (Index == 0 || Index == 1)
			{
				txtUN.Text = string.Empty;
				txtTotalUsed.Text = "";
				lblUnits[0].Text = "Years";
				lblUnits[1].Text = "Years";
			}
			else
			{
				lblUnits[0].Text = txtUN.Text;
				lblUnits[1].Text = txtUN.Text;
			}
			boolDataChanged = true;
		}

		public void optDepreciation_Click(int Index)
		{
			optDepreciation_CheckedChanged(Index, cmbDepreciation, new System.EventArgs());
		}

		private void optDepreciation_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbDepreciation.SelectedIndex;
			optDepreciation_CheckedChanged(index, sender, e);
		}

		private void txtAcct1_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtAcct1_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (modVariables.Statics.boolBudgetaryExists)
			{
				//FC:FINAL:DDU:#2118 - don't show tooltip
				//ToolTip1.SetToolTip(txtAcct1, modAccountTitle.ReturnAccountDescription(txtAcct1.TextMatrix(0, 0)));
				//this.Refresh();
			}
		}

		private void txtAcct2_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtAcct2_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (modVariables.Statics.boolBudgetaryExists)
			{
				//FC:FINAL:DDU:#2118 - don't show tooltip
				//ToolTip1.SetToolTip(txtAcct2, modAccountTitle.ReturnAccountDescription(txtAcct2.TextMatrix(0, 0)));
			}
		}

		private void txtAcct3_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtAcct3_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (modVariables.Statics.boolBudgetaryExists)
			{
				//FC:FINAL:DDU:#2118 - don't show tooltip
				//ToolTip1.SetToolTip(txtAcct3, modAccountTitle.ReturnAccountDescription(txtAcct3.TextMatrix(0, 0)));
			}
		}

		private void txtAcct4_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtAcct4_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (modVariables.Statics.boolBudgetaryExists)
			{
				//FC:FINAL:DDU:#2118 - don't show tooltip
				//ToolTip1.SetToolTip(txtAcct4, modAccountTitle.ReturnAccountDescription(txtAcct4.TextMatrix(0, 0)));
			}
		}

		private void txtBasisCost_Change(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtBasisCost_Enter(object sender, System.EventArgs e)
		{
			txtBasisCost.SelectionStart = 0;
			txtBasisCost.SelectionLength = txtBasisCost.Text.Length;
		}

		private void txtBasisCost_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// vbPorter upgrade warning: dblCurrentValue As double	OnWriteFCConvert.ToDecimal(
			double dblCurrentValue = 0;
			if (Strings.Trim(txtSalvageValue.Text) == "")
			{
				txtSalvageValue.Text = "0.00";
			}
			if (Strings.Trim(txtBasisCost.Text) == "")
			{
				txtBasisCost.Text = "0.00";
			}
			if (FCConvert.ToDecimal(txtSalvageValue.Text) > FCConvert.ToDecimal(txtBasisCost.Text))
			{
				MessageBox.Show("Salvage Value cannot be greater then the Basis Cost.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				txtBasisCost.Focus();
				txtBasisCost.Text = "0.00";
				txtBasisCost.SelectionStart = 0;
				txtBasisCost.SelectionLength = 1;
			}
			else
			{
				dblCurrentValue = FCConvert.ToDouble(CalcCurrentValue());
				if (dblCurrentValue < 0)
				{
					MessageBox.Show("Current Value cannot be less then 0.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					txtBasisCost.Focus();
					txtBasisCost.Text = "0.00";
					txtBasisCost.SelectionStart = 0;
					txtBasisCost.SelectionLength = 1;
				}
				else
				{
					txtValue.Text = Strings.Format(dblCurrentValue, "#,##0.00");
				}
			}
		}

		private void txtBasisReduction_Change()
		{
			boolDataChanged = true;
		}

		private void txtBasisReduction_GotFocus()
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
		}

		private void txtBusinessUse_Change()
		{
			boolDataChanged = true;
		}

		private void txtBusinessUse_GotFocus()
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
		}

		private void txtDB_Change()
		{
			boolDataChanged = true;
		}

		private void txtDB_GotFocus()
		{
			if (cmbDepreciation.SelectedIndex == 1)
			{
				//this.ActiveControlSelStart = 0;
				//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			}
			else
			{
				Support.SendKeys("{TAB}", false);
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool NumberCheckOK(ref FCTextBox ControlName, int intLength, int intKeyCode)
		{
			bool NumberCheckOK = false;
			if (ControlName.Text == string.Empty)
				return NumberCheckOK;
			// BACKSPACE KEY
			if (intKeyCode == 8)
				return NumberCheckOK;
			// DELETE KEY
			if (intKeyCode == 46)
				return NumberCheckOK;
			// ARROW KEYS
			if (intKeyCode > 36 && intKeyCode < 41)
				return NumberCheckOK;
			// LENGHT OF THE BOX XX.XX FOR DBRATE OR XXX.XX FOR LIFE
			if (ControlName.Text.Length > intLength)
			{
				ControlName.Text = Strings.Left(ControlName.Text, ControlName.Text.Length - 1);
				ControlName.SelectionStart = ControlName.Text.Length;
				return NumberCheckOK;
			}
			// ALLOW THE DECIMAL POINT
			if (intKeyCode == 110 || intKeyCode == 190)
			{
				strSplit = Strings.Split(ControlName.Text, ".", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound((object[])strSplit, 1) > 1)
				{
					ControlName.Text = Strings.Left(ControlName.Text, ControlName.Text.Length - 1);
					ControlName.SelectionStart = ControlName.Text.Length;
				}
				return NumberCheckOK;
			}
			// ALLOW NUMBERS FROM EITHER THE NUMBER PAD OR THE TOP ROW
			if (intKeyCode < 48 || intKeyCode > 57)
			{
				if (intKeyCode > 95 && intKeyCode < 106)
				{
				}
				else if (intKeyCode == 13)
				{
				}
				else
				{
					ControlName.Text = Strings.Left(ControlName.Text, ControlName.Text.Length - 1);
					ControlName.SelectionStart = ControlName.Text.Length;
					return NumberCheckOK;
				}
			}
			NumberCheckOK = true;
			return NumberCheckOK;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool NumberCheckOK2(ref FCTextBox ControlName, int intLength, int intKeyCode)
		{
			bool NumberCheckOK2 = false;
			if (ControlName.Text == string.Empty)
				return NumberCheckOK2;
			// BACKSPACE KEY
			if (intKeyCode == 8)
				return NumberCheckOK2;
			// DELETE KEY
			if (intKeyCode == 46)
				return NumberCheckOK2;
			// ARROW KEYS
			if (intKeyCode > 36 && intKeyCode < 41)
				return NumberCheckOK2;
			// ALLOW NUMBERS FROM EITHER THE NUMBER PAD OR THE TOP ROW
			if (intKeyCode < 48 || intKeyCode > 57)
			{
				if (intKeyCode > 95 && intKeyCode < 106)
				{
				}
				else if (intKeyCode == 13)
				{
				}
				else
				{
					ControlName.Text = Strings.Left(ControlName.Text, ControlName.Text.Length - 1);
					ControlName.SelectionStart = ControlName.Text.Length;
					return NumberCheckOK2;
				}
			}
			NumberCheckOK2 = true;
			return NumberCheckOK2;
		}

		private void txtDeptDivDescription_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtDescription1_Change()
		{
			boolDataChanged = true;
		}

		private void txtDescription2_Change()
		{
			boolDataChanged = true;
		}

		private void txtDescription3_Change()
		{
			boolDataChanged = true;
		}

		private void txtDescription4_Change()
		{
			boolDataChanged = true;
		}

		private void txtLocation1_Change()
		{
			boolDataChanged = true;
		}

		private void txtLocation2_Change()
		{
			boolDataChanged = true;
		}

		private void txtDescription_Enter(object sender, System.EventArgs e)
		{
			// Dave 2/25/03
			txtDescription.SelectionStart = 0;
			txtDescription.SelectionLength = this.ActiveControl.Text.Length;
		}

		private void txtLife_Change(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtLife_Leave(object sender, System.EventArgs e)
		{
			if (cmbDepreciation.SelectedIndex == 0 || cmbDepreciation.SelectedIndex == 1)
			{
				strSplit = Strings.Split(txtLife.Text, ".", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound((object[])strSplit, 1) == 1)
				{
					// THERE IS A DECIMAL SO CHECK THAT THE NUMBER IS NOT OVER 1000
					if (Conversion.Val(txtLife.Text) > 999.99)
					{
						MessageBox.Show("Invalid Life Number. Must be smaller then 999.99", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtLife.Focus();
					}
					else
					{
						txtLife.Text = Strings.Format(txtLife.Text, "#,##0");
					}
				}
				else
				{
					if (Conversion.Val(txtLife.Text) > 999.99)
					{
						MessageBox.Show("Invalid Life Number. Must be smaller then 999.99", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						txtLife.Text = string.Empty;
						txtLife.Focus();
					}
					else
					{
						txtLife.Text = Strings.Format(txtLife.Text, "#,##0");
					}
				}
			}
		}

		private void txtMake_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtMake_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtMake.SelectAll();
		}

		private void txtModel_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtModel_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtModel.SelectAll();
		}

		private void txtReplacementCost_Change(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtReplacementCost_Enter(object sender, System.EventArgs e)
		{
			txtReplacementCost.SelectionStart = 0;
			txtReplacementCost.SelectionLength = txtReplacementCost.Text.Length;
		}

		private void txtReplacementCost_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Strings.Trim(txtSalvageValue.Text) == "")
			{
				txtSalvageValue.Text = "0.00";
			}
			if (Strings.Trim(txtBasisCost.Text) == "")
			{
				txtBasisCost.Text = "0.00";
			}
			if (Strings.Trim(txtReplacementCost.Text) == "")
			{
				txtReplacementCost.Text = "0.00";
			}
		}

		private void txtSalvageValue_Change(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtSalvageValue_Enter(object sender, System.EventArgs e)
		{
			txtSalvageValue.SelectionStart = 0;
			txtSalvageValue.SelectionLength = txtSalvageValue.Text.Length;
		}

		private void txtSalvageValue_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Strings.Trim(txtSalvageValue.Text) == "")
			{
				txtSalvageValue.Text = "0.00";
			}
			if (Strings.Trim(txtBasisCost.Text) == "")
			{
				txtBasisCost.Text = "0.00";
			}
			if (Strings.Trim(txtReplacementCost.Text) == "")
			{
				txtReplacementCost.Text = "0.00";
			}
			if (FCConvert.ToDecimal(txtSalvageValue.Text) > FCConvert.ToDecimal(txtBasisCost.Text))
			{
				MessageBox.Show("Salvage Value cannot be greater than the Basis Cost.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				txtSalvageValue.Focus();
				txtSalvageValue.Text = "0.00";
				txtSalvageValue.SelectionStart = 0;
				txtSalvageValue.SelectionLength = 1;
			}
		}

		private void txtSerialNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtSerialNumber_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtSerialNumber.SelectAll();
		}

		private void txtTagNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtTagNumber_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtTagNumber.SelectAll();
		}

		private void txtTotalDepreciated_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtTotalPosted_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtTotalPosted_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtTotalPosted.SelectAll();
		}

		private void txtTotalUsed_Change(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtTotalUsed_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = this.ActiveControlText.Length;
			this.txtTotalUsed.SelectAll();
		}

		private void txtUN_TextChanged(object sender, System.EventArgs e)
		{
			lblUnits[0].Text = txtUN.Text;
			lblUnits[1].Text = txtUN.Text;
			boolDataChanged = true;
		}

		private void txtUN_Enter(object sender, System.EventArgs e)
		{
			if (cmbDepreciation.SelectedIndex == 2 || cmbDepreciation.SelectedIndex == 3)
			{
				//this.ActiveControlSelStart = 0;
				//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
				this.txtUN.SelectAll();
			}
			else
			{
				Support.SendKeys("{TAB}", false);
			}
		}

		private void txtWarrantyDescription_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtWarrantyDescription_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtWarrantyDescription.SelectAll();
		}

		private void txtYear_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtYear_Enter(object sender, System.EventArgs e)
		{
			//this.ActiveControlSelStart = 0;
			//this.ActiveControlSelLength = Marshal.SizeOf(this.ActiveControl);
			this.txtYear.SelectAll();
		}

		private void txtYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsDepreciationsPosted_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsDepreciationsPosted.TextMatrix(vsDepreciationsPosted.Row, 1, FCConvert.ToString(1));
			boolDataChanged = true;
		}

		private void vsDepreciationsPosted_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsDepreciationsPosted.Col == 2)
				vsDepreciationsPosted.EditMask = "##/##/####";
			if (vsDepreciationsPosted.Col != 2)
				vsDepreciationsPosted.EditMask = string.Empty;
		}

		private void vsDepreciationsPosted_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsDepreciationsPosted.Rows < 2)
				return;
			if (vsDepreciationsPosted.MouseRow == 0)
			{
				// sort this grid
				vsDepreciationsPosted.Select(1, vsDepreciationsPosted.Col);
				if (strSort == "flexSortGenericAscending")
				{
					vsDepreciationsPosted.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					strSort = "flexSortGenericDescending";
				}
				else
				{
					vsDepreciationsPosted.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					strSort = "flexSortGenericAscending";
				}
			}
		}

		public object SaveGrid(int lngID)
		{
			object SaveGrid = null;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				vsDepreciationsPosted.Select(0, 0);
				for (intCounter = 1; intCounter <= vsDepreciationsPosted.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(vsDepreciationsPosted.TextMatrix(intCounter, 1)) == 1)
					{
						// then this record has changed
						if (Conversion.Val(vsDepreciationsPosted.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							rsData.Execute("Insert into tblDepreciation (DepreciationDate,DepreciationDesc,ItemID) VALUES ('" + modGlobalRoutines.FixQuote(vsDepreciationsPosted.TextMatrix(intCounter, 2)) + "','" + modGlobalRoutines.FixQuote(vsDepreciationsPosted.TextMatrix(intCounter, 3)) + "'," + FCConvert.ToString(lngID) + ")", modGlobal.DatabaseNamePath);
						}
						else
						{
							// this is an edit record
							rsData.OpenRecordset("Select * from tblDepreciation where ID = " + vsDepreciationsPosted.TextMatrix(intCounter, 0), modGlobal.DatabaseNamePath);
							if (rsData.EndOfFile())
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
							rsData.Set_Fields("ItemID", lngID);
							rsData.Set_Fields("DepreciationDate", Strings.Trim(modGlobalRoutines.FixQuote(vsDepreciationsPosted.TextMatrix(intCounter, 2))));
							rsData.Set_Fields("DepreciationDesc", Strings.Trim(modGlobalRoutines.FixQuote(vsDepreciationsPosted.TextMatrix(intCounter, 3))));
							rsData.Update();
						}
					}
				}
				return SaveGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveGrid;
		}

		private Decimal CalcCurrentValue()
		{
			Decimal CalcCurrentValue = 0;
			if (Strings.Trim(txtBasisCost.Text) == "")
			{
				txtBasisCost.Text = FCConvert.ToString(0);
			}
			if (Strings.Trim(txtTotalDepreciated.Text) == "")
			{
				txtTotalDepreciated.Text = FCConvert.ToString(0);
			}
			CalcCurrentValue = FCConvert.ToDecimal(txtBasisCost.Text) - FCConvert.ToDecimal(txtTotalDepreciated.Text);
			return CalcCurrentValue;
		}

		private bool InitialDepreciationAlreadyRun()
		{
			bool InitialDepreciationAlreadyRun = false;
			clsDRWrapper rsDepData = new clsDRWrapper();
			rsDepData.OpenRecordset("SELECT * FROM tblDepreciation INNER JOIN tblDepreciationMaster ON tblDepreciation.ReportId = tblDepreciationMaster.ID WHERE ItemID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationType = 'I'", modGlobal.DatabaseNamePath);
			if (rsDepData.EndOfFile() != true && rsDepData.BeginningOfFile() != true)
			{
				InitialDepreciationAlreadyRun = true;
			}
			else
			{
				InitialDepreciationAlreadyRun = false;
			}
			return InitialDepreciationAlreadyRun;
		}

		private bool RegularDepreciationAlreadyRun()
		{
			bool RegularDepreciationAlreadyRun = false;
			clsDRWrapper rsDepData = new clsDRWrapper();
			rsDepData.OpenRecordset("SELECT * FROM tblDepreciation INNER JOIN tblDepreciationMaster ON tblDepreciation.ReportId = tblDepreciationMaster.ID WHERE ItemID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationType <> 'I'", modGlobal.DatabaseNamePath);
			if (rsDepData.EndOfFile() != true && rsDepData.BeginningOfFile() != true)
			{
				RegularDepreciationAlreadyRun = true;
			}
			else
			{
				RegularDepreciationAlreadyRun = false;
			}
			return RegularDepreciationAlreadyRun;
		}
		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToInt16(
		private Decimal GetInitialDepreciationAmount()
		{
			Decimal GetInitialDepreciationAmount = 0;
			clsDRWrapper rsDepData = new clsDRWrapper();
			rsDepData.OpenRecordset("SELECT * FROM tblDepreciation INNER JOIN tblDepreciationMaster ON tblDepreciation.ReportId = tblDepreciationMaster.ID WHERE ItemID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationType = 'I'", modGlobal.DatabaseNamePath);
			if (rsDepData.EndOfFile() != true && rsDepData.BeginningOfFile() != true)
			{
				GetInitialDepreciationAmount = FCConvert.ToDecimal(rsDepData.Get_Fields_Decimal("DepreciationAmount"));
			}
			else
			{
				GetInitialDepreciationAmount = 0;
			}
			return GetInitialDepreciationAmount;
		}

		private void SetInitialDepreciationAmount(ref Decimal curNewAmount)
		{
			clsDRWrapper rsDepData = new clsDRWrapper();
			int lngRecordNumber = 0;
			rsDepData.OpenRecordset("SELECT *, tblDepreciation.ID as IDToCheck FROM tblDepreciation INNER JOIN tblDepreciationMaster ON tblDepreciation.ReportId = tblDepreciationMaster.ID WHERE ItemID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber) + " AND DepreciationType = 'I'", modGlobal.DatabaseNamePath);
			if (rsDepData.EndOfFile() != true && rsDepData.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Field [IDToCheck] not found!! (maybe it is an alias?)
				lngRecordNumber = FCConvert.ToInt32(rsDepData.Get_Fields("IDToCheck"));
				rsDepData.OpenRecordset("SELECT * FROM tblDepreciation WHERE ID = " + FCConvert.ToString(lngRecordNumber), modGlobal.DatabaseNamePath);
				rsDepData.Edit();
				rsDepData.Set_Fields("DepreciationAmount", curNewAmount);
				rsDepData.Update();
			}
		}

		private bool SetInitialDepreciationMenuEnabledProperty(ref int lngID)
		{
			bool SetInitialDepreciationMenuEnabledProperty = false;
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (lngID == 0)
			{
				SetInitialDepreciationMenuEnabledProperty = false;
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM tblMaster WHERE ID = " + FCConvert.ToString(lngID), "TWFA0000.vb1");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					if (RegularDepreciationAlreadyRun())
					{
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int16("DepreciationMethod")) == 1)
						{
							SetInitialDepreciationMenuEnabledProperty = false;
						}
						else
						{
							if (FCConvert.ToInt32(rsInfo.Get_Fields_Int16("DepreciationMethod")) == 2)
							{
								SetInitialDepreciationMenuEnabledProperty = false;
							}
							else
							{
								SetInitialDepreciationMenuEnabledProperty = true;
							}
						}
					}
					else
					{
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int16("DepreciationMethod")) == 2)
						{
							SetInitialDepreciationMenuEnabledProperty = false;
						}
						else
						{
							SetInitialDepreciationMenuEnabledProperty = true;
						}
					}
				}
				else
				{
					SetInitialDepreciationMenuEnabledProperty = false;
				}
			}
			return SetInitialDepreciationMenuEnabledProperty;
		}

		private void SetConditionCombo_2(string strSearch)
		{
			SetConditionCombo(ref strSearch);
		}

		private void SetConditionCombo(ref string strSearch)
		{
			int counter;
			for (counter = 0; counter <= cboCondition.Items.Count - 1; counter++)
			{
				if (Strings.UCase(cboCondition.Items[counter].ToString()) == Strings.UCase(Strings.Trim(strSearch)))
				{
					cboCondition.SelectedIndex = counter;
					break;
				}
			}
		}

		private void LoadConditionCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboCondition.Clear();
			rsInfo.OpenRecordset("SELECT * FROM Condition ORDER BY Condition", "TWFA0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					cboCondition.AddItem(rsInfo.Get_Fields_String("Condition"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			mnuDelete_Click(sender, e);
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuNew_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(sender, e);
		}

		private void cmdRemoveLast_Click(object sender, EventArgs e)
		{
			mnuFileRemoveDepreciation_Click(sender, e);
		}

		private void cmdPostNew_Click(object sender, EventArgs e)
		{
			mnuDepreciate_Click(sender, e);
		}

        private void LoadRecord(int lngID = 0)
        {
            int intCounter;
            clsDRWrapper rsData = new clsDRWrapper();
            clsDRWrapper rsDepData = new clsDRWrapper();

            LoadVendorCombos(ref cboVendor);
            LoadDeptCombos(ref cboDept);
            LoadClassCodeCombos(ref cboClassCode);
            if (lngID == 0)
            {
                // this is a new record
                clrForm();
                mnuFileInitialDepreciation.Enabled = false;
                mnuFileComments.Enabled = false;
            }
            else
            {
                rsData.OpenRecordset("Select * from tblMaster where ID = " + FCConvert.ToString(lngID), modGlobal.DatabaseNamePath);
                if (rsData.EndOfFile())
                {
                    clrForm();
                }
                else
                {
                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Comments"))) != "")
                    {
                        Image1.Visible = true;
                    }
                    else
                    {
                        Image1.Visible = false;
                    }
                    txtDescription.Text = FCConvert.ToString(rsData.Get_Fields_String("Description"));
                    // TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
                    txtYear.Text = FCConvert.ToString(rsData.Get_Fields("Year"));
                    txtMake.Text = FCConvert.ToString(rsData.Get_Fields_String("Make"));
                    txtModel.Text = FCConvert.ToString(rsData.Get_Fields_String("Model"));
                    txtSerialNumber.Text = FCConvert.ToString(rsData.Get_Fields_String("SerialNumber"));
                    txtTagNumber.Text = FCConvert.ToString(rsData.Get_Fields_String("TagNumber"));
                    if (rsData.Get_Fields_Boolean("Discarded") == true)
                    {
                        chkDiscarded.CheckState = Wisej.Web.CheckState.Checked;
                        if (Information.IsDate(rsData.Get_Fields("DiscardedDate")))
                        {
                            txtDiscardedDate.Text = Strings.Format(rsData.Get_Fields_DateTime("DiscardedDate"), "MM/dd/yyyy");
                        }
                    }
                    else
                    {
                        chkDiscarded.CheckState = Wisej.Web.CheckState.Unchecked;
                    }

                    txtDiscardedDate.Enabled = chkDiscarded.Checked;
                    txtWarrantyDescription.Text = FCConvert.ToString(rsData.Get_Fields_String("WarrantyDescription"));
                    if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsData.Get_Fields_DateTime("PurchaseDate")) && FCConvert.ToString(rsData.Get_Fields_DateTime("PurchaseDate")) != string.Empty)
                    {
                        mskPurchaseDate.Text = FCConvert.ToString(modDateRoutines.LongDate(rsData.Get_Fields_DateTime("PurchaseDate")));
                    }
                    if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsData.Get_Fields_DateTime("WarrantyExpDate")) && FCConvert.ToString(rsData.Get_Fields_DateTime("WarrantyExpDate")) != string.Empty)
                    {
                        mskWarrantyDate.Text = FCConvert.ToString(modDateRoutines.LongDate(rsData.Get_Fields("WarrantyExpDate")));
                    }

                    int locationType = 0;
                    int locationId = 0;
                    if (rsData.Get_Fields_Int32("Location1Code") > 0)
                    {
                        locationType = 1;
                        locationId = rsData.Get_Fields_Int32("Location1Code");
                    }
                    if (rsData.Get_Fields_Int32("Location2Code") > 0)
                    {
                        locationType = 2;
                        locationId = rsData.Get_Fields_Int32("Location2Code");
                    }
                    if (rsData.Get_Fields_Int32("Location3Code") > 0)
                    {
                        locationType = 3;
                        locationId = rsData.Get_Fields_Int32("Location3Code");
                    }
                    if (rsData.Get_Fields_Int32("Location4Code") > 0)
                    {
                        locationType = 4;
                        locationId = rsData.Get_Fields_Int32("Location4Code");
                    }
                    SetLocationType(locationType);
                    SetLocationCombo(locationId);
                    
                    if (Conversion.Val(rsData.Get_Fields_String("VendorCode")) == 0)
                    {
                        if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ManualVendor"))) == string.Empty)
                        {
                            cboVendor.SelectedIndex = 0;
                        }
                        else
                        {
                            cboVendor.Items[1] = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ManualVendor")));
                            cboVendor.SelectedIndex = 1;
                        }
                    }
                    else
                    {
                        SearchComboBoxes(ref cboVendor, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_String("VendorCode"))), "VENDOR");
                    }
                    if (Conversion.Val(rsData.Get_Fields_String("Dept")) != 0)
                    {
                        SetDeptDivComboBoxes(ref cboDept, FCConvert.ToString(Conversion.Val(rsData.Get_Fields_String("Dept"))));
                    }
                    if (Conversion.Val(rsData.Get_Fields_String("Div")) != 0)
                    {
                        SetDeptDivComboBoxes(ref cboDiv, FCConvert.ToString(Conversion.Val(rsData.Get_Fields_String("Div"))));
                    }
                    txtDeptDivDescription.Text = SearchDeptDivComboBoxes(ref cboDept, ref cboDiv, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_String("Dept"))), FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_String("Div"))));
                    if (Conversion.Val(rsData.Get_Fields_String("ClassCode")) == 0)
                    {
                        if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ManualClassCode"))) == string.Empty)
                        {
                            cboClassCode.SelectedIndex = 0;
                            fraDepreciationMethod.Visible = true;
                            mnuFileInitialDepreciation.Enabled = SetInitialDepreciationMenuEnabledProperty(ref lngID);
                        }
                        else
                        {
                            cboClassCode.Items[1] = FCConvert.ToString((rsData.Get_Fields_String("ManualClassCode")));
                            cboClassCode.SelectedIndex = 1;
                            fraDepreciationMethod.Visible = false;
                            mnuFileInitialDepreciation.Enabled = false;
                        }
                    }
                    else
                    {
                        SearchClassCodeComboBoxes(ref cboClassCode, FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_String("ClassCode"))));
                        fraDepreciationMethod.Visible = true;
                        mnuFileInitialDepreciation.Enabled = SetInitialDepreciationMenuEnabledProperty(ref lngID);
                    }
                    chkFederalFunds.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("FederalFunds")) ? CheckState.Checked : CheckState.Unchecked);
                    chkExpensedFirstYear.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("ExpensedFirstYear")) ? CheckState.Checked : CheckState.Unchecked);
                    txtTotalDepreciated.Text = Strings.Format(rsData.Get_Fields_Decimal("TotalDepreciated"), "#,##0.00");
                    txtBasisCost.Text = Strings.Format(rsData.Get_Fields_Decimal("BasisCost"), "#,##0.00");
                    txtReplacementCost.Text = Strings.Format(FCConvert.ToString(Conversion.Val(rsData.Get_Fields_Double("ReplacementCost"))), "#,##0.00");
                    txtSalvageValue.Text = Strings.Format(rsData.Get_Fields_Decimal("SalvageValue"), "#,##0.00");
                    txtImprovements.Text = Strings.Format(rsData.Get_Fields_Double("Improvements"), "#,##0.00");
                    txtAcct1.TextMatrix(0, 0, FCConvert.ToString(rsData.Get_Fields_String("Acct1")));
                    txtAcct2.TextMatrix(0, 0, FCConvert.ToString(rsData.Get_Fields_String("Acct2")));
                    txtAcct3.TextMatrix(0, 0, FCConvert.ToString(rsData.Get_Fields_String("Acct3")));
                    txtAcct4.TextMatrix(0, 0, FCConvert.ToString(rsData.Get_Fields_String("Acct4")));
                    txtTotalUsed.Text = Strings.Format(rsData.Get_Fields_Int32("TotalUsed"), "#,##0");
                    // TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
                    if (FCConvert.ToInt32(rsData.Get_Fields("Life")) > 999)
                    {
                        txtLife.Text = "999";
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
                        txtLife.Text = FCConvert.ToString(rsData.Get_Fields("Life"));
                    }
                    txtTotalPosted.Text = FCConvert.ToString(rsData.Get_Fields_String("TotalPosted"));
                    txtValue.Text = Strings.Format(rsData.Get_Fields_Double("TotalValue"), "#,##0.00");
                    if (fecherFoundation.FCUtils.IsEmptyDateTime(rsData.Get_Fields_DateTime("DateLastDepreciation")) || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_DateTime("DateLastDepreciation"))) == string.Empty)
                    {
                    }
                    else
                    {
                        mskDateLastDepreciation.Text = FCConvert.ToString(modDateRoutines.LongDate(rsData.Get_Fields_DateTime("DateLastDepreciation")));
                    }
                    if (fecherFoundation.FCUtils.IsEmptyDateTime(rsData.Get_Fields_DateTime("OutOfServiceDate")) || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_DateTime("OutOfServiceDate"))) == string.Empty)
                    {
                    }
                    else
                    {
                        mebOutOfServiceDate.Text = FCConvert.ToString(modDateRoutines.LongDate(rsData.Get_Fields("OutOfServiceDate")));
                    }
                    if (Conversion.Val(rsData.Get_Fields_Double("DBRate")) == 0)
                    {
                        cboRate.Text = FCConvert.ToString(2);
                    }
                    else
                    {
                        cboRate.Text = FCConvert.ToString(rsData.Get_Fields_Double("DBRate"));
                    }
                    txtUN.Text = FCConvert.ToString(rsData.Get_Fields_String("UNRate"));
                    cmbDepreciation.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_Int16("DepreciationMethod")));
                    if (cmbDepreciation.SelectedIndex == 3)
                    {
                        optDepreciation_Click(3);
                        SetConditionCombo_2(rsData.Get_Fields_String("Condition"));
                    }
                }
                mnuFileComments.Enabled = true;
            }
            LoadGrid(lngID);
            boolDataChanged = false;
        }

        private object SearchComboBoxes(ref FCComboBox ControlName, int lngID, string strType = "LOCATION", int intTypeID = 1)
        {
            object SearchComboBoxes = null;
            int intCounter;
            if (lngID == 0)
            {
                // do nothing
            }
            else
            {
                for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
                {
                    if (ControlName.ItemData(intCounter) == lngID)
                    {
                        ControlName.SelectedIndex = intCounter;
                        return SearchComboBoxes;
                    }
                }
            }
            return SearchComboBoxes;
        }
    }
}
