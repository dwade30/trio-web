﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSettings.
	/// </summary>
	public partial class frmSettings : BaseForm
	{
		public frmSettings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSettings InstancePtr
		{
			get
			{
				return (frmSettings)Sys.GetInstance(typeof(frmSettings));
			}
		}

		protected frmSettings _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/04/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		bool boolDataChanged;

		private void chkDeptDiv_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkDeptDiv.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkDiv.CheckState = Wisej.Web.CheckState.Unchecked;
				chkDiv.Enabled = false;
			}
			else
			{
				chkDiv.Enabled = true;
			}
			boolDataChanged = true;
		}

		private void chkDiv_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void chkSearch_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void chkVendors_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void frmSettings_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmSettings_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmSettings_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			if (KeyAscii == Keys.Return)
				Support.SendKeys("{TAB}", false);
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSettings_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSettings.ScaleWidth	= 5880;
			//frmSettings.ScaleHeight	= 4140;
			//frmSettings.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this, false);
			LoadData();
			// if the user does not have budgetary then let them know
			lblBud.Visible = !modVariables.Statics.boolBudgetaryExists;
			lblBud2.Visible = !modVariables.Statics.boolBudgetaryExists;
			lblBud.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			lblBud2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			chkDeptDiv.Enabled = modVariables.Statics.boolBudgetaryExists;
			chkVendors.Enabled = modVariables.Statics.boolBudgetaryExists;
			boolDataChanged = false;
			modGlobalFunctions.SetFixedSize(this, 1);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							if (SaveData())
							{
								mnuExit_Click();
							}
							else
							{
								MessageBox.Show("Error in saving data", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								e.Cancel = true;
								this.Focus();
								return;
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							Close();
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							e.Cancel = true;
							this.Focus();
							return;
						}
				}
				//end switch
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("Error in saving data", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
			else
			{
				MessageBox.Show("Error in saving data", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsData = new clsDRWrapper();
				rsData.OpenRecordset("Select * from tblSettings", modGlobal.DatabaseNamePath);
				if (rsData.EndOfFile())
				{
					rsData.AddNew();
				}
				else
				{
					rsData.Edit();
				}
				rsData.Set_Fields("RefreshSeachOnKeyPress", (0 != (chkSearch.CheckState) ? 1 : 0));
				rsData.Set_Fields("DeptDiv", (0 != (chkDeptDiv.CheckState) ? 1 : 0));
				rsData.Set_Fields("Vendors", (0 != (chkVendors.CheckState) ? 1 : 0));
				rsData.Set_Fields("DivisionUsed", (0 != (chkDiv.CheckState) ? 1 : 0));
				rsData.Set_Fields("Location1Caption", Strings.Trim(txtLocation1.Text));
				rsData.Set_Fields("Location2Caption", Strings.Trim(txtLocation2.Text));
				rsData.Set_Fields("Location3Caption", Strings.Trim(txtLocation3.Text));
				rsData.Set_Fields("Location4Caption", Strings.Trim(txtLocation4.Text));
				if (Strings.Trim(txtLocation1.Text) == "")
				{
					modStartup.Statics.strLoc1Title = "Location 1";
				}
				else
				{
					modStartup.Statics.strLoc1Title = Strings.Trim(txtLocation1.Text);
				}
				if (Strings.Trim(txtLocation2.Text) == "")
				{
					modStartup.Statics.strLoc2Title = "Location 2";
				}
				else
				{
					modStartup.Statics.strLoc2Title = Strings.Trim(txtLocation2.Text);
				}
				if (Strings.Trim(txtLocation3.Text) == "")
				{
					modStartup.Statics.strLoc3Title = "Location 3";
				}
				else
				{
					modStartup.Statics.strLoc3Title = Strings.Trim(txtLocation3.Text);
				}
				if (Strings.Trim(txtLocation4.Text) == "")
				{
					modStartup.Statics.strLoc4Title = "Location 4";
				}
				else
				{
					modStartup.Statics.strLoc4Title = Strings.Trim(txtLocation4.Text);
				}
				rsData.Update();
				SaveData = true;
				MessageBox.Show("Record saved successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				modVariables.Statics.boolVendorsFromBud = FCConvert.ToBoolean((chkVendors.CheckState == Wisej.Web.CheckState.Checked ? 1 : 0));
				modVariables.Statics.boolDeptDivFromBud = FCConvert.ToBoolean((chkDeptDiv.CheckState == Wisej.Web.CheckState.Checked ? 1 : 0));
				modVariables.Statics.boolUseDivision = FCConvert.ToBoolean((chkDiv.CheckState == Wisej.Web.CheckState.Checked ? 1 : 0));
				boolDataChanged = false;
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveData;
		}

		private object LoadData()
		{
			object LoadData = null;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblSettings", modGlobal.DatabaseNamePath);
			if (rsData.EndOfFile())
			{
				chkSearch.CheckState = Wisej.Web.CheckState.Checked;
				chkDiv.CheckState = Wisej.Web.CheckState.Checked;
				chkDeptDiv.CheckState = (modVariables.Statics.boolBudgetaryExists ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				chkVendors.CheckState = (modVariables.Statics.boolBudgetaryExists ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			}
			else
			{
				chkSearch.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("RefreshSeachOnKeyPress")) ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				// TODO Get_Fields: Check the table for the column [DeptDiv] and replace with corresponding Get_Field method
				chkDeptDiv.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields("DeptDiv")) ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				chkVendors.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Vendors")) ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				chkDiv.CheckState = (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DivisionUsed")) ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
				txtLocation1.Text = FCConvert.ToString(rsData.Get_Fields_String("Location1Caption"));
				txtLocation2.Text = FCConvert.ToString(rsData.Get_Fields_String("Location2Caption"));
				txtLocation3.Text = FCConvert.ToString(rsData.Get_Fields_String("Location3Caption"));
				txtLocation4.Text = FCConvert.ToString(rsData.Get_Fields_String("Location4Caption"));
			}
			return LoadData;
		}

		private void txtLocation1_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtLocation2_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtLocation3_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtLocation4_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
