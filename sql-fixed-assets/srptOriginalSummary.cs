﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for srptOriginalSummary.
	/// </summary>
	public partial class srptOriginalSummary : FCSectionReport
	{
		public static srptOriginalSummary InstancePtr
		{
			get
			{
				return (srptOriginalSummary)Sys.GetInstance(typeof(srptOriginalSummary));
			}
		}

		protected srptOriginalSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptOriginalSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
	}
}
