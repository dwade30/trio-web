﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using GrapeCity.ActiveReports;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSetupAssetDepreciation.
	/// </summary>
	public partial class frmSetupAssetDepreciation : BaseForm
	{
		public frmSetupAssetDepreciation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupAssetDepreciation InstancePtr
		{
			get
			{
				return (frmSetupAssetDepreciation)Sys.GetInstance(typeof(frmSetupAssetDepreciation));
			}
		}

		protected frmSetupAssetDepreciation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime datReturnedDate;

		private void cmdEndDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdStartDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void frmSetupAssetDepreciation_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupAssetDepreciation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupAssetDepreciation.FillStyle	= 0;
			//frmSetupAssetDepreciation.ScaleWidth	= 3885;
			//frmSetupAssetDepreciation.ScaleHeight	= 2565;
			//frmSetupAssetDepreciation.LinkTopic	= "Form2";
			//frmSetupAssetDepreciation.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			txtStartDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSetupAssetDepreciation_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptAssetDepreciation.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
			{
				MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rptAssetDepreciation.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrintPreview_Click(object sender, EventArgs e)
		{
			mnuFilePreview_Click(mnuFilePreview, EventArgs.Empty);
		}

		private void fcButton1_Click(object sender, EventArgs e)
		{
			mnuFilePrint_Click(mnuFilePrint, EventArgs.Empty);
		}
	}
}
