﻿//Fecher vbPorter - Version 1.0.0.27
using Wisej.Web;
using Global;
using fecherFoundation;
using System;

namespace TWFA0000
{
	public class modDirtyForm
	{
		//=========================================================
		public static void SaveChanges(int intDataChanged, ref Form objName, string strSaveRoutine)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "SaveChanges";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			// have any rows been altered?
			if (intDataChanged > 0)
			{
				if (MessageBox.Show("Current changes have not been saved. Do so now?", "Clerk", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					// save all changes
					FCUtils.CallByName(objName, strSaveRoutine, CallType.Method);
				}
			}
		}

		public static object ClearDirtyControls(ref Form FormName)
		{
			object ClearDirtyControls = null;
			/*? On Error Resume Next  *//* Control ControlName = new Control(); *///FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
			//foreach (Control ControlName in FormName.Controls)
			var controls = fecherFoundation.Extensions.ControlExtension.GetAllControls(FormName);
			foreach (Control ControlName in controls)
			{
				if (ControlName is FCTextBox)
				{
					(ControlName as FCTextBox).DataChanged = false;
				}
				else if (ControlName is FCComboBox)
				{
					(ControlName as FCComboBox).DataChanged = false;
				}
				else if (ControlName is Global.T2KDateBox)
				{
					(ControlName as T2KDateBox).Modified = false;
				}
			}
			return ClearDirtyControls;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As short
		public static short NumberDirtyControls(ref Form FormName)
		{
			short NumberDirtyControls = 0;
			/*? On Error Resume Next  *//* Control ControlName = new Control(); *///FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
			//foreach (Control ControlName in FormName.Controls)
			var controls = fecherFoundation.Extensions.ControlExtension.GetAllControls(FormName);
			foreach (Control ControlName in controls)
			{
				if (ControlName is FCTextBox)
				{
					if (!(ControlName as FCTextBox).DataChanged)
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
				else if (ControlName is FCComboBox)
				{
					if (!(ControlName as FCComboBox).DataChanged)
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
				else if (ControlName is Global.T2KDateBox)
				{
					if (!(ControlName as T2KDateBox).Modified)
					{
					}
					else
					{
						NumberDirtyControls += 1;
					}
				}
			}
			return NumberDirtyControls;
		}
	}
}
