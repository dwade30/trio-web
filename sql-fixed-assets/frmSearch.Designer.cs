﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSearch.
	/// </summary>
	partial class frmSearch : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdSch;
		public fecherFoundation.FCTextBox txtColumn15;
		public fecherFoundation.FCTextBox txtColumn16;
		public fecherFoundation.FCTextBox txtColumn14;
		public fecherFoundation.FCTextBox txtColumn13;
		public fecherFoundation.FCTextBox txtColumn12;
		public fecherFoundation.FCTextBox txtColumn11;
		public fecherFoundation.FCTextBox txtColumn10;
		public fecherFoundation.FCTextBox txtColumn9;
		public fecherFoundation.FCTextBox txtColumn8;
		public fecherFoundation.FCTextBox txtColumn7;
		public fecherFoundation.FCTextBox txtColumn6;
		public fecherFoundation.FCTextBox txtColumn5;
		public fecherFoundation.FCTextBox txtColumn4;
		public fecherFoundation.FCTextBox txtColumn3;
		public fecherFoundation.FCTextBox txtColumn2;
		public fecherFoundation.FCTextBox txtColumn1;
		public fecherFoundation.FCGrid SearchGrid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		//public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuShow;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuEdit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearch));
            this.txtColumn15 = new fecherFoundation.FCTextBox();
            this.txtColumn16 = new fecherFoundation.FCTextBox();
            this.txtColumn14 = new fecherFoundation.FCTextBox();
            this.txtColumn13 = new fecherFoundation.FCTextBox();
            this.txtColumn12 = new fecherFoundation.FCTextBox();
            this.txtColumn11 = new fecherFoundation.FCTextBox();
            this.txtColumn10 = new fecherFoundation.FCTextBox();
            this.txtColumn9 = new fecherFoundation.FCTextBox();
            this.txtColumn8 = new fecherFoundation.FCTextBox();
            this.txtColumn7 = new fecherFoundation.FCTextBox();
            this.txtColumn6 = new fecherFoundation.FCTextBox();
            this.txtColumn5 = new fecherFoundation.FCTextBox();
            this.txtColumn4 = new fecherFoundation.FCTextBox();
            this.txtColumn3 = new fecherFoundation.FCTextBox();
            this.txtColumn2 = new fecherFoundation.FCTextBox();
            this.txtColumn1 = new fecherFoundation.FCTextBox();
            this.SearchGrid = new fecherFoundation.FCGrid();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            //this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuShow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEdit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSch_4 = new fecherFoundation.FCButton();
            this.cmdSch_0 = new fecherFoundation.FCButton();
            this.cmdSch_3 = new fecherFoundation.FCButton();
            this.cmdSch_1 = new fecherFoundation.FCButton();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSch_4);
            this.BottomPanel.Location = new System.Drawing.Point(0, 468);
            this.BottomPanel.Size = new System.Drawing.Size(1230, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.txtColumn15);
            this.ClientArea.Controls.Add(this.txtColumn16);
            this.ClientArea.Controls.Add(this.txtColumn14);
            this.ClientArea.Controls.Add(this.txtColumn13);
            this.ClientArea.Controls.Add(this.txtColumn12);
            this.ClientArea.Controls.Add(this.txtColumn11);
            this.ClientArea.Controls.Add(this.txtColumn10);
            this.ClientArea.Controls.Add(this.txtColumn9);
            this.ClientArea.Controls.Add(this.txtColumn8);
            this.ClientArea.Controls.Add(this.txtColumn7);
            this.ClientArea.Controls.Add(this.txtColumn6);
            this.ClientArea.Controls.Add(this.txtColumn5);
            this.ClientArea.Controls.Add(this.txtColumn4);
            this.ClientArea.Controls.Add(this.txtColumn3);
            this.ClientArea.Controls.Add(this.txtColumn2);
            this.ClientArea.Controls.Add(this.txtColumn1);
            this.ClientArea.Controls.Add(this.SearchGrid);
            this.ClientArea.Size = new System.Drawing.Size(1230, 408);
            this.ClientArea.ScrollBars = ScrollBars.None;
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSch_0);
            this.TopPanel.Controls.Add(this.cmdSch_3);
            this.TopPanel.Controls.Add(this.cmdSch_1);
            this.TopPanel.Size = new System.Drawing.Size(1230, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdSch_1, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSch_3, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSch_0, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(171, 30);
            this.HeaderText.Text = "Search Assets";
            // 
            // txtColumn15
            // 
            this.txtColumn15.AutoSize = false;
            this.txtColumn15.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn15.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn15.LinkItem = null;
            this.txtColumn15.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn15.LinkTopic = null;
            this.txtColumn15.Location = new System.Drawing.Point(348, 49);
            this.txtColumn15.Name = "txtColumn15";
            this.txtColumn15.Size = new System.Drawing.Size(35, 40);
            this.txtColumn15.TabIndex = 21;
            this.txtColumn15.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn15_KeyUp);
            // 
            // txtColumn16
            // 
            this.txtColumn16.AutoSize = false;
            this.txtColumn16.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn16.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn16.LinkItem = null;
            this.txtColumn16.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn16.LinkTopic = null;
            this.txtColumn16.Location = new System.Drawing.Point(388, 49);
            this.txtColumn16.Name = "txtColumn16";
            this.txtColumn16.Size = new System.Drawing.Size(35, 40);
            this.txtColumn16.TabIndex = 20;
            this.txtColumn16.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn16_KeyUp);

            // 
            // txtColumn14
            // 
            this.txtColumn14.AutoSize = false;
            this.txtColumn14.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn14.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn14.LinkItem = null;
            this.txtColumn14.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn14.LinkTopic = null;
            this.txtColumn14.Location = new System.Drawing.Point(305, 50);
            this.txtColumn14.Name = "txtColumn14";
            this.txtColumn14.Size = new System.Drawing.Size(35, 40);
            this.txtColumn14.TabIndex = 19;
            this.txtColumn14.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn14_KeyUp);
            // 
            // txtColumn13
            // 
            this.txtColumn13.AutoSize = false;
            this.txtColumn13.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn13.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn13.LinkItem = null;
            this.txtColumn13.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn13.LinkTopic = null;
            this.txtColumn13.Location = new System.Drawing.Point(263, 50);
            this.txtColumn13.Name = "txtColumn13";
            this.txtColumn13.Size = new System.Drawing.Size(35, 40);
            this.txtColumn13.TabIndex = 18;
            this.txtColumn13.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn13_KeyUp);
            // 
            // txtColumn12
            // 
            this.txtColumn12.AutoSize = false;
            this.txtColumn12.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn12.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn12.LinkItem = null;
            this.txtColumn12.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn12.LinkTopic = null;
            this.txtColumn12.Location = new System.Drawing.Point(220, 48);
            this.txtColumn12.Name = "txtColumn12";
            this.txtColumn12.Size = new System.Drawing.Size(35, 40);
            this.txtColumn12.TabIndex = 17;
            this.txtColumn12.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn12_KeyUp);
            // 
            // txtColumn11
            // 
            this.txtColumn11.AutoSize = false;
            this.txtColumn11.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn11.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn11.LinkItem = null;
            this.txtColumn11.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn11.LinkTopic = null;
            this.txtColumn11.Location = new System.Drawing.Point(182, 48);
            this.txtColumn11.Name = "txtColumn11";
            this.txtColumn11.Size = new System.Drawing.Size(35, 40);
            this.txtColumn11.TabIndex = 16;
            this.txtColumn11.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn11_KeyUp);
            // 
            // txtColumn10
            // 
            this.txtColumn10.AutoSize = false;
            this.txtColumn10.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn10.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn10.LinkItem = null;
            this.txtColumn10.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn10.LinkTopic = null;
            this.txtColumn10.Location = new System.Drawing.Point(142, 48);
            this.txtColumn10.Name = "txtColumn10";
            this.txtColumn10.Size = new System.Drawing.Size(35, 40);
            this.txtColumn10.TabIndex = 15;
            this.txtColumn10.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn10_KeyUp);
            // 
            // txtColumn9
            // 
            this.txtColumn9.AutoSize = false;
            this.txtColumn9.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn9.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn9.LinkItem = null;
            this.txtColumn9.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn9.LinkTopic = null;
            this.txtColumn9.Location = new System.Drawing.Point(103, 48);
            this.txtColumn9.Name = "txtColumn9";
            this.txtColumn9.Size = new System.Drawing.Size(35, 40);
            this.txtColumn9.TabIndex = 14;
            this.txtColumn9.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn9_KeyUp);

            // 
            // txtColumn8
            // 
            this.txtColumn8.AutoSize = false;
            this.txtColumn8.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn8.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn8.LinkItem = null;
            this.txtColumn8.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn8.LinkTopic = null;
            this.txtColumn8.Location = new System.Drawing.Point(305, 28);
            this.txtColumn8.Name = "txtColumn8";
            this.txtColumn8.Size = new System.Drawing.Size(35, 40);
            this.txtColumn8.TabIndex = 13;
            this.txtColumn8.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn8_KeyUp);
            // 
            // txtColumn7
            // 
            this.txtColumn7.AutoSize = false;
            this.txtColumn7.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn7.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn7.LinkItem = null;
            this.txtColumn7.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn7.LinkTopic = null;
            this.txtColumn7.Location = new System.Drawing.Point(263, 28);
            this.txtColumn7.Name = "txtColumn7";
            this.txtColumn7.Size = new System.Drawing.Size(35, 40);
            this.txtColumn7.TabIndex = 12;
            this.txtColumn7.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn7_KeyUp);
            // 
            // txtColumn6
            // 
            this.txtColumn6.AutoSize = false;
            this.txtColumn6.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn6.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn6.LinkItem = null;
            this.txtColumn6.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn6.LinkTopic = null;
            this.txtColumn6.Location = new System.Drawing.Point(220, 28);
            this.txtColumn6.Name = "txtColumn6";
            this.txtColumn6.Size = new System.Drawing.Size(35, 40);
            this.txtColumn6.TabIndex = 11;
            this.txtColumn6.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn6_KeyUp);
            // 
            // txtColumn5
            // 
            this.txtColumn5.AutoSize = false;
            this.txtColumn5.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn5.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn5.LinkItem = null;
            this.txtColumn5.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn5.LinkTopic = null;
            this.txtColumn5.Location = new System.Drawing.Point(184, 28);
            this.txtColumn5.Name = "txtColumn5";
            this.txtColumn5.Size = new System.Drawing.Size(31, 40);
            this.txtColumn5.TabIndex = 10;
            this.txtColumn5.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn5_KeyUp);
            // 
            // txtColumn4
            // 
            this.txtColumn4.AutoSize = false;
            this.txtColumn4.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn4.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn4.LinkItem = null;
            this.txtColumn4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn4.LinkTopic = null;
            this.txtColumn4.Location = new System.Drawing.Point(144, 28);
            this.txtColumn4.Name = "txtColumn4";
            this.txtColumn4.Size = new System.Drawing.Size(35, 40);
            this.txtColumn4.TabIndex = 9;
            this.txtColumn4.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn4_KeyUp);
            // 
            // txtColumn3
            // 
            this.txtColumn3.AutoSize = false;
            this.txtColumn3.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn3.LinkItem = null;
            this.txtColumn3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn3.LinkTopic = null;
            this.txtColumn3.Location = new System.Drawing.Point(111, 28);
            this.txtColumn3.Name = "txtColumn3";
            this.txtColumn3.Size = new System.Drawing.Size(27, 40);
            this.txtColumn3.TabIndex = 8;
            this.txtColumn3.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn3_KeyUp);
            // 
            // txtColumn2
            // 
            this.txtColumn2.AutoSize = false;
            this.txtColumn2.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn2.LinkItem = null;
            this.txtColumn2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn2.LinkTopic = null;
            this.txtColumn2.Location = new System.Drawing.Point(75, 28);
            this.txtColumn2.Name = "txtColumn2";
            this.txtColumn2.Size = new System.Drawing.Size(31, 40);
            this.txtColumn2.TabIndex = 7;
            this.txtColumn2.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn2_KeyUp);
            // 
            // txtColumn1
            // 
            this.txtColumn1.AutoSize = false;
            this.txtColumn1.BackColor = System.Drawing.SystemColors.Window;
            this.txtColumn1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtColumn1.LinkItem = null;
            this.txtColumn1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColumn1.LinkTopic = null;
            this.txtColumn1.Location = new System.Drawing.Point(38, 28);
            this.txtColumn1.Name = "txtColumn1";
            this.txtColumn1.Size = new System.Drawing.Size(33, 40);
            this.txtColumn1.TabIndex = 6;
            this.txtColumn1.KeyUp += new Wisej.Web.KeyEventHandler(this.txtColumn1_KeyUp);
            // 
            // SearchGrid
            // 
            this.SearchGrid.AllowSelection = false;
            this.SearchGrid.AllowUserToResizeColumns = false;
            this.SearchGrid.AllowUserToResizeRows = false;
            this.SearchGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left | Wisej.Web.AnchorStyles.Right)));
            this.SearchGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
            this.SearchGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.SearchGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.SearchGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.SearchGrid.BackColorSel = System.Drawing.Color.Empty;
            this.SearchGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.SearchGrid.Cols = 10;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.SearchGrid.ColumnHeadersHeight = 30;
            this.SearchGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.SearchGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.SearchGrid.DragIcon = null;
            this.SearchGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.SearchGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.SearchGrid.FrozenCols = 0;
            this.SearchGrid.GridColor = System.Drawing.Color.Empty;
            this.SearchGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.SearchGrid.Location = new System.Drawing.Point(30, 30);
            this.SearchGrid.Name = "SearchGrid";
            this.SearchGrid.OutlineCol = 0;
            this.SearchGrid.ReadOnly = true;
            this.SearchGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.SearchGrid.RowHeightMin = 0;
            this.SearchGrid.Rows = 50;
            this.SearchGrid.ScrollTipText = null;
            this.SearchGrid.ShowColumnVisibilityMenu = false;
            this.SearchGrid.Size = new System.Drawing.Size(1200, 324);
            this.SearchGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.SearchGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.SearchGrid.TabIndex = 0;
            this.SearchGrid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.SearchGrid_KeyDownEdit);
            this.SearchGrid.CurrentCellChanged += new System.EventHandler(this.SearchGrid_RowColChange);
            this.SearchGrid.Scroll += new Wisej.Web.ScrollEventHandler(this.SearchGrid_AfterScroll);
            this.SearchGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.SearchGrid_KeyDownEvent);
            this.SearchGrid.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.SearchGrid_ClickEvent);
            this.SearchGrid.DoubleClick += new System.EventHandler(this.SearchGrid_DblClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            //this.mnuSearch,
            this.mnuShow,
            this.mnuSP1,
            this.mnuEdit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            //// 
            //// mnuSearch
            //// 
            //this.mnuSearch.Index = 1;
            //this.mnuSearch.Name = "mnuSearch";
            //this.mnuSearch.Shortcut = Wisej.Web.Shortcut.F12;
            //this.mnuSearch.Text = "Search";
            //this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // mnuShow
            // 
            this.mnuShow.Index = 2;
            this.mnuShow.Name = "mnuShow";
            this.mnuShow.Text = "Show Record";
            this.mnuShow.Click += new System.EventHandler(this.mnuShow_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 3;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuEdit
            // 
            this.mnuEdit.Index = 4;
            this.mnuEdit.Name = "mnuEdit";
            this.mnuEdit.Text = "Exit";
            this.mnuEdit.Click += new System.EventHandler(this.mnuEdit_Click);
            // 
            // cmdSch_4
            // 
            this.cmdSch_4.AppearanceKey = "acceptButton";
            this.cmdSch_4.Location = new System.Drawing.Point(496, 30);
            this.cmdSch_4.Name = "cmdSch_4";
            //this.cmdSch_4.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSch_4.Size = new System.Drawing.Size(147, 48);
            this.cmdSch_4.TabIndex = 5;
            this.cmdSch_4.Text = "Show Record";
            this.cmdSch_4.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // cmdSch_0
            // 
            this.cmdSch_0.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSch_0.AppearanceKey = "toolbarButton";
            this.cmdSch_0.Location = new System.Drawing.Point(966, 29);
            this.cmdSch_0.Name = "cmdSch_0";
            this.cmdSch_0.Size = new System.Drawing.Size(42, 24);
            this.cmdSch_0.TabIndex = 1;
            this.cmdSch_0.Text = "Quit";
            this.cmdSch_0.Visible = false;
            this.cmdSch_0.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // cmdSch_3
            // 
            this.cmdSch_3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSch_3.AppearanceKey = "toolbarButton";
            this.cmdSch_3.Location = new System.Drawing.Point(1097, 29);
            this.cmdSch_3.Name = "cmdSch_3";
            this.cmdSch_3.Size = new System.Drawing.Size(49, 24);
            this.cmdSch_3.TabIndex = 2;
            this.cmdSch_3.Text = "Clear";
            this.cmdSch_3.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // cmdSch_1
            // 
            this.cmdSch_1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSch_1.AppearanceKey = "toolbarButton";
            this.cmdSch_1.ImageSource = "button-search";
            this.cmdSch_1.Location = new System.Drawing.Point(1012, 29);
            this.cmdSch_1.Name = "cmdSch_1";
            this.cmdSch_1.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSch_1.Size = new System.Drawing.Size(81, 24);
            this.cmdSch_1.TabIndex = 4;
            this.cmdSch_1.Text = "Search";
            this.cmdSch_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSch_1.Click += new System.EventHandler(this.cmdSch_Click);
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(2057, 293);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(4, 14);
            this.fcLabel1.TabIndex = 22;
            // 
            // frmSearch
            // 
            this.AcceptButton = this.cmdSch_1;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1230, 564);
            this.KeyPreview = true;
            this.Name = "frmSearch";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Search Assets";
            this.Load += new System.EventHandler(this.frmSearch_Load);
            this.Activated += new System.EventHandler(this.frmSearch_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSearch_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSearch_KeyPress);
            this.Resize += new System.EventHandler(this.frmSearch_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSch_1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCButton cmdSch_1;
		public FCButton cmdSch_3;
		public FCButton cmdSch_0;
		public FCButton cmdSch_4;
		private FCLabel fcLabel1;
	}
}
