﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptDepreciationTotals.
	/// </summary>
	public partial class rptDepreciationTotals : BaseSectionReport
	{
		public static rptDepreciationTotals InstancePtr
		{
			get
			{
				return (rptDepreciationTotals)Sys.GetInstance(typeof(rptDepreciationTotals));
			}
		}

		protected rptDepreciationTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDepreciationTotals	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsInfo1 = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotalCost As Decimal	OnWrite(short, Decimal)
		Decimal curTotalCost;
		// vbPorter upgrade warning: curTotalDepreciated As Decimal	OnWrite(short, Decimal)
		Decimal curTotalDepreciated;
		// vbPorter upgrade warning: curTotalTotalDepreciated As Decimal	OnWrite(short, Decimal)
		Decimal curTotalTotalDepreciated;
		// vbPorter upgrade warning: curClassCost As Decimal	OnWrite(short, Decimal)
		Decimal curClassCost;
		// vbPorter upgrade warning: curClassDepreciated As Decimal	OnWrite(short, Decimal)
		Decimal curClassDepreciated;
		// vbPorter upgrade warning: curClassTotalDepreciated As Decimal	OnWrite(short, Decimal)
		Decimal curClassTotalDepreciated;
		bool blnGroupByClass;
		bool blnGroupByDeptDiv;
		bool blnDiscardedDate;
		// vbPorter upgrade warning: datLowDepreciationDate As DateTime	OnWrite(DateTime, string)
		DateTime datLowDepreciationDate;
		// vbPorter upgrade warning: datHighDepreciationDate As DateTime	OnWrite(DateTime, string)
		DateTime datHighDepreciationDate;

		public rptDepreciationTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Depreciation Totals";
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
			if (blnGroupByClass)
			{
				if (!eArgs.EOF)
				{
					this.Fields["Binder"].Value = rsInfo.Get_Fields_String("ClassCode");
				}
			}
			if (blnGroupByDeptDiv)
			{
				if (!eArgs.EOF)
				{
					this.Fields["Binder"].Value = rsInfo.Get_Fields_String("Dept") + "-" + rsInfo.Get_Fields_String("Div");
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string strTotalDep = "";
			string strWhereDep = "";
			string strWhereDiscard = "";
			int counter;
			string strOpenRS = "";
			string strPurchaseDisacrdDateSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			Label2.Text = "TRIO Software Corp";
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			curTotalCost = 0;
			curTotalDepreciated = 0;
			curTotalTotalDepreciated = 0;
			curClassCost = 0;
			curClassDepreciated = 0;
			curClassTotalDepreciated = 0;
			blnFirstRecord = true;
			if (frmSetupDepraciationTotals.InstancePtr.cmbAllDate.SelectedIndex == 0)
			{
				lblDateRange.Text = "Depreciation Dates: ALL";
				strPurchaseDisacrdDateSQL = "";
				blnDiscardedDate = false;
				datLowDepreciationDate = FCConvert.ToDateTime("1/1/1800");
				datHighDepreciationDate = DateTime.Today;
				lblPurchaseDate.Text = "Pur. Date";
			}
			else if (frmSetupDepraciationTotals.InstancePtr.cmbAllDate.SelectedIndex == 1)
			{
				lblDateRange.Text = "Depreciation Dates: " + frmSetupDepraciationTotals.InstancePtr.txtStartDate.Text + " To " + frmSetupDepraciationTotals.InstancePtr.txtEndDate.Text;
				strPurchaseDisacrdDateSQL = "";
				blnDiscardedDate = false;
				datLowDepreciationDate = FCConvert.ToDateTime(frmSetupDepraciationTotals.InstancePtr.txtStartDate.Text);
				datHighDepreciationDate = FCConvert.ToDateTime(frmSetupDepraciationTotals.InstancePtr.txtEndDate.Text);
				lblPurchaseDate.Text = "Pur. Date";
			}
			else if (frmSetupDepraciationTotals.InstancePtr.cmbAllDate.SelectedIndex == 2)
			{
				lblDateRange.Text = "Purchase Dates: " + frmSetupDepraciationTotals.InstancePtr.txtStartPurchaseDate.Text + " To " + frmSetupDepraciationTotals.InstancePtr.txtEndPurchaseDate.Text;
				strPurchaseDisacrdDateSQL = " AND (PurchaseDate >= '" + FCConvert.ToString(DateAndTime.DateValue(frmSetupDepraciationTotals.InstancePtr.txtStartPurchaseDate.Text)) + "' AND PurchaseDate <= '" + FCConvert.ToString(DateAndTime.DateValue(frmSetupDepraciationTotals.InstancePtr.txtEndPurchaseDate.Text)) + "') ";
				blnDiscardedDate = false;
				datLowDepreciationDate = FCConvert.ToDateTime(frmSetupDepraciationTotals.InstancePtr.txtStartPurchaseDate.Text);
				datHighDepreciationDate = FCConvert.ToDateTime(frmSetupDepraciationTotals.InstancePtr.txtEndPurchaseDate.Text);
				lblPurchaseDate.Text = "Pur. Date";
			}
			else
			{
				lblDateRange.Text = "Discarded Dates: " + frmSetupDepraciationTotals.InstancePtr.txtStartDiscardedDate.Text + " To " + frmSetupDepraciationTotals.InstancePtr.txtEndDiscardedDate.Text;
				strPurchaseDisacrdDateSQL = " AND (DiscardedDate >= '" + FCConvert.ToString(DateAndTime.DateValue(frmSetupDepraciationTotals.InstancePtr.txtStartDiscardedDate.Text)) + "' AND DiscardedDate <= '" + FCConvert.ToString(DateAndTime.DateValue(frmSetupDepraciationTotals.InstancePtr.txtEndDiscardedDate.Text)) + "') ";
				blnDiscardedDate = true;
				datLowDepreciationDate = FCConvert.ToDateTime(frmSetupDepraciationTotals.InstancePtr.txtStartDiscardedDate.Text);
				datHighDepreciationDate = FCConvert.ToDateTime(frmSetupDepraciationTotals.InstancePtr.txtEndDiscardedDate.Text);
				lblPurchaseDate.Text = "Dis. Date";
			}
			if (frmSetupDepraciationTotals.InstancePtr.cmbAll.SelectedIndex == 0)
			{
				strSQL = "";
				lblClassCodes.Text = "Class Codes: ALL";
			}
			else
			{
				lblClassCodes.Text = "Class Codes: ";
				strSQL = "(";
				for (counter = 1; counter <= frmSetupDepraciationTotals.InstancePtr.vsClass.Rows - 1; counter++)
				{
					if (FCUtils.CBool(frmSetupDepraciationTotals.InstancePtr.vsClass.TextMatrix(counter, 0)) == true)
					{
						strSQL += "'" + frmSetupDepraciationTotals.InstancePtr.vsClass.TextMatrix(counter, 1) + "', ";
						lblClassCodes.Text = lblClassCodes.Text + frmSetupDepraciationTotals.InstancePtr.vsClass.TextMatrix(counter, 1) + ", ";
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
				lblClassCodes.Text = Strings.Left(lblClassCodes.Text, lblClassCodes.Text.Length - 2);
			}
			if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 1)
			{
				blnGroupByClass = true;
				lblClass.Visible = true;
				Line3.Visible = true;
				lblClassTotal.Visible = true;
				fldClassCost.Visible = true;
				fldClassDepreciation.Visible = true;
				fldClassTotalDeprecition.Visible = true;
			}
			else if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 0)
			{
				blnGroupByClass = false;
				lblClass.Visible = false;
				Line3.Visible = false;
				lblClassTotal.Visible = false;
				fldClassCost.Visible = false;
				fldClassDepreciation.Visible = false;
				fldClassTotalDeprecition.Visible = false;
			}
			else if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 2)
			{
				blnGroupByClass = false;
				blnGroupByDeptDiv = true;
				lblClass.Visible = true;
				Line3.Visible = true;
				lblClassTotal.Visible = true;
				fldClassCost.Visible = true;
				fldClassDepreciation.Visible = true;
				fldClassTotalDeprecition.Visible = true;
				lblClassTotal.Text = "Dept/Div Totals:";
			}
			if (frmSetupDepraciationTotals.InstancePtr.chkZeroCurDep.CheckState == Wisej.Web.CheckState.Checked)
			{
				strWhereDep = " or  (tblMaster.BasisCost = tblMaster.BasisCost) ";
			}
			else if (frmSetupDepraciationTotals.InstancePtr.chkDep.CheckState == Wisej.Web.CheckState.Checked)
			{
				strWhereDep = " or  (tblMaster.BasisCost - convert(money, tblMaster.TotalDepreciated) = tblMaster.SalvageValue) ";
			}
			else
			{
				strWhereDep = " ";
			}
			if (frmSetupDepraciationTotals.InstancePtr.chkExcludeDiscarded.CheckState == Wisej.Web.CheckState.Checked)
			{
				strWhereDiscard = " and discarded = 0 ";
			}
			if (frmSetupDepraciationTotals.InstancePtr.cmbAll.SelectedIndex == 1)
			{
				if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 0)
				{
					rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " tblClassCodes.Code IN " + strSQL + strPurchaseDisacrdDateSQL + "AND (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'" + strWhereDep + ") " + strWhereDiscard + "   GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue ORDER BY tblMaster.Description", "TWFA0000.vb1");
				}
				else if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 1)
				{
					rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " tblClassCodes.Code IN " + strSQL + strPurchaseDisacrdDateSQL + "AND (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "' " + strWhereDep + ") " + strWhereDiscard + "   GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue ORDER BY tblClassCodes.Code, tblMaster.Description", "TWFA0000.vb1");
				}
				else if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 2)
				{
					rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue, tblMaster.Dept as Dept, tblMaster.Div as Div FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " tblClassCodes.Code IN " + strSQL + strPurchaseDisacrdDateSQL + "AND (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "' " + strWhereDep + ") " + strWhereDiscard + "   GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue,dept,div ORDER BY dept,div, tblMaster.Description", "TWFA0000.vb1");
				}
			}
			else
			{
				if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 0)
				{
					rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'" + strWhereDep + ") " + strWhereDiscard + "  " + strPurchaseDisacrdDateSQL + " GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue ORDER BY tblMaster.Description", "TWFA0000.vb1");
				}
				else if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 1)
				{
					rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'" + strWhereDep + ") " + strWhereDiscard + "  " + strPurchaseDisacrdDateSQL + " GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue ORDER BY tblClassCodes.Code, tblMaster.Description", "TWFA0000.vb1");
				}
				else if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex == 2)
				{
					rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue, tblMaster.Dept as Dept, tblMaster.Div as Div FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'" + strWhereDep + ") " + strWhereDiscard + "  " + strPurchaseDisacrdDateSQL + " GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue,dept,div ORDER BY dept,div, tblMaster.Description", "TWFA0000.vb1");
				}
			}
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldDescription.Text = rsInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Field [Tag] not found!! (maybe it is an alias?)
			fldTag.Text = rsInfo.Get_Fields_String("Tag");
			rsInfo1.OpenRecordset("Select SUM(tblDepreciation.DepreciationAmount) as DepTotal from tblDepreciation Where ItemId=" + rsInfo.Get_Fields_Int32("ID") + "AND tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'", "TWFA0000.vb1");
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			fldDepreciation.Text = Strings.Format(Conversion.Val(rsInfo1.Get_Fields("DepTotal")), "#,##0.00");
			fldClass.Text = rsInfo.Get_Fields_String("ClassCode");
			if (blnDiscardedDate)
			{
				fldPurchaseDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("DiscardedDate"), "MM/dd/yyyy");
			}
			else
			{
				fldPurchaseDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy");
			}
			fldCost.Text = Strings.Format(rsInfo.Get_Fields_Decimal("BasisCost"), "#,##0.00");
			fldTotalDepreciated.Text = Strings.Format(rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue")), "#,##0.00");
			curTotalCost += rsInfo.Get_Fields_Decimal("BasisCost");
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			curTotalDepreciated += FCConvert.ToDecimal(rsInfo1.Get_Fields("DepTotal"));
			curTotalTotalDepreciated += (rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue")));
			curClassCost += rsInfo.Get_Fields_Decimal("BasisCost");
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			curClassDepreciated += FCConvert.ToDecimal(rsInfo1.Get_Fields("DepTotal"));
			curClassTotalDepreciated += (rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue")));
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCost.Text = Strings.Format(curTotalCost, "#,##0.00");
			fldTotalDepreciation.Text = Strings.Format(curTotalDepreciated, "#,##0.00");
			fldTotalTotalDepreciated.Text = Strings.Format(curTotalTotalDepreciated, "#,##0.00");
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldClassCost.Text = Strings.Format(curClassCost, "#,##0.00");
			fldClassDepreciation.Text = Strings.Format(curClassDepreciated, "#,##0.00");
			fldClassTotalDeprecition.Text = Strings.Format(curClassTotalDepreciated, "#,##0.00");
			curClassCost = 0;
			curClassDepreciated = 0;
			curClassTotalDepreciated = 0;
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsClassInfo = new clsDRWrapper();
			if (frmSetupDepraciationTotals.InstancePtr.cmbDescription.SelectedIndex != 2)
			{
				rsClassInfo.OpenRecordset("SELECT * FROM tblClassCodes WHERE Code = '" + rsInfo.Get_Fields_String("ClassCode") + "'", "TWFA0000.vb1");
				if (rsClassInfo.EndOfFile() != true)
				{
					lblClass.Text = rsInfo.Get_Fields_String("ClassCode") + " " + rsClassInfo.Get_Fields_String("Description");
				}
				else
				{
					lblClass.Text = rsInfo.Get_Fields_String("ClassCode") + " UNKNOWN";
				}
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("dept"))) != string.Empty || Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("div"))) != string.Empty)
				{
					lblClass.Text = rsInfo.Get_Fields_String("dept") + "-" + rsInfo.Get_Fields_String("div");
				}
				else
				{
					lblClass.Text = "None";
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptDepreciationTotals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptDepreciationTotals.Caption	= "Depreciation Totals";
			//rptDepreciationTotals.Icon	= "rptDepreciationTotals.dsx":0000";
			//rptDepreciationTotals.Left	= 0;
			//rptDepreciationTotals.Top	= 0;
			//rptDepreciationTotals.Width	= 11880;
			//rptDepreciationTotals.Height	= 8595;
			//rptDepreciationTotals.StartUpPosition	= 3;
			//rptDepreciationTotals.SectionData	= "rptDepreciationTotals.dsx":058A;
			//End Unmaped Properties
		}

		private void rptDepreciationTotals_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
	}
}
