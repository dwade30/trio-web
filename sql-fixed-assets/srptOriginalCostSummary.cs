﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Global;
using TWSharedLibrary;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for srptOriginalCostSummary.
	/// </summary>
	public partial class srptOriginalCostSummary : BaseSectionReport
	{
		public static srptOriginalCostSummary InstancePtr
		{
			get
			{
				return (srptOriginalCostSummary)Sys.GetInstance(typeof(srptOriginalCostSummary));
			}
		}

		protected srptOriginalCostSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo?.Dispose();
            }
			base.Dispose(disposing);
		}

		public srptOriginalCostSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		double curTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs e)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				e.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				e.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			curTotal = 0;
			rsInfo.OpenRecordset("SELECT Acct4, SUM(BasisCost) as TotalCost FROM tblMaster GROUP BY Acct4", "TWFA0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				//do nothing
			}
			else
			{
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldAccount.Text = rsInfo.Get_Fields_String("Acct4");
			fldAmount.Text = Strings.Format(rsInfo.Get_Fields_String("TotalCost"), "#,##0.00");
			curTotal += rsInfo.Get_Fields_Double("TotalCost");
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldFinalAmount.Text = Strings.Format(curTotal, "#,##0.00");
		}
	}
}
