﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using System.Drawing;
using System.Linq;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	public partial class frmCustomReport : BaseForm
	{
		public frmCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbReport.SelectedIndex = 0;
			this.cmbPortrait.SelectedIndex = 0;
            this.lstSort.View = View.SmallIcon;
			this.lstSort.ItemSize = new Size(lstSort.Width - 20,32);
			
			lstSort.DragStart += LstSort_DragStart;
			//FC:FINAL:AM:#327 - show  borders for columns
			this.vsLayout.CellBorderStyle = DataGridViewCellBorderStyle.Both;
		}

        private void LstSort_DragStart(object sender, EventArgs e)
        {
            var item = lstSort.FocusedItem;
            if (item == null)
            {
                return;
            }

            lstSort.DoDragDrop(lstSort.FocusedItem,  DragDropEffects.Move);
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCustomReport InstancePtr
		{
			get
			{
				return (frmCustomReport)Sys.GetInstance(typeof(frmCustomReport));
			}
		}

		protected frmCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ****************************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/16/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ****************************************************************************
		// ****************************************************************************
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomReport.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomReport.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomReport, "Births")
		// ****************************************************************************
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolPrintPreview;
		bool boolSaveReport;
		int SelectCol;
		int CodeCol;
		int DescriptionCol;
		bool blnCodesSelected;
		// vbPorter upgrade warning: dblRatio As double	OnWriteFCConvert.ToSingle(
		double dblRatio;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			clsDRWrapper rs = new clsDRWrapper();
			int intCounter;
			int intCounter2;
			if (cmbReport.SelectedIndex == 2)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DatabaseNamePath);
					LoadCombo();
					MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					lstSort.SetSelected(intCounter, false);
				}
				for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
				{
					for (intCounter2 = 1; intCounter2 <= 2; intCounter2++)
					{
						if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, intCounter, intCounter2) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
						{
							vsWhere.TextMatrix(intCounter, intCounter2, "");
						}
					}
				}
				vsLayout.Clear();
				vsLayout.Rows = 2;
				vsLayout.Cols = 1;
				rs.OpenRecordset("Select * from tblReportLayout where ReportID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)) + " Order by RowID, ColumnID", modGlobal.DatabaseNamePath);
				while (!rs.EndOfFile())
				{
					if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) == 1)
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int16("ColumnID")) > 0)
						{
							vsLayout.Cols += 1;
						}
					}
					else
					{
						if (FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")) >= vsLayout.Rows)
						{
							vsLayout.Rows += 1;
                            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
                            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
						}
					}
					vsLayout.TextMatrix(FCConvert.ToInt32(rs.Get_Fields_Int16("RowID")), FCConvert.ToInt32(rs.Get_Fields_Int16("ColumnID")), FCConvert.ToString(rs.Get_Fields_String("DisplayText")));
					// TODO Get_Fields: Check the table for the column [FieldID] and replace with corresponding Get_Field method
					vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, rs.Get_Fields_Int16("RowID"), rs.Get_Fields_Int16("ColumnID"), rs.Get_Fields("FieldID"));
					// TODO Get_Fields: Check the table for the column [Width] and replace with corresponding Get_Field method
					vsLayout.ColWidth(FCConvert.ToInt32(rs.Get_Fields_Int16("ColumnID")), FCConvert.ToInt32(rs.Get_Fields("Width")));
					rs.MoveNext();
				}
				rs.OpenRecordset("Select * from tblCustomReports where ID = " + FCConvert.ToString(this.cboSavedReport.ItemData(this.cboSavedReport.SelectedIndex)), modGlobal.DatabaseNamePath);
				if (!rs.EndOfFile())
				{
					this.txtHeading.Text = FCConvert.ToString(rs.Get_Fields_String("Title"));
					if (FCConvert.ToString(rs.Get_Fields_String("Orientation")) == "L")
					{
						cmbPortrait.SelectedIndex = 1;
					}
					else
					{
						cmbPortrait.SelectedIndex = 0;
					}
				}
			}
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY*** IMPORTANT
			int intReportID = 0;
			int intRow;
			int intCol;
			string strReturn;
			clsDRWrapper RSLayout = new clsDRWrapper();
			clsDRWrapper rsSave = new clsDRWrapper();
			string strOrientation = "";
			strReturn = Interaction.InputBox("Enter name for saved report", "New Custom Report", Strings.Trim(cboSavedReport.Text));
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				boolSaveReport = true;
				cmdPrint_Click();
				boolSaveReport = false;
				if (cmbPortrait.SelectedIndex == 0)
				{
					strOrientation = "P";
				}
				else
				{
					strOrientation = "L";
				}
				// SAVE THE REPORT
				rsSave.OpenRecordset("Select * from tblCustomReports where ReportName = '" + modGlobalRoutines.FixQuote(strReturn) + "' and type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DatabaseNamePath);
				bool executeNewReport = false;
				if (!rsSave.EndOfFile())
				{
					if (MessageBox.Show("A report by that name already exists. Save new formatted report?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						intReportID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
						rsSave.Execute("Delete from tblCustomReports where ID = " + FCConvert.ToString(intReportID), modGlobal.DatabaseNamePath);
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + FCConvert.ToString(intReportID), modGlobal.DatabaseNamePath);
						executeNewReport = true;
						goto NewReport;
					}
					else
					{
						MessageBox.Show("Report was not saved.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
				}
				else
				{
					executeNewReport = true;
					goto NewReport;
				}
				NewReport:
				;
				if (executeNewReport)
				{
					rsSave.Execute("Insert into tblCustomReports (ReportName,SQL,Type,Title,Orientation,LastUpdated) VALUES ('" + modGlobalRoutines.FixQuote(strReturn) + "','" + modCustomReport.FixQuotes(modCustomReport.Statics.strCustomSQL) + "','" + Strings.UCase(modCustomReport.Statics.strReportType) + "','" + modGlobalRoutines.FixQuote(Strings.Trim(this.txtHeading.Text)) + "','" + strOrientation + "','" + FCConvert.ToString(DateTime.Now) + "')", modGlobal.DatabaseNamePath);
					rsSave.OpenRecordset("Select max(ID) as ReportID from tblCustomReports", modGlobal.DatabaseNamePath);
					if (!rsSave.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
						rsSave.Execute("Delete from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobal.DatabaseNamePath);
						for (intRow = 1; intRow <= frmCustomReport.InstancePtr.vsLayout.Rows - 1; intRow++)
						{
							for (intCol = 0; intCol <= frmCustomReport.InstancePtr.vsLayout.Cols - 1; intCol++)
							{
								// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
								RSLayout.OpenRecordset("Select * from tblReportLayout where ReportID = " + rsSave.Get_Fields("ReportID"), modGlobal.DatabaseNamePath);
								RSLayout.AddNew();
								// TODO Get_Fields: Check the table for the column [ReportID] and replace with corresponding Get_Field method
								RSLayout.Set_Fields("ReportID", rsSave.Get_Fields("ReportID"));
								RSLayout.Set_Fields("RowID", intRow);
								RSLayout.Set_Fields("ColumnID", intCol);
								if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
								{
									RSLayout.Set_Fields("FieldID", -1);
								}
								else
								{
									RSLayout.Set_Fields("FieldID", FCConvert.ToString(Conversion.Val(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))));
								}
								RSLayout.Set_Fields("Width", frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol));
								RSLayout.Set_Fields("DisplayText", frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
								RSLayout.Update();
							}
						}
					}
					LoadCombo();
					MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			fraClassCodes.Visible = false;
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 1; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
			vsWhere.EditText = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(mnuExit, new System.EventArgs());
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			bool blnSelected;
			blnSelected = false;
			for (intCounter = 1; intCounter <= vsClassCodes.Rows - 1; intCounter++)
			{
				if (FCUtils.CBool(vsClassCodes.TextMatrix(intCounter, SelectCol)) == true)
				{
					blnSelected = true;
					break;
				}
			}
			if (!blnSelected)
			{
				MessageBox.Show("You must select at least one class before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			blnCodesSelected = true;
			fraClassCodes.Visible = false;
			cmdPrint_Click();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:AM:#i383 - always use preview
			boolPrintPreview = true;
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			clsDRWrapper rsSQL = new clsDRWrapper();
			vsLayout.Focus();
			if (blnCodesSelected == false && vsWhere.TextMatrix(20, 1) == "S")
			{
				fraClassCodes.Visible = true;
				return;
			}
			else
			{
				blnCodesSelected = false;
			}
			if (Strings.Trim(txtHeading.Text) == string.Empty)
			{
				modCustomReport.Statics.strReportHeading = "Custom Report";
			}
			else
			{
				modCustomReport.Statics.strReportHeading = Strings.Trim(txtHeading.Text);
			}
			// CLEAR THE FIELDS WIDTH ARRAY
			for (intCounter = 0; intCounter <= 49; intCounter++)
			{
				modCustomReport.Statics.strFieldWidth[intCounter] = 0;
			}
			bool executeNoFields = false;
			// PREPARE THE SQL TO SHOW THE REPORT
			if (cmbReport.SelectedIndex == 0)
			{
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				BuildSQL();
				// IF NO FIELDS WERE CHOSEN TO PRINT THEN DO NOT SHOW THE REPORT
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
				else if (modCustomReport.Statics.intNumberOfSQLFields == 9999)
				{
					MessageBox.Show("You must choose to display department if you wish to display division information.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			else
			{
				// IF THE USER DOES NOT CHOSE A REPORT THEN DO NOT SHOW ONE
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// GET THE SAVED SQL STATEMENT FOR THE CUSTOM REPORT
				rsSQL.OpenRecordset("Select SQL from tblCustomReports where ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modGlobal.DatabaseNamePath);
				if (!rsSQL.EndOfFile())
				{
					modCustomReport.Statics.strCustomSQL = FCConvert.ToString(rsSQL.Get_Fields_String("SQL"));
				}
			}
			// GET THE NUMBER OF FIELDS TO DISPLAY AND FILL THE FIELDS CAPTION
			// ARRAY WITH THE DATABASE FIELD NAMES FOR THOSE THAT WERE CHOSEN
			modCustomReport.GetNumberOfFields(modCustomReport.Statics.strCustomSQL);
			if (modGlobal.Statics.strPreSetReport == string.Empty)
			{
				if (modCustomReport.Statics.intNumberOfSQLFields < 0)
				{
					executeNoFields = true;
					goto NoFields;
				}
			}
			NoFields:
			;
			if (executeNoFields)
			{
				MessageBox.Show("No fields were selected to display.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			// GET THE CAPTIONS TO DISPLAY ON THE REPORT. THE DEVELOPER MAY
			// WANT THE DISPLAYED 'HEADINGS' TO BE DIFFERENT THEN THE DATABASE
			// FIELD NAMES OR THE CUSTOM REPORT FORM'S DISPLAY NAME
			// Call SetColumnCaptions(Me)
			if (!boolSaveReport)
			{
				//if (modGlobal.Statics.strPreSetReport == "Dog Reminders")
				//{
				//	// rptDogReminders.Show , MDIParent
				//}
				//else
				//{
					// SHOW THE REPORT
					if (boolPrintPreview)
					{
						frmReportViewer.InstancePtr.Init(rptCustomReport.InstancePtr);
					}
					else
					{
						modDuplexPrinting.DuplexPrintReport(rptCustomReport.InstancePtr);
					}
				//}
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		public void BuildSQL()
		{
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			int intCounter;
			int intRow;
			int intCol;
			string[] strSelectedFields = new string[500 + 1];
			bool blnSelectedDivision;
			bool blnSelectedDepartment;
			blnSelectedDepartment = false;
			blnSelectedDivision = false;
			vsWhere.Select(0, 0);
			// CLEAR OUT VARIABLES
			modCustomReport.Statics.intNumberOfSQLFields = 0;
			modCustomReport.Statics.strCustomSQL = string.Empty;
			// GET THE FIELD NAMES THAT THE USER HAS SELECTED
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				strSelectedFields[intCounter] = string.Empty;
			}
			for (intRow = 1; intRow <= vsLayout.Rows - 1; intRow++)
			{
				for (intCol = 0; intCol <= vsLayout.Cols - 1; intCol++)
				{
					for (intCounter = 0; intCounter <= 499; intCounter++)
					{
						if (strSelectedFields[intCounter] == string.Empty)
						{
							modCustomReport.Statics.intNumberOfSQLFields += 1;
							strSelectedFields[intCounter] = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol));
							break;
						}
						else if (strSelectedFields[intCounter] == vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol))
						{
							goto NextCell;
						}
					}
					NextCell:
					;
				}
			}
			for (intCounter = 0; intCounter <= 499; intCounter++)
			{
				if (strSelectedFields[intCounter] == string.Empty)
				{
					goto Continue;
				}
				else
				{
					for (intRow = 0; intRow <= lstFields.Items.Count - 1; intRow++)
					{
						if (lstFields.List(intRow) == strSelectedFields[intCounter])
						{
							if (modCustomReport.Statics.strCustomSQL != string.Empty)
								modCustomReport.Statics.strCustomSQL += ", ";
							if (modCustomReport.Statics.strFields[lstFields.ItemData(intRow)] == "TotalDepreciated")
							{
								modCustomReport.Statics.strCustomSQL += "(tblMaster.BasisCost - tblMaster.TotalValue) AS TotalDepreciated";
							}
							else
							{
								if (modCustomReport.Statics.strFields[lstFields.ItemData(intRow)] == "Dept")
								{
									blnSelectedDepartment = true;
								}
								else if (modCustomReport.Statics.strFields[lstFields.ItemData(intRow)] == "Div")
								{
									blnSelectedDivision = true;
								}
								modCustomReport.Statics.strCustomSQL += modCustomReport.Statics.strFields[lstFields.ItemData(intRow)];
							}
							break;
						}
					}
				}
			}
			Continue:
			;
			// CHECK TO SEE IF ANY FIELDS WERE SELCTED. IF THEY WERE THEN THE
			// STRCUSTOMSQL WOULD HAVE SOME DATA IN IT
			if (modGlobal.Statics.strPreSetReport == string.Empty)
			{
				if (Strings.Trim(modCustomReport.Statics.strCustomSQL) == string.Empty)
				{
					modCustomReport.Statics.intNumberOfSQLFields = -1;
					return;
				}
			}
			if (blnSelectedDivision && !blnSelectedDepartment)
			{
				modCustomReport.Statics.intNumberOfSQLFields = 9999;
				return;
			}
			// CREATE THE SQL STATEMENT WITH THE SELECTED FIELDS
			modCustomReport.Statics.strCustomSQL = "SELECT " + modCustomReport.Statics.strCustomSQL + " " + fraFields.Tag;
			// BUILD A WHERE CLAUSE TO APPEND TO THE SQL STATEMENT
			BuildWhereParameter();
			// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
			BuildSortParameter();
		}

		public void BuildSortParameter()
		{
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					if (modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)] == "VendorCode")
					{
						strSort += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1)) + ", ManualVendor";
					}
					else
					{
						strSort += FCConvert.ToString(modCustomReport.CheckForAS(modCustomReport.Statics.strFields[lstSort.ItemData(intCounter)], 1));
					}
				}
			}
			// IF THERE WERE SOME FIELDS TO SORT BY THEN APPEND THEM TO THE
			// PREVIOUSLY CREATED SQL STATEMENT
			if (Strings.Trim(strSort) != string.Empty)
			{
				modCustomReport.Statics.strCustomSQL += " Order by" + strSort;
			}
		}

		public void BuildWhereParameter()
		{
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			int intClassCounter;
			// CLEAR THE VARIABLES
			strWhere = " ";
			// GET THE FIELDS TO FILTER BY
			for (intCounter = 1; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				// NEED TO KNOW WHAT TYPE OF WHERE CLAUSE TO APPEND BY
				switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[intCounter]))
				{
					case modCustomReport.GRIDTEXT:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
							}
							break;
						}
					case modCustomReport.GRIDDATE:
						{
							// THIS IS A DATE FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS
							// THE POUND SYMBOLS WRAPPED AROUND IT
							// THE P
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									// THE USER HAS FILLED IN BOTH FIELDS FOR THIS DATE
									// FIELD SO WE NOW HAVE A DATE RANGE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter]))) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' and " + Strings.Trim(FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter]))) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									// ONE ONE FIELD WAS FILLED IN FOR THIS FIELD
									// SO WE HAVE THIS BEING AN EQUAL TO CLAUSE
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter]))) + " = '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "'";
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDTEXT:
						{
							// THIS IS OF A TYPE COMBO SO THE DATA IN THE DATABASE
							// IS THE ***ID*** NUMBER FROM ANOTHER TABLE
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " LIKE '" + vsWhere.TextMatrix(intCounter, 3) + "*'";
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUM:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (vsWhere.TextMatrix(intCounter, 0) == "Class Code")
								{
									if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) == "S")
									{
										if (strWhere != " ")
											strWhere += " AND ";
										strWhere += "(";
										for (intClassCounter = 1; intClassCounter <= vsClassCodes.Rows - 1; intClassCounter++)
										{
											if (FCUtils.CBool(vsClassCodes.TextMatrix(intClassCounter, SelectCol)) == true)
											{
												strWhere += "tblClassCodes.ID = " + vsClassCodes.RowData(intClassCounter) + " OR ";
											}
										}
										strWhere = Strings.Left(strWhere, strWhere.Length - 4) + ")";
									}
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " = " + vsWhere.TextMatrix(intCounter, 3);
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									// If Trim(strComboList(intCounter, 1)) = "Dept" Then
									// strWhere = strWhere & "val(" & Trim(strComboList(intCounter, 1)) & ") >= Val(" & vsWhere.TextMatrix(intCounter, 1) & ") AND Val(" & Trim(strComboList(intCounter, 1)) & ") <= Val(" & vsWhere.TextMatrix(intCounter, 2) & ")"
									// Else
									strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " >= '" + vsWhere.TextMatrix(intCounter, 1) + "' AND " + Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " <= '" + vsWhere.TextMatrix(intCounter, 2) + "'";
									// End If
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									// If Trim(strComboList(intCounter, 1)) = "Dept" Then
									// strWhere = strWhere & "Val(" & Trim(strComboList(intCounter, 1)) & ") = Val(" & vsWhere.TextMatrix(intCounter, 1) & ")"
									// Else
									strWhere += Strings.Trim(modCustomReport.Statics.strComboList[intCounter, 1]) + " = '" + vsWhere.TextMatrix(intCounter, 1) + "'";
									// End If
								}
							}
							break;
						}
					case modCustomReport.GRIDCOMBOTEXT:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (strWhere != " ")
									strWhere += " AND ";
								strWhere += Strings.Trim(FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter]))) + " LIKE '" + vsWhere.TextMatrix(intCounter, 1) + "*'";
							}
							break;
						}
					case modCustomReport.GRIDNUMRANGE:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " >= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + " AND " + FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " <= " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2);
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " = " + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1);
								}
							}
							break;
						}
					case modCustomReport.GRIDTEXTRANGE:
						{
							// THIS IS A TEXT FIELD IN THE DATABASE SO WE NEED TO
							// MAKE SURE THAT THIS PIECE OF THE WHERE CLAUSE HAS SINGLE
							// QUOTES AROUND IT
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " >= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "' AND " + FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " <= '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2) + "'";
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + " LIKE '" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1) + "*'";
								}
							}
							break;
						}
					case modCustomReport.GRIDBOOLEAN:
						{
							if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)) != string.Empty)
							{
								if (FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 2)) != string.Empty)
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + (Strings.Left(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == "T" ? " <> 0" : " = 0");
								}
								else
								{
									if (strWhere != " ")
										strWhere += " AND ";
									strWhere += FCConvert.ToString(modCustomReport.GetASFieldName(modCustomReport.Statics.strFields[intCounter])) + (Strings.Left(Strings.Trim(FCConvert.ToString(vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1))), 1) == "T" ? " <> 0" : " = 0");
								}
							}
							break;
						}
				}
				//end switch
			}
			// IF SOME WHERE PARAMETERS WHERE CHOSEN THEN APPEND THIS
			// TO THE SQL STATEMENT
			if (strWhere != " ")
			{
				modCustomReport.Statics.strCustomSQL += " WHERE" + strWhere;
			}
		}

		private void frmCustomReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F2:
					{
						lstFields.Focus();
						mnuAddRow_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F3:
					{
						lstFields.Focus();
						mnuAddColumn_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F4:
					{
						lstFields.Focus();
						mnuDeleteRow_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F5:
					{
						lstFields.Focus();
						mnuDeleteColumn_Click();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Escape:
					{
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomReport.ScaleWidth	= 9045;
			//frmCustomReport.ScaleHeight	= 7080;
			//frmCustomReport.LinkTopic	= "Form1";
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
			// OF THE MDI PARENT FORM
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			// SET THE PROPERTIES FOR THE REPORT LAYOUT SECTION
			Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			vsLayout.Rows = 2;
			vsLayout.Cols = 1;
			//Image1.WidthOriginal = 1440 * 14;
			//HScroll1.Width = Image1.Width;
			Image1.Image = ImageList1.Images[0];
			//HScroll1.Minimum = 0;
			//if (Image1.Width > Frame1.Width)
			//{
			//	HScroll1.Maximum = Image1.Width - Frame1.Width;
			//             //FC:FINAL:BBE:#329 - HScroll1 must not be wider than the frame1
			//             HScroll1.Width = Frame1.Width - 30;
			//         }
			//else
			//{
			//	HScroll1.Enabled = false;
			//}
			//vsLayout.WidthOriginal = Image1.WidthOriginal - 720;
			vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(1, true);
            vsLayout.Left = Image1.Left;
			//dblRatio = Line2.X2 / 1440;
			//Line1.X1 = FCConvert.ToSingle(((1440 * 7.5) * dblRatio));
			//Line1.X2 = Line1.X1;
			//Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;
			// fraLayout.Enabled = strPreSetReport = vbNullString
			// fraFields.Enabled = strPreSetReport = vbNullString
			if (modGlobal.Statics.strPreSetReport == string.Empty)
			{
				fraSort.Enabled = true;
				fraFields.Enabled = true;
				fraReports.Enabled = true;
				fraMessage.Visible = false;
				vsLayout.Visible = true;
			}
			else
			{
				fraSort.Enabled = false;
				fraFields.Enabled = false;
				fraReports.Visible = false;
				fraMessage.Visible = true;
				vsLayout.Visible = false;
			}
			SelectCol = 0;
			CodeCol = 1;
			DescriptionCol = 2;
			vsClassCodes.TextMatrix(0, SelectCol, "Select");
			vsClassCodes.TextMatrix(0, CodeCol, "Code");
			vsClassCodes.TextMatrix(0, DescriptionCol, "Description");
			vsClassCodes.ColWidth(SelectCol, FCConvert.ToInt32(vsClassCodes.WidthOriginal * 0.15));
			vsClassCodes.ColWidth(CodeCol, FCConvert.ToInt32(vsClassCodes.WidthOriginal * 0.15));
			vsClassCodes.ColWidth(DescriptionCol, FCConvert.ToInt32(vsClassCodes.WidthOriginal * 0.5));
			vsClassCodes.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsClassCodes.ColAlignment(CodeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			FillClassCodes();
			blnCodesSelected = false;
			cmbPortrait.SelectedIndex = 0;
			// 
			// fraNotes.Enabled = strPreSetReport = vbNullString
			// fraReports.Enabled = strPreSetReport = vbNullString
			// fraMessage.Visible = Not strPreSetReport = vbNullString
		}

		private void frmCustomReport_Resize(object sender, System.EventArgs e)
		{
			int lngTemp;
			//dblRatio = Line2.X2 / 1440;
			//Image1.WidthOriginal = FCConvert.ToInt32((14 * 1440) * dblRatio);
			if (cmbPortrait.SelectedIndex == 0)
			{
				optPortrait_Click();
			}
			else
			{
				optLandscape_Click();
			}
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			modGlobal.Statics.strPreSetReport = string.Empty;
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.Left = FCConvert.ToInt32(-HScroll1.Value + (30 * dblRatio));
			vsLayout.Left = Image1.Left;
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.Left = FCConvert.ToInt32(-HScroll1.Value + (30 * dblRatio));
			vsLayout.Left = Image1.Left;
		}

		public void LoadCombo()
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			rsReports.OpenRecordset("Select * from tblCustomReports where Type = '" + Strings.UCase(modCustomReport.Statics.strReportType) + "'", modGlobal.DatabaseNamePath);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}
		// Private Sub fraFields_DblClick()
		// Dim intCounter As Integer
		// For intCounter = 0 To lstFields.ListCount - 1
		// lstFields.Selected(intCounter) = True
		// Next
		// End Sub
		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			// FC: FINAL: VGE - #331 Flag to prevent double click on parrent panel
			if (this.lstSortHovered)
				return;
			int intCounter;
			int intCount = 0;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					intCount += 1;
				}
				lstSort.SetSelected(intCounter, true);
			}
			if (intCount == lstSort.Items.Count)
			{
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					lstSort.SetSelected(intCounter, false);
				}
			}
			if (lstSort.Items.Count > 0)
				lstSort.SelectedIndex = 0;
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			if (vsLayout.Row < 1)
				vsLayout.Row = 1;
			if (vsLayout.Col < 1)
				vsLayout.Col = 0;
			vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, lstFields.List(lstFields.ListIndex));
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, lstFields.ItemData(lstFields.SelectedIndex));
		}

		private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)((FCConvert.ToInt32(e.Button) / 0x100000));
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
			// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
			// WHERE TO SWAP THE TWO ITEMS.
			// 
			// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
			// WILL BE DISPLAYED ON THE REPORT ITSELF
			// intStart = lstFields.ListIndex
		}

		private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)((FCConvert.ToInt32(e.Button) / 0x100000));
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
			// ITEMS THAT ARE TO BE SWAPED
			// 
			// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
			// WILL BE DISPLAYED ON THE REPORT ITSELF
			// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
			// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
			// SAVE THE CAPTION AND ID FOR THE NEW ITEM
			// strtemp = lstFields.List(lstFields.ListIndex)
			// intID = lstFields.ItemData(lstFields.ListIndex)
			// 
			// CHANGE THE NEW ITEM
			// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
			// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
			// 
			// SAVE THE OLD ITEM
			// lstFields.List(intStart) = strtemp
			// lstFields.ItemData(intStart) = intID
			// 
			// SET BOTH ITEMS TO BE SELECTED
			// lstFields.Selected(lstFields.ListIndex) = True
			// lstFields.Selected(intStart) = True
			// End If
		}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			vsLayout.Focus();
			vsLayout.Cols += 1;
			vsLayout.Select(vsLayout.Row, vsLayout.Cols - 1);
			vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Cols - 1, -1);
		}

		public void mnuAddColumn_Click()
		{
			mnuAddColumn_Click(mnuAddColumn, new System.EventArgs());
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			vsLayout.Focus();
			vsLayout.Rows += 1;
            //FC:FINAL:AM:#i2291 - don't merge the row because the text in the columns won't be visible
            //vsLayout.MergeRow(vsLayout.Rows - 1, true);
			vsLayout.Select(vsLayout.Rows - 1, 0);
		}

		public void mnuAddRow_Click()
		{
			mnuAddRow_Click(mnuAddRow, new System.EventArgs());
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			vsWhere.Select(0, 0);
			cmdClear_Click();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Cols > 1)
			{
				if (vsLayout.Col >= 0)
				{
					vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
					vsLayout.Cols -= 1;
				}
				else
				{
					MessageBox.Show("You must select a column in a report before you may delete it.");
				}
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report.");
			}
		}

		public void mnuDeleteColumn_Click()
		{
			mnuDeleteColumn_Click(mnuDeleteColumn, new System.EventArgs());
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report.");
			}
		}

		public void mnuDeleteRow_Click()
		{
			mnuDeleteRow_Click(mnuDeleteRow, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			boolPrintPreview = false;
			cmdPrint_Click();
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			boolPrintPreview = true;
			cmdPrint_Click();
		}

		private void optLandscape_CheckedChanged(object sender, System.EventArgs e)
		{
			//HScroll1.Value = 0;
			//Application.DoEvents();
			// Line1.X1 = Image1.Left + (1440 * 10 * dblRatio) - (800 * dblRatio)
			//Line1.X1 = FCConvert.ToSingle(((1440 * 10)) * dblRatio);
			//Line1.X2 = Line1.X1;
			//Line1.Y2 = vsLayout.Top + vsLayout.Height;
		}

		public void optLandscape_Click()
		{
			optLandscape_CheckedChanged(cmbPortrait, new System.EventArgs());
		}

		private void optPortrait_CheckedChanged(object sender, System.EventArgs e)
		{
			//HScroll1.Value = 0;
			//Application.DoEvents();
			// Line1.X1 = Image1.Left + (1440 * 8 * dblRatio) - (800 * dblRatio)
			//Line1.X1 = FCConvert.ToSingle(((1440 * 7.5)) * dblRatio);
			//Line1.X2 = Line1.X1;
			//Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;
		}

		public void optPortrait_Click()
		{
			optPortrait_CheckedChanged(cmbPortrait, new System.EventArgs());
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// THIS WILL ENABLE/DISABLE THE FRAMES ACCORDING TO THE RADIO BUTTONS
			// ON THE TOP OF THE FORM.
			// 
			// IF THE USER IS SHOWING OR DELETING A PREVIOUSLY SAVED REPORT THEN
			// WE DO NOT WANT THE USER TO SELECT ANY PARAMETERS
			//FC:FINAL:DDU:#2657 - always show delete prompt when selecting a report to delete
			if ((cmbReport.Text == "Show Saved Report") || (cmbReport.Text == "Delete Saved Report"))
			{
				cboSavedReport.Visible = true;
				cboSavedReport.Text = "";
			}
			cmdAdd.Visible = Index == 0;
			fraSort.Enabled = Index == 0;
			fraFields.Enabled = Index == 0;
			fraWhere.Enabled = Index == 0;
			if (cboSavedReport.Visible)
			{
				if (cboSavedReport.Items.Count > 0)
				{
					cboSavedReport.SelectedIndex = 0;
				}
			}
			//FC:FINAL:DDU:#2138 - get prompt on select index change combobox to delete an item
			if (Index == 2)
			{
				cboSavedReport.Text = "";
			}
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void vsClassCodes_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsClassCodes.Row > 0)
			{
				if (FCUtils.CBool(vsClassCodes.TextMatrix(vsClassCodes.Row, SelectCol)) != true)
				{
					vsClassCodes.TextMatrix(vsClassCodes.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsClassCodes.TextMatrix(vsClassCodes.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsClassCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsClassCodes.Row > 0)
				{
					if (FCUtils.CBool(vsClassCodes.TextMatrix(vsClassCodes.Row, SelectCol)) != true)
					{
						vsClassCodes.TextMatrix(vsClassCodes.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsClassCodes.TextMatrix(vsClassCodes.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			clsDRWrapper rsDivInfo = new clsDRWrapper();
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[vsWhere.Row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
				case modCustomReport.GRIDCOMBOIDNUM:
				case modCustomReport.GRIDCOMBOIDNUMRANGE:
				case modCustomReport.GRIDCOMBOTEXT:
				case modCustomReport.GRIDBOOLEAN:
					{
						vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (modCustomReport.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
			{
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsWhere.ComboList = modCustomReport.Statics.strComboList[vsWhere.Row, 0];
			}
			if (vsWhere.Col == 2)
			{
				if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
				{
					vsWhere.Col = 1;
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			switch (FCConvert.ToInt32(modCustomReport.Statics.strWhereType[row]))
			{
				case modCustomReport.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modCustomReport.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(row, col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!modCustomReport.IsValidDate(vsWhere.EditText))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modCustomReport.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modCustomReport.Statics.strComboList[row, 0];
						break;
					}
			}
			//end switch
		}

		private void FillClassCodes()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM tblClassCodes ORDER BY Code", "TWFA0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsClassCodes.Rows += 1;
					vsClassCodes.TextMatrix(vsClassCodes.Rows - 1, SelectCol, FCConvert.ToString(false));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					vsClassCodes.TextMatrix(vsClassCodes.Rows - 1, CodeCol, FCConvert.ToString(rs.Get_Fields("Code")));
					vsClassCodes.TextMatrix(vsClassCodes.Rows - 1, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("Description")));
					vsClassCodes.RowData(vsClassCodes.Rows - 1, rs.Get_Fields_Int32("ID"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void cmdDeleteRow_Click(object sender, EventArgs e)
		{
			mnuDeleteRow_Click();
		}

		private void cmdDeleteColumn_Click(object sender, EventArgs e)
		{
			mnuDeleteColumn_Click();
		}

		private void cmdAddRow_Click(object sender, EventArgs e)
		{
			mnuAddRow_Click();
		}

		private void cmdAddColumn_Click(object sender, EventArgs e)
		{
			mnuAddColumn_Click();
		}
		//FC:FINAL:AM:#i382 - reimplement drag&drop
		private void lstSort_DragDrop(object sender, DragEventArgs e)
		{
            //int index = (e.DropTarget as ListViewItem).Index;
            //ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
            //if (data != null)
            //{
            //    this.lstSort.Items.Remove(data);
            //    this.lstSort.Items.Insert(index, data);
            //}

            var item = e.Data.GetData(typeof(ListViewItem)) as ListViewItem;
            if (item != null)
            {
                var listView = (ListView)sender;
                var target = e.DropTarget as ListViewItem;
                if (target == null)
                {
                   // listView.Items.Add(item);
                }
                else
                {
                    listView.Items.Insert(target.Index, item);
                }
                listView.FocusedItem = item;
            }
		}


		private void cmdPortrait_CheckedChanged(object sender, EventArgs e)
		{
			if (cmbPortrait.SelectedIndex == 0)
			{
				optPortrait_CheckedChanged(sender, e);
			}
			else if (cmbPortrait.SelectedIndex == 1)
			{
				optLandscape_CheckedChanged(sender, e);
			}
		}
		// FC:FINAL:VGE - #331 Flag to prevent double click on parrent panel

		#region #331
		private bool lstSortHovered;

		private void lstSort_MouseEnter(object sender, EventArgs e)
		{
			this.lstSortHovered = true;
		}

		private void lstSort_MouseLeave(object sender, EventArgs e)
		{
			this.lstSortHovered = false;
		}
		#endregion
	}
}
