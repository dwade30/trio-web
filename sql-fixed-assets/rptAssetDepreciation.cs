﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptAssetDepreciation.
	/// </summary>
	public partial class rptAssetDepreciation : BaseSectionReport
	{
		public static rptAssetDepreciation InstancePtr
		{
			get
			{
				return (rptAssetDepreciation)Sys.GetInstance(typeof(rptAssetDepreciation));
			}
		}

		protected rptAssetDepreciation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            //FC:FINAL:AM:#4527 - close the form 
            frmSetupAssetDepreciation.InstancePtr.Close();
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAssetDepreciation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsInfo1 = new clsDRWrapper();
		bool blnFirstRecord;
		// vbPorter upgrade warning: curTotalCost As Decimal	OnWrite(short, Decimal)
		Decimal curTotalCost;
		// vbPorter upgrade warning: curTotalDepreciated As Decimal	OnWrite(short, Decimal)
		Decimal curTotalDepreciated;
		// vbPorter upgrade warning: curTotalTotalDepreciated As Decimal	OnWrite(short, Decimal)
		Decimal curTotalTotalDepreciated;
		// vbPorter upgrade warning: curTotalPreviousDepreciation As Decimal	OnWrite(short, Decimal)
		Decimal curTotalPreviousDepreciation;
		// vbPorter upgrade warning: curTotalNetValue As Decimal	OnWrite(short, Decimal)
		Decimal curTotalNetValue;
		// vbPorter upgrade warning: datLowDepreciationDate As DateTime	OnWrite(string)
		DateTime datLowDepreciationDate;
		// vbPorter upgrade warning: datHighDepreciationDate As DateTime	OnWrite(string)
		DateTime datHighDepreciationDate;

		public rptAssetDepreciation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Asset Depreciation Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			string strTotalDep = "";
			string strWhereDep = "";
			int counter;
			string strOpenRS = "";
			string strPurchaseDisacrdDateSQL = "";
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 14000 / 1440F;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			Label2.Text = "TRIO Software Corp";
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			curTotalCost = 0;
			curTotalDepreciated = 0;
			curTotalTotalDepreciated = 0;
			curTotalPreviousDepreciation = 0;
			curTotalNetValue = 0;
			blnFirstRecord = true;
			lblDateRange.Text = "Depreciation Dates: " + frmSetupAssetDepreciation.InstancePtr.txtStartDate.Text + " To " + frmSetupAssetDepreciation.InstancePtr.txtEndDate.Text;
			datLowDepreciationDate = FCConvert.ToDateTime(frmSetupAssetDepreciation.InstancePtr.txtStartDate.Text);
			datHighDepreciationDate = FCConvert.ToDateTime(frmSetupAssetDepreciation.InstancePtr.txtEndDate.Text);
			lblPurchaseDate.Text = "Pur. Date";
			if (frmSetupAssetDepreciation.InstancePtr.chkZeroCurDep.CheckState == Wisej.Web.CheckState.Checked && frmSetupAssetDepreciation.InstancePtr.chkDep.CheckState == Wisej.Web.CheckState.Checked)
			{
				strWhereDep = " or  (tblMaster.BasisCost = tblMaster.BasisCost) or  (tblMaster.BasisCost - convert(money, tblMaster.TotalDepreciated) = tblMaster.SalvageValue) ";
			}
			else if (frmSetupAssetDepreciation.InstancePtr.chkZeroCurDep.CheckState == Wisej.Web.CheckState.Checked)
			{
				strWhereDep = " or  (tblMaster.BasisCost = tblMaster.BasisCost) ";
			}
			else if (frmSetupAssetDepreciation.InstancePtr.chkDep.CheckState == Wisej.Web.CheckState.Checked)
			{
				strWhereDep = " or  (tblMaster.BasisCost - convert(int, tblMaster.TotalDepreciated) = tblMaster.SalvageValue) ";
			}
			else
			{
				strWhereDep = " ";
			}
			if (frmSetupAssetDepreciation.InstancePtr.chkIncludeDiscarded.CheckState != Wisej.Web.CheckState.Checked)
			{
				strPurchaseDisacrdDateSQL = "AND Discarded = 0 ";
			}
			rsInfo.OpenRecordset("SELECT SUM(tblDepreciation.DepreciationAmount) as DepTotal, tblMaster.Description as Description, tblMaster.Dept as Dept, tblMaster.ID as ID, tblClassCodes.Code as ClassCode, tblMaster.TagNumber as Tag, tblMaster.TotalDepreciated as TotalDepreciated, tblMaster.BasisCost as BasisCost, tblMaster.PurchaseDate as PurchaseDate, tblMaster.DiscardedDate as DiscardedDate, tblMaster.TotalValue as TotalValue FROM (tblMaster LEFT JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID) INNER JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID WHERE" + " (tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'" + strWhereDep + ")" + strPurchaseDisacrdDateSQL + " GROUP BY tblMaster.ID, tblMaster.TagNumber, tblMaster.Description, tblMaster.Dept, tblClassCodes.Code, tblMaster.TotalDepreciated, tblMaster.BasisCost, tblMaster.PurchaseDate, tblMaster.DiscardedDate, tblMaster.TotalValue ORDER BY tblMaster.Description", "TWFA0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			rsInfo1.OpenRecordset("Select SUM(tblDepreciation.DepreciationAmount) as DepTotal from tblDepreciation Where ItemId=" + rsInfo.Get_Fields_Int32("ID") + "AND tblDepreciation.DepreciationDate >= ' " + FCConvert.ToString(datLowDepreciationDate) + "' AND tblDepreciation.DepreciationDate <= ' " + FCConvert.ToString(datHighDepreciationDate) + "'", "TWFA0000.vb1");
			fldDescription.Text = rsInfo.Get_Fields_String("Description");
			// TODO Get_Fields: Field [Tag] not found!! (maybe it is an alias?)
			fldTag.Text = rsInfo.Get_Fields_String("Tag");
			fldClass.Text = rsInfo.Get_Fields_String("ClassCode");
			fldPurchaseDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy");
			fldDept.Text = rsInfo.Get_Fields_String("Dept");
			fldCost.Text = Strings.Format(rsInfo.Get_Fields_Decimal("BasisCost"), "#,##0.00");
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			fldPreviousDepreciation.Text = Strings.Format(rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue")) - rsInfo1.Get_Fields("DepTotal"), "#,##0.00");
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			fldDepreciation.Text = Strings.Format(Conversion.Val(rsInfo1.Get_Fields("DepTotal")), "#,##0.00");
			fldTotalDepreciated.Text = Strings.Format((rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue"))), "#,##0.00");
			fldNetValue.Text = Strings.Format(rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(fldTotalDepreciated.Text), "#,##0.00");
			curTotalCost += rsInfo.Get_Fields_Decimal("BasisCost");
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			curTotalDepreciated += FCConvert.ToDecimal(rsInfo1.Get_Fields("DepTotal"));
			// TODO Get_Fields: Field [DepTotal] not found!! (maybe it is an alias?)
			curTotalPreviousDepreciation += (rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue")) - rsInfo1.Get_Fields("DepTotal"));
			curTotalTotalDepreciated += (rsInfo.Get_Fields_Decimal("BasisCost") - FCConvert.ToDecimal(rsInfo.Get_Fields_Double("TotalValue")));
			curTotalNetValue += FCConvert.ToDecimal(fldNetValue.Text);
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalCost.Text = Strings.Format(curTotalCost, "#,##0.00");
			fldTotalPreviousDepreciation.Text = Strings.Format(curTotalPreviousDepreciation, "#,##0.00");
			fldTotalDepreciation.Text = Strings.Format(curTotalDepreciated, "#,##0.00");
			fldTotalTotalDepreciated.Text = Strings.Format(curTotalTotalDepreciated, "#,##0.00");
			fldTotalNetValue.Text = Strings.Format(curTotalNetValue, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber.ToString();
		}

		private void rptAssetDepreciation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAssetDepreciation.Caption	= "Asset Depreciation Report";
			//rptAssetDepreciation.Icon	= "rptAssetDepreciation.dsx":0000";
			//rptAssetDepreciation.Left	= 0;
			//rptAssetDepreciation.Top	= 0;
			//rptAssetDepreciation.Width	= 11880;
			//rptAssetDepreciation.Height	= 8595;
			//rptAssetDepreciation.StartUpPosition	= 3;
			//rptAssetDepreciation.SectionData	= "rptAssetDepreciation.dsx":508A;
			//End Unmaped Properties
		}

		private void rptAssetDepreciation_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
		}
	}
}
