﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using TWSharedLibrary;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptDepreciation.
	/// </summary>
	public partial class rptDepreciation : BaseSectionReport
	{
		public static rptDepreciation InstancePtr
		{
			get
			{
				return (rptDepreciation)Sys.GetInstance(typeof(rptDepreciation));
			}
		}

		protected rptDepreciation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDepreciation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsData = new clsDRWrapper();
		int intPageNumber;
		Decimal curAmountTotal;
		// vbPorter upgrade warning: lngUnitsTotal As int	OnWriteFCConvert.ToDouble(
		int lngUnitsTotal;
		int intRecordTotal;
		bool boolSummary;
		bool blnReturnToMaster;

		public rptDepreciation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Depreciation Report";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int intRow;
			int intCol;
			int intControl;
			try
			{
				// On Error GoTo ErrorHandler
				// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
				if (rsData.EndOfFile())
				{
					txtAmountSum.Text = Strings.Format(curAmountTotal, "0.00");
					txtUnitsTotal.Text = Strings.Format(lngUnitsTotal, "0.00");
					lblRecordNumber.Text = "Item(s) Depreciated: " + FCConvert.ToString(intRecordTotal);
					boolSummary = true;
					eArgs.EOF = true;
					return;
				}
				txtDescription.Text = rsData.Get_Fields_String("Description");
				// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
				txtYear.Text = rsData.Get_Fields_String("Year");
				txtMake.Text = rsData.Get_Fields_String("Make");
				txtModel.Text = rsData.Get_Fields_String("Model");
				txtAmount.Text = Strings.Format(rsData.Get_Fields_Decimal("DepreciationAmount"), "0.00");
				fldTag.Text = rsData.Get_Fields_String("TagNumber");
				txtUnits.Text = Strings.Format(rsData.Get_Fields_String("DepreciationUnits"), "0.00");
				txtUnitsDesc.Text = rsData.Get_Fields_String("DepreciationDesc");
				curAmountTotal += rsData.Get_Fields_Decimal("DepreciationAmount");
				lngUnitsTotal += FCConvert.ToInt32(Conversion.Val(txtUnits.Text));
				intRecordTotal += 1;
				rsData.MoveNext();
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Unable to build custom report. Close custom report screen and start again.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
			if (boolSummary)
			{
				Field1.Visible = false;
				Field2.Visible = false;
				Field3.Visible = false;
				Field4.Visible = false;
				Field5.Visible = false;
				Field6.Visible = false;
				Field7.Visible = true;
				Field8.Visible = true;
				Field9.Visible = true;
				// txtDepreciationDate.Visible = True
				txtTitle.Text = "Fixed Assets - Accounting Summary";
			}
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//srptAccountingSummary.InstancePtr.Hide();
			SubReport1.Report = null;
			if (frmDepreciation.InstancePtr.blnSingleDepreciation)
			{
				blnReturnToMaster = true;
			}
			if (frmDepreciation.InstancePtr.blnPreview == false)
			{
				frmDepreciation.InstancePtr.Unload();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			if (frmDepreciation.InstancePtr.blnPreview)
			{
				txtTitle.Text = "Fixed Asset - Depreciation List (PREVIEW)";
			}
			else
			{
				txtTitle.Text = "Fixed Asset - Depreciation List";
			}
			SubReport1.Report = new srptAccountingSummary();
            //FC:FINAL:AM:#2125 - moved code from ReportFooter_Format
            subCostSummary.Report = new srptOriginalCostSummary();
            txtJournalNumber.Text = FCConvert.ToString(modVariables.Statics.glngJournalNumber) + " - " + modVariables.Statics.gdatDepreciationDate + " - Depreciation";
			txtPeriod.Text = Strings.Format(modVariables.Statics.gintPeriodNumber, "00") + " - " + modGlobalRoutines.GetDateName(modVariables.Statics.gintPeriodNumber);
			txtDepreciationDate.Text = modVariables.Statics.gdatDepreciationDate;
			blnReturnToMaster = false;
			if (frmDepreciation.InstancePtr.blnPreview == true)
			{
				rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where Pending = 1 ORDER BY tblMaster.Description", modGlobal.DatabaseNamePath);
			}
			else
			{
				rsData.OpenRecordset("Select tblMaster.*,tblDepreciation.* from tblMaster INNER JOIN tblDepreciation ON tblMaster.ID = tblDepreciation.ItemID where ReportID = " + FCConvert.ToString(modVariables.Statics.glngDepReportID) + " ORDER BY tblMaster.Description", modGlobal.DatabaseNamePath);
			}
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (blnReturnToMaster)
			{
				frmMaster.InstancePtr.Show(App.MainForm);
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void ReportFooter_Format(object sender, EventArgs e)
		{
            //FC:FINAL:AM:#2125 - moved code to ReportStart
            //subCostSummary.Report = new srptOriginalCostSummary();
        }

        private void rptDepreciation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptDepreciation.Caption	= "Depreciation Report";
			//rptDepreciation.Icon	= "rptDepreciation.dsx":0000";
			//rptDepreciation.Left	= 0;
			//rptDepreciation.Top	= 0;
			//rptDepreciation.Width	= 11880;
			//rptDepreciation.Height	= 8595;
			//rptDepreciation.StartUpPosition	= 3;
			//rptDepreciation.SectionData	= "rptDepreciation.dsx":058A;
			//End Unmaped Properties
		}
	}
}
