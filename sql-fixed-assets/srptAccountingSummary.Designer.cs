﻿namespace TWFA0000
{
	/// <summary>
	/// Summary description for srptAccountingSummary.
	/// </summary>
	partial class srptAccountingSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptAccountingSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDepreciationDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtJournalNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepreciationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournalNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			//this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtAccount,
				this.txtAmount,
				this.txtDescription
			});
			this.Detail.Height = 0.2083333F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtDate,
				this.txtPage,
				this.txtMuniName,
				this.txtTime,
				this.txtDepreciationDate
			});
			this.PageHeader.Height = 0.96875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.Field2,
				this.txtJournalNumber,
				this.txtPeriod
			});
			this.PageFooter.Height = 0.5F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 0.0625F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 6.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 4.125F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 2.1875F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 5.1875F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtMuniName.Text = null;
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.0625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// txtDepreciationDate
			// 
			this.txtDepreciationDate.Height = 0.2083333F;
			this.txtDepreciationDate.Left = 2.3125F;
			this.txtDepreciationDate.MultiLine = false;
			this.txtDepreciationDate.Name = "txtDepreciationDate";
			this.txtDepreciationDate.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtDepreciationDate.Text = null;
			this.txtDepreciationDate.Top = 0.3333333F;
			this.txtDepreciationDate.Width = 1.84375F;
			// 
			// txtAccount
			// 
			this.txtAccount.Height = 0.2083333F;
			this.txtAccount.Left = 0.09375F;
			this.txtAccount.MultiLine = false;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtAccount.Text = null;
			this.txtAccount.Top = 0F;
			this.txtAccount.Width = 1.375F;
			// 
			// txtAmount
			// 
			this.txtAmount.Height = 0.2083333F;
			this.txtAmount.Left = 1.59375F;
			this.txtAmount.MultiLine = false;
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right";
			this.txtAmount.Text = null;
			this.txtAmount.Top = 0F;
			this.txtAmount.Width = 1.375F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.2083333F;
			this.txtDescription.Left = 3.15625F;
			this.txtDescription.MultiLine = false;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 3.3125F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.2083333F;
			this.Field1.Left = 2.3125F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.Field1.Text = "Journal Number:";
			this.Field1.Top = 0.04166667F;
			this.Field1.Width = 0.875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.2083333F;
			this.Field2.Left = 2.21875F;
			this.Field2.MultiLine = false;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.Field2.Text = "Accounting Period:";
			this.Field2.Top = 0.25F;
			this.Field2.Width = 0.96875F;
			// 
			// txtJournalNumber
			// 
			this.txtJournalNumber.Height = 0.2083333F;
			this.txtJournalNumber.Left = 3.1875F;
			this.txtJournalNumber.MultiLine = false;
			this.txtJournalNumber.Name = "txtJournalNumber";
			this.txtJournalNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtJournalNumber.Text = null;
			this.txtJournalNumber.Top = 0.04166667F;
			this.txtJournalNumber.Width = 2.125F;
			// 
			// txtPeriod
			// 
			this.txtPeriod.Height = 0.2083333F;
			this.txtPeriod.Left = 3.1875F;
			this.txtPeriod.MultiLine = false;
			this.txtPeriod.Name = "txtPeriod";
			this.txtPeriod.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtPeriod.Text = null;
			this.txtPeriod.Top = 0.25F;
			this.txtPeriod.Width = 1.0625F;
			// 
			// srptAccountingSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDepreciationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtJournalNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDepreciationDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtJournalNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPeriod;
	}
}
