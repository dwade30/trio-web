//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmDeptDiv.
	/// </summary>
	partial class frmDeptDiv : BaseForm
	{
		public fecherFoundation.FCGrid DeptDivGrid;
		public fecherFoundation.FCLabel lblBudLabel;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.DeptDivGrid = new fecherFoundation.FCGrid();
			this.lblBudLabel = new fecherFoundation.FCLabel();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.DeptDivGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 399);
			this.BottomPanel.Size = new System.Drawing.Size(658, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.DeptDivGrid);
			this.ClientArea.Controls.Add(this.lblBudLabel);
			this.ClientArea.Size = new System.Drawing.Size(658, 339);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(658, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(198, 30);
			this.HeaderText.Text = "Dept / Div Codes";
			// 
			// DeptDivGrid
			// 
			this.DeptDivGrid.AllowSelection = false;
			this.DeptDivGrid.AllowUserToResizeColumns = false;
			this.DeptDivGrid.AllowUserToResizeRows = false;
			this.DeptDivGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.DeptDivGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.DeptDivGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.DeptDivGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.DeptDivGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.DeptDivGrid.BackColorSel = System.Drawing.Color.Empty;
			this.DeptDivGrid.Cols = 10;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.DeptDivGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.DeptDivGrid.ColumnHeadersHeight = 30;
			this.DeptDivGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.DeptDivGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.DeptDivGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.DeptDivGrid.ExtendLastCol = true;
			this.DeptDivGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.DeptDivGrid.FrozenCols = 0;
			this.DeptDivGrid.GridColor = System.Drawing.Color.Empty;
			this.DeptDivGrid.Location = new System.Drawing.Point(30, 60);
			this.DeptDivGrid.Name = "DeptDivGrid";
			this.DeptDivGrid.ReadOnly = true;
			this.DeptDivGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.DeptDivGrid.RowHeightMin = 0;
			this.DeptDivGrid.Rows = 50;
			this.DeptDivGrid.ShowColumnVisibilityMenu = false;
			this.DeptDivGrid.Size = new System.Drawing.Size(598, 254);
			this.DeptDivGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.DeptDivGrid.TabIndex = 1;
			this.DeptDivGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.DeptDivGrid_KeyPressEdit);
			this.DeptDivGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.DeptDivGrid_BeforeEdit);
			this.DeptDivGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.DeptDivGrid_AfterEdit);
			this.DeptDivGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.DeptDivGrid_ValidateEdit);
			this.DeptDivGrid.KeyPress += new Wisej.Web.KeyPressEventHandler(this.DeptDivGrid_KeyPressEvent);
			this.DeptDivGrid.Click += new System.EventHandler(this.DeptDivGrid_ClickEvent);
			// 
			// lblBudLabel
			// 
			this.lblBudLabel.Location = new System.Drawing.Point(30, 30);
			this.lblBudLabel.Name = "lblBudLabel";
			this.lblBudLabel.Size = new System.Drawing.Size(207, 19);
			this.lblBudLabel.TabIndex = 0;
			this.lblBudLabel.Text = "BUDGETARY VALUES - READ ONLY";
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(571, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.cmdDelete.Size = new System.Drawing.Size(57, 24);
			this.cmdDelete.TabIndex = 13;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(522, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdNew.Size = new System.Drawing.Size(45, 24);
			this.cmdNew.TabIndex = 15;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(291, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 4;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuNew,
				this.mnuDelete,
				this.mnuSP2,
				this.mnuSave,
				this.mnuSaveExit,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 2;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                      ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 4;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 5;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmDeptDiv
			// 
			this.ClientSize = new System.Drawing.Size(658, 495);
			this.KeyPreview = true;
			this.Name = "frmDeptDiv";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Dept / Div Codes";
			this.Load += new System.EventHandler(this.frmDeptDiv_Load);
			this.Activated += new System.EventHandler(this.frmDeptDiv_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDeptDiv_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDeptDiv_KeyPress);
			this.Resize += new System.EventHandler(this.frmDeptDiv_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.DeptDivGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdDelete;
		private FCButton cmdNew;
		private FCButton cmdSave;
	}
}