﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSearch.
	/// </summary>
	public partial class frmSearch : BaseForm
	{
		public frmSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.cmdSch = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			this.cmdSch.AddControlArrayElement(cmdSch_1, 1);
			this.cmdSch.AddControlArrayElement(cmdSch_3, 3);
			this.cmdSch.AddControlArrayElement(cmdSch_0, 0);
			this.cmdSch.AddControlArrayElement(cmdSch_4, 4);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSearch InstancePtr
		{
			get
			{
				return (frmSearch)Sys.GetInstance(typeof(frmSearch));
			}
		}

		protected frmSearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   11/27/2002                          *
		// *
		// MODIFIED BY    :   David Wade                                    *
		// Last Updated   :   02/25/2003                                    *
		// ********************************************************
		string strSort = "";
		bool boolSearchOnKeyPress;
		string[,] LocationCodes1;
		string[,] LocationCodes2;
		string[,] LocationCodes3;
		string[,] LocationCodes4;
		int lngArraySize1;
		int lngArraySize2;
		int lngArraySize3;
		int lngArraySize4;
		int intAutoIDCol;
		int intDescriptionCol;
		int intYearCol;
		int intMakeCol;
		int intModelCol;
		int intSerialCol;
		int intTagCol;
		int intDeptCol;
		int intDivCol;
		// grid column variables
		int intLoc1CodeCol;
		int intLoc1DescCol;
		int intLoc2CodeCol;
		int intLoc2DescCol;
		int intLoc3CodeCol;
		int intLoc3DescCol;
		int intLoc4CodeCol;
		int intLoc4DescCol;
		bool blnDivUsed;
		// should the division be used in this screen
		bool blnUsedBudgetaryDeptDiv;
		const string GRIDCOLUMN0 = "ID";
		const string GRIDCOLUMN1 = "Description";
		const string GRIDCOLUMN2 = "Year";
		const string GRIDCOLUMN3 = "Make";
		const string GRIDCOLUMN4 = "Model";
		const string GRIDCOLUMN5 = "Serial #";
		const string GRIDCOLUMN6 = "Tag #";
		const string GRIDCOLUMN7 = "Dept #";
		const string GRIDCOLUMN8 = "Div #";
		const string GRIDCOLUMN9 = "Loc 1 Code";
		const string GRIDCOLUMN10 = "Loc 1 Description";
		const string GRIDCOLUMN11 = "Loc 2 Code";
		const string GRIDCOLUMN12 = "Loc 2 Description";
		const string GRIDCOLUMN13 = "Loc 3 Code";
		const string GRIDCOLUMN14 = "Loc 3 Description";
		const string GRIDCOLUMN15 = "Loc 4 Code";
		const string GRIDCOLUMN16 = "Loc 4 Description";

		private void SetGridProperties()
		{
			SearchGrid.FixedCols = 0;
			//FC:FINAL:AM:#448 - set the FrozenRows property instead
			//SearchGrid.FixedRows = 2;
			SearchGrid.Rows = 2;
			SearchGrid.FrozenRows = 2;
			SearchGrid.Cols = 17;
			SearchGrid.ColHidden(intAutoIDCol, true);
			if ((blnUsedBudgetaryDeptDiv && modAccountTitle.Statics.ExpDivFlag) || !blnDivUsed)
			{
				SearchGrid.ColHidden(intDivCol, true);
			}
			else
			{
				SearchGrid.ColHidden(intDivCol, false);
			}
			SearchGrid.ColWidth(intAutoIDCol, 400);
			SearchGrid.ColWidth(intDescriptionCol, 4500);
			SearchGrid.ColWidth(intYearCol, 1000);
			SearchGrid.ColWidth(intMakeCol, 2000);
			SearchGrid.ColWidth(intModelCol, 2480);
			SearchGrid.ColWidth(intSerialCol, 2500);
			SearchGrid.ColWidth(intTagCol, 1500);
			SearchGrid.ColWidth(intDeptCol, 1000);
			SearchGrid.ColWidth(intDivCol, 1000);
			SearchGrid.ColWidth(intLoc1CodeCol, 1500);
			SearchGrid.ColWidth(intLoc1DescCol, 2000);
			SearchGrid.ColWidth(intLoc2CodeCol, 1500);
			SearchGrid.ColWidth(intLoc2DescCol, 2000);
			SearchGrid.ColWidth(intLoc3CodeCol, 1500);
			SearchGrid.ColWidth(intLoc3DescCol, 2000);
			SearchGrid.ColWidth(intLoc4CodeCol, 1500);
			SearchGrid.ColWidth(intLoc4DescCol, 2000);
			SearchGrid.RowHeight(0, txtColumn1.HeightOriginal + 60);
			SearchGrid.RowHeight(1, txtColumn1.HeightOriginal + 60);
			//SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, SearchGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// 
			SearchGrid.TextMatrix(0, intAutoIDCol, GRIDCOLUMN0);
			SearchGrid.TextMatrix(0, intDescriptionCol, GRIDCOLUMN1);
			SearchGrid.TextMatrix(0, intYearCol, GRIDCOLUMN2);
			SearchGrid.TextMatrix(0, intMakeCol, GRIDCOLUMN3);
			SearchGrid.TextMatrix(0, intModelCol, GRIDCOLUMN4);
			SearchGrid.TextMatrix(0, intSerialCol, GRIDCOLUMN5);
			SearchGrid.TextMatrix(0, intTagCol, GRIDCOLUMN6);
			SearchGrid.TextMatrix(0, intDeptCol, GRIDCOLUMN7);
			SearchGrid.TextMatrix(0, intDivCol, GRIDCOLUMN8);
			SearchGrid.TextMatrix(0, intLoc1CodeCol, GRIDCOLUMN9);
			SearchGrid.TextMatrix(0, intLoc1DescCol, GRIDCOLUMN10);
			SearchGrid.TextMatrix(0, intLoc2CodeCol, GRIDCOLUMN11);
			SearchGrid.TextMatrix(0, intLoc2DescCol, GRIDCOLUMN12);
			SearchGrid.TextMatrix(0, intLoc3CodeCol, GRIDCOLUMN13);
			SearchGrid.TextMatrix(0, intLoc3DescCol, GRIDCOLUMN14);
			SearchGrid.TextMatrix(0, intLoc4CodeCol, GRIDCOLUMN15);
			SearchGrid.TextMatrix(0, intLoc4DescCol, GRIDCOLUMN16);
			SetColumns();
			SearchGrid.Select(0, 0);
			SearchGrid.FrozenCols = 2;
		}

		private void SetColumns()
		{
			// SET THE SEARCH TEXT BOXES TO BE ALIGNED WITH THE COLUMNS IN THE GRID
			SetColumnSearchBox(ref txtColumn1, 1);
			SetColumnSearchBox(ref txtColumn2, 2);
			SetColumnSearchBox(ref txtColumn3, 3);
			SetColumnSearchBox(ref txtColumn4, 4);
			SetColumnSearchBox(ref txtColumn5, 5);
			SetColumnSearchBox(ref txtColumn6, 6);
			SetColumnSearchBox(ref txtColumn7, 7);
			if (modVariables.Statics.boolUseDivision)
			{
				SetColumnSearchBox(ref txtColumn8, 8);
			}
			else
			{
				txtColumn8.Visible = false;
			}
			SetColumnSearchBox(ref txtColumn9, 9);
			SetColumnSearchBox(ref txtColumn10, 10);
			SetColumnSearchBox(ref txtColumn11, 11);
			SetColumnSearchBox(ref txtColumn12, 12);
			SetColumnSearchBox(ref txtColumn13, 13);
			SetColumnSearchBox(ref txtColumn14, 14);
			SetColumnSearchBox(ref txtColumn15, 15);
			SetColumnSearchBox(ref txtColumn16, 16);
		}

		private void SetColumnSearchBox(ref FCTextBox ControlName, int ColumnNumber)
		{
			try
			{
				// On Error GoTo ErrorHandler
				ControlName.LeftOriginal = FCConvert.ToInt32(SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpLeft, 1, ColumnNumber)) + SearchGrid.LeftOriginal + 40 / FCScreen.TwipsPerPixelX;
				ControlName.TopOriginal = FCConvert.ToInt32(SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpTop, 1, ColumnNumber)) + SearchGrid.TopOriginal + 85 / FCScreen.TwipsPerPixelY;
				if (FCConvert.ToInt32(SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpWidth, 1, ColumnNumber)) != 0)
				{
					ControlName.WidthOriginal = FCConvert.ToInt32(SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpWidth, 1, ColumnNumber)) - 40 / FCScreen.TwipsPerPixelX;
				}
				// DO NOT WANT TO HIDE THE FROZEN COLUMN
				if (ColumnNumber > 1)
				{
                    //ControlName.Visible = ColumnNumber >= SearchGrid.LeftCol;
                    ControlName.Visible = ControlName.LeftOriginal > SearchGrid.ColWidth(1);

                }
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 381)
				{
					return;
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
				}
			}
		}

		public void cmdSch_Click_2(short Index)
		{
			cmdSch_Click(Index);
		}

		public void cmdSch_Click(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						Close();
						break;
					}
				case 1:
					{
						SearchBth();
						break;
					}
				case 2:
					{
						break;
					}
				case 3:
					{
						clrForm();
						break;
					}
				case 4:
					{
						SearchGrid_DblClick();
						break;
					}
			}
			//end switch
		}

		public void cmdSch_Click(short Index)
		{
			cmdSch_Click(Index, cmdSch[Index], new System.EventArgs());
		}

		private void cmdSch_Click(object sender, System.EventArgs e)
		{
			short index = cmdSch.GetIndex((FCButton)sender);
			cmdSch_Click(index, sender, e);
		}

		private void frmSearch_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
			txtColumn1.Focus();
		}

		private void frmSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
				Close();
		}

		private void frmSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			if (KeyAscii == Keys.Return)
			{
				// ENTER KEY WAS PRESSED
				if (SearchGrid.Row >= 2)
				{
					SearchGrid_DblClick();
				}
				else if (!boolSearchOnKeyPress)
				{
					mnuSearch_Click();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSearch.ScaleWidth	= 9045;
			//frmSearch.ScaleHeight	= 7905;
			//frmSearch.LinkTopic	= "Form1";
			//End Unmaped Properties
			/* object obj; */
			int intCount;
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();

            try
            { 
			//FC:FINAL:MSH - in VB6 control contain all controls, include child controls in each control(same with i.issue #1649)
			//foreach (Control obj in this.Controls)
			var formControls = this.GetAllControls();
			foreach (Control obj in formControls)
			{
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "##/##/####";
					(obj as MaskedTextBox).SelectionStart = 0;
				}
			}
			intAutoIDCol = 0;
			intDescriptionCol = 1;
			intYearCol = 2;
			intMakeCol = 3;
			intModelCol = 4;
			intSerialCol = 5;
			intTagCol = 6;
			intDeptCol = 7;
			intDivCol = 8;
			intLoc1CodeCol = 9;
			intLoc1DescCol = 10;
			intLoc2CodeCol = 11;
			intLoc2DescCol = 12;
			intLoc3CodeCol = 13;
			intLoc3DescCol = 14;
			intLoc4CodeCol = 15;
			intLoc4DescCol = 16;
			LocationCodes1 = new string[1,1];
			LocationCodes2 = new string[1,1];
			LocationCodes3 = new string[1,1];
			LocationCodes4 = new string[1,1];
			for (intCount = 1; intCount <= 4; intCount++)
			{
				rsData.OpenRecordset($"Select ID, Code, Description from tblLocations Where TypeID = {FCConvert.ToString(intCount)} Order By TypeID,Code", modGlobal.DatabaseNamePath);
				if (!rsData.EndOfFile())
				{
					switch (intCount)
					{
						case 1:
							{
								LocationCodes1 = new string[rsData.RecordCount() + 1, 3 + 1];
								lngArraySize1 = rsData.RecordCount();
								for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
								{
									LocationCodes1[intCounter, 0] = FCConvert.ToString(rsData.Get_Fields_Int32("ID"));
									LocationCodes1[intCounter, 1] = FCConvert.ToString(rsData.Get_Fields("Code"));
									LocationCodes1[intCounter, 2] = FCConvert.ToString(rsData.Get_Fields_String("Description"));
									rsData.MoveNext();
								}
								break;
							}
						case 2:
							{
								LocationCodes2 = new string[rsData.RecordCount() + 1, 3 + 1];
								lngArraySize2 = rsData.RecordCount();
								for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
								{
									LocationCodes2[intCounter, 0] = FCConvert.ToString(rsData.Get_Fields_Int32("ID"));
									LocationCodes2[intCounter, 1] = FCConvert.ToString(rsData.Get_Fields("Code"));
									LocationCodes2[intCounter, 2] = FCConvert.ToString(rsData.Get_Fields_String("Description"));
									rsData.MoveNext();
								}
								break;
							}
						case 3:
							{
								LocationCodes3 = new string[rsData.RecordCount() + 1, 3 + 1];
								lngArraySize3 = rsData.RecordCount();
								for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
								{
									LocationCodes3[intCounter, 0] = FCConvert.ToString(rsData.Get_Fields_Int32("ID"));
									LocationCodes3[intCounter, 1] = FCConvert.ToString(rsData.Get_Fields("Code"));
									LocationCodes3[intCounter, 2] = FCConvert.ToString(rsData.Get_Fields_String("Description"));
									rsData.MoveNext();
								}
								break;
							}
						case 4:
							{
								LocationCodes4 = new string[rsData.RecordCount() + 1, 3 + 1];
								lngArraySize4 = rsData.RecordCount();
								for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
								{
									LocationCodes4[intCounter, 0] = FCConvert.ToString(rsData.Get_Fields_Int32("ID"));
									LocationCodes4[intCounter, 1] = FCConvert.ToString(rsData.Get_Fields("Code"));
									LocationCodes4[intCounter, 2] = FCConvert.ToString(rsData.Get_Fields_String("Description"));
									rsData.MoveNext();
								}
								break;
							}
					}
					//end switch
				}
			}
			rsData.OpenRecordset("Select RefreshSeachOnKeyPress, DeptDiv, DivisionUsed from tblSettings", modGlobal.DatabaseNamePath);
			if (!rsData.EndOfFile())
			{
				boolSearchOnKeyPress = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("RefreshSeachOnKeyPress"));
				blnUsedBudgetaryDeptDiv = FCConvert.ToBoolean(rsData.Get_Fields("DeptDiv"));
				blnDivUsed = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DivisionUsed"));
			}
			modGlobalFunctions.SetFixedSize(this, 0);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetGridProperties();
            }
            finally
            {
                rsData.DisposeOf();
            }
		}

		private void frmSearch_Resize(object sender, System.EventArgs e)
		{
			return;
			SearchGrid.Cols = 17;
			SearchGrid.ColWidth(intAutoIDCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.04));
			SearchGrid.ColWidth(intDescriptionCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.3));
			SearchGrid.ColWidth(intYearCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.06));
			SearchGrid.ColWidth(intMakeCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intModelCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intSerialCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.25));
			SearchGrid.ColWidth(intTagCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intDeptCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.1));
			SearchGrid.ColWidth(intDivCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.1));
			SearchGrid.ColWidth(intLoc1CodeCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intLoc1DescCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.2));
			SearchGrid.ColWidth(intLoc2CodeCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intLoc2DescCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.2));
			SearchGrid.ColWidth(intLoc3CodeCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intLoc3DescCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.2));
			SearchGrid.ColWidth(intLoc4CodeCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.15));
			SearchGrid.ColWidth(intLoc4DescCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.2));
			SearchGrid.RowHeight(0, txtColumn1.HeightOriginal + 60);
			SearchGrid.RowHeight(1, txtColumn1.HeightOriginal + 60);
			SetColumns();
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdSch_Click_2(3);
		}

		private void mnuEdit_Click(object sender, System.EventArgs e)
		{
			cmdSch_Click_2(0);
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			cmdSch_Click_2(1);
		}

		public void mnuSearch_Click()
		{
			mnuSearch_Click(cmdSch, new System.EventArgs());
		}

		private void mnuShow_Click(object sender, System.EventArgs e)
		{
			SearchGrid_DblClick();
		}

		private void SearchGrid_AfterScroll(object sender, ScrollEventArgs e)
		{
			SetColumns();
		}

        //FC:FINAL:AM:#2093 - sort the column on ColumnHeaderMouseClick
        private void SearchGrid_ClickEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			//if (SearchGrid.MouseRow == 0)
			{
				// sort this grid
				SearchGrid.Select(1, e.ColumnIndex);
				if (strSort == "flexSortGenericAscending")
				{
					SearchGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					strSort = "flexSortGenericDescending";
				}
				else
				{
					SearchGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					strSort = "flexSortGenericAscending";
				}
			}
		}

		public void SearchGrid_DblClick(object sender, System.EventArgs e)
		{
			SearchGrid_DblClick();
		}

		public void SearchGrid_DblClick()
		{
			if (SearchGrid.Row < 2)
				return;
			if (modVariables.Statics.gboolMasterOpen)
			{
				MessageBox.Show("Fixed Assets Master screen is already open. It must be closed before search record can be displayed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				modVariables.Statics.glngMasterIDNumber = FCConvert.ToInt32(SearchGrid.TextMatrix(SearchGrid.Row, 0));
				if (modVariables.Statics.glngMasterIDNumber > 0)
				{
					//FC:FINAL:JEI:IIT807+FC-8697
					this.Hide();
					frmMaster.InstancePtr.blnFromSearch = true;
					frmMaster.InstancePtr.Show(App.MainForm);
				}
			}
		}

		public void SearchBth()
		{
			int intCounter;
			string strWhere;
			clsDRWrapper rsData = new clsDRWrapper();
			strWhere = " Where";

            try
            { 
                if (txtColumn1.Text != string.Empty)
                {
                    strWhere += " Description LIKE '" + txtColumn1.Text + "%' AND ";
                }
                if (txtColumn2.Text != string.Empty)
                {
                    strWhere += " Year LIKE '" + txtColumn2.Text + "%' AND ";
                }
                if (txtColumn3.Text != string.Empty)
                {
                    strWhere += " Make LIKE '" + txtColumn3.Text + "%' AND ";
                }
                if (txtColumn4.Text != string.Empty)
                {
                    strWhere += " Model LIKE '" + txtColumn4.Text + "%' AND ";
                }
                if (txtColumn5.Text != string.Empty)
                {
                    strWhere += " SerialNumber LIKE '" + txtColumn5.Text + "%' AND ";
                }
                if (txtColumn6.Text != string.Empty)
                {
                    strWhere += " TagNumber LIKE '" + txtColumn6.Text + "%' AND ";
                }
                if (txtColumn7.Text != string.Empty)
                {
                    strWhere += " Dept LIKE '" + txtColumn7.Text + "%' AND ";
                }
                if (txtColumn8.Text != string.Empty)
                {
                    strWhere += " Div LIKE '" + txtColumn8.Text + "%' AND ";
                }
                if (txtColumn9.Text != string.Empty)
                {
                    strWhere += " Location1Code LIKE '" + txtColumn9.Text + "%' AND ";
                }
                if (txtColumn10.Text != string.Empty)
                {
                    strWhere += " Location1Desc LIKE '" + txtColumn10.Text + "%' AND ";
                }
                if (txtColumn11.Text != string.Empty)
                {
                    strWhere += " Location2Code LIKE '" + txtColumn11.Text + "%' AND ";
                }
                if (txtColumn12.Text != string.Empty)
                {
                    strWhere += " Location2Desc LIKE '" + txtColumn12.Text + "%' AND ";
                }
                if (txtColumn13.Text != string.Empty)
                {
                    strWhere += " Location3Code LIKE '" + txtColumn13.Text + "%' AND ";
                }
                if (txtColumn14.Text != string.Empty)
                {
                    strWhere += " Location3Desc LIKE '" + txtColumn14.Text + "%' AND ";
                }
                if (txtColumn15.Text != string.Empty)
                {
                    strWhere += " Location4Code LIKE '" + txtColumn15.Text + "%' AND ";
                }
                if (txtColumn16.Text != string.Empty)
                {
                    strWhere += " Location4Desc LIKE '" + txtColumn16.Text + "%' AND ";
                }
                if (Strings.Trim(strWhere) == "Where")
                {
                    strWhere = string.Empty;
                }
                else
                {
                    strWhere = Strings.Left(strWhere, strWhere.Length - 4);
                }
                rsData.OpenRecordset("Select ID, Description, [Year], Make, Model, SerialNumber, TagNumber, Dept, Div, Location1Code, Location2Code, Location3Code, location4Code from tblMaster" + strWhere + " ORDER BY Description", modGlobal.DatabaseNamePath);
                if (!rsData.EndOfFile())
                {
                    SearchGrid.Rows = rsData.RecordCount() + 2;
                }
                else
                {
                    if (!boolSearchOnKeyPress)
                    {
                        MessageBox.Show("No information was found that matched your search criteria", "No Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
                for (intCounter = 2; intCounter <= rsData.RecordCount() + 1; intCounter++)
                {
                    SearchGrid.TextMatrix(intCounter, intAutoIDCol, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
                    SearchGrid.TextMatrix(intCounter, intDescriptionCol, FCConvert.ToString(rsData.Get_Fields_String("Description")));
                    SearchGrid.TextMatrix(intCounter, intYearCol, FCConvert.ToString(rsData.Get_Fields("Year")));
                    SearchGrid.TextMatrix(intCounter, intMakeCol, FCConvert.ToString(rsData.Get_Fields_String("Make")));
                    SearchGrid.TextMatrix(intCounter, intModelCol, FCConvert.ToString(rsData.Get_Fields_String("Model")));
                    SearchGrid.TextMatrix(intCounter, intSerialCol, FCConvert.ToString(rsData.Get_Fields_String("SerialNumber")));
                    SearchGrid.TextMatrix(intCounter, intTagCol, FCConvert.ToString(rsData.Get_Fields_String("TagNumber")));
                    SearchGrid.TextMatrix(intCounter, intDeptCol, FCConvert.ToString(rsData.Get_Fields_String("Dept")));
                    SearchGrid.TextMatrix(intCounter, intDivCol, FCConvert.ToString(rsData.Get_Fields_String("Div")));

                    var location1Code = FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("Location1Code"))))));
                    var location2Code = FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("Location2Code"))))));
                    var location3Code = FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("Location3Code"))))));
                    var location4Code = FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("Location4Code"))))));
                    SearchGrid.TextMatrix(intCounter, intLoc1CodeCol, FCConvert.ToString(GetLocationCodeText(location1Code, 1)));
                    SearchGrid.TextMatrix(intCounter, intLoc1DescCol, FCConvert.ToString(GetLocationDescriptionText(location1Code, 1)));
                    SearchGrid.TextMatrix(intCounter, intLoc2CodeCol, FCConvert.ToString(GetLocationCodeText(location2Code, 2)));
                    SearchGrid.TextMatrix(intCounter, intLoc2DescCol, FCConvert.ToString(GetLocationDescriptionText(location2Code, 2)));
                    SearchGrid.TextMatrix(intCounter, intLoc3CodeCol, FCConvert.ToString(GetLocationCodeText(location3Code, 3)));
                    SearchGrid.TextMatrix(intCounter, intLoc3DescCol, FCConvert.ToString(GetLocationDescriptionText(location3Code, 3)));
                    SearchGrid.TextMatrix(intCounter, intLoc4CodeCol, FCConvert.ToString(GetLocationCodeText(location4Code, 4)));
                    SearchGrid.TextMatrix(intCounter, intLoc4DescCol, FCConvert.ToString(GetLocationDescriptionText(location4Code, 4)));
                    rsData.MoveNext();
                }

                if (SearchGrid.Rows > 2)
                {
                    SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 2, 0, SearchGrid.Rows - 1, SearchGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                }
            }

            finally
            {
               rsData.DisposeOf(); 
            }
		}
		// vbPorter upgrade warning: lngValue As int	OnWrite(string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, int)
		public object GetLocationCodeText(int lngValue, int intTypeID)
		{
			object GetLocationCodeText = null;
			int intCounter;
			switch (intTypeID)
			{
				case 1:
					{
						for (intCounter = 0; intCounter <= lngArraySize1; intCounter++)
						{
							if (Conversion.Val(LocationCodes1[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes1[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationCodeText = LocationCodes1[intCounter, 1];
									return GetLocationCodeText;
								}
							}
						}
						break;
					}
				case 2:
					{
						for (intCounter = 0; intCounter <= lngArraySize2; intCounter++)
						{
							if (Conversion.Val(LocationCodes2[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes2[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationCodeText = LocationCodes2[intCounter, 1];
									return GetLocationCodeText;
								}
							}
						}
						break;
					}
				case 3:
					{
						for (intCounter = 0; intCounter <= lngArraySize3; intCounter++)
						{
							if (Conversion.Val(LocationCodes3[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes3[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationCodeText = LocationCodes3[intCounter, 1];
									return GetLocationCodeText;
								}
							}
						}
						break;
					}
				case 4:
					{
						for (intCounter = 0; intCounter <= lngArraySize4; intCounter++)
						{
							if (Conversion.Val(LocationCodes4[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes4[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationCodeText = LocationCodes4[intCounter, 1];
									return GetLocationCodeText;
								}
							}
						}
						break;
					}
			}
			//end switch
			GetLocationCodeText = lngValue;
			return GetLocationCodeText;
		}
		// vbPorter upgrade warning: lngValue As int	OnWrite(string)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, int)
		public object GetLocationDescriptionText(int lngValue, int intTypeID)
		{
			object GetLocationDescriptionText = null;
			int intCounter;
			switch (intTypeID)
			{
				case 1:
					{
						for (intCounter = 0; intCounter <= lngArraySize1; intCounter++)
						{
							if (Conversion.Val(LocationCodes1[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes1[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationDescriptionText = LocationCodes1[intCounter, 2];
									return GetLocationDescriptionText;
								}
							}
						}
						break;
					}
				case 2:
					{
						for (intCounter = 0; intCounter <= lngArraySize2; intCounter++)
						{
							if (Conversion.Val(LocationCodes2[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes2[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationDescriptionText = LocationCodes2[intCounter, 2];
									return GetLocationDescriptionText;
								}
							}
						}
						break;
					}
				case 3:
					{
						for (intCounter = 0; intCounter <= lngArraySize3; intCounter++)
						{
							if (Conversion.Val(LocationCodes3[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes3[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationDescriptionText = LocationCodes3[intCounter, 2];
									return GetLocationDescriptionText;
								}
							}
						}
						break;
					}
				case 4:
					{
						for (intCounter = 0; intCounter <= lngArraySize4; intCounter++)
						{
							if (Conversion.Val(LocationCodes4[intCounter, 0]) == lngValue)
							{
								if (Conversion.Val(LocationCodes4[intCounter, 0]) == 0)
								{
								}
								else
								{
									GetLocationDescriptionText = LocationCodes4[intCounter, 2];
									return GetLocationDescriptionText;
								}
							}
						}
						break;
					}
			}
			//end switch
			GetLocationDescriptionText = lngValue;
			return GetLocationDescriptionText;
		}

		public void clrForm(bool boolClear = true)
		{
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					(obj as MaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (FCConvert.ToBoolean(boolClear))
						(obj as FCComboBox).Clear();
				}
			}
			// obj
			SearchGrid.Rows = 2;
		}

		private void SearchGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (SearchGrid.Row == 2 && KeyCode == Keys.Up)
			{
				KeyCode = 0;
				switch (SearchGrid.Col)
				{
					case 1:
						{
							txtColumn1.Focus();
							break;
						}
					case 2:
						{
							txtColumn2.Focus();
							break;
						}
					case 3:
						{
							txtColumn3.Focus();
							break;
						}
					case 4:
						{
							txtColumn4.Focus();
							break;
						}
					case 5:
						{
							txtColumn5.Focus();
							break;
						}
					case 6:
						{
							txtColumn6.Focus();
							break;
						}
					case 7:
						{
							txtColumn7.Focus();
							break;
						}
					case 8:
						{
							txtColumn8.Focus();
							break;
						}
					case 9:
						{
							txtColumn9.Focus();
							break;
						}
					case 10:
						{
							txtColumn10.Focus();
							break;
						}
					case 11:
						{
							txtColumn11.Focus();
							break;
						}
					case 12:
						{
							txtColumn12.Focus();
							break;
						}
					case 13:
						{
							txtColumn13.Focus();
							break;
						}
					case 14:
						{
							txtColumn14.Focus();
							break;
						}
					case 15:
						{
							txtColumn15.Focus();
							break;
						}
					case 16:
						{
							txtColumn16.Focus();
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
		}

		private void SearchGrid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (SearchGrid.Row == 2 && KeyCode == Keys.Up)
			{
				KeyCode = 0;
				switch (SearchGrid.Col)
				{
					case 1:
						{
							txtColumn1.Focus();
							break;
						}
					case 2:
						{
							txtColumn2.Focus();
							break;
						}
					case 3:
						{
							txtColumn3.Focus();
							break;
						}
					case 4:
						{
							txtColumn4.Focus();
							break;
						}
					case 5:
						{
							txtColumn5.Focus();
							break;
						}
					case 6:
						{
							txtColumn6.Focus();
							break;
						}
					case 7:
						{
							txtColumn7.Focus();
							break;
						}
					case 8:
						{
							txtColumn8.Focus();
							break;
						}
					case 9:
						{
							txtColumn9.Focus();
							break;
						}
					case 10:
						{
							txtColumn10.Focus();
							break;
						}
					case 11:
						{
							txtColumn11.Focus();
							break;
						}
					case 12:
						{
							txtColumn12.Focus();
							break;
						}
					case 13:
						{
							txtColumn13.Focus();
							break;
						}
					case 14:
						{
							txtColumn14.Focus();
							break;
						}
					case 15:
						{
							txtColumn15.Focus();
							break;
						}
					case 16:
						{
							txtColumn16.Focus();
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
		}

		private void SearchGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (SearchGrid.Row == SearchGrid.Rows - 1 && SearchGrid.Col == SearchGrid.Cols - 1)
			{
				SearchGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				SearchGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void txtColumn1_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 1);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn2_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 2);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn3_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 3);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn4_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 4);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn5_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 5);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn6_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 6);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn7_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 7);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn8_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 8);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn9_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 9);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn10_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 10);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn11_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 11);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn12_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 12);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn13_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 13);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn14_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 14);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn15_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 15);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}

		private void txtColumn16_KeyUp(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Down)
			{
				KeyCode = (Keys)0;
				if (SearchGrid.Rows > 2)
				{
					SearchGrid.Select(2, 16);
					SearchGrid.Focus();
				}
			}
			else
			{
				if (boolSearchOnKeyPress)
					SearchBth();
			}
		}
	}
}
