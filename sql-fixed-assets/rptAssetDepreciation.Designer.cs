﻿namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptAssetDepreciation.
	/// </summary>
	partial class rptAssetDepreciation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAssetDepreciation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDepreciated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDept = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNetValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPreviousDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotalDepreciated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPreviousDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalNetValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDept)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreviousDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalDepreciated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPreviousDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDescription,
				this.fldDepreciation,
				this.fldTotalDepreciated,
				this.fldCost,
				this.fldPurchaseDate,
				this.fldClass,
				this.fldTag,
				this.fldDept,
				this.fldNetValue,
				this.fldPreviousDepreciation
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 0F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDescription.Text = "Field1";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 2.34375F;
			// 
			// fldDepreciation
			// 
			this.fldDepreciation.Height = 0.1875F;
			this.fldDepreciation.Left = 7.5F;
			this.fldDepreciation.Name = "fldDepreciation";
			this.fldDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDepreciation.Text = "Field1";
			this.fldDepreciation.Top = 0F;
			this.fldDepreciation.Width = 0.9375F;
			// 
			// fldTotalDepreciated
			// 
			this.fldTotalDepreciated.Height = 0.1875F;
			this.fldTotalDepreciated.Left = 8.4375F;
			this.fldTotalDepreciated.Name = "fldTotalDepreciated";
			this.fldTotalDepreciated.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldTotalDepreciated.Text = "Field1";
			this.fldTotalDepreciated.Top = 0F;
			this.fldTotalDepreciated.Width = 0.9375F;
			// 
			// fldCost
			// 
			this.fldCost.Height = 0.1875F;
			this.fldCost.Left = 5.5625F;
			this.fldCost.Name = "fldCost";
			this.fldCost.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCost.Text = "Field1";
			this.fldCost.Top = 0F;
			this.fldCost.Width = 0.9375F;
			// 
			// fldPurchaseDate
			// 
			this.fldPurchaseDate.Height = 0.1875F;
			this.fldPurchaseDate.Left = 4.6875F;
			this.fldPurchaseDate.Name = "fldPurchaseDate";
			this.fldPurchaseDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPurchaseDate.Text = "Field1";
			this.fldPurchaseDate.Top = 0F;
			this.fldPurchaseDate.Width = 0.8125F;
			// 
			// fldClass
			// 
			this.fldClass.Height = 0.1875F;
			this.fldClass.Left = 3.09375F;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldClass.Text = "Field1";
			this.fldClass.Top = 0F;
			this.fldClass.Width = 0.84375F;
			// 
			// fldTag
			// 
			this.fldTag.Height = 0.1875F;
			this.fldTag.Left = 2.40625F;
			this.fldTag.Name = "fldTag";
			this.fldTag.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldTag.Text = "Field1";
			this.fldTag.Top = 0F;
			this.fldTag.Width = 0.625F;
			// 
			// fldDept
			// 
			this.fldDept.Height = 0.1875F;
			this.fldDept.Left = 4.03125F;
			this.fldDept.Name = "fldDept";
			this.fldDept.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldDept.Text = "Field1";
			this.fldDept.Top = 0F;
			this.fldDept.Width = 0.5625F;
			// 
			// fldNetValue
			// 
			this.fldNetValue.Height = 0.1875F;
			this.fldNetValue.Left = 9.40625F;
			this.fldNetValue.Name = "fldNetValue";
			this.fldNetValue.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldNetValue.Text = "Field1";
			this.fldNetValue.Top = 0F;
			this.fldNetValue.Width = 0.9375F;
			// 
			// fldPreviousDepreciation
			// 
			this.fldPreviousDepreciation.Height = 0.1875F;
			this.fldPreviousDepreciation.Left = 6.5625F;
			this.fldPreviousDepreciation.Name = "fldPreviousDepreciation";
			this.fldPreviousDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldPreviousDepreciation.Text = "Field1";
			this.fldPreviousDepreciation.Top = 0F;
			this.fldPreviousDepreciation.Width = 0.9375F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblDateRange,
				this.Label9,
				this.Label12,
				this.Line1,
				this.Label13,
				this.Label14,
				this.Label15,
				this.lblPurchaseDate,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21
			});
			this.PageHeader.Height = 0.7291667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Asset Depreciation Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 9.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 9.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.5F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label6";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 7.6875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label9.Text = "Description";
			this.Label9.Top = 0.53125F;
			this.Label9.Width = 2.3125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 7.5F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "Curr Dep";
			this.Label12.Top = 0.53125F;
			this.Label12.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.71875F;
			this.Line1.Width = 10.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10.5F;
			this.Line1.Y1 = 0.71875F;
			this.Line1.Y2 = 0.71875F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 8.4375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "Total Dep";
			this.Label13.Top = 0.53125F;
			this.Label13.Width = 0.9375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.09375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label14.Text = "Class";
			this.Label14.Top = 0.53125F;
			this.Label14.Width = 0.875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.5625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "Cost";
			this.Label15.Top = 0.53125F;
			this.Label15.Width = 0.9375F;
			// 
			// lblPurchaseDate
			// 
			this.lblPurchaseDate.Height = 0.1875F;
			this.lblPurchaseDate.HyperLink = null;
			this.lblPurchaseDate.Left = 4.6875F;
			this.lblPurchaseDate.Name = "lblPurchaseDate";
			this.lblPurchaseDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblPurchaseDate.Text = "Pur. Date";
			this.lblPurchaseDate.Top = 0.53125F;
			this.lblPurchaseDate.Width = 0.8125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.40625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "Tag #";
			this.Label18.Top = 0.53125F;
			this.Label18.Width = 0.625F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4.03125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label19.Text = "Dept";
			this.Label19.Top = 0.53125F;
			this.Label19.Width = 0.5625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 6.5625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label20.Text = "Previous Dep";
			this.Label20.Top = 0.53125F;
			this.Label20.Width = 0.9375F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 9.40625F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label21.Text = "Net Value";
			this.Label21.Top = 0.53125F;
			this.Label21.Width = 0.9375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.fldTotalDepreciation,
				this.fldTotalTotalDepreciated,
				this.fldTotalCost,
				this.Label17,
				this.fldTotalPreviousDepreciation,
				this.fldTotalNetValue
			});
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.8125F;
			this.Line2.X1 = 4.625F;
			this.Line2.X2 = 10.4375F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldTotalDepreciation
			// 
			this.fldTotalDepreciation.Height = 0.1875F;
			this.fldTotalDepreciation.Left = 7.5F;
			this.fldTotalDepreciation.Name = "fldTotalDepreciation";
			this.fldTotalDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalDepreciation.Text = "Field1";
			this.fldTotalDepreciation.Top = 0.03125F;
			this.fldTotalDepreciation.Width = 0.9375F;
			// 
			// fldTotalTotalDepreciated
			// 
			this.fldTotalTotalDepreciated.Height = 0.1875F;
			this.fldTotalTotalDepreciated.Left = 8.4375F;
			this.fldTotalTotalDepreciated.Name = "fldTotalTotalDepreciated";
			this.fldTotalTotalDepreciated.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalTotalDepreciated.Text = "Field1";
			this.fldTotalTotalDepreciated.Top = 0.03125F;
			this.fldTotalTotalDepreciated.Width = 0.9375F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 5.59375F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalCost.Text = "Field1";
			this.fldTotalCost.Top = 0.03125F;
			this.fldTotalCost.Width = 0.9375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4.625F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8pt; font-weight: bold";
			this.Label17.Text = "Final Totals:";
			this.Label17.Top = 0.03125F;
			this.Label17.Width = 0.90625F;
			// 
			// fldTotalPreviousDepreciation
			// 
			this.fldTotalPreviousDepreciation.Height = 0.1875F;
			this.fldTotalPreviousDepreciation.Left = 6.5625F;
			this.fldTotalPreviousDepreciation.Name = "fldTotalPreviousDepreciation";
			this.fldTotalPreviousDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalPreviousDepreciation.Text = "Field1";
			this.fldTotalPreviousDepreciation.Top = 0.03125F;
			this.fldTotalPreviousDepreciation.Width = 0.9375F;
			// 
			// fldTotalNetValue
			// 
			this.fldTotalNetValue.Height = 0.1875F;
			this.fldTotalNetValue.Left = 9.40625F;
			this.fldTotalNetValue.Name = "fldTotalNetValue";
			this.fldTotalNetValue.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalNetValue.Text = "Field1";
			this.fldTotalNetValue.Top = 0.03125F;
			this.fldTotalNetValue.Width = 0.9375F;
			// 
			// rptAssetDepreciation
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptAssetDepreciation_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDept)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNetValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPreviousDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalDepreciated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPreviousDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalNetValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDepreciated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTag;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDept;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNetValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPreviousDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotalDepreciated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPreviousDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNetValue;
	}
}
