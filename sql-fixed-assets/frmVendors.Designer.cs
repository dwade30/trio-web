//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmVendors.
	/// </summary>
	partial class frmVendors : BaseForm
	{
		public fecherFoundation.FCGrid VendorGrid;
		public fecherFoundation.FCLabel lblBudLabel;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.VendorGrid = new fecherFoundation.FCGrid();
			this.lblBudLabel = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdNew = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.VendorGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 380);
			this.BottomPanel.Size = new System.Drawing.Size(556, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.VendorGrid);
			this.ClientArea.Controls.Add(this.lblBudLabel);
			this.ClientArea.Size = new System.Drawing.Size(556, 320);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(556, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(169, 30);
			this.HeaderText.Text = "Vendor Codes";
			// 
			// VendorGrid
			// 
			this.VendorGrid.AllowSelection = false;
			this.VendorGrid.AllowUserToResizeColumns = false;
			this.VendorGrid.AllowUserToResizeRows = false;
			this.VendorGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.VendorGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.VendorGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.VendorGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.VendorGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.VendorGrid.BackColorSel = System.Drawing.Color.Empty;
			this.VendorGrid.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.VendorGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.VendorGrid.ColumnHeadersHeight = 30;
			this.VendorGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.VendorGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.VendorGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.VendorGrid.ExtendLastCol = true;
			this.VendorGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.VendorGrid.FrozenCols = 0;
			this.VendorGrid.GridColor = System.Drawing.Color.Empty;
			this.VendorGrid.Location = new System.Drawing.Point(30, 60);
			this.VendorGrid.Name = "VendorGrid";
			this.VendorGrid.ReadOnly = true;
			this.VendorGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.VendorGrid.RowHeightMin = 0;
			this.VendorGrid.Rows = 50;
			this.VendorGrid.ShowColumnVisibilityMenu = false;
			this.VendorGrid.Size = new System.Drawing.Size(496, 245);
			this.VendorGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.VendorGrid.TabIndex = 0;
			this.VendorGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.VendorGrid_KeyPressEdit);
			this.VendorGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.VendorGrid_BeforeEdit);
			this.VendorGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.VendorGrid_AfterEdit);
			this.VendorGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.VendorGrid_ValidateEdit);
			this.VendorGrid.KeyPress += new Wisej.Web.KeyPressEventHandler(this.VendorGrid_KeyPressEvent);
			this.VendorGrid.Click += new System.EventHandler(this.VendorGrid_ClickEvent);
			// 
			// lblBudLabel
			// 
			this.lblBudLabel.Location = new System.Drawing.Point(30, 30);
			this.lblBudLabel.Name = "lblBudLabel";
			this.lblBudLabel.Size = new System.Drawing.Size(207, 17);
			this.lblBudLabel.TabIndex = 1;
			this.lblBudLabel.Text = "BUDGETARY VALUES - READ ONLY";
			this.lblBudLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuNew,
				this.mnuDelete,
				this.mnuSP2,
				this.mnuSave,
				this.mnuSaveExit,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 0;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuDelete.Text = "Delete";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSP2
			// 
			this.mnuSP2.Index = 2;
			this.mnuSP2.Name = "mnuSP2";
			this.mnuSP2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save                      ";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 4;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 5;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 6;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(410, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdNew.Size = new System.Drawing.Size(49, 24);
			this.cmdNew.TabIndex = 17;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(465, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.cmdDelete.Size = new System.Drawing.Size(61, 24);
			this.cmdDelete.TabIndex = 16;
			this.cmdDelete.Text = "Delete";
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(240, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 5;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmVendors
			// 
			this.ClientSize = new System.Drawing.Size(556, 476);
			this.KeyPreview = true;
			this.Name = "frmVendors";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Vendor Codes";
			this.Load += new System.EventHandler(this.frmVendors_Load);
			this.Activated += new System.EventHandler(this.frmVendors_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmVendors_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmVendors_KeyPress);
			this.Resize += new System.EventHandler(this.frmVendors_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.VendorGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdNew;
		private FCButton cmdDelete;
		private FCButton cmdSave;
	}
}