﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmInitialDepreciation.
	/// </summary>
	public partial class frmInitialDepreciation : BaseForm
	{
		public frmInitialDepreciation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmInitialDepreciation InstancePtr
		{
			get
			{
				return (frmInitialDepreciation)Sys.GetInstance(typeof(frmInitialDepreciation));
			}
		}

		protected frmInitialDepreciation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By   Dave Wade
		// Date         3/19/03
		// This form will be used to enter an initial depreciation
		// for an item that was entered into fixed assets
		// ********************************************************
		clsDRWrapper rsMasterInfo = new clsDRWrapper();
		int lngReportID;
		Decimal curNewAmount;

		private void cboDepreciationType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboDepreciationType.SelectedIndex == 0)
			{
				fraSLDepreciation.Top = fraDBDepreciation.Top;
				fraSLDepreciation.Visible = true;
				fraDBDepreciation.Visible = false;
				txtDBInitialDepreciationDate.Text = "";
				txtDBInitialDepreciationAmount.Text = "";
			}
			else if (cboDepreciationType.SelectedIndex == 1)
			{
				fraSLDepreciation.Visible = false;
				fraDBDepreciation.Visible = true;
				txtSLInitialDepreciationDate.Text = "";
				txtSLInitialDepreciationAmount.Text = "";
			}
			else
			{
				fraSLDepreciation.Visible = true;
				fraDBDepreciation.Visible = true;
				fraSLDepreciation.Top = fraDBDepreciation.Top + fraDBDepreciation.Height + 30;
			}
		}

		private void frmInitialDepreciation_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmInitialDepreciation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInitialDepreciation.FillStyle	= 0;
			//frmInitialDepreciation.ScaleWidth	= 5880;
			//frmInitialDepreciation.ScaleHeight	= 3810;
			//frmInitialDepreciation.LinkTopic	= "Form2";
			//frmInitialDepreciation.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			lngReportID = 0;
			rsMasterInfo.OpenRecordset("Select * from tblMaster where ID = " + FCConvert.ToString(modVariables.Statics.glngMasterIDNumber), modGlobal.DatabaseNamePath);
			if (FCConvert.ToInt32(rsMasterInfo.Get_Fields_Int16("DepreciationMethod")) == 2)
			{
				cboDepreciationType.Visible = false;
				lblDepreciationType.Visible = false;
			}
			else
			{
				cboDepreciationType.SelectedIndex = FCConvert.ToInt32(rsMasterInfo.Get_Fields_Int16("DepreciationMethod"));
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmInitialDepreciation_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cboDepreciationType.Visible == false)
			{
				if (ValidateUNDepreciation())
				{
					goto SaveInfo;
				}
				else
				{
					return;
				}
			}
			else
			{
				if (cboDepreciationType.SelectedIndex == 0)
				{
					if (ValidateSLDepreciation())
					{
						goto SaveInfo;
					}
					else
					{
						return;
					}
				}
				else if (cboDepreciationType.SelectedIndex == 1)
				{
					if (ValidateDBDepreciation())
					{
						goto SaveInfo;
					}
					else
					{
						return;
					}
				}
				else
				{
					if (ValidateDBDepreciation())
					{
						if (ValidateSLDepreciation_2(true))
						{
							goto SaveInfo;
						}
						else
						{
							return;
						}
					}
					else
					{
						return;
					}
				}
			}
			SaveInfo:
			;
			rsMasterInfo.Edit();
			if (cboDepreciationType.Visible == false)
			{
				SaveUNDepreciation();
			}
			else
			{
				if (cboDepreciationType.SelectedIndex == 0)
				{
					SaveSLDepreciation();
				}
				else if (cboDepreciationType.SelectedIndex == 1)
				{
					SaveDBDepreciation();
				}
				else
				{
					SaveDBDepreciation(true);
					SaveSLDepreciation_2(true);
				}
			}
			rsMasterInfo.Update(true);
			frmMaster.InstancePtr.LoadGrid(modVariables.Statics.glngMasterIDNumber);
			frmMaster.InstancePtr.boolDataChanged = false;
			Close();
		}

		private bool SaveSLDepreciation_2(bool blnBothDepreciations)
		{
			return SaveSLDepreciation(blnBothDepreciations);
		}

		private bool SaveSLDepreciation(bool blnBothDepreciations = false)
		{
			bool SaveSLDepreciation = false;
			clsDRWrapper rsDepreciation = new clsDRWrapper();
			SaveSLDepreciation = false;
			if (lngReportID == 0)
			{
				rsDepreciation.OpenRecordset("SELECT * FROM tblDepreciationMaster WHERE ID = 0", modGlobal.DatabaseNamePath);
				rsDepreciation.AddNew();
				rsDepreciation.Set_Fields("UserID", modGlobalConstants.Statics.gstrUserID);
				rsDepreciation.Set_Fields("DepreciationType", "I");
				lngReportID = FCConvert.ToInt32(rsDepreciation.Get_Fields_Int32("ID"));
				rsDepreciation.Update(true);
			}
			rsDepreciation.OpenRecordset("SELECT * FROM tblDepreciation WHERE ID = 0", modGlobal.DatabaseNamePath);
			rsDepreciation.AddNew();
			rsDepreciation.Set_Fields("ItemID", rsMasterInfo.Get_Fields_Int32("ID"));
			rsDepreciation.Set_Fields("DepreciationDate", DateAndTime.DateValue(txtSLInitialDepreciationDate.Text));
			rsDepreciation.Set_Fields("DepreciationAmount", FCConvert.ToDecimal(txtSLInitialDepreciationAmount.Text));
			if (!blnBothDepreciations)
			{
				rsDepreciation.Set_Fields("DepreciationUnits", Math.Abs(DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMasterInfo.Get_Fields_DateTime("DateLastDepreciation"), "MM/dd/yyyy")), FCConvert.ToDateTime(txtSLInitialDepreciationDate.Text))) + 1);
			}
			else
			{
				rsDepreciation.Set_Fields("DepreciationUnits", Math.Abs(DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMasterInfo.Get_Fields_DateTime("DateLastDepreciation"), "MM/dd/yyyy")), FCConvert.ToDateTime(txtSLInitialDepreciationDate.Text))));
			}
			rsDepreciation.Set_Fields("DepreciationDesc", "Days (SL) ");
			rsDepreciation.Set_Fields("Pending", false);
			rsDepreciation.Set_Fields("ReportID", lngReportID);
			rsDepreciation.Set_Fields("ChangedToSL", false);
			rsDepreciation.Update(true);
			rsMasterInfo.Set_Fields("TotalValue", FCConvert.ToDecimal(rsMasterInfo.Get_Fields_Double("TotalValue")) - FCConvert.ToDecimal(txtSLInitialDepreciationAmount.Text));
			rsMasterInfo.Set_Fields("TotalDepreciated", rsMasterInfo.Get_Fields_Decimal("TotalDepreciated") + FCConvert.ToDecimal(txtSLInitialDepreciationAmount.Text));
			frmMaster.InstancePtr.txtValue.Text = Strings.Format(rsMasterInfo.Get_Fields_Double("TotalValue"), "#,##0.00");
			frmMaster.InstancePtr.txtTotalDepreciated.Text = Strings.Format(rsMasterInfo.Get_Fields_Decimal("TotalDepreciated"), "#,##0.00");
			rsMasterInfo.Set_Fields("DateLastDepreciation", DateAndTime.DateValue(txtSLInitialDepreciationDate.Text));
			frmMaster.InstancePtr.mskDateLastDepreciation.Text = txtSLInitialDepreciationDate.Text;
			SaveSLDepreciation = true;
			return SaveSLDepreciation;
		}

		private bool SaveDBDepreciation(bool blnBothDepreciations = false)
		{
			bool SaveDBDepreciation = false;
			clsDRWrapper rsDepreciation = new clsDRWrapper();
			SaveDBDepreciation = false;
			if (lngReportID == 0)
			{
				rsDepreciation.OpenRecordset("SELECT * FROM tblDepreciationMaster WHERE ID = 0", modGlobal.DatabaseNamePath);
				rsDepreciation.AddNew();
				rsDepreciation.Set_Fields("UserID", modGlobalConstants.Statics.gstrUserID);
				rsDepreciation.Set_Fields("DepreciationType", "I");
				lngReportID = FCConvert.ToInt32(rsDepreciation.Get_Fields_Int32("ID"));
				rsDepreciation.Update(true);
			}
			rsDepreciation.OpenRecordset("SELECT * FROM tblDepreciation WHERE ID = 0", modGlobal.DatabaseNamePath);
			rsDepreciation.AddNew();
			rsDepreciation.Set_Fields("ItemID", rsMasterInfo.Get_Fields_Int32("ID"));
			rsDepreciation.Set_Fields("DepreciationDate", DateAndTime.DateValue(txtDBInitialDepreciationDate.Text));
			rsDepreciation.Set_Fields("DepreciationAmount", FCConvert.ToDecimal(txtDBInitialDepreciationAmount.Text));
			rsDepreciation.Set_Fields("DepreciationUnits", Math.Abs(DateAndTime.DateDiff("d", FCConvert.ToDateTime(Strings.Format(rsMasterInfo.Get_Fields_DateTime("DateLastDepreciation"), "MM/dd/yyyy")), FCConvert.ToDateTime(txtDBInitialDepreciationDate.Text))) + 1);
			rsDepreciation.Set_Fields("DepreciationDesc", "Days (DB) ");
			rsDepreciation.Set_Fields("Pending", false);
			rsDepreciation.Set_Fields("ReportID", lngReportID);
			if (blnBothDepreciations)
			{
				rsDepreciation.Set_Fields("ChangedToSL", true);
			}
			else
			{
				rsDepreciation.Set_Fields("ChangedToSL", false);
			}
			rsDepreciation.Update(true);
			rsMasterInfo.Set_Fields("TotalValue", FCConvert.ToDecimal(rsMasterInfo.Get_Fields_Double("TotalValue")) - FCConvert.ToDecimal(txtDBInitialDepreciationAmount.Text));
			rsMasterInfo.Set_Fields("TotalDepreciated", rsMasterInfo.Get_Fields_Decimal("TotalDepreciated") + FCConvert.ToDecimal(txtDBInitialDepreciationAmount.Text));
			frmMaster.InstancePtr.txtValue.Text = Strings.Format(rsMasterInfo.Get_Fields_Double("TotalValue"), "#,##0.00");
			frmMaster.InstancePtr.txtTotalDepreciated.Text = Strings.Format(rsMasterInfo.Get_Fields_Decimal("TotalDepreciated"), "#,##0.00");
			rsMasterInfo.Set_Fields("DateLastDepreciation", DateAndTime.DateValue(txtDBInitialDepreciationDate.Text));
			frmMaster.InstancePtr.mskDateLastDepreciation.Text = txtDBInitialDepreciationDate.Text;
			SaveDBDepreciation = true;
			return SaveDBDepreciation;
		}

		private bool SaveUNDepreciation()
		{
			bool SaveUNDepreciation = false;
			clsDRWrapper rsDepreciation = new clsDRWrapper();
			SaveUNDepreciation = false;
			if (lngReportID == 0)
			{
				rsDepreciation.OpenRecordset("SELECT * FROM tblDepreciationMaster WHERE ID = 0", modGlobal.DatabaseNamePath);
				rsDepreciation.AddNew();
				rsDepreciation.Set_Fields("UserID", modGlobalConstants.Statics.gstrUserID);
				rsDepreciation.Set_Fields("DepreciationType", "I");
				lngReportID = FCConvert.ToInt32(rsDepreciation.Get_Fields_Int32("ID"));
				rsDepreciation.Update(true);
			}
			rsDepreciation.OpenRecordset("SELECT * FROM tblDepreciation WHERE ID = 0", modGlobal.DatabaseNamePath);
			rsDepreciation.AddNew();
			rsDepreciation.Set_Fields("ItemID", rsMasterInfo.Get_Fields_Int32("ID"));
			rsDepreciation.Set_Fields("DepreciationDate", DateAndTime.DateValue(txtUNInitialDepreciationDate.Text));
			rsDepreciation.Set_Fields("DepreciationAmount", FCConvert.ToDecimal(txtUNInitialDepreciationAmount.Text));
			rsDepreciation.Set_Fields("DepreciationUnits", FCConvert.ToDecimal(txtUNInitialAmountUsed.Text));
			rsDepreciation.Set_Fields("DepreciationDesc", rsMasterInfo.Get_Fields_String("UNRate") + "(UN)");
			rsDepreciation.Set_Fields("Pending", false);
			rsDepreciation.Set_Fields("ReportID", lngReportID);
			rsDepreciation.Set_Fields("ChangedToSL", false);
			rsDepreciation.Update(true);
			rsMasterInfo.Set_Fields("TotalValue", rsMasterInfo.Get_Fields_Double("TotalValue") - FCConvert.ToDouble(txtUNInitialDepreciationAmount.Text));
			rsMasterInfo.Set_Fields("TotalDepreciated", rsMasterInfo.Get_Fields_Decimal("TotalDepreciated") + FCConvert.ToDecimal(txtUNInitialDepreciationAmount.Text));
			frmMaster.InstancePtr.txtValue.Text = Strings.Format(rsMasterInfo.Get_Fields_Double("TotalValue"), "#,##0.00");
			frmMaster.InstancePtr.txtTotalDepreciated.Text = Strings.Format(rsMasterInfo.Get_Fields_Decimal("TotalDepreciated"), "#,##0.00");
			rsMasterInfo.Set_Fields("DateLastDepreciation", DateAndTime.DateValue(txtUNInitialDepreciationDate.Text));
			frmMaster.InstancePtr.mskDateLastDepreciation.Text = txtUNInitialDepreciationDate.Text;
			rsMasterInfo.Set_Fields("TotalUsed", FCConvert.ToDecimal(rsMasterInfo.Get_Fields_Int32("TotalUsed")) + FCConvert.ToDecimal(txtUNInitialAmountUsed.Text));
			frmMaster.InstancePtr.txtTotalUsed.Text = FCConvert.ToString(rsMasterInfo.Get_Fields_Int32("TotalUsed"));
			SaveUNDepreciation = true;
			return SaveUNDepreciation;
		}

		private bool ValidateDBDepreciation()
		{
			bool ValidateDBDepreciation = false;
			ValidateDBDepreciation = false;
			if (Information.IsDate(txtDBInitialDepreciationDate.Text))
			{
				if (DateAndTime.DateValue(txtDBInitialDepreciationDate.Text).ToOADate() < rsMasterInfo.Get_Fields_DateTime("PurchaseDate").ToOADate())
				{
					MessageBox.Show("Your initial depreciation date must be later than the purchase date associated with this item.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtDBInitialDepreciationDate.Focus();
					return ValidateDBDepreciation;
				}
				else if (DateAndTime.DateValue(txtDBInitialDepreciationDate.Text).ToOADate() > DateTime.Today.ToOADate())
				{
					MessageBox.Show("Your initial depreciation date must be no later than the current date.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtDBInitialDepreciationDate.Focus();
					return ValidateDBDepreciation;
				}
			}
			else
			{
				MessageBox.Show("You must enter a valid initial depreciation date before you may continue.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDBInitialDepreciationDate.Focus();
				return ValidateDBDepreciation;
			}
			if (Information.IsNumeric(txtDBInitialDepreciationAmount.Text))
			{
				if (FCConvert.ToDecimal(txtDBInitialDepreciationAmount.Text) > (rsMasterInfo.Get_Fields_Decimal("BasisCost") - rsMasterInfo.Get_Fields_Decimal("SalvageValue")))
				{
					MessageBox.Show("The amount of initial depreciation you entered is more than you are allowed to depreciate based on your basis cost and salvage value.  Please enter a valid depreciation amount before you continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtDBInitialDepreciationAmount.Focus();
					return ValidateDBDepreciation;
				}
			}
			else
			{
				MessageBox.Show("The must enter a valid initial depreciation amount before you may continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtDBInitialDepreciationAmount.Focus();
				return ValidateDBDepreciation;
			}
			ValidateDBDepreciation = true;
			return ValidateDBDepreciation;
		}

		private bool ValidateSLDepreciation_2(bool blnBothTypes)
		{
			return ValidateSLDepreciation(blnBothTypes);
		}

		private bool ValidateSLDepreciation(bool blnBothTypes = false)
		{
			bool ValidateSLDepreciation = false;
			ValidateSLDepreciation = false;
			if (Information.IsDate(txtSLInitialDepreciationDate.Text))
			{
				if (!blnBothTypes)
				{
					if (DateAndTime.DateValue(txtSLInitialDepreciationDate.Text).ToOADate() < rsMasterInfo.Get_Fields_DateTime("PurchaseDate").ToOADate())
					{
						MessageBox.Show("Your initial depreciation date must be later than the purchase date associated with this item.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSLInitialDepreciationDate.Focus();
						return ValidateSLDepreciation;
					}
					else if (DateAndTime.DateValue(txtSLInitialDepreciationDate.Text).ToOADate() > DateTime.Today.ToOADate())
					{
						MessageBox.Show("Your initial depreciation date must be no later than the current date.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSLInitialDepreciationDate.Focus();
						return ValidateSLDepreciation;
					}
				}
				else
				{
					if (DateAndTime.DateValue(txtSLInitialDepreciationDate.Text).ToOADate() < DateAndTime.DateValue(txtDBInitialDepreciationDate.Text).ToOADate())
					{
						MessageBox.Show("Your initial SL depreciation date must be later than the initial DB depreciation date associated with this item.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSLInitialDepreciationDate.Focus();
						return ValidateSLDepreciation;
					}
					else if (DateAndTime.DateValue(txtSLInitialDepreciationDate.Text).ToOADate() > DateTime.Today.ToOADate())
					{
						MessageBox.Show("Your initial depreciation date must be no later than the current date.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSLInitialDepreciationDate.Focus();
						return ValidateSLDepreciation;
					}
				}
			}
			else
			{
				MessageBox.Show("You must enter a valid initial depreciation date before you may continue.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtSLInitialDepreciationDate.Focus();
				return ValidateSLDepreciation;
			}
			if (Information.IsNumeric(txtSLInitialDepreciationAmount.Text))
			{
				if (!blnBothTypes)
				{
					if (FCConvert.ToDecimal(txtSLInitialDepreciationAmount.Text) > (rsMasterInfo.Get_Fields_Decimal("BasisCost") - rsMasterInfo.Get_Fields_Decimal("SalvageValue")))
					{
						MessageBox.Show("The amount of initial depreciation you entered is more than you are allowed to depreciate based on your basis cost and salvage value.  Please enter a valid depreciation amount before you continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSLInitialDepreciationAmount.Focus();
						return ValidateSLDepreciation;
					}
				}
				else
				{
					if (FCConvert.ToDecimal(txtSLInitialDepreciationAmount.Text) > (rsMasterInfo.Get_Fields_Decimal("BasisCost") - rsMasterInfo.Get_Fields_Decimal("SalvageValue") - FCConvert.ToDecimal(txtDBInitialDepreciationAmount.Text)))
					{
						MessageBox.Show("The amount of initial depreciation you entered is more than you are allowed to depreciate based on your basis cost and salvage value and initial DB depreciation amount.  Please enter a valid depreciation amount before you continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSLInitialDepreciationAmount.Focus();
						return ValidateSLDepreciation;
					}
				}
			}
			else
			{
				MessageBox.Show("The must enter a valid initial depreciation amount before you may continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtSLInitialDepreciationAmount.Focus();
				return ValidateSLDepreciation;
			}
			ValidateSLDepreciation = true;
			return ValidateSLDepreciation;
		}

		private bool ValidateUNDepreciation()
		{
			bool ValidateUNDepreciation = false;
			ValidateUNDepreciation = false;
			if (Information.IsDate(txtUNInitialDepreciationDate.Text))
			{
				if (DateAndTime.DateValue(txtUNInitialDepreciationDate.Text).ToOADate() < rsMasterInfo.Get_Fields_DateTime("PurchaseDate").ToOADate())
				{
					MessageBox.Show("Your initial depreciation date must be later than the purchase date associated with this item.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtUNInitialDepreciationDate.Focus();
					return ValidateUNDepreciation;
				}
				else if (DateAndTime.DateValue(txtUNInitialDepreciationDate.Text).ToOADate() > DateTime.Today.ToOADate())
				{
					MessageBox.Show("Your initial depreciation date must be no later than the current date.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtUNInitialDepreciationDate.Focus();
					return ValidateUNDepreciation;
				}
			}
			else
			{
				MessageBox.Show("You must enter a valid initial depreciation date before you may continue.", "Invalid Depreciation Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtUNInitialDepreciationDate.Focus();
				return ValidateUNDepreciation;
			}
			if (Information.IsNumeric(txtUNInitialDepreciationAmount.Text))
			{
				if (FCConvert.ToDecimal(txtUNInitialDepreciationAmount.Text) > (rsMasterInfo.Get_Fields_Decimal("BasisCost") - rsMasterInfo.Get_Fields_Decimal("SalvageValue")))
				{
					MessageBox.Show("The amount of initial depreciation you entered is more than you are allowed to depreciate based on your basis cost and salvage value.  Please enter a valid depreciation amount before you continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtUNInitialDepreciationAmount.Focus();
					return ValidateUNDepreciation;
				}
			}
			else
			{
				MessageBox.Show("The must enter a valid initial depreciation amount before you may continue.", "Invalid Depreciation Amount", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtUNInitialDepreciationAmount.Focus();
				return ValidateUNDepreciation;
			}
			if (txtUNInitialAmountUsed.Visible == true)
			{
				if (Information.IsNumeric(txtUNInitialAmountUsed.Text))
				{
					// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
					if (FCConvert.ToDecimal(txtUNInitialAmountUsed.Text) > (rsMasterInfo.Get_Fields("Life") - Conversion.Val(rsMasterInfo.Get_Fields_Int32("TotalUsed"))))
					{
						MessageBox.Show("The initial amount used you entered is more than you are allowed to enter based on the life of this item and the amount already used.  Please enter a valid amount used before you continue.", "Invalid Amount Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtUNInitialAmountUsed.Focus();
						return ValidateUNDepreciation;
					}
				}
			}
			else
			{
				MessageBox.Show("The must enter a valid initial amount used before you may continue.", "Invalid Amount Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtUNInitialAmountUsed.Focus();
				return ValidateUNDepreciation;
			}
			ValidateUNDepreciation = true;
			return ValidateUNDepreciation;
		}

		private void btnSave_Click(object sender, EventArgs e)
		{
			mnuProcessSave_Click(null, EventArgs.Empty);
		}
	}
}
