﻿//Fecher vbPorter - Version 1.0.0.27
using Global;

namespace TWFA0000
{
	public class modVariables
	{
		public class StaticVariables
		{
			// vbPorter upgrade warning: glngMasterIDNumber As int	OnWrite(int, string)
			// ********************************************************
			// Last Updated   :                                       *
			// MODIFIED BY    :                                       *
			// *
			// Date           :   12/04/2002                          *
			// WRITTEN BY     :   Matthew Larrabee                    *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public int glngMasterIDNumber;
			// vbPorter upgrade warning: boolVendorsFromBud As bool	OnWrite(bool, short)
			public bool boolVendorsFromBud;
			// vbPorter upgrade warning: boolDeptDivFromBud As bool	OnWrite(bool, short)
			public bool boolDeptDivFromBud;
			public bool boolBudgetaryExists;
			// vbPorter upgrade warning: boolUseDivision As bool	OnWrite(bool, short)
			public bool boolUseDivision;
			public bool gboolMasterOpen;
			public int glngDepReportID;
			// vbPorter upgrade warning: gintPeriodNumber As short --> As int	OnWrite(string)
			public int gintPeriodNumber;
			public int glngJournalNumber;
			public string gdatDepreciationDate = "";
			public bool gboolCancelSelected;
			public modBudgetaryAccounting.FundType[] AccountStrut = null;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
