﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using GrapeCity.ActiveReports.SectionReportModel;
using System.Drawing;
using TWSharedLibrary;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptCustomReport.
	/// </summary>
	public partial class rptCustomReport : BaseSectionReport
	{
		public static rptCustomReport InstancePtr
		{
			get
			{
				return (rptCustomReport)Sys.GetInstance(typeof(rptCustomReport));
			}
		}

		protected rptCustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// THIS REPORT IS TOTOALLY GENERIC AND IF USED BY WITH
		// MODCUSTOMREPORT.MOD AND FRMCUSTOMREPORT.FRM THEN THERE
		// DOES NOT NEED TO HAVE ANY CHANGES DONE TO THIS REPORT
		clsDRWrapper rsData = new clsDRWrapper();
		int intPageNumber;
		FieldMessage[] strMessage = new FieldMessage[50 + 1];
		// vbPorter upgrade warning: curTotalCost As Decimal	OnWrite(Decimal, short)
		Decimal curTotalCost;
		// vbPorter upgrade warning: curTotalDepreciation As Decimal	OnWrite(Decimal, short)
		Decimal curTotalDepreciation;
		// vbPorter upgrade warning: curTotalCurrentValue As Decimal	OnWrite(Decimal, short)
		Decimal curTotalCurrentValue;
		int TotalCostCol;
		int TotalCurrentValueCol;
		int TotalDepreciationCol;
		bool blnCostTotalsExist;
		bool blnCurrentValueTotalsExist;
		bool blnDepreciationTotalsExist;
		float lngNextTop;
		// vbPorter upgrade warning: dblWidthRatio As double	OnWriteFCConvert.ToSingle(
		double dblWidthRatio;

		public rptCustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Report";
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			for (int i = 0; i < 50; i++)
			{
				strMessage[i] = new FieldMessage(0);
			}
		}

		private struct FieldMessage
		{
			public string strMessage;
			public bool boolShown;
			public bool boolDontContinue;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public FieldMessage(int unusedParam)
			{
				this.strMessage = string.Empty;
				this.boolShown = false;
				this.boolDontContinue = false;
			}
		};

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int intRow;
			int intCol;
			int intControl;
			clsDRWrapper rsDeptDivInfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
					return;
				}
				// 
				for (intRow = 1; intRow <= frmCustomReport.InstancePtr.vsLayout.Rows - 1; intRow++)
				{
					for (intCol = 0; intCol <= frmCustomReport.InstancePtr.vsLayout.Cols - 1; intCol++)
					{
						for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
						{
							if (Detail.Controls[intControl].Name == "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol))
							{
								break;
							}
						}
						if (FCConvert.ToInt32(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) >= 0)
						{
							if (intCol > 0)
							{
								if (!FCUtils.IsEmpty(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))
								{
									if (FCUtils.IsEmpty(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))
									{
										// ADD THE DATA TO THE CORRECT CELL IN THE GRID
										if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
										{
											SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString()), "MM/dd/yyyy"), intRow, intCol);
										}
										else
										{
											if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
											{
												SetCellWithData(intControl, string.Empty, intRow, intCol);
											}
											else
											{
												if (Strings.Left(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]), 14) == "tblMaster.Life")
												{
													// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
													if (FCConvert.ToInt32(rsData.Get_Fields("Life")) == 0)
													{
														SetCellWithData(intControl, string.Empty, intRow, intCol);
													}
													else
													{
														if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))) != "")
														{
															// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
															SetCellWithData(intControl, rsData.Get_Fields("Life") + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))), intRow, intCol);
														}
														else
														{
															// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
															SetCellWithData(intControl, rsData.Get_Fields("Life") + " Years", intRow, intCol);
														}
													}
												}
												else
												{
													if (FCConvert.ToString(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))) == "Dept")
													{
														if (modVariables.Statics.boolDeptDivFromBud)
														{
															SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")), intRow, intCol);
														}
														else
														{
															rsDeptDivInfo.OpenRecordset("SELECT * FROM tblDeptDiv WHERE Dept = '" + rsData.Get_Fields_String("Dept") + "'", "TWFA0000.vb1");
															if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
															{
																SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
															}
															else
															{
																SetCellWithData(intControl, "", intRow, intCol);
															}
														}
													}
													else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "Div")
													{
														if (modVariables.Statics.boolDeptDivFromBud)
														{
															SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), intRow, intCol);
														}
														else
														{
															SetCellWithData(intControl, Strings.Right(modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")).Length - modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")).Length + 1), intRow, intCol);
															if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
															{
																SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
															}
															else
															{
																SetCellWithData(intControl, "", intRow, intCol);
															}
														}
													}
													else
													{
														SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString())), intRow, intCol);
													}
												}
											}
										}
									}
									else
									{
										if (FCConvert.ToInt32(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)) >= 0)
										{
											// this will check to see if to merge the columns
											// not sure about this change but we ran into the case where
											// the data in the first two fields were the same but the fields
											// were different so we did NOT want to merge the two fields.
											// If rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))))) = rsData.Fields(CheckForAS(Trim(strFields(Val(frmCustomReport.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))))) Then
											if (Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]) == Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol - 1)))]))
											{
												if ((intControl + 2) > Detail.Controls.Count)
												{
													Detail.Controls[intControl].Width = Detail.Controls[intControl].Width;
												}
												else
												{
													Detail.Controls[intControl].Width += Detail.Controls[intControl + 1].Width;
												}
											}
											else
											{
												// ADD THE DATA TO THE CORRECT CELL IN THE GRID
												if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
												{
													SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString()), "MM/dd/yyyy"), intRow, intCol);
												}
												else
												{
													if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
													{
														SetCellWithData(intControl, string.Empty, intRow, intCol);
													}
													else
													{
														if (Strings.Left(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]), 14) == "tblMaster.Life")
														{
															// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
															if (FCConvert.ToInt32(rsData.Get_Fields("Life")) == 0)
															{
																SetCellWithData(intControl, string.Empty, intRow, intCol);
															}
															else
															{
																if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))) != "")
																{
																	// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
																	SetCellWithData(intControl, rsData.Get_Fields("Life") + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))), intRow, intCol);
																}
																else
																{
																	// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
																	SetCellWithData(intControl, rsData.Get_Fields("Life") + " Years", intRow, intCol);
																}
															}
														}
														else
														{
															if (FCConvert.ToString(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))) == "Dept")
															{
																if (modVariables.Statics.boolDeptDivFromBud)
																{
																	SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")), intRow, intCol);
																}
																else
																{
																	rsDeptDivInfo.OpenRecordset("SELECT * FROM tblDeptDiv WHERE Dept = '" + rsData.Get_Fields_String("Dept") + "'", "TWFA0000.vb1");
																	if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
																	{
																		SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
																	}
																	else
																	{
																		SetCellWithData(intControl, "", intRow, intCol);
																	}
																}
															}
															else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "Div")
															{
																if (modVariables.Statics.boolDeptDivFromBud)
																{
																	SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), intRow, intCol);
																}
																else
																{
																	SetCellWithData(intControl, Strings.Right(modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")).Length - modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")).Length + 1), intRow, intCol);
																	if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
																	{
																		SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
																	}
																	else
																	{
																		SetCellWithData(intControl, "", intRow, intCol);
																	}
																}
															}
															else
															{
																SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString())), intRow, intCol);
															}
														}
													}
												}
											}
										}
										else
										{
											// ADD THE DATA TO THE CORRECT CELL IN THE GRID
											if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
											{
												SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString()), "MM/dd/yyyy"), intRow, intCol);
											}
											else
											{
												if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
												{
													SetCellWithData(intControl, string.Empty, intRow, intCol);
												}
												else
												{
													if (Strings.Left(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]), 14) == "tblMaster.Life")
													{
														// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
														if (FCConvert.ToInt32(rsData.Get_Fields("Life")) == 0)
														{
															SetCellWithData(intControl, string.Empty, intRow, intCol);
														}
														else
														{
															if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))) != "")
															{
																// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
																SetCellWithData(intControl, rsData.Get_Fields("Life") + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))), intRow, intCol);
															}
															else
															{
																// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
																SetCellWithData(intControl, rsData.Get_Fields("Life") + " Years", intRow, intCol);
															}
														}
													}
													else
													{
														if (FCConvert.ToString(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))) == "Dept")
														{
															if (modVariables.Statics.boolDeptDivFromBud)
															{
																SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")), intRow, intCol);
															}
															else
															{
																rsDeptDivInfo.OpenRecordset("SELECT * FROM tblDeptDiv WHERE Dept = '" + rsData.Get_Fields_String("Dept") + "'", "TWFA0000.vb1");
																if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
																{
																	SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
																}
																else
																{
																	SetCellWithData(intControl, "", intRow, intCol);
																}
															}
														}
														else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "Div")
														{
															if (modVariables.Statics.boolDeptDivFromBud)
															{
																SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), intRow, intCol);
															}
															else
															{
																SetCellWithData(intControl, Strings.Right(modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")).Length - modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")).Length + 1), intRow, intCol);
																if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
																{
																	SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
																}
																else
																{
																	SetCellWithData(intControl, "", intRow, intCol);
																}
															}
														}
														else
														{
															SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString()).ToString()), intRow, intCol);
														}
													}
												}
											}
										}
									}
								}
							}
							else
							{
								// ADD THE DATA TO THE CORRECT CELL IN THE GRID
								if (FCConvert.ToDouble(modCustomReport.Statics.strWhereType[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]) == modCustomReport.GRIDDATE)
								{
									SetCellWithData(intControl, Strings.Format(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString()), "MM/dd/yyyy"), intRow, intCol);
								}
								else
								{
									if (FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) == string.Empty)
									{
										SetCellWithData(intControl, string.Empty, intRow, intCol);
									}
									else
									{
										if (Strings.Left(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol))]), 14) == "tblMaster.Life")
										{
											// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
											if (FCConvert.ToInt32(rsData.Get_Fields("Life")) == 0)
											{
												SetCellWithData(intControl, string.Empty, intRow, intCol);
											}
											else
											{
												if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))) != "")
												{
													// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
													SetCellWithData(intControl, rsData.Get_Fields("Life") + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("LifeUnits"))), intRow, intCol);
												}
												else
												{
													// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
													SetCellWithData(intControl, rsData.Get_Fields("Life") + " Years", intRow, intCol);
												}
											}
										}
										else
										{
											if (FCConvert.ToString(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))) == "Dept")
											{
												if (modVariables.Statics.boolDeptDivFromBud)
												{
													SetCellWithData(intControl, modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")), intRow, intCol);
												}
												else
												{
													rsDeptDivInfo.OpenRecordset("SELECT * FROM tblDeptDiv WHERE Dept = '" + rsData.Get_Fields_String("Dept") + "'", "TWFA0000.vb1");
													if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
													{
														SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
													}
													else
													{
														SetCellWithData(intControl, "", intRow, intCol);
													}
												}
											}
											else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "Div")
											{
												if (modVariables.Statics.boolDeptDivFromBud)
												{
													SetCellWithData(intControl, Strings.Right(modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")).Length - modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")).Length + 1), intRow, intCol);
												}
												else
												{
													SetCellWithData(intControl, Strings.Right(modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")), modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept") + "-" + rsData.Get_Fields_String("Div")).Length - modAccountTitle.ReturnAccountDescription("E " + rsData.Get_Fields_String("Dept")).Length + 1), intRow, intCol);
													if (rsDeptDivInfo.EndOfFile() != true && rsDeptDivInfo.BeginningOfFile() != true)
													{
														SetCellWithData(intControl, rsDeptDivInfo.Get_Fields_String("Description"), intRow, intCol);
													}
													else
													{
														SetCellWithData(intControl, "", intRow, intCol);
													}
												}
											}
											else
											{
												SetCellWithData(intControl, FCConvert.ToString(rsData.Get_Fields(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])).ToString())), intRow, intCol);
											}
										}
									}
								}
							}
						}
						if (FCConvert.ToInt32(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) >= 0)
						{
							(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Alignment = (GrapeCity.ActiveReports.Document.Section.TextAlignment)FCConvert.ToInt32(modCustomReport.Statics.strJustify[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]);
						}
						if (strMessage[intControl].boolDontContinue)
						{
							eArgs.EOF = true;
							return;
						}
					}
				}
				rsData.MoveNext();
				eArgs.EOF = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Unable to build custom report. Close custom report screen and start again.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				eArgs.EOF = true;
			}
		}

		private void SetCellWithData(int intCounter, string strData, int intRow, int intCol)
		{
			bool blnFormatCurrency;
			blnFormatCurrency = false;
			if (FCConvert.ToString(modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))]))) == "BasisCost")
			{
				TotalCostCol = intCounter;
				curTotalCost += Conversion.CCur(strData);
				blnCostTotalsExist = true;
				blnFormatCurrency = true;
			}
			else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "TotalValue")
			{
				TotalCurrentValueCol = intCounter;
				curTotalCurrentValue += Conversion.CCur(strData);
				blnCurrentValueTotalsExist = true;
				blnFormatCurrency = true;
			}
			else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "TotalDepreciated")
			{
				TotalDepreciationCol = intCounter;
				curTotalDepreciation += Conversion.CCur(strData);
				blnDepreciationTotalsExist = true;
				blnFormatCurrency = true;
			}
			else if (modCustomReport.CheckForAS(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "SalvageValue")
			{
				blnFormatCurrency = true;
			}
			if (Strings.UCase(Strings.Trim(modCustomReport.Statics.strFields[FCConvert.ToInt32(FCConvert.ToString(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)))])) == "LEGITIMATE")
			{
				if (strMessage[intCounter].boolShown)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = (!FCConvert.CBool(strData)).ToString();
				}
				else
				{
					if (MessageBox.Show("A BOW record is about to be shown. Continue with report?", "TRIO Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = (!FCConvert.CBool(strData)).ToString();
						strMessage[intCounter].boolShown = true;
						strMessage[intCounter].boolDontContinue = false;
					}
					else
					{
						strMessage[intCounter].boolDontContinue = true;
					}
				}
			}
			else
			{
				if (blnFormatCurrency)
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(strData, "#,##0.00");
				}
				else
				{
					(Detail.Controls[intCounter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strData;
				}
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			// SHOW THE PAGE NUMBER
			txtPage.Text = "Page " + FCConvert.ToString(intPageNumber);
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			// INCREMENT THE PAGE NUMBER TO DISPLAY ON THE REPORT
			intPageNumber += 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SET UP THE INFORMATION ON THE TOP OF THE REPORT SUCH AS TITLE,
			// MUNINAME, DATE AND TIME
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			intPageNumber = 1;
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			txtTitle.Text = modCustomReport.Statics.strReportHeading;
			curTotalCost = 0;
			curTotalCurrentValue = 0;
			curTotalDepreciation = 0;
			blnCostTotalsExist = false;
			blnCurrentValueTotalsExist = false;
			blnDepreciationTotalsExist = false;
			if (frmCustomReport.InstancePtr.cmbPortrait.SelectedIndex == 0)
			{
				// do nothing
			}
			else
			{
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				this.PrintWidth = 14000 / 1440f;
				Line1.X2 = this.PrintWidth;
				txtTitle.Width = this.PrintWidth;
				txtDate.Left = this.PrintWidth - txtDate.Width;
				txtPage.Left = this.PrintWidth - txtPage.Width;
			}
			rsData.OpenRecordset(modCustomReport.Statics.strCustomSQL, modGlobal.DatabaseNamePath);
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			//dblWidthRatio = (frmCustomReport.InstancePtr.Line2.X2 - frmCustomReport.InstancePtr.Line2.X1) / 1440;
			// dblWidthRatio = 1
			CreateHeaderFields();
			CreateDataFields();
			// SET THE DISPLAY ZOOM TO E THE PAGE WIDTH
			//this.Zoom = -1;
			modPrintToFile.SetPrintProperties(this);
		}
		//FC:FINAL:SBE - #352 - show/activate custom report form after custom report was closed
		private void RptCustomReport_Disposed(object sender, System.EventArgs e)
		{
			frmCustomReport.InstancePtr.Show(App.MainForm);
		}

		private void CreateHeaderFields()
		{
			int intControlNumber = 0;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 0; intRow <= frmCustomReport.InstancePtr.vsLayout.Rows - 1; intRow++)
			{
				for (intCol = 0; intCol <= frmCustomReport.InstancePtr.vsLayout.Cols - 1; intCol++)
				{
					intControlNumber += 1;
                    NewField = PageHeader.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtHeader" + FCConvert.ToString(intControlNumber));
					//NewField.Name = "txtHeader" + FCConvert.ToString(intControlNumber);
					NewField.Top = ((intRow * frmCustomReport.InstancePtr.vsLayout.RowHeight(0)) + 540) / 1440f;
					NewField.Left = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / FCConvert.ToSingle(dblWidthRatio) / 1440f;
					if (intCol > 0)
					{
						if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) == frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol - 1))
						{
							NewField.Text = string.Empty;
						}
						else
						{
							NewField.Text = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
						}
					}
					else
					{
						NewField.Text = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					if (frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol) != null && FCConvert.ToInt32(frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)) >= 0)
					{
						NewField.Alignment = (GrapeCity.ActiveReports.Document.Section.TextAlignment)Conversion.Val(modCustomReport.Statics.strJustify[frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, intRow, intCol)]);
					}
					// if this field is right justified then I want to add a space
					// so that  this field caption will not run into the next column
					// If NewField.Alignment = 1 Then NewField.Text = NewField.Text & " "
					NewField.Width = FCConvert.ToSingle(frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol) / dblWidthRatio / 1440f);
					NewField.WordWrap = false;
					// make this field bold as it is a caption
					NewField.Font = new Font(NewField.Font, FontStyle.Bold);
				}
			}
			// ADD A LINE SEPERATOR BETWEEN THE CAPTIONS AND THE DATA THAT
			// IS DISPLAYED
			Line1.X1 = 0;
			Line1.X2 = this.PrintWidth / 1440F;
			Line1.Y1 = ((intRow * frmCustomReport.InstancePtr.vsLayout.RowHeight(0)) + 540) / 1440f;
			Line1.Y2 = Line1.Y1;
			this.PageHeader.Height = Line1.Y1 + 300 / 1440f;
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			for (intRow = 1; intRow <= frmCustomReport.InstancePtr.vsLayout.Rows - 1; intRow++)
			{
				for (intCol = 0; intCol <= frmCustomReport.InstancePtr.vsLayout.Cols - 1; intCol++)
				{
					NewField = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol));
					//Detail.Controls.Add(NewField);
					//NewField.Name = "txtData" + FCConvert.ToString(intRow) + FCConvert.ToString(intCol);
					NewField.Top = ((intRow - 1) * frmCustomReport.InstancePtr.vsLayout.RowHeight(0) + 50) / 1440f;
					NewField.Left = frmCustomReport.InstancePtr.vsLayout.Cell(FCGrid.CellPropertySettings.flexcpLeft, intRow, intCol) / FCConvert.ToSingle(dblWidthRatio) / 1440f;
					NewField.Width = FCConvert.ToSingle(frmCustomReport.InstancePtr.vsLayout.ColWidth(intCol) / FCConvert.ToSingle(dblWidthRatio) / 1440f);
					// NewField.Height = frmCustomReport.vsLayout.RowHeight(1)
					NewField.Text = string.Empty;
					NewField.WordWrap = true;
				}
			}
		}
		//public void ActiveReport_ToolbarClick(DDActiveReports2.DDTool Tool)
		//{
		//	modPrintToFile.VerifyPrintToFile(this, ref Tool);
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= Detail.Controls.Count - 1; intCounter++)
			{
				if (Detail.Controls[intCounter].Tag != null && Detail.Controls[intCounter].Tag != string.Empty)
				{
					if (Strings.Left(Detail.Controls[intCounter].Name, 7) == "txtData")
					{
						if (Conversion.Val(Detail.Controls[intCounter].Tag) <= this.PrintWidth)
						{
							// MsgBox Val(Detail.Controls[intCounter].Tag)
							Detail.Controls[intCounter].Width = FCConvert.ToSingle(Conversion.Val(Detail.Controls[intCounter].Tag));
						}
						else
						{
							Detail.Controls[intCounter].Width = this.PrintWidth;
						}
					}
				}
			}
			Detail.Height = frmCustomReport.InstancePtr.vsLayout.RowHeight(0) * (frmCustomReport.InstancePtr.vsLayout.Rows - 1) / 1440f;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			int counter = 0;
			if (blnCostTotalsExist != true && blnCurrentValueTotalsExist != true && blnDepreciationTotalsExist != true)
			{
				lblTotals.Visible = false;
				lblTotalCost.Visible = false;
				lblTotalCurrentValue.Visible = false;
				lblTotalDepreciation.Visible = false;
				fldTotalCost.Visible = false;
				fldTotalCurrentValue.Visible = false;
				fldTotalDepreciation.Visible = false;
			}
			else
			{
				if (blnCostTotalsExist != true)
				{
					lblTotalCost.Visible = false;
					fldTotalCost.Visible = false;
				}
				if (blnCurrentValueTotalsExist != true)
				{
					lblTotalCurrentValue.Visible = false;
					fldTotalCurrentValue.Visible = false;
				}
				if (blnDepreciationTotalsExist != true)
				{
					lblTotalDepreciation.Visible = false;
					fldTotalDepreciation.Visible = false;
				}
				counter = 1;
				if (blnCostTotalsExist)
				{
					fldTotalCost.Text = Strings.Format(curTotalCost, "#,##0.00");
					counter = 2;
				}
				if (blnCurrentValueTotalsExist)
				{
					if (counter == 1)
					{
						lngNextTop = fldTotalCurrentValue.Top;
						lblTotalCurrentValue.Top = lblTotalCost.Top;
						fldTotalCurrentValue.Top = fldTotalCost.Top;
						counter = 2;
					}
					else
					{
						counter = 3;
					}
					fldTotalCurrentValue.Text = Strings.Format(curTotalCurrentValue, "#,##0.00");
				}
				if (blnDepreciationTotalsExist)
				{
					if (counter == 1)
					{
						lblTotalDepreciation.Top = lblTotalCost.Top;
						fldTotalDepreciation.Top = fldTotalCost.Top;
					}
					else if (counter == 2)
					{
						lblTotalDepreciation.Top = lngNextTop;
						fldTotalDepreciation.Top = lngNextTop;
					}
					fldTotalDepreciation.Text = Strings.Format(curTotalDepreciation, "#,##0.00");
				}
			}
		}

		private void rptCustomReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomReport.Caption	= "Custom Report";
			//rptCustomReport.Icon	= "rptCustomReport.dsx":0000";
			//rptCustomReport.Left	= 0;
			//rptCustomReport.Top	= 0;
			//rptCustomReport.Width	= 11880;
			//rptCustomReport.Height	= 8595;
			//rptCustomReport.StartUpPosition	= 3;
			//rptCustomReport.SectionData	= "rptCustomReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
