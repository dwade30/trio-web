﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmClassCodes.
	/// </summary>
	public partial class frmClassCodes : BaseForm
	{
		public frmClassCodes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmClassCodes InstancePtr
		{
			get
			{
				return (frmClassCodes)Sys.GetInstance(typeof(frmClassCodes));
			}
		}

		protected frmClassCodes _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/30/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		string strSort = "";
		bool boolDataChanged;
		const string GRIDCOLUMN0 = "ID";
		const string GRIDCOLUMN1 = "Changed";
		const string GRIDCOLUMN2 = "Code";
		const string GRIDCOLUMN3 = "Description";
		const string GRIDCOLUMN4 = "Life";

		private void SetGridProperties()
		{
			vsGrid.Cols = 5;
			vsGrid.FixedCols = 0;
			vsGrid.FixedRows = 1;
			vsGrid.Rows = 1;
			vsGrid.ColHidden(0, true);
			vsGrid.ColHidden(1, true);
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.75));
            //vsGrid.ColWidth(4, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
            vsGrid.ExtendLastCol = true;
			//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsGrid.TextMatrix(0, 0, GRIDCOLUMN0);
			vsGrid.TextMatrix(0, 1, GRIDCOLUMN1);
			vsGrid.TextMatrix(0, 2, GRIDCOLUMN2);
			vsGrid.TextMatrix(0, 3, GRIDCOLUMN3);
			vsGrid.TextMatrix(0, 4, GRIDCOLUMN4);
			vsGrid.Select(0, 0);
		}

		private void frmClassCodes_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmClassCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				KeyCode = (Keys)0;
				mnuNew_Click();
			}
		}

		private void frmClassCodes_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmClassCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmClassCodes.ScaleWidth	= 5880;
			//frmClassCodes.ScaleHeight	= 4260;
			//frmClassCodes.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetGridProperties();
			LoadRecord();
			vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			//FC:FINAL:SBE - #248 - set allowed keys for client side check. KeyPress on server side does not support key restrictions
			vsGrid.ColAllowedKeys(4, "'Left','Right','Delete','Home','End','Insert','Backspace','Tab','Enter','.',48..57");
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			vsGrid.Select(0, 0);
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							modVariables.Statics.gboolCancelSelected = false;
							if (SaveRecord())
							{
								e.Cancel = false;
							}
							else
							{
								e.Cancel = true;
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							modVariables.Statics.gboolCancelSelected = false;
							Close();
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							modVariables.Statics.gboolCancelSelected = true;
							e.Cancel = true;
							this.Focus();
							break;
						}
				}
				//end switch
			}
			else
			{
				Close();
			}
		}

		private void frmClassCodes_Resize(object sender, System.EventArgs e)
		{
			vsGrid.ColWidth(2, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.12));
			vsGrid.ColWidth(3, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.75));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void vsGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			vsGrid.TextMatrix(vsGrid.Row, 1, FCConvert.ToString(1));
			boolDataChanged = true;
		}

		private void vsGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsGrid.MouseRow == 0)
			{
				// sort this grid
				if (vsGrid.Rows < 2)
					return;
				vsGrid.Select(1, vsGrid.Col);
				if (strSort == "flexSortGenericAscending")
				{
					vsGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					strSort = "flexSortGenericDescending";
				}
				else
				{
					vsGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					strSort = "flexSortGenericAscending";
				}
			}
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					(obj as MaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (FCConvert.ToBoolean(boolClear))
						(obj as FCComboBox).Clear();
				}
			}
			// obj
			vsGrid.Rows = 1;
			tmpString = new object[3 + 1];
		}

		private bool SaveRecord()
		{
			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 0)) != 0)
					{
						if (Strings.Trim(vsGrid.TextMatrix(intCounter, 2)) == "")
						{
							MessageBox.Show("You must enter a class code before you may proceed.", "Invalid Class Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							vsGrid.Select(intCounter, 2);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(vsGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a class description before you may proceed.", "Invalid Class Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							vsGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
					}
					else
					{
						if (Strings.Trim(vsGrid.TextMatrix(intCounter, 2)) == "" && Strings.Trim(vsGrid.TextMatrix(intCounter, 3)) == "")
						{
							// do nothing
						}
						else if (Strings.Trim(vsGrid.TextMatrix(intCounter, 2)) == "")
						{
							MessageBox.Show("You must enter a class code before you may proceed.", "Invalid Class Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							vsGrid.Select(intCounter, 2);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(vsGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a class description before you may proceed.", "Invalid Class Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							vsGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
					}
				}
				for (intCounter = 1; intCounter <= vsGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 1)) == 1)
					{
						// then this record has changed
						if (FCConvert.ToDouble(vsGrid.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							if (Strings.Trim(vsGrid.TextMatrix(intCounter, 2)) == "" && Strings.Trim(vsGrid.TextMatrix(intCounter, 3)) == "")
							{
								// dont save record if no code or description are entered
							}
							else
							{
								rsData.Execute("Insert into tblClassCodes (Code,Description,Life) VALUES ('" + Strings.Trim(modGlobalRoutines.FixQuote(vsGrid.TextMatrix(intCounter, 2))) + "','" + Strings.Trim(modGlobalRoutines.FixQuote(vsGrid.TextMatrix(intCounter, 3))) + "','" + Strings.Trim(vsGrid.TextMatrix(intCounter, 4)) + "')", modGlobal.DatabaseNamePath);
							}
						}
						else
						{
							// this is an edit record
							rsData.OpenRecordset("Select * from tblClassCodes where ID = " + vsGrid.TextMatrix(intCounter, 0), modGlobal.DatabaseNamePath);
							if (rsData.EndOfFile())
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
							rsData.Set_Fields("Code", Strings.Trim(modGlobalRoutines.FixQuote(vsGrid.TextMatrix(intCounter, 2))));
							rsData.Set_Fields("Description", Strings.Trim(modGlobalRoutines.FixQuote(vsGrid.TextMatrix(intCounter, 3))));
							rsData.Set_Fields("Life", Strings.Trim(vsGrid.TextMatrix(intCounter, 4)));
							rsData.Update();
						}
					}
				}
				if (boolDataChanged)
					LoadRecord();
				SaveRecord = true;
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveRecord;
		}

		private void LoadRecord()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			vsGrid.Rows = 1;
			// this is an edit record
			rsData.OpenRecordset("Select * from tblClassCodes Order by convert(int, code), Code", modGlobal.DatabaseNamePath);
			vsGrid.Rows = rsData.RecordCount() + 1;
			for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
			{
				vsGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				vsGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				vsGrid.TextMatrix(intCounter, 2, Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))));
				vsGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Description"))));
				// TODO Get_Fields: Check the table for the column [Life] and replace with corresponding Get_Field method
				vsGrid.TextMatrix(intCounter, 4, Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Life"))));
				rsData.MoveNext();
			}
			if (vsGrid.Rows > 1)
			{
				vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, vsGrid.Rows - 1, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			boolDataChanged = false;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			int intRow = 0;
			int intCol = 0;
			if (vsGrid.Row > 0)
			{
				intRow = vsGrid.Row;
				intCol = vsGrid.Col;
				if (MessageBox.Show("Are you sure you wish to delete this item?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (FCConvert.ToDouble(vsGrid.TextMatrix(vsGrid.Row, 0)) == 0)
					{
						vsGrid.RemoveItem(vsGrid.Row);
					}
					else
					{
						rsData.Execute("Delete from tblClassCodes where ID = " + vsGrid.TextMatrix(vsGrid.Row, 0), modGlobal.DatabaseNamePath);
						vsGrid.RemoveItem(vsGrid.Row);
					}
					if (intRow <= vsGrid.Rows - 1)
					{
						vsGrid.Select(intRow, intCol);
					}
					else if (vsGrid.Rows > 1)
					{
						vsGrid.Select(intRow - 1, intCol);
					}
					MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			int lngNextNumber;
			clsDRWrapper rsData = new clsDRWrapper();
			vsGrid.Rows += 1;
			vsGrid.TextMatrix(vsGrid.Rows - 1, 0, FCConvert.ToString(0));
			vsGrid.TextMatrix(vsGrid.Rows - 1, 1, FCConvert.ToString(1));
			vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, vsGrid.Rows - 1, 2, vsGrid.Rows - 1, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.Select(vsGrid.Rows - 1, 2);
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			vsGrid.Select(0, 0);
			SaveRecord();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			vsGrid.Select(0, 0);
			if (SaveRecord())
				Close();
		}

		private void vsGrid_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (vsGrid.Row == vsGrid.Rows - 1 && vsGrid.Col == vsGrid.Cols - 1)
				{
					keyAscii = 0;
					mnuNew_Click();
					//Application.DoEvents();
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void vsGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (vsGrid.Col == 4)
			{
				if ((keyAscii < 48 || keyAscii > 57) && keyAscii != 8)
				{
					keyAscii = 0;
				}
			}
			if (keyAscii == 13)
			{
				if (vsGrid.Row == vsGrid.Rows - 1 && vsGrid.Col == vsGrid.Cols - 1)
				{
					keyAscii = 0;
					mnuNew_Click();
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void vsGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int row = vsGrid.GetFlexRowIndex(e.RowIndex);
			int col = vsGrid.GetFlexColIndex(e.ColumnIndex);
			if (col == 2)
			{
				if (modGlobalRoutines.DuplicateFound(2, row, vsGrid, vsGrid.EditText))
				{
					MessageBox.Show("This class code already exists.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					vsGrid.TextMatrix(row, 2, string.Empty);
					e.Cancel = true;
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuNew_Click(mnuNew, EventArgs.Empty);
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			mnuDelete_Click(mnuDelete, EventArgs.Empty);
		}
	}
}
