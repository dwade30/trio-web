﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace Cryptography    
{
    public class CryptoUtility
    {
        public static string ComputeSHA512Hash(string strStringToHash, string strSalt)
        {
            return ComputeSHA512Hash(strStringToHash + strSalt);
        }

        public static string ComputeSHA512Hash(string strStringToHash)
        {
            SHA512Managed tSHA = new SHA512Managed();
            byte[] tBytes = Encoding.ASCII.GetBytes(strStringToHash);
            byte[] hashBytes = ComputeSHA512HashBytes(tBytes);
            if (hashBytes != null)
                return Convert.ToBase64String(hashBytes);
            else
                return null;
        }

        public static byte[] ComputeSHA512HashBytes(byte[] bytBytesToHash)
        {
            SHA512Managed tSHA = new SHA512Managed();
            return tSHA.ComputeHash(bytBytesToHash);
        }

        public static string EncryptByPassword(string strToEncrypt, string strPassword, string strSalt)
        {
            return EncryptByPassword(strToEncrypt, strPassword, strSalt, 2, "", KeySize.Bits256);
        }

        public static byte[] KeyFromPassword(string strPassword, string strSalt)
        {
            return KeyFromPassword(strPassword, strSalt, 2, KeySize.Bits256);
        }

        public static string GetRandomSalt()
        {
            byte[] saltBytes;
            saltBytes = GetRandomSaltBytes();
            if (saltBytes != null)
                return Convert.ToBase64String(saltBytes);
            else
                return "";
        }

        public static byte[] GetRandomSaltBytes()
        {
            try
            {
                int intMinSaltSize = 5;
                int intMaxSaltSize = 10;
                Random r = new Random();
                int intSaltSize = r.Next(intMinSaltSize, intMaxSaltSize);
                byte[] saltbytes = new Byte[intSaltSize - 1];
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                rng.GetNonZeroBytes(saltbytes);
                return saltbytes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static byte[] KeyFromPassword(string strPassword, string strSalt, int intPasswordIterations, KeySize intKeySize)
        {
            if (String.IsNullOrEmpty(strPassword))
                return null;
            byte[] SaltBytes = Convert.FromBase64String(strSalt);
            Rfc2898DeriveBytes DerivedPassword = new Rfc2898DeriveBytes(strPassword, SaltBytes, intPasswordIterations);
            int intSize = 256;
            switch(intKeySize)
            {
                case KeySize.Bits128:
                    intSize = 128;
                    break;
                case KeySize.Bits192:
                    intSize = 192;
                    break;
                case KeySize.Bits256:
                    intSize = 256;
                    break;
                default:
                    intSize = 256;
                    break;
            }
            byte[] KeyBytes = DerivedPassword.GetBytes(intSize / 8);
            return KeyBytes;
        }

        public static byte[] Encrypt(string strToEncrypt, byte[] keybytes, byte[] InitialVectorBytes)
        {
            if (String.IsNullOrEmpty(strToEncrypt))
            {
                return null;
            }
            try
            {
                //aesAlg.KeySize = intSize;
                using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
                {
                    aesAlg.Key = keybytes;
                    aesAlg.IV = InitialVectorBytes;
                    byte[] encrypted;
                    // string strEncrypted = "";
                    ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    MemoryStream msEncrypt = new MemoryStream();
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(strToEncrypt);
                        }
                        encrypted = msEncrypt.ToArray();
                        return encrypted;
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string GenerateKey()
        {
            using (AesCryptoServiceProvider aesalg = new AesCryptoServiceProvider())
            {
                return Convert.ToBase64String(aesalg.Key);
            }
        }

        public static string GenerateInitializationVector()
        {
            using (AesCryptoServiceProvider aesalg = new AesCryptoServiceProvider())
            {
                return Convert.ToBase64String(aesalg.IV);
            }
        }

        public static string Encrypt(string strToEncrypt, string strKey, string strInitialVector)
        {
            if (String.IsNullOrEmpty(strKey))
            {
                return null;
            }
            if (String.IsNullOrEmpty(strInitialVector))
            {
                return null;
            }
            byte[] KeyBytes = Convert.FromBase64String(strKey);

            byte[] IVBytes = Convert.FromBase64String(strInitialVector);
            byte[] encoded = null;
            encoded = Encrypt(strToEncrypt, KeyBytes, IVBytes);
            if (encoded != null)
            {
                string strEncrypted = "";
                strEncrypted = Convert.ToBase64String(encoded);
                return strEncrypted;
            }
            else
            {
                return "";
            }
        }

        public static string EncryptByPassword(string strToEncrypt, string strPassword, string strSalt, int intPasswordIterations, string strInitialVector, KeySize intKeySize)
        {
            if (String.IsNullOrEmpty(strToEncrypt))
            {
                return String.Empty;
            }
            try
            {
                byte[] InitialVectorBytes;   //  '= Encoding.ASCII.GetBytes(strInitialVector)               
                byte[] keybytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, intKeySize);
                if (strInitialVector != String.Empty)
                {
                    InitialVectorBytes = Convert.FromBase64String(strInitialVector);  // 'strinitialvector needs to be 128 bits
                }
                else
                {
                    InitialVectorBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, KeySize.Bits128);  // 'initial vector must be 128 bits
                }

                byte[] encrypted;
                encrypted = Encrypt(strToEncrypt, keybytes, InitialVectorBytes);
                if (encrypted != null)
                {
                    string strEncrypted = String.Empty;
                    strEncrypted = Convert.ToBase64String(encrypted);
                    return strEncrypted;
                }
                else
                {
                    return String.Empty;
                }
            }
            catch (Exception ex)
            {
                return String.Empty;
            }            
        }

        public static string DecryptByPassword(string strToDecrypt, string strPassword, string strSalt)
        {
            return DecryptByPassword(strToDecrypt, strPassword, strSalt, String.Empty, 2, KeySize.Bits256);
        }

        public static string Decrypt(string strToDecrypt, string strKey, string strInitialVector)
        {
            if (String.IsNullOrEmpty(strToDecrypt))
            {
                return String.Empty;
            }
            if (String.IsNullOrEmpty(strKey))
            {
                return String.Empty;
            }
            if (String.IsNullOrEmpty(strInitialVector))
            {
                return String.Empty;
            }
            byte[] KeyBytes = Convert.FromBase64String(strKey);
            byte[] IVBytes = Convert.FromBase64String(strInitialVector);
            byte[] ToDecryptBytes = Convert.FromBase64String(strToDecrypt);
            return Decrypt(ToDecryptBytes, KeyBytes, IVBytes);
        }

        public static string Decrypt(byte[] ToDecryptBytes, byte[] keybytes, byte[] InitialVectorBytes)
        {
            string strReturn = String.Empty;
            if (ToDecryptBytes == null)
            {
                return strReturn;
            }
            if (ToDecryptBytes.LongLength < 1)
            {
                return strReturn;
            }
            try
            {
                using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
                {
                    aesAlg.Key = keybytes;
                    aesAlg.IV = InitialVectorBytes;
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (MemoryStream msDecrypt = new MemoryStream(ToDecryptBytes))
                    {
                        using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {
                            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            {
                                strReturn = srDecrypt.ReadToEnd();
                            }
                        }
                    }
                }
                return strReturn;
            }
            catch (Exception ex)
            {
                return String.Empty;
            }
        }

        public static string DecryptByPassword(string strToDecrypt, string strPassword, string strSalt, string strInitialVector, int intPasswordIterations, KeySize intKeySize)
        {
            string strReturn = String.Empty;
            if (String.IsNullOrEmpty(strToDecrypt))
            {
                return strReturn;
            }
            try
            {
                byte[] InitialVectorBytes;
                byte[] SaltBytes = Convert.FromBase64String(strSalt);
                Rfc2898DeriveBytes DerivedPassword = new Rfc2898DeriveBytes(strPassword, SaltBytes, intPasswordIterations);
                byte[] ToDecryptBytes;
                ToDecryptBytes = Convert.FromBase64String(strToDecrypt);
                /*
                'Dim intSize As Integer = 256
                'Select Case intKeySize
                '    Case KeySize.Bits128
                '        intSize = 128
                '    Case KeySize.Bits192
                '        intSize = 192
                '    Case KeySize.Bits256
                '        intSize = 256
                '    Case Else
                '        intSize = 256
                'End Select
                'Dim KeyBytes As Byte() = DerivedPassword.GetBytes(intSize / 8)
                'If strInitialVector <> "" Then
                '    InitialVectorBytes = Encoding.ASCII.GetBytes(strInitialVector) 'strinitialvector needs to be 128 bits
                'Else
                '    InitialVectorBytes = DerivedPassword.GetBytes(16) ' 128 / 8
                'End If
                 */
                byte[]KeyBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, intKeySize);
                if (strInitialVector != String.Empty)
                {
                    InitialVectorBytes = Convert.FromBase64String(strInitialVector);
                }
                else
                {
                    InitialVectorBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, KeySize.Bits128);   // ' initial vector must be 128 bits
                }
                return Decrypt(ToDecryptBytes, KeyBytes, InitialVectorBytes);
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }

    public enum KeySize
    {
        Bits128,
        Bits192,
        Bits256
    };
}
