﻿namespace TRIOConfigEditor
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.grpServer = new System.Windows.Forms.GroupBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.lblUserName = new System.Windows.Forms.Label();
			this.txtUserName = new System.Windows.Forms.TextBox();
			this.lblDataSource = new System.Windows.Forms.Label();
			this.txtDataSource = new System.Windows.Forms.TextBox();
			this.grpGlobal = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lblBackupDir = new System.Windows.Forms.Label();
			this.txtMasterDir = new System.Windows.Forms.TextBox();
			this.txtBackupDir = new System.Windows.Forms.TextBox();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.mnuStrip = new System.Windows.Forms.MenuStrip();
			this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuFileNew = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuFileOpen = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuFileSave = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.mnuFileExit = new System.Windows.Forms.ToolStripMenuItem();
			this.gridEnvs = new System.Windows.Forms.DataGridView();
			this.grpEnvs = new System.Windows.Forms.GroupBox();
			this.lblError = new System.Windows.Forms.Label();
			this.gpbClients = new System.Windows.Forms.GroupBox();
			this.btnDelete = new System.Windows.Forms.Button();
			this.btnAdd = new System.Windows.Forms.Button();
			this.grdClients = new System.Windows.Forms.DataGridView();
			this.chkHarrisStaff = new System.Windows.Forms.CheckBox();
			this.grpServer.SuspendLayout();
			this.grpGlobal.SuspendLayout();
			this.mnuStrip.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridEnvs)).BeginInit();
			this.grpEnvs.SuspendLayout();
			this.gpbClients.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.grdClients)).BeginInit();
			this.SuspendLayout();
			// 
			// grpServer
			// 
			this.grpServer.Controls.Add(this.lblPassword);
			this.grpServer.Controls.Add(this.txtPassword);
			this.grpServer.Controls.Add(this.lblUserName);
			this.grpServer.Controls.Add(this.txtUserName);
			this.grpServer.Controls.Add(this.lblDataSource);
			this.grpServer.Controls.Add(this.txtDataSource);
			this.grpServer.Location = new System.Drawing.Point(44, 194);
			this.grpServer.Margin = new System.Windows.Forms.Padding(2);
			this.grpServer.Name = "grpServer";
			this.grpServer.Padding = new System.Windows.Forms.Padding(2);
			this.grpServer.Size = new System.Drawing.Size(464, 130);
			this.grpServer.TabIndex = 0;
			this.grpServer.TabStop = false;
			this.grpServer.Text = "Server Configuration";
			// 
			// lblPassword
			// 
			this.lblPassword.AutoSize = true;
			this.lblPassword.Location = new System.Drawing.Point(32, 99);
			this.lblPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(56, 13);
			this.lblPassword.TabIndex = 5;
			this.lblPassword.Text = "Password:";
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(148, 95);
			this.txtPassword.Margin = new System.Windows.Forms.Padding(2);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.PasswordChar = '*';
			this.txtPassword.Size = new System.Drawing.Size(193, 20);
			this.txtPassword.TabIndex = 4;
			// 
			// lblUserName
			// 
			this.lblUserName.AutoSize = true;
			this.lblUserName.Location = new System.Drawing.Point(32, 64);
			this.lblUserName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblUserName.Name = "lblUserName";
			this.lblUserName.Size = new System.Drawing.Size(63, 13);
			this.lblUserName.TabIndex = 3;
			this.lblUserName.Text = "User Name:";
			// 
			// txtUserName
			// 
			this.txtUserName.Location = new System.Drawing.Point(148, 60);
			this.txtUserName.Margin = new System.Windows.Forms.Padding(2);
			this.txtUserName.Name = "txtUserName";
			this.txtUserName.Size = new System.Drawing.Size(193, 20);
			this.txtUserName.TabIndex = 2;
			// 
			// lblDataSource
			// 
			this.lblDataSource.AutoSize = true;
			this.lblDataSource.Location = new System.Drawing.Point(32, 29);
			this.lblDataSource.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblDataSource.Name = "lblDataSource";
			this.lblDataSource.Size = new System.Drawing.Size(70, 13);
			this.lblDataSource.TabIndex = 1;
			this.lblDataSource.Text = "Data Source:";
			// 
			// txtDataSource
			// 
			this.txtDataSource.BackColor = System.Drawing.SystemColors.Window;
			this.txtDataSource.Location = new System.Drawing.Point(148, 25);
			this.txtDataSource.Margin = new System.Windows.Forms.Padding(2);
			this.txtDataSource.Name = "txtDataSource";
			this.txtDataSource.Size = new System.Drawing.Size(193, 20);
			this.txtDataSource.TabIndex = 0;
			// 
			// grpGlobal
			// 
			this.grpGlobal.Controls.Add(this.chkHarrisStaff);
			this.grpGlobal.Controls.Add(this.label1);
			this.grpGlobal.Controls.Add(this.lblBackupDir);
			this.grpGlobal.Controls.Add(this.txtMasterDir);
			this.grpGlobal.Controls.Add(this.txtBackupDir);
			this.grpGlobal.Location = new System.Drawing.Point(44, 329);
			this.grpGlobal.Margin = new System.Windows.Forms.Padding(2);
			this.grpGlobal.Name = "grpGlobal";
			this.grpGlobal.Padding = new System.Windows.Forms.Padding(2);
			this.grpGlobal.Size = new System.Drawing.Size(464, 128);
			this.grpGlobal.TabIndex = 1;
			this.grpGlobal.TabStop = false;
			this.grpGlobal.Text = "Global Settings";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(32, 63);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(87, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Master Directory:";
			// 
			// lblBackupDir
			// 
			this.lblBackupDir.AutoSize = true;
			this.lblBackupDir.Location = new System.Drawing.Point(32, 29);
			this.lblBackupDir.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblBackupDir.Name = "lblBackupDir";
			this.lblBackupDir.Size = new System.Drawing.Size(92, 13);
			this.lblBackupDir.TabIndex = 2;
			this.lblBackupDir.Text = "Backup Directory:";
			// 
			// txtMasterDir
			// 
			this.txtMasterDir.Location = new System.Drawing.Point(148, 58);
			this.txtMasterDir.Margin = new System.Windows.Forms.Padding(2);
			this.txtMasterDir.Name = "txtMasterDir";
			this.txtMasterDir.Size = new System.Drawing.Size(193, 20);
			this.txtMasterDir.TabIndex = 1;
			// 
			// txtBackupDir
			// 
			this.txtBackupDir.Location = new System.Drawing.Point(148, 25);
			this.txtBackupDir.Margin = new System.Windows.Forms.Padding(2);
			this.txtBackupDir.Name = "txtBackupDir";
			this.txtBackupDir.Size = new System.Drawing.Size(193, 20);
			this.txtBackupDir.TabIndex = 0;
			// 
			// mnuStrip
			// 
			this.mnuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile});
			this.mnuStrip.Location = new System.Drawing.Point(0, 0);
			this.mnuStrip.Name = "mnuStrip";
			this.mnuStrip.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
			this.mnuStrip.Size = new System.Drawing.Size(550, 24);
			this.mnuStrip.TabIndex = 2;
			this.mnuStrip.Text = "menuStrip1";
			// 
			// mnuFile
			// 
			this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFileNew,
            this.mnuFileOpen,
            this.mnuFileSave,
            this.mnuFileSaveAs,
            this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Size = new System.Drawing.Size(37, 20);
			this.mnuFile.Text = "File";
			// 
			// mnuFileNew
			// 
			this.mnuFileNew.Name = "mnuFileNew";
			this.mnuFileNew.Size = new System.Drawing.Size(114, 22);
			this.mnuFileNew.Text = "New";
			this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// mnuFileOpen
			// 
			this.mnuFileOpen.Name = "mnuFileOpen";
			this.mnuFileOpen.Size = new System.Drawing.Size(114, 22);
			this.mnuFileOpen.Text = "Open";
			this.mnuFileOpen.Click += new System.EventHandler(this.mnuFileOpen_Click);
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Size = new System.Drawing.Size(114, 22);
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveAs
			// 
			this.mnuFileSaveAs.Name = "mnuFileSaveAs";
			this.mnuFileSaveAs.Size = new System.Drawing.Size(114, 22);
			this.mnuFileSaveAs.Text = "Save As";
			this.mnuFileSaveAs.Click += new System.EventHandler(this.mnuFileSaveAs_Click);
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Size = new System.Drawing.Size(114, 22);
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// gridEnvs
			// 
			this.gridEnvs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.gridEnvs.Location = new System.Drawing.Point(0, 17);
			this.gridEnvs.Margin = new System.Windows.Forms.Padding(2);
			this.gridEnvs.Name = "gridEnvs";
			this.gridEnvs.RowTemplate.Height = 24;
			this.gridEnvs.Size = new System.Drawing.Size(464, 154);
			this.gridEnvs.TabIndex = 3;
			// 
			// grpEnvs
			// 
			this.grpEnvs.Controls.Add(this.gridEnvs);
			this.grpEnvs.Location = new System.Drawing.Point(44, 473);
			this.grpEnvs.Margin = new System.Windows.Forms.Padding(2);
			this.grpEnvs.Name = "grpEnvs";
			this.grpEnvs.Padding = new System.Windows.Forms.Padding(2);
			this.grpEnvs.Size = new System.Drawing.Size(464, 171);
			this.grpEnvs.TabIndex = 4;
			this.grpEnvs.TabStop = false;
			this.grpEnvs.Text = "Environments";
			// 
			// lblError
			// 
			this.lblError.AutoSize = true;
			this.lblError.BackColor = System.Drawing.SystemColors.Control;
			this.lblError.ForeColor = System.Drawing.Color.Red;
			this.lblError.Location = new System.Drawing.Point(190, 665);
			this.lblError.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(161, 13);
			this.lblError.TabIndex = 5;
			this.lblError.Text = "Please fill all fields before saving.";
			this.lblError.Visible = false;
			// 
			// gpbClients
			// 
			this.gpbClients.Controls.Add(this.btnDelete);
			this.gpbClients.Controls.Add(this.btnAdd);
			this.gpbClients.Controls.Add(this.grdClients);
			this.gpbClients.Location = new System.Drawing.Point(130, 44);
			this.gpbClients.Margin = new System.Windows.Forms.Padding(2);
			this.gpbClients.Name = "gpbClients";
			this.gpbClients.Padding = new System.Windows.Forms.Padding(2);
			this.gpbClients.Size = new System.Drawing.Size(291, 133);
			this.gpbClients.TabIndex = 6;
			this.gpbClients.TabStop = false;
			this.gpbClients.Text = "Clients";
			// 
			// btnDelete
			// 
			this.btnDelete.Location = new System.Drawing.Point(192, 71);
			this.btnDelete.Name = "btnDelete";
			this.btnDelete.Size = new System.Drawing.Size(72, 29);
			this.btnDelete.TabIndex = 5;
			this.btnDelete.Text = "Delete";
			this.btnDelete.UseVisualStyleBackColor = true;
			this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(192, 36);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(72, 29);
			this.btnAdd.TabIndex = 4;
			this.btnAdd.Text = "Add";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// grdClients
			// 
			this.grdClients.AllowUserToAddRows = false;
			this.grdClients.AllowUserToDeleteRows = false;
			this.grdClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.grdClients.Location = new System.Drawing.Point(9, 20);
			this.grdClients.Margin = new System.Windows.Forms.Padding(2);
			this.grdClients.Name = "grdClients";
			this.grdClients.RowTemplate.Height = 24;
			this.grdClients.Size = new System.Drawing.Size(160, 101);
			this.grdClients.TabIndex = 3;
			this.grdClients.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdClients_RowEnter);
			// 
			// chkHarrisStaff
			// 
			this.chkHarrisStaff.AutoSize = true;
			this.chkHarrisStaff.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkHarrisStaff.Location = new System.Drawing.Point(193, 93);
			this.chkHarrisStaff.Name = "chkHarrisStaff";
			this.chkHarrisStaff.Size = new System.Drawing.Size(78, 17);
			this.chkHarrisStaff.TabIndex = 4;
			this.chkHarrisStaff.Text = "Harris Staff";
			this.chkHarrisStaff.UseVisualStyleBackColor = true;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(550, 695);
			this.Controls.Add(this.gpbClients);
			this.Controls.Add(this.lblError);
			this.Controls.Add(this.grpEnvs);
			this.Controls.Add(this.grpGlobal);
			this.Controls.Add(this.grpServer);
			this.Controls.Add(this.mnuStrip);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MainMenuStrip = this.mnuStrip;
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "frmMain";
			this.Text = "TRIO Configuration Editor";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.grpServer.ResumeLayout(false);
			this.grpServer.PerformLayout();
			this.grpGlobal.ResumeLayout(false);
			this.grpGlobal.PerformLayout();
			this.mnuStrip.ResumeLayout(false);
			this.mnuStrip.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridEnvs)).EndInit();
			this.grpEnvs.ResumeLayout(false);
			this.gpbClients.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.grdClients)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpServer;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label lblDataSource;
        private System.Windows.Forms.TextBox txtDataSource;
        private System.Windows.Forms.GroupBox grpGlobal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblBackupDir;
        private System.Windows.Forms.TextBox txtMasterDir;
        private System.Windows.Forms.TextBox txtBackupDir;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MenuStrip mnuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuFileNew;
        private System.Windows.Forms.ToolStripMenuItem mnuFileOpen;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSave;
        private System.Windows.Forms.ToolStripMenuItem mnuFileExit;
        private System.Windows.Forms.DataGridView gridEnvs;
        private System.Windows.Forms.ToolStripMenuItem mnuFileSaveAs;
        private System.Windows.Forms.GroupBox grpEnvs;
        private System.Windows.Forms.Label lblError;
		private System.Windows.Forms.GroupBox gpbClients;
		private System.Windows.Forms.DataGridView grdClients;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.CheckBox chkHarrisStaff;
	}
}

