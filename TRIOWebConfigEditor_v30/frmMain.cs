﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Runtime.InteropServices;

namespace TRIOConfigEditor
{
    public partial class frmMain : Form
    {
        private Setup config = new Setup();
        private Setup configBak = new Setup();
		private string filename = "";

        public frmMain()
        {
            InitializeComponent();
        }

        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            //Check for changes - prompt to save before loading a new one
            if (!config.Equals(configBak))
            {
                if (MessageBox.Show("Changes will be lost. Continue anyway?", "Unsaved Changes", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2)
                        == DialogResult.No)
                {
                    return;
                }
            }
            ResetScreen();
            filename = "";
            config = new Setup();
            configBak = config.Copy();

			CreateClientBindings();
		}
        
        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            //Check for changes - prompt to save before loading a new one
            if (!config.Equals(configBak))
            {
                if (MessageBox.Show("Changes will be lost. Continue anyway?", "Unsaved Changes", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2)
                        == DialogResult.No)
                {
                    return;
                }
            }
            
            OpenFileDialog fOpen = new OpenFileDialog();
            fOpen.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
            fOpen.RestoreDirectory = true;
            if (fOpen.ShowDialog() == DialogResult.OK)
            {
                string strFilename = fOpen.FileName;
                if (LoadSetupFromFile(strFilename))
                {
                    ResetScreen();
                    filename = strFilename;
                    configBak = config.Copy();

					CreateClientBindings();
				}
            }
        }

        private void mnuFileSave_Click(object sender, EventArgs e)
        {
            if (ValidateConfig())
            {
                //If no filename is assigned then get a filename
                if (filename == "")
                {
                    string strFilename = GetFilenameForSave();
                    if (strFilename != "")
                    {
                        if (SaveSetupToFile(strFilename))
                        {
                            configBak = config.Copy();
                            filename = strFilename;
                        }
                    }
                }
                else
                {
                    if (SaveSetupToFile(filename))
                    {
                        configBak = config.Copy();
                    }
                }
            }
        }

        private void mnuFileSaveAs_Click(object sender, EventArgs e)
        {
            if (ValidateConfig())
            {
                string strFilename = GetFilenameForSave();
                if (strFilename != "")
                {
                    if (SaveSetupToFile(strFilename))
                    {
                        configBak = config.Copy();
                        filename = strFilename;
                    }
                }
            }
        }

        private void mnuFileExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            filename = "";
            config = new Setup();
            configBak = config.Copy();
			
            CreateClientBindings();
        }

        private void CreateClientBindings()
        {
	        grdClients.DataSource = config.Clients;
	        foreach (DataGridViewColumn col in grdClients.Columns)
	        {
		        col.Visible = false;
	        }
	        grdClients.Columns["Name"].Visible = true;
	        grdClients.Columns["Name"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Check for changes - prompt to save before closing
            if (!config.Equals(configBak))
            {
                if (MessageBox.Show("Changes will be lost. Continue anyway?", "Unsaved Changes", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2)
                        == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private string GetFilenameForSave()
        {
			using (SaveFileDialog fSave = new SaveFileDialog())
			{
				fSave.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
				fSave.OverwritePrompt = true;
				fSave.RestoreDirectory = true;
				if (fSave.ShowDialog() == DialogResult.OK)
				{
					return fSave.FileName;
				}
				else
				{
					return "";
				}
			}
        }

        private void ClearBindings()
        {
	        txtDataSource.DataBindings.Clear();
	        txtUserName.DataBindings.Clear();
	        txtPassword.DataBindings.Clear();
	        txtBackupDir.DataBindings.Clear();
	        txtMasterDir.DataBindings.Clear();
			chkHarrisStaff.DataBindings.Clear();

	        txtDataSource.Text = "";
	        txtUserName.Text = "";
	        txtPassword.Text = "";
	        txtBackupDir.Text = "";
	        txtMasterDir.Text = "";
	        chkHarrisStaff.Checked = false;

			gridEnvs.DataSource = null;
			gridEnvs.Rows.Clear();
	        gridEnvs.AllowUserToAddRows = false;
	        gridEnvs.AllowUserToDeleteRows = false;
		}

		private void CreateBindings(int index)
        {
            txtDataSource.DataBindings.Clear();
            txtDataSource.DataBindings.Add("Text", ((Client)grdClients.Rows[index].DataBoundItem).Server, "DataSource");
            txtUserName.DataBindings.Clear();
            txtUserName.DataBindings.Add("Text", ((Client)grdClients.Rows[index].DataBoundItem).Server, "UserName");
            txtPassword.DataBindings.Clear();
            txtPassword.DataBindings.Add("Text", ((Client)grdClients.Rows[index].DataBoundItem).Server, "Password");
            txtBackupDir.DataBindings.Clear();
            txtBackupDir.DataBindings.Add("Text", ((Client)grdClients.Rows[index].DataBoundItem).Global, "BackupDirectory");
            txtMasterDir.DataBindings.Clear();
            txtMasterDir.DataBindings.Add("Text", ((Client)grdClients.Rows[index].DataBoundItem).Global, "MasterDirectory");
            chkHarrisStaff.DataBindings.Clear();


            Binding binding = new Binding("Checked", ((Client)grdClients.Rows[index].DataBoundItem).Global, "HarrisStaffUser");
            binding.Format += new ConvertEventHandler(binding_Format);
            binding.Parse += new ConvertEventHandler(binding_Parse);

            void binding_Format(object sender, ConvertEventArgs e)
            {
	            if (e.Value.ToString() == "TRUE") e.Value = true;
	            else e.Value = false;
            }

            void binding_Parse(object sender, ConvertEventArgs e)
            {
	            if ((bool)e.Value) e.Value = "TRUE";
	            else e.Value = "FALSE";
            }

			chkHarrisStaff.DataBindings.Add(binding);

			gridEnvs.DataSource = ((Client)grdClients.Rows[index].DataBoundItem).Envs;
            gridEnvs.AllowUserToAddRows = true;
            gridEnvs.AllowUserToDeleteRows = true;
            gridEnvs.Columns["DataDirectory"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }

        private bool LoadSetupFromFile(string strFilename)
        {
            try
            {
				XmlSerializer tXs = new XmlSerializer(typeof(Setup));
				
				using (System.IO.FileStream fs = new System.IO.FileStream(strFilename, System.IO.FileMode.Open))
				{
					config = (Setup)tXs.Deserialize(fs);
					CreateClientBindings();
					fs.Close();
				}				
				tXs = null;
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }

        private bool SaveSetupToFile(string strFilename)
        {
            try
            {
				using (System.IO.FileStream fs = new System.IO.FileStream(strFilename, System.IO.FileMode.Create))
				{
					XmlSerializer tXs = new XmlSerializer(typeof(Setup));
					tXs.Serialize(fs, config);
					fs.Close();
				}
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void ResetScreen()
        {
            txtDataSource.BackColor = Color.FromName("Window");
            txtUserName.BackColor = Color.FromName("Window");
            txtPassword.BackColor = Color.FromName("Window");
            txtMasterDir.BackColor = Color.FromName("Window");
            txtBackupDir.BackColor = Color.FromName("Window");

            foreach (DataGridViewRow tRow in gridEnvs.Rows)
            {
                foreach (DataGridViewCell tCell in tRow.Cells)
                {
                    tCell.Style.BackColor = Color.FromName("Window");
                }
            }
            lblError.Visible = false;
        }

        private bool ValidateConfig()
        {
            bool boolPass = true;

            ResetScreen();

            this.SelectNextControl(this.Controls[0], true, true, true, true);
 
            if (txtDataSource.Text == "")
            {
                txtDataSource.BackColor = Color.FromArgb(255, 192, 192);
                boolPass = false;
            }

            if (txtUserName.Text == "")
            {
                txtUserName.BackColor = Color.FromArgb(255, 192, 192);
                boolPass = false;
            }

            if (txtPassword.Text == "")
            {
                txtPassword.BackColor = Color.FromArgb(255, 192, 192);
                boolPass = false;
            }

            if (txtMasterDir.Text == "")
            {
                txtMasterDir.BackColor = Color.FromArgb(255, 192, 192);
                boolPass = false;
            }

            if (txtBackupDir.Text == "")
            {
                txtBackupDir.BackColor = Color.FromArgb(255, 192, 192);
                boolPass = false;
            }

            if (gridEnvs.RowCount <= 1)
            {
                gridEnvs.Rows[0].Cells[0].Style.BackColor = Color.FromArgb(255, 192, 192);
                boolPass = false;
            }
            else
            {
                foreach (DataGridViewRow tRow in gridEnvs.Rows)
                {
                    if (!tRow.IsNewRow)
                    {
                        foreach (DataGridViewCell tCell in tRow.Cells)
                        {
                            if (tCell.Value == null || tCell.Value.ToString() == "")
                            {
                                tCell.Style.BackColor = Color.FromArgb(255,192,192);
                                boolPass = false;
                            }
                        }
                    }
                }
            }

            lblError.Visible = !boolPass;
            return boolPass;
        }

		private void grdClients_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			CreateBindings(e.RowIndex);
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			config.Clients.Add(new Client());
		}

		private void btnDelete_Click(object sender, EventArgs e)
		{
			if (grdClients.CurrentRow != null)
			{
				config.Clients.Remove((Client) grdClients.CurrentRow.DataBoundItem);
				if (config.Clients.Count == 0)
				{
					ClearBindings();
				}
			}
		}
	}
}
