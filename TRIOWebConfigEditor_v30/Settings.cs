﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using System.Windows.Forms;

namespace TRIOConfigEditor
{
    public class Setup
    {
        public Setup()
        {
	        Clients = new BindingList<Client>();
	        Clients.AllowNew = true;
	        Clients.AllowRemove = true;
            Clients.AllowEdit = true;         
        }

		
        [System.Xml.Serialization.XmlAttribute]
        public string Version = "3.0";

        public BindingList<Client> Clients { get; set; }

        public Setup Copy()
        {
            Setup s = new Setup();
            
            foreach (Client tClient in Clients)
            {
                s.Clients.Add(tClient.Copy());
            }
            return s;
        }

        public bool Equals(Setup s)
        {
            bool r;
            
            r = s.Clients.Count == Clients.Count;
            foreach (Client sClient in s.Clients)
            {
                r = r && (Clients.Any(x => x.Equals(sClient)));
            }
            foreach (Client rClient in Clients)
            {
                r = r && (s.Clients.Any(x => x.Equals(rClient)));
            }
            return r;
        }

        public void Clear()
        {
            Clients.Clear();
        }
    }

    public class ServerClass
    {
        private static string strToUse = "B4A45E16-2828-4157-908E-F2266FA1810A";
        private static string sToUse = "O72SajdzgdE=";

        public ServerClass()
        {
            DataSource = "";
            ConnectionType = "";
            EncryptedUserName = "";
            EncryptedPassword = "";
        }

        public string DataSource { get; set; }
        public string ConnectionType { get; set; }

        [System.Xml.Serialization.XmlElement(ElementName = "UserName")]
        public string EncryptedUserName { get; set; }
        [System.Xml.Serialization.XmlElement(ElementName = "Password")]
        public string EncryptedPassword { get; set; }

        // This is the plain text 
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public string UserName
        {
            get
            {
                return Cryptography.CryptoUtility.DecryptByPassword(EncryptedUserName, strToUse, sToUse);
            }
            set
            {
                EncryptedUserName = Cryptography.CryptoUtility.EncryptByPassword(value, strToUse, sToUse);
            }
        }

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public string Password
        {
            get
            {
                return Cryptography.CryptoUtility.DecryptByPassword(EncryptedPassword, strToUse, sToUse);
            }
            set
            {
                EncryptedPassword = Cryptography.CryptoUtility.EncryptByPassword(value, strToUse, sToUse);
            }
        }

        public ServerClass Copy()
        {
            ServerClass s = new ServerClass();
            s.ConnectionType = ConnectionType;
            s.DataSource = DataSource;
            s.EncryptedPassword = EncryptedPassword;
            s.EncryptedUserName = EncryptedUserName;
            return s;
        }

        public bool Equals(ServerClass s)
        {
            bool r;
            r = s.ConnectionType == ConnectionType;
            r = r && (s.DataSource == DataSource);
            r = r && (s.EncryptedPassword == EncryptedPassword);
            r = r && (s.EncryptedUserName == EncryptedUserName);
            return r;
        }

        public void Clear()
        {
            DataSource = "";
            ConnectionType = "";
            EncryptedPassword = "";
            EncryptedUserName = "";
        }
    }

    public class GlobalClass
    {
        public GlobalClass()
        {
            BackupDirectory = "";
            MasterDirectory = "";
            HarrisStaffUser = "FALSE";
        }
        public string BackupDirectory { get; set; }
        public string MasterDirectory { get; set; }
        public string HarrisStaffUser { get; set; }

		public GlobalClass Copy()
        {
            GlobalClass g = new GlobalClass();
            g.BackupDirectory = BackupDirectory;
            g.MasterDirectory = MasterDirectory;
            g.HarrisStaffUser = HarrisStaffUser;
			return g;
        }

        public bool Equals(GlobalClass g)
        {
            bool r;
            r = g.BackupDirectory == BackupDirectory;
            r = r && (g.MasterDirectory == MasterDirectory);
            r = r && (g.HarrisStaffUser == HarrisStaffUser);
			return r;
        }

        public void Clear()
        {
            BackupDirectory = "";
            MasterDirectory = "";
            HarrisStaffUser = "FALSE";
        }
    }

    public class Env
    {
        public Env()
        {
            Name = "";
            DataDirectory = "";
        }
        public string Name { get; set; }
        public string DataDirectory { get; set; }

        public Env Copy()
        {
            Env e = new Env();
            e.Name = Name;
            e.DataDirectory = DataDirectory;
            return e;
        }

        public bool Equals(Env e)
        {
            bool r;
            r = e.Name == Name;
            r = r && (e.DataDirectory == DataDirectory);
            return r;
        }
    }

	public class Client
	{
		public Client()
		{
			Server = new ServerClass();
			Global = new GlobalClass();
			Envs = new BindingList<Env>();
			Envs.AllowNew = true;
			Envs.AllowRemove = true;
			Envs.AllowEdit = true;
		}


		[System.Xml.Serialization.XmlAttribute]
		public string Name { get; set; } = "";

		public ServerClass Server { get; set; }
		public GlobalClass Global { get; set; }
		public BindingList<Env> Envs { get; set; }

		public Client Copy()
		{
			Client s = new Client();
			s.Name = Name;
			s.Server = Server.Copy();
			s.Global = Global.Copy();

			foreach (Env tEnv in Envs)
			{
				s.Envs.Add(tEnv.Copy());
			}
			return s;
		}

		public bool Equals(Client c)
		{
			bool r;

			r = (c.Name == Name);
			r = r && (c.Server.Equals(Server));
			r = r && (c.Global.Equals(Global));
			r = r && (c.Envs.Count == Envs.Count);
			foreach (Env sEnv in c.Envs)
			{
				r = r && (Envs.Any(x => x.Equals(sEnv)));
			}
			foreach (Env rEnv in Envs)
			{
				r = r && (c.Envs.Any(x => x.Equals(rEnv)));
			}
			return r;
		}

		public void Clear()
		{
			Name = "";
			Server.Clear();
			Global.Clear();
			Envs.Clear();
		}
	}

}
