﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
	public class GlobalActiveModuleSettings : IGlobalActiveModuleSettings
	{

		public bool AccountsReceivableIsActive { get; set; } = false;

        public bool BudgetaryIsActive { get; set; } = false;

        public bool TaxBillingIsActive { get; set; } = false;

        public bool CodeEnforcementIsActive { get; set; } = false;

        public bool TaxCollectionsIsActive { get; set; } = false;

        public bool ClerkIsActive { get; set; } = false;

        public bool FixedAssetsIsActive { get; set; } = false;

        public bool MosesIsActive { get; set; } = false;

        public bool MotorVehicleIsActive { get; set; } = false;

        public bool PersonalPropertyIsActive { get; set; } = false;

        public bool PayrollIsActive { get; set; } = false;

        public bool RedbookIsActive { get; set; } = false;

        public bool RealEstateIsActive { get; set; } = false;

        public bool UtilityBillingIsActive { get; set; } = false;

        public bool TaxServiceIsActive { get; set; } = false;

        public bool CashReceiptingIsActive { get; set; } = false;

        public bool CRMyRecIsActive { get; set; } = false;
    }
}
