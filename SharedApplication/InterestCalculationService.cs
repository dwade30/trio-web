﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
	public class InterestCalculationService : IInterestCalculationService
	{
		public double IPmt(double rate, int paymentPeriod, int numberOfPayments, double amount)
		{
			if (rate == 0)
			{
				return 0;
			}
			else
			{
				double payment = Pmt(rate, numberOfPayments, amount);
				double futureValue = FV(rate, (paymentPeriod - 1), payment, amount);

				return (futureValue * rate);
			}
		}

		public double Pmt(double rate, int numberOfPayments, double amount)
		{
			double temp = System.Math.Pow((rate + 1), numberOfPayments);

			return ((-amount * temp) / ((temp - 1)) * rate);
		}

		public double FV(double rate, int numberOfPayments, double payment, double amount)
		{
			double temp = System.Math.Pow(rate + 1, numberOfPayments);

			return ((-amount) * temp) - ((payment / rate) * (temp - 1));
		}
	}
}
