﻿using System;
using System.Runtime.Remoting.Messaging;

namespace SharedApplication.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsEarlierThan(this DateTime dateTime, DateTime compareToDateTime)
        {
            if (DateTime.Compare(dateTime, compareToDateTime) < 0)
            {
                return true;
            }

            return false;
        }

        public static bool IsLaterThan(this DateTime dateTime, DateTime compareToDateTime)
        {
            if (DateTime.Compare(dateTime, compareToDateTime) > 0)
            {
                return true;
            }

            return false;
        }

        public static string FormatAndPadShortDate(this DateTime date)
        {
            return date.FormatAndPadShortDate(@"/");
        }

        public static string FormatAndPadShortDate(this DateTime date,string separator)
        {
            return date.Month.ToString().PadLeft(2, '0') +separator + date.Day.ToString().PadLeft(2, '0') + separator +
                   date.Year.ToString();
        }

        public static string FormatAndPadTime(this DateTime date)
        {
            return date.ToString("hh:mm tt");
        }

        public static int DifferenceInMonths(this DateTime dateTime, DateTime compareToDateTime)
        {
            return ((dateTime.Year - compareToDateTime.Year) * 12) + dateTime.Month - compareToDateTime.Month;
        }

        public static int DifferenceInWholeYears(this DateTime dateTime, DateTime compareToDateTime)
        {
            decimal months = dateTime.DifferenceInMonths(compareToDateTime);
            return (months / 12).Truncate().ToInteger();
        }

        public static DateTime LastDayOfMonth(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, DateTime.DaysInMonth(dateTime.Year, dateTime.Month));
        }

        public static DateTime OneMonthBefore(this DateTime dateTime)
        {
            return  dateTime.AddMonths(-1);
        }

        public static DateTime StartOfMonth(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year,dateTime.Month,1);
        }

        public static DateTime StartOfDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year,dateTime.Month,dateTime.Day,0,0,0);
        }

        public static DateTime EndOfDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year,dateTime.Month,dateTime.Day,23,59,59);
        }

        public static bool IsEmptyDate(this DateTime? dateTime)
        {
            if (!dateTime.HasValue)
            {
                return true;
            }

            return dateTime.Value.Equals(new DateTime(1900, 1, 1)) ||
                    dateTime.Value.Equals(new DateTime(1899, 12, 30));
        }

        public static bool IsEmptyDate(this DateTime dateTime)
        {
            return dateTime.Equals(new DateTime(1900, 1, 1)) ||
                   dateTime.Equals(new DateTime(1899, 12, 30));
        }
    }
}