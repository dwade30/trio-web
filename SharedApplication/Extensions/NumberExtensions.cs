﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using String = System.String;

namespace SharedApplication.Extensions
{
    public static class NumberExtensions
    {
        public static string FormatAsNumber(this int amount)
        {
            return string.Format("{0:N0}", amount);
        }
        public static string FormatAsMoney(this decimal amount)
        {
            return $"{amount:0.00}";
        }

        public static string FormatAsMoney(this double amount)
        {
            return $"{amount:0.00}";
        }

        public static string FormatAsCurrencyNoSymbol(this decimal amount)
        {

            return amount.ToString("#,##0.00");
           // return string.Format("{0:C}", amount).Substring(1);
        }

        public static string FormatAsNumber(this decimal amount, int places)
        {
            if (places < 1)
            {
                return amount.ToString("#,##0");
            }

            var placeString = "".PadRight(places, '0');
            return amount.ToString("#,##0." + placeString);
        }

        public static string FormatAsNumberOptionalDecimal(this decimal amount, int places)
        {
            var placeString = "".PadRight(places, '#');
            return amount.ToString("#,##0." + placeString);
        }

        public static string FormatAsNumberOptionalDecimal(this double amount, int places)
        {
            var placeString = "".PadRight(places, '#');
            return amount.ToString("#,##0." + placeString);
        }

        public static string FormatAsCurrencyOptionalDecimalNoSymbol(this decimal amount)
        {
            return amount.ToString("#,##0.##");
        }

        public static string FormatAsCurrencyOptionalDecimalNoSymbol(this double amount)
        {
            return amount.ToString("#,##0.##");
        }

        public static string FormatAsCurrencyNoSymbol(this double amount)
        {
            return amount.ToString("#,##0.00");
            //return string.Format("{0:C}", amount).Substring(1);
        }

        public static string FormatAsCurrency(this decimal amount)
        {
            return string.Format("{0:C}", amount);
        }
        public static string FormatAsCurrency(this double amount)
        {
            return string.Format("{0:C}", amount);
        }

        public static decimal RoundToMoney(this decimal amount)
        {
            return Math.Round(amount, 2, MidpointRounding.AwayFromZero);
        }

        public static double RoundToMoney(this double amount)
        {
            return Math.Round(amount, 2, MidpointRounding.AwayFromZero);
        }

        public static decimal RoundPlaces(this decimal amount, int places)
        {
            return Math.Round(amount, places, MidpointRounding.AwayFromZero);
        }

        public static double RoundPlaces(this double amount, int places)
        {
            return Math.Round(amount, places, MidpointRounding.AwayFromZero);
        }

        public static double ToDouble(this decimal amount)
        {
            return Convert.ToDouble(amount);
        }

        public static decimal ToDecimal(this double amount)
        {
            return Convert.ToDecimal(amount);
        }

        public static int ToInteger(this double amount)
        {
            return Convert.ToInt32(amount);
        }

        public static int ToInteger(this decimal amount)
        {
            return Convert.ToInt32(amount);
        }

        public static decimal Truncate(this decimal amount)
        {
            return decimal.Truncate(amount);
        }

        public static bool IsBetween(this int compare,int start, int end)
        {
            if (compare < start)
            {
                return false;
            }
            if (compare > end)
            {
                return false;
            }
            return true;
        }

        public static bool IsBetween(this decimal compare, decimal start, decimal end)
        {
            if (compare < start)
            {
                return false;
            }
            if (compare > end)
            {
                return false;
            }
            return true;
        }
    }
}
