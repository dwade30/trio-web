﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Extensions
{
	public static class QueryableExtensions
	{
		public static IOrderedQueryable<T> AppendOrderBy<T, TKey>(this IQueryable<T> query, Expression<Func<T, TKey>> keySelector)
		{
			if (query.Expression.Type == typeof(IOrderedQueryable<T>))
			{
				return ((IOrderedQueryable<T>) query).ThenBy(keySelector);
			}
			else
			{
				return query.OrderBy(keySelector);
			}
		} 

		public static IOrderedQueryable<T> AppendOrderByDescending<T, TKey>(this IQueryable<T> query, Expression<Func<T, TKey>> keySelector)
			=> query.Expression.Type == typeof(IOrderedQueryable<T>)
				? ((IOrderedQueryable<T>)query).ThenByDescending(keySelector)
				: query.OrderByDescending(keySelector);
	}
}
