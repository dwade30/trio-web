﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedApplication.Extensions
{
    public static class CollectionExtensions
    {
        public static void AddRange<TKey, TValue>(this Dictionary<TKey, TValue> source, IEnumerable<KeyValuePair<TKey, TValue>> collection)
        {
            if (collection == null)
            {
                return;
            }
            
            foreach (var item in collection)
            {
                if (!source.ContainsKey(item.Key))
                {
                    source.Add(item.Key, item.Value);
                }
                else
                {
                    source[item.Key] = item.Value;

                }
            }
        }

        public static void AddOrUpdate<TKey, TValue>(this Dictionary<TKey, TValue> source, TKey key, TValue value)
        {
            if (!source.ContainsKey(key))
            {
                source.Add(key,value);
            }
            else
            {
                source[key] = value;
            }
        }

        public static string ToCRLFDelimitedList(this IEnumerable<string> list)
        {
            if (list == null || !list.Any())
            {
                return "";
            }

            var crlf = "";
            var returnList = new StringBuilder();
            foreach (var entry in list)
            {
                returnList.Append(crlf + entry);
                crlf = Environment.NewLine;
            }
            return returnList.ToString();
        }
    }
}