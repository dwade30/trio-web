﻿using System;
using System.Web.UI.WebControls.WebParts;

namespace SharedApplication.Extensions
{
    public static class StringExtensions
    {
        public static Guid ToGuid(this string guidString)
        {
            if (Guid.TryParse(guidString, out var result))
            {
                return result;
            }
            return Guid.Empty;
        }

        public static Boolean IsDate (this string dateString)
        {
            DateTime parsedDate;
            if (DateTime.TryParse(dateString,out parsedDate))
            {
                return true;
            }
            return false;
        }

        public static string HtmlEncode(this string textToEncode)
        {
            if (string.IsNullOrEmpty(textToEncode)) return textToEncode;

            return System.Web.Security.AntiXss.AntiXssEncoder.HtmlEncode(textToEncode, false);
        }

        public static string WithQuotes(this string textToQuote)
        {
	        return Convert.ToChar(34) + textToQuote + Convert.ToChar(34);
        }

        public static bool IsValidInt32(this string source)
        {
	        return int.TryParse(source, out int i);
		}

        public static bool IsValidDecimal(this string source)
        {
	        return decimal.TryParse(source, out decimal i);
        }

		public static string Left(this string sourceString, int numberOfCharacters)
        {
            if (numberOfCharacters > sourceString.Length)
            {
                return sourceString;
            }

            if (numberOfCharacters < 1)
            {
                return "";
            }
            return sourceString.Substring(0, numberOfCharacters);
        }
        public static string Right(this string sourceString ,int numberOfCharacters)
        {
            if (numberOfCharacters > sourceString.Length)
            {
                return sourceString;
            }

            if (numberOfCharacters < 1)
            {
                return "";
            }
            return sourceString.Substring(sourceString.Length - numberOfCharacters, numberOfCharacters);
        }

        public static DateTime ToDate(this string sourceString)
        {
            if (String.IsNullOrWhiteSpace(sourceString))
            {
                return DateTime.MinValue;
            }

            var parts = sourceString.Split(new string[] {@"/"}, StringSplitOptions.None);
            return new DateTime(Convert.ToInt32(parts[2]),Convert.ToInt32(parts[0]),Convert.ToInt32(parts[1]));
        }

        public static Int32 ToIntegerValue(this string sourceString)
        {
            if (string.IsNullOrWhiteSpace(sourceString))
            {
                return 0;
            }
            if (Int32.TryParse(sourceString.Replace(",","").Replace("$",""),out var result))
            {
                return result;
            }

            return 0;
        }

        public static short ToShortValue(this string sourceString)
        {
	        if (short.TryParse(sourceString, out var result))
	        {
		        return result;
	        }

	        return 0;
        }

		public static decimal ToDecimalValue(this string sourceString)
        {
            if (decimal.TryParse(sourceString.Replace("%", "").Replace("$","").Replace(",",""), out var result))
            {
                return result;
            }

            return 0;
        }

        public static decimal AbsoluteValue(this decimal value)
        {
            return Math.Abs(value);
        }
        public static double ToDoubleValue(this string sourceString)
        {
            if (double.TryParse(sourceString.Replace(",","").Replace("$",""), out var result))
            {
                return result;
            }

            return 0;
        }

        public static bool IsBetween(this string sourceString, string lowString, string highString)
        {
            if (sourceString.CompareTo(lowString) < 0)
            {
                return false;
            }

            if (sourceString.CompareTo(highString) > 0)
            {
                return false;
            }

            return true;
        }

        public static bool ToBoolean(this string sourceString)
        {
            if (bool.TryParse(sourceString, out var result))
            {
                return result;
            }

            if (sourceString == "-1")
            {
	            return true;
            }
            else
            {
	            return false;
            }
        }

        public static bool IsNullOrWhiteSpace(this string sourceString)
        {
            return string.IsNullOrWhiteSpace(sourceString);
        }

        public static bool HasText(this string sourceString)
        {
            return !string.IsNullOrWhiteSpace(sourceString);
        }

        public static string EscapeSingleQuotes(this string a)
        {
            //// searches for a singlequote and puts another next to it
            //int stringindex;
            //if (a == string.Empty)
            //    return "";
            //stringindex = 0;
            //while (stringindex < a.Length)
            //{
            //    var intpos = a.IndexOf("'", stringindex);// Strings.InStr(stringindex, a, "'");
            //    if (intpos < 0)
            //        break;
            //    var tstring = a.Substring(0, intpos); // Strings.Mid(a, 1, intpos);
            //    var t2string = a.Substring(intpos + 1);// Strings.Mid(a, intpos + 1);
            //    if (intpos == a.Length - 1)
            //    {
            //        a = a.Substring(0, intpos - 1); // Strings.Mid(a, 1, intpos - 1);
            //        break;
            //    }
            //    else
            //    {
            //        a = tstring + "'" + t2string;
            //    }
            //    stringindex = intpos + 2;
            //}
            //return a;
            return a.Replace("'", "''");
        }
    }
}
