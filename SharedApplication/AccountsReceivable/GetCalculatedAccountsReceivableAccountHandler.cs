﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Commands;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.AccountsReceivable
{
    public class GetCalculatedAccountsReceivableAccountHandler : CommandHandler<GetCalculatedAccountsReceivableAccount, CalculatedAccountsReceivableAccount>
    {
        private IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>> billQueryHandler;
        private IAccountsReceivableContext accountsReceivableContext;
        public GetCalculatedAccountsReceivableAccountHandler(IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>> billQueryHandler, IAccountsReceivableContext accountsReceivableContext)
        {
            this.billQueryHandler = billQueryHandler;
            this.accountsReceivableContext = accountsReceivableContext;
        }
        protected override CalculatedAccountsReceivableAccount Handle(GetCalculatedAccountsReceivableAccount command)
        {
            var accountId = accountsReceivableContext.CustomerMasters.Where(u => u.CustomerId == command.Account).Select(u => u.Id).FirstOrDefault();
            if (accountId == 0)
            {
                return null;
            }
            
            var bills = billQueryHandler.ExecuteQuery(new ARBillSearchCriteria
            {
                CustomerId = accountId,
                VoidedSelection = VoidedStatusSelection.NotVoided,
                CalculateCurrentInterest = true,
                EffectiveDate = DateTime.Today
            }).ToList();

            var accountsReceivableAccount = new AccountsReceivableAccount(command.Account);
            accountsReceivableAccount.AddBills(bills);
            var calculatedAccount = new CalculatedAccountsReceivableAccount();
            calculatedAccount.AccountSummary = new AccountsReceivableCalculationSummary(command.Account);
            if (bills != null)
            {
                foreach (var bill in bills)
                {
                    var summary = new AccountsReceivableCalculationSummary(command.Account);
                    summary.Balance = bill.BalanceRemaining;
                    calculatedAccount.AccountSummary.AddTo(summary);
                }
            }

            return calculatedAccount;
        }
    }
}
