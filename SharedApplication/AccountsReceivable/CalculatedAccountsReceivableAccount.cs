﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable
{
    public class CalculatedAccountsReceivableAccount
    {
        public AccountsReceivableAccount UtilityAccount { get; set; }
        public AccountsReceivableCalculationSummary AccountSummary { get; set; }


    }
}
