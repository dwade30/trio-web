﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SharedApplication.AccountsReceivable.Commands;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;

namespace SharedApplication.AccountsReceivable
{
    public class ValidateAllARAccountsForPaymentsHandler : CommandHandler<ValidateAllARAccountsForPayments, (bool Success, IEnumerable<string> ValidationErrors)>
    {
        private IAccountsReceivableContext arContext;
        private ICashReceiptsContext crContext;
        private IBudgetaryAccountingService accountingService;
        private IAccountValidationService accountValidationService;
        private ITelemetryService telemetryService;
        private List<string> validationErrors = new List<string>();
        public ValidateAllARAccountsForPaymentsHandler(IAccountsReceivableContext arContext, ICashReceiptsContext crContext, IBudgetaryAccountingService accountingService, IAccountValidationService accountValidationService, ITelemetryService temeletryService)
        {
            this.arContext = arContext;
            this.crContext = crContext;
            this.accountingService = accountingService;
            this.accountValidationService = accountValidationService;
            this.telemetryService = temeletryService;
        }

        protected override (bool Success, IEnumerable<string> ValidationErrors) Handle(ValidateAllARAccountsForPayments command)
        {
	        try
	        {
		        var funds = GetFunds();
		        var success = ValidatePrePayAccount();

		        var defaultTypes = arContext.DefaultBillTypes.Where(d => d.DefaultAccount != "" && d.OverrideIntAccount == "" && d.OverrideTaxAccount == "");
		        foreach (var defaultType in defaultTypes)
		        {
			        var altCashFund = accountingService.GetFundFromAccount(defaultType.DefaultAccount);
			        if (altCashFund != funds.InterestFund || altCashFund != funds.TaxFund)
			        {
				        success = false;
				        validationErrors.Add("AR bill type " + defaultType.TypeCode.GetValueOrDefault().ToString() + " has mismatching funds");
			        }
		        }

		        return (Success: success, ValidationErrors: validationErrors);
			}
	        catch (Exception e)
	        {
		        telemetryService.TrackException(e);

				return (Success: false, new List<string>
		        {
			        "Unable to verify Accounts Receivable receipt types."
		        });
	        }
        }

        private bool ValidatePrePayAccount()
        {
	        try
	        {
		        var customize = arContext.Customize.FirstOrDefault();
		        if (customize != null)
		        {
			        if (!accountValidationService.AccountValidate(customize.PrePayReceivableAccount))
			        {
				        validationErrors.Add("The AR pre-pay receivable account is invalid");
				        return false;
			        }
		        }
		        else
		        {
			        return false;
		        }

		        return true;
			}
	        catch (Exception e)
	        {
		        Console.WriteLine(e);
				throw;
	        }
        }
    

        private (int InterestFund, int TaxFund) GetFunds()
        {
	        try
	        {
				var interestFund = 0;
				var taxFund = 0;
				var arType = crContext.ReceiptTypes.Where(r => r.IsAccountsReceivableType()).FirstOrDefault();
				if (!string.IsNullOrWhiteSpace(arType.Account2))
				{
					interestFund = accountingService.GetFundFromAccount(arType.Account2);
				}

				if (!string.IsNullOrWhiteSpace(arType.Account3))
				{
					taxFund = accountingService.GetFundFromAccount(arType.Account3);
				}

				return (InterestFund: interestFund, TaxFund: taxFund);
			}
	        catch (Exception e)
	        {
		        Console.WriteLine(e);
		        throw;
	        }
        }
    }
}