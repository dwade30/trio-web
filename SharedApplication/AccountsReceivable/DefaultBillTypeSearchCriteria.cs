﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable
{
	public class DefaultBillTypeSearchCriteria
	{
		public int TypeCode { get; set; }
	}
}
