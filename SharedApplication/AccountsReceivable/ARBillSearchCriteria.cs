﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable
{
	public class ARBillSearchCriteria
	{
		public int CustomerId { get; set; }
		public VoidedStatusSelection VoidedSelection { get; set; }
        public bool CalculateCurrentInterest { get; set; } = false;
        public DateTime EffectiveDate { get; set; } = DateTime.Today;
	}

	public enum VoidedStatusSelection
	{
		All = 0,
		NotVoided = 1,
		VoidedOnly = 2
	}
}
