﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable
{
	public class CustomerSearchCriteria
	{
		public int Id { get; set; }
		public int AccountNumber { get; set; }
		public DeletedSelection deletedOption { get; set; }
	}

	public enum DeletedSelection
	{
		All = 0,
		NotDeleted = 1,
		OnlyDeleted = 2
	}
}
