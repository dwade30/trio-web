﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable
{
	public class CustomerPartySearchCriteria
	{
		public int AccountNumber { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string Invoice { get; set; }
	}
}
