﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CentralData.Models;

namespace SharedApplication.AccountsReceivable
{
	public interface IAccountsReceivableContext
	{
		IQueryable<CustomerParty> CustomerParties { get; }
		IQueryable<AuditChanges> AuditChanges { get; }
		IQueryable<AuditChangesArchive> AuditChangesArchive { get; }
		IQueryable<Bill> Bills { get; }
		IQueryable<CloseOut> CloseOut { get; }
		IQueryable<Comment> Comments { get; }
		IQueryable<CustomBillCodes> CustomBillCodes { get; }
		IQueryable<CustomBillFields> CustomBillFields { get; }
		IQueryable<CustomBills> CustomBills { get; }
		IQueryable<CustomerBills> CustomerBills { get; }
		IQueryable<CustomerMaster> CustomerMasters { get; }
		IQueryable<Customize> Customize { get; }
		IQueryable<Cya> Cya { get; }
		IQueryable<Dbversion> Dbversion { get; }
		IQueryable<DefaultBillType> DefaultBillTypes { get; }
		IQueryable<Payees> Payees { get; }
		IQueryable<PaymentRec> PaymentRec { get; }
		IQueryable<RateKey> RateKeys { get; }
		IQueryable<SavedStatusReports> SavedStatusReports { get; }
	}
}
