﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Receipting;

namespace SharedApplication.AccountsReceivable
{
    public class AccountsReceivableAccount
    {
        public int Account { get; }
        private List<ARBill> bills = new List<ARBill>();
        public IEnumerable<ARBill> Bills
        {
            get => bills;
        }

        public void AddBill(ARBill bill)
        {
            if (bill != null)
            {
                bills.Add(bill);
            }
        }

        public void AddBills(IEnumerable<ARBill> bills)
        {
            if (bills != null && bills.Any())
            {
                this.bills.AddRange(bills);
            }
        }

        public AccountsReceivableAccount(int account)
        {
            Account = account;
        }
    }
}
