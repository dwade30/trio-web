﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class CustomBillFields
    {
        public int Id { get; set; }
        public int? FieldNumber { get; set; }
        public int? FormatId { get; set; }
        public int? FieldId { get; set; }
        public double? Top { get; set; }
        public double? Left { get; set; }
        public double? Height { get; set; }
        public double? Width { get; set; }
        public int? BillType { get; set; }
        public string Description { get; set; }
        public short? Alignment { get; set; }
        public string UserText { get; set; }
        public string Font { get; set; }
        public double? FontSize { get; set; }
        public short? FontStyle { get; set; }
        public string ExtraParameters { get; set; }
    }
}
