﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class RateKey
    {
        public int Id { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public DateTime? IntStart { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Description { get; set; }
        public double? IntRate { get; set; }
        public decimal? FlatRate { get; set; }
        public DateTime? BillDate { get; set; }
        public int? BillType { get; set; }
        public int? BillMonth { get; set; }
        public int? BillYear { get; set; }
        public string InterestMethod { get; set; }
    }
}
