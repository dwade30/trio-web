﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class Bill
    {
        public int Id { get; set; }
        public int? AccountKey { get; set; }
        public int? ActualAccountNumber { get; set; }
        public int? BillNumber { get; set; }
        public string InvoiceNumber { get; set; }
        public string BillStatus { get; set; }
        public DateTime? IntPaidDate { get; set; }
        public decimal? PrinOwed { get; set; } = 0;
        public decimal? TaxOwed { get; set; } = 0;
        public decimal? IntAdded { get; set; } = 0;
        public decimal? PrinPaid { get; set; } = 0;
        public decimal? TaxPaid { get; set; } = 0;
        public decimal? IntPaid { get; set; } = 0;
        public DateTime? CreationDate { get; set; }
        public DateTime? LastUpdatedDate { get; set; }
        public string Bname { get; set; }
        public string Bname2 { get; set; }
        public string Baddress1 { get; set; }
        public string Baddress2 { get; set; }
        public string Baddress3 { get; set; }
        public string Bcity { get; set; }
        public string Bstate { get; set; }
        public string Bzip { get; set; }
        public string Bzip4 { get; set; }
        public int? BillType { get; set; }
        public decimal? Amount1 { get; set; }
        public decimal? Amount2 { get; set; }
        public decimal? Amount3 { get; set; }
        public decimal? Amount4 { get; set; }
        public decimal? Amount5 { get; set; }
        public decimal? Amount6 { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Description4 { get; set; }
        public string Description5 { get; set; }
        public string Description6 { get; set; }
        public string Reference { get; set; }
        public string Control1 { get; set; }
        public string Control2 { get; set; }
        public string Control3 { get; set; }
        public int? JournalNumber { get; set; }
        public bool? Prorated { get; set; }
        public DateTime? ProrateStartDate { get; set; }
        public DateTime? BillDate { get; set; }
        public decimal? PrePaymentAmount { get; set; }
        public double? Quantity1 { get; set; }
        public double? Quantity2 { get; set; }
        public double? Quantity3 { get; set; }
        public double? Quantity4 { get; set; }
        public double? Quantity5 { get; set; }
        public double? Quantity6 { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? PreviousInterestPaidDate { get; set; }
		public CustomerMaster Customer { get; set; }
        public CustomerParty CustomerParty { get; set; }
		public ICollection<PaymentRec> Payments { get; set; } = new List<PaymentRec>();
	}
}
