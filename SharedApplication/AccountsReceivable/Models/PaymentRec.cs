﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class PaymentRec
    {
        public int Id { get; set; }
        public int? AccountKey { get; set; }
        public int? ActualAccountNumber { get; set; }
        public int? BillKey { get; set; }
        public int? BillNumber { get; set; }
        public int? BillType { get; set; }
        public string InvoiceNumber { get; set; }
        public int? Chgintnumber { get; set; }
        public DateTime? Chgintdate { get; set; }
        public DateTime? ActualSystemDate { get; set; }
        public DateTime? EffectiveInterestDate { get; set; }
        public DateTime? RecordedTransactionDate { get; set; }
        public string Teller { get; set; }
        public string Reference { get; set; }
        public string Code { get; set; }
        public int? ReceiptNumber { get; set; }
        public decimal? Principal { get; set; }
        public decimal? Tax { get; set; }
        public decimal? Interest { get; set; }
        public string TransNumber { get; set; }
        public string PaidBy { get; set; }
        public string Comments { get; set; }
        public string CashDrawer { get; set; }
        public string GeneralLedger { get; set; }
        public string BudgetaryAccountNumber { get; set; }
        public int? DailyCloseOut { get; set; }
		public Guid? CorrelationIdentifier { get; set; }
		public Guid? TransactionIdentifier { get; set; }
		public Bill Bill { get; set; }
	}
}
