﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class CloseOut
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime? CloseOutDate { get; set; }
        public string TellerId { get; set; }
    }
}
