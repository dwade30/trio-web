﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class CustomerMaster
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? PartyId { get; set; }
        public string BilledName { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string DataEntryMessage { get; set; }
        public string BillMessage { get; set; }
        public string Message { get; set; }
        public bool? OneTimeBillMessage { get; set; }
        public DateTime? TimeUpdated { get; set; }
        public DateTime? DateUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public bool? Deleted { get; set; }
        public bool? SalesTax { get; set; }
		public ICollection<Bill> Bills { get; set; }
	}
}
