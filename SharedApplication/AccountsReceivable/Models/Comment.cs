﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class Comment
    {
        public int Id { get; set; }
        public int? Account { get; set; }
        public string Text { get; set; }
        public int? Priority { get; set; }
        public string Type { get; set; }
    }
}
