﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable.Models
{
	public class CustomerParty
	{
		public int Id { get; set; }
		public int? CustomerId { get; set; }
		public int? PartyId { get; set; }
		public string BilledName { get; set; }
		public string Comment { get; set; }
		public string Status { get; set; }
		public string DataEntryMessage { get; set; }
		public string BillMessage { get; set; }
		public string Message { get; set; }
		public bool? OneTimeBillMessage { get; set; }
		public DateTime? TimeUpdated { get; set; }
		public DateTime? DateUpdated { get; set; }
		public string UpdatedBy { get; set; }
		public bool? Deleted { get; set; }
		public bool? SalesTax { get; set; }
		public Guid? PartyGuid { get; set; }
		public int? PartyType { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string Designation { get; set; }
		public string Email { get; set; }
		public string WebAddress { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Country { get; set; }
		public string OverrideName { get; set; }
		public string FullName { get; set; }
		public string FullNameLF { get; set; }

		public ICollection<Bill> Bills { get; set; }
	}
}
