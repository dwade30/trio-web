﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class Payees
    {
        public int Id { get; set; }
        public int? PayeeId { get; set; }
        public string Description { get; set; }
        public string LogoPath { get; set; }
        public string RemittanceMessage { get; set; }
    }
}
