﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class CustomerBills
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public int? Type { get; set; }
        public decimal? Amount1 { get; set; }
        public decimal? Amount2 { get; set; }
        public decimal? Amount3 { get; set; }
        public decimal? Amount4 { get; set; }
        public decimal? Amount5 { get; set; }
        public decimal? Amount6 { get; set; }
        public string Reference { get; set; }
        public string Control1 { get; set; }
        public string Control2 { get; set; }
        public string Control3 { get; set; }
        public DateTime? StartDate { get; set; }
        public bool? Prorate { get; set; }
        public string Comment { get; set; }
        public bool? Exclude { get; set; }
        public double? Quantity1 { get; set; }
        public double? Quantity2 { get; set; }
        public double? Quantity3 { get; set; }
        public double? Quantity4 { get; set; }
        public double? Quantity5 { get; set; }
        public double? Quantity6 { get; set; }
    }
}
