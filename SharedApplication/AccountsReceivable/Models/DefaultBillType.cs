﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class DefaultBillType
    {
        public int Id { get; set; }
        public string InterestMethod { get; set; }
        public decimal? FlatAmount { get; set; }
        public string FrequencyCode { get; set; }
        public int? FirstMonth { get; set; }
        public string DefaultAccount { get; set; }
        public int? TypeCode { get; set; }
        public string TypeTitle { get; set; }
        public string Title1 { get; set; }
        public string Title1Abbrev { get; set; }
        public string Account1 { get; set; }
        public double? DefaultAmount1 { get; set; }
        public string Title2 { get; set; }
        public string Title2Abbrev { get; set; }
        public string Account2 { get; set; }
        public double? DefaultAmount2 { get; set; }
        public string Title3 { get; set; }
        public string Title3Abbrev { get; set; }
        public string Account3 { get; set; }
        public double? DefaultAmount3 { get; set; }
        public string Title4 { get; set; }
        public string Title4Abbrev { get; set; }
        public string Account4 { get; set; }
        public double? DefaultAmount4 { get; set; }
        public string Title5 { get; set; }
        public string Title5Abbrev { get; set; }
        public string Account5 { get; set; }
        public double? DefaultAmount5 { get; set; }
        public string Title6 { get; set; }
        public string Title6Abbrev { get; set; }
        public string Account6 { get; set; }
        public double? DefaultAmount6 { get; set; }
        public bool? ChangeType { get; set; }
        public bool? Year1 { get; set; }
        public bool? Year2 { get; set; }
        public bool? Year3 { get; set; }
        public bool? Year4 { get; set; }
        public bool? Year5 { get; set; }
        public bool? Year6 { get; set; }
        public string Reference { get; set; }
        public string Control1 { get; set; }
        public string Control2 { get; set; }
        public string Control3 { get; set; }
        public bool? ReferenceMandatory { get; set; }
        public bool? Control1Mandatory { get; set; }
        public bool? Control2Mandatory { get; set; }
        public bool? Control3Mandatory { get; set; }
        public bool? ReferenceChanges { get; set; }
        public bool? Control1Changes { get; set; }
        public bool? Control2Changes { get; set; }
        public bool? Control3Changes { get; set; }
        public bool? SalesTax { get; set; }
        public string ReceivableAccount { get; set; }
        public double? PerDiemRate { get; set; }
        public int? InterestStartDate { get; set; }
        public string OverrideIntAccount { get; set; }
        public string OverrideTaxAccount { get; set; }
        public int? PayeeId { get; set; }
    }
}
