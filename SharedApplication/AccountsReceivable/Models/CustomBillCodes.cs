﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class CustomBillCodes
    {
        public int Id { get; set; }
        public int? FieldId { get; set; }
        public int? OrderNo { get; set; }
        public int? CategoryNo { get; set; }
        public string Description { get; set; }
        public double? DefaultWidth { get; set; }
        public double? DefaultHeight { get; set; }
        public bool? SpecialCase { get; set; }
        public string FieldName { get; set; }
        public string TableName { get; set; }
        public string Dbname { get; set; }
        public short? DataType { get; set; }
        public string Category { get; set; }
        public string HelpDescription { get; set; }
        public short? DefaultAlignment { get; set; }
        public bool? BoolUserDefined { get; set; }
        public string FormatString { get; set; }
        public string ExtraParameters { get; set; }
        public string ToolTipText { get; set; }
        public string ParametersToolTip { get; set; }
    }
}
