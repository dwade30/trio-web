﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class Customize
    {
        public int Id { get; set; }
        public int? Basis { get; set; }
        public double? TaxRate { get; set; }
        public bool? ShowLastAraccountInCr { get; set; }
        public string RecordSalesTax { get; set; }
        public string SalesTaxReceivableAccount { get; set; }
        public string SalesTaxPayableAccount { get; set; }
        public double? SalesTaxRate { get; set; }
        public string DefaultCity { get; set; }
        public string DefaultState { get; set; }
        public string DefaultZip { get; set; }
        public string DefaultInterestMethod { get; set; }
        public string DefaultBillType { get; set; }
        public float? LabelLaserAdjustment { get; set; }
        public float? BillLaserAdjustment { get; set; }
        public double? DefaultPerDiemRate { get; set; }
        public decimal? DefaultFlatRate { get; set; }
        public int? DefaultInterestStartDate { get; set; }
        public bool? AutoCreateJournal { get; set; }
        public string PrePayReceivableAccount { get; set; }
        public int? DefaultPayeeId { get; set; }
        public string Version { get; set; }
    }
}
