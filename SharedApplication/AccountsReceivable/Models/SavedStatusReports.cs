﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class SavedStatusReports
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public string Type { get; set; }
        public string Sql { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string WhereSelection { get; set; }
        public string SortSelection { get; set; }
        public string FieldConstraint0 { get; set; }
        public string FieldConstraint1 { get; set; }
        public string FieldConstraint2 { get; set; }
        public string FieldConstraint3 { get; set; }
        public string FieldConstraint4 { get; set; }
        public string FieldConstraint5 { get; set; }
        public string FieldConstraint6 { get; set; }
        public string FieldConstraint7 { get; set; }
        public string FieldConstraint8 { get; set; }
        public string FieldConstraint9 { get; set; }
    }
}
