﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable.Models
{
	public class CustomerPaymentStatusResult
	{
		public int Id { get; set; }
		public int CustomerId { get; set; }
		public int PartyId { get; set; }
		public string FullName { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string InvoiceNumber { get; set; }
		public bool Paid { get; set; }
	}
}
