﻿using System;
using System.Collections.Generic;

namespace SharedApplication.AccountsReceivable.Models
{
    public partial class CustomBills
    {
        public int Id { get; set; }
        public int? BillFormat { get; set; }
        public string FormatName { get; set; }
        public short? Units { get; set; }
        public double? PageHeight { get; set; }
        public double? PageWidth { get; set; }
        public double? TopMargin { get; set; }
        public double? BottomMargin { get; set; }
        public double? LeftMargin { get; set; }
        public double? RightMargin { get; set; }
        public bool? BoolWide { get; set; }
        public short? BillType { get; set; }
        public bool? IsLaser { get; set; }
        public bool? IsDeletable { get; set; }
        public string Description { get; set; }
        public string BackgroundImage { get; set; }
        public double? DefaultFontSize { get; set; }

        
	}
}
