﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Models;

namespace SharedApplication.AccountsReceivable
{
	public interface IPaymentService
	{
		CalculateBillResult CalculateBillTotals(ARBill bill, DateTime effectiveDate);

		CalculateInterestResult CalculateInterest(Double curAmount, DateTime dtInterestStartDate, DateTime dtDateNow,
			double dblRateAsDecimal, int intBasis, int usingStartDate, double dblOverPaymentIntRate);
	}
}
