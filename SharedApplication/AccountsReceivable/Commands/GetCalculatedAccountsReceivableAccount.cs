﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.AccountsReceivable.Commands
{
    public class GetCalculatedAccountsReceivableAccount : Command<CalculatedAccountsReceivableAccount>
    {
        public int Account { get; }
        public DateTime EffectiveDate { get; }

        public GetCalculatedAccountsReceivableAccount(int account, DateTime effectiveDate)
        {
            Account = account;
            EffectiveDate = effectiveDate;
        }
    }
}
