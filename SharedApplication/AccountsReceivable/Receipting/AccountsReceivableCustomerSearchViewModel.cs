﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class AccountsReceivableCustomerSearchViewModel : IAccountsReceivableCustomerSearchViewModel
	{
		private Guid correlationID;
		private EventPublisher eventPublisher;
		private CommandDispatcher commandDispatcher;
		private Setting lastAccountAccessedSetting;
		private ISettingService settingService;
		private IGlobalActiveModuleSettings activeModuleSettings;
		private GlobalAccountsReceivableSettings globalAccountsReceivableSettings;
		private IQueryHandler<CustomerSearchCriteria, IEnumerable<CustomerMaster>> customerQueryHandler;
		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>> customerPartyQueryHandler;
		private CustomerMaster selectedCustomer = null;
		private IEnumerable<CustomerPaymentStatusResult> searchResults = new List<CustomerPaymentStatusResult>();

		public AccountsReceivableCustomerSearchViewModel(EventPublisher eventPublisher, ISettingService settingService, IGlobalActiveModuleSettings activeModuleSettings, GlobalAccountsReceivableSettings globalAccountsReceivableSettings, CommandDispatcher commandDispatcher, IQueryHandler<CustomerSearchCriteria, IEnumerable<CustomerMaster>> customerQueryHandler, IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>> customerPartyQueryHandler)
		{
			this.eventPublisher = eventPublisher;
			this.settingService = settingService;
			this.activeModuleSettings = activeModuleSettings;
			this.globalAccountsReceivableSettings = globalAccountsReceivableSettings;
			this.commandDispatcher = commandDispatcher;
			this.customerQueryHandler = customerQueryHandler;
			this.customerPartyQueryHandler = customerPartyQueryHandler;

			lastAccountAccessedSetting = settingService.GetSettingValue(SettingOwnerType.User, "ARLastAccountNumber");

			if (lastAccountAccessedSetting == null)
			{
				lastAccountAccessedSetting = new Setting
				{
					SettingName = "ARLastAccountNumber",
					SettingValue = "0"
				};
			}

			SearchResultsVisible = false;
		}

		public int LastAccountAccessed
		{
			get => lastAccountAccessedSetting.SettingValue.ToIntegerValue();
			set
			{
				lastAccountAccessedSetting.SettingValue = value.ToString();
				settingService.SaveSetting(SettingOwnerType.User, lastAccountAccessedSetting.SettingName, lastAccountAccessedSetting.SettingValue);
			} 
		}

		public void SetSelectedCustomer(int account, bool deletedOnly = false)
		{
			selectedCustomer = FindCustomer(account, deletedOnly);
		}

		public CustomerMaster SelectedCustomer
		{
			get => selectedCustomer;
			set => selectedCustomer = value;
		}

		public bool StartedFromCashReceipts { get; set; }

		public bool SearchResultsVisible { get; set; }

		public bool ShowPaymentScreen { get; set; }

		public bool ShowLastAccount()
		{
			return (StartedFromCashReceipts && globalAccountsReceivableSettings.ShowLastARAccountInCR) || !StartedFromCashReceipts;
		}

		public void Cancel()
		{
			eventPublisher.Publish(new TransactionCancelled(correlationID,null) );
		}

		public void SetCorrelationId(Guid correlationID)
		{
			this.correlationID = correlationID;
		}

		public void ProceedToNextScreen(int account)
		{
			commandDispatcher.Send(new ShowARPaymentStatusScreen()
			{
				AccountNumber = account,
				CorrelationId = correlationID,
				IsPaymentScreen = ShowPaymentScreen,
				StartedFromCashReceipts = StartedFromCashReceipts
			});
		}

		public bool UndeleteCustomer(int account)
		{
			return commandDispatcher.Send(new UndeleteCustomer()
			{
				Account = account
			}).Result;
		}

		public CustomerMaster FindCustomer(int accountNumber, bool deletedOnly)
		{
			return customerQueryHandler.ExecuteQuery(new CustomerSearchCriteria
			{
				AccountNumber = accountNumber,
				deletedOption = deletedOnly ? DeletedSelection.OnlyDeleted : DeletedSelection.All
			}).FirstOrDefault();
		}

		public void Search(SearchType searchType, string criteria)
		{
			try
			{
				searchResults = customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
				{
					Invoice = searchType == SearchType.Invoice ? criteria : "",
					Name = searchType == SearchType.Name ? criteria : "",
					Address = searchType == SearchType.Address ? criteria : "",
				});
			}
			catch (Exception e)
			{
				searchResults = new List<CustomerPaymentStatusResult>();
			}
		}

		public IEnumerable<CustomerPaymentStatusResult> SearchResults
		{
			get => searchResults;
		}

		public enum SearchType
		{
			Name = 1,
			Address = 2,
			Invoice = 3
		}
	}
}
