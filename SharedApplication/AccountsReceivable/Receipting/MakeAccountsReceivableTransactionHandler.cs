﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class MakeAccountsReceivableTransactionHandler : CommandHandler<MakeAccountsReceivableTransaction>
	{
		private IView<IAccountsReceivableCustomerSearchViewModel> customerSearchView;
		private CommandDispatcher commandDispatcher;

		public MakeAccountsReceivableTransactionHandler(IView<IAccountsReceivableCustomerSearchViewModel> searchView, CommandDispatcher commandDispatcher)  
		{
			this.customerSearchView = searchView;
			this.commandDispatcher = commandDispatcher;
		}
		protected override void Handle(MakeAccountsReceivableTransaction command)
		{
			AccountsReceivableTransaction transaction = new AccountsReceivableTransaction();
			transaction.CorrelationId = command.CorrelationId;

			if (command.AccountNumber == 0)
			{
				customerSearchView.ViewModel.ShowPaymentScreen = command.ShowPaymentScreen;
				customerSearchView.ViewModel.SetCorrelationId(command.CorrelationId);
				customerSearchView.ViewModel.StartedFromCashReceipts = command.StartedFromCashReceipts;
				customerSearchView.Show();
			}
			else
			{
				commandDispatcher.Send(new ShowARPaymentStatusScreen()
				{
					AccountNumber = command.AccountNumber,
					CorrelationId = command.CorrelationId,
					IsPaymentScreen = command.ShowPaymentScreen,
					StartedFromCashReceipts = command.StartedFromCashReceipts
				});
			}
		}
	}
}
