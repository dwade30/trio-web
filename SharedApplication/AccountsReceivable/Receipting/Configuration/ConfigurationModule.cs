﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;

namespace SharedApplication.AccountsReceivable.Receipting.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<AccountsReceivableCustomerSearchViewModel>().As<IAccountsReceivableCustomerSearchViewModel>();
			builder.RegisterType<AccountsReceivablePaymentViewModel>().As<IAccountsReceivablePaymentViewModel>();
			builder.RegisterType<PaymentService>().As<IPaymentService>();
		}

	}
}
