﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class ARBill
	{
		public DateTime? BillDate { get; set; }
		public int Id { get; set; }
		public int BillType { get; set; } = 0;
		public string BillStatus { get; set; } = "";
		public int? BillNumber { get; set; } = 0;
		public string InvoiceNumber { get; set; } = "";
		public string CreatedBy { get; set; } = "";
		public DateTime? RateCreatedDate { get; set; }
		public DateTime? RateStart { get; set; }
		public DateTime? RateEnd { get; set; }
		public DateTime? IntPaidDate { get; set; }
		public string Reference { get; set; } = "";
		public string Control1 { get; set; } = "";
		public string Control2 { get; set; } = "";
		public string Control3 { get; set; } = "";
		public decimal? PrinOwed { get; set; } = 0;
		public decimal? TaxOwed { get; set; } = 0;
		public decimal? IntAdded { get; set; } = 0;
		public decimal? PrinPaid { get; set; } = 0;
		public decimal? TaxPaid { get; set; } = 0;
		public decimal? IntPaid { get; set; } = 0;
		public decimal PendingPrincipalAmount { get; set; } = 0;
		public decimal PendingTaxAmount { get; set; } = 0;
		public decimal PendingInterestAmount { get; set; } = 0;
		public decimal PendingChargedInterest { get; set; } = 0;
		public bool Pending { get; set; } = false;
		public decimal BalanceRemaining { get; set; } = 0;
		public string Bname { get; set; } = "";
		public string Bname2 { get; set; } = "";
		public string City { get; set; } = "";
		public string State { get; set; } = "";
		public string Address1 { get; set; } = "";
		public string Address2 { get; set; } = "";
		public string Address3 { get; set; } = "";
		public string Zip { get; set; } = "";
		public string Zip4 { get; set; } = "";
		public double InterestRate { get; set; } = 0;
		public decimal FlatRate { get; set; } = 0;
		public string InterestMethod { get; set; } = "";
		public decimal CurrentInterest { get; set; } = 0;
		public decimal PerDiem { get; set; } = 0;
		public decimal Amount1 { get; set; } = 0;
		public decimal Amount2{ get; set; } = 0;
		public decimal Amount3 { get; set; } = 0;
		public decimal Amount4 { get; set; } = 0;
		public decimal Amount5 { get; set; } = 0;
		public decimal Amount6 { get; set; } = 0;
		public DateTime? LastInterestDate { get; set; }
		public DateTime? CurrentInterestDate { get; set; }
		public DateTime? InterestStartDate { get; set; }
		public DateTime? PreviousInterestDate { get; set; }
		public DateTime? SavedPreviousInterestDate { get; set; }
		public List<ARPayment> Payments { get; set; } = new List<ARPayment>();
	}
}
