﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;

namespace SharedApplication.AccountsReceivable.Receipting.Interfaces
{
	public interface IAccountsReceivableCustomerSearchViewModel
	{
		int LastAccountAccessed { get; set; }
		bool StartedFromCashReceipts { get; set; }
		bool ShowLastAccount();
		void Cancel();
		void SetCorrelationId(Guid correlationID);
		bool ShowPaymentScreen { get; set; }
		void ProceedToNextScreen(int account);
		bool SearchResultsVisible { get; set; }
		CustomerMaster FindCustomer(int accountNumber, bool deletedOnly);
		void SetSelectedCustomer(int account, bool deletedOnly = false);
		CustomerMaster SelectedCustomer { get; set; }
		bool UndeleteCustomer(int account);
		void Search(AccountsReceivableCustomerSearchViewModel.SearchType searchType, string criteria);
		IEnumerable<CustomerPaymentStatusResult> SearchResults { get; }
	}
}
