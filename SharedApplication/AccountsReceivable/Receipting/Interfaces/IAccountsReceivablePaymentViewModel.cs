﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData.Models;
using SharedApplication.Models;

namespace SharedApplication.AccountsReceivable.Receipting.Interfaces
{
	public interface IAccountsReceivablePaymentViewModel
	{
		bool IsPaymentScreen { get; set; }
		void SetCorrelationId(Guid correlationID);
		AccountsReceivableTransaction Transaction { get; set; }
		DateTime EffectiveDate { get; set; }
		bool StartedFromCashReceipts { get; set; }
		bool IsCashReceiptsActive();
		bool ShowAllBills { get; set; }
		List<ARBill> Bills { get; }
		CustomerPaymentStatusResult Customer { get; }
		GroupMaster GroupInfo { get; }
		List<ARPayment> BillPayments(ARBill bill);
		CalculateBillResult CalculateBill(ARBill bill);
		decimal TotalPerDiem { get; }
		ARBill GetBill(int id);
		ARBill GetBill(string invoiceNumber);
		ARBill GetBill(ARPayment payment);
		Comment AccountComment { get; set; }
		bool PopUpComment();
		void UpdateComment(string comment);
		void UpdateCommentPriority(int priorityLevel);
		void Cancel();
		bool DontFireCollapse { get; set; }
		bool NegativeBillsExist { get; }
		void AdjustNegativeBillsToAddPayment();
		ARPayment GetPayment(int id);
		bool LatestPaymentOnBill(ARPayment payment);
		ARPayment ChargedInterestToReverse { get; set; }
		bool ChargedInterestExistsOnBill(int id);
		bool BudgetaryExists { get; }
		bool ValidManualAccount(string account, string invoiceNumber);
		DefaultBillType GetDefaultBillType(int typeCode);
		string GetPaymentReceiptNumber(int receiptKey, DateTime? receiptDateTime);
		bool PaymentGreaterThanOwed(string invoiceNumber, decimal paymentTotal);
		void AddPendingPayment(string invoiceNumber, string code, string reference, string cashDrawer, string generaslLedger, string budgetaryAccountNumber, string comment, DateTime? transactionDate, decimal principal, decimal tax, decimal interest, bool skipRecalc = false);
		bool PendingPaymentsExistForSelectedBill(string invoiceNumber);
		bool PendingPaymentsExist();
		decimal TotalPendingPaymentsAmount();
		void RemoveAllPendingPayments();
		void RemovePendingPayment(ARPayment payment);
		void UpdateBalanceRemainingOnBill(ARBill bill);
		ARPayment GetPendingPayment(Guid pendingId);
		void GoToSearchScreen();
		void CompleteTransaction();
		
		event EventHandler AccountChanged;
		event EventHandler<ARPayment> PendingPaymentAdded;
	}
}
