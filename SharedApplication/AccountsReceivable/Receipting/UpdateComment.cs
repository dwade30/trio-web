﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class UpdateComment : SharedApplication.Messaging.Command<bool>
	{
		public int CustomerId { get; set; }
		public string Comment { get; set; } = null;
		public int Priority { get; set; } = -1;

	}
}
