﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class AccountsReceivableTransaction
	{
		public Guid CorrelationId { get; set; }
		public int AccountNumber { get; set; }
		public Guid Id { get; set; } = Guid.NewGuid();
		public IEnumerable<ARBill> Bills { get; set; } = new List<ARBill>();
	}
}
