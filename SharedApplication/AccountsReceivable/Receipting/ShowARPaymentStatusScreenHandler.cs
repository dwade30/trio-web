﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class ShowARPaymentStatusScreenHandler : CommandHandler<ShowARPaymentStatusScreen>
	{
		private IView<IAccountsReceivablePaymentViewModel> paymentView;
		private CRTransactionGroupRepository crTransactionRepository;

		public ShowARPaymentStatusScreenHandler(IView<IAccountsReceivablePaymentViewModel> paymentView, CRTransactionGroupRepository crTransactionRepository)
		{
			this.paymentView = paymentView;
			this.crTransactionRepository = crTransactionRepository;
		}

		protected override void Handle(ShowARPaymentStatusScreen command)
		{
			AccountsReceivableTransaction transaction = new AccountsReceivableTransaction();
			transaction.CorrelationId = command.CorrelationId;
			transaction.AccountNumber = command.AccountNumber;

			var crTran = crTransactionRepository.Get(command.CorrelationId);

			if (crTran != null)
			{
				var transactions = crTran.AccountsReceivableTransactions(command.AccountNumber).ToList();
				if (transactions.Any())
				{
					List<ARBill> existingBills = new List<ARBill>();

					foreach (var trans in transactions)
					{
						existingBills.AddRange(trans.TransactionDetails.Bills);
					}
					transaction.Bills = existingBills;
				}
			}

			paymentView.ViewModel.SetCorrelationId(command.CorrelationId);
			paymentView.ViewModel.IsPaymentScreen = command.IsPaymentScreen;
			paymentView.ViewModel.StartedFromCashReceipts = command.StartedFromCashReceipts;
			paymentView.ViewModel.Transaction = transaction;
			paymentView.Show();

		}


	}
}
