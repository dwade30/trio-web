﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Models;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.AccountsReceivable.Receipting
{
	public class AccountsReceivablePaymentViewModel : IAccountsReceivablePaymentViewModel
	{
		private Guid correlationID;
		private AccountsReceivableTransaction transaction;
		private List<ARBill> bills = new List<ARBill>();
		private bool isPaymentScreen;

		private EventPublisher eventPublisher;
		private CommandDispatcher commandDispatcher;
		private IGlobalActiveModuleSettings activeModuleSettings;
		private IPaymentService paymentService;
		private GlobalAccountsReceivableSettings globalAccountsReceivableSettings;
		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>> customerPartyQueryHandler;
		private IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>> billQueryHandler;
		private IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler;
		private IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>> commentQueryHandler;
		private IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>> defaultBillTypeQueryHandler;
		private IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler;
		private IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>> lastYearReceiptQueryHandler;
		private IAccountValidationService accountValidationService;
		private IBudgetaryAccountingService budgetaryAccountingService;
		private CRTransactionGroupRepository crTransactionRepository;

		private CustomerPaymentStatusResult customer;
		private GroupMaster groupMaster;
		private Comment comment;
		private ARPayment chargedInterestToReverse;
		private DateTime effectiveDate;

		public AccountsReceivablePaymentViewModel(EventPublisher eventPublisher,  GlobalAccountsReceivableSettings globalAccountsReceivableSettings, CommandDispatcher commandDispatcher, IQueryHandler<CustomerPartySearchCriteria, IEnumerable<CustomerPaymentStatusResult>> customerPartyQueryHandler, IQueryHandler<ARBillSearchCriteria, IEnumerable<ARBill>> billQueryHandler, IGlobalActiveModuleSettings activeModuleSettings, IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler, IPaymentService paymentService, IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>> commentQueryHandler, IQueryHandler<DefaultBillTypeSearchCriteria, IEnumerable<DefaultBillType>> defaultBillTypeQueryHandler, IAccountValidationService accountValidationService, IBudgetaryAccountingService budgetaryAccountingService, IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler, IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>> lastYearReceiptQueryHandler, CRTransactionGroupRepository crTransactionRepository)
		{
			this.eventPublisher = eventPublisher;
			this.globalAccountsReceivableSettings = globalAccountsReceivableSettings;
			this.commandDispatcher = commandDispatcher;
			this.customerPartyQueryHandler = customerPartyQueryHandler;
			this.billQueryHandler = billQueryHandler;
			this.activeModuleSettings = activeModuleSettings;
			this.groupMasterQueryHandler = groupMasterQueryHandler;
			this.paymentService = paymentService;
			this.commentQueryHandler = commentQueryHandler;
			this.defaultBillTypeQueryHandler = defaultBillTypeQueryHandler;
			this.accountValidationService = accountValidationService;
			this.budgetaryAccountingService = budgetaryAccountingService;
			this.receiptQueryHandler = receiptQueryHandler;
			this.lastYearReceiptQueryHandler = lastYearReceiptQueryHandler;
			this.crTransactionRepository = crTransactionRepository;

			EffectiveDate = DateTime.Now;
			
		}

		private void GetComment()
		{
			comment = this.commentQueryHandler.ExecuteQuery(new CommentSearchCriteria
			{
				CustomerId = customer.CustomerId,
			}).FirstOrDefault();
		}

		public void GoToSearchScreen()
		{
			commandDispatcher.Send(new MakeAccountsReceivableTransaction
			{
				StartedFromCashReceipts = StartedFromCashReceipts,
				CorrelationId = correlationID,
				AccountNumber = 0,
				Id = transaction.Id,
				ShowPaymentScreen = isPaymentScreen
			});
		}

		public bool DontFireCollapse { get; set; }

		public bool BudgetaryExists
		{
			get => activeModuleSettings.BudgetaryIsActive;
		}

		public DefaultBillType GetDefaultBillType(int typeCode)
		{
			return defaultBillTypeQueryHandler.ExecuteQuery(new DefaultBillTypeSearchCriteria
			{
				TypeCode = typeCode
			}).FirstOrDefault();
		}

		public bool ValidManualAccount(string account, string invoiceNumber)
		{
			if (activeModuleSettings.BudgetaryIsActive)
			{
				if (!accountValidationService.AccountValidate(account))
				{
					return false;
				}

				var bill = GetBill(invoiceNumber);
				if (bill != null)
				{
					var defaultBillType = defaultBillTypeQueryHandler.ExecuteQuery(new DefaultBillTypeSearchCriteria
					{
						TypeCode = bill.BillType
					}).FirstOrDefault();

					if (defaultBillType != null)
					{
						var tempFund = budgetaryAccountingService.GetFundFromAccount(account);
						if (tempFund == 0 || budgetaryAccountingService.GetFundFromAccount(account) != budgetaryAccountingService.GetFundFromAccount(defaultBillType.Account1))
						{
							return false;
						}
					}
				}

				return true;
			}
			else
			{
				return account.Left(1) == "M" && account.Length > 2;
			}
		}

		public bool NegativeBillsExist
		{
			get { return bills.Any(x => x.BalanceRemaining < 0 && x.BillNumber != 0); }
		}

		public void AdjustNegativeBillsToAddPayment()
		{
			decimal totalAdjustment = 0;

			foreach (var bill in bills.Where(x => x.BalanceRemaining < 0 && x.BillNumber != 0))
			{
				var billAmounts = CalculatePaymentAmountsForBill(bill, 0, true);

				AddPendingPayment(bill.InvoiceNumber, "P", "AUTO", "Y", "Y", "", "", null,billAmounts.Principal, billAmounts.Tax, billAmounts.Interest, true);
				totalAdjustment += billAmounts.RemainingAmount;
			}

			if (totalAdjustment > 0)
			{
				AddPendingPayment("Auto", "P", "AUTO", "Y", "Y", "", "", null, totalAdjustment, 0, 0);
			}
		}

		public void RemoveAllPendingPayments()
		{
			bills.RemoveAll(x => x.Pending);

			foreach (var bill in bills)
			{
				foreach (var payment in bill.Payments.Where(x => x.Pending).ToList())
				{
					RemovePendingPayment(payment);
				}
			}
		}

		public void RemovePendingPayment(ARPayment payment)
		{
			var bill = GetBill(payment);

			if (payment.ChargedInterestRecord != null)
			{
				bill.LastInterestDate = bill.PreviousInterestDate;
				bill.PreviousInterestDate = bill.SavedPreviousInterestDate;
				bill.PendingChargedInterest = bill.PendingChargedInterest - payment.ChargedInterestRecord.Interest ?? 0;
			}

			bill.PendingInterestAmount = bill.PendingInterestAmount - (payment.Interest ?? 0);
			bill.PendingTaxAmount = bill.PendingTaxAmount - (payment.Tax ?? 0);
			bill.PendingPrincipalAmount = bill.PendingPrincipalAmount - (payment.Principal ?? 0);
			bill.Payments.Remove(payment);
			UpdateBalanceRemainingOnBill(bill);
		}

		public ARPayment GetPendingPayment(Guid pendingId)
		{
			return bills.SelectMany(x => x.Payments).FirstOrDefault(y => y.PendingId == pendingId);
		}

		public void UpdateBalanceRemainingOnBill(ARBill bill)
		{
			bill.BalanceRemaining = (decimal) bill.PrinOwed - (decimal) bill.PrinPaid - bill.PendingPrincipalAmount + (decimal) bill.TaxOwed -
			                        (decimal) bill.TaxPaid - bill.PendingTaxAmount + BillInterestAmount(bill);
		}

		public bool ChargedInterestExistsOnBill(int id)
		{
			return bills.FirstOrDefault(x => x.Id == id).Payments.Any(y => y.ChargedInterestRecord?.Reference == "CHGINT" || y.ChargedInterestRecord?.Reference == "EARNINT");
		}

		public void SetCorrelationId(Guid correlationID)
		{
			this.correlationID = correlationID;
		}

		public ARBill GetBill(int id)
		{
			return bills.FirstOrDefault(x => x.Id == id);
		}

		public ARBill GetBill(string invoiceNumber)
		{
			return bills.FirstOrDefault(x => x.InvoiceNumber == invoiceNumber.Replace("*", ""));
		}

		public ARBill GetBill(ARPayment payment)
		{
			return bills.FirstOrDefault(x => x.Payments.Contains(payment));
		}

		public ARPayment GetPayment(int id)
		{
			return bills.SelectMany(x => x.Payments.Where(y => y.Id == id)).FirstOrDefault();
		}

		public bool LatestPaymentOnBill(ARPayment payment)
		{
			var bill = GetBill(payment);

			return !bill.Payments.Any(y => y.EffectiveInterestDate > payment.EffectiveInterestDate);
		}

		public ARPayment ChargedInterestToReverse
		{
			get => chargedInterestToReverse;
			set => chargedInterestToReverse = value;
		}

		public Comment AccountComment
		{
			get => comment;
			set => comment = value;
		}

		public void UpdateComment(string comment)
		{
			var result = commandDispatcher.Send(new UpdateComment
			{
				CustomerId = customer.CustomerId,
				Comment = comment
			}).Result;

			if (result)
				GetComment();
		}

		public void UpdateCommentPriority(int priorityLevel)
		{
			var result = commandDispatcher.Send(new UpdateComment
			{
				CustomerId = customer.CustomerId,
				Priority = priorityLevel
			}).Result;

			if (result)
				GetComment();
		}

		public bool PopUpComment()
		{
			return (comment?.Priority ?? 0) > 0;
		}

		public void Cancel()
		{
			eventPublisher.Publish(new TransactionCancelled(correlationID,null));
		}

		public CalculateBillResult CalculateBill(ARBill bill)
		{
			var result =  paymentService.CalculateBillTotals(bill, EffectiveDate);

			if (result.InterestCalculated)
			{
				bill.LastInterestDate = result.LastInterestDate;
				bill.CurrentInterest = result.CalculatedInterestResult.Interest * -1;
				bill.CurrentInterestDate = result.CalculatedInterestDate;
				bill.PerDiem = (decimal)result.CalculatedInterestResult.PerDiem;
				UpdateBalanceRemainingOnBill(bill);
			}
			
			return result;
		}

		public decimal TotalPerDiem
		{
			get
			{
				return bills.Select(x => x.PerDiem).DefaultIfEmpty(0).Sum();
			}
		}

		public List<ARBill> Bills
		{
			get
			{
				if (bills == null)
				{
					return new List<ARBill>();
				}
				else
				{
					var result = bills;
					if (!ShowAllBills)
					{
						result = bills.Where(x => x.BalanceRemaining + x.PendingInterestAmount + x.PendingTaxAmount + x.PendingPrincipalAmount != 0 || x.BillNumber == 0).ToList();
					}
					return result.OrderBy(x => x.BillStatus == "D" ? 0 : 1).ThenByDescending(y => y.BillDate).ThenByDescending(z => z.InvoiceNumber).ThenByDescending(a => a.BillNumber).ToList();
				}
			}
		}

		public GroupMaster GroupInfo
		{
			get { return groupMaster; }
		}

		public CustomerPaymentStatusResult Customer
		{
			get { return customer; }
		}

		public bool PaymentGreaterThanOwed(string invoiceNumber, decimal paymentTotal)
		{
			if (invoiceNumber != "Auto")
			{
				var bill = GetBill(invoiceNumber);

				if (bill != null)
				{
					return paymentTotal > bill.BalanceRemaining;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return paymentTotal > bills.Select(x => x.BalanceRemaining).DefaultIfEmpty(0).Sum();
			}
		}

		private decimal BillInterestAmount(ARBill bill)
		{
			return ((bill.IntAdded ?? 0) * -1) - (bill.IntPaid ?? 0) - bill.CurrentInterest - bill.PendingInterestAmount;
		}

		private BillPaymentCalculationResult CalculatePaymentAmountsForBill(ARBill bill, decimal paymentAmount, bool negativeBill = false)
		{
			var result = new BillPaymentCalculationResult();

			result.RemainingAmount = paymentAmount;

			decimal intNeeded = BillInterestAmount(bill);
			if (intNeeded < 0 && !negativeBill)
			{
				intNeeded = 0;
			}

			if (intNeeded >= result.RemainingAmount)
			{
				result.Interest = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Interest = intNeeded;
				result.RemainingAmount = result.RemainingAmount - intNeeded;
			}

			decimal taxNeeded = (bill.TaxOwed ?? 0) - (bill.TaxPaid ?? 0);
			if (taxNeeded < 0 && !negativeBill)
			{
				taxNeeded = 0;
			}

			if (taxNeeded >= result.RemainingAmount)
			{
				result.Tax = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Tax = taxNeeded;
				result.RemainingAmount = result.RemainingAmount - taxNeeded;
			}

			decimal principalNeeded = (bill.PrinOwed ?? 0) - (bill.PrinPaid ?? 0);
			if (principalNeeded < 0 && !negativeBill)
			{
				principalNeeded = 0;
			}
			if (principalNeeded >= result.RemainingAmount)
			{
				result.Principal = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Principal = principalNeeded;
				result.RemainingAmount = result.RemainingAmount - principalNeeded;
			}

			return result;
		}

		private ARPayment AddPendingChargedInterest(ARBill bill, ARPayment linkedPayment)
		{
			var chargedInterest = new ARPayment();


			chargedInterest.Pending = true;
			chargedInterest.PendingId = Guid.NewGuid();
			chargedInterest.ActualSystemDate = linkedPayment.ActualSystemDate;
			chargedInterest.RecordedTransactionDate = linkedPayment.RecordedTransactionDate;
			chargedInterest.EffectiveInterestDate = linkedPayment.EffectiveInterestDate;
			chargedInterest.Code = "I";
			chargedInterest.Principal = 0;
			chargedInterest.Tax = 0;
			if (chargedInterestToReverse != null)
			{
				chargedInterest.Interest = chargedInterestToReverse.Interest * -1;
				chargedInterest.Reference = chargedInterest.Interest > 0 ? "Interest" : "CHGINT";
				chargedInterest.ChargedInterestDate = bill.PreviousInterestDate;
				var temp = bill.LastInterestDate;
				bill.LastInterestDate = bill.PreviousInterestDate;
				bill.PreviousInterestDate = temp;
			}
			else
			{
				chargedInterest.Interest = bill.CurrentInterest - bill.PendingChargedInterest;
				chargedInterest.Reference = chargedInterest.Interest > 0 ? "EARNINT" : "CHGINT";
				chargedInterest.ChargedInterestDate = bill.CurrentInterestDate;
				bill.PreviousInterestDate = bill.LastInterestDate;
				bill.LastInterestDate = bill.CurrentInterestDate;
			}
				
			chargedInterest.GeneralLedger = linkedPayment.GeneralLedger;
			chargedInterest.CashDrawer = linkedPayment.CashDrawer;
			chargedInterest.BudgetaryAccountNumber = linkedPayment.BudgetaryAccountNumber;

			bill.PendingChargedInterest = bill.PendingChargedInterest + chargedInterest.Interest ?? 0;

			chargedInterestToReverse = null;

			return chargedInterest;
		}

		private ARBill GetPrepaymentBill()
		{
			return bills.FirstOrDefault(x => x.BillNumber == 0);
		}

		private void AddPendingOverPayment(decimal amount, string comment, string cashDrawer, string budgetaryAccountNumber, DateTime? transactionDate, bool isPrepayment)
		{
			var payment = new ARPayment();
			var bill = new ARBill();

			if (isPrepayment)
			{
				bill = GetPrepaymentBill();

				if (bill == null)
				{
					bill = new ARBill
					{
						Pending = true,
						BillStatus = "D",
						BillNumber = 0,
						InvoiceNumber = "0",
					};

					bills.Add(bill);
				}

				payment.Code = "Y";
			}
			else
			{
				payment.Code = "P";
				bill = bills.OrderByDescending(x => x.InvoiceNumber).FirstOrDefault();
			}
			
			payment.ActualSystemDate = DateTime.Today;
			payment.Reference = "PREPAY-A";
			payment.EffectiveInterestDate = EffectiveDate;
			payment.GeneralLedger = "Y";
			payment.Principal = amount;
			payment.CashDrawer = cashDrawer;
			payment.Pending = true;
			payment.PendingId = Guid.NewGuid();
			if (transactionDate == null)
			{
				payment.RecordedTransactionDate = DateTime.Today;
			}
			else
			{
				payment.RecordedTransactionDate = transactionDate;
			}

			if (cashDrawer == "N")
			{
				payment.BudgetaryAccountNumber = budgetaryAccountNumber;
			}
			else
			{
				payment.BudgetaryAccountNumber = "";
			}

			payment.Comments = comment;
			AddPaymentToBill(bill, payment);
		}

		public bool PendingPaymentsExistForSelectedBill(string invoiceNumber)
		{
			if (invoiceNumber != "Auto")
			{
				return GetBill(invoiceNumber)?.Payments.Any(x => x.Pending == true) ?? false;
			}
			else
			{
				return PendingPaymentsExist();
			}
		}

		public bool PendingPaymentsExist()
		{
			return bills.SelectMany(x => x.Payments).Any(y => y.Pending);
		}

		public decimal TotalPendingPaymentsAmount()
		{
			return bills.Select(x => x.PendingInterestAmount + x.PendingTaxAmount + x.PendingPrincipalAmount).Sum();
		}

		public void AddPendingPayment(string invoiceNumber, string code, string reference, string cashDrawer, string generaslLedger, string budgetaryAccountNumber, string comment, DateTime? transactionDate, decimal principal, decimal tax, decimal interest, bool skipRecalc = false)
		{
			if (invoiceNumber == "Auto")
			{
				decimal totalAmount = principal + tax + interest;

				foreach (var bill in bills)
				{
					if (bill.BalanceRemaining > 0)
					{
						var billAmounts = CalculatePaymentAmountsForBill(bill, totalAmount);
						var payment = new ARPayment();

						payment.Interest = billAmounts.Interest;
						payment.Tax = billAmounts.Tax;
						payment.Principal = billAmounts.Principal;
						payment.Pending = true;
						payment.PendingId = Guid.NewGuid();
						payment.ActualSystemDate = DateTime.Today;
						payment.CashDrawer = cashDrawer;
						payment.Code = code;
						payment.Comments = comment;
						payment.EffectiveInterestDate = EffectiveDate;
						payment.GeneralLedger = generaslLedger;
						if (transactionDate == null)
						{
							payment.RecordedTransactionDate = DateTime.Today; 
						}
						else
						{
							payment.RecordedTransactionDate = transactionDate;
						}
						
						if (cashDrawer == "N")
						{
							payment.BudgetaryAccountNumber = budgetaryAccountNumber;
						}
						else
						{
							payment.BudgetaryAccountNumber = "";
						}

						payment.Reference = reference;
						if (bill.CurrentInterest - bill.PendingChargedInterest != 0)
						{
							payment.ChargedInterestRecord = AddPendingChargedInterest(bill, payment);
						}

						totalAmount = billAmounts.RemainingAmount;
						AddPaymentToBill(bill, payment);

						if (totalAmount == 0)
						{
							break;
						}
					}
				}

				if (totalAmount > 0)
				{
					AddPendingOverPayment(totalAmount, comment, cashDrawer, budgetaryAccountNumber, transactionDate, true);
				}
			}
			else
			{
				ARBill bill;
				if (code == "Y" || invoiceNumber.Contains("*") || invoiceNumber == "0")
				{
					bill = GetPrepaymentBill();
                    if (bill == null)
                    {
                        bill = new ARBill
                        {
                            Pending = true,
                            BillStatus = "D",
                            BillNumber = 0,
                            InvoiceNumber = "0",
                        };

                        bills.Add(bill);
                    }

				}
				else
				{
					bill = GetBill(invoiceNumber);
				}

				decimal totalAmount = principal + tax + interest;
				BillPaymentCalculationResult billAmounts = new BillPaymentCalculationResult();

				if ((code == "P" || !(code == "C" && interest < 0)) && !(reference.ToUpper() == "REVERS" || code == "C") && !skipRecalc)
				{
					billAmounts = CalculatePaymentAmountsForBill(bill, totalAmount);
					if (billAmounts.RemainingAmount > 0)
					{
						billAmounts.Principal = billAmounts.Principal + billAmounts.RemainingAmount;
						billAmounts.RemainingAmount = 0;
					}
				}
				else
				{
					billAmounts.Principal = principal;
					billAmounts.Interest = interest;
					billAmounts.Tax = tax;
					billAmounts.RemainingAmount = 0;
				}

				var payment = new ARPayment();

				payment.Interest = billAmounts.Interest;
				payment.Tax = billAmounts.Tax;
				payment.Principal = billAmounts.Principal;
				payment.Pending = true;
				payment.PendingId = Guid.NewGuid();
				payment.ActualSystemDate = DateTime.Today;
				payment.CashDrawer = cashDrawer;
				payment.Code = code;
				payment.Comments = comment;
				if (reference == "REVERS")
				{
					payment.EffectiveInterestDate = bill.PreviousInterestDate??EffectiveDate;
				}
				else
				{
					payment.EffectiveInterestDate = EffectiveDate;
				}
				
				payment.GeneralLedger = generaslLedger;
				if (transactionDate == null)
				{
					payment.RecordedTransactionDate = DateTime.Today;
				}
				else
				{
					payment.RecordedTransactionDate = transactionDate;
				}

				if (cashDrawer == "N")
				{
					payment.BudgetaryAccountNumber = budgetaryAccountNumber;
				}
				else
				{
					payment.BudgetaryAccountNumber = "";
				}

				payment.Reference = reference;
				if (reference == "REVERS")
				{
					if (ChargedInterestToReverse != null)
					{
						payment.ChargedInterestRecord = AddPendingChargedInterest(bill, payment);
					}
				}
				else
				{
					if (bill.CurrentInterest - bill.PendingChargedInterest != 0)
					{
						payment.ChargedInterestRecord = AddPendingChargedInterest(bill, payment);
					}
				}
				

				AddPaymentToBill(bill, payment);
			}
		}

		private void AddPaymentToBill(ARBill bill, ARPayment payment)
		{
			bill.PendingInterestAmount = bill.PendingInterestAmount + (payment.Interest ?? 0);
			bill.PendingTaxAmount = bill.PendingTaxAmount + (payment.Tax ?? 0);
			bill.PendingPrincipalAmount = bill.PendingPrincipalAmount + (payment.Principal ?? 0);

			UpdateBalanceRemainingOnBill(bill);

			payment.PaidBy = customer.FullName;

			bill.Payments.Add(payment);
			if (PendingPaymentAdded != null)
			{
				PendingPaymentAdded(this, payment);
			}
		}

		public string GetPaymentReceiptNumber(int receiptKey, DateTime? receiptDateTime)
		{
			if (receiptKey > 0)
			{
				if (activeModuleSettings.CashReceiptingIsActive)
				{
					var receipt = receiptQueryHandler.ExecuteQuery(new ReceiptSearchCriteria
					{
						ReceiptId = receiptKey,
						ReceiptDate = receiptDateTime
					}).FirstOrDefault();

					if (receipt != null)
					{
						return receipt.ReceiptNumber.ToString();
					}
					else
					{
						var lastReceipt = lastYearReceiptQueryHandler.ExecuteQuery(new LastYearReceiptSearchCriteria
						{
							ReceiptId = receiptKey,
							ReceiptDate = receiptDateTime
						}).FirstOrDefault();

						if (lastReceipt != null)
						{
							return lastReceipt.ReceiptNumber.ToString();
						}
						else
						{
							return receiptKey.ToString();
						}
					}
				}
				else
				{
					return receiptKey.ToString();
				}
			}
			else
			{
				return "0";
			}
		}

		private void GetBillInfo()
		{
			bills = billQueryHandler.ExecuteQuery(new ARBillSearchCriteria
			{
				CustomerId = customer.Id,
				VoidedSelection = VoidedStatusSelection.NotVoided
			}).ToList();

			CalculateCurrentInterest();

			if (transaction.Bills != null)
			{
				foreach (var existingBill in transaction.Bills)
				{
					if (existingBill.Pending)
					{
						bills.Add(existingBill);
					}
					else
					{
						var bill = GetBill(existingBill.Id);
						if (bill != null)
						{
							foreach (var payment in existingBill.Payments.Where(x => x.Pending))
							{
								AddPaymentToBill(bill, payment);
							}
						}
					}
				}
			}
		}

		private void CalculateCurrentInterest()
		{
			foreach (var bill in bills)
			{
				CalculateBill(bill);
			}
		}

		public bool ShowAllBills { get; set; }

		public bool StartedFromCashReceipts { get; set; }

		public bool IsCashReceiptsActive()
		{
			return activeModuleSettings.CashReceiptingIsActive;
		}

		public bool IsPaymentScreen
		{
			get => isPaymentScreen;
			set => isPaymentScreen = value;
		}

		public DateTime EffectiveDate
		{
			get => effectiveDate;
			set
			{
				effectiveDate = value;
				CalculateCurrentInterest();
			}
		}

		public void CompleteTransaction()
		{
			if (StartedFromCashReceipts)
			{
				var crTran = crTransactionRepository.Get(correlationID);

				if (crTran != null)
				{
					var transactions = crTran.AccountsReceivableTransactions(customer.CustomerId).ToList();
					if (transactions.Any())
					{
						transactions.ForEach(x => crTran.RemoveTransaction(x.Id));
					}
				}

				eventPublisher.Publish(new TransactionCompleted
				{
					CorrelationId = correlationID,
					CompletedTransactions = ConvertTransactionToAccountsReceivableTransactionBase(transaction)
				});
			}
			else
			{
				commandDispatcher.Send(new FinalizeTransactions<AccountsReceivableTransactionBase>
				{
					CorrelationIdentifier = correlationID,
					ReceiptId = 0,
					TellerId = "",
					Transactions = ConvertTransactionToAccountsReceivableTransactionBase(transaction)
				});
			}
		}

		private IEnumerable<AccountsReceivableTransactionBase> ConvertTransactionToAccountsReceivableTransactionBase(AccountsReceivableTransaction transaction)
		{
			var result = new List<AccountsReceivableTransactionBase>();
			foreach (var bill in bills.Where(x => x.Pending || x.Payments.Any(y => y.Pending)))
			{
				var billType = GetDefaultBillType(bill.BillType);
				var controls = new List<ControlItem>();

				if (billType.Control1.Trim() != "")
				{
					controls.Add(new ControlItem
					{
						Name = "Control1",
						Description = billType.Control1.Trim(),
						Value = bill.Control1
					});
				}

				if (billType.Control2.Trim() != "")
				{
					controls.Add(new ControlItem
					{
						Name = "Control2",
						Description = billType.Control2.Trim(),
						Value = bill.Control2
					});
				}

				if (billType.Control3.Trim() != "")
				{
					controls.Add(new ControlItem
					{
						Name = "Control3",
						Description = billType.Control3.Trim(),
						Value = bill.Control3
					});
				}

				foreach (var payment in bill.Payments.Where(x => x.Pending))
				{
					var cashReceiptsTransaction = new AccountsReceivableTransactionBase
					{
						ActualDateTime = (DateTime) payment.ActualSystemDate,
						EffectiveDateTime = (DateTime) payment.EffectiveInterestDate,
						RecordedTransactionDate = (DateTime) payment.RecordedTransactionDate,
						AccountNumber = customer.CustomerId,
						Comment = payment.Comments,
						Controls = controls,
						CorrelationIdentifier = transaction.CorrelationId,
						Id = transaction.Id,
						PayerName = customer.FullName,
						PayerPartyId = customer.PartyId,
						AffectCashDrawer = payment.CashDrawer == "Y" ? true : false,
						AffectCash = payment.GeneralLedger == "Y" ? true : false,
						IncludeInCashDrawerTotal = payment.CashDrawer == "Y" ? true : false,
						Code = payment.Code,
						BudgetaryAccountNumber = payment.BudgetaryAccountNumber,
						DefaultCashAccount = billType.DefaultAccount.Trim(),
						EPmtAccountID = billType.PayeeId?.ToString() ?? "",
						Reference = payment.Reference,
						TransactionTypeCode = "97",
						TransactionDetails = new AccountsReceivableTransaction
						{
							Id = Guid.NewGuid(),
							CorrelationId = transaction.CorrelationId,
							AccountNumber = customer.CustomerId,
							Bills = new List<ARBill>
							{
								bill
							}
						},
						TransactionTypeDescription = billType.TypeTitle,
                        OverrideTaxAccount = billType.OverrideTaxAccount,
                        OverrideInterestAccount = billType.OverrideIntAccount
					};

					cashReceiptsTransaction.AddTransactionAmounts(new List<TransactionAmountItem>
					{
						new TransactionAmountItem
						{
							Name = "Principal", 
							Description = "Principal",
							Total = bill.PendingPrincipalAmount
						},
						new TransactionAmountItem
						{
							Name = "Interest",
							Description = "Interest",
							Total = bill.PendingInterestAmount
						},
						new TransactionAmountItem
						{
							Name = "Sales Tax",
							Description = "Sales Tax",
							Total = bill.PendingTaxAmount
						}
					});

					result.Add(cashReceiptsTransaction);
				}
			}
			
			return result;
		}

		public AccountsReceivableTransaction Transaction
		{
			get => transaction;
			set
			{
				transaction = value;

				customer = this.customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
				{
					AccountNumber = transaction.AccountNumber
				}).FirstOrDefault();

				GetComment();

				groupMaster = this.groupMasterQueryHandler.ExecuteQuery(new GroupMasterSearchCriteria
				{
					AssociatedWith = new ModuleAssociationSearchCriteria
					{
						AccountNumber = customer?.CustomerId ?? 0,
						Module = "AR"
					}
				}).FirstOrDefault();

				GetBillInfo();

				if (customer != null)
				{
					if (AccountChanged != null)
					{
						AccountChanged(this, new EventArgs());
					}
				}
			}
		}

		public event EventHandler AccountChanged;

		public event EventHandler<ARPayment> PendingPaymentAdded; 

		public List<ARPayment> BillPayments(ARBill bill)
		{
			return bill.Payments.OrderBy(d => d.Pending).OrderBy(a => a.RecordedTransactionDate)
				.ThenBy(b => b.ActualSystemDate).ThenBy(c => c.ReceiptId).ThenBy(d => d.Id).ToList();
		}

		private class BillPaymentCalculationResult
		{
			public decimal Principal { get; set; }
			public decimal Tax { get; set; }
			public decimal Interest { get; set; }
			public decimal RemainingAmount { get; set; }
		}
	}
}
