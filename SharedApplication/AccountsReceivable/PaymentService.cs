﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Models;

namespace SharedApplication.AccountsReceivable
{
	public class PaymentService : IPaymentService
	{
		private GlobalAccountsReceivableSettings globalAccountsReceivableSettings;
		private IInterestCalculationService interestCalculationService;

		public PaymentService(GlobalAccountsReceivableSettings globalAccountsReceivableSettings, IInterestCalculationService interestCalculationService)
		{
			this.globalAccountsReceivableSettings = globalAccountsReceivableSettings;
			this.interestCalculationService = interestCalculationService;
		}

		public CalculateBillResult CalculateBillTotals(ARBill bill, DateTime effectiveDate)
		{
			DateTime dtStartDate;
			bool boolAutoInterest = false;
			bool boolPerDiem = false;

			int intDayAdded = 0;

			CalculateBillResult result = new CalculateBillResult();
			
		
			boolAutoInterest = bill.InterestMethod.Left(1) == "A";
			boolPerDiem = bill.InterestMethod.Right(1) == "P";
			
			if (bill.IntPaidDate != null && bill.IntPaidDate > (bill.InterestStartDate??DateTime.MinValue))
			{
				dtStartDate = (DateTime)bill.IntPaidDate;
				intDayAdded = 0;
			}
			else
			{
				dtStartDate = (bill.InterestStartDate??DateTime.MaxValue);
				intDayAdded = 1;
			}

			result.LastInterestDate = dtStartDate;
			result.CalculatedInterestDate = effectiveDate.Date;
			result.InterestCalculated = false;

			if (effectiveDate < dtStartDate)
			{
				result.CalculatedInterestResult.Interest = 0;
				result.CalculatedInterestDate = dtStartDate;
				result.CalculatedInterestResult.PerDiem = 0;
			}
			else
			{
				if (boolAutoInterest)
				{
					if (bill.PrinOwed - bill.PrinPaid > 0)
					{
						if (boolPerDiem)
						{
							result.CalculatedInterestResult = CalculateInterest((double)((bill.PrinOwed?? 0) - (bill.PrinPaid ?? 0)), dtStartDate, effectiveDate, bill.InterestRate, globalAccountsReceivableSettings.gintBasis, intDayAdded, 0);
							result.InterestCalculated = true;
						}
						else
						{
							if (bill.IntAdded == 0)
							{
								result.CalculatedInterestResult.Interest = bill.FlatRate;
								result.InterestCalculated = true;
							}
						}
					}
					else
					{
						result.CalculatedInterestResult.Interest = 0;
						result.InterestCalculated = false;
					}
				}
			}
			
			return result;
		}

		public CalculateInterestResult CalculateInterest(Double curAmount, DateTime dtInterestStartDate, DateTime dtDateNow, double dblRateAsDecimal, int intBasis, int usingStartDate, double dblOverPaymentIntRate = 0)
		{
			CalculateInterestResult result = new CalculateInterestResult();
			double dblPrincipalDue;
			int intDays;
			int lngPerDays;

			dblPrincipalDue = curAmount * -1;
			intDays = intBasis;
			if (intDays == 0)
				intDays = 360;

			TimeSpan difference = dtDateNow - dtInterestStartDate;
			lngPerDays = difference.Days + usingStartDate + 1;

			if (lngPerDays < 1)
			{
				result.Interest = 0;
				result.PerDiem = interestCalculationService.IPmt((dblRateAsDecimal / intDays), 1, 1, dblPrincipalDue);
			}
			else if (curAmount == 0)
			{
				result.Interest = 0;
				result.PerDiem = 0;
			}
			else
			{
				if (curAmount > 0)
				{
					result.PerDiem = interestCalculationService.IPmt((dblRateAsDecimal / intDays), 1, lngPerDays, dblPrincipalDue);
				}
				else
				{
					result.PerDiem = interestCalculationService.IPmt((dblOverPaymentIntRate / intDays), 1, lngPerDays, dblPrincipalDue);
				}
				result.Interest = (decimal)Math.Round(result.PerDiem * lngPerDays, 2, MidpointRounding.AwayFromZero);
			}
			return result;
		}

		
	}
}
