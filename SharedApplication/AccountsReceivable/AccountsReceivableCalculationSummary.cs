﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.AccountsReceivable
{
    public class AccountsReceivableCalculationSummary
    {
        public int Account { get; }
        public decimal Balance { get; set; }

        public AccountsReceivableCalculationSummary(int account)
        {
            Account = account;
        }

        public void AddTo(AccountsReceivableCalculationSummary billSummary)
        {
            Balance += billSummary.Balance;
        }
    }
}
