﻿using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.MVR;

namespace SharedApplication
{
    public class ShowMunicipalValuationReturnHandler : CommandHandler<ShowMunicipalValuationReturn>
    {
        private IView<IMunicipalValuationReturnViewModel> mvrView;

        public ShowMunicipalValuationReturnHandler(IView<IMunicipalValuationReturnViewModel> mvrView)
        {
            this.mvrView = mvrView;
        }
        protected override void Handle(ShowMunicipalValuationReturn command)
        {
            mvrView.ViewModel.TownId = command.TownId;            
            mvrView.ViewModel.LoadAndCalculateMVR();
            mvrView.ViewModel.ValuationReturn.ReportYear = command.ReportYear;
            mvrView.Show();
        }
    }
}