﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.CentralData.Extensions;

namespace SharedApplication
{
	public class SettingService : ISettingService
	{
		private CommandDispatcher commandDispatcher;
		private IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>> settingQueryHandler;

		public SettingService(CommandDispatcher commandDispatcher,
			IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>> settingQueryHandler)
		{
			this.commandDispatcher = commandDispatcher;
			this.settingQueryHandler = settingQueryHandler;


		}

		public Setting GetSettingValue(SettingOwnerType type, string settingName)
		{
			return settingQueryHandler.ExecuteQuery(new SettingSearchCriteria
			{
				SettingName = settingName,
                OwnerType = type
			}).FirstOrDefault();
		}

        public Setting GetSettingValue(SettingOwnerType type, string settingType, string settingName)
        {
            return settingQueryHandler.ExecuteQuery(new SettingSearchCriteria()
            {
                SettingName = settingName,
                SettingType = settingType,
                OwnerType = type
            }).FirstOrDefault();
        }

        public void SaveSetting(SettingOwnerType type, string name, string value)
		{
			commandDispatcher.Send(new SaveSetting
			{
				setting = new Setting
				{
					SettingName = name,
					SettingValue = value,
                    OwnerType = type.AsString()
				},
				type = type
			});
		}

        public void SaveSetting(SettingOwnerType type, string settingType, string name, string value)
        {
            commandDispatcher.Send(new SaveSetting
            {
                setting = new Setting
                {
                    SettingName = name,
                    SettingValue = value,
                    SettingType = settingType,
                    OwnerType = type.AsString()
                },
                type = type
            });
        }
    }
}
