﻿using SharedApplication.Messaging;
using SharedApplication.Models;

namespace SharedApplication.TaxBilling
{
    public class UpdateTaxBillingDatabase : Command<bool>
    {
        public SQLConfigInfo SqlConfigInfo { get; }
        public string DataEnvironment { get; }
        public UpdateTaxBillingDatabase(SQLConfigInfo sqlInfo, string dataEnvironment)
        {
            SqlConfigInfo = sqlInfo;
            DataEnvironment = dataEnvironment;
        }
    }
}