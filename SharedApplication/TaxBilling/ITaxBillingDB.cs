﻿using SharedApplication.Versioning;

namespace SharedApplication.TaxBilling
{
    public interface ITaxBillingDB
    {
        string LastErrorMessage { get; }
        string ArchiveGroup { get; set; }
        string DataEnvironment { get; set; }
        void ClearErrors();
        cVersionInfo GetVersion();
        void SetVersion( cVersionInfo nVersion);
        bool CheckVersion();
    }
}