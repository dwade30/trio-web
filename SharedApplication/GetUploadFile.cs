﻿using SharedApplication.Messaging;

namespace SharedApplication
{
    public class GetUploadFile : Command<string>
    {
        public string Title { get; }
        public GetUploadFile(string title)
        {
            Title = title;
        }
    }
}