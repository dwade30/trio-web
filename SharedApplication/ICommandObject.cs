﻿using System;

namespace SharedApplication
{
    public interface ICommandObject
    {
        void Execute();
    }
}