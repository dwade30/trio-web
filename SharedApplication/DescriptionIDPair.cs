﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
    public class DescriptionIDPair
    {
        public string Description { get; set; } = "";
        public int ID { get; set; } = 0;
    }

    public class DescriptionIDCodePair
    {
        public string Description { get; set; } = "";
        public string Code { get; set; } = "";
        public int ID { get; set; } = 0;
    }
    public class GenericDescriptionPair<TType>
    {
        public string Description { get; set; } = "";

        public TType ID { get; set; }
    }
}
