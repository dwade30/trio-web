﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;

namespace SharedApplication
{
    public class DomainEvents<TEventBase> where TEventBase : DomainEvent
    {
        private List<TEventBase> domainEvents = new List<TEventBase>();

        public IEnumerable<TEventBase> GetEvents()
        {
            return new List<TEventBase>(domainEvents);
        }

        public void AddEvent(TEventBase eventToAdd)
        {
            if (eventToAdd != null)
            {
                domainEvents.Add(eventToAdd);
            }
        }

        public void AddEvents(IEnumerable<TEventBase> eventsToAdd)
        {
            if (eventsToAdd != null)
            {
                domainEvents.AddRange(eventsToAdd);
            }
        }

        public void AddEvents(DomainEvents<TEventBase> eventsToAdd)
        {
            if (eventsToAdd != null)
            {
                AddEvents(eventsToAdd.GetEvents());
            }
        }

        public void RemoveEvent(Guid eventId)
        {
            domainEvents.RemoveAll(de => de.Id == eventId);
        }

        public void RemoveEvents(IEnumerable<Guid> eventIds)
        {
            foreach (var eventId in eventIds)
            {
                RemoveEvent(eventId);
            }
        }

        public bool HasEvent(Guid eventId)
        {
            return domainEvents.Any(de => de.Id == eventId);
        }
    }
}