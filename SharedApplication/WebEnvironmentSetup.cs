﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SharedApplication.Models;

namespace SharedApplication
{
	public class WebEnvironmentSetup : IWebEnvironmentSetup
	{
		private static string strToUse = "B4A45E16-2828-4157-908E-F2266FA1810A";
		private static string sToUse = "O72SajdzgdE=";

        private string webConfigLocation = "";
        private String ConfigFileName { get; set; } = "TrioClientConfig.xml";
		private SQLConfigInfo WebServerInfo { get; set; } = new SQLConfigInfo();

		public string WebConfigLocation
		{
			get => webConfigLocation;
			set => webConfigLocation = value;
		}

		public (bool success, string issueDescription) VerifyConfigFileLocation()
		{
			if (String.IsNullOrEmpty(WebConfigLocation))
			{
				return (false, "No path specified");
			}
			if (!System.IO.Directory.Exists(WebConfigLocation))
			{
				return (false, "Invalid path");
			}
			if (!System.IO.File.Exists(WebConfigLocation + "\\" + ConfigFileName))
			{
				return (false, "File doesn't exist");
			}

			return (true, "");
		}

        public cGlobalSettings LoadGlobalSettings(string dataEnvironment, string clientName = "")
        {
            var result = new cGlobalSettings();

			try
            {
				result.EnvironmentGroup = "Live";

	            var doc = XDocument.Load(WebConfigLocation + "\\" + ConfigFileName);
	            var root = doc.Element("Setup");
	            var clients = root.Element("Clients");

	            if (clients != null)
	            {
		            var temp = "";
		            var client = clients.Elements().FirstOrDefault(x => x.Attribute("Name").Value.ToLower() == clientName.ToLower());

		            if (client != null)
		            {
			            var serv = client.Element("Server");
			            result.DataSource = serv.Element("DataSource").Value;
			            temp = serv.Element("UserName").Value;
			            result.UserName = DecryptByPassword(temp, strToUse, sToUse);
			            temp = serv.Element("Password").Value;
			            result.Password = DecryptByPassword(temp, strToUse, sToUse);
			            if (client.Elements("Envs").Any())
			            {
				            var envs = client.Element("Envs");
				            var env = envs.Elements()
					            .FirstOrDefault(x => x.Element("Name").Value.ToLower() == dataEnvironment.ToLower());

				            if (env != null)
				            {
					            result.DataEnvironment = env.Element("Name").Value;
					            result.GlobalDataDirectory = env.Element("DataDirectory").Value;
				            }
				            else
				            {
								result.DataEnvironment = "";
								result.GlobalDataDirectory = "";
							}
			            }
			            else
			            {
				            Console.WriteLine("No Envs Found for Client");
							return null;
			            }
		            }
		            else
		            {
			            Console.WriteLine("Client " + clientName + " Not Found");
						return null;
		            }
	            }
	            else
	            {
		            Console.WriteLine("Clients Collection Not Set Up");
					return null;
	            }
	            

                return result;
            }
            catch (Exception ex)
            {
	            Console.WriteLine("Error Loading Global Settings:  " + ex);
				return null;
            }
        }

        private string DecryptByPassword(string strToDecrypt, string strPassword, string strSalt)
        {
	        return DecryptByPassword(strToDecrypt, strPassword, strSalt, "", 2, KeySize.Bits256);
        }

		private string Decrypt(byte[] ToDecryptBytes, byte[] keybytes, byte[] InitialVectorBytes)
		{
			string strReturn = null;
			if (ToDecryptBytes == null)
			{
				return strReturn;
			}
			if (ToDecryptBytes.LongLength < 1)
			{
				return strReturn;
			}
			try
			{
				using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
				{
					aesAlg.Key = keybytes;
					aesAlg.IV = InitialVectorBytes;
					ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
					using (MemoryStream msDecrypt = new MemoryStream(ToDecryptBytes))
					{
						using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
						{
							using (StreamReader srDecrypt = new StreamReader(csDecrypt))
							{
								strReturn = srDecrypt.ReadToEnd();
							}
						}
					}
				}
				return strReturn;
			}
			catch (Exception ex)
			{
				return "";
			}
		}

		private string DecryptByPassword(string strToDecrypt, string strPassword, string strSalt, string strInitialVector, int intPasswordIterations, KeySize intKeySize)
		{
			string strReturn = null;
			if (string.IsNullOrEmpty(strToDecrypt))
			{
				return strReturn;
			}
			try
			{
				byte[] InitialVectorBytes;
				byte[] SaltBytes = Convert.FromBase64String(strSalt);
				Rfc2898DeriveBytes DerivedPassword = new Rfc2898DeriveBytes(strPassword, SaltBytes, intPasswordIterations);
				byte[] ToDecryptBytes;
				ToDecryptBytes = Convert.FromBase64String(strToDecrypt);
				
				byte[] KeyBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, intKeySize);
				if (strInitialVector != "")
				{
					InitialVectorBytes = Convert.FromBase64String(strInitialVector);
				}
				else
				{
					InitialVectorBytes = KeyFromPassword(strPassword, strSalt, intPasswordIterations, KeySize.Bits128);
					// initial vector must be 128 bits
				}
				return Decrypt(ToDecryptBytes, KeyBytes, InitialVectorBytes);
			}
			catch (Exception ex)
			{
				return "";
			}
		}

		private byte[] KeyFromPassword(string strPassword, string strSalt, int intPasswordIterations, KeySize intKeySize)
		{
			if (string.IsNullOrEmpty(strPassword))
			{
				return null;
			}
			byte[] SaltBytes = Convert.FromBase64String(strSalt);
			Rfc2898DeriveBytes DerivedPassword = new Rfc2898DeriveBytes(strPassword, SaltBytes, intPasswordIterations);
			int intSize = 256;
			switch (intKeySize)
			{
				case KeySize.Bits128:
					intSize = 128;
					break;
				case KeySize.Bits192:
					intSize = 192;
					break;
				case KeySize.Bits256:
					intSize = 256;
					break;
				default:
					intSize = 256;
					break;
			}
			byte[] KeyBytes = DerivedPassword.GetBytes(intSize / 8);
			return KeyBytes;
		}

		private enum KeySize
		{
			Bits128,
			Bits192,
			Bits256
		}
	}
}
