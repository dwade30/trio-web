﻿namespace SharedApplication.AccountGroups
{
    public enum AccountBillingType
    {
        RealEstate = 0,
        PersonalProperty = 1,
        Utility = 2
    }

    public static class AccountBillingTypeExtensions
    {
        public static int ToInteger(this AccountBillingType accountType)
        {
            return (int) accountType;
        }

        public static string ToAbbreviation(this AccountBillingType accountType)
        {
            switch (accountType)
            {
                case AccountBillingType.Utility:
                    return "UT";
                    break;
                case AccountBillingType.PersonalProperty:
                    return "PP";
                    break;
                case AccountBillingType.RealEstate:
                    return "RE";
                    break;
            }

            return "";
        }

    }

    
}