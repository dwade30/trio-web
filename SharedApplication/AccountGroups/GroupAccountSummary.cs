﻿namespace SharedApplication.AccountGroups
{
    public class GroupAccountSummary
    {
        public int Account { get; set; } = 0;
        public AccountBillingType AccountType { get; set; } = AccountBillingType.RealEstate;

        public decimal Balance { get; set; } = 0;
    }
}