﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections;

namespace SharedApplication.AccountGroups
{
    public class AccountGroup
    {
        public int GroupNumber { get; } = 0;
        private List<GroupAccountSummary> accountSummaries = new List<GroupAccountSummary>();

        public decimal Balance
        {
            get => accountSummaries.Sum(s => s.Balance);
        }

        public IEnumerable<GroupAccountSummary> AccountSummaries
        {
            get => accountSummaries;
        }

        public AccountGroup(int groupNumber, IEnumerable<GroupAccountSummary> accountSummaries)
        {
            GroupNumber = groupNumber;
            this.accountSummaries.AddRange(accountSummaries);
        }
    }
}