﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.AccountGroups.Commands
{
    public class CalculateAccountGroup : Command<AccountGroup>
    {
        public int GroupNumber { get; }
        public DateTime EffectiveDate { get; }

        public CalculateAccountGroup(int groupNumber,DateTime effectiveDate)
        {
            GroupNumber = groupNumber;
            EffectiveDate = effectiveDate;
        }
    }
}