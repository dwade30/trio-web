﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
	public class UserInformation
	{
		public string UserId { get; set; }
		public int Id { get; set; }
		public bool AllowSiteUpdate { get; set; } = false;
	}
}
