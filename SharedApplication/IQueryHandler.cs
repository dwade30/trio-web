﻿namespace SharedApplication
{
    public interface IQueryHandler<TQueryType,TReturnType>
    {
        TReturnType ExecuteQuery(TQueryType query);
    }
}