﻿using SharedApplication.Messaging;

namespace SharedApplication
{
    public class ShowInterestedParties : Command
    {
        public string Module { get; set; } = "";
        public bool AllowEdit { get; set; } = false;
        public int Account { get; set; } = 0;
        public int AssociatedId { get; set; } = 0;
        public int IdType { get; set; } = 0;

        public ShowInterestedParties(int account, string module, int associatedId, bool allowEdit, int idType)
        {
            Account = account;
            Module = module;
            AssociatedId = associatedId;
            AllowEdit = allowEdit;
            IdType = idType;
        }
    }
}