﻿namespace SharedApplication
{
    public class DataEnvironmentSettings
    {
        public bool IsMultiTown { get; set; } = false;
        public string ClientName { get; set; } = "";
		public string CityTown { get; set; } = "";

		public string CityTownClientName
		{
			get
			{
				if (CityTown.Trim() == "")
				{
					return ClientName;
				}
				else
				{
					return CityTown + " of " + ClientName;
				}
			}
		}
    }
}