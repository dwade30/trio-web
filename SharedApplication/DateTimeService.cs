﻿using System;

namespace SharedApplication
{
    /// <summary>
    /// This is for testing purposes, so that tests can always produce the same results for functions using the current date and time
    /// </summary>
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now()
        {
            return DateTime.Now;
        }

        public DateTime Today()
        {
            return DateTime.Today;
        }
    }
}