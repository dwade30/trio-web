﻿namespace SharedApplication
{
    public interface IModalView<TViewModelType> : IView<TViewModelType>
    {
        void ShowModal();
    }
}