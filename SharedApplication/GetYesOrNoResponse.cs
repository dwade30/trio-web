﻿using SharedApplication.Messaging;

namespace SharedApplication
{
    public class GetYesOrNoResponse: Command<bool>
    {
        public string Title { get; }
        public string Message { get; }
        public bool ShowWarningIcon { get; }
        public GetYesOrNoResponse( string message,string title, bool showWarningIcon)
        {
            Title = title;
            Message = message;
            ShowWarningIcon = showWarningIcon;
        }
    }
}