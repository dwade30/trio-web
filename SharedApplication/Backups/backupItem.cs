﻿using System;

namespace SharedApplication.Backups
{
    public class backupItem
    {
        public string FileName { get; set; }
        public DateTime BackupDate { get; set; }
        public long Size { get; set; }
    }
}
