﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
    public class OrderedDescriptionIDPair
    {
        public string Description { get; set; } = "";
        public int ID { get; set; } = 0;
        public int OrderNumber { get; set; } = 0;
    }
}
