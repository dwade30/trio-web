﻿using System;
using System.IO;
using System.IO.Abstractions;

namespace SharedApplication
{
    /// <summary>
    /// Original properties/methods are copied from WiseJ IFileSystemProvider
    /// Some methods are commented out if they aren't needed.  This will make it easier
    /// to create non-wisej implementations if unneeded methods aren't required
    /// </summary>
    public interface ITrioFileSystem 
    {
        string[] GetDirectories(string path, string pattern, SearchOption searchOption = SearchOption.TopDirectoryOnly);

        string[] GetFiles(string path, string pattern, SearchOption searchOption = SearchOption.TopDirectoryOnly);

        //FileAttributes GetAttributes(string path);

        void CreateDirectory(string path);

        void DeleteDirectory(string path, bool recursive);

       // DateTime GetCreationTime(string path);

       // DateTime GetLastWriteTime(string path);

        bool Exists(string path);

        long GetFileSize(string path);

        void DeleteFile(string path);

       // void RenameFile(string path, string newName);

        //void RenameDirectory(string path, string newName);

        bool Contains(string path);

        //string MapPath(string path);

        Stream OpenFileStream(string path, FileMode mode, FileAccess access);

        string Root { get; set; }
        string Name { get; set; }
    }
}