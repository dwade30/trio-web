﻿namespace SharedApplication
{
    public interface ISelectableItem
    {
        bool IsSelected { get; set; }

    }
}