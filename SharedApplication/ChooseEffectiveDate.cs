﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication
{
    public class ChooseEffectiveDate : Command<EffectiveDateChoice>
    {
        public DateTime DefaultDate { get; set; }
        public bool AllowChangingRecordedDate { get; set; }
        public bool UpdateRecordedDate { get; set; }

        public ChooseEffectiveDate(DateTime defaultDate, bool allowChangingRecordedDate, bool updateRecordedDate)
        {
            DefaultDate = defaultDate;
            AllowChangingRecordedDate = allowChangingRecordedDate;
            UpdateRecordedDate = updateRecordedDate;
        }
    }
}