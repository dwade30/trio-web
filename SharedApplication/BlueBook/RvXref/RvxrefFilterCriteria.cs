﻿namespace SharedApplication.BlueBook.RvXref
{
    public class RvxrefFilterCriteria
    {
        public string Model { get; }

        public RvxrefFilterCriteria(string model)
        {
            Model = model;
        }
    }
}