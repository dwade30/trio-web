﻿namespace SharedApplication.BlueBook
{
    public class TrailerDataFilterCriteria
    {
        public int Year { get; }
        public string Manufacturer { get; }
        public string Model { get; }
        public bool UseExactMatch { get; }

        public TrailerDataFilterCriteria(int year, string manufacturer, bool useExactMatch, string model)
        {
            Year = year;
            Manufacturer = manufacturer;
            useExactMatch = UseExactMatch;
            Model = model;
        }
    }
}