﻿namespace SharedApplication.BlueBook
{
    public enum VehicleClassification
    {
        None = 0,
        CommercialTrucks = 2,
        PassengerVehicles = 4,
        Powersport = 5,
        RecreationalVehicles = 6,
        CommercialTrailers = 7,
        TruckBodies = 8
    }
}