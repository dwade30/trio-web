﻿using SharedApplication.Messaging;

namespace SharedApplication.BlueBook
{
    public class GetBlueBookRepositoryHandler : CommandHandler<GetBlueBookRepository,IBlueBookRepository>
    {
        private IBlueBookRepository blueBookRepository;
        public GetBlueBookRepositoryHandler(IBlueBookRepository blueBookRepository)
        {
            this.blueBookRepository = blueBookRepository;
        }
        protected override IBlueBookRepository Handle(GetBlueBookRepository command)
        {
            return blueBookRepository;
        }
    }
}