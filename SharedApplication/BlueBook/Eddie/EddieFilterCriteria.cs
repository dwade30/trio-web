﻿namespace SharedApplication.BlueBook
{
    public class EddieFilterCriteria
    {
        public int Year { get; }
        public string Make { get; }
        public string Description { get; }

        public EddieFilterCriteria(int year, string make, string description)
        {
            Year = year;
            Make = make;
            Description = description;
        }
    }
}