﻿using System.Collections.Generic;
using System.Linq;
using BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.Extensions;

namespace SharedApplication.BlueBook
{
    public static class VehicleModelExtensions
    {
        //public static VehicleClassificationNumber ToClassificationNumber(VehicleConfigurationSpecs vSpec)
        //{
        //    switch (vSpec.ClassificationId)
        //    {
        //        case VehicleClassificationNumber.PassengerVehicles:
        //            return VehicleClassificationNumber.PassengerVehicles;
        //            break;

        //    }
        //}
        public static CycleMakeList ToCycleMakeList(this VehicleManufacturer manufacturer)
        {
            return new CycleMakeList()
            {
                Id = manufacturer.Id,
                ManufacturerLong = manufacturer.Name,
                ManufacturerShort = manufacturer.Name
            };
        }

        public static IEnumerable<CycleMakeList> ToCycleMakeLists(this IEnumerable<VehicleManufacturer> manufacturers)
        {
            return manufacturers.Select(m => m.ToCycleMakeList());
        }

        public static HeavyTruckMakeList ToHeavyTruckMakeList(this VehicleManufacturer manufacturer)
        {
            return new HeavyTruckMakeList()
            {
                Id = manufacturer.Id,
                ManufacturerLong = manufacturer.Name,
                ManufacturerShort = manufacturer.Name
            };
        }

        public static IEnumerable<HeavyTruckMakeList> ToHeavyTruckMakeLists(
            this IEnumerable<VehicleManufacturer> manufacturers)
        {
            return manufacturers.Select(m => m.ToHeavyTruckMakeList());
        }

        public static RvmakeList ToRvMakeList(this VehicleManufacturer manufacturer)
        {
            return new RvmakeList()
            {
                Id = manufacturer.Id,
                ManufacturerLong = manufacturer.Name,
                ManufacturerShort = manufacturer.Name
            };
        }

        public static IEnumerable<RvmakeList> ToRvMakeLists(this IEnumerable<VehicleManufacturer> manufacturers)
        {
            return manufacturers.Select(m => m.ToRvMakeList());
        }

        public static Rv ToRv(this VehicleConfigurationSpecs specs)
        {
            return new Rv()
            {
                Description = specs.GetSpecByName("legacyManufacturer"),
                Id = specs.ConfigurationId,
                Model = specs.ModelName,
                Manufacturer = specs.ManufacturerName,
                Year = specs.ModelYear.ToString(),
                Weight = specs.GetSpecByName("weight"),
                Feet = specs.GetSpecByName("feet").ToIntegerValue(),
                Inches = specs.GetSpecByName("inches"),
                Srp = specs.GetSpecByName("msrp"),

            };
        }

        public static TruckBodyData ToTruckBodyData(this VehicleConfigurationSpecs specs)
        {
            return new TruckBodyData()
            {
                Id = specs.ConfigurationId,
                Description = specs.Description,
                Model = specs.ModelName,
                Manufacturer = specs.ManufacturerName,
                Year = specs.ModelYear,
                Type = specs.SizeClassName
            };
        }

        public static TrailerData ToTrailerData(this VehicleConfigurationSpecs specs)
        {
            return new TrailerData()
            {
                Id = specs.ConfigurationId,
                Year = specs.ModelYear,
                Manufacturer = specs.ManufacturerName,
                List = specs.GetSpecByName("msrp"),
                Vin = specs.GetSpecByName("vin"),//specs.GetSpecByName("vin"),
                Length = specs.GetSpecByName("length"),
                Width = specs.GetSpecByName("width"),
                Type = specs.GetSpecByName("vehicleType"),
                Section = specs.SubTypeName
            };
        }

        public static HeavyTruck ToHeavyTruck(this VehicleConfigurationSpecs configurationSpecs)
        {
            var specs = configurationSpecs.Specs.ToDictionary(s => s.Name);
            return new HeavyTruck()
            {
                Manufacturer = configurationSpecs.ManufacturerName,
                Year = configurationSpecs.ModelYear.ToString(),
                ModelNumber = configurationSpecs.ModelName,
                Description = specs.TryGetValue("description", out var description) ? description?.Value : "",
                Id = configurationSpecs.ConfigurationId,
                TransmissionDescription = specs.TryGetValue("transmission", out var transmission) ? transmission?.Value : "",
                Make = configurationSpecs.ManufacturerName,
                EngineDescription = specs.TryGetValue("engineModel", out var engine) ? engine?.Value : "",
                GrossVehicleWeight = specs.TryGetValue("gvw", out var gvw) ? gvw?.Value : "",
                Wheelbase = specs.TryGetValue("wheelbase", out var wheelbase) ? wheelbase?.Value : "",
                NetWeight = specs.TryGetValue("netWeight", out var netweight) ? netweight?.Value : "",
                EngineMake = specs.TryGetValue("engineMake", out var engineMake) ? engineMake?.Value : "",
                TransmissionMake = specs.TryGetValue("transmissionMake", out var transmissionMake) ? transmissionMake?.Value : "",
                Vin = specs.TryGetValue("vin", out var vinModel) ? vinModel?.Value : "",
                Axle1Description = specs.TryGetValue("axle1Model", out var axle1Model) ? axle1Model?.Value : "",
                Axle1Make = specs.TryGetValue("axle1Manufacturer", out var axle1Manufacturer) ? axle1Manufacturer?.Value : "",
                Axle2Description = specs.TryGetValue("axle2Model", out var axle2Model) ? axle2Model?.Value : "",
                Axle2Make = specs.TryGetValue("axle2Manufacturer", out var axle2Manufacturer) ? axle2Manufacturer?.Value : "",
                FactoryPrice = specs.TryGetValue("msrp", out var msrp) ? msrp?.Value : ""
            };
        }

        public static Cycle ToCycle(this VehicleConfigurationSpecs configurationSpecs)
        {
            return new Cycle()
            {
                Manufacturer = configurationSpecs.ManufacturerName,
                Year = configurationSpecs.ModelYear.ToString(),
                ModelNumber = configurationSpecs.GetSpecByName("vinModelNumber"),
                Description = configurationSpecs.ModelName, //configurationSpecs.Specs.FirstOrDefault(s => s.Name == "description")?.Value ??"",
                Cylinders = configurationSpecs.GetSpecByName("cylinders"),
                Id = configurationSpecs.ConfigurationId,
                Displacement = configurationSpecs.GetSpecByName("displacement"),
                Msrp = configurationSpecs.GetSpecByName("msrp"),
                Weight = configurationSpecs.GetSpecByName("weight"),
                Color = configurationSpecs.GetSpecByName("modelColor")
            };
        }

        public static Rbcar ToRbcar(this VehicleConfigurationSpecs configurationSpecs)
        {
            return new Rbcar()
            {
                Description = configurationSpecs.Description,
                Year = configurationSpecs.ModelYear.ToString(),
                Model = configurationSpecs.ModelName,
                Id = configurationSpecs.ConfigurationId,
                NumberOfPassengers = configurationSpecs.GetSpecByName("passengers"),
                AirCondition = configurationSpecs.GetSpecByName("air"),
                BrakeType = configurationSpecs.GetSpecByName("brakeType"),
                SuggestedRetailValue = configurationSpecs.GetSpecByName("msrp"),
                Engine = configurationSpecs.GetSpecByName("engineSize"),
                Transmission = configurationSpecs.GetSpecByName("transmissionType"),
                Manufacturer = configurationSpecs.ManufacturerName,
                GrossVehicleWeight = configurationSpecs.GetSpecByName("gvw"),
                EquipmentPackage = configurationSpecs.GetSpecByName("equipmentCode"),
                WheelBase = configurationSpecs.GetSpecByName("wheelbase"),
                ModelNumber = configurationSpecs.GetSpecByName("vinModelNumber")
            };
        }
    }
}

