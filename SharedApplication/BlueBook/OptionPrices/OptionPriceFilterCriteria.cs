﻿namespace SharedApplication.BlueBook.OptionPrices
{
    public class OptionPriceFilterCriteria
    {
        public string VehicleType { get; }

        public OptionPriceFilterCriteria(string vehicleType)
        {
            VehicleType = vehicleType;
        }
    }
}