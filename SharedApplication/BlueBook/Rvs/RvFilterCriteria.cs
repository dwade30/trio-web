﻿namespace SharedApplication.BlueBook.Rvs
{
    public class RvFilterCriteria
    {
        public int Year { get; }
        public string Manufacturer { get; }
        public string ModelNumber { get; }
        public int MinSize { get; }
        public int MaxSize { get; }
        public RvFilterCriteria(int year, string manufacturer, string modelNumber, int minSize, int maxSize )
        {
            Year = year;
            Manufacturer = manufacturer;
            ModelNumber = modelNumber;
            MinSize = minSize;
            MaxSize = maxSize;
        }
    }
}