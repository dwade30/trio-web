﻿using System;
using System.Collections.Generic;
using SharedApplication.BlueBook.Models;

namespace SharedApplication.BlueBook
{
    public interface IMultipleVehicleViewModel
    {
        bool IsLoadingDone { get; }
        event EventHandler<VehicleConfigurationsReturnedArgs> VehicleConfigurationsReturned;
        IEnumerable<VehicleConfigurationSpecs> ConfigurationSpecs { get; }
        void SetConfigurations(IEnumerable<VehicleConfiguration> configurations);
        void LoadNext();
        void SetPageSize(int pageSize);
        int PageSize { get; }
        int Offset { get; }
    }

    public class VehicleConfigurationsReturnedArgs
    {
        public bool IsEndOfData { get; }
        public IEnumerable<VehicleConfigurationSpecs> ConfigurationSpecs { get; }

        public VehicleConfigurationsReturnedArgs(bool isEndOfData, IEnumerable<VehicleConfigurationSpecs> specs)
        {
            IsEndOfData = isEndOfData;
            ConfigurationSpecs = specs;
            
        }
    }


}