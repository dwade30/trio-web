﻿using System.Collections.Generic;
using System.Linq;
using BlueBook;
using SharedApplication.BlueBook.Models;

namespace SharedApplication.BlueBook
{
    public interface IBlueBookContext
    {
        IQueryable<CarMakeList> CarMakeList { get;  }
        IQueryable<Customize> Customize { get;  }
        IQueryable<CycleMakeList> CycleMakeList { get;  }
        IQueryable<Cycle> Cycles { get;  }
        IQueryable<Dbversion> Dbversion { get;  }
        IQueryable<Eddie> Eddies { get;  }
        IQueryable<HeavyTruckMakeList> HeavyTruckMakeList { get;  }
        IQueryable<HeavyTruck> HeavyTrucks { get;  }
        IQueryable<LightTruckMakeList> LightTruckMakeList { get;  }
        IQueryable<Lv> Lv { get;  }
        IQueryable<Mfg> Mfg { get;  }
        IQueryable<OptionPrice> OptionPrices { get;  }
        IQueryable<OptionPricesBackup> OptionPricesBackup { get;  }
        IQueryable<Option> Options { get;  }
        IQueryable<Packages> Packages { get;  }
        IQueryable<Rbcar> Rbcars { get;  }
        IQueryable<Rv> Rvs { get;  }
        IQueryable<RvmakeList> RvmakeList { get;  }
        IQueryable<Rvxref> Rvxref { get;  }
        IQueryable<StandardOption> StandardOptions { get;  }
        IQueryable<TrailerData> TrailerDatas { get;  }
        IQueryable<TrailerMakeList> TrailerMakeList { get;  }
        IQueryable<TruckBodyData> TruckBodyData { get;  }
        IQueryable<TruckBodyOption> TruckBodyOptions { get;  }
        IQueryable<TruckMfg> TruckMfg { get;  }
    }
}