﻿namespace SharedApplication.BlueBook.HeavyTrucks
{
    public class HeavyTruckFilterCriteria
    {
        public int Year { get; }
        public string Manufacturer { get; }
        public string ModelNumber { get; }
        public bool UseExactMatch { get; }
        public HeavyTruckFilterCriteria(int year, string manufacturer, string modelNumber,bool useExactMatch)
        {
            Year = year;
            Manufacturer = manufacturer;
            ModelNumber = modelNumber;
            useExactMatch = UseExactMatch;
        }
    }
}