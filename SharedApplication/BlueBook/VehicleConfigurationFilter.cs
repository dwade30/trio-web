﻿namespace SharedApplication.BlueBook
{
    public class VehicleConfigurationFilter
    {
        public int ModelId { get; } = 0;
        public int Year { get; } = 0;
        public string ModelName { get; set; } = "";
        public string ManufacturerName { get; set; } = "";
        public int CategoryId { get; set; } = 0;
        public int ClassificationId { get; set; } = 0;
        public VehicleConfigurationFilter(int modelId, int year,string modelName,string manufacturerName,int categoryId, int classificationId)
        {
            ModelId = modelId;
            this.Year = year;
            ModelName = modelName;
            ManufacturerName = manufacturerName;
            CategoryId = categoryId;
            ClassificationId = classificationId;
        }
    }
}