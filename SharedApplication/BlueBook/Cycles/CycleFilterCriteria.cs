﻿namespace SharedApplication.BlueBook.Services
{
    public class CycleFilterCriteria
    {
        public int Year { get; }
        public string Manufacturer { get; }
        public string ModelNumber { get; }
        public bool UseExactMatch { get; }
        public CycleFilterCriteria(int year,string manufacturer,string modelNumber,bool useExactMatch)
        {
            Year = year;
            Manufacturer = manufacturer;
            ModelNumber = modelNumber;
            UseExactMatch = useExactMatch;
        }
    }
}