﻿namespace SharedApplication.BlueBook
{
    public enum VehicleClassificationNumber
    {
        None = 0,
        CommercialTrucks = 2,
        PassengerVehicles = 4,
        Powersport = 5,
        RecreationalVehicles = 6,
        CommercialTrailers = 7,
        TruckBodies = 8
    }

    public enum VehicleCategoryNumber
    {
        None = 0,
        Automobiles = 21,
        StationWagons = 22,
        SUVs = 23,
        Minivans = 24,
        LightTrucks = 25, // light trucks
        MotorCycles = 29,
        CommericalLightTrucks = 72,
        MediumTrucks=73,
        HeavyTrucks = 74
    }

    public static class VehicleClassificationNumberExtensions
    {
        public static int ToInteger(this VehicleClassificationNumber classificationNumber)
        {
            return (int) classificationNumber;
        }

        public static int ToInteger(this VehicleCategoryNumber categoryNumber)
        {
            return (int) categoryNumber;
        }
    }


}