﻿using SharedApplication.BlueBook.Models;

namespace SharedApplication.BlueBook
{
    public static class BlueBookExtensions
    {
        public static string GetValue(this VehicleSpec spec)
        {
            var value = spec?.Value ?? "";
                return value != "null" ? value : "";
        }
    }
}