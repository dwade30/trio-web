﻿using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;

namespace SharedApplication.BlueBook
{
    public abstract class BlueBookFilterCriteria
    {
        public abstract IEnumerable<(string Name, string value)> ToParameters();
    }
}