﻿namespace SharedApplication.BlueBook
{
    public class ConfigurationVinIdPair
    {
        public string Vin { get;  }
        public int ConfigurationId { get; }

        public ConfigurationVinIdPair(string vin, int configurationId)
        {
            Vin = vin;
            ConfigurationId = configurationId;
        }
    }
}