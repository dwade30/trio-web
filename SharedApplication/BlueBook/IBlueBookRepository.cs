﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook.HeavyTrucks;
using SharedApplication.BlueBook.Manufacturer;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.OptionPrices;
using SharedApplication.BlueBook.Rbcars;
using SharedApplication.BlueBook.Rvs;
using SharedApplication.BlueBook.RvXref;
using SharedApplication.BlueBook.Services;
using SharedApplication.BlueBook.TruckBodies;
using SharedApplication.TaxCollections.Commands;

namespace SharedApplication.BlueBook
{
    public interface IBlueBookRepository
    {
        ICycleService Cycles { get; }
        IHeavyTruckService HeavyTrucks { get; }
        IManufacturerService Manufacturers { get; }
        IRbCarService RbCars { get; }
        ITrailerDataService TrailerData { get; }
        IRvService Rvs { get; }

        IHeavyTruckMakeService HeavyTruckMakes { get; }
        IRvMakeListService RvMakes { get; }
        ICycleMakeListService CycleMakes { get; }
        IOptionsService Options { get; }
        IOptionPricesService OptionPrices { get; }
        IRVXrefService RvXrefs { get; }
        ITruckBodyOptionService TruckBodyOptions { get; }
        IEddieService Eddies { get; }
        IMakeService Makes { get; }
        ITruckBodyDataService TruckBodyDatas { get; }
        IVehicleConfigurationsService VehicleConfigurations { get; }
        IVinService VehicleInformation { get; }
        IVehicleSpecService VehicleSpecs { get; }
        
    }

    public interface IOptionsService
    {
        VehicleConfigurationOptions GetOptions(OptionFilterCriteria filterCriteria);
    }
    public interface IVinService
    {
        IEnumerable<VehicleInformation> GetVehicleInformation(string vin);
    }

    public interface ICycleService
    {
        Cycle GetCycle(int id);
        IEnumerable<Cycle> GetCycles(CycleFilterCriteria filterCriteria);
    }

    public interface IHeavyTruckService
    {
        HeavyTruck GetHeavyTruck(int id);
        IEnumerable<HeavyTruck> GetHeavyTrucks(HeavyTruckFilterCriteria filterCriteria);
    }

    public interface IRbCarService
    {
        Rbcar GetRbCar(int id);
        IEnumerable<Rbcar> GetRbCars(RbCarFilterCriteria filterCriteria);
    }

    public interface IManufacturerService
    {
        IEnumerable<Mfg> GetManufacturers(ManufacturerFilterCriteria filterCriteria);
    }

    public interface ITrailerDataService
    {
       TrailerData GetTrailerData(int id);
        IEnumerable<TrailerData> GetTrailerDatas(TrailerDataFilterCriteria filterCriteria);
        IEnumerable<VehicleManufacturer> GetTrailerManufacturersForYear(int year);
    }

    public interface IRvService
    {
        Rv GetRv(int id);
        IEnumerable<Rv> GetRvs(RvFilterCriteria filterCriteria);
    }

    public interface IRvMakeListService
    {
        IEnumerable<RvmakeList> GetRvMakes();
    }

    public interface ICycleMakeListService
    {
        IEnumerable<CycleMakeList> GetCycleMakes();
    }

    public interface IHeavyTruckMakeService
    {
        IEnumerable<HeavyTruckMakeList> GetHeavyTruckMakes();
    }

    public interface IOptionPricesService
    {
        IEnumerable<OptionPrice> GetOptionPrices(OptionPriceFilterCriteria filterCriteria);
    }

    public interface IRVXrefService
    {
        IEnumerable<Rvxref> GetRvxrefs(RvxrefFilterCriteria filterCriteria);
    }

    public interface ITruckBodyOptionService
    {
        IEnumerable<TruckBodyOption> GetTruckBodyOptions(TruckBodyOptionFilterCriteria filterCriteria);
    }

    public interface IEddieService
    {
        IEnumerable<Eddie> GetEddies(EddieFilterCriteria filterCriteria);
    }

    public interface IMakeService
    {
        IEnumerable<string> GetCarManufacturers();
        IEnumerable<string> GetCarAndLightTruckManufacturers();
        IEnumerable<string> GetLightTruckManufacturers();
        IEnumerable<string> GetCarAndLightTruckManufacturerShortDescriptions(string manufacturer);
        IEnumerable<(string manufacturer, int year )> GetTrailerMakeForYear(int year);
    }

    public interface ITruckBodyDataService
    {
        TruckBodyData GetTruckBodyData(int id);
        IEnumerable<TruckBodyData> GetTruckBodyDatas(TruckBodyDataFilterCriteria filterCriteria);
        IEnumerable<(string manufacturer, int year)> GetTrailerMakeForYear(int year);
        IEnumerable<VehicleManufacturer> GetTruckBodyManufacturers(int year);
    }
}