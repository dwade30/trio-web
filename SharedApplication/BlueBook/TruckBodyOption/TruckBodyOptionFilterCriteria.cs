﻿namespace SharedApplication.BlueBook
{
    public class TruckBodyOptionFilterCriteria
    {
        public int Year { get; }
        public string Manufacturer { get; }

        public TruckBodyOptionFilterCriteria(int year, string manufacturer)
        {
            Year = year;
            Manufacturer = manufacturer;
        }
    }
}