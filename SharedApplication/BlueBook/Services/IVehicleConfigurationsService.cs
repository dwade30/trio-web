﻿using System.Collections.Generic;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;

namespace SharedApplication.BlueBook.Services
{
    public interface IVehicleConfigurationsService
    {
        IEnumerable<VehicleConfiguration> GetConfigurations(VehicleConfigurationFilter filter);
    }
}