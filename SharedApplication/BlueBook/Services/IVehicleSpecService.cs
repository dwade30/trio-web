﻿using System.Collections.Generic;
using SharedApplication.BlueBook.Models;

namespace SharedApplication.BlueBook.Services
{
    public interface IVehicleSpecService
    {
        VehicleConfigurationSpecs GetVehicleSpecs(int configurationId);
        IEnumerable<VehicleConfigurationSpecs> GetVehicleListSpecs(IEnumerable<int> configurations);
        //IEnumerable<VehicleConfigurationSpecs> GetVehicleListSpecs(IEnumerable<ConfigurationVinIdPair> configurations);
    }
}