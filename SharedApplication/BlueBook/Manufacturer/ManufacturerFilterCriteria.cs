﻿using System.Collections;
using System.Collections.Generic;

namespace SharedApplication.BlueBook.Manufacturer
{
    public class ManufacturerFilterCriteria 
    {
        public string VCode { get; }
        public IEnumerable<string> VehicleTypes { get; }
        public int Year { get; }

        public ManufacturerFilterCriteria(string vCode, IEnumerable<string> vehicleTypes, int year)
        {
            VCode = vCode;
            VehicleTypes = vehicleTypes;
            Year = year;
        }
    }
}