﻿using System.Collections.Generic;
using SharedApplication.BlueBook.Models;
using SharedApplication.Messaging;

namespace SharedApplication.BlueBook
{
    public class ShowMultipleVehicles: Command
    {
        public IEnumerable<VehicleConfiguration> VehicleConfigurations { get; }

        public ShowMultipleVehicles(IEnumerable<VehicleConfiguration> configurations)
        {
            VehicleConfigurations = configurations;
        }
        
    }
}