﻿using System.Globalization;

namespace SharedApplication.BlueBook
{
    public class VehicleOption
    {
        public int FamilyId { get; private set; } = 0;
        public string FamilyName { get; private set; } = "";
        public string Name { get; private set; } = "";
        public string Value { get; private set; } = "";
        public decimal MSRP { get; private set; }
        public bool IsStandard { get; private set; }

        public VehicleOption(int familyId, string familyName, string name, string value, decimal msrp)
        {
            FamilyId = familyId;
            FamilyName = familyName;
            Name = name;
            Value = value;
            MSRP = msrp;
            if (familyName.StartsWith("standard options", true, null))
            {
                IsStandard = true;
            }
            else
            {
                IsStandard = false;
            }
        }
    }
}