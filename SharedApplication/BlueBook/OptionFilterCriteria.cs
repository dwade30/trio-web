﻿namespace SharedApplication.BlueBook
{
    public class OptionFilterCriteria
    {
        public int SizeClassId { get; private set; }
        public int Year { get; private set; }

        public OptionFilterCriteria(int sizeClassId, int year)
        {
            SizeClassId = sizeClassId;
            Year = year;
        }
    }
}