﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.BlueBook.Models
{
    public class VehicleManufacturer
    {
        public int Id { get; } = 0;
        public string Name { get;  } = "";
        public IEnumerable<string> Aliases { get;  } 

        public VehicleManufacturer(int id, string name, IEnumerable<string> aliases)
        {
            Id = id;
            Name = name;
            Aliases = aliases.ToList();
        }
    }
}