﻿using System.Collections.Generic;

namespace SharedApplication.BlueBook.Models
{
    public class VehicleConfigurationOptions
    {
        public VehicleClassificationNumber ClassificationId { get; set; } = VehicleClassificationNumber.None;
        public string ClassificationName { get; set; } = "";
        public int CategoryId { get; set; } = 0;
        public int SubtypeId { get; set; } = 0;
        public string SubtypeName { get; set; } = "";
        public int SizeClassId { get; set; } = 0;
        public string SizeClassUom { get; set; } = "";
        public int Year { get; set; } = 0;

        public List<VehicleOption> Options { get; set; } = new List<VehicleOption>();
    }
}