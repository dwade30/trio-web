﻿namespace SharedApplication.BlueBook.Models
{
    public partial class Cycle
    {
        public int Id { get; set; }
        public string Year { get; set; }
        public string Manufacturer { get; set; }
        public string ModelNumber { get; set; }
        public string Description { get; set; }
        public string Cylinders { get; set; }
        public string Displacement { get; set; }
        public string Weight { get; set; }
        public string Msrp { get; set; }
        public string Color { get; set; } = "";
    }
}