﻿namespace SharedApplication.BlueBook.Models
{
    public class VehicleConfiguration
    {
        public int ModelId { get; set; } = 0;
        public string ModelName { get; set; } = "";
        public int ManufacturerId { get; set; } = 0;
        public int ClassificationId { get; set; } = 0;
        public int CategoryId { get; set; } = 0;
        public string CategoryName { get; set; } = "";
        public int SubtypeId { get; set; } = 0;
        public string SubtypeName { get; set; } = "";
        public int SizeClassId { get; set; } = 0;
        public string SizeClassName { get; set; } = "";
        public int SizeClassMin { get; set; } = 0;
        public int SizeClassMax { get; set; } = 0;
        public string SizeClassUom { get; set; } = "";
        public int ConfigurationId { get; set; } = 0;
        public int ModelYear { get; set; } = 0;
        public string VinModelNumber { get; set; } = "";
    }
}