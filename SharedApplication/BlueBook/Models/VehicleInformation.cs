﻿namespace SharedApplication.BlueBook.Models
{
    public class VehicleInformation
    {
        public int ModelId { get; set; } = 0;
        public string ModelName { get; set; } = "";
        public int ConfigurationId { get; set; } = 0;
        public int CategoryId { get; set; } = 0;
        public string CategoryName { get; set; } = "";
        public int ManufacturerId { get; set; } = 0;
        public string ManufacturerName { get; set; } = "";
        public string[] ManufacturerAliases { get; set; } = new string[] { };
        public int ClassificationId { get; set; } = 0;
        public string ClassificationName { get; set; } = "";
        public string VinModelNumber { get; set; } = "";
        public int ModelYear { get; set; } = 0;
    }
}