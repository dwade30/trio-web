﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.BlueBook
{
    public partial class Rv
    {
        public int Id { get; set; }
        public string Year { get; set; }
        public string Manufacturer { get; set; }
        public string Notes { get; set; }
        public string Model { get; set; }
        public string Description { get; set; }
        public string Srp { get; set; }
        public string Weight { get; set; }
        public int? Feet { get; set; }
        public string Inches { get; set; }
        public string Reserved { get; set; }
    }
}