﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.BlueBook.Models
{
    public class VehicleConfigurationSpecs
    {
        public int ModelId { get; set; } = 0;
        public string ModelName { get; set; } = "";
        public int ConfigurationId { get; set; } = 0;
        public VehicleClassificationNumber ClassificationId { get; set; } = VehicleClassificationNumber.None;
        public int ModelYear { get; set; } = 0;
        public string ManufacturerName { get; set; } = "";
        public int SizeClassId { get; set; } = 0;
        public string SizeClassName { get; set; } = "";
        public string SubTypeName { get; set; } = "";
        public string Description
        {
            get
            {
                return GetSpecByName("description");
            }
        }

        private List<VehicleSpec> specs = new List<VehicleSpec>();
        private  Dictionary<string,VehicleSpec> specsDictionary = new Dictionary<string, VehicleSpec>();
        public List<VehicleSpec> Specs
        {
            get
            {
                return specs;
            }
            set 
            { 
                specs = value;
               // specsDictionary = specs.ToDictionary(s => s.Name);
            }
        }

        public string GetSpecByName(string specName)
        {
            return specs.FirstOrDefault(s => s.Name == specName)?.Value ?? "";
            if (specsDictionary.TryGetValue(specName, out var vehicleSpec))
            {
                return vehicleSpec.Value;
            }
            return "";
        }
    }

    public class VehicleSpec
    {
        public string Name { get; set; } = "";
        public string FriendlyName { get; set; } = "";
        public string Uom { get; set; } = "";
        public string Description { get; set; } = "";
        public string Family { get; set; } = "";
        public string Value { get; set; } = "";
    }
}