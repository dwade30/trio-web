﻿using System;

namespace SharedApplication
{
    public class BillPaymentUtility : IBillPaymentUtility
    {
        public decimal CalculateInterest(decimal currentAmount, DateTime interestStartDate, DateTime asOfDate, decimal interestRateAsDecimal, decimal overPaymentInterestRate, int periodsInYear)
        {
            var interestAndPerDiem = CalculateInterestAndPerDiem(currentAmount, interestStartDate, asOfDate, interestRateAsDecimal,
                overPaymentInterestRate, periodsInYear);
            return interestAndPerDiem.interest;           
        }

        public (decimal interest, decimal perDiem) CalculateInterestAndPerDiem(decimal currentAmount, DateTime interestStartDate, DateTime asOfDate, decimal interestRateAsDecimal, decimal overPaymentInterestRate, int periodsInYear)
        {
            decimal principalDue;
            decimal perDiem = 0;
            int daysPassed = 0;
            principalDue = currentAmount * -1;
            daysPassed = (asOfDate.Date - interestStartDate.Date).Days;
            if (daysPassed < 1)
            {
                perDiem = GetInterestPayment((interestRateAsDecimal / periodsInYear), 1, 1, principalDue);
            }
            else if (currentAmount == 0)
            {
                perDiem = 0;
            }
            else
            {
                if (currentAmount > 0)
                {
                    perDiem = GetInterestPayment((interestRateAsDecimal / periodsInYear), 1, daysPassed, principalDue);
                }
                else
                {
                    perDiem = GetInterestPayment((overPaymentInterestRate / periodsInYear), 1, daysPassed, principalDue);
                }                
                return (interest: Math.Round(perDiem * daysPassed, 2), perDiem: perDiem);
            }

            return (interest: 0, perDiem: perDiem);
        }
        private  decimal GetInterestPayment(decimal rate, decimal period, decimal numberOfPeriods, decimal principal)
        {
            if (period < 1 || period > numberOfPeriods)
            {
                return 0;
            }
            else if (rate == 0)
            {
                return 0;
            }

            decimal payment = GetPayment(rate, numberOfPeriods, principal);
            var futureValue = GetFutureValue(rate, (period - 1), payment, principal);

            return (futureValue * rate);
        }
        private  decimal GetPayment(decimal rate, decimal numberOfPeriods, decimal principal)
        {
            decimal temp = Convert.ToDecimal(System.Math.Pow((Convert.ToDouble(rate + 1)), Convert.ToDouble(numberOfPeriods)));

            return ((-principal * temp) / ((temp - 1)) * rate);
        }

        /// <summary>
        /// Calculates the future value of an investment based on a constant interest rate. You can use future value with either periodic, constant payments, or a single lump sum payment.
        /// </summary>
        private  decimal GetFutureValue(decimal interestRate, decimal numberOfPeriods, decimal payment, decimal presentValue)
        {
            decimal temp = Convert.ToDecimal(System.Math.Pow(Convert.ToDouble(interestRate + 1), Convert.ToDouble(numberOfPeriods)));

            return ((-presentValue) * temp) - ((payment / interestRate) * (temp - 1));
        }
    }
}