﻿using System;

namespace SharedApplication
{
    public class EffectiveDateChoice
    {
        public DateTime EffectiveDate { get; set; }
        public bool UpdateRecordedDate { get; set; } = false;
        public bool Cancelled { get; set; } = false;
    }
}