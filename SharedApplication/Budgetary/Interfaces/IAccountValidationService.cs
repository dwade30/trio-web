﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary.Interfaces
{
	public interface IAccountValidationService
	{
		bool AccountValidate(string account, bool blnValidateMulti = false);
		bool ValidExpense(string account);
		bool ValidRevenue(string account);
		bool ValidLedger(string account);
		bool CheckValidAccount(string x);
		bool MakeExistingAccountValid(AccountBreakdownInformation accountBreakdown);
		AccountBreakdownInformation CheckValidAccountNoUserInteraction(string x);
	}
}
