﻿using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary.Interfaces
{
	public interface IGlobalBudgetaryAccountSettings
	{
		bool ExpDivFlag { get; set; }
		bool RevDivFlag { get; set; }
		bool ObjFlag { get; set; }
		bool YearFlag { get; set; }
		string Exp { get; set; }
		string Rev { get; set; }
		string Ledger { get; set; }
        bool UsePUCChartOfAccounts { get; set; }
        ValidAccountSetting ValidAcctCheck { get; set; }
		int ExpenseDepartmentLength();
		int ExpenseDivisionLength();
		int ExpenseExpenseLength();
		int ExpenseObjectLength();
		int LedgerFundLength();
		int LedgerAccountLength();
		int LedgerSuffixLength();
		int RevenueDepartmentLength();
		int RevenueDivisionLength();
		int RevenueRevenueLength();
		bool DivisionExistsInExpense();
		bool DivisionExistsInRevenue();
		bool ObjectExistsInExpense();
		bool SuffixExistsInLedger();
		int ExpenseTotalLength();
		int RevenueTotalLength();
		int LedgerTotalLength();
		string RevenueZeroDivisionString();
		string ExpenseZeroDivisionString();
		string ObjectZeroString();
		string AccountZeroString();
		string FormatBudgetaryFund(int fund);
		string BuildLedgerAccount(int fund, int account, int suffix = 0);


	}
}