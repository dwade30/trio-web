﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary.Interfaces
{
	public interface IBudgetaryJournalService
	{
		int CreateGeneralJournal(int period, string description, IEnumerable<JournalEntryInfo> entries);
	}
}
