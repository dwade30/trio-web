﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary.Interfaces
{
	public interface IBudgetaryContext
	{
		IQueryable<LedgerTitle> LedgerTitles { get; }
		IQueryable<RevTitle> RevTitles { get; }
		IQueryable<DeptDivTitle> DeptDivTitles { get; }
		IQueryable<ExpObjTitle> ExpObjTitles { get; }
		IQueryable<AccountMaster> AccountMasters { get; }
        IQueryable<LedgerRange> LedgerRanges { get; }
        IQueryable<BudgetarySetting> BudgetarySettings { get; }
        IQueryable<StandardAccount> StandardAccounts { get; }
        IQueryable<JournalLock> JournalLocks { get; }
        IQueryable<JournalMaster> JournalMasters { get; }
        IQueryable<JournalEntry> JournalEntries { get; }

        IQueryable<APJournal> APJournals { get; }
        IQueryable<APJournalDetail> APJournalDetails { get; }
        IQueryable<Adjustment> Adjustments { get; }
        IQueryable<VendorMaster> VendorMasters { get; }
        IQueryable<APjournalArchive> APJournalArchives { get; }
        IQueryable<APJournalDetailArchive> APJournalDetailArchives { get; }
        IQueryable<CdjournalArchive> CDJournalArchives { get; }

        IQueryable<TaxTitle> TaxTitles { get; }
        IQueryable<VendorTaxInfo> VendorTaxInfos { get; }
        IQueryable<VendorTaxInfoArchive> VendorTaxInfoArchives { get; }

	}
}
