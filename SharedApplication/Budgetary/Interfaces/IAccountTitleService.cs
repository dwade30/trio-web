﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Interfaces
{
	public interface IAccountTitleService
	{
		string GetDepartmentName(string Department, bool blnLongDescription = false);
		string GetExpDivisionName(string Department, string Division, bool blnLongDescription = false);
		string GetRevDivisionName(string Department, string Division, bool blnLongDescription = false);
		string GetExpenseName(string Expense, bool blnLongDescription = false);
		string GetObjectName(string Expense, string Object, bool blnLongDescription = false);
		string GetRevenueName(string Department, string Division, string Revenue, bool blnLongDescription = false);
		string GetFundName(string Fund, bool blnLongDescription = false);
		string GetAccountName(string Fund, string Account, string Suffix, bool blnLongDescription = false);
		string ReturnAccountDescription(string x, bool blnLongDescription = false);
	}
}
