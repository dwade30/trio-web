﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary.Interfaces
{
	public interface IBudgetaryAccountingService
	{
		string GetDepartment(string x);
		string GetExpense(string x);
		string GetFundFromLedger(string account);
		int GetFundFromAccount(string account);
		AccountBreakdownInformation GetAccountBreakDownInformation(string account);
		ExpenseAccountBreakdown ConvertAccountBreakdownToExpenseAccountBreakdown(AccountBreakdownInformation accountBreakdown);
		LedgerAccountBreakdown ConvertAccountBreakdownToLedgerAccountBreakdown(AccountBreakdownInformation accountBreakdown);
		RevenueAccountBreakdown ConvertAccountBreakdownToRevenueAccountBreakdown(AccountBreakdownInformation accountBreakdown);
	}
}
