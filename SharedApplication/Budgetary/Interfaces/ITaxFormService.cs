﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Models;
using SharedApplication.Enums;

namespace SharedApplication.Budgetary.Interfaces
{
    public interface ITaxFormService
    {
        string GetTaxCategoryDescription(TaxFormType type, int categoryCode);
        decimal GetVendorTaxCategoryTotal(int vendorNumber, TaxFormType type, int categoryCode);
        List<int> GetEligibleVendorNumbers(TaxFormType type, decimal minimumAmount);
        List<VendorInfo1099Process> GetEligibleVendors(TaxFormType type, decimal minimumAmount);
        List<Adjustment> GetVendorAdjustments(TaxFormType type, int vendorNumber);
        List<EditReportDetailInfo> GetVendorEditReportDetails(TaxFormType type, int vendorNumber, int year, bool includeAllPayments,
            string taxCodeOverride = "");
        string GetTaxCodeDescription(int taxCode);
    }
}
