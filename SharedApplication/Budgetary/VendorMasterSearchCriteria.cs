﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary
{
    public class VendorMasterSearchCriteria
    {
        public int VendorNumber { get; set; } = 0;
        public List<int> VendorNumbers = new List<int>();
    }
}
