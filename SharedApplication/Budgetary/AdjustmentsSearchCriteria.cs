﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public class AdjustmentsSearchCriteria
    {
        public int VendorNumber { get; set; }
        public TaxFormType FormType { get; set; } = TaxFormType.All;
    }
}
