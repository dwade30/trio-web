﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public class EditReportDetailSearchCriteria
    {
        public int VendorNumber { get; set; } = 0;
        public int Year { get; set; } = 0;
        public bool IncludeAllPayments { get; set; } = false;
        public string Override1099Code { get; set; } = "";
    }
}
