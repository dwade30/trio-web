﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CreditMemoDetail
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string _1099 { get; set; }
        public string Account { get; set; }
        public decimal? Amount { get; set; }
        public string Project { get; set; }
        public decimal? Adjustments { get; set; }
        public decimal? Liquidated { get; set; }
    }
}
