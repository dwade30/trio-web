﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ArchiveAPAmount
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public string CashAccount { get; set; }
        public short? JournalNumber { get; set; }
        public decimal? Amount { get; set; }
        public bool? Correction { get; set; }
        public bool? Transferred { get; set; }
    }
}
