﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class APjournalArchive
    {
        public int Id { get; set; }
        public int? JournalNumber { get; set; }
        public int? VendorNumber { get; set; }
        public string TempVendorName { get; set; }
        public string TempVendorAddress1 { get; set; }
        public string TempVendorAddress2 { get; set; }
        public string TempVendorAddress3 { get; set; }
        public string TempVendorAddress4 { get; set; }
        public int? Period { get; set; }
        public DateTime? Payable { get; set; }
        public string Frequency { get; set; }
        public DateTime? Until { get; set; }
        public bool? Seperate { get; set; }
        public string Description { get; set; }
        public string Reference { get; set; }
        public decimal? Amount { get; set; }
        public string CheckNumber { get; set; }
        public string Status { get; set; }
        public DateTime? PostedDate { get; set; }
        public string Warrant { get; set; }
        public DateTime? CheckDate { get; set; }
        public string Returned { get; set; }
        public decimal? TotalEncumbrance { get; set; }
        public int? EncumbranceRecord { get; set; }
        public bool? UseAlternateCash { get; set; }
        public string AlternateCashAccount { get; set; }
        public string TempVendorCity { get; set; }
        public string TempVendorState { get; set; }
        public string TempVendorZip { get; set; }
        public string TempVendorZip4 { get; set; }
        public bool? PrepaidCheck { get; set; }
        public int? SeperateCheckRecord { get; set; }
        public bool? CreditMemoCorrection { get; set; }
        public int? CreditMemoRecord { get; set; }
        public string SchoolOverride { get; set; }
        public string TownOverride { get; set; }
        public bool? PrintedIndividual { get; set; }
        public int? PurchaseOrderId { get; set; }
        public bool? Eftcheck { get; set; }
        public VendorMaster VendorMaster { get; set; }
        public ICollection<APJournalDetailArchive> APJournalDetails { get; set; }
    }
}
