﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CalculationNeeded
    {
        public int Id { get; set; }
        public bool? ExpPendingSummary { get; set; }
        public bool? ExpPostedSummary { get; set; }
        public bool? RevPendingSummary { get; set; }
        public bool? RevPostedSummary { get; set; }
        public bool? LedgerPendingSummary { get; set; }
        public bool? LedgerPostedSummary { get; set; }
        public bool? ExpPendingDetail { get; set; }
        public bool? ExpPostedDetail { get; set; }
        public bool? RevPendingDetail { get; set; }
        public bool? RevPostedDetail { get; set; }
        public bool? LedgerPendingDetail { get; set; }
        public bool? LedgerPostedDetail { get; set; }
        public bool? SchoolExpPendingSummary { get; set; }
        public bool? SchoolExpPostedSummary { get; set; }
        public bool? SchoolRevPendingSummary { get; set; }
        public bool? SchoolRevPostedSummary { get; set; }
        public bool? SchoolLedgerPendingSummary { get; set; }
        public bool? SchoolLedgerPostedSummary { get; set; }
        public bool? SchoolExpPendingDetail { get; set; }
        public bool? SchoolExpPostedDetail { get; set; }
        public bool? SchoolRevPendingDetail { get; set; }
        public bool? SchoolRevPostedDetail { get; set; }
        public bool? SchoolLedgerPendingDetail { get; set; }
        public bool? SchoolLedgerPostedDetail { get; set; }
    }
}
