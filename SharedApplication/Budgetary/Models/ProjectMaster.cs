﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ProjectMaster
    {
        public int Id { get; set; }
        public string ProjectCode { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        public string Fund { get; set; }
        public decimal? ProjectedCost { get; set; }
    }
}
