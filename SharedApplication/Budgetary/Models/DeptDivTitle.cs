﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class DeptDivTitle
    {
        public string Department { get; set; }
        
        public string Division { get; set; }

        
        public string ShortDescription { get; set; }

        
        public string LongDescription { get; set; }

        public int ID { get; set; }

        
        public string Fund { get; set; }

        
        public string CloseoutAccount { get; set; }

        public int? BreakdownCode { get; set; }
    }
}
