﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CustomReportsDetail
    {
        public int Id { get; set; }
        public int? ReportKey { get; set; }
        public int? Row { get; set; }
        public string Type { get; set; }
        public string Account { get; set; }
        public string DetailSummary { get; set; }
        public string Title { get; set; }
    }
}
