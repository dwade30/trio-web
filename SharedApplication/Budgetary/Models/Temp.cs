﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Temp
    {
        public int Id { get; set; }
        public string AccountType { get; set; }
        public string FirstAccountField { get; set; }
        public string SecondAccountField { get; set; }
        public string ThirdAccountField { get; set; }
        public string FourthAccountField { get; set; }
        public string Account { get; set; }
        public string FifthAccountField { get; set; }
        public string GlaccountType { get; set; }
    }
}
