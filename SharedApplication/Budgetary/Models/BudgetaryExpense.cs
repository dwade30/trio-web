﻿using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public class BudgetaryExpense
    {
        public int id { get; set; } = 0;
        public string Description { get; set; } = "";
        public string ShortDescription { get; set; } = "";
        public string ExpenseCode { get; set; } = "";
        public int BreakdownCode { get; set; } = 0;

        private List<BudgetaryObject> objects = new List<BudgetaryObject>();

        public IEnumerable<BudgetaryObject> Objects()
        {
            return objects;
        }

        public void AddObject (BudgetaryObject budgetaryObject)
        {
            if (budgetaryObject != null)
            {
                objects.Add(budgetaryObject);
            }
        }
    }
}