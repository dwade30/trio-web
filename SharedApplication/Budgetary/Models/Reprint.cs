﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Reprint
    {
        public int Id { get; set; }
        public int? ReportOrder { get; set; }
        public int? WarrantNumber { get; set; }
        public string Type { get; set; }
        public int? TitleOption { get; set; }
    }
}
