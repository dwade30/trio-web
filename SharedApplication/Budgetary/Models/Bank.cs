﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class Bank
    {
        public int? BankNumber { get; set; }
        public string Name { get; set; }
        public decimal? Balance { get; set; }
        public bool? CurrentBank { get; set; }
        public DateTime? StatementDate { get; set; }
        public string Fund { get; set; }
        public string CashAccount { get; set; }
        public string TownCashAccount { get; set; }
        public string SchoolCashAccount { get; set; }
        public string RoutingNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public string AccountType { get; set; }
        public int ID { get; set; }
    }
}
