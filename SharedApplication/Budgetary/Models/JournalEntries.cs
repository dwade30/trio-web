﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class JournalEntry
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime? JournalEntriesDate { get; set; }
        public string Description { get; set; }
        public int? VendorNumber { get; set; }
        public int? JournalNumber { get; set; }
        public string CheckNumber { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public decimal? Amount { get; set; }
        public int? WarrantNumber { get; set; }
        public int? Period { get; set; }
        public string Rcb { get; set; }
        public string Status { get; set; }
        public DateTime? PostedDate { get; set; }
        public string Po { get; set; }
        public string _1099 { get; set; }
        public bool? CarryForward { get; set; }
        public int? BankNumber { get; set; }
        public VendorMaster VendorMaster { get; set; }
    }
}
