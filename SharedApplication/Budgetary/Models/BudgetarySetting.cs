﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class BudgetarySetting
    {
        public int ID { get; set; }
        public string UseProjectNumber { get; set; }
        public string FiscalStart { get; set; }
        public string SummaryReportOption { get; set; }
        public string WarrantPrevOutput { get; set; }
        public string WarrantPrevSigLine { get; set; }
        public string CheckRef { get; set; }
        public string PreviewPrint { get; set; }
        public string WarrantPrintOption { get; set; }
        public string APSeq { get; set; }
        public string EOYStatus { get; set; }
        public string CashDue { get; set; }
        public string DueFrom { get; set; }
        public string DueTo { get; set; }
        public string Due { get; set; }
        public string Expenditure { get; set; }
        public string Revenue { get; set; }
        public string Ledger { get; set; }
        public string ValidAccountUse { get; set; }
        public string CheckRec { get; set; }

        public bool? ValidAccountBudgetProcess { get; set; }

        public DateTime? PayDate { get; set; }

        public DateTime? StatementDate { get; set; }

        public int? CheckFormat { get; set; }
        public bool? C1099Laser { get; set; }

        public double? LaserAdjustment { get; set; }
        public double? C1099LaserAdjustment { get; set; }

        public double? CheckLaserAdjustment { get; set; }

        public double? ReportAdjustment { get; set; }
        public string ShadeReports { get; set; }

        public bool? ShowIcon { get; set; }
        public string CheckIcon { get; set; }

        public bool? WarrantPrevDeptSummary { get; set; }

        public bool? LandscapeWarrant { get; set; }

        public bool? PrintSignature { get; set; }

        public double? LabelLaserAdjustment { get; set; }

        public bool? AllowManualWarrantNumbers { get; set; }

        public bool? ConvertedCityStateZip { get; set; }

        public bool? RecapOrderByAccounts { get; set; }

        public bool? SameVendorOnly { get; set; }

        public bool? FixedPastYearBudget { get; set; }

        public bool? AllowSchoolAccounts { get; set; }

        public int? BalanceValidation { get; set; }

        public bool? UseTownSeal { get; set; }

        public bool? EncByDept { get; set; }

        public bool? AllowTownAccounts { get; set; }

        public int? SchoolPreviewPrint { get; set; }

        public bool? AutonumberPO { get; set; }

        public double? C1099LaserAdjustmentHorizontal { get; set; }

        public bool? ExcludeAPDueToFrom { get; set; }

        public bool? ExcludeCRDueToFrom { get; set; }

        public bool? ExcludePYDueToFrom { get; set; }

        public bool? AutomaticInterest { get; set; }

        public bool? PUCChartOfAccounts { get; set; }
        public string ImmediateOriginODFINumber { get; set; }
        public string ImmediateOriginName { get; set; }
        public string ImmediateOriginRoutingNumber { get; set; }

        public string ImmediateDestinationName { get; set; }
        public string ImmediateDestinationRoutingNumber { get; set; }
        public string ACHTownName { get; set; }
        public string ACHFederalID { get; set; }

        public bool? BalancedACHFile { get; set; }

        public short? ACHFederalIDPrefix { get; set; }

        public short? EncCarryForward { get; set; }
        public string ReturnAddress1 { get; set; }
        public string ReturnAddress2 { get; set; }
        public string ReturnAddress3 { get; set; }
        public string ReturnAddress4 { get; set; }

        public bool? AutoPostAP { get; set; }

        public bool? AutoPostGJ { get; set; }

        public bool? AutoPostEN { get; set; }

        public bool? AutoPostCR { get; set; }

        public bool? AutoPostCD { get; set; }

        public bool? AutoPostCM { get; set; }

        public bool? AutoPostAPCorr { get; set; }

        public bool? AutoPostReversingJournals { get; set; }

        public bool? AutoPostBudgetTransfer { get; set; }

        public bool? AutoPostCreateOpeningAdjustments { get; set; }

        public bool? AutoPostARBills { get; set; }

        public bool? AutoPostCRDailyAudit { get; set; }

        public bool? AutoPostFADepreciation { get; set; }

        public bool? AutoPostMARR { get; set; }

        public bool? AutoPostPYProcesses { get; set; }

        public bool? AutoPostCLLiens { get; set; }

        public bool? AutoPostCLTA { get; set; }

        public bool? AutoPostBLCommitments { get; set; }

        public bool? AutoPostBLSupplementals { get; set; }

        public bool? AutoPostUBBills { get; set; }

        public bool? AutoPostUBLiens { get; set; }
        public string Version { get; set; }
    }
}
