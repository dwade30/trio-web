﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ItemsInChart
    {
        public int Id { get; set; }
        public int? GraphItemId { get; set; }
        public int? ChartKey { get; set; }
    }
}
