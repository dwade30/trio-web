﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public class VendorInfo1099Process
    {
        public string VendorName { get; set; }
        public int VendorNumber { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string TaxIdNumber { get; set; }
        public bool IncludeAllPayments { get; set; }
        public string TaxCode { get; set; }
        public bool IsAttorney { get; set; }
    }
}
