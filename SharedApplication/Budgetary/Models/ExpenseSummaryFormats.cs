﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ExpenseSummaryFormat
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool? Budget { get; set; }
        public bool? BudgetAdjustment { get; set; }
        public bool? NetBudget { get; set; }
        public bool? CurrentDebitCredit { get; set; }
        public bool? CurrentNet { get; set; }
        public bool? YtddebitCredit { get; set; }
        public bool? Ytdnet { get; set; }
        public bool? Osencumbrance { get; set; }
        public bool? SeperatePending { get; set; }
        public bool? IncludePending { get; set; }
        public bool? Balance { get; set; }
        public bool? Spent { get; set; }
        public string DescriptionLength { get; set; }
        public string Font { get; set; }
        public string PaperWidth { get; set; }
        public string Use { get; set; }
        public string Printer { get; set; }
        public bool? Bold { get; set; }
        public bool? Italic { get; set; }
        public bool? StrikeThru { get; set; }
        public bool? Underline { get; set; }
        public string FontName { get; set; }
        public int? FontSize { get; set; }
        public bool? Mtdbudget { get; set; }
    }
}
