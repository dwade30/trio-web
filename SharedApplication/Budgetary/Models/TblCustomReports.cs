﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class TblCustomReports
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public string Type { get; set; }
        public string Sql { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
