﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class RevTitle
    {
        public int Id { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string Revenue { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string CloseoutAccount { get; set; }
    }
}
