﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary.Models
{
    public class EditReportSetupInfo
    {
        public decimal MinimumAmount { get; set; } = 0;
        public VendorSortOption SortOption { get; set; }
        public int Year { get; set; }
        public TaxFormType FormType { get; set; }
    }
}
