﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class LedgerAccountBreakdown
	{
		public string Fund;
		public string Account;
		public string Suffix;
	}
}
