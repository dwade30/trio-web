﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CustomReport
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool? IncludePending { get; set; }
        public bool? IncludeEnc { get; set; }
        public bool? Budget { get; set; }
        public bool? CurrentActivity { get; set; }
        public bool? Ytdactivity { get; set; }
        public bool? Balance { get; set; }
        public bool? PercentSpent { get; set; }
        public string DateRange { get; set; }
        public int? LowDate { get; set; }
        public int? HighDate { get; set; }
        public bool? CheckMonthRange { get; set; }
        public string AcctInfo { get; set; }
        public bool? CurrentDebCred { get; set; }
        public bool? YtddebCred { get; set; }
    }
}
