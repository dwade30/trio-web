﻿using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public class BudgetaryDepartment
    {
        public int Id { get; set; }
        public string DepartmentCode { get; set; } = "";
        public string Description { get; set; } = "";
        public string ShortDescription { get; set; } = "";
        public string Fund { get; set; } = "";
        public string CloseoutAccount { get; set; } = "";
        public int BreakdownCode { get; set; }

        private List<BudgetaryDivision> divisions = new List<BudgetaryDivision>();
        public IEnumerable<BudgetaryDivision> Divisions
        {
            get { return divisions; }
        }

        public void AddDivision(BudgetaryDivision division)
        {
            if (division != null)
            {
                divisions.Add(division);
            }
        }
    }
}