﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class BankCheckHistory
    {
        public int Id { get; set; }
        public int? BankId { get; set; }
        public string CheckType { get; set; }
        public int? LastCheckNumber { get; set; }
    }
}
