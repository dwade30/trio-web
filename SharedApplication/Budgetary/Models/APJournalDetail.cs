﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class APJournalDetail
    {
        public int ID { get; set; }

        public int? APJournalID { get; set; }
        public string Description { get; set; }
        public string Account { get; set; }
        public decimal? Amount { get; set; }
        public string Project { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Encumbrance { get; set; }
        public string C1099 { get; set; }
        public string RCB { get; set; }
        public bool? Corrected { get; set; }
        public int? EncumbranceDetailRecord { get; set; }
        public decimal? CorrectedAmount { get; set; }
        public int? PurchaseOrderDetailsID { get; set; }
        public APJournal APJournal { get; set; }
    }
}
