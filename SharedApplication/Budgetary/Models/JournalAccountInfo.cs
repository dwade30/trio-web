﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class JournalAccountInfo
	{
		public string BudgetaryAccountNumber { get; set; } = "";
		public decimal Amount { get; set; } = 0;
	}
}
