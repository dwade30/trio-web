﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class LastChecksRun
    {
        public int Id { get; set; }
        public int? StartCheckNumber { get; set; }
        public int? EndCheckNumber { get; set; }
    }
}
