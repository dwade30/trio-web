﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Polock
    {
        public int Id { get; set; }
        public bool? MasterLock { get; set; }
        public string OpId { get; set; }
        public int? NextPo { get; set; }
    }
}
