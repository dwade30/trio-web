﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Eoyprocess
    {
        public int Id { get; set; }
        public bool? CreateArchive { get; set; }
        public bool? CloseControl { get; set; }
        public bool? PostClosing { get; set; }
        public bool? ClearBudgets { get; set; }
        public bool? ResetNumbers { get; set; }
        public bool? Save1099 { get; set; }
        public bool? ClearDetail { get; set; }
        public DateTime? EoycompletedDate { get; set; }
        public bool? CloseUnliquidated { get; set; }
        public bool? CarryOver { get; set; }
    }
}
