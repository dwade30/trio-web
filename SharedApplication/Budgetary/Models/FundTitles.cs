﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class FundTitle
    {
        public int Id { get; set; }
        public int? Fund { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Abbreviation { get; set; }
    }
}
