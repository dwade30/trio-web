﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CustomCheck
    {
        public int Id { get; set; }
        public string CheckType { get; set; }
        public string CheckPosition { get; set; }
        public string ScannedCheckFile { get; set; }
        public bool? UseDoubleStub { get; set; }
        public string Description { get; set; }
        public bool? UseTwoStubs { get; set; }
        public string VoidAfterMessage { get; set; }
    }
}
