﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class Budget
    {
        public int ID { get; set; }


        public string Account { get; set; }

   
        public decimal? InitialRequest { get; set; }
        
        public decimal? ManagerRequest { get; set; }
        
        public decimal? CommitteeRequest { get; set; }

     
        public decimal? ApprovedAmount { get; set; }

        public string Comments { get; set; }

     
        public decimal? JanBudget { get; set; }
        
        public decimal? FebBudget { get; set; }

        public decimal? MarBudget { get; set; }

       
        public decimal? AprBudget { get; set; }

        public decimal? MayBudget { get; set; }

     
        public decimal? JuneBudget { get; set; }
        
        public decimal? JulyBudget { get; set; }

        public decimal? AugBudget { get; set; }

        public decimal? SeptBudget { get; set; }


        public decimal? OctBudget { get; set; }

        public decimal? NovBudget { get; set; }
        
        public decimal? DecBudget { get; set; }

        public float? JanPercent { get; set; }

        public float? FebPercent { get; set; }

        public float? MarPercent { get; set; }

        public float? AprPercent { get; set; }

        public float? MayPercent { get; set; }

        public float? JunePercent { get; set; }

        public float? JulyPercent { get; set; }

        public float? AugPercent { get; set; }

        public float? SeptPercent { get; set; }

        public float? OctPercent { get; set; }

        public float? NovPercent { get; set; }

        public float? DecPercent { get; set; }

        public bool? PercentFlag { get; set; }

        
        public decimal? ElectedRequest { get; set; }
    }
}
