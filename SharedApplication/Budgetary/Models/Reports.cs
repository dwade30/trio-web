﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Reports
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string Title { get; set; }
        public int? CriteriaId { get; set; }
        public int? FormatId { get; set; }
        public bool? ShowReportTitle { get; set; }
    }
}
