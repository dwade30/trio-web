﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class WarrantLock
    {
        public int Id { get; set; }
        public bool? MasterLock { get; set; }
        public string OpId { get; set; }
    }
}
