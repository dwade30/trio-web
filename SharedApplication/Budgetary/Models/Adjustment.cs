﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class Adjustment
    {
        public int ID { get; set; }

        public int? AdjustmentNumber { get; set; }

        public int? VendorNumber { get; set; }
        
        public decimal? Amount { get; set; }

        public bool? Positive { get; set; }

        public int? Code { get; set; }
        public VendorMaster VendorMaster { get; set; }
    }
}
