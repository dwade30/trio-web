﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class JournalMaster
    {
        public int Id { get; set; }
        public int? JournalNumber { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public int? Period { get; set; }
        public bool? PostedOob { get; set; }
        public string OobpostingOpId { get; set; }
        public DateTime? OobpostingDate { get; set; }
        public DateTime? OobpostingTime { get; set; }
        public DateTime? StatusChangeDate { get; set; }
        public DateTime? StatusChangeTime { get; set; }
        public string StatusChangeOpId { get; set; }
        public DateTime? CheckDate { get; set; }
        public bool? AutomaticJournal { get; set; }
        public bool? CreditMemo { get; set; }
        public bool? CreditMemoCorrection { get; set; }
        public short? BankNumber { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? TotalCreditMemo { get; set; }
        public int? CrperiodCloseoutKey { get; set; }
    }
}
