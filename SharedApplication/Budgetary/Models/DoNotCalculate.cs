﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class DoNotCalculate
    {
        public int Id { get; set; }
        public string Account { get; set; }
    }
}
