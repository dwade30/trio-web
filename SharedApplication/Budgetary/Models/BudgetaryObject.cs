﻿namespace SharedApplication.Budgetary.Models
{
    public class BudgetaryObject
    {
        public int Id { get; set; } = 0;
        public string ObjectCode { get; set; } = "";
        public int BreakdownCode { get; set; } = 0;
        public string Description { get; set; } = "";
        public string ShortDescription { get; set; } = "";
        public string Tax { get; set; } = "N";
    }
}