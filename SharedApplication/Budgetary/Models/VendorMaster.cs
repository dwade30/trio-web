﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class VendorMaster
    {
        public int? VendorNumber { get; set; }
        public string CheckName { get; set; }
        public string CheckAddress1 { get; set; }
        public string CheckAddress2 { get; set; }
        public string CheckAddress3 { get; set; }
        public string CheckAddress4 { get; set; }
        public bool? Same { get; set; }
        public string CorrespondName { get; set; }
        public string CorrespondAddress1 { get; set; }
        public string CorrespondAddress2 { get; set; }
        public string CorrespondAddress3 { get; set; }
        public string CorrespondAddress4 { get; set; }
        public string Contact { get; set; }
        public string TelephoneNumber { get; set; }
        public string Extension { get; set; }
        public string Status { get; set; }
        public string Class { get; set; }
        public bool? C1099 { get; set; }
        public string TaxNumber { get; set; }
        public string Message { get; set; }
        public string CheckMessage { get; set; }
        public string DataEntryMessage { get; set; }
        public DateTime? LastPayment { get; set; }
        public string CheckNumber { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string UpdatedBy { get; set; }
        public string DefaultDescription { get; set; }
        public bool? Attorney { get; set; }
        public bool? C1099UseCorrName { get; set; }
        public bool? OneTimeCheckMessage { get; set; }
        public int ID { get; set; }
        public string Email { get; set; }
        public string FaxNumber { get; set; }
        public string CheckCity { get; set; }
        public string CheckState { get; set; }
        public string CheckZip { get; set; }
        public string CheckZip4 { get; set; }
        public string CorrespondCity { get; set; }
        public string CorrespondState { get; set; }
        public string CorrespondZip { get; set; }
        public string CorrespondZip4 { get; set; }
        public string C1099Code { get; set; }
        public bool? IncludeAllPaymentsFor1099 { get; set; }
        public string DefaultCheckNumber { get; set; }
        public bool? InsuranceRequired { get; set; }
        public DateTime? InsuranceVerifiedDate { get; set; }
        public bool? EFT { get; set; }
        public bool? Prenote { get; set; }
        public string RoutingNumber { get; set; }
        public string BankAccountNumber { get; set; }
        public string AccountType { get; set; }
        public ICollection<VendorTaxInfo> VendorTaxInfos { get; set; }
        public ICollection<VendorTaxInfoArchive> VendorTaxInfoArchives { get; set; }
        public ICollection<APJournal> APJournals { get; set; }
        public ICollection<APjournalArchive> APJournalArchives { get; set; }
        public ICollection<CdjournalArchive> CDJournalArchives { get; set; }
        public ICollection<JournalEntry> JournalEntries { get; set; }
        public ICollection<Adjustment> Adjustments { get; set; }
    }
}
