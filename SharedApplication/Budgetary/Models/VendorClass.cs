﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class VendorClass
    {
        public int Id { get; set; }
        public int? VendorNumber { get; set; }
        public string Class { get; set; }
    }
}
