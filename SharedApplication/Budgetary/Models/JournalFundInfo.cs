﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class JournalFundInfo
	{
		public int Fund { get; set; }
		public decimal Amount { get; set; }
		public bool UseDueToFrom { get; set; }
		public string DefaultCashFund { get; set; }
		public string OverrideAccountsPayableCashFund { get; set; }
		public string OverrideCashReceiptsCashFund { get; set; }
		public string OverridePayrollCashFund { get; set; }
	}
}
