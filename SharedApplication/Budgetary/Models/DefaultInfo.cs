﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class DefaultInfo
    {
        public int Id { get; set; }
        public int? VendorNumber { get; set; }
        public int? DescriptionNumber { get; set; }
        public string Tax { get; set; }
        public string Project { get; set; }
        public string AccountNumber { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
    }
}
