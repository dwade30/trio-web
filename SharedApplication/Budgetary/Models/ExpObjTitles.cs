﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ExpObjTitle
    {
        public int Id { get; set; }
        public string Expense { get; set; }
        public string Object { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Tax { get; set; }
        public int? BreakdownCode { get; set; }
    }
}
