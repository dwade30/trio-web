﻿using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public class BudgetaryAccount
    {
        public int ID { get; set; }
        public string Account { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        private List<string> accountSegments = new List<string>();
        private string accountPrefix = "g";
        public string AccountType
        {
            get { return accountPrefix; }
            set
            {
                accountPrefix = value;
                SetGLAccountType(accountPrefix);
            }
        }
        public string FirstAccountField { get; set; }
        public string SecondAccountField { get; set; }
        public string ThirdAccountField { get; set; }
        public string FourthAccountField { get; set; }
        public bool? IsValid { get; set; }

        public void SetGLAccountType(string glAccountType)
        {
            switch (glAccountType.ToLower())
            {
                case "1":
                case "a":
                    TypeOfAccount = BudgetaryAccountType.Asset;
                    return;
                case "2":
                case "l":
                    TypeOfAccount = BudgetaryAccountType.Liability;
                    return;
                case "3":
                case "f":
                    TypeOfAccount = BudgetaryAccountType.FundBalance;
                    return;
                case "e":
                    TypeOfAccount = BudgetaryAccountType.Expense;
                    return;
                case "r":
                    TypeOfAccount = BudgetaryAccountType.Revenue;
                    return;
            }
        }
        public BudgetaryAccountType TypeOfAccount { get; set; }
        public bool IsLedger()
        {
            return AccountType.ToLower() == "g";
        }

        public bool IsAsset()
        {
            return TypeOfAccount == BudgetaryAccountType.Asset;
        }

        public bool IsLiability()
        {
            return TypeOfAccount == BudgetaryAccountType.Liability;
        }

        public bool IsFundBalance()
        {
            return TypeOfAccount == BudgetaryAccountType.FundBalance;
        }

        public bool IsExpense()
        {
            return TypeOfAccount == BudgetaryAccountType.Expense;
        }

        public bool IsRevenue()
        {
            return TypeOfAccount == BudgetaryAccountType.Revenue;
        }
    }

    public enum BudgetaryAccountType
    {
        Asset = 0,
        Liability = 1,
        FundBalance = 2,
        Expense = 3,
        Revenue = 4
    }
}