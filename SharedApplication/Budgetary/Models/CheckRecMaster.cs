﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CheckRecMaster
    {
        public int Id { get; set; }
        public int? CheckNumber { get; set; }
        public string Type { get; set; }
        public DateTime? CheckDate { get; set; }
        public string Name { get; set; }
        public decimal? Amount { get; set; }
        public string Status { get; set; }
        public DateTime? StatusDate { get; set; }
        public int? BankNumber { get; set; }
        public bool? DirectDeposit { get; set; }
        public int? CrperiodCloseoutKey { get; set; }
        public DateTime? PypayDate { get; set; }
        public int? PypayRunId { get; set; }
        public bool? Cdentry { get; set; }
        public bool? Transferred { get; set; }
        public bool? Automatic { get; set; }
        public string EnteredBy { get; set; }
        public DateTime? EnteredDate { get; set; }
    }
}
