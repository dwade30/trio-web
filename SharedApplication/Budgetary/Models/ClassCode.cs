﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class ClassCode
    {

        public string ClassKey { get; set; }

        public string Code { get; set; }

        public int ID { get; set; }
    }
}
