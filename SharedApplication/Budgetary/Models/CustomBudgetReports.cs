﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CustomBudgetReport
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Orientation { get; set; }
        public string AccountTypes { get; set; }
        public string ReportingRange { get; set; }
        public string LowDept { get; set; }
        public string HighDept { get; set; }
        public string CurrentYear { get; set; }
        public string BudgetYear { get; set; }
        public string DeptPageBreak { get; set; }
        public string DivPageBreak { get; set; }
        public string DeptTotals { get; set; }
        public string DivTotals { get; set; }
        public string ExpTotals { get; set; }
        public string Comments { get; set; }
        public string ReportTitle { get; set; }
        public string IncludeBudgetAdjustments { get; set; }
        public int? StartingPage { get; set; }
        public bool? ThreeYearBudget { get; set; }
        public bool? ThreeYearActual { get; set; }
        public bool? TwoYearBudget { get; set; }
        public bool? TwoYearActual { get; set; }
        public bool? OneYearBudget { get; set; }
        public bool? OneYearActual { get; set; }
        public bool? CurrentBudget { get; set; }
        public bool? CurrentYtd { get; set; }
        public bool? InitialRequest { get; set; }
        public bool? ManagerRequest { get; set; }
        public bool? CommitteeRequest { get; set; }
        public bool? ApprovedAmounts { get; set; }
        public bool? InitReqVsCurBudMon { get; set; }
        public bool? InitReqVsCurBudPer { get; set; }
        public bool? ManReqVsCurBudMon { get; set; }
        public bool? ManReqVsCurBudPer { get; set; }
        public bool? CommReqVsCurBudMon { get; set; }
        public bool? CommReqVsCurBudPer { get; set; }
        public bool? AppAmtVsCurBudMon { get; set; }
        public bool? AppAmtVsCurBudPer { get; set; }
        public string TownSchool { get; set; }
        public string FunctionPageBreak { get; set; }
        public string ObjTotals { get; set; }
        public string IncludeCarryForward { get; set; }
        public string ExpLowestDetailLevel { get; set; }
        public string RevLowestDetailLevel { get; set; }
        public bool? ElecReqVsCurBudMon { get; set; }
        public bool? ElecReqVsCurBudPer { get; set; }
        public bool? InitReqVsLastYrBudMon { get; set; }
        public bool? InitReqVsLastYrBudPer { get; set; }
        public bool? ManReqVsLastYrBudMon { get; set; }
        public bool? ManReqVsLastYrBudPer { get; set; }
        public bool? CommReqVsLastYrBudMon { get; set; }
        public bool? CommReqVsLastYrBudPer { get; set; }
        public bool? ElecReqVsLastYrBudMon { get; set; }
        public bool? ElecReqVsLastYrBudPer { get; set; }
        public bool? AppAmtVsLastYrBudMon { get; set; }
        public bool? AppAmtVsLastYrBudPer { get; set; }
        public bool? ElectedRequest { get; set; }
        public bool? CurrentBalance { get; set; }
        public bool? BlankInitialRequest { get; set; }
        public bool? BlankManagerRequest { get; set; }
        public bool? BlankCommitteeRequest { get; set; }
        public bool? BlankElectedRequest { get; set; }
        public bool? BlankApprovedAmount { get; set; }
        public bool? ThreeYearVsTwoYearActMon { get; set; }
        public bool? ThreeYearVsTwoYearActPer { get; set; }
        public bool? TwoYearVsOneYearActPer { get; set; }
        public bool? TwoYearVsOneYearActMon { get; set; }
        public bool? OneYearVsCurrentActMon { get; set; }
        public bool? OneYearVsCurrentActPer { get; set; }
    }
}
