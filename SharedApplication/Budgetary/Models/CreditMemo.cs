﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class CreditMemo
    {
        public int Id { get; set; }
        public int? JournalNumber { get; set; }
        public DateTime? CreditMemoDate { get; set; }
        public int? VendorNumber { get; set; }
        public string Description { get; set; }
        public string MemoNumber { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Liquidated { get; set; }
        public decimal? Adjustments { get; set; }
        public DateTime? PostedDate { get; set; }
        public int? Period { get; set; }
        public int? OriginalAprecord { get; set; }
        public string Fund { get; set; }
        public string Status { get; set; }
    }
}
