﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Encumbrance
    {
        public int Id { get; set; }
        public int? JournalNumber { get; set; }
        public DateTime? EncumbrancesDate { get; set; }
        public int? VendorNumber { get; set; }
        public string TempVendorName { get; set; }
        public string TempVendorAddress1 { get; set; }
        public string TempVendorAddress2 { get; set; }
        public string TempVendorAddress3 { get; set; }
        public string TempVendorAddress4 { get; set; }
        public string Description { get; set; }
        public string Po { get; set; }
        public decimal? Amount { get; set; }
        public string Status { get; set; }
        public decimal? Adjustments { get; set; }
        public decimal? Liquidated { get; set; }
        public int? Period { get; set; }
        public DateTime? PostedDate { get; set; }
        public string TempVendorCity { get; set; }
        public string TempVendorState { get; set; }
        public string TempVendorZip { get; set; }
        public string TempVendorZip4 { get; set; }
        public bool? PastYearEnc { get; set; }
    }
}
