﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public class EditReportDetailInfo
    {
        public string BudgetaryAccount { get; set; }
        public int JournalNumber { get; set; }
        public decimal Amount { get; set; }
        public string TaxCode { get; set; }
        public string Description { get; set; }
        public DateTime CheckDate { get; set; }
    }
}
