﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class VendorTaxInfo
    {
        public int Id { get; set; }
        public int? VendorNumber { get; set; }
        public int? TaxCode { get; set; }
        public decimal? Amount { get; set; }
        public int? Year { get; set; }
        public TaxTitle TaxTitle { get; set; }
        public VendorMaster VendorMaster { get; set; }

    }
}
