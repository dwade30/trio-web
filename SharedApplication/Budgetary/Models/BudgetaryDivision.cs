﻿namespace SharedApplication.Budgetary.Models
{
    public class BudgetaryDivision
    {
        public int Id { get; set; } = 0;
        public string DivisionCode { get; set; } = "";
        public string Description { get; set; } = "";
        public string ShortDescription { get; set; } = "";
        public string CloseoutAccount { get; set; } = "";
        public int BreakdownCode { get; set; } = 0;
    }
}