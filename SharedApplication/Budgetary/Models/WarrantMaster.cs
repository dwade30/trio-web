﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class WarrantMaster
    {
        public int Id { get; set; }
        public int? WarrantNumber { get; set; }
        public DateTime? WarrantDate { get; set; }
        public DateTime? WarrantTime { get; set; }
        public string WarrantOperator { get; set; }
    }
}
