﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class RevenueDetailInfo
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public int? Period { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string Revenue { get; set; }
        public string Status { get; set; }
        public DateTime? PostedDate { get; set; }
        public DateTime? TransDate { get; set; }
        public string Rcb { get; set; }
        public int? JournalNumber { get; set; }
        public string Description { get; set; }
        public int? Warrant { get; set; }
        public string CheckNumber { get; set; }
        public int? VendorNumber { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Encumbrance { get; set; }
        public decimal? Adjustments { get; set; }
        public decimal? Liquidated { get; set; }
        public string Type { get; set; }
        public int? OrderMonth { get; set; }
        public string Project { get; set; }
        public int? EncumbranceKey { get; set; }
    }
}
