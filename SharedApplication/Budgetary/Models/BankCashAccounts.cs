﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class BankCashAccount
    {
        public int Id { get; set; }
        public int? BankNumber { get; set; }
        public string Account { get; set; }
        public bool? AllSuffix { get; set; }
        public bool? Interest { get; set; }
    }
}
