﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class PastBudgets
    {
        public int Id { get; set; }
        public int? Year { get; set; }
        public string Account { get; set; }
        public decimal? StartBudget { get; set; }
        public decimal? JanBudget { get; set; }
        public decimal? FebBudget { get; set; }
        public decimal? MarBudget { get; set; }
        public decimal? AprBudget { get; set; }
        public decimal? MayBudget { get; set; }
        public decimal? JuneBudget { get; set; }
        public decimal? JulyBudget { get; set; }
        public decimal? AugBudget { get; set; }
        public decimal? SeptBudget { get; set; }
        public decimal? OctBudget { get; set; }
        public decimal? NovBudget { get; set; }
        public decimal? DecBudget { get; set; }
        public decimal? JanActual { get; set; }
        public decimal? FebActual { get; set; }
        public decimal? MarActual { get; set; }
        public decimal? AprActual { get; set; }
        public decimal? MayActual { get; set; }
        public decimal? JuneActual { get; set; }
        public decimal? JulyActual { get; set; }
        public decimal? AugActual { get; set; }
        public decimal? SeptActual { get; set; }
        public decimal? OctActual { get; set; }
        public decimal? NovActual { get; set; }
        public decimal? DecActual { get; set; }
        public decimal? EndBudget { get; set; }
        public decimal? ActualSpent { get; set; }
        public decimal? CarryForward { get; set; }
        public decimal? BudgetAdjustments { get; set; }
        public decimal? Ytdadjustments { get; set; }
        public decimal? YtdbudgetAdjustments { get; set; }
        public decimal? YtdcarryForward { get; set; }
    }
}
