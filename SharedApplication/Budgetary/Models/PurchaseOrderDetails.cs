﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class PurchaseOrderDetails
    {
        public int Id { get; set; }
        public int? PurchaseOrderId { get; set; }
        public string Description { get; set; }
        public string Account { get; set; }
        public decimal? Price { get; set; }
        public double? Quantity { get; set; }
        public decimal? Total { get; set; }
    }
}
