﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class AccountBreakdownInformation
	{
		public string Account;
		public string AccountType;
		public string FirstAccountField;
		public string SecondAccountField;
		public string ThirdAccountField;
		public string FourthAccountField;
		public string FifthAccountField;
		public bool Valid;
	}
}
