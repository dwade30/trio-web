﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ReportTitles
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int? Format { get; set; }
        public string Totals { get; set; }
        public string SelectedMonths { get; set; }
        public int? BegMonth { get; set; }
        public int? EndMonth { get; set; }
        public string MajorSequence { get; set; }
        public string LowDetail { get; set; }
        public string SelectedAccounts { get; set; }
        public string BegAccount { get; set; }
        public string EndAccount { get; set; }
        public string BegDeptExp { get; set; }
        public string EndDeptExp { get; set; }
        public bool? CheckMonthRange { get; set; }
        public bool? CheckAccountRange { get; set; }
        public bool? DepartmentBreak { get; set; }
        public bool? DivisionBreak { get; set; }
        public bool? HideExpenseInfo { get; set; }
        public bool? FundBreak { get; set; }
        public bool? ProgramBreak { get; set; }
        public bool? FunctionBreak { get; set; }
        public bool? ObjectBreak { get; set; }
        public string SortOrder { get; set; }
        public bool? MonthlySubtotals { get; set; }
        public bool? ShowZeroBalance { get; set; }
    }
}
