﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class TempCheckFile
    {
        public int Id { get; set; }
        public int? CheckNumber { get; set; }
        public int? VendorNumber { get; set; }
        public string VendorName { get; set; }
        public string CheckAddress1 { get; set; }
        public string CheckAddress2 { get; set; }
        public string CheckAddress3 { get; set; }
        public string CheckAddress4 { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? CheckDate { get; set; }
        public string Journals { get; set; }
        public string Status { get; set; }
        public int? ReportNumber { get; set; }
        public int? WarrantNumber { get; set; }
        public string CheckCity { get; set; }
        public string CheckState { get; set; }
        public string CheckZip { get; set; }
        public string CheckZip4 { get; set; }
        public bool? ReprintedCheck { get; set; }
        public string OldCheckNumber { get; set; }
        public bool? CarryOver { get; set; }
        public bool? PrintedIndividual { get; set; }
        public bool? Eftcheck { get; set; }
    }
}
