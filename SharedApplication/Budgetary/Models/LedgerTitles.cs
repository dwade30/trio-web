﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class LedgerTitle
    {
        public int Id { get; set; }
        public string Fund { get; set; }
        public string Account { get; set; }
        public string Year { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public bool? UseDueToFrom { get; set; }
        public string Tax { get; set; }
        public string GlaccountType { get; set; }
        public string CashFund { get; set; }
        public string PyoverrideCashFund { get; set; }
        public string CroverrideCashFund { get; set; }
        public string ApoverrideCashFund { get; set; }
    }
}
