﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class VendorErrorReportData
	{
		public int VendorNumber { get; set; }
		public string VendorName { get; set; }
		public string TaxNumber { get; set; }
		public string CheckCity { get; set; }
		public string CheckState { get; set; }
		public string CheckZip { get; set; }
	}
}
