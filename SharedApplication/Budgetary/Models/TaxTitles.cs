﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public class TaxTitle
    {
        public int Id { get; set; }
        public int? TaxCode { get; set; }
        public string TaxDescription { get; set; }
        public int? Category { get; set; }
        public string FormName { get; set; }
        public ICollection<VendorTaxInfo> VendorTaxInfos { get; set; }
        public ICollection<VendorTaxInfoArchive> VendorTaxInfoArchives { get; set; }

    }
}
