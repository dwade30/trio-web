﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class StandardAccount
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public int? Row { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Code { get; set; }
        public string Type { get; set; }
    }
}
