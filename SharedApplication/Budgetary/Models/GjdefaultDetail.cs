﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class GjdefaultDetail
    {
        public int Id { get; set; }
        public int? GjdefaultMasterId { get; set; }
        public string Description { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public decimal? Amount { get; set; }
        public string Rcb { get; set; }
    }
}
