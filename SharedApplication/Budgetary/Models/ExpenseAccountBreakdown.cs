﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class ExpenseAccountBreakdown
	{
		public string Department;
		public string Division;
		public string Expense;
		public string Object;
	}
}
