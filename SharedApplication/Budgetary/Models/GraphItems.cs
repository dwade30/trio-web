﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class GraphItems
    {
        public int Id { get; set; }
        public int? CashChartId { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string AccountSelectionType { get; set; }
        public string Low { get; set; }
        public string High { get; set; }
        public string SelectedAccounts { get; set; }
        public string SelectedLiabilities { get; set; }
    }
}
