﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class LedgerDetailSummaryInfo
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public int? Period { get; set; }
        public string Fund { get; set; }
        public string LedgerAccount { get; set; }
        public string Suffix { get; set; }
        public decimal? BudgetAdjustments { get; set; }
        public decimal? OriginalBudget { get; set; }
        public decimal? PostedDebits { get; set; }
        public decimal? PostedCredits { get; set; }
        public decimal? EncumbActivity { get; set; }
        public decimal? PendingDebits { get; set; }
        public decimal? PendingCredits { get; set; }
        public decimal? EncumbranceDebits { get; set; }
        public decimal? EncumbranceCredits { get; set; }
        public decimal? CarryForward { get; set; }
    }
}
