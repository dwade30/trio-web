﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ExpenseReportInfo
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public int? Period { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string Expense { get; set; }
        public string Object { get; set; }
        public decimal? BudgetAdjustments { get; set; }
        public decimal? OriginalBudget { get; set; }
        public decimal? PostedDebits { get; set; }
        public decimal? PostedCredits { get; set; }
        public decimal? EncumbActivity { get; set; }
        public decimal? PendingDebits { get; set; }
        public decimal? PendingCredits { get; set; }
        public decimal? EncumbranceDebits { get; set; }
        public decimal? EncumbranceCredits { get; set; }
        public decimal? MonthlyBudget { get; set; }
        public decimal? MonthlyBudgetAdjustments { get; set; }
        public string Project { get; set; }
        public decimal? AutomaticBudgetAdjustments { get; set; }
        public decimal? PendingEncumbActivity { get; set; }
        public decimal? PendingEncumbranceDebits { get; set; }
        public decimal? CarryForward { get; set; }
    }
}
