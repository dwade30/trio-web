﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
	public class JournalEntryInfo
	{
		public string BudgetaryAccountNumber { get; set; } = "";
		public decimal Amount { get; set; } = 0;
		public string Type { get; set; } = "";
		public string Description { get; set; } = "";
		public int Period { get; set; }
		public DateTime Date { get; set; }
		public string Rcb { get; set; } = "";
		public string Status { get; set; } = "";
	}
}
