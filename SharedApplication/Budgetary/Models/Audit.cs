﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class Audit
    {
        public int Id { get; set; }
        public DateTime? OverrideDate { get; set; }
        public DateTime? OverrideTime { get; set; }
        public string OverrideOperator { get; set; }
        public string OverrideScreen { get; set; }
        public string OverrideType { get; set; }
    }
}
