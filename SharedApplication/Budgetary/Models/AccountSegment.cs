﻿namespace SharedApplication.Budgetary.Models
{
    public class AccountSegment
    {
        public string Value { get; set; } = "";
        public int Order { get; set; } = 0;
        public AccountSegmentType SegmentType { get; set; } = AccountSegmentType.UserDefined;
    }

    public enum AccountSegmentType
    {
        Prefix = 0,
        Fund = 1,
        Account = 2,
        Suffix = 3,
        Department = 4,
        Division = 5,
        Revenue = 6,
        Expense = 7,
        Object = 8,
        UserDefined = 9
    }
}