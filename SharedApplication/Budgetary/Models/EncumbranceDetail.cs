﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class EncumbranceDetail
    {
        public int Id { get; set; }
        public int? EncumbranceId { get; set; }
        public string Description { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Adjustments { get; set; }
        public decimal? Liquidated { get; set; }
    }
}
