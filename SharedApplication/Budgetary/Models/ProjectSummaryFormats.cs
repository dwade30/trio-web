﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ProjectSummaryFormats
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool? CurrentDebitCredit { get; set; }
        public bool? CurrentNet { get; set; }
        public bool? YtddebitCredit { get; set; }
        public bool? Ytdnet { get; set; }
        public bool? Osencumbrance { get; set; }
        public bool? SeperatePending { get; set; }
        public bool? IncludePending { get; set; }
        public string DescriptionLength { get; set; }
        public string Printer { get; set; }
        public string Font { get; set; }
        public bool? Bold { get; set; }
        public bool? Italic { get; set; }
        public bool? StrikeThru { get; set; }
        public bool? Underline { get; set; }
        public string FontName { get; set; }
        public short? FontSize { get; set; }
    }
}
