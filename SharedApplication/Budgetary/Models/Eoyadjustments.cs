﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class EoyAdjustment
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public decimal? Amount { get; set; }
        public int? Year { get; set; }
    }
}
