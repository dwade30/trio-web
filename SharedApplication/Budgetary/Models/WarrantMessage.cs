﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class WarrantMessage
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}
