﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class ProjectDetailFormats
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool? YtddebitCredit { get; set; }
        public bool? Ytdnet { get; set; }
        public bool? PendingDetail { get; set; }
        public bool? PendingSummary { get; set; }
        public string DescriptionLength { get; set; }
        public string Printer { get; set; }
        public string Font { get; set; }
        public bool? Bold { get; set; }
        public bool? Italic { get; set; }
        public bool? StrikeThru { get; set; }
        public bool? Underline { get; set; }
        public string FontName { get; set; }
        public short? FontSize { get; set; }
    }
}
