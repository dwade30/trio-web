﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class APJournalDetailArchive
    {
        public int Id { get; set; }
        public int? ApjournalArchiveId { get; set; }
        public string Description { get; set; }
        public string Account { get; set; }
        public decimal? Amount { get; set; }
        public string Project { get; set; }
        public decimal? Discount { get; set; }
        public decimal? Encumbrance { get; set; }
        public string _1099 { get; set; }
        public string Rcb { get; set; }
        public bool? Corrected { get; set; }
        public int? EncumbranceDetailRecord { get; set; }
        public decimal? CorrectedAmount { get; set; }
        public int? PurchaseOrderDetailsId { get; set; }
        public APjournalArchive APJournalArchive { get; set; }
    }
}
