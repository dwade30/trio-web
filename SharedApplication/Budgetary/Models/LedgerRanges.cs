﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class LedgerRange
    {
        public int Id { get; set; }
        public string RecordName { get; set; }
        public string Low { get; set; }
        public string High { get; set; }
        public string Name1 { get; set; }
        public string Low1 { get; set; }
        public string High1 { get; set; }
        public string Name11 { get; set; }
        public string Low11 { get; set; }
        public string High11 { get; set; }
        public string Name12 { get; set; }
        public string Low12 { get; set; }
        public string High12 { get; set; }
        public string Name13 { get; set; }
        public string Low13 { get; set; }
        public string High13 { get; set; }
        public string Name2 { get; set; }
        public string Low2 { get; set; }
        public string High2 { get; set; }
        public string Name21 { get; set; }
        public string Low21 { get; set; }
        public string High21 { get; set; }
        public string Name22 { get; set; }
        public string Low22 { get; set; }
        public string High22 { get; set; }
        public string Name23 { get; set; }
        public string Low23 { get; set; }
        public string High23 { get; set; }
        public string Name3 { get; set; }
        public string Low3 { get; set; }
        public string High3 { get; set; }
        public string Name31 { get; set; }
        public string Low31 { get; set; }
        public string High31 { get; set; }
        public string Name32 { get; set; }
        public string Low32 { get; set; }
        public string High32 { get; set; }
        public string Name33 { get; set; }
        public string Low33 { get; set; }
        public string High33 { get; set; }
        public string Name4 { get; set; }
        public string Low4 { get; set; }
        public string High4 { get; set; }
        public string Name41 { get; set; }
        public string Low41 { get; set; }
        public string High41 { get; set; }
        public string Name42 { get; set; }
        public string Low42 { get; set; }
        public string High42 { get; set; }
        public string Name43 { get; set; }
        public string Low43 { get; set; }
        public string High43 { get; set; }
    }
}
