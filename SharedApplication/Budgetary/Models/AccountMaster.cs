﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Models
{
    public partial class AccountMaster
    {
        public int ID { get; set; }
        public string Account { get; set; }
        public string AccountType { get; set; }
        public string FirstAccountField { get; set; }
        public string SecondAccountField { get; set; }
        public string ThirdAccountField { get; set; }
        public string FourthAccountField { get; set; }
        public DateTime? DateCreated { get; set; }
        public string Status { get; set; }
        public decimal? CurrentBudget { get; set; }
        public bool? Valid { get; set; }
        public decimal? JanBudget { get; set; }
        public decimal? FebBudget { get; set; }
        public decimal? MarBudget { get; set; }
        public decimal? AprBudget { get; set; }
        public decimal? MayBudget { get; set; }
        public decimal? JuneBudget { get; set; }
        public decimal? JulyBudget { get; set; }
        public decimal? AugBudget { get; set; }
        public decimal? SeptBudget { get; set; }
        public decimal? OctBudget { get; set; }
        public decimal? NovBudget { get; set; }
        public decimal? DecBudget { get; set; }
        public bool? PercentFlag { get; set; }
        public double? JanPercent { get; set; }
        public double? FebPercent { get; set; }
        public double? MarPercent { get; set; }
        public double? AprPercent { get; set; }
        public double? MayPercent { get; set; }
        public double? JunePercent { get; set; }
        public double? JulyPercent { get; set; }
        public double? AugPercent { get; set; }
        public double? SeptPercent { get; set; }
        public double? OctPercent { get; set; }
        public double? NovPercent { get; set; }
        public double? DecPercent { get; set; }
        public string EFM45AccountNumber { get; set; }
        public string EFM45Title { get; set; }
       // public int? ArticleNumber { get; set; }
        public decimal? PreviousYearEndingBalance { get; set; }
        public string FifthAccountField { get; set; }
        public bool? ValidateBalance { get; set; }
        public string SchoolReportingAccount { get; set; }
        public string GLAccountType { get; set; }
    }
}
