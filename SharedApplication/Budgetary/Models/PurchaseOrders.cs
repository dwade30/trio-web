﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class PurchaseOrders
    {
        public int Id { get; set; }
        public int? Po { get; set; }
        public bool? Closed { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public string Comments { get; set; }
        public DateTime? Podate { get; set; }
        public int? VendorNumber { get; set; }
    }
}
