﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class TaxCommitment
    {
        public int Id { get; set; }
        public string TaxType { get; set; }
        public DateTime? ActivityDate { get; set; }
        public decimal? Re { get; set; }
        public decimal? Pp { get; set; }
        public int? Year { get; set; }
    }
}
