﻿namespace SharedApplication.Budgetary.Models
{
    public class BudgetaryFund
    {
        public int Id { get; set; } = 0;
        public string Description { get; set; } = "";
        public string ShortDescription { get; set; } = "";
        public bool UseDueToDueFrom { get; set; } = false;
        public string CashFund { get; set; } = "";
        public string PayrollOverrideCashFund { get; set; } = "";
        public string CashReceiptingOverrideCashFund { get; set; } = "";
        public string AccountsPayableOverrideCashFund { get; set; } = "";
    }
}