﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Budgetary.Models
{
    public partial class LinkedDocuments
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public int? DocReferenceId { get; set; }
        public string DocReferenceType { get; set; }
    }
}
