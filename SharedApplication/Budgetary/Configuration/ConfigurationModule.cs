﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Budgetary.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<AccountTitleService>().As<IAccountTitleService>();
            builder.RegisterType<TaxFormService>().As<ITaxFormService>();
            builder.RegisterType<SelectTaxFormTypeViewModel>().As<ISelectTaxFormTypeViewModel>();
		}
	}
}
