﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary
{
	public class DeptDivTitleSearchCriteria 
	{
		public string Department { get; set; } = "";
		public string Division { get; set; } = "";
	}
}
