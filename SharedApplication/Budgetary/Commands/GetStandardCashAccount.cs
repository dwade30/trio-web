﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Messaging;

namespace SharedApplication.Budgetary.Commands
{
	public class GetStandardCashAccount : Command<string>
	{
		public CashAccountType CashAccountType { get; set; }
	}
}
