﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Messaging;

namespace SharedApplication.Budgetary.Commands
{
	public class CreateJournal : Command<int>
	{
		public string Description { get; set; }
		public JournalType JournalType { get; set; }
		public int Period { get; set; }
	}
}
