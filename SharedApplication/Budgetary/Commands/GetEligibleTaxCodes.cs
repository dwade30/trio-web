﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Messaging;

namespace SharedApplication.Budgetary.Commands
{
    public class GetEligibleTaxCodes :Command<List<int>>
    {
        public TaxFormType FormType { get; set; }
    }
}
