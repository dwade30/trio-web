﻿using SharedApplication.Messaging;

namespace SharedApplication.Budgetary.Commands
{
    public class SelectValidAccount : Command<string>
    {
        public string Account { get; set; }
        public string AccountType { get; set; }
        public SelectValidAccount(string account, string accountType)
        {
            Account = account;
            AccountType = accountType;
        }
    }
}