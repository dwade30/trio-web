﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public class SelectTaxFormTypeViewModel : ISelectTaxFormTypeViewModel
    {
        public TaxFormType ChosenFormType { get; set; }
        public bool CancelSelected { get; set; } = false;
        public bool ShowAllOption { get; set; }

        public List<GenericDescriptionPair<TaxFormType>> FormTypeOptions()
        {
            var result = new List<GenericDescriptionPair<TaxFormType>>();

            if (ShowAllOption)
            {
                result.Add(new GenericDescriptionPair<TaxFormType>() { ID = TaxFormType.All, Description = "All" });
            }
            result.Add(new GenericDescriptionPair<TaxFormType>() { ID = TaxFormType.MISC1099, Description = "1099 MISC"});
            result.Add(new GenericDescriptionPair<TaxFormType>() { ID = TaxFormType.NEC1099, Description = "1099 NEC" });

            return result;
        }
    }
}
