﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public interface ISelectTaxFormTypeViewModel
    {
        TaxFormType ChosenFormType { get; set; }
        bool CancelSelected { get; set; }
        bool ShowAllOption { get; set; }
        List<GenericDescriptionPair<TaxFormType>> FormTypeOptions();
    }
}
