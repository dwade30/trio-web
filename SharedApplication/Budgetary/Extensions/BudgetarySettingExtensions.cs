﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Extensions;

namespace SharedApplication.Budgetary.Extensions
{
	public static class BudgetarySettingExtensions
	{

        public static IGlobalBudgetaryAccountSettings ToGlobalBudgetaryAccountSettings(
            this BudgetarySetting budgetarySettings)
        {
            if (budgetarySettings == null)
            {
                return new GlobalBudgetaryAccountSettings();
            }
            var setting = new GlobalBudgetaryAccountSettings();

            setting.Exp = budgetarySettings.Expenditure;
            setting.Rev = budgetarySettings.Revenue;
            setting.Ledger = budgetarySettings.Ledger;

			if (setting.Exp != "")
			{
				if (setting.Exp.Substring(2, 2).ToIntegerValue() == 0)
				{
					setting.ExpDivFlag = true;
				}
				else
				{
					setting.ExpDivFlag = false;
				}
				if (setting.Exp.Substring(6, 2).ToIntegerValue() == 0)
				{
					setting.ObjFlag = true;
				}
				else
				{
					setting.ObjFlag = false;
				}
			}
			if (setting.Rev != "")
			{
				if (setting.Rev.Substring(2, 2).ToIntegerValue() == 0)
				{
					setting.RevDivFlag = true;
				}
				else
				{
					setting.RevDivFlag = false;
				}
			}
			if (setting.Ledger != "")
			{
				if (setting.Ledger.Substring(4, 2).ToIntegerValue() == 0)
				{
					setting.YearFlag = true;
				}
				else
				{
					setting.YearFlag = false;
				}
			}

			setting.UsePUCChartOfAccounts = budgetarySettings.PUCChartOfAccounts ?? false;
			setting.ValidAcctCheck = (ValidAccountSetting)budgetarySettings.ValidAccountUse.ToIntegerValue();

			return setting;
        }
	}
}
