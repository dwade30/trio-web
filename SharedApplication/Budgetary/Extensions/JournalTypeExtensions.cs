﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary.Extensions
{
	public static class JournalTypeExtensions
	{
		public static string ToJournalTypeString(this JournalType journalType)
		{
			switch (journalType)
			{
				case JournalType.AccountsPayable:
					return "AP";
				case JournalType.AccountsPayableCorrection:
					return "AC";
				case JournalType.CashDisbursements:
					return "CD";
				case JournalType.CashReceipts:
					return "CR";
				case JournalType.Encumbrance:
					return "EN";
				case JournalType.GeneralJournal:
					return "GJ";
				default:
					return "";
			}
		}
	}
}
