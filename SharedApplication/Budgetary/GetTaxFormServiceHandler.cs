﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.Budgetary
{
    public class GetTaxFormServiceHandler : CommandHandler<GetTaxFormService, ITaxFormService>
    {
        private ITaxFormService taxFormService;
        public GetTaxFormServiceHandler(ITaxFormService taxFormService)
        {
            this.taxFormService = taxFormService;
        }

        protected override ITaxFormService Handle(GetTaxFormService command)
        {
            return taxFormService;
        }
    }
}
