﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary
{
    public class ChartOfAccounts
    {
        private List<BudgetaryAccount> accounts = new List<BudgetaryAccount>();
        private List<BudgetaryDepartment> departments = new List<BudgetaryDepartment>();
        private List<BudgetaryFund> funds = new List<BudgetaryFund>();

        public void AddFund(BudgetaryFund fund)
        {
            if (fund != null)
            {
                funds.Add(fund);
            }
        }

        public void AddFunds(IEnumerable<BudgetaryFund> fundRange)
        {
            if (fundRange != null)
            {
                funds.AddRange(fundRange);
            }
        }

        public void AddDepartments(IEnumerable<BudgetaryDepartment> departmentRange)
        {
            if (departmentRange != null)
            {
                departments.AddRange(departmentRange);
            }
        }

        public void AddDepartment(BudgetaryDepartment department)
        {
            if (department != null)
            {
                departments.Add(department);
            }
        }

        public void AddAccount(BudgetaryAccount account)
        {
            if (account != null)
            {
                accounts.Add(account);
            }
        }

        public void AddAccounts(IEnumerable<BudgetaryAccount> accountRange)
        {
            if (accountRange != null)
            {
                accounts.AddRange(accountRange);
            }
        }

        public IEnumerable<BudgetaryAccount> AllAccounts()
        {
            return accounts;
        }

        public IEnumerable<BudgetaryAccount> ValidAccounts()
        {
            return accounts.Where(a => a.IsValid.GetValueOrDefault());
        }

        public IEnumerable<BudgetaryDepartment> AllDepartments()
        {
            return departments;
        }

        public IEnumerable<BudgetaryDepartment> DepartmentsForFund(string fund)
        {
            return departments.Where(d => d.Fund == fund);
        }

        public IEnumerable<BudgetaryFund> AllFunds()
        {
            return funds;
        }
    }
}