﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharedApplication.Budgetary;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;

namespace SharedApplication.Budgetary
{
	public class AccountTitleService : IAccountTitleService
	{
		private IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>> deptDivTitleTypeQueryHandler;
		private IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>> expObjTitleQueryHandler;
		private IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>> revTitleQueryHandler;
		private IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler;
		private IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings;
		private IBudgetaryAccountingService budgetaryAccountingService;

		public AccountTitleService(IQueryHandler<DeptDivTitleSearchCriteria, IEnumerable<DeptDivTitle>> deptDivTitleTypeQueryHandler,
			IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>> expObjTitleQueryHandler,
			IQueryHandler<RevTitleSearchCriteria, IEnumerable<RevTitle>> revTitleQueryHandler,
			IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler,
			IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings,
		    IBudgetaryAccountingService budgetaryAccountingService)
		{
			this.deptDivTitleTypeQueryHandler = deptDivTitleTypeQueryHandler;
			this.expObjTitleQueryHandler = expObjTitleQueryHandler;
			this.revTitleQueryHandler = revTitleQueryHandler;
			this.ledgerTitleQueryHandler = ledgerTitleQueryHandler;
			this.globalBudgetaryAccountSettings = globalBudgetaryAccountSettings;
			this.budgetaryAccountingService = budgetaryAccountingService;
		}
		public string GetDepartmentName(string Department, bool blnLongDescription = false)
		{
			DeptDivTitleSearchCriteria searchCriteria = new DeptDivTitleSearchCriteria();

			searchCriteria.Department = Department;
			searchCriteria.Division = globalBudgetaryAccountSettings.DivisionExistsInExpense()
				? globalBudgetaryAccountSettings.ExpenseZeroDivisionString()
				: "";

			var result = deptDivTitleTypeQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string GetExpDivisionName(string Department, string Division, bool blnLongDescription = false)
		{
			if (!globalBudgetaryAccountSettings.DivisionExistsInExpense())
			{
				return "";
			}

			DeptDivTitleSearchCriteria searchCriteria = new DeptDivTitleSearchCriteria();

			searchCriteria.Department = Department;
			searchCriteria.Division = Division;

			var result = deptDivTitleTypeQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}
		
		public string GetRevDivisionName(string Department, string Division, bool blnLongDescription = false)
		{
			if (!globalBudgetaryAccountSettings.DivisionExistsInRevenue())
			{
				return "";
			}

			DeptDivTitleSearchCriteria searchCriteria = new DeptDivTitleSearchCriteria();

			searchCriteria.Department = Department;
			searchCriteria.Division = Division;

			var result = deptDivTitleTypeQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string GetExpenseName(string Expense, bool blnLongDescription = false)
		{
			ExpObjTitleSearchCriteria searchCriteria = new ExpObjTitleSearchCriteria();

			searchCriteria.Expense = Expense;
			searchCriteria.Object = globalBudgetaryAccountSettings.ObjectZeroString();

			var result = expObjTitleQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string GetObjectName(string Expense, string Object, bool blnLongDescription = false)
		{
			ExpObjTitleSearchCriteria searchCriteria = new ExpObjTitleSearchCriteria();

			searchCriteria.Expense = Expense;
			searchCriteria.Object = Object;

			var result = expObjTitleQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string GetRevenueName(string Department, string Division, string Revenue, bool blnLongDescription = false)
		{
			RevTitleSearchCriteria searchCriteria = new RevTitleSearchCriteria();

			searchCriteria.Department = Department;
			searchCriteria.Division = Division;
			searchCriteria.Revenue = Revenue;

			var result = revTitleQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string GetFundName(string Fund, bool blnLongDescription = false)
		{
			LedgerTitleSearchCriteria searchCriteria = new LedgerTitleSearchCriteria();

			searchCriteria.Fund = Fund;
			searchCriteria.Account = globalBudgetaryAccountSettings.AccountZeroString();
			
			var result = ledgerTitleQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string GetAccountName(string Fund, string Account, string Suffix, bool blnLongDescription = false)
		{
			LedgerTitleSearchCriteria searchCriteria = new LedgerTitleSearchCriteria();

			searchCriteria.Fund = Fund;
			searchCriteria.Account = Account;
			searchCriteria.Suffix = Suffix;

			var result = ledgerTitleQueryHandler.ExecuteQuery(searchCriteria).FirstOrDefault();

			if (result != null)
			{
				if (!blnLongDescription)
				{
					return result.ShortDescription;
				}
				else
				{
					return result.LongDescription;
				}
			}
			else
			{
				return "";
			}
		}

		public string ReturnAccountDescription(string account, bool blnLongDescription = false)
		{
			string TempFirst = "";
			string TempSecond = "";
			string TempThird = "";
			string TempFourth = "";
			string TempFifth = "";
			string strLabel = "";
	
			if (account.Length > 0 && (account.Substring(0, 1).ToUpper() == "M" || account.Contains("_")))
			{
                return "";
            }

			AccountBreakdownInformation accountBreakdown;

			accountBreakdown = budgetaryAccountingService.GetAccountBreakDownInformation(account);

			if (accountBreakdown.AccountType == "E")
			{
				ExpenseAccountBreakdown expenseBreakdown = budgetaryAccountingService.ConvertAccountBreakdownToExpenseAccountBreakdown(accountBreakdown);

				TempFirst = GetDepartmentName(expenseBreakdown.Department, blnLongDescription);
				TempSecond = GetExpDivisionName(expenseBreakdown.Department, expenseBreakdown.Division, blnLongDescription);
				TempThird = GetExpenseName(expenseBreakdown.Expense, blnLongDescription);
				TempFourth = GetObjectName(expenseBreakdown.Expense, expenseBreakdown.Object, blnLongDescription);
			}
			else if (accountBreakdown.AccountType == "R")
			{
				RevenueAccountBreakdown revenueBreakdown = budgetaryAccountingService.ConvertAccountBreakdownToRevenueAccountBreakdown(accountBreakdown);

				TempFirst = GetDepartmentName(revenueBreakdown.Department, blnLongDescription);
				TempSecond = GetRevDivisionName(revenueBreakdown.Department, revenueBreakdown.Division, blnLongDescription);
				TempThird = GetRevenueName(revenueBreakdown.Department, revenueBreakdown.Division, revenueBreakdown.Revenue, blnLongDescription);
				TempFourth = "";
			}
			else if (accountBreakdown.AccountType == "G")
			{
				LedgerAccountBreakdown ledgerBreakdown = budgetaryAccountingService.ConvertAccountBreakdownToLedgerAccountBreakdown(accountBreakdown);

				TempFirst = GetFundName(ledgerBreakdown.Fund, blnLongDescription);
				TempSecond = GetAccountName(ledgerBreakdown.Fund, ledgerBreakdown.Account, ledgerBreakdown.Suffix, blnLongDescription);
				TempThird = "";
				TempFourth = "";
			}
			
			
			if (TempFirst != "" && TempSecond != "")
			{
				strLabel = TempFirst + " / " + TempSecond;
			}
			else
			{
				strLabel = TempFirst;
			}
			if (TempThird == "" && TempFourth == "")
			{
				// do nothing
			}
			else if (TempFourth == "")
			{
				strLabel += " - " + TempThird;
			}
			else if (TempThird == "")
			{
				strLabel += " - " + TempFourth;
			}
			else
			{
				strLabel += " - " + TempThird + " / " + TempFourth;
			}

			return strLabel;
		}
	}
}
