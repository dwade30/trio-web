﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public class WarrantPreviewReportOptions
    {
        public bool EnteredOrder { get; set; } = false;
        public List<int> SelectedJournals { get; set; } = new List<int>();
        public DateTime PayDate { get; set; }
        public WarrantAccountTitleOption AccountTitleOption { get; set; } = WarrantAccountTitleOption.NoAccountTitles;
    }
}
