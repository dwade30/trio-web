﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public class TaxTitleSearchCriteria
    {
        public TaxFormType FormType { get; set; } = TaxFormType.All;
        public int CategoryCode { get; set; } = 0;
        public int TaxCode { get; set; } = 0;
    }
}
