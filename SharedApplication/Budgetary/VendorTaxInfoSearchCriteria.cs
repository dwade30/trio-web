﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Enums;

namespace SharedApplication.Budgetary
{
    public class VendorTaxInfoSearchCriteria
    {
        public int VendorNumber { get; set; } = 0;
        public TaxFormType FormType { get; set; } = TaxFormType.All;
        public int Category { get; set; } = 0;
        public AttorneySearchOption AttorneySelection { get; set; } = AttorneySearchOption.All;
    }
}
