﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary
{
	public class ExpObjTitleSearchCriteria
	{
		public string Expense { get; set; } = "";
		public string Object { get; set; } = "";
	}
}
