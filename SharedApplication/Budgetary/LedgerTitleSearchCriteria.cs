﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary
{
	public class LedgerTitleSearchCriteria
	{
		public string Fund { get; set; } = "";
		public string Account { get; set; } = "";
		public string Suffix { get; set; } = "";
	}
}
