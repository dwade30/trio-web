﻿namespace SharedApplication.Budgetary.BudgetRequests
{
    public enum BudgetRequestType
    {
        Initial = 0,
        Manager = 1,
        Committee = 2,
        Elected = 3,
        Approved = 4
    }

    public static class BudgetRequestTypeExtensions
    {
        public static string ToString(this BudgetRequestType requestType)
        {
            switch (requestType)
            {
                case BudgetRequestType.Approved:
                    return "Approved Amount";
                case BudgetRequestType.Committee:
                    return "Committee Request";
                case BudgetRequestType.Elected:
                    return "Elected Request";
                case BudgetRequestType.Initial:
                    return "Initial Request";
                case BudgetRequestType.Manager:
                    return "Manager Request";
            }

            return "";
        }

        public static string ToCode(this BudgetRequestType requestType)
        {
            switch (requestType)
            {
                case BudgetRequestType.Manager:
                    return "M";
                case BudgetRequestType.Committee:
                    return "C";
                case BudgetRequestType.Elected:
                    return "E";
                case BudgetRequestType.Approved:
                    return "A";
                case BudgetRequestType.Initial:
                    return "I";
            }

            return "";
        }

        public static BudgetRequestType BudgetRequestTypeFromCode(string code)
        {
            switch (code.ToLower())
            {
                case "a":
                    return BudgetRequestType.Approved;
                case "m":
                    return BudgetRequestType.Manager;
                case "c":
                    return BudgetRequestType.Committee;
                case "e":
                    return BudgetRequestType.Elected;
                case "i":
                    return BudgetRequestType.Initial;
            }

            return  BudgetRequestType.Initial;
        }
    }
}