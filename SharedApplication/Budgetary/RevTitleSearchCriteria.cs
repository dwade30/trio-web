﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary
{
	public class RevTitleSearchCriteria
	{
		public string Department { get; set; } = "";
		public string Division { get; set; } = "";
		public string Revenue { get; set; } = "";
	}
}
