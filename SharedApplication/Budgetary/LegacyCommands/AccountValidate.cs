﻿using SharedApplication.Messaging;

namespace SharedApplication.Budgetary.LegacyCommands
{
    public class AccountValidate : Command<bool>
    {
        public string Account { get; set; }
        public bool ValidateMulti { get; set; }

        public AccountValidate(string account, bool validateMulti)
        {
            Account = account;
            ValidateMulti = validateMulti;
        }
    }
}