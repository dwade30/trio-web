﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.Messaging;

namespace SharedApplication.Budgetary
{
    public class TaxFormService : ITaxFormService
    {
        private IQueryHandler<TaxTitleSearchCriteria, IEnumerable<TaxTitle>> taxTitleTypeQueryHandler;
        private IQueryHandler<VendorTaxInfoSearchCriteria, IEnumerable<VendorTaxInfo>> vendorTaxInfoTypeQueryHandler;
        private IQueryHandler<VendorMasterSearchCriteria, IEnumerable<VendorMaster>> vendorMasterQueryHandler;
        private IQueryHandler<AdjustmentsSearchCriteria, IEnumerable<Adjustment>> adjustmentsQueryHandler;
        private IQueryHandler<EditReportDetailSearchCriteria, IEnumerable<EditReportDetailInfo>> editReportDetailQueryHandler;
        private IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>> expObjTitleQueryHandler;
        private IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler;
        private IBudgetaryAccountingService budgetaryAccountingService;
        private CommandDispatcher commandDispatcher;

        public TaxFormService(IQueryHandler<TaxTitleSearchCriteria, IEnumerable<TaxTitle>> taxTitleTypeQueryHandler, IQueryHandler<VendorTaxInfoSearchCriteria, IEnumerable<VendorTaxInfo>> vendorTaxInfoTypeQueryHandler, IQueryHandler<VendorMasterSearchCriteria, IEnumerable<VendorMaster>> vendorMasterQueryHandler, IQueryHandler<AdjustmentsSearchCriteria, IEnumerable<Adjustment>> adjustmentsQueryHandler, IQueryHandler<EditReportDetailSearchCriteria, IEnumerable<EditReportDetailInfo>> editReportDetailQueryHandler, IBudgetaryAccountingService budgetaryAccountingService, IQueryHandler<ExpObjTitleSearchCriteria, IEnumerable<ExpObjTitle>> expObjTitleQueryHandler, IQueryHandler<LedgerTitleSearchCriteria, IEnumerable<LedgerTitle>> ledgerTitleQueryHandler, CommandDispatcher commandDispatcher)
        {
            this.taxTitleTypeQueryHandler = taxTitleTypeQueryHandler;
            this.vendorTaxInfoTypeQueryHandler = vendorTaxInfoTypeQueryHandler;
            this.vendorMasterQueryHandler = vendorMasterQueryHandler;
            this.adjustmentsQueryHandler = adjustmentsQueryHandler;
            this.editReportDetailQueryHandler = editReportDetailQueryHandler;
            this.budgetaryAccountingService = budgetaryAccountingService;
            this.expObjTitleQueryHandler = expObjTitleQueryHandler;
            this.ledgerTitleQueryHandler = ledgerTitleQueryHandler;
            this.commandDispatcher = commandDispatcher;
        }

        public string GetTaxCategoryDescription(TaxFormType type, int categoryCode)
        {
            switch (type)
            {
                case TaxFormType.MISC1099:
                    return TaxForm1099MISCTitles(categoryCode);
                case TaxFormType.NEC1099:
                    return TaxForm1099NECTitles(categoryCode);
                default:
                    return "";
            }
        }

        public decimal GetVendorTaxCategoryTotal(int vendorNumber, TaxFormType type, int categoryCode)
        {
            switch (type)
            {
                case TaxFormType.MISC1099:
                    return GetVendor1099MISCTaxCategoryTotal(vendorNumber, categoryCode);
                case TaxFormType.NEC1099:
                    return GetVendor1099NECTaxCategoryTotal(vendorNumber, categoryCode);
                default:
                    return 0;
            }
        }

        public List<int> GetEligibleVendorNumbers(TaxFormType type, decimal minimumAmount)
        {
            var result = new List<int>();

            result.AddRange(vendorTaxInfoTypeQueryHandler.ExecuteQuery(new VendorTaxInfoSearchCriteria
            {
                FormType = type,
                AttorneySelection = AttorneySearchOption.AttorneyOnly
            }).GroupBy(x => x.VendorNumber ?? 0).Select(y => y.Key));

            result.AddRange(vendorTaxInfoTypeQueryHandler.ExecuteQuery(new VendorTaxInfoSearchCriteria
            {
                FormType = type,
                AttorneySelection = AttorneySearchOption.All
            }).GroupBy(x => x.VendorNumber).Select(y => new
            {
                VendorNumber = y.Key ?? 0,
                VendorTotal = y.Sum(z => z.Amount ?? 0)
            }).Where(z => z.VendorTotal >= minimumAmount).Select(a => a.VendorNumber));

            return result.Distinct().ToList();
        }

        public List<VendorInfo1099Process> GetEligibleVendors(TaxFormType type, decimal minimumAmount)
        {
            var result = GetEligibleVendorNumbers(type, minimumAmount);

            if (result.Count > 0)
            {
                return vendorMasterQueryHandler.ExecuteQuery(new VendorMasterSearchCriteria
                {
                    VendorNumbers = result
                }).Select(y => new VendorInfo1099Process
                {
                    VendorNumber = y.VendorNumber ?? 0,
                    VendorName = (y.C1099UseCorrName ?? false) ? (y.CorrespondName ?? "") : (y.CheckName ?? ""),
                    State = y.CheckState ?? "",
                    City = y.CheckCity ?? "",
                    Zip = (y.CheckZip ?? "") + (y.CheckZip4 ?? "").Trim() != "" ? "-" + (y.CheckZip4 ?? "") : "",
                    Address = y.CheckAddress1 ?? "",
                    TaxIdNumber = y.TaxNumber ?? "",
                    IncludeAllPayments = y.IncludeAllPaymentsFor1099 ?? false,
                    TaxCode = y.C1099Code ?? "",
                    IsAttorney = y.Attorney ?? false,
                }).ToList();
            }
            else
            {
                return new List<VendorInfo1099Process>();
            }
        }

        public List<Adjustment> GetVendorAdjustments(TaxFormType type, int vendorNumber)
        {
            return adjustmentsQueryHandler.ExecuteQuery(new AdjustmentsSearchCriteria
            {
                VendorNumber = vendorNumber,
                FormType = type
            }).ToList();
        }

        public List<EditReportDetailInfo> GetVendorEditReportDetails(TaxFormType type, int vendorNumber, int year, bool includeAllPayments, string taxCodeOverride = "")
        {
            var results = editReportDetailQueryHandler.ExecuteQuery(new EditReportDetailSearchCriteria
            {
                VendorNumber = vendorNumber,
                Override1099Code = taxCodeOverride,
                Year = year,
                IncludeAllPayments = includeAllPayments,
            }).ToList();

            foreach (var result in results)
            {
                if (result.TaxCode == "D")
                {
                    result.TaxCode = GetDefaultTaxCode(result.BudgetaryAccount);
                }
            }

            results.RemoveAll(item => item.TaxCode == "N");

            var eligibleCodes = commandDispatcher.Send(new GetEligibleTaxCodes
            {
                FormType = type
            }).Result;

            results.RemoveAll(x => !eligibleCodes.Any(z => z.ToString() == x.TaxCode));
            return results;
        }

        private string GetDefaultTaxCode(string account)
        {
            var result = "N";

            AccountBreakdownInformation accountBreakdown;

            accountBreakdown = budgetaryAccountingService.GetAccountBreakDownInformation(account);

            if (accountBreakdown.AccountType == "E")
            {
                ExpenseAccountBreakdown expenseBreakdown =
                    budgetaryAccountingService.ConvertAccountBreakdownToExpenseAccountBreakdown(accountBreakdown);

                var taxCodeResult = expObjTitleQueryHandler.ExecuteQuery(new ExpObjTitleSearchCriteria
                {
                    Expense = expenseBreakdown.Expense,
                    Object = expenseBreakdown.Object
                }).FirstOrDefault();

                if (taxCodeResult != null)
                {
                    if (taxCodeResult.Tax.Trim() != "")
                    {
                        result = taxCodeResult.Tax.Trim();
                    }
                }
            }
            else if (accountBreakdown.AccountType == "G")
            {
                LedgerAccountBreakdown ledgerBreakdown =
                    budgetaryAccountingService.ConvertAccountBreakdownToLedgerAccountBreakdown(accountBreakdown);

                var taxCodeResult = ledgerTitleQueryHandler.ExecuteQuery(new LedgerTitleSearchCriteria
                {
                    Account = ledgerBreakdown.Account,
                    Fund = ledgerBreakdown.Fund,
                    Suffix = ledgerBreakdown.Suffix
                }).FirstOrDefault();

                if (taxCodeResult != null)
                {
                    if (taxCodeResult.Tax.Trim() != "")
                    {
                        result = taxCodeResult.Tax.Trim();
                    }
                }
            }
            else
            {
                result = "N";
            }

            return result;
        }

        private decimal GetVendor1099MISCTaxCategoryTotal(int vendorNumber, int categoryCode)
        {
            var results = vendorTaxInfoTypeQueryHandler.ExecuteQuery(new VendorTaxInfoSearchCriteria
            {
                VendorNumber = vendorNumber,
                FormType = TaxFormType.MISC1099,
                Category = categoryCode
            });

            return results.Sum(x => x.Amount ?? 0);
        }

        private decimal GetVendor1099NECTaxCategoryTotal(int vendorNumber, int categoryCode)
        {
            return vendorTaxInfoTypeQueryHandler.ExecuteQuery(new VendorTaxInfoSearchCriteria
            {
                VendorNumber = vendorNumber,
                FormType = TaxFormType.NEC1099,
                Category = categoryCode
            }).Sum(x => x.Amount ?? 0);
        }

        private string TaxForm1099MISCTitles(int categoryCode)
        {
            switch (categoryCode)
            {
                case 1:
                    return "Rents";
                case 2:
                    return "Royalties";
                case 3:
                    return "Other Income";
                case 4:
                    return "Federal Income Tax Withheld";
                case 5:
                    return "Fishing Boat Proceeds";
                case 6:
                    return "Medical and Health Care Payments";
                case 14:
                    return "Gross Proceeds Paid to an Attorney";
                default:
                    return "";
            }
        }

        private string TaxForm1099NECTitles(int categoryCode)
        {
            switch (categoryCode)
            {
            case 1:
                return "Nonemployee Compensation";
            case 4:
                return "Federal Income Tax Withheld";
            default:
                return "";
            }
        }

        public string GetTaxCodeDescription(int taxCode)
        {
            var result = taxTitleTypeQueryHandler.ExecuteQuery(new TaxTitleSearchCriteria
            {
                TaxCode = taxCode
            }).FirstOrDefault();

            if (result != null)
            {
                return result.TaxDescription.Trim();
            }
            else
            {
                return "UNKNOWN";
            }
        }


    }
}
