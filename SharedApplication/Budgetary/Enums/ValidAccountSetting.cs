﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Enums
{
	public enum ValidAccountSetting
	{
		NotSet = 0,
		ValidAccountsOnly = 1,
		ValidOrInvalidAccounts = 2,
		AnyExistingAccount = 3
	}
}
