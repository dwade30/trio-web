﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Enums
{
    public enum TaxFormType
    {
        All,
        MISC1099,
        NEC1099
    }
}
