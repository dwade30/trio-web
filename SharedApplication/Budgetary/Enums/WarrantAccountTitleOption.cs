﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Enums
{
    public enum WarrantAccountTitleOption
    {
        NoAccountTitles = 1,
        PrintFullTitles = 2,
        PrintDeptDivFundOnly = 3,
        PrintExpObjLedgerAccountOnly = 4
    }
}
