﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Budgetary.Enums
{
	public enum BudgetarySecurityItems
	{
		ProgramAccess = 1,
		Journals = 2,
		AccountsPayable = 3,
		CashDisbursements = 4,
		CashReceipts = 5,
		GeneralJournal = 6,
		AccountsPayableCorrections = 7,
		Encumbrances = 8,
		EncumbranceCorrections = 9,
		AccountsPayableProcessing = 12,
		Posting = 13,
		Printing = 14,
		ReportWriter = 16,
		Vendors = 17,
		VendorMaintenance = 18,
		ClassCodes = 19,
		Clear1099Adjustments = 20,
		AccountSetup = 21,
		BudgetProcess = 22,
		ClearBudget = 23,
		BudgetBreakdown = 24,
		TransferApprovedToBudget = 25,
		LoadBudget = 26,
		EndOfYear = 27,
		TaxProcessing = 28,
		RestoreArchive = 29,
		DeleteUnpostedJournal = 30,
		BankReconcilliation = 31,
		AddCheckOrDeposit = 32,
		FlagCashed = 33,
		PurgeChecks = 34,
		UpdateStatusCheckOrDeposit = 35,
		FileMaintenance = 36,
		WarrantMessage = 37,
		DefaultAccounts = 38,
		LedgerRanges = 39,
		Customize = 40,
		ValidAccounts = 41,
		CustomReportSetup = 43,
		AccountsPayableOverSpend = 44,
		CreditMemo = 45,
		PurchaseOrders = 47,
		EditJournalDescriptions = 48,
		CreateReversingEntries = 49,
		CreateOpeningAdjustments = 50,
		CreditMemoCorrection = 51,
		Payables = 52,
		Accounts = 53,
		Projects = 54,
		EndofYearMenu = 55



	//	public const int TAXPROCESSING = 28;
	//public const int RESTOREARCHIVE = 29;
	//public const int ACCOUNTSETUP = 21;
	//public const int ACCOUNTSPAYABLEPROCESSING = 12;
	//public const int APOVERSPEND = 44;
	//public const int BUDGETPROCESS = 22;
	//public const int BUDGETBREAKDOWN = 24;
	//public const int CLEARBUDGETINFORMATION = 23;
	//public const int LOADBUDGETINFO = 26;
	//public const int TRANSFERAPPROVEDTOBUDGET = 25;
	//public const int CHECKRECONCILIATION = 31;
	//public const int ADDCHECKORDEPOSIT = 32;
	//public const int FLAGCASHED = 33;
	//public const int PurgeChecks = 34;
	//public const int UPDATESTATUSCHECKORDEPOSIT = 35;
	//public const int DATAENTRY = 2;
	//public const int APCORRECTIONS = 7;
	//public const int ACCOUNTSPAYABLE = 3;
	//public const int BUILDTAXCOMMITMENT = 10;
	//public const int BUILDUNFINISHEDJOURNALS = 11;
	//public const int CASHDUSBURSEMENTS = 4;
	//public const int CASHRECEIPTS = 5;
	//public const int ENCUMBRANCES = 8;
	//public const int GENERALJOURNAL = 6;
	//public const int CreditMemo = 45;
	//public const int CREDITMEMOCORRECTION = 51;
	//public const int CreateOpeningAdjustments = 50;
	//public const int CREATEREVERSINGJOURNAL = 49;
	//public const int EDITJOURNALDESCRIPTIONS = 48;
	//public const int PURCHASEORDERS = 47;
	//public const int UPDATEOSENCUMBRANCES = 9;
	//public const int DeleteUnpostedJournal = 30;
	//public const int ENDOFYEAR = 27;
	//public const int FILEMAINTENANCE = 36;
	//public const int ADDUPDATEVALIDACCOUNT = 41;
	//public const int CUSTOMREPORTSETUP = 43;
	//public const int CUSTOMIZE = 40;
	//public const int GLCONTROLACCOUNTS = 38;
	//public const int GLSUBTOTALRANGES = 39;
	//public const int REVENUETOTALRANGES = 42;
	//public const int WARRANTMESSAGE = 37;
	//public const int POSTING = 13;
	//public const int PRINTING = 14;
	//public const int CASHCHARTING = 15;
	//public const int REPORTWRITER = 16;
	//public const int VENDORFILEROUTINES = 17;
	//public const int CLASSCODES = 19;
	//public const int CLEAR1099ADJUSTMENTS = 20;
	//public const int VENDORMAINTENANCE = 18;
	//public const int PAYABLES = 52;
	//public const int ACCOUNTS = 53;
	//public const int PROJECTS = 54;
	}
}
