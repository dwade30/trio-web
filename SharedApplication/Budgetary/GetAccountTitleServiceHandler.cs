﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Commands;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.Budgetary
{
    public class GetAccountTitleServiceHandler : CommandHandler<GetAccountTitleService, IAccountTitleService>
    {
        private IAccountTitleService accountTitleService;
        public GetAccountTitleServiceHandler(IAccountTitleService accountTitleService)
        {
            this.accountTitleService = accountTitleService;
        }

        protected override IAccountTitleService Handle(GetAccountTitleService command)
        {
            return accountTitleService;
        }
    }
}
