﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
	public interface IInterestCalculationService
	{
		double IPmt(double rate, int paymentPeriod, int numberOfPayments, double amount);
		double Pmt(double rate, int numberOfPayments, double amount);
		double FV(double rate, int numberOfPayments, double payment, double amount);
	}
}
