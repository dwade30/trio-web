﻿using SharedApplication.Messaging;
using SharedApplication.Models;

namespace SharedApplication.FixedAssets
{
    public class UpdateFixedAssetsDatabase : Command<bool>
    {
        public SQLConfigInfo SqlConfigInfo { get; }
        public string DataEnvironment { get; }

        public UpdateFixedAssetsDatabase(SQLConfigInfo sqlConfigInfo, string dataEnvironment)
        {
            DataEnvironment = dataEnvironment;
            SqlConfigInfo = sqlConfigInfo;
        }
    }
}