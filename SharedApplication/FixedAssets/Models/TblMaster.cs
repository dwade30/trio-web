﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblMaster
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string TagNumber { get; set; }
        public int? Location1Code { get; set; }
        public string Location1Desc { get; set; }
        public int? Location2Code { get; set; }
        public string Location2Desc { get; set; }
        public int? Location3Code { get; set; }
        public string Location3Desc { get; set; }
        public int? Location4Code { get; set; }
        public string Location4Desc { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public string VendorCode { get; set; }
        public DateTime? WarrantyExpDate { get; set; }
        public string WarrantyDescription { get; set; }
        public bool? FederalFunds { get; set; }
        public bool? ExpensedFirstYear { get; set; }
        public string ClassCode { get; set; }
        public decimal? BasisReduction { get; set; }
        public decimal? TotalDepreciated { get; set; }
        public double? BusinessUsePercent { get; set; }
        public decimal? BasisCost { get; set; }
        public decimal? SalvageValue { get; set; }
        public string Acct1 { get; set; }
        public string Acct2 { get; set; }
        public string Acct3 { get; set; }
        public string Acct4 { get; set; }
        public short? DepreciationMethod { get; set; }
        public double? Dbrate { get; set; }
        public string Unrate { get; set; }
        public DateTime? DateLastDepreciation { get; set; }
        public int? TotalUsed { get; set; }
        public int? Life { get; set; }
        public string LifeUnits { get; set; }
        public string TotalPosted { get; set; }
        public string Dept { get; set; }
        public string Div { get; set; }
        public DateTime? OutOfServiceDate { get; set; }
        public double? TotalValue { get; set; }
        public string ManualVendor { get; set; }
        public string ManualClassCode { get; set; }
        public string ChangedToSl { get; set; }
        public DateTime? OldDepreciationDate { get; set; }
        public short? RemainingLife { get; set; }
        public double? AdjustedBasisCost { get; set; }
        public string Comments { get; set; }
        public double? Improvements { get; set; }
        public double? ReplacementCost { get; set; }
        public string Condition { get; set; }
        public bool? Discarded { get; set; }
        public DateTime? DiscardedDate { get; set; }
    }
}
