﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblDepreciation
    {
        public int Id { get; set; }
        public int? ItemId { get; set; }
        public DateTime? DepreciationDate { get; set; }
        public decimal? DepreciationAmount { get; set; }
        public string DepreciationUnits { get; set; }
        public string DepreciationDesc { get; set; }
        public bool? Pending { get; set; }
        public int? ReportId { get; set; }
        public string ChangedToSl { get; set; }
        public DateTime? OldDepreciationDate { get; set; }
    }
}
