﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblSettings
    {
        public int Id { get; set; }
        public bool? RefreshSeachOnKeyPress { get; set; }
        public bool? DeptDiv { get; set; }
        public bool? DivisionUsed { get; set; }
        public bool? Vendors { get; set; }
        public string Location1Caption { get; set; }
        public string Location2Caption { get; set; }
        public string Location3Caption { get; set; }
        public string Location4Caption { get; set; }
        public bool? UpdatedDeptDiv { get; set; }
        public string Version { get; set; }
    }
}
