﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblDeptDiv
    {
        public int Id { get; set; }
        public string Dept { get; set; }
        public string Div { get; set; }
        public string Description { get; set; }
    }
}
