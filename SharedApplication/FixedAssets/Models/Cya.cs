﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class Cya
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public DateTime? Cyadate { get; set; }
        public DateTime? Cyatime { get; set; }
        public string Description1 { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Description4 { get; set; }
        public string Description5 { get; set; }
        public bool? BoolUseSecurity { get; set; }
    }
}
