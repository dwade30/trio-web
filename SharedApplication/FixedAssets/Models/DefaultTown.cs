﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class DefaultTown
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public short? County { get; set; }
    }
}
