﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblDepreciationMaster
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string DepreciationType { get; set; }
    }
}
