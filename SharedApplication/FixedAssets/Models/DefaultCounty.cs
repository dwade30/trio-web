﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class DefaultCounty
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
