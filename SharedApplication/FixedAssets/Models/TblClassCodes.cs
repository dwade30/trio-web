﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblClassCodes
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Life { get; set; }
    }
}
