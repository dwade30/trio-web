﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class States
    {
        public int Id { get; set; }
        public short? ConcurrencyId { get; set; }
        public string Description { get; set; }
        public string LastUserId { get; set; }
        public DateTime? LastUpdate { get; set; }
        public string State { get; set; }
    }
}
