﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblLocations
    {
        public int Id { get; set; }
        public int? TypeId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
