﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class TblVendors
    {
        public int Id { get; set; }
        public int? Code { get; set; }
        public string Description { get; set; }
    }
}
