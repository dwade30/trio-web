﻿using System;
using System.Collections.Generic;

namespace SharedApplication.FixedAssets.Models
{
    public partial class Condition
    {
        public int Id { get; set; }
        public string Condition1 { get; set; }
        public double? DepreciationPercentage { get; set; }
    }
}
