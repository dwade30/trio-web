﻿using System;

namespace SharedApplication
{
    public class EffectiveDateChangeViewModel : IEffectiveDateChangeViewModel
    {
        public DateTime EffectiveDate { get; set; }
        public bool UpdateRecordedDate { get; set; } = false;
        public bool AllowChangingRecordedDate { get; set; } = false;

        public bool Cancelled { get; set; } = false;
        public void SetEffectiveDate(EffectiveDateChoice choice)
        {
            EffectiveDate = choice.EffectiveDate;
            UpdateRecordedDate = choice.UpdateRecordedDate;
            Cancelled = false;
        }

        public void Cancel()
        {
            Cancelled = true;
        }

    }
}