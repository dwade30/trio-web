﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;

namespace SharedApplication
{
	public interface ISettingService
	{
		Setting GetSettingValue(SettingOwnerType type, string settingName);
        Setting GetSettingValue(SettingOwnerType type, string settingType, string settingName);
		void SaveSetting(SettingOwnerType type, string name, string value);
        void SaveSetting(SettingOwnerType type, string settingType, string name, string value);
    }
}
