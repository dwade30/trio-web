﻿using SharedApplication.AccountsReceivable;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CentralData;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralParties;
using SharedApplication.Clerk;
using SharedApplication.ClientSettings.Interfaces;
using SharedApplication.MotorVehicle;
using SharedApplication.Payroll.Interfaces;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.TaxCollections;
using SharedApplication.UtilityBilling;

namespace SharedApplication
{
    public interface ITrioContextInterfaceFactory
    {
        ITaxCollectionsContext GetTaxCollectionsContextInterface();
        IRealEstateContext GetRealEstateContextInterface();
        IPersonalPropertyContext GetPersonalPropertyContextInterface();
        ISystemSettingsContext GetSystemSettingsContextInterface();
        IClientSettingsContext GetClientSettingsContextInterface();
        IUtilityBillingContext GetUtilityBillingContextInterface();
        IAccountsReceivableContext GetAccountsReceivableContextInterface();
        ICashReceiptsContext GetCashReceiptsContextInterface();
        ICentralDataContext GetCentralDataContextInterface();
        ICentralDocumentContext GetCentralDocumentContextInterface();
        IClerkContext GetClerkContextInterface();
        ICentralPartyContext GetCentralPartyContextInterface();
        IBudgetaryContext GetBudgetaryContextInterface();
        IMotorVehicleContext GetMotorVehicleContextInterface();
        IPayrollContext GetPayrollContextInterface();
    }
}