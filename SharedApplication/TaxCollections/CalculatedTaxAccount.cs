﻿using System.Collections.Generic;

namespace SharedApplication.TaxCollections
{
    public class CalculatedTaxAccount
    {
        public PropertyTaxAccount TaxAccount { get; set; }
        public TaxBillCalculationSummary AccountSummary { get; set; }
        public Dictionary<int, TaxBillCalculationSummary> CalculationSummaries { get; } = new Dictionary<int, TaxBillCalculationSummary>();
    }
}