﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Queries;

namespace SharedApplication.TaxCollections
{
    public class RealEstateTaxAccountSearchViewModel : IRealEstateTaxAccountSearchViewModel
    {
        private CommandDispatcher commandDispatcher;
        private IQueryHandler<RealEstateTaxAccountSearch, IEnumerable<TaxAccountSearchResult>> searchHandler;
        private List<TaxAccountSearchResult> searchResults = new List<TaxAccountSearchResult>();
        public IEnumerable<TaxAccountSearchResult> SearchResults
        {
            get => searchResults;
        }
        public RealEstateTaxAccountSearchViewModel(CommandDispatcher commandDispatcher, IQueryHandler<RealEstateTaxAccountSearch, IEnumerable<TaxAccountSearchResult>> searchHandler)
        {
            this.commandDispatcher = commandDispatcher;
            this.searchHandler = searchHandler;
        }
        public int SelectedAccount { get; set; } = 0;
        public void Search(string name, string maplot)
        {
            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(maplot))
            {
                searchResults.Clear();
                return;
            }
            searchResults = searchHandler.ExecuteQuery(new RealEstateTaxAccountSearch(name,maplot) ).ToList();
        }

        public void SelectAccount(int account)
        {
            SelectedAccount = account;
        }

        public void Cancel()
        {
            SelectedAccount = 0;
        }
    }
}