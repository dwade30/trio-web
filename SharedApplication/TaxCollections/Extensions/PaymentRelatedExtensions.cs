﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Extensions
{
    public static class PaymentRelatedExtensions
    {
        public static IEnumerable<PaymentRec> WithoutAbatements(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(o => o.Code != "A");
        }

        public static IEnumerable<PaymentRec> Abatements(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => p.Code == "A");
        }
        public static PaymentSummary SummarizeAsPaymentSummary(this IEnumerable<PaymentRec> payments)
        {
            return payments.Aggregate(new PaymentSummary(), (s1, s2) => new PaymentSummary() { Principal = s1.Principal + s2.Principal.Value, Costs = s1.Costs + s2.LienCost.Value, Interest = s1.Interest + s2.PreLienInterest.Value + s2.CurrentInterest.Value });
        }

        public static ReceiptTotals SummarizeAsReceiptTotals(this IEnumerable<PaymentRec> payments)
        {
            return payments.Aggregate(new ReceiptTotals(), (s1, s2) => new ReceiptTotals() { Principal = s1.Principal + s2.Principal.GetValueOrDefault(), LienCost = s1.LienCost + s2.LienCost.GetValueOrDefault(), CurrentInterest = s1.CurrentInterest + s2.CurrentInterest.GetValueOrDefault(), PreLienInterest = s1.PreLienInterest + s2.PreLienInterest.GetValueOrDefault()});
        }
        
        public static Payments ToPayments(this PaymentRec paymentRec)
        {
            if (paymentRec == null)
            {
                return new Payments();
            }
            var payments = new Payments();
            payments.AddPayment(paymentRec);
            return payments;
        }

        public static PaymentRec LastPaymentById(this IEnumerable<PaymentRec> payments)
        {
            if (payments == null)
            {
                return null;
            }

            return payments.OrderByDescending(p => p.Id).FirstOrDefault();
        }

        public static PaymentRec LastPaymentByRecordedTransactionDate(this IEnumerable<PaymentRec> payments)
        {
            if (payments == null)
            {
                return null;
            }

            return payments.OrderByDescending(p => p.RecordedTransactionDate).FirstOrDefault();
        }

        public static Payments ToPayments(this IEnumerable<PaymentRec> payments)
        {
            if (payments == null)
            {
                return new Payments();
            }
            var paymentObject = new Payments();
            paymentObject.AddPayments(payments);
            return paymentObject;
        }

        public static Payments ToNonZeroPayments(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p =>
                p.PreLienInterest != 0 || p.Principal != 0 || p.LienCost != 0 || p.CurrentInterest != 0).ToPayments();
        }

        public static IEnumerable<PaymentRec> InterestChargedPayments(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => p.Code == "I");
        }

        public static decimal TotalChargedInterest(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => p.IsChargedInterest()).Aggregate(0M, (total, p) => total += p.CurrentInterest ?? 0) * -1;
        }


        public static decimal TotalCostsCharged(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => p.IsDemandFeeCharge() || p.IsLienCostCharge()).Aggregate(0M, (total, p) => total += p.LienCost ?? 0) * -1;
        }

        public static decimal TotalCostsPaid(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => !p.IsDemandFeeCharge() && !p.IsLienCostCharge() && !p.IsAbatement()).Aggregate(0M, (total, p) => total += p.LienCost ?? 0);
        }

        public static decimal TotalPrincipalPaid(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => !p.IsAbatement()).Aggregate(0M, (total, p) => total += p.Principal ?? 0);
        }

        public static decimal TotalInterestPaid(this IEnumerable<PaymentRec> payments)
        {
            return payments.Where(p => !p.IsAbatement() && !p.IsCreatedInterest())
                .Aggregate(0M, (total, p) => total += p.CurrentInterest ?? 0);
        }
    }
}