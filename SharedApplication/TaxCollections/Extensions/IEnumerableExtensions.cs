﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<DescriptionIDPair> MapToDescriptionIDPairsShowingIDAndDate(
            this IEnumerable<RateRec> rateRecords)
        {
            return rateRecords.Select(rateRec => new DescriptionIDPair()
            {
                Description = rateRec.Id + " " + rateRec.RateType + " " + (rateRec.BillingDate.HasValue ? rateRec.BillingDate.Value.ToShortDateString() : ""),
                ID = rateRec.Id
            }).ToList();
        }
    }
}
