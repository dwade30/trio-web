﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Extensions
{
    public static class ModelConversionExtensions
    {
        public static TaxRateInfo ToTaxRateInfo(this RateRec rateRec)
        {
            if (rateRec == null)
            {
                return null;
            }
            var taxRateInfo = new TaxRateInfo()
            {
                LienDate = rateRec?.LienDate ?? DateTime.MinValue,
                BillingDate =  rateRec?.BillingDate ?? DateTime.MinValue,
                CommitmentDate = rateRec?.CommitmentDate ?? DateTime.MinValue,
                Description = rateRec?.Description,
                InterestRate = Convert.ToDecimal(rateRec?.InterestRate ?? 0),
                id = rateRec?.Id??0,
                OverPayRate = 0,
                TaxRate = Convert.ToDecimal(rateRec?.TaxRate ??0),
                Year = rateRec.Year ?? 0,
                CreationDate = ((rateRec?.CreationDate?.Date?? new DateTime(1899,12,30) )== new DateTime(1899, 12, 30)) ? DateTime.MinValue : rateRec?.CreationDate ?? DateTime.MinValue
            };
            switch (rateRec?.RateType)
            {
                case "L":
                    taxRateInfo.RateType = PropertyTaxRateType.Lien;
                    break;
                case "S":
                    taxRateInfo.RateType = PropertyTaxRateType.Supplemental;
                    break;
                default:
                    taxRateInfo.RateType = PropertyTaxRateType.Regular;
                    break;
            }
            taxRateInfo.Installments.Add(new InstallmentInfo()
            {
                DueDate = rateRec?.DueDate1 ?? DateTime.MinValue,
                InstallmentNumber = 1,
                InterestStartDate = rateRec?.InterestStartDate1 ?? DateTime.MinValue,
                Weight = rateRec.Weight1 ?? 0
            });
            if (rateRec.NumberOfPeriods > 1)
            {
                taxRateInfo.Installments.Add(new InstallmentInfo()
                {
                    DueDate = rateRec?.DueDate2 ?? DateTime.MinValue,
                    InstallmentNumber = 2,
                    InterestStartDate = rateRec?.InterestStartDate2 ?? DateTime.MinValue,
                    Weight = rateRec?.Weight2 ?? 0
                });
            }

            if (rateRec?.NumberOfPeriods > 2)
            {
                taxRateInfo.Installments.Add(new InstallmentInfo()
                {
                    DueDate = rateRec?.DueDate3 ?? DateTime.MinValue,
                    InstallmentNumber =  3,
                    InterestStartDate = rateRec?.InterestStartDate3 ?? DateTime.MinValue,
                    Weight = rateRec?.Weight3 ?? 0
                });
            }

            if (rateRec?.NumberOfPeriods > 3)
            {
                taxRateInfo.Installments.Add(new InstallmentInfo()
                {
                    DueDate = rateRec?.DueDate4 ?? DateTime.MinValue,
                    InstallmentNumber = 4,
                    InterestStartDate = rateRec?.InterestStartDate4 ?? DateTime.MinValue,
                    Weight = rateRec?.Weight4 ?? 0
                });
            }

            return taxRateInfo;
        }

        public static IEnumerable<TaxRateInfo> MapToTaxRateInfos(this IEnumerable<RateRec> rateRecords)
        {
            return rateRecords.Select(ToTaxRateInfo).ToList();
        }
    }
}