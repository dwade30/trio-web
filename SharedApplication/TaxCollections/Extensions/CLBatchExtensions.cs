﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Extensions
{
    public static class CLBatchExtensions
    {
        public static TaxCollectionsBatch ToCollectionsBatch(this BatchRecover item)
        {
            var batch = new TaxCollectionsBatch();
            batch.BatchIdentifier = item.BatchIdentifier.GetValueOrDefault();
            batch.BatchRecoverDate = item.BatchRecoverDate.GetValueOrDefault();
            batch.TellerID = item.TellerId;
            batch.PaidBy = item.PaidBy;
            batch.EffectiveDate = item.Etdate.GetValueOrDefault();
            batch.BillType = item.BillingType == "PP" ? PropertyTaxBillType.Personal : PropertyTaxBillType.Real;
            batch.DefaultTaxYear = item.Year
                .GetValueOrDefault();
            return batch;
        }
        //public static TaxCollectionsBatch ToCollectionsBatch(this IEnumerable<BatchRecover> batchItems)
        //{
        //    if (batchItems == null )
        //    {
        //        return new TaxCollectionsBatch();
        //    }
        //    var items = batchItems.ToList();
        //    if (!items.Any())
        //    {
        //        return new TaxCollectionsBatch();
        //    }
        //    var batch = new TaxCollectionsBatch();
        //    batch.BatchItems.AddRange(items);
        //    var item = items.FirstOrDefault();
        //    batch.BatchIdentifier = item.BatchIdentifier.GetValueOrDefault();
        //    batch.BatchRecoverDate = item.BatchRecoverDate.GetValueOrDefault();
        //    batch.TellerID = item.TellerId;
        //    batch.PaidBy = item.PaidBy;
        //    batch.EffectiveDate = item.Etdate.GetValueOrDefault();
        //    batch.BillType = item.BillingType == "PP" ? PropertyTaxBillType.Personal : PropertyTaxBillType.Real;
        //    batch.DefaultTaxYear = items.OrderByDescending(i => i.Year).FirstOrDefault().Year
        //        .GetValueOrDefault();
        //    return batch;
        //}

        public static TaxCollectionsBatch ToCollectionsBatch(this CLBatchInputSetup inputSetup)
        {
            return new TaxCollectionsBatch()
            {
                BatchIdentifier = Guid.NewGuid(),
                BatchRecoverDate = inputSetup.BatchRecoverDate,
                DefaultTaxYear = inputSetup.DefaultTaxYear,
                EffectiveDate = inputSetup.EffectiveDate,
                PaidBy = inputSetup.PaidBy,
                Period = inputSetup.Period,
                TellerID = inputSetup.TellerID
            };
        }

        public static CLBatchInputSetup ToBatchDefault(this TaxCollectionsBatch batch)
        {
            return new CLBatchInputSetup()
            {
                BatchRecoverDate = batch.BatchRecoverDate,
                EffectiveDate = batch.EffectiveDate,
                PaidBy = batch.PaidBy,
                TellerID = batch.TellerID,
                DefaultTaxYear = batch.DefaultTaxYear,
                Period = batch.Period
            };
        }
    }
}