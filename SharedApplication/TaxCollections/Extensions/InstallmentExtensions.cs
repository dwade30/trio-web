﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Extensions
{
    public static class InstallmentExtensions
    {
        public static PropertyTaxPaymentInstallment ToPropertyTaxPaymentInstallment(
            this PropertyTaxInstallment installment)
        {
            return new PropertyTaxPaymentInstallment(){AdjustedAmount = installment.AdjustedAmount,DueDate = installment.DueDate,InstallmentNumber = installment.InstallmentNumber,InterestDate = installment.InterestDate,OriginalAmount = installment.OriginalAmount};
        }

        public static PropertyTaxPaymentInstallments ToPropertyTaxPaymentInstallments(
            this IEnumerable<PropertyTaxInstallment> installments)
        {
            if (installments != null)
            {
                return new PropertyTaxPaymentInstallments( installments.Select(i => i.ToPropertyTaxPaymentInstallment()));
            }
            return new PropertyTaxPaymentInstallments();
        }
    }
}