﻿using System;
using SharedApplication.Enums;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections
{
    public enum TaxBillingHardCodedReport
    {
        NonZeroBalanceOnNonLienAccounts = 0,
        NonZeroBalanceOnLienAccounts = 1,
        NonZeroBalanceOnAllAccounts = 2,
        LienBreakdown = 3,
        ZeroBalanceReport = 4,
        NegativeBalanceReport = 5,
        SupplementalOutstandingBalanceReport = 6,
        SupplementalNegativeBalanceReport = 7,
        SupplementalZeroBalanceReport = 8,
        OutstandingBalanceByPeriod = 9,
        AccountDetailReport = 10,
        AuditSummaryReport = 11,
        SupplementalBills = 12,
        NonZeroBalanceOnAllAccountsByYear = 13,
        BankruptAccounts = 14
    }

    public enum TaxBillStatusType
    {
        Regular = 0,
        Lien = 1,
        PreLien = 2
    }

    public enum TaxReportAccountSortType
    {
        Account = 0,
        Name = 1,
        Year = 2
    }

    public enum TaxReportNameOption
    {
        CurrentOwner = 0,
        BilledOwner = 1,
        BilledOwnerCareOfCurrentOwner = 2,
        CurrentOwnerBilledAsOwner = 3
    }

    public enum TaxAcquiredOptionType
    {
        None = 0,
        TaxAcquired = 1,
        NonTaxAcquired = 2
    }
    public class TaxCollectionReportOption
    {
        public TaxReportNameOption NameToShow { get; set; } = TaxReportNameOption.BilledOwner;
        public bool ShowCurrentInterest { get; set; } = false;
        public bool ShowPayments { get; set; } = false;
        public bool ShowSummaryOnly { get; set; } = false;
        public bool ExcludePaid { get; set; } = false;
        public bool UseDefaultReport { get; set; } = false;
        public string ReportType { get; set; } = "Account";
        public bool ShowAddress { get; set; } = false;
        public bool ShowMapLot { get; set; } = false;
        public bool ShowLocation { get; set; } = false;
        public DateTime AsOfDate { get; set; }
        public bool FullStatusAmounts { get; set; } = false;
        public short Period { get; set; } = 4;
        public bool UseAsOfDate()
        {
            if (AsOfDate != DateTime.MinValue && AsOfDate.Date.CompareTo(DateTime.Today) != 0)
            {
                return true;
            }

            return false;
        }
        public bool PreLienOnly { get; set; } = false;

        public bool ShowPersonalPropertyInformation { get; set; } = false;
        //public List<String> ExtraFields { get; private set; } = new List<string>();
        public TaxBillingHardCodedReport DefaultReportType { get; set; } =
            TaxBillingHardCodedReport.NonZeroBalanceOnAllAccounts;

        public bool DefaultReportTypeIsSupplemental()
        {
            if (DefaultReportType == TaxBillingHardCodedReport.SupplementalBills ||
                DefaultReportType == TaxBillingHardCodedReport.SupplementalNegativeBalanceReport ||
                DefaultReportType == TaxBillingHardCodedReport.SupplementalOutstandingBalanceReport ||
                DefaultReportType == TaxBillingHardCodedReport.SupplementalZeroBalanceReport)
            {
                return true;
            }

            return false;
        }
    }

    public class TaxCollectionStatusReportConfiguration
    {
        public TaxCollectionReportOption Options { get; private set; }
        public TaxCollectionReportFilter Filter { get; private set; }
        public string WhereClause { get; set; } = "";
        public string SortClause { get; set; } = "";
        public string SelectClause { get; set; } = "";
        public string SortListing { get; set; } = "";
        public string SQLStatement { get; set; } = "";
        public PropertyTaxBillType BillingType { get; set; } = PropertyTaxBillType.Real;
        public TaxCollectionStatusReportConfiguration(TaxCollectionReportOption reportOptions, TaxCollectionReportFilter filter)
        {
            Options = reportOptions.GetCopy();
            Filter = filter.GetCopy();
        }
    }

    public class TaxCollectionReportFilter
    {
        public string NameMin { get; set; } = "";
        public string NameMax { get; set; } = "";
        public int? TaxYearMin { get; set; } = null;
        public int? TaxYearMax { get; set; } = null;
        public int? AccountMin { get; set; } = null;
        public int? AccountMax { get; set; } = null;
        public ComparisonOptionType? BalanceDueComparisonType { get; set; }
        public Decimal? BalanceDueAmount { get; set; } = null;
        public int? AccountPaymentType { get; set; } = null;
        public DateTime? AsOfDate { get; set; } = null;
        public DateTime? PaymentDateMin { get; set; } = null;
        public DateTime? PaymentDateMax { get; set; } = null;
        public DateTime? SupplementalBillDateStart { get; set; } = null;
        public DateTime? SupplementalBillDateEnd { get; set; } = null;
        public int? TranCodeMin { get; set; } = null;
        public int? TranCodeMax { get; set; } = null;
        public bool? IsTaxAcquired { get; set; } = null;
        public int? RateRecordMin { get; set; } = null;
        public int? RateRecordMax { get; set; } = null;
        public TaxBillStatusType? TaxBillStatus { get; set; } = null;
        // public int BillStatus { get; set; }

        public bool MeetsBalanceDueCriteria(decimal amountToCompare)
        {
            switch (BalanceDueComparisonType)
            {
                case ComparisonOptionType.LessThan:
                    return amountToCompare < BalanceDueAmount;
                    break;
                case ComparisonOptionType.GreaterThan:
                    return amountToCompare > BalanceDueAmount;
                    break;
                case ComparisonOptionType.EqualTo:
                    return amountToCompare == BalanceDueAmount;
                    break;
                case ComparisonOptionType.NotEqualTo:
                    return amountToCompare != BalanceDueAmount;
                    break;
            }

            return false;
        }

        public bool AccountRangeUsed()
        {
            return AccountMin.HasValue || AccountMax.HasValue;
        }

        public bool NameRangeUsed()
        {
            return !String.IsNullOrWhiteSpace(NameMin) || !String.IsNullOrWhiteSpace(NameMax);
        }

        public bool TaxYearRangeUsed()
        {
            return TaxYearMin.HasValue || TaxYearMax.HasValue;
        }

        public bool PaymentDateRangeUsed()
        {
            return PaymentDateMin.HasValue || PaymentDateMax.HasValue;
        }

        public bool SupplementalDateRangeUsed()
        {
            return SupplementalBillDateStart.HasValue || SupplementalBillDateEnd.HasValue;
        }

        public bool TranCodeRangeUsed()
        {
            return TranCodeMin.HasValue || TranCodeMax.HasValue;
        }

        public bool RateRecordRangeUsed()
        {
            return RateRecordMin.HasValue || RateRecordMax.HasValue;
        }

        public bool BalanceDueUsed()
        {
            return BalanceDueComparisonType.HasValue && BalanceDueAmount.HasValue;
        }
    }

    public static class TaxCollectionReportExtensions
    {
        public static TaxCollectionReportFilter GetCopy(this TaxCollectionReportFilter filter)
        {
            return new TaxCollectionReportFilter()
            {
                AccountMax = filter.AccountMax,
                AccountMin = filter.AccountMin,
                AccountPaymentType = filter.AccountPaymentType,
                AsOfDate = filter.AsOfDate,
                BalanceDueAmount = filter.BalanceDueAmount,
                BalanceDueComparisonType = filter.BalanceDueComparisonType,
                //BillStatus = filter.BillStatus,
                IsTaxAcquired = filter.IsTaxAcquired,
                NameMax = filter.NameMax,
                NameMin = filter.NameMin,
                PaymentDateMax = filter.PaymentDateMax,
                PaymentDateMin = filter.PaymentDateMin,
                RateRecordMax = filter.RateRecordMax,
                RateRecordMin = filter.RateRecordMin,
                SupplementalBillDateStart = filter.SupplementalBillDateStart,
                SupplementalBillDateEnd = filter.SupplementalBillDateEnd,
                TaxYearMax = filter.TaxYearMax,
                TaxYearMin = filter.TaxYearMin,
                TranCodeMax = filter.TranCodeMax,
                TranCodeMin = filter.TranCodeMin,
                TaxBillStatus = filter.TaxBillStatus
            };
        }

        public static TaxCollectionReportOption GetCopy(this TaxCollectionReportOption option)
        {
            return new TaxCollectionReportOption()
            {
                NameToShow = option.NameToShow,
                DefaultReportType = option.DefaultReportType,
                FullStatusAmounts = option.FullStatusAmounts,
                ShowLocation = option.ShowLocation,
                ShowSummaryOnly = option.ShowSummaryOnly,
                ShowCurrentInterest = option.ShowCurrentInterest,
                ShowPayments = option.ShowPayments,
                ShowPersonalPropertyInformation = option.ShowPersonalPropertyInformation,
                ShowMapLot = option.ShowMapLot,
                ExcludePaid = option.ExcludePaid,
                ShowAddress = option.ShowAddress,
                UseDefaultReport = option.UseDefaultReport,
                PreLienOnly = option.PreLienOnly,
                AsOfDate = option.AsOfDate,
                ReportType = option.ReportType
            };
        }
    }
}