﻿using System.Linq;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public interface ITaxCollectionsContext
    {
        IQueryable<BillingMaster> BillingMasters { get;  }
        IQueryable<LienRec> LienRecords { get;  }
        IQueryable<PaymentRec> PaymentRecords { get; }
        IQueryable<RateRec> RateRecords { get; }
        IQueryable<SavedStatusReport> SavedStatusReports { get; }
        IQueryable<TaxClub> TaxClubs { get;  }
        IQueryable<CollectionSetting> CollectionSettings { get; }
        IQueryable<CollectionsComment> CollectionsComments { get; }
        IQueryable<ControlDischargeNotice> ControlDischargeNotices { get; }
        IQueryable<DischargeNeeded> NeededDischarges { get; }
        IQueryable<Note> Notes { get; }
        IQueryable<ControlLienProcess> ControlLienProcesses { get; }
        IQueryable<BatchRecover> BatchRecovers { get; }
        IQueryable<PartialPaymentWaiver> PartialPaymentWaivers { get; }
        IQueryable<RealEstateMasterBillingMasterView> RealEstateMasterBillingMasterViews { get; }
        IQueryable<PersonalPropertyMasterBillingMasterView> PersonalPropertyMasterBillingMasterViews { get; }
        IQueryable<RealEstateBillAccountPartyView> RealEstateBillAccountParties { get; }
        IQueryable<PersonalPropertyBillAccountPartyView> PersonalPropertyBillAccountParties { get; }
    }

    public interface ITaxCollectionsContextFactory
    {

    }
}