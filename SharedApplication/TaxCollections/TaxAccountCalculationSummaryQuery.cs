﻿using System;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections
{
    public class TaxAccountCalculationSummaryQuery
    {
        public int Account { get; }
        public PropertyTaxBillType BillType { get; }
        public DateTime EffectiveDate { get; }

        public TaxAccountCalculationSummaryQuery(int account, PropertyTaxBillType billType, DateTime effectiveDate)
        {
            Account = account;
            BillType = billType;
            EffectiveDate = effectiveDate;
        }
    }
}