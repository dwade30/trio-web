﻿using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections
{
    public class PropertyTaxAccountCalculationSummaries
    {
        public TaxBillCalculationSummary AccountSummary { get; private set; }
        public Dictionary<int, TaxBillCalculationSummary> CalculationSummaries { get; } = new Dictionary<int, TaxBillCalculationSummary>();

        public PropertyTaxAccountCalculationSummaries(int account, PropertyTaxBillType billType)
        {
            AccountSummary = new TaxBillCalculationSummary(account,billType);
        }
    }
}