﻿using System;
using System.Linq;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;

namespace SharedApplication.TaxCollections
{
    public class TaxAccountCalculationSummaryQueryHandler : IQueryHandler<TaxAccountCalculationSummaryQuery,TaxBillCalculationSummary>
    {
        private IPropertyTaxBillCalculator taxCalculator;
        private IPropertyTaxBillQueryHandler billQueryHandler;

        public TaxAccountCalculationSummaryQueryHandler(IPropertyTaxBillCalculator taxCalculator, IPropertyTaxBillQueryHandler billQueryHandler) //, IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler, IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> reTotalsHandler, IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo> ppBriefHandler, IQueryHandler<PersonalPropertyAccountTotalsQuery, PersonalPropertyAccountTotals> ppTotalsHandler)
        {
            this.taxCalculator = taxCalculator;
            this.billQueryHandler = billQueryHandler;

        }
        public TaxBillCalculationSummary ExecuteQuery(TaxAccountCalculationSummaryQuery query)
        {
            switch (query.BillType)
            {
                case PropertyTaxBillType.Personal:
                    return LoadPPAccount(query.Account, query.EffectiveDate);
                    break;
                case PropertyTaxBillType.Real:
                    return LoadREAccount(query.Account,query.EffectiveDate);
                    break;
                default:
                    return null;
                    break;
            }

        }

        private TaxBillCalculationSummary LoadREAccount(int Account,DateTime effectiveDate)
        {
            var bills = billQueryHandler.Find(b => b.Account == Account && b.BillingType == "RE").ToList();
            var taxAccount =  new RealEstateTaxAccount();
            taxAccount.AddBills(bills);
            return CalculateBills(taxAccount, effectiveDate);
        }

        private TaxBillCalculationSummary LoadPPAccount(int Account, DateTime effectiveDate)
        {
            var bills = billQueryHandler.Find(b => b.Account == Account && b.BillingType == "PP").ToList();
            var taxAccount = new RealEstateTaxAccount();
            taxAccount.AddBills(bills);
            return CalculateBills(taxAccount, effectiveDate);
        }

        private TaxBillCalculationSummary CalculateBills(PropertyTaxAccount taxAccount, DateTime effectiveDate)
        {           
            var accountSummary = new TaxBillCalculationSummary(taxAccount.Account,taxAccount.BillType);
            var bills = taxAccount.Bills.ToList();

            foreach (var bill in bills)
            {
                var summary = taxCalculator.CalculateBill(bill, effectiveDate);
                accountSummary.AddTo(summary);                
            }

            return accountSummary;
        }
    }
}