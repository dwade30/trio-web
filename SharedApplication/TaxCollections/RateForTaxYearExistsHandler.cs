﻿using System.Linq;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class RateForTaxYearExistsHandler : CommandHandler<RateForTaxYearExists,bool>
    {
        private ITaxCollectionsContext clContext;
        public RateForTaxYearExistsHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        protected override bool Handle(RateForTaxYearExists command)
        {
            return clContext.RateRecords.Any(r => r.Year == command.TaxYear);
        }
    }
}