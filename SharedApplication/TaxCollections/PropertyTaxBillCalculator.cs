﻿using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.TaxCollections
{
    public class PropertyTaxBillCalculator : IPropertyTaxBillCalculator
    {
        private IBillPaymentUtility billPaymentUtility;
        private decimal overPayRate;
        private int periodsInYear;

        public PropertyTaxBillCalculator(IBillPaymentUtility paymentUtility, GlobalTaxCollectionSettings taxCollectionSettings)
        {
            billPaymentUtility = paymentUtility;

            periodsInYear = taxCollectionSettings.DaysInAYear;
            overPayRate = taxCollectionSettings.OverPaymentInterestRate;
        }
        
        #region Implementation of IPropertyTaxBillCalculator

        public TaxBillCalculationSummary CalculateBill(PropertyTaxBill bill, DateTime asOfDate)
        {
            //return CalculateBill(bill, asOfDate, true);
            return CalculateBill(bill, asOfDate, false);
        }

        #endregion
        
        public TaxBillCalculationSummary CalculateBill(PropertyTaxBill bill, DateTime asOfDate, bool includeAbatements)
        {
            if (bill == null) return null;

            return bill.HasLien() ? CalculateLien(bill, asOfDate, true) : CalculateRegularBill(bill, asOfDate, includeAbatements);
        }

        private TaxBillCalculationSummary CalculateRegularBill(PropertyTaxBill bill, DateTime asOfDate, bool includeAbatements)
        {
            try
            {
                var paymentInstallments = new PropertyTaxPaymentInstallments();
                var interestPaidDate = GetMaxOfDates(DateTime.MinValue, bill.InterestAppliedThroughDate);
                decimal cumulativePerDiem = 0;
                Decimal newInterest = 0;
                var paymentSummary = bill.Payments.GetPaymentsSummary(includeAbatements);
                if (interestPaidDate.IsEarlierThan(asOfDate))
                {

                    var principalDue = bill.TaxInstallments.GetEffectiveDueAsOf(asOfDate);

                    // var currentPrincipalDue = principalDue - bill.BillSummary.PrincipalPaid;
                    var currentPrincipalDue = principalDue - paymentSummary.Principal;
                    var lastPayment = bill.Payments.GetAll().Where(p => p.Code != "I")
                        .OrderByDescending(p => p.EffectiveInterestDate).FirstOrDefault();
                    var calculateInterest = true;
                    if (lastPayment != null)
                    {
                        if (lastPayment.Code == "U")
                        {
                            if (bill.HasTaxClub())
                            {
                                if (!bill.TaxClub.NonActive ?? true)
                                {
                                    calculateInterest = false;
                                }
                            }
                        }
                    }

                    paymentInstallments = bill.TaxInstallments.GetInstallments().ToPropertyTaxPaymentInstallments();
                    paymentInstallments.AddPayment(bill.BillSummary.PrincipalPaid);

                    if (calculateInterest && currentPrincipalDue != 0)
                    {
                        decimal interestRate = 0;
                        if (bill.RateRecord != null)
                        {
                            interestRate = bill.RateRecord.InterestRate;
                        }

                        
                        // paymentInstallments.AddPayment(paymentSummary.Principal);
                        var installments = paymentInstallments.GetInstallments();
                        if (installments.Any())
                        {
                            foreach (var installment in installments)
                            {
                                var installmentInterestPaidToDate = installment.InterestDate.AddDays(-1);
                                var latestDate = GetMaxOfDates(installmentInterestPaidToDate, interestPaidDate);
                                var interestAndPerDiem = billPaymentUtility.CalculateInterestAndPerDiem(
                                    installment.NetOwed, latestDate,
                                    asOfDate, interestRate, overPayRate, periodsInYear);
                                newInterest += interestAndPerDiem.interest;
                                cumulativePerDiem += interestAndPerDiem.perDiem;
                                installment.Interest = interestAndPerDiem.interest;
                            }
                        }
                        else
                        {
                            var latestDate = interestPaidDate;
                            var interestAndPerDiem = billPaymentUtility.CalculateInterestAndPerDiem(
                                currentPrincipalDue, latestDate,
                                asOfDate, interestRate, overPayRate, periodsInYear);
                            newInterest += interestAndPerDiem.interest;
                            cumulativePerDiem += interestAndPerDiem.perDiem;
                        }
                    }
                }
                else
                {
                    paymentInstallments = bill.TaxInstallments.GetInstallments().ToPropertyTaxPaymentInstallments();
                    paymentInstallments.AddPayment(bill.BillSummary.PrincipalPaid);
                }

                return new TaxBillCalculationSummary(bill.Account.GetValueOrDefault(), bill.BillType)
                {
                    PrincipalPaid = bill.BillSummary.PrincipalPaid, // - abat,
                    Balance = bill.BillSummary.GetNetOwed() + newInterest,
                    Charged = bill.BillSummary.PrincipalCharged,
                    ChargedInterest = bill.BillSummary.InterestCharged,
                    Interest = newInterest,
                    InterestPaid = bill.BillSummary.InterestPaid,
                    CostsCharged = bill.BillSummary.DemandFeesCharged,
                    CostsPaid = bill.BillSummary.DemandFeesPaid,
                    PerDiem = cumulativePerDiem,
                    Installments = paymentInstallments.GetInstallments()
                };
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        protected DateTime GetMaxOfDates(DateTime? Date1, DateTime? Date2)
        {
            if (!Date1.HasValue)
            {
                return GetMaxOfDates(DateTime.MinValue, Date2);
            }
            if (!Date2.HasValue)
            {
                return GetMaxOfDates(DateTime.MinValue, Date1);
            }
            if (DateTime.Compare(Date1.Value, Date2.Value) < 0)
            {
                return Date2.Value;
            }
            else
            {
                return Date1.Value;
            }
        }

        private TaxBillCalculationSummary CalculateLien(PropertyTaxBill bill, DateTime asOfDate, bool includeAbatements)
        {
            var lien = bill.Lien;
            var interestPaidDate = GetMaxOfDates(DateTime.MinValue, lien.InterestAppliedThroughDate);
            var paymentsSummary = lien.Payments.GetPaymentsSummary(includeAbatements);
            var calculationSummary = new TaxBillCalculationSummary(bill.Account.GetValueOrDefault(), bill.BillType)
            {
                PrincipalPaid = lien.LienSummary.PrincipalPaid,
                Charged = lien.LienSummary.AdjustedPrincipalCharged,
                ChargedInterest = lien.LienSummary.GetTotalInterestCharged(),
                InterestPaid = lien.LienSummary.GetTotalInterestPaid(),
                CostsCharged = lien.LienSummary.GetTotalCostsCharged(),
                CostsPaid = lien.LienSummary.CostsPaid,
                PerDiem = 0
            };
            if (calculationSummary.Charged - paymentsSummary.Principal > 0 && interestPaidDate.IsEarlierThan(asOfDate))
            {
                var rateRecord = lien.RateRecord;
                var interestStartDate =
                    GetMaxOfDates(interestPaidDate, rateRecord.Installments[0].InterestStartDate.AddDays(-1));
                var interestAndPerDiem = billPaymentUtility.CalculateInterestAndPerDiem(calculationSummary.Charged - paymentsSummary.Principal,
                    interestStartDate, asOfDate, rateRecord.InterestRate, overPayRate, periodsInYear);
                calculationSummary.Interest = interestAndPerDiem.interest;
                calculationSummary.PerDiem = interestAndPerDiem.perDiem;
            }

            calculationSummary.Balance = lien.LienSummary.GetNetOwed() + calculationSummary.Interest;
            return calculationSummary;
        }


    }
}