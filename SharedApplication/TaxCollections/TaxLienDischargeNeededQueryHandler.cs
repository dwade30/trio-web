﻿using System.Linq;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Queries;

namespace SharedApplication.TaxCollections
{
    public class TaxLienDischargeNeededQueryHandler : IQueryHandler<TaxLienDischargeNeededQuery,DischargeNeeded>
    {
        private ITaxCollectionsContext clContext;
        public TaxLienDischargeNeededQueryHandler(ITaxCollectionsContext clContext)
        {
            this.clContext = clContext;
        }
        public DischargeNeeded ExecuteQuery(TaxLienDischargeNeededQuery query)
        {
            return clContext.NeededDischarges.FirstOrDefault(nd => nd.LienId == query.LienId);
        }
    }
}