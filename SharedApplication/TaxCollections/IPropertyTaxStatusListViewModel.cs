﻿using System.Collections.Generic;

namespace SharedApplication.TaxCollections
{
    public interface IPropertyTaxStatusListViewModel
    {
        TaxCollectionStatusReportConfiguration ReportConfiguration { get; set; }
        IEnumerable<PropertyTaxAccountBill> AccountBills { get; set; }
    }
}