﻿using System.Collections.Generic;
using SharedApplication.TaxCollections.AccountPayment;

namespace SharedApplication.TaxCollections
{
    public interface IRealEstateTaxAccountSearchViewModel
    {
        int SelectedAccount { get; set; }
        void Search(string name, string maplot);
        void SelectAccount(int account);
        void Cancel();
        IEnumerable<TaxAccountSearchResult> SearchResults { get; }
    }
}