﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Comment
{
    public interface ICollectionsCommentViewModel
    {
        int Account { get; set; } 
        PropertyTaxBillType BillType { get; set; }
        string Comment { get; set; }
        void LoadComment();
        void Save();
    }
}