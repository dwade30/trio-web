﻿using System;
using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public class TaxBillCalculationSummary
    {
        public TaxBillCalculationSummary(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }
        public Decimal CostsCharged { get; set; }
        public Decimal CostsPaid { get; set; }
        public Decimal InterestPaid { get; set; }
        public Decimal ChargedInterest { get; set; }
        public Decimal Charged { get; set; }
        public Decimal PrincipalPaid { get; set; }
        public Decimal Balance { get; set; }
        public Decimal Interest { get; set; }
        public Decimal PerDiem { get; set; } = 0;
        public IEnumerable<PropertyTaxPaymentInstallment> Installments { get; set; } = new List<PropertyTaxPaymentInstallment>();
        public Decimal InterestDue()
        {
            return ChargedInterest + Interest - InterestPaid;
        }

        public Decimal PrincipalDue()
        {
            return Charged - PrincipalPaid;
        }

        public Decimal CostsDue()
        {
            return CostsCharged - CostsPaid;
        }

        public Decimal TotalDue()
        {
            return PrincipalDue() + CostsDue() + InterestDue();
        }

        public void AddTo(TaxBillCalculationSummary billSummary)
        {
            CostsCharged += billSummary.CostsCharged;
            CostsPaid += billSummary.CostsPaid;
            InterestPaid += billSummary.InterestPaid;
            ChargedInterest += billSummary.ChargedInterest;
            Charged += billSummary.Charged;
            PrincipalPaid += billSummary.PrincipalPaid;
            Balance += billSummary.Balance;
            Interest += billSummary.Interest;
            PerDiem += billSummary.PerDiem;
        }
    }
}