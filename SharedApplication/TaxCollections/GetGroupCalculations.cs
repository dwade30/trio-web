﻿using System.Collections.Generic;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class GetGroupCalculations : Command<IEnumerable<TaxBillCalculationSummary>>
    {
        public int GroupNumber { get; }

        public GetGroupCalculations(int groupNumber)
        {
            GroupNumber = groupNumber;
        }
    }
}