﻿using System;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections
{
    public class PaymentSetupInfo
    {
        public int YearBill { get; set; }
        public PropertyTaxPaymentCode PayCode { get; set; }
        public string Reference { get; set; }
        public string AffectCash { get; set; }
        public string AffectCashDrawer { get; set; }
        public string Account { get; set; }
        public int Period { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Principal { get; set; } = 0;
        public decimal Interest { get; set; } = 0;
        public decimal Costs { get; set; } = 0;
        public decimal PrelienInterest { get; set; } = 0;
        public Guid TransactionIdentifier { get; set; } = Guid.NewGuid();
        public string Comment { get; set; } = "";
        public bool ForcePaymentDistributionAsIs { get; set; } = false;
        public string ExternalPaymentIdentifier { get; set; } = "";
        public decimal GetTotal()
        {
            return Principal + Interest + Costs + PrelienInterest;
        }

        public Guid CorrelationIdentifier { get; set; } = Guid.Empty;
        public string PaidBy { get; set; } = "";
    }

    public static class PaymentSetupInfoExtensions
    {
        public static PaymentSetupInfo GetCopy(this PaymentSetupInfo paymentInfo)
        {
            return new PaymentSetupInfo()
            {
                YearBill = paymentInfo.YearBill,
                Principal = paymentInfo.Principal,
                PayCode = paymentInfo.PayCode,
                Reference = paymentInfo.Reference,
                AffectCash = paymentInfo.AffectCash,
                AffectCashDrawer = paymentInfo.AffectCashDrawer,
                Account = paymentInfo.Account,
                Period = paymentInfo.Period,
                TransactionDate = paymentInfo.TransactionDate,
                Interest = paymentInfo.Interest,
                Costs = paymentInfo.Costs,
                PrelienInterest = paymentInfo.PrelienInterest,
                TransactionIdentifier = paymentInfo.TransactionIdentifier,
                Comment =  paymentInfo.Comment,
                ForcePaymentDistributionAsIs = paymentInfo.ForcePaymentDistributionAsIs,
                CorrelationIdentifier = paymentInfo.CorrelationIdentifier,
                ExternalPaymentIdentifier = paymentInfo.ExternalPaymentIdentifier,
                PaidBy = paymentInfo.PaidBy
            };
        }
    }
}