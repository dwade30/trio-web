﻿namespace SharedApplication.TaxCollections
{
    public class PropertyTaxAmounts
    {
        public decimal Principal { get; set; } = 0;
        public decimal Interest { get; set; } = 0;
        public decimal PreLienInterest { get; set; } = 0;
        public decimal Costs { get; set; } = 0;

        public bool IsNonZero()
        {
            return Principal != 0 || Interest != 0 || PreLienInterest != 0 || Costs != 0;
        }
    }
}