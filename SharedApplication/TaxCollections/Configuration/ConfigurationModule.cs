﻿using System;
using System.Collections.Generic;
using Autofac;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Notes;
using SharedApplication.TaxCollections.Queries;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.TaxCollections.TaxExtract;

namespace SharedApplication.TaxCollections.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TaxAccountStatusViewModel>().As<ITaxAccountStatusViewModel>();
            builder.RegisterType<TaxAccountSearchViewModel>().As<ITaxAccountSearchViewModel>();
            builder.RegisterType<CollectionNoteViewModel>().As<ICollectionsNoteViewModel>();
            builder.RegisterType<PropertyTaxBillCalculator>().As<IPropertyTaxBillCalculator>();
            builder.RegisterType<TaxBillInfoViewModel>().As<ITaxBillInfoViewModel>();
            builder.RegisterType<TaxPaymentInfoViewModel>().As<ITaxPaymentInfoViewModel>();
            builder.RegisterType<TaxLienInfoViewModel>().As<ITaxLienInfoViewModel>();
            builder.RegisterType<TaxExtractService>().As<ITaxExtractService>();
            builder.RegisterType<ShowRealEstateTaxAccountInfoViewModel>().As<IShowRealEstateTaxAccountInfoViewModel>();
            builder.RegisterType<TaxAccountCalculationSummaryQueryHandler>()
                .As<IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary>>();
            builder.RegisterType<ShowPersonalPropertyTaxAccountInfoViewModel>()
                .As<IShowPersonalPropertyTaxAccountInfoViewModel>();
            builder.RegisterType<TaxAccountDetailReportModel>().As<ITaxAccountDetailReportModel>();
            builder.RegisterType<TaxBillInfoReportModel>().As<ITaxBillInfoReportModel>();
            builder.RegisterType<TaxLienDischargeNeededQueryHandler>()
                .As<IQueryHandler<TaxLienDischargeNeededQuery, DischargeNeeded>>();
            builder.RegisterType<TaxLienDischargeNoticeViewModel>().As<ITaxLienDischargeNoticeViewModel>();
            builder.RegisterType<TaxLienDischargeNoticeReportViewModel>().As<ITaxLienDischargeNoticeReportViewModel>();
            builder.RegisterType<TaxLienDischargeAlternateNoticeReportModel>().As<ITaxLienDischargeAlternateNoticeReportModel>();
            builder.RegisterType<CLReceiptViewModel>().As<ICLReceiptViewModel>();
            builder.RegisterType<CLBatchInputSetupViewModel>().As<ICLBatchInputSetupViewModel>();
            builder.RegisterType<SelectCLBatchViewModel>().As<ISelectCLBatchViewModel>();
            builder.RegisterType<TaxCollectionsBatchViewModel>().As<ITaxCollectionsBatchViewModel>();
            builder.RegisterType<RealEstateTaxAccountSearchViewModel>().As<IRealEstateTaxAccountSearchViewModel>();
            builder.RegisterType<NonBudgetaryPaymentViewModel>().As<INonBudgetaryPaymentViewModel>();
            builder.RegisterType<GetPropertyTaxDiscountViewModel>().As<IGetPropertyTaxDiscountViewModel>();
            builder.RegisterType<PartialPaymentWaiverViewModel>().As<IPartialPaymentWaiverViewModel>();
            RegisterGlobalSettings(ref builder);
        }

        private void RegisterGlobalSettings(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var tcf = f.Resolve<IQueryHandler<TaxCollectionsSettingsQuery,GlobalTaxCollectionSettings>>();
                return tcf.ExecuteQuery(new TaxCollectionsSettingsQuery());
            }).As<GlobalTaxCollectionSettings>();
        }
    }
}