﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Commands
{
    public class GetPropertyTaxAccountStatuses : Command
    {
        private TaxCollectionStatusReportConfiguration ReportConfiguration { get; }

        public GetPropertyTaxAccountStatuses(TaxCollectionStatusReportConfiguration reportConfiguration)
        {
            ReportConfiguration = reportConfiguration;
        }
    }
}