﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Commands
{
    public class GetTaxCollectionsComment : Command<string>
    {
        public int Account { get; set; } = 0;
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;

        public GetTaxCollectionsComment(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
    }
}