﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Commands
{
    public class UpdateTaxExtractLastRunInfo : Command
    {
        public int BillYear { get; set; }
        public int Period { get; set; }
    }
}
