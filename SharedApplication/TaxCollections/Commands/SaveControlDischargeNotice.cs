﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Commands
{
    public class SaveControlDischargeNotice : Command
    {
        public ControlDischargeNotice DischargeNotice { get; }

        public SaveControlDischargeNotice(ControlDischargeNotice dischargeNotice)
        {
            DischargeNotice = dischargeNotice;
        }
    }
}