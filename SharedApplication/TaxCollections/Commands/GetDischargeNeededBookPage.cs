﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Commands
{
	public class GetDischargeNeededBookPage : Command<string>
	{
		public int LienId { get; set; }
	}
}
