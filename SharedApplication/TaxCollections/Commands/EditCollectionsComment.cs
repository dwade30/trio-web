﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Commands
{
    public class EditCollectionsComment : Command
    {
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }

        public EditCollectionsComment(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
    }
}