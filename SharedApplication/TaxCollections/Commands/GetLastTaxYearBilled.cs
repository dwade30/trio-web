﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Commands
{
    public class GetLastTaxYearBilled : Command<int>
    {
        public PropertyTaxBillType BillType { get; set; }

        public GetLastTaxYearBilled(PropertyTaxBillType billType)
        {
            BillType = billType;
        }
    }
}