﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Commands
{
    public class GetREPropertyHasNewOwner : Command<bool>
    {
        public DateTime EffectiveDate { get; }
        public int Account { get; }
        public GetREPropertyHasNewOwner(int account,DateTime effectiveDate)
        {
            Account = account;
            EffectiveDate = effectiveDate;
        }
    }
}