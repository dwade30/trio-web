﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Commands
{
    public class GetCalculatedTaxAccount : Command<CalculatedTaxAccount>
    {
        public int Account { get; }
        public PropertyTaxBillType BillType { get; }
        public DateTime EffectiveDate { get; }

        public GetCalculatedTaxAccount(int account, PropertyTaxBillType billType,DateTime effectiveDate)
        {
            Account = account;
            BillType = billType;
            EffectiveDate = effectiveDate;
        }
    }
}