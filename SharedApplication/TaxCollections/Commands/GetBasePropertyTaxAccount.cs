﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Commands
{
    public class GetBasePropertyTaxAccount : Command<PropertyTaxAccount>
    {
        public int Account { get; }
        public PropertyTaxBillType BillType { get; }

        public GetBasePropertyTaxAccount(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
    }
}