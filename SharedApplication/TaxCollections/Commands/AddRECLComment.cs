﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Commands
{
    public class AddRECLComment : Command
    {
        public int Account { get; } = 0;
        public bool AsModal { get; } = false;
        public AddRECLComment(int account, bool asModal)
        {
            Account = account;
            AsModal = asModal;
        }
    }
}