﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class SearchForRealEstateTaxAccountHandler : CommandHandler<SearchForRealEstateTaxAccount,int>
    {
        private IModalView<IRealEstateTaxAccountSearchViewModel> searchView;
        public SearchForRealEstateTaxAccountHandler(IModalView<IRealEstateTaxAccountSearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override int Handle(SearchForRealEstateTaxAccount command)
        {
            searchView.ShowModal();
            return searchView.ViewModel.SelectedAccount;
        }
    }
}