﻿using System.Collections.Generic;

namespace SharedApplication.TaxCollections
{
    public class PropertyTaxStatusListViewModel : IPropertyTaxStatusListViewModel
    {
        public TaxCollectionStatusReportConfiguration ReportConfiguration { get; set; }
        public IEnumerable<PropertyTaxAccountBill> AccountBills { get; set; }
    }
}