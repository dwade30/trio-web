﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.TaxCollections.Enums
{
    public enum PropertyTaxRateType
    {
        Regular = 0,
        Supplemental = 1,
        Lien = 2
    }
}
