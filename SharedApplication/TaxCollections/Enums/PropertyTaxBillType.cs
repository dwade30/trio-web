﻿namespace SharedApplication.TaxCollections.Enums
{
    public enum PropertyTaxBillType
    {
        Real = 0,
        Personal = 1
    }
}
