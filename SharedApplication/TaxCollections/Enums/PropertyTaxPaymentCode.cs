﻿namespace SharedApplication.TaxCollections.Enums
{
    public enum PropertyTaxPaymentCode
    {
        PrePayment = 0,
        Abatement = 1,
        Correction = 2,
        Discount = 3,
        RegularPayment = 4,
        RefundedAbatement = 5,
        TaxClubPayment = 6,
        NonBudgetary = 7,
        OverpayRefund = 8,
        Interest = 9,
        ThirtyDayNoticeFee = 10,
        LienMaturityFee = 11
    }

    public static class PropertyTaxPaymentCodeExtensions
    {
        public static int ToInteger(this PropertyTaxPaymentCode paymentCode)
        {
            return (int) paymentCode;
        }

        public static string ToCode(this PropertyTaxPaymentCode paymentCode)
        {
            switch (paymentCode)
            {
                case PropertyTaxPaymentCode.Abatement: //not a payment
                    return "A";
                case PropertyTaxPaymentCode.Correction: //not a payment
                    return "C";
                case PropertyTaxPaymentCode.Discount: //not a payment
                    return "D";
                case PropertyTaxPaymentCode.PrePayment:
                    return "Y";
                case PropertyTaxPaymentCode.RefundedAbatement: //not a payment
                    return "R";
                case PropertyTaxPaymentCode.NonBudgetary:
                    return "N";
                case PropertyTaxPaymentCode.OverpayRefund: //not a payment
                    return "F";
                case PropertyTaxPaymentCode.TaxClubPayment:
                    return "U";
                case PropertyTaxPaymentCode.Interest: //not a payment
                    return "I";
                case PropertyTaxPaymentCode.LienMaturityFee: // not a payment
                    return "L";
                case PropertyTaxPaymentCode.ThirtyDayNoticeFee: //not a payment
                    return "3";
                default:
                    return "P";
            }
        }

        public static string ToDescription(this PropertyTaxPaymentCode paymentCode)
        {
            switch (paymentCode)
            {
                case PropertyTaxPaymentCode.Abatement:
                    return "Abatement";
                case PropertyTaxPaymentCode.Correction:
                    return "Correction";
                case PropertyTaxPaymentCode.Discount:
                    return "Discount";
                case PropertyTaxPaymentCode.PrePayment:
                    return "PrePayment";
                case PropertyTaxPaymentCode.RefundedAbatement:
                    return "Refunded Abatement";
                case PropertyTaxPaymentCode.NonBudgetary:
                    return "Non-Budgetary";
                case PropertyTaxPaymentCode.OverpayRefund:
                    return "Overpay Refund";
                case PropertyTaxPaymentCode.TaxClubPayment:
                    return "Tax Club Payment";
                case PropertyTaxPaymentCode.ThirtyDayNoticeFee:
                    return "30 Day Notice";
                case PropertyTaxPaymentCode.LienMaturityFee:
                    return "Lien Maturity";
                default:
                    return "Regular Payment";
            }
        }

        public static GenericDescriptionPair<PropertyTaxPaymentCode> ToDescriptionEnumPair(
            this PropertyTaxPaymentCode paymentCode)
        {
            return new GenericDescriptionPair<PropertyTaxPaymentCode>(){ Description = paymentCode.ToCode() + "   - " + paymentCode.ToDescription() , ID = paymentCode};            
        }
    }
}