﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.TaxCollections.Enums
{
    public enum PropertyTaxNonBudgetaryPaymentType
    {
        None,
        Principal,
        Interest, 
        PreLienInterest,
        Cost
    }
}
