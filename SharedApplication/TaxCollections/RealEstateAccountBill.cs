﻿using System;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public class RealEstateAccountBill : PropertyTaxAccountBill
    {
        public RealEstateAccountBill()
        {
            BillType = PropertyTaxBillType.Real;
        }

        public override Payments Payments
        {
            get
            {
                return Bill.Lien != null ? Bill.Lien.Payments : Bill.Payments;
            }
        }
        public string DeedName1 { get; set; }
        public string DeedName2 { get; set; }
        //public Guid? OwnerGuid { get; set; }
        //public int? OwnerPartyType { get; set; }
        //public string OwnerFirstName { get; set; }
        //public string OwnerMiddleName { get; set; }
        //public string OwnerLastName { get; set; }
        //public string OwnerDesignation { get; set; }
        //public string OwnerEmail { get; set; }
        //public string OwnerAddress1 { get; set; }
        //public string OwnerAddress2 { get; set; }
        //public string OwnerAddress3 { get; set; }
        //public string OwnerCity { get; set; }
        //public string OwnerState { get; set; }
        //public string OwnerZip { get; set; }
        public Guid? SecOwnerGuid { get; set; }
        public int? SecOwnerPartyType { get; set; } = 0;
        public string SecOwnerFirstname { get; set; } = "";
        public string SecOwnerMiddleName { get; set; } = "";
        public string SecOwnerLastName { get; set; } = "";
        public string SecOwnerDesignation { get; set; } = "";
        public string AccountId { get; set; } = "";
        public string MapLot { get; set; } = "";
        public string Reference1 { get; set; } = "";
        public string Reference2 { get; set; } = "";
        public bool IsTaxAcquired { get; set; } = false;
        public DateTime? TaxAcquiredDate { get; set; } = null;
        public RealEstateTaxBill Bill { get; set; }
        public override PropertyTaxBill TaxBill => Bill;
        public bool Remove = false;
    }
}