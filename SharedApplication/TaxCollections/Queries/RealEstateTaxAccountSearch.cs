﻿namespace SharedApplication.TaxCollections.Queries
{
    public class RealEstateTaxAccountSearch
    {
        public string Name { get; }
        public string Maplot { get; }

        public RealEstateTaxAccountSearch(string name, string maplot)
        {
            Name = name;
            Maplot = maplot;
        }
    }
}