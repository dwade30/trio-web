﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Queries
{
    public class CollectionsNoteQuery
    {
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }

        public CollectionsNoteQuery(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
    }
}