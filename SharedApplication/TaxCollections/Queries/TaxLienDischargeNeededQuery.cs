﻿namespace SharedApplication.TaxCollections.Queries
{
    public class TaxLienDischargeNeededQuery 
    {
        public int LienId { get; }

        public TaxLienDischargeNeededQuery(int lienId)
        {
            LienId = lienId;
        }
    }
}