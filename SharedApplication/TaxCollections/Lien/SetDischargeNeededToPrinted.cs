﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class SetDischargeNeededToPrinted : Command
    {
        public DateTime UpdatedDate { get; }
        public int LienId { get; }
        public DateTime DatePaid { get; }
        public SetDischargeNeededToPrinted(int lienId,DateTime updatedDate, DateTime datePaid)
        {
            UpdatedDate = updatedDate;
            LienId = lienId;
            DatePaid = datePaid;
        }
    }
}