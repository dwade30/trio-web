﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class SetPartialPaymentToPrinted : Command
    {
        public int LienId { get; }

        public SetPartialPaymentToPrinted(int lienId)
        {
            LienId = lienId;
        }
    }
}