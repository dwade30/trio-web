﻿using System;
using System.Linq;
using SharedApplication.CentralData;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Interfaces;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public class TaxLienDischargeNoticeViewModel : ITaxLienDischargeNoticeViewModel
    {
        private CommandDispatcher commandDispatcher;
        private DataEnvironmentSettings envSettings;
        private IRegionalTownService regionalTownService;
        public TaxLienDischargeNoticeViewModel(CommandDispatcher commandDispatcher,DataEnvironmentSettings envSettings, IRegionalTownService regionalTownService)
        {
            this.commandDispatcher = commandDispatcher;
            this.envSettings = envSettings;
            this.regionalTownService = regionalTownService;
        }
        public ControlDischargeNotice GetControlDischargeNotice()
        {
            return commandDispatcher.Send(new GetControlDischargeNotice()).Result;
        }

        public ControlLienProcess GetControlLienProcess()
        {
            return commandDispatcher.Send(new GetControlLienProcess()).Result;
        }

        public RealEstateTaxBill TaxBill { get; set; }
        public DateTime PayDate { get; set; }
        public void SaveNotice(ControlDischargeNotice dischargeNotice)
        {
            commandDispatcher.Send(new SaveControlDischargeNotice(dischargeNotice));
        }

        public void SetLienDischargePrinted()
        {
            commandDispatcher.Send(new SetLienDischargeNoticePrinted(TaxBill.LienID));
        }

        public void SetDischargeNeededToPrinted()
        {
            commandDispatcher.Send(new SetDischargeNeededToPrinted(TaxBill.LienID,DateTime.Today,TaxBill.Lien.InterestAppliedThroughDate.GetValueOrDefault()));
        }

        public string TownName()
        {
            if (!envSettings.IsMultiTown)
            {
                return envSettings.ClientName;
            }

            if (TaxBill.TranCode == 0)
            {
                return envSettings.ClientName;
            }

            var town = regionalTownService.RegionalTowns().FirstOrDefault(t => t.TownNumber == TaxBill.TranCode);
            if (town != null)
            {
                return town.TownName;
            }

            return envSettings.ClientName;
        }
    }
}