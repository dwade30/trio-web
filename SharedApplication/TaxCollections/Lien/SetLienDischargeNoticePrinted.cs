﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class SetLienDischargeNoticePrinted : Command
    {
        public int LienId { get;  }

        public SetLienDischargeNoticePrinted(int lienId)
        {
            LienId = lienId;
        }
    }
}