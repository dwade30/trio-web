﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowLienDischargeNotice : Command
    {
        public DateTime DatePaid { get; }
        public RealEstateTaxBill TaxBill { get; }
        public ShowLienDischargeNotice( DateTime datePaid, RealEstateTaxBill taxBill)
        {
            DatePaid = datePaid;
            TaxBill = taxBill;
        }
    }
}