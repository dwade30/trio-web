﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class AddPartialPaymentWaiver : Command
    {
        public int LienId { get;  }
        public int YearBill { get; }
        public int Account { get; }
        public decimal PaymentAmount { get; }
        public DateTime EffectiveDate { get; }
        public decimal OwedAmount { get; }

        public AddPartialPaymentWaiver(int lienId, int yearBill, int account, decimal paymentAmount, DateTime effectiveDate,decimal owedAmount)
        {
            LienId = lienId;
            YearBill = yearBill;
            Account = account;
            PaymentAmount = paymentAmount;
            EffectiveDate = effectiveDate;
            OwedAmount = owedAmount;
        }
    }
}