﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowLienDischargeAlternateReport : Command
    {
        public RealEstateTaxAccount TaxAccount { get; }
        public DateTime PayDate { get; }
        public ControlDischargeNotice DischargeNotice { get; }
        public ShowLienDischargeAlternateReport(RealEstateTaxAccount taxAccount,DateTime payDate,ControlDischargeNotice dischargeNotice)
        {
            this.TaxAccount = taxAccount;
            this.PayDate = payDate;
            DischargeNotice = dischargeNotice;
        }
    }
}