﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowAlternateDischargeNoticeHandler : CommandHandler<ShowAlternateDischargeNotice>
    {
        private IView<ITaxLienDischargeNoticeReportViewModel> view;
        public ShowAlternateDischargeNoticeHandler(IView<ITaxLienDischargeNoticeReportViewModel> view)
        {
            this.view = view;
        }
        protected override void Handle(ShowAlternateDischargeNotice command)
        {
            view.ViewModel.TaxAccount = command.TaxAccount;
            view.ViewModel.PayDate = command.PayDate;
            view.Show();
        }
    }
}