﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowPartialPaymentWaiver :Command
    {
        public string Municipality { get; }
        public string State { get; }
        public string Owner { get; }
        public DateTime PaymentDate { get; }
        public decimal PaymentAmount { get; }
        public string Witness { get; }
        public DateTime SignedDate { get; }
        public string MapLot { get; }
        public int LienId { get; }
        public bool ShowSignedDate { get; }
        public int Account { get; }
        public int TaxYear { get; }
        public ShowPartialPaymentWaiver(string municipality, string state, string owner, DateTime paymentDate,
            decimal paymentAmount, string witness, DateTime signedDate, string mapLot, int lienId,bool showSignedDate,int account,int taxYear)
        {
            Municipality = municipality;
            State = state;
            Owner = owner;
            PaymentDate = paymentDate;
            PaymentAmount = paymentAmount;
            Witness = witness;
            SignedDate = signedDate;
            MapLot = mapLot;
            LienId = lienId;
            ShowSignedDate = showSignedDate;
            Account  = account;
            TaxYear = taxYear;
        }
    }
}