﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowPartialPaymentWaiverHandler : CommandHandler<ShowPartialPaymentWaiver>
    {
        private IModalView<IPartialPaymentWaiverViewModel> waiverView;

        public ShowPartialPaymentWaiverHandler(IModalView<IPartialPaymentWaiverViewModel> waiverView)
        {
            this.waiverView = waiverView;
        }
        protected override void Handle(ShowPartialPaymentWaiver command)
        {
            this.waiverView.ViewModel.MapLot = command.MapLot;
            this.waiverView.ViewModel.Municipality = command.Municipality;
            this.waiverView.ViewModel.Owner = command.Owner;
            this.waiverView.ViewModel.PaymentAmount = command.PaymentAmount;
            this.waiverView.ViewModel.PaymentDate = command.PaymentDate;
            this.waiverView.ViewModel.SignedDate = command.SignedDate;
            this.waiverView.ViewModel.State = command.State;
            this.waiverView.ViewModel.Witness = command.Witness;
            this.waiverView.ViewModel.LienId = command.LienId;
            this.waiverView.ViewModel.Account = command.Account;
            this.waiverView.ViewModel.TaxYear = command.TaxYear;
            this.waiverView.ShowModal();
        }
    }
}