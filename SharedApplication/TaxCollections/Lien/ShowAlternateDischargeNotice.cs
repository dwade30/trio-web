﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowAlternateDischargeNotice : Command
    {
        public RealEstateTaxAccount TaxAccount { get; }
        public DateTime PayDate { get; }
        public ShowAlternateDischargeNotice(RealEstateTaxAccount taxAccount,DateTime payDate)
        {
            TaxAccount = taxAccount;
            PayDate = payDate;
        }
    }
}