﻿using System;

namespace SharedApplication.TaxCollections.Lien
{
    public interface IPartialPaymentWaiverViewModel
    {
        string Municipality { get; set; }
        string State { get; set; }
        string Owner { get; set; }
        DateTime PaymentDate { get; set; }
        decimal PaymentAmount { get; set; }
        string Witness { get; set; }
        DateTime SignedDate { get; set; }
        string MapLot { get; set; }
        int LienId { get; set; }
        bool ShowSignDate { get; set; }
        int TaxYear { get; set; }
        int Account { get; set; }
        void PrintWaiver();
    }
}