﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class PartialPaymentWaiverViewModel : IPartialPaymentWaiverViewModel
    {
        public string Municipality { get; set; }
        public string State { get; set; }
        public string Owner { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public string Witness { get; set; }
        public DateTime SignedDate { get; set; }
        public string MapLot { get; set; }
        public int LienId { get; set; } = 0;
        public int TaxYear { get; set; }
        public int Account { get; set; }
        public bool ShowSignDate { get; set; } = false;
        private CommandDispatcher commandDispatcher;

        public PartialPaymentWaiverViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }

        public void PrintWaiver()
        {
            commandDispatcher.Send(new SetPartialPaymentToPrinted(LienId));
            commandDispatcher.Send(new ShowPartialPaymentWaiverReport(Municipality, State, Owner, PaymentDate,
                PaymentAmount, Witness, SignedDate, MapLot,TaxYear,Account,true));            
        }
    }
}