﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Queries;

namespace SharedApplication.TaxCollections.Lien
{
    public class TaxLienDischargeAlternateNoticeReportModel : ITaxLienDischargeAlternateNoticeReportModel
    {
        private IQueryHandler<TaxLienDischargeNeededQuery, DischargeNeeded> dischargeNeededHandler;

        public TaxLienDischargeAlternateNoticeReportModel(
            IQueryHandler<TaxLienDischargeNeededQuery,DischargeNeeded> dischargeNeededHandler)
        {
            this.dischargeNeededHandler = dischargeNeededHandler;
        }
        public RealEstateTaxAccount TaxAccount { get; set; }
        public ControlDischargeNotice DischargeNotice { get; set; }
        public IEnumerable<RealEstateTaxBill> PaidLiens()
        {
            var lienedBills = new List<RealEstateTaxBill>();
            var bills = TaxAccount.Bills.Where(b => b.HasLien());
            foreach (RealEstateTaxBill bill in bills)
            {
                if (bill.Lien.LienSummary.GetNetOwed() <= 0)
                {
                    lienedBills.Add(bill);
                }
            }

            return lienedBills;
        }

        public DateTime PayDate { get; set; }
        public DischargeNeeded GetDischargeNeeded(int lienId)
        {
            return dischargeNeededHandler.ExecuteQuery(new TaxLienDischargeNeededQuery(lienId));
        }
    }
}