﻿using System;
using System.Collections.Generic;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public interface ITaxLienDischargeAlternateNoticeReportModel
    {
        RealEstateTaxAccount TaxAccount { get; set; }
        ControlDischargeNotice DischargeNotice { get; set; }
        IEnumerable<RealEstateTaxBill> PaidLiens();
        DateTime PayDate { get; set; }
        DischargeNeeded GetDischargeNeeded(int lienId);
    }
}