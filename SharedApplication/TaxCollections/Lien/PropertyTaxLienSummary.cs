﻿namespace SharedApplication.TaxCollections.Lien
{
    public class PropertyTaxLienSummary
    {
        //public decimal PrincipalCharged { get; set; }
        //public decimal PreLienInterestCharged { get; set; }
       // public decimal CostsCharged { get; set; }
       // public decimal InterestCharged { get; set; }
        public decimal MaturityFee { get; set; }
        public decimal PrincipalPaid { get; set; }
        public decimal InterestPaid { get; set; }
        public decimal PreLienInterestPaid { get; set; }
        public decimal CostsPaid { get; set; }
        public decimal OriginalPrincipalCharged { get; set; }
        public decimal OriginalPreLienInterestCharged { get; set; }
        public decimal OriginalCostsCharged { get; set; }
        public decimal OriginalInterestCharged { get; set; }
        public decimal AdjustedPrincipalCharged { get; set; }
        public decimal AdjustedPreLienInterestCharged { get; set; }
        public decimal AdjustedCostsCharged { get; set; }
        public decimal AdjustedInterestCharged { get; set; }

        public decimal GetNetOwed()
        {
            return GetNetPrincipal() + GetNetInterest() +GetNetCosts();
        }

        public decimal GetNetPrincipal()
        {
            //return PrincipalCharged - PrincipalPaid;
            return AdjustedPrincipalCharged - PrincipalPaid;
        }

        public decimal GetNetInterest()
        {
            return GetNetPreLienInterest() + GetNetPostLienInterest();
        }

        public decimal GetNetPreLienInterest()
        {
            //return PreLienInterestCharged - PreLienInterestPaid;
            return AdjustedPreLienInterestCharged - PreLienInterestPaid;
        }

        public decimal GetNetPostLienInterest()
        {
            //return InterestCharged - InterestPaid;
            return AdjustedInterestCharged - InterestPaid;
        }

        public decimal GetNetCosts()
        {
            //return CostsCharged + MaturityFee - CostsPaid;
            return AdjustedCostsCharged + MaturityFee - CostsPaid;
        }

        public decimal GetTotalInterestCharged()
        {
            //return InterestCharged + PreLienInterestCharged;
            return AdjustedInterestCharged + AdjustedPreLienInterestCharged;
        }

        public decimal GetTotalOriginalInterestCharged()
        {
            return OriginalInterestCharged + OriginalPreLienInterestCharged;
        }

        public decimal GetTotalCostsCharged()
        {
            return AdjustedCostsCharged + MaturityFee;
        }

        public decimal GetTotalOriginalCostsCharged()
        {
            return OriginalCostsCharged + MaturityFee;
        }

        public decimal GetTotalInterestPaid()
        {
            return InterestPaid + PreLienInterestPaid;
        }

        public decimal GetTotalCharged()
        {
            return GetTotalInterestCharged() + GetTotalCostsCharged() + AdjustedPrincipalCharged;
        }

        public decimal GetTotalOriginalCharged()
        {
            return GetTotalOriginalInterestCharged() + GetTotalOriginalCostsCharged() + OriginalPrincipalCharged;
        }

        public decimal GetTotalPaid()
        {
            return GetTotalInterestPaid() + PrincipalPaid + CostsPaid;
        }
    }
}