﻿using System;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public interface ITaxLienDischargeNoticeReportViewModel
    {
        RealEstateTaxAccount TaxAccount { get; set; }
        DateTime PayDate { get; set; }
        ControlDischargeNotice GetControlDischargeNotice();
        void SaveNotice(ControlDischargeNotice dischargeNotice);
        string TownName();
        void ShowReport();
    }
}