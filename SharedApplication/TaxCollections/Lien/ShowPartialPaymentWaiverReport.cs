﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class ShowPartialPaymentWaiverReport : Command
    {
        public string Municipality { get; }
        public string State { get; }
        public string Owner { get; }
        public DateTime PaymentDate { get; }
        public decimal PaymentAmount { get; }
        public string Witness { get; }
        public DateTime SignedDate { get; }
        public string MapLot { get; }
        public int TaxYear { get; }
        public int Account { get; }
        public bool ShowModal { get; }
        public ShowPartialPaymentWaiverReport(string municipality, string state, string owner, DateTime paymentDate, decimal paymentAmount,string witness,DateTime signedDate,string mapLot,int taxYear,int account, bool showModal)
        {
            Municipality = municipality;
            Owner = owner;
            State = state;
            PaymentAmount = paymentAmount;
            PaymentDate = paymentDate;
            Witness = witness;
            SignedDate = signedDate;
            MapLot = mapLot;
            TaxYear = taxYear;
            Account = account;
            ShowModal = showModal;
        }
    }
}
