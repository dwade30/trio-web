﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Lien
{
    public class AddLienDischargeNotice : Command
    {
        public int LienId { get; }
        public DateTime DatePaid { get; }
        public string Teller { get; }
        public int BillId { get; }

        public AddLienDischargeNotice(int lienId, DateTime datePaid, string teller, int billId)
        {
            LienId = lienId;
            DatePaid = datePaid;
            Teller = teller;
            BillId = billId;
        }
    }
}