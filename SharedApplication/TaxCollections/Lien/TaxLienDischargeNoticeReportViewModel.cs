﻿using System;
using System.Linq;
using SharedApplication.CentralData;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public class TaxLienDischargeNoticeReportViewModel : ITaxLienDischargeNoticeReportViewModel
    {
        public RealEstateTaxAccount TaxAccount { get; set; }
        public DateTime PayDate { get; set; }
        private CommandDispatcher commandDispatcher;
        private DataEnvironmentSettings envSettings;
        private IRegionalTownService regionalTownService;
        public TaxLienDischargeNoticeReportViewModel(CommandDispatcher commandDispatcher, DataEnvironmentSettings envSettings, IRegionalTownService regionalTownService)
        {
            this.commandDispatcher = commandDispatcher;
        }
        public ControlDischargeNotice GetControlDischargeNotice()
        {
            return commandDispatcher.Send(new GetControlDischargeNotice()).Result;
        }

        public void SaveNotice(ControlDischargeNotice dischargeNotice)
        {
            commandDispatcher.Send(new SaveControlDischargeNotice(dischargeNotice));
        }

        public string TownName()
        {
            if (!envSettings.IsMultiTown)
            {
                return envSettings.ClientName;
            }

            if (TaxAccount.TownId == 0)
            {
                return envSettings.ClientName;
            }

            var town = regionalTownService.RegionalTowns().FirstOrDefault(t => t.TownNumber == TaxAccount.TownId);
            if (town != null)
            {
                return town.TownName;
            }

            return envSettings.ClientName;
        }

        public void ShowReport()
        {
            commandDispatcher.Send(new ShowLienDischargeAlternateReport(TaxAccount, PayDate,GetControlDischargeNotice()));
        }
    }
}