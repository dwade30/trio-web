﻿using System;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Lien
{
    public interface ITaxLienDischargeNoticeViewModel 
    {
        ControlDischargeNotice GetControlDischargeNotice();
        ControlLienProcess GetControlLienProcess();
        RealEstateTaxBill TaxBill { get; set; }
        DateTime PayDate { get; set; }
        void SaveNotice(ControlDischargeNotice dischargeNotice);
        void SetLienDischargePrinted();
        void SetDischargeNeededToPrinted();
        string TownName();
    }
}
