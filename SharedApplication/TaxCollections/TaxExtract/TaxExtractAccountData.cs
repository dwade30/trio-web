﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.TaxExtract
{
    public class TaxExtractAccountData
    {
        public int Account { get; set; } = 0;
        public string StreetNumber { get; set; } = "";
        public string StreetName { get; set; } = "";
        public string MapLot { get; set; } = "";
        public decimal Acres { get; set; } = 0;
        public int LastBuildingValue { get; set; } = 0;
        public string Book { get; set; } = "";
        public string Page { get; set; } = "";
        public string Ref1 { get; set; } = "";

        public List<PropertyTaxBill> Bills = new List<PropertyTaxBill>();

    }
}
