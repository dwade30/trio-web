﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Queries;

namespace SharedApplication.TaxCollections.TaxExtract
{
    public class TaxExtractService : ITaxExtractService
    {
        public IEnumerable<TaxExtractAccountData> TaxAccounts { get; set; }
        private IPropertyTaxBillQueryHandler billQueryHandler;
        private IQueryHandler<TaxExtractAccountQuery, IEnumerable<TaxExtractAccountData>> extractHandler;
        private IQueryHandler<TaxCollectionsSettingsQuery, GlobalTaxCollectionSettings> collectionsSettingsHandler;
        private IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> reTotalsHandler;
        private IQueryHandler<PersonalPropertyAccountTotalsQuery, PersonalPropertyAccountTotals> ppTotalsHandler;
        private CommandDispatcher commandDispatcher;
        private IPropertyTaxBillCalculator billCalculator;
        private bool cancelled = false;

        public TaxExtractService(IPropertyTaxBillQueryHandler billQueryHandler,
            IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> reTotalsHandler,
            IQueryHandler<PersonalPropertyAccountTotalsQuery, PersonalPropertyAccountTotals> ppTotalsHandler,
            CommandDispatcher commandDispatcher, IPropertyTaxBillCalculator billCalculator,
            IQueryHandler<TaxExtractAccountQuery, IEnumerable<TaxExtractAccountData>> extractHandler, IQueryHandler<TaxCollectionsSettingsQuery, GlobalTaxCollectionSettings> collectionsSettingsHandler)
        {
            this.billQueryHandler = billQueryHandler;
            this.reTotalsHandler = reTotalsHandler;
            this.ppTotalsHandler = ppTotalsHandler;
            this.billCalculator = billCalculator;
            this.extractHandler = extractHandler;
            this.commandDispatcher = commandDispatcher;
            this.collectionsSettingsHandler = collectionsSettingsHandler;

        }

        public void Cancel()
        {
            cancelled = true;
        }

        public event EventHandler<(int Max, int Current)> ProgressIncremented;

        public void UpdateLastExtractRunInfo(int year, int period)
        {
            commandDispatcher.Send(new UpdateTaxExtractLastRunInfo
            {
                Period = period,
                BillYear = year
            });
        }

        public IEnumerable<TaxServiceExtractRecord> GetExtractData(PropertyTaxBillType billType, int billingYear, int period, DateTime effectiveDate, int tranCode = 0)
        {
            var result = new List<TaxServiceExtractRecord>();
            var settings = collectionsSettingsHandler.ExecuteQuery(new TaxCollectionsSettingsQuery());

            cancelled = false;

            TaxAccounts = extractHandler.ExecuteQuery(new TaxExtractAccountQuery
            {
                TranCode = tranCode,
                BillingYear = billingYear,
                BillType = billType
            });

            int counter = 0;

            ProgressIncremented(this, (TaxAccounts.Count(), counter));
            
            var testYear = (billingYear.ToString() + "9").ToIntegerValue();
            foreach (var account in TaxAccounts)
            {
                if (account.Account == 58)
                {
                    Console.Write("hi");
                }

                account.Bills.AddRange(billQueryHandler
                    .Find(b => b.Account == account.Account &&
                               b.BillingType == (billType == PropertyTaxBillType.Real ? "RE" : "PP") &&
                               b.BillingYear <= testYear &&
                               (b.TaxYear() == billingYear ||
                                b.TaxDue1 + b.TaxDue2 + b.TaxDue3 + b.TaxDue4 - b.PrincipalPaid != 0))
                    .Where(s =>
                        (s.HasLien() ? s.Lien.LienSummary.GetNetOwed() != 0 : s.BillSummary.GetNetOwed() != 0) ||
                        s.TaxYear == billingYear).OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber));

                foreach (var bill in account.Bills)
                {
                    if (cancelled)
                    {
                        return new List<TaxServiceExtractRecord>();
                    }
                    var summary = billCalculator.CalculateBill(bill, effectiveDate);
                    var recordToAdd = new TaxServiceExtractRecord();

                    recordToAdd.Abatement = 0;
                    recordToAdd.Account = account.Account;
                    recordToAdd.Acres = account.Acres;
                    recordToAdd.BettermentAmount = 0;
                    recordToAdd.BettermentBalance = 0;
                    var billBookPage = billType == PropertyTaxBillType.Real
                        ? ((RealEstateTaxBill) bill).BookPage
                        : "";

                    recordToAdd.Book = GetBookPage(billType, true, account.Book, billBookPage ?? "", account.Ref1 ?? "");
                    recordToAdd.Page = GetBookPage(billType, false, account.Page, billBookPage ?? "", account.Ref1 ?? "");

                    recordToAdd.BuildingValue = bill.BillType == PropertyTaxBillType.Real
                        ? ((RealEstateTaxBill) bill).LandValue + ((RealEstateTaxBill) bill).BuildingValue > 0
                            ?
                            ((RealEstateTaxBill) bill).LandValue + ((RealEstateTaxBill) bill).BuildingValue
                            : account.LastBuildingValue
                        : 0;
                    recordToAdd.ExemptFlag = "";
                    recordToAdd.Exemption = bill.BillType == PropertyTaxBillType.Real
                        ? ((RealEstateTaxBill)bill).ExemptValue
                        : 0;
                    recordToAdd.LocationNumber = account.StreetNumber;
                    recordToAdd.LocationStreet = account.StreetName;
                    recordToAdd.MapLot = bill.BillType == PropertyTaxBillType.Real ? ((RealEstateTaxBill)bill).MapLot != "" ? ((RealEstateTaxBill)bill).MapLot : account.MapLot : "";
                    recordToAdd.Name = bill.Name1;
                    recordToAdd.SquareFootage = 0;
                    recordToAdd.PriorYearDelinquent =
                        account.Bills.Any(x => (!x.HasLien() && x.BillSummary.GetNetOwed() > 0 && x.TaxYear < billingYear) || (x.HasLien() && x.Lien.LienSummary.GetNetOwed() > 0 && x.TaxYear < billingYear)) ? "Y" : "N";
                    recordToAdd.Year = bill.TaxYear;
                    
                    if (summary.Installments.Count() > 0 && !bill.HasLien())
                    {
                        var installment = summary.Installments.FirstOrDefault(x => x.InstallmentNumber == 1);
                        if (installment != null)
                        {
                            recordToAdd.Tax1 = installment.OriginalAmount;
                            recordToAdd.Tax1Balance = installment.NetOwed;
                            recordToAdd.Tax1InterestDue = installment.Interest;
                        }

                        installment = summary.Installments.FirstOrDefault(x => x.InstallmentNumber == 2);
                        if (installment != null)
                        {
                            recordToAdd.Tax2 = installment.OriginalAmount;
                            recordToAdd.Tax2Balance = installment.NetOwed;
                            recordToAdd.Tax2InterestDue = installment.Interest;
                        }

                        installment = summary.Installments.FirstOrDefault(x => x.InstallmentNumber == 3);
                        if (installment != null)
                        {
                            recordToAdd.Tax3 = installment.OriginalAmount;
                            recordToAdd.Tax3Balance = installment.NetOwed;
                            recordToAdd.Tax3InterestDue = installment.Interest;
                        }

                        installment = summary.Installments.FirstOrDefault(x => x.InstallmentNumber == 4);
                        if (installment != null)
                        {
                            recordToAdd.Tax4 = installment.OriginalAmount;
                            recordToAdd.Tax4Balance = installment.NetOwed;
                            recordToAdd.Tax4InterestDue = installment.Interest;
                        }
                    }
                    else
                    {
                        if (bill.HasLien())
                        {
                            recordToAdd.Tax1 = summary.Charged + bill.Lien.LienSummary.OriginalCostsCharged + bill.Lien.LienSummary.OriginalPreLienInterestCharged;
                            recordToAdd.Tax1InterestDue = summary.InterestDue() - (bill.Lien.LienSummary.OriginalPreLienInterestCharged - bill.Lien.LienSummary.PreLienInterestPaid);
                            recordToAdd.Tax1Balance = bill.Lien.LienSummary.GetNetOwed();
                            //summary.Charged + bill.Lien.LienSummary.CostsCharged +
                            //bill.Lien.LienSummary.PreLienInterestCharged -
                            //bill.Lien.LienSummary.PreLienInterestPaid -
                            //bill.Lien.LienSummary.PrincipalPaid -
                            //(bill.Lien.LienSummary.CostsCharged >
                            // bill.Lien.LienSummary.CostsPaid
                            //    ? bill.Lien.LienSummary.CostsPaid
                            //    : bill.Lien.LienSummary.CostsCharged);
                        }
                        else
                        {
                            recordToAdd.Tax1 = summary.Charged;
                            recordToAdd.Tax1InterestDue = summary.InterestDue();
                            recordToAdd.Tax1Balance = summary.Balance - summary.Interest;
                        }
                        
                        
                        
                    }

                    if (!bill.HasLien())
                    {
                        recordToAdd.LienAmount = 0;
                        recordToAdd.LienBalance = 0;
                        recordToAdd.LienIndicator = "";
                        recordToAdd.Period = period;
                        recordToAdd.PaidFlag = bill.BillSummary.GetNetOwed() > 0 ? "" : "PD";
                        recordToAdd.TotalDemandFeesDue = bill.BillSummary.DemandFeesCharged - bill.BillSummary.DemandFeesPaid;
                        recordToAdd.TotalInterestDue = recordToAdd.Tax1InterestDue + recordToAdd.Tax2InterestDue + recordToAdd.Tax3InterestDue + recordToAdd.Tax4InterestDue;
                        if (bill.TaxYear == billingYear)
                        {
                            switch (period)
                            {
                                case 1:
                                    recordToAdd.TotalAmountDue = recordToAdd.Tax1Balance + recordToAdd.TotalInterestDue + recordToAdd.TotalDemandFeesDue;
                                    break;
                                case 2:
                                    recordToAdd.TotalAmountDue = recordToAdd.Tax1Balance + recordToAdd.Tax2Balance + recordToAdd.TotalInterestDue + recordToAdd.TotalDemandFeesDue;
                                    break;
                                case 3:
                                    recordToAdd.TotalAmountDue = recordToAdd.Tax1Balance + recordToAdd.Tax2Balance + recordToAdd.Tax3Balance + recordToAdd.TotalInterestDue + recordToAdd.TotalDemandFeesDue;
                                    break;
                                case 4:
                                    recordToAdd.TotalAmountDue = recordToAdd.Tax1Balance + recordToAdd.Tax2Balance + recordToAdd.Tax3Balance + recordToAdd.Tax4Balance + recordToAdd.TotalInterestDue + recordToAdd.TotalDemandFeesDue;
                                    break;
                                default:
                                    recordToAdd.TotalAmountDue = summary.Balance;
                                    break;
                            }
                        }
                        else
                        {
                            recordToAdd.TotalAmountDue = summary.Balance;
                        }
                    }
                    else
                    {
                        recordToAdd.Period = 1;
                        recordToAdd.TotalDemandFeesDue = bill.Lien.LienSummary.CostsPaid > bill.Lien.LienSummary.OriginalCostsCharged ? summary.CostsCharged - bill.Lien.LienSummary.CostsPaid : bill.Lien.LienSummary.MaturityFee;
                        recordToAdd.TotalInterestDue = recordToAdd.Tax1InterestDue + recordToAdd.Tax2InterestDue + recordToAdd.Tax3InterestDue + recordToAdd.Tax4InterestDue;
                        recordToAdd.LienAmount = summary.Charged + bill.Lien.LienSummary.OriginalCostsCharged + bill.Lien.LienSummary.OriginalPreLienInterestCharged;
                        recordToAdd.LienBalance = bill.Lien.LienSummary.GetNetOwed();
                        recordToAdd.LienIndicator = bill.Lien.LienSummary.MaturityFee > 0 ? "FF" : "";
                        recordToAdd.PaidFlag = bill.Lien.LienSummary.GetNetOwed() > 0 ? "" : "PD";
                        recordToAdd.TotalAmountDue = summary.Balance;
                    }

                    recordToAdd.TotalDiscount = 0;          //Math.Round(recordToAdd.TotalAmountDue * settings.DiscountRate, 2, MidpointRounding.AwayFromZero);
                    recordToAdd.NetAmountDue = recordToAdd.TotalAmountDue;       // - Discount

                    result.Add(recordToAdd);
                }

                counter++;
                ProgressIncremented(this, (TaxAccounts.Count(), counter));
            }

            return result;
        }

        private string GetBookPage(PropertyTaxBillType billType, bool book, string accountBookPage, string billBookPage, string accountRef1)
        {
            if (billType == PropertyTaxBillType.Real)
            {
                if (accountBookPage != "")
                {
                    return accountBookPage;
                }
                else
                {
                    var billValue = ParseBookPage(book, billBookPage);
                    if (billValue != "")
                    {
                        return billValue;
                    }
                    else
                    {
                        var refValue = ParseBookPage(book, accountRef1);
                        if (refValue != "")
                        {
                            return refValue;
                        }
                    }
                }

                return "";
            }
            else
            {
                return "";
            }
        }

        private string ParseBookPage(bool parseBook, string bookPage)
        {
            try
            {
                int intpos;
                string strTemp = "";
                string strTemp2 = "";
                int x;
                int lngBook = 0;
                int lngPage = 0;

                intpos = bookPage.IndexOf("B", StringComparison.InvariantCultureIgnoreCase);
                if (intpos >= 0)
                {
                    bookPage = bookPage.Substring(intpos);
                }
                else
                {
                    return "";
                }

                intpos = bookPage.IndexOf("P", StringComparison.InvariantCultureIgnoreCase);
                if (intpos < 0)
                    return "";


                if (intpos > 1)
                {
                    strTemp = bookPage.Substring(1, intpos - 1).Trim();
                    // if this isn't numeric then exit
                    int output;
                    if (!Int32.TryParse(strTemp, out output))
                    {
                        // check if it is bk, this could still be legit
                        if (intpos > 2 && strTemp.Length >= 2)
                        {
                            if (strTemp.Substring(1, 1).ToUpper() == "K")
                            {
                                strTemp = bookPage.Substring(2, intpos - 1).Trim();
                                if (!Int32.TryParse(strTemp, out output))
                                {
                                    return "";
                                }
                            }
                        }
                        else
                        {
                            return "";
                        }
                    }

                    lngBook = strTemp.ToIntegerValue();
                    if (lngBook == 0) return "";

                    // we have the book, now parse the page
                    if (bookPage.Length <= intpos) return "";

                    // there is at least one char after the p
                    strTemp = bookPage.Substring(intpos + 1).Trim();
                    if (strTemp == string.Empty) return "";

                    if (strTemp.Substring(0, 1).ToUpper() == "G")
                    {
                        if (strTemp.Length > 1)
                        {
                            strTemp = strTemp.Substring(1).Trim();
                            if (strTemp == string.Empty) return "";
                        }
                        else
                        {
                            return "";
                        }
                    }

                    if (Int32.TryParse(strTemp, out output))
                    {
                        lngPage = output;
                        if (lngPage > 0)
                        {
                            return parseBook ? lngBook.ToString() : lngPage.ToString();
                        }
                        else
                        {
                            return "";
                        }
                    }

                    // must try to parse it more
                    strTemp2 = "";
                    for (x = 0; x <= strTemp.Length; x++)
                    {
                        if (Int32.TryParse(strTemp.Substring(x, 1), out output))
                        {
                            strTemp2 += strTemp.Substring(x, 1);
                        }
                        else
                        {
                            break;
                        }
                    }

                    if (Int32.TryParse(strTemp2, out output))
                    {
                        lngPage = output;
                        if (lngPage <= 0)
                            return "";
                        if (parseBook)
                        {
                            return lngBook.ToString();
                        }
                        else
                        {
                            return lngPage.ToString();
                        }
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
