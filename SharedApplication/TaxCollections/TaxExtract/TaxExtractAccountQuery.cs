﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.TaxExtract
{
    public class TaxExtractAccountQuery
    {
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
        public int BillingYear { get; set; } = 0;
        public int TranCode { get; set; } = 0;
    }
}
