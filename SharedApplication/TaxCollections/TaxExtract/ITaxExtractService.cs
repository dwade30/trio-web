﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.TaxExtract
{
    public interface ITaxExtractService
    {
        IEnumerable<TaxServiceExtractRecord> GetExtractData(PropertyTaxBillType billType, int billingYear, int period,
            DateTime effectiveDate, int tranCode = 0);

        void Cancel();

        event EventHandler<(int Max, int Current)> ProgressIncremented;
        void UpdateLastExtractRunInfo(int year, int period);
    }
}
