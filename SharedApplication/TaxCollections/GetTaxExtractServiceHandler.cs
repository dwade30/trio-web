﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.TaxExtract;

namespace SharedApplication.TaxCollections
{
    public class GetTaxExtractServiceHandler : CommandHandler<GetTaxExtractService, ITaxExtractService>
    {
        private ITaxExtractService taxExtractService;

        public GetTaxExtractServiceHandler(ITaxExtractService taxExtractService)
        {
            this.taxExtractService = taxExtractService;
        }

        protected override ITaxExtractService Handle(GetTaxExtractService command)
        {
            return taxExtractService;
        }
    }
}
