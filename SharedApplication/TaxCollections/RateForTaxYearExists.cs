﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class RateForTaxYearExists : Command<bool>
    {
        public int TaxYear { get; }

        public RateForTaxYearExists(int taxYear)
        {
            TaxYear = taxYear;
        }
    }
}