﻿using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public class PersonalPropertyAccountBill : PropertyTaxAccountBill
    {
        public PersonalPropertyAccountBill()
        {
            BillType = PropertyTaxBillType.Personal;
        }

        public PersonalPropertyTaxBill Bill { get; set; }
        public override PropertyTaxBill TaxBill => Bill;

        public string Open1 { get; set; } = "";
        public string Open2 { get; set; } = "";

    }
}