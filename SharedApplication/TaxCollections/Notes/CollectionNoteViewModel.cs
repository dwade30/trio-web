﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Queries;

namespace SharedApplication.TaxCollections.Notes
{
    public class CollectionNoteViewModel : ICollectionsNoteViewModel
    {
        public int Id { get; set; }
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }
        public string Comment { get; set; }
        public int Priority { get; set; }
        public bool ShowInRealEstate { get; set; }
        public bool DisplayPopupReminder()
        {
            return Priority == 1;
        }

        private CommandDispatcher commandDispatcher;
        private IQueryHandler<CollectionsNoteQuery, Note> noteQuery;
        public CollectionNoteViewModel(CommandDispatcher commandDispatcher, IQueryHandler<CollectionsNoteQuery,Note> noteQuery)
        {
            this.commandDispatcher = commandDispatcher;
            this.noteQuery = noteQuery;
        }

        public void LoadNote()
        {
            var note = noteQuery.ExecuteQuery(new CollectionsNoteQuery(Account, BillType));
            if (note != null)
            {
                Id = note.Id;
                Comment = note.Comment;
                Priority = note.Priority.GetValueOrDefault();
                ShowInRealEstate = note.ShowInRealEstate.GetValueOrDefault();
            }
            else
            {
                Comment = "";
                Priority = 0;
                ShowInRealEstate = false;
            }
        }

        public void Save()
        {
            if (string.IsNullOrWhiteSpace(Comment))
            {
                DeleteComment();
                ClearComment();
                return;
            }

            SaveNote();
            
        }

        private void SaveNote()
        {
            var note = noteQuery.ExecuteQuery(new CollectionsNoteQuery(Account, BillType));
            if (note == null)
            {
                note = new Note();
                note.Account = Account;
                note.BillType = BillType;
            }

            note.Comment = Comment;
            note.Priority = Priority;
            note.ShowInRealEstate = ShowInRealEstate;
            commandDispatcher.Send(new AddUpdateCollectionsNote(Account, Comment, Priority, ShowInRealEstate,
                BillType));
        }
        private void DeleteComment()
        {
            if (Id > 0)
            {
                commandDispatcher.Send(new DeleteCollectionsNote(Id));
            }
        }

        private void ClearComment()
        {
            Id = 0;
            Comment = "";
            Priority = 0;
            ShowInRealEstate = false;
        }
    }
}