﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Notes
{
    public class EditCollectionsNote : Command
    {
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }

        public EditCollectionsNote(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
    }
}