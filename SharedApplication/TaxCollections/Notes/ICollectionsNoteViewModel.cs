﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Notes
{
    public interface ICollectionsNoteViewModel
    {
        int Id { get; set; }
        int Account { get; set; }
        PropertyTaxBillType BillType { get; set; }
        string Comment { get; set; }
        int Priority { get; set; }
        bool ShowInRealEstate { get; set; }

        bool DisplayPopupReminder();
        void LoadNote();
        void Save();
    }
}