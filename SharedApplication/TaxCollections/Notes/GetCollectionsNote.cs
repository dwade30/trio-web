﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Notes
{
    public class GetCollectionsNote : Command<Note>
    {
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }

        public GetCollectionsNote(int account, PropertyTaxBillType billType)
        {
            Account = account;
            BillType = billType;
        }
    }
}