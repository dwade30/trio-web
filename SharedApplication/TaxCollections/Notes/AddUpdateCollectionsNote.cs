﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Notes
{
    public class AddUpdateCollectionsNote : Command<int>
    {
        public int Account { get; set; }
        public string Comment { get; set; }
        public int Priority { get; set; }
        public bool ShowInRealEstate { get; set; }
        public PropertyTaxBillType BillType { get; set; }

        public AddUpdateCollectionsNote(int account, string comment, int priority, bool showInRealEstate,
            PropertyTaxBillType billType)
        {
            Account = account;
            Comment = comment;
            Priority = priority;
            ShowInRealEstate = showInRealEstate;
            BillType = billType;
        }
    }
}