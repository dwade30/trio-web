﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Notes
{
    public class DeleteCollectionsNote : Command
    {
        public int Id { get; set; }

        public DeleteCollectionsNote(int id)
        {
            Id = id;
        }
    }
}