﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Notes
{
    public class EditCollectionsNoteHandler : CommandHandler<EditCollectionsNote>
    {
        private IModalView<ICollectionsNoteViewModel> view;

        public EditCollectionsNoteHandler(IModalView<ICollectionsNoteViewModel> view)
        {
            this.view = view;
        }
        protected override void Handle(EditCollectionsNote command)
        {
            view.ViewModel.Account = command.Account;
            view.ViewModel.BillType = command.BillType;
            view.ViewModel.LoadNote();
            view.ShowModal();
        }
    }
}