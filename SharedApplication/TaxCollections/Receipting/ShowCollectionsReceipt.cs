﻿using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Receipting
{
    public class ShowCollectionsReceipt : Command
    {
        public int Account { get; }
        public int TownNumber { get; }
        public PropertyTaxBillType BillType { get; }
        public TaxBillCalculationSummary AccountCalculationSummary { get;  }
        public bool IsReprint { get; }
        public string Name { get;  }
        public string Location { get;  }
        public IEnumerable<PropertyTaxTransaction> Transactions { get; }
        public bool PrintNoPreview { get; }
        public ShowCollectionsReceipt(int account, PropertyTaxBillType billType,
            TaxBillCalculationSummary accountCalculationSummary, bool isReprint, string name, string location,
             IEnumerable<PropertyTaxTransaction> transactions, int townNumber,bool printNoPreview)
        {
            Account = account;
            BillType = billType;
            AccountCalculationSummary = accountCalculationSummary;
            IsReprint = isReprint;
            Name = name;
            Location = location;
            Transactions = transactions;
            TownNumber = townNumber;
            PrintNoPreview = printNoPreview;
        }
    }
}