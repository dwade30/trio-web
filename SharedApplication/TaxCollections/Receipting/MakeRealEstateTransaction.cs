﻿using System;
using System.Collections.Generic;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.TaxCollections.Receipting
{
    public class MakeRealEstateTransaction : SharedApplication.Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }
        public bool OriginIsTaxCollections { get; set; }
        public int Account { get; }
       // public IEnumerable<(string PaymentGroup,string Name,string Account)> AccountingInfo { get; }
        public MakeRealEstateTransaction(Guid correlationId, Guid transactionId, bool originIsTaxCollections,int account)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
            OriginIsTaxCollections = originIsTaxCollections;
            Id = new Guid();
            Account = account;
        }
    }
}