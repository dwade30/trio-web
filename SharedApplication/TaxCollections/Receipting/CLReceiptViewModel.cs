﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CentralData;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Receipting
{
    public class CLReceiptViewModel : ICLReceiptViewModel
    {
        public bool UseNarrowReceipt { get; set; }
        public string MuniName { get; set; }
        public string CityTown { get; set; }
        public int Account { get; set; }
        public PropertyTaxBillType BillType { get; set; }
        public TaxBillCalculationSummary AccountCalculationSummary { get; set; }

        public bool IsReprint { get; set; }
        //public string PaidBy { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }       
        public PropertyTaxAccount TaxAccount { get; set; }
        public IEnumerable<PropertyTaxTransaction> Transactions { get; set; }
        public IEnumerable<PropertyTaxTransaction> CompletedTransactions { get; } = new List<PropertyTaxTransaction>();
        public int GroupNumber { get; set; } = 0;
        public bool PrintWithoutPreview { get; set; } = false;
        public string ReceiptPrinterSetting { get; set; }

        private IPropertyTaxBillCalculator billCalculator;
        private CommandDispatcher commandDispatcher;

        public CLReceiptViewModel(IPropertyTaxBillCalculator billCalculator,CommandDispatcher commandDispatcher)
        {
            this.billCalculator = billCalculator;
            this.commandDispatcher = commandDispatcher;
        }
        //private TaxBillCalculationSummary CalculateBills(PropertyTaxAccount taxAccount,DateTime effectiveDate)
        //{
        //    var accountSummary = new TaxBillCalculationSummary(taxAccount.Account,taxAccount.BillType);
        //    List<PropertyTaxBill> bills;
        //    bills = taxAccount.Bills.ToList();
            
        //    foreach (var bill in bills)
        //    {
        //        var summary = billCalculator.CalculateBill(bill, effectiveDate);
        //        accountSummary.AddTo(summary);
        //    }

        //    return accountSummary;
        //}

        public (TaxBillCalculationSummary groupSummary,int groupCount) GetGroupSummary()
        {
            if (GroupNumber == 0)
            {
                return (groupSummary: AccountCalculationSummary,groupCount: 1);
            }
            var summaries = commandDispatcher.Send(new GetGroupCalculations(GroupNumber)).Result;
            var groupSummary = new TaxBillCalculationSummary(Account,BillType);
            foreach (var summary in summaries)
            {
                groupSummary.AddTo(summary);
            }

            return (groupSummary: groupSummary, groupCount: summaries.Count());
        }
    }
}