﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.Receipting
{
    public class ShowNonBudgetaryCLPayment : Command<(bool Cancelled, PropertyTaxAmounts Amounts)>
    {
        public decimal Interest { get; private set; }
        public decimal Principal { get; private set; }
        public decimal PreLienInterest { get; private set; }
        public decimal Costs { get; private set; }
        public bool IsLien { get; private set; }

        public ShowNonBudgetaryCLPayment(decimal principal, decimal interest, decimal preLienInterest, decimal costs, bool isLien)
        {
            Interest = interest;
            Principal = principal;
            PreLienInterest = preLienInterest;
            Costs = costs;
            IsLien = isLien;
        }
    }
}