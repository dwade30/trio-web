﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Receipting
{
    public static class PropertyTaxCRTransactionGroupExtensions       
    {
        public static IEnumerable<PropertyTaxTransaction> PropertyTaxTransactions(
            this CRTransactionGroup transactionGroup)
        {
            if (transactionGroup == null)
            {
                return new List<PropertyTaxTransaction>();
            }

            var taxTransactions = new List<PropertyTaxTransaction>();
            var transactions = transactionGroup.GetTransactions()
                .Where(t => t.GetType() == typeof(PropertyTaxTransaction));
            foreach (var transaction in transactions)
            {
                taxTransactions.Add((PropertyTaxTransaction)transaction);
            }

            return taxTransactions;
        }

        public static IEnumerable<PropertyTaxBatchTransaction> PropertyTaxBatchTransactions(
            this CRTransactionGroup transactionGroup)
        {
            if (transactionGroup == null)
            {
                return new List<PropertyTaxBatchTransaction>();
            }

            var taxTransactions = new List<PropertyTaxBatchTransaction>();
            var transactions = transactionGroup.GetTransactions()
                .Where(t => t.GetType() == typeof(PropertyTaxBatchTransaction));
            foreach (var transaction in transactions)
            {
                taxTransactions.Add((PropertyTaxBatchTransaction)transaction);
            }

            return taxTransactions;
            {
                
            }
        }

        public static IEnumerable<PropertyTaxTransaction> RealEstateTransactions(this CRTransactionGroup transactionGroup)
        {
            if (transactionGroup == null)
            {
                return new List<PropertyTaxTransaction>();
            }

            return transactionGroup.PropertyTaxTransactions().Where(t => t.BillType == PropertyTaxBillType.Real);            
        }

        public static IEnumerable<PropertyTaxTransaction> PersonalPropertyTransactions(
            this CRTransactionGroup transactionGroup)
        {
            if (transactionGroup == null)
            {
                return new List<PropertyTaxTransaction>();
            }

            return transactionGroup.PropertyTaxTransactions().Where(t => t.BillType == PropertyTaxBillType.Personal);
        }

        public static IEnumerable<PropertyTaxTransaction> PropertyTaxTransactionsForAccount(
            this CRTransactionGroup transactionGroup, int account, PropertyTaxBillType billType)
        {
            if (transactionGroup == null)
            {
                return new List<PropertyTaxTransaction>();
            }

            return transactionGroup.PropertyTaxTransactions()
                .Where(t => t.BillType == billType && t.Account == account);
        }

        public static IEnumerable<AccountsReceivableTransactionBase> AccountsReceivableTransactions(
	        this CRTransactionGroup transactionGroup, int account)
        {
	        if (transactionGroup == null)
	        {
		        return new List<AccountsReceivableTransactionBase>();
	        }

	        var accountsReceivableTransactions = new List<AccountsReceivableTransactionBase>();
	        var transactions = transactionGroup.GetTransactions()
		        .Where(t => t.GetType() == typeof(AccountsReceivableTransactionBase));
	        foreach (var transaction in transactions)
	        {
		        accountsReceivableTransactions.Add((AccountsReceivableTransactionBase)transaction);
	        }

			return accountsReceivableTransactions.Where(t => t.AccountNumber == account);
        }

        public static IEnumerable<UtilityBillingTransactionBase> UtilityBillingTransactions(
	        this CRTransactionGroup transactionGroup, int account)
        {
	        if (transactionGroup == null)
	        {
		        return new List<UtilityBillingTransactionBase>();
	        }

	        var accountsReceivableTransactions = new List<UtilityBillingTransactionBase>();
	        var transactions = transactionGroup.GetTransactions()
		        .Where(t => t.GetType() == typeof(UtilityBillingTransactionBase));
	        foreach (var transaction in transactions)
	        {
		        accountsReceivableTransactions.Add((UtilityBillingTransactionBase)transaction);
	        }

	        return accountsReceivableTransactions.Where(t => t.AccountNumber == account);
        }
	}
}