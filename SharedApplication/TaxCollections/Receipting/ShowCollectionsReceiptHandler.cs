﻿using System.Linq;
using SharedApplication.CentralData;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.Extensions;

namespace SharedApplication.TaxCollections.Receipting
{
    public class ShowCollectionsReceiptHandler : CommandHandler<ShowCollectionsReceipt>
    {
        private IView<ICLReceiptViewModel> reportView;
        private DataEnvironmentSettings envSettings;
        private IRegionalTownService regionalTownService;
        private ICentralDataContext cdContext;
        private ISettingService settingService;
        public ShowCollectionsReceiptHandler(IView<ICLReceiptViewModel> reportView, DataEnvironmentSettings envSettings, IRegionalTownService regionalTownService,ICentralDataContext cdContext, ISettingService settingService)
        {
            this.reportView = reportView;
            this.envSettings = envSettings;
            this.regionalTownService = regionalTownService;
            this.cdContext = cdContext;
            this.settingService = settingService;
        }
        protected override void Handle(ShowCollectionsReceipt command)
        {
            reportView.ViewModel.Account = command.Account;
            reportView.ViewModel.Transactions = command.Transactions;
            reportView.ViewModel.AccountCalculationSummary = command.AccountCalculationSummary;
            reportView.ViewModel.BillType = command.BillType;
            reportView.ViewModel.Location = command.Location;
            reportView.ViewModel.Name = command.Name;
            reportView.ViewModel.IsReprint = command.IsReprint;
            reportView.ViewModel.MuniName = TownName(command.TownNumber);
            reportView.ViewModel.CityTown = envSettings.CityTown;
            var moduleType = "";
            if (command.BillType == PropertyTaxBillType.Personal)
            {
                moduleType = "PP";
            }
            else
            {
                moduleType = "RE";
            }

            var association = cdContext.ModuleAssociations.FirstOrDefault(a =>
                a.Account.GetValueOrDefault() == command.Account && a.Module == moduleType);
            if (association != null)
            {
                reportView.ViewModel.GroupNumber = association.GroupNumber.GetValueOrDefault();
            }
            var receiptPrinter = settingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName");
            if (receiptPrinter != null)
            {
                reportView.ViewModel.ReceiptPrinterSetting = receiptPrinter.SettingValue;
                var setting = receiptPrinter.SettingValue;

                reportView.ViewModel.PrintWithoutPreview = !reportView.ViewModel.ReceiptPrinterSetting.IsNullOrWhiteSpace();
            }
            reportView.Show();
        }

        private string TownName(int townNumber)
        {
            if (!envSettings.IsMultiTown)
            {
                return envSettings.ClientName;
            }

            if (townNumber == 0)
            {
                return envSettings.ClientName;
            }

            var town = regionalTownService.RegionalTowns().FirstOrDefault(t => t.TownNumber == townNumber);
            if (town != null)
            {                
                return town.TownName;
            }

            return envSettings.ClientName;
        }
    }
}