﻿using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Receipting
{
    public interface ICLReceiptViewModel
    {
        bool UseNarrowReceipt { get; set; } 
        string MuniName { get; set; }
        string CityTown { get; set; }
        int Account { get; set; }
        PropertyTaxBillType BillType { get; set; }
        TaxBillCalculationSummary AccountCalculationSummary { get; set; }
        bool IsReprint { get; set; }
       // string PaidBy { get; set; }
        string Name { get; set; }
        string Location { get; set; }
        int GroupNumber { get; set; }
        //PropertyTaxAccount TaxAccount { get; set; }
        IEnumerable<PropertyTaxTransaction> Transactions { get; set; }
        (TaxBillCalculationSummary groupSummary, int groupCount) GetGroupSummary();
        bool PrintWithoutPreview { get; set; }
        string ReceiptPrinterSetting { get; set; }
    }
}