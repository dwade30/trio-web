﻿namespace SharedApplication.TaxCollections.Receipting
{
    public class RETaxTransaction : PropertyTaxTransaction
    {
        public bool IsTaxAcquired { get; set; } = false;
    }
}