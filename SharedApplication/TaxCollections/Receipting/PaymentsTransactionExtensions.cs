﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Receipting
{
    public static class PaymentsTransactionExtensions
    {
        public static IEnumerable<TransactionAmountItem> ToTransactionAmountItems(this Payments payments)
        {
            var items = new List<TransactionAmountItem>();            
            if (payments != null)
            {                
                var nonChargePayments = payments.GetAll().Where(p => !p.IsCreatedInterest());
                
                var principal = nonChargePayments.Where(p => !p.IsAbatement()).Sum(p => p.Principal).GetValueOrDefault();
                var costs = nonChargePayments.Where(p => !p.IsLien()).Sum(p => p.LienCost).GetValueOrDefault();
                var lienCosts = nonChargePayments.Where(p => p.IsLien()).Sum(p => p.LienCost).GetValueOrDefault();
                var preLienInterest = nonChargePayments.Sum(p => p.PreLienInterest).GetValueOrDefault();
                var interest = nonChargePayments.Sum(p => p.CurrentInterest).GetValueOrDefault();
                var abatements = nonChargePayments.Where(p => p.IsAbatement()).Sum(p => p.Principal)
                    .GetValueOrDefault();
                if (principal != 0)
                {
                    items.Add(new TransactionAmountItem()
                    {
                        Description = "Principal",
                        Name = "Principal",
                        Total = principal
                    });
                }

                if (interest != 0)
                {
                    items.Add(new TransactionAmountItem()
                    {
                        Description = "Interest",
                        Name = "Interest",
                        Total = interest
                    });
                }

                if (preLienInterest != 0)
                {
                    items.Add(new TransactionAmountItem()
                    {
                        Description = "Pre Lien Interest",
                        Name = "Pre Lien Interest",
                        Total = preLienInterest
                    });
                }

                if (costs != 0)
                {
                    items.Add(new TransactionAmountItem()
                    {
                        Total = costs,
                        Description = "Costs",
                        Name = "Costs"
                    });
                }

                if (lienCosts != 0)
                {
                    items.Add(new TransactionAmountItem()
                    {
                        Description = "Lien Costs",
                        Name = "Lien Costs",
                        Total = lienCosts
                    });
                }

                if (abatements != 0)
                {
                    items.Add(new TransactionAmountItem()
                    {
                        Description = "Abatement",
                        Name = "Abatement",
                        Total = abatements
                    });
                }
            }

            return items;
        }

        public static IEnumerable<TransactionAmountItem> Combine(
            this IEnumerable<TransactionAmountItem> items, IEnumerable<TransactionAmountItem> additionalItems)
        {
            var source = items.ToList();
            foreach (var item in additionalItems)
            {
                var originalItem = source.FirstOrDefault(t => t.Name == item.Name);
                if (originalItem == null)
                {
                    originalItem = new TransactionAmountItem();
                    originalItem.Name = item.Name;
                    originalItem.Description = item.Description;
                    source.Add(originalItem);
                }

                originalItem.Total += item.Total;
            }

            return source;
        }
    }
}