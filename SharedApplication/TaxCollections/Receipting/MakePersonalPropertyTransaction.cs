﻿using System;
using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.TaxCollections.Receipting
{
    public class MakePersonalPropertyTransaction: Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }
        public bool OriginIsTaxCollections { get; set; }
        public int Account { get; }

        public MakePersonalPropertyTransaction(Guid correlationId, Guid transactionId, bool originIsTaxCollections,int account)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
            OriginIsTaxCollections = originIsTaxCollections;
            Id = Guid.NewGuid();
            Account = account;
        }
    }
}