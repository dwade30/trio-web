﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Receipting
{
    public class MakePersonalPropertyTransactionHandler : CommandHandler<MakePersonalPropertyTransaction>
    {
        private IView<ITaxAccountSearchViewModel> searchView;
        public MakePersonalPropertyTransactionHandler(IView<ITaxAccountSearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override void Handle(MakePersonalPropertyTransaction command)
        {
            searchView.ViewModel.BillType = PropertyTaxBillType.Personal;
            searchView.ViewModel.CorrelationId = command.CorrelationId;
            searchView.ViewModel.TransactionId = command.TransactionId;
            searchView.ViewModel.OriginIsTaxCollections = command.OriginIsTaxCollections;
            searchView.ViewModel.AllowPayments = true;
            if (command.Account > 0)
            {
                if (searchView.ViewModel.SelectAccount(command.Account,false))
                {
                    return;
                }
            }
            searchView.Show();
        }
    }
}