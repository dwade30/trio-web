﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Receipting
{
    public  class PropertyTaxTransaction : TransactionBase
    {
        public int Account { get; set; } = 0;
        public int BillId { get; set; } = 0;
        //public int TownNumber { get; set; } = 0;
        public PropertyTaxBillType BillType { get; set; }
        public Payments Payments { get; set; } = new Payments();
        public DomainEvents<TaxBillEvent> NewEvents { get; set; } = new DomainEvents<TaxBillEvent>();
        public bool IsTaxAcquired { get; set; } = false;
        public bool IsLien { get; set; } = false;

        public int YearBill { get; set; } = 0;

        public bool AffectCash { get; set; } = true;
        public string OverrideAccount { get; set; } = "";
    }

}