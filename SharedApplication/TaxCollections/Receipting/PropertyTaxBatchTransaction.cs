﻿using System;
using System.Collections.Generic;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.AccountPayment;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Receipting
{
    public class PropertyTaxBatchTransaction : TransactionBase
    {
        public PropertyTaxBillType BillType { get; set; }
        
        private List<PropertyTaxBatchItem> batchItems = new List<PropertyTaxBatchItem>();
        public IEnumerable<PropertyTaxBatchItem> BatchItems { get => batchItems;  }
        public Guid BatchIdentifier { get; set; }
        public int TaxYear { get; set; }

        public void AddBatchItem(PropertyTaxBatchItem batchItem)
        {
            batchItems.Add(batchItem);
        }

        public void AddBatchItemRange(IEnumerable<PropertyTaxBatchItem> items)
        {
            batchItems.AddRange(items);
        }
    }
}