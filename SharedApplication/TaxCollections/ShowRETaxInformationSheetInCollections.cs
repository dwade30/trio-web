﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class ShowRETaxInformationSheetInCollections : Command
    {
        public int Account { get; set; }
        public DateTime EffectiveDate { get; set; }
        public ShowRETaxInformationSheetInCollections(int account, DateTime effectiveDate)
        {
            Account = account;
            EffectiveDate = effectiveDate;
        }
    }
}