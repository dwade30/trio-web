﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class ControlLienProcess
    {
        public int Id { get; set; }
        public int? BillingYear { get; set; }
        public double? MinimumAmount { get; set; }
        public DateTime? FilingDate { get; set; }
        public string CollectorName { get; set; }
        public string Muni { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string Signer { get; set; }
        public string Designation { get; set; }
        public double? FilingFee { get; set; }
        public double? CertMailFee { get; set; }
        public string MapPreparer { get; set; }
        public string MapPrepareDate { get; set; }
        public bool? PayCertMailFee { get; set; }
        public bool? SendCopyToMortHolder { get; set; }
        public bool? ChargeForMortHolder { get; set; }
        public string SendCopyToNewOwner { get; set; }
        public DateTime? DateCreated { get; set; }
        public string User { get; set; }
        public string OldCollector { get; set; }
        public DateTime? RecommitmentDate { get; set; }
        public DateTime? DateUpdated { get; set; }
        public double? MailTo { get; set; }
        public DateTime? CommissionExpirationDate { get; set; }
        public int? ShowLocation { get; set; }
        public bool? SendCopytoIntParty { get; set; }
        public bool? ChargeforIntParty { get; set; }
        public string DefaultSort { get; set; }
    }
}