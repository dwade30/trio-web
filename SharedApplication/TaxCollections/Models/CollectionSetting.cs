﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class CollectionSetting
    {
        public int ID { get; set; }

        public int? DaysInYear { get; set; }

        public int? RefReceipt { get; set; }

        public bool? AddLienCharge { get; set; }
        public string TaxBillFormat { get; set; }

        public bool? PayIntOnPrePayment { get; set; }

        public double? PrePaymentInterest { get; set; }

        public double? TaxRate { get; set; }

        public double? OverPayInterestRate { get; set; }

        public bool? ShowDefaultYear { get; set; }

        public bool? AuditSeqReceipt { get; set; }

        public int? LastBilledYear { get; set; }
        public string ThirtyDayNotice { get; set; }
        public string LienNotice { get; set; }
        public string MaturityNotice { get; set; }
        public string TreasurerSig { get; set; }

        public double? AdjustmentCMFLaser { get; set; }

        public double? AdjustmentLabels { get; set; }

        public double? AdjustmentLabelsDMH { get; set; }

        public double? AdjustmentLabelsDMV { get; set; }


        public string ThirtyDayNoticeR { get; set; }

        public string LienNoticeR { get; set; }

        public string MaturityNoticeR { get; set; }

        public bool? ShowLastCLAccountInCR { get; set; }

        public bool? ShowMapLotInCLAudit { get; set; }


        public string RNForm { get; set; }

        public double? gdblLienAdjustmentTop { get; set; }

        public double? gdblLDNAdjustmentTop { get; set; }

        public string LienAbateText { get; set; }

        public bool? PLIPayments { get; set; }

        public bool? IntAppliedProblem { get; set; }

        public bool? ShowTownSeal30DN { get; set; }

        public bool? ShowTownSealLien { get; set; }

        public bool? ShowTownSealLienMat { get; set; }

        public bool? ShowTownSealLienDischarge { get; set; }

        public int? ThirtyDNYear { get; set; }

        public int? ReminderFormAdjustH { get; set; }

        public int? ReminderFormAdjustV { get; set; }

       // public bool? AddLienCharge { get; set; }

        public int? LBOrder { get; set; }

        public int? TCReport { get; set; }

        public bool? DefaultRTDToAutoChange { get; set; }

        public string TownEmail { get; set; }

        public bool? DoNotAutofillPrepayment { get; set; }

        public double? AbatementInterestRate { get; set; }

        public string AcctDetailAssesment { get; set; }

        public bool? DefaultPaymentsToAuto { get; set; }

        public bool? PageBreakForTCBooklet { get; set; }

        public bool? UseRKRateForAbate { get; set; }

        public bool? RanGeneralFix { get; set; }

        public string TaxInfoSheetMessage { get; set; }

        public bool? GeneralFixPastDueAmts { get; set; }

        public bool? ReceiptNumonAcctDetail { get; set; }

        public bool? ShowTSReminder { get; set; }

        public DateTime? TSRemindLastDate { get; set; }

        public DateTime? TSRemindNextDate { get; set; }

        public int? TSLastRunYear { get; set; }

        public DateTime? LienFeeIncrRunOn { get; set; }

        public bool? RateEntryUpdateDone { get; set; }

        public double? DiscountPercent { get; set; }

        public string TaxServiceEmail { get; set; }

        public string Version { get; set; }

        public bool? DisableAutoPmtFill { get; set; }
    }
}