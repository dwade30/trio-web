﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class DischargeNeeded
    {
        public int Id { get; set; }
        public DateTime? DatePaid { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string Teller { get; set; }
        public int? LienId { get; set; }
        public int? BillId { get; set; }
        public bool? Printed { get; set; }
        public bool? Batch { get; set; }
        public bool? Cancelled { get; set; }
        public string Book { get; set; }
        public string Page { get; set; }
    }
}