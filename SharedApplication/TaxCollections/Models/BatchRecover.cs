﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class BatchRecover
    {
        public int Id { get; set; }
        public string TellerId { get; set; }
        public string PaidBy { get; set; }
        public string Ref { get; set; }
        public int? Year { get; set; }
        public int? AccountNumber { get; set; }
        public string Name { get; set; }
        public double? Prin { get; set; }
        public double? Int { get; set; }
        public DateTime? BatchRecoverDate { get; set; }
        public double? Cost { get; set; }
        public bool? PrintReceipt { get; set; }
        public string Type { get; set; }
        public int? PaymentKey { get; set; }
        public bool? PayCorrect { get; set; }
        public DateTime? Etdate { get; set; }
        public string BillingType { get; set; }
        public int? ImportId { get; set; }
        public string PaymentIdentifier { get; set; }
        public bool? Processed { get; set; }
        public Guid? BatchIdentifier { get; set; }
    }
}