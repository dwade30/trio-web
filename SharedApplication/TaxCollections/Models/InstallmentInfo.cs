﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public class InstallmentInfo
    {
        public int InstallmentNumber { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime InterestStartDate { get; set; }
        public Double Weight { get; set; } = 0;
    }
}