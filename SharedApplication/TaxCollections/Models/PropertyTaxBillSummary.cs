﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public class PropertyTaxBillSummary
    {
        public Decimal PrincipalCharged { get; set; } = 0;
        public Decimal PrincipalPaid { get; set; } = 0;
        public Decimal InterestCharged { get; set; } = 0;
        public Decimal InterestPaid { get; set; } = 0;
        public Decimal DemandFeesCharged { get; set; } = 0;
        public Decimal DemandFeesPaid { get; set; } = 0;
        public Decimal GetNetOwed()
        {
            return (PrincipalCharged + InterestCharged + DemandFeesCharged - PrincipalPaid - InterestPaid - DemandFeesPaid);
        }

        public Decimal GetNetInterest()
        {
            return InterestCharged - InterestPaid;
        }

        public Decimal GetNetCosts()
        {
            return DemandFeesCharged - DemandFeesPaid;
        }

        public Decimal GetNetPrincipal()
        {
            return PrincipalCharged - PrincipalPaid;
        }

        public void Add(PropertyTaxBillSummary billSummary)
        {
            PrincipalCharged += billSummary.PrincipalCharged;
            PrincipalPaid += billSummary.PrincipalPaid;
            InterestCharged += billSummary.InterestCharged;
            InterestPaid += billSummary.InterestPaid;
            DemandFeesCharged += billSummary.DemandFeesCharged;
            DemandFeesPaid += billSummary.DemandFeesPaid;
        }
    }
}