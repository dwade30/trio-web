﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.TaxCollections.Models
{
    public class PropertyTaxInstallments
    {
        private List<PropertyTaxInstallment> installments = new List<PropertyTaxInstallment>();

        public PropertyTaxInstallments()
        {

        }
        public PropertyTaxInstallments(IEnumerable<PropertyTaxInstallment> taxInstallments)
        {
            if (taxInstallments != null)
            {
                installments = taxInstallments.ToList();
            }
        }
        public void AddInstallment(PropertyTaxInstallment installment)
        {
            installments.Add(new PropertyTaxInstallment() { AdjustedAmount = installment.AdjustedAmount, DueDate = installment.DueDate, InstallmentNumber = installment.InstallmentNumber, OriginalAmount = installment.OriginalAmount, InterestDate = installment.InterestDate });
        }
        public Decimal GetOriginalCharged()
        {
            return installments.Sum<PropertyTaxInstallment>(o => o.OriginalAmount);
        }
        public Decimal GetEffectiveCharged()
        {
            return installments.Sum<PropertyTaxInstallment>(o => o.AdjustedAmount);
        }

        public Decimal GetEffectiveDueAsOf(DateTime asOfDate)
        {
            return installments.Where(o => DateTime.Compare(o.InterestDate, asOfDate) <= 0).Sum<PropertyTaxInstallment>(o => o.AdjustedAmount);
        }

        public void ApplyAbatementToInstallment(Decimal abatementAmount, int installmentNumber)
        {
            var installment = installments.Where(o => o.InstallmentNumber == installmentNumber).FirstOrDefault();
            if (installment == null)
            {
                return;
            }
            //if (abatementAmount <= 0)
            //{
            //    return;
            //}

            if (installment.AdjustedAmount >= abatementAmount)
            {
                installment.AdjustedAmount -= abatementAmount;
            }
            else
            {
                var leftOver = abatementAmount - installment.AdjustedAmount;
                installment.AdjustedAmount = 0;
                ApplyAbatementToInstallment(leftOver, installmentNumber + 1);
            }
        }

        public void ApplyAbatementToAllInstallments(Decimal abatementAmount)
        {
            var numberOfInstallments = installments.Count();
            if (numberOfInstallments < 1)
            {
                return;
            }
            int paymentInCents = Convert.ToInt32(abatementAmount * 100);
            int perEachCents = paymentInCents / numberOfInstallments;
            int leftOverCents = paymentInCents - (perEachCents * numberOfInstallments);
            Decimal perEach = Convert.ToDecimal(perEachCents) / 100M;
            decimal notApplied = 0;
            foreach (var installment in installments)
            {
                decimal leftOver = 0;
                var toApply = perEach;
                if (perEach > installment.AdjustedAmount && installment.InstallmentNumber > 1)
                {
                    leftOver = perEach - installment.AdjustedAmount;
                    toApply = installment.AdjustedAmount;
                    notApplied += leftOver;
                }

                if (toApply != 0)
                {
                    ApplyAbatementToInstallment(perEach, installment.InstallmentNumber);
                }
            }
            if (leftOverCents > 0 || notApplied > 0)
            {
                ApplyAbatementToInstallment((Convert.ToDecimal(leftOverCents) / 100) + notApplied, 1);
            }
        }

        public IEnumerable<PropertyTaxInstallment> GetInstallments()
        {
            return installments.AsReadOnly();
        }

        public void ReverseAbatementFromInstallment(Decimal abatementAmount, int installmentNumber)
        {
            var installment = installments.Where(o => o.InstallmentNumber == installmentNumber).FirstOrDefault();
            if (installment == null)
            {
                return;
            }
            if (abatementAmount <= 0)
            {
                return;
            }
            installment.AdjustedAmount += abatementAmount;
        }

        public void ReverseAbatementFromInstallments(decimal abatementAmount)
        {
            var numberOfInstallments = installments.Count();
            if (numberOfInstallments < 1)
            {
                return;
            }
            int paymentInCents = Convert.ToInt32(abatementAmount * 100);
            int perEachCents = paymentInCents / numberOfInstallments;
            int leftOverCents = paymentInCents - (perEachCents * numberOfInstallments);
            Decimal perEach = Convert.ToDecimal(perEachCents) / 100M;
            foreach (var installment in installments)
            {
                ReverseAbatementFromInstallment(perEach, installment.InstallmentNumber);
            }
            if (leftOverCents > 0)
            {
                ReverseAbatementFromInstallment(Convert.ToDecimal(leftOverCents) / 100, 1);
            }
        }

    }
}