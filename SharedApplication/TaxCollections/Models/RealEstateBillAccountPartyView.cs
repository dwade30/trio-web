﻿using System;
using SharedApplication.Extensions;

namespace SharedApplication.TaxCollections.Models
{
    public partial class RealEstateBillAccountPartyView
    {
        private int taxYear = 0;
        private int billNumber = 1;
        private int? billingYear;

        public int Id { get; set; }
        public int? Account { get; set; }
        public string BillingType { get; set; }
        public int? BillingYear
        {
            get
            {
                return billingYear;
            }
            set
            {
                billingYear = value;
                taxYear = value.HasValue ? Convert.ToInt32(this.billingYear.ToString().PadLeft(5, '0').Substring(0, 4)) : 0;
                billNumber = value.HasValue ? Convert.ToInt32(this.billingYear.ToString().Right(1)) : 0;
            }
        }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string MapLot { get; set; }
        public int? Streetnumber { get; set; }
        public string Apt { get; set; }
        public string StreetName { get; set; }
        public int? LandValue { get; set; }
        public int? BuildingValue { get; set; }
        public int? ExemptValue { get; set; }
        public int? TranCode { get; set; }
        public int? LandCode { get; set; }
        public int? BuildingCode { get; set; }
        public int? OtherCode1 { get; set; }
        public int? OtherCode2 { get; set; }
        public decimal? TaxDue1 { get; set; }
        public decimal? TaxDue2 { get; set; }
        public decimal? TaxDue3 { get; set; }
        public decimal? TaxDue4 { get; set; }
        public int? LienRecordNumber { get; set; }
        public decimal? PrincipalPaid { get; set; }
        public decimal? InterestPaid { get; set; }
        public decimal? InterestCharged { get; set; }
        public decimal? DemandFees { get; set; }
        public decimal? DemandFeesPaid { get; set; }
        public DateTime? InterestAppliedThroughDate { get; set; }
        public int? RateKey { get; set; }
        public DateTime? TransferFromBillingDateFirst { get; set; }
        public DateTime? TransferFromBillingDateLast { get; set; }
        public string WhetherBilledBefore { get; set; }
        public int? OwnerGroup { get; set; }
        public int? Category1 { get; set; }
        public int? Category2 { get; set; }
        public int? Category3 { get; set; }
        public int? Category4 { get; set; }
        public int? Category5 { get; set; }
        public int? Category6 { get; set; }
        public int? Category7 { get; set; }
        public int? Category8 { get; set; }
        public int? Category9 { get; set; }
        public double? Acres { get; set; }
        public double? HomesteadExemption { get; set; }
        public double? OtherExempt1 { get; set; }
        public double? OtherExempt2 { get; set; }
        public string BookPage { get; set; }
        public int? Ppassessment { get; set; }
        public int? Exempt1 { get; set; }
        public int? Exempt2 { get; set; }
        public int? Exempt3 { get; set; }
        public int? LienStatusEligibility { get; set; }
        public int? LienProcessStatus { get; set; }
        public int? Copies { get; set; }
        public string CertifiedMailNumber { get; set; }
        public bool? AbatementPaymentMade { get; set; }
        public double? TgmixedAcres { get; set; }
        public double? TgsoftAcres { get; set; }
        public double? TghardAcres { get; set; }
        public int? TgmixedValue { get; set; }
        public int? TgsoftValue { get; set; }
        public int? TghardValue { get; set; }
        public string Ref2 { get; set; }
        public string Ref1 { get; set; }
        public string Zip { get; set; }
        public bool? ShowRef1 { get; set; }
        public bool? LienProcessExclusion { get; set; }
        public string ImpbtrackingNumber { get; set; }
        public string MailingAddress3 { get; set; }
        public DateTime? PreviousInterestAppliedDate { get; set; }
        //public DateTime? OriginalInterestAppliedThroughDate { get; set; }
        //public DateTime? OriginalPreviousInterestDate
        public int AccountPartyAddressViewId { get; set; }
        public int? LastBldgVal { get; set; }
        public int? LastLandVal { get; set; }
        public int? Pineighborhood { get; set; }
        public int? PistreetCode { get; set; }
        public int? Pizone { get; set; }
        public int? PisecZone { get; set; }
        public int? Pitopography1 { get; set; }
        public int? Pitopography2 { get; set; }
        public int? Piutilities1 { get; set; }
        public int? Piutilities2 { get; set; }
        public int? Pistreet { get; set; }
        public int? Piopen1 { get; set; }
        public int? Piopen2 { get; set; }
        public double? Piacres { get; set; }
        public string RspropertyCode { get; set; }
        public int? OwnerPartyId { get; set; }
        public int? SecOwnerPartyId { get; set; }
        public string RslocNumAlph { get; set; }
        public string RslocApt { get; set; }
        public string RslocStreet { get; set; }
        public string Rsref1 { get; set; }
        public string Rsref2 { get; set; }
        public int? RilandCode { get; set; }
        public int? RibldgCode { get; set; }
        public int? RllandVal { get; set; }
        public int? RlbldgVal { get; set; }
        public int? Rlexemption { get; set; }
        public int? RiexemptCd1 { get; set; }
        public int? RiexemptCd2 { get; set; }
        public int? RiexemptCd3 { get; set; }
        public int? RitranCode { get; set; }
        public string RsmapLot { get; set; }
        public string RspreviousMaster { get; set; }
        public int? Rsaccount { get; set; }
        public int? Rscard { get; set; }
        public bool? Rsdeleted { get; set; }
        public DateTime? SaleDate { get; set; }
        public bool? TaxAcquired { get; set; }
        public double? Rssoft { get; set; }
        public double? Rshard { get; set; }
        public double? Rsmixed { get; set; }
        public int? RssoftValue { get; set; }
        public int? RshardValue { get; set; }
        public int? RsmixedValue { get; set; }
        public int? HomesteadValue { get; set; }
        public bool? InBankruptcy { get; set; }
        public int? ExemptVal1 { get; set; }
        public int? ExemptVal2 { get; set; }
        public int? ExemptVal3 { get; set; }
        public int? RsotherValue { get; set; }
        public double? Rsother { get; set; }
        public bool? HasHomestead { get; set; }
        public int? LandExempt { get; set; }
        public int? BldgExempt { get; set; }
        public int? ExemptPct1 { get; set; }
        public int? ExemptPct2 { get; set; }
        public int? ExemptPct3 { get; set; }
        public bool? RevocableTrust { get; set; }
        public DateTime? Tadate { get; set; }
        public int? PropertyCode { get; set; }
        public bool? ZoneOverride { get; set; }
        public string CardId { get; set; }
        public string AccountId { get; set; }
        public string DeedName1 { get; set; }
        public string DeedName2 { get; set; }
        public Guid? OwnerGuid { get; set; }
        public int? OwnerPartyType { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerMiddleName { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerDesignation { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerAddress1 { get; set; }
        public string OwnerAddress2 { get; set; }
        public string OwnerAddress3 { get; set; }
        public string OwnerCity { get; set; }
        public string OwnerState { get; set; }
        public string OwnerZip { get; set; }
        public Guid? SecOwnerGuid { get; set; }
        public int? SecOwnerPartyType { get; set; }
        public string SecOwnerFirstname { get; set; }
        public string SecOwnerMiddleName { get; set; }
        public string SecOwnerLastName { get; set; }
        public string SecOwnerDesignation { get; set; }
        public decimal? LienCosts { get; set; }
        public decimal? LienCostsPaid { get; set; }
        public decimal? LienInterest { get; set; }
        public DateTime? LienInterestAppliedThroughDate { get; set; }
        public decimal? LienInterestCharged { get; set; }
        public decimal? LienInterestPaid { get; set; }
        public decimal? LienMaturityFee { get; set; }
        public string LienBook { get; set; }
        public string LienPage { get; set; }
        public decimal? LienPlipaid { get; set; }
        public DateTime? LienPreviousInterestAppliedDate { get; set; }
        public decimal? LienPrincipal { get; set; }
        public decimal? LienPrincipalPaid { get; set; }
        public bool? LienPrintedLdn { get; set; }
        public int? LienRateKey { get; set; }
        public string LienStatus { get; set; }
        public bool IsLien()
        {
            return LienRecordNumber > 0;
        }
        public int TaxYear()
        {
            return taxYear;
            //return billingYear.HasValue? Convert.ToInt32(this.billingYear.ToString().PadLeft(5, '0').Substring(0, 4)) : 0;
        }

        public int BillNumber()
        {
             return billNumber;
            //return billingYear.HasValue ? Convert.ToInt32(this.billingYear.ToString().Right(1)) : 0;
        }
    }
}