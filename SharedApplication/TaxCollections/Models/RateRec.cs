﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.TaxCollections.Models
{
    public partial class RateRec
    {
        public int Id { get; set; }

        public int? Year { get; set; }

        public double? TaxRate { get; set; }

        public short? NumberOfPeriods { get; set; }

        public string RateType { get; set; }

        public DateTime? CommitmentDate { get; set; }

        public DateTime? BillingDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public DateTime? InterestStartDate1 { get; set; }

        public DateTime? InterestStartDate2 { get; set; }

        public DateTime? InterestStartDate3 { get; set; }

        public DateTime? InterestStartDate4 { get; set; }

        public DateTime? DueDate1 { get; set; }

        public DateTime? DueDate2 { get; set; }

        public DateTime? DueDate3 { get; set; }

        public DateTime? DueDate4 { get; set; }

        public float? InterestRate { get; set; }

        public string Description { get; set; }

        public DateTime? ThirtyDayNoticeDate { get; set; }

        public DateTime? LienDate { get; set; }

        public DateTime? MaturityDate { get; set; }

        public double? Weight1 { get; set; }

        public double? Weight2 { get; set; }

        public double? Weight3 { get; set; }

        public double? Weight4 { get; set; }

        public ICollection<BillingMaster> Bills { get; set; }

        public ICollection<LienRec> Liens { get; set; }
    }
}
