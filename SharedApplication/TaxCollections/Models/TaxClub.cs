﻿using System;
using System.Collections.Generic;

namespace SharedApplication.TaxCollections.Models
{
    public partial class TaxClub
    {
        public int Id { get; set; }
        public int? BillId { get; set; }
        public string Name { get; set; }
        public int? Account { get; set; }
        public int? Year { get; set; }
        public DateTime? AgreementDate { get; set; }
        public DateTime? StartDate { get; set; }
        public int? NumberOfPayments { get; set; }
        public double? PaymentAmount { get; set; }
        public double? TotalPayments { get; set; }
        public string MapLot { get; set; }
        public string Location { get; set; }
        public double? TotalPaid { get; set; }
        public string Type { get; set; }
        public bool? NonActive { get; set; }
        public short? Freq { get; set; }
        public DateTime? LastPaidDate { get; set; }
    }
}
