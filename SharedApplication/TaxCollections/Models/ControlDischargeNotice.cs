﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class ControlDischargeNotice
    {
        public int Id { get; set; }
        public string County { get; set; }
        public string Treasurer { get; set; }
        public bool? Male { get; set; }
        public string SignerName { get; set; }
        public string SignerDesignation { get; set; }
        public DateTime? CommissionExpiration { get; set; }
        public string TreasurerTitle { get; set; }
        public string AltTopText { get; set; }
        public string AltBottomText { get; set; }
        public string AltName1 { get; set; }
        public string AltName2 { get; set; }
        public string AltName3 { get; set; }
        public string AltName4 { get; set; }
    }
}