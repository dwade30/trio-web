﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class Note
    {
        public int Id { get; set; } = 0;
        public int? Account { get; set; }
        public string Comment { get; set; }
        public int? Priority { get; set; }
        public string AccountTypeAbbreviation { get; set; }
        public bool? ShowInRealEstate { get; set; }
        public PropertyTaxBillType BillType {
            get
            {
                if (AccountTypeAbbreviation == "PP")
                {
                    return PropertyTaxBillType.Personal;
                }

                return PropertyTaxBillType.Real;
            }
            set
            {
                switch (value)
                {
                    case PropertyTaxBillType.Personal:
                        AccountTypeAbbreviation = "PP";
                        break;
                    case PropertyTaxBillType.Real:
                        AccountTypeAbbreviation = "RE";
                        break;
                }
            }
        }


    }
}