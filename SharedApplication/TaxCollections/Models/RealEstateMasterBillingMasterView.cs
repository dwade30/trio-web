﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;

namespace SharedApplication.TaxCollections.Models
{
    public class RealEstateMasterBillingMasterView
    {
        private int taxYear = 0;
        private int billNumber = 1;
        private int? billingYear;
        public int ID { get; set; }

        public int? Account { get; set; }

        public string BillingType { get; set; }

        public int? BillingYear
        {
            get
            {
                return billingYear;
            }
            set
            {
                billingYear = value;
                taxYear = value.HasValue ? Convert.ToInt32(this.billingYear.ToString().PadLeft(5, '0').Substring(0, 4)) : 0;
                billNumber = value.HasValue ? Convert.ToInt32(this.billingYear.ToString().Right(1)) : 0;
            }
        }

        public string Name1 { get; set; }

        public string Name2 { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string MapLot { get; set; }

        public int? streetnumber { get; set; }

        public string Apt { get; set; }

        public string StreetName { get; set; }

        public int? LandValue { get; set; }

        public int? BuildingValue { get; set; }

        public int? ExemptValue { get; set; }

        public int? TranCode { get; set; }

        public int? LandCode { get; set; }

        public int? BuildingCode { get; set; }

        public int? OtherCode1 { get; set; }

        public int? OtherCode2 { get; set; }

        public decimal? TaxDue1 { get; set; }
        public decimal? TaxDue2 { get; set; }

        public decimal? TaxDue3 { get; set; }

        public decimal? TaxDue4 { get; set; }

        public int? LienRecordNumber { get; set; }

        public decimal? PrincipalPaid { get; set; }

        public decimal? InterestPaid { get; set; }

        public decimal? InterestCharged { get; set; }

        public decimal? DemandFees { get; set; }

        public decimal? DemandFeesPaid { get; set; }

        public DateTime? InterestAppliedThroughDate { get; set; }

        public int? RateRecId { get; set; }

        public DateTime? TransferFromBillingDateFirst { get; set; }

        public DateTime? TransferFromBillingDateLast { get; set; }

        public string WhetherBilledBefore { get; set; }

        public int? OwnerGroup { get; set; }

        public int? Category1 { get; set; }

        public int? Category2 { get; set; }

        public int? Category3 { get; set; }

        public int? Category4 { get; set; }

        public int? Category5 { get; set; }

        public int? Category6 { get; set; }

        public int? Category7 { get; set; }

        public int? Category8 { get; set; }

        public int? Category9 { get; set; }

        public double? Acres { get; set; }

        public double? HomesteadExemption { get; set; }

        public double? OtherExempt1 { get; set; }

        public double? OtherExempt2 { get; set; }

        public string BookPage { get; set; }

        public int? PPAssessment { get; set; }

        public int? Exempt1 { get; set; }

        public int? Exempt2 { get; set; }

        public int? Exempt3 { get; set; }

        public int? LienStatusEligibility { get; set; }

        public int? LienProcessStatus { get; set; }

        public int? Copies { get; set; }

        public string CertifiedMailNumber { get; set; }

        public bool? AbatementPaymentMade { get; set; }

        public double? TGMixedAcres { get; set; }

        public double? TGSoftAcres { get; set; }

        public double? TGHardAcres { get; set; }

        public int? TGMixedValue { get; set; }

        public int? TGSoftValue { get; set; }

        public int? TGHardValue { get; set; }

        public string Ref2 { get; set; }

        public string Ref1 { get; set; }

        public string Zip { get; set; }

        public bool? ShowRef1 { get; set; }

        public bool? LienProcessExclusion { get; set; }

        public string IMPBTrackingNumber { get; set; }

        public string MailingAddress3 { get; set; }

        public DateTime? PreviousInterestAppliedDate { get; set; }

        public bool IsLien()
        {
            return LienRecordNumber > 0;
        }
        public int TaxYear()
        {
            return taxYear;
        }

        public int BillNumber()
        {
            return billNumber;
        }

        public RateRec RateRec { get; set; }

        public int MasterId { get; set; }
        public int? LastBldgVal { get; set; }
        public int? LastLandVal { get; set; }
        public DateTime? HlUpdate { get; set; }
        public string HiupdCode { get; set; }
        public int? HlcompValBldg { get; set; }
        public int? HlcompValLand { get; set; }
        public int? HlvalBldg { get; set; }
        public int? HivalBldgCode { get; set; }
        public int? HlvalLand { get; set; }
        public int? HivalLandCode { get; set; }
        public int? Hlalt1Bldg { get; set; }
        public int? Hialt1BldgCode { get; set; }
        public int? Hlalt2Bldg { get; set; }
        public int? Hialt2BldgCode { get; set; }
        public int? Pineighborhood { get; set; }
        public int? PistreetCode { get; set; }
        public string Pixcoord { get; set; }
        public string Piycoord { get; set; }
        public int? Pizone { get; set; }
        public int? PisecZone { get; set; }
        public int? Pitopography1 { get; set; }
        public int? Pitopography2 { get; set; }
        public int? Piutilities1 { get; set; }
        public int? Piutilities2 { get; set; }
        public int? Pistreet { get; set; }
        public int? Piopen1 { get; set; }
        public int? Piopen2 { get; set; }
        public int? PisalePrice { get; set; }
        public int? PisaleType { get; set; }
        public int? PisaleFinancing { get; set; }
        public int? PisaleVerified { get; set; }
        public int? PisaleValidity { get; set; }
        public int? Hldolexemption { get; set; }
        public int? Piland1Type { get; set; }
        public string Piland1UnitsA { get; set; }
        public string Piland1UnitsB { get; set; }
        public double? Piland1Inf { get; set; }
        public int? Piland1InfCode { get; set; }
        public int? Piland2Type { get; set; }
        public string Piland2UnitsA { get; set; }
        public string Piland2UnitsB { get; set; }
        public double? Piland2Inf { get; set; }
        public int? Piland2InfCode { get; set; }
        public int? Piland3Type { get; set; }
        public string Piland3UnitsA { get; set; }
        public string Piland3UnitsB { get; set; }
        public double? Piland3Inf { get; set; }
        public int? Piland3InfCode { get; set; }
        public int? Piland4Type { get; set; }
        public string Piland4UnitsA { get; set; }
        public string Piland4UnitsB { get; set; }
        public double? Piland4Inf { get; set; }
        public int? Piland4InfCode { get; set; }
        public int? Piland5Type { get; set; }
        public string Piland5UnitsA { get; set; }
        public string Piland5UnitsB { get; set; }
        public double? Piland5Inf { get; set; }
        public int? Piland5InfCode { get; set; }
        public int? Piland6Type { get; set; }
        public string Piland6UnitsA { get; set; }
        public string Piland6UnitsB { get; set; }
        public double? Piland6Inf { get; set; }
        public int? Piland6InfCode { get; set; }
        public int? Piland7Type { get; set; }
        public string Piland7UnitsA { get; set; }
        public string Piland7UnitsB { get; set; }
        public double? Piland7Inf { get; set; }
        public int? Piland7InfCode { get; set; }
        public double? Piacres { get; set; }
        public string RspropertyCode { get; set; }
        public string RsdwellingCode { get; set; }
        public string RsoutbuildingCode { get; set; }
        public int? OwnerPartyId { get; set; }
        public int? SecOwnerPartyId { get; set; }
        public string RslocNumAlph { get; set; }
        public string RslocApt { get; set; }
        public string RslocStreet { get; set; }
        public string Rsref1 { get; set; }
        public string Rsref2 { get; set; }
        public int? RilandCode { get; set; }
        public int? RibldgCode { get; set; }
        public int? RllandVal { get; set; }
        public int? RlbldgVal { get; set; }
        public int? CorrExemption { get; set; }
        public int? Rlexemption { get; set; }
        public int? RiexemptCd1 { get; set; }
        public int? RiexemptCd2 { get; set; }
        public int? RiexemptCd3 { get; set; }
        public int? RitranCode { get; set; }
        public string RsmapLot { get; set; }
        public string RspreviousMaster { get; set; }
        public int? Rsaccount { get; set; }
        public int? Rscard { get; set; }
        public bool? Rsdeleted { get; set; }
        public DateTime? SaleDate { get; set; }
        public bool? TaxAcquired { get; set; }
        public double? Rssoft { get; set; }
        public double? Rshard { get; set; }
        public double? Rsmixed { get; set; }
        public int? RssoftValue { get; set; }
        public int? RshardValue { get; set; }
        public int? RsmixedValue { get; set; }
        public int? HomesteadValue { get; set; }
        public bool? InBankruptcy { get; set; }
        public int? ExemptVal1 { get; set; }
        public int? ExemptVal2 { get; set; }
        public int? ExemptVal3 { get; set; }
        public int? EntranceCode { get; set; }
        public int? InformationCode { get; set; }
        public DateTime? DateInspected { get; set; }
        public int? RsotherValue { get; set; }
        public double? Rsother { get; set; }
        public bool? HasHomestead { get; set; }
        public int? CurrLandExempt { get; set; }
        public int? CurrBldgExempt { get; set; }
        public int? LandExempt { get; set; }
        public int? BldgExempt { get; set; }
        public int? ExemptPct1 { get; set; }
        public int? ExemptPct2 { get; set; }
        public int? ExemptPct3 { get; set; }
        public int? CurrHomesteadValue { get; set; }
        public bool? CurrHasHomestead { get; set; }
        public int? CurrExemptVal1 { get; set; }
        public int? CurrExemptVal2 { get; set; }
        public int? CurrExemptVal3 { get; set; }
        public bool? RevocableTrust { get; set; }
        public bool? AccountLocked { get; set; }
        public string ReasonAccountLocked { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? TADate { get; set; }
        public int? PropertyCode { get; set; }
        public bool? ZoneOverride { get; set; }
        public string CardId { get; set; }
        public string AccountId { get; set; }
        public string DeedName1 { get; set; }
        public string DeedName2 { get; set; }
        public string LinkedBook { get; set; }
        public string LinkedPage { get; set; }
    }
}
