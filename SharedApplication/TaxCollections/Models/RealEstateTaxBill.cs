﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class RealEstateTaxBill : PropertyTaxBill
    {
        public string MapLot { get; set; } = "";
        public decimal Acreage { get; set; } = 0;
        public int LandValue { get; set; } = 0;
        public int BuildingValue { get; set; } = 0;
        public int ExemptValue { get; set; } = 0;
        public int HomesteadExemption { get; set; } = 0;
        public string BookPage { get; set; } = "";
        public string Reference1 { get; set; } = "";
        public string Reference2 { get; set; } = "";
        public int ExemptCode1 { get; set; } = 0;
        public int ExemptCode2 { get; set; } = 0;
        public int ExemptCode3 { get; set; } = 0;

        public RealEstateTaxBill() : base()
        {
            BillType = PropertyTaxBillType.Real;
        }
    }
}