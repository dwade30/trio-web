﻿using System;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class CollectionsComment
    {
        public int Id { get; set; }
        public int? Account { get; set; }
        public bool? IsRealEstate { get; set; }
        public string Comment { get; set; }
        public DateTime? Updated { get; set; }

        public PropertyTaxBillType BillType
        {
            get
            {
                if (IsRealEstate.GetValueOrDefault())
                {
                    return PropertyTaxBillType.Real;
                }
                else
                {
                    return PropertyTaxBillType.Personal;
                }
            }

            set { IsRealEstate = value == PropertyTaxBillType.Real; }
        }
    }
}