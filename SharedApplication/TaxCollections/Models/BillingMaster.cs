﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;

namespace SharedApplication.TaxCollections.Models
{
    public partial class BillingMaster
    {
        private int taxYear = 0;
        private int billNumber = 1;
        private int? billingYear;
        public int ID { get; set; }

        public int? Account { get; set; }

        public string BillingType { get; set; }

        public int? BillingYear
        {
            get
            {
                return billingYear;
            }
            set
            {
                billingYear = value;
                taxYear = value.HasValue ? Convert.ToInt32(this.billingYear.ToString().PadLeft(5, '0').Substring(0, 4)) : 0;
                billNumber = value.HasValue ? Convert.ToInt32(this.billingYear.ToString().Right(1)) : 0;
            }
        }

        public string Name1 { get; set; }

        public string Name2 { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string MapLot { get; set; }

        public int? streetnumber { get; set; }

        public string Apt { get; set; }

        public string StreetName { get; set; }

        public int? LandValue { get; set; }

        public int? BuildingValue { get; set; }

        public int? ExemptValue { get; set; }

        public int? TranCode { get; set; }

        public int? LandCode { get; set; }

        public int? BuildingCode { get; set; }

        public int? OtherCode1 { get; set; }

        public int? OtherCode2 { get; set; }

        public decimal? TaxDue1 { get; set; }
        public decimal? TaxDue2 { get; set; }

        public decimal? TaxDue3 { get; set; }

        public decimal? TaxDue4 { get; set; }

        public int? LienRecordNumber { get; set; }

        public decimal? PrincipalPaid { get; set; }

        public decimal? InterestPaid { get; set; }

        public decimal? InterestCharged { get; set; }

        public decimal? DemandFees { get; set; }

        public decimal? DemandFeesPaid { get; set; }

        public DateTime? InterestAppliedThroughDate { get; set; }

        public int? RateRecId { get; set; }

        public DateTime? TransferFromBillingDateFirst { get; set; }

        public DateTime? TransferFromBillingDateLast { get; set; }

        public string WhetherBilledBefore { get; set; }

        public int? OwnerGroup { get; set; }

        public int? Category1 { get; set; }

        public int? Category2 { get; set; }

        public int? Category3 { get; set; }

        public int? Category4 { get; set; }

        public int? Category5 { get; set; }

        public int? Category6 { get; set; }

        public int? Category7 { get; set; }

        public int? Category8 { get; set; }

        public int? Category9 { get; set; }

        public double? Acres { get; set; }

        public double? HomesteadExemption { get; set; }

        public double? OtherExempt1 { get; set; }

        public double? OtherExempt2 { get; set; }

        public string BookPage { get; set; }

        public int? PPAssessment { get; set; }

        public int? Exempt1 { get; set; }

        public int? Exempt2 { get; set; }

        public int? Exempt3 { get; set; }

        public int? LienStatusEligibility { get; set; }

        public int? LienProcessStatus { get; set; }

        public int? Copies { get; set; }

        public string CertifiedMailNumber { get; set; }

        public bool? AbatementPaymentMade { get; set; }

        public double? TGMixedAcres { get; set; }

        public double? TGSoftAcres { get; set; }

        public double? TGHardAcres { get; set; }

        public int? TGMixedValue { get; set; }

        public int? TGSoftValue { get; set; }

        public int? TGHardValue { get; set; }

        public string Ref2 { get; set; }

        public string Ref1 { get; set; }

        public string Zip { get; set; }

        public bool? ShowRef1 { get; set; }

        public bool? LienProcessExclusion { get; set; }

        public string IMPBTrackingNumber { get; set; }

        public string MailingAddress3 { get; set; }

        public DateTime? PreviousInterestAppliedDate { get; set; }

        public bool IsLien()
        {
            return LienRecordNumber > 0;
        }
        public int TaxYear()
        {
            return taxYear;
        }

        public int BillNumber()
        {
            return billNumber;
        }

        public RateRec RateRec { get; set; }
    }
}
