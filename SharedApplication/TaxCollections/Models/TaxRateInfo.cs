﻿using System;
using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class TaxRateInfo
    {
        public int id { get; set; } = 0;
        public Decimal InterestRate { get; set; } = 0;
        public Decimal TaxRate { get; set; } = 0;
        public Decimal OverPayRate { get; set; } = 0;
        public String Description { get; set; } = "";
        public DateTime CommitmentDate { get; set; }
        public DateTime BillingDate { get; set; }
        public DateTime CreationDate { get; set; }
        public PropertyTaxRateType RateType { get; set; } =  PropertyTaxRateType.Regular;
        public int Year { get; set; } = 0;
        public List<InstallmentInfo> Installments = new List<InstallmentInfo>();
        public DateTime LienDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public DateTime ThirtyDayNoticeDate { get; set; }

    }
}