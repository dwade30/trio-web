﻿using System;
using System.Collections.Generic;

namespace SharedApplication.TaxCollections.Models
{
    public partial class LienRec
    {
        public int Id { get; set; }
        public decimal? Principal { get; set; }
        public decimal? Interest { get; set; }
        public decimal? Costs { get; set; }
        public int? RateKey { get; set; }
        public decimal? InterestCharged { get; set; }
        public decimal? MaturityFee { get; set; }
        public decimal? PrincipalPaid { get; set; }
        public decimal? InterestPaid { get; set; }
        public decimal? Plipaid { get; set; }
        public decimal? CostsPaid { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? InterestAppliedThroughDate { get; set; }
        public string Status { get; set; }
        public string Book { get; set; }
        public string Page { get; set; }
        public bool? PrintedLdn { get; set; }
        public DateTime? PreviousInterestAppliedDate { get; set; }
        public RateRec RateRec { get; set; }
    }
}
