﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public class PaymentSummary
    {
        public Decimal Principal { get; set; } = 0;
        public Decimal Interest { get; set; } = 0;
        public Decimal Costs { get; set; } = 0;
    }
}