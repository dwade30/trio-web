﻿using System;
using System.Collections.Generic;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public partial class PaymentRec
    {
        private int taxYear = 0;
        private int billNumber = 1;
        private int? year;
        public int Id { get; set; }
        public int? Account { get; set; }
        public int? Year
        {
            get { return year; }
            set
            {
                year = value;
                taxYear = value.HasValue ? Convert.ToInt32(this.year.ToString().PadLeft(5, '0').Substring(0, 4)) : 0 ;
                billNumber = value.HasValue ? Convert.ToInt32(this.year.ToString().Right(1)) : 0;
            }
        }
        public int? BillId { get; set; }
        public int? ChargedInterestId { get; set; }
        public DateTime? ChargedInterestdate { get; set; }
        public DateTime? ActualSystemDate { get; set; }
        public DateTime? EffectiveInterestDate { get; set; }
        public DateTime? RecordedTransactionDate { get; set; }
        public string Teller { get; set; }
        public string Reference { get; set; } = "";
        public string Period { get; set; }
        public string Code { get; set; } = "";
        public int? ReceiptNumber { get; set; }
        public bool? DosreceiptNumber { get; set; }
        public decimal? Principal { get; set; }
        public decimal? PreLienInterest { get; set; }
        public decimal? CurrentInterest { get; set; }
        public decimal? LienCost { get; set; }
        public string TransactionNumber { get; set; }
        public string PaidBy { get; set; }
        public string Comments { get; set; }
        public string CashDrawer { get; set; }
        public string GeneralLedger { get; set; }
        public string BudgetaryAccountNumber { get; set; }
        public string BillCode { get; set; }
        public int? DailyCloseOut { get; set; }
        public DateTime? OldInterestDate { get; set; }
        public bool? IsReversal { get; set; }
        public DateTime? SetEidate { get; set; }
		public string ExternalPaymentIdentifier { get; set; }
		public Guid? CorrelationIdentifier { get; set; }
        public Guid? TransactionIdentifier { get; set; }
        public Guid? PaymentIdentifier { get; set; }
        public int TaxYear()
        {
            return taxYear;
        }

        public int BillNumber()
        {
            return billNumber;
        }

        public bool IsAbatement()
        {
            return Code.ToLower() == "a";            
        }

        public bool IsRefundedAbatement()
        {
            return Code.ToLower() == "r";            
        }

        public bool IsTypeOfAbatement()
        {
            return IsAbatement() || IsRefundedAbatement();
        }

        public bool IsDiscount()
        {
            return Code.ToLower() == "d";
        }
        public bool IsPrepayment()
        {
            return Code.ToLower() == "y";            
        }

        public bool IsNonBudgetary()
        {
            return Code.ToLower() == "n";
        }
        public PropertyTaxBillType BillType()
        {
            if (BillCode == "P")
            {
                return PropertyTaxBillType.Personal;
            }

            return PropertyTaxBillType.Real;
        }

        public bool IsLien()
        {
            return BillCode == "L";            
        }

        public bool IsChargedInterest()
        {
            return Reference?.ToLower() == "chgint";            
        }

        public bool IsEarnedInterest()
        {
            return Reference == "EARNINT";            
        }

        public bool IsCreatedInterest()
        {
            return IsChargedInterest() || IsEarnedInterest();
        }

        public bool IsConversionRecord()
        {
            return Reference == "CNVRSN";
        }

        public bool IsInterest()
        {
            return Reference == "INTEREST";
        }

        public bool IsDemandFeeCharge()
        {
            return Reference == "DEMAND";
        }

        public bool IsLienCostCharge()
        {
            return Code == "L";
        }

        public PropertyTaxPaymentCode GetPayCode()
        {
            switch (Code.ToLower())
            {
                case "y":
                    return PropertyTaxPaymentCode.PrePayment;
                    break;
                case "a":
                    return PropertyTaxPaymentCode.Abatement;
                    break;
                case "c":
                    return PropertyTaxPaymentCode.Correction;
                    break;
                case "d":
                    return PropertyTaxPaymentCode.Discount;
                    break;
                case "u":
                    return PropertyTaxPaymentCode.TaxClubPayment;
                    break;
                case "n":
                    return PropertyTaxPaymentCode.NonBudgetary;
                    break;
                case "r":
                    return PropertyTaxPaymentCode.RefundedAbatement;
                case "f":
                    return PropertyTaxPaymentCode.OverpayRefund;
                    break;
                default:
                    return PropertyTaxPaymentCode.RegularPayment;
                    break;
            }
        }

        public decimal Total()
        {
            return PreLienInterest.GetValueOrDefault() + Principal.GetValueOrDefault() +
                   CurrentInterest.GetValueOrDefault() + LienCost.GetValueOrDefault();
        }

        public bool IsTaxClub()
        {
            return Code.ToLower() == "u";
        }
    }
}
