﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public class PropertyTaxInstallment
    {
        public int InstallmentNumber { get; set; } = 0;
        public Decimal OriginalAmount { get; set; } = 0;
        public Decimal AdjustedAmount { get; set; } = 0;
        public DateTime DueDate { get; set; } = DateTime.MinValue;
        public DateTime InterestDate { get; set; } = DateTime.MinValue;
    }

    public class PropertyTaxPaymentInstallment : PropertyTaxInstallment
    {
        public Decimal AmountPaid { get; set; } = 0;
        public Decimal NetOwed
        {
            get { return AdjustedAmount - AmountPaid; }
        }

        public Decimal Interest { get; set; } = 0;
    }
}