﻿using SharedApplication.TaxCollections.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class Payments
    {
        private List<PaymentRec> payments = new List<PaymentRec>();

        public void AddPayments(IEnumerable<PaymentRec> paymentCollection)
        {
            foreach (var payment in paymentCollection)
            {
                AddPayment(payment);
            }
        }

        public void AddPayments(Payments paymentsToAdd)
        {
            AddPayments(paymentsToAdd.GetAll());
        }
        public void AddPayment(PaymentRec payment)
        {
            payments.Add(new PaymentRec()
            {
                Account = payment.Account,
                ActualSystemDate = payment.ActualSystemDate,
                BillCode = payment.BillCode,
                BillId = payment.BillId,
                BudgetaryAccountNumber = payment.BudgetaryAccountNumber,
                CashDrawer = payment.CashDrawer,
                ChargedInterestdate = payment.ChargedInterestdate,
                ChargedInterestId = payment.ChargedInterestId,
                Code = payment.Code,
                Comments = payment.Comments,
                CurrentInterest = payment.CurrentInterest,
                DailyCloseOut = payment.DailyCloseOut,
                DosreceiptNumber = payment.DosreceiptNumber,
                EffectiveInterestDate = payment.EffectiveInterestDate,
                GeneralLedger = payment.GeneralLedger,
                Id = payment.Id,
                IsReversal = payment.IsReversal,
                LienCost = payment.LienCost,
                OldInterestDate = payment.OldInterestDate,
                PaidBy = payment.PaidBy,
                Period = payment.Period,
                PreLienInterest = payment.PreLienInterest,
                Principal = payment.Principal,
                ReceiptNumber = payment.ReceiptNumber,
                RecordedTransactionDate = payment.RecordedTransactionDate,
                Reference = payment.Reference,
                SetEidate = payment.SetEidate,
                Teller = payment.Teller,
                TransactionNumber = payment.TransactionNumber,
                Year = payment.Year,
                PaymentIdentifier = payment.PaymentIdentifier,
                CorrelationIdentifier = payment.CorrelationIdentifier,
                TransactionIdentifier = payment.TransactionIdentifier,
                //ChargedInterestIdentifier = payment.ChargedInterestIdentifier,
                ExternalPaymentIdentifier = payment.ExternalPaymentIdentifier
            });
        }
        public PaymentSummary GetPaymentsSummary(bool includeAbatements = true)
        {
            var returnPayments = payments.WithoutAbatements().SummarizeAsPaymentSummary();

            if (includeAbatements)
            {
                var tempPayments = payments.Abatements().SummarizeAsPaymentSummary();
                returnPayments.Interest += tempPayments.Interest;
                returnPayments.Costs += tempPayments.Costs;
                if (payments.TrueForAll(x => x.IsLien()))
                {
	                returnPayments.Principal += tempPayments.Principal;
                }
            }

            return returnPayments;
        }

        public IEnumerable<PaymentRec> GetAll()
        {
            return new List<PaymentRec>(payments);
        }

        public void RemovePayment(int id)
        {
            payments.RemoveAll(p => p.Id == id);
        }

        public void RemovePayment(Guid paymentIdentifier)
        {
            if (paymentIdentifier != null)
            {
                payments.RemoveAll(p => p.PaymentIdentifier.GetValueOrDefault() == paymentIdentifier);
            }
        }

        public bool HasDosPayments()
        {
            return payments.Any(p => p.DosreceiptNumber.GetValueOrDefault());
        }

        public PaymentRec LastRecordedPayment()
        {
            return payments.OrderByDescending(p => p.RecordedTransactionDate).ThenByDescending(p => p.Id).FirstOrDefault();
        }

        public bool HasCreatedInterest()
        {
            return payments.Any(p => p.IsCreatedInterest());
        }

        public bool HasNonBudgetaryPayment()
        {
            return payments.Any(p => p.IsNonBudgetary());
        }

        public bool HasPaymentType(PropertyTaxPaymentCode paymentCode)
        {
            return payments.Any(p => p.Code == paymentCode.ToCode());
        }
        public Payments GetPaymentsAfterRecordedOrder(PaymentRec payment)
        {
            return GetPaymentsAfter(payment,
                payments.OrderByDescending(p => p.RecordedTransactionDate).ThenByDescending(p => p.Id));
        }

        public Payments GetPaymentsAfterListedOrder(PaymentRec payment)
        {
            return GetPaymentsAfter(payment, payments);
        }

        private Payments GetPaymentsAfter(PaymentRec paymentToFind, IEnumerable<PaymentRec> paymentsToSearch)
        {
            var returnPayments = new Payments();
            if (paymentToFind == null || !payments.Any())
            {
                return returnPayments;
            }

            var found = false;
            foreach (var payment in paymentsToSearch)
            {
                if (found)
                {
                    returnPayments.AddPayment(payment);
                }
                else
                {
                    if (payment.PaymentIdentifier == paymentToFind.PaymentIdentifier)
                    {
                        found = true;
                    }
                }
            }

            return returnPayments;
        }

        public bool HasPayment(Guid paymentIdentifier)
        {
            return payments.Any(p => p.PaymentIdentifier.GetValueOrDefault() == paymentIdentifier);
        }

        public bool HasAny()
        {
            return payments.Any();
        }

        public Payments GetNonZeroPayments()
        {
            var nonZeroPayments = payments.Where(p =>
                p.PreLienInterest != 0 || p.Principal != 0 || p.LienCost != 0 || p.CurrentInterest != 0);
            var returnPayments = new Payments();
            returnPayments.AddPayments(nonZeroPayments);
            return returnPayments;
        }

        public Payments GetPaymentsInRange(DateTime startDate, DateTime endDate)
        {
            return payments.Where(p => (p.RecordedTransactionDate??DateTime.MinValue) >= startDate && (p.RecordedTransactionDate??DateTime.MaxValue) <= endDate).ToPayments();
        }

        public bool HasPaymentsInRange(DateTime startDate, DateTime endDate)
        {
            return payments.Any(p =>
                (p.RecordedTransactionDate ?? DateTime.MinValue) >= startDate &&
                (p.RecordedTransactionDate ?? DateTime.MaxValue) <= endDate);
        }
    }
}