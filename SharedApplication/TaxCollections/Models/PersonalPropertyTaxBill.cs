﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.PersonalProperty;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class PersonalPropertyTaxBill : PropertyTaxBill
    {
        public int OtherCode1 { get; set; } = 0;
        public int OtherCode2 { get; set; } = 0;
        public PersonalPropertyValuations PersonalPropertyValuations { get; } = new PersonalPropertyValuations();
        public PersonalPropertyTaxBill() : base()
        {
            BillType = PropertyTaxBillType.Personal;
        }
    }
}