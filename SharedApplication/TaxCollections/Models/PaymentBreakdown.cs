﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public class PaymentBreakdown
    {
        public Decimal PrincipalCharged { get; set; } = 0;
        public Decimal PrincipalPaid { get; set; } = 0;
        public Decimal InterestCharged { get; set; } = 0;
        public Decimal InterestPaid { get; set; } = 0;
        public Decimal DemandFeesCharged { get; set; } = 0;
        public Decimal DemandFeesPaid { get; set; } = 0;

    }
}