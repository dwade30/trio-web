﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class PartialPaymentWaiver
    {
        public int Id { get; set; }
        public int? LienKey { get; set; }
        public int? Account { get; set; }
        public int? BillYear { get; set; }
        public double? Owed { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public bool? Printed { get; set; }
        public double? Payment { get; set; }
        
    }
}