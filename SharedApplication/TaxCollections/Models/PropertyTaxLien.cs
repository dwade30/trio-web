﻿using System;
using System.Linq;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Lien;

namespace SharedApplication.TaxCollections.Models
{
    public class PropertyTaxLien
    {

        public int Id { get; set; }
        public int? RateId { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? InterestAppliedThroughDate { get; set; }
        public DomainEvents<TaxBillEvent> NewEvents { get; private set; }
        public string Status { get; set; }
        public string Book { get; set; }
        public string Page { get; set; }
        public bool? PrintedLdn { get; set; }
        public PropertyTaxLienSummary LienSummary { get; private set; }
        public Payments Payments { get; private set; }
        public Payments NewPayments { get; private set; }
        public TaxRateInfo RateRecord { get; set; }
        public DateTime? PreviousInterestAppliedDate { get; set; }
        public int Account { get; set; }
        public int BillId { get; set; }
        public PropertyTaxLien()
        {
            Payments = new Payments();
            NewPayments = new Payments();
            LienSummary = new PropertyTaxLienSummary();
            NewEvents = new DomainEvents<TaxBillEvent>();
        }

        /// <summary>
        /// Add payments when this is a snapshot. Apply payments when totals should be calculated from the payments
        /// </summary>
        /// <param name="payments"></param>
        public void AddPayments(Payments payments)
        {
            if (payments == null)
            {
                return;
            }

            var paymentsList = payments.GetAll();
            foreach (var payment in paymentsList)
            {
                AddPayment(payment);              
            }
        }

        protected void AddPayment(PaymentRec payment)
        {
            Payments.AddPayment(payment);
        }

        /// <summary>
        /// Use ApplyPayments when paid totals should be calculated.  Use AddPayments when adding the totals manually as a snapshot
        /// </summary>
        /// <param name="payments"></param>
        public void ApplyPayments(Payments payments)
        {
            ApplyPayments(payments,true);
        }

        public void ApplyPayments(Payments payments, bool useEvents)
        {
            if (payments == null)
            {
                return;
            }
            var paymentsList = payments.GetAll();
            foreach (var payment in paymentsList)
            {
                AddPayment(payment);
                ApplyPayment(payment,useEvents);
            }
        }

        protected void ApplyPayment(PaymentRec payment)
        {
            ApplyPayment(payment,true);
        }

        protected void ApplyPaymentWithoutEvents(PaymentRec payment)
        {
            ApplyPayment(payment,false);
        }

        protected void ApplyPayment(PaymentRec payment,bool useEvents)
        {
            switch (payment.Code)
            {
                case "I":
                    LienSummary.OriginalInterestCharged += (payment.CurrentInterest.GetValueOrDefault() * -1);
                    LienSummary.AdjustedInterestCharged += (payment.CurrentInterest.GetValueOrDefault() * -1);
                    break;
                case "L":
                    LienSummary.MaturityFee += (payment.LienCost.GetValueOrDefault() * -1);
                    break;
                case "A":
                    //LienSummary.PrincipalCharged -= payment.Principal.GetValueOrDefault();
                    LienSummary.AdjustedPrincipalCharged -= payment.Principal.GetValueOrDefault();
                    LienSummary.AdjustedInterestCharged -= payment.CurrentInterest.GetValueOrDefault();
                    LienSummary.AdjustedPreLienInterestCharged -= payment.PreLienInterest.GetValueOrDefault();
                    LienSummary.AdjustedCostsCharged -= payment.LienCost.GetValueOrDefault();
                    if (LienSummary.GetNetOwed() <= 0 && useEvents)
                    {
                        AddPaidEvent(payment);                
                    }
                    
                    break;
                default:
                    LienSummary.PrincipalPaid = LienSummary.PrincipalPaid + payment.Principal.GetValueOrDefault();
                    LienSummary.CostsPaid += payment.LienCost.GetValueOrDefault();
                    LienSummary.InterestPaid += payment.CurrentInterest.GetValueOrDefault();
                    LienSummary.PreLienInterestPaid += payment.PreLienInterest.GetValueOrDefault();
                    if (LienSummary.GetNetOwed() > 0 && useEvents)
                    {
                        var paidEvents = NewEvents.GetEvents()
                            .Where(de => de.EventType == TaxBillEventType.PropertyTaxLienPaid).Select(de => de.Id);
                        NewEvents.RemoveEvents(paidEvents);
                        if (payment.Code != "C")
                        {
                            AddPartiallyPaidEvent(payment);
                        }
                    }

                    if (LienSummary.GetNetOwed() <= 0 && useEvents)
                    {
                        AddPaidEvent(payment);
                    }
                    break;
            }

            if (payment.EffectiveInterestDate.HasValue && !payment.IsAbatement())
            {                
                InterestAppliedThroughDate = payment.EffectiveInterestDate;
            }
        }

        protected void ReversePayment(PaymentRec payment)
        {
            switch (payment.Code)
            {
                case "I":
                    LienSummary.OriginalInterestCharged -= (payment.CurrentInterest.GetValueOrDefault() * -1);
                    LienSummary.AdjustedInterestCharged -= (payment.CurrentInterest.GetValueOrDefault() * -1);
                    break;
                case "L":
                    LienSummary.MaturityFee -= (payment.LienCost.GetValueOrDefault() * -1);
                    break;
                case "A":
                    LienSummary.AdjustedPrincipalCharged += payment.Principal.GetValueOrDefault();
                    LienSummary.AdjustedInterestCharged += payment.CurrentInterest.GetValueOrDefault();
                    LienSummary.AdjustedPreLienInterestCharged += payment.PreLienInterest.GetValueOrDefault();
                    LienSummary.AdjustedCostsCharged += payment.LienCost.GetValueOrDefault();
                    if (LienSummary.GetNetOwed() > 0)
                    {
                        var paidEvents = NewEvents.GetEvents()
                            .Where(de => de.EventType == TaxBillEventType.PropertyTaxLienPaid).Select(de => de.Id);
                        NewEvents.RemoveEvents(paidEvents);
                    }

                    break;
                default:
                    LienSummary.PrincipalPaid = LienSummary.PrincipalPaid - payment.Principal.GetValueOrDefault();
                    LienSummary.CostsPaid -= payment.LienCost.GetValueOrDefault();
                    LienSummary.InterestPaid -= payment.CurrentInterest.GetValueOrDefault();
                    LienSummary.PreLienInterestPaid -= payment.PreLienInterest.GetValueOrDefault();
                    if (LienSummary.GetNetOwed() > 0)
                    {
                        var paidEvents = NewEvents.GetEvents()
                            .Where(de => de.EventType == TaxBillEventType.PropertyTaxLienPaid).Select(de => de.Id);
                        NewEvents.RemoveEvents(paidEvents);
                    }

                    if (LienSummary.GetNetOwed() <= 0)
                    {
                        AddPaidEvent(payment);
                    }
                    break;
            }
        }

        public void AddNewPayments(Payments payments)
        {
            var paymentList = payments.GetAll();
            foreach (var payment in paymentList)
            {
                payment.BillId = Id;
                payment.BillCode = "L";

                if (payment.PaymentIdentifier == null)
                {
                    payment.PaymentIdentifier = Guid.NewGuid();
                }
            }
            NewPayments.AddPayments(payments.GetAll());
            this.ApplyPayments(payments);
        }

        public void RemovePendingPayment(PaymentRec payment)
        {
            if (payment != null)
            {
                ReversePayment(payment);
                Payments.RemovePayment(payment.PaymentIdentifier.GetValueOrDefault());
                var lastRecordedPayment = Payments.LastRecordedPayment();
                if (lastRecordedPayment != null)
                {
                    InterestAppliedThroughDate = lastRecordedPayment.EffectiveInterestDate;
                }
                else
                {
                    if (RateRecord != null)
                    {
                        var installment = RateRecord.Installments.Where(i => i.InstallmentNumber == 1).FirstOrDefault();
                        if (installment != null)
                        {
                            InterestAppliedThroughDate = installment.InterestStartDate.AddDays(-1);
                        }
                    }
                }
                NewPayments.RemovePayment(payment.PaymentIdentifier.GetValueOrDefault());
            }
        }

        protected void AddPaidEvent(PaymentRec payment)
        {
            if (payment != null)
            {
                if (!NewEvents.GetEvents().Any(de => de.EventType == TaxBillEventType.PropertyTaxLienPaid))
                {                    
                    var taxYear = payment.TaxYear();
                    var billNumber = payment.BillNumber();
                    var paidEvent = new PropertyTaxLienPaid(Id,BillId,Account , taxYear, billNumber, payment.RecordedTransactionDate.GetValueOrDefault(), payment.Teller, payment.PaymentIdentifier.GetValueOrDefault());
                    NewEvents.AddEvent(paidEvent);
                }
            }
        }

        protected void AddPartiallyPaidEvent(PaymentRec payment)
        {
            if (payment != null)
            {
                if (!NewEvents.GetEvents().Any(de => de.EventType == TaxBillEventType.PropertyTaxLienPartiallyPaid))
                {
                    var taxYear = payment.TaxYear();
                    var billNumber = payment.BillNumber();
                    var partialEvent = new PropertyTaxLienPartiallyPaid(Id,BillId,Account,taxYear,billNumber,payment.RecordedTransactionDate.GetValueOrDefault(),payment.Teller,payment.PaymentIdentifier.GetValueOrDefault(),payment.Total());
                    NewEvents.AddEvent(partialEvent);
                }
            }
        }

        public DateTime? OriginalPreviousInterestDate { get; set; }
        public DateTime? OriginalInterestAppliedThroughDate { get; set; }

        public bool HasNewPayments()
        {
            return NewPayments.HasAny();
        }
    }
}