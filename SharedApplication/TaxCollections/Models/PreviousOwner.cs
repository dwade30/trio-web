﻿using System;

namespace SharedApplication.TaxCollections.Models
{
    public partial class PreviousOwner
    {
        public int Id { get; set; }
        public int? Account { get; set; }
        public int? CardId { get; set; }
        public int? PartyId1 { get; set; }
        public int? PartyId2 { get; set; }
        public string Name { get; set; }
        public string SecOwner { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Zip4 { get; set; }
        public DateTime? SaleDate { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}