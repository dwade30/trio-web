﻿using System;
using System.Collections;
using System.Collections.Generic;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Models
{
    public class PropertyTaxBill
    {
        public int ID { get; set; }

        public int? Account { get; set; }
        public DomainEvents<TaxBillEvent> NewEvents { get; private set; }
        public Payments Payments { get; private set; }
        public Payments NewPayments { get; private set; }
        public PropertyTaxBillType BillType { get; set; }
        public int TaxYear { get; set; }
        public int BillNumber { get; set; }
        public PropertyTaxInstallments TaxInstallments { get; private set; }
        public PropertyTaxBillSummary BillSummary { get; private set; }
        public DateTime? InterestAppliedThroughDate { get; set; } = null;
        public DateTime? TransferFromBillingDateFirst { get; set; } = null;
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingAddress3 { get; set; }
        public string CityStateZip { get; set; }
        public string Zip { get; set; }
        public int RateId { get; set; }
        public TaxRateInfo RateRecord { get; set; }
        public int LienID { get; set; }
        public DateTime? PreviousInterestAppliedDate { get; set; } = null;

        
        public PropertyTaxLien Lien { get; set; }
        public bool HasLien()
        {
            return LienID > 0;
        }
        public TaxClub TaxClub { get; set; }
        public int StreetNumber { get; set; } = 0;
        public string Apt { get; set; } = "";
        public string StreetName { get; set; } = "";
        public int TranCode { get; set; } = 0;
        public bool HasTaxClub()
        {
            if (TaxClub != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Use AddPayments when manually setting totals in BillSummary as a snapshot. Use ApplyPayments when using the payments to update totals
        /// </summary>
        /// <param name="payments"></param>
        public void AddPayments(Payments payments)
        {
            if (payments == null)
            {
                return;
            }

            var paymentsList = payments.GetAll();
            foreach (var payment in paymentsList)
            {
                AddPayment(payment);
                if (payment.IsAbatement())
                {
                    ApplyPayment(payment);
                }
            }
        }

        /// <summary>
        /// Use ApplyPayments when using the payments to update totals. Use AddPayments when manually setting totals in BillSummary as a snapshot
        /// </summary>
        /// <param name="payments"></param>
        public void ApplyPayments(Payments payments)
        {
            if (payments == null)
            {
                return;
            }

            var paymentsList = payments.GetAll();
            foreach (var payment in paymentsList)
            {
                AddPayment(payment);
                ApplyPayment(payment);
            }
        }

        protected void AddPayment(PaymentRec payment)
        {
            Payments.AddPayment(payment);
        }
        protected void ApplyPayment(PaymentRec payment)
        {
            if (payment.IsAbatement())
            {
                ApplyAbatement(payment);
            }
            else
            {
                ApplyRegularPayment(payment);
            }
        }

        protected void ApplyAbatement(PaymentRec payment)
        {
            switch (payment.Period.ToLower())
            {
                case "a":
                    TaxInstallments.ApplyAbatementToAllInstallments(payment.Principal ?? 0);
                    break;
                default:                    
                    TaxInstallments.ApplyAbatementToInstallment(payment.Principal ?? 0,Convert.ToInt32(payment.Period));
                    break;
            }

            BillSummary.InterestCharged = BillSummary.InterestCharged - payment.CurrentInterest ?? 0;
            BillSummary.DemandFeesCharged = BillSummary.DemandFeesCharged - payment.LienCost ?? 0;
            ReSummarizeCharges();
        }

        protected void RemoveAbatement(PaymentRec payment)
        {
            switch (payment.Period.ToLower())
            {
                case "a":
                    TaxInstallments.ReverseAbatementFromInstallments(payment.Principal.GetValueOrDefault());
                    BillSummary.InterestCharged += payment.CurrentInterest.GetValueOrDefault();
                    BillSummary.DemandFeesCharged += payment.LienCost.GetValueOrDefault();
                    break;
                default:
                    TaxInstallments.ReverseAbatementFromInstallment(payment.Principal.GetValueOrDefault(),payment.Period.ToIntegerValue());
                    break;
            }
            ReSummarizeCharges();
            Payments.RemovePayment(payment.PaymentIdentifier.GetValueOrDefault());
        }

        protected void RemoveRegularPayment(PaymentRec payment)
        {
            ReverseRegularPayment(payment);
            Payments.RemovePayment(payment.PaymentIdentifier.GetValueOrDefault());
        }

        public void RemovePendingPayment(PaymentRec payment)
        {
            if (payment != null)
            {
                if (payment.IsLien())
                {
                    Lien.RemovePendingPayment(payment);                    
                }
                else
                {
                    if (payment.IsAbatement())
                    {
                        RemoveAbatement(payment);
                    }
                    else
                    {
                        RemoveRegularPayment(payment);
                    }
                    NewPayments.RemovePayment(payment.PaymentIdentifier.GetValueOrDefault());
                }
            }
        }
        protected void ReSummarizeCharges()
        {            
            BillSummary.PrincipalCharged = TaxInstallments.GetEffectiveCharged();
        }
        protected void ApplyRegularPayment(PaymentRec payment)
        {
            switch (payment.Code)
            {
                case "I":
                    this.BillSummary.InterestCharged += (payment.CurrentInterest.GetValueOrDefault() * -1);
                    break;
                default:
                    BillSummary.DemandFeesPaid += payment.LienCost.GetValueOrDefault();
                    BillSummary.InterestPaid += payment.CurrentInterest.GetValueOrDefault();
                    BillSummary.PrincipalPaid += payment.Principal.GetValueOrDefault();
                    break;
            }

            if (payment.EffectiveInterestDate.HasValue)
            {
                InterestAppliedThroughDate = payment.EffectiveInterestDate;
            }
        }

        protected void ReverseRegularPayment(PaymentRec payment)
        {
            switch (payment.Code)
            {
                case "I":
                    this.BillSummary.InterestCharged -= (payment.CurrentInterest.GetValueOrDefault() * -1);
                    break;
                default:
                    BillSummary.DemandFeesPaid -= payment.LienCost.GetValueOrDefault();
                    BillSummary.InterestPaid -= payment.CurrentInterest.GetValueOrDefault();
                    BillSummary.PrincipalPaid -= payment.Principal.GetValueOrDefault();
                    break;
            }
        }

        public string OwnerNames()
        {
            if (!string.IsNullOrWhiteSpace(Name2))
            {
                return Name1 + " & " + Name2;
            }

            return Name1;
        }


        public int YearBill()
        {
            return (TaxYear * 10) + BillNumber;
        }

        public string FormattedYearBill()
        {

            return TaxYear.ToString() + "-" + BillNumber.ToString();
        }

        public IEnumerable<decimal> GetInstallmentsDue(decimal extraInterest)
        {
            var amounts = new List<decimal>();
            decimal totalExtra = BillSummary.DemandFeesCharged + BillSummary.InterestCharged + extraInterest;
            decimal totalPaid = BillSummary.PrincipalPaid + BillSummary.InterestPaid + BillSummary.DemandFeesPaid;
            var installments = TaxInstallments.GetInstallments();
            foreach (var installment in installments)
            {
                var periodAmount = installment.AdjustedAmount + totalExtra;
                totalExtra = 0;
                if (periodAmount >= totalPaid)
                {
                    periodAmount = periodAmount - totalPaid;
                    totalPaid = 0;
                }
                else
                {
                    totalPaid = totalPaid - periodAmount;
                    periodAmount = 0;
                }

                if (periodAmount < 0)
                {
                    periodAmount = 0;
                }
                amounts.Add(periodAmount);
            }

            return amounts;
        }
        public PropertyTaxBill()
        {
            Payments = new Payments();
            NewPayments = new Payments();
            TaxInstallments = new PropertyTaxInstallments();
            BillSummary = new PropertyTaxBillSummary();
            NewEvents = new DomainEvents<TaxBillEvent>();
        }

        public void AddNewPayments(Payments payments)
        {
            if (HasLien())
            {
                Lien.AddNewPayments(payments);
                return;
            }
            var paymentList = payments.GetAll();
            foreach (var payment in paymentList)
            {
                payment.BillId = ID;
                if (BillType == PropertyTaxBillType.Personal)
                {
                    payment.BillCode = "P";
                }               
                else
                {
                    payment.BillCode = "R";
                }

                payment.Year = YearBill();
                if (payment.PaymentIdentifier == null)
                {
                    payment.PaymentIdentifier = Guid.NewGuid();
                }
            }
            NewPayments.AddPayments(payments.GetAll());
            this.ApplyPayments(payments);
        }

        public bool HasPayment(Guid paymentIdentifier)
        {
            if (Payments.HasPayment(paymentIdentifier))
            {
                return true;
            }

            if (HasLien())
            {
                if (Lien.Payments.HasPayment(paymentIdentifier))
                {
                    return true;
                }
            }
            return false;
        }

        public DateTime? OriginalPreviousInterestDate { get; set; }
        public DateTime? OriginalInterestAppliedThroughDate { get; set; }

        public void ResetInterestDates()
        {
            InterestAppliedThroughDate = OriginalInterestAppliedThroughDate;
            PreviousInterestAppliedDate = OriginalPreviousInterestDate;
        }

        public bool HasNewPayments()
        {
            if (HasLien())
            {
                return Lien.HasNewPayments();
            }

            return NewPayments.HasAny();
        }

        public bool HasBeenBilled()
        {
            return RateId != 0;
        }
    }
}