﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharedApplication.Extensions;

namespace SharedApplication.TaxCollections
{
    public class TaxServiceExtractRecord
    {
        public int Year { get; set; } = 0;
        public int Period { get; set; } = 0;
        public string MapLot { get; set; } = "";
        public string Name { get; set; } = "";
        public string LocationNumber { get; set; } = "";
        public string LocationStreet { get; set; } = "";
        public decimal Acres { get; set; } = 0;
        public int SquareFootage { get; set; } = 0;
        public int BuildingValue { get; set; } = 0;
        public string Book { get; set; } = "";
        public string Page { get; set; } = "";
        public int Account { get; set; } = 0;
        public decimal Abatement { get; set; } = 0;
        public decimal Exemption { get; set; } = 0;
        public decimal LienAmount { get; set; } = 0;
        public decimal LienBalance { get; set; } = 0;
        public decimal BettermentAmount { get; set; } = 0;
        public decimal BettermentBalance { get; set; } = 0;
        public decimal Tax1 { get; set; } = 0;
        public decimal Tax1Balance { get; set; } = 0;
        public decimal Tax1InterestDue { get; set; } = 0;
        public decimal Tax2 { get; set; } = 0;
        public decimal Tax2Balance { get; set; } = 0;
        public decimal Tax2InterestDue { get; set; } = 0;
        public decimal Tax3 { get; set; } = 0;
        public decimal Tax3Balance { get; set; } = 0;
        public decimal Tax3InterestDue { get; set; } = 0;
        public decimal Tax4 { get; set; } = 0;
        public decimal Tax4Balance { get; set; } = 0;
        public decimal Tax4InterestDue { get; set; } = 0;
        public decimal TotalDemandFeesDue { get; set; } = 0;
        public decimal TotalInterestDue { get; set; } = 0;
        public decimal TotalAmountDue { get; set; } = 0;
        public string PaidFlag { get; set; } = "";
        public string ExemptFlag { get; set; } = "";
        public string PriorYearDelinquent { get; set; } = "";
        public string LienIndicator { get; set; } = "";
        public decimal TotalDiscount { get; set; } = 0;
        public decimal NetAmountDue { get; set; } = 0;

        
    }

    public static class TaxServiceExtractExtension
    {

        public static string ToFixedLengthRecordFormat(
            this TaxServiceExtractRecord record)
        {
            try
            {
				StringBuilder sb = new StringBuilder();

				sb.Append(record.Year.ToString().PadLeft(4, '0'));
				sb.Append(record.Period.ToString());
				sb.Append(record.MapLot.PadRight(20));
				sb.Append((record.Name.Length <= 40 ? record.Name : record.Name.Left(40)).PadRight(40));
                var locationNumber = record.LocationNumber != "0" ? record.LocationNumber : "";
				sb.Append((locationNumber.Length <= 5 ? locationNumber : locationNumber.Left(5)).PadLeft(5));
				sb.Append(record.LocationStreet.PadRight(30));
				sb.Append((record.Acres * 100).ToInteger().ToString().PadLeft(8, '0'));
				sb.Append(record.SquareFootage.ToString().PadLeft(8, '0'));
				sb.Append(record.BuildingValue.ToString().PadLeft(9, '0'));
				sb.Append(record.Book.PadLeft(5, '0'));
				sb.Append(record.Page.PadLeft(5, '0'));
				sb.Append(record.Account.ToString().PadLeft(10, '0'));
				sb.Append(record.Abatement.ToInteger().ToString().PadLeft(11, '0'));
				sb.Append(record.Exemption.ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.LienAmount * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.LienBalance * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.BettermentAmount * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.BettermentBalance * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax1 * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax1Balance * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax1InterestDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax2 * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax2Balance * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax2InterestDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax3 * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax3Balance * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax3InterestDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax4 * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax4Balance * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.Tax4InterestDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.TotalInterestDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.TotalDemandFeesDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append((record.TotalAmountDue * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append(record.PaidFlag.PadLeft(2));
				sb.Append(record.ExemptFlag.PadLeft(2));
				sb.Append(record.PriorYearDelinquent);
				sb.Append(record.LienIndicator.PadRight(8));
				sb.Append((record.TotalDiscount * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append(((record.Tax1 + record.Tax2 + record.Tax3 + record.Tax4) * 100).ToInteger().ToString().PadLeft(11, '0'));
				sb.Append(new string('0', 11));
				return sb.ToString();
			}
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

		public static string ToCommaDelimitedRecordFormat(
			this TaxServiceExtractRecord record)
		{
			StringBuilder sb = new StringBuilder();

			sb.Append(record.Year.ToString().PadLeft(4, '0') + "\",");
			sb.Append(record.Period.ToString() + ",");
			sb.Append("\"" + record.MapLot.PadRight(20) + "\",");
			sb.Append("\"" + (record.Name.Length <= 40 ? record.Name : record.Name.Left(40)).PadRight(40) + "\",");
            var locationNumber = record.LocationNumber != "0" ? record.LocationNumber : "";
			sb.Append("\"" + (locationNumber.Length <= 5 ? locationNumber : locationNumber.Left(5)).PadLeft(5) + "\",");
			sb.Append("\"" + record.LocationStreet.PadRight(30) + "\",");
			sb.Append("\"" + (record.Acres * 100).ToInteger().ToString().PadLeft(8, '0') + "\",");
			sb.Append("\"" + record.SquareFootage.ToString().PadLeft(8, '0') + "\",");
			sb.Append("\"" + record.BuildingValue.ToString().PadLeft(9, '0') + "\",");
			sb.Append("\"" + record.Book.PadLeft(5, '0') + "\",");
			sb.Append("\"" + record.Page.PadLeft(5, '0') + "\",");
			sb.Append("\"" + record.Account.ToString().PadLeft(10, '0') + "\",");
			sb.Append("\"" + record.Abatement.ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + record.Exemption.ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.LienAmount * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.LienBalance * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.BettermentAmount * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.BettermentBalance * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax1 * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax1Balance * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax1InterestDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax2 * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax2Balance * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax2InterestDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax3 * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax3Balance * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax3InterestDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax4 * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax4Balance * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.Tax4InterestDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.TotalInterestDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.TotalDemandFeesDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.TotalAmountDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + record.PaidFlag.PadLeft(2) + "\",");
			sb.Append("\"" + record.ExemptFlag.PadLeft(2) + "\",");
			sb.Append("\"" + record.PriorYearDelinquent + "\",");
			sb.Append("\"" + record.LienIndicator.PadRight(8) + "\",");
			sb.Append("\"" + (record.TotalDiscount * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + (record.NetAmountDue * 100).ToInteger().ToString().PadLeft(11, '0') + "\",");
			sb.Append("\"" + new string('0', 11));

			return sb.ToString();
		}

		public static IEnumerable<string> ToFixedLengthRecordFormats(
            this IEnumerable<TaxServiceExtractRecord> records)
        {
            if (records != null)
            {
                var result = new List<string>();
                result.AddRange(records.Select(i => i.ToFixedLengthRecordFormat()));
                return result;
            }
            return new List<string>();
        }
    }
}