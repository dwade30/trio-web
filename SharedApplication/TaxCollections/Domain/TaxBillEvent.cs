﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Domain
{
    public abstract class TaxBillEvent : DomainEvent
    {
        public int Account { get; protected set; }
        public int TaxYear { get; protected set; }
        public int BillNumber { get; protected set; }
        public int BillId { get; protected set; }
        public PropertyTaxBillType BillType { get; protected set; }
        public TaxBillEventType EventType { get; protected set; }
    }
}