﻿using System;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Domain
{
    public class PropertyTaxLienPartiallyPaid : TaxBillEvent
    {
        public int LienId { get; }
        public DateTime DatePaid { get; }
        public string Teller { get; }
        public Guid PaymentIdentifier { get; }
        public decimal PaymentAmount { get; }

        public PropertyTaxLienPartiallyPaid(int lienId, int billId, int account,int taxYear, int billNumber, DateTime datePaid,string teller,Guid paymentIdentifier, decimal paymentAmount)
        {

            LienId = lienId;
            BillId = billId;
            Account = account;
            BillType = PropertyTaxBillType.Real;
            TaxYear = taxYear;
            BillNumber = billNumber;
            DatePaid = datePaid;
            Teller = teller;
            PaymentIdentifier = paymentIdentifier;
            PaymentAmount = paymentAmount;
            EventType = TaxBillEventType.PropertyTaxLienPartiallyPaid;
        }
    }
}