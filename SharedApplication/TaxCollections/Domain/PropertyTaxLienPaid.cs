﻿using System;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.Domain
{
    public class PropertyTaxLienPaid : TaxBillEvent
    {
        public int LienId { get; }
        public DateTime DatePaid { get; }
        public string Teller { get; }
        public Guid PaymentIdentifier { get; }
        public PropertyTaxLienPaid(int lienId, int billId, int account, int taxYear, int billNumber, DateTime datepaid, string teller,Guid paymentIdentifier)
        {
            LienId = lienId;
            BillId = billId;
            Account = account;
            BillType = PropertyTaxBillType.Real;
            TaxYear = taxYear;
            BillNumber = billNumber;
            DatePaid = datepaid;
            Teller = teller;
            PaymentIdentifier = paymentIdentifier;
            EventType = TaxBillEventType.PropertyTaxLienPaid;
        }
    }
}