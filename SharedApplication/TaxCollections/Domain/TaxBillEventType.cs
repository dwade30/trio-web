﻿namespace SharedApplication.TaxCollections.Domain
{
    public enum TaxBillEventType
    {
        PropertyTaxLienPaid = 0,
        PropertyTaxLienPartiallyPaid = 1
    }
}