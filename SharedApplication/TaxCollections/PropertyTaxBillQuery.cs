﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections
{
    public class PropertyTaxBillQuery
    {
        public int? Id { get; set; }
        public PropertyTaxBillType? BillType { get; set; }
        public bool? IsPaid { get; set; }
        public int? TaxYear { get; set; }
        public bool? IsLiened { get; set; }
    }
}
