﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public class PropertyTaxPaymentInstallments
    {
        private List<PropertyTaxPaymentInstallment> installments = new List<PropertyTaxPaymentInstallment>();

        public PropertyTaxPaymentInstallments()
        {

        }

        public PropertyTaxPaymentInstallments(IEnumerable<PropertyTaxPaymentInstallment> installments)
        {
            this.installments.AddRange(installments);
        }

        public decimal TotalAdjusted()
        {
            return installments.Sum(i => i.AdjustedAmount);
        }

        public decimal NetOwed()
        {
            return installments.Sum(i => i.NetOwed);
        }

        public void AddPayment(decimal amount)
        {
            var amountLeft = amount;
            foreach (var installment in installments)
            {
                if (installment.NetOwed > 0 && amountLeft >= 0)
                {
                    if (installment.NetOwed > amountLeft)
                    {
                        installment.AmountPaid += amountLeft;
                        amountLeft = 0;
                    }
                    else
                    {
                        var difference = amountLeft - installment.NetOwed;
                        installment.AmountPaid += installment.NetOwed;
                        amountLeft = difference;
                    }
                }
            }
        }

    public IEnumerable<PropertyTaxPaymentInstallment> GetInstallments()
        {
            return installments;
        }
    }
}