﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class ShowREBookPageReportFromCollections : Command
    {
        public int Account { get; set; }

        public ShowREBookPageReportFromCollections(int account)
        {
            Account = account;
        }
    }
}