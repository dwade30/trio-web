﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Interfaces
{
    public interface IPropertyTaxBillQueryHandler
    {
        PropertyTaxBill Get(int id);
        IEnumerable<PropertyTaxBill> GetAll();
        IEnumerable<PropertyTaxBill> Find(Expression<Func<BillingMaster, bool>> predicate);
    }
}
