﻿namespace SharedApplication.TaxCollections.Interfaces
{
    public interface ITaxRateChoiceCommand : ICommandObject
    {
        void SetRateIdList(string rateIdList);
    }
}