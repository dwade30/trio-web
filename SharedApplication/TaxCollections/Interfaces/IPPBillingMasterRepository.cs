﻿using SharedApplication;

namespace SharedApplication.TaxCollections.Interfaces
{
    public interface IPPBillingMasterRepository: IBillingMasterRepository, IPurge
    {
    }
}
