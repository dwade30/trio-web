﻿using SharedApplication;

namespace SharedApplication.TaxCollections.Interfaces
{
    public interface IREBillingMasterRepository : IBillingMasterRepository, IPurge
    {
 
    }
}
