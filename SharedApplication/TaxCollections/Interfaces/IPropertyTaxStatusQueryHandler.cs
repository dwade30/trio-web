﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.StatusList;

namespace SharedApplication.TaxCollections.Interfaces
{
    public interface IPropertyTaxStatusQueryHandler
    {
        RealEstateAccountBill GetReal(int id);
       // RealEstateAccountBill GetRealAndCalculate(int id, DateTime asOfDate);
        PersonalPropertyAccountBill GetPersonal(int id,bool calculate);
        IEnumerable<RealEstateAccountBill> GetAllReal();
        IEnumerable<RealEstateAccountBill> FindReal(Expression<Func<RealEstateBillAccountPartyView, bool>> predicate);

        IEnumerable<RealEstateAccountBill> FindRealAsOf(Expression<Func<RealEstateBillAccountPartyView, bool>> predicate,
            bool snapshotAsOfEffectiveDate, DateTime? effectiveDate);

        Queue<RealEstateAccountBill> FindQueueOfRealAsOf(
            Expression<Func<RealEstateBillAccountPartyView, bool>> predicate, bool snapshotAsOfEffectiveDate,
            DateTime? effectiveDate);
        IEnumerable<PersonalPropertyAccountBill> FindPersonal(Expression<Func<PersonalPropertyBillAccountPartyView, bool>> predicate);

        IEnumerable<PersonalPropertyAccountBill> FindPersonalAsOf(
            Expression<Func<PersonalPropertyBillAccountPartyView, bool>> predicate, bool snapshotAsOfEffectiveDate,
            DateTime? effectiveDate);
    }
}