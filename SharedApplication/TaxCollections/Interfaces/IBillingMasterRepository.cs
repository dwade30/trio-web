﻿using System.Collections.Generic;
using SharedApplication;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.Interfaces
{
    public interface IBillingMasterRepository : IRepository<BillingMaster>
    {
        IEnumerable<int> GetTaxYears();
    }
}
