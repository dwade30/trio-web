﻿using System;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public interface IPropertyTaxBillCalculator
    {
        TaxBillCalculationSummary CalculateBill(PropertyTaxBill bill,DateTime asOfDate);
    }
}