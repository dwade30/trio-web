﻿using System;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.StatusList
{
    public class PropertyTaxStatusListDetailItem
    {

        public int Account { get; set; } = 0;
        public int TaxYear { get; set; } = 0;

        public int BillNumber { get; set; } = 1;
        //public string MapLot { get; set; } = 0;
        public int BillYear => TaxYear > 0 ? (
            TaxYear.ToString() + BillNumber.ToString()).ToIntegerValue() : 0;
        public PropertyTaxBillType BillType { get; set; }
        public string Name1 { get; set; } = "";
        public string Name2 { get; set; } = "";
        public string Address1 { get; set; } = "";
        public string Address2 { get; set; } = "";
        public string Address3 { get; set; } = "";

        public int StreetNumber { get; set; } = 0;

        public string StreetName { get; set; } = "";

        public int TranCode { get; set; } = 0;
        public decimal TaxDue1 { get; set; } = 0;
        public decimal TaxDue2 { get; set; } = 0;
        public decimal TaxDue3 { get; set; } = 0;
        public decimal TaxDue4 { get; set; } = 0;

        public string Location()
        {
            return StreetNumber > 0 ? (StreetNumber + " " + StreetName).Trim() : StreetName.Trim();
        }

        public RealEstatePropertyTaxStatusListDetailItem RealProperty { get; set; } = null;
        public PersonalPropertyTaxStatusListDetailItem PersonalProperty { get; set; } = null;
    }

    public class RealEstatePropertyTaxStatusListDetailItem 
    {
        public string MapLot { get; set; }
        public int LandValue { get; set; } = 0;
        public int BuildingValue { get; set; } = 0;
        public int ExemptValue { get; set; } = 0;
    }

    public class PersonalPropertyTaxStatusListDetailItem 
    {

    }
}