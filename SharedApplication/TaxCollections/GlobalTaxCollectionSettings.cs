﻿namespace SharedApplication.TaxCollections
{
    public class GlobalTaxCollectionSettings
    {
        public int DaysInAYear { get; set; } = 365;
        public decimal OverPaymentInterestRate { get; set; } = 0;
        public decimal DiscountRate { get; set; } = 0;
        public bool ShowLastAccountInCR { get; set; } = false;
        public bool DisableAutoPaymentFill { get; set; } = false;
        public string AccountDetailAssessmentSetting { get; set; } = "";
        public bool ShowReceiptNumberOnDetail { get; set; } = false;
        public bool AutoChangeRecordedTransactionDateWithEffectiveDate { get; set; } = false;
    }
}