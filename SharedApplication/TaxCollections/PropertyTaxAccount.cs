﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{

    public abstract class PropertyTaxAccount
    {
        protected List<PropertyTaxBill> bills = new List<PropertyTaxBill>();
        public int Account { get; set; } = 0;
        public abstract int TownId { get; }
        public PropertyTaxBillType BillType { get; protected set; }

        public MailingAddress MailingAddress { get; set; } = new MailingAddress();
        public void AddPayments(Payments payments)
        {
            if (payments == null)
            {
                return;
            }
            var paymentsList = payments.GetAll();

            foreach (var payment in paymentsList)
            {
                PropertyTaxBill bill = bills.FirstOrDefault(b => b.BillType == payment.BillType() && ((b.ID == payment.BillId && !payment.IsLien()) ||
                                                                                          (b.LienID == payment.BillId && payment.IsLien())));
                if (bill != null)
                {
                    if (payment.IsLien())
                    {
                        bill.Lien.AddPayments(payment.ToPayments());
                    }
                    else
                    {
                        bill.AddPayments(payment.ToPayments());
                    }

                }
                else
                {
                    if (payment.IsPrepayment() && payment.BillId == 0)
                    {
                        bill = new PropertyTaxBill()
                        {
                            Account = Account,
                            BillNumber = 1,
                            BillType = BillType,
                            ID = 0,
                            TaxYear = payment.TaxYear(),
                            MailingAddress1 = MailingAddress.Address1,
                            MailingAddress2 = MailingAddress.Address2,
                            MailingAddress3 = MailingAddress.Address3,
                            Zip = MailingAddress.Zip,
                            CityStateZip = (MailingAddress.City + " " + MailingAddress.State + " " + MailingAddress.Zip).Trim(),
                            Name1 = Name1(),
                            Name2 = Name2(),
                            BillSummary = { DemandFeesPaid = 0, InterestPaid = 0, InterestCharged = 0, PrincipalCharged = 0, PrincipalPaid = 0, DemandFeesCharged = 0 }
                        };
                        bill.ApplyPayments(payment.ToPayments());
                        bills.Add(bill);
                    }
                }
            }
        }

        public abstract PropertyTaxBill CreateBill(int yearBill);
        public void ApplyPayments(Payments payments)
        {
            if (payments == null)
            {
                return;
            }

            var paymentsList = payments.GetAll();
            foreach (var payment in paymentsList)
            {
                PropertyTaxBill bill = bills.FirstOrDefault(b => b.BillType == payment.BillType() && ((b.ID == payment.BillId.GetValueOrDefault() && !payment.IsLien()) ||
                                                                                                      (b.LienID == payment.BillId.GetValueOrDefault() && payment.IsLien())));
                if (bill != null)
                {
                    if (payment.IsLien())
                    {
                        bill.Lien.ApplyPayments(payment.ToPayments());
                    }
                    else
                    {
                        bill.ApplyPayments(payment.ToPayments());
                    }
                }
                else
                {
                    if (payment.IsPrepayment() && payment.BillId.GetValueOrDefault() == 0)
                    {
                        bill = CreateBill(payment.Year.GetValueOrDefault());
                        if (!payment.IsAbatement() && !payment.IsRefundedAbatement())
                        {
                            bill.BillSummary.PrincipalPaid = payment.Principal.GetValueOrDefault();
                        }

                        bill.ApplyPayments(payment.ToPayments());
                    }
                }
            }
        }

        public void AddNewPayments(Payments payments)
        {
            if (payments == null)
            {
                return;
            }
            var paymentsList = payments.GetAll();
            foreach (var payment in paymentsList)
            {
                PropertyTaxBill bill = bills.FirstOrDefault(b => b.BillType == payment.BillType() && ((b.ID == payment.BillId && !payment.IsLien()) ||
                                                                                                     (b.LienID == payment.BillId && payment.IsLien())));
                if (bill != null)
                {
                    bill.AddNewPayments(payment.ToPayments());
                }
                else
                {
                    if (payment.IsPrepayment() && payment.BillId == 0)
                    {
                        bill = CreateBill(payment.TaxYear());
                        //if (!payment.IsAbatement() && !payment.IsRefundedAbatement())
                        //{
                        //    bill.BillSummary.PrincipalPaid = payment.Principal.GetValueOrDefault();
                        //}

                        bill.AddNewPayments(payment.ToPayments());
                        bills.Add(bill);
                        bills = bills.OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber).ToList();
                    }
                }
            }
        }

        public IQueryable<PropertyTaxBill> Bills { get => bills.AsQueryable(); }
        public void AddBills(IEnumerable<PropertyTaxBill> taxBills)
        {
            if (taxBills != null && taxBills.Any())
            {
                bills.AddRange(taxBills);
            }
        }

        public void InsertBills(IEnumerable<PropertyTaxBill> taxBills)
        {
            if (taxBills != null && taxBills.Any())
            {
                bills.InsertRange(0, taxBills);
            }
        }

        public abstract int LocationNumber();
        public abstract string LocationStreet();

        public abstract string Name1();
        public abstract string Name2();
        public string OwnerNames()
        {
            if (!string.IsNullOrWhiteSpace(Name2()))
            {
                return Name1() + " & " + Name2();
            }

            return Name1();
        }
        public Payments GetPendingPayments()
        {
            var payments = new Payments();
            foreach (var bill in bills)
            {
                if (!bill.HasLien())
                {
                    payments.AddPayments(bill.NewPayments);
                }
                else
                {
                    payments.AddPayments(bill.Lien.NewPayments);
                }
            }
            return payments;
        }
        public Payments GetPendingPayments(int billNumber)
        {
            var payments = new Payments();
            var bill = bills.Where(x => x.TaxYear.ToString() + x.BillNumber.ToString() == billNumber.ToString()).FirstOrDefault();

            if (bill != null)
            {
                if (!bill.HasLien())
                {
                    payments.AddPayments(bill.NewPayments);
                }
                else
                {
                    payments.AddPayments(bill.Lien.NewPayments);
                }
            }
            return payments;
        }
        public DomainEvents<TaxBillEvent> GetNewEvents()
        {
            var newEvents = new DomainEvents<TaxBillEvent>();
            foreach (var bill in bills)
            {

                if (!bill.HasLien())
                {
                    newEvents.AddEvents(bill.NewEvents);
                }
                else
                {
                    newEvents.AddEvents(bill.Lien.NewEvents);
                }
            }

            return newEvents;
        }
    }

    public class RealEstateTaxAccount : PropertyTaxAccount
    {       
        public RealEstateAccountBriefInfo AccountBriefInfo { get;  } = new RealEstateAccountBriefInfo();
        public RealEstateAccountTotals AccountTotals { get; } = new RealEstateAccountTotals();

        public RealEstateAccountTotals LatestBillTotals { get; } = new RealEstateAccountTotals();
        public RealEstateTaxAccount()
        {
            BillType = PropertyTaxBillType.Real;
        }

        public override int TownId { get => AccountBriefInfo.TownId; }

        public override string Name1()
        {
            return AccountBriefInfo.DeedName1;
        }

        public override string Name2()
        {
            return AccountBriefInfo.DeedName2;
        }

        public override string LocationStreet()
        {
            return AccountBriefInfo.LocationStreet;
        }


        public override int LocationNumber()
        {
            return AccountBriefInfo.LocationNumber;
        }

        public override PropertyTaxBill CreateBill(int taxYear)
        {
            return new RealEstateTaxBill()
            {
                Account = Account,
                BillNumber = 1,
                BillType = BillType,
                ID = 0,
                TaxYear = taxYear,
                MailingAddress1 = MailingAddress.Address1,
                MailingAddress2 = MailingAddress.Address2,
                MailingAddress3 = MailingAddress.Address3,
                MapLot = AccountBriefInfo.MapLot,
                StreetName = LocationStreet(),
                StreetNumber = LocationNumber(),
                    Zip = MailingAddress.Zip,
                CityStateZip = (MailingAddress.City + " " + MailingAddress.State + " " + MailingAddress.Zip).Trim(),
                Name1 = Name1(),
                Name2 = Name2(),
                BillSummary = { DemandFeesPaid = 0, InterestPaid = 0, InterestCharged = 0, PrincipalCharged = 0, PrincipalPaid = 0, DemandFeesCharged = 0 }

            };
        }

        public void FillBriefInfo(RealEstateAccountBriefInfo briefInfo)
        {
            AccountBriefInfo.TownId = briefInfo.TownId;
            AccountBriefInfo.Account = briefInfo.Account;
            AccountBriefInfo.DeedName2 = briefInfo.DeedName2;
            AccountBriefInfo.DeedName1 = briefInfo.DeedName1;
            AccountBriefInfo.Deleted = briefInfo.Deleted;
            AccountBriefInfo.IsTaxAcquired = briefInfo.IsTaxAcquired;
            AccountBriefInfo.LocationNumber = briefInfo.LocationNumber;
            AccountBriefInfo.LocationStreet = briefInfo.LocationStreet;
            AccountBriefInfo.MapLot = briefInfo.MapLot;
            AccountBriefInfo.PartyId1 = briefInfo.PartyId1;
            AccountBriefInfo.PartyId2 = briefInfo.PartyId2;
            AccountBriefInfo.Reference1 = briefInfo.Reference1;
            AccountBriefInfo.Reference2 = briefInfo.Reference2;
        }
    }

    public class PersonalPropertyTaxAccount : PropertyTaxAccount
    {
        public PersonalPropertyAccountBriefInfo AccountBriefInfo { get; } = new PersonalPropertyAccountBriefInfo();
        public PersonalPropertyAccountTotals AccountTotals { get; } = new PersonalPropertyAccountTotals();
        public PersonalPropertyAccountTotals LatestBillTotals { get; }  = new PersonalPropertyAccountTotals();
        public PersonalPropertyTaxAccount()
        {
            BillType = PropertyTaxBillType.Personal;
        }

        public override int TownId { get => AccountBriefInfo.TownId; }

        public override string Name1()
        {
            return AccountBriefInfo.Name;
        }

        public override string Name2()
        {
            return "";
        }

        public override string LocationStreet()
        {
            return AccountBriefInfo.LocationStreet;
        }

        public override int LocationNumber()
        {
            return AccountBriefInfo.LocationNumber;
        }

        public override PropertyTaxBill CreateBill(int taxYear)
        {
            return new PersonalPropertyTaxBill()
            {
                Account = Account,
                BillNumber = 1,
                BillType = BillType,
                ID = 0,
                TaxYear = taxYear,
                MailingAddress1 = MailingAddress.Address1,
                MailingAddress2 = MailingAddress.Address2,
                MailingAddress3 = MailingAddress.Address3,
                StreetName = LocationStreet(),
                StreetNumber = LocationNumber(),
                Zip = MailingAddress.Zip,
                CityStateZip = (MailingAddress.City + " " + MailingAddress.State + " " + MailingAddress.Zip).Trim(),
                Name1 = Name1(),
                Name2 = Name2(),
                BillSummary = { DemandFeesPaid = 0, InterestPaid = 0, InterestCharged = 0, PrincipalCharged = 0, PrincipalPaid = 0, DemandFeesCharged = 0 }
            };
        }

        public void FillBriefInfo(PersonalPropertyAccountBriefInfo briefInfo)
        {
            AccountBriefInfo.Account = briefInfo.Account;
            AccountBriefInfo.Deleted = briefInfo.Deleted;
            AccountBriefInfo.LocationNumber = briefInfo.LocationNumber;
            AccountBriefInfo.LocationStreet = briefInfo.LocationStreet;
            AccountBriefInfo.Name = briefInfo.Name;
            AccountBriefInfo.PartyId = briefInfo.PartyId;
            AccountBriefInfo.TownId = briefInfo.TownId;
        }
    }

}