﻿using System;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public abstract class PropertyTaxAccountBill
    {
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
        public  int Account { get; set; }

        public virtual Payments Payments
        {
            get => TaxBill != null ? TaxBill.Payments : new Payments();
        }
        public abstract PropertyTaxBill TaxBill { get; }

        public int StreetNumber { get; set; } = 0;
        public string StreetName { get; set; } = "";
        public string Apt { get; set; } = "";
        public string Location => StreetNumber > 0 ? (StreetNumber + Apt + " " + StreetName).Trim() : StreetName;
        public Guid? OwnerGuid { get; set; }
        public int? OwnerPartyType { get; set; } = 0;
        public string OwnerFirstName { get; set; } = "";
        public string OwnerMiddleName { get; set; } = "";
        public string OwnerLastName { get; set; } = "";
        public string OwnerDesignation { get; set; } = "";
        public string OwnerEmail { get; set; } = "";
        public string OwnerAddress1 { get; set; } = "";
        public string OwnerAddress2 { get; set; } = "";
        public string OwnerAddress3 { get; set; } = "";
        public string OwnerCity { get; set; } = "";
        public string OwnerState { get; set; } = "";
        public string OwnerZip { get; set; } = "";

        public string OwnerFullName => (((OwnerFirstName + " " + OwnerMiddleName).Trim() + " " + OwnerLastName).Trim() +
                                        " " + OwnerDesignation).Trim();

        public string OwnerFullNameLastFirst
        {
            get
            {
                if (OwnerLastName.HasText())
                {
                    return (((OwnerLastName + ", " + OwnerFirstName).Trim() + " " + OwnerMiddleName).Trim() + " " +
                            OwnerDesignation).Trim();
                }
                else
                {
                    return OwnerFullName;
                }
            }
        }

        public bool IsDeleted { get; set; } = false;
        public decimal NewInterest { get; set; }
    }
}