﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CentralData;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections
{
    public class GetGroupCalculationsHandler : CommandHandler<GetGroupCalculations,IEnumerable<TaxBillCalculationSummary>>
    {
        private ICentralDataContext cdContext;
        private IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary> accountQueryHandler;
        public GetGroupCalculationsHandler(ICentralDataContext cdContext,  IQueryHandler<TaxAccountCalculationSummaryQuery, TaxBillCalculationSummary> accountQueryHandler)
        {
            this.cdContext = cdContext;
            this.accountQueryHandler = accountQueryHandler;
        }
        protected override IEnumerable<TaxBillCalculationSummary> Handle(GetGroupCalculations command)
        {
            var summaries = new List<TaxBillCalculationSummary>();

            var associations = cdContext.ModuleAssociations.Where(m => m.GroupNumber.GetValueOrDefault() == command.GroupNumber).ToList();
            foreach (var association in associations)
            {
                if (association.Module.ToLower() == "pp" || association.Module.ToLower() == "re")
                {
                    TaxBillCalculationSummary accountSummary;
                    PropertyTaxBillType billType;
                    if (association.Module.ToLower() == "pp")
                    {
                        billType = PropertyTaxBillType.Personal;
                    }
                    else
                    {
                        billType = PropertyTaxBillType.Real;
                    }
                    accountSummary = accountQueryHandler.ExecuteQuery(
                        new TaxAccountCalculationSummaryQuery(association.Account.GetValueOrDefault(),
                            billType, DateTime.Today));
                    summaries.Add(accountSummary);
                }
            }
            return summaries;
        }        
    }
}