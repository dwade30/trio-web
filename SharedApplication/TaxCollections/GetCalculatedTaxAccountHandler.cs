﻿using System;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Interfaces;

namespace SharedApplication.TaxCollections
{
    public class GetCalculatedTaxAccountHandler : CommandHandler<GetCalculatedTaxAccount,CalculatedTaxAccount>
    {
        private IPropertyTaxBillCalculator taxCalculator;
        private IPropertyTaxBillQueryHandler billQueryHandler;
        public GetCalculatedTaxAccountHandler(IPropertyTaxBillCalculator taxCalculator, IPropertyTaxBillQueryHandler billQueryHandler)
        {
            this.taxCalculator = taxCalculator;
            this.billQueryHandler = billQueryHandler;
        }
        protected override CalculatedTaxAccount Handle(GetCalculatedTaxAccount command)
        {
            PropertyTaxAccount taxAccount;
            if (command.BillType == PropertyTaxBillType.Personal)
            {
                taxAccount = GetPPAccount(command.Account);
            }
            else
            {
                taxAccount = GetREAccount(command.Account);
            }

            if (taxAccount == null)
            {
                return null;
            }

            return CalculateAccount(taxAccount,command.EffectiveDate);
        }

        private RealEstateTaxAccount GetREAccount(int account)
        {
            var bills = billQueryHandler.Find(b => b.Account == account && b.BillingType == "RE").ToList();
            var reAccount = new RealEstateTaxAccount();
            reAccount.Account = account;
            reAccount.AddBills(bills);
            return reAccount;
        }

        private PersonalPropertyTaxAccount GetPPAccount(int account)
        {
            var bills = billQueryHandler.Find(b => b.Account == account && b.BillingType == "PP").ToList();
            var ppAccount = new PersonalPropertyTaxAccount();
            ppAccount.Account = account;
            ppAccount.AddBills(bills);
            return ppAccount;
        }

        private CalculatedTaxAccount CalculateAccount(PropertyTaxAccount taxAccount,DateTime effectiveDate)
        {
            var calculatedAccount = new CalculatedTaxAccount();
            calculatedAccount.TaxAccount = taxAccount;
            calculatedAccount.AccountSummary = new TaxBillCalculationSummary(taxAccount.Account, taxAccount.BillType);
            var bills = taxAccount.Bills;
            foreach (var bill in bills)
            {
                var summary = taxCalculator.CalculateBill(bill, effectiveDate);
                calculatedAccount.AccountSummary.AddTo(summary);
                calculatedAccount.CalculationSummaries.Add(bill.ID, summary);
            }

            return calculatedAccount;
        }
    }
}