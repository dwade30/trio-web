﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections
{
    public class ShowMortgageHolderInCollections : Command
    {
        public int Account { get; set; } = 0;
        public int AccountType { get; set; } = 0;

        public ShowMortgageHolderInCollections(int account, int accountType)
        {
            Account = account;
            AccountType = accountType;
        }
    }
}