﻿using System;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.StatusList;

namespace SharedApplication.TaxCollections
{
    public static class PropertyTaxStatusListQueryExtensions
    {
        public static PropertyTaxStatusListDetailItem ToStatusListDetailItem(
            this RealEstateBillAccountPartyView billAccountParty)
        {
            if (billAccountParty == null)
            {
                return null;
            }
            var taxDetail = new PropertyTaxStatusListDetailItem()
            {
                Account = billAccountParty.Account.GetValueOrDefault(),
                Name1 = billAccountParty.Name1,
                Name2 = billAccountParty.Name2,
                Address1 = billAccountParty.Address1,
                Address2 =  billAccountParty.Address2,
                Address3 =  billAccountParty.Address3,
                BillNumber = billAccountParty.BillNumber(),
                TaxYear =  billAccountParty.TaxYear(),
                StreetName = billAccountParty.StreetName,
                StreetNumber = billAccountParty.Streetnumber.GetValueOrDefault(),
                TaxDue1 = billAccountParty.TaxDue1.GetValueOrDefault(),
                TaxDue2 = billAccountParty.TaxDue2.GetValueOrDefault(),
                TaxDue3 = billAccountParty.TaxDue3.GetValueOrDefault(),
                TaxDue4 = billAccountParty.TaxDue4.GetValueOrDefault(),
                TranCode = billAccountParty.TranCode.GetValueOrDefault(),
                RealProperty = new RealEstatePropertyTaxStatusListDetailItem()
                {
                    MapLot = billAccountParty.MapLot,
                    BuildingValue = billAccountParty.BuildingValue??0,
                    ExemptValue =  billAccountParty.ExemptValue??0,
                    LandValue = billAccountParty.LandValue??0
                }
            };
            
            return taxDetail;
        }

        public static RealEstateTaxBill ToTaxBill(this RealEstateBillAccountPartyView billAccountParty)
        {
            if (billAccountParty == null)
            {
                return null;
            }
            var reBill = new RealEstateTaxBill()
            {
                Account =  billAccountParty.Account,
                BillNumber = billAccountParty.BillNumber(),
                ID = billAccountParty.Id,
                LienID = billAccountParty.LienRecordNumber?? 0,
                TaxYear = billAccountParty.TaxYear(),
                InterestAppliedThroughDate = billAccountParty.InterestAppliedThroughDate?? DateTime.MinValue,
                PreviousInterestAppliedDate = billAccountParty.PreviousInterestAppliedDate ?? DateTime.MinValue,
                OriginalInterestAppliedThroughDate = billAccountParty.InterestAppliedThroughDate ?? DateTime.MinValue,
                OriginalPreviousInterestDate = billAccountParty.PreviousInterestAppliedDate ?? DateTime.MinValue,
                Acreage = billAccountParty.Acres.GetValueOrDefault().ToDecimal(),
                BillType = PropertyTaxBillType.Real,
                BookPage = billAccountParty.BookPage,
                ExemptCode1 = billAccountParty.Exempt1??0,
                ExemptCode2 =  billAccountParty.Exempt2??0,
                ExemptCode3 = billAccountParty.Exempt3??0,
                BuildingValue = billAccountParty.BuildingValue??0,
                LandValue = billAccountParty.LandValue??0,
                HomesteadExemption = billAccountParty.HomesteadExemption.GetValueOrDefault().ToInteger(),
                MailingAddress1 = billAccountParty.Address1,
                MailingAddress2 = billAccountParty.Address2,
                MailingAddress3 = billAccountParty.MailingAddress3,
                CityStateZip = billAccountParty.Address3,
                MapLot = billAccountParty.MapLot,
                Name1 = billAccountParty.Name1,
                Name2 = billAccountParty.Name2,
                RateId = billAccountParty.RateKey??0,
                StreetName    = billAccountParty.StreetName,
                StreetNumber = billAccountParty.Streetnumber??0,
                Apt = billAccountParty.Apt,
                TranCode = billAccountParty.TranCode??0,
                TransferFromBillingDateFirst = billAccountParty.TransferFromBillingDateFirst,
                Reference1 = billAccountParty.Ref1,
                Reference2 = billAccountParty.Ref2,
                Lien = billAccountParty.IsLien()? new PropertyTaxLien()
                {
                    Account = billAccountParty.Account??0,
                    BillId = billAccountParty.Id,
                    Book = billAccountParty.LienBook,
                    DateCreated = DateTime.MinValue,
                    Id = billAccountParty.LienRecordNumber??0,
                    InterestAppliedThroughDate = billAccountParty.LienInterestAppliedThroughDate,
                    OriginalInterestAppliedThroughDate = billAccountParty.LienInterestAppliedThroughDate,
                    OriginalPreviousInterestDate = billAccountParty.LienPreviousInterestAppliedDate,
                    PreviousInterestAppliedDate = billAccountParty.LienPreviousInterestAppliedDate,
                    Page = billAccountParty.LienPage,
                    PrintedLdn = billAccountParty.LienPrintedLdn,
                    RateId = billAccountParty.LienRateKey??0,
                    Status = billAccountParty.LienStatus
                } : null,
            };

            if (reBill.Lien != null)
            {
                reBill.Lien.LienSummary.OriginalPreLienInterestCharged = billAccountParty.LienInterest ?? 0;
                reBill.Lien.LienSummary.AdjustedPreLienInterestCharged =
                    reBill.Lien.LienSummary.OriginalPreLienInterestCharged;
                reBill.Lien.LienSummary.OriginalPrincipalCharged = billAccountParty.LienPrincipal ?? 0;
                reBill.Lien.LienSummary.AdjustedPrincipalCharged = reBill.Lien.LienSummary.OriginalPrincipalCharged;
                reBill.Lien.LienSummary.OriginalCostsCharged = billAccountParty.LienCosts ?? 0;
                reBill.Lien.LienSummary.AdjustedCostsCharged = reBill.Lien.LienSummary.OriginalCostsCharged;
            }
            
            return reBill;
        }

        public static PersonalPropertyTaxBill ToTaxBill(this PersonalPropertyBillAccountPartyView billAccountParty)
        {
            return new PersonalPropertyTaxBill()
            {
                Account = billAccountParty.Account ?? 0,
                Apt = "",
                BillNumber = billAccountParty.BillNumber(),
                BillType = PropertyTaxBillType.Personal,
                Lien = null,
                ID = billAccountParty.Id,
                LienID = 0,
                TaxYear = billAccountParty.TaxYear(),
                InterestAppliedThroughDate = billAccountParty.InterestAppliedThroughDate ?? DateTime.MinValue,
                PreviousInterestAppliedDate = billAccountParty.PreviousInterestAppliedDate ?? DateTime.MinValue,
                OriginalInterestAppliedThroughDate = billAccountParty.InterestAppliedThroughDate ?? DateTime.MinValue,
                OriginalPreviousInterestDate = billAccountParty.PreviousInterestAppliedDate ?? DateTime.MinValue,
                MailingAddress1 = billAccountParty.Address1,
                MailingAddress2 = billAccountParty.Address2,
                MailingAddress3 = billAccountParty.MailingAddress3,
                CityStateZip = billAccountParty.Address3,
                Name1 = billAccountParty.Name1,
                Name2 = billAccountParty.Name2,
                RateId = billAccountParty.RateKey ?? 0,
                StreetName = billAccountParty.StreetName,
                StreetNumber = billAccountParty.Streetnumber ?? 0,
                TranCode = billAccountParty.TranCode ?? 0,
                TransferFromBillingDateFirst = billAccountParty.TransferFromBillingDateFirst,
                Zip = billAccountParty.Zip,
                OtherCode1 = billAccountParty.OtherCode1 ?? 0,
                OtherCode2 = billAccountParty.OtherCode2 ?? 0
            };
        }

        public static RealEstateAccountBill ToAccountBill(this RealEstateBillAccountPartyView billAccountParty)
        {
            if (billAccountParty == null)
            {
                return null;
            }
            return new RealEstateAccountBill()
            {
                Bill = billAccountParty.ToTaxBill(),
                Account = billAccountParty.Account??0,
                BillType = PropertyTaxBillType.Real,
                AccountId = billAccountParty.AccountId,
                DeedName1 = billAccountParty.DeedName1,
                DeedName2 = billAccountParty.DeedName2,
                IsDeleted = billAccountParty.Rsdeleted??false,
                OwnerAddress1 = billAccountParty.OwnerAddress1,
                OwnerAddress2 = billAccountParty.OwnerAddress2,
                OwnerAddress3 = billAccountParty.OwnerAddress3,
                OwnerCity = billAccountParty.OwnerCity,
                OwnerState = billAccountParty.OwnerState,
                OwnerZip = billAccountParty.OwnerZip,
                OwnerEmail = billAccountParty.OwnerEmail,
                OwnerFirstName = billAccountParty.OwnerFirstName,
                OwnerMiddleName = billAccountParty.OwnerMiddleName,
                OwnerLastName = billAccountParty.OwnerLastName,
                OwnerDesignation = billAccountParty.OwnerDesignation,
                OwnerGuid = billAccountParty.OwnerGuid,
                OwnerPartyType = billAccountParty.OwnerPartyType??0,
                SecOwnerFirstname = billAccountParty.SecOwnerFirstname,
                SecOwnerMiddleName =  billAccountParty.SecOwnerMiddleName,
                SecOwnerLastName = billAccountParty.SecOwnerLastName,
                SecOwnerDesignation = billAccountParty.SecOwnerDesignation,
                SecOwnerGuid = billAccountParty.SecOwnerGuid,
                SecOwnerPartyType = billAccountParty.SecOwnerPartyType??0,
                StreetNumber = billAccountParty.RslocNumAlph.ToIntegerValue(),
                StreetName = billAccountParty.RslocStreet,
                Apt = billAccountParty.RslocApt,
                Reference1 = billAccountParty.Rsref1,
                Reference2 = billAccountParty.Rsref2,
                IsTaxAcquired = billAccountParty.TaxAcquired??false,
                TaxAcquiredDate = billAccountParty.Tadate
            };
        }

        public static PersonalPropertyAccountBill ToAccountBill(
            this PersonalPropertyBillAccountPartyView billAccountParty)
        {
            if (billAccountParty == null)
            {
                return null;
            }
            return new PersonalPropertyAccountBill()
            {
                Bill = billAccountParty.ToTaxBill(),
                Account = billAccountParty.Account??0,
                Open1 = billAccountParty.Open1,
                Open2 =  billAccountParty.Open2,
                BillType = PropertyTaxBillType.Personal,
                StreetName = billAccountParty.Street,
                StreetNumber = billAccountParty.MasterStreetNumber??0,
                OwnerFirstName = billAccountParty.OwnerFirstName,
                OwnerMiddleName = billAccountParty.OwnerMiddleName,
                OwnerLastName = billAccountParty.OwnerLastName,
                OwnerDesignation = billAccountParty.OwnerDesignation,
                OwnerPartyType = billAccountParty.PartyType??0,
                OwnerEmail = billAccountParty.OwnerEmail,
                OwnerAddress1 = billAccountParty.OwnerAddress1,
                OwnerAddress2 = billAccountParty.OwnerAddress2,
                OwnerAddress3 = billAccountParty.OwnerAddress3,
                OwnerCity = billAccountParty.OwnerCity,
                OwnerState = billAccountParty.OwnerState,
                OwnerZip = billAccountParty.OwnerZip,
                Apt = billAccountParty.Apt,
                OwnerGuid = billAccountParty.PartyGuid,
                IsDeleted = billAccountParty.Deleted??false
            };
        }
    }
}