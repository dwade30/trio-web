﻿using System;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxBillInfoViewModel
    {
        PropertyTaxBill TaxBill { get; set; }
        TaxBillCalculationSummary CalculationSummary { get; set; }
        decimal TotalPerDiem { get; set; }
        object AccountTotals { get; set; }
        string LienDischargeBookPage { get; set; }
        DateTime EffectiveDate { get; set; }
        void Print();
    }
}