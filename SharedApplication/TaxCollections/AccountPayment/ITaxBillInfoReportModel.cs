﻿using System;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxBillInfoReportModel
    {
        PropertyTaxBill TaxBill { get; set; }
        TaxBillCalculationSummary CalculationSummary { get; set; }
        decimal TotalPerDiem { get; set; }
        object AccountTotals { get; set; }
        DateTime EffectiveDate { get; set; }

        string ClientName { get; set; }
    }
}