﻿using System.Collections.Generic;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ISelectCLBatchViewModel
    {
        CLBatchChoice SelectedBatch { get; set; }
        bool Cancelled { get; set; }

        IEnumerable<CLBatchChoice> BatchChoices { get; }
        void AddBatches(IEnumerable<CLBatchChoice> batchChoices);
    }
}