﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface IShowRealEstateTaxAccountInfoViewModel
    {
        int Account { get; set; }

        RealEstateTaxAccountInfo AccountInfo { get; set; }


    }
}