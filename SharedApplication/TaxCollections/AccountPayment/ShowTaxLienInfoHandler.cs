﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxLienInfoHandler : CommandHandler<ShowTaxLienInfo>
    {
        private IView<ITaxLienInfoViewModel> infoView;

        public ShowTaxLienInfoHandler(IView<ITaxLienInfoViewModel> infoView)
        {
            this.infoView = infoView;
        }
        protected override void Handle(ShowTaxLienInfo command)
        {
            infoView.ViewModel.Book = command.Book;
            infoView.ViewModel.DateCreated = command.DateCreated;
            infoView.ViewModel.InterestAppliedThroughDate = command.InterestAppliedThroughDate;
            infoView.ViewModel.LienId = command.LienId;
            infoView.ViewModel.LienSummary = command.LienSummary;
            infoView.ViewModel.Page = command.Page;
            infoView.ViewModel.RateId = command.RateId;
            infoView.ViewModel.Status = command.Status;
            infoView.Show();
        }
    }
}