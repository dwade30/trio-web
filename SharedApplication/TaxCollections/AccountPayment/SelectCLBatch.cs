﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class SelectCLBatch : Command<(bool Cancelled, CLBatchChoice BatchChoice)>
    {
        public bool ShowProcessedBatches { get; }

        public SelectCLBatch(bool showProcessedBatches)
        {
            ShowProcessedBatches = showProcessedBatches;
        }
    }
}