﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public class PersonalPropertyTaxAccountInfoQuery
    {
        public int Account { get; }

        public PersonalPropertyTaxAccountInfoQuery(int account)
        {
            Account = account;
        }
    }
}