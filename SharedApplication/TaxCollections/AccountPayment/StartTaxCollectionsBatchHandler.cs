﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class StartTaxCollectionsBatchHandler : CommandHandler<StartTaxCollectionsBatch>
    {
        private IView<ITaxCollectionsBatchViewModel> batchView;

        public StartTaxCollectionsBatchHandler(IView<ITaxCollectionsBatchViewModel> batchView)
        {
            this.batchView = batchView;
        }
        protected override void Handle(StartTaxCollectionsBatch command)
        {
            batchView.ViewModel.BatchInputSetup = command.BatchSetup;
            batchView.ViewModel.CorrelationIdentifier = command.CorrelationIdentifier;
            batchView.ViewModel.TransactionIdentifier = command.TransactionIdentifier;
            batchView.ViewModel.TaxBatch = new TaxCollectionsBatch()
            {
                BillType = command.BillType,
                BatchRecoverDate = command.BatchSetup.BatchRecoverDate,
                EffectiveDate = command.BatchSetup.EffectiveDate,
                DefaultTaxYear = command.BatchSetup.DefaultTaxYear,
                PaidBy = command.BatchSetup.PaidBy,
                TellerID = command.BatchSetup.TellerID,
                BatchIdentifier = command.BatchIdentifier,
                Period = 4                
            };
            batchView.Show();
        }
    }
}