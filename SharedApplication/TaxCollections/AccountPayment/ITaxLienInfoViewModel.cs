﻿using System;
using SharedApplication.TaxCollections.Lien;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxLienInfoViewModel
    {
        PropertyTaxLienSummary LienSummary { get; set; }
        int LienId { get; set; }
        DateTime DateCreated { get; set; }
        DateTime InterestAppliedThroughDate { get; set; }
        string Book { get; set; }
        string Page { get; set; }
        int RateId { get; set; }
        string Status { get; set; }
    }
}