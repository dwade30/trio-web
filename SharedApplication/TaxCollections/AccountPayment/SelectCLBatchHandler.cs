﻿using System.Linq;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class SelectCLBatchHandler :CommandHandler<SelectCLBatch,(bool Cancelled, CLBatchChoice BatchChoice)>
    {
        private IModalView<ISelectCLBatchViewModel> selectView;
        private ITaxCollectionsContext clContext;
        public SelectCLBatchHandler(IModalView<ISelectCLBatchViewModel> selectView, ITaxCollectionsContext clContext)
        {
            this.selectView = selectView;
            this.clContext = clContext;
        }

        protected override (bool Cancelled, CLBatchChoice BatchChoice) Handle(SelectCLBatch command)
        {
            var batchQuery = clContext.BatchRecovers;
            if (!command.ShowProcessedBatches)
            {
                batchQuery = batchQuery.Where(b => b.Processed != true);
            }
            var batches = batchQuery.GroupBy(b => new {b.TellerId, b.BatchRecoverDate, b.PaidBy, b.BatchIdentifier}).Select(b =>
                    new CLBatchChoice()
                    {
                        BatchIdentifier = b.Key.BatchIdentifier.GetValueOrDefault(),
                        BatchDate = b.Key.BatchRecoverDate.GetValueOrDefault(), TellerId = b.Key.TellerId,
                        PaidBy = b.Key.PaidBy
                    }).ToList();
            selectView.ViewModel.AddBatches( batches);
            selectView.ShowModal();
            if (selectView.ViewModel.Cancelled)
            {
                return (Cancelled: true, BatchChoice: null);
            }

            return (Cancelled: false, selectView.ViewModel.SelectedBatch);
        }
    }
}