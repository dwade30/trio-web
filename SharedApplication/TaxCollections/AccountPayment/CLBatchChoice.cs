﻿using System;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class CLBatchChoice
    {
        public Guid BatchIdentifier { get; set; } = Guid.Empty;
        public string PaidBy { get; set; } = "";
        public DateTime BatchDate { get; set; }
        public string TellerId { get; set; } = "";
    }
}