﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface IGetPropertyTaxDiscountViewModel
    {
        decimal OriginalPrincipal { get; set; }
        decimal OriginalTotal { get; set; }
        decimal Principal { get; set; }
        decimal DiscountPercentage { get; set; }
        decimal Discount { get; set; }
        bool Cancelled { get; set; }
        void Cancel();
        void SetPayment(decimal principal, decimal discount);
        void Recalculate();
        void SetPrincipal(decimal prinicpal);
    }
}