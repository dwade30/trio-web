﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowRealEstateTaxAccountInfoHandler : CommandHandler<ShowRealEstateTaxAccountInfo>
    {
        private IView<IShowRealEstateTaxAccountInfoViewModel> infoView;
        private IQueryHandler<RealEstateTaxAccountInfoQuery, RealEstateTaxAccountInfo> reQueryHandler;
        public ShowRealEstateTaxAccountInfoHandler(IView<IShowRealEstateTaxAccountInfoViewModel> infoView, IQueryHandler<RealEstateTaxAccountInfoQuery, RealEstateTaxAccountInfo> reQueryHandler)
        {
            this.infoView = infoView;
            this.reQueryHandler = reQueryHandler;
        }
        protected override void Handle(ShowRealEstateTaxAccountInfo command)
        {
            infoView.ViewModel.AccountInfo =
                reQueryHandler.ExecuteQuery(new RealEstateTaxAccountInfoQuery(command.Account));
            infoView.Show();
        }
    }
}