﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class DeleteCLBatchItem : Command
    {
        public int Id { get; }

        public DeleteCLBatchItem(int id)
        {
            Id = id;
        }
    }
}