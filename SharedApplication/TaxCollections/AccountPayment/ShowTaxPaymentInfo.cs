﻿using SharedApplication.TaxCollections.Models;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxPaymentInfo : Command
    {
        public PaymentRec Payment { get;  }

        public ShowTaxPaymentInfo(PaymentRec payment)
        {
            Payment = payment;
        }
    }
}