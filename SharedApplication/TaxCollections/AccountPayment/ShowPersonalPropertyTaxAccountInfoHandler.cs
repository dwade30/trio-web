﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowPersonalPropertyTaxAccountInfoHandler : CommandHandler<ShowPersonalPropertyTaxAccountInfo>
    {
        private IView<IShowPersonalPropertyTaxAccountInfoViewModel> infoView;
        private IQueryHandler<PersonalPropertyTaxAccountInfoQuery,PersonalPropertyTaxAccountInfo> ppQueryHandler;
        private IPersonalPropertyContext ppContext;
        public ShowPersonalPropertyTaxAccountInfoHandler(IView<IShowPersonalPropertyTaxAccountInfoViewModel> infoView, IQueryHandler<PersonalPropertyTaxAccountInfoQuery,PersonalPropertyTaxAccountInfo> ppQueryHandler, IPersonalPropertyContext ppContext)
        {
            this.infoView = infoView;
            this.ppQueryHandler = ppQueryHandler;
            this.ppContext = ppContext;
        }
        protected override void Handle(ShowPersonalPropertyTaxAccountInfo command)
        {
            infoView.ViewModel.AccountInfo =
                ppQueryHandler.ExecuteQuery(new PersonalPropertyTaxAccountInfoQuery(command.Account));
            var opens = ppContext.PPRatioOpens.FirstOrDefault();
            infoView.ViewModel.Open1Description =
                !string.IsNullOrWhiteSpace(opens.OpenField1) ? opens.OpenField1 : "Open 1";
            infoView.ViewModel.Open2Description =
                !string.IsNullOrWhiteSpace(opens.OpenField2) ? opens.OpenField1 : "Open 2";
            infoView.Show();
        }
    }
}