﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountSearchViewModel : ITaxAccountSearchViewModel
    {
        private PropertyTaxBillType billType = PropertyTaxBillType.Real;
        public PropertyTaxBillType BillType
        {
            get => billType;
            set
            {
                billType = value;
                SetSearchByOptions();
            }
        }
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }
        public bool AllowPayments { get; set; }
        public bool OriginIsTaxCollections { get; set; }
        public IEnumerable<GenericDescriptionPair<SearchCriteriaMatchType>> CriteriaMatchOptions { get; private set; }
        public IEnumerable<GenericDescriptionPair<TaxAccountSearchByOption>> SearchByOptions { get; private set; }

        private CommandDispatcher commandDispatcher;
        private EventPublisher eventPublisher;
        private IQueryHandler<TaxAccountSearch, IEnumerable<TaxAccountSearchResult>> searchHandler;
        private List<TaxAccountSearchResult> searchResults = new List<TaxAccountSearchResult>();
        private ISettingService settingService;
        private GlobalTaxCollectionSettings taxSettings;
        private GlobalTaxCollectionValues taxValues;

        public TaxAccountSearchViewModel(CommandDispatcher commandDispatcher,EventPublisher eventPublisher,IQueryHandler<TaxAccountSearch,IEnumerable<TaxAccountSearchResult>> searchHandler, ISettingService settingService,GlobalTaxCollectionSettings taxSettings,GlobalTaxCollectionValues taxValues)
        {
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;
            this.searchHandler = searchHandler;
            this.settingService = settingService;
            this.taxSettings = taxSettings;
            this.taxValues = taxValues;
            SetCriteriaMatchOptions();
            SetSearchByOptions();
        }

        private void SetSearchByOptions()
        {
            var options = new List<GenericDescriptionPair<TaxAccountSearchByOption>>()
            {
                new GenericDescriptionPair<TaxAccountSearchByOption>(){ID = TaxAccountSearchByOption.Name,Description = "Name"},
                new GenericDescriptionPair<TaxAccountSearchByOption>(){ID = TaxAccountSearchByOption.StreetName,Description = "Street Name"}
            };
            if (BillType == PropertyTaxBillType.Real)
            {
                options.Add(new GenericDescriptionPair<TaxAccountSearchByOption>() { ID = TaxAccountSearchByOption.MapLot, Description = @"Map / Lot" });
            }
            SearchByOptions = options;
        }

        private void SetCriteriaMatchOptions()
        {
            CriteriaMatchOptions = new List<GenericDescriptionPair<SearchCriteriaMatchType>>()
            {
                new GenericDescriptionPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.Contains, Description =  "Contains"},
                new GenericDescriptionPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.StartsWith, Description =  "Starts With"}
            };
        }

        public IEnumerable<TaxAccountSearchResult> SearchResults()
        {
            return searchResults;
        }

        public void Cancel()
        {
            eventPublisher.Publish(new TransactionCancelled(CorrelationId, null));
        }

        public void Search(SearchCriteriaMatchType matchType, TaxAccountSearchByOption searchBy, string searchFor,bool includePreviousOwners, bool includeDeletedAccounts)
        {
            if (String.IsNullOrWhiteSpace(searchFor))
            {
                return;
            }

            searchResults = searchHandler.ExecuteQuery(new TaxAccountSearch() {BillType = BillType, IncludePreviousOwners = includePreviousOwners, SearchFor = searchFor, MatchType = matchType, SearchBy = searchBy, IncludeDeletedAccounts = includeDeletedAccounts}).ToList();
        }

        public bool SelectAccount(int account,bool includeSearchResults)
        {
            if (AccountExists(account,BillType))
            {
                if (!OriginIsTaxCollections || taxSettings.ShowLastAccountInCR)
                {
                    var settingName = "";
                    if (BillType == PropertyTaxBillType.Personal)
                    {
                        settingName = "PPLastAccountNumber";
                        taxValues.LastAccountPP = account;
                    }
                        else
                    {
                        settingName = "RELastAccountNumber";
                        taxValues.LastAccountRE = account;
                    }
                    settingService.SaveSetting(SettingOwnerType.User,settingName,account.ToString());
                    
                }
                var accountList = includeSearchResults? searchResults.Select(s => s.Account) : new List<int>(){account};
                commandDispatcher.Send(new ShowTaxAccountStatus(account,BillType,AllowPayments, OriginIsTaxCollections,CorrelationId,accountList));
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetLastAccountAccessed()
        {
            if (!OriginIsTaxCollections && !taxSettings.ShowLastAccountInCR)
            {
                return 0;
            }
            switch (BillType)
            {
                case PropertyTaxBillType.Personal:
                    return taxValues.LastAccountPP;
                    break;
                default:
                    return taxValues.LastAccountRE;
                    break;
            }
        }

        public void StartBatch()
        {
            var setup = commandDispatcher.Send(new GetCLBatchInputSetup()).Result;
            if (setup.Success)
            {
                SendClose();
                commandDispatcher.Send(new StartTaxCollectionsBatch(setup.BatchSetup,Guid.NewGuid(),BillType,CorrelationId, TransactionId));
            }
        }

        public void PurgeBatch()
        {           
           var choice = commandDispatcher.Send<(bool Cancelled, CLBatchChoice BatchChoice)>(new SelectCLBatch(true)).Result;
           if (!choice.Cancelled)
           {
               if (choice.BatchChoice != null)
               {
                   DeleteBatch(choice.BatchChoice.BatchIdentifier);
               }
           }
        }

        public void RecoverBatch()
        {
            var choice = commandDispatcher.Send<(bool Cancelled, CLBatchChoice BatchChoice)>(new SelectCLBatch(false)).Result;
            if (!choice.Cancelled)
            {
                if (choice.BatchChoice != null)
                {
                    LoadBatch(choice.BatchChoice.BatchIdentifier);
                }
            }
        }

        public event EventHandler Closing;

        private void SendClose()
        {
            if (Closing != null)
            {
                Closing(this, new EventArgs());
            }
        }

        private void DeleteBatch(Guid batchIdentifier)
        {
            commandDispatcher.Send(new DeleteCLBatch(batchIdentifier));
        }

        private bool AccountExists(int account,PropertyTaxBillType billType)
        {
            if (billType == PropertyTaxBillType.Personal)
            {
                return commandDispatcher.Send(new PersonalPropertyAccountExists(account)).Result;
            }
            
            return commandDispatcher.Send(new RealEstateAccountExists(account)).Result;
        }

        private void LoadBatch(Guid batchIdentifier)
        {
            SendClose();
            commandDispatcher.Send(new EditTaxCollectionsBatch(batchIdentifier,CorrelationId));
        }
    }
}