﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class PrintRealEstateTaxAccountDetail : PrintTaxAccountDetail
    {
        public RealEstateTaxAccount RETaxAccount { get; set; }
    }
}