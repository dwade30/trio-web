﻿using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class CollectionsTransactionCompletedHandler : MessageEventHandler<CollectionsTransactionCompleted>
    {
        private CommandDispatcher commandDispatcher;
        public CollectionsTransactionCompletedHandler(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }
        protected override void Handle(CollectionsTransactionCompleted notification)
        {
            
            commandDispatcher.Send(new FinalizeTransactions<PropertyTaxTransaction>()
            {
                CorrelationIdentifier = notification.Id,
                ReceiptId = 0,
                TellerId = "",
                Transactions =  notification.CompletedTransactions
            });
            
        }
    }
}