﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountDetailReportModel : ITaxAccountDetailReportModel
    {
        private List<TaxAccountDetailLine> details = new List<TaxAccountDetailLine>();
        public IEnumerable<TaxAccountDetailLine> Details
        {
            get => details;
        }

        public string MuniName { get; set; } = "";
        public bool ShowReceiptNumber { get; set; }
        public string AccountDetailAssessmentSetting { get; set; } = "";

        public string CurrentOwnerName
        {
            get
            {
                switch (BillType)
                {
                    case PropertyTaxBillType.Real:
                        if (!string.IsNullOrWhiteSpace(RETaxAccount.Name2()))
                        {
                            return RETaxAccount.Name1() + " & " + RETaxAccount.Name2();
                        }
                        else
                        {
                            return RETaxAccount.Name1();
                        }
                        break;
                    case PropertyTaxBillType.Personal:
                        return PPTaxAccount.Name1();
                        break;
                }

                return "";
            }
        }
        public string Name { get; set; }

        public string Location
        {
            get
            {
                if (TaxAccount.LocationNumber() > 0)
                {
                    return TaxAccount.LocationNumber().ToString() + " " + TaxAccount.LocationStreet();
                }

                return TaxAccount.LocationStreet();
            }
        }

        public MailingAddress MailingAddress { get;  } = new MailingAddress();
        public int Account { get; set; } = 0;
        public Dictionary<int, TaxBillCalculationSummary> CalculationSummaries { get; } = new Dictionary<int, TaxBillCalculationSummary>();
        public TaxBillCalculationSummary AccountSummary { get; set; }
        public DateTime EffectiveDate { get; set; }

        public IEnumerable<TaxAccountDetailLine> CreateDetailLines()
        {
            var taxAccount = TaxAccount;
            var lines = new List<TaxAccountDetailLine>();
            TaxBillCalculationSummary summary = null;

            foreach (var bill in taxAccount.Bills)
            {
                if (CalculationSummaries.ContainsKey(bill.ID))
                {
                    summary = CalculationSummaries[bill.ID];
                }
                else
                {
                    summary = null;
                }

                if (ExpandedYearBills.Contains(bill.YearBill()))
                {
                    lines.Add( AddHeaderLine(bill));
                    lines.AddRange(AddPaymentLines(bill));
                    if (summary != null)
                    {
                        if (summary.Interest != 0)
                        {
                            lines.Add(AddInterestLine(summary.Interest));
                        }

                        lines.Add(AddTotalLine(summary));
                    }
                }
                else
                {
                   lines.Add(AddSummaryLine(summary, bill.YearBill(), !bill.HasLien() ? "R" : "L"));
                }
            }

            return lines;
        }

        private TaxAccountDetailLine AddInterestLine(decimal interest)
        {
            var currInterest = interest * -1;
            return new TaxAccountDetailLine()
            {
                LineType = TaxAccountDetailLineType.NewInterest,
                Interest = currInterest,
                Total = currInterest,
                Reference = currInterest < 0 ? "CURINT" : "EARNINT"
            };
        }

        private IEnumerable<TaxAccountDetailLine> AddPaymentLines(PropertyTaxBill bill)
        {
            var lines = new List<TaxAccountDetailLine>();
            if (bill.OwnerNames().Trim() != CurrentOwnerName)
            {
                lines.Add(new TaxAccountDetailLine()
                {
                    Reference = "Billed To:",
                    Name = bill.OwnerNames(),
                    LineType = TaxAccountDetailLineType.Name
                });              
            }
            var payments = bill.Payments.GetAll().OrderBy(p => p.RecordedTransactionDate)
                .ThenBy(p => p.ActualSystemDate);
            foreach (var payment in payments)
            {
                if (payment.ReceiptNumber != -1)
                {
                    lines.Add( AddPaymentLine(payment));
                }
            }

            if (bill.HasLien())
            {
                lines.AddRange(AddLienPayments(bill.Lien));
            }

            return lines;
        }

        private IEnumerable<TaxAccountDetailLine> AddLienPayments(PropertyTaxLien lien)
        {
            var lines = new List<TaxAccountDetailLine>();
            if (lien != null)
            {
                lines.Add(new TaxAccountDetailLine()
                {
                    EffectiveDate = lien.RateRecord.BillingDate,
                    BillCategory = "L",
                    Reference = "Liened",
                    Principal = lien.LienSummary.OriginalPrincipalCharged,
                    Interest = lien.LienSummary.OriginalPreLienInterestCharged,
                    Costs = lien.LienSummary.OriginalCostsCharged,
                    LineType = TaxAccountDetailLineType.LienHeader
                });
                var payments = lien.Payments.GetAll().OrderBy(p => p.RecordedTransactionDate)
                    .ThenBy(p => p.ActualSystemDate);
                foreach (var payment in payments)
                {
                    lines.Add(AddPaymentLine(payment));
                }
            }

            return lines;
        }

        private TaxAccountDetailLine AddPaymentLine(PaymentRec payment)
        {
            return new TaxAccountDetailLine()
            {
                Comment =  payment.Comments,
                Costs = payment.LienCost.GetValueOrDefault(),
                EffectiveDate = payment.RecordedTransactionDate.GetValueOrDefault(),
                Interest = payment.PreLienInterest.GetValueOrDefault() + payment.CurrentInterest.GetValueOrDefault(),
                LineType = TaxAccountDetailLineType.Payment,
                Principal = payment.Principal.GetValueOrDefault(),
                PayCode = payment.Code,
                Period = payment.Period,
                Reference = payment.Reference,
                Receipt = payment.ReceiptNumber.GetValueOrDefault(),
                Total = payment.Total()
            };
        }

        private TaxAccountDetailLine AddTotalLine(TaxBillCalculationSummary summary)
        {
            return new TaxAccountDetailLine()
            {
                Costs = summary.CostsDue(),
                Interest = summary.InterestDue(),
                Principal = summary.PrincipalDue(),
                PayCode = "",
                Period = "",
                Reference = "",
                Total = summary.TotalDue(),
                LineType = TaxAccountDetailLineType.Subtotal
            };
        }

        private TaxAccountDetailLine AddHeaderLine(PropertyTaxBill bill)
        {
            var line = new  TaxAccountDetailLine()
            {
                BillCategory = "R",
                LineType = TaxAccountDetailLineType.Header,
                
            };
            if (bill.HasLien())
            {
                line.BillCategory = "L";
            }
            else
            {
                if (bill.RateRecord != null)
                {
                    if (bill.RateRecord.RateType == PropertyTaxRateType.Supplemental)
                    {
                        line.BillCategory = "S";
                    }
                }
            }
            if (bill.RateRecord != null)
            {
                line.EffectiveDate = bill.RateRecord.BillingDate;
                line.Reference = "Original";
                line.RateId = bill.RateId;
            }
            else
            {
                line.RateId = 0;
            }

            line.Principal = bill.TaxInstallments.GetOriginalCharged();
            line.Total = line.Principal;
            line.YearBill = bill.YearBill();
            return line;
        }

        private TaxAccountDetailLine AddSummaryLine(TaxBillCalculationSummary billSummary,int yearBill,string billCategory)
        {
            return new TaxAccountDetailLine()
            {
                Costs = billSummary.CostsDue(),
                Interest = billSummary.InterestDue(),
                Principal = billSummary.PrincipalDue(),
                PayCode = "",
                Period = "",
                Reference = "",
                Total =  billSummary.TotalDue(),
                YearBill = yearBill,
                LineType = TaxAccountDetailLineType.SummaryHeader,
                BillCategory =  billCategory
            };
        }

        public IEnumerable<int> ExpandedYearBills { get; set; } = new List<int>();
        public List<string> ExemptionCodes { get; set; } = new List<string>();
        public string BookPages { get; set; } = "";

        public PropertyTaxBillType BillType { get; set; }
        public RealEstateTaxAccount RETaxAccount { get; set; }
        public PersonalPropertyTaxAccount PPTaxAccount { get; set; }
        public PropertyTaxAccount TaxAccount {
            get
            {
                if (BillType == PropertyTaxBillType.Personal)
                {
                    return PPTaxAccount;
                }

                return RETaxAccount;
            }
        }
    }

   
}