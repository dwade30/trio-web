﻿using System;
using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxCollectionsBatch
    {
        public Guid BatchIdentifier { get; set; } = Guid.NewGuid();
        public DateTime BatchRecoverDate { get; set; }
        public int DefaultTaxYear { get; set; } = 0;
        public string TellerID { get; set; } = "";
        public DateTime EffectiveDate { get; set; }
        public string PaidBy { get; set; } = "";
        public int Period { get; set; } = 0;
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
        //public List<BatchRecover> BatchItems { get; } = new List<BatchRecover>();
        public List<PropertyTaxBatchItemGroup> BatchItems { get; } = new List<PropertyTaxBatchItemGroup>();
    }
}