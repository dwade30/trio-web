﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class CollectionsTransactionCompleted : Message
    {
        public Guid Id { get; }
        public IEnumerable<PropertyTaxTransaction> CompletedTransactions { get; }        
        public CollectionsTransactionCompleted(IEnumerable<PropertyTaxTransaction> completedTransactions)
        {
            CompletedTransactions = completedTransactions;
        }
    }
}