﻿using System;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class CLBatchInputSetupViewModel : ICLBatchInputSetupViewModel
    {
        private CommandDispatcher commandDispatcher;
        public CLBatchInputSetupViewModel(CommandDispatcher commandDispatcher)
        {
            BatchSetup.BatchRecoverDate = DateTime.Today;
            BatchSetup.EffectiveDate = DateTime.Today;
            this.commandDispatcher = commandDispatcher;
        }
        public CLBatchInputSetup BatchSetup { get; } = new CLBatchInputSetup();
        public void Cancel()
        {            
            Cancelled = true;
        }

        public bool Cancelled { get; private set; } = true;
        public void Save(CLBatchInputSetup batchInput)
        {
            Cancelled = false;
            BatchSetup.BatchRecoverDate = batchInput.BatchRecoverDate;
            BatchSetup.DefaultTaxYear = batchInput.DefaultTaxYear;
            BatchSetup.EffectiveDate = batchInput.EffectiveDate;
            BatchSetup.PaidBy = batchInput.PaidBy;
            BatchSetup.Period = batchInput.Period;
            BatchSetup.TellerID = batchInput.TellerID;
        }

        public bool ValidateTeller(string tellerId)
        {
          return commandDispatcher.Send<bool>(new IsValidTellerId(tellerId)).Result;
        }

        public bool ValidateTaxYear(int taxYear)
        {
            return commandDispatcher.Send(new RateForTaxYearExists(taxYear)).Result;
        }
    }
}