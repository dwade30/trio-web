﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowPersonalPropertyTaxAccountInfoViewModel : IShowPersonalPropertyTaxAccountInfoViewModel
    {
        public int Account { get; set; } = 0;
        public PersonalPropertyTaxAccountInfo AccountInfo { get; set; }
        public string Open1Description { get; set; } = "";
        public string Open2Description { get; set; } = "";
    }
}