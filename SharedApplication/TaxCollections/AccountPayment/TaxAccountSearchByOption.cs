﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public enum TaxAccountSearchByOption
    {
        Name = 0,
        StreetName = 1,
        MapLot = 2
    }
}