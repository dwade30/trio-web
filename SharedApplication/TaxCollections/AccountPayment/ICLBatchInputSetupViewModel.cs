﻿using SharedApplication.Receipting;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ICLBatchInputSetupViewModel
    {
        CLBatchInputSetup BatchSetup { get; }
        void Cancel();
        bool Cancelled { get; }
        void Save(CLBatchInputSetup batchInput);
        bool ValidateTeller(string tellerId);
        bool ValidateTaxYear(int taxYear);
    }
}