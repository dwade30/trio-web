﻿using System;
using SharedApplication.TaxCollections.Lien;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxLienInfoViewModel : ITaxLienInfoViewModel
    {
        public PropertyTaxLienSummary LienSummary { get; set; }
        public int LienId { get; set; } = 0;
        public DateTime DateCreated { get; set; }
        public DateTime InterestAppliedThroughDate { get; set; }
        public string Book { get; set; } = "";
        public string Page { get; set; } = "";
        public int RateId { get; set; } = 0;
        public string Status { get; set; } = "";
    }
}