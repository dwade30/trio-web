﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxBillInfo : Command
    {
        public PropertyTaxBill TaxBill { get; }
        public TaxBillCalculationSummary CalculationSummary { get; }
        public decimal TotalPerDiem { get; }
        public object AccountTotals { get; }
        public string LienDischargeBookPage { get; }
        public DateTime EffectiveDate { get; }
        public ShowTaxBillInfo(PropertyTaxBill taxBill, TaxBillCalculationSummary calculationSummary, decimal totalPerDiem, object accountTotals, DateTime effectiveDate, string lienDischargeBookPage)
        {
            TaxBill = taxBill;
            CalculationSummary = calculationSummary;
            TotalPerDiem = totalPerDiem;
            AccountTotals = accountTotals;
            EffectiveDate = effectiveDate;
            LienDischargeBookPage = lienDischargeBookPage;
        }
    }
}