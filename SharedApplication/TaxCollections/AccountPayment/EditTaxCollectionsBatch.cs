﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class EditTaxCollectionsBatch : Command
    {
        public Guid BatchIdentifier { get; }
        public Guid CorrelationIdentifier { get; }
        public EditTaxCollectionsBatch(Guid batchIdentifier, Guid correlationIdentifier)
        {
            BatchIdentifier = batchIdentifier;
            CorrelationIdentifier = correlationIdentifier;
        }
    }
}