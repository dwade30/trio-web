﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowPropertyTaxBatchListingHandler : CommandHandler<ShowPropertyTaxBatchListing>
    {
        private IView<IPropertyTaxBatchListingViewModel> view;
        public ShowPropertyTaxBatchListingHandler(IView<IPropertyTaxBatchListingViewModel> view)
        {
            this.view = view;
        }
        protected override void Handle(ShowPropertyTaxBatchListing command)
        {
            view.ViewModel = command.ViewModel;
            view.Show();
        }
    }
}