﻿using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxPaymentInfoViewModel
    {
        PaymentRec Payment { get; set; }
        int ReceiptNumber { get; set; }
    }
}