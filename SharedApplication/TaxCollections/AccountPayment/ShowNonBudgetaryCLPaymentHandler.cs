﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowNonBudgetaryCLPaymentHandler : CommandHandler<ShowNonBudgetaryCLPayment,(bool Cancelled,PropertyTaxAmounts Amounts)>
    {
        private IModalView<INonBudgetaryPaymentViewModel> view;
        public ShowNonBudgetaryCLPaymentHandler(IModalView<INonBudgetaryPaymentViewModel> view)
        {
            this.view = view;
        }
        protected override (bool Cancelled, PropertyTaxAmounts Amounts) Handle(ShowNonBudgetaryCLPayment command)
        {
            view.ViewModel.Interest = command.Interest;
            view.ViewModel.Costs = command.Costs;
            view.ViewModel.PreLienInterest = command.PreLienInterest;
            view.ViewModel.Principal = command.Principal;
            view.ViewModel.IsLien = command.IsLien;
            view.ShowModal();
            if (view.ViewModel.Cancelled)
            {
                return (true, new PropertyTaxAmounts());
            }
            return (false, new PropertyTaxAmounts()
            {
                Costs = view.ViewModel.Costs,
                Interest = view.ViewModel.Interest,
                PreLienInterest = view.ViewModel.PreLienInterest,
                Principal =  view.ViewModel.Principal
            });
        }
    }
}