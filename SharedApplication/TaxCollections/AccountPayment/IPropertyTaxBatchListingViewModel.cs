﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface IPropertyTaxBatchListingViewModel
    {
        PropertyTaxBillType BillType { get; set; }
        CLBatchInputSetup BatchInputSetup { get; set; }
        TaxCollectionsBatch TaxBatch { get; set; }
    }
}