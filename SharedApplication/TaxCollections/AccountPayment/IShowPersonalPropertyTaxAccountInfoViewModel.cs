﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface IShowPersonalPropertyTaxAccountInfoViewModel
    {
        int Account { get; set; }
        PersonalPropertyTaxAccountInfo AccountInfo { get; set; }
        string Open1Description { get; set; } 
        string Open2Description { get; set; }
    }
}