﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public class PrintPersonalPropertyTaxAccountDetail : PrintTaxAccountDetail
    {
        public PersonalPropertyTaxAccount PPTaxAccount { get; set; }
    }
}