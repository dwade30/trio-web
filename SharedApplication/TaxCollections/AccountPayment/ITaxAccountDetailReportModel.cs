﻿using System;
using System.Collections.Generic;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxAccountDetailReportModel
    {
        IEnumerable<TaxAccountDetailLine> Details { get; }
        string CurrentOwnerName { get;  }
        string MuniName { get; set; }
        bool ShowReceiptNumber { get; set; }
        string AccountDetailAssessmentSetting { get; set; }
        string Location { get;}
        MailingAddress MailingAddress { get; }
        int Account { get; set; }
        Dictionary<int, TaxBillCalculationSummary> CalculationSummaries { get; }
        TaxBillCalculationSummary AccountSummary { get; set; }
        DateTime EffectiveDate { get; set; }
        PropertyTaxBillType BillType { get; set; }
        RealEstateTaxAccount RETaxAccount { get; set; }
        PersonalPropertyTaxAccount PPTaxAccount { get; set; }
        PropertyTaxAccount TaxAccount { get; }
        IEnumerable<TaxAccountDetailLine> CreateDetailLines();
        IEnumerable<int> ExpandedYearBills { get; set; }
        List<string> ExemptionCodes { get; set; }
        string BookPages { get; set; } 
    }
}