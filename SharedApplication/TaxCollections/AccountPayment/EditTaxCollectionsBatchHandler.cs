﻿using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Extensions;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class EditTaxCollectionsBatchHandler : CommandHandler<EditTaxCollectionsBatch>
    {
        private ITaxCollectionsContext clContext;
        private IView<ITaxCollectionsBatchViewModel> batchView;
        public EditTaxCollectionsBatchHandler(ITaxCollectionsContext clContext, IView<ITaxCollectionsBatchViewModel> batchView)
        {
            this.clContext = clContext;
            this.batchView = batchView;
        }
        protected override void Handle(EditTaxCollectionsBatch command)
        {
            var batchItems = clContext.BatchRecovers.Where(b => b.BatchIdentifier == command.BatchIdentifier).ToList();
            TaxCollectionsBatch batch;
            if (batchItems.Any())
            {
                 batch = batchItems.FirstOrDefault().ToCollectionsBatch();               
            }
            else
            {
                batch = new TaxCollectionsBatch();
            }
           // var batch  = batchItems.ToCollectionsBatch();
            batchView.ViewModel.TaxBatch = batch;
            batchView.ViewModel.CorrelationIdentifier = command.CorrelationIdentifier;
            batchView.ViewModel.BatchInputSetup = batch.ToBatchDefault();
            foreach (var item in batchItems)
            {
                if (item.Year.GetValueOrDefault() > batch.DefaultTaxYear)
                {
                    batch.DefaultTaxYear = item.Year.GetValueOrDefault();
                }
                batchView.ViewModel.AddBatchRecoverToBatch(item);
            }    
            batchView.Show();
        }
    }
}