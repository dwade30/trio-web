﻿using System.Collections.Generic;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class PersonalPropertyTaxAccountInfo
    {
        public int Account { get; set; } = 0;
        public string Name { get; set; } = "";
        public MailingAddress MailingAddress { get; set; } = new MailingAddress();
        public int AssociatedREAccount { get; set; } = 0;
        public decimal OutstandingREAmount { get; set; } = 0;
        public int AccountGroup { get; set; } = 0;
        public List<string> ExemptionCodes { get; set; } = new List<string>();
        public int LocationNumber { get; set; } = 0;
        public string LocationStreet { get; set; } = "";
        public string Location
        {
            get
            {
                if (LocationNumber > 0)
                {
                    return (LocationNumber.ToString() + " " + LocationStreet).Trim();
                }
                else
                {
                    return LocationStreet.Trim();
                }
            }
        }

        public string Open1 { get; set; } = "";
        public string Open2 { get; set; } = "";
    }
}