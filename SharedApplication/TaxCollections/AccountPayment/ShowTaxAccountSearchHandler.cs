﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxAccountSearchHandler : CommandHandler<ShowTaxAccountSearch>
    {
        private IView<ITaxAccountSearchViewModel> searchView;

        public ShowTaxAccountSearchHandler(IView<ITaxAccountSearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override void Handle(ShowTaxAccountSearch command)
        {
            searchView.ViewModel.BillType = command.BillType;
            searchView.ViewModel.CorrelationId = Guid.NewGuid();
            searchView.ViewModel.TransactionId = Guid.NewGuid();
            searchView.ViewModel.OriginIsTaxCollections = true;
            searchView.ViewModel.AllowPayments = false;
            searchView.Show();
        }
    }
}