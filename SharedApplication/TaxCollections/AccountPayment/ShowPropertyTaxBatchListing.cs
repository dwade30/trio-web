﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowPropertyTaxBatchListing : Command
    {
        public IPropertyTaxBatchListingViewModel ViewModel { get; }

        public ShowPropertyTaxBatchListing(IPropertyTaxBatchListingViewModel viewModel)
        {
            ViewModel = viewModel;
        }
    }
}