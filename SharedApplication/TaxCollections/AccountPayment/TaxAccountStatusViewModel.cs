﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using SharedApplication.AccountGroups;
using SharedApplication.AccountGroups.Commands;
using SharedApplication.Budgetary.Commands;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.PersonalProperty;
using SharedApplication.RealEstate;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Domain;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Extensions;
using SharedApplication.TaxCollections.Interfaces;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Notes;
using SharedApplication.TaxCollections.Queries;
using SharedApplication.TaxCollections.Receipting;
using Bill = SharedApplication.UtilityBilling.Models.Bill;
using PaymentRec = SharedApplication.TaxCollections.Models.PaymentRec;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountStatusViewModel : ITaxAccountStatusViewModel
    {
        public Guid CorrelationIdentifier { get; set; }
        public int Account { get; set; } = 0;
        public List<int> AccountList { get; set; }
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
        public List<DescriptionIDPair> TaxYears { get; private set; }
        public int LatestBilledYear { get; set; } = 0;
        public string Comment { get; set; } = "";
        public string Note { get; set; } = "";
        public bool AutoShowNote { get; set; }

        public bool HasComment()
        {
            return (!string.IsNullOrWhiteSpace(Comment));
        }

        public event EventHandler EffectiveDateChanged ;
        public int OldestBillYearWithABalance { get; set; } = 0;

        private List<PropertyTaxTransaction> taxTransactions = new List<PropertyTaxTransaction>();
        private int DefaultPrePayYear { get; set; } = 0;
        public bool HasNote()
        {
            return (!string.IsNullOrWhiteSpace(Note));
        }

        public GroupMaster GroupInfo
        {
            get => groupMaster;
        }

        public AccountGroup GetGroupSummary(DateTime effectiveDate)
        {
            if (GroupInfo == null || GroupInfo.GroupNumber.GetValueOrDefault() == 0)
            {
                return new AccountGroup(0, new List<GroupAccountSummary>());
            }

            return commandDispatcher.Send(new CalculateAccountGroup(GroupInfo.GroupNumber.GetValueOrDefault(), effectiveDate)).Result;
        }

        public ModuleAssociation ModuleAssociationInfo
        {
            get => moduleAssociation;
            set => moduleAssociation = value;
        }

        public string CurrentOwnerName
        {
            get
            {
                switch (BillType)
                {
                    case PropertyTaxBillType.Real:
                        if (!string.IsNullOrWhiteSpace(RETaxAccount.Name2()))
                        {
                            return RETaxAccount.Name1() + " & " + RETaxAccount.Name2();
                        }
                        else
                        {
                            return RETaxAccount.Name1();
                        }
                        break;
                    case PropertyTaxBillType.Personal:
                        return PPTaxAccount.Name1();
                        break;
                }

                return "";
            }
        }

        public RealEstateTaxAccount RETaxAccount { get; private set; }
        public PersonalPropertyTaxAccount PPTaxAccount { get; private set; }
        
        private IPropertyTaxBillQueryHandler billQueryHandler;
        private IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler;
        private IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> reTotalsHandler;
        private IQueryHandler<PersonalPropertyAccountBriefQuery, PersonalPropertyAccountBriefInfo> ppBriefHandler;
        private IQueryHandler<PersonalPropertyAccountTotalsQuery, PersonalPropertyAccountTotals> ppTotalsHandler;
        private CommandDispatcher commandDispatcher;
        private IPropertyTaxBillCalculator billCalculator;
       // private ITaxAccountStatusViewModel _taxAccountStatusViewModelImplementation;
        private DataEnvironmentSettings environmentSettings;
        private IReceiptTypesService receiptTypesService;
        private IGlobalActiveModuleSettings activeModuleSettings;
        private CRTransactionGroupRepository crTransactionRepository;
        private EventPublisher eventPublisher;
        private GlobalTaxCollectionSettings taxSettings;
        private IQueryHandler<TaxLienDischargeNeededQuery, DischargeNeeded> dischargeNeededHandler;
        private GroupMaster groupMaster;
        private IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler;
        private ModuleAssociation moduleAssociation;

        private IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>
            moduleAssociationQueryHandler;
        private PropertyTaxAccount TaxAccount
        {
            get
            {
                if (BillType == PropertyTaxBillType.Personal)
                {
                    return PPTaxAccount;
                }

                return RETaxAccount;
            }
        }

        public bool OriginIsTaxCollections { get; set; }
        public Dictionary<int,TaxBillCalculationSummary> CalculationSummaries { get; } = new Dictionary<int, TaxBillCalculationSummary>();
        public TaxBillCalculationSummary AccountSummary { get; private set; }
        
        public TaxAccountStatusViewModel(CommandDispatcher commandDispatcher, IPropertyTaxBillQueryHandler billQueryHandler
            , IQueryHandler<RealEstateAccountBriefQuery, RealEstateAccountBriefInfo> reBriefHandler
            , IQueryHandler<RealEstateAccountTotalsQuery, RealEstateAccountTotals> reTotalsHandler, IPropertyTaxBillCalculator billCalculator
            ,DataEnvironmentSettings environmentSettings,IReceiptTypesService receiptTypesService, IGlobalActiveModuleSettings activeModuleSettings
            , CRTransactionGroupRepository crTransactionRepository,IQueryHandler<PersonalPropertyAccountBriefQuery,PersonalPropertyAccountBriefInfo> ppBriefHandler
            , IQueryHandler<PersonalPropertyAccountTotalsQuery,PersonalPropertyAccountTotals> ppTotalsHandler, EventPublisher eventPublisher
            ,GlobalTaxCollectionSettings taxSettings, IQueryHandler<TaxLienDischargeNeededQuery,DischargeNeeded> dischargeNeededHandler, IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler, IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>
                moduleAssociationQueryHandler)
        {
            
            PendingPayments = new List<PaymentRec>();
            EffectiveDate = DateTime.Today;
            RecordedTransactionDate = DateTime.Today;
            this.environmentSettings = environmentSettings;
            this.receiptTypesService = receiptTypesService;
            this.activeModuleSettings = activeModuleSettings;
            this.crTransactionRepository = crTransactionRepository;
            this.eventPublisher = eventPublisher;
            this.taxSettings = taxSettings;
            TaxYears = new List<DescriptionIDPair>();
            AccountSummary = new TaxBillCalculationSummary(0, PropertyTaxBillType.Real);
            this.commandDispatcher = commandDispatcher;
            this.billQueryHandler = billQueryHandler;
            this.reBriefHandler = reBriefHandler;
            this.reTotalsHandler = reTotalsHandler;
            this.billCalculator = billCalculator;
            this.ppBriefHandler = ppBriefHandler;
            this.ppTotalsHandler = ppTotalsHandler;
            this.dischargeNeededHandler = dischargeNeededHandler;
            this.groupMasterQueryHandler = groupMasterQueryHandler;
            this.moduleAssociationQueryHandler = moduleAssociationQueryHandler;
        }

        public void LoadAccount()
        {
            if (!OriginIsTaxCollections)
            {
                LatestBilledYear = commandDispatcher.Send(new GetLastTaxYearBilled(BillType)).Result;
                if (LatestBilledYear == 0)
                {
                    LatestBilledYear = DateTime.Now.Year;
                }
            }

            switch (BillType)
            {
                case PropertyTaxBillType.Real:
                    LoadREAccount();
                    break;
                case PropertyTaxBillType.Personal:
                    LoadPPAccount();
                    break;
            }
            
        }

        private void LoadREAccount()
        {
            CheckForComment();
            CheckForNote();
            GetModuleAssociationInfo();

            var bills = billQueryHandler.Find(b => b.Account == Account &&  b.BillingType == "RE")
                .OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber); 
            var newestBill = (RealEstateTaxBill)bills.FirstOrDefault(b => b.RateId > 0);
            var accountInfo = reBriefHandler.ExecuteQuery(new RealEstateAccountBriefQuery() {Account = Account});
            RETaxAccount = new RealEstateTaxAccount();
            RETaxAccount.Account = Account;

            groupMaster = this.groupMasterQueryHandler.ExecuteQuery(new GroupMasterSearchCriteria
            {
                AssociatedWith = new ModuleAssociationSearchCriteria
                {
                    AccountNumber = Account,
                    Module = "RE"
                }
            }).FirstOrDefault();

            RETaxAccount.AddBills(bills);
            
            if (accountInfo != null)
            {   RETaxAccount.FillBriefInfo(accountInfo);           
                RETaxAccount.AccountBriefInfo.Account = Account;
            }

            LoadTransactions();
            foreach (var transaction in taxTransactions)
            {
                //RETaxAccount.ApplyPayments(transaction.Payments);
                RETaxAccount.AddNewPayments(transaction.Payments);
            }

            var accountTotals = reTotalsHandler.ExecuteQuery(new RealEstateAccountTotalsQuery(Account));
            if (accountTotals != null)
            {
                RETaxAccount.AccountTotals.Account = Account;
                RETaxAccount.AccountTotals.Acreage = accountTotals.Acreage;
                RETaxAccount.AccountTotals.Building = accountTotals.Building;
                RETaxAccount.AccountTotals.Exemptions = accountTotals.Exemptions;
                RETaxAccount.AccountTotals.Land = accountTotals.Land;
            }

            if (newestBill != null)
            {
                RETaxAccount.LatestBillTotals.Account = Account;
                RETaxAccount.LatestBillTotals.Acreage = newestBill.Acreage;
                RETaxAccount.LatestBillTotals.Building = newestBill.BuildingValue;
                RETaxAccount.LatestBillTotals.Land = newestBill.LandValue;
                RETaxAccount.LatestBillTotals.Exemptions = newestBill.ExemptValue;
                LatestBilledYear = newestBill.TaxYear;
            }
            else
            {
                LatestBilledYear = 0;
            }

            CalculateBills();
        }

        private void GetModuleAssociationInfo()
        {
            moduleAssociation = this.moduleAssociationQueryHandler.ExecuteQuery(new ModuleAssociationSearchCriteria
            {
                Module = "PP",
                REMasterAccount = BillType == PropertyTaxBillType.Real ? Account : 0,
                AccountNumber = BillType == PropertyTaxBillType.Personal ? Account : 0
            }).FirstOrDefault();
        }

        private void CheckForNote()
        {
            var note = commandDispatcher.Send(new GetCollectionsNote(Account, BillType)).Result;
            if (note != null)
            {
                Note = note.Comment;
                AutoShowNote = note.Priority.GetValueOrDefault() > 0;
            }
            else
            {
                Note = "";
                AutoShowNote = false;
            }
        }

        private void LoadPPAccount()
        {
            CheckForComment();
            CheckForNote();
            GetModuleAssociationInfo();

            var bills = billQueryHandler.Find(b => b.Account == Account && b.BillingType == "PP")
                .OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber); ;
            var newestBill = (PersonalPropertyTaxBill)bills.FirstOrDefault(b => b.RateId > 0);
            var accountInfo = ppBriefHandler.ExecuteQuery(new PersonalPropertyAccountBriefQuery(Account));
            PPTaxAccount = new PersonalPropertyTaxAccount();
            PPTaxAccount.Account = Account;

            groupMaster = this.groupMasterQueryHandler.ExecuteQuery(new GroupMasterSearchCriteria
            {
                AssociatedWith = new ModuleAssociationSearchCriteria
                {
                    AccountNumber = Account,
                    Module = "PP"
                }
            }).FirstOrDefault();

            PPTaxAccount.AddBills(bills);
            LoadTransactions();
            foreach (var transaction in taxTransactions)
            {
                PPTaxAccount.AddPayments(transaction.Payments);
            }
            if (accountInfo != null)
            {
                PPTaxAccount.AccountBriefInfo.LocationNumber = accountInfo.LocationNumber;
                PPTaxAccount.AccountBriefInfo.LocationStreet = accountInfo.LocationStreet;
                PPTaxAccount.AccountBriefInfo.Name = accountInfo.Name;
                PPTaxAccount.AccountBriefInfo.Account = Account;
                PPTaxAccount.AccountBriefInfo.Deleted = accountInfo.Deleted;
                PPTaxAccount.AccountBriefInfo.PartyId = accountInfo.PartyId;
            }
            var accountTotals = ppTotalsHandler.ExecuteQuery(new PersonalPropertyAccountTotalsQuery(Account));
            if (accountTotals != null)
            {
                PPTaxAccount.AccountTotals.Account = Account;
                PPTaxAccount.AccountTotals.TotalItemized = accountTotals.TotalItemized;
                PPTaxAccount.AccountTotals.TotalLeased = accountTotals.TotalLeased;
                PPTaxAccount.AccountTotals.Valuations.AddValuations(accountTotals.Valuations.Valuations);
            }

            if (newestBill != null)
            {
                PPTaxAccount.LatestBillTotals.Account = Account;
                PPTaxAccount.LatestBillTotals.Valuations.AddValuations( newestBill.PersonalPropertyValuations.Valuations);                
                LatestBilledYear = newestBill.TaxYear;
            }
            else
            {
                LatestBilledYear = 0;
            }
            CalculateBills();           
        }

        private void CheckForComment()
        {
            Comment = commandDispatcher.Send(new GetTaxCollectionsComment(Account,BillType)).Result;
        }

        public void AddRECLComment()
        {
            commandDispatcher.Send(new AddRECLComment(Account, true));
            CheckForComment();
        }

        public bool ArePreviousAccountsInList()
        {
            if (!AccountList.Any())
            {
                return false;
            }

            if (!AccountList.Contains(Account))
            {
                return false;
            }
            return AccountList.FirstOrDefault() != Account;
        }

        public bool AreFollowingAccountsInList()
        {
            if (!AccountList.Any())
            {
                return false;
            }

            if (!AccountList.Contains(Account))
            {
                return false;
            }
            return AccountList.LastOrDefault() != Account;
        }

        private int GetPreviousAccount()
        {
            if (!AccountList.Any())
            {
                return 0;
            }

            var index = AccountList.IndexOf(Account) - 1;
            if (index >= 0)
            {
                return AccountList[index];
            }

            return 0;
        }
        public bool ShowPreviousAccount()
        {
            var prevAccount = GetPreviousAccount();
            if (prevAccount < 1)
            {
                return false;
            }
            commandDispatcher.Send(new ShowTaxAccountStatus(prevAccount, BillType, AllowPayments, OriginIsTaxCollections, CorrelationIdentifier, AccountList));
            return true;
        }

        private int GetNextAccount()
        {
            if (!AccountList.Any())
            {
                return 0;
            }

            var index = AccountList.IndexOf(Account) + 1;
            if (index >= AccountList.Count || index < 1)
            {
                return 0;
            }

            return AccountList[index];
        }

        public bool ShowNextAccount()
        {
            var nextAccount = GetNextAccount();
            if (nextAccount < 1)
            {
                return false;
            }
            commandDispatcher.Send(new ShowTaxAccountStatus(nextAccount, BillType, AllowPayments, OriginIsTaxCollections, CorrelationIdentifier, AccountList));
            return true;
        }


        public void EditComment()
        {
            commandDispatcher.Send(new EditCollectionsComment(Account, BillType));
            CheckForComment();
        }

        public void EditNote()
        {
            commandDispatcher.Send(new EditCollectionsNote(Account, BillType));
            CheckForNote();
        }

        public DateTime EffectiveDate { get; set; }
        public DateTime RecordedTransactionDate { get; set; }
        public bool AllowPayments { get; set; }
        public void ChangeEffectiveDate()
        {
            var dateChoice = commandDispatcher.Send(new ChooseEffectiveDate(EffectiveDate,AllowPayments,taxSettings.AutoChangeRecordedTransactionDateWithEffectiveDate)).Result;
            if (!dateChoice.Cancelled)
            {
                var recalculationNeeded = false;
                var datesChanged = false;
                if (!EffectiveDate.Equals(dateChoice.EffectiveDate))
                {
                    EffectiveDate = dateChoice.EffectiveDate;
                    datesChanged = true;
                    recalculationNeeded = true;
                }
                
                if (dateChoice.UpdateRecordedDate)
                {
                    if (!RecordedTransactionDate.Equals(dateChoice.EffectiveDate))
                    {
                        RecordedTransactionDate = dateChoice.EffectiveDate;
                        datesChanged = true;
                    }
                }
                CalculateBills();
                if (datesChanged && EffectiveDateChanged != null)
                {
                    EffectiveDateChanged(this, new EventArgs());
                }
            }
        }

        private void CalculateBills()
        {
            CalculationSummaries.Clear();
            AccountSummary = new TaxBillCalculationSummary(Account,BillType);
            List<PropertyTaxBill> bills;
            if (RETaxAccount != null)
            {
                bills = RETaxAccount.Bills.OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber).ToList();
            }
            else
            {
                bills = PPTaxAccount.Bills.OrderByDescending(b => b.TaxYear).ThenByDescending(b => b.BillNumber)
                    .ToList();
            }
            TaxYears.Clear();
           
            var lastBilledYearFound = false;
            foreach (var bill in bills)
            {
                var summary = billCalculator.CalculateBill(bill, EffectiveDate);                
                AccountSummary.AddTo(summary);
                CalculationSummaries.Add(bill.ID, summary);
                
                var taxYear = (bill.TaxYear.ToString() + bill.BillNumber.ToString()).ToIntegerValue();
                if (summary.Balance > 0)
                {
                    OldestBillYearWithABalance =
                        taxYear;
                }

                if (bill.TaxYear == LatestBilledYear)
                {
                    lastBilledYearFound = true;
                }

                if (bill.RateId != 0)
                {
                    TaxYears.Insert(0,
                        new DescriptionIDPair() {ID = taxYear, Description = bill.TaxYear + "-" + bill.BillNumber});
                }
                else
                {
                    TaxYears.Insert(0,
                        new DescriptionIDPair() { ID = taxYear, Description = bill.TaxYear + "-" + bill.BillNumber  + "*"});
                }

            }

            if (!lastBilledYearFound)
            {
                TaxYears.Add(new DescriptionIDPair(){ID = (LatestBilledYear.ToString() + "1").ToIntegerValue(), Description = LatestBilledYear + "-1*" });
            }

            if (!TaxYears.Any(y => y.ID == ((LatestBilledYear + 1)*10 + 1)))
            {
                TaxYears.Add(new DescriptionIDPair()
                    {ID = ((LatestBilledYear + 1) * 10) + 1, Description = (LatestBilledYear + 1).ToString() + "-1*"});
            }

            if (!TaxYears.Any(y => y.ID == ((LatestBilledYear + 2) * 10 + 1)))
            {
                TaxYears.Add(new DescriptionIDPair()
                    { ID = ((LatestBilledYear + 2) * 10) + 1, Description = (LatestBilledYear + 2).ToString() + "-1*" });
            }

            DefaultPrePayYear = ((LatestBilledYear + 1) * 10) + 1;
            TaxYears.Add(new DescriptionIDPair() { Description = "Auto", ID = 0 });
        }

        public bool ValidateYearAndCode(int taxYear, PropertyTaxPaymentCode paymentCode)
        {
            if (taxYear == 0) // if they chose Auto
            {
                if (paymentCode != PropertyTaxPaymentCode.RegularPayment)
                {
                    return false;
                }
            }
            else
            {
                var taxYearPair = TaxYears.FirstOrDefault(t => t.ID == taxYear);
                if (taxYearPair == null)
                {
                    return false;
                }

                if (taxYearPair.Description.Right(1) == "*")
                {
                    if (paymentCode != PropertyTaxPaymentCode.PrePayment &&
                        paymentCode != PropertyTaxPaymentCode.TaxClubPayment &
                        paymentCode != PropertyTaxPaymentCode.Discount)
                    {
                        return false;
                    }
                }
                else if (paymentCode == PropertyTaxPaymentCode.PrePayment)
                {
                    return false;
                }
            }

            return true;
        }

        public PropertyTaxBill GetBill(int yearBill)
        {
            if (BillType == PropertyTaxBillType.Personal)
            {
                return GetPersonalPropertyBill(yearBill);
            }
            else
            {
                return GetRealEstateBill(yearBill);
            }
        }

        public string GetReceivableAccount(int taxYear, bool isLien)
        {
            if (activeModuleSettings.CashReceiptingIsActive)
            {
                ReceiptType receiptType;
                if (BillType == PropertyTaxBillType.Personal)
                {
                    receiptType = receiptTypesService.PersonalPropertyTypes.FirstOrDefault();
                }
                else
                {
                    if (isLien)
                    {
                        receiptType = receiptTypesService.RealEstateTypes.FirstOrDefault(r => r.IsLienType() && !r.IsTaxAcquired());
                    }
                    else
                    {
                        receiptType = receiptTypesService.RealEstateTypes.FirstOrDefault(r => !r.IsLienType() && !r.IsTaxAcquired());
                    }
                }
                if (receiptType != null)
                {
                    if (receiptType.Account1.StartsWith("G"))
                    {
                        if (receiptType.Year1.GetValueOrDefault())
                        {
                            return receiptType.Account1.Substring(0, receiptType.Account1.Length - 2) +
                                   taxYear.ToString().Right(2);
                        }
                    }
                   
                }
            }

            return "M NO ENTRY";
        }

        public string GetCLAccount(PropertyTaxPaymentCode payCode, int taxYear, bool isLien, bool isTaxAcquired, PropertyTaxNonBudgetaryPaymentType type = PropertyTaxNonBudgetaryPaymentType.None)
        {
            if (activeModuleSettings.CashReceiptingIsActive)
            {
                ReceiptType receiptType;
                if (BillType == PropertyTaxBillType.Personal)
                {
                    receiptType = receiptTypesService.PersonalPropertyTypes.FirstOrDefault();
                }
                else
                {
                    receiptType = receiptTypesService.RealEstateTypes.FirstOrDefault(r => r.IsLienType() == isLien && r.IsTaxAcquired() == isTaxAcquired);
                }

                if (receiptType != null)
                {
                    switch (payCode)
                    {
                        case PropertyTaxPaymentCode.NonBudgetary:
                            switch (type)
                            {
                                case PropertyTaxNonBudgetaryPaymentType.Principal:
                                    if (!string.IsNullOrWhiteSpace(receiptType.Account1))
                                    {
                                        return ReturnFormattedAccount(receiptType.Account1, receiptType.Year1 ?? false, taxYear);
                                    }
                                    break;
                                case PropertyTaxNonBudgetaryPaymentType.Interest:
                                    if (isLien)
                                    {
                                        if (!string.IsNullOrWhiteSpace(receiptType.Account3))
                                        {
                                            return ReturnFormattedAccount(receiptType.Account3, receiptType.Year3 ?? false, taxYear);
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrWhiteSpace(receiptType.Account2))
                                        {
                                            return ReturnFormattedAccount(receiptType.Account2, receiptType.Year2 ?? false, taxYear);
                                        }
                                    }
                                    break;
                                case PropertyTaxNonBudgetaryPaymentType.PreLienInterest:
                                    if (!string.IsNullOrWhiteSpace(receiptType.Account2))
                                    {
                                        return ReturnFormattedAccount(receiptType.Account2, receiptType.Year2 ?? false, taxYear);
                                    }
                                    break;
                                case PropertyTaxNonBudgetaryPaymentType.Cost:
                                    if (!string.IsNullOrWhiteSpace(receiptType.Account4))
                                    {
                                        return ReturnFormattedAccount(receiptType.Account4, receiptType.Year4 ?? false, taxYear);
                                    }
                                    break;
                            }
                            break;
                        case PropertyTaxPaymentCode.Abatement:
                        case PropertyTaxPaymentCode.RefundedAbatement:
                            if (!string.IsNullOrWhiteSpace(receiptType.Account6))
                            {
                                return ReturnFormattedAccount(receiptType.Account6, receiptType.Year6 ?? false, taxYear); ;
                            }
                            break;
                        case PropertyTaxPaymentCode.Discount:
                            if (!receiptType.IsLienType())
                            {
                                return ReturnFormattedAccount(receiptType.Account3, receiptType.Year3 ?? false, taxYear);
                            }

                            break;
                    }
                }
            }

            switch (payCode)
            {
                case PropertyTaxPaymentCode.Abatement:
                    return "M ABATEMENT";
                    break;
                case PropertyTaxPaymentCode.Discount:
                    return "M DISCOUNT";
                    break;
            }
            return "";
        }

        private string ReturnFormattedAccount(string account, bool useCurrentYear, int taxYear)
        {
            if (account.StartsWith("G"))
            {
                if (useCurrentYear)
                {
                    return account.Substring(0, account.Length - 2) + taxYear.ToString().Substring(2,2);
                }
            }

            return account;
        }
        public (bool success, string message,string caption) ReversePayment(PaymentRec payment)
        {
            var paymentInfo = new PaymentSetupInfo();
            var bill = GetBill(payment.Year.GetValueOrDefault());
            var recordedTransactionDate = DateTime.Today;
            if (payment.Id == 0)
            {
                Payments payments;
                if (!payment.IsLien())
                {
                    payments = bill.NewPayments;
                }
                else
                {
                    payments = bill.Lien.NewPayments;
                }               

                var laterPayments = payments.GetPaymentsAfterListedOrder(payment);
                var paymentsList = laterPayments.GetAll();
                if (paymentsList.Any())
                {
                    if (paymentsList.Any(p => p.IsCreatedInterest()))
                    {
                        return (success: false, message: "You must remove later created payments first",
                            caption: "Cannot Remove");
                    }
                }
                RemovePendingPayments(payments.GetAll().Where(p => p.TransactionIdentifier == payment.TransactionIdentifier));
                if (!payment.IsLien())
                {
                    if (!bill.NewPayments.HasAny())
                    {
                        bill.ResetInterestDates();
                    }
                }
                else
                {
                    if (!bill.Lien.NewPayments.HasAny())
                    {
                        bill.ResetInterestDates();
                    }
                }
                CalculateBills();
                return (success: true, message: "", caption: "");
            }

            if (!payment.IsLien() && bill.HasLien())
            {
                return (success: false, message: "The selected payment was made prior to lien and cannot be reversed",
                    caption: "Invalid Option");
            }

           // CheckForDiscount();
            paymentInfo.YearBill = bill.YearBill();
            if (payment.CashDrawer == "Y" && payment.DailyCloseOut == 0)
            {
                paymentInfo.AffectCash = "Y";
                paymentInfo.AffectCashDrawer = "Y";
            }
            else
            {
                paymentInfo.AffectCashDrawer = "N";
                if (payment.GeneralLedger == "N")
                {
                    paymentInfo.AffectCash = "N";
                    paymentInfo.Account = payment.BudgetaryAccountNumber;
                }
                else
                {
                    paymentInfo.AffectCash = "Y";
                }
            }

            paymentInfo.TransactionDate = DateTime.Today;
            paymentInfo.Reference = "REVERSE";
            paymentInfo.Period = payment.Period.ToIntegerValue();
            switch (payment.GetPayCode())
            {
                case PropertyTaxPaymentCode.RegularPayment:
                case PropertyTaxPaymentCode.Correction:
                case PropertyTaxPaymentCode.PrePayment:
                    paymentInfo.PayCode = PropertyTaxPaymentCode.Correction;
                    if (payment.RecordedTransactionDate.HasValue)
                    {
                        recordedTransactionDate = payment.RecordedTransactionDate.GetValueOrDefault();
                    }

                    if (bill.Payments.HasCreatedInterest())
                    {
                        if (bill.PreviousInterestAppliedDate == null ||
                            bill.PreviousInterestAppliedDate == DateTime.MinValue)
                        {
                            return(success: false,message: "This transaction contains no previous interest date information, please correct manually.", caption: "Missing Date");
                        }

                        var lastPayment = bill.Payments.LastRecordedPayment();
                        if (lastPayment != null)
                        {
                            if (lastPayment.Id != payment.Id)
                            {
                                return (success: false, message:
                                    "Only the last payment applied can be reversed when interest was charged.",
                                    caption: "Invalid Payment");
                            }
                        }
                    }
                    else if (bill.Payments.HasDosPayments())
                    {
                        return(success: false,"This transaction contains older information, please correct manually", caption:
                            "Correct Manually");
                    }

                    break;
                case PropertyTaxPaymentCode.Discount:
                    break;
                case PropertyTaxPaymentCode.Abatement:
                    break;
                case PropertyTaxPaymentCode.TaxClubPayment:
                    break;
                case PropertyTaxPaymentCode.NonBudgetary:
                    break;
                case PropertyTaxPaymentCode.OverpayRefund:
                    break;
                default:
                    paymentInfo.PayCode = PropertyTaxPaymentCode.Correction;
                    break;
            }

            paymentInfo.Principal = payment.Principal.GetValueOrDefault() * -1;
            paymentInfo.Interest = (payment.CurrentInterest.GetValueOrDefault() + payment.PreLienInterest.GetValueOrDefault()) * -1;
            paymentInfo.Costs = payment.LienCost.GetValueOrDefault() * -1;
            var newPayment = PaymentFromPaymentInfo(paymentInfo);
            newPayment.IsReversal = true;
            bill.AddNewPayments(newPayment.ToPayments());
            CalculateBills();
            return (success: true, message: "", caption: "");
        }
        
        public IEnumerable<PaymentRec> PendingPayments { get ; private set; }
        public (bool success, string message, string caption) ValidateAccount(string account)
        {
            if (!OriginIsTaxCollections && activeModuleSettings.BudgetaryIsActive)
            {
                if (account.Length > 2 && !account.Contains("_"))
                {
                    if (!account.StartsWith("M"))
                    {
                        
                    }
                }
            }

            return (success:true,message:"",caption: "");
        }

        public (bool success, string message, string caption) AddPendingPayment(PaymentSetupInfo paymentInfo)
        {
            if (paymentInfo.AffectCash == "N" && paymentInfo.AffectCashDrawer == "N" && paymentInfo.PayCode != PropertyTaxPaymentCode.Abatement && paymentInfo.PayCode != PropertyTaxPaymentCode.Discount && paymentInfo.PayCode != PropertyTaxPaymentCode.RefundedAbatement)
            {
                var accountValidation = ValidateAccount(paymentInfo.Account);
                if (!accountValidation.success)
                {
                    return (success: false, message: accountValidation.message, caption: accountValidation.caption);
                }
            }

            if (paymentInfo.Principal == 0 && paymentInfo.Interest == 0 && paymentInfo.Costs == 0 &&
                paymentInfo.PrelienInterest == 0)
            {
                return (success: false, message: "No values were specified", "No Values");
            }

            switch (paymentInfo.PayCode)
            {
                case PropertyTaxPaymentCode.Abatement:
                    break;
                case PropertyTaxPaymentCode.RefundedAbatement:
                    break;
                case PropertyTaxPaymentCode.Discount:
                    break;
                case PropertyTaxPaymentCode.RegularPayment:
                case PropertyTaxPaymentCode.Correction:
                    break;
                case PropertyTaxPaymentCode.TaxClubPayment:
                    break;
                case PropertyTaxPaymentCode.PrePayment:
                    break;
                case PropertyTaxPaymentCode.NonBudgetary:
                    break;
                case PropertyTaxPaymentCode.OverpayRefund:
                    paymentInfo.Principal = paymentInfo.Principal + paymentInfo.Costs + paymentInfo.Interest +
                                            paymentInfo.PrelienInterest;
                    paymentInfo.Costs = 0;
                    paymentInfo.Interest = 0;
                    paymentInfo.PrelienInterest = 0;
                    break;
            }

            CreatePendingPayments(paymentInfo);
            CalculateBills();
            return (success: true, message: "",caption:"");
        }

        private void CreatePendingPayments(PaymentSetupInfo paymentInfo)
        {
            var paymentSetup = paymentInfo.GetCopy();
            var taxAccount = TaxAccount;
            decimal amountOver = 0;
            if (paymentInfo.YearBill != 0)
            {
               var bill =  taxAccount.Bills.Where(b => b.YearBill() == paymentInfo.YearBill).FirstOrDefault();
               if (bill == null)
               {
                   if (paymentInfo.PayCode == PropertyTaxPaymentCode.PrePayment)
                   {
                       bill = CreatePrePayBill(paymentInfo.YearBill);
                       TaxAccount.InsertBills(new List<PropertyTaxBill>() { bill });                       
                    }
               }
               if (bill != null)
               {
                   amountOver = CreatePendingPayment(bill,paymentSetup,true);
               }
            }
            else
            {
                var totalOwed = AccountSummary.Balance;
                var applyAsOverpayOnLastBill = false;
                amountOver = paymentInfo.GetTotal();
               // var bills = taxAccount.Bills.Where(b => b.BillSummary.GetNetOwed() > 0).OrderBy(b => b.YearBill()).ToList();
                
               // PropertyTaxBill lastBill;
                
                if (amountOver > totalOwed)
                {
                    var prepayBill = GetDefaultPrePayBill();
                    if (prepayBill == null)
                    {
                        applyAsOverpayOnLastBill = !commandDispatcher.Send(new GetYesOrNoResponse("There is an overpayment on the last billed year. Would you like to create a new billing year and apply the overpayment to that?","Overpayment",false)).Result;
                        //if (!bills.Any() && applyAsOverpayOnLastBill)
                        //{
                        //    bills = taxAccount.Bills.OrderByDescending(b => b.YearBill()).Take(1).ToList();
                        //}
                    }
                }
                AutoCreatePayments(paymentInfo,applyAsOverpayOnLastBill);
                //lastBill = bills.LastOrDefault();


                //foreach (var bill in bills)
                //{
                //    var applyExtraAsOverpay = false;
                //    if (bill.ID == lastBill.ID)
                //    {
                //        applyExtraAsOverpay = applyAsOverpayOnLastBill;
                //    }
                //    paymentSetup.TransactionIdentifier = Guid.NewGuid();
                //    amountOver = CreatePendingPayment(bill, paymentSetup, applyExtraAsOverpay);
                //    paymentSetup.Principal = amountOver;
                //    if (paymentSetup.Principal == 0)
                //    {
                //        break;
                //    }
                //}

                //if (amountOver > 0)
                //{
                //    CreatePrePayPayment(amountOver, paymentSetup.TransactionDate);
                //}
            }
        }

        private void AutoCreatePayments(PaymentSetupInfo paymentInfo, bool applyAsOverpayOnLastBill)
        {
            var paymentSetup = paymentInfo.GetCopy();
            var bills = TaxAccount.Bills.Where(b => b.BillSummary.GetNetOwed() > 0).OrderBy(b => b.YearBill()).ToList();
            var totalOwed = AccountSummary.Balance;
            decimal amountOver = paymentInfo.GetTotal();
            PropertyTaxBill lastBill;
            lastBill = TaxAccount.Bills.Where(b => b.RateId > 0).OrderByDescending(b => b.YearBill()).FirstOrDefault();
            var prepayBill = GetDefaultPrePayBill();
            
            foreach (var bill in bills)
            {
                var applyExtraAsOverpay = false;
                if (bill.ID == lastBill.ID)
                {
                    applyExtraAsOverpay = applyAsOverpayOnLastBill;
                }
                paymentSetup.TransactionIdentifier = Guid.NewGuid();
                amountOver = CreatePendingPayment(bill, paymentSetup, applyExtraAsOverpay);
                paymentSetup.Principal = amountOver;
                if (paymentSetup.Principal == 0)
                {
                    break;
                }
            }

            if (amountOver > 0)
            {
                if (lastBill != null && applyAsOverpayOnLastBill)
                {
                    CreatePendingPayment(lastBill, paymentSetup, true);
                }
                else
                {
                    CreatePrePayPayment(amountOver, paymentSetup.TransactionDate,paymentInfo.AffectCashDrawer);
                }
                
            }
        }
        private PropertyTaxBill GetDefaultPrePayBill()
        {
            return TaxAccount.Bills.Where(b => b.YearBill() == DefaultPrePayYear).FirstOrDefault();            
        }
        private void CreatePrePayPayment(decimal amountOver,DateTime transactionDate,string affectCashDrawer)
        {
            var bill = GetDefaultPrePayBill();
            if (bill == null)
            {
                bill = CreatePrePayBill(DefaultPrePayYear);
                TaxAccount.InsertBills(new List<PropertyTaxBill>(){bill});
            }

            PaymentRec paymentRec = PaymentFromPaymentInfo(new PaymentSetupInfo()
            {
                Account = "",
                Reference = "PREPAY-A",
                PayCode = PropertyTaxPaymentCode.PrePayment,
                Principal = amountOver,
                YearBill = bill.YearBill(),
                Costs = 0,
                Interest = 0,
                PrelienInterest = 0,
                AffectCash = "Y",
                AffectCashDrawer = affectCashDrawer,
                Period = 0,
                TransactionDate = transactionDate               
            });
            paymentRec.BillId = bill.ID;
            //TaxAccount.ApplyPayments(paymentRec.ToPayments());
            bill.AddNewPayments(paymentRec.ToPayments());
        }

        private PropertyTaxBill CreatePrePayBill(int yearBill)
        {
            PropertyTaxBill taxBill;
            if (BillType == PropertyTaxBillType.Personal)
            {
                taxBill = CreatePPBill();
            }
            else
            {
                taxBill = CreateREBill();
            }

            var taxYear = yearBill.ToString().Substring(0, 4).ToIntegerValue();
            return new PropertyTaxBill()
            {
                Account = Account,
                BillType = BillType,
                BillNumber = 1,
                BillSummary = { DemandFeesPaid = 0, DemandFeesCharged = 0, InterestPaid = 0, InterestCharged = 0, PrincipalCharged = 0, PrincipalPaid = 0},
                CityStateZip =  TaxAccount.MailingAddress.City + " " + TaxAccount.MailingAddress.State + " " + TaxAccount.MailingAddress.Zip,
                Zip = TaxAccount.MailingAddress.Zip,
                LienID = 0,
                MailingAddress1 = TaxAccount.MailingAddress.Address1,
                MailingAddress2 = TaxAccount.MailingAddress.Address2,
                MailingAddress3 = TaxAccount.MailingAddress.Address3,
                StreetName = TaxAccount.LocationStreet(),
                StreetNumber = TaxAccount.LocationNumber(),
                Name1 = TaxAccount.Name1(),
                Name2 = TaxAccount.Name2(),
                RateId =  0,
                TaxYear = taxYear,
                InterestAppliedThroughDate = new DateTime(1899,12,30),
                PreviousInterestAppliedDate = new DateTime(1899,12,30) 
                               
            };
        }

        private RealEstateTaxBill CreateREBill()
        {
            return new RealEstateTaxBill()
            {
                MapLot = RETaxAccount.AccountBriefInfo.MapLot
            };
        }

        private PersonalPropertyTaxBill CreatePPBill()
        {
            return new PersonalPropertyTaxBill(){};
        }
        private decimal CreatePendingPayment(PropertyTaxBill bill, PaymentSetupInfo paymentInfo, bool applyExtraAsOverpay)
        {
            decimal amountLeft = 0;
            if (CalculationSummaries.ContainsKey(bill.ID))
            {
                PaymentRec paymentRec;
                Payments payments;
                var calcSummary = CalculationSummaries[bill.ID];
                if ((paymentInfo.PayCode == PropertyTaxPaymentCode.RegularPayment || paymentInfo.PayCode == PropertyTaxPaymentCode.Correction ||
                     paymentInfo.PayCode == PropertyTaxPaymentCode.NonBudgetary) && calcSummary.Interest > 0 && !paymentInfo.ForcePaymentDistributionAsIs)
                {
                    var chargedInterestPayment = CreateChargedInterestPayment(bill, paymentInfo, calcSummary.Interest);
                }
                if (paymentInfo.PayCode == PropertyTaxPaymentCode.RegularPayment  && !paymentInfo.ForcePaymentDistributionAsIs)
                {
                    //var chargedInterestPayment = CreateChargedInterestPayment(bill, paymentInfo, calcSummary.Interest);
                    // var interestIdentifier = chargedInterestPayment.PaymentIdentifier;
                    var newInfo = paymentInfo.GetCopy();                    
                    if (bill.HasLien())
                    {
                        var paidAmounts = MakePaymentInfoForLien(bill.Lien.LienSummary, paymentInfo.Principal);
                        newInfo.Costs = paidAmounts.costs;
                        newInfo.Interest = paidAmounts.interest;
                        newInfo.Principal = paidAmounts.principal;
                        newInfo.PrelienInterest = paidAmounts.preLienInterest;
                        if (paidAmounts.remaining > 0 && applyExtraAsOverpay)
                        {
                            newInfo.Principal += paidAmounts.remaining;
                        }
                        else
                        {
                            amountLeft = paidAmounts.remaining;
                        }
                    }
                    else
                    {
                        var paidAmounts = MakePaymentInfoForBill(bill.BillSummary, paymentInfo.Principal);
                        newInfo.Costs = paidAmounts.costs;
                        newInfo.Interest = paidAmounts.interest;
                        newInfo.Principal = paidAmounts.principal;
                        if (paidAmounts.remaining > 0 && applyExtraAsOverpay)
                        {
                            newInfo.Principal += paidAmounts.remaining;
                        }
                        else
                        {
                            amountLeft = paidAmounts.remaining;
                        }
                        newInfo.PrelienInterest = 0;
                    }

                    paymentRec = CreatePaymentRecForBill(bill, newInfo);
                    payments = paymentRec.ToPayments();
                    // paymentRec.ChargedInterestIdentifier = interestIdentifier;
                }
                else if (paymentInfo.PayCode == PropertyTaxPaymentCode.RefundedAbatement && !paymentInfo.ForcePaymentDistributionAsIs)
                {
                    var opposingRec = PaymentFromPaymentInfo(paymentInfo);
                    opposingRec.Principal = opposingRec.Principal * -1;
                    opposingRec.LienCost = opposingRec.LienCost * -1;
                    opposingRec.PreLienInterest = opposingRec.PreLienInterest * -1;
                    opposingRec.CurrentInterest = opposingRec.CurrentInterest * -1;
                    opposingRec.BudgetaryAccountNumber = GetReceivableAccount(bill.TaxYear, bill.HasLien());
                    opposingRec.Period = "1";
                    opposingRec.PaymentIdentifier = Guid.NewGuid(); //Cash receipts will put the two as one entry if they don't have different identifiers
                    paymentRec = PaymentFromPaymentInfo(paymentInfo);
                    payments = paymentRec.ToPayments();
                    payments.AddPayment(opposingRec);
                }
                else
                {
                    paymentRec = PaymentFromPaymentInfo(paymentInfo);
                    payments = paymentRec.ToPayments();
                    
                }

                var nonZeroPayments = payments.GetNonZeroPayments();
                if (nonZeroPayments.HasAny())
                {
                    bill.AddNewPayments(nonZeroPayments);
                }
            }
            else if (bill.ID == 0 && paymentInfo.PayCode == PropertyTaxPaymentCode.PrePayment)
            {
                var paymentRec = PaymentFromPaymentInfo(paymentInfo);
                if (paymentRec.Principal != 0 || paymentRec.CurrentInterest != 0 || paymentRec.PreLienInterest != 0 ||
                    paymentRec.LienCost != 0)
                {
                    bill.AddNewPayments(paymentRec.ToPayments());
                }
            }

            return amountLeft;
        }

        private (decimal principal, decimal preLienInterest, decimal interest, decimal costs, decimal remaining) MakePaymentInfoForLien(PropertyTaxLienSummary lienSummary, decimal amountToPay)
        {
            var amountLeft = amountToPay;
            decimal paidInterest = 0;
            decimal paidPrincipal = 0;
            decimal paidCosts = 0;
            decimal paidPreLienInterest = 0;
            if (amountLeft >= lienSummary.GetNetOwed())
            {
                paidPreLienInterest = lienSummary.GetNetPreLienInterest();
                paidCosts = lienSummary.GetNetCosts();
                paidInterest = lienSummary.GetNetPostLienInterest();
                paidPrincipal = lienSummary.GetNetPrincipal();
                amountLeft -= paidPreLienInterest;
                amountLeft -= paidCosts;
                amountLeft -= paidInterest;
                amountLeft -= paidPrincipal;
            }
            else
            {
                if (lienSummary.GetNetPreLienInterest() > 0)
                {
                    if (amountLeft > lienSummary.GetNetPreLienInterest())
                    {
                        paidPreLienInterest = lienSummary.GetNetPreLienInterest();
                        amountLeft -= paidPreLienInterest;
                    }
                    else
                    {
                        paidPreLienInterest = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (lienSummary.GetNetPostLienInterest() > 0 && amountLeft > 0)
                {
                    if (amountLeft > lienSummary.GetNetPostLienInterest())
                    {
                        paidInterest = lienSummary.GetNetPostLienInterest();
                        amountLeft -= paidInterest;
                    }
                    else
                    {
                        paidInterest = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (lienSummary.GetNetCosts() > 0 && amountLeft > 0)
                {
                    if (amountLeft > lienSummary.GetNetCosts())
                    {
                        paidCosts = lienSummary.GetNetCosts();
                        amountLeft -= paidCosts;
                    }
                    else
                    {
                        paidCosts = amountLeft;
                        amountLeft = 0;

                    }
                }

                if (lienSummary.GetNetPrincipal() > 0 && amountLeft > 0)
                {
                    if (amountLeft > lienSummary.GetNetPrincipal())
                    {
                        paidPrincipal = lienSummary.GetNetPrincipal();
                        amountLeft -= paidPrincipal;
                    }
                    else
                    {
                        paidPrincipal = amountLeft;
                        amountLeft = 0;
                    }
                }
            }
            return (principal: paidPrincipal, preLienInterest: paidPreLienInterest, interest: paidInterest, costs: paidCosts, remaining: amountLeft);
        }

        private PaymentRec CreatePaymentRecForBill(PropertyTaxBill bill, PaymentSetupInfo paymentInfo)
        {
            var paymentRec = new PaymentRec()
            {
                Account = bill.Account.GetValueOrDefault(),
                ActualSystemDate = DateTime.Now,
                BillCode = bill.BillType == PropertyTaxBillType.Personal ? "P" : "R",
                BillId = bill.HasLien() ? bill.LienID : bill.ID,
                BudgetaryAccountNumber = paymentInfo.Account,
                CashDrawer = paymentInfo.AffectCashDrawer,
                ChargedInterestdate = EffectiveDate,
                ChargedInterestId = 0,
                Code = paymentInfo.PayCode.ToCode(),
                Reference = paymentInfo.Reference,
                PaymentIdentifier = Guid.NewGuid(),
                CorrelationIdentifier = CorrelationIdentifier,
                //ChargedInterestIdentifier = null,
                Comments = paymentInfo.Comment,
                CurrentInterest = paymentInfo.Interest,
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                DailyCloseOut = 0,
                EffectiveInterestDate = EffectiveDate,
                ExternalPaymentIdentifier = "",
                GeneralLedger = paymentInfo.AffectCash,
                IsReversal = paymentInfo.PayCode == PropertyTaxPaymentCode.Correction && paymentInfo.Reference == "REVERSE" ,
                LienCost = paymentInfo.Costs,
                PaidBy = "",
                Period = paymentInfo.Period == 0 ? "A" : paymentInfo.Period.ToString(),
                PreLienInterest = paymentInfo.PrelienInterest,
                Principal = paymentInfo.Principal,
                RecordedTransactionDate = paymentInfo.TransactionDate,
                SetEidate = new DateTime(1899, 12, 30),
                Teller = "",
                Year = bill.YearBill(),
                TransactionNumber = "",
                TransactionIdentifier = paymentInfo.TransactionIdentifier
            };

            if (bill.HasLien())
            {
                paymentRec.BillCode = "L";
                paymentRec.OldInterestDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.PreviousInterestAppliedDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;
               // bill.Lien.AddNewPayments(paymentRec.ToPayments());
            }
            else
            {
                paymentRec.OldInterestDate = bill.InterestAppliedThroughDate;
                bill.PreviousInterestAppliedDate = bill.InterestAppliedThroughDate;
                bill.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;
               // bill.AddNewPayments(paymentRec.ToPayments());
            }

            return paymentRec;
        }

        private PropertyTaxTransaction CreateTransactionForBill(PropertyTaxBill bill)
        {
            var transaction = new PropertyTaxTransaction()
            {
                Account = Account,
                Id = Guid.NewGuid(),
                BillType = BillType,
                BillId = bill.ID,
                CorrelationIdentifier = CorrelationIdentifier,
                IsLien = bill.HasLien(),
                TownId = TaxAccount.TownId,
                ActualDateTime = DateTime.Now,
                YearBill = bill.YearBill()
            };
            if (BillType == PropertyTaxBillType.Real)
            {
                transaction.IsTaxAcquired = RETaxAccount.AccountBriefInfo.IsTaxAcquired;
            }

            if (BillType == PropertyTaxBillType.Personal)
            {
                //transaction.TransactionTypeCode = "92";
                transaction.TransactionTypeCode = "PPTAX";
            }
            else
            {
                if (bill.HasLien())
                {
                    if (transaction.IsTaxAcquired)
                    {
                        //transaction.TransactionTypeCode = "891";
                        transaction.TransactionTypeCode = "TARETAXLIEN";
                    }
                    else
                    {
                        //transaction.TransactionTypeCode = "91";
                        transaction.TransactionTypeCode = "RETAXLIEN";
                    }
                }
                else
                {
                    if (transaction.IsTaxAcquired)
                    {
                        //transaction.TransactionTypeCode = "890";
                        transaction.TransactionTypeCode = "TARETAX";
                    }
                    else
                    {
                        //transaction.TransactionTypeCode = "90";
                        transaction.TransactionTypeCode = "RETAX";
                    }
                }
            }
            return transaction;
        }

        private (decimal principal,decimal interest, decimal costs,decimal remaining) MakePaymentInfoForBill(PropertyTaxBillSummary billSummary, decimal amountToPay)
        {
            var amountLeft = amountToPay ;           
            decimal paidInterest = 0;
            decimal paidPrincipal = 0;
            decimal paidCosts = 0;
            if (amountToPay >= billSummary.GetNetOwed())
            {
                paidPrincipal = billSummary.GetNetPrincipal();
                paidCosts = billSummary.GetNetCosts();                
                paidInterest = billSummary.GetNetInterest();
                amountLeft = amountToPay - billSummary.GetNetOwed();
            }
            else
            {
                if (billSummary.GetNetInterest()  > 0)
                {
                    if (amountLeft > billSummary.GetNetInterest())
                    {
                        paidInterest = billSummary.GetNetInterest();
                        amountLeft -= paidInterest;
                    }
                    else
                    {
                        paidInterest = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (billSummary.GetNetCosts() > 0 && amountLeft > 0)
                {
                    if (amountLeft > billSummary.GetNetCosts())
                    {
                        paidCosts = billSummary.GetNetCosts();
                        amountLeft -= paidCosts;
                    }
                    else
                    {
                        paidCosts = amountLeft;
                        amountLeft = 0;
                    }
                }

                if (billSummary.GetNetPrincipal() > 0 && amountLeft > 0)
                {
                    if (amountLeft > billSummary.GetNetPrincipal())
                    {
                        paidPrincipal = billSummary.GetNetPrincipal();
                        amountLeft -= paidPrincipal;
                    }
                    else
                    {
                        paidPrincipal = amountLeft;
                        amountLeft = 0;
                    }
                }

            }

            return (principal: paidPrincipal, interest: paidInterest, costs: paidCosts, remaining: amountLeft);
        }

        private PaymentRec CreateChargedInterestPayment(PropertyTaxBill bill, PaymentSetupInfo paymentInfo, decimal interest)
        {
            var paymentRec = new PaymentRec()
            {
                Account = bill.Account.GetValueOrDefault(),
                ActualSystemDate = DateTime.Now,
                BillCode = bill.BillType == PropertyTaxBillType.Personal ? "P" : "R",
                BillId   = bill.HasLien() ? bill.LienID : bill.ID,
                BudgetaryAccountNumber = paymentInfo.Account,
                CashDrawer = paymentInfo.AffectCashDrawer,
                ChargedInterestdate = EffectiveDate,
                ChargedInterestId = 0,
                Code = PropertyTaxPaymentCode.Interest.ToCode(),
                Reference = "CHGINT",
                PaymentIdentifier = Guid.NewGuid(),
                CorrelationIdentifier = CorrelationIdentifier,
                //ChargedInterestIdentifier = null,
                Comments =  "",
                CurrentInterest = -1 * interest,
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                DailyCloseOut = 0,
                EffectiveInterestDate = EffectiveDate,
                ExternalPaymentIdentifier = "",
                GeneralLedger = "Y",
                IsReversal = false,
                LienCost = 0,
                PaidBy = "",
                Period = "1",
                PreLienInterest = 0,
                Principal = 0,
                RecordedTransactionDate = paymentInfo.TransactionDate,
                SetEidate = new DateTime(1899,12,30),
                Teller = "",
                Year = bill.YearBill(),
                TransactionIdentifier = paymentInfo.TransactionIdentifier
                
            };
           
            if (bill.HasLien())
            {
                paymentRec.BillCode = "L";
                paymentRec.OldInterestDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.PreviousInterestAppliedDate = bill.Lien.InterestAppliedThroughDate;
                bill.Lien.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;               
            }
            else
            {
                paymentRec.OldInterestDate = bill.InterestAppliedThroughDate;
                bill.PreviousInterestAppliedDate = bill.InterestAppliedThroughDate;
                bill.InterestAppliedThroughDate = paymentRec.EffectiveInterestDate;                
            }
            bill.AddNewPayments(paymentRec.ToPayments());
            return paymentRec;
        }


        private void RemovePendingPayments(IEnumerable<PaymentRec> payments)
        {
            foreach (var payment in payments)
            {
                RemovePendingPayment(payment);
            }
        }
        private void RemovePendingPayment(PaymentRec payment)
        {
            PropertyTaxAccount taxAccount;
            switch (BillType)
            {
                case PropertyTaxBillType.Personal:
                    taxAccount = PPTaxAccount;
                    break;
                case PropertyTaxBillType.Real:
                    taxAccount = RETaxAccount;
                    break;
                default:
                    return;
                    break;
            }

            foreach (var bill in taxAccount.Bills)
            {
                if (bill.HasPayment(payment.PaymentIdentifier.GetValueOrDefault()))
                {
                   bill.RemovePendingPayment(payment);
                    return;
                }
            }
        }

        private PropertyTaxBill GetPersonalPropertyBill(int yearBill)
        {
            var bill = PPTaxAccount.Bills.FirstOrDefault(b => b.YearBill() == yearBill);
            return bill;
        }

        private PropertyTaxBill GetRealEstateBill(int yearBill)
        {
            var bill = RETaxAccount.Bills.FirstOrDefault(b => b.YearBill() == yearBill);
            return bill;
        }

        private PaymentRec PaymentFromPaymentInfo(PaymentSetupInfo paymentInfo)
        {
            if (paymentInfo == null)
            {
                return null;
            }
            var payment = new PaymentRec()
            {
                ActualSystemDate = DateTime.Now,
                Account = Account,
                BudgetaryAccountNumber = paymentInfo.Account,
                GeneralLedger = paymentInfo.AffectCash,
                CashDrawer = paymentInfo.AffectCashDrawer,
                Code = paymentInfo.PayCode.ToCode(),
                CorrelationIdentifier = CorrelationIdentifier,
                EffectiveInterestDate = EffectiveDate,
                RecordedTransactionDate = paymentInfo.TransactionDate,
                Principal = paymentInfo.Principal,
                Period = paymentInfo.Period == 0 ? "A" : paymentInfo.Period.ToString(),
                Reference = paymentInfo.Reference,
                CurrentInterest = paymentInfo.Interest,
                PreLienInterest = paymentInfo.PrelienInterest,
                LienCost = paymentInfo.Costs,
                PaymentIdentifier = Guid.NewGuid(),
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                ChargedInterestId = 0,
                Comments =  "",
                Teller = "",
                PaidBy = "",
                //ChargedInterestIdentifier = null,
                ExternalPaymentIdentifier = paymentInfo.ExternalPaymentIdentifier,
                TransactionIdentifier = paymentInfo.TransactionIdentifier,
                Year = paymentInfo.YearBill
            };
            return payment;
        }

        protected string GetAccountFromReceiptType(PaymentSetupInfo payment)
        {
            if (OriginIsTaxCollections)
            {
                switch (payment.PayCode)
                {
                    case PropertyTaxPaymentCode.Abatement:
                        return "M ABATEMENT";
                        break;
                    case PropertyTaxPaymentCode.Discount:
                        return "M DISCOUNT";
                        break;
                    default:
                        return "";
                        break;
                }
            }

            ReceiptType rType;
            switch (BillType)
            {
                case PropertyTaxBillType.Personal:
                    rType = receiptTypesService.PersonalPropertyTypes.FirstOrDefault(t => !t.IsTaxAcquired());
                    break;
                default:
                    
                    rType = receiptTypesService.RealEstateTypes.FirstOrDefault(t =>
                        !t.IsLienType() && !t.IsTaxAcquired());
                    break;
            }
            switch (payment.PayCode)
            {
                case PropertyTaxPaymentCode.Abatement:
                case PropertyTaxPaymentCode.RefundedAbatement:
                    if (rType.Year6.GetValueOrDefault())
                    {
                        throw new NotImplementedException();
                        return rType.Account6.Substring(0, rType.Account6.Length);
                    }
                    else
                    {
                        return rType.Account6;
                    }
                    
                    break;
                case PropertyTaxPaymentCode.Discount:
                    return rType.Account3;
                    break;
                default:
                    return "";
            }
        }

        public void Cancel()
        {
            eventPublisher.Publish(new TransactionCancelled(CorrelationIdentifier, null));
        }

        public void CompleteTransaction(bool printReceipt)
        {
            var payments = TaxAccount.GetPendingPayments();            
            var pendingPayments = payments.GetAll();            
            var transactions = new List<PropertyTaxTransaction>();

            foreach (var payment in pendingPayments)
            {
               
                var bill = TaxAccount.Bills.FirstOrDefault(b => b.YearBill() == payment.Year.GetValueOrDefault());
                if (bill != null)
                {
                        PropertyTaxTransaction transaction;
                    transaction = transactions.FirstOrDefault(t => t.Id == payment.TransactionIdentifier);
                    if (transaction == null || transaction.OverrideAccount != payment.BudgetaryAccountNumber)
                    {
                        transaction = CreateTransactionForBill(bill);
                        transaction.Id = payment.TransactionIdentifier.GetValueOrDefault();
                        transaction.OverrideAccount = payment.BudgetaryAccountNumber;
                        transactions.Add(transaction);
                    }

                    if (bill.HasLien())
                    {
                        var lienEvents = bill.Lien.NewEvents.GetEvents().Where(ne => ne.EventType == TaxBillEventType.PropertyTaxLienPaid || ne.EventType == TaxBillEventType.PropertyTaxLienPartiallyPaid);
                        foreach (var lienEvent in lienEvents)
                        {
                            if (lienEvent.EventType == TaxBillEventType.PropertyTaxLienPaid)
                            {
                                if (((PropertyTaxLienPaid) lienEvent).PaymentIdentifier == payment.PaymentIdentifier)
                                {
                                    transaction.NewEvents.AddEvent(lienEvent);
                                    //commandDispatcher.Send(new AddLienDischargeNotice(bill.LienID, payment.RecordedTransactionDate.Value, "", bill.ID));
                                    //if (commandDispatcher
                                    //    .Send(new GetYesOrNoResponse(
                                    //        "Would you like to print the Lien Discharge Notice for " +
                                    //        lienEvent.TaxYear + "-" + lienEvent.BillNumber + "?", "Lien Discharge Notice", false))
                                    //    .Result)
                                    //{
                                    //    ShowLienDischargeNotice(bill.ID);
                                    //}
                                }
                            }
                            else if (lienEvent.EventType == TaxBillEventType.PropertyTaxLienPartiallyPaid)
                            {
                                if (((PropertyTaxLienPartiallyPaid) lienEvent).PaymentIdentifier == payment.PaymentIdentifier)
                                {
                                    transaction.NewEvents.AddEvent(lienEvent);
                                }
                            }
                        }

                    }
                    
                    //transaction.EffectiveDateTime = payment.EffectiveInterestDate.GetValueOrDefault();
                    transaction.EffectiveDateTime = payment.RecordedTransactionDate.GetValueOrDefault();
                    transaction.Payments.AddPayment(payment);
                    transaction.ActualDateTime = DateTime.Now;
                    transaction.Reference = bill.Account + "-" + bill.TaxYear.ToString();
                    transaction.IncludeInCashDrawerTotal = payment.CashDrawer == "Y";
                    transaction.AffectCash = payment.GeneralLedger == "Y";
                }
            }

            
            foreach (var transaction in transactions)
            {
                var amountItems = transaction.Payments.ToTransactionAmountItems();
                var lastPayment = transaction.Payments.GetAll().LastOrDefault();
                if (lastPayment != null)
                {
                    transaction.Controls.Add(new ControlItem()
                    {
                        Name = "Control1",
                        Description = "Control1",
                        Value = lastPayment.Period
                    });
                    transaction.Controls.Add(new ControlItem()
                    {
                        Name = "Control2",
                        Description = "Control2",
                        Value = lastPayment.Code
                    });
                    transaction.Controls.Add(new ControlItem()
                    {
                        Name = "Control3",
                        Description = "Control3",
                        Value = (TaxAccount.LocationNumber().ToString() + " " + TaxAccount.LocationStreet()).Trim()
                    });
                    transaction.OverrideAccount = lastPayment.BudgetaryAccountNumber;
                }
                transaction.AddTransactionAmounts(amountItems);
                if (BillType == PropertyTaxBillType.Personal)
                {
                    transaction.PayerPartyId = PPTaxAccount.AccountBriefInfo.PartyId;
                    transaction.PayerName = PPTaxAccount.AccountBriefInfo.Name;
                }
                else
                {
                    transaction.PayerPartyId = RETaxAccount.AccountBriefInfo.PartyId1;
                    //transaction.PayerName = RETaxAccount.AccountBriefInfo.DeedName1;
                    transaction.PayerName = RETaxAccount.OwnerNames();
                }               
            }

            
            if (OriginIsTaxCollections)
            {
                eventPublisher.Publish(new CollectionsTransactionCompleted(transactions));
                var location = TaxAccount.LocationStreet();
                if (TaxAccount.LocationNumber() > 0)
                {
                    location = TaxAccount.LocationNumber().ToString() + " " + location;
                }

                if (printReceipt)
                {
                    commandDispatcher.Send(new ShowCollectionsReceipt(Account, BillType, AccountSummary, false,
                        CurrentOwnerName, location, transactions, TaxAccount.TownId,true));
                }
            }
            else
            {
                eventPublisher.Publish(new TransactionCompleted()
                    {CorrelationId = CorrelationIdentifier, CompletedTransactions = transactions});
            }
        }

        public void ShowRateInfo(int yearBill)
        {
            var bill = TaxAccount.Bills.FirstOrDefault(b => b.YearBill() == yearBill);
            var calcSummary = new TaxBillCalculationSummary(bill.Account.GetValueOrDefault(),bill.BillType);
            if (CalculationSummaries.ContainsKey(bill.ID))
            {
               calcSummary = CalculationSummaries[bill.ID];
               
            }

            string lienDischargeBookPage = "";

            if (bill.HasLien())
            {
	            lienDischargeBookPage = commandDispatcher.Send(new GetDischargeNeededBookPage
                {
                    LienId = bill.LienID
	            }).Result;
            }
            var totalPerDiem = CalculationSummaries.Values.Sum(v => v.PerDiem);
            if (BillType == PropertyTaxBillType.Personal)
            {
                commandDispatcher.Send(new ShowTaxBillInfo(bill, calcSummary, totalPerDiem,PPTaxAccount.AccountTotals,EffectiveDate, lienDischargeBookPage));
            }
            else
            {
                commandDispatcher.Send(new ShowTaxBillInfo(bill, calcSummary, totalPerDiem,RETaxAccount.AccountTotals,EffectiveDate, lienDischargeBookPage));
            }            
        }

        public void ShowPaymentInfo(PaymentRec payment)
        {
            if (payment != null)
            {
                commandDispatcher.Send(new ShowTaxPaymentInfo(payment));
            }
        }

        public void ShowLienInfo(int yearBill)
        {
            var bill = TaxAccount.Bills.FirstOrDefault(b => b.YearBill() == yearBill);
            commandDispatcher.Send(new ShowTaxLienInfo(bill.Lien.LienSummary,bill.LienID,bill.Lien.DateCreated.GetValueOrDefault(), bill.Lien.InterestAppliedThroughDate.GetValueOrDefault(), bill.Lien.Book, bill.Lien.Page,bill.Lien.RateId.GetValueOrDefault(), bill.Lien.Status));
        }

        public bool AllowAutoPaymentFill { get => !taxSettings.DisableAutoPaymentFill; }
        public void ShowAccountInfo()
        {
            if (BillType == PropertyTaxBillType.Personal)
            {
                commandDispatcher.Send(new ShowPersonalPropertyTaxAccountInfo(Account));
            }
            else
            {
                commandDispatcher.Send(new ShowRealEstateTaxAccountInfo(Account));
            }
        }

        public void ShowAccountDetail(IEnumerable<int> expandedYearBills)
        {
            if (BillType == PropertyTaxBillType.Personal)
            {
                ShowPPAccountDetail(expandedYearBills);
            }
            else
            {
                ShowREAccountDetail(expandedYearBills);
            }            
        }

        public void ShowInterestedParties()
        {
            commandDispatcher.Send(new ShowInterestedParties(Account,"CL",0,true,2) );
        }

        public void ShowMortgageHolder()
        {
            if (OriginIsTaxCollections)
            {
                commandDispatcher.Send(new ShowMortgageHolderInCollections(Account,2));
            }
            else
            {
                commandDispatcher.Send(new ShowMortgageHolderInCR(Account, 2));
            }
        }

        public void ShowTaxInformationSheet()
        {
            if (BillType != PropertyTaxBillType.Real)
            {
                return;
            }
            if (OriginIsTaxCollections)
            {
                commandDispatcher.Send(new ShowRETaxInformationSheetInCollections(Account,EffectiveDate));
            }
            else
            {
                commandDispatcher.Send(new ShowRETaxInformationSheetInCR(Account,EffectiveDate));
            }
            
        }

        public void ShowBookPageReport()
        {
            if (BillType != PropertyTaxBillType.Real)
            {
                return;
            }

            if (OriginIsTaxCollections)
            {
                commandDispatcher.Send(new ShowREBookPageReportFromCollections(Account));
            }
            else
            {
                commandDispatcher.Send(new ShowREBookPageReportFromCR(Account));
            }
        }

        public string SelectValidAccount(string account)
        {
            if (activeModuleSettings.BudgetaryIsActive)
            {
                return commandDispatcher.Send(new SelectValidAccount(account,"")).Result;
            }

            return account;
        }

        public void ShowLienDischargeNotice( int billId)
        {
            var bill = (RealEstateTaxBill)TaxAccount.Bills.FirstOrDefault(b => b.ID == billId);
          
            if (bill == null)
            {
                return;
            }
            DateTime datePaid = DateTime.Today;
            var dischargeNeeded = dischargeNeededHandler.ExecuteQuery(new TaxLienDischargeNeededQuery(bill.LienID));
            if (dischargeNeeded == null)
            {
                commandDispatcher.Send(new AddLienDischargeNotice(bill.LienID, datePaid, "", billId));
            }
            else
            {
                datePaid = dischargeNeeded.DatePaid.GetValueOrDefault();
            }

            if (string.IsNullOrWhiteSpace(bill.MapLot))
            {
                bill.MapLot = RETaxAccount.AccountBriefInfo.MapLot;
            }
            commandDispatcher.Send(new ShowLienDischargeNotice(datePaid, bill));
        }

        public void ShowAlternateDischargeNotice()
        {
            commandDispatcher.Send(new ShowAlternateDischargeNotice(RETaxAccount, DateTime.Today));
        }

        public void ReprintReceipt(PaymentRec payment)
        {
            if (payment != null && OriginIsTaxCollections)
            {
                var transaction = new PropertyTaxTransaction()
                {
                    Account = Account,
                    ActualDateTime = payment.ActualSystemDate.GetValueOrDefault(),
                    BillType = payment.BillType(),
                    CorrelationIdentifier = payment.CorrelationIdentifier.GetValueOrDefault(),
                    Controls =  new List<ControlItem>(),
                    EffectiveDateTime = payment.EffectiveInterestDate.GetValueOrDefault(),
                    Id = payment.TransactionIdentifier.GetValueOrDefault(),
                    IncludeInCashDrawerTotal = payment.CashDrawer == "Y",
                    IsLien = payment.IsLien(),
                    TownId = TaxAccount.TownId,
                    YearBill = payment.Year.GetValueOrDefault()
                };
                transaction.Payments = new Payments();
                transaction.Payments.AddPayment(payment);
                var location = TaxAccount.LocationStreet();
                if (TaxAccount.LocationNumber() > 0)
                {
                    location = TaxAccount.LocationNumber().ToString() + " " + location;
                }

                var transactions = new List<PropertyTaxTransaction>(){transaction};
                commandDispatcher.Send(new ShowCollectionsReceipt(Account, BillType, AccountSummary, true, CurrentOwnerName, location, transactions, TaxAccount.TownId,true));
            }
        }

        public void RemoveAllPendingPayments()
        {
            Payments payments;
            foreach (var bill in TaxAccount.Bills)
            {
                if (bill.HasLien())
                {
                    payments = bill.Lien.NewPayments;
                }
                else
                {
                    payments = bill.NewPayments;
                }

                if (payments.HasAny())
                {
                    RemovePendingPayments(payments.GetAll().ToList());
                    bill.ResetInterestDates();
                }
            }
            CalculateBills();
        }

        public void SearchForAccount()
        {
	        if (AllowPayments)
	        {
		        if (BillType == PropertyTaxBillType.Personal)
		        {
			        commandDispatcher.Send(new MakePersonalPropertyTransaction(CorrelationIdentifier, Guid.NewGuid(),
				        OriginIsTaxCollections,0));
		        }
		        else
		        {
			        commandDispatcher.Send(new MakeRealEstateTransaction(CorrelationIdentifier, Guid.NewGuid(),
				        OriginIsTaxCollections,0));
		        }
            }
	        else
	        {
		        commandDispatcher.Send(new ShowTaxAccountSearch(BillType));
            }
        }

        //public bool WriteOffsExistForBill(int yearBill)
        //{
        //    throw new NotImplementedException();
        //}

        public (bool Cancelled, PropertyTaxAmounts Amounts) GetNonBudgetaryPayment(int billId)
        {

            var bill = TaxAccount.Bills.FirstOrDefault(b => b.ID == billId);
            if (bill == null)
            {
                return (Cancelled: true, Amounts: new PropertyTaxAmounts());
            }
            var summaryPair = CalculationSummaries.Where(s => s.Key == bill.ID).FirstOrDefault();
            decimal currentInterest = 0;
            if (summaryPair.Value != null)
            {
                if (summaryPair.Value.Interest > 0)
                {
                    currentInterest = summaryPair.Value.Interest;
                }
            }
            ShowNonBudgetaryCLPayment showCommand;
            if (bill.HasLien())
            {
                var summary = bill.Lien.LienSummary;
                showCommand = new ShowNonBudgetaryCLPayment(summary.GetNetPrincipal(),summary.GetNetPostLienInterest() + currentInterest, summary.GetNetPreLienInterest(),summary.GetNetCosts(), bill.HasLien());
            }
            else
            {
                var summary = bill.BillSummary;
                showCommand = new ShowNonBudgetaryCLPayment(summary.GetNetPrincipal(), summary.GetNetInterest() + currentInterest, 0, summary.GetNetCosts(), bill.HasLien());
            }
            
            //var lien = RETaxAccount.Bills.FirstOrDefault().Lien.LienSummary;
           
            var result = commandDispatcher.Send(showCommand).Result;
            return (Cancelled: result.Cancelled, Amounts: result.Amounts);
        }

        public (bool Cancelled, decimal Principal, decimal Discount) GetDiscount(int billId)
        {
            var bill = TaxAccount.Bills.FirstOrDefault(b => b.ID == billId);
            if (bill == null)
            {
                return (Cancelled: true, Principal: 0, Discount: 0);
            }

            GetPropertyTaxDiscount showCommand;
            var discountRate = taxSettings.DiscountRate;
            if (bill.HasLien())
            {
                var summary = bill.Lien.LienSummary;               
                decimal owed = summary.GetNetOwed();
                var discount = (discountRate * owed).RoundToMoney();
                var principal = owed - discount;
                showCommand = new GetPropertyTaxDiscount( summary.OriginalPrincipalCharged,discountRate,principal,discount);
            }
            else
            {
                var summary = bill.BillSummary;
                decimal owed = summary.GetNetOwed();
                var discount = (discountRate * owed).RoundToMoney();
                var principal = owed - discount;
                showCommand = new GetPropertyTaxDiscount(summary.PrincipalCharged,discountRate,principal,discount);
            }

            var result = commandDispatcher.Send(showCommand).Result;
            return (Cancelled: result.Cancelled, Principal: result.Principal, Discount: result.Discount);
        }

        public bool HasPendingPayments()
        {
            return TaxAccount.GetPendingPayments().HasAny();
        }

        public bool HasPendingPaymentsOnBill(int billNumber)
        {
            return TaxAccount.GetPendingPayments(billNumber).HasAny();
        }

        public int GetDefaultPaymentYear()
        {
            var defaultYear = 0;
            if (OldestBillYearWithABalance > 0)
            {
                return OldestBillYearWithABalance;
            }

            var oldestTaxClubYearBill = GetOldestTaxClubYearBillAfter(LatestBilledYear);
            if (oldestTaxClubYearBill > 0)
            {
                defaultYear = oldestTaxClubYearBill;
            }

            return defaultYear;
        }

        public int GetDefaultTaxClubYear()
        {
            var oldestTaxClubYearBill = GetOldestTaxClubYearBillAfter(LatestBilledYear);
            if (oldestTaxClubYearBill > 0)
            {
                return oldestTaxClubYearBill;
            }

            return 0;
        }

        public IEnumerable<int> GetBillIdsWithPending()
        {
            return TaxAccount.Bills.Where(b => b.HasNewPayments()).Select(b => b.ID).ToList();
        }

        public void ApplyCredits(bool addOverPayToLastBill)
        {
            var creditInfo = GetCreditsOnOlderYears();
            decimal creditLeft = creditInfo.OlderCredits;
            var paymentSetup = new PaymentSetupInfo()
            {
                Principal = creditLeft,
                Interest = 0,
                Costs = 0,
                YearBill = 0,
                AffectCash = "Y",
                Account = "",
                PayCode = PropertyTaxPaymentCode.RegularPayment,
                Reference = "AUTO",
                AffectCashDrawer = "N",
                Period = 0,
                PrelienInterest = 0,
                Comment = "",
                ForcePaymentDistributionAsIs = false,
                TransactionIdentifier = Guid.NewGuid(),
                TransactionDate = RecordedTransactionDate
            };
            var bills = TaxAccount.Bills.Where(b => b.HasBeenBilled() &&
                ((!b.HasLien() && b.BillSummary.GetNetOwed() < 0) ||
                (b.HasLien() && b.Lien.LienSummary.GetNetOwed() < 0)));
            foreach (var bill in bills)
            {
                TaxBillCalculationSummary summary = null;
                if (CalculationSummaries.ContainsKey(bill.ID))
                {
                    summary = CalculationSummaries.FirstOrDefault(s => s.Key == bill.ID).Value;
                }

                if (summary != null)
                {
                    decimal amount = 0;
                    decimal credit = summary.Balance * -1;
                    if (credit >= creditLeft)
                    {
                        amount = -creditLeft;
                        creditLeft = 0;
                    }
                    else
                    {
                        amount = summary.Balance;
                        creditLeft -= amount;
                    }
                    var creditPayment = new PaymentSetupInfo()
                    {
                        Principal = amount,
                        Interest = 0,
                        Costs = 0,
                        YearBill = bill.YearBill(),
                        AffectCash = "Y",
                        Account = "",
                        Reference = "AUTO",
                        AffectCashDrawer = "N",
                        Period = 0,
                        PayCode = PropertyTaxPaymentCode.RegularPayment,
                        PrelienInterest = 0,
                        Comment = "",
                        ForcePaymentDistributionAsIs = false,
                        TransactionIdentifier = Guid.NewGuid(),
                        TransactionDate = RecordedTransactionDate
                    };
                   // AddPendingPayment(creditPayment);
                    CreatePendingPayment(bill, creditPayment, false);
                }
                
            }
        //CalculateBills();
            AutoCreatePayments(paymentSetup,addOverPayToLastBill);
            CalculateBills();

        }

        private PropertyTaxBill GetOldestBillWithBalance()
        {
          return  TaxAccount.Bills
                .Where(b => (!b.HasLien() && b.BillSummary.GetNetOwed() > 0) ||
                            (b.HasLien() && b.Lien.LienSummary.GetNetOwed() > 0)).OrderBy(b => b.TaxYear)
                .ThenBy(b => b.BillNumber).FirstOrDefault();
        }

        public (bool HasCreditsOnOlderYears, bool PrePayExists,decimal OlderCredits ,decimal TotalCredits, decimal TotalPositive) GetCreditsOnOlderYears()
        {
            var creditBills = TaxAccount.Bills.Where(b => b.RateId > 0 && b.TaxYear < LatestBilledYear &&
                                             ((!b.HasLien() && b.BillSummary.GetNetOwed() < 0) ||
                                              (b.HasLien() && b.Lien.LienSummary.GetNetOwed() < 0))).ToList();
            if (!creditBills.Any())
            {
                return (HasCreditsOnOlderYears: false, PrePayExists: false, OlderCredits: 0, TotalCredits: 0, TotalPositive: 0);
            }

            var olderCredits = 0M;
            var totalCredits = 0M;
            var totalPositive = 0M;
            var prepayExists = TaxAccount.Bills.Any(b => b.RateId == 0);
            var allBills = TaxAccount.Bills.OrderBy(b => b.TaxYear).ThenBy(b => b.BillNumber).ToList();
            foreach (var bill in allBills)
            {
                              
                if (bill.HasBeenBilled())
                {
                    var summary = CalculationSummaries.FirstOrDefault(s => s.Key == bill.ID).Value;
                    if (summary != null)
                    {
                        if (summary.Balance < 0)
                        {
                            totalCredits += summary.Balance * -1;
                            if (bill.TaxYear < LatestBilledYear)
                            {
                                olderCredits += summary.Balance * -1;
                            }
                        }
                        else
                        {
                            totalPositive += summary.Balance;
                        }
                    }
                }
            }

            return (HasCreditsOnOlderYears: true, PrePayExists: prepayExists, OlderCredits: olderCredits, TotalCredits: totalCredits,
                TotalPositive: totalPositive);
        }

        public bool HasCreditsOnOlderYears()
        {
            return TaxAccount.Bills.Any(b => b.RateId > 0 && b.TaxYear < LatestBilledYear &&
                                                    ((!b.HasLien() && b.BillSummary.GetNetOwed() < 0) ||
                                                     (b.HasLien() && b.Lien.LienSummary.GetNetOwed() < 0)));

        }
        private int GetOldestTaxClubYearBillAfter(int yearBill)
        {
            var oldestTaxClub = TaxAccount.Bills.Where(b => b.HasTaxClub() && !b.TaxClub.NonActive.GetValueOrDefault() && b.TaxClub.Year > yearBill && b.TaxClub.TotalPayments >= b.TaxClub.TotalPaid)
                .OrderBy(b => b.TaxClub.Year.GetValueOrDefault()).FirstOrDefault();
            if (oldestTaxClub != null)
            {
                return oldestTaxClub.YearBill();
            }

            return 0;
        }
        private void ShowREAccountDetail(IEnumerable<int> expandedYearBills)
        {
            var accountSummary = new TaxBillCalculationSummary(Account, PropertyTaxBillType.Real);
            accountSummary.AddTo(AccountSummary);
            var showCommand = new PrintRealEstateTaxAccountDetail()
            {
                Account = Account,
                AccountSummary = accountSummary,
                BillType = BillType,
                EffectiveDate = EffectiveDate,
                RETaxAccount = RETaxAccount,
                ExpandedYearBills = expandedYearBills
            };
            foreach (var keyPair in CalculationSummaries)
            {
                showCommand.CalculationSummaries.Add(keyPair.Key, keyPair.Value);
            }

            commandDispatcher.Send(showCommand);
        }

        private void ShowPPAccountDetail(IEnumerable<int> expandedYearBills)
        {
            var accountSummary = new TaxBillCalculationSummary(Account, PropertyTaxBillType.Personal);
            accountSummary.AddTo(AccountSummary);
            var showCommand = new PrintPersonalPropertyTaxAccountDetail()
            {
                Account = Account,
                AccountSummary = accountSummary,
                BillType = BillType,
                EffectiveDate = EffectiveDate,
                PPTaxAccount = PPTaxAccount,
                ExpandedYearBills = expandedYearBills
            };
            foreach (var keyPair in CalculationSummaries)
            {
                showCommand.CalculationSummaries.Add(keyPair.Key, keyPair.Value);
            }

            commandDispatcher.Send(showCommand);
        }
        private void LoadTransactions()
        {
            var crTran = crTransactionRepository.Get(CorrelationIdentifier);
           
            if (crTran != null)
            {
                var transactions = crTran.PropertyTaxTransactionsForAccount(Account, BillType).ToList();
                if (transactions.Any())
                {
                    taxTransactions.AddRange(transactions);
                }
            }
        }
        private string FormatBillYear(int billYear)
        {
            var tempYear = billYear.ToString();
            return tempYear.Left(4) + "-" + tempYear.Substring(4);
        }

    }
}