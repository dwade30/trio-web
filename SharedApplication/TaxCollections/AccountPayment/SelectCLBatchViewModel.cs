﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class SelectCLBatchViewModel : ISelectCLBatchViewModel
    {
        public CLBatchChoice SelectedBatch { get; set; } = new CLBatchChoice();
        public bool Cancelled { get; set; } = true;
        public IEnumerable<CLBatchChoice> BatchChoices { get; private set; } = new List<CLBatchChoice>();
        public void AddBatches(IEnumerable<CLBatchChoice> batchChoices)
        {
            if (batchChoices.Any())
            {
                BatchChoices = batchChoices.ToList();
            }
        }

    }
}