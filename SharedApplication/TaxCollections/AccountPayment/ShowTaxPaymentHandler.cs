﻿using SharedApplication.CashReceipts;
using SharedApplication.Messaging;
using System.Linq;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxPaymentHandler : CommandHandler<ShowTaxPaymentInfo>
    {
        private IView<ITaxPaymentInfoViewModel> paymentView;
        private ICashReceiptsContext crContext;
        public ShowTaxPaymentHandler(IView<ITaxPaymentInfoViewModel> paymentView, ICashReceiptsContext crContext)
        {
            this.paymentView = paymentView;
            this.crContext = crContext;
        }
        protected override void Handle(ShowTaxPaymentInfo command)
        {
            if ((command.Payment?.ReceiptNumber ?? 0) > 0)
            {
                var receiptNumber = crContext.Receipts.Where(r => r.Id == command.Payment.ReceiptNumber).Select(r => r.ReceiptNumber)
                    .FirstOrDefault().GetValueOrDefault();
                if (receiptNumber == 0)
                {
                    receiptNumber = crContext.LastYearReceipts.Where(r => r.Id == command.Payment.ReceiptNumber)
                        .Select(r => r.ReceiptNumber).FirstOrDefault().GetValueOrDefault();
                }

                paymentView.ViewModel.ReceiptNumber = receiptNumber;
            }
            paymentView.ViewModel.Payment = command.Payment;
            paymentView.Show();
        }
    }
}