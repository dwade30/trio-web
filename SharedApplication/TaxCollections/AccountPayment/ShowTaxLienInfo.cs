﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Lien;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxLienInfo : Command
    {
        public PropertyTaxLienSummary LienSummary { get; }
        public int LienId { get; }
        public DateTime DateCreated { get; }
        public DateTime InterestAppliedThroughDate { get; }
        public string Book { get; }
        public string Page { get; }
        public int RateId { get; }
        public string Status { get; }
        public ShowTaxLienInfo(PropertyTaxLienSummary lienSummary, int lienId, DateTime dateCreated,
            DateTime interestAppliedThroughDate, string book, string page,int rateId,string status)
        {
            LienSummary = lienSummary;
            LienId = lienId;
            DateCreated = dateCreated;
            InterestAppliedThroughDate = interestAppliedThroughDate;
            Book = book;
            Page = page;
            RateId = rateId;
            Status = status;
        }
    }
}