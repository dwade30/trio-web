﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ReceiptTotals
    {
        public decimal Principal { get; set; }
        public decimal PreLienInterest { get; set; }
        public decimal CurrentInterest { get; set; }
        public decimal LienCost { get; set; }


    }
}