﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class NonBudgetaryPaymentViewModel : INonBudgetaryPaymentViewModel
    {
        public decimal Principal { get;  set; } = 0;
        public decimal Interest { get;  set; } = 0;
        public decimal PreLienInterest { get;  set; } = 0;
        public decimal Costs { get;  set; } = 0;
        public bool Cancelled { get;  set; } = true;
        public bool IsLien { get; set; } = false;
        //private CommandDispatcher commandDispatcher;
        public NonBudgetaryPaymentViewModel()
        {
            //this.commandDispatcher = commandDispatcher;
        }
        public void Cancel()
        {
            Cancelled = true;
            
        }

        public void SetPayment(decimal principal, decimal interest, decimal preLienInterest, decimal costs)
        {
            Cancelled = false;
            Principal = principal;
            Interest = interest;
            PreLienInterest = preLienInterest;
            Costs = costs;
        }
    }
}