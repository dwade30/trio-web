﻿using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class PropertyTaxBatchItemGroup
    {
        public BatchRecover BatchRecover { get; set; }
        public int BillId { get; set; } = 0;
        public TaxBillCalculationSummary Summary { get; set; }
        public PropertyTaxBillType BillType { get; set; }

        public PropertyTaxBatchItemGroup(int billId, PropertyTaxBillType billType, BatchRecover batchRecover, TaxBillCalculationSummary summary)
        {
            BatchRecover = batchRecover;
            BillId = billId;
            BillType = billType;
            Summary = summary;
        }
    }
}