﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class GetPropertyTaxDiscountHandler : CommandHandler<GetPropertyTaxDiscount, (bool Cancelled, decimal Principal, decimal Discount)>
    {
        private IModalView<IGetPropertyTaxDiscountViewModel> discountView;
        public GetPropertyTaxDiscountHandler(IModalView<IGetPropertyTaxDiscountViewModel> discountView)
        {
            this.discountView = discountView;
        }
        protected override (bool Cancelled, decimal Principal, decimal Discount) Handle(GetPropertyTaxDiscount command)
        {
            discountView.ViewModel.Principal = command.Principal;
            discountView.ViewModel.Discount = command.Discount;
            discountView.ViewModel.DiscountPercentage = command.DiscountPercentage;
            discountView.ViewModel.OriginalPrincipal = command.OriginalPrincipal;
            discountView.ViewModel.OriginalTotal = command.Principal + command.Discount;
            discountView.ShowModal();
            if (discountView.ViewModel.Cancelled)
            {
                return (Cancelled: true, Principal: 0, Discount: 0);
            }

            return (Cancelled: false, Principal: discountView.ViewModel.Principal,
                Discount: discountView.ViewModel.Discount);
        }
    }
}