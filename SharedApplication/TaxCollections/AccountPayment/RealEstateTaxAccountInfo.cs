﻿using System.Collections.Generic;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class RealEstateTaxAccountInfo
    {
        public int Account { get; set; } = 0;
        public string Name1 { get; set; } = "";
        public string Name2 { get; set; } = "";
        public MailingAddress MailingAddress { get; set; } = new MailingAddress();
        public string MapLot { get; set; } = "";
        public string BookPage { get; set; } = "";
        public decimal Acreage { get; set; } = 0;
        public bool IsTaxAcquired { get; set; } = false;
        public int AssociatedPPAccount { get; set; } = 0;
        public decimal OutstandingPPAmount { get; set; } = 0;
        public int AccountGroup { get; set; } = 0;
        public List<string> ExemptionCodes { get; set; } = new List<string>();

        public int LocationNumber { get; set; } = 0;
        public string LocationStreet { get; set; } = "";

        public string Location {
            get
            {
                if (LocationNumber > 0)
                {
                    return (LocationNumber.ToString() + " " + LocationStreet).Trim();
                }
                else
                {
                    return LocationStreet.Trim();
                }
            }
        }
    }
}