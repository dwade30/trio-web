﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowPersonalPropertyTaxAccountInfo : Command
    {
        public int Account { get; }

        public ShowPersonalPropertyTaxAccountInfo(int account)
        {
            Account = account;
        }
    }
}