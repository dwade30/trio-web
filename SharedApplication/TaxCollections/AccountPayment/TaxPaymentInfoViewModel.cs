﻿using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxPaymentInfoViewModel : ITaxPaymentInfoViewModel
    {
        public PaymentRec Payment { get; set; }
        public int ReceiptNumber { get; set; } = 0;
    }
}