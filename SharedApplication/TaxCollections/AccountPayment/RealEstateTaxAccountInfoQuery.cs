﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public class RealEstateTaxAccountInfoQuery
    {
        public int Account { get; }

        public RealEstateTaxAccountInfoQuery(int account)
        {
            Account = account;
        }
    }
}