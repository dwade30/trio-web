﻿using System;
using System.Collections.Generic;
using SharedApplication.AccountGroups;
using SharedApplication.CentralData.Models;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxAccountStatusViewModel
    {
        Guid CorrelationIdentifier { get; set; }
        int Account { get; set; }
        GroupMaster GroupInfo { get; }
        List<int> AccountList { get; set; }
        string CurrentOwnerName { get; }
        PropertyTaxBillType BillType { get; set; }
        RealEstateTaxAccount RETaxAccount { get; }
        PersonalPropertyTaxAccount PPTaxAccount { get; }
        List<DescriptionIDPair> TaxYears { get; }
        int LatestBilledYear { get; set; }
        void LoadAccount();
        string Comment { get; set; }
        string Note { get; set; }
        bool AutoShowNote { get; set; }
        ModuleAssociation ModuleAssociationInfo { get; set; }
        AccountGroup GetGroupSummary(DateTime effectiveDate);
        bool HasComment();
        bool HasNote();
        void EditComment();
        void EditNote();
        DateTime EffectiveDate { get; set; }
        DateTime RecordedTransactionDate { get; set; }
        bool AllowPayments { get; set; }
        bool OriginIsTaxCollections { get; set; }
        Dictionary<int, TaxBillCalculationSummary> CalculationSummaries { get; }
        TaxBillCalculationSummary AccountSummary { get; }
        void ChangeEffectiveDate();
        event EventHandler EffectiveDateChanged;

        int OldestBillYearWithABalance { get; set; }
        bool ValidateYearAndCode(int taxYear, PropertyTaxPaymentCode paymentCode);

        PropertyTaxBill GetBill(int yearBill);
        string GetCLAccount(PropertyTaxPaymentCode payCode, int yearBill, bool isLien, bool isTaxAcquired, PropertyTaxNonBudgetaryPaymentType type = PropertyTaxNonBudgetaryPaymentType.None);
        (bool success, string message, string caption) ReversePayment(PaymentRec payment);

       // IEnumerable<PaymentRec> PendingPayments { get; }

        (bool success, string message, string caption) ValidateAccount(string account);
        (bool success, string message, string caption) AddPendingPayment(PaymentSetupInfo paymentInfo);
        void Cancel();
        void CompleteTransaction(bool printReceipt);
        void ShowRateInfo(int yearBill);
        void ShowPaymentInfo(PaymentRec payment);
        void ShowLienInfo(int yearBill);
        bool AllowAutoPaymentFill { get; }
        void ShowAccountInfo();
        void ShowAccountDetail(IEnumerable<int> expandedYearBills);
        void ShowInterestedParties();
        void ShowMortgageHolder();
        void ShowTaxInformationSheet();
        void ShowBookPageReport();
        string SelectValidAccount(string account);

        void ShowLienDischargeNotice(int billId);
        void ShowAlternateDischargeNotice();
        void ReprintReceipt(PaymentRec payment);
        void RemoveAllPendingPayments();
        void SearchForAccount();
       // bool WriteOffsExistForBill(int yearBill);
        (bool Cancelled, PropertyTaxAmounts Amounts) GetNonBudgetaryPayment(int billId);
        (bool Cancelled, decimal Principal, decimal Discount) GetDiscount(int billId);
        bool HasPendingPayments();
        bool HasPendingPaymentsOnBill(int billNumber); 
        int GetDefaultPaymentYear();

        int GetDefaultTaxClubYear();
        //void ResetInterestDatesOnAllBills();
        //void ResetInterestDateOnBill(int billId);
        IEnumerable<int> GetBillIdsWithPending();
        void ApplyCredits(bool addOverPayToLastBill);
        (bool HasCreditsOnOlderYears, bool PrePayExists, decimal OlderCredits, decimal TotalCredits, decimal TotalPositive) GetCreditsOnOlderYears();
        bool HasCreditsOnOlderYears();
        void AddRECLComment();
        bool ArePreviousAccountsInList();
        bool AreFollowingAccountsInList();
        bool ShowPreviousAccount();
        bool ShowNextAccount();
    }
}