﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxBillInfoReportHandler : CommandHandler<ShowTaxBillInfoReport>
    {
        private IView<ITaxBillInfoReportModel> reportView;
        private DataEnvironmentSettings envSettings;
        public ShowTaxBillInfoReportHandler(IView<ITaxBillInfoReportModel> reportView, DataEnvironmentSettings envSettings)
        {
            this.reportView = reportView;
            this.envSettings = envSettings;
        }
        protected override void Handle(ShowTaxBillInfoReport command)
        {
            reportView.ViewModel.CalculationSummary = command.CalculationSummary;
            reportView.ViewModel.TaxBill = command.TaxBill;
            reportView.ViewModel.TotalPerDiem = command.TotalPerDiem;
            reportView.ViewModel.AccountTotals = command.AccountTotals;
            reportView.ViewModel.EffectiveDate = command.EffectiveDate;
            reportView.ViewModel.ClientName = envSettings.ClientName;
            reportView.Show();
        }
    }
}