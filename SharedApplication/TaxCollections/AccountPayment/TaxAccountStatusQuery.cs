﻿using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountStatusQuery
    {
        public int Account { get; set; } = 0;
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
    }
}