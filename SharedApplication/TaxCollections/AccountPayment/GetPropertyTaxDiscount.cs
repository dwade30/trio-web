﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class GetPropertyTaxDiscount : Command<(bool Cancelled,decimal Principal, decimal Discount)>
    {
        public decimal OriginalPrincipal { get; }
        public decimal DiscountPercentage { get; }
        public decimal Principal { get; }
        public decimal Discount { get; }

        public GetPropertyTaxDiscount(decimal originalPrincipal, decimal discountPercentage, decimal principal,
            decimal discount)
        {
            OriginalPrincipal = originalPrincipal;
            DiscountPercentage = discountPercentage;
            Principal = principal;
            Discount = discount;
        }
    }
}