﻿using System;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class PropertyTaxBatchItem
    {
        public Payments Payments { get; } = new Payments();
        public PropertyTaxBillType BillType { get; set; }
        public int Account { get; set; }
        public int BillId { get; set; }
        public int BatchItemId { get; set; }
        public Guid BatchItemIdentifier { get; set; } = Guid.NewGuid();
    }
}