﻿using System;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxCollectionsBatchViewModel
    {
        PropertyTaxBillType BillType { get; set; }
        CLBatchInputSetup BatchInputSetup { get; set; }
        TaxCollectionsBatch TaxBatch { get; set; }
        Guid CorrelationIdentifier { get; set; }
        Guid TransactionIdentifier { get; set; }
        void DeleteBatchItem(int id);        
        CalculatedTaxAccount LoadTaxAccount(int account, PropertyTaxBillType billType);
        CalculatedTaxAccount CurrentTaxAccount();
        void AddCYAEntry(string message);
        void AddPaymentToBatch (int account, decimal amount, PropertyTaxBillType billType);
        void AddBatchRecoverToBatch(BatchRecover batchRecover);
        void ImportFirstAmerican();
        void ImportBatch();
        void Cancel();
        void CompleteTransaction();
        int Search();
        void ShowBatchListing();
    }
}