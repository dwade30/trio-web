﻿using System;
using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxAccountStatus : Command
    {
        public int Account { get; set; } = 0;

        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
        public bool AllowPayments { get; set; } = false;
        public bool OriginIsTaxCollections { get; set; } = true;
        public Guid CorrelationIdentifier;
        public IEnumerable<int> AccountList { get; }
        public ShowTaxAccountStatus(int account, PropertyTaxBillType billType, bool allowPayments,bool originIsTaxCollections, Guid correlationIdentifier, IEnumerable<int> accountList)
        {
            Account = account;
            BillType = billType;
            AllowPayments = allowPayments;
            OriginIsTaxCollections = originIsTaxCollections;
            CorrelationIdentifier = correlationIdentifier;
            this.AccountList = accountList;
        }
    }
}