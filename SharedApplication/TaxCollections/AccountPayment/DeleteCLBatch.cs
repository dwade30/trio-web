﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class DeleteCLBatch : Command
    {
        public Guid BatchIdentifier { get; }

        public DeleteCLBatch(Guid batchIdentifier)
        {
            BatchIdentifier = batchIdentifier;
        }
    }
}