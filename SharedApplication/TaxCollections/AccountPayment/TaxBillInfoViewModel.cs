﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxBillInfoViewModel : ITaxBillInfoViewModel
    {
        public PropertyTaxBill TaxBill { get; set; }
        public TaxBillCalculationSummary CalculationSummary { get; set; }
        public decimal TotalPerDiem { get; set; }
        public object AccountTotals { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string LienDischargeBookPage { get; set; }

        private CommandDispatcher commandDispatcher;

        public TaxBillInfoViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }


        public void Print()
        {
            commandDispatcher.Send(new ShowTaxBillInfoReport(TaxBill,CalculationSummary,TotalPerDiem,AccountTotals,EffectiveDate));
        }
    }
}