﻿using System;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountSearchResult
    {
        public int Id { get; set; } = 0;
        public int Account { get; set; } = 0;
        public string Name { get; set; } = "";
        //public string Name2 { get; set; } = "";
        public string StreetName { get; set; } = "";
        public int StreetNumber { get; set; } = 0;
        public string MapLot { get; set; } = "";
        public bool IsPreviousOwner { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public PropertyTaxBillType AccountType { get; set; } = PropertyTaxBillType.Real;
    }
}