﻿using SharedApplication.Enums;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountSearch
    {
        public PropertyTaxBillType BillType { get; set; } = PropertyTaxBillType.Real;
        public TaxAccountSearchByOption SearchBy { get; set; } = TaxAccountSearchByOption.Name;
        public SearchCriteriaMatchType MatchType { get; set; } = SearchCriteriaMatchType.StartsWith;
        public string SearchFor { get; set; } = "";
        public bool IncludePreviousOwners { get; set; } = false;
        public bool IncludeDeletedAccounts { get; set; } = false;
    }
}