﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class StartTaxCollectionsBatch :Command
    {
        public CLBatchInputSetup BatchSetup { get; }
        public Guid BatchIdentifier { get; }
        public PropertyTaxBillType BillType { get; }
        public Guid CorrelationIdentifier { get; }
        public Guid TransactionIdentifier { get; }
        public StartTaxCollectionsBatch(CLBatchInputSetup batchSetup, Guid batchIdentifier, PropertyTaxBillType billType,Guid correlationIdentifier, Guid transactionIdentifier)
        {
            BatchSetup = batchSetup;
            BatchIdentifier = batchIdentifier;
            BillType = billType;
            CorrelationIdentifier = correlationIdentifier;
            TransactionIdentifier = transactionIdentifier;
        }
    }
}