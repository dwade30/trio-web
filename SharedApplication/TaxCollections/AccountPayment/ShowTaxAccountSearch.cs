﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxAccountSearch : Command
    {
        public PropertyTaxBillType BillType { get; }

        public ShowTaxAccountSearch(PropertyTaxBillType billType)
        {
            BillType = billType;
        }
    }
}