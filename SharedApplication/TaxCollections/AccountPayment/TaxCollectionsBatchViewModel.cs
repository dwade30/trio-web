﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Commands;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Models;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxCollectionsBatchViewModel : ITaxCollectionsBatchViewModel, IPropertyTaxBatchListingViewModel
    {
        public PropertyTaxBillType BillType { get; set; }
        public CLBatchInputSetup BatchInputSetup { get; set; }
        public TaxCollectionsBatch TaxBatch { get; set; } = new TaxCollectionsBatch();
        public Guid CorrelationIdentifier { get; set; }
        public Guid TransactionIdentifier { get; set; }
        private CommandDispatcher commandDispatcher;
        private CalculatedTaxAccount currentTaxAccount;
        private EventPublisher eventPublisher;
        public TaxCollectionsBatchViewModel(CommandDispatcher commandDispatcher, EventPublisher eventPublisher)
        {
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;
        }
        public void DeleteBatchItem(int id)
        {
            TaxBatch.BatchItems.RemoveAll(b => b.BatchRecover.Id == id);
            commandDispatcher.Send(new DeleteCLBatchItem(id));
        }

        public CalculatedTaxAccount LoadTaxAccount(int account, PropertyTaxBillType billType)
        {

            currentTaxAccount = commandDispatcher.Send(new GetCalculatedTaxAccount(account, billType, TaxBatch.EffectiveDate))
                .Result;
            return currentTaxAccount;
        }

        public CalculatedTaxAccount CurrentTaxAccount()
        {
            return currentTaxAccount;
        }

        public void AddCYAEntry(string message)
        {
            var entry = new AddCYAEntry();
            entry.Description1 = message;
            entry.LoggingProgram = TrioPrograms.CashReceipts;
            commandDispatcher.Send(entry);
        }

        public void AddPaymentToBatch(int account, decimal amount, PropertyTaxBillType billType)
        {
            if (currentTaxAccount == null || currentTaxAccount.TaxAccount.Account != account || currentTaxAccount.TaxAccount.BillType != billType)
            {
                LoadTaxAccount(account, billType);
            }
            if (currentTaxAccount == null)
            {
                return;
            }

            var batchItem = new BatchRecover()
            {
                AccountNumber = account,
                BatchIdentifier = TaxBatch.BatchIdentifier,
                BatchRecoverDate = TaxBatch.BatchRecoverDate,
                BillingType = billType == PropertyTaxBillType.Personal ? "PP" : "RE",
                Etdate =  TaxBatch.EffectiveDate,
                ImportId = 0,
                PaidBy =  TaxBatch.PaidBy,
                TellerId = TaxBatch.TellerID,
                Type = billType == PropertyTaxBillType.Personal ? "92" : "90",
                Processed = false,
                PrintReceipt = false,
                PaymentKey = 0,
                Ref = account + "-" + TaxBatch.DefaultTaxYear + "1",
                Name = currentTaxAccount.TaxAccount.Name1(),
                PaymentIdentifier = "",
                Year = TaxBatch.DefaultTaxYear,
                PayCorrect = false
            };
            var bill = currentTaxAccount.TaxAccount.Bills.FirstOrDefault(b => b.TaxYear == TaxBatch.DefaultTaxYear);
            if (bill == null)
            {
                return;
            }

            batchItem.Name = bill.Name1;
            var summary = currentTaxAccount.CalculationSummaries.FirstOrDefault(s => s.Key == bill.ID).Value;
            if (summary == null)
            {
                summary = new TaxBillCalculationSummary(account,billType);
            }
            decimal amountLeft = amount;
            var principal = summary.PrincipalDue();
            var interest = summary.InterestDue();
            var costs = summary.CostsDue();
            
            if (amountLeft >= summary.InterestDue())
            {
                amountLeft -= summary.InterestDue();
                if (amountLeft >= summary.CostsDue())
                {
                    amountLeft -= summary.CostsDue();
                    if (amountLeft >= summary.PrincipalDue())
                    {
                        amountLeft = amountLeft - summary.PrincipalDue();
                    }
                    else
                    {
                        // less than interest, costs and principal
                       principal = amountLeft;
                        amountLeft = 0;
                    }
                }
                else
                {
                    // less than interest and costs
                    principal = 0;
                    costs = amountLeft;
                    amountLeft = 0;
                }
            }
            else
            {
                interest = amountLeft;
                principal = 0;
                costs = 0;
                amountLeft = 0;
            }

            batchItem.Prin = principal.ToDouble() + amountLeft.ToDouble();
            batchItem.Int = interest.ToDouble();
            batchItem.Cost = costs.ToDouble();
            
            TaxBatch.BatchItems.Add(new PropertyTaxBatchItemGroup(bill.ID,bill.BillType,batchItem,summary));
        }

        public void AddBatchRecoverToBatch(BatchRecover batchRecover)
        {
            var account = batchRecover.AccountNumber.GetValueOrDefault();
            var billType = batchRecover.BillingType == "PP" ? PropertyTaxBillType.Personal : PropertyTaxBillType.Real;
            if (currentTaxAccount == null || currentTaxAccount.TaxAccount.Account != account || currentTaxAccount.TaxAccount.BillType != billType)
            {
                LoadTaxAccount(account, billType);
            }
            if (currentTaxAccount == null)
            {
                return;
            }
            var bill = currentTaxAccount.TaxAccount.Bills.FirstOrDefault(b => b.TaxYear == TaxBatch.DefaultTaxYear);
            if (bill == null)
            {
                return;
            }
            var summary = currentTaxAccount.CalculationSummaries.FirstOrDefault(s => s.Key == bill.ID).Value;
            if (summary == null)
            {
                summary = new TaxBillCalculationSummary(account, billType);
            }
            TaxBatch.BatchItems.Add(new PropertyTaxBatchItemGroup(bill.ID, bill.BillType, batchRecover, summary));
        }
        public void ImportFirstAmerican()
        {
            var result = commandDispatcher.Send(new ImportFirstAmericanTaxBatch()).Result;
            if (!result.Success)
            {
                return;
            }

            var items = result.Items.ToList();
            foreach (var item in items)
            {
                if (item.Year > 0 && TaxBatch.DefaultTaxYear == item.Year)
                {
                    TaxBatch.DefaultTaxYear = item.Year;
                }
                AddPaymentToBatch(item.Account,item.Amount,BillType);
            }
        }

        public void ImportBatch()
        {
            var result = commandDispatcher.Send(new ImportPropertyTaxBatch()).Result;
            if (!result.Success)
            {
                return;
            }
            var items = result.Items.ToList();
            foreach (var item in items)
            {
                if (item.Year > 0 && TaxBatch.DefaultTaxYear == item.Year)
                {
                    TaxBatch.DefaultTaxYear = item.Year;
                }
                AddPaymentToBatch(item.Account, item.Amount, BillType);
            }
        }

        public void Cancel()
        {
            eventPublisher.Publish(new TransactionCancelled(CorrelationIdentifier, null));
            if (BillType == PropertyTaxBillType.Personal)
            {
                commandDispatcher.Send(
                    new MakePersonalPropertyTransaction(CorrelationIdentifier, TransactionIdentifier, false,0));
            }
            else
            {
                commandDispatcher.Send(
                    new MakeRealEstateTransaction(CorrelationIdentifier, TransactionIdentifier, false,0));
            }

        }

        public void CompleteTransaction()
        {
            var batchItems = new List<PropertyTaxBatchItem>();
            IEnumerable<TransactionAmountItem> amountItems = new List<TransactionAmountItem>();
            foreach (var batchGroup in TaxBatch.BatchItems)
            {
                var batchItem = BatchGroupToBatchItem(batchGroup);
                amountItems = amountItems.Combine(batchItem.Payments.ToTransactionAmountItems());
                batchItems.Add(batchItem);
            }
            var transaction = new PropertyTaxBatchTransaction()
            {
                ActualDateTime = DateTime.Now,
                BatchIdentifier = TaxBatch.BatchIdentifier,
                BillType = TaxBatch.BillType,
                CorrelationIdentifier = CorrelationIdentifier,
                EffectiveDateTime = TaxBatch.EffectiveDate,
                IncludeInCashDrawerTotal = true,
                PayerName = TaxBatch.PaidBy,
                TaxYear = TaxBatch.DefaultTaxYear,
                TransactionTypeCode = TaxBatch.BillType == PropertyTaxBillType.Personal ? "PP" : "RE"                
            };
            transaction.AddBatchItemRange(batchItems);
            
            transaction.AddTransactionAmounts(amountItems);
            var transactions = new List<PropertyTaxBatchTransaction>(){transaction};
            eventPublisher.Publish(new TransactionCompleted()
                { CorrelationId = CorrelationIdentifier, CompletedTransactions = transactions });
        }

        public int Search()
        {
            return commandDispatcher.Send(new SearchForRealEstateTaxAccount()).Result;
        }

        public void ShowBatchListing()
        {
            commandDispatcher.Send(new ShowPropertyTaxBatchListing(this));
        }

        private PropertyTaxBatchItem BatchGroupToBatchItem(PropertyTaxBatchItemGroup itemGroup)
        {
            var batchItem = new PropertyTaxBatchItem()
            {
                Account = itemGroup.BatchRecover.AccountNumber.GetValueOrDefault(),
                BatchItemId = itemGroup.BatchRecover.Id,
                BillId = itemGroup.BillId,
                BillType = itemGroup.BillType
            };
            
            var summary = itemGroup.Summary;
            PaymentRec interestPaymentRec = null;
            if (summary.Interest > 0)
            {
                interestPaymentRec = new PaymentRec()
                {
                    Account = batchItem.Account,
                    ActualSystemDate = DateTime.Now,
                    BillCode = batchItem.BillType == PropertyTaxBillType.Personal ? "P" : "R",
                    BillId = batchItem.BillId,
                    BudgetaryAccountNumber = "",
                    CashDrawer = "Y",
                    ChargedInterestdate = TaxBatch.EffectiveDate,
                    ChargedInterestId = 0,
                    Code = PropertyTaxPaymentCode.Interest.ToCode(),
                    Reference = "CHGINT",
                    PaymentIdentifier = Guid.NewGuid(),
                    CorrelationIdentifier = CorrelationIdentifier,
                    //ChargedInterestIdentifier = null,
                    Comments = "",
                    CurrentInterest = -1 * summary.Interest,
                    ReceiptNumber = 0,
                    DosreceiptNumber = false,
                    DailyCloseOut = 0,
                    EffectiveInterestDate = TaxBatch.EffectiveDate,
                    ExternalPaymentIdentifier = "",
                    GeneralLedger = "Y",
                    IsReversal = false,
                    LienCost = 0,
                    PaidBy = "",
                    Period = "1",
                    PreLienInterest = 0,
                    Principal = 0,
                    RecordedTransactionDate = TaxBatch.BatchRecoverDate,
                    SetEidate = new DateTime(1899, 12, 30),
                    Teller = "",
                    Year = (itemGroup.BatchRecover.Year + "1").ToIntegerValue(),
                    TransactionIdentifier = batchItem.BatchItemIdentifier
                };
                batchItem.Payments.AddPayment(interestPaymentRec);
            }

            var paymentRec = MakePaymentRecForRegularBill(itemGroup,batchItem.BatchItemIdentifier);
            batchItem.Payments.AddPayment(paymentRec);
            return batchItem;
        }

        private PaymentRec MakePaymentRecForRegularBill(PropertyTaxBatchItemGroup batchItemGroup,Guid batchItemIdentifier)
        {
            var batchRecover = batchItemGroup.BatchRecover;
            var payment = new PaymentRec()
            {
                Account = batchItemGroup.BatchRecover.AccountNumber.GetValueOrDefault(),
                ActualSystemDate = DateTime.Now,
                BillCode = batchItemGroup.BillType == PropertyTaxBillType.Personal ? "P" : "R",
                BillId = batchItemGroup.BillId,
                BudgetaryAccountNumber = "",
                CashDrawer = "Y",
                ChargedInterestdate = TaxBatch.EffectiveDate,
                ChargedInterestId = 0,
                Code = PropertyTaxPaymentCode.RegularPayment.ToCode(),
                Reference = "",
                PaymentIdentifier = Guid.NewGuid(),
                CorrelationIdentifier = CorrelationIdentifier,
                //ChargedInterestIdentifier = null,
                Comments = "",
                CurrentInterest = batchRecover.Int.GetValueOrDefault().ToDecimal(),
                ReceiptNumber = 0,
                DosreceiptNumber = false,
                DailyCloseOut = 0,
                EffectiveInterestDate = TaxBatch.EffectiveDate,
                ExternalPaymentIdentifier = "",
                GeneralLedger = "Y",
                IsReversal = false,
                LienCost = batchRecover.Cost.GetValueOrDefault().ToDecimal(),
                PaidBy = "",
                Period = "A",
                PreLienInterest =  0,
                Principal = batchRecover.Prin.GetValueOrDefault().ToDecimal(),
                RecordedTransactionDate = TaxBatch.BatchRecoverDate,
                SetEidate = new DateTime(1899, 12, 30),
                Teller = "",
                Year = (batchItemGroup.BatchRecover.Year + "1").ToIntegerValue(),
                TransactionIdentifier = batchItemIdentifier
            };
            batchItemGroup.BatchRecover.PaymentIdentifier = payment.PaymentIdentifier.GetValueOrDefault().ToString();
            return payment;
        }
    }
}