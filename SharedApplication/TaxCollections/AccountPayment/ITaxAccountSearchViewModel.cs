﻿using System;
using System.Collections.Generic;
using SharedApplication.Enums;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface ITaxAccountSearchViewModel
    {
        PropertyTaxBillType BillType { get; set; }
        Guid CorrelationId { get; set; }
        Guid TransactionId { get; set; }
        bool AllowPayments { get; set; }
        bool OriginIsTaxCollections { get; set; }
        IEnumerable<GenericDescriptionPair<SearchCriteriaMatchType>> CriteriaMatchOptions { get; }
        IEnumerable<GenericDescriptionPair<TaxAccountSearchByOption>> SearchByOptions { get; }
        IEnumerable<TaxAccountSearchResult> SearchResults();
        void Cancel();
        void Search(SearchCriteriaMatchType matchType, TaxAccountSearchByOption searchBy, string searchFor, bool includePreviousOwners, bool includeDeletedAccounts);
        bool SelectAccount(int account, bool includeSearchResults);
        int GetLastAccountAccessed();

        void StartBatch();
        void PurgeBatch();
        void RecoverBatch();
        event EventHandler Closing ;
    }
}