﻿using System;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxAccountDetailLine
    {
        public int YearBill { get; set; } = 0;
        public int Id { get; set; } = 0;
        public string Reference { get; set; } = "";
        public string Period { get; set; } = "";
        public string PayCode { get; set; } = "";
        public decimal Principal { get; set; } = 0M;
        public decimal Interest { get; set; } = 0M;
        public decimal Costs  {get;set;} =0M;
        public decimal Total { get; set; } = 0M;
        public TaxAccountDetailLineType LineType { get; set; } = TaxAccountDetailLineType.None;
        public DateTime EffectiveDate { get; set; } = DateTime.MinValue;
        public string BillCategory { get; set; } = "R";
        public int RateId { get; set; } = 0;
        public string Name { get; set; } = "";
        public int Receipt { get; set; } = 0;
        public string Comment { get; set; } = "";
    }

    public enum TaxAccountDetailLineType
    {
        Header = 0,
        Payment = 1,
        NewInterest = 2,
        LienHeader = 3,
        Subtotal = 4,
        None = 5,
        SummaryHeader = 6,
        Name = 7
    }
}