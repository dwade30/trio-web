﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public interface INonBudgetaryPaymentViewModel
    {
        decimal Principal { get; set; }
        decimal Interest { get; set; }
        decimal PreLienInterest { get; set; }
        decimal Costs { get; set; }
        bool Cancelled { get; set; }
        bool IsLien { get; set; }
        void Cancel();
        void SetPayment(decimal principal, decimal interest, decimal preLienInterest, decimal costs);
    }
}