﻿using System;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxBillInfoReport : Command
    {
        public PropertyTaxBill TaxBill { get; set; }
        public TaxBillCalculationSummary CalculationSummary { get; set; }
        public decimal TotalPerDiem { get; set; }
        public object AccountTotals { get; }
        public DateTime EffectiveDate { get; }
        public ShowTaxBillInfoReport(PropertyTaxBill taxBill, TaxBillCalculationSummary calculationSummary,
            decimal totalPerDiem, object accountTotals, DateTime effectiveDate)
        {
            TaxBill = taxBill;
            CalculationSummary = calculationSummary;
            TotalPerDiem = totalPerDiem;
            AccountTotals = accountTotals;
            EffectiveDate = effectiveDate;
        }
    }
}