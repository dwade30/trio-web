﻿using System;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class TaxBillInfoReportModel : ITaxBillInfoReportModel
    {
        public PropertyTaxBill TaxBill { get; set; }
        public TaxBillCalculationSummary CalculationSummary { get; set; }
        public decimal TotalPerDiem { get; set; }
        public object AccountTotals { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string ClientName { get; set; } = "";
    }
}