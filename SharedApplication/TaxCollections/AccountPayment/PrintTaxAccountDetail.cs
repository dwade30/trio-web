﻿using System;
using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public abstract class PrintTaxAccountDetail : Command
    {
        public int Account { get; set; }
        public Dictionary<int, TaxBillCalculationSummary> CalculationSummaries { get; } = new Dictionary<int, TaxBillCalculationSummary>();
        public TaxBillCalculationSummary AccountSummary { get; set; }
        public DateTime EffectiveDate { get; set; }
        public PropertyTaxBillType BillType { get; set; }
        //public RealEstateTaxAccount RETaxAccount { get; set; }
        //public PersonalPropertyTaxAccount PPTaxAccount { get; set; }

        public IEnumerable<int> ExpandedYearBills { get; set; } = new List<int>();
        //public ITaxAccountDetailReportModel ReportModel { get; set; }
    }
}