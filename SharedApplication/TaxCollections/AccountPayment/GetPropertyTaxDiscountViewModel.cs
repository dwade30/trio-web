﻿using SharedApplication.Extensions;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class GetPropertyTaxDiscountViewModel : IGetPropertyTaxDiscountViewModel
    {
        public decimal OriginalPrincipal { get; set; }
        public decimal OriginalTotal { get; set; }
        public decimal Principal { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal Discount { get; set; }
        public bool Cancelled { get; set; } = true;

        public GetPropertyTaxDiscountViewModel()
        {

        }
        public void Cancel()
        {
            Cancelled = true;
        }

        public void SetPayment(decimal principal, decimal discount)
        {
            Cancelled = false;
            Principal = principal;
            Discount = discount;
        }

        public void Recalculate()
        {
            Discount = (OriginalPrincipal * DiscountPercentage).RoundToMoney();
            Principal = OriginalTotal - Discount;
        }

        public void SetPrincipal(decimal principal)
        {
            Principal = principal;
            Discount = CalculateDiscountFromPrincipal(principal);
        }

        private decimal CalculateDiscountFromPrincipal(decimal principal)
        {
            if (principal == 0)
            {
                return (OriginalPrincipal * DiscountPercentage).RoundToMoney();
            }
            return ((DiscountPercentage * principal) / (1 - DiscountPercentage)).RoundToMoney();
        }
    }
}