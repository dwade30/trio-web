﻿namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowRealEstateTaxAccountInfoViewModel : IShowRealEstateTaxAccountInfoViewModel
    {
        public int Account { get; set; }
        public RealEstateTaxAccountInfo AccountInfo { get; set; }
    }
}