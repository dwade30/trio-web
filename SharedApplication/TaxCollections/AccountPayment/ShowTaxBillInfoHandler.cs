﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxBillInfoHandler : CommandHandler<ShowTaxBillInfo>
    {
        private IView<ITaxBillInfoViewModel> infoView;
        public ShowTaxBillInfoHandler(IView<ITaxBillInfoViewModel> infoView)
        {
            this.infoView = infoView;
        }
        protected override void Handle(ShowTaxBillInfo command)
        {
            infoView.ViewModel.TaxBill = command.TaxBill;
            infoView.ViewModel.CalculationSummary = command.CalculationSummary;
            infoView.ViewModel.TotalPerDiem = command.TotalPerDiem;
            infoView.ViewModel.AccountTotals = command.AccountTotals;
            infoView.ViewModel.EffectiveDate = command.EffectiveDate;
            infoView.ViewModel.LienDischargeBookPage = command.LienDischargeBookPage;
            infoView.Show();
        }
    }
}