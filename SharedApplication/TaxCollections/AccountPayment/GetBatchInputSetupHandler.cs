﻿using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class GetCLBatchInputSetupHandler : CommandHandler<GetCLBatchInputSetup,(bool Success,CLBatchInputSetup BatchSetup)>
    {
        private IModalView<ICLBatchInputSetupViewModel> setupView;
        public GetCLBatchInputSetupHandler(IModalView<ICLBatchInputSetupViewModel> setupView)
        {
            this.setupView = setupView;
        }

        protected override (bool Success, CLBatchInputSetup BatchSetup) Handle(GetCLBatchInputSetup command)
        {
            setupView.ShowModal();
            if (!setupView.ViewModel.Cancelled)
            {
                return (Success: true, setupView.ViewModel.BatchSetup);
            }

            return (Success: false, null);
        }
    }
}