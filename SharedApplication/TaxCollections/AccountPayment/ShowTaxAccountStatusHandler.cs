﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowTaxAccountStatusHandler : CommandHandler<ShowTaxAccountStatus>
    {
        private IView<ITaxAccountStatusViewModel> statusView;
        public ShowTaxAccountStatusHandler(IView<ITaxAccountStatusViewModel> statusView)
        {
            this.statusView = statusView;
        }
        protected override void Handle(ShowTaxAccountStatus command)
        {
            statusView.ViewModel.Account = command.Account;
            statusView.ViewModel.BillType = command.BillType;
            statusView.ViewModel.AllowPayments = command.AllowPayments;
            statusView.ViewModel.CorrelationIdentifier = command.CorrelationIdentifier;
            statusView.ViewModel.OriginIsTaxCollections = command.OriginIsTaxCollections;
            statusView.ViewModel.AccountList = command.AccountList.ToList();
            statusView.ViewModel.LoadAccount();
            statusView.Show();
        }
    }
}