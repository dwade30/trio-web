﻿using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Enums;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class ShowRealEstateTaxAccountInfo : Command
    {
        public int Account { get; }

        public ShowRealEstateTaxAccountInfo(int account)
        {
            Account = account;
        }
    }
}