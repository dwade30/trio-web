﻿using System;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class CLBatchInputSetup
    {
        public DateTime BatchRecoverDate { get; set; }
        public int DefaultTaxYear { get; set; } = 0;
        public string TellerID { get; set; } = "";
        public DateTime EffectiveDate { get; set; }
        public string PaidBy { get; set; }
        public int Period { get; set; }

    }
}