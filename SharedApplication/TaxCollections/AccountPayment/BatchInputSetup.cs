﻿using System;

namespace SharedApplication.TaxCollections.AccountPayment
{
    public class BatchInputSetup
    {
        public DateTime BatchRecoverDate { get; set; }
        public int DefaultTaxYear { get; set; } = 0;
        public string TellerID { get; set; } = "";
        public DateTime EffectiveDate { get; set; }
        public string PaidBy { get; set; }
        public string Period { get; set; }

    }
}