﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SharedApplication
{
    public interface IWritableSet<TEntity> : IQueryable<TEntity> where TEntity: class
    {
        void AddEntity(TEntity entity);
    }
}