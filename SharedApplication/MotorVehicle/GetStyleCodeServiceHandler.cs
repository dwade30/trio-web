﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Interfaces;

namespace SharedApplication.MotorVehicle
{
    public class GetStyleCodeServiceHandler : CommandHandler<GetStyleCodeService, IStyleCodeService>
    {
        private IStyleCodeService styleCodeService;

        public GetStyleCodeServiceHandler(IStyleCodeService styleCodeService)
        {
            this.styleCodeService = styleCodeService;
        }

        protected override IStyleCodeService Handle(GetStyleCodeService command)
        {
            return styleCodeService;
        }
    }
}
