﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.MotorVehicle.Receipting
{
    public class MakeMotorVehicleTransaction : SharedApplication.Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }

    }
}