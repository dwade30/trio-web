﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.MotorVehicle.Receipting
{
	public class FinalizeMotorVehicleTransactionHandler : CommandHandler<FinalizeTransactions<MotorVehicleTransactionBase>, bool>
	{
		protected override bool Handle(FinalizeTransactions<MotorVehicleTransactionBase> command)
		{
			return true;
		}
	}
}
