﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;

namespace SharedApplication.MotorVehicle.Receipting
{
	public class MotorVehicleTransactionBase : TransactionBase
	{
		public string TransactionTypeDescription { get; set; }
		public string RegistrationType { get; set; }
		public string ResidenceCode { get; set; }
		public int RecordKey { get; set; }
	}
}
