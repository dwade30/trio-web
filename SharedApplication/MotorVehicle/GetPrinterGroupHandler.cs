﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;

namespace SharedApplication.MotorVehicle
{
    public class GetPrinterGroupHandler : CommandHandler<GetPrinterGroup, int>
    {
        private ISettingService settingService;

        public GetPrinterGroupHandler(ISettingService settingService)
        {
            this.settingService = settingService;
        }

        protected override int Handle(GetPrinterGroup command)
        {
            int currentGroup = 0;

            var result = settingService.GetSettingValue(SettingOwnerType.Machine, "MVR3PrinterGroup");

            if (result != null)
            {
                currentGroup = result.SettingValue.ToIntegerValue();
            }

            if (currentGroup == 0)
            {
                currentGroup = 1;
                settingService.SaveSetting(SettingOwnerType.Machine, "MVR3PrinterGroup", currentGroup.ToString());
            }

            return currentGroup;
        }
    }
}
