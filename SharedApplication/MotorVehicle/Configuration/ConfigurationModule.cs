﻿using Autofac;

namespace SharedApplication.MotorVehicle.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EditRegistrantViewModel>().As<IEditRegistrantNameViewModel>();
            
        }

        

    }
}