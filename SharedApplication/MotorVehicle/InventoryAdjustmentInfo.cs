﻿using SharedApplication.MotorVehicle.Enums;

namespace SharedApplication.MotorVehicle
{
	public class InventoryAdjustmentInfo 
	{
		public InventoryAdjustmentType AdjustmentType { get; set; }
		public string Code { get; set; }
		public string Prefix { get; set; }
		public string Suffix { get; set; }
		public int Low { get; set; }
		public int High { get; set; }
	}
}
