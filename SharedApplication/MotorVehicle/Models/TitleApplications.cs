﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.MotorVehicle.Models
{
    public partial class TitleApplication
    {
        public int ID { get; set; }
        public string CTANumber { get; set; }
        public decimal? Fee { get; set; }
        public string Class { get; set; }
        public string Plate { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string CustomerName { get; set; }
        public string OPID { get; set; }
        public int? DoubleIfApplicable { get; set; }
        public int? PeriodCloseoutID { get; set; }
        public int? TellerCloseoutID { get; set; }
        public string MSRP { get; set; }
    }
}