﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.MotorVehicle.Models
{
    public partial class Residence
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Town { get; set; }
        public string State { get; set; }
    }
}