﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Models
{
	public class Inventory
	{
		public int Id { get; set; }

		public string Code { get; set; }

		public string Prefix { get; set; }

		public int? Number { get; set; }

		public string Suffix { get; set; }

		public DateTime? InventoryDate { get; set; }

		public string OPID { get; set; }

		public int? MVR3 { get; set; }

		public string Status { get; set; }

		public string GroupID { get; set; }

		public string AssignedBy { get; set; }

		public DateTime? AssignedDate { get; set; }

		public string Comment { get; set; }

		public short? Group { get; set; }

		public int? PeriodCloseoutId { get; set; }

		public int? TellerCloseoutId { get; set; }

		public string FormattedInventory { get; set; }

		public bool? Pending { get; set; }
	}
}
