﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Models
{
    public class TownSummaryHeading
    {
        public int Id { get; set; }

        public string Heading { get; set; }

        public int? CategoryCode { get; set; }
    }
}
