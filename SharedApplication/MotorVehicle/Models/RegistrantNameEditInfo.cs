﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.MotorVehicle.Enums;

namespace SharedApplication.MotorVehicle.Models
{
    public class RegistrantNameEditInfo
    {
        public RegistrantType Type { get; set; } = RegistrantType.Individual;
        public string CompanyName { get; set; } = "";
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string MiddleInitial { get; set; } = "";
        public string Designation { get; set; } = "";
    }
}
