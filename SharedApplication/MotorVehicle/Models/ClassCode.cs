﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Models
{
    public class ClassCode
    {
        public int Id { get; set; }

        public string SystemCode { get; set; }

        public string BMVCode { get; set; }

        public string Description { get; set; }

        public string ExciseRequired { get; set; }

        public decimal? RegistrationFee { get; set; }

        public decimal? OtherFeesNew { get; set; }

        public decimal? OtherFeesRenew { get; set; }

        public string HalfRateAllowed { get; set; }

        public string TwoYear { get; set; }

        public string LocationNew { get; set; }

        public string LocationTransfer { get; set; }

        public string LocationRenewal { get; set; }

        public int? NumberOfPlateStickers { get; set; }

        public string RenewalDate { get; set; }

        public string TitleRequired { get; set; }

        public string Emission { get; set; }

        public string InsuranceRequired { get; set; }

        public int? ShortDescription { get; set; }

        public short? FixedMonth { get; set; }

        public bool? IssueNewPlate { get; set; }
    }
}
