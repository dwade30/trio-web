﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.MotorVehicle.Models
{
    public partial class SpecialResCode
    {
        public int ID { get; set; }
        public string ResCode { get; set; }
    }
}