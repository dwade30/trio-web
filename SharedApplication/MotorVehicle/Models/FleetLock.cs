﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.MotorVehicle.Models
{
    public partial class FleetLock
    {
        public int ID { get; set; }
        public bool? MasterLock { get; set; }
        public string OpID { get; set; }
    }
}