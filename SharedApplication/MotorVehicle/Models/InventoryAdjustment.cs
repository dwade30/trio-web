﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Models
{
	public class InventoryAdjustment
	{
		public int Id { get; set; }

		public string AdjustmentCode { get; set; }

		public DateTime? DateOfAdjustment { get; set; }

		public int? QuantityAdjusted { get; set; }

		public string InventoryType { get; set; }

		public string Low { get; set; }

		public string High { get; set; }

		public string OPID { get; set; }

		public string Reason { get; set; }

		public int? PeriodCloseoutId { get; set; }

		public int? TellerCloseoutId { get; set; }
	}
}
