﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;
using SharedApplication.MotorVehicle.Models;

namespace SharedApplication.MotorVehicle
{
	public interface IMotorVehicleContext
	{
		IQueryable<Inventory> Inventories { get; }
		IQueryable<InventoryAdjustment> InventoryAdjustments { get; }
        IQueryable<ClassCode> ClassCodes { get; }
        IQueryable<StyleCode> StyleCodes { get; }
        IQueryable<TownSummaryHeading> TownSummaryHeadings { get; }

		IQueryable<ActivityMaster> ActivityMasters { get; }
		IQueryable<ArchiveMaster> ArchiveMasters { get; }
		IQueryable<AuditChange> AuditChanges { get; }
		IQueryable<AuditChangesArchive> AuditChangesArchives { get; }
        IQueryable<AuditReport> AuditReports { get; }
        IQueryable<Booster> Boosters { get; }
        IQueryable<CYA> CYAs { get; }
        IQueryable<CloseoutInventory> CloseoutInventories { get; }
        IQueryable <Color> Colors { get; }
        IQueryable<MVCounty> Counties { get; }
        IQueryable<DBVersion> DBVersions { get;  }
        IQueryable<Defaultinfo> Defaultinfos { get; }
        IQueryable<DoubleCTA> DoubleCTAs { get;  }

        IQueryable<ExceptionReport> ExceptionReports { get ;   }
        IQueryable<ExciseTax> ExciseTaxes { get ; }
        IQueryable<FleetLock> FleetLocks { get ; }
        IQueryable<FleetMaster> FleetMasters { get ; }
        IQueryable<GiftCertificateRegister> GiftCertificateRegisters { get ; }
        IQueryable<GrossVehicleWeight> GrossVehicleWeights { get ; }
        IQueryable<GroupAddressOverride> GroupAddressOverrides { get ; }
        IQueryable<HeldRegistrationMaster> HeldRegistrationMasters { get ; }
        IQueryable<HeldTitleUseTax> HeldTitleUseTaxes { get ; }
        IQueryable<InventoryLock> InventoryLocks { get ; }
        IQueryable<InventoryMaster> InventoryMasters { get ; }
        IQueryable<InventoryOnHandAtPeriodCloseout> InventoryOnHandAtPeriodCloseouts { get ; }
        IQueryable<LastMVRNumber> LastMVRNumbers { get ; }
        IQueryable<LongTermTitleApplication> LongTermTitleApplications { get ; }
        IQueryable<LongTermTrailerRegistration> LongTermTrailerRegistrations { get ; }
        IQueryable<Make> Makes { get ; }
        IQueryable<MVMaster> Masters { get; }

        IQueryable<Operator> Operators { get;  }
        IQueryable<PendingActivityMaster> PendingActivityMasters { get;  }
        IQueryable<PendingExceptionReport> PendingExceptionReports { get;  }
        IQueryable<PendingTitleApplication> PendingTitleApplications { get;  }
        IQueryable<PeriodCloseout> PeriodCloseouts { get;  }
        IQueryable<PeriodCloseoutLongTerm> PeriodCloseoutLongTerms { get;  }
        IQueryable<PrintPriority> PrintPriorities { get;  }
        IQueryable<ReasonCode> ReasonCodes { get;  }
        IQueryable<Residence> Residences { get;  }
        IQueryable<SpecialRegistration> SpecialRegistrations { get;  }
        IQueryable<SpecialResCode> SpecialResCodes { get;  }
        IQueryable<SpecialtyPlate> SpecialtyPlates { get;  }
        IQueryable<MVState> States { get;  }
        IQueryable<TellerCloseoutTable> TellerCloseoutTables { get;  }
        IQueryable<TemporaryPlateRegister> TemporaryPlateRegisters { get;  }
        IQueryable<Title> Titles { get;  }
        IQueryable<TitleApplication> TitleApplications { get;  }
        IQueryable<TownDetailSummary> TownDetailSummaries { get;  }
        IQueryable<UseTax> UseTaxes { get;  }
        IQueryable<WMV> WMVs { get;  }
        IQueryable<MVYear> Years { get;  }
        IQueryable<tblMasterTemp> tblMasterTemps { get;  }

    }
}
