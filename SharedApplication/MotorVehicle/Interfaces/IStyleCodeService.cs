﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.MotorVehicle.Models;

namespace SharedApplication.MotorVehicle.Interfaces
{
    public interface IStyleCodeService
    {
        bool InitializeStyleCodes();
        bool ValidateStyleCode(string vehicleClass, string vehicleSubClass, string plate, string styleCode);
        IEnumerable<StyleCode> GetValidStyleCodeList(string vehicleClass, string vehicleSubClass, string plate);
    }
}
