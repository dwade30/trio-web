﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.MotorVehicle.Enums;

namespace SharedApplication.MotorVehicle.Interfaces
{
    public interface IInventoryService
    {
        InventoryAdjustmentValidationResult ValidateInventoryAdjustmentInfo(InventoryAdjustmentInfo info);
        bool AdjustInventory(AdjustInventoryConfigurationInfo config);
        void CancelAdjustment();
        event EventHandler CancelUnavailable;
    }
}
