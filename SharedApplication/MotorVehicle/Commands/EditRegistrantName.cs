﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Models;

namespace SharedApplication.MotorVehicle.Commands
{
    public class EditRegistrantName : Command<RegistrantNameEditInfo>
    {
        public RegistrantNameEditInfo CurrentInfo { get; set; }
    }
}
