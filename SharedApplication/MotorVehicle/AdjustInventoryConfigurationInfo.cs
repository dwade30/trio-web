﻿namespace SharedApplication.MotorVehicle
{
	public class AdjustInventoryConfigurationInfo
	{
		public InventoryAdjustmentInfo AdjustmentDetails { get; set; }
		public string Reason { get; set; }
		public string OpId { get; set; }
		public short Group { get; set; }
	}
}
