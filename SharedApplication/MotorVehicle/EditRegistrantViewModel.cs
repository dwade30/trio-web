﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.MotorVehicle.Models;

namespace SharedApplication.MotorVehicle
{
    public class EditRegistrantViewModel : IEditRegistrantNameViewModel
    {
        public RegistrantNameEditInfo RegistrantInfo { get; set; }
    }
}
