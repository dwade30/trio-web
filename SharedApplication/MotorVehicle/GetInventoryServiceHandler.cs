﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Commands;
using SharedApplication.MotorVehicle.Interfaces;

namespace SharedApplication.MotorVehicle
{
    public class GetInventoryServiceHandler : CommandHandler<GetInventoryService, IInventoryService>
    {
        private IInventoryService inventoryService;

        public GetInventoryServiceHandler(IInventoryService inventoryService)
        {
            this.inventoryService = inventoryService;
        }

        protected override IInventoryService Handle(GetInventoryService command)
        {
            return inventoryService;
        }
    }
}
