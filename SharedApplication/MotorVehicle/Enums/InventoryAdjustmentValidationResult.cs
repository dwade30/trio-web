﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Enums
{
	public enum InventoryAdjustmentValidationResult
	{
		InventoryExistsAvailable,
		InventoryExistsVoidedIssued,
		InventoryNotFound,
		InvalidRequest,
		VerifyRange,
		NoIssues
	}
}
