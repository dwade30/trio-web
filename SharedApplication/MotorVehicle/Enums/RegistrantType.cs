﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Enums
{
    public enum RegistrantType
    {
        Individual,
        Company
    }
}
