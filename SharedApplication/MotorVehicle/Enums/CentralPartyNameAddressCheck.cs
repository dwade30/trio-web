﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Enums
{
    public enum CentralPartyNameAddressCheck
    {
        UseCentralPartyInfo,
        DoNotUseCentralPartyInfo,
        AskToUseCentralPartyInfoIfDifferent
    }
}
