﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.MotorVehicle.Enums
{
    public enum VehicleStyleType
    {
        Unknown,
        Commercial,
        NonCommercial,
        All
    }
}
