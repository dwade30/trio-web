﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.Commands
{
	public class SetArchiveMode : Command
	{
		public bool ArchiveMode { get; set; }
		public string ArchiveTag { get; set; }
	}
}
