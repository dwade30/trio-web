﻿using System.Collections.Generic;
using SharedApplication.Messaging;

namespace SharedApplication.Commands
{
    public class GetOpenFormsForModule : Command<IEnumerable<string>>
    {
        public string Module { get; }

        public GetOpenFormsForModule(string module)
        {
            this.Module = module.ToUpper();
        }
    }
}