﻿using SharedApplication.Messaging;
using SharedApplication.Models;

namespace SharedApplication.CodeEnforcement
{
    public class UpdateCodeEnforcementDatabase : Command<bool>
    {
        public SQLConfigInfo SqlConfigInfo { get; }
        public string DataEnvironment { get; }

        public UpdateCodeEnforcementDatabase(SQLConfigInfo sqlConfigInfo, string dataEnvironment)
        {
            DataEnvironment = dataEnvironment;
            SqlConfigInfo = sqlConfigInfo;
        }

    }
}