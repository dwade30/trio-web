﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
	public class CryptoUtility
	{
		public string ComputeSHA512Hash(string strStringToHash, string strSalt)
		{
			return ComputeSHA512Hash(strStringToHash + strSalt);
		}

		public string ComputeSHA512Hash(string strStringToHash)
		{
			SHA512Managed tSHA = new SHA512Managed();
			byte[] tBytes = Encoding.ASCII.GetBytes(strStringToHash);
			byte[] hashBytes = ComputeSHA512HashBytes(tBytes);
			if (hashBytes != null)
			{
				return Convert.ToBase64String(hashBytes);
			}
			else
			{
				return null;
			}
		}

		public byte[] ComputeSHA512HashBytes(byte[] bytBytesToHash)
		{
			SHA512Managed tSHA = new SHA512Managed();
			return tSHA.ComputeHash(bytBytesToHash);
		}

		public string GetRandomSalt()
		{
			byte[] saltBytes;
			saltBytes = GetRandomSaltBytes();
			if (saltBytes != null)
			{
				return Convert.ToBase64String(saltBytes);
			}
			else
			{
				return "";
			}
		}

		public byte[] GetRandomSaltBytes()
		{
			try
			{
				int intMinSaltSize = 5;
				int intMaxSaltSize = 10;
				Random r = new Random();
				int intSaltSize = r.Next(intMinSaltSize, intMaxSaltSize);
				intSaltSize = intSaltSize - 1;
				byte[] saltbytes = new byte[intSaltSize];
				RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
				rng.GetNonZeroBytes(saltbytes);
				return saltbytes;
			}
			catch (Exception ex)
			{
				return null;
			}
		}
	}
}
