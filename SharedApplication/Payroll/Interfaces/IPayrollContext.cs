﻿using System.Linq;
using SharedApplication.Payroll.Models;

namespace SharedApplication.Payroll.Interfaces
{
	public interface IPayrollContext
	{
		IQueryable<PayrollPermission> PayrollPermissions { get; }
		IQueryable<EmployeeMaster> EmployeeMasters { get; }
		IQueryable<W2c> W2Cs { get; }
		IQueryable<W2archive> W2Archives { get; }
		IQueryable<CheckDetail> CheckDetails { get; }
		IQueryable<TempPayProcess> TempPayProcesses { get; }
        IQueryable<CategoryTag> CategoryTags { get; }
        IQueryable<TblPayStatus> PayStatuses { get; }
	}
}
