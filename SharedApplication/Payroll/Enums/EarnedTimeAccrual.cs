﻿using System.Web.UI.WebControls;

namespace SharedApplication.Payroll.Enums
{
    public enum EarnedTimeAccrual
    { 
        None = 0,
        PayPeriod = 1,
        Month = 2,
        Quarter = 4,
        Year = 5,
        AnniversaryMonth = 6,
        SinglePayPeriod = 7,
        AnniversaryDate = 8,
        Hour = 9
    }

    public static class EarnedTimeAccrualExtensions 
    {
        public static int ToInteger(this EarnedTimeAccrual accrual)
        {
            return (int) accrual;
        }

        public static EarnedTimeAccrual FromEarnedTimeAccrual(int accrualCode)
        {
            switch (accrualCode)
            {
                case (int)EarnedTimeAccrual.Year:
                    return EarnedTimeAccrual.Year;
                    break;
                case (int)EarnedTimeAccrual.AnniversaryDate:
                    return EarnedTimeAccrual.AnniversaryDate;
                    break;
                case (int)EarnedTimeAccrual.AnniversaryMonth:
                    return EarnedTimeAccrual.AnniversaryMonth;
                case (int)EarnedTimeAccrual.Hour:
                    return EarnedTimeAccrual.Hour;
                case (int)EarnedTimeAccrual.Month:
                    return EarnedTimeAccrual.Month;
                case (int)EarnedTimeAccrual.PayPeriod:
                    return EarnedTimeAccrual.PayPeriod;
                case (int)EarnedTimeAccrual.Quarter:
                    return EarnedTimeAccrual.Quarter;
                case (int)EarnedTimeAccrual.SinglePayPeriod:
                    return EarnedTimeAccrual.SinglePayPeriod;
            }

            return EarnedTimeAccrual.None;
        }

       
        public static string ToName(this EarnedTimeAccrual accrual)
        {
            switch (accrual)
            {
                case EarnedTimeAccrual.AnniversaryDate:
                    return "Anniversary Date";
                case EarnedTimeAccrual.AnniversaryMonth:
                    return "Anniversary Month";
                case EarnedTimeAccrual.Hour:
                    return "Hour";
                case EarnedTimeAccrual.Month:
                    return "Month";
                case EarnedTimeAccrual.PayPeriod:
                    return "Pay Period";
                case EarnedTimeAccrual.Quarter:
                    return "Quarter";
                case EarnedTimeAccrual.SinglePayPeriod:
                    return "One Time";
                case EarnedTimeAccrual.Year:
                    return "Year";
                    break;
            }

            return "None";
        }

        public static string ToDescription(this EarnedTimeAccrual accrual)
        {
            switch (accrual)
            {
                case EarnedTimeAccrual.AnniversaryDate:
                    return "Added Anniversary Date";
                case EarnedTimeAccrual.AnniversaryMonth:
                    return "Added Anniversary Month";
                case EarnedTimeAccrual.Hour:
                    return "Added Hourly";
                case EarnedTimeAccrual.Month:
                    return "Added Monthly";
                case EarnedTimeAccrual.PayPeriod:
                    return "Added Each Pay Period";
                case EarnedTimeAccrual.Quarter:
                    return "Added Quarterly";
                case EarnedTimeAccrual.SinglePayPeriod:
                    return "This Pay Period Only";
                case EarnedTimeAccrual.Year:
                    return "Added Yearly";
            }

            return "None";
        }
    }
}