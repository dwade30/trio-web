﻿namespace SharedApplication.Payroll.Enums
{
    public enum SSNViewType
    {
        None = 0,
        Masked = 1,
        Full = 2
    }

    public enum BankAccountViewType
    {
        None = 0,
        Masked = 1,
        Full = 2
    }
}