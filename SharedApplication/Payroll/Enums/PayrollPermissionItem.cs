﻿namespace SharedApplication.Payroll.Enums
{
    public enum PayrollPermissionItem
    {
        UsePayroll = 1,
        EmployeeFileUpdate = 2,
        PayrollProcessing = 3,
        CalendarEndOfYear = 4,
        Printing = 5,
        TableAndFileSetup = 6,
        FileMaintenance = 7,
        EmployeeAddUpdate = 8,
        PayrollDistribution = 9,
        Deductions = 10,
        EmployersMatch = 11,
        VacationSick = 12,
        MSRSUnemploymentInfo = 13,
        PayTotals = 14,
        DirectDepositSecurity = 15,
        SelectEmployee = 16,
        NonPaidMSRSinfo = 17,
        CheckReturn = 18,
        FileSetup = 19,
        DataEntrySecurity = 20,
        DataEditAndCalculation = 21,
        VerifyAndAccept = 22,
        Checks = 23,
        PayrollProcessReports = 24,
        PayrollProcessStatus = 25,
        ClearEICCodes = 26,
        DeductionSetup = 27,
        TaxTableMaintenance = 28,
        PayCategorySetup = 29,
        StandardLimitsRates = 30,
        VacationSickOtherCodes = 31,
        CheckRecipients = 32,
        CheckRecipientsCategories = 33,
        SetupPayrollAccounts = 34,
        ReportSelections = 35,
        DataEntrySelection = 36,
        CreateTaxTableDatabase = 37,
        LoadNewTaxTables = 38,
        UpdatePayCodes = 39,
        FrequencyCodes = 40,
        TaxStatusCodes = 41,
        EmployeeLengthsEtc = 42,
        Customize = 43,
        EditStatesTownsCounties = 44,
        TestValidAccounts = 45,
        DirectDepositBanks = 46,
        VerifyAdjustPayTotals = 47,
        AdjustTARecords = 48,
        EmployeeEditSave = 59,
        ContractWages = 60,
        EditPayRecords = 62,
        LoadBack = 63,
        PurgeTerminatedEmployees = 64,
        PurgeAudit = 65,
        ClearLTDDeductions = 67,
        ResetUnemployment = 68,
        PurgeDatabaseData = 69,
        PostPYJournals = 70,
        HealthCareSetup = 71,
        ViewSocialSecurityNumbers = 72,
        ViewBankAccountNumbers = 73
    }

    public static class PayrollPermissionItemExtensions
    {
        public static int ToInteger(this PayrollPermissionItem permissionItem)
        {
            return (int) permissionItem;
        }        
    }
}