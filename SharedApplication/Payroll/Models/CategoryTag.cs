﻿namespace SharedApplication.Payroll.Models
{
    public class CategoryTag
    {
        public int Id { get; set; }
        public int PayCategory { get; set; }
        public string Tag { get; set; }
    }
}