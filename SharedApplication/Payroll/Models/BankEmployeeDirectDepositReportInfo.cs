﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Payroll.Models
{
	public class BankEmployeeDirectDepositReportInfo
	{
		public int EmployeeNumber { get; set; }
		public string DirectDepositAccountNumber { get; set; }
		public string EmployeeName { get; set; }
		public decimal DirectDepositTotal { get; set; }
		public string LastName { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string Designation { get; set; }
	}
}
