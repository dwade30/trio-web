﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.Payroll.Models
{
    public partial class AggregatedAlemember
    {
        public int Id { get; set; }
        public int? YearCovered { get; set; }
        public string MemberName { get; set; }
        public string Ein { get; set; }
    }
}