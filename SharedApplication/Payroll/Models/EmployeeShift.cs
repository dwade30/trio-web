﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.Payroll.Models
{
    public partial class EmployeeShift
    {
        public int Id { get; set; }
        public int? ShiftTypeId { get; set; }
        public string EmployeeNumber { get; set; }
        public string ActualStart { get; set; }
        public string ActualEnd { get; set; }
        public string ScheduledStart { get; set; }
        public string ScheduledEnd { get; set; }
        public short? DayNo { get; set; }
        public DateTime? ShiftDate { get; set; }
        public double? LunchTime { get; set; }
        public string Account { get; set; }
    }
}