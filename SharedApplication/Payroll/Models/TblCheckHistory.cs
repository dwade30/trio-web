﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.Payroll.Models
{
    public partial class TblCheckHistory
    {
        public int Id { get; set; }
        public int? CheckNumber { get; set; }
        public int? ReplacedByCheckNumber { get; set; }
        public string EmployeeNumber { get; set; }
        public string EmployeeName { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? CheckDate { get; set; }
        public int? CheckRun { get; set; }
        public string CheckType { get; set; }
        public string Status { get; set; }
        public int? ReportNumber { get; set; }
        public int? WarrantNumber { get; set; }
    }
}