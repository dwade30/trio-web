﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace SharedApplication.Payroll.Models
{
    public partial class ScheduleWeek
    {
        public int Id { get; set; }
        public int? ScheduleId { get; set; }
        public short? OrderNo { get; set; }
        public string Name { get; set; }
        public double? AlwaysPayHours { get; set; }
        public double? OvertimeStart { get; set; }
    }
}