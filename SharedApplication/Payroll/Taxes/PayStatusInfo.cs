﻿namespace SharedApplication.Payroll
{
    public class PayStatusInfo
    {
        public int Id { get; set; } = 0;
        public int Code { get; set; } = 0;
        public string Description { get; set; } = "";
        public string UseTable { get; set; } = "";
        public string StatusType { get; set; } = "";
        public string TableName { get; set; } = "";
    }
}