﻿namespace SharedApplication.Payroll
{
    public class MaineStateTaxTableRules : ITaxTableRules
    {
        public PayrollLocality Locality { get; }

        public MaineStateTaxTableRules()
        {
            Locality = new PayrollLocality("US","ME","");
        }
        public string GetTableName(PayrollFilingStatusInfo filingStatus)
        {
            switch (filingStatus.StatusCode)
            {
                case (int)PayStatusType.Single:
                case (int)PayStatusType.MarriedFilingSingle:
                    return "US_ME_Single";
                case (int)PayStatusType.Married:
                    return "US_ME_Married";

            }

            return "";
        }
    }
}