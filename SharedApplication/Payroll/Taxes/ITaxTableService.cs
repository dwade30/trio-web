﻿namespace SharedApplication.Payroll
{
    public interface ITaxTableService
    {
        int Year { get; }
        decimal GetTax(decimal amount, PayrollFilingInfo filingInfo);
        bool HasTables();

        (decimal Value, bool Success) GetSetupValue(PayrollLocality locality, string valueName);
    }
}