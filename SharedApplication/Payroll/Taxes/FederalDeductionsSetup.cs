﻿namespace SharedApplication.Payroll
{
    public class FederalDeductionsSetup
    {
        public decimal StandardDeduction { get; set; }
        public decimal SingleDeduction { get; set; }
        public decimal MarriedDeduction { get; set; }
        public decimal MedicareMaximumWage { get; set; }
        public decimal MedicareEmployeePercentage { get; set; }
        public decimal MedicareEmployerPercentage { get; set; }
        public decimal MedicareThreshold { get; set; }
        public decimal MedicareThresholdRate { get; set; }
        public int QualifyingChildren { get; set; }
        public int OtherDependents { get; set; }

        public decimal FicaMaximumWage { get; set; }
        public decimal FicaEmployeePercentage { get; set; }
        public decimal FicaEmployerPercentage { get; set; }
    }
}