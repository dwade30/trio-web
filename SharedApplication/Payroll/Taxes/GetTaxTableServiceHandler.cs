﻿using System.Collections.Generic;
using SharedApplication.Messaging;

namespace SharedApplication.Payroll
{
    public class GetTaxTableServiceHandler : CommandHandler<GetTaxTableService,ITaxTableService>
    {
        private IEnumerable<ITaxTableRules> taxTableRules;
        private IPayrollCloudAPI cloudApi;
        public GetTaxTableServiceHandler(IEnumerable<ITaxTableRules> taxTableRules, IPayrollCloudAPI cloudApi)
        {
            this.taxTableRules = taxTableRules;
            this.cloudApi = cloudApi;
        }

        protected override ITaxTableService Handle(GetTaxTableService command)
        {
            var tableGroups = cloudApi.GetTaxTableGroupsForYear(command.Year);
            var taxGroups = new TaxGroups(command.Year,tableGroups);
           // var tables = cloudApi.GetTablesForYear(command.Year);
           // var setups = cloudApi.GetTaxSetupsForYear(command.Year);
            var service = new TaxTableService(taxGroups,taxTableRules);
            return service;
        }
    }
}