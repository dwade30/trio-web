﻿using System.Collections.Generic;

namespace SharedApplication.Payroll
{
    public interface IPayrollCloudAPI
    {
        //IEnumerable<TaxTable> GetTablesForYear(int year);
        IEnumerable<int> GetAvailableYears();
        //IEnumerable<ILocalityTaxSetup> GetTaxSetupsForYear(int year);
        IEnumerable<TaxTableGroup> GetTaxTableGroupsForYear(int year);
    }
}