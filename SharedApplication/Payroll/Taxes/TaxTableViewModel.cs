﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace SharedApplication.Payroll
{
    public class TaxTableViewModel : ITaxTableViewModel
    {
        public int Year { get; private set; }
        private IPayrollCloudAPI cloudApi;
        private Dictionary<int,TaxGroups> TaxSetupByYear = new Dictionary<int, TaxGroups>();
        private List<int> availableYears = new List<int>();
        public TaxTableViewModel(IPayrollCloudAPI cloudApi)
        {
            this.cloudApi = cloudApi;
        }
        public void SetYear(int year)
        {
            Year = year;
        }

        public IEnumerable<int> GetAvailableYears()
        {
            if (!availableYears.Any())
            {
                availableYears.AddRange(cloudApi.GetAvailableYears());
            }

            return availableYears;
        }

        public TaxGroups GetGroupForYear(int year)
        {
            if (!TaxSetupByYear.ContainsKey(year))
            {
                var groups = cloudApi.GetTaxTableGroupsForYear(year);
                //var tables = cloudApi.GetTablesForYear(year);
                //var setups = cloudApi.GetTaxSetupsForYear(year);
                var group = new TaxGroups(year,groups);
                TaxSetupByYear.Add(year,group);
            }

            TaxSetupByYear.TryGetValue(year, out var result);
            return result;
        }

        private IEnumerable<TaxTableGroup> MakeTableGroups(IEnumerable<TaxTable> taxTables,
            IEnumerable<ILocalityTaxSetup> setups)
        {
            var tableGroups = new List<TaxTableGroup>();
            var groups = taxTables.GroupBy(t => t.Locality.FullLocalityName).Select(t => t.Key);
            foreach (var groupName in groups)
            {
                var groupTables = taxTables.Where(t => t.Locality.FullLocalityName == groupName);
                var groupSetup = setups.FirstOrDefault(s => s.Locality.FullLocalityName == groupName);
                var firstTable = groupTables.FirstOrDefault();
                tableGroups.Add(new TaxTableGroup(new PayrollLocality(firstTable.Locality.FederalLocality,firstTable.Locality.StateProvince,firstTable.Locality.Municipality),groupTables,groupSetup ));
            }

            return tableGroups;
        }
    }
}