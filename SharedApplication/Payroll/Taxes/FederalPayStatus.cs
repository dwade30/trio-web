﻿using System.ComponentModel;
using System.Web.UI.WebControls;

namespace SharedApplication.Payroll
{
    public class FederalPayStatus
    {
        public int Id { get; set; }
        public int StatusCode { get; set; }
        public string Description { get; set; }
        public string UseTable { get; set; }        
        public FederalDeductionType DeductionType { get; set; }
        public FederalPayStatusType StatusType { get; set; }
    }

    public enum FederalDeductionType
    {
        None = 0,
        Single = 1,
        Married = 2
    }

    public enum FederalPayStatusType
    {
        Single = 2,
        Married = 3,
        MarriedFilingSingle = 1,
        HeadOfHousehold = 4
    }

    public class FederalPayStatusService
    {
        public string TableToUse(FederalPayStatusType payStatusType, bool isMultiJob)
        {
            switch (payStatusType)
            {
                case FederalPayStatusType.Married:
                    if (isMultiJob)
                    {
                        return "tblFederalMarriedMultiJobsTaxTable";
                    }
                    else
                    {
                        return "tblFederalMarriedTaxTable";
                    }
                    break;
                case FederalPayStatusType.MarriedFilingSingle:
                    if (isMultiJob)
                    {
                        return "tblFederalSingleMultiJobsTaxTable";
                    }
                    else
                    {
                        return "tblFederalSingleTaxTable";
                    }
                    break;
                case FederalPayStatusType.HeadOfHousehold:
                    if (isMultiJob)
                    {
                        return "tblFederalHeadOfHouseholdMultiJobsTaxTable";
                    }
                    else
                    {
                        return "tblFederalHeadOfHouseholdTaxTable";
                    }
                    break;
                case FederalPayStatusType.Single:
                    if (isMultiJob)
                    {
                        return "tblFederalSingleMultiJobsTaxTable";
                    }
                    else
                    {
                        return "tblFederalSingleTaxTable";
                    }
                    break;
                default:
                    throw new InvalidEnumArgumentException("FederalPayStatusType", (int) payStatusType,
                        typeof(FederalPayStatusType));
                    break;
            }
        }

        public FederalDeductionType DeductionToUse(FederalPayStatusType payStatusType, bool isMultiJob)
        {
            if (isMultiJob)
            {
                return FederalDeductionType.None;
            }

            if (payStatusType == FederalPayStatusType.Married)
            {
                return FederalDeductionType.Married;
            }

            return FederalDeductionType.Single;
        }

        public FederalPayStatusType StatusFromCode(int code)
        {
            switch (code)
            {
                case 1:
                    return FederalPayStatusType.MarriedFilingSingle;
                case 2:
                    return FederalPayStatusType.Single;
                case 3:
                    return FederalPayStatusType.Married;
                case 4:
                    return FederalPayStatusType.HeadOfHousehold;
            }

            return FederalPayStatusType.Single;
        }
    }
}