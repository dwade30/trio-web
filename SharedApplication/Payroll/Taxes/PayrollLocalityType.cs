﻿namespace SharedApplication.Payroll
{
    public enum PayrollLocalityType
    {
        Federal = 0,
        State = 1,
        County = 2,
        Municipality = 3
    }
}