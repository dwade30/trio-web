﻿using SharedApplication.Extensions;

namespace SharedApplication.Payroll
{
    public class PayrollLocality
    {
        public string FederalLocality { get; set; } 
        public string StateProvince { get; set; }
        public string Municipality { get; set; }
        public string County { get; set; } //not used yet. Maybe should just use municipality but need to deal with a county and municipality having the same name. I.E. Penobscot or Merrimack

        public PayrollLocalityType LocalityType { get; set; }
        public string FullLocalityName
        {
            get
            {
                if (StateProvince.IsNullOrWhiteSpace())
                {
                    return FederalLocality;
                }

                if (Municipality.IsNullOrWhiteSpace())
                {
                    return FederalLocality + "_" + StateProvince;
                }

                return FederalLocality + "_" + StateProvince + "_" + Municipality;
            }
        }
        //public string LocalityName {
        //    get
        //    {
        //        switch (LocalityType)
        //        {
        //            case PayrollLocalityType.Municipality:
        //                return Municipality;
        //            case PayrollLocalityType.State:
        //                return StateProvince;
        //            case PayrollLocalityType.Federal:
        //                return FederalLocality;
        //        }

        //        return "";
        //    }
        //}

        public PayrollLocality()
        {

        }
        public PayrollLocality(string federalLocality, string stateProvince, string municipality)
        {
            FederalLocality = federalLocality;
            StateProvince = stateProvince;
            Municipality = municipality;
            if (municipality.HasText())
            {
                LocalityType = PayrollLocalityType.Municipality;
            }
            else if (stateProvince.HasText())
            {
                LocalityType = PayrollLocalityType.State;
            }
            else
            {
                LocalityType = PayrollLocalityType.Federal;
            }

        }

        public PayrollLocality(string federalLocality)
        {
            FederalLocality = federalLocality;
            StateProvince = "";
            Municipality = "";
            LocalityType = PayrollLocalityType.Federal;
        }

        public PayrollLocality(string federalLocality, string stateProvince)
        {
            FederalLocality = federalLocality;
            StateProvince = stateProvince;
            Municipality = "";
            LocalityType = PayrollLocalityType.State;
        }

        public string GetLocalName()
        {
            switch (LocalityType)
            {
                case PayrollLocalityType.State:
                    return StateProvince;
                    break;
                case PayrollLocalityType.Municipality:
                    return Municipality;
                    break;
                case PayrollLocalityType.Federal:
                    return FederalLocality;
                    break;
                case PayrollLocalityType.County:
                    return County;
                    break;
            }

            return "";
        }
    }
}