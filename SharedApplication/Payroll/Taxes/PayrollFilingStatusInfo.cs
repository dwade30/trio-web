﻿namespace SharedApplication.Payroll
{
    public class PayrollFilingStatusInfo
    {
        public bool IsMultiJob { get; } 
        public int StatusCode { get; } 

        public PayrollFilingStatusInfo(int statusCode, bool isMultiJob)
        {
            IsMultiJob = isMultiJob;
            StatusCode = statusCode;
        }
    }
}