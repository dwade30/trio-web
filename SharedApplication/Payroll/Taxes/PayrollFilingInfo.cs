﻿namespace SharedApplication.Payroll
{
    public class PayrollFilingInfo
    {
        public PayrollFilingStatusInfo FilingStatus { get; }
        public PayrollLocality Locality { get; }

        public PayrollFilingInfo(PayrollFilingStatusInfo filingStatus, PayrollLocality locality)
        {
            FilingStatus = filingStatus;
            Locality = locality;
        }
    }
}