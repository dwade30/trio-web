﻿using System.Collections;
using System.Collections.Generic;
using SharedApplication.Messaging;

namespace SharedApplication.Payroll
{
    public class GetAvailableTaxYearsHandler : CommandHandler<GetAvailableTaxYears,IEnumerable<int>>
    {
        private IPayrollCloudAPI cloudApi;
        public GetAvailableTaxYearsHandler(IPayrollCloudAPI cloudApi)
        {
            this.cloudApi = cloudApi;
        }
        protected override IEnumerable<int> Handle(GetAvailableTaxYears command)
        {
            return cloudApi.GetAvailableYears();
        }
    }
}