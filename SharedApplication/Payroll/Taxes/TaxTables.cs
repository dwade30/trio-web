﻿using System;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace SharedApplication.Payroll
{
    public class TaxGroups
    {
        public int TaxYear { get; set; }

        public IEnumerable<TaxTableGroup> Groups { get; } = new List<TaxTableGroup>();
        public TaxGroups(int year, IEnumerable<TaxTableGroup> taxGroups)
        {
            TaxYear = year;
            if (taxGroups != null)
            {
                Groups = taxGroups;
            }
        }
    }

    public class NamedAmount
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
    }

    public class TaxTableGroup
    {
        public PayrollLocalityType LocalityType
        {
            get;
            private set;
        }
        public string LocalityName { get => Locality.GetLocalName();}
        public PayrollLocality Locality { get; set; }
        public IEnumerable<TaxTable> TaxTables { get; } = new List<TaxTable>();

        public ILocalityTaxSetup TaxSetup { get; protected set; }

        public TaxTableGroup(PayrollLocality locality, IEnumerable<TaxTable> tables, ILocalityTaxSetup taxSetup)
        {
            Locality = locality;
            TaxTables = tables;
            TaxSetup = taxSetup;

            if (Locality.Municipality.HasText())
            {
                LocalityType = PayrollLocalityType.Municipality;
                //LocalityName = locality.Municipality;
            }
            else if (Locality.StateProvince.HasText())
            {
                LocalityType = PayrollLocalityType.State;
               // LocalityName = locality.StateProvince;
            }
            else
            {
                LocalityType = PayrollLocalityType.Federal;
               // LocalityName = locality.FederalLocality;
            }

        }
    }

    public class TaxTableGroupDTO
    {
        public int TaxYear { get; set; } = 0;
        public string FullLocalityName { get; set; } = "";
        public PayrollLocalityType LocalityType { get; set; }
        public IEnumerable<NamedAmount> Amounts
        {
            get;
            set;
        }
        public PayrollLocality Locality { get; set; }
        public IEnumerable<TaxTableDTO> TaxTables { get; set; }
    }

    public class TaxTableDTO
    {
        //public PayrollLocalityType LocalityType { get; set; }
        public string TableName { get; set; } = "";
        public string TableDescription { get; set; } = "";
        //public PayrollLocality Locality { get; set; }// = new PayrollLocality("US","","");
        public IEnumerable<TaxTableEntry> Entries { get; set; }
    }

    public class TaxTable
    {
        public PayrollLocalityType LocalityType { get; set; } = PayrollLocalityType.Federal;
        //public string LocalityName { get; set; } = "US";
        public int FilingStatusCode { get; set; } = 0;
        public string TableName { get; set; } = "";
        public string TableDescription { get; set; } = "";
        public PayrollLocality Locality { get; set; }// = new PayrollLocality("US","","");
        public IEnumerable<TaxTableEntry> Entries { get; set; }
    }



    public class TaxTableEntry
    {
        public decimal TaxAmount { get; set; } = 0;
        //public decimal AmountOver { get; set; } = 0;
        public decimal TaxPercentage { get; set; } = 0;
        public decimal MinimumAmount { get; set; } = 0;
        public decimal MaximumAmount { get; set; } = 0;

    }

   
}