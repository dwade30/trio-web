﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.Payroll
{
    public interface ILocalityTaxSetup
    {
        (decimal Amount, bool Success) GetAmount(string amountName);

        PayrollLocality Locality { get; }

    }

    public class LocalityTaxSetup : ILocalityTaxSetup
    {
        private Dictionary<string,decimal> setupAmounts = new Dictionary<string, decimal>();
        

        public LocalityTaxSetup(PayrollLocality locality, IEnumerable<NamedAmount> setupAmounts)
        {
            this.setupAmounts = setupAmounts.ToDictionary(s => s.Name, s => s.Amount);
            Locality = locality;
        }

        public PayrollLocality Locality { get; private set; }

        public (decimal Amount, bool Success) GetAmount(string amountName)
        {
            if (setupAmounts.TryGetValue(amountName, out var amount))
            {
                return (Amount: amount, Success: true);
            }
            else
            {
                return (Amount: 0, Success: false);
            }
        }
    }
}