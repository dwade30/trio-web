﻿namespace SharedApplication.Payroll
{
    public class USFederalTaxTableRules : ITaxTableRules
    {
        public PayrollLocality Locality { get; }

        public USFederalTaxTableRules()
        {
            Locality = new PayrollLocality("US","","");
        }
        public string GetTableName(PayrollFilingStatusInfo filingStatus)
        {
            switch (filingStatus.StatusCode)
            {
                case (int)FederalPayStatusType.Married:
                    if (filingStatus.IsMultiJob)
                    {
                        return "US_MarriedMultiJobs";
                    }

                    return "US_Married";
                case (int)FederalPayStatusType.MarriedFilingSingle:
                    if (filingStatus.IsMultiJob)
                    {
                        return "US_SingleMultiJobs";
                    }

                    return "US_Single";
                case (int)FederalPayStatusType.HeadOfHousehold:
                    if (filingStatus.IsMultiJob)
                    {
                        return "US_HeadOfHouseholdMultiJobs";
                    }

                    return "US_HeadOfHousehold";
                case (int)FederalPayStatusType.Single:
                    if (filingStatus.IsMultiJob)
                    {
                        return "US_SingleMultiJobs";
                    }

                    return "US_Single";
            }

            return "";
        }
    }
}