﻿using System.Collections.Generic;

namespace SharedApplication.Payroll
{
    public interface ITaxTableViewModel
    {
        int Year { get; }
        void SetYear(int year);
        IEnumerable<int> GetAvailableYears();
        TaxGroups GetGroupForYear(int year);
    }
}