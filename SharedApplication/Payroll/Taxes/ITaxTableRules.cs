﻿namespace SharedApplication.Payroll
{
    public interface ITaxTableRules
    {
        PayrollLocality Locality { get; }
        string GetTableName(PayrollFilingStatusInfo filingStatus);

    }
}