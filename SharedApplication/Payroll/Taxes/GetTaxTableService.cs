﻿using SharedApplication.Messaging;

namespace SharedApplication.Payroll
{
    public class GetTaxTableService : Command<ITaxTableService>
    {
        public int Year { get; }

        public GetTaxTableService(int year)
        {
            Year = year;
        }
    }
}