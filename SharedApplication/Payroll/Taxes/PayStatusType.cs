﻿namespace SharedApplication.Payroll
{
    public enum PayStatusType
    {
        None = 0,
        Married = 1,
        Single = 2,
        MarriedFilingSingle = 3
    }
}