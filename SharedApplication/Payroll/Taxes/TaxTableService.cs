﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.Payroll
{
    public class TaxTableService : ITaxTableService
    {
        //private Dictionary<string,TaxTable> taxTables = new Dictionary<string, TaxTable>();
        private List<ITaxTableRules> rules = new List<ITaxTableRules>();
        //private List<ILocalityTaxSetup> taxSetups = new List<ILocalityTaxSetup>();
        private TaxGroups taxGroups;
        public int Year { get; set; }

        public TaxTableService(TaxGroups taxGroups, IEnumerable<ITaxTableRules> taxTableRules)
        {
            this.taxGroups = taxGroups;
            Year = taxGroups.TaxYear;
            rules.AddRange(taxTableRules);
            //taxTables = tables.ToDictionary(t => t.TableName);
            //this.taxSetups.AddRange(taxSetups);
        }
        public decimal GetTax(decimal amount, PayrollFilingInfo filingInfo)
        {
            
            var table = GetTableFromFilingInfo(filingInfo);
            if (table != null)
            {
                return GetTaxFromTable(amount, table);
            }
            else
            {
                return 0;
            }
        }

        public bool HasTables()
        {
            return taxGroups.Groups.Any(g => g.TaxTables.Any());
        }

        public (decimal Value, bool Success) GetSetupValue(PayrollLocality locality, string valueName)
        {
            var group = taxGroups.Groups.FirstOrDefault(g => g.Locality.FullLocalityName == locality.FullLocalityName);
            var setup = @group?.TaxSetup;

            if (setup != null)
            {
                var result = setup.GetAmount(valueName);
                return (Value: result.Amount, Success: result.Success);
            }

            return (Value: 0, Success: false);
        }

        private TaxTable GetTableFromFilingInfo(PayrollFilingInfo filingInfo)
        {
            var rule = rules.FirstOrDefault(r => r.Locality.FullLocalityName == filingInfo.Locality.FullLocalityName);
            if (rule == null)
            {
                return null;
            }

            var tableName = rule.GetTableName(filingInfo.FilingStatus);
            var group = taxGroups.Groups.FirstOrDefault(g =>
                g.Locality.FullLocalityName == filingInfo.Locality.FullLocalityName);
            return @group?.TaxTables.FirstOrDefault(t => t.TableName == tableName);
        }

        private decimal GetTaxFromTable(decimal amount, TaxTable table)
        {
            if (table == null)
            {
                return 0;
            }

            foreach (var entry in table.Entries)
            {
                if (amount <= entry.MaximumAmount)
                {
                    if (entry.TaxPercentage == 0)
                    {
                        return 0;
                    }

                    return entry.TaxAmount + ((amount - entry.MinimumAmount) * entry.TaxPercentage);
                }
            }

            return 0;
        }


    }
}