﻿using Autofac;

namespace SharedApplication.Payroll.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<USFederalTaxTableRules>().As<ITaxTableRules>();
            builder.RegisterType<MaineStateTaxTableRules>().As<ITaxTableRules>();
            builder.RegisterType<TaxTableViewModel>().As<ITaxTableViewModel>();
        }
    }
}