﻿using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.Payroll.Models;

namespace SharedApplication.Payroll.Commands
{
    public class GetCategoryTags : Command<IEnumerable<CategoryTag>>
    {
        public int PayCategory { get;  }

        public GetCategoryTags(int payCategory)
        {
            PayCategory = payCategory;
        }
    }
}