﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.Payroll.Models;

namespace SharedApplication.Payroll.Commands
{
	public class SaveW2CInfo : Command<int>
	{
		public W2c W2CInfo { get; set; }
	}
}
