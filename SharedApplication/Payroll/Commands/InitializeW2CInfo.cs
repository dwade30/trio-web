﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.Payroll.Commands
{
	public class InitializeW2CInfo : Command<int>
	{
		public string SocialSecurityNumber { get; set; }
		public int TaxYear { get; set; }
		public string EmployeeNumber { get; set; }
		public string DeptDiv { get; set; }
		public int SequenceNumber { get; set; }
	}
}
