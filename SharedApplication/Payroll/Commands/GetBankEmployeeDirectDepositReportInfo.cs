﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;using SharedApplication.Messaging;
using SharedApplication.Payroll.Enums;
using SharedApplication.Payroll.Models;

namespace SharedApplication.Payroll.Commands
{
	public class GetBankEmployeeDirectDepositReportInfo : Command<IEnumerable<BankEmployeeDirectDepositReportInfo>>
	{
		public bool UseTempTable { get; set; } = false;
		public DateTime PayDate { get; set; }
		public int BankNumber { get; set; } = 0;
		public int PayRunId { get; set; } = 0;
	}
}
