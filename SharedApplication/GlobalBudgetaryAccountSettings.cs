﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharedApplication.Budgetary.Enums;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Extensions;

namespace SharedApplication
{
	public class GlobalBudgetaryAccountSettings : IGlobalBudgetaryAccountSettings
	{
		public bool ExpDivFlag { get; set; }
		public bool RevDivFlag { get; set; }
		public bool ObjFlag { get; set; }
		public bool YearFlag { get; set; }
		public string Exp { get; set; } = "";
		public string Rev { get; set; } = "";
		public string Ledger { get; set; } = "";
        public bool UsePUCChartOfAccounts { get; set; } = false;

		private ValidAccountSetting validAccountCheck = 0;
		public ValidAccountSetting ValidAcctCheck
		{
			get 
			{
				return validAccountCheck;
			} 
			set
			{
				if ((int) value < 0 || (int) value > 3)
				{
					validAccountCheck = ValidAccountSetting.NotSet;
				}
				else
				{
					validAccountCheck = value;
				}
			}
		}

		public int ExpenseDepartmentLength()
		{
			return Exp.Left(2).ToIntegerValue();
		}

		public int ExpenseDivisionLength()
		{
			return Exp.Substring(2, 2).ToIntegerValue();
		}

		public int ExpenseExpenseLength()
		{
			return Exp.Substring(4, 2).ToIntegerValue();
		}

		public int ExpenseObjectLength()
		{
			return Exp.Substring(6, 2).ToIntegerValue();
		}

		public int LedgerFundLength()
		{
			return Ledger.Substring(0, 2).ToIntegerValue();
		}

		public int LedgerAccountLength()
		{
			return Ledger.Substring(2, 2).ToIntegerValue();
		}

		public int LedgerSuffixLength()
		{
			return Ledger.Substring(4, 2).ToIntegerValue();
		}

		public int RevenueDepartmentLength()
		{
			return Rev.Left(2).ToIntegerValue();
		}

		public int RevenueDivisionLength()
		{
			return Rev.Substring(2, 2).ToIntegerValue();
		}

		public int RevenueRevenueLength()
		{
			return Rev.Substring(4, 2).ToIntegerValue();
		}

		public bool DivisionExistsInExpense()
		{
			return !ExpDivFlag;
		}

		public bool DivisionExistsInRevenue()
		{
			return !RevDivFlag;
		}

		public bool ObjectExistsInExpense()
		{
			return !ObjFlag;
		}

		public bool SuffixExistsInLedger()
		{
			return !YearFlag;
		}

		public int ExpenseTotalLength()
		{
			return ExpenseDepartmentLength() + ExpenseDivisionLength() + ExpenseExpenseLength() + ExpenseObjectLength();
		}

		public int RevenueTotalLength()
		{
			return RevenueDepartmentLength() + RevenueDivisionLength() + RevenueRevenueLength();
		}

		public int LedgerTotalLength()
		{
			return LedgerFundLength() + LedgerAccountLength() + LedgerSuffixLength();
		}

		public string RevenueZeroDivisionString()
		{
			if (!DivisionExistsInRevenue())
			{
				if (!DivisionExistsInExpense())
				{
					return "0";
				}
				else
				{
					return new string('0', ExpenseDivisionLength());
				}
			}
			else
			{
				return new string('0', RevenueDivisionLength());
			}
		}

		public string ExpenseZeroDivisionString()
		{
			if (!DivisionExistsInExpense())
			{
				return "0";
			}
			else
			{
				return new string('0', ExpenseDivisionLength());
			}
		}

		public string ObjectZeroString()
		{
			if (!ObjectExistsInExpense())
			{
				return "0";
			}
			else
			{
				return new string('0', ExpenseObjectLength());
			}
		}

		public string AccountZeroString()
		{
			return new string('0', LedgerAccountLength());
		}

		public string BuildLedgerAccount(int fund, int account, int suffix = 0)
		{
			return "G " + fund.ToString().PadLeft(LedgerFundLength(), '0') + "-" +
			             account.ToString().PadLeft(LedgerAccountLength(), '0') + (SuffixExistsInLedger() ? "-" + suffix.ToString().PadLeft(LedgerSuffixLength(), '0') : "");
		}

		public string FormatBudgetaryFund(int fund)
		{
			return fund.ToString().PadLeft(LedgerFundLength(), '0');
		}
    }
}
