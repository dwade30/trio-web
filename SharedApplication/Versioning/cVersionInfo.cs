﻿using System;

namespace SharedApplication.Versioning
{

		public class cVersionInfo
		{
			//=========================================================
			private int intMajor = 0;
			private int intMinor = 0;
			private int intRevision = 0;
			private int intBuild = 0;
			// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
            public int Major
            {
                get
                {
                    return intMajor;
                }
                set
                {
                    intMajor = value;
                }
            } 
			// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			public int Minor
			{
				get
				{
					return intMinor;
				}
				set
				{
					intMinor = value;
				}
			}
			// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			public int Revision
			{
				get
				{
					return intRevision;
				}
				set
				{
					intRevision = value;
				}
			}
			// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			public int Build
			{
				get
				{
					return intBuild;
				}
				set
				{
					intBuild = value;
				}
			}

			public string VersionString
			{
				get
				{
					string VersionString = "";
					string strReturn;
					strReturn = intMajor.ToString() + "." + intMinor.ToString() + "." + intRevision.ToString() + "." + intBuild.ToString();
					VersionString = strReturn;
					return VersionString;
				}
			}

			public void Copy(cVersionInfo SourceVersion)
			{
				if (!(SourceVersion == null))
				{
					intMajor = SourceVersion.Major;
					intMinor = SourceVersion.Minor;
					intRevision = SourceVersion.Revision;
					intBuild = SourceVersion.Build;
				}
			}

			public bool IsOlder(cVersionInfo CompareVersion)
			{
				bool IsOlder = false;
				// IsOlder is true if CompareVersion is newer than this instance
				IsOlder = false;
				if (!(CompareVersion == null))
				{
					if (CompareVersion.Major > intMajor)
					{
						IsOlder = true;
					}
					else
					{
						if (CompareVersion.Major == intMajor)
						{
							if (CompareVersion.Minor > intMinor)
							{
								IsOlder = true;
							}
							else
							{
								if (CompareVersion.Minor == intMinor)
								{
									if (CompareVersion.Revision > intRevision)
									{
										IsOlder = true;
									}
									else
									{
										if (CompareVersion.Revision == intRevision)
										{
											if (CompareVersion.Build > intBuild)
											{
												IsOlder = true;
											}
										}
									}
								}
							}
						}
					}
				}
				return IsOlder;
			}

			public bool IsEqual(ref cVersionInfo CompareVersion)
			{
				bool IsEqual = false;
				IsEqual = false;
				if (!(CompareVersion == null))
				{
					if (CompareVersion.Major == intMajor)
					{
						if (CompareVersion.Minor == intMinor)
						{
							if (CompareVersion.Revision == intRevision)
							{
								if (CompareVersion.Build == intBuild)
								{
									IsEqual = true;
								}
							}
						}
					}
				}
				return IsEqual;
			}

			public bool IsNewer(cVersionInfo CompareVersion)
			{
				bool IsNewer = false;
				IsNewer = false;
				if (!(CompareVersion == null))
				{
					IsNewer = CompareVersion.IsOlder(this);
				}
				return IsNewer;
			}

			public cVersionInfo() : base()
			{
				intMinor = 0;
				intRevision = 0;
				intBuild = 0;
				intMajor = 0;
			}
		}
}
