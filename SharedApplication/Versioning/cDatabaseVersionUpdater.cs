﻿using System;
using SharedApplication.Models;
using System.Data;
using System.Data.SqlClient;
using SharedApplication.Extensions;

namespace SharedApplication.Versioning
{
    public abstract class cDatabaseVersionUpdater
    {
        protected string strLastError = "";
        protected string defaultDatabaseName = "";
        protected SQLConfigInfo dbInfo = new SQLConfigInfo();
        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
            protected set
            {
                strLastError = value;
            }
        }

        public string ArchiveGroup { get; set; } = "Live";
        public string DataEnvironment { get; set; } = "";
        

        public void ClearErrors()
        {
            strLastError = "";
        }

        protected string GetConnectionString()
        {
            var builder = new SqlConnectionStringBuilder()
            {
                Password = dbInfo.Password,
                UserID =  dbInfo.UserName,
                DataSource = dbInfo.ServerInstance,
                InitialCatalog =  MakeDBName()
            };
            return builder.ConnectionString;
        }

        protected string GetMasterConnectionString()
        {
            var builder = new SqlConnectionStringBuilder()
            {
                Password = dbInfo.Password,
                UserID = dbInfo.UserName,
                DataSource = dbInfo.ServerInstance
            };
            return builder.ConnectionString;
        }

        protected string MakeDBName()
        {
            var group = DataEnvironment;
            if (DataEnvironment.IsNullOrWhiteSpace() || defaultDatabaseName.ToLower() == "bluebook" || defaultDatabaseName.ToLower() == "clientsettings")
            {
                group = "ALL";
            }

            return "TRIO_" + group + "_" + ArchiveGroup + "_" + defaultDatabaseName;
        }

        public cVersionInfo GetVersion()
        {
            cVersionInfo GetVersion = null;
            try
            {   // On Error GoTo ErrorHandler
                ClearErrors();
                cVersionInfo tVer = new cVersionInfo();
                using (var connection = new SqlConnection(GetConnectionString()))
                {
                    using (var command = new SqlCommand("select * from dbversion", connection))
                    {
                        connection.Open();
                        var reader = command.ExecuteReader();
                        if (reader.Read())
                        {
                            tVer.Major =  (int)reader["Major"];
                            tVer.Minor = (int) reader["Minor"];
                            tVer.Revision = (int) reader["Revision"];
                            tVer.Build = (int) reader["Build"];
                        }
                    }
                }

                return tVer;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = ex.Message;
            }
            return GetVersion;
        }

        public void SetVersion( cVersionInfo nVersion)
        {
            try
            {   // On Error GoTo ErrorHandler
                if (!(nVersion == null))
                {
                    using (var connection = new SqlConnection(GetConnectionString()))
                    {
                        using (var command = new SqlCommand("select * from dbversion", connection))
                        {
                            connection.Open();
                            var reader = command.ExecuteReader();
                            if (reader.HasRows)
                            {
                                command.CommandText = "Update dbversion set Major = " + nVersion.Major.ToString()
                                                                                      + ", Minor = " +
                                                                                      nVersion.Minor.ToString()
                                                                                      + ", Revision = " +
                                                                                      nVersion.Revision.ToString()
                                                                                      + ",Build = " +
                                                                                      nVersion.Build.ToString();
                            }
                            else
                            {
                                command.CommandText = "Insert into dbversion (Major,Minor,Revision,Build) Values ("
                                                      + nVersion.Major.ToString()
                                                      + "," + nVersion.Minor.ToString()
                                                      + "," + nVersion.Revision.ToString()
                                                      + "," + nVersion.Build.ToString()
                                                      + ")";
                            }
                            reader.Close();
                            command.ExecuteNonQuery();
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = ex.Message;
            }
        }

        protected string GetTableCreationHeaderSQL(string strTableName)
        {
            string strSQL;
            strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
            strSQL += "Create Table [dbo].[" + strTableName + "] (";
            strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
            return strSQL;
        }

        protected string GetTablePKSql(string strTableName)
        {
            return " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
        }

        protected bool FieldExists(string strTable, string strField)
        {
            bool FieldExists = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL;
                strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";

                using (var connection = new SqlConnection(GetConnectionString()))
                {
                    using (var command = new SqlCommand(strSQL, connection))
                    {
                        connection.Open();
                        var reader = command.ExecuteReader();
                        var fieldExists = reader.HasRows;
                        reader.Close();
                        return fieldExists;
                    }
                }
            }
            catch
            {   // ErrorHandler:

            }
            return false;
        }

        protected bool TableExists(string tableName)
        {
            var sql = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'" + tableName + "'";
            using (var connection = new SqlConnection(GetConnectionString()))
            {
                using (var command = new SqlCommand(sql, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    var tableExists = reader.HasRows;
                    reader.Close();
                    return tableExists;
                }
            }
        }

        protected virtual bool CheckForDatabase()
        {
            var strDB = MakeDBName();
            string strSQL;
         
            strSQL = "select * from sys.databases where name = '" + strDB + "'";
            using (var connection = new SqlConnection(GetMasterConnectionString()))
            {
                using (var command = new SqlCommand(strSQL, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    var hasDB = reader.HasRows;
                    reader.Close();
                    return hasDB;

                }
            }
        }

        protected void ExecuteStatement(string statement)
        {
            ExecuteStatement(statement,GetConnectionString());
        }

        protected void ExecuteStatement(string statement, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand(statement, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }
        
        public bool CheckVersion()
        {
            bool CheckVersion = false;
            ClearErrors();
            try
            {   // On Error GoTo ErrorHandler
                cVersionInfo nVer = new cVersionInfo();
                cVersionInfo tVer = new cVersionInfo();
                cVersionInfo cVer;
                bool boolNeedUpdate;
                
                if (!CheckForDatabase())
                {
                    return false;
                }
                cVer = GetVersion();
                if (cVer == null)
                {
                    cVer = new cVersionInfo();
                }
                nVer.Major = 1; // default to 1.0.0.0

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 0;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    SetVersion(tVer);
                }

                PerformChecks(cVer);
                return true;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = ex.Message;
            }
            return false;
        }

        protected abstract bool PerformChecks(cVersionInfo currentVersion);
        public delegate bool Func<out TResult>();
        protected void CheckFunction(Func<bool> checkFunction,cVersionInfo  currentVersion, cVersionInfo expectedVersion)
        {

            if (expectedVersion.IsNewer(currentVersion))
            {
                if (!checkFunction())
                {
                    throw new Exception("Could not update " + defaultDatabaseName + " to " + expectedVersion.ToString());
                }
                SetVersion(expectedVersion);
            }
        }

    }
}