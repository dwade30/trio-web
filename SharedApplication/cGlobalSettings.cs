﻿using System;
using System.Security.Permissions;

namespace SharedApplication
{
    public class cGlobalSettings
    {
        private string strMasterPath = "";
        private string strAppPath = "";
        private string strGlobalDataDir = "";
        private string strLocalDataDir = "";
        private string strBackupPath = string.Empty;
        private bool boolIsHarrisStaff;
        public Guid ClientIdentifier { get; set; }
        public string DataEnvironment { get; set; } = "";
        public string ClientEnvironment { get; set; } = "";
        public string UserName { get; set; } = "";
        public string Password { get; set; } = "";
        public string DataSource { get; set; } = "";
        public string EnvironmentGroup { get; set; } = "Live";
        public string MachineOwnerID { get; set; } = "";
        public string SFTPSiteAddress { get; } = "64.26.137.209";
        public string TrioDataAPIUri { get; set; } = @"https://DataApi.trio-web.com/api";
        public bool IsHarrisStaffComputer
        {
            set
            {
                boolIsHarrisStaff = value;
            }
            get
            {
                bool IsHarrisStaffComputer = false;
                IsHarrisStaffComputer = boolIsHarrisStaff;
                return IsHarrisStaffComputer;
            }
        }
		public string BackupPath
        {
            set
            {
                strBackupPath = value;
            }
            get
            {
                string BackupPath = "";
                BackupPath = strBackupPath;
                return BackupPath;
            }
        }
		public string MasterPath
        {
            get
            {
                string MasterPath = "";
                MasterPath = strMasterPath;
                return MasterPath;
            }
            set
            {
                strMasterPath = value;
            }
        }
		public string ApplicationPath
        {
            get
            {
                string ApplicationPath = "";
                ApplicationPath = strAppPath;
                return ApplicationPath;
            }
            set
            {
                strAppPath = value;
            }
        }
		public string GlobalDataDirectory
        {
            get
            {
                string GlobalDataDirectory = "";
                GlobalDataDirectory = strGlobalDataDir;
                return GlobalDataDirectory;
            }
            set
            {
                strGlobalDataDir = value;
            }
        }
		public string LocalDataDirectory
        {
            get
            {
                string LocalDataDirectory = "";
                LocalDataDirectory = strLocalDataDir;
                return LocalDataDirectory;
            }
            set
            {
                strLocalDataDir = value;
            }
        }

        public bool IsHostedSite { get; set; } = false;
        public cGlobalSettings() : base()
        {
            strMasterPath = "\\TRIOSQLMaster";


            // default to TRIOSQLMaster in the current directory
        }
        
    }
}