﻿namespace SharedApplication.Enums
{
    public enum ComparisonOptionType
    {
        EqualTo = 0,
        GreaterThan = 1,
        LessThan = 2,
        NotEqualTo = 3
    }

    public static class ComparisonOptionTypeExtensions
    {
        public static string ToFriendlyString(this ComparisonOptionType optionType)
        {
            switch (optionType)
            {
                case ComparisonOptionType.EqualTo:
                    return "Equal";
                case ComparisonOptionType.GreaterThan:
                    return "Greater Than";
                case ComparisonOptionType.LessThan:
                    return "Less Than";
                case ComparisonOptionType.NotEqualTo:
                    return "Not Equal";
            }

            return "";
        }

        public static string ToVerboseString(this ComparisonOptionType optionType)
        {
            switch (optionType)
            {
                case ComparisonOptionType.EqualTo:
                    return "Equals";
                case ComparisonOptionType.GreaterThan:
                    return "Is Greater Than";
                case ComparisonOptionType.LessThan:
                    return "Is Less Than";
                case ComparisonOptionType.NotEqualTo:
                    return "Does Not Equal";
            }

            return "";
        }

        public static string ToSymbolString(this ComparisonOptionType optionType)
        {
            switch (optionType)
            {
                case ComparisonOptionType.EqualTo:
                    return "=";
                case ComparisonOptionType.GreaterThan:
                    return ">";
                case ComparisonOptionType.LessThan:
                    return "<";
                case ComparisonOptionType.NotEqualTo:
                    return "<>";
            }

            return "";
        }
    }
}