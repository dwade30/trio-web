﻿namespace SharedApplication.Enums
{
    public enum SearchCriteriaMatchType
    {
        Contains = 0,
        StartsWith = 1,
        EndsWith = 2
    }
}