﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Enums
{
	public enum DeletedOption
	{
		DeletedOnly,
		NotDeletedOnly,
		All
	}
}
