﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Enums
{
	public enum TrioPrograms
	{
		AccountsReceivable,
		Budgetary,
		TaxBilling,
		CodeEnforcement,
		Clerk,
		CashReceipts,
		RealEstateTaxCollections,
		PersonalPropertyTaxCollections,
		FixedAssets,
		MotorVehicle,
		PersonalProperty,
		Payroll,
		RealEstate,
		RedBook,
		UtilityBilling,
		GeneralEntry
	}
}
