﻿namespace SharedApplication
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity> CreateRepository<TEntity>() where TEntity : class;
    }

    public interface IRepositoryFactory<TContextType>
    {
        IRepository<TEntity> CreateRepository<TEntity>() where TEntity : class;
    }
}