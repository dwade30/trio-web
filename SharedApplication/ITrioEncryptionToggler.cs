﻿namespace SharedApplication
{
    public interface ITrioEncryptionToggler
    {
        string ToggleEncryptionCode(string code);
    }
}