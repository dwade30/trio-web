﻿using System;

namespace SharedApplication.Clerk
{
    public interface IPrintVitalsViewModel
    {
        void Cancel();
        void UpdateTransaction(decimal vitalAmount, decimal stateFee);
        void CompleteTransaction();
       // void SetReturnCommand(SharedApplication.Messaging.Command command);
        void AddTransaction(VitalTransaction vitalTransaction);
        event EventHandler Cancelled;
        event EventHandler<VitalTransaction> CompletedTransaction;
    }
}