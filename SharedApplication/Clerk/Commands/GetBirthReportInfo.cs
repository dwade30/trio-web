﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Clerk.Enums;
using SharedApplication.Clerk.Models;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Commands
{
	public class GetBirthReportInfo : Command<IEnumerable<BirthReportInfo>>
	{
		public BirthReportSortOptions SortBy { get; set; } = BirthReportSortOptions.Name;
		public string StartLastName { get; set; } = "";
		public string EndLastName { get; set; } = "";
		public string StartBirthDate { get; set; } = "";
		public string EndBirthDate { get; set; } = "";
		public string StartMothersLastName { get; set; } = "";
		public string EndMothersLastName { get; set; } = "";
		public string StartBirthPlace { get; set; } = "";
		public string EndBirthPlace { get; set; } = "";

	}


}
