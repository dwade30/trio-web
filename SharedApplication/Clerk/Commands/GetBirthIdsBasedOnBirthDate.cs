﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Clerk.Enums;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Commands
{
	public class GetBirthIdsBasedOnBirthDate : Command<IEnumerable<int>>
	{
		public BirthDateType Type { get; set; } = BirthDateType.LiveBirth;
		public DateTime StartDate { get; set; }
		public DateTime? EndDate { get; set; }
	}
}
