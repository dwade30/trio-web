﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Marriages
{
    public class AddMarriageHandler : CommandHandler<AddMarriage>
    {
        private IView<IMarriageViewModel> marriageView;
        public AddMarriageHandler(IView<IMarriageViewModel> viewModel)
        {
             marriageView = viewModel;
        }

        protected override void Handle(AddMarriage command)
        {
            marriageView.ViewModel.VitalId = command.MarriageId;
            marriageView.Show();
        }
    }
}