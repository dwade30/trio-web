﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Marriages
{
    public class MarriageViewModel :VitalViewModel, IMarriageViewModel
    {
        //public int MarriageId { get; set; }

        public MarriageViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
            transactionTypeCode = "MAR";
        }
        //private VitalTransaction currentTransaction;
        //public void Cancel()
        //{
        //    if (Cancelled != null)
        //    {
        //        Cancelled(this, new EventArgs());
        //    }
        //}

        //public void UpdateTransaction(VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    currentTransaction.TypeCode = "MAR";
        //}

        //public void CompleteTransaction()
        //{
        //    if (CompletedTransaction != null)
        //    {
        //        CompletedTransaction(this, currentTransaction);
        //    }
        //}

        //public void PrintVital()
        //{
        //    if (PrintingTransaction != null)
        //    {
        //        PrintingTransaction(this, currentTransaction);
        //    }
        //}

        public bool ChargeFees { get; set; }

        //public event EventHandler Cancelled;
        //public event EventHandler<VitalTransaction> CompletedTransaction;
        //public event EventHandler<VitalTransaction> PrintingTransaction;
    }
}