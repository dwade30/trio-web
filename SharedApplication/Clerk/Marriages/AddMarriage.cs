﻿namespace SharedApplication.Clerk.Marriages
{
    public class AddMarriage : Messaging.Command
    {
        public int MarriageId { get; set; }

        public AddMarriage(int Id)
        {
            MarriageId = Id;
        }
    }
}