﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Marriages
{
    public class ShowMarriageRecordHandler : CommandHandler<ShowMarriageRecord>
    {
        private IView<IMarriageViewModel> marriageView;
        public ShowMarriageRecordHandler(IView<IMarriageViewModel> viewModel)
        {
            marriageView = viewModel;
        }
        protected override void Handle(ShowMarriageRecord command)
        {
            marriageView.ViewModel.VitalId = command.MarriageId;
            marriageView.ViewModel.ChargeFees = command.ChargeFees;
            command.FlowProcessor.Register(marriageView.ViewModel);
            marriageView.Show();
        }
    }
}