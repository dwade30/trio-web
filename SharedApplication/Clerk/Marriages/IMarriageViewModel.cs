﻿using System;

namespace SharedApplication.Clerk.Marriages
{
    public interface IMarriageViewModel : IVitalViewModel
    {
        //int MarriageId { get; set; }
        //void Cancel();
        //void UpdateTransaction(VitalTransaction vitalTransaction);
        //void CompleteTransaction();
        //void PrintVital();
        bool ChargeFees { get; set; } 
        //event EventHandler Cancelled;
        //event EventHandler<VitalTransaction> CompletedTransaction;
        //event EventHandler<VitalTransaction> PrintingTransaction;
    }
}