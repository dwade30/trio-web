﻿using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Marriages
{
    public class MakeMarriageTransactionHandler : CommandHandler<MakeMarriageTransaction>
    {
        private CommandDispatcher commandDispatcher;
        private MarriageTransactionFlowProcessor transactionFlowProcessor;

        public MakeMarriageTransactionHandler(CommandDispatcher commandDispatcher,
            MarriageTransactionFlowProcessor flowProcessor)
        {
            transactionFlowProcessor = flowProcessor;
            this.commandDispatcher = commandDispatcher;
        }
        protected override void Handle(MakeMarriageTransaction command)
        {
            transactionFlowProcessor.CorrelationId = command.CorrelationId;
            transactionFlowProcessor.TransactionId = command.TransactionId;
            transactionFlowProcessor.Search();
        }
    }
}