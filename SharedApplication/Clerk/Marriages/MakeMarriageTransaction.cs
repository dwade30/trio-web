﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Marriages
{
    public class MakeMarriageTransaction : Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }

        public MakeMarriageTransaction(Guid correlationId, Guid transactionId)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
        }
    }
}