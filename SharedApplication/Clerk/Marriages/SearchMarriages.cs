﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Marriages
{
    public class SearchMarriages : SharedApplication.Messaging.Command
    {
        public SearchMarriages(MarriageTransactionFlowProcessor flowProcessor)
        {
            FlowProcessor = flowProcessor;
        }
        public MarriageTransactionFlowProcessor FlowProcessor { get; set; }
    }
}