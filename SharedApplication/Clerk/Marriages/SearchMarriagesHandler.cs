﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Marriages
{
    public class SearchMarriagesHandler : CommandHandler<SearchMarriages>
    {
        private IView<IMarriageSearchViewModel> searchView;

        public SearchMarriagesHandler(IView<IMarriageSearchViewModel> marriageView)
        {
            this.searchView = marriageView;
        }
        protected override void Handle(SearchMarriages command)
        {
            command.FlowProcessor.Register(searchView.ViewModel);
            searchView.Show();
        }
    }
}