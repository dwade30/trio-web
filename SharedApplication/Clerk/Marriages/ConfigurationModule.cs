﻿using Autofac;

namespace SharedApplication.Clerk.Marriages
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MarriageViewModel>().As<IMarriageViewModel>();
            builder.RegisterType<MarriageSearchViewModel>().As<IMarriageSearchViewModel>();
        }
    }
}