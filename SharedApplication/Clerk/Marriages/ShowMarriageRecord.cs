﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Marriages
{
    public class ShowMarriageRecord : Messaging.Command
    {
        public int MarriageId { get; set; }
        public MarriageTransactionFlowProcessor FlowProcessor { get; set; }
        public bool ChargeFees { get; set; }
        public ShowMarriageRecord(int Id, MarriageTransactionFlowProcessor flowProcessor, bool chargeFees )
        {
            MarriageId = Id;
            this.FlowProcessor = flowProcessor;
            ChargeFees = chargeFees;
        }
    }
}