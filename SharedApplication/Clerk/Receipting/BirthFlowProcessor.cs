﻿using System;
using SharedApplication.Clerk.Births;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public class BirthFlowProcessor : VitalFlowProcessor, BirthTransactionFlowProcessor
    {
        //public Guid CorrelationId { get; set; }
        //public Guid TransactionId { get; set; }

        private IBirthSearchViewModel birthSearchViewModel;
        private IBirthViewModel birthViewModel;
        //private IPrintVitalsViewModel printViewModel;
       // private VitalTransaction currentTransaction = new VitalTransaction(){TypeCode = "BIR"};
       
       // private int birthId = 0;
        public BirthFlowProcessor(CommandDispatcher commandDispatcher, EventPublisher eventPublisher)
        {
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;
            currentTransaction = new VitalTransaction() { TypeCode = "BIR" };
        }

        //public void Register(IBirthSearchViewModel viewModel)
        //{
        //    if (viewModel != null)
        //    {
        //        birthSearchViewModel = viewModel;
        //        birthSearchViewModel.Cancelled += BirthSearchViewModel_Cancelled;
        //        birthSearchViewModel.Selected += BirthSearchViewModel_SelectedLiveBirth;
        //    }
        //}

        //private void BirthSearchViewModel_SelectedLiveBirth(object sender, int birthId)
        //{
        //    commandDispatcher.Send(new ShowLiveBirth(birthId,this) );
        //}

        //public void Register(IBirthViewModel viewModel)
        //{
        //    if (viewModel != null)
        //    {
        //        birthViewModel = viewModel;
        //        birthId = birthViewModel.BirthId;
        //        birthViewModel.Cancelled += BirthViewModel_Cancelled;
        //        birthViewModel.CompletedTransaction += BirthViewModel_CompletedTransaction;
        //        birthViewModel.PrintingTransaction += BirthViewModel_PrintingTransaction;
        //    }
        //}

        //public void Register(IPrintVitalsViewModel viewModel)
        //{
        //    printViewModel = viewModel;
        //    printViewModel.Cancelled += PrintViewModel_Cancelled;
        //    printViewModel.CompletedTransaction += PrintViewModel_CompletedTransaction;
        //}

        //private void PrintViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
        //    clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
        //    clerkVitalTransaction.Id = TransactionId;
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        eventPublisher.Publish(new TransactionCompleted()
        //            { CorrelationId = CorrelationId, CompletedTransaction = clerkVitalTransaction });
        //    }
        //}

        //private void PrintViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    commandDispatcher.Send(new ShowLiveBirth(birthId, this));
        //}

        protected override void ShowSearch()
        {
            SearchForBirth();
        }

        protected override void ShowVital()
        {
            commandDispatcher.Send(new ShowLiveBirth(vitalId, this));
        }

        //private void BirthViewModel_PrintingTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    commandDispatcher.Send(
        //        new ShowPrintVital(vitalTransaction,this));
        //}

        //private void BirthViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
        //    clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
        //    clerkVitalTransaction.Id = TransactionId;
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        eventPublisher.Publish(new TransactionCompleted()
        //            {CorrelationId = CorrelationId, CompletedTransaction = clerkVitalTransaction});
        //    }
        //}

        public override void Search()
        {
            SearchForBirth();
        }

        //private void BirthViewModel_Cancelled(object sender, EventArgs e)
        //{
        //   SearchForBirth();
        //}

        //private void BirthSearchViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    CancelTransaction();
        //}

        //private void CancelTransaction()
        //{
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        commandDispatcher.Send(new MakeClerkTransaction() {CorrelationId = CorrelationId});
        //    }
        //}

        private void SearchForBirth()
        {
            commandDispatcher.Send(new SearchBirths() { FlowProcessor = this });
        }
    }
}