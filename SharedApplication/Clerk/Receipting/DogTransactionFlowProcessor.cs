﻿using System;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Receipting
{
    public interface DogTransactionFlowProcessor : IDogOwnerSearchViewModel, TransactionFlowProcessor
    {
        void Register(IDogOwnerViewModel dogOwnerViewModel);
    }
}