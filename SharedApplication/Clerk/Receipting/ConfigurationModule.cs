﻿using System;
using Autofac;
using SharedApplication.CashReceipts;

namespace SharedApplication.Clerk.Receipting
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ChooseClerkTransactionTypeViewModel>().As<IChooseClerkTransactionTypeViewModel>();
            builder.RegisterType<DogTransactionProcessor>().As<DogTransactionFlowProcessor>();
            builder.RegisterType<BirthFlowProcessor>().As<BirthTransactionFlowProcessor>();
            builder.RegisterType<DeathFlowProcessor>().As<DeathTransactionFlowProcessor>();
            builder.RegisterType< MarriageFlowProcessor>().As<MarriageTransactionFlowProcessor>();
            builder.RegisterType<BurialFlowProcessor>().As<BurialTransactionFlowProcessor>();
        }

    }
}