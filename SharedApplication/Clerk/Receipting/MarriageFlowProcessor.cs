﻿using System;
using SharedApplication.Clerk.Marriages;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public class MarriageFlowProcessor : VitalFlowProcessor,  MarriageTransactionFlowProcessor
    {
        //public Guid CorrelationId { get; set; }
        //public Guid TransactionId { get; set; }

        //private IPrintVitalsViewModel printViewModel;
        //private IMarriageSearchViewModel searchViewModel;
        //private IMarriageViewModel marriageViewModel;
        //private int marriageId;
        //private CommandDispatcher commandDispatcher;
        //private VitalTransaction currentTransaction;
        //private EventPublisher eventPublisher;

        public MarriageFlowProcessor(CommandDispatcher commandDispatcher, EventPublisher eventPublisher)
        {
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;
        }
        //public void Register(IPrintVitalsViewModel viewModel)
        //{
        //    printViewModel = viewModel;
        //    printViewModel.Cancelled += PrintViewModel_Cancelled;
        //    printViewModel.CompletedTransaction += PrintViewModel_CompletedTransaction;
        //}

        //private void PrintViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
        //    clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
        //    clerkVitalTransaction.Id = TransactionId;
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        eventPublisher.Publish(new TransactionCompleted()
        //            { CorrelationId = CorrelationId, CompletedTransaction = clerkVitalTransaction });
        //    }
        //}

        //private void PrintViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    commandDispatcher.Send(new ShowMarriageRecord(marriageId, this, CorrelationId != Guid.Empty));
        //}

        public override void Search()
        {
            commandDispatcher.Send(new SearchMarriages(this));
        }

        //public void Register(IMarriageSearchViewModel viewModel)
        //{
        //    searchViewModel = viewModel;
        //    searchViewModel.Cancelled += SearchViewModel_Cancelled;
        //    searchViewModel.SelectedMarriageRecord += SearchViewModel_SelectedMarriageRecord;
        //}

        //private void SearchViewModel_SelectedMarriageRecord(object sender, int Id)
        //{
        //    marriageId = Id;
        //    commandDispatcher.Send(new ShowMarriageRecord(Id, this, CorrelationId != Guid.Empty));
        //}

        protected override void ShowSearch()
        {
            Search();
        }

        protected override void ShowVital()
        {
            commandDispatcher.Send(new ShowMarriageRecord(vitalId, this, CorrelationId != Guid.Empty));
        }

        //private void SearchViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    CancelTransaction();
        //}

        //private void CancelTransaction()
        //{
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        commandDispatcher.Send(new MakeClerkTransaction() { CorrelationId = CorrelationId });
        //    }
        //}
        //public void Register(IMarriageViewModel viewModel)
        //{
        //    marriageViewModel = viewModel;
        //    marriageId = viewModel.MarriageId;
        //    marriageViewModel.Cancelled += MarriageViewModel_Cancelled;
        //    marriageViewModel.CompletedTransaction += MarriageViewModel_CompletedTransaction;
        //    marriageViewModel.PrintingTransaction += MarriageViewModel_PrintingTransaction;
        //}

        //private void MarriageViewModel_PrintingTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    commandDispatcher.Send(
        //        new ShowPrintVital(vitalTransaction, this));
        //}

        //private void MarriageViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
        //    clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
        //    clerkVitalTransaction.Id = TransactionId;
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        eventPublisher.Publish(new TransactionCompleted()
        //            { CorrelationId = CorrelationId, CompletedTransaction = clerkVitalTransaction });
        //    }
        //}

        //private void MarriageViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    SearchForMarriageRecord();
        //}

        //private void SearchForMarriageRecord()
        //{
        //    commandDispatcher.Send(new SearchMarriages(this));
        //}
    }
}