﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using MediatR;
using SharedApplication.CentralParties;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Receipting
{
    public class DogTransactionProcessor : DogTransactionFlowProcessor
    {
        private EventPublisher eventPublisher;
        private IClerkRepositoryFactory clerkRepositoryFactory;
        private IEditableRepository<ClerkTransaction, Guid> transactionRepository;
        private CommandDispatcher commandDispatcher;
        private IDogOwnerSearchViewModel searchViewModel;
        private IDogOwnerViewModel ownerViewModel;

        public event EventHandler<int> OwnerSelected;
        public event EventHandler Cancelled;
        public event EventHandler<int> UndeleteOwnerHandler;

        public bool ProcessCancelled { get; protected set; } = false;
        public Guid CorrelationId { get;  set; }
        public Guid TransactionId { get; set; }



        public DogTransactionProcessor(IClerkRepositoryFactory clerkRepositoryFactory, CommandDispatcher commandDispatcher, IDogOwnerSearchViewModel searchViewModel, EventPublisher eventPublisher, IEditableRepository<ClerkTransaction, Guid> transactionRepository)
        {
            this.clerkRepositoryFactory = clerkRepositoryFactory;
            this.commandDispatcher = commandDispatcher;
            this.searchViewModel = searchViewModel;
            this.eventPublisher = eventPublisher;
            this.transactionRepository = transactionRepository;

            searchViewModel.UndeleteOwnerHandler += SearchViewModel_UndeleteOwner;
        }

        public void Register(IDogOwnerViewModel dogOwnerViewModel)
        {
            ownerViewModel = dogOwnerViewModel;
            ownerViewModel.Cancelled += OwnerViewModel_Cancelled;
            ownerViewModel.CompletedTransaction += OwnerViewModel_CompletedTransaction;           
        }

        private void OwnerViewModel_CompletedTransaction(object sender, DogOwnerTransaction dogTransaction)
        {
            var currTransaction = transactionRepository.Get(TransactionId);
            if (currTransaction == null)
            {
                currTransaction = new ClerkTransaction(){CorrelationIdentifier = CorrelationId, Id = TransactionId, TransactionTypeCode = "DOG", PayerName = dogTransaction.TransactionPerson, EffectiveDateTime = dogTransaction.TransactionDate, ActualDateTime = dogTransaction.TransactionDate};
                transactionRepository.Add(currTransaction);
            }

            currTransaction.TransactionTypeCode = "DOG";
            currTransaction.Reference = dogTransaction.Description;
            currTransaction.IncludeInCashDrawerTotal = true;

            currTransaction.AddTransactionAmounts(dogTransaction.ToTransactionAmountItems());
            eventPublisher.Publish(new TransactionCompleted()
            {
	            CorrelationId = CorrelationId,
	            CompletedTransactions = new List<TransactionBase>
				{
					currTransaction
				}
            });
            transactionRepository.Remove(TransactionId);
          
        }

        private void OwnerViewModel_Cancelled(object sender, EventArgs e)
        {
            var searchCommand = new SearchDogOwner(this);
            commandDispatcher.Send(searchCommand);
        }

        private void SearchViewModel_UndeleteOwner(object sender, int ownerId)
		{
			UndeleteOwner(ownerId);
		}

		public DogSearchCriteria SearchCriteria
        {
            get { return searchViewModel.SearchCriteria; }
        }

        public IEnumerable<GenericDescriptionPair<DogSearchBy>> SearchByOptions
        {
            get { return searchViewModel.SearchByOptions; }
        }
        public IEnumerable<GenericDescriptionPair<DogSearchRecordType>> DogSearchRecordOptions
        {
            get { return searchViewModel.DogSearchRecordOptions; }
        }

        public IEnumerable<GenericDescriptionPair<SearchCriteriaMatchType>> CriteriaMatchOptions
        {
            get { return searchViewModel.CriteriaMatchOptions; }
        } 

         void IDogOwnerSearchViewModel.Search()
        {
            Search();

        }

         public void Search()
         {
            searchViewModel.Search();
            if (searchViewModel.SearchResults().Any())
            {
                ShowResults();
            }
        }

        private void ShowResults()
        {

        }

        IEnumerable<DogSearchResult> IDogOwnerSearchViewModel.SearchResults()
        {
            return searchViewModel.SearchResults();
        }

        public void AddNewOwner()
        {
            commandDispatcher.Send(new ShowDogOwner(0, this));
        }

        void IDogOwnerSearchViewModel.Cancel()
        {
            searchViewModel.Cancel();
            eventPublisher.Publish(new TransactionCancelled(CorrelationId,null));
        }

        public void SelectOwner(int id)
        {
	        if (OwnerSelected != null)
	        {
		        OwnerSelected(this, id);
	        }
            commandDispatcher.Send(new ShowDogOwner(id, this));
        }

        public void UndeleteOwner(int id)
        {
	        commandDispatcher.Send(new UndeleteDogOwner
	        {
		        OwnerId = id
	        });
        }
    }

    public static class DogTransactionExtensions
    {
        public static IEnumerable<TransactionAmountItem> ToTransactionAmountItems(this DogTransaction dogTransaction)
        {
            var amountItems = new List<TransactionAmountItem>();
            if (dogTransaction.TownFee.GetValueOrDefault() != 0)
            {
                amountItems.Add(new TransactionAmountItem()
                {
                    Description = "Town Fee",
                    Name = "Town Fee",
                    Total = Convert.ToDecimal(dogTransaction.TownFee.GetValueOrDefault())
                });
            }

            if (dogTransaction.StateFee.GetValueOrDefault() != 0)
            {
                amountItems.Add(new TransactionAmountItem()
                {
                    Description = "State Fee",
                    Name = "State Fee",
                    Total = Convert.ToDecimal(dogTransaction.StateFee.GetValueOrDefault())

                });
            }

            if (dogTransaction.ClerkFee.GetValueOrDefault() != 0)
            {
                amountItems.Add(new TransactionAmountItem()
                {
                    Description = "Clerk Fee",
                    Name = "Clerk Fee",
                    Total = Convert.ToDecimal(dogTransaction.ClerkFee.GetValueOrDefault())
                });
            }

            if (dogTransaction.LateFee.GetValueOrDefault() != 0)
            {
                amountItems.Add(new TransactionAmountItem()
                {
                    Description = "Late Fee",
                    Name = "Late Fee",
                    Total = Convert.ToDecimal(dogTransaction.LateFee.GetValueOrDefault())
                });
            }

            if (dogTransaction.AnimalControl.GetValueOrDefault() != 0)
            {
                amountItems.Add(new TransactionAmountItem()
                {
                    Description = "Misc Fee",
                    Name = "Misc Fee",
                    Total = Convert.ToDecimal(dogTransaction.AnimalControl.GetValueOrDefault())
                });
            }
            return amountItems;
        }

        public static IEnumerable<TransactionAmountItem> ToTransactionAmountItems(this DogOwnerTransaction transaction)
        {
            List<TransactionAmountItem> amountItems = new List<TransactionAmountItem>();
            if (transaction != null && transaction.DogTransactions.Any())
            {
                foreach (var dogTransaction in transaction.DogTransactions)
                {
                    amountItems.AddRange(dogTransaction.ToTransactionAmountItems());
                }
            }

            var groupData = amountItems.GroupBy(x => new {x.Description, x.Name});
            List<TransactionAmountItem> result = new List<TransactionAmountItem>();

            foreach (var amountTotal in groupData)
            {
                result.Add(new TransactionAmountItem
                {
                    Name = amountTotal.Key.Name,
                    Description = amountTotal.Key.Description,
                    Total = amountTotal.Sum(x => x.Total)
                });
            }
            
            return result;
        }
    }
}