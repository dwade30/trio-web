﻿using System;
using SharedApplication.Clerk.Deaths;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public class DeathFlowProcessor : VitalFlowProcessor, DeathTransactionFlowProcessor
    {
        //public Guid CorrelationId { get; set; }
        //public Guid TransactionId { get; set; }

        
        //public void Register(IPrintVitalsViewModel viewModel)
        //{
        //    printViewModel = viewModel;
        //    printViewModel.Cancelled += PrintViewModel_Cancelled;
        //    printViewModel.CompletedTransaction += PrintViewModel_CompletedTransaction;
        //}

        //private void PrintViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
        //    clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
        //    clerkVitalTransaction.Id = TransactionId;
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        eventPublisher.Publish(new TransactionCompleted()
        //            { CorrelationId = CorrelationId, CompletedTransaction = clerkVitalTransaction });
        //    }
        //}

        //private void PrintViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    commandDispatcher.Send(new ShowDeathRecord(deathId, this));
        //}

        //private int deathId;
        //private IDeathSearchViewModel searchViewModel;
        //private IDeathViewModel deathViewModel;
        //private IPrintVitalsViewModel printViewModel;
        //private CommandDispatcher commandDispatcher;
        //private VitalTransaction currentTransaction = new VitalTransaction() { TypeCode = "DEA" };
        //private EventPublisher eventPublisher;
        public DeathFlowProcessor(CommandDispatcher commandDispatcher, EventPublisher eventPublisher)
        {
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;            
        }
        public override void Search()
        {
            SearchForDeathRecord();
        }

        protected override void ShowSearch()
        {
            SearchForDeathRecord();
        }

        protected override void ShowVital()
        {
            commandDispatcher.Send(new ShowDeathRecord(vitalId, this));
        }

        //public void Register(IDeathSearchViewModel viewModel)
        //{
        //    searchViewModel = viewModel;
        //    searchViewModel.SelectedDeathRecord += SearchViewModel_SelectedDeathRecord;
        //    searchViewModel.Cancelled += SearchViewModel_Cancelled;
        //}

        //private void SearchViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    CancelTransaction();
        //}

        //private void CancelTransaction()
        //{
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        commandDispatcher.Send(new MakeClerkTransaction() { CorrelationId = CorrelationId });
        //    }
        //}

        //private void SearchViewModel_SelectedDeathRecord(object sender, int deathId)
        //{
        //    commandDispatcher.Send(new ShowDeathRecord(deathId, this));
        //}

        //public void Register(IDeathViewModel viewModel)
        //{
        //    deathViewModel = viewModel;
        //    deathId = deathViewModel.DeathId;
        //    deathViewModel.Cancelled += DeathViewModel_Cancelled;
        //    deathViewModel.CompletedTransaction += DeathViewModel_CompletedTransaction;
        //    deathViewModel.PrintingTransaction += DeathViewModel_PrintingTransaction;
        //}

        //private void DeathViewModel_PrintingTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    commandDispatcher.Send(
        //        new ShowPrintVital(vitalTransaction, this));
        //}

        //private void DeathViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
        //    clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
        //    clerkVitalTransaction.Id = TransactionId;
        //    if (CorrelationId != Guid.Empty)
        //    {
        //        eventPublisher.Publish(new TransactionCompleted()
        //            { CorrelationId = CorrelationId, CompletedTransaction = clerkVitalTransaction });
        //    }
        //}

        //private void DeathViewModel_Cancelled(object sender, EventArgs e)
        //{
        //    SearchForDeathRecord();
        //}

        private void SearchForDeathRecord()
        {
            commandDispatcher.Send(new SearchDeaths(this));
        }
    }
}