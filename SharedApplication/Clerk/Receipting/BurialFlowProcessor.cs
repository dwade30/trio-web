﻿using System;
using SharedApplication.Clerk.Burials;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Receipting
{
    public class BurialFlowProcessor : VitalFlowProcessor, BurialTransactionFlowProcessor
    {
        public BurialFlowProcessor(CommandDispatcher commandDispatcher, EventPublisher eventPublisher)
        {
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;
            
        }


        public override void Search()
        {
            commandDispatcher.Send(new SearchBurials(this));
        }

        protected override void ShowSearch()
        {
            Search();
        }

        protected override void ShowVital()
        {
            commandDispatcher.Send(new ShowBurial(vitalId, this, CorrelationId != Guid.Empty));
        }
    }
}