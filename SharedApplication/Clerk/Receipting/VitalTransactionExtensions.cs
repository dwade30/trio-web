﻿using System;
using System.Collections.Generic;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public static class VitalTransactionExtensions
    {
        public static IEnumerable<TransactionAmountItem> ToTransactionAmountItems(this VitalTransaction transaction)
        {
            return new List<TransactionAmountItem>()
            {
                new TransactionAmountItem(){Name = "Amount1", Total = transaction.VitalAmount,Description = "Vital Records"},
                new TransactionAmountItem(){Name="Amount2", Total = transaction.StateFee,Description = "State Fee"}
            };
        }

        public static ClerkVitalTransaction ToClerkVitalTransaction(this VitalTransaction transaction)
        {
            var clerkTransaction = new ClerkVitalTransaction();
            clerkTransaction.PayerName = transaction.Name;
            clerkTransaction.Reference = transaction.Reference;
            //clerkTransaction.Controls.Add();
			clerkTransaction.ActualDateTime = DateTime.Today;
			clerkTransaction.EffectiveDateTime = DateTime.Today;
            clerkTransaction.TransactionTypeCode = transaction.TypeCode;
            clerkTransaction.AddTransactionAmounts(transaction.ToTransactionAmountItems());
            return clerkTransaction;
        }
    }
}