﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Receipting
{
    public class MakeClerkTransaction : SharedApplication.Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }

    }
}