﻿using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public class FinalizeClerkTransactionHandler : CommandHandler<FinalizeTransactions<ClerkTransaction>,bool>
    {
        protected override bool Handle(FinalizeTransactions<ClerkTransaction> command)
        {
            return true;
        }
    }
}