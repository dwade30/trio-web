﻿using System.Threading;
using SharedApplication.Clerk.Marriages;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Receipting
{
    public interface VitalTransactionFlowProcessor : TransactionFlowProcessor
    {
        void Register(IPrintVitalsViewModel viewModel);
        void Register(IVitalViewModel viewModel);
        void Register(IVitalSearchViewModel viewModel);
    }
}