﻿using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public class FinalizeVitalTransactionHandler : CommandHandler<FinalizeTransactions<ClerkVitalTransaction>,bool>
    {
        protected override bool Handle(FinalizeTransactions<ClerkVitalTransaction> command)
        {
            return true;
        }
    }
}