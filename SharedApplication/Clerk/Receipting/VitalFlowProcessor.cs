﻿using System;
using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public abstract class VitalFlowProcessor : VitalTransactionFlowProcessor
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }
        public abstract void Search();
       

        protected IPrintVitalsViewModel printViewModel;
        protected IVitalSearchViewModel searchViewModel;
        protected IVitalViewModel vitalViewModel;
        protected VitalTransaction currentTransaction;
        protected CommandDispatcher commandDispatcher;
        protected EventPublisher eventPublisher;
        protected int vitalId;
        public void Register(IPrintVitalsViewModel viewModel)
        {
            printViewModel = viewModel;
            printViewModel.Cancelled += PrintViewModel_Cancelled;
            printViewModel.CompletedTransaction += PrintViewModel_CompletedTransaction;
        }

        public void Register(IVitalSearchViewModel viewModel)
        {
            searchViewModel = viewModel;
            searchViewModel.Cancelled += SearchViewModel_Cancelled;
            searchViewModel.Selected += SearchViewModel_Selected;
        }

        private void SearchViewModel_Selected(object sender, int Id)
        {
            vitalId = Id;
           ShowVital();
        }

        private void SearchViewModel_Cancelled(object sender, EventArgs e)
        {
            CancelTransaction();
        }

        protected void CancelTransaction()
        {
            if (CorrelationId != Guid.Empty)
            {
                //go back to CR or go back to clerk transaction type screen?
                commandDispatcher.Send(new MakeClerkTransaction() { CorrelationId = CorrelationId });
                //eventPublisher.Publish(new TransactionCancelled(CorrelationId, null));
            }
        }

        public void Register(IVitalViewModel viewModel)
        {
            vitalViewModel = viewModel;
            vitalViewModel.Cancelled += VitalViewModel_Cancelled;
            vitalViewModel.CompletedTransaction += VitalViewModel_CompletedTransaction;
            vitalViewModel.PrintingTransaction += VitalViewModel_PrintingTransaction;
        }

        private void VitalViewModel_PrintingTransaction(object sender, VitalTransaction vitalTransaction)
        {
            commandDispatcher.Send(
                   new ShowPrintVital(vitalTransaction,this));
        }

        private void VitalViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        {
           CompleteTransaction(vitalTransaction);
        }

        private void VitalViewModel_Cancelled(object sender, EventArgs e)
        {
            ShowSearch();
        }

        private void PrintViewModel_CompletedTransaction(object sender, VitalTransaction vitalTransaction)
        {
            CompleteTransaction(vitalTransaction);
        }

        private void PrintViewModel_Cancelled(object sender, EventArgs e)
        {
            ShowVital();
        }

        protected abstract void ShowSearch();

        protected abstract void ShowVital();

        protected void CompleteTransaction(VitalTransaction vitalTransaction)
        {
            currentTransaction = vitalTransaction;
            var clerkVitalTransaction = currentTransaction.ToClerkVitalTransaction();
            clerkVitalTransaction.CorrelationIdentifier = CorrelationId;
            clerkVitalTransaction.Id = TransactionId;
            clerkVitalTransaction.IncludeInCashDrawerTotal = true;
            if (CorrelationId != Guid.Empty)
            {
                eventPublisher.Publish(new TransactionCompleted
                {
	                CorrelationId = CorrelationId,
	                CompletedTransactions = new List<TransactionBase>
	                {
		                clerkVitalTransaction
					} 
                });
            }
        }
    }
}