﻿using System;
using SharedApplication.Clerk.Dogs;

namespace SharedApplication.Clerk.Receipting
{
    public interface IDogTransactionViewModel : IDogOwnerSearchViewModel, ISearchResultsDogViewModel
    {
        Guid CorrelationId { get; set; }
        Guid TransactionId { get; set; }        
    }
}