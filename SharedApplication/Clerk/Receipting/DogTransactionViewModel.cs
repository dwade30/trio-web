﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using MediatR;
using SharedApplication.CentralParties;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.Clerk.Receipting
{
    public class DogTransactionViewModel : IDogTransactionViewModel 
    {
        private EventPublisher eventPublisher;
        private IClerkRepositoryFactory clerkRepositoryFactory;
        private CommandDispatcher commandDispatcher;
        private IDogOwnerSearchViewModel searchViewModel;
        public bool ProcessCancelled { get; protected set; } = false;
        public Guid CorrelationId { get;  set; }
        public Guid TransactionId { get; set; }

        public DogSearchCriteria SearchCriteria
        {
            get { return searchViewModel.SearchCriteria; }
        }

        public IEnumerable<DescriptionEnumPair<DogSearchBy>> SearchByOptions
        {
            get { return searchViewModel.SearchByOptions; }
        }
        public IEnumerable<DescriptionEnumPair<DogSearchRecordType>> DogSearchRecordOptions
        {
            get { return searchViewModel.DogSearchRecordOptions; }
        }

        public IEnumerable<DescriptionEnumPair<SearchCriteriaMatchType>> CriteriaMatchOptions
        {
            get { return searchViewModel.CriteriaMatchOptions; }
        } 

        

        public DogTransactionViewModel(IClerkRepositoryFactory clerkRepositoryFactory, CommandDispatcher commandDispatcher, IDogOwnerSearchViewModel searchViewModel,EventPublisher eventPublisher)
        {
            this.clerkRepositoryFactory = clerkRepositoryFactory;
            this.commandDispatcher = commandDispatcher;
            this.searchViewModel = searchViewModel;
            this.eventPublisher = eventPublisher;
        }

        void IDogOwnerSearchViewModel.Search()
        {
            searchViewModel.Search();
        }

        IEnumerable<DogSearchResult> IDogOwnerSearchViewModel.SearchResults()
        {
            return searchViewModel.SearchResults();
        }

        void IDogOwnerSearchViewModel.Cancel()
        {
            searchViewModel.Cancel();
            eventPublisher.Publish(new TransactionCancelled(){CorrelationId =  CorrelationId});
        }
    }

    public class DogSearchViewModel : IDogOwnerSearchViewModel
    {
        public DogSearchCriteria SearchCriteria { get; } = new DogSearchCriteria();
        private List<DogSearchResult> searchResults = new List<DogSearchResult>();
       // private IClerkRepositoryFactory clerkRepositoryFactory;
       private IQueryHandler<DogSearchCriteria, IEnumerable<DogSearchResult>> dogQueryHandler;
        private ICentralPartyRepositoryFactory partyRepositoryFactory;
        public IEnumerable<DescriptionEnumPair<DogSearchBy>> SearchByOptions { get; protected set; }
        public IEnumerable<DescriptionEnumPair<DogSearchRecordType>> DogSearchRecordOptions { get; protected set; }

        public IEnumerable<DescriptionEnumPair<SearchCriteriaMatchType>> CriteriaMatchOptions { get; protected set; }

        //public DogSearchViewModel( ICentralPartyRepositoryFactory partyRepositoryFactory)
        public DogSearchViewModel(IQueryHandler<DogSearchCriteria, IEnumerable<DogSearchResult>> dogQueryHandler, ICentralPartyRepositoryFactory partyRepositoryFactory)
        {
            this.partyRepositoryFactory = partyRepositoryFactory;
            this.dogQueryHandler = dogQueryHandler;        
            SetSearchByOptions();
            SetSearchRecordOptions();
            SetCriteriaMatchOptions();
        }

        private void SetSearchByOptions()
        {
            SearchByOptions = new List<DescriptionEnumPair<DogSearchBy>>()
            {
               
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.OwnerName, Description = "Owner Name"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.City, Description = "City"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.Location, Description = "Location"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.KennelLicense, Description = "Kennel License"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.DogName,Description = "Dog Name"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.Breed, Description =  "Breed"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.LicenseTagNumber, Description = "License Tag Number"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.RabiesTagNumber, Description =   "Rabies Tag Number"},
               new DescriptionEnumPair<DogSearchBy>(){ID = DogSearchBy.Veterinarian, Description =  "Veterinarian"}
            };
        }

        private void SetSearchRecordOptions()
        {
            DogSearchRecordOptions = new List<DescriptionEnumPair<DogSearchRecordType>>()
            {
                new DescriptionEnumPair<DogSearchRecordType>(){ID = DogSearchRecordType.All, Description = "All"},
                new DescriptionEnumPair<DogSearchRecordType>(){ID = DogSearchRecordType.Dog,Description = "Dogs"},
                new DescriptionEnumPair<DogSearchRecordType>(){ID = DogSearchRecordType.DogsInKennels, Description = "Dogs In Kennels"}
            };
        }

        private void SetCriteriaMatchOptions()
        {
            CriteriaMatchOptions = new List<DescriptionEnumPair<SearchCriteriaMatchType>>()
            {
                new DescriptionEnumPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.Contains, Description =  "Contains Search Criteria"},
                new DescriptionEnumPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.StartsWith, Description =  "Starts With Search Criteria"},
                new DescriptionEnumPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.EndsWith, Description =  "Ends With Search Criteria"}
            };
        }

        public void Search()
        {
            searchResults = dogQueryHandler.ExecuteQuery(SearchCriteria).ToList();
        }

        public IEnumerable<DogSearchResult> SearchResults()
        {
            return searchResults;
        }

        public void Cancel()
        {
 
        }
    }
}