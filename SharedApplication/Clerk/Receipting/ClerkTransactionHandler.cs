﻿using System;
using System.Linq;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Receipting
{
    public class ClerkTransactionHandler : CommandHandler<MakeClerkTransaction>
    {
        private IView<IChooseClerkTransactionTypeViewModel> choiceView;
        public ClerkTransactionHandler(IView<IChooseClerkTransactionTypeViewModel> view)
        {
            choiceView = view;
        }
        protected override void Handle(MakeClerkTransaction command)
        {            
            choiceView.ViewModel.SetCorrelationId(command.CorrelationId);
            var types = choiceView.ViewModel.GetAvailableTransactionTypes();

            if (types.Count() > 1)
            {
	            choiceView.Show();
            }
            else
            {
	            choiceView.ViewModel.StartTransaction((ClerkTransactionType)(types.FirstOrDefault().ID));
            }
        }
    }
}