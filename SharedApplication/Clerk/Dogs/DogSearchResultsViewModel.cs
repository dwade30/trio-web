﻿using System;
using System.Collections.Generic;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Dogs
{
    public class DogSearchResultsViewModel : ISearchResultsDogViewModel
    {

        public void SelectOwner(int id)
        {
            if (OwnerSelected != null)
            {
                OwnerSelected(this, id);
            }
        }

        public void UndeleteOwner(int id)
        {
	        UndeleteOwnerHandler(this, id);
        }

        public void Cancel()
        {
            Cancelled(this,new EventArgs());
        }

        public IEnumerable<DogSearchResult> SearchResults { get; set; }

        public DogSearchCriteria SearchCriteria { get; set; }
        public event EventHandler<int> OwnerSelected;
        public event EventHandler Cancelled;
        public event EventHandler<int> UndeleteOwnerHandler;
	}
}