﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Dogs
{
    public class ShowDogOwnerHandler : CommandHandler<ShowDogOwner>
    {
        private IView<IDogOwnerViewModel> ownerView;
        public ShowDogOwnerHandler(IView<IDogOwnerViewModel> view)
        {
            ownerView = view;
        }
        protected override void Handle(ShowDogOwner command)
        {
            ownerView.ViewModel.GetDogOwner(command.OwnerId);
            command.FlowProcessor.Register(ownerView.ViewModel);
            ownerView.Show();
        }
    }
}