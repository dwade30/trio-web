﻿using System;
using MediatR;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk
{
    public class MakeDogTransactionHandler : CommandHandler<MakeDogTransaction>
    {
        private IEditableRepository<ClerkTransaction, Guid> transactionRepository;
        private CommandDispatcher commandDispatcher;
        private DogTransactionFlowProcessor transactionViewModel;
        public MakeDogTransactionHandler(IEditableRepository<ClerkTransaction, Guid> transactionRepository, CommandDispatcher commandDispatcher, DogTransactionFlowProcessor transactionViewModel)

        {
            this.transactionRepository = transactionRepository;
            this.commandDispatcher = commandDispatcher;
            this.transactionViewModel = transactionViewModel;
        }
        protected override void Handle(MakeDogTransaction command)
        {
            transactionViewModel.CorrelationId = command.CorrelationId;
            transactionViewModel.TransactionId = command.TransactionId;
            var searchCommand = new SearchDogOwner(transactionViewModel);
            commandDispatcher.Send(searchCommand);
            
        }
    }
}