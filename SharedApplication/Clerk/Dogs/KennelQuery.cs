﻿namespace SharedApplication.Clerk.Dogs
{
    public class KennelQuery
    {
        public int Id { get; set; }
        public KennelQuery(int Id)
        {
            this.Id = Id;
        }
    }
}