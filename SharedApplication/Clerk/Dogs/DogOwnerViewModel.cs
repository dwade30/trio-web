﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CentralParties;
using SharedApplication.CentralParties.Models;
using SharedApplication.Clerk.Models;

namespace SharedApplication.Clerk.Dogs
{
    public class DogOwnerViewModel : IDogOwnerViewModel
    {
        private IClerkRepositoryFactory clerkRepositoryFactory;
        private ICentralPartyRepositoryFactory partyRepositoryFactory;
        private DogOwner currentDogOwner;
        private DogOwnerTransaction currentTransaction = new DogOwnerTransaction();
        private IQueryHandler<DogQuery, DogInfo> dogQueryHandler;
        private IQueryHandler<KennelQuery, KennelLicense> kennelQueryHandler;
        public DogOwnerViewModel(IClerkRepositoryFactory clerkRepositoryFactory, ICentralPartyRepositoryFactory centralPartyRepositoryFactory,IQueryHandler<DogQuery,DogInfo> dogQueryHandler, IQueryHandler<KennelQuery, KennelLicense> kennelQueryHandler)
        {
            this.clerkRepositoryFactory = clerkRepositoryFactory;
            partyRepositoryFactory = centralPartyRepositoryFactory;
            this.kennelQueryHandler = kennelQueryHandler;
            this.dogQueryHandler = dogQueryHandler;
        }

        public int CurrentOwnerId {
            get
            {
                if (currentDogOwner != null)
                {
                    return currentDogOwner.Id;
                }

                return 0;
            }
        }

        public DogOwner GetDogOwner(int Id)
        {
            if (currentDogOwner != null && currentDogOwner.Id == Id)
            {
                return currentDogOwner;
            }
            if (Id == 0)
            {
                currentDogOwner = new DogOwner();
                return currentDogOwner;
            }
            var ownerRepo = clerkRepositoryFactory.CreateRepository<DogOwner>();            
            currentDogOwner = ownerRepo.GetIncluding(o => o.Id == Id, o => o.KennelLicenses, o => o.Dogs).FirstOrDefault();
            return currentDogOwner;
        }

        public void SetParty(int Id)
        {
            if (currentDogOwner != null)
            {
                currentDogOwner.PartyId = Id;
            }
        }

        public CentralParty GetOwnerParty()
        {
            if (currentDogOwner == null)
            {
                return null;
            }

            var partyRepo = partyRepositoryFactory.CreateRepository<CentralParty>();
            var party = partyRepo.GetIncluding(p => p.Id == currentDogOwner.PartyId, p => p.Addresses).FirstOrDefault();
            return party;
        }

        public string TransactionPerson
        {
            get { return currentTransaction.TransactionPerson; }
            set { currentTransaction.TransactionPerson = value; }
        }
        public string Reference
        {
            get { return currentTransaction.Description;}
            set { currentTransaction.Description = value; }
        }
        public DateTime TransactionDate
        {
            get { return currentTransaction.TransactionDate;}
            set
            {
                currentTransaction.TransactionDate = value;
            }
        }

        public void RefreshDog(int Id)
        {

            var dog = dogQueryHandler.ExecuteQuery(new DogQuery(Id));
            if (dog != null && dog.Id == Id)
            {
                if (currentDogOwner.Dogs.Any(d => d.Id == Id))
                {
                    var origDog = currentDogOwner.Dogs.Where(d => d.Id == Id).FirstOrDefault();
                    UpdateDog(ref origDog , dog);
                }
                else
                {
                    currentDogOwner.Dogs.Add(dog);
                }
            }
        }

        public void RefreshKennel(int Id)
        {
            var kennel = kennelQueryHandler.ExecuteQuery(new KennelQuery(Id));
            if (kennel != null && kennel.Id == Id)
            {
                if (currentDogOwner.KennelLicenses.Any(k => k.Id == Id))
                {
                    var origKennel = currentDogOwner.KennelLicenses.Where(k => k.Id == Id).FirstOrDefault();
                    UpdateKennel(ref origKennel, kennel);
                }
                else
                {
                    currentDogOwner.KennelLicenses.Add(kennel);
                }
            }
        }

        private void UpdateDog(ref DogInfo origDog, DogInfo updatedDog)
        {
            if (origDog != null && updatedDog != null)
            {
                origDog.Comments = updatedDog.Comments;
                origDog.DateVaccinated = updatedDog.DateVaccinated;
                origDog.DogBreed = updatedDog.DogBreed;
                origDog.DogColor = updatedDog.DogColor;
                origDog.DogDeceased = updatedDog.DogDeceased;
                origDog.DogDob = updatedDog.DogDob;
                origDog.DogName = updatedDog.DogName;
                origDog.DogNeuter = updatedDog.DogNeuter;
                origDog.DogSex = updatedDog.DogSex;
                origDog.DogToKennelDate = updatedDog.DogToKennelDate;
                origDog.Dogmarkings = updatedDog.Dogmarkings;
                origDog.IsDangerousDog = updatedDog.IsDangerousDog;
                origDog.IsNuisanceDog = updatedDog.IsNuisanceDog;
                origDog.KennelDog = updatedDog.KennelDog;
                origDog.LastUpdated = updatedDog.LastUpdated;
                origDog.NoRequiredFields = updatedDog.NoRequiredFields;
                origDog.OutsideSource = updatedDog.OutsideSource;
                origDog.Dogvet = updatedDog.Dogvet;
                origDog.ExcludeFromWarrant = updatedDog.ExcludeFromWarrant;
                origDog.LateFee = updatedDog.LateFee;
                origDog.HearGuide = updatedDog.HearGuide;
                origDog.NewTagAdded = updatedDog.NewTagAdded;
                origDog.PreviousOwner = updatedDog.PreviousOwner;
                origDog.PreviousTag = updatedDog.PreviousTag;
                origDog.RabStickerIssue = updatedDog.RabStickerIssue;
                origDog.RabStickerNum = updatedDog.RabStickerNum;
                origDog.RabiesExempt = updatedDog.RabiesExempt;
                origDog.RabiesExpDate = updatedDog.RabiesExpDate;
                origDog.RabiesShotDate = updatedDog.RabiesShotDate;
                origDog.RabiesCertNum = updatedDog.RabiesCertNum;
                origDog.Replacement = updatedDog.Replacement;
                origDog.RabiesTagNo = updatedDog.RabiesTagNo;
                origDog.SandR = updatedDog.SandR;
                origDog.SncertNum = updatedDog.SncertNum;
                origDog.StickerLicNum = updatedDog.StickerLicNum;
                origDog.TagLicNum = updatedDog.TagLicNum;
                origDog.TempLicense = updatedDog.TempLicense;
                origDog.WarrantYear = updatedDog.WarrantYear;
                origDog.WolfHybrid = updatedDog.WolfHybrid;
                origDog.Year = updatedDog.Year;
            }
        }

        private void UpdateKennel(ref KennelLicense origKennel, KennelLicense updatedKennel)
        {
            if (origKennel != null && updatedKennel != null)
            {
                origKennel.DogNumbers = updatedKennel.DogNumbers;
                origKennel.InspectionDate = updatedKennel.InspectionDate;
                origKennel.LicenseId = updatedKennel.LicenseId;
                origKennel.OriginalIssueDate = updatedKennel.OriginalIssueDate;
                origKennel.ReIssueDate = updatedKennel.ReIssueDate;
                origKennel.Year = updatedKennel.Year;
            }
        }
        public void AddDogTransaction(DogTransaction transaction)
        {
           currentTransaction.AddDogTransaction(transaction);
        }

        public void Cancel()
        {
            if (Cancelled != null)
            {
                Cancelled(this,new EventArgs());
            }
        }

        public void CompleteTransaction()
        {
            if (CompletedTransaction != null)
            {
                CompletedTransaction(this, currentTransaction);
            }
        }

        public event EventHandler<DogOwnerTransaction> CompletedTransaction;
        public event EventHandler Cancelled;

    }
}