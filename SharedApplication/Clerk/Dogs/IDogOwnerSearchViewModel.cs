﻿using System;
using System.Collections.Generic;
using SharedApplication.Enums;

namespace SharedApplication.Clerk.Dogs
{
    public interface IDogOwnerSearchViewModel
    {
        DogSearchCriteria SearchCriteria { get; }
        IEnumerable<GenericDescriptionPair<DogSearchBy>> SearchByOptions { get; }
        IEnumerable<GenericDescriptionPair<DogSearchRecordType>> DogSearchRecordOptions { get; }
        IEnumerable<GenericDescriptionPair<SearchCriteriaMatchType>> CriteriaMatchOptions { get; }
        
        void Cancel();
        void Search();
        IEnumerable<DogSearchResult> SearchResults();
        
        void AddNewOwner();
        void SelectOwner(int id);
        void UndeleteOwner(int id);

        event EventHandler<int> OwnerSelected;
        event EventHandler Cancelled;
        event EventHandler<int> UndeleteOwnerHandler;
    }
}