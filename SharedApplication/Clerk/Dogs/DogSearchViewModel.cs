﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.Enums;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Dogs
{
    public class DogSearchViewModel : IDogOwnerSearchViewModel
    {
        public DogSearchCriteria SearchCriteria { get; } = new DogSearchCriteria();
        private List<DogSearchResult> searchResults = new List<DogSearchResult>();

        private IQueryHandler<DogSearchCriteria, IEnumerable<DogSearchResult>> dogQueryHandler;
        public IEnumerable<GenericDescriptionPair<DogSearchBy>> SearchByOptions { get; protected set; }
        public IEnumerable<GenericDescriptionPair<DogSearchRecordType>> DogSearchRecordOptions { get; protected set; }
        public IEnumerable<GenericDescriptionPair<SearchCriteriaMatchType>> CriteriaMatchOptions { get; protected set; }
     
        public DogSearchViewModel(IQueryHandler<DogSearchCriteria, IEnumerable<DogSearchResult>> dogQueryHandler)
        {
            this.dogQueryHandler = dogQueryHandler;
            SetSearchByOptions();
            SetSearchRecordOptions();
            SetCriteriaMatchOptions();
        }

        private void SetSearchByOptions()
        {
            SearchByOptions = new List<GenericDescriptionPair<DogSearchBy>>()
            {
               
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.OwnerName, Description = "Owner Name"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.City, Description = "City"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.Location, Description = "Location"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.KennelLicense, Description = "Kennel License"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.DogName,Description = "Dog Name"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.Breed, Description =  "Breed"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.LicenseTagNumber, Description = "License Number"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.RabiesTagNumber, Description =   "Rabies Number"},
                new GenericDescriptionPair<DogSearchBy>(){ID = DogSearchBy.Veterinarian, Description =  "Veterinarian"}
            };
        }

        private void SetSearchRecordOptions()
        {
            DogSearchRecordOptions = new List<GenericDescriptionPair<DogSearchRecordType>>()
            {
                new GenericDescriptionPair<DogSearchRecordType>(){ID = DogSearchRecordType.All, Description = "All"},
                new GenericDescriptionPair<DogSearchRecordType>(){ID = DogSearchRecordType.Dog,Description = "Dogs"},
                new GenericDescriptionPair<DogSearchRecordType>(){ID = DogSearchRecordType.DogsInKennels, Description = "Dogs In Kennels"}
            };
        }

        private void SetCriteriaMatchOptions()
        {
            CriteriaMatchOptions = new List<GenericDescriptionPair<SearchCriteriaMatchType>>()
            {
                new GenericDescriptionPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.Contains, Description =  "Contains"},
                new GenericDescriptionPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.StartsWith, Description =  "Starts With"},
                new GenericDescriptionPair<SearchCriteriaMatchType>(){ID = SearchCriteriaMatchType.EndsWith, Description =  "Ends With"}
            };
        }

        public void Search()
        {
            searchResults = dogQueryHandler.ExecuteQuery(SearchCriteria).ToList();
        }

        public IEnumerable<DogSearchResult> SearchResults()
        {
            return searchResults;
        }

        public void AddNewOwner()
        {
            
        }

        public void SelectOwner(int id)
        {
	        
        }

        public void UndeleteOwner(int id)
        {
	        UndeleteOwnerHandler(this, id);
        }

        public void Cancel()
        {
	        if (Cancelled != null)
	        {
		        Cancelled(this, new EventArgs());
	        }
        }

        public event EventHandler<int> OwnerSelected;
        public event EventHandler Cancelled;
        public event EventHandler<int> UndeleteOwnerHandler;
    }
}