﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Clerk.Dogs
{
	public class UndeleteDogOwner : SharedApplication.Messaging.Command<bool>
	{
		public int OwnerId { get; set; }
	}
}
