﻿using System;
using System.Collections.Generic;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Dogs
{
    public interface ISearchResultsDogViewModel 
    {
        void SelectOwner(int id);
        IEnumerable<DogSearchResult> SearchResults {get; set;}
        DogSearchCriteria SearchCriteria { get; set; }

        void UndeleteOwner(int id);
        // void SetFlowProcessor(TransactionFlowProcessor transactionFlowProcessor);
        event EventHandler<int> OwnerSelected;
        event EventHandler Cancelled;
        event EventHandler<int> UndeleteOwnerHandler;
		void Cancel();
    }
}