﻿using System;

namespace SharedApplication.Clerk.Dogs
{
    public interface IDogAuditOptionsViewModel
    {
        bool IncludeOnlineTransactions { get; } 
        DateTime StartDate { get;  }
        DateTime EndDate { get;  }
        bool Cancelled { get; set; }
        void Cancel();
        void SetOptions(bool includeOnlineTransactions, DateTime startDate, DateTime endDate);
        void InitializeOptions(bool includeOnlineTransactions, DateTime startDate, DateTime endDate);
    }
}