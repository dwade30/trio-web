﻿using Autofac;
using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Dogs
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DogSearchViewModel>().As<IDogOwnerSearchViewModel>();
            builder.RegisterType<DogOwnerViewModel>().As<IDogOwnerViewModel>();
        }
    }
}