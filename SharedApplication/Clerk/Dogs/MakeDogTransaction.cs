﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk
{
    public class MakeDogTransaction : SharedApplication.Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }
        public MakeDogTransaction(Guid correlationId, Guid transactionId)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
        }
    }
}