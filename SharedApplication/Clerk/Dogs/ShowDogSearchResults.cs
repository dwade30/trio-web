﻿using System.Collections.Generic;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Dogs
{
    public class ShowDogSearchResults : Command
    {
        public  DogTransactionFlowProcessor FlowProcessor { get; private set; }
        public IEnumerable<DogSearchResult> SearchResults { get; private set; }
        public DogSearchCriteria SearchCriteria { get; private set; }
        public ShowDogSearchResults(DogTransactionFlowProcessor transactionFlowProcessor,
            IEnumerable<DogSearchResult> searchResults, DogSearchCriteria searchCriteria) : base()
        {
            this.FlowProcessor = transactionFlowProcessor;
            this.SearchResults = searchResults;
            this.SearchCriteria = searchCriteria;
        }
    }
}