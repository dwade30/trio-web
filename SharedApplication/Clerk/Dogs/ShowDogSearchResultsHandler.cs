﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Dogs
{
    public class ShowDogSearchResultsHandler : CommandHandler<ShowDogSearchResults>
    {
        private IView<ISearchResultsDogViewModel> resultsView;
        public ShowDogSearchResultsHandler(IView<ISearchResultsDogViewModel> resultsView)
        {
            this.resultsView = resultsView;
        }
        protected override void Handle(ShowDogSearchResults command)
        {
            resultsView.ViewModel.SearchResults = command.SearchResults;
            resultsView.ViewModel.SearchCriteria = command.SearchCriteria;
            command.FlowProcessor.Register(resultsView.ViewModel);
            resultsView.Show();
        }
    }
}