﻿using System;
using System.Collections.Generic;
using SharedApplication.Clerk.Models;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Dogs
{
    public class GetDogAuditItems : Command<IEnumerable<DogAuditItem>>
    {
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }
        public bool IncludeOnlineTransactions { get; }

        public GetDogAuditItems(DateTime startDate, DateTime endDate, bool includeOnlineTransactions)
        {
            StartDate = startDate;
            EndDate = endDate;
            IncludeOnlineTransactions = includeOnlineTransactions;
        }
    }
}