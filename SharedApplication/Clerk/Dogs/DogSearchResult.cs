﻿using System.Collections.Generic;

namespace SharedApplication.Clerk
{
    public class DogSearchResult
    {
        public string OwnerLast { get; set; } = "";
        public string OwnerFirst { get; set; } = "";
        public int StreetNumber { get; set; } = 0;
        public string StreetName { get; set; } = "";
        public string DogName { get; set; } = "";
        public string Breed { get; set; } = "";
        public string Color { get; set; } = "";
        public string Veterinarian { get; set; } = "";
        public int OwnerId { get; set; } = 0;
        public int DogId { get; set; } = 0;
        public int PartyId { get; set; } = 0;
        public string City { get; set; } = "";
        public string KennelLicense { get; set; } = "";
        public bool OwnerDeleted { get; set; } = false;
        public IEnumerable<string> DogNames { get; set; } = new List<string>();
    }
}