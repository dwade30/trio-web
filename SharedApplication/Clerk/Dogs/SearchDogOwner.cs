﻿using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk
{
    public class SearchDogOwner : SharedApplication.Messaging.Command, ICommand
    {
     
        public IDogOwnerSearchViewModel ViewModel { get; set; }
        public SearchDogOwner(IDogOwnerSearchViewModel viewModel)
        {
            ViewModel = viewModel;
        }
    }
}