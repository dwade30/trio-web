﻿using System;
using System.Security.Permissions;

namespace SharedApplication.Clerk.Dogs
{
    public class DogAuditOptions
    {
        public bool InlcudeOnlineTransactions { get; }
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }
        public bool Cancelled { get; }
        public DogAuditOptions(bool includeOnlineTransactions, DateTime startDate, DateTime endDate, bool cancelled)
        {
            InlcudeOnlineTransactions = includeOnlineTransactions;
            StartDate = startDate;
            EndDate = endDate;
            Cancelled = cancelled;
        }
    }
}