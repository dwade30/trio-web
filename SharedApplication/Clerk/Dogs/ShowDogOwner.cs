﻿using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Dogs
{
    public class ShowDogOwner : SharedApplication.Messaging.Command, ICommand
    {
     
        public int OwnerId { get; private set; }
        public DogTransactionFlowProcessor FlowProcessor { get; private set; }
        public ShowDogOwner(int ownerId, DogTransactionFlowProcessor transactionFlowProcessor)
        {
            OwnerId = ownerId;
            FlowProcessor = transactionFlowProcessor;
        }
    }
}