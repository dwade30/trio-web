﻿using System;
using System.Collections.Generic;
using SharedApplication.Clerk.Models;

namespace SharedApplication.Clerk.Dogs
{
    public class DogOwnerTransaction
    {
        private List<DogTransaction> dogTransactions = new List<DogTransaction>();
        public IEnumerable<DogTransaction> DogTransactions
        {
            get { return dogTransactions; }
        }

        public void AddDogTransaction(DogTransaction dogTransaction)
        {
            TownFee += Convert.ToDecimal(dogTransaction.TownFee.GetValueOrDefault());
            StateFee += Convert.ToDecimal(dogTransaction.StateFee.GetValueOrDefault());
            ClerkFee += Convert.ToDecimal(dogTransaction.ClerkFee.GetValueOrDefault());
            LateFee += Convert.ToDecimal(dogTransaction.LateFee.GetValueOrDefault());
            MiscFee += Convert.ToDecimal(dogTransaction.AnimalControl.GetValueOrDefault());
            dogTransactions.Add(dogTransaction.GetCopy());
        }

        public string Description { get; set; } = "";
        public Decimal TownFee { get; set; } = 0;
        public Decimal StateFee { get; set; } = 0;
        public Decimal ClerkFee { get; set; } = 0;
        public Decimal LateFee { get; set; } = 0;
        public Decimal MiscFee { get; set; } = 0; //6
       
        public int TownCode { get; set; } = 0;
        public string TransactionPerson { get; set; } = "";
        public DateTime TransactionDate { get; set; }
        public Decimal GetTotalAmount()
        {
            return TownFee + StateFee + ClerkFee + LateFee + MiscFee ;
        }
    }
}
