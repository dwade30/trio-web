﻿using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk
{
    public class SearchDogOwnerHandler : CommandHandler<SearchDogOwner>
    {
        private IView<IDogOwnerSearchViewModel> searchView;
        public SearchDogOwnerHandler(IView<IDogOwnerSearchViewModel> view)
        {
            searchView = view;
        }
        protected override void Handle(SearchDogOwner command)
        {
            searchView.ViewModel = command.ViewModel;
            searchView.Show();
        }
    }
}