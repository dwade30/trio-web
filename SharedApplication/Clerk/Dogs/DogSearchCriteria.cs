﻿using SharedApplication.Enums;

namespace SharedApplication.Clerk
{
    public class DogSearchCriteria
    {
        public DogSearchRecordType TypesOfRecords { get; set; } = DogSearchRecordType.All;
        public bool InlcudeDeletedDogsOrOwners { get; set; } = false;
        public DogSearchBy TypeToSearchBy { get; set; } = DogSearchBy.OwnerName;
        public SearchCriteriaMatchType MatchCriteriaType { get; set; } = SearchCriteriaMatchType.StartsWith;

        public string OwnerLastName { get; set; } = "";
        public string OwnerFirstName { get; set; } = "";
        public string City { get; set; } = "";
        public int LocationNumber { get; set; } = 0;
        public string LocationStreet { get; set; } = "";
        public string DogName { get; set; } = "";
        public string Breed { get; set; } = "";
        public string Veterinarian { get; set; } = "";
        public int KennelLicense { get; set; } = 0;
        public string RabiesTag { get; set; } = "";
        public string LicenseTag { get; set; } = "";

        public bool ByDog()
        {
            switch (TypeToSearchBy)
            {
                case DogSearchBy.City:
                case DogSearchBy.KennelLicense:
                case DogSearchBy.Location:
                case DogSearchBy.OwnerName:
                    return false;
                    break;
                default:
                    return true;
            }
        }
    }

    public enum DogSearchRecordType
    {
        All = 0,
        Dog = 1,
        DogsInKennels = 2
    }

    public enum DogSearchBy
    {
        OwnerName = 0,
        City = 1,
        Location = 2,
        KennelLicense = 3,
        DogName = 4,
        Veterinarian = 5,
        RabiesTagNumber = 6,
        LicenseTagNumber = 7,
        Breed = 8
    }
}