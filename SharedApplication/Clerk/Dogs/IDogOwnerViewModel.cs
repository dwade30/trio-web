﻿using System;
using SharedApplication.CentralParties.Models;
using SharedApplication.Clerk.Models;

namespace SharedApplication.Clerk.Dogs
{
    public interface IDogOwnerViewModel
    {
        int CurrentOwnerId { get; }
        DogOwner GetDogOwner(int Id);
        void SetParty(int Id);
        CentralParty GetOwnerParty();
        string TransactionPerson { get; set; }
        string Reference { get; set; }
        DateTime TransactionDate { get; set; }
        void RefreshDog(int Id);
        void RefreshKennel(int Id);
        void AddDogTransaction(DogTransaction transaction);
        void Cancel();
        void CompleteTransaction();
        event EventHandler<DogOwnerTransaction> CompletedTransaction;
        event EventHandler Cancelled;
    }
}