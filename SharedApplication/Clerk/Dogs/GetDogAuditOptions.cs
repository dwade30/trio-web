﻿using SharedApplication.Messaging;
using System;

namespace SharedApplication.Clerk.Dogs
{
    public class GetDogAuditOptions : Command<DogAuditOptions>
    {
        public bool InlcudeOnlineTransactions { get; }
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }

        public GetDogAuditOptions(bool includeOnlineTransactions, DateTime startDate, DateTime endDate)
        {
            InlcudeOnlineTransactions = includeOnlineTransactions;
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}