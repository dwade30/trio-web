﻿using System;

namespace SharedApplication.Clerk.Dogs
{
    public class DogAuditOptionsViewModel : IDogAuditOptionsViewModel
    {
        public bool IncludeOnlineTransactions { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }
        public bool Cancelled { get; set; } = false;

        
        public void Cancel()
        {
            Cancelled = true;
        }

        public void SetOptions(bool includeOnlineTransactions, DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
            IncludeOnlineTransactions = includeOnlineTransactions;
            Cancelled = false;
        }

        public void InitializeOptions(bool includeOnlineTransactions, DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
            IncludeOnlineTransactions = includeOnlineTransactions;
        }
    }
}