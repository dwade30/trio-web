﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk
{
    public class ShowPrintVitalHandler : CommandHandler<ShowPrintVital>
    {
        private IView<IPrintVitalsViewModel> printView;
        public ShowPrintVitalHandler(IView<IPrintVitalsViewModel> printView)
        {
            this.printView = printView;
        }
        protected override void Handle(ShowPrintVital command)
        {
            printView.ViewModel.AddTransaction(command.VitalTransaction);
            command.FlowProcessor.Register(printView.ViewModel);
            printView.Show();
        }
    }
}