﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk
{
    public class ShowPrintVital : SharedApplication.Messaging.Command
    {
        public ShowPrintVital(VitalTransaction vitalTransaction, VitalFlowProcessor flowProcessor)
        {
            VitalTransaction = vitalTransaction;
            FlowProcessor = flowProcessor;
        }
        public VitalTransaction VitalTransaction { get; set; }
        //public SharedApplication.Messaging.Command CancelCommand { get; set; }

        public VitalFlowProcessor FlowProcessor { get; set; }
    }
}