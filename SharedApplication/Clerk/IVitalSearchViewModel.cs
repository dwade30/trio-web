﻿using System;

namespace SharedApplication.Clerk
{
    public interface IVitalSearchViewModel
    {
        void Cancel();

        event EventHandler Cancelled;
        event EventHandler<int> Selected;
        void Select(int Id);
    }
}