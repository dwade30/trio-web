﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Clerk.Enums
{
	public enum BirthDateType
	{
		LiveBirth,
		DelayedBirth,
		ForeignBirth,
		MarraigeGroom,
		MarraigeBride,
		Death
	}
}
