﻿using System;

namespace SharedApplication.Clerk
{
    public class VitalTransaction
    {
        public int TownCode { get; set; } = 0;
        public decimal VitalAmount { get; set; } = 0;
        public decimal StateFee { get; set; } = 0;
        public string Reference { get; set; } = "";
        public string Control1 { get; set; } = "";

        public string Control2 { get; set; } = "";
        public string TypeCode { get; set; } = "";
        public string Name { get; set; } = "";
    }
}