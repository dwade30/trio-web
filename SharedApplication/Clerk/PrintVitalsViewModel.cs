﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk
{
    public class PrintVitalsViewModel : IPrintVitalsViewModel
    {
        private VitalTransaction currentTransaction;
        //private SharedApplication.Messaging.Command returnCommand;
        //private CommandDispatcher commandDispatcher;
        public PrintVitalsViewModel(CommandDispatcher commandDispatcher)
        {
            //this.commandDispatcher = commandDispatcher;
        }


        public void Cancel()
        {

            if (Cancelled != null)
            {
                Cancelled(this, new EventArgs());
            }
        }

        public void AddTransaction(VitalTransaction vitalTransaction)
        {
            currentTransaction = vitalTransaction;
        }

        public event EventHandler Cancelled;
        public event EventHandler<VitalTransaction> CompletedTransaction;

        public void UpdateTransaction(decimal vitalAmount, decimal stateFee)
        {
            if (currentTransaction == null)
            {
                AddTransaction(new VitalTransaction());
            }

            currentTransaction.VitalAmount = vitalAmount;
            currentTransaction.StateFee = stateFee;
        }

        public void CompleteTransaction()
        {
            if (CompletedTransaction != null)
            {
                CompletedTransaction(this, currentTransaction);
            }
        }
    }
}