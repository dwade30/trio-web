﻿namespace SharedApplication.Clerk
{
    public enum ClerkTransactionType
    {
        Dog = 0,
        Birth = 1,
        Death = 2,
        Marriage = 3,
        Burial = 4
    }
}