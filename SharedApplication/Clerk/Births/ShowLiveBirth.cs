﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Births
{
    public class ShowLiveBirth : SharedApplication.Messaging.Command
    {
        public int BirthId { get; set; }
        public VitalTransactionFlowProcessor FlowProcessor { get; set; }
        public ShowLiveBirth(int Id, BirthTransactionFlowProcessor flowProcessor)
        {
            BirthId = Id;
            this.FlowProcessor = flowProcessor;
        }
    }
}