﻿using Autofac;

namespace SharedApplication.Clerk.Births
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BirthSearchViewModel>().As<IBirthSearchViewModel>();
            builder.RegisterType<BirthViewModel>().As<IBirthViewModel>();
        }
    }
}