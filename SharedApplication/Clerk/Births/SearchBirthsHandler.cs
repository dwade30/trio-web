﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Births
{
    public class SearchBirthsHandler : CommandHandler<SearchBirths>
    {
        private IView<IBirthSearchViewModel> searchView;

        public SearchBirthsHandler(IView<IBirthSearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override void Handle(SearchBirths command)
        {
            command.FlowProcessor.Register(searchView.ViewModel);
            searchView.Show();
        }
    }
}