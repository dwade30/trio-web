﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Births
{
    public class SearchBirths : SharedApplication.Messaging.Command
    {
        public BirthTransactionFlowProcessor FlowProcessor { get; set; }
    }
}