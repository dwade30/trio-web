﻿using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Births
{
    public class MakeBirthTransactionHandler : CommandHandler<MakeBirthTransaction>
    {
        private CommandDispatcher commandDispatcher;
        private BirthTransactionFlowProcessor transactionFlowProcessor;

        public MakeBirthTransactionHandler(CommandDispatcher commandDispatcher, BirthTransactionFlowProcessor transactionFlowProcessor)
        {
            this.commandDispatcher = commandDispatcher;
            this.transactionFlowProcessor = transactionFlowProcessor;
        }
        protected override void Handle(MakeBirthTransaction command)
        {
            transactionFlowProcessor.CorrelationId = command.CorrelationId;
            transactionFlowProcessor.TransactionId = command.TransactionId;
            transactionFlowProcessor.Search();
        }
    }
}