﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Births
{
    public class MakeBirthTransaction : SharedApplication.Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }

        public MakeBirthTransaction(Guid correlationId, Guid transactionId)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
        }
    }
}