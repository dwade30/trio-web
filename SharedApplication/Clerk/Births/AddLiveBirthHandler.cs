﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Births
{
    public class AddLiveBirthHandler : CommandHandler<AddLiveBirth>
    {
        private IView<IBirthViewModel> birthView;
        public AddLiveBirthHandler(IView<IBirthViewModel> birthView)
        {
            this.birthView = birthView;
        }
        protected override void Handle(AddLiveBirth command)
        {
            birthView.ViewModel.VitalId = command.BirthId;
            birthView.Show();
        }
    }
}