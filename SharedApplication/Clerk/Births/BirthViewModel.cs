﻿using System;
using SharedApplication.Clerk.Models;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Births
{
    public class BirthViewModel : VitalViewModel, IBirthViewModel
    {
        //public int BirthId { get; set; }
        private IClerkRepositoryFactory clerkRepositoryFactory;
        private Birth currentBirth;
        //private VitalTransaction currentTransaction;
        //private CommandDispatcher commandDispatcher;

        public BirthViewModel(IClerkRepositoryFactory clerkRepositoryFactory,CommandDispatcher commandDispatcher)
        {
            this.clerkRepositoryFactory = clerkRepositoryFactory;
            this.commandDispatcher = commandDispatcher;
            transactionTypeCode = "BIR";
        }

        //public Birth GetBirth(int Id)
        //{
        //    if (currentBirth != null && Id == currentBirth.Id)
        //    {
        //        return currentBirth;
        //    }
        //    var birthRepo = clerkRepositoryFactory.CreateRepository<Birth>();
        //    currentBirth = birthRepo.Get(Id);
        //    return currentBirth;
        //}

        //public void Cancel()
        //{
        //    if (Cancelled != null)
        //    {
        //        Cancelled(this,new EventArgs());
        //    }
        //}

        //public void CompleteTransaction()
        //{
        //    if (CompletedTransaction != null)
        //    {
        //        CompletedTransaction(this, currentTransaction);
        //    }
        //}

        //public void PrintVital()
        //{
        //    if (PrintingTransaction != null)
        //    {
        //        PrintingTransaction(this,currentTransaction);
        //    }
        //}

        //public void UpdateTransaction(VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    currentTransaction.TypeCode = "BIR";
        //}

        //public event EventHandler Cancelled;
        //public event EventHandler<VitalTransaction> CompletedTransaction;
        //public event EventHandler<VitalTransaction> PrintingTransaction;
    }
}