﻿namespace SharedApplication.Clerk.Births
{
    public class AddLiveBirth : SharedApplication.Messaging.Command
    {
        public AddLiveBirth(int Id)
        {
            BirthId = Id;
        }
        public int BirthId { get; set; }
    }
}