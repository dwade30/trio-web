﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Births
{
    public class ShowLiveBirthHandler : CommandHandler<ShowLiveBirth>
    {
        private IView<IBirthViewModel> birthView;
        public ShowLiveBirthHandler(IView<IBirthViewModel> birthView)
        {
            this.birthView = birthView;
        }
        protected override void Handle(ShowLiveBirth command)
        {
            birthView.ViewModel.VitalId = command.BirthId;
            command.FlowProcessor.Register(birthView.ViewModel);
            birthView.Show();
        }
    }
}