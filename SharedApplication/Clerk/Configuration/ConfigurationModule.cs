﻿using System;
using Autofac;
using SharedApplication.Clerk.Dogs;
using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {            
            builder.RegisterInstance(new InMemoryEditableRepository<ClerkTransaction,Guid>()).As<IEditableRepository<ClerkTransaction,Guid>>();
            builder.RegisterType<PrintVitalsViewModel>().As<IPrintVitalsViewModel>();
            builder.RegisterType<DogAuditOptionsViewModel>().As<IDogAuditOptionsViewModel>();
        }
    }
}