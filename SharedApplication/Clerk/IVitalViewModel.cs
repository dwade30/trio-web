﻿using System;

namespace SharedApplication.Clerk
{
    public interface IVitalViewModel
    {
        int VitalId { get; set; }
       
        void Cancel();
        void UpdateTransaction(VitalTransaction vitalTransaction);
        void CompleteTransaction();
        void PrintVital();
        event EventHandler Cancelled;
        event EventHandler<VitalTransaction> CompletedTransaction;
        event EventHandler<VitalTransaction> PrintingTransaction;
    }
}