﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk
{
    public abstract class VitalViewModel : IVitalViewModel
    {
        public int VitalId { get; set; }
        public void Cancel()
        {
            if (Cancelled != null)
            {
                Cancelled(this, new EventArgs());
            }
        }

        protected string transactionTypeCode;
        protected VitalTransaction currentTransaction;
        protected CommandDispatcher commandDispatcher;
        public void UpdateTransaction(VitalTransaction vitalTransaction)
        {
            currentTransaction = vitalTransaction;
            currentTransaction.TypeCode = transactionTypeCode;
        }

        public void CompleteTransaction()
        {
            if (CompletedTransaction != null)
            {
                CompletedTransaction(this, currentTransaction);
            }
        }

        public void PrintVital()
        {
            if (PrintingTransaction != null)
            {
                PrintingTransaction(this, currentTransaction);
            }
        }

        public event EventHandler Cancelled;
        public event EventHandler<VitalTransaction> CompletedTransaction;
        public event EventHandler<VitalTransaction> PrintingTransaction;
    }
}