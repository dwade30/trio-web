﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class AuditChangesArchive
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string ChangeDescription { get; set; }
        public string UserField1 { get; set; }
        public string UserField2 { get; set; }
        public string UserField3 { get; set; }
        public string UserField4 { get; set; }
        public DateTime? DateUpdated { get; set; }
        public DateTime? TimeUpdated { get; set; }
        public string UserId { get; set; }
    }
}