﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class LinkedDocument
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Description { get; set; }
        public int? DocReferenceId { get; set; }
        public int? DocReferenceType { get; set; }
    }
}