﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogDefault
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string UnlicensedReminder { get; set; }
        public string ExpiringReminder { get; set; }
        public string UnlicensedSignature { get; set; }
        public string WarrantReminder { get; set; }
    }
}