﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogLicenseReport
    {
        public int Id { get; set; }
        public string Municipality { get; set; }
        public string ClerkName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
    }
}