﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class ReportLayout
    {
        public int Id { get; set; }
        public short? ReportId { get; set; }
        public short? RowId { get; set; }
        public short? ColumnId { get; set; }
        public short? FieldId { get; set; }
        public short? Width { get; set; }
        public string DisplayText { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}