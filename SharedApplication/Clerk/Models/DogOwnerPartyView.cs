﻿using System;

namespace SharedApplication.Clerk.Models
{
    public partial class DogOwnerPartyView
    {
        public int Id { get; set; }
        public int PartyId { get; set; } = 0;
        public string DogNumbers { get; set; } = "";
        public string LocationNum { get; set; } = "";
        public string LocationStr { get; set; } = "";
        public bool? NoRequiredFields { get; set; } = false;
        public bool? Deleted { get; set; } = false;
        public string Comments { get; set; } = "";
        public DateTime? LastUpdated { get; set; }
        public int? TownCode { get; set; } = 0;
        public Guid? PartyGuid { get; set; }
        public int? PartyType { get; set; }
        public string FirstName { get; set; } = "";
        public string MiddleName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string Designation { get; set; } = "";
        public string Email { get; set; } = "";
        public string WebAddress { get; set; } = "";

        public string FullName()
        {
            return (((FirstName.Trim() + " " + MiddleName).Trim() + " " + LastName).Trim() + " " + Designation).Trim();
        }

        public string FullNameLastFirst()
        {
            if (LastName != "")
            {
                return ((LastName.Trim() + ", " + FirstName + " " + MiddleName).ToString() + " " + Designation).ToString();
            }
            else
            {
                return ((FirstName + " " + MiddleName).Trim() + " " + Designation).Trim();
            }
        }

    }
}