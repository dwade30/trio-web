﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class ClerkFee
    {
        public int Id { get; set; }
        public double? NewBirthFee { get; set; }
        public double? ReplacementBirthFee { get; set; }
        public double? NewDeathFee { get; set; }
        public double? ReplacementDeathFee { get; set; }
        public double? BurialPermitFee { get; set; }
        public double? NewMarriageCertificateFee { get; set; }
        public double? ReplacementMarriageCertificateFee { get; set; }
        public double? NewMarriageLicenseFee { get; set; }
        public double? ReplacementMarriageLicenseFee { get; set; }
        public string AdditionalFeeCaption1 { get; set; }
        public string AdditionalFeeCaption2 { get; set; }
        public string AdditionalFeeCaption3 { get; set; }
        public string AdditionalFeeCaption4 { get; set; }
        public string AdditionalFeeCaption5 { get; set; }
        public double? AdditionalFee1 { get; set; }
        public double? AdditionalFee2 { get; set; }
        public double? AdditionalFee3 { get; set; }
        public double? AdditionalFee4 { get; set; }
        public double? AdditionalFee5 { get; set; }
        public DateTime? LastUpdated { get; set; }
        public double? DeathStateFee { get; set; }
        public double? DeathReplacementStateFee { get; set; }
        public double? VetDeathStateFee { get; set; }
        public double? BirthStateFee { get; set; }
        public double? BirthReplacementStateFee { get; set; }
        public double? DispositionStateFee { get; set; }
        public double? MarriageCertStateFee { get; set; }
        public double? MarriageCertReplacementStateFee { get; set; }
        public double? MarriageLicStateFee { get; set; }
        public double? MarriageLicReplacementStateFee { get; set; }
    }
}