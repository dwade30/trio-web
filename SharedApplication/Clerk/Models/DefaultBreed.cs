﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DefaultBreed
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}