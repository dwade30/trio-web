﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DispositionLocation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}