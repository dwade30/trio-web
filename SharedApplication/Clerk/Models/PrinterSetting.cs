﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class PrinterSetting
    {
        public int Id { get; set; }
        public short? DogLicense { get; set; }
        public short? KennelLicense { get; set; }
        public string LicenseYear { get; set; }
        public DateTime? DateVaccinated { get; set; }
        public DateTime? LastUpdated { get; set; }
        public short? MonthlyLicense { get; set; }
        public string LaserLineAdjustment { get; set; }
        public string BirthAdjustment { get; set; }
        public string DeathAdjustment { get; set; }
        public string MarriageAdjustment { get; set; }
        public string DogAdjustment { get; set; }
        public string BurialAdjustment { get; set; }
        public string MarriageLicenseAdjustment { get; set; }
        public string KennelAdjustment { get; set; }
        public bool? PrintDogReRegOnFront { get; set; }
        public bool? PrintDogReRegExpiration { get; set; }
        public double? DogRemindersAdjustment { get; set; }
        public bool? PrintMonthlyDogOptionalSection { get; set; }
    }
}