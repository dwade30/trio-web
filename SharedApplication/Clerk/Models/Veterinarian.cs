﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class Veterinarian
    {
        public int Id { get; set; }
        public string VetName { get; set; }
    }
}