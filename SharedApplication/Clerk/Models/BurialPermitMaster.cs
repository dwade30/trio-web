﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class BurialPermitMaster
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Designation { get; set; }
        public string Sex { get; set; }
        public string DateOfDeath { get; set; }
        public string Race { get; set; }
        public string Age { get; set; }
        public string PlaceOfDeathCity { get; set; }
        public string PlaceOfDeathState { get; set; }
        public string LicenseNumber { get; set; }
        public string Nameoffuneralestablishment { get; set; }
        public string BusinessAddress { get; set; }
        public string Typeofpermit { get; set; }
        public string Authorizationforpermit { get; set; }
        public string PlaceOfDisposition { get; set; }
        public string CityOrTown { get; set; }
        public string NameOfClerkOrSubregistrar { get; set; }
        public string DateOfDisposition { get; set; }
        public string DateSigned { get; set; }
        public string Disposition { get; set; }
        public string DispositionDate { get; set; }
        public string Location { get; set; }
        public string NameOfCemetery { get; set; }
        public string Field5 { get; set; }
        public int? DeathNumber { get; set; }
        public bool? Burial { get; set; }
        public bool? Cremation { get; set; }
        public string Signature { get; set; }
        public string LocationState { get; set; }
        public string BusinessAddress2 { get; set; }
        public string BusinessCity { get; set; }
        public string BusinessState { get; set; }
        public string BusinessZip { get; set; }
        public int? TownCode { get; set; }
        public bool? InArmedForces { get; set; }
        public string ClerkTown { get; set; }
        public string CremDate { get; set; }
        public string CremLoc { get; set; }
        public string CremName { get; set; }
        public bool? CremBuried { get; set; }
        public bool? CremScattered { get; set; }
        public bool? CremFam { get; set; }
        public string TempCemetery { get; set; }
        public string TempDate { get; set; }
        public string TempLocation { get; set; }
        public string TempSignature { get; set; }
        public bool? InTemporaryStorage { get; set; }
        public bool? AuthorizationCertificate { get; set; }
        public bool? AuthorizationReport { get; set; }
        public bool? AuthorizationExaminer { get; set; }
        public bool? AuthorizationApplication { get; set; }
    }
}