﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class BirthsForeign
    {
        public int Id { get; set; }
        public string FileNumber { get; set; }
        public DateTime? DeceasedDate { get; set; }
        public string AmendedInfo { get; set; }
        public bool? Legitimate { get; set; }
        public bool? NoRequiredFields { get; set; }
        public bool? DeathRecordInOffice { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string ChildFirstName { get; set; }
        public string ChildMiddleName { get; set; }
        public string ChildLastName { get; set; }
        public string ChildSex { get; set; }
        public string ChildDob { get; set; }
        public string ChildCountry { get; set; }
        public string ChildState { get; set; }
        public string ChildTown { get; set; }
        public string MothersFirstName { get; set; }
        public string MothersMiddleName { get; set; }
        public string MothersLastName { get; set; }
        public string MothersMaidenName { get; set; }
        public string MothersAge { get; set; }
        public string MothersStateOfBirth { get; set; }
        public string MothersStreet { get; set; }
        public string MothersTown { get; set; }
        public string MothersState { get; set; }
        public string FathersFirstName { get; set; }
        public string FathersMiddleName { get; set; }
        public string FathersLastName { get; set; }
        public string FathersDesignation { get; set; }
        public string FathersAge { get; set; }
        public string FathersStateOfBirth { get; set; }
        public string ClerkFirstName { get; set; }
        public string ClerkMiddleName { get; set; }
        public string ClerkLastName { get; set; }
        public string ClerkTown { get; set; }
        public string ClerkAttestDate { get; set; }
        public bool? MarriageOnFile { get; set; }
        public bool? DeathOnFile { get; set; }
        public string ChildDobdescription { get; set; }
        public string MotherDob { get; set; }
        public string FatherDob { get; set; }
        public int? TownCode { get; set; }
        public string Comments { get; set; }
        public string ClerkName { get; set; }
        public DateTime? ActualDate { get; set; }
    }
}