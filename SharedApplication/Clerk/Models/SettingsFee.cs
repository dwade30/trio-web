﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class SettingsFee
    {
        public int Id { get; set; }
        public decimal? AgentFeeAtv { get; set; }
        public decimal? AgentFeeSnow { get; set; }
        public decimal? AgentFeeBoat { get; set; }
        public decimal? LateDog { get; set; }
        public decimal? WarrantDog { get; set; }
        public decimal? TransferDog { get; set; }
        public decimal? ReplacementDog { get; set; }
        public decimal? FixedDog { get; set; }
        public decimal? UnFixedDog { get; set; }
        public decimal? AgentRegFeeAtv { get; set; }
        public decimal? AgentRegFeeSnow { get; set; }
        public decimal? AgentRegFeeBoat { get; set; }
        public decimal? DogTagFee { get; set; }
        public DateTime? LastUpdated { get; set; }
        public double? UserDefinedFee1 { get; set; }
        public string UserDefinedDescription1 { get; set; }
        public double? DogState { get; set; }
        public double? DogTown { get; set; }
        public double? DogClerk { get; set; }
        public double? FixedState { get; set; }
        public double? FixedTown { get; set; }
        public double? FixedClerk { get; set; }
        public double? KennelState { get; set; }
        public double? KennelTown { get; set; }
        public double? KennelClerk { get; set; }
        public double? KennelWarrantFee { get; set; }
        public double? KennelLateFee { get; set; }
        public bool? LateFeeOnServiceDog { get; set; }
        public decimal? DangerousClerkFee { get; set; }
        public decimal? DangerousStateFee { get; set; }
        public decimal? DangerousTownFee { get; set; }
        public decimal? DangerousLateFee { get; set; }
        public decimal? NuisanceClerkFee { get; set; }
        public decimal? NuisanceStateFee { get; set; }
        public decimal? NuisanceTownFee { get; set; }
        public decimal? NuisanceLateFee { get; set; }
    }
}