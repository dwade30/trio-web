﻿using System;

namespace SharedApplication.Clerk.Models
{
    public class DogAuditItem
    {
        //public DogOwnerPartyView DogOwner { get; set; }
        public int DogTransactionID { get; set; } = 0;
        public int TransactionNumber { get; set; } = 0;
        public string Description { get; set; } = "";
        public DateTime TransactionDate { get; set; }
        public bool IsAdjustmentVoid { get; set; } = false;
        public int OwnerNum { get; set; } = 0;
        public int DogNumber { get; set; } = 0;
        public string KennelDogs { get; set; } = "";
        public int KennelLicenseId { get; set; } = 0;
        public bool IsLicense { get; set; } = false;
        public bool IsKennel { get; set; } = false;
        public decimal Amount { get; set; } = 0;
        public decimal StateFee { get; set; } = 0;
        public decimal ClerkFee { get; set; } = 0;
        public decimal TownFee { get; set; } = 0;
        public decimal LateFee { get; set; } = 0;
        public int DogYear { get; set; } = 0;
        public decimal KennelWarrantFee { get; set; } = 0;
        public decimal KennelLateFee { get; set; } = 0;
        public decimal KennelLic1Fee { get; set; } = 0;
        public decimal KennelLic2Fee { get; set; } = 0;
        public int KennelLic1Num { get; set; } = 0;
        public int KennelLic2Num { get; set; } = 0;
        public decimal ReplacementStickerFee { get; set; } = 0;
        public bool IsReplacementSticker { get; set; } = false;
        public decimal ReplacementLicenseFee { get; set; } = 0;
        public bool IsReplacementLicense { get; set; } = false;
        public decimal ReplacementTagFee { get; set; } = 0;
        public bool IsReplacementTag { get; set; } = false;
        public bool IsSpayNeuter { get; set; } = false;
        public decimal SpayNonSpayAmount { get; set; } = 0;
        public bool IsSearchRescue { get; set; } = false;
        public bool IsHearingGuid { get; set; } = false;
        public bool IsTransfer { get; set; } = false;
        public decimal TransferLicenseFee { get; set; } = 0;
        public bool IsTempLicense { get; set; } = false;
        public decimal TempLicenseFee { get; set; } = 0;
        public string TagNumber { get; set; } = "";
        public int StickerNumber { get; set; } = 0;
        public string RabiesTagNumber { get; set; } = "";
        public int OldSticker { get; set; } = 0;
        public int OldYear { get; set; } = 0;
        public string OldTagNumber { get; set; } = "";
        public DateTime? OldIssueDate { get; set; }
        public decimal AnimalControl { get; set; } = 0;
        public decimal WarrantFee { get; set; } = 0;
        public DateTime? TimeStamp { get; set; }
        public int TownCode { get; set; } = 0;
        public bool Online { get; set; } = false;
        public bool DangerousDog { get; set; } = false;
        public bool NuisanceDog { get; set; } = false;


        public int UserID { get; set; } = 0;
        public string UserName { get; set; } = "";
        public string TransactionPerson { get; set; } = "";
        public string TransactionDescription { get; set; } = "";

        public string OwnerName { get; set; } = "";
        public string DogName { get; set; } = "";
    }
}