﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class BirthsDelayed
    {
        public int Id { get; set; }
        public string FileNumber { get; set; }
        public DateTime? DeceasedDate { get; set; }
        public string AmendedInfo { get; set; }
        public bool? Legitimate { get; set; }
        public bool? NoRequiredFields { get; set; }
        public bool? DeathRecordInOffice { get; set; }
        public DateTime? LastUpdated { get; set; }
        public string ChildFirstName { get; set; }
        public string ChildMiddleName { get; set; }
        public string ChildLastName { get; set; }
        public string ChildSex { get; set; }
        public string ChildDob { get; set; }
        public string ChildCounty { get; set; }
        public string ChildRace { get; set; }
        public string ChildTown { get; set; }
        public string ChildCurrentAddress { get; set; }
        public string MothersFirstName { get; set; }
        public string MothersMiddleName { get; set; }
        public string MothersLastName { get; set; }
        public string MothersMaidenName { get; set; }
        public string MothersBirthPlace { get; set; }
        public string FathersFirstName { get; set; }
        public string FathersMiddleName { get; set; }
        public string FathersLastName { get; set; }
        public string FathersDesignation { get; set; }
        public string FathersBirthPlace { get; set; }
        public string NotaryFirstName { get; set; }
        public string NotaryMiddleName { get; set; }
        public string NotaryLastName { get; set; }
        public string NotaryCommissionExpires { get; set; }
        public DateTime? NotarySwornDate { get; set; }
        public string ClerkFirstName { get; set; }
        public string ClerkMiddleName { get; set; }
        public string ClerkLastName { get; set; }
        public string ClerkTown { get; set; }
        public DateTime? ClerkSwornDate { get; set; }
        public string TypeDocument1 { get; set; }
        public string WhomSigned1 { get; set; }
        public string DateIssued1 { get; set; }
        public string DateOrigEntry1 { get; set; }
        public DateTime? Dob1 { get; set; }
        public string BirthPlace1 { get; set; }
        public string FullNameFather1 { get; set; }
        public string FullNameMother1 { get; set; }
        public string TypeDocument2 { get; set; }
        public string WhomSigned2 { get; set; }
        public string DateIssued2 { get; set; }
        public string DateOrigEntry2 { get; set; }
        public DateTime? Dob2 { get; set; }
        public string BirthPlace2 { get; set; }
        public string FullNameFather2 { get; set; }
        public string FullNameMother2 { get; set; }
        public string TypeDocument3 { get; set; }
        public string WhomSigned3 { get; set; }
        public string DateIssued3 { get; set; }
        public string DateOrigEntry3 { get; set; }
        public DateTime? Dob3 { get; set; }
        public string BirthPlace3 { get; set; }
        public string FullNameFather3 { get; set; }
        public string FullNameMother3 { get; set; }
        public string StateRegistrar { get; set; }
        public string EvidenceReviewedBy { get; set; }
        public string DateOfFiling { get; set; }
        public bool? MarriageOnFile { get; set; }
        public bool? DeathOnFile { get; set; }
        public string ChildDobdescription { get; set; }
        public int? TownCode { get; set; }
        public string Comments { get; set; }
        public DateTime? ActualDate { get; set; }
    }
}