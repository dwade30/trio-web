﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DefaultName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Other { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}