﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogOwner
    {
        public DogOwner()
        {
            Dogs = new HashSet<DogInfo>();
            KennelLicenses = new HashSet<KennelLicense>();
        }

        public int Id { get; set; }
        public int PartyId { get; set; }
        public string DogNumbers { get; set; }
        public string LocationNum { get; set; }
        public string LocationStr { get; set; }
        public bool? NoRequiredFields { get; set; }
        public bool? Deleted { get; set; }
        public string Comments { get; set; }
        public DateTime? LastUpdated { get; set; }
        public int? TownCode { get; set; }
        public virtual ICollection<DogInfo> Dogs { get; set; }

        public virtual ICollection<KennelLicense> KennelLicenses { get; set; }
        
    }
}