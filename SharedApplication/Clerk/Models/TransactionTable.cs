﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class TransactionTable
    {
        public int Id { get; set; }
        public DateTime? TransactionDate { get; set; }
        public short? TransactionType { get; set; }
        public int? UserId { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public double? TotalAmount { get; set; }
        public double? Amount1 { get; set; }
        public double? Amount2 { get; set; }
        public double? Amount3 { get; set; }
        public double? Amount4 { get; set; }
        public double? Amount5 { get; set; }
        public double? Amount6 { get; set; }
        public string TransactionPerson { get; set; }
        public int? TownCode { get; set; }
    }
}