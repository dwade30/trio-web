﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class ClerkDefault
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string LegalResidence { get; set; }
        public string AgentNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string County { get; set; }
        public string StateName { get; set; }
        public bool? BoolFileDuplicateWarning { get; set; }
        public bool? UseSignatures { get; set; }
        public string BirthDefaultCounty { get; set; }
        public string BirthDefaultTown { get; set; }
        public string LastBirthFileNumber { get; set; }
        public string LastDeathFileNumber { get; set; }
        public string LastMarriageFileNumber { get; set; }
        public bool? DontPrintAttested { get; set; }
        public string MotherDefaultCounty { get; set; }
        public string MotherDefaultState { get; set; }
        public string MotherDefaultCity { get; set; }
        public bool? DefaultAttestedBy { get; set; }
        public bool? AutoFillActualDate { get; set; }
        public short? OnlineDogImportOption { get; set; }
    }
}