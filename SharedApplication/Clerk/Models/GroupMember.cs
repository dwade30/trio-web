﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class GroupMember
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Zip4 { get; set; }
        public DateTime? TermFrom { get; set; }
        public DateTime? TermTo { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string UserDefined { get; set; }
        public string Title { get; set; }
        public int? GroupId { get; set; }
        public string HomeNumber { get; set; }
        public string CellNumber { get; set; }
        public string OtherNumber { get; set; }
    }
}