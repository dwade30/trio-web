﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Clerk.Models
{
	public class ClerkLevelPermissions
	{
		public bool AllowDogs { get; set; } = false;
		public bool AllowBirths { get; set; } = false;
		public bool AllowMarriages { get; set; } = false;
		public bool AllowDeaths { get; set; } = false;
		public bool AllowGroups { get; set; } = false;
	}
}
