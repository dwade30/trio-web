﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DefaultRegistrarName
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Mi { get; set; }
        public DateTime? LastUpdated { get; set; }
        public short? ClerkTownId { get; set; }
        public string ClerkTown { get; set; }
    }
}