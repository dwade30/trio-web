﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DatabaseVersion
    {
        public int? VersionNumber { get; set; }
        public DateTime? LastUpdated { get; set; }
        public int Id { get; set; }
    }
}