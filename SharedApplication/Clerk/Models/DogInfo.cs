﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogInfo
    {
        public int? OwnerNum { get; set; }
        public int Id { get; set; }
        public string DogName { get; set; }
        public DateTime? DogDob { get; set; }
        public string DogSex { get; set; }
        public string DogColor { get; set; }
        public string DogBreed { get; set; }
        public string Dogmarkings { get; set; }
        public string Dogvet { get; set; }
        public bool? DogNeuter { get; set; }
        public string SncertNum { get; set; }
        public string StickerLicNum { get; set; }
        public string TagLicNum { get; set; }
        public string RabiesTagNo { get; set; }
        public DateTime? RabiesShotDate { get; set; }
        public string RabiesCertNum { get; set; }
        public DateTime? RabStickerIssue { get; set; }
        public string RabStickerNum { get; set; }
        public DateTime? RabiesExpDate { get; set; }
        public bool? LateFee { get; set; }
        public bool? WarrantFee { get; set; }
        public bool? Replacement { get; set; }
        public bool? Transfer { get; set; }
        public bool? SandR { get; set; }
        public bool? HearGuide { get; set; }
        public bool? WolfHybrid { get; set; }
        public int? Year { get; set; }
        public decimal? AmountC { get; set; }
        public bool? DogDeceased { get; set; }
        public bool? TempLicense { get; set; }
        public bool? ReplacementTagFee { get; set; }
        public DateTime? LastUpdated { get; set; }
        public bool? KennelDog { get; set; }
        public DateTime? DogToKennelDate { get; set; }
        public DateTime? DateVaccinated { get; set; }
        public bool? NoRequiredFields { get; set; }
        public DateTime? ReplacementTagDate { get; set; }
        public DateTime? ReplacementLicenseDate { get; set; }
        public DateTime? TransferLicenseDate { get; set; }
        public DateTime? ReplacementStickerDate { get; set; }
        public bool? ReplacementStickerFee { get; set; }
        public string NewTagAdded { get; set; }
        public DateTime? NewTagAddedDate { get; set; }
        public int? PreviousOwner { get; set; }
        public bool? OutsideSource { get; set; }
        public string Comments { get; set; }
        public int? WarrantYear { get; set; }
        public bool? ExcludeFromWarrant { get; set; }
        public string PreviousTag { get; set; }
        public bool? RabiesExempt { get; set; }
        public bool? IsDangerousDog { get; set; }
        public bool? IsNuisanceDog { get; set; }
        public virtual DogOwner Owner { get; set; }
    }
}