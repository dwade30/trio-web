﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class CustomReport
    {
        public int Id { get; set; }
        public string ReportName { get; set; }
        public string Type { get; set; }
        public string Sql { get; set; }
        public DateTime? LastUpdated { get; set; }
        public short? LabelType { get; set; }
        public bool? Landscape { get; set; }
    }
}