﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class Title
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}