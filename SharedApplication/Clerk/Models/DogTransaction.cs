﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogTransaction
    {
        public int? TransactionNumber { get; set; }
        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? TransactionDate { get; set; }
        public bool? BoolAdjustmentVoid { get; set; }
        public int? OwnerNum { get; set; }
        public int? DogNumber { get; set; }
        public string KennelDogs { get; set; }
        public int? KennelLicenseId { get; set; }
        public bool? BoolLicense { get; set; }
        public bool? BoolKennel { get; set; }
        public double? Amount { get; set; }
        public double? StateFee { get; set; }
        public double? ClerkFee { get; set; }
        public double? TownFee { get; set; }
        public double? LateFee { get; set; }
        public int? DogYear { get; set; }
        public double? KennelWarrantFee { get; set; }
        public double? KennelLateFee { get; set; }
        public double? KennelLic1Fee { get; set; }
        public double? KennelLic2Fee { get; set; }
        public int? KennelLic1Num { get; set; }
        public int? KennelLic2Num { get; set; }
        public double? ReplacementStickerFee { get; set; }
        public bool? BoolReplacementSticker { get; set; }
        public double? ReplacementLicenseFee { get; set; }
        public bool? BoolReplacementLicense { get; set; }
        public double? ReplacementTagFee { get; set; }
        public bool? BoolReplacementTag { get; set; }
        public bool? BoolSpayNeuter { get; set; }
        public double? SpayNonSpayAmount { get; set; }
        public bool? BoolSearchRescue { get; set; }
        public bool? BoolHearingGuid { get; set; }
        public bool? BoolTransfer { get; set; }
        public double? TransferLicenseFee { get; set; }
        public bool? BoolTempLicense { get; set; }
        public double? TempLicenseFee { get; set; }
        public string TagNumber { get; set; }
        public int? StickerNumber { get; set; }
        public string RabiesTagNumber { get; set; }
        public int? OldSticker { get; set; }
        public int? OldYear { get; set; }
        public string OldTagNumber { get; set; }
        public DateTime? OldIssueDate { get; set; }
        public double? AnimalControl { get; set; }
        public double? WarrantFee { get; set; }
        public DateTime? TimeStamp { get; set; }
        public int? TownCode { get; set; }
        public bool? Online { get; set; }
        public bool? DangerousDog { get; set; }
        public bool? NuisanceDog { get; set; }

        public virtual TransactionTable TransactionTable { get; set; }

        public DogTransaction GetCopy()
        {
            var dogTransaction = new DogTransaction()
            {
                 Amount = Amount,
                 AnimalControl = AnimalControl,
                 BoolAdjustmentVoid =  BoolAdjustmentVoid,
                 BoolHearingGuid = BoolHearingGuid,
                 BoolKennel = BoolKennel,
                 BoolLicense = BoolLicense,
                 BoolReplacementLicense = BoolReplacementLicense,
                 BoolReplacementSticker = BoolReplacementSticker,
                 BoolReplacementTag = BoolReplacementTag,
                 BoolSearchRescue = BoolSearchRescue,
                 BoolSpayNeuter = BoolSpayNeuter,
                 BoolTempLicense = BoolTempLicense,
                 BoolTransfer = BoolTransfer,
                 ClerkFee = ClerkFee,
                 DangerousDog = DangerousDog,
                 Description = Description,
                 DogNumber = DogNumber,
                 DogYear = DogYear,
                 KennelDogs = KennelDogs,
                 KennelLateFee = KennelLateFee,
                 KennelLic1Fee = KennelLic1Fee,
                 KennelLic1Num = KennelLic1Num,
                 KennelLic2Fee = KennelLic2Fee,
                 KennelLic2Num = KennelLic2Num,
                 KennelLicenseId = KennelLicenseId,
                 KennelWarrantFee = KennelWarrantFee,
                 LateFee = LateFee,
                 NuisanceDog = NuisanceDog,
                 OldIssueDate = OldIssueDate,
                 OldSticker = OldSticker,
                 OldTagNumber = OldTagNumber,
                 OldYear = OldYear,
                 Online = Online,
                 OwnerNum = OwnerNum,
                 RabiesTagNumber = RabiesTagNumber,
                 ReplacementLicenseFee = ReplacementLicenseFee,
                 ReplacementStickerFee = ReplacementStickerFee,
                 ReplacementTagFee = ReplacementTagFee,
                 SpayNonSpayAmount = SpayNonSpayAmount,
                 StateFee = StateFee,
                 StickerNumber =  StickerNumber,
                 TagNumber = TagNumber,
                 TownCode = TownCode,
                 TempLicenseFee = TempLicenseFee,
                 TimeStamp = TimeStamp,
                 TownFee = TownFee,
                 TransactionDate = TransactionDate,
                 TransactionNumber = TransactionNumber,
                 TransferLicenseFee = TransferLicenseFee,
                 WarrantFee =  WarrantFee                
            };
            return dogTransaction;
        }
    }
}