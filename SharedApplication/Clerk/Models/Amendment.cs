﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class Amendment
    {
        public int Id { get; set; }
        public string Statement { get; set; }
        public string Parameter1 { get; set; }
        public string Parameter2 { get; set; }
        public string Parameter3 { get; set; }
        public string Type { get; set; }
        public string AmendmentId { get; set; }
        public string Amendment1 { get; set; }
        public bool? PrintAmendment { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}