﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogInventory
    {
        public int Id { get; set; }
        public string StickerNumber { get; set; }
        public int? Year { get; set; }
        public string Type { get; set; }
        public string PermTemp { get; set; }
        public string Status { get; set; }
        public DateTime? DateReceived { get; set; }
        public string ReceivedOpid { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}