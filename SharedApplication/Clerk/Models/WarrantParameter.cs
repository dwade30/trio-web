﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class WarrantParameter
    {
        public int Id { get; set; }
        public string TownCounty { get; set; }
        public string TownName { get; set; }
        public string ControlOfficer { get; set; }
        public string CourtCounty { get; set; }
        public string CourtTown { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}