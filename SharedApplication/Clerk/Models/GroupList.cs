﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class GroupList
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string UserDefinedDescription { get; set; }
        public string GroupDescription { get; set; }
    }
}