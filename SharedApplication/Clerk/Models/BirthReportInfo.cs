﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Clerk.Models
{
	public class BirthReportInfo
	{
		public string FirstName { get; set; } = "";
		public string MiddleName { get; set; } = "";
		public string LastName { get; set; } = "";
		public string Sex { get; set; } = "";
		public string RecordedDateOfBirth { get; set; } = "";
		public DateTime? ConvertedRecordedDateOfBirth { get; set; } = null;
		public DateTime? ActualDateOfBirth { get; set; } = null;
		public string BirthPlace { get; set; } = "";
		public string MothersFirstName { get; set; } = "";
		public string MothersMiddleName { get; set; } = "";
		public string MothersLastName { get; set; } = "";
	}
}
