﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class KennelLicense
    {
        public int Id { get; set; }
        public int? LicenseId { get; set; }
        public int? OwnerNum { get; set; }
        public string DogNumbers { get; set; }
        public DateTime? OriginalIssueDate { get; set; }
        public DateTime? ReIssueDate { get; set; }
        public DateTime? InspectionDate { get; set; }
        public int? Year { get; set; }
        public virtual DogOwner Owner { get; set; }
    }
}