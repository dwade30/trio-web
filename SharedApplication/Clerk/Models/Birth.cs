﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class Birth
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Designation { get; set; }
        public string Dateofbirth { get; set; }
        public string Sex { get; set; }
        public string BirthPlace { get; set; }
        public string AttendantsLastName { get; set; }
        public string AttendantsFirstName { get; set; }
        public string AttendantsMiddleInitial { get; set; }
        public string AttendantTitle { get; set; }
        public string AttendantAddress { get; set; }
        public string MothersLastName { get; set; }
        public string MothersFirstName { get; set; }
        public string MothersMiddleName { get; set; }
        public string MothersMaidenName { get; set; }
        public string ResidenceOfMother { get; set; }
        public string FathersLastName { get; set; }
        public string FathersFirstName { get; set; }
        public string FathersMiddleInitial { get; set; }
        public string FathersDesignation { get; set; }
        public string RecordingClerksLastName { get; set; }
        public string RecordingClerksFirstName { get; set; }
        public string RecordingClerksMiddleInitial { get; set; }
        public string CityOrTown { get; set; }
        public DateTime? DateOfFiling { get; set; }
        public DateTime? AttestDate { get; set; }
        public string StateRegistrarMunicipalClerk { get; set; }
        public string TownOf { get; set; }
        public bool? Legitimate { get; set; }
        public string FileNumber { get; set; }
        public DateTime? LastUpdate { get; set; }
        public bool? NoRequiredFields { get; set; }
        public bool? DeathRecordInOffice { get; set; }
        public string ChildTime { get; set; }
        public string ChildCounty { get; set; }
        public string ChildTown { get; set; }
        public string FacilityName { get; set; }
        public string MothersDob { get; set; }
        public string MotherState { get; set; }
        public string MotherCounty { get; set; }
        public string MotherTown { get; set; }
        public string MothersAddress { get; set; }
        public string MothersZipCode { get; set; }
        public string MothersYears { get; set; }
        public string FathersDob { get; set; }
        public string AmendedInfo { get; set; }
        public DateTime? DeceasedDate { get; set; }
        public string RegistrarFirstName { get; set; }
        public string RegistrarLastName { get; set; }
        public string RegistrarMiddleName { get; set; }
        public DateTime? RegistrarDateFiled { get; set; }
        public string Amended { get; set; }
        public DateTime? DateAmended { get; set; }
        public string Court { get; set; }
        public string MothersBirthPlace { get; set; }
        public string FathersBirthPlace { get; set; }
        public bool? SoleParent { get; set; }
        public bool? MarriageOnFile { get; set; }
        public bool? DeathOnFile { get; set; }
        public bool? Aop { get; set; }
        public string Comments { get; set; }
        public string RegistrarDateFiledDescription { get; set; }
        public bool? CourtDeterminedPaternity { get; set; }
        public int? TownCode { get; set; }
        public string RegTown { get; set; }
        public string RegistrarName { get; set; }
        public DateTime? ActualDate { get; set; }
    }
}