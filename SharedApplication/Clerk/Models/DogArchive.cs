﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class DogArchive
    {
        public int Id { get; set; }
        public int? DogNumber { get; set; }
        public int? CurrentOwnerId { get; set; }
        public int? OldOwnerId { get; set; }
        public bool? Deceased { get; set; }
        public DateTime? ThisRecDate { get; set; }
        public string Transaction { get; set; }
    }
}