﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Clerk.Models
{
    public partial class VitalRecordCertNum
    {
        public string CertNumber { get; set; }
        public DateTime? PrintDate { get; set; }
        public int? TblKeyNum { get; set; }
        public string TblName { get; set; }
        public string AttestedBy { get; set; }
        public int Id { get; set; }
    }
}