﻿using System;

namespace SharedApplication.Clerk
{
    public abstract class VitalSearchViewModel : IVitalSearchViewModel
    {
        public void Cancel()
        {
            if (Cancelled != null)
            {
                Cancelled(this,new EventArgs());
            }
        }

        public event EventHandler Cancelled;
        public event EventHandler<int> Selected;
        public void Select(int Id)
        {
            if (Selected != null)
            {
                Selected(this, Id);
            }
        }
    }
}