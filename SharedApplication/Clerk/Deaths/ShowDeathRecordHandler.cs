﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Deaths
{
    public class ShowDeathRecordHandler : CommandHandler<ShowDeathRecord>
    {
        private IView<IDeathViewModel> deathView;

        public ShowDeathRecordHandler(IView<IDeathViewModel> deathView)
        {
            this.deathView = deathView;
        }

        protected override void Handle(ShowDeathRecord command)
        {
            deathView.ViewModel.VitalId = command.DeathId;
            command.FlowProcessor.Register(deathView.ViewModel);
            deathView.Show();
        }
    }
}