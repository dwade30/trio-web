﻿using System;
using SharedApplication.Clerk.Models;

namespace SharedApplication.Clerk.Deaths
{
    public  interface IDeathViewModel : IVitalViewModel
    {
        void ShowBurialPermit(int Id);
        //int DeathId { get; set; }
        ////Death GetDeath(int Id);
        //void Cancel();
        //void UpdateTransaction(VitalTransaction vitalTransaction);
        //void CompleteTransaction();
        //void PrintVital();
        //event EventHandler Cancelled;
        //event EventHandler<VitalTransaction> CompletedTransaction;
        //event EventHandler<VitalTransaction> PrintingTransaction;
        event EventHandler<int> ShowingBurialPermit;
    }
}