﻿using SharedApplication.Clerk.Burials;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Deaths
{
    public class AddBurialHandler :CommandHandler<AddBurial>
    {
        private IView<IBurialViewModel> burialView;

        public AddBurialHandler(IView<IBurialViewModel> burialView)
        {
            this.burialView = burialView;
        }
        protected override void Handle(AddBurial command)
        {
            burialView.ViewModel.VitalId = 0;
            burialView.Show();
        }
    }
}