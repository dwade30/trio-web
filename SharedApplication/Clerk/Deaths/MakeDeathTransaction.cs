﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Deaths
{
    public class MakeDeathTransaction : SharedApplication.Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }

        public MakeDeathTransaction(Guid correlationId, Guid transactionId)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
        }
    }
}