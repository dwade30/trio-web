﻿using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Deaths
{
    public class MakeDeathTransactionHandler : CommandHandler<MakeDeathTransaction>
    {
        private CommandDispatcher commandDispatcher;
        private DeathTransactionFlowProcessor transactionFlowProcessor;

        public MakeDeathTransactionHandler(CommandDispatcher commandDispatcher,
            DeathTransactionFlowProcessor flowProcessor)
        {
            transactionFlowProcessor = flowProcessor;
            this.commandDispatcher = commandDispatcher;
        }
        protected override void Handle(MakeDeathTransaction command)
        {
            transactionFlowProcessor.CorrelationId = command.CorrelationId;
            transactionFlowProcessor.TransactionId = command.TransactionId;
            transactionFlowProcessor.Search();
        }
    }
}