﻿using Autofac;

namespace SharedApplication.Clerk.Deaths
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DeathSearchViewModel>().As<IDeathSearchViewModel>();
            builder.RegisterType<DeathViewModel>().As<IDeathViewModel>();
        }
    }
}