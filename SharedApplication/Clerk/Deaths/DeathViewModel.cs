﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Deaths
{
    public class DeathViewModel : VitalViewModel, IDeathViewModel
    {
        //public int DeathId { get; set; }
        //private VitalTransaction currentTransaction;

        public DeathViewModel(CommandDispatcher commandDispatcher)
        {
            this.transactionTypeCode = "DEA";
            this.commandDispatcher = commandDispatcher;            
        }
        //public void Cancel()
        //{
        //    if (Cancelled != null)
        //    {
        //        Cancelled(this, new EventArgs());
        //    }
        //}

        //public void UpdateTransaction(VitalTransaction vitalTransaction)
        //{
        //    currentTransaction = vitalTransaction;
        //    currentTransaction.TypeCode = "DEA";
        //}
        //public void CompleteTransaction()
        //{
        //    if (CompletedTransaction != null)
        //    {
        //        CompletedTransaction(this, currentTransaction);
        //    }
        //}

        //public void PrintVital()
        //{
        //    if (PrintingTransaction != null)
        //    {
        //        PrintingTransaction(this, currentTransaction);
        //    }
        //}

        //public event EventHandler Cancelled;
        //public event EventHandler<VitalTransaction> CompletedTransaction;
        //public event EventHandler<VitalTransaction> PrintingTransaction;
        public void ShowBurialPermit(int Id)
        {
            throw new NotImplementedException();
        }

        public event EventHandler<int> ShowingBurialPermit;
    }
}