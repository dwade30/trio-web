﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Deaths
{
    public class SearchDeathsHandler : CommandHandler<SearchDeaths>
    {
        private IView<IDeathSearchViewModel> searchView;

        public SearchDeathsHandler(IView<IDeathSearchViewModel> deathView)
        {
            this.searchView = deathView;
        }
        protected override void Handle(SearchDeaths command)
        {
            command.FlowProcessor.Register(searchView.ViewModel);
            searchView.Show();
        }
    }
}