﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Deaths
{
    public class SearchDeaths : SharedApplication.Messaging.Command
    {
        public SearchDeaths(DeathTransactionFlowProcessor flowProcessor)
        {
            FlowProcessor = flowProcessor;
        }
        public DeathTransactionFlowProcessor FlowProcessor { get; set; }
    }
}