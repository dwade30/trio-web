﻿namespace SharedApplication.Clerk.Deaths
{
    public class AddDeath : Messaging.Command
    {
        public int DeathId { get; set; }

        public AddDeath(int Id)
        {
            DeathId = Id;
        }
    }
}