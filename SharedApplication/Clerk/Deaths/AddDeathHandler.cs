﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Deaths
{
    public class AddDeathHandler : CommandHandler<AddDeath>
    {
        private IView<IDeathViewModel> deathView;

        public AddDeathHandler(IView<IDeathViewModel> viewModel)
        {
            deathView = viewModel;
        }
        protected override void Handle(AddDeath command)
        {
            deathView.ViewModel.VitalId = command.DeathId;
            deathView.Show();
        }
    }
}