﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Deaths
{
    public class ShowDeathRecord : SharedApplication.Messaging.Command
    {
        public int DeathId { get; set; }
        public DeathTransactionFlowProcessor FlowProcessor { get; set; }
        public ShowDeathRecord(int Id, DeathTransactionFlowProcessor flowProcessor)
        {
            DeathId = Id;
            this.FlowProcessor = flowProcessor;
        }

    }
}