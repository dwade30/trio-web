﻿using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Burials
{
    public class MakeBurialTransactionHandler : CommandHandler<MakeBurialTransaction>
    {
        private CommandDispatcher commandDispatcher;
        private BurialTransactionFlowProcessor transactionFlowProcessor;

        public MakeBurialTransactionHandler(CommandDispatcher commandDispatcher,BurialTransactionFlowProcessor transactionFlowProcessor)
        {
            this.commandDispatcher = commandDispatcher;
            this.transactionFlowProcessor = transactionFlowProcessor;
        }
        protected override void Handle(MakeBurialTransaction command)
        {
            transactionFlowProcessor.CorrelationId = command.CorrelationId;
            transactionFlowProcessor.TransactionId = command.TransactionId;
            transactionFlowProcessor.Search();
        }
    }
}