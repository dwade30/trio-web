﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Burials
{
    public class ShowBurialHandler : CommandHandler<ShowBurial>
    {
        private CommandDispatcher commandDispatcher;
        private IView<IBurialViewModel> burialView;

        public ShowBurialHandler( IView<IBurialViewModel> burialView)
        {
            this.burialView = burialView;
        }
        protected override void Handle(ShowBurial command)
        {
            burialView.ViewModel.VitalId = command.BurialId;
            burialView.ViewModel.ChargeFees = command.ChargeFees;
            command.FlowProcessor.Register(burialView.ViewModel);
            burialView.Show();
        }
    }
}