﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Burials
{
    public class BurialViewModel : VitalViewModel, IBurialViewModel
    {
        public BurialViewModel(CommandDispatcher commandDispatcher)
        {
            transactionTypeCode = "BUR";
            this.commandDispatcher = commandDispatcher;           
        }
        public bool ChargeFees { get; set; }
        public int DeathId { get; set; } = 0;
    }
}