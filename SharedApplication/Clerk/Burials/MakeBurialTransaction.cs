﻿using System;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Clerk.Burials
{
    public class MakeBurialTransaction : Messaging.Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public Guid TransactionId { get; set; }

        public MakeBurialTransaction(Guid correlationId, Guid transactionId)
        {
            CorrelationId = correlationId;
            TransactionId = transactionId;
        }
    }
}