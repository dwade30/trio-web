﻿using Autofac;

namespace SharedApplication.Clerk.Burials
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BurialSearchViewModel>().As<IBurialSearchViewModel>();
            builder.RegisterType<BurialViewModel>().As<IBurialViewModel>();
        }
    }
}