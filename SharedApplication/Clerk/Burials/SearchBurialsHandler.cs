﻿using SharedApplication.Messaging;

namespace SharedApplication.Clerk.Burials
{
    public class SearchBurialsHandler : CommandHandler<SearchBurials>
    {
        private IView<IBurialSearchViewModel> searchView;

        public SearchBurialsHandler(IView<IBurialSearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override void Handle(SearchBurials command)
        {
            command.FlowProcessor.Register(searchView.ViewModel);
            searchView.Show();
        }
    }
}
