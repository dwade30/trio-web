﻿namespace SharedApplication.Clerk.Burials
{
    public interface IBurialViewModel : IVitalViewModel
    {
        int DeathId { get; set; }
        bool ChargeFees { get; set; }
    }
}