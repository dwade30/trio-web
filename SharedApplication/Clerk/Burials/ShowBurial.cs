﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Burials
{
    public class ShowBurial : Messaging.Command
    {
        public int BurialId { get; set; }
        public bool ChargeFees { get; set; }
        public VitalTransactionFlowProcessor FlowProcessor { get; set; }
        public ShowBurial(int Id, VitalTransactionFlowProcessor flowProcessor, bool chargeFees)
        {
            BurialId = Id;
            this.FlowProcessor = flowProcessor;
            this.ChargeFees = chargeFees;
        }
    }
}