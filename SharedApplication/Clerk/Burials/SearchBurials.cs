﻿using SharedApplication.Clerk.Receipting;

namespace SharedApplication.Clerk.Burials
{
    public class SearchBurials : Messaging.Command
    {
        public BurialTransactionFlowProcessor FlowProcessor { get; set; }

        public SearchBurials(BurialTransactionFlowProcessor flowProcessor)
        {
            FlowProcessor = flowProcessor;
        }
    }
}