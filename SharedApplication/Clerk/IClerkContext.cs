﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using SharedApplication.Clerk.Models;

namespace SharedApplication.Clerk
{
    public interface IClerkContext
    {
        // IQueryable<DogInfo> DogInfos { get; }
        IQueryable<DogInfo> DogInfos { get; }
        IQueryable<DogOwner> DogOwners { get; }
        IQueryable<Cya> Cyas { get; }
		IQueryable<KennelLicense> KennelLicenses { get; }
        IQueryable<DogInventory> DogInventories { get; }
        IQueryable<Birth> Births { get; }
        IQueryable<BirthsDelayed> BirthsDelayeds { get; }
        IQueryable<BirthsForeign> BirthsForeigns { get; }
        IQueryable<Marriage> Marriages { get; }
        IQueryable<Death> Deaths { get; }
        IQueryable<DogAndOwnerPartyView> DogAndOwnerParties { get; }
        IQueryable<DogOwnerPartyView> DogOwnerParties { get; }
        IQueryable<DogTransaction> DogTransactions { get; }
        IQueryable<TransactionTable> TransactionTables { get; }
        

    }

    
}