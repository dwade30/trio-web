﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Models
{
	public class CalculateBillResult
	{
		public CalculateInterestResult CalculatedInterestResult { get; set; } = new CalculateInterestResult();
		public DateTime CalculatedInterestDate { get; set; }
		public bool InterestCalculated { get; set; }
		public DateTime LastInterestDate { get; set; }
	}
}
