﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Models
{
	public class CalculateInterestResult
	{
		public decimal Interest { get; set; }
		public double PerDiem { get; set; }
	}
}
