﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication
{
	public class GlobalAccountsReceivableSettings
	{
		public bool ShowLastARAccountInCR { get; set; }
		public int gintBasis { get; set; }
	}
}
