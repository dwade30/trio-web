﻿namespace SharedApplication.Authorization
{
    public class GetUserPermissions
    {
        public int UserId { get; set; }

        public GetUserPermissions(int userId)
        {
            UserId = userId;
        }
    }
}