﻿namespace SharedApplication.Authorization
{
    public interface IUserPermissionSet
    {
        int UserId { get;  }
        string GetPermission(string module, int functionId);
    }
}