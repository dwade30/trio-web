﻿namespace SharedApplication.Authorization
{
    public class PermissionItem
    {
        public string PermissionValue { get; set; } = "";
        public string Category { get; set; } = "";
        public int FunctionId { get; set; } = 0;
    }
}