﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.Authorization
{
    public class UserPermissionSet : IUserPermissionSet
    {
        public UserPermissionSet(int userId, IEnumerable<PermissionItem> permissions)
        {
            UserId = userId;
            FillPermissions(permissions);
        }
        public int UserId { get; private set; }
        
        public string GetPermission(string module, int functionId)
        {
            if (!permissionCategories.ContainsKey(module))
            {
                return "N";
            }

            var category = permissionCategories[module];
            var permission = category.FirstOrDefault(p => p.FunctionId == functionId);
            if (permission == null)
            {
                return "N";
            }

            return permission.PermissionValue;
        }

        private Dictionary<string,List<PermissionItem>> permissionCategories = new Dictionary<string, List<PermissionItem>>();

        private void FillPermissions(IEnumerable<PermissionItem> permissions)
        {
            if (permissions != null)
            {
                foreach (var permission in permissions)
                {
                    List<PermissionItem> category;
                    if (!permissionCategories.ContainsKey(permission.Category))
                    {
                        category = new List<PermissionItem>();
                        permissionCategories.Add(permission.Category,category);
                    }
                    else
                    {
                        category = permissionCategories[permission.Category];
                    }

                    var permissionItem = category.FirstOrDefault(p => p.FunctionId == permission.FunctionId);
                    if (permissionItem != null)
                    {
                        permissionItem.PermissionValue = permission.PermissionValue;
                    }
                    else
                    {
                        permissionItem = new PermissionItem(){Category = permission.Category, FunctionId = permission.FunctionId, PermissionValue = permission.PermissionValue};
                        category.Add(permissionItem);
                    }
                }
            }
        }
    }
}