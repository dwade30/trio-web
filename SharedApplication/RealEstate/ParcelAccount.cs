﻿namespace SharedApplication.RealEstate
{
    public class ParcelCard
    {
        public int Account { get; set; } = 0;
        public int Card { get; set; } = 1;
        public decimal Acres { get; set; } = 0;
        public decimal MixedAcres { get; set; } = 0;
        public decimal SoftAcres { get; set; } = 0;
        public decimal HardAcres { get; set; } = 0;
        public int HardValue { get; set; } = 0;
        public int MixedValue { get; set; } = 0;
        public int SoftValue { get; set; } = 0;
    }
}