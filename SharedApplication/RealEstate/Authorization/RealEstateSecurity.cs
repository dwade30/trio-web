﻿namespace SharedApplication.RealEstate.Authorization
{
    public static class RealEstateSecurity
    {
        public const string AddPictures = "RealEstate_34";
        public const string DeletePictures = "RealEstate_37";
        public const string ViewAccountInformation = "RealEstate_15";
    }
}