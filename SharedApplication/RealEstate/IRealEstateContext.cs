﻿using System.Linq;
using SharedApplication.RealEstate.Models;
using SharedApplication.TaxCollections.Models;

namespace SharedApplication.RealEstate
{
    public interface IRealEstateContext
    {
        IQueryable<REDefault> Defaults { get; }
        IQueryable<Mvrreport> MvrReports { get; }
        IQueryable<TranCode> TranCodes { get;  }
        IQueryable<REMaster> REMasters { get; }
        IQueryable<ExemptCode> ExemptCodes { get; }
        IQueryable<BookPage> BookPages { get; }
        IQueryable<LandType> LandTypes { get; }
        IQueryable<PictureRecord> PictureRecords { get; }
        IQueryable<PreviousOwner> PreviousOwners { get; }
        IQueryable<REAccountPartyAddressView> REAccountPartyAddressViews { get; }
        IQueryable<ReportTitle> ReportTitles { get; }
        IQueryable<Summrecord> SummationRecords { get; }
        IQueryable<Taxratecalculation> TaxRateCalculations { get; }
        IQueryable<Srmaster> SRMasters { get; }
    }
}