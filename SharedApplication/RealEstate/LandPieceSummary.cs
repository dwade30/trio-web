﻿namespace SharedApplication.RealEstate
{
    public class LandPieceSummary : LandPieceBase
    {       
        public decimal Acres { get; set; } = 0;
        public decimal Value { get; set; } = 0;
    }
}