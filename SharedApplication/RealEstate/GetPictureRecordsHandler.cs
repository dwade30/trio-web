﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class GetPictureRecordsHandler : CommandHandler<GetPictureRecords,IEnumerable<PictureRecord>>
    {
        private IRealEstateContext reContext;
        public GetPictureRecordsHandler(IRealEstateContext reContext)
        {
            this.reContext = reContext;
        }
        protected override IEnumerable<PictureRecord> Handle(GetPictureRecords command)
        {
            return reContext.PictureRecords.Where(p => p.Account == command.Account && p.Card == command.Card)
                .OrderBy(p => p.PicNum).ToList();
        }
    }
}