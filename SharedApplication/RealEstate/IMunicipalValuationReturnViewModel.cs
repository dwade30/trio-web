﻿namespace SharedApplication.RealEstate
{
    public interface IMunicipalValuationReturnViewModel
    {
        MunicipalValuationReturn ValuationReturn { get; }
        int TownId { get; set; }
        void LoadMVR();
        void SaveMVR();
        void LoadAndCalculateMVR();
        void PrintMVR();
    }
}