﻿
namespace SharedApplication.RealEstate.Models
{
    //[Table("tblTranCode")]
    public  class TranCode
    {
        public int? Code { get; set; }

       // [StringLength(50)]
        public string Description { get; set; }

        //[StringLength(50)]
        public string ShortDescription { get; set; }

        public double? Amount { get; set; }

        public int ID { get; set; }
    }
}
