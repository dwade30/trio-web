﻿using System;

namespace SharedApplication.RealEstate.Models
{
    public partial class BookPage
    {
        public string Book { get; set; }
        public string Page { get; set; }
        public string Bpdate { get; set; }
        public int? Account { get; set; }
        public short? Card { get; set; }
        public DateTime? SaleDate { get; set; }
        public short? Line { get; set; }
        public bool? Current { get; set; }
        public int Id { get; set; }
    }
}