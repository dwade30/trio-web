﻿using System;

namespace SharedApplication.RealEstate.Models
{
    public partial class PictureRecord
    {
        public int Id { get; set; }
        public int? Account { get; set; }
        public int? Card { get; set; }
        public int? PicNum { get; set; }
        public string Description { get; set; }
        public string PictureLocation { get; set; }
        public Guid DocumentIdentifier { get; set; }
    }
}