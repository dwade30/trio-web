﻿using System.Collections.Generic;
using SharedApplication.Extensions;

namespace SharedApplication.RealEstate.Models
{
    public partial class Summrecord
    {
        public int? Sacct { get; set; }
        public int? Strancode { get; set; }
        public int? Sdwellrcnld { get; set; }
        public int? Slandc1 { get; set; }
        public double? Slandv1 { get; set; }
        public int? Slandc2 { get; set; }
        public double? Slandv2 { get; set; }
        public int? Slandc3 { get; set; }
        public double? Slandv3 { get; set; }
        public int? Slandc4 { get; set; }
        public double? Slandv4 { get; set; }
        public int? Slandc5 { get; set; }
        public double? Slandv5 { get; set; }
        public int? Slandc6 { get; set; }
        public double? Slandv6 { get; set; }
        public int? Slandc7 { get; set; }
        public double? Slandv7 { get; set; }
        public short? Sobc1 { get; set; }
        public int? Sobv1 { get; set; }
        public short? Sobc2 { get; set; }
        public int? Sobv2 { get; set; }
        public short? Sobc3 { get; set; }
        public int? Sobv3 { get; set; }
        public short? Sobc4 { get; set; }
        public int? Sobv4 { get; set; }
        public short? Sobc5 { get; set; }
        public int? Sobv5 { get; set; }
        public short? Sobc6 { get; set; }
        public int? Sobv6 { get; set; }
        public short? Sobc7 { get; set; }
        public int? Sobv7 { get; set; }
        public short? Sobc8 { get; set; }
        public int? Sobv8 { get; set; }
        public short? Sobc9 { get; set; }
        public int? Sobv9 { get; set; }
        public short? Sobc10 { get; set; }
        public int? Sobv10 { get; set; }
        public int? Ssfla { get; set; }
        public double? Sacres { get; set; }
        public int? Sovlandc { get; set; }
        public double? Sovlanda { get; set; }
        public int? Sovbldgc { get; set; }
        public double? Sovbldga { get; set; }
        public string Sdate { get; set; }
        public int? Scmocc1 { get; set; }
        public int? Scmval1 { get; set; }
        public int? Scmocc2 { get; set; }
        public int? Scmval2 { get; set; }
        public int? Sbuilt { get; set; }
        public int? Sstyle { get; set; }
        public string Sexpansion { get; set; }
        public int? Scode { get; set; }
        public int? SrecordNumber { get; set; }
        public double? Slanda1 { get; set; }
        public double? Slanda2 { get; set; }
        public double? Slanda3 { get; set; }
        public double? Slanda4 { get; set; }
        public double? Slanda5 { get; set; }
        public double? Slanda6 { get; set; }
        public double? Slanda7 { get; set; }
        public int? Dwellcode { get; set; }
        public int? CardNumber { get; set; }
        public int? Sage { get; set; }
        public int? Comval1 { get; set; }
        public int? Comval2 { get; set; }
        public short? LandCode { get; set; }
        public int Id { get; set; }

        public LandPieceSummary GetLandPieceSummary(int pieceNumber)
        {
            switch (pieceNumber)
            {
                case 1:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc1.GetValueOrDefault(),
                        Acres = Slanda1.GetValueOrDefault().ToDecimal(),
                        Value = Slandv1.GetValueOrDefault().ToDecimal()
                    };
                case 2:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc2.GetValueOrDefault(),
                        Acres = Slanda2.GetValueOrDefault().ToDecimal(),
                        Value = Slandv2.GetValueOrDefault().ToDecimal()
                    };
                case 3:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc3.GetValueOrDefault(),
                        Acres = Slanda3.GetValueOrDefault().ToDecimal(),
                        Value = Slandv3.GetValueOrDefault().ToDecimal()
                    };
                case 4:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc4.GetValueOrDefault(),
                        Acres = Slanda4.GetValueOrDefault().ToDecimal(),
                        Value = Slandv4.GetValueOrDefault().ToDecimal()
                    };
                case 5:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc5.GetValueOrDefault(),
                        Acres = Slanda5.GetValueOrDefault().ToDecimal(),
                        Value = Slandv5.GetValueOrDefault().ToDecimal()
                    };
                case 6:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc6.GetValueOrDefault(),
                        Acres = Slanda6.GetValueOrDefault().ToDecimal(),
                        Value = Slandv6.GetValueOrDefault().ToDecimal()
                    };
                case 7:
                    return new LandPieceSummary()
                    {
                        LandTypeCode = Slandc7.GetValueOrDefault(),
                        Acres = Slanda7.GetValueOrDefault().ToDecimal(),
                        Value = Slandv7.GetValueOrDefault().ToDecimal()
                    };
            }

            return new LandPieceSummary()
            {
                LandTypeCode = 0,
                Value = 0,
                Acres = 0
            };
        }

        public IEnumerable<LandPieceSummary> GetLandSummaries()
        {
            var landPieces = new List<LandPieceSummary>();
            for (int x = 1; x < 8; x++)
            {
                var landPiece = GetLandPieceSummary(x);
                if (landPiece.LandTypeCode > 0)
                {
                    landPieces.Add(landPiece);
                }
            }

            return landPieces;
        }
    }
}