﻿using System;

namespace SharedApplication.RealEstate.Models
{
    public partial class Taxratecalculation
    {
        public int? PersonalPropertyValuation { get; set; }
        public double? CountyTax { get; set; }
        public double? MunicipalAppropriation { get; set; }
        public double? Tif { get; set; }
        public double? Education { get; set; }
        public double? StateSharing { get; set; }
        public double? Other { get; set; }
        public double? TaxRate { get; set; }
        public DateTime? FiscalStartDate { get; set; }
        public DateTime? FiscalEndDate { get; set; }
        public int? TaxYear { get; set; }
        public string Treasurer { get; set; }
        public string TaxCollector { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? InterestDate { get; set; }
        public int? StartPage { get; set; }
        public int? EndPage { get; set; }
        public string County { get; set; }
        public int? TownNumber { get; set; }
        public DateTime? CollectionDate { get; set; }
        public double? InterestRate { get; set; }
        public double? Overlay { get; set; }
        public double? HomesteadReimbursement { get; set; }
        public DateTime? CommitmentDate { get; set; }
        public DateTime? InterestDate2 { get; set; }
        public DateTime? DueDate2 { get; set; }
        public int? Beteexempt { get; set; }
        public double? Betereimbursement { get; set; }
        public double? Reamount { get; set; }
        public double? BetereimburseValue { get; set; }
        public DateTime? InterestDate3 { get; set; }
        public DateTime? DueDate3 { get; set; }
        public DateTime? InterestDate4 { get; set; }
        public DateTime? DueDate4 { get; set; }
        public bool? UseEnhancedBete { get; set; }
        public double? Tifbeteval { get; set; }
        public double? TifmuniRetentionPerc { get; set; }
        public int Id { get; set; }
    }
}