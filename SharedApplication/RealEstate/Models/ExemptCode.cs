﻿namespace SharedApplication.RealEstate.Models
{
    public partial class ExemptCode
    {
        public int Id { get; set; }
        public int? Code { get; set; }
        public int? Amount { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public short? Category { get; set; }
        public short? Enlisted { get; set; }
        public short? ApplyTo { get; set; }
    }
}