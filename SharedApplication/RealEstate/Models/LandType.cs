﻿using SharedApplication.RealEstate.Enums;

namespace SharedApplication.RealEstate.Models
{
    public partial class LandType
    {
        public int Id { get; set; }
        public int? Code { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public int? Type { get; set; }
        public int? Category { get; set; }
        public double? Factor { get; set; }

        public LandCategory LandCategory()
        {
            return (LandCategory)(Category.GetValueOrDefault());
        }
    }
}