﻿namespace SharedApplication.RealEstate.Models
{
    public partial class REDefault
    {
        public double? OverallRate { get; set; }
        public double? SoftCost { get; set; }
        public double? MixedCost { get; set; }
        public double? HardCost { get; set; }
        public string TaxNoticeMsg1 { get; set; }
        public string TaxNoticeMsg2 { get; set; }
        public string TaxNoticeTitle { get; set; }
        public short? TownNumber { get; set; }
        public int Id { get; set; }
    }
}