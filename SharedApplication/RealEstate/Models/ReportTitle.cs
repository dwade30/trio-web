﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.RealEstate.Models
{
	public class ReportTitle
	{
		public int? ReportNumber { get; set; }
		public string Title { get; set; }
		public int? UserId { get; set; }
		public string Description { get; set; }
		public int Id { get; set; }
		public bool? MovedToClientSettings { get; set; }

	}
}
