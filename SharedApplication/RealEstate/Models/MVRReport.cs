﻿namespace SharedApplication.RealEstate.Models
{
    public partial class Mvrreport
    {
        public int? Code { get; set; }
        public string DataValue { get; set; }
        public int? TownNumber { get; set; }
        public int Id { get; set; }
    }
}