﻿using System;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace SharedApplication.RealEstate.Models
{
    public partial class REMaster
    {
        public int Id { get; set; }
        public int? LastBldgVal { get; set; }
        public int? LastLandVal { get; set; }
        public DateTime? HlUpdate { get; set; }
        public string HiupdCode { get; set; }
        public int? HlcompValBldg { get; set; }
        public int? HlcompValLand { get; set; }
        public int? HlvalBldg { get; set; }
        public int? HivalBldgCode { get; set; }
        public int? HlvalLand { get; set; }
        public int? HivalLandCode { get; set; }
        public int? Hlalt1Bldg { get; set; }
        public int? Hialt1BldgCode { get; set; }
        public int? Hlalt2Bldg { get; set; }
        public int? Hialt2BldgCode { get; set; }
        public int? Pineighborhood { get; set; }
        public int? PistreetCode { get; set; }
        public string Pixcoord { get; set; }
        public string Piycoord { get; set; }
        public int? Pizone { get; set; }
        public int? PisecZone { get; set; }
        public int? Pitopography1 { get; set; }
        public int? Pitopography2 { get; set; }
        public int? Piutilities1 { get; set; }
        public int? Piutilities2 { get; set; }
        public int? Pistreet { get; set; }
        public int? Piopen1 { get; set; }
        public int? Piopen2 { get; set; }
        public int? PisalePrice { get; set; }
        public int? PisaleType { get; set; }
        public int? PisaleFinancing { get; set; }
        public int? PisaleVerified { get; set; }
        public int? PisaleValidity { get; set; }
        public int? Hldolexemption { get; set; }
        public int? Piland1Type { get; set; }
        public string Piland1UnitsA { get; set; }
        public string Piland1UnitsB { get; set; }
        public double? Piland1Inf { get; set; }
        public int? Piland1InfCode { get; set; }
        public int? Piland2Type { get; set; }
        public string Piland2UnitsA { get; set; }
        public string Piland2UnitsB { get; set; }
        public double? Piland2Inf { get; set; }
        public int? Piland2InfCode { get; set; }
        public int? Piland3Type { get; set; }
        public string Piland3UnitsA { get; set; }
        public string Piland3UnitsB { get; set; }
        public double? Piland3Inf { get; set; }
        public int? Piland3InfCode { get; set; }
        public int? Piland4Type { get; set; }
        public string Piland4UnitsA { get; set; }
        public string Piland4UnitsB { get; set; }
        public double? Piland4Inf { get; set; }
        public int? Piland4InfCode { get; set; }
        public int? Piland5Type { get; set; }
        public string Piland5UnitsA { get; set; }
        public string Piland5UnitsB { get; set; }
        public double? Piland5Inf { get; set; }
        public int? Piland5InfCode { get; set; }
        public int? Piland6Type { get; set; }
        public string Piland6UnitsA { get; set; }
        public string Piland6UnitsB { get; set; }
        public double? Piland6Inf { get; set; }
        public int? Piland6InfCode { get; set; }
        public int? Piland7Type { get; set; }
        public string Piland7UnitsA { get; set; }
        public string Piland7UnitsB { get; set; }
        public double? Piland7Inf { get; set; }
        public int? Piland7InfCode { get; set; }
        public double? Piacres { get; set; }
        public string RspropertyCode { get; set; }
        public string RsdwellingCode { get; set; }
        public string RsoutbuildingCode { get; set; }
        public int? OwnerPartyId { get; set; }
        public int? SecOwnerPartyId { get; set; }
        public string RslocNumAlph { get; set; }
        public string RslocApt { get; set; }
        public string RslocStreet { get; set; }
        public string Rsref1 { get; set; }
        public string Rsref2 { get; set; }
        public int? RilandCode { get; set; }
        public int? RibldgCode { get; set; }
        public int? RllandVal { get; set; }
        public int? RlbldgVal { get; set; }
        public int? CorrExemption { get; set; }
        public int? Rlexemption { get; set; }
        public int? RiexemptCd1 { get; set; }
        public int? RiexemptCd2 { get; set; }
        public int? RiexemptCd3 { get; set; }
        public int? RitranCode { get; set; }
        public string RsmapLot { get; set; }
        public string RspreviousMaster { get; set; }
        public int? Rsaccount { get; set; }
        public int? Rscard { get; set; }
        public bool? Rsdeleted { get; set; }
        public DateTime? SaleDate { get; set; }
        public bool? TaxAcquired { get; set; }
        public double? Rssoft { get; set; }
        public double? Rshard { get; set; }
        public double? Rsmixed { get; set; }
        public int? RssoftValue { get; set; }
        public int? RshardValue { get; set; }
        public int? RsmixedValue { get; set; }
        public int? HomesteadValue { get; set; }
        public bool? InBankruptcy { get; set; }
        public int? ExemptVal1 { get; set; }
        public int? ExemptVal2 { get; set; }
        public int? ExemptVal3 { get; set; }
        public int? EntranceCode { get; set; }
        public int? InformationCode { get; set; }
        public DateTime? DateInspected { get; set; }
        public int? RsotherValue { get; set; }
        public double? Rsother { get; set; }
        public bool? HasHomestead { get; set; }
        public int? CurrLandExempt { get; set; }
        public int? CurrBldgExempt { get; set; }
        public int? LandExempt { get; set; }
        public int? BldgExempt { get; set; }
        public int? ExemptPct1 { get; set; }
        public int? ExemptPct2 { get; set; }
        public int? ExemptPct3 { get; set; }
        public int? CurrHomesteadValue { get; set; }
        public bool? CurrHasHomestead { get; set; }
        public int? CurrExemptVal1 { get; set; }
        public int? CurrExemptVal2 { get; set; }
        public int? CurrExemptVal3 { get; set; }
        public bool? RevocableTrust { get; set; }
        public bool? AccountLocked { get; set; }
        public string ReasonAccountLocked { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? TADate { get; set; }
        public int? PropertyCode { get; set; }
        public bool? ZoneOverride { get; set; }
        public string CardId { get; set; }
        public string AccountId { get; set; }
        public string DeedName1 { get; set; }
        public string DeedName2 { get; set; }

        public LandPiece GetLandPiece(int pieceNumber)
        {            
            switch (pieceNumber)
            {
                case 1:
                    return new LandPiece()
                    {
                        Influence = Piland1Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland1InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland1Type.GetValueOrDefault(),
                        UnitsA = Piland1UnitsA?.Trim() ?? "",
                        UnitsB = Piland1UnitsB?.Trim() ?? ""
                    };
                case 2:
                    return new LandPiece()
                    {
                        Influence = Piland2Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland2InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland2Type.GetValueOrDefault(),
                        UnitsA = Piland2UnitsA?.Trim() ?? "",
                        UnitsB = Piland2UnitsB?.Trim() ?? ""
                    };
                case 3:
                    return new LandPiece()
                    {
                        Influence = Piland3Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland3InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland3Type.GetValueOrDefault(),
                        UnitsA = Piland3UnitsA?.Trim() ?? "",
                        UnitsB = Piland3UnitsB?.Trim() ?? ""
                    };
                case 4:
                    return new LandPiece()
                    {
                        Influence = Piland4Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland4InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland4Type.GetValueOrDefault(),
                        UnitsA = Piland4UnitsA?.Trim() ?? "",
                        UnitsB = Piland4UnitsB?.Trim() ?? ""
                    };
                case 5:
                    return new LandPiece()
                    {
                        Influence = Piland5Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland5InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland5Type.GetValueOrDefault(),
                        UnitsA = Piland5UnitsA?.Trim() ?? "",
                        UnitsB = Piland5UnitsB?.Trim() ?? ""
                    };
                case 6:
                    return new LandPiece()
                    {
                        Influence = Piland6Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland6InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland6Type.GetValueOrDefault(),
                        UnitsA = Piland6UnitsA?.Trim() ?? "",
                        UnitsB = Piland6UnitsB?.Trim() ?? ""
                    };
                case 7:
                    return new LandPiece()
                    {
                        Influence = Piland7Inf.GetValueOrDefault().ToDecimal(),
                        InfluenceCode = Piland7InfCode.GetValueOrDefault(),
                        LandTypeCode = Piland7Type.GetValueOrDefault(),
                        UnitsA = Piland7UnitsA?.Trim() ?? "",
                        UnitsB = Piland7UnitsB?.Trim() ?? ""
                    };
            }

            return new LandPiece()
            {
                Influence = 0,
                InfluenceCode = 0,
                LandTypeCode = 0,
                UnitsA = "000",
                UnitsB = "000"
            };
        }

        public IEnumerable<LandPiece> GetLandPieces()
        {
            var landPieces = new List<LandPiece>();
            for (int x = 1; x < 8; x++)
            {
                var landPiece = GetLandPiece(x);
                if (landPiece.LandTypeCode > 0)
                {
                    landPieces.Add(landPiece);
                }
            }

            return landPieces;
        }
    }
}