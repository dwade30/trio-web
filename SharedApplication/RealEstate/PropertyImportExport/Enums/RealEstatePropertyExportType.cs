﻿namespace SharedApplication.RealEstate.PropertyImportExport.Enums
{
    public enum RealEstatePropertyExportType
    {
        Full = 0,
        Range = 1
    }
}