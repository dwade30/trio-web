﻿namespace SharedApplication.RealEstate.PropertyImportExport.Enums
{
    public enum RealEstatePropertyExportRangeType
    {
        Account = 0,
        Name = 1,
        MapLot = 2,
        Location = 3
    }
}