﻿using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class GetMunicipalValuationReturn : Command<MunicipalValuationReturn>
    {
        public int TownNumber { get;  }
        public bool CalculateCurrent { get; }
        public GetMunicipalValuationReturn(int townNumber,bool calculateCurrent)
        {
            TownNumber = townNumber;
            CalculateCurrent = calculateCurrent;
        }
    }
}