﻿namespace SharedApplication.RealEstate
{
    public class RealEstateBookPageQuery
    {
        public int Account { get; }
        public bool OnlyCurrent { get; }

        public RealEstateBookPageQuery(int account, bool onlyCurrent)
        {
            Account = account;
            OnlyCurrent = onlyCurrent;
        }
    }
}