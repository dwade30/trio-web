﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class CreatePictureRecord : Command<int>
    {
        public int Account { get; }
        public int Card { get; }
        public int SequenceNumber { get; }
        public Guid DocumentIdentifier { get; }
        public string FileName { get; }
        public string Description { get; }

        public CreatePictureRecord(int account, int card, int sequenceNumber, Guid documentIdentifier, string fileName,
            string description)
        {
            Account = account;
            Card = card;
            SequenceNumber = sequenceNumber;
            DocumentIdentifier = documentIdentifier;
            FileName = fileName;
            Description = description;
        }
    }
}