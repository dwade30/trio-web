﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CentralDocuments;

namespace SharedApplication.RealEstate
{
    public class PropertySketches
    {
        private List<SketchInfo> sketches = new List<SketchInfo>();
        private int currentSketchIndex = -1;
        private SketchInfo pendingSketch = null;
        public PropertySketches()
        {

        }
        public PropertySketches(IEnumerable<SketchInfo> sketchInfos)
        {
            AddSketchInfos(sketchInfos);
        }
        public IEnumerable<SketchInfo> Sketches
        {
            get => sketches.OrderBy(s => s.SequenceNumber).ToList();
        }

        public void AddSketchInfo(SketchInfo sketchInfo)
        {
            if (sketchInfo != null)
            {
                sketches.Add(sketchInfo);
            }
        }

        public void AddSketchInfos(IEnumerable<SketchInfo> sketchInfos)
        {
            sketches.AddRange(sketchInfos);
        }

        public SketchInfo CurrentSketch()
        {
            if (HasCurrentSketch())
            {
                return sketches[currentSketchIndex];
            }

            return null;
        }

        public SketchInfo GetSketchByIdentifier(Guid sketchIdentifier)
        {
            return sketches.FirstOrDefault(s => s.SketchIdentifier == sketchIdentifier);
        }
        public SketchInfo NextSketch()
        {
            ClearPendingSketch();
            if (OnLastSketch())
            {
                currentSketchIndex = -1;
                return null;
            }

            if (HasCurrentSketch())
            {
                currentSketchIndex += 1;
                return CurrentSketch();
            }

            return null;
        }

        public SketchInfo PreviousSketch()
        {
            ClearPendingSketch();
            if (OnFirstSketch())
            {
                currentSketchIndex = -1;
                return null;
            }

            if (HasCurrentSketch())
            {
                currentSketchIndex -= 1;
                return CurrentSketch();
            }

            return null;
        }

        public bool OnLastSketch()
        {
            if (HasCurrentSketch() && currentSketchIndex == sketches.Count - 1)
            {
                return true;
            }

            return false;
        }

        public bool OnFirstSketch()
        {
            if (HasCurrentSketch() && currentSketchIndex == 0)
            {
                return true;
            }

            return false;
        }

        public bool HasCurrentSketch()
        {
            if (currentSketchIndex >= 0 && currentSketchIndex <= sketches.Count - 1)
            {
                return true;
            }

            return false;
        }

        public void SetToFirstSketch()
        {
            ClearPendingSketch();
            if (sketches.Count > 0)
            {
                currentSketchIndex = 0;
                return;
            }

            currentSketchIndex = -1;
        }

        public void SetToLastSketch()
        {
            ClearPendingSketch();
            currentSketchIndex = sketches.Count - 1;
        }
        public int Count()
        {
            return sketches.Count;
        }

        public bool HasPendingSketch()
        {
            return pendingSketch != null;
        }

        public SketchInfo GetPendingSketch()
        {
            return pendingSketch;
        }

        public void ClearPendingSketch()
        {
            pendingSketch = null;
        }
        public void AddPendingSketch(SketchInfo sketchInfo)
        {
            pendingSketch = sketchInfo;
        }



        public bool HasSketch(Guid sketchIdentifier)
        {
            return sketches.Any(s => s.SketchIdentifier == sketchIdentifier);
        }
    }
}