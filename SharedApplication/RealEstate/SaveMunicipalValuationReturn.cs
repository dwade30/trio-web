﻿using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class SaveMunicipalValuationReturn : Command
    {
        public MunicipalValuationReturn MunicipalValuationReturn { get; }
        public SaveMunicipalValuationReturn(MunicipalValuationReturn municipalValuationReturn)
        {
            this.MunicipalValuationReturn = municipalValuationReturn;
           
        }

        
    }
}