﻿using System;

namespace SharedApplication.RealEstate
{
    public interface IPropertySketchesViewModel
    {
        PropertySketches Sketches { get; }
        Guid CardIdentifier { get; }
        void LoadSketches(int account, int card,Guid cardIdentifier);
        byte[] GetCurrentSketch();
        byte[] GetCurrentSketchImage();
        void DeleteCurrentSketch();
        void AddNewSketch(Guid sketchIdentifier, int sequenceNumber, string calculations,string mediaType, byte[] sketchImage, byte[] sketchData);
        void UpdateSketch(Guid sketchIdentifier, string calculations, string mediaType, byte[] sketchImage, byte[] sketchData);        
    }
}