﻿using Autofac;
using SharedApplication.RealEstate.MVR;

namespace SharedApplication.RealEstate.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PropertyPicturesViewModel>().As<IPropertyPicturesViewModel>();
            builder.RegisterType<PropertySketchesViewModel>().As<IPropertySketchesViewModel>();
            builder.RegisterType<MunicipalValuationReturnViewModel>().As<IMunicipalValuationReturnViewModel>();
            builder.RegisterType<MunicipalValuationReportViewModel>().As<IMunicipalValuationReportViewModel>();
        }
    }
}