﻿namespace SharedApplication.RealEstate
{
    public class RealEstateImageFileImport
    {
        public string DataEnvironment { get; set; } = "";
        public int Account { get; set; } = 0;
        public int Card { get; set; } = 0;
        public string MapLot { get; set; } = "";
        public string Filename { get; set; } = "";
        public byte[] ImageData { get; set; }
        public string MediaType { get; set; } = "";
        public string Description { get; set; } = "";
    }
}