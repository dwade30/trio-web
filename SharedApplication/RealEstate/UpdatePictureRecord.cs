﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class UpdatePictureRecord : Command
    {
        public int Id { get; }
        public string Description { get; }
        public int SequenceNumber { get; }
        public int Account { get; }
        public int Card { get; }
        public Guid DocumentIdentifier { get; }
        public UpdatePictureRecord(int id, int account,int card, string description, int orderNumber, Guid documentIdentifier)
        {
            Id = id;
            Description = description;
            SequenceNumber = orderNumber;
            Account = account;
            Card = card;
            DocumentIdentifier = documentIdentifier;
        }
    }
}