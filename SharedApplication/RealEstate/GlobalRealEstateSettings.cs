﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.RealEstate
{
    public class GlobalRealEstateSettings
    {
        public decimal CertifiedRatio { get; set; } = 0;
        public PersonalPropertyTaxCategories PersonalPropertyCategories { get; set; } = new PersonalPropertyTaxCategories();
        public List<int> IndustrialBuildingCodes { get; set; } = new List<int>();
        public int PersonalPropertyIndustrialCategory { get; set; } = 0;
        public bool ApplyCertifiedRatioToTreeGrowthRates { get; set; } = false;
    }

    public class PersonalPropertyTaxCategories
    {
        private Dictionary<int,PersonalPropertyMVRCategory> categories = new Dictionary<int, PersonalPropertyMVRCategory>();

        public PersonalPropertyMVRCategory GetCategoryType(int category)
        {
            if (categories.ContainsKey(category))
            {
                return categories[category];
            }
            return PersonalPropertyMVRCategory.Other;
        }

        public void SetCategoryType(int category, PersonalPropertyMVRCategory categoryType)
        {
            categories[category] = categoryType;
        }

        public IEnumerable<int> OtherCategories()
        {
            return categories.Where(c => c.Value == PersonalPropertyMVRCategory.Other).Select(c => c.Key);
        }

        public IEnumerable<int> MachineryAndEquipmentCategories()
        {
            return categories.Where(c => c.Value == PersonalPropertyMVRCategory.MachineryAndEquipment).Select(c => c.Key);
        }

        public IEnumerable<int> BusinessEquipmentCategories()
        {
            return categories.Where(c => c.Value == PersonalPropertyMVRCategory.BusinessEquipment).Select(c => c.Key);
        }
    }

    public enum PersonalPropertyMVRCategory
    {
        Other = 0,
        MachineryAndEquipment = 1,
        BusinessEquipment = 2
    }
}