﻿namespace SharedApplication.RealEstate.Enums
{
    public enum LandUnitType
    {
        FrontFoot = 0,
        SquareFeet = 1,
        FractionalAcreage = 2,
        Acres = 3,
        ExtraInfluence = 4,
        Site = 5,
        LinearFeet = 6,
        Improvements = 7,
        FrontFootScaled = 8,
        LinearFeetScaled = 9
    }

    
}