﻿namespace SharedApplication.RealEstate.Enums
{
    public enum LandCategory
    {
        None = 0,
        Softwood = 1,
        Mixedwood = 2,
        Hardwood = 3,
        SoftwoodFarm = 4,
        MixedwoodFarm = 5,
        HardwoodFarm = 6,
        CropLand = 7,
        Orchard = 8,
        Pasture = 9,
        OpenSpace = 10,
        Waterfront = 11
    }
}