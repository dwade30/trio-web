﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class RealEstateAccountExists : Command<bool>
    {
        public int Account { get; set; }

        public RealEstateAccountExists(int account)
        {
            Account = account;
        }
    }
}