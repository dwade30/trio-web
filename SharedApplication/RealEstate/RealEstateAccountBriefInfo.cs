﻿namespace SharedApplication.RealEstate
{
    public class RealEstateAccountBriefInfo
    {
        public int Account { get; set; } = 0;
        public string MapLot { get; set; } = "";
        public string Reference1 { get; set; } = "";
        public string Reference2 { get; set; } = "";
        public string DeedName1 { get; set; } = "";
        public string DeedName2 { get; set; } = "";
        public int LocationNumber { get; set; } = 0;
        public string LocationStreet { get; set; } = "";
        public int PartyId1 { get; set; } = 0;
        public int PartyId2 { get; set; } = 0;
       // public decimal Acreage { get; set; } = 0;
        public bool Deleted { get; set; } = false;
        public bool IsTaxAcquired { get; set; } = false;
        public int TownId { get; set; } = 0;
    }
}