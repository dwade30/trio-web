﻿using System;
using System.Collections.Generic;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class GetSketchInfos : Command<IEnumerable<SketchInfo>>
    {
        public Guid CardIdentifier { get; }
        public GetSketchInfos( Guid cardIdentifier)
        {
            CardIdentifier = cardIdentifier;
        }
    }
}