﻿namespace SharedApplication.RealEstate
{
    public class RealEstateAccountTotalsQuery
    {
        public int Account { get; set; } = 0;

        public RealEstateAccountTotalsQuery(int account)
        {
            Account = account;
        }
    }
}