﻿using SharedApplication.RealEstate.Extensions;

namespace SharedApplication.RealEstate
{
    public abstract class LandPieceBase
    {
        public int LandTypeCode { get; set; } = 0;

        public bool IsTreeGrowth(LandTypes landTypes)
        {
            var landType = landTypes.GetType(LandTypeCode);
            return landType?.IsTreeGrowth() ?? false;
        }

        public bool IsOpenSpace(LandTypes landTypes)
        {
            var landType = landTypes.GetType(LandTypeCode);
            return landType?.IsOpenSpace() ?? false;
        }

        public bool IsTillable(LandTypes landTypes)
        {
            var landType = landTypes.GetType(LandTypeCode);
            return landType?.IsTillable() ?? false;
        }

        public bool IsWaterfront(LandTypes landTypes)
        {
            var landType = landTypes.GetType(LandTypeCode);
            return landType?.IsWaterfront() ?? false;
        }
    }
}