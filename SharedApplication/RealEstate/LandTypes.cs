﻿using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class LandTypes
    {
        private List<LandType> landTypes = new List<LandType>();

        public void SetType(LandType landType)
        {
            LandType originalLandType = landTypes.FirstOrDefault(l => l.Code == landType.Code);
            if (originalLandType == null)
            {
                originalLandType = new LandType(){Code = landType.Code,Id = landType.Id};
                landTypes.Add(originalLandType);
            }

            originalLandType.Category = landType.Category;
            originalLandType.Description = landType.Description;
            originalLandType.Factor = landType.Factor;
            originalLandType.ShortDescription = landType.ShortDescription;
            originalLandType.Type = landType.Type;
        }

        public void SetRangeTypes(IEnumerable<LandType> landTypeRange)
        {
            foreach (var landType in landTypeRange)
            {
                SetType(landType);
            }
        }

        public LandType GetType(int landTypeCode)
        {
            return landTypes.FirstOrDefault(l => l.Code == landTypeCode);
        }
    }
}