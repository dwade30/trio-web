﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.RealEstate
{
    public class MunicipalValuationReturn
    {
	    private decimal certifiedRatio = 0;

        //private Dictionary<int,string> mvrValues = new Dictionary<int, string>();
        //public void SetValueForCode(int code, string value)
        //{
        //    mvrValues.Add(code,value);
        //}

        //public string GetValueForCode(int code)
        //{
        //    if (mvrValues.ContainsKey(code))
        //    {
        //        return mvrValues[code];
        //    }

        //    return "";
        //}

        //public string GetValueForCode(MunicipalValuationCode code)
        //{
        //    return GetValueForCode((int) code);
        //}
        public int TownNumber { get; set; } = 0;
        public MunicipalValuationRealEstate RealEstate { get; private set; } = new MunicipalValuationRealEstate();

        public MunicipalValuationPersonalProperty PersonalProperty { get; private set; } =
            new MunicipalValuationPersonalProperty();

        public MunicipalValuationHomestead Homestead { get; private set; } = new MunicipalValuationHomestead();

        public MunicipalValuationOtherTaxInformation OtherTaxInformation { get; private set; } =
            new MunicipalValuationOtherTaxInformation();

        public MunicipalValuationBete Bete { get; private set; } = new MunicipalValuationBete();
        public MunicipalValuationTIF TIF { get; private set; } = new MunicipalValuationTIF();
        public MunicipalValuationExcise Excise { get; private set; } = new MunicipalValuationExcise();

        public MunicipalValuationElectricalGeneration ElectricalGeneration { get; private set; } =
            new MunicipalValuationElectricalGeneration();

        public MunicipalValuationForest ForestTreeGrowth { get; private set; } = new MunicipalValuationForest();

        public MunicipalValuationFarmAndOpenSpace FarmAndOpenSpace { get; private set; } =
            new MunicipalValuationFarmAndOpenSpace();

        public MunicipalValuationWaterfront Waterfront { get; private set; } = new MunicipalValuationWaterfront();

        public MunicipalValuationExemptProperty ExemptProperty { get; private set; } =
            new MunicipalValuationExemptProperty();

        public MunicipalValuationVeteranExemptions VeteranExemptions { get; private set; } =
            new MunicipalValuationVeteranExemptions();

        public MunicipalValuationMunicipalRecords MunicipalRecords { get; private set; } =
            new MunicipalValuationMunicipalRecords();

        public MunicipalValuationInformation ValuationInformation { get; private set; } =
            new MunicipalValuationInformation();

        public DateTime CommitmentDate { get; set; }
        public string County { get; set; } = "";
        public string Municipality { get; set; } = "";
        public decimal CertifiedRatio
        {
	        get => certifiedRatio;
	        set
	        {
		        certifiedRatio = value;
		        Homestead.CertifiedRatio = value;
	        }
        }
        public int ReportYear { get; set; } = 0;
    }

    public class MunicipalValuationRealEstate
    {
        public int TaxableLand { get; set; } = 0;
        public int TaxableBuilding { get; set; } = 0;

        public int TaxableRealEstate
        {
            get => TaxableBuilding + TaxableLand;
        }
    }

    public class MunicipalValuationPersonalProperty
    {
        public int ProductionMachineryAndEquipment { get; set; } = 0;
        public int BusinessEquipment { get; set; } = 0;
        public int OtherPersonalProperty { get; set; } = 0;

        public int TaxablePersonalProperty
        {
            get => ProductionMachineryAndEquipment + BusinessEquipment + OtherPersonalProperty;
        }
    }

    public class MunicipalValuationOtherTaxInformation
    {
        public decimal TaxRate { get; set; } = 0;
        public decimal TaxLevy { get; set; } = 0;
    }

    public class MunicipalValuationHomestead
    {
	    public decimal CertifiedRatio { get; set; } = 0;
        public int NumberOfFullHomesteadsGranted { get; set; } = 0;

        public decimal TotalValueOfFullHomesteadExemptions
        {
            get => NumberOfFullHomesteadsGranted * 25000 * (CertifiedRatio / 100);
        }

        public int NumberOfFullyExemptedHomesteads { get; set; } = 0;
        public decimal TotalValueOfFullyExemptedHomesteads { get; set; } = 0;

        public int TotalNumberOfHomesteadExemptions
        {
            get => NumberOfFullHomesteadsGranted + NumberOfFullyExemptedHomesteads;
        }

        public decimal TotalExemptValueOfAllHomesteads
        {
            get => TotalValueOfFullHomesteadExemptions + TotalValueOfFullyExemptedHomesteads;
        }

        public int TotalAssessedValueOfAllHomesteadProperty { get; set; } = 0;
    }

    public class MunicipalValuationBete
    {
        public int ApplicationsProcessed { get; set; } = 0;
        public int ApplicationsApproved { get; set; } = 0;
        public int TotalBeteQualifiedExempt { get; set; } = 0;
        public int TotalBeteInTIF { get; set; } = 0;
    }

    public class MunicipalValuationTIF
    {
        public int TifIncrease { get; set; } = 0;
        public int CapturedAssessedValue { get; set; } = 0;
        public decimal RevenueDeposited { get; set; } = 0;
        public decimal BeteRevenueDeposited { get; set; } = 0;
    }

    public class MunicipalValuationExcise
    {
        public bool Fiscal { get; set; } = false;
        public decimal MVExcise { get; set; } = 0;
        public decimal WatercraftExcise { get; set; } = 0;
    }

    public class MunicipalValuationElectricalGeneration
    {
        public int DistributionAndTransmissionLines { get; set; } = 0;
        public int GenerationFacilities { get; set; } = 0;
    }

    public class MunicipalValuationForest
    {
        public decimal AveragePerAcreUnitValue { get; set; } = 0;
        public int NumberOfParcelsClassified { get; set; } = 0;
        public decimal SoftwoodAcreage { get; set; } = 0;
        public decimal MixedwoodAcreage { get; set; } = 0;
        public decimal HardwoodAcreage { get; set; } = 0;

        public decimal TotalAcres
        {
            get => SoftwoodAcreage + MixedwoodAcreage + HardwoodAcreage;
        }

        public decimal TotalAssessedValuation { get; set; } = 0;
        public decimal SoftwoodRate { get; set; } = 0;
        public decimal MixedWoodRate { get; set; } = 0;
        public decimal HardwoodRate { get; set; } = 0;

        public decimal NumberOfAcresFirstClassifiedForTaxYear { get; set; } = 0;
        public int NumberOfParcelsWithdrawn { get; set; } = 0;
        public decimal NumberOfAcresWithdrawn { get; set; } = 0;
        public decimal TotalAmountOfPenaltiesAssessed { get; set; } = 0;
        public int TotalNumberOfNonCompliancePenalties { get; set; } = 0;
        public bool TreeGrowthHasBeenTransferredToFarmlandThisYear { get; set; } = false;
    }

    public class MunicipalValuationFarmAndOpenSpace
    {
        public int NumberOfParcelsClassifiedAsFarmland { get; set; } = 0;
        public decimal NumberOfAcresClassifiedAsFarmlandForTaxYear { get; set; } = 0;
        public decimal TotalNumberOfAcresOfAllFarmland { get; set; } = 0;
        public decimal TotalValuationOfAllFarmland { get; set; } = 0;
        public decimal NumberOfFarmSoftwoodAcres { get; set; } = 0;
        public decimal NumberOfFarmMixedWoodAcres { get; set; } = 0;
        public decimal NumberOfFarmHardwoodAcres { get; set; } = 0;
        public decimal TotalNumberOfFarmWoodlandAcres { get; set; } = 0;
        public decimal TotalValuationOfFarmWoodland { get; set; } = 0;
        public decimal FarmSoftwoodRate { get; set; } = 0;
        public decimal FarmMixedWoodRate { get; set; } = 0;
        public decimal FarmHardwoodRate { get; set; } = 0;
        public int TotalNumberOfFarmlandParcelsWithdrawn { get; set; } = 0;
        public decimal TotalNumberOfFarmlandAcresWithdrawn { get; set; } = 0;
        public decimal TotalAmountOfPenaltiesForWithdrawnFarmland { get; set; } = 0;
        public int NumberOfOpenSpaceParcelsFirstClassified { get; set; } = 0;
        public decimal NumberOfOpenSpaceAcresFirstClassified { get; set; } = 0;
        public decimal TotalAcresOfOpenSpaceFirstClassified { get; set; } = 0;
        public decimal TotalValuationOfAllOpenSpace { get; set; } = 0;
        public int TotalNumberOfOpenSpaceParcelsWithdrawn { get; set; } = 0;
        public decimal TotalNumberOfOpenSpaceAcresWithdrawn { get; set; } = 0;
        public decimal TotalAmountOfPenaltiesForWithdrawnOpenSpace { get; set; } = 0;
    }

    public class MunicipalValuationWaterfront
    {
        public int NumberOfParcelsFirstClassified { get; set; } = 0;
        public decimal NumberOfAcresFirstClassified { get; set; } = 0;
        public decimal TotalAcresOfWaterfront { get; set; } = 0;
        public decimal TotalValuationOfWaterfront { get; set; } = 0;
        public int NumberOfWaterfrontParcelsWithdrawn { get; set; } = 0;
        public decimal NumberOfWaterfrontAcresWithdrawn { get; set; } = 0;
        public decimal TotalAmountOfPenalties { get; set; } = 0;
    }

    public class MunicipalValuationExemptProperty
    {
        public decimal UnitedStates { get; set; } = 0;
        public decimal StateOfMaine { get; set; } = 0;
        public decimal NewhampshireWater { get; set; } = 0;
        public decimal Municipal { get; set; } = 0;
        public decimal MunicipalUtilities { get; set; } = 0;
        public decimal MunicipalAirport { get; set; } = 0;
        public decimal PrivateAirport { get; set; } = 0;
        public decimal MunicipalSewage { get; set; } = 0;
        public decimal Benevolent { get; set; } = 0;
        public decimal LiteraryAndScientific { get; set; } = 0;
        public decimal VeteranOrganizations { get; set; } = 0;
        public decimal VeteranOrgReimbursableExemption { get; set; } = 0;
        public decimal ChamberOfCommerce { get; set; } = 0;
        public int NumberOfParsonages { get; set; } = 0;
        public decimal ExemptValueOfParsonages { get; set; } = 0;
        public decimal TaxableValueOfParsonages { get; set; } = 0;
        public decimal ReligiousWorship { get; set; } = 0;
        public decimal FraternalOrganizations { get; set; } = 0;
        public decimal HospitalPersonalProperty { get; set; } = 0;
        public decimal LegallyBlind { get; set; } = 0;
        public decimal WaterSupplyTransport { get; set; } = 0;
        public decimal CertifiedAnimalWasteStorage { get; set; } = 0;
        public decimal PollutionControlFacilities { get; set; } = 0;
        public decimal SnowmobileGroomingEquipment { get; set; } = 0;
        public int SolarAndWindApplicationsProcessed { get; set; } = 0;
        public int SolarAndWindApplicationsApproved { get; set; } = 0;
        public decimal SolarAndWindEquipment { get; set; } = 0;
        public List<ExemptOrganization> ExemptOrganizations { get; private set; } = new List<ExemptOrganization>();

        public decimal TotalExempt {
            get
            {
                return ExemptOrganizations.Sum(o => o.ExemptValue) + SolarAndWindEquipment + SnowmobileGroomingEquipment
                       + PollutionControlFacilities + CertifiedAnimalWasteStorage + WaterSupplyTransport
                       + LegallyBlind + HospitalPersonalProperty + FraternalOrganizations
                       + ReligiousWorship + ExemptValueOfParsonages + ChamberOfCommerce
                       + VeteranOrganizations + VeteranOrgReimbursableExemption + LiteraryAndScientific
                       + Benevolent + MunicipalSewage + PrivateAirport
                       + MunicipalAirport + MunicipalUtilities + Municipal
                       + NewhampshireWater + StateOfMaine + UnitedStates;
            }
        }
    }

    public class ExemptOrganization
    {
        public string Name { get; set; } = "";
        public string ProvisionOfLaw { get; set; } = "";
        public decimal ExemptValue { get; set; } = 0;
    }

    public class VeteranExemptionStatistic
    {
        public int Count { get; set; } = 0;
        public decimal ExemptValue { get; set; } = 0;
    }

    public class MunicipalValuationVeteranExemptions
    {
        public VeteranExemptionStatistic SurvivingMale { get; private set; } = new VeteranExemptionStatistic();

        public VeteranExemptionStatistic ParaplegicRevocableTrusts { get; private set; } =
            new VeteranExemptionStatistic();

        public VeteranExemptionStatistic MiscRevocableTrust { get; private set; } = new VeteranExemptionStatistic();

        public VeteranExemptionStatistic MaineEnlistedWWIVeteran { get; private set; } =
            new VeteranExemptionStatistic();

        public VeteranExemptionStatistic NonResidentWWIVeteran { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic ParaplegicVeteran { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic COOPHousing { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic MiscMaineEnlisted { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic MiscNonResident { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic DisabledInLineOfDuty { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic LebanonPanama { get; private set; } = new VeteranExemptionStatistic();
        public VeteranExemptionStatistic EarlyVietnamConflict { get; private set; } = new VeteranExemptionStatistic();
        
        public VeteranExemptionStatistic Totals {
            get
            {
                return new VeteranExemptionStatistic()
                {
                    Count = EarlyVietnamConflict.Count + LebanonPanama.Count + DisabledInLineOfDuty.Count 
                            + MiscNonResident.Count + MiscMaineEnlisted.Count + COOPHousing.Count
                            + ParaplegicVeteran.Count + NonResidentWWIVeteran.Count + MaineEnlistedWWIVeteran.Count
                            + MiscRevocableTrust.Count + ParaplegicRevocableTrusts.Count + SurvivingMale.Count,
                    ExemptValue = EarlyVietnamConflict.ExemptValue + LebanonPanama.ExemptValue + DisabledInLineOfDuty.ExemptValue
                                  + MiscNonResident.ExemptValue + MiscMaineEnlisted.ExemptValue + COOPHousing.ExemptValue
                                  + ParaplegicVeteran.ExemptValue + NonResidentWWIVeteran.ExemptValue + MaineEnlistedWWIVeteran.ExemptValue
                                  + MiscRevocableTrust.ExemptValue + ParaplegicRevocableTrusts.ExemptValue + SurvivingMale.ExemptValue
                };
            }
        }

    }

    public enum MVRTaxMapType
    {
        None = 0,
        Paper = 1,
        GIS = 2,
        CAD = 3
    }

    public class MunicipalValuationMunicipalRecords
    {
        public bool HasTaxMaps { get; set; } = false;
        public DateTime? DateMapsOriginallyObtained { get; set; } = null;
        public string NameOfOriginalContractor { get; set; } = "";
        public MVRTaxMapType TaxMapType { get; set; } = MVRTaxMapType.None;
        public int NumberOfParcelsInMunicipality { get; set; } = 0;
        public decimal TotalTaxableAcreage { get; set; } = 0;
        public bool ProfessionalRevaluationWasCompleted { get; set; } = false;
        public bool RevaluationIncludedLand { get; set; } = false;
        public bool RevaluationIncludedBuildings { get; set; } = false;
        public bool RevaluationIncludedPersonalProperty { get; set; } = false;
        public DateTime? RevaluationEffectiveDate { get; set; } = null;
        public string RevaluationContractorName { get; set; } = "";
        public decimal RevaluationCost { get; set; } = 0;
        public DateTime? SignatureDate { get; set; } = null;

        public MVRMunicipalityAssessmentFunction AssessmentFunction { get; set; } =
            MVRMunicipalityAssessmentFunction.None;

        public string AssessorOrAgentName { get; set; } = "";
        public string AssessorOrAgentEmail { get; set; } = "";
        public DateTime? FiscalYearStart { get; set; } = null;
        public DateTime? FiscalYearEnd { get; set; } = null;
        public decimal OverDueInterestRate { get; set; } = 0;
        public DateTime? Period1DueDate { get; set; } = null;
        public DateTime? Period2DueDate { get; set; } = null;
        public DateTime? Period3DueDate { get; set; } = null;
        public DateTime? Period4DueDate { get; set; } = null;
        public bool AssessmentRecordsAreComputerized { get; set; } = true;
        public string AssessmentSoftware { get; set; } = "TRIO Software";
        public bool ImplementedATaxReliefProgram { get; set; } = false;
        public int NumberOfPeopleQualifiedForTaxProgram { get; set; } = 0;
        public decimal AmountOfReliefGrantedForTaxProgram { get; set; } = 0;
        public bool ImplementedElderlyTaxCredit { get; set; } = false;
        public int NumberOfPeopleQualifiedForElderlyCredit { get; set; } = 0;
        public decimal AmountOfReliefGrantedForElderlyCredit { get; set; } = 0;
        public bool ImplementedSeniorTaxDeferral { get; set; } = false;
        public int NumberOfPeopleQualifiedForSeniorTaxDeferral { get; set; } = 0;
        public decimal AmountOfReliefGrantedForSeniorTaxDeferral { get; set; } = 0;
    }

    public enum MVRMunicipalityAssessmentFunction
    {
        None = 0,
        SingleAssessor = 1,
        AssessorsAgent = 2,
        BoardOfAssessors = 3
    }

  public class ValuationInformationStats
    {
        public int OneFamily { get; set; } = 0;
        public int TwoFamily { get; set; } = 0;
        public int ThreeToFourFamily { get; set; } = 0;
        public int FiveFamilyPlus { get; set; }
        public int MobileHomes { get; set; } = 0;
        public int SeasonalHomes { get; set; } = 0;
    }

    public class ValuationInformationValues
    {
        public decimal OneFamily { get; set; } = 0;
        public decimal TwoFamily { get; set; } = 0;
        public decimal ThreeToFourFamily { get; set; } = 0;
        public decimal FiveFamilyPlus { get; set; }
        public decimal MobileHomes { get; set; } = 0;
        public decimal SeasonalHomes { get; set; } = 0;
    }

    public class MunicipalValuationInformation
    {
        public ValuationInformationStats New { get; set; } = new ValuationInformationStats();
        public ValuationInformationStats Demolished { get; set; } = new ValuationInformationStats();
        public ValuationInformationStats Converted { get; set; } = new ValuationInformationStats();

        public ValuationInformationValues ValuationIncrease { get; set; } = new ValuationInformationValues();
        public ValuationInformationValues ValuationDecrease { get; set; } = new ValuationInformationValues();
        public ValuationInformationValues ValuationNet {
            get
            {
                return new ValuationInformationValues()
                {
                    OneFamily = ValuationIncrease.OneFamily - ValuationDecrease.OneFamily,
                    TwoFamily =  ValuationIncrease.TwoFamily - ValuationDecrease.TwoFamily,
                    ThreeToFourFamily = ValuationIncrease.ThreeToFourFamily - ValuationDecrease.ThreeToFourFamily,
                    FiveFamilyPlus = ValuationIncrease.FiveFamilyPlus - ValuationDecrease.FiveFamilyPlus,
                    MobileHomes = ValuationIncrease.MobileHomes - ValuationDecrease.MobileHomes,
                    SeasonalHomes = ValuationIncrease.SeasonalHomes - ValuationDecrease.SeasonalHomes
                };
            }
        }
        public List<String> IndustrialMercantileGrowth { get; set; } = new List<String>();
        public List<String> ExtremeLosses { get; set; } = new List<String>();
        public List<String> GeneralValuationChange { get; set; } = new List<String>();
    }
}