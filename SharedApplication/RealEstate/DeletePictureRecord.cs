﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class DeletePictureRecord : Command
    {
        public int Id { get; }

        public DeletePictureRecord(int id)
        {
            Id = id;
        }
    }
}