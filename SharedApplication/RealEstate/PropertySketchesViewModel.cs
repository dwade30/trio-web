﻿using System;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class PropertySketchesViewModel : IPropertySketchesViewModel
    {
        private CommandDispatcher commandDispatcher;
        private int account = 0;
        private int card = 0;
        private Guid cardIdentifier;
        public PropertySketches Sketches { get; private set; } = new PropertySketches();

        public Guid CardIdentifier
        {
            get => cardIdentifier;
        }
        public PropertySketchesViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }
        public void LoadSketches(int account, int card, Guid cardIdentifier)
        {
            this.account = account;
            this.card = card;
            this.cardIdentifier = cardIdentifier;
            var sketches = commandDispatcher.Send(new GetSketchInfos(cardIdentifier)).Result;
            Sketches = sketches.ToPropertySketches();
            Sketches.SetToFirstSketch();
        }

        public byte[] GetCurrentSketch()
        {
            if (Sketches.HasCurrentSketch())
            {
                var sketch = Sketches.CurrentSketch();
                var sketchData = commandDispatcher.Send(new GetSketchData(sketch.Id)).Result;
                return sketchData;
            }

            return null;
        }

        public byte[] GetCurrentSketchImage()
        {
            if (Sketches.HasCurrentSketch())
            {
                var sketch = Sketches.CurrentSketch();
                var sketchImage = commandDispatcher.Send(new GetSketchImage(sketch.Id)).Result;
                return sketchImage;
            }

            return null;
        }

        public void DeleteCurrentSketch()
        {
            if (Sketches.HasCurrentSketch())
            {
                var sketch = Sketches.CurrentSketch();
                commandDispatcher.Send(new DeleteSketch(sketch.Id));
                commandDispatcher.Send(new RenumberSketches(cardIdentifier));
            }
        }

        public void UpdateSketch(Guid sketchIdentifier,  string calculations, string mediaType, byte[] sketchImage,
            byte[] sketchData)
        {
            var dateUpdated = DateTime.Now;
            var sketch = Sketches.GetSketchByIdentifier(sketchIdentifier);
            if (sketch == null)
            {
                return;
            }
            commandDispatcher.Send(new UpdateSketchDocument(sketch.Id,cardIdentifier,sketchIdentifier,sketch.SequenceNumber.GetValueOrDefault(),dateUpdated,calculations,mediaType,sketchImage,sketchData));
            sketch.Calculations = calculations;
            sketch.DateUpdated = dateUpdated;
            sketch.MediaType = mediaType;            
        }

        public void AddNewSketch(Guid sketchIdentifier, int sequenceNumber, string calculations, string mediaType, byte[] sketchImage,
            byte[] sketchData)
        {
            var dateUpdated = DateTime.Now;
            var id = commandDispatcher.Send(new CreateSketchDocument( cardIdentifier, sketchIdentifier, sequenceNumber, dateUpdated, calculations, mediaType, sketchImage, sketchData)).Result;
            if (id > 0)
            {
                Sketches.AddSketchInfo(new SketchInfo()
                {
                    Account = account,
                    Card = card,
                    Calculations = calculations,
                    CardIdentifier = cardIdentifier,
                    DateUpdated = dateUpdated,
                    Id = id,
                    SequenceNumber = sequenceNumber,
                    SketchIdentifier = sketchIdentifier,
                    MediaType = mediaType
                });
            }
        }
    }
}