﻿using System;

namespace SharedApplication.RealEstate
{
    public interface IPropertyPicturesViewModel
    {
        PropertyPictures Pictures { get; }
        void LoadPictures(int account, int card, Guid cardIdentifier);
        byte[] GetCurrentPicture();
        void SaveCurrentPicture();
        void DeleteCurrentPicture();

        void AddNewPicture(string description, int sequenceNumber, Guid documentIdentifier, string fileName,
            byte[] imageData, string mediaType);
    }
}