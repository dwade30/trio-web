﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class ShowMunicipalValuationReturn : Command
    {
        public int TownId { get; set; } = 0;

        public ShowMunicipalValuationReturn(int townId)
        {
            TownId = townId;
        }
    }
}