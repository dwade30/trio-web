﻿namespace SharedApplication.RealEstate
{
    public class RealEstateAccountTotals
    {
        public int Account { get; set; } = 0;
        public int Land { get; set; } = 0;
        public int Building { get; set; } = 0;
        public int Exemptions { get; set; } = 0;
        public decimal Acreage { get; set; } = 0;
        
        public int TotalValuation()
        {
            return Land + Building;
        }

        public int NetTaxable()
        {
            return TotalValuation() - Exemptions;
        }
    }
}