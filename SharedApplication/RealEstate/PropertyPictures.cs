﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class PropertyPictures
    {
        private List<PictureRecord> pictures = new List<PictureRecord>();
        private int currentPictureIndex = -1;
        public PropertyPictures()
        {

        }

        public PropertyPictures(IEnumerable<PictureRecord> pictureRecords)
        {
            AddPictures(pictureRecords);
        }
        public IEnumerable<PictureRecord> Pictures
        {
            get => pictures.OrderBy(p => p.PicNum).ToList();
        }
        public void AddPicture(PictureRecord picture)
        {
            if (picture != null)
            {
                pictures.Add(picture);
            }
        }

        public void AddPictures(IEnumerable<PictureRecord> pictureRecords)
        {
            pictures.AddRange(pictureRecords);
        }

        public PictureRecord CurrentPicture()
        {
            if (HasCurrentPicture())
            {
                return pictures[currentPictureIndex];
            }

            return null;
        }

        public PictureRecord NextPicture()
        {
            if (OnLastPicture())
            {
                currentPictureIndex = -1;
                return null;
            }

            if (HasCurrentPicture())
            {
                currentPictureIndex += 1;
                return CurrentPicture();
            }

            return null;
        }

        public PictureRecord PreviousPicture()
        {
            if (OnFirstPicture())
            {
                currentPictureIndex = -1;
                return null;
            }

            if (HasCurrentPicture())
            {
                currentPictureIndex -= 1;
                return CurrentPicture();
            }

            return null;
        }

        public bool OnLastPicture()
        {
            if (HasCurrentPicture() && currentPictureIndex == pictures.Count - 1)
            {
                return true;
            }

            return false;
        }

        public bool OnFirstPicture()
        {
            if (HasCurrentPicture() && currentPictureIndex == 0)
            {
                return true;
            }

            return false;
        }

        public bool HasCurrentPicture()
        {
            if (currentPictureIndex >= 0 && currentPictureIndex <= pictures.Count - 1)
            {
                return true;
            }

            return false;
        }

        public void SetToFirstPicture()
        {
            if (pictures.Count > 0)
            {
                currentPictureIndex = 0;
                return;
            }

            currentPictureIndex = -1;
        }

        public void SetToLastPicture()
        {
            currentPictureIndex = pictures.Count - 1;
        }
        public int Count()
        {
            return pictures.Count;
        }
    }
}