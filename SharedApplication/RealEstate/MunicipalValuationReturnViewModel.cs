﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;
using SharedApplication.RealEstate.MVR;
using SharedApplication.UtilityBilling;

namespace SharedApplication.RealEstate
{
    public class MunicipalValuationReturnViewModel : IMunicipalValuationReturnViewModel
    {        
        private CommandDispatcher commandDispatcher;
        public MunicipalValuationReturnViewModel(CommandDispatcher commandDispatcher)
        {     
            this.commandDispatcher = commandDispatcher;
        }
        public MunicipalValuationReturn ValuationReturn { get; private set; } = new MunicipalValuationReturn();
        public int TownId { get; set; } = 0;

        public void LoadMVR()
        {
            ValuationReturn = commandDispatcher.Send(new GetMunicipalValuationReturn(TownId,false)).Result;                        
        }

        public void SaveMVR()
        {
            commandDispatcher.Send(new SaveMunicipalValuationReturn(ValuationReturn));
        }

        public void LoadAndCalculateMVR()
        {
            ValuationReturn = commandDispatcher.Send(new GetMunicipalValuationReturn(TownId, true)).Result;
        }

        public void PrintMVR()
        {
            commandDispatcher.Send(new ShowMunicipalValuationReturnReport(ValuationReturn));
        }
    }
}