﻿using System.Collections.Generic;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class GetPictureRecords : Command<IEnumerable<PictureRecord>>
    {
        public int Card { get; }
        public int Account { get; }

        public GetPictureRecords(int account, int card)
        {
            Card = card;
            Account = account;
        }
    }
}