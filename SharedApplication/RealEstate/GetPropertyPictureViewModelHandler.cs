﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class GetPropertyPictureViewModelHandler : CommandHandler<GetPropertyPictureViewModel,IPropertyPicturesViewModel>
    {
        private IPropertyPicturesViewModel pictureViewModel;
        public GetPropertyPictureViewModelHandler(IPropertyPicturesViewModel pictureViewModel)
        {
            this.pictureViewModel = pictureViewModel;
        }
        protected override IPropertyPicturesViewModel Handle(GetPropertyPictureViewModel command)
        {
            return pictureViewModel;
        }
    }
}