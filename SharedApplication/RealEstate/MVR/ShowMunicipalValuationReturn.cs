﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate.MVR
{
    public class ShowMunicipalValuationReturn : Command
    {
        public int TownId { get;  } = 0;
        public int ReportYear { get;  } = 0;
        public ShowMunicipalValuationReturn(int townId,int reportYear)
        {
            TownId = townId;
            ReportYear = reportYear;
        }
    }
}