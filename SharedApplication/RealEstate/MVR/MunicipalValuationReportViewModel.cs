﻿namespace SharedApplication.RealEstate.MVR
{
    public class MunicipalValuationReportViewModel : IMunicipalValuationReportViewModel
    {      
        public MunicipalValuationReturn ValuationReturn { get; private set; }

        public void SetValuationReturn(MunicipalValuationReturn valuationReturn)
        {
            ValuationReturn = valuationReturn;
        }
    }
}