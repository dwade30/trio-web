﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate.MVR
{
    public class ShowMunicipalValuationReturnReportHandler : CommandHandler<ShowMunicipalValuationReturnReport>
    {
        private IView<IMunicipalValuationReportViewModel> reportView;

        public ShowMunicipalValuationReturnReportHandler(IView<IMunicipalValuationReportViewModel> reportView)
        {
            this.reportView = reportView;
        }
        protected override void Handle(ShowMunicipalValuationReturnReport command)
        {
            reportView.ViewModel.SetValuationReturn(command.ValuationReturn);
            reportView.Show();
        }
    }
}