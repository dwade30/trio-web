﻿namespace SharedApplication.RealEstate.MVR
{
    public interface IMunicipalValuationReportViewModel
    {
        MunicipalValuationReturn ValuationReturn { get; }
        void SetValuationReturn(MunicipalValuationReturn valuationReturn);
    }
}