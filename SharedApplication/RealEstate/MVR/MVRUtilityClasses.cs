﻿namespace SharedApplication.RealEstate.MVR
{
    public class HomesteadSummary
    {
        public int HomesteadCount { get; set; } = 0;
        public int Assessment { get; set; } = 0;
        public int HomesteadExempt { get; set; } = 0;

    }
}