﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate.MVR
{
    public class ShowMunicipalValuationReturnReport : Command
    {
        public MunicipalValuationReturn ValuationReturn { get; }

        public ShowMunicipalValuationReturnReport(MunicipalValuationReturn valuationReturn)
        {
            ValuationReturn = valuationReturn;
        }
    }
}