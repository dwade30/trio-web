﻿using SharedApplication.RealEstate.Enums;

namespace SharedApplication.RealEstate
{
    public class LandPiece : LandPieceBase
    {
        //public int LandTypeCode { get; set; } = 0;        
        public string UnitsA { get; set; } = "000";
        public string UnitsB { get; set; } = "000";
        public decimal Influence { get; set; } = 0;
        public int InfluenceCode { get; set; } = 0;
    }
}