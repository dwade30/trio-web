﻿using SharedApplication.Messaging;

namespace SharedApplication.RealEstate
{
    public class GetPropertySketchesViewModelHandler : CommandHandler<GetPropertySketchesViewModel,IPropertySketchesViewModel>
    {
        private IPropertySketchesViewModel viewModel;
        public GetPropertySketchesViewModelHandler(IPropertySketchesViewModel viewModel)
        {
            this.viewModel = viewModel;
        }
        protected override IPropertySketchesViewModel Handle(GetPropertySketchesViewModel command)
        {
            return viewModel;
        }
    }
}