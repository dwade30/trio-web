﻿using SharedApplication.RealEstate.Enums;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate.Extensions
{
    public static class LandExtensions
    {
        public static bool IsSoftWood(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.Softwood:
                case LandCategory.SoftwoodFarm:
                    return true;
            }
            return false;
        }

        public static bool IsSoftwood(this LandType landType)
        {
            return landType.LandCategory().IsSoftWood();
        }
        public static bool IsHardWood(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.Hardwood:
                case LandCategory.HardwoodFarm:
                    return true;
            }

            return false;
        }
        public static bool IsHardWood(this LandType landType)
        {
            return landType.LandCategory().IsHardWood();
        }
        public static bool IsMixedwood(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.Mixedwood:
                case LandCategory.MixedwoodFarm:
                    return true;
            }

            return false;

        }
        public static bool IsMixedwood(this LandType landType)
        {
            return landType.LandCategory().IsMixedwood();
        }

        public static bool IsTreeGrowth(this LandCategory landCategory)
        {
            return landCategory.IsSoftWood() || landCategory.IsHardWood() || landCategory.IsMixedwood();
        }

        public static bool IsTreeGrowth(this LandType landType)
        {
            return landType.LandCategory().IsTreeGrowth();
        }

        public static bool IsFarmTreeGrowth(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.HardwoodFarm:
                case LandCategory.MixedwoodFarm:
                case LandCategory.SoftwoodFarm:
                    return true;
            }

            return false;
        }

        public static bool IsPlainTreeGrowth(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.Hardwood:
                case LandCategory.Mixedwood:
                case LandCategory.Softwood:
                    return true;
            }

            return false;
        }

        public static bool IsFarmTreeGrowth(this LandType landType)
        {
            return landType.LandCategory().IsFarmTreeGrowth();
        }

        public static bool IsPlainTreeGrowth(this LandType landType)
        {
            return landType.LandCategory().IsPlainTreeGrowth();
        }

        public static bool IsTillable(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.CropLand:
                case LandCategory.Orchard:
                case LandCategory.Pasture:
                    return true;
            }

            return false;
        }

        public static bool IsTillable(this LandType landType)
        {
            return landType.LandCategory().IsTillable();
        }

        public static bool IsWaterfront(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.Waterfront:
                    return true;
            }

            return false;
        }

        public static bool IsOpenSpace(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.OpenSpace:
                    return true;
            }

            return false;
        }

        public static bool IsWaterfront(this LandType landType)
        {
            return landType.LandCategory().IsWaterfront();
        }

        public static bool IsOpenSpace(this LandType landType)
        {
            return landType.LandCategory().IsOpenSpace();
        }

        public static string ToDescription(this LandUnitType landUnitType)
        {
            switch (landUnitType)
            {
                case LandUnitType.FrontFoot:
                    return "Front Foot";
                case LandUnitType.SquareFeet:
                    return "Square Feet";
                case LandUnitType.FractionalAcreage:
                    return "Fractional Acreage";
                case LandUnitType.Acres:
                    return "Acres";
                case LandUnitType.Site:
                    return "Site";
                case LandUnitType.LinearFeet:
                    return "Linear Feet";
                case LandUnitType.Improvements:
                    return "Improvements";
                case LandUnitType.FrontFootScaled:
                    return "Front Foot - Width";
                case LandUnitType.LinearFeetScaled:
                    return "Linear Feet - Width";
            }

            return "";
        }

        public static string ToDescription(this LandCategory landCategory)
        {
            switch (landCategory)
            {
                case LandCategory.Softwood:
                    return "Softwood";
                case LandCategory.Mixedwood:
                    return "Mixedwood";
                case LandCategory.Hardwood:
                    return "Hardwood";
                case LandCategory.SoftwoodFarm:
                    return "Softwood (Farm)";
                case LandCategory.HardwoodFarm:
                    return "Hardwood (Farm)";
                case LandCategory.MixedwoodFarm:
                    return "Mixedwood (Farm)";
                case LandCategory.CropLand:
                    return "Crop";
                case LandCategory.Orchard:
                    return "Orchard";
                case LandCategory.Pasture:
                    return "Pasture";
                case LandCategory.OpenSpace:
                    return "Open Space";
                case LandCategory.Waterfront:
                    return "Waterfront";
            }

            return "";
        }
    }
}