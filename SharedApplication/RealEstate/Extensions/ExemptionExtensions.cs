﻿using System;
using SharedApplication.RealEstate.Enums;

namespace SharedApplication.RealEstate.Models
{
    public static class ExemptionExtensions
    {
        public static int ToInteger(this PropertyExemptionCategory category)
        {
            return (int) category;
        }
    }

    public static class ResidencyExtensions
    {
        public static int ToInteger(this ExemptEnlistedResidency residency)
        {
            return (int) residency;
        }
    }
}