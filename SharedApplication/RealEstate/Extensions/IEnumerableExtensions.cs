﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralDocuments;

namespace SharedApplication.RealEstate.Models
{
     public static class IEnumerableExtensions
    {
        public static IEnumerable<DescriptionIDPair> MapToDescriptionIDPairs(this IEnumerable<TranCode> tranCodes)
        {
            return tranCodes.Where(tranCode => tranCode.Description != "").Select(tranCode => new DescriptionIDPair()
            {
                Description = tranCode.Description,
                ID = tranCode.Code ?? 0
            }).ToList();
        }

        public static IEnumerable<DescriptionIDPair> MapToDescriptionIDPairsShowingCode(
            this IEnumerable<TranCode> tranCodes)
        {
            return tranCodes.Select(tranCode => new DescriptionIDPair()
            {
                Description = (tranCode.Code.HasValue ? tranCode.Code.Value.ToString() : "0") + " " + tranCode.Description,
                ID = tranCode.Code ?? 0
            }).ToList();
        }

        public static PropertyPictures ToPropertyPictures(this IEnumerable<PictureRecord> pictureRecords)
        {
            return new PropertyPictures(pictureRecords);
        }

        public static PropertySketches ToPropertySketches(this IEnumerable<SketchInfo> sketchInfos)
        {
            return new PropertySketches(sketchInfos);
        }
    }
}
