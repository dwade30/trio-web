﻿using System;
using System.Collections.Generic;
using SharedApplication.Extensions;

namespace SharedApplication.RealEstate.Models
{
    public static class MunicipalValuationReturnExtensions
    {
        public static List<Mvrreport> ToMvrreports(this MunicipalValuationReturn municipalValuationReturn)
        {
            var mvrValues = new List<Mvrreport>();
            var townNumber = municipalValuationReturn.TownNumber;
            mvrValues.Add(MakeMvrreport(MunicipalValuationCode.CertifiedRatio,townNumber,municipalValuationReturn.CertifiedRatio.ToString()));
            mvrValues.Add(MakeMvrreport(MunicipalValuationCode.County,townNumber,municipalValuationReturn.County));
            mvrValues.Add(MakeMvrreport(MunicipalValuationCode.Municipality,townNumber,municipalValuationReturn.Municipality));            
            mvrValues.Add(MakeMvrreport(MunicipalValuationCode.CommitmentDate,townNumber,municipalValuationReturn.CommitmentDate));
            mvrValues.AddRange(GetRealEstateValues(municipalValuationReturn));
            mvrValues.AddRange(GetPersonalPropertyValues(municipalValuationReturn));
            mvrValues.AddRange(GetOtherTaxInformationValues(municipalValuationReturn)); 
            mvrValues.AddRange(GetHomesteadValues(municipalValuationReturn));     
            mvrValues.AddRange(GetBETEValues(municipalValuationReturn));
            mvrValues.AddRange(GetTIFValues(municipalValuationReturn));
            mvrValues.AddRange(GetExciseValues(municipalValuationReturn));
            mvrValues.AddRange(GetElectricalGenerationValues(municipalValuationReturn));
            mvrValues.AddRange(GetForestTreeGrowthValues(municipalValuationReturn));
            mvrValues.AddRange(GetFarmAndOpenSpaceValues(municipalValuationReturn));
            mvrValues.AddRange(GetWaterfrontValues(municipalValuationReturn));
            mvrValues.AddRange(GetExemptPropertyValues(municipalValuationReturn));
            mvrValues.AddRange(GetVeteranExemptionsValues(municipalValuationReturn));
            mvrValues.AddRange(GetMunicipalRecordsValues(municipalValuationReturn));
            mvrValues.AddRange(GetValuationInformationValues(municipalValuationReturn));

            return mvrValues;
        }

        private static Mvrreport MakeMvrreportFromStats(MunicipalValuationCode valuationCode, int townNumber,
            ValuationInformationStats stats)
        {
            var valueString = stats.OneFamily + "," + stats.TwoFamily + "," + stats.ThreeToFourFamily + "," + stats.FiveFamilyPlus + "," + stats.MobileHomes + "," + stats.SeasonalHomes;
            return MakeMvrreport(valuationCode, townNumber, valueString);

        }

        private static Mvrreport MakeMvrreportFromValues(MunicipalValuationCode valuationCode, int townNumber,
            ValuationInformationValues values)
        {
            var valueString = values.OneFamily + "," + values.TwoFamily + "," + values.ThreeToFourFamily + "," + values.FiveFamilyPlus + "," + values.MobileHomes + "," + values.SeasonalHomes;
            return MakeMvrreport(valuationCode, townNumber, valueString);
        }

        private static IEnumerable<Mvrreport> MakeMvrreportsFromIndustrialGrowth(int townNumber,
            MunicipalValuationInformation valuationInformation)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2a,townNumber,valuationInformation.IndustrialMercantileGrowth[0]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2b,townNumber,valuationInformation.IndustrialMercantileGrowth[1]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2c,townNumber,valuationInformation.IndustrialMercantileGrowth[2]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2d,townNumber,valuationInformation.IndustrialMercantileGrowth[3]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2e,townNumber,valuationInformation.IndustrialMercantileGrowth[4]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2f,townNumber,valuationInformation.IndustrialMercantileGrowth[5]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2G,townNumber,valuationInformation.IndustrialMercantileGrowth[6]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion2h,townNumber,valuationInformation.IndustrialMercantileGrowth[7])
            };
        }

        private static IEnumerable<Mvrreport> MakeMvrreportsFromExtremeLosses(int townNumber,
            MunicipalValuationInformation valuationInformation)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3A,townNumber,valuationInformation.ExtremeLosses[0]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3B,townNumber,valuationInformation.ExtremeLosses[1]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3C,townNumber,valuationInformation.ExtremeLosses[2]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3d,townNumber,valuationInformation.ExtremeLosses[3]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3E,townNumber,valuationInformation.ExtremeLosses[4]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3F,townNumber,valuationInformation.ExtremeLosses[5]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3G,townNumber,valuationInformation.ExtremeLosses[6]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion3H,townNumber,valuationInformation.ExtremeLosses[7])
            };
        }

        private static IEnumerable<Mvrreport> MakeMvrreportsFromGeneralChanges(int townNumber,
            MunicipalValuationInformation valuationInformation)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4A,townNumber,valuationInformation.GeneralValuationChange[0]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4B,townNumber,valuationInformation.GeneralValuationChange[1]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4C,townNumber,valuationInformation.GeneralValuationChange[2]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4D,townNumber,valuationInformation.GeneralValuationChange[3]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4E,townNumber,valuationInformation.GeneralValuationChange[4]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4F,townNumber,valuationInformation.GeneralValuationChange[5]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4G,townNumber,valuationInformation.GeneralValuationChange[6]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4H,townNumber,valuationInformation.GeneralValuationChange[7]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4I,townNumber,valuationInformation.GeneralValuationChange[8]),
                MakeMvrreport(MunicipalValuationCode.ValuationQuestion4J,townNumber,valuationInformation.GeneralValuationChange[9])
            };
        }
        private static IEnumerable<Mvrreport> GetValuationInformationValues(MunicipalValuationReturn municipalValuationReturn)
        {
            var values = new List<Mvrreport>()
            {
                MakeMvrreportFromStats(MunicipalValuationCode.ValuationGridNew,municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation.New),
                MakeMvrreportFromStats(MunicipalValuationCode.ValuationGridConverted,municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation.Converted),
                MakeMvrreportFromStats(MunicipalValuationCode.ValuationGridDemolished,municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation.Demolished),
                MakeMvrreportFromValues(MunicipalValuationCode.ValuationGridIncrease,municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation.ValuationIncrease),
                MakeMvrreportFromValues(MunicipalValuationCode.ValuationGridLoss,municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation.ValuationDecrease),                
            };
            values.AddRange(MakeMvrreportsFromIndustrialGrowth(municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation));
            values.AddRange(MakeMvrreportsFromExtremeLosses(municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation));
            values.AddRange(MakeMvrreportsFromGeneralChanges(municipalValuationReturn.TownNumber,municipalValuationReturn.ValuationInformation));
            return values;            
        }

        public static string ToString(this MVRTaxMapType taxMapType)
        {
            switch (taxMapType)
            {
                case MVRTaxMapType.Paper:
                    return "Paper";
                case MVRTaxMapType.GIS:
                    return "GIS";
                case MVRTaxMapType.CAD:
                    return "CAD";
            }

            return "";
        }

        public static int ToInteger(this MVRMunicipalityAssessmentFunction assessmentFunction)
        {
            return (int) assessmentFunction;
        }

        private static IEnumerable<Mvrreport> GetMunicipalRecordsValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsTaxMapsYesNo, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.HasTaxMaps),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsTaxMapsDate, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.DateMapsOriginallyObtained),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsTaxMapsContractor, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.NameOfOriginalContractor),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsTaxMapType, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.TaxMapType.ToString()),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsLandParcels, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.NumberOfParcelsInMunicipality),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsTaxableAcreage, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.TotalTaxableAcreage),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationYesNo, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.ProfessionalRevaluationWasCompleted),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationLand, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.RevaluationIncludedLand),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationBuildings, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.RevaluationIncludedBuildings),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationPersonalProperty, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.RevaluationIncludedPersonalProperty),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationEffectiveDate, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.RevaluationEffectiveDate),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationContractorName, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.RevaluationContractorName),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsRevaluationCost, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.RevaluationCost),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsAssessmentFunctionSingleAssessor, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AssessmentFunction.ToString()),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsAssessmentFunctionSingleAssessorName, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AssessorOrAgentName),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsEmail, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AssessorOrAgentEmail),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsFiscalStart, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.FiscalYearStart),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsFiscalEnd, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.FiscalYearEnd),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsInterestRate, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.OverDueInterestRate),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsDateDue1, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.Period1DueDate),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsDateDue2, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.Period2DueDate),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsDateDue3, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.Period3DueDate),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsDateDue4, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.Period4DueDate),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsComputerizedYesNo, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AssessmentRecordsAreComputerized),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsComputerizedContractor, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AssessmentSoftware),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsLocalTaxReliefYesNo, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.ImplementedATaxReliefProgram),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsLocalTaxReliefHowMany, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForTaxProgram),
                MakeMvrreport(MunicipalValuationCode.MunicipalRecsLocalTaxReliefHowMuch, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AmountOfReliefGrantedForTaxProgram),
                MakeMvrreport(MunicipalValuationCode.MunicipalElderlyTaxCreditYesNo, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.ImplementedElderlyTaxCredit),
                MakeMvrreport(MunicipalValuationCode.MunicipalElderlyTaxCreditHowMany, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForElderlyCredit),
                MakeMvrreport(MunicipalValuationCode.MunicipalElderlyTaxCreditHowMuch, municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AmountOfReliefGrantedForElderlyCredit),
                MakeMvrreport(MunicipalValuationCode.MunicipalSeniorTaxDeferralYesNo,municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.ImplementedSeniorTaxDeferral),
                MakeMvrreport(MunicipalValuationCode.MunicipalSeniorTaxDeferralHowMany,municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.NumberOfPeopleQualifiedForSeniorTaxDeferral),
                MakeMvrreport(MunicipalValuationCode.MunicipalSeniorTaxDeferralHowMuch,municipalValuationReturn.TownNumber,municipalValuationReturn.MunicipalRecords.AmountOfReliefGrantedForSeniorTaxDeferral)
            };            
        }

        
        private static IEnumerable<Mvrreport> GetVeteranExemptionsValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.ExemptSurvivingMaleCount,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.SurvivingMale.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptSurvivingMaleValue,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.SurvivingMale.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumParaplegicLivingTrust,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptParaplegicLivingTrust,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.ParaplegicRevocableTrusts.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumOtherLivingTrust,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MiscRevocableTrust.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptOtherLivingTrust,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MiscRevocableTrust.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumWWIResident,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptWWIResident,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MaineEnlistedWWIVeteran.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumParaplegic,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.ParaplegicVeteran.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptParaplegic,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.ParaplegicVeteran.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.VetExemptNumCoop,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.COOPHousing.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.VetExemptCoop,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.COOPHousing.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumOtherResident,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MiscMaineEnlisted.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptTotOtherResident,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MiscMaineEnlisted.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumOtherNonResident,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MiscNonResident.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptOtherNonResident,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.MiscNonResident.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumVetLineOfDuty,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.DisabledInLineOfDuty.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptVetLineOfDuty,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.DisabledInLineOfDuty.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.VetExemptNumLebanonPanama,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.LebanonPanama.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.VetExemptLebanonPanama,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.LebanonPanama.ExemptValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumEarlyVietnam,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.EarlyVietnamConflict.Count.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptEarlyVietnam,municipalValuationReturn.TownNumber,municipalValuationReturn.VeteranExemptions.EarlyVietnamConflict.ExemptValue.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetExemptPropertyValues(MunicipalValuationReturn municipalValuationReturn)
        {
            var exemptValues = new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.ExemptUnitedStates,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.UnitedStates.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptMaine,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.StateOfMaine.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNHWater,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.NewhampshireWater.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptQuasiMuni,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.Municipal.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptHydroOutsidedMuni,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.MunicipalUtilities.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptPublicAirport,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.MunicipalAirport.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptPrivateAirport,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.PrivateAirport.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptSewageDisposal,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.MunicipalSewage.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptBenevolent,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.Benevolent.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptLiterary,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.LiteraryAndScientific.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptVetOrgs,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.VeteranOrganizations.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptVetOrgsAdministrative,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.VeteranOrgReimbursableExemption.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptChamberCommerce,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.ChamberOfCommerce.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptNumParsonages,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.NumberOfParsonages.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptParsonages,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.ExemptValueOfParsonages.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptTaxableParsonages,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.TaxableValueOfParsonages.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptTotalReligious,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.ReligiousWorship.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptFraternalOrgs,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.FraternalOrganizations.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptLeasedHospital,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.HospitalPersonalProperty.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptBlind,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.LegallyBlind.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptWaterPipes,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.WaterSupplyTransport.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptAnimalWaste,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.CertifiedAnimalWasteStorage.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptPollution,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.PollutionControlFacilities.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptSnowGrooming,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.SnowmobileGroomingEquipment.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExemptSolarWindProcessed,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.SolarAndWindApplicationsProcessed),
                MakeMvrreport(MunicipalValuationCode.ExemptSolarWindApproved,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.SolarAndWindApplicationsApproved),
                MakeMvrreport(MunicipalValuationCode.ExemptSolarWindValue,municipalValuationReturn.TownNumber,municipalValuationReturn.ExemptProperty.SolarAndWindEquipment)
            };
            exemptValues.AddRange(MakeExemptOrganizationsRecords(municipalValuationReturn));
            return exemptValues;
        }

        private static IEnumerable<Mvrreport> MakeExemptOrganizationsRecords(MunicipalValuationReturn municipalValuationReturn)
        {
            int count = 0;
            var organizations = new List<Mvrreport>();
            foreach (var organization in municipalValuationReturn.ExemptProperty.ExemptOrganizations)
            {
                count++;
                switch (count)
                {
                    case 1:
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherName1,municipalValuationReturn.TownNumber,organization.Name));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherProvision1,municipalValuationReturn.TownNumber,organization.ProvisionOfLaw));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherValue1,municipalValuationReturn.TownNumber,organization.ExemptValue.ToString()));
                        break;
                    case 2:
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherName2, municipalValuationReturn.TownNumber, organization.Name));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherProvision2, municipalValuationReturn.TownNumber, organization.ProvisionOfLaw));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherValue2, municipalValuationReturn.TownNumber, organization.ExemptValue.ToString()));
                        break;
                    case 3:
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherName3, municipalValuationReturn.TownNumber, organization.Name));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherProvision3, municipalValuationReturn.TownNumber, organization.ProvisionOfLaw));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherValue3, municipalValuationReturn.TownNumber, organization.ExemptValue.ToString()));
                        break;
                    case 4:
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherName4, municipalValuationReturn.TownNumber, organization.Name));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherProvision4, municipalValuationReturn.TownNumber, organization.ProvisionOfLaw));
                        organizations.Add(MakeMvrreport(MunicipalValuationCode.ExemptOtherValue4, municipalValuationReturn.TownNumber, organization.ExemptValue.ToString()));
                        break;
                }
            }

            return organizations;
        }

        private static IEnumerable<Mvrreport> GetWaterfrontValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.WaterfrontParcels,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.NumberOfParcelsFirstClassified.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterfrontFirstClassified,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.NumberOfAcresFirstClassified.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterfrontCurrentAcreage,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.TotalAcresOfWaterfront.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterfrontCurrentValuation,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.TotalValuationOfWaterfront.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterfrontTotalWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.NumberOfWaterfrontParcelsWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterfrontTotalAcresWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.NumberOfWaterfrontAcresWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterfrontTotalPenalties,municipalValuationReturn.TownNumber,municipalValuationReturn.Waterfront.TotalAmountOfPenalties.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetFarmAndOpenSpaceValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.FarmParcelsClassified,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfParcelsClassifiedAsFarmland.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmAcreageNewClassified,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfAcresClassifiedAsFarmlandForTaxYear.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalAcreageTillable,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalNumberOfAcresOfAllFarmland.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalValueTillable,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalValuationOfAllFarmland.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmSoftAcres,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfFarmSoftwoodAcres.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmMixedAcres,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfFarmMixedWoodAcres.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmHardAcres,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfFarmHardwoodAcres.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalAcreageWoodland,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmWoodlandAcres.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalValueWoodland,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalValuationOfFarmWoodland.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmSoftPerAcre,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.FarmSoftwoodRate.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmMixedPerAcre,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.FarmMixedWoodRate.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmHardPerAcre,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.FarmHardwoodRate.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmlandParcelsWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalAcresWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalNumberOfFarmlandAcresWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.FarmTotalPenalties,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnOpenSpace.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenParcels,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceParcelsFirstClassified.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenNewClassified,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.NumberOfOpenSpaceAcresFirstClassified.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenTotalAcreageClassified,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalAcresOfOpenSpaceFirstClassified.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenTotalValueClassified,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalValuationOfAllOpenSpace.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenParcelsWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalNumberOfOpenSpaceParcelsWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenAcresWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalNumberOfOpenSpaceAcresWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.OpenTotalPenalties,municipalValuationReturn.TownNumber,municipalValuationReturn.FarmAndOpenSpace.TotalAmountOfPenaltiesForWithdrawnOpenSpace.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetForestTreeGrowthValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.TreeGrowthPerAcreUndeveloped,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.AveragePerAcreUnitValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthHardAcreage,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.HardwoodAcreage.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthMixedAcreage,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.MixedwoodAcreage.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthSoftAcreage,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.SoftwoodAcreage.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthNumberParcels,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.NumberOfParcelsClassified.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthTotalAssessed,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.TotalAssessedValuation.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthSoftPerAcre,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.SoftwoodRate.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthMixedPerAcre,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.MixedWoodRate.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthHardPerAcre,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.HardwoodRate.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthTotalAcreageClassifiedCurrent,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.NumberOfAcresFirstClassifiedForTaxYear.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthTotalWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.NumberOfAcresWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthTotalAcresWithdrawn,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.NumberOfAcresWithdrawn.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthTotalPenalties,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.TotalAmountOfPenaltiesAssessed.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthNumTotalPenalties,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.TotalNumberOfNonCompliancePenalties.ToString()),
                MakeMvrreport(MunicipalValuationCode.TreeGrowthTransferredYesNo,municipalValuationReturn.TownNumber,municipalValuationReturn.ForestTreeGrowth.TreeGrowthHasBeenTransferredToFarmlandThisYear ? "True":"False")
            };
        }

        private static IEnumerable<Mvrreport> GetElectricalGenerationValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.IndustrialTransmissionDistributionLines,municipalValuationReturn.TownNumber,municipalValuationReturn.ElectricalGeneration.DistributionAndTransmissionLines.ToString()),
                MakeMvrreport(MunicipalValuationCode.ElectricalGenerationFacilities,municipalValuationReturn.TownNumber,municipalValuationReturn.ElectricalGeneration.GenerationFacilities.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetExciseValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.ExciseCollected,municipalValuationReturn.TownNumber,municipalValuationReturn.Excise.MVExcise.ToString()),
                MakeMvrreport(MunicipalValuationCode.WaterCraftExciseCollected,municipalValuationReturn.TownNumber,municipalValuationReturn.Excise.WatercraftExcise.ToString()),
                MakeMvrreport(MunicipalValuationCode.ExciseCalendarFiscal,municipalValuationReturn.TownNumber,municipalValuationReturn.Excise.Fiscal ? "Fiscal" : "Calendar")
            };
        }

        private static IEnumerable<Mvrreport> GetTIFValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.TIFBeteDepositied,municipalValuationReturn.TownNumber,municipalValuationReturn.TIF.BeteRevenueDeposited.ToString()),
                MakeMvrreport(MunicipalValuationCode.TifAssessed,municipalValuationReturn.TownNumber,municipalValuationReturn.TIF.CapturedAssessedValue.ToString()),
                MakeMvrreport(MunicipalValuationCode.TifTaxRevenue,municipalValuationReturn.TownNumber,municipalValuationReturn.TIF.RevenueDeposited.ToString()),
                MakeMvrreport(MunicipalValuationCode.TIFIncrease,municipalValuationReturn.TownNumber,municipalValuationReturn.TIF.TifIncrease.ToString()),
            };            
        }

        private static IEnumerable<Mvrreport> GetBETEValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.TotalBETEApproved,municipalValuationReturn.TownNumber,municipalValuationReturn.Bete.ApplicationsApproved.ToString()),
                MakeMvrreport(MunicipalValuationCode.NumberBete,municipalValuationReturn.TownNumber,municipalValuationReturn.Bete.ApplicationsProcessed.ToString()),
                MakeMvrreport(MunicipalValuationCode.TIFBete,municipalValuationReturn.TownNumber,municipalValuationReturn.Bete.TotalBeteInTIF.ToString()),
                MakeMvrreport(MunicipalValuationCode.TotalBete,municipalValuationReturn.TownNumber,municipalValuationReturn.Bete.TotalBeteQualifiedExempt.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetHomesteadValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.HomesteadReimTotFullGranted, municipalValuationReturn.TownNumber,municipalValuationReturn.Homestead.NumberOfFullHomesteadsGranted.ToString()),
                MakeMvrreport(MunicipalValuationCode.HomesteadReimTotFullExemptGranted,municipalValuationReturn.TownNumber,municipalValuationReturn.Homestead.NumberOfFullyExemptedHomesteads.ToString()),
                MakeMvrreport(MunicipalValuationCode.HomesteadReimTotValueFullExemptGranted,municipalValuationReturn.TownNumber,municipalValuationReturn.Homestead.TotalValueOfFullyExemptedHomesteads.ToString()),
                MakeMvrreport(MunicipalValuationCode.HomesteadReimTotAssessed,municipalValuationReturn.TownNumber,municipalValuationReturn.Homestead.TotalAssessedValueOfAllHomesteadProperty.ToString()),
            };
        }

        private static IEnumerable<Mvrreport> GetOtherTaxInformationValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.TaxLevy,municipalValuationReturn.TownNumber,municipalValuationReturn.OtherTaxInformation.TaxLevy.ToString()),
                MakeMvrreport(MunicipalValuationCode.TaxRate,municipalValuationReturn.TownNumber,municipalValuationReturn.OtherTaxInformation.TaxRate.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetPersonalPropertyValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.FurnitureFixtures,municipalValuationReturn.TownNumber,municipalValuationReturn.PersonalProperty.BusinessEquipment.ToString()),
                MakeMvrreport(MunicipalValuationCode.MachineryAndEquipment,municipalValuationReturn.TownNumber,municipalValuationReturn.PersonalProperty.ProductionMachineryAndEquipment.ToString()),
                MakeMvrreport(MunicipalValuationCode.OtherPersonalProperty,municipalValuationReturn.TownNumber,municipalValuationReturn.PersonalProperty.OtherPersonalProperty.ToString())
            };
        }

        private static IEnumerable<Mvrreport> GetRealEstateValues(MunicipalValuationReturn municipalValuationReturn)
        {
            return new List<Mvrreport>()
            {
                MakeMvrreport(MunicipalValuationCode.TaxableBuilding,municipalValuationReturn.TownNumber,municipalValuationReturn.RealEstate.TaxableBuilding.ToString()),
                MakeMvrreport(MunicipalValuationCode.TaxableLand,municipalValuationReturn.TownNumber,municipalValuationReturn.RealEstate.TaxableLand.ToString())
            };           
        }

        private static Mvrreport MakeMvrreport(MunicipalValuationCode valuationCode, int townNumber, string dataValue)
        {
            return new Mvrreport()
            {
                Code = (int)valuationCode,
                TownNumber = townNumber,
                DataValue = dataValue
            };
        }

        private static Mvrreport MakeMvrreport(MunicipalValuationCode valuationCode, int townNumber,
            DateTime? dataValue)
        {
            return MakeMvrreport(valuationCode, townNumber, DataValueFromDate(dataValue));
        }

        private static Mvrreport MakeMvrreport(MunicipalValuationCode valuationCode, int townNumber, bool? dataValue)
        {
            return MakeMvrreport(valuationCode, townNumber, dataValue.GetValueOrDefault() ?  bool.TrueString : bool.FalseString);
        }

        private static Mvrreport MakeMvrreport(MunicipalValuationCode valuationCode, int townNumber, decimal dataValue)
        {
            return MakeMvrreport(valuationCode, townNumber, dataValue.ToString());
        }

        private static Mvrreport MakeMvrreport(MunicipalValuationCode valuationCode, int townNumber, int dataValue)
        {
            return MakeMvrreport(valuationCode, townNumber, dataValue.ToString());
        }
        private static string DataValueFromDate(DateTime? dataValue)
        {
            return dataValue.HasValue ? DateStringFromDate(dataValue.Value) : "";
        }

        private static string DateStringFromDate(DateTime date)
        {
            return date.CompareTo(DateTime.MinValue) == 0 ? "" : date.FormatAndPadShortDate();
        }
    }
}