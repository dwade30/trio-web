﻿using System;
using SharedApplication.CentralDocuments;
using SharedApplication.Messaging;
using SharedApplication.RealEstate.Models;

namespace SharedApplication.RealEstate
{
    public class PropertyPicturesViewModel : IPropertyPicturesViewModel
    {
        private CommandDispatcher commandDispatcher;
        private int account = 0;
        private int card = 0;
        private Guid cardIdentifier = Guid.Empty;
        public PropertyPicturesViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }
        public PropertyPictures Pictures { get; private set; } = new PropertyPictures();
        public void LoadPictures(int account, int card, Guid cardIdentifier)
        {
            this.account = account;
            this.card = card;
            this.cardIdentifier = cardIdentifier;
            var pictures = commandDispatcher.Send(new GetPictureRecords(account, card)).Result;
            Pictures = pictures.ToPropertyPictures();
            Pictures.SetToFirstPicture();
        }

        public byte[] GetCurrentPicture()
        {
            if (Pictures.HasCurrentPicture())
            {
                var pic = Pictures.CurrentPicture();
                var centralDoc = commandDispatcher.Send(new GetCentralDocumentByIdentifier(pic.DocumentIdentifier)).Result;
                if (centralDoc != null)
                {
                    return centralDoc.ItemData;
                }
            }

            return null;
        }

        public void SaveCurrentPicture()
        {
            if (Pictures.HasCurrentPicture())
            {
                var pic = Pictures.CurrentPicture();
                commandDispatcher.Send(new UpdatePictureRecord(pic.Id, pic.Account.GetValueOrDefault(),pic.Card.GetValueOrDefault(), pic.Description,pic.PicNum.GetValueOrDefault(),pic.DocumentIdentifier));
            }
        }

        public void DeleteCurrentPicture()
        {
            if (Pictures.HasCurrentPicture())
            {
                var pic = Pictures.CurrentPicture();
                commandDispatcher.Send(new DeleteCentralDocumentByIdentifier(pic.DocumentIdentifier));
                commandDispatcher.Send(new DeletePictureRecord(pic.Id));
            }
        }


        public void AddNewPicture(string description, int sequenceNumber, Guid documentIdentifier, string fileName,byte[] imageData,string mediaType)
        {
            var id = commandDispatcher.Send(new CreateCentralDocument(mediaType, description,
                "PropertyPicture", account, cardIdentifier.ToString(), "RealEstate", documentIdentifier, imageData,fileName,DateTime.Now)).Result;
            if (id > 0)
            {
                var picId = commandDispatcher.Send(new CreatePictureRecord(account, card, sequenceNumber,
                    documentIdentifier,
                    fileName, description)).Result;
                if (picId > 0)
                {
                    Pictures.AddPicture(new PictureRecord()
                    {
                        Account = account,
                        Card = card,
                        Description =  description,
                        DocumentIdentifier = documentIdentifier,
                        Id = picId,
                        PicNum = sequenceNumber,
                        PictureLocation = fileName
                    });
                    Pictures.SetToLastPicture();
                }
            }
        }
    }
}