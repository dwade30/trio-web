﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication.ClientSettings.ScheduleUpdate;

namespace SharedApplication.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<InterestCalculationService>().As<IInterestCalculationService>();
            builder.RegisterType<TrioEncryptionToggler>().As<ITrioEncryptionToggler>();
            builder.RegisterType<BillPaymentUtility>().As<IBillPaymentUtility>();
            builder.RegisterType<SettingService>().As<ISettingService>();
			builder.RegisterType<EffectiveDateChangeViewModel>().As<IEffectiveDateChangeViewModel>();
			builder.RegisterType<ScheduleUpdateViewModel>().As<IScheduleUpdateViewModel>();
            builder.RegisterType<DateTimeService>().As<IDateTimeService>();
        }
	}
}
