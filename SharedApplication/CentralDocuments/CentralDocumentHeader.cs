﻿using System;

namespace SharedApplication.CentralDocuments
{
    public class CentralDocumentHeader
    {
        private byte[] aryItemData;
        private bool boolDeleted;

        public string DataGroup { set; get; } = string.Empty;

        public int ID { set; get; }

        public string MediaType { set; get; } = string.Empty;

        public string ItemName { set; get; } = string.Empty;

        public string ItemDescription { set; get; } = string.Empty;

        public string ReferenceType { set; get; } = "0";

        public int ReferenceId { set; get; }

        public string AltReference { set; get; } = string.Empty;

        public int ItemLength { set; get; }
        public Guid DocumentIdentifier { get; set; }
        public byte[] HashedValue { get; set; } 
    }
}
