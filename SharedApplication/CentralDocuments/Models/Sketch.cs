﻿using System;

namespace SharedApplication.CentralDocuments.Models
{
    public partial class Sketch
    {
        public int Id { get; set; }
        public string MediaType { get; set; }
        public byte[] SketchData { get; set; }
        //public int? Account { get; set; }
        //public int? Card { get; set; }
        public Guid SketchIdentifier { get; set; } = Guid.NewGuid();
        public Guid CardIdentifier { get; set; }
        public byte[] SketchImage { get; set; }
        public int? SequenceNumber { get; set; }
        public string Calculations { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}