﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class CreateCentralDocumentFromFilename : Command<int>
    {
        public string Filename { get; }
        public string Description { get; }
        public string ReferenceType { get; }
        public int ReferenceId { get; }
        public string AltReference { get; }
        public string DataGroup { get; }
        public Guid DocumentIdentifier { get; }
        public byte [] ItemData { get; }
        public DateTime DateCreated { get; }
        public CreateCentralDocumentFromFilename(string filename, string description, string referenceType, int referenceId, string altReference, string dataGroup, Guid documentIdentifier, byte[] itemData,DateTime dateCreated)
        {
            Filename = filename;
            Description = description;
            ReferenceType = referenceType;
            ReferenceId = referenceId;
            AltReference = altReference;
            DataGroup = dataGroup;
            DocumentIdentifier = documentIdentifier;
            ItemData = itemData;
            DateCreated = dateCreated;
        }
    }
}