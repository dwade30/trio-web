﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class CreateCentralDocument : Command<int>
    {
        public string ItemName { get; }
        public string MediaType { get; }
        public string Description { get; }
        public string ReferenceType { get; }
        public int ReferenceId { get; }
        public string AltReference { get; }
        public string DataGroup { get; }
        public Guid DocumentIdentifier { get; }
        public byte[] ItemData { get; }
        public DateTime DateCreated { get; }
        public CreateCentralDocument(string mediaType, string description, string referenceType, int referenceId,
            string altReference, string dataGroup, Guid documentIdentifier, byte[] itemData,string itemName,DateTime dateCreated)
        {
            MediaType = mediaType;
            Description = description;
            ReferenceType = referenceType;
            ReferenceId = referenceId;
            AltReference = altReference;
            DataGroup = dataGroup;
            DocumentIdentifier = documentIdentifier;
            ItemData = itemData;
            ItemName = itemName;
            DateCreated = dateCreated;
        }
    }
}