﻿using System.Collections.ObjectModel;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class GetHeadersByReferenceCommand: Command<Collection<CentralDocumentHeader>>
    {
        public string DataGroup { get; set; }

        public string ReferenceType { get; set; }

        public int ReferenceId { get; set; }
    }
}