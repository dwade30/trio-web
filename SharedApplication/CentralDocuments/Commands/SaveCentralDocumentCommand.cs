﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class SaveCommand: Command
    {
        public CentralDocument Document { get; set; }
    }
}