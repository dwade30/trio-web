﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteByReferenceCommand: Command
    {
        public string DataGroup { get; set; }
        public string ReferenceType { get; set; }
        public int ReferenceId { get; set; }
    }
}
