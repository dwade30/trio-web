﻿namespace SharedApplication.CentralDocuments
{
    public class AddCentralDocumentViewModel : IAddCentralDocumentViewModel
    {
        public string ReferenceGroup { get; set; }
        public string ReferenceType { get; set; }
        public int ReferenceId { get; set; }
        public string AltReferenceId { get; set; }
    }
}