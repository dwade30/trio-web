﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments.Commands
{
    public class RenumberSketches : Command
    {
        public Guid CardId { get; }

        public RenumberSketches(Guid cardId)
        {
            CardId = cardId;
        }
    }
}