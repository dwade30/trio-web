﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteCommand: Command
    {
        public int Id { get; set; }
    }
}