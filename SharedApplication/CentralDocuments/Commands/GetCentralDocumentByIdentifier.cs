﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class GetCentralDocumentByIdentifier : Command<CentralDocument>
    {
        public Guid DocumentIdentifier { get; }

        public GetCentralDocumentByIdentifier(Guid documentIdentifier)
        {
            DocumentIdentifier = documentIdentifier;
        }
    }
}