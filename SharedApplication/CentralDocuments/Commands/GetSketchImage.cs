﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments.Commands
{
    public class GetSketchImage : Command<byte[]>
    {
        public int Id { get; }

        public GetSketchImage(int id)
        {
            Id = id;
        }
    }
}