﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments.Commands
{
    public class DeleteSketch : Command
    {
        public int Id { get; }

        public DeleteSketch(int id)
        {
            Id = id;
        }
    }
}