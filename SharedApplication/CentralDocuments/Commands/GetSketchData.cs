﻿using SharedApplication.Messaging;

namespace SharedApplication
{
    public class GetSketchData : Command<byte[]>
    {
        public int Id { get; }

        public GetSketchData(int id)
        {
            Id = id;
        }
    }
}