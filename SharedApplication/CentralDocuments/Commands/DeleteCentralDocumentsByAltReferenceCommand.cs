﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteByAltReferenceCommand: Command
    {
        public string DataGroup { get; set; }
        public string ReferenceType { get; set; }
        public int ReferenceId { get; set; }
        public string AltReference { get; set; }
    }
}
