﻿using SharedApplication.Messaging;

namespace SharedApplication
{
    public class ShowDocumentViewer : Command
    {
        public string Title { get; }
        public string ReferenceGroup { get; }
        public string ReferenceType { get; }
        public int ReferenceId { get; }
        public string AltReference { get; }
        public bool ShowModal { get; }
        public bool ReadOnly { get; }

        public ShowDocumentViewer(string title, string referenceGroup, string referenceType, int referenceId,
            string altReference, bool showModal, bool readOnly)
        {
            Title = title;
            ReferenceType = referenceType;
            ReferenceGroup = referenceGroup;
            ReferenceId = referenceId;
            AltReference = altReference;
            ShowModal = showModal;
            ReadOnly = readOnly;
        }
    }
}
