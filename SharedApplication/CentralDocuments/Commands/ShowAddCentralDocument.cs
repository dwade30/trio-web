﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments.Commands
{
    public class ShowAddCentralDocument : Command
    {
        public string ReferenceGroup { get; }
        public string ReferenceType { get; }
        public int ReferenceId { get; }
        public string AltReferenceId { get; }

        public ShowAddCentralDocument(string referenceGroup, string referenceType, int referenceId,
            string altReferenceId)
        {
            ReferenceType = referenceType;
            ReferenceGroup = referenceGroup;
            ReferenceId = referenceId;
            AltReferenceId = altReferenceId;
        }
    }
}
