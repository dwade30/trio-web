﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteCentralDocumentByIdentifier : Command
    {
        public Guid DocumentIdentifier { get; }

        public DeleteCentralDocumentByIdentifier(Guid documentIdentifier)
        {
            DocumentIdentifier = documentIdentifier;
        }
    }
}