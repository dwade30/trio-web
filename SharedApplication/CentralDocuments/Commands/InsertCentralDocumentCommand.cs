﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class InsertCommand: Command
    {
        public CentralDocument Document { get; set; }
    }
}