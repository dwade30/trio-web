﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class GetCommand: Command<CentralDocument>
    {
        public int Id { get; set; }
    }
}