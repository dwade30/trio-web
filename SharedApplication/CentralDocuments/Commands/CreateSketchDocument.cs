﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments.Commands
{
    public class CreateSketchDocument : Command<int>
    {
        public string MediaType { get; }
        public byte[] SketchData { get; }
        public byte [] SketchImage { get; }
        //public int Account { get; }
        //public int Card { get; }
        public Guid CardIdentifier { get; }
        public Guid SketchIdentifier { get; }
        public int SequenceNumber { get; }
        public string Calculations { get; }
        public DateTime DateUpdated { get; }

        public CreateSketchDocument( Guid cardIdentifier, Guid sketchIdentifier, int sequenceNumber, 
            DateTime dateUpdated, string calculations, string mediaType, byte[] sketchImage, byte[] sketchData)
        {
            //Account = account;
            Calculations = calculations;
            //Card = card;
            CardIdentifier = cardIdentifier;
            SketchIdentifier = sketchIdentifier;
            SequenceNumber = sequenceNumber;
            DateUpdated = dateUpdated;
            SketchImage = sketchImage;
            SketchData = sketchData;
            MediaType = mediaType;
        }
    }
}