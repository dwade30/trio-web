﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class UpdateCommand: Command
    {
        public CentralDocument Document { get; set; }
    }
}