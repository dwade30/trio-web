﻿using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class InsertCentralDocumentHandler : CommandHandler<InsertCommand>
    {
        private readonly IInsertCentralDocument _worker;

        public InsertCentralDocumentHandler(IInsertCentralDocument worker)
        {
            _worker = worker;
        }

        protected override void Handle(InsertCommand command)
        {
            _worker.Insert(command.Document);
        }
    }
}
