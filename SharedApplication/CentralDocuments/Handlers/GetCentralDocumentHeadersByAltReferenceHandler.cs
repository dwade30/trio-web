﻿using System.Collections.ObjectModel;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class GetHeadersByAltReferenceHandler : CommandHandler<GetHeadersByAltReferenceCommand, Collection<CentralDocumentHeader>>
    {
        private readonly IGetCentralDocumentHeadersByAltReference _worker;

        public GetHeadersByAltReferenceHandler(IGetCentralDocumentHeadersByAltReference worker)
        {
            _worker = worker;
        }

        /// <inheritdoc />
        protected override Collection<CentralDocumentHeader> Handle(GetHeadersByAltReferenceCommand command)
        {
            return _worker.GetByAltRef(command.DataGroup, command.ReferenceType, command.ReferenceId, command.AltReference);
        }
    }
}
