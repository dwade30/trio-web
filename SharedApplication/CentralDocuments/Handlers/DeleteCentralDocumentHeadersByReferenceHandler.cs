﻿using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteByReferenceHandler : CommandHandler<DeleteByReferenceCommand>
    {
        private readonly IDeleteCentralDocumentsByReference _worker;

        public DeleteByReferenceHandler(IDeleteCentralDocumentsByReference worker)
        {
            _worker = worker;
        }

        protected override void Handle(DeleteByReferenceCommand command)
        {
             _worker.DeleteByReference(command.DataGroup, command.ReferenceType, command.ReferenceId);
        }
    }
}
