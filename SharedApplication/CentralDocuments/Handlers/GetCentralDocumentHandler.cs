﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class GetCentralDocumentHandler : CommandHandler<GetCommand, CentralDocument>
    {
        private readonly IGetCentralDocument _worker;

        public GetCentralDocumentHandler(IGetCentralDocument worker)
        {
            _worker = worker;
        }

        protected override CentralDocument Handle(GetCommand command)
        {
            return _worker.Get(command.Id);
        }
    }
}
