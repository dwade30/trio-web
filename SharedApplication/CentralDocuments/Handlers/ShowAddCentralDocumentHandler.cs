﻿using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class ShowAddCentralDocumentHandler : CommandHandler<ShowAddCentralDocument>
    {
        private IModalView<IAddCentralDocumentViewModel> addView;

        public ShowAddCentralDocumentHandler(IModalView<IAddCentralDocumentViewModel> addView)
        {
            this.addView = addView;
        }
        protected override void Handle(ShowAddCentralDocument command)
        {
            addView.ViewModel.AltReferenceId = command.AltReferenceId;
            addView.ViewModel.ReferenceGroup = command.ReferenceGroup;
            addView.ViewModel.ReferenceId = command.ReferenceId;
            addView.ViewModel.ReferenceType = command.ReferenceType;
            addView.ShowModal();
        }
    }
}