﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteHandler : CommandHandler<DeleteCommand>
    {
        private readonly IDeleteCentralDocument _worker;

        public DeleteHandler(IDeleteCentralDocument worker)
        {
            _worker = worker;
        }

        protected override void Handle(DeleteCommand command)
        {
            _worker.Delete(command.Id);
        }
    }
}
