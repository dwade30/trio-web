﻿using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteCentralDocumentInfoesByAltReferenceHandler : CommandHandler<DeleteCentralDocumentsByAltReferenceCommand>
    {
        private readonly IDeleteCentralDocumentsByAltReference _worker;

        public DeleteCentralDocumentInfoesByAltReferenceHandler(IDeleteCentralDocumentsByAltReference worker)
        {
            _worker = worker;
        }

        /// <inheritdoc />
        protected override void Handle(DeleteCentralDocumentsByAltReferenceCommand command)
        {
             _worker.DeleteCentralDocumentsByAltReference(command.ReferenceGroup, command.ReferenceSubGroup, command.AltReference);
        }
    }
}
