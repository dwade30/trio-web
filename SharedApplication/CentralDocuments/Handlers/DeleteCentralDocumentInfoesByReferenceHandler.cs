﻿using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteCentralDocumentInfoesByReferenceHandler : CommandHandler<DeleteCentralDocumentsByReferenceCommand>
    {
        private readonly IDeleteCentralDocumentsByReference _worker;

        public DeleteCentralDocumentInfoesByReferenceHandler(IDeleteCentralDocumentsByReference worker)
        {
            _worker = worker;
        }

        protected override void Handle(DeleteCentralDocumentsByReferenceCommand command)
        {
             _worker.DeleteCentralDocumentsByReference(command.ReferenceGroup, command.ReferenceSubGroup, command.ReferenceId);
        }
    }
}
