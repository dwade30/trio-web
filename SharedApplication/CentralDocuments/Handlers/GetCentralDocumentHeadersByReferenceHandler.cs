﻿using System.Collections.ObjectModel;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class GetCentralDocumentHeadersByReferenceHandler : CommandHandler<GetHeadersByReferenceCommand, Collection<CentralDocumentHeader>>
    {
        private readonly IGetCentralDocumentHeadersByReference _worker;

        public GetCentralDocumentHeadersByReferenceHandler(IGetCentralDocumentHeadersByReference worker)
        {
            _worker = worker;
        }

        protected override Collection<CentralDocumentHeader> Handle(GetHeadersByReferenceCommand command)
        {
            return _worker.GetByRef(command.DataGroup, command.ReferenceType, command.ReferenceId);
        }
    }
}
