﻿using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class ShowSingleDocumentHandler : CommandHandler<ShowSingleDocument>
    {
        private IModalView<IDocumentViewerViewModel> documentView;
        public ShowSingleDocumentHandler(IModalView<IDocumentViewerViewModel> documentView)
        {
            this.documentView = documentView;
        }
        protected override void Handle(ShowSingleDocument command)
        {
            documentView.ViewModel.AltReference = command.AltReference;
            documentView.ViewModel.ReferenceType = command.ReferenceType;
            documentView.ViewModel.ReadOnly = command.ReadOnly;
            documentView.ViewModel.ReferenceGroup = command.ReferenceGroup;
            documentView.ViewModel.ReferenceId = command.ReferenceId;
            documentView.ViewModel.Title = command.Title;
            documentView.ViewModel.SingleDocumentId = command.DocumentId;
            if (command.ShowModal)
            {
                documentView.ShowModal();
            }
            else
            {
                documentView.Show();
            }
        }
    }
}