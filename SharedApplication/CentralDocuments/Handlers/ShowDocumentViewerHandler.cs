﻿using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class ShowDocumentViewerHandler : CommandHandler<ShowDocumentViewer>
    {
        private IModalView<IDocumentViewerViewModel> documentView;
        public ShowDocumentViewerHandler(IModalView<IDocumentViewerViewModel> documentView)
        {
            this.documentView = documentView;
        }
        protected override void Handle(ShowDocumentViewer command)
        {
            documentView.ViewModel.AltReference = command.AltReference;
            documentView.ViewModel.ReferenceType = command.ReferenceType;
            documentView.ViewModel.ReadOnly = command.ReadOnly;
            documentView.ViewModel.ReferenceGroup = command.ReferenceGroup;
            documentView.ViewModel.ReferenceId = command.ReferenceId;
            documentView.ViewModel.Title = command.Title;
            if (command.ShowModal)
            {
                documentView.ShowModal();
            }
            else
            {
                documentView.Show();
            }
        }
    }
}