﻿using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class UpdateCentralDocumentHandler : CommandHandler<UpdateCommand>
    {
        private readonly IUpdateCentralDocument _worker;

        public UpdateCentralDocumentHandler(IUpdateCentralDocument worker)
        {
            _worker = worker;
        }

        protected override void Handle(UpdateCommand command)
        {
            _worker.Update(command.Document);
        }
    }
}
