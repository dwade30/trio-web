﻿using SharedApplication.CentralDocuments.Interfaces;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class DeleteByAltReferenceHandler : CommandHandler<DeleteByAltReferenceCommand>
    {
        private readonly IDeleteCentralDocumentsByAltReference _worker;

        public DeleteByAltReferenceHandler(IDeleteCentralDocumentsByAltReference worker)
        {
            _worker = worker;
        }

        /// <inheritdoc />
        protected override void Handle(DeleteByAltReferenceCommand command)
        {
             _worker.DeleteByAltReference(command.DataGroup, command.ReferenceType, command.ReferenceId, command.AltReference);
        }
    }
}
