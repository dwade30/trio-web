﻿using System;

namespace SharedApplication.CentralDocuments
{
    public class CentralDocument
    {
        private string strMediaType = string.Empty;
        private string strItemName = string.Empty;
        private string strItemDescription = string.Empty;
        private string _referenceType;
        private int lngReferenceId;
        private string strAltReference = string.Empty;
        private byte[] aryItemData;
        private int itemLength;
        private string strDataGroup = string.Empty;
        private bool boolDeleted;
        private Guid documentIdentifier = Guid.NewGuid();
        private DateTime dateCreated = DateTime.Now;
        public DateTime DateCreated
        {
            get => dateCreated;
            set
            {
                dateCreated = value;
                IsUpdated = true;
            }
        }
        public Guid DocumentIdentifier
        {
            get => documentIdentifier;
            set
            {
                documentIdentifier = value;
                IsUpdated = true;
            }
        }
        public string DataGroup
        {
            set
            {
                strDataGroup = value;
                IsUpdated = true;
            }
            get => strDataGroup;
        }

        public bool IsUpdated { set; get; }

        public bool IsDeleted
        {
            set
            {
                boolDeleted = value;
                IsUpdated = true;
            }
            get => boolDeleted;
        }

        public int ID { set; get; }

        public string MediaType
        {
            set
            {
                strMediaType = value;
                IsUpdated = true;
            }
            get => strMediaType;

        }

        public string ItemName
        {
            set
            {
                strItemName = value;
                IsUpdated = true;
            }
            get => strItemName;

        }

        public string ItemDescription
        {
            set
            {
                strItemDescription = value;
                IsUpdated = true;
            }
            get => strItemDescription;

        }

        public string ReferenceType
        {
            set
            {
                _referenceType = value;
                IsUpdated = true;
            }

            get => _referenceType;

        }

        public int ReferenceId
        {
            set
            {
                lngReferenceId = value;
                IsUpdated = true;
            }

            get => lngReferenceId;

        }

        public string AltReference
        {
            set
            {
                strAltReference = value;
                IsUpdated = true;
            }
            get => strAltReference;
        }

        public byte[] ItemData
        {
            set
            {
                aryItemData = value;
                IsUpdated = true;
            }
            get => aryItemData;
        }

        public int ItemLength
        {
            set
            {
                itemLength = value;
                IsUpdated = true;
            }
            get => itemLength;
        }

        public byte[] HashedValue { get; set; }

        public CentralDocumentHeader ToHeader()
        {
            var hdr = new CentralDocumentHeader
            {
                AltReference = AltReference,
                ID = ID,
                ItemDescription = ItemDescription,
                ItemLength = ItemLength,
                ItemName = ItemName,
                MediaType = MediaType,
                DataGroup = DataGroup,
                ReferenceId = ReferenceId,
                ReferenceType = ReferenceType,
                DocumentIdentifier = DocumentIdentifier,
                HashedValue =  HashedValue
            };

            return hdr;
        }
    }
}
