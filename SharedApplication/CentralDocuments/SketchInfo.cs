﻿using System;

namespace SharedApplication.CentralDocuments
{
    public class SketchInfo
    {
        public int Id { get; set; }
        public string MediaType { get; set; }
        public int? Account { get; set; }
        public int? Card { get; set; }
        public Guid CardIdentifier { get; set; }
        public Guid SketchIdentifier { get; set; } = Guid.NewGuid();
        public int? SequenceNumber { get; set; }
        public string Calculations { get; set; }
        public DateTime DateUpdated { get; set; }
    }
}