﻿using SharedApplication.Messaging;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Security.Cryptography;

namespace SharedApplication.CentralDocuments
{
    public class CentralDocumentService : BaseEndpointController
    {
        #region Properties
        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public bool HadError
        {
            get
            {
                bool HadError = false;
                HadError = lngLastError != 0;
                return HadError;
            }
        }

        #endregion

        public CentralDocumentService(CommandDispatcher dispatcher)
        {
            Dispatcher = dispatcher;
        }
        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        private void SetError(int lngError, string strError)
        {
            strLastError = strError;
            lngLastError = lngError;
        }

        #region Get
        public CentralDocument GetCentralDocument(GetCommand cmd)
        {
            return Dispatch(cmd);
        }

        public Collection<CentralDocumentHeader> GetHeadersByReference(GetHeadersByReferenceCommand cmd)
        {
            return Dispatch(cmd);
        }

        public Collection<CentralDocumentHeader> GetHeadersByAltReference(GetHeadersByAltReferenceCommand cmd)
        {
            return Dispatch(cmd);
        }

        #endregion

        #region Save

        public void SaveCentralDocument(SaveCommand cmd)
        {
            ClearErrors();

            if (cmd == null) throw new ArgumentNullException("Command object is required");

            try
            {
                var doc = cmd.Document;
                if (doc == null) throw new ArgumentNullException("Must have a document to save");

                if (doc.IsDeleted)
                {
                    // do a delete
                    var delCmd = new DeleteCommand { Id = doc.ID };
                    DeleteCentralDocument(delCmd);

                    return;
                }

                // not a delete so see if update or insert
                if (doc.ID > 0)
                {
                    // do an update
                    var updCmd = new UpdateCommand { Document = doc };
                    UpdateCentralDocument(updCmd);
                }
                else
                {
                    // do an insert
                    var insCmd = new InsertCommand { Document = doc };
                    InsertCentralDocument(insCmd);
                }

            }
            catch (FileNotFoundException ex)
            {
                SetError(9999, "Central document record not found");
            }
            catch (Exception e)
            {
                SetError(e.GetBaseException().HResult, e.GetBaseException().Message);
            }
        }

        #region Delete
        public void DeleteCentralDocument(DeleteCommand cmd)
        {
            Dispatch(cmd);
        }

        public void DeleteCentralDocumentsByReference(DeleteCommand cmd)
        {
            Dispatch(cmd);
        }

        public void DeleteCentralDocumentsByAltReference(DeleteByAltReferenceCommand cmd)
        {
            Dispatch(cmd);
        }

        #endregion

        #region Update

        private void UpdateCentralDocument(UpdateCommand cmd)
        {
            Dispatch(cmd);
        }

        #endregion

        #region Insert

        private void InsertCentralDocument(InsertCommand cmd)
        {
            Dispatch(cmd);
        }

        #endregion

        #endregion
        public CentralDocument MakeCentralDocumentFromFile(string strFileName, string strItemDescription, string strReferenceType, int lngReferenceID, string strAltReference, string strDataGroup, Guid documentIdentifier , byte[] ItemData = null)
        {
            ClearErrors();
            CentralDocument cenDoc;

            if (!string.IsNullOrEmpty(strFileName))
            {
                cenDoc = new CentralDocument();
                string strTemp = "";
                
                strTemp = Path.GetFileName(strFileName);
                cenDoc.ItemName = strTemp;
                
                strTemp = Path.GetExtension(strFileName).TrimStart('.');
                var extension = strTemp.ToLower();

                switch (extension)
                {
                    case "pdf":
                        cenDoc.MediaType = "application/pdf";

                        break;
                    case "jpg":
                    case "jpeg":
                        cenDoc.MediaType = "image/jpeg";

                        break;
                    case "bmp":
                        cenDoc.MediaType = "image/bmp";

                        break;
                    case "png":
                        cenDoc.MediaType = "image/png";

                        break;
                    case "gif":
                        cenDoc.MediaType = "image/gif";

                        break;
                    case "tif":
                    case "tiff":
                        cenDoc.MediaType = "image/tif";

                        break;
                    default:
                        throw new ApplicationException("Unsupported file extension. Must be an image file or pdf."); 
                }
 
                cenDoc.ItemData = ItemData;
                cenDoc.ItemLength = ItemData?.Length ?? 0;
                cenDoc.ItemDescription = strItemDescription;
                cenDoc.ReferenceType = strReferenceType;
                cenDoc.ReferenceId = lngReferenceID;
                cenDoc.AltReference = strAltReference;
                cenDoc.DataGroup = strDataGroup;
                cenDoc.DocumentIdentifier = documentIdentifier;
                cenDoc.DateCreated = DateTime.Now;
                var sha256 = new SHA256Managed();
                cenDoc.HashedValue = sha256.ComputeHash(ItemData);
            }
            else
            {
                throw new FileNotFoundException(); 
            }

            return cenDoc;
        }

        #region Dispatch

        private void Dispatch(Command cmd)
        {
            ClearErrors();

            try
            {
                Dispatcher.Send(cmd);
            }
            catch (Exception e)
            {
                SetError(e.GetBaseException().HResult, e.GetBaseException().Message);
            }
        }
        private T Dispatch<T>(Command<T> cmd) where T : class
        {
            ClearErrors();

            try
            {
                return Dispatcher.Send(cmd).Result;
            }
            catch (Exception e)
            {
                SetError(e.GetBaseException().HResult, e.GetBaseException().Message);

                return null;
            }
        }

        #endregion
    }
}
