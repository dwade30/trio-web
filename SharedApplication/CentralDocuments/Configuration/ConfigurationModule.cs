﻿using Autofac;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Interfaces;

namespace SharedApplication.CentralDocuments.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // central docs stuff
            builder.RegisterType<AddCentralDocumentViewModel>().As<IAddCentralDocumentViewModel>();
            builder.RegisterType<DocumentViewerViewModel>().As<IDocumentViewerViewModel>();            
        }

    }
}