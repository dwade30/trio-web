﻿using SharedApplication.CentralDocuments.Commands;
using SharedApplication.Messaging;

namespace SharedApplication
{
    public class DocumentViewerViewModel : IDocumentViewerViewModel
    {
        public string Title { get; set; }
        public string ReferenceGroup { get; set; }
        public string ReferenceType { get; set; }
        public int ReferenceId { get; set; }
        public string AltReference { get; set; }
        public bool ReadOnly { get; set; }
        public int SingleDocumentId { get; set; }

        private CommandDispatcher commandDispatcher;
        public DocumentViewerViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }
        public void AddCentralDocument()
        {
            commandDispatcher.Send(new ShowAddCentralDocument(ReferenceGroup, ReferenceType, ReferenceId,
                AltReference));
        }
    }
}