﻿namespace SharedApplication.CentralDocuments.Interfaces
{
    public interface IInsertCentralDocument
    {
        void Insert(CentralDocument document);
    }
}
