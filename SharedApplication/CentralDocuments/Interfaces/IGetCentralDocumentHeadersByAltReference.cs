﻿using System.Collections.ObjectModel;

namespace SharedApplication.CentralDocuments
{
    public interface IGetCentralDocumentHeadersByAltReference
    {
        Collection<CentralDocumentHeader> GetByAltRef(string DataGroup, string referenceType, int referenceId, string altReference);
    }
}
