﻿namespace SharedApplication
{
    public interface IDocumentViewerViewModel
    {
        string Title { get; set; }
        string ReferenceGroup { get; set; }
        string ReferenceType { get; set; }
        int ReferenceId { get; set; }
        string AltReference { get; set; }        
        bool ReadOnly { get; set; }
        int SingleDocumentId { get; set; }
        void AddCentralDocument();

    }
}