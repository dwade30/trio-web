﻿using System.Linq;
using SharedApplication.CentralDocuments.Models;

namespace SharedApplication.CentralDocuments
{
    public interface ICentralDocumentContext
    {
        IQueryable<Sketch> Sketches { get; }
        IQueryable<SketchInfo> SketchInfos { get; }
        IQueryable<CentralDocument> Documents { get; }
    }
}