﻿namespace SharedApplication.CentralDocuments.Interfaces
{
    public interface IUpdateCentralDocument
    {
        void Update(CentralDocument document);
    }
}
