﻿namespace SharedApplication.CentralDocuments
{
    public interface IAddCentralDocumentViewModel
    {
        string ReferenceGroup { get; set; }
        string ReferenceType { get; set; }
        int ReferenceId { get; set; }
        string AltReferenceId { get; set; }
    }
}