﻿namespace SharedApplication.CentralDocuments
{
    public interface IDeleteCentralDocument
    {
        void Delete(int id);
    }
}
