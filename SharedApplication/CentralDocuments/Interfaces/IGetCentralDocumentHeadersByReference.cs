﻿using System.Collections.ObjectModel;

namespace SharedApplication.CentralDocuments
{
    public interface IGetCentralDocumentHeadersByReference
    {
        Collection<CentralDocumentHeader> GetByRef(string DataGroup, string referenceType, int referenceId);
    }
}
