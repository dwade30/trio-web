﻿namespace SharedApplication.CentralDocuments.Interfaces
{
    public interface IDeleteCentralDocumentsByReference
    {
        void DeleteByReference(string DataGroup, string referenceType, int referenceId);
    }
}
