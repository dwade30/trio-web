﻿namespace SharedApplication.CentralDocuments
{
    public interface IGetCentralDocument
    {
        CentralDocument Get(int id);
    }
}
