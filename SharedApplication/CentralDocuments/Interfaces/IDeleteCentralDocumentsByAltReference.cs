﻿namespace SharedApplication.CentralDocuments.Interfaces
{
    public interface IDeleteCentralDocumentsByAltReference
    {
        void DeleteByAltReference(string DataGroup, string referenceType, int referenceId, string altReference);
    }
}
