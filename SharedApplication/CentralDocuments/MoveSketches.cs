﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CentralDocuments
{
    public class MoveSketches : Command
    {
        public Guid SourceCardIdentifier { get; }
        public Guid DestinationCardIdentifier { get; }

        public MoveSketches(Guid srcCardIdentifier, Guid destCardIdentifier)
        {
            SourceCardIdentifier = srcCardIdentifier;
            DestinationCardIdentifier = destCardIdentifier;
        }
    }
}