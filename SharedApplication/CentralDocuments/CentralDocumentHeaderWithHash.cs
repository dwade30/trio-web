﻿using System;

namespace SharedApplication.CentralDocuments
{
    public class CentralDocumentHeaderWithHash : CentralDocumentHeader
    {
        public string Hash
        {
            get => Convert.ToBase64String(HashBytes);
        }
        public byte[] HashBytes { get; set; }
    }
}