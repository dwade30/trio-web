﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Enums;

namespace SharedApplication
{
	public class AddCYAEntry : SharedApplication.Messaging.Command
	{
		public TrioPrograms LoggingProgram { get; set; }
		public string Description1 { get; set; }
		public string Description2 { get; set; }
		public string Description3 { get; set; }
		public string Description4 { get; set; }
		public string Description5 { get; set; }

	}
}
