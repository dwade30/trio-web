﻿namespace SharedApplication
{
	public interface IGlobalActiveModuleSettings
	{
        bool AccountsReceivableIsActive { get; set; }
        bool BudgetaryIsActive { get; set; }
        bool TaxBillingIsActive { get; set; }
        bool CodeEnforcementIsActive { get; set; }
        bool TaxCollectionsIsActive { get; set; }
        bool ClerkIsActive { get; set; }
        bool FixedAssetsIsActive { get; set; }
        bool MosesIsActive { get; set; }
        bool MotorVehicleIsActive { get; set; }
        bool PersonalPropertyIsActive { get; set; }
        bool PayrollIsActive { get; set; }
        bool RedbookIsActive { get; set; }
        bool RealEstateIsActive { get; set; }
        bool UtilityBillingIsActive { get; set; }
        bool TaxServiceIsActive { get; set; }
        bool CashReceiptingIsActive { get; set; }
        bool CRMyRecIsActive { get; set; }
    }
}