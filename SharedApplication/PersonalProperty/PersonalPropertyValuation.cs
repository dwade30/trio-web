﻿namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyValuation
    {
        public int CategoryNumber { get; set; } = 0;
        public string CategoryName { get; set; } = "";
        public int Amount { get; set; } = 0;
        public int Exempt { get; set; } = 0;
    }
}