﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyAccountTotals
    {
        public int Account { get; set; } = 0;
        public decimal TotalLeased { get; set; } = 0;
        public decimal TotalItemized { get; set; } = 0;

        public PersonalPropertyValuations Valuations { get; } = new PersonalPropertyValuations();
    }

    
}