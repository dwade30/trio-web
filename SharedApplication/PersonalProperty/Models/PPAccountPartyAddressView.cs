﻿using System;

namespace SharedApplication.PersonalProperty.Models
{
    public class PPAccountPartyAddressView
    {
        public int Id { get; set; }
        public int? Account { get; set; }
        public int? RealAssoc { get; set; }
        public int? PartyId { get; set; }
        public int? StreetNumber { get; set; }
        public string Street { get; set; }
        public int? Value { get; set; }
        public int? Exemption { get; set; }
        public short? ExemptCode1 { get; set; }
        public short? ExemptCode2 { get; set; }
        public short? TranCode { get; set; }
        public int? BusinessCode { get; set; }
        public short? StreetCode { get; set; }
        public string Open1 { get; set; }
        public string Open2 { get; set; }
        public short? Mo { get; set; }
        public short? Da { get; set; }
        public short? Yr { get; set; }
        public int? CompValue { get; set; }
        public string Orcode { get; set; }
        public string Updcode { get; set; }
        public bool? Deleted { get; set; }
        public string Rbcode { get; set; }
        public int? SquareFootage { get; set; }
        public string StreetApt { get; set; }
        public string Comment { get; set; }
        public int? Exemption1 { get; set; }
        public int? Exemption2 { get; set; }
        public string AccountId { get; set; }
        public Guid? PartyGuid { get; set; }
        public int? PartyType { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string AddressType { get; set; }
        public string FullName()
        {
            return (((FirstName.Trim() + " " + MiddleName).Trim() + " " + LastName).Trim() + " " + Designation).Trim();
        }

        public string FullNameLastFirst()
        {
            if (LastName != "")
            {
                return ((LastName.Trim() + ", " + FirstName + " " + MiddleName).ToString() + " " + Designation).ToString();
            }
            else
            {
                return ((FirstName + " " + MiddleName).Trim() + " " + Designation).Trim();
            }
        }
    }
}