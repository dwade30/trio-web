﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.PersonalProperty.Models
{
    public  class TranCode
    {
        public int? Code { get; set; }
        public string Description { get; set; }

        public int ID { get; set; }
    }
}
