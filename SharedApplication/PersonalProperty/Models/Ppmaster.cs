﻿using System;
using System.Collections.Generic;

namespace PersonalProperty
{
    public partial class PPMaster
    {
        public int Id { get; set; }
        public int? Account { get; set; }
        public int? RealAssoc { get; set; }
        public int? PartyId { get; set; }
        public int? StreetNumber { get; set; }
        public string Street { get; set; }
        public int? Value { get; set; }
        public int? Exemption { get; set; }
        public short? ExemptCode1 { get; set; }
        public short? ExemptCode2 { get; set; }
        public short? TranCode { get; set; }
        public int? BusinessCode { get; set; }
        public short? StreetCode { get; set; }
        public string Open1 { get; set; }
        public string Open2 { get; set; }
        public short? Mo { get; set; }
        public short? Da { get; set; }
        public short? Yr { get; set; }
        public int? CompValue { get; set; }
        public string Orcode { get; set; }
        public string Updcode { get; set; }
        public bool? Deleted { get; set; }
        public string Rbcode { get; set; }
        public int? SquareFootage { get; set; }
        public string StreetApt { get; set; }
        public string Comment { get; set; }
        public int? Exemption1 { get; set; }
        public int? Exemption2 { get; set; }
        public string AccountId { get; set; }
    }
}