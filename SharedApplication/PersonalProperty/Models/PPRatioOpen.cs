﻿namespace SharedApplication.PersonalProperty.Models
{
    public class PPRatioOpen
    {
        public string Ratio { get; set; }
        public string OpenField1 { get; set; }
        public string OpenField2 { get; set; }
        public int Id { get; set; }
    }
}