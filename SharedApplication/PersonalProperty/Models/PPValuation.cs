﻿namespace SharedApplication.PersonalProperty.Models
{
    public class PPValuation
    {
        public int? Account { get; set; }
        public int? Category1 { get; set; }
        public int? Category2 { get; set; }
        public int? Category3 { get; set; }
        public int? Category4 { get; set; }
        public int? Category5 { get; set; }
        public int? Category6 { get; set; }
        public int? Category7 { get; set; }
        public int? Category8 { get; set; }
        public int? Category9 { get; set; }
        public int? OverRideAmount { get; set; }
        public int? TotalItemized { get; set; }
        public int? TotalLeased { get; set; }
        public int? Cat1Exempt { get; set; }
        public int? Cat2Exempt { get; set; }
        public int? Cat3Exempt { get; set; }
        public int? Cat4Exempt { get; set; }
        public int? Cat5Exempt { get; set; }
        public int? Cat6Exempt { get; set; }
        public int? Cat7Exempt { get; set; }
        public int? Cat8Exempt { get; set; }
        public int? Cat9Exempt { get; set; }
        public int Id { get; set; }

        public int CategoryAmount(int category)
        {
            switch (category)
            {
                case 1:
                    return Category1.GetValueOrDefault();
                case 2:
                    return Category2.GetValueOrDefault();
                case 3:
                    return Category3.GetValueOrDefault();
                case 4:
                    return Category4.GetValueOrDefault();
                case 5:
                    return Category5.GetValueOrDefault();
                case 6:
                    return Category6.GetValueOrDefault();
                case 7:
                    return Category7.GetValueOrDefault();
                case 8:
                    return Category8.GetValueOrDefault();
                case 9:
                    return Category9.GetValueOrDefault();
            }

            return 0;
        }
    }
}