﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.PersonalProperty.Models
{
	public class ExemptCode
	{
		public int? Code { get; set; }
		public int? Amount { get; set; }
		public string Description { get; set; }
		public string MVRCategory { get; set; }

		public int ID { get; set; }
	}
}
