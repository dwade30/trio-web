﻿namespace SharedApplication.PersonalProperty.Models
{
    public class RatioTrend
    {
        public int? Type { get; set; }
        public short? High { get; set; }
        public short? Low { get; set; }
        public double? Exponent { get; set; }
        public short? Life { get; set; }
        public string Sd { get; set; }
        public string Trend { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
        public int? CategoryType { get; set; }
    }
}