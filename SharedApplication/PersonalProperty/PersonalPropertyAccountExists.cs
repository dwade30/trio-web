﻿using SharedApplication.Messaging;

namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyAccountExists : Command<bool>
    {
        public int Account { get; }

        public PersonalPropertyAccountExists(int account)
        {
            Account = account;
        }
    }
}