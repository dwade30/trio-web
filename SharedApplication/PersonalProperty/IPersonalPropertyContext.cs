﻿using System.Linq;
using PersonalProperty;
using SharedApplication.PersonalProperty.Models;

namespace SharedApplication.PersonalProperty
{
    public interface IPersonalPropertyContext
    {
        IQueryable<TranCode> TranCodes { get; }
        IQueryable<PPMaster> PPMasters { get; }
        IQueryable<PPValuation> PPValuations { get; }
        IQueryable<PPRatioOpen> PPRatioOpens { get; }
        IQueryable<RatioTrend> RatioTrends { get; }
		IQueryable<ExemptCode> ExemptCodes { get; }
        IQueryable<PPAccountPartyView> PPAccountPartyViews { get; }
        IQueryable<PPAccountPartyAddressView> PPAccountPartyAddressViews { get; }
    }
}