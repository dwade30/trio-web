﻿namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyAccountBriefQuery
    {
        public int Account { get; set; }

        public PersonalPropertyAccountBriefQuery(int account)
        {
            Account = account;
        }
    }
}