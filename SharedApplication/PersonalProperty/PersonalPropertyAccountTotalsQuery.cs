﻿namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyAccountTotalsQuery
    {
        public int Account { get; set; }

        public PersonalPropertyAccountTotalsQuery(int account)
        {
            Account = account;
        }
    }
}