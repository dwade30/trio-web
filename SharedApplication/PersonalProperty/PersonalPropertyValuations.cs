﻿using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyValuations
    {
        private List<PersonalPropertyValuation> valuations = new List<PersonalPropertyValuation>();

        public IEnumerable<PersonalPropertyValuation> Valuations
        {
            get => valuations;
        }

        public void AddValuation(PersonalPropertyValuation valuation)
        {
            if (valuation != null)
            {
                PersonalPropertyValuation newValuation;
                if (valuations.Any(v => v.CategoryNumber == valuation.CategoryNumber))
                {
                    newValuation = valuations.FirstOrDefault(v => v.CategoryNumber == valuation.CategoryNumber);
                }
                else
                {
                    newValuation = new PersonalPropertyValuation(){CategoryNumber = valuation.CategoryNumber, CategoryName = valuation.CategoryName};
                    valuations.Add(newValuation);
                }

                newValuation.Amount = valuation.Amount;
                newValuation.Exempt = valuation.Exempt;
                
            }
        }

        public void AddValuations(IEnumerable<PersonalPropertyValuation> categoryValuations)
        {
            if (valuations != null)
            {
                foreach (var valuation in categoryValuations)
                {
                    AddValuation(valuation);                    
                }
            }
        }

        public (int totalAmount, int totalExempt) Totals()
        {
            return (totalAmount: valuations.Sum(v => v.Amount), valuations.Sum(v => v.Exempt));
        }
    }
}