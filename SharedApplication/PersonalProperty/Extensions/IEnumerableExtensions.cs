﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.PersonalProperty.Models;

namespace SharedApplication.PersonalProperty.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<DescriptionIDPair> MapToDescriptionIDPairs(this IEnumerable<TranCode> tranCodes)
        {
            return tranCodes.Where(tranCode => tranCode.Description != "").Select(tranCode => new DescriptionIDPair()
            {
                Description = tranCode.Description,
                ID = tranCode.Code ?? 0
            });
        }

        public static IEnumerable<DescriptionIDPair> MapToDescriptionIDPairsShowingCode(
            this IEnumerable<TranCode> tranCodes)
        {
            return tranCodes.Select(tranCode => new DescriptionIDPair()
            {
                Description = (tranCode.Code.HasValue ? tranCode.Code.Value.ToString() : "0") + " " + tranCode.Description,
                ID = tranCode.Code ?? 0
            }).ToList();
        }
    }
}
