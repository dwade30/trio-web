﻿namespace SharedApplication.PersonalProperty
{
    public class PersonalPropertyAccountBriefInfo
    {
        public int Account { get; set; } = 0;
        public string Name { get; set; } = "";
        public int LocationNumber { get; set; } = 0;
        public string LocationStreet { get; set; } = "";
        public int PartyId { get; set; } = 0;
        public bool Deleted { get; set; } = false;
        public int TownId { get; set; } = 0;
    }
}