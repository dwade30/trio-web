﻿namespace SharedApplication.UtilityBilling
{
    public class GlobalUtilityBillingSettings
    {
        public bool HasWaterService { get; set; }
        public bool HasSewerService { get; set; }
        public bool ShowLastUBAccountInCR { get; set; }
        public int gintBasis { get; set; }
		public string TownService { get; set; }
		public bool OnlyCheckOldestBillForLien { get; set; }
		public bool DisableAutoPaymentInsert { get; set; }
		public decimal FlatInterestAmount { get; set; }
		public decimal EarnedInterestRate { get; set; }
		public string DefaultInterestMethod { get; set; }
		public bool CheckRETaxAcquired { get; set; }
		public bool PayWaterFirst { get; set; }
		public bool AutoPayOldestBillFirst { get; set; }
	}
}