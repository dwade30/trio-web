﻿using Autofac;

namespace SharedApplication.UtilityBilling.Notes.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UtilityNoteViewModel>().As<IUtilityNoteViewModel>();
        }

    }

}