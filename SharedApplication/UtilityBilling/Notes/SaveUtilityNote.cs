﻿using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Notes
{
    public class SaveUtilityNote : Command<int>
    {
        public int Account { get;  }
        public string Text { get;  } = null;
        public int Priority { get; } 
        public SaveUtilityNote(int account, int priority, string noteText)
        {
            Account = account;
            Text = noteText;
            Priority = priority;
        }
    }
}