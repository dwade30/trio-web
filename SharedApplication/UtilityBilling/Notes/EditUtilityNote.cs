﻿using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Notes
{
    public class EditUtilityNote : Command
    {
        public int Account { get; }

        public EditUtilityNote(int account)
        {
            Account = account;
        }
    }   
}