﻿using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Receipting;

//using SharedApplication.TaxCollections.Enums;
//using SharedApplication.TaxCollections.Models;
//using SharedApplication.TaxCollections.Queries;

namespace SharedApplication.UtilityBilling.Notes
{
    public class UtilityNoteViewModel : IUtilityNoteViewModel
    {
        public int Id { get; set; }
        public int Account { get; set; }
        public string Comment { get; set; }
        public int Priority { get; set; }
        public bool DisplayPopupReminder()
        {
            return Priority == 1;
        }

        private CommandDispatcher commandDispatcher;
        public UtilityNoteViewModel(CommandDispatcher commandDispatcher)
        {
            this.commandDispatcher = commandDispatcher;
        }

        public void LoadNote()
        {
            var note = commandDispatcher.Send(new GetUtilityNote(Account)).Result;
            if (note != null)
            {
                Id = note.Id;
                Comment = note.Text;
                Priority = note.Priority.GetValueOrDefault();
            }
            else
            {
                Comment = "";
                Priority = 0;
            }
        }

        public void Save()
        {
            if (string.IsNullOrWhiteSpace(Comment))
            {
                DeleteComment();
                ClearComment();
                return;
            }

            SaveNote();
            
        }

        private void SaveNote()
        {
            Id = commandDispatcher.Send(new SaveUtilityNote(Account, Priority, Comment)).Result;
        }
        private void DeleteComment()
        {
            if (Id > 0)
            {
                commandDispatcher.Send(new DeleteUtilityNote(Id));
            }
        }

        private void ClearComment()
        {
            Id = 0;
            Comment = "";
            Priority = 0;
        }
    }
}