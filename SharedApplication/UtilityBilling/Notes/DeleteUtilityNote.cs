﻿using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Notes
{
    public class DeleteUtilityNote : Command
    {
        public int NoteId { get; }

        public DeleteUtilityNote(int id)
        {
            NoteId = id;
        }
    }
}