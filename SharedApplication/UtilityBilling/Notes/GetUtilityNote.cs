﻿using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Models;
namespace SharedApplication.UtilityBilling.Notes
{
    public class GetUtilityNote : Command<Comment>
    {
        public int Account { get;  }

        public GetUtilityNote(int account)
        {
            Account = account;
        }
    }
}