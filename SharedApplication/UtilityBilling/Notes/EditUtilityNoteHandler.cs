﻿using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Notes
{
    public class EditUtilityNoteHandler : CommandHandler<EditUtilityNote>
    {
        private IModalView<IUtilityNoteViewModel> view;

        public EditUtilityNoteHandler(IModalView<IUtilityNoteViewModel> view)
        {
            this.view = view;
        }
        protected override void Handle(EditUtilityNote command)
        {
            view.ViewModel.Account = command.Account;
            view.ViewModel.LoadNote();
            view.ShowModal();
        }
    }
}