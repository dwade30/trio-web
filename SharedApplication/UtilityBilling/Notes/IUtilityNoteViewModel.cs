﻿//using SharedApplication.UtilityBilling.Enums;

namespace SharedApplication.UtilityBilling.Notes
{
    public interface IUtilityNoteViewModel
    {
        int Id { get; set; }
        int Account { get; set; }
        string Comment { get; set; }
        int Priority { get; set; }
        bool DisplayPopupReminder();
        void LoadNote();
        void Save();
    }
}