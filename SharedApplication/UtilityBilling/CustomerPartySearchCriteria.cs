﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Enums;

namespace SharedApplication.UtilityBilling
{
	public class CustomerPartySearchCriteria
	{
		public int AccountNumber { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string StreetName { get; set; }
		public string StreetNumber { get; set; }
		public string MapLot { get; set; }
		public int REAccount { get; set; }
		public string SerialNumber { get; set; }
		public string Remote { get; set; }
		public string XRef1 { get; set; }
		public SearchCriteriaMatchType MatchCriteriaType { get; set; } = SearchCriteriaMatchType.StartsWith;
		public bool IncludePreviousOwners { get; set; }
	}
}
