﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling
{
	public class UBBillSearchCriteria
	{
		public int AccountId { get; set; }
		public VoidedStatusSelection VoidedSelection { get; set; }
		public PaidStatusSelection PaidSelection { get; set; }
		public AccountStatusSelection AccountSelection { get; set; }
		public bool CalculateCurrentInterest { get; set; } = false;
		public DateTime EffectiveDate { get; set; } = DateTime.Today;
		public int LienId { get; set; } = 0;
	}

	public enum VoidedStatusSelection
	{
		All = 0,
		NotVoided = 1,
		VoidedOnly = 2
	}

	public enum PaidStatusSelection
	{
		All = 0,
		NotPaid = 1,
		PaidOnly = 2
	}

	public enum AccountStatusSelection
	{
		All = 0,
		NotDeleted = 1,
		DeletedOnly = 2
	}
}
