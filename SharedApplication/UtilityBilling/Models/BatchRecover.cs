﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class BatchRecover
	{
		public int Id { get; set; }
		public string TellerId { get; set; } = "";
		public string PaidBy { get; set; } = "";
		public string Reference { get; set; } = "";
		public string Name { get; set; } = "";
		public int? AccountNumber { get; set; } = 0;
		public int? BillNumber { get; set; } = 0;
		public string Service { get; set; } = "";
		public double? Principal { get; set; } = 0;
		public double? Tax { get; set; } = 0;
		public double? Interest { get; set; } = 0;
		public double? Cost { get; set; } = 0;
		public bool PrintReceipt { get; set; } = false;
		public DateTime? BatchRecoverDate { get; set; }
		public DateTime? ETDate { get; set; }
		public int? PaymentId { get; set; } = 0;
		public int? ImportId { get; set; } = 0;
		public bool PayCorrect { get; set; } = false;
		public bool Processed { get; set; } = false;
		public string PaymentIdentifier { get; set; } = "";
	}
}
