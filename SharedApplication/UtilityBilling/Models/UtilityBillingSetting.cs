﻿using System;

namespace SharedApplication.UtilityBilling.Models
{
    public partial class UtilityBillingSetting
    {
        public int ID { get; set; }

        public int? CurrentAccount { get; set; }

        public double? ConsumptionPercent { get; set; }

        public string Interest { get; set; }

        public bool? LienBill { get; set; }

        public string LienIntMethod { get; set; }

        public string PayFirst { get; set; }

        public string Sequence { get; set; }

        public double? TaxRate { get; set; }

        public string BillFormat { get; set; }


        public string CustomBillFormat { get; set; }

        public string BillSortOrder { get; set; }

        public string Service { get; set; } = "";


        public decimal? FlatInterestAmount { get; set; }

        public bool? ShowDEConsumption { get; set; }

  
        public string RegularIntMethod { get; set; }

 
        public string DEMethod { get; set; }


        public string SewerCostAccount { get; set; }


        public string SewerIntAccount { get; set; }

        public string SewerPrincipalAccount { get; set; }


        public string SewerTaxAccount { get; set; }

        public int? SewerKey { get; set; }


        public string WaterCostAccount { get; set; }

        public string WaterIntAccount { get; set; }

        public string WaterPrincipalAccount { get; set; }

        public string WaterTaxAccount { get; set; }

        public int? WaterKey { get; set; }

        public string Code { get; set; }

        public bool? PayWaterFirst { get; set; }

        public int? Basis { get; set; }

        public double? DiscountPercent { get; set; }

        public double? OverPayInterestRate { get; set; }

        public bool? AuditSeqReceipt { get; set; }

        public int? AdjustmentCMFLaser { get; set; }

        public int? AdjustmentLabels { get; set; }

        public int? AdjustmentLabelsDMH { get; set; }

        public int? AdjustmentLabelsDMV { get; set; }

        public bool? ShowLastUTAccountInCR { get; set; }


        public string DefaultBillFormat { get; set; }

        public string ReaderExtractType { get; set; }

        public string ExtractFileType { get; set; }

        public bool? ChargeIntPrin { get; set; }

        public bool? ChargeIntTax { get; set; }

        public bool? ChargeIntInt { get; set; }

        public bool? ChargeIntCost { get; set; }

        public bool? ShowLienAmountOnBill { get; set; }

        public bool? SBillToOwner { get; set; }

        public bool? WBillToOwner { get; set; }

        public int? DEOrder { get; set; }

        public int? AccountDefaultCategoryW { get; set; }

        public int? AccountDefaultCategoryS { get; set; }

        public int? MeterDefaultSize { get; set; }

        public int? MeterDefaultDigits { get; set; }

        public int? MeterDefaultFrequency { get; set; }


        public string AccountDefaultAccountW { get; set; }

        public string AccountDefaultAccountS { get; set; }

        public string AccountDefaultCity { get; set; }


        public string AccountDefaultState { get; set; }

        public string AccountDefaultZip { get; set; }

        public bool? AccountDefaultUseREInfo { get; set; }

        public bool? AccountDefaultMHInfo { get; set; }

        public int? ReadingUnits { get; set; }

        public string UTReturnAddress1 { get; set; }

        public string UTReturnAddress2 { get; set; }


        public string UTReturnAddress3 { get; set; }


        public string UTReturnAddress4 { get; set; }

        public bool? SplitBilling { get; set; }

        public bool? OnlyCheckOldestBillForLien { get; set; }

        public bool? UseBookForLien { get; set; }

        public double? MeterReadingAdjustmentV { get; set; }

        public double? MeterReadingAdjustmentH { get; set; }

        public int? ReadingUnitsOnBill { get; set; }

        public int? BillingRepSeq { get; set; }

        public bool? HideMinimumVarianceWarning { get; set; }

        public int? RemoteReaderExportType { get; set; }

        public bool? ExportNoBillRecords { get; set; }

        public bool? ShowMapLotInUTAudit { get; set; }

        public bool? AutoPrepayments { get; set; }

        public int? AutoPaymentApplyTo { get; set; }

        public bool? GeneralFixPastDueAmts { get; set; }

        public bool? ShowTaxAcquiredCaption { get; set; }

        public int? IConnectUtilityID { get; set; }

        public bool? ICUseCRAccounts { get; set; }

        public string ICMFeeAccount { get; set; }

        public bool? UpdateRETaxAcquired { get; set; }

        public bool? CheckRETaxAcquired { get; set; }

        public bool? ZeroBillOption { get; set; }

        public bool? DefaultUTRTDToAutoChange { get; set; }

        public DateTime? RanClearOldReplacementConsumption { get; set; }

        public string RptDesc_PastDue { get; set; }

 
        public string RptDesc_Credit { get; set; }

 
        public string RptDesc_Regular { get; set; }

        public string RptDesc_Override { get; set; }


        public string RptDesc_Interest { get; set; }


        public string RptDesc_Liens { get; set; }

        public string RptDesc_Tax { get; set; }


        public string RptDesc_Misc { get; set; }

        public string RptDesc_Adj { get; set; }

        public bool? RateTypeChange { get; set; }

        public bool? OutprintChapt660 { get; set; }


        public string InvCldBillerGUID { get; set; }

        public string InvCldWebSrvcKey { get; set; }

        public DateTime? InvCldInvStartDate { get; set; }

        public bool? RanGeneralFix { get; set; }

        public int? AutoPayOption { get; set; }

        public bool? ExportLocation { get; set; }

        public string Version { get; set; }

        public bool? BillStormwaterFee { get; set; }

        public DateTime? LienFeeIncrRunOn { get; set; }

        public bool? ForceReadingDate { get; set; }

        public bool? DisableAutoPmtFill { get; set; }

        public bool? SkipLDNPrompt { get; set; }

        public int? OutPrintIncludeChangeOutCons { get; set; }

        public bool? DefDisplayHistory { get; set; }
    }
}