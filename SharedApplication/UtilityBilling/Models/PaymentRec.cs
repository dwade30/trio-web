﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public partial class PaymentRec
	{
		public int ID { get; set; }
		public int? AccountKey { get; set; } = 0;
		public int? Year { get; set; } = 0;
		public int? BillKey { get; set; } = 0;
		public int? BillId { get; set; }
		public int? LienId { get; set; }
		public int? BillNumber { get; set; }
		public int? CHGINTNumber { get; set; } = 0;
		public DateTime? CHGINTDate { get; set; }
		public DateTime? ActualSystemDate { get; set; }
		public DateTime? EffectiveInterestDate { get; set; }
		public DateTime? RecordedTransactionDate { get; set; }
		public string Teller { get; set; } = "";
		public string Reference { get; set; } = "";
		public string Period { get; set; } = "";
		public string Code { get; set; } = "";
		public int? ReceiptNumber { get; set; } = 0;
		public bool? DOSReceiptNumber { get; set; } = false;
		public decimal? Principal { get; set; } = 0;
		public decimal? PreLienInterest { get; set; } = 0;
		public decimal? CurrentInterest { get; set; } = 0;
		public decimal? LienCost { get; set; } = 0;
		public decimal? Tax { get; set; } = 0;
		public string TransNumber { get; set; } = "";
		public string PaidBy { get; set; } = "";
		public string Comments { get; set; } = "";
		public string CashDrawer { get; set; } = "";
		public string GeneralLedger { get; set; } = "";
		public string BudgetaryAccountNumber { get; set; }
		public string Service { get; set; } = "";
		public bool? LienPayment { get; set; } = false;
		public int? DailyCloseOut { get; set; } = 0;
		public bool? Uploaded { get; set; } = false;
		public string EPmtID { get; set; } = "";
		public Guid? CorrelationIdentifier { get; set; }
		public Guid? TransactionIdentifier { get; set; }
		public Bill Bill { get; set; }
		public Lien Lien { get; set; }
	}
}
