﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public partial class MeterTable
	{
		public int ID { get; set; }

		public int? MeterNumber { get; set; }

		public int? AccountKey { get; set; }

		public int? BookNumber { get; set; }

		public int? Sequence { get; set; }

		public string SerialNumber { get; set; }

		public int? Size { get; set; }

		public string Service { get; set; }

		public int? Frequency { get; set; }

		public int? Digits { get; set; }

		public int? Multiplier { get; set; }

		public string Combine { get; set; }

		public int? ReplacementConsumption { get; set; }

		public DateTime? ReplacementDate { get; set; }

		public string Location { get; set; }

		public string AdjustDescription { get; set; }

		public double? WaterCharged { get; set; }

		public double? SewerCharged { get; set; }

		public int? WaterPercent { get; set; }

		public int? WaterTaxPercent { get; set; }

		public int? WaterType1 { get; set; }

		public int? WaterType2 { get; set; }

		public int? WaterType3 { get; set; }

		public int? WaterType4 { get; set; }

		public int? WaterType5 { get; set; }

		public double? WaterAmount1 { get; set; }

		public double? WaterAmount2 { get; set; }

		public double? WaterAmount3 { get; set; }

		public double? WaterAmount4 { get; set; }

		public double? WaterAmount5 { get; set; }

		public int? WaterKey1 { get; set; }

		public int? WaterKey2 { get; set; }

		public int? WaterKey3 { get; set; }

		public int? WaterKey4 { get; set; }

		public int? WaterKey5 { get; set; }

		public string WaterAccount1 { get; set; }

		public string WaterAccount2 { get; set; }

		public string WaterAccount3 { get; set; }

		public string WaterAccount4 { get; set; }

		public string WaterAccount5 { get; set; }

		public int? WaterAdjustKey { get; set; }

		public double? WaterAdjustAmount { get; set; }

		public string WaterAdjustAccount { get; set; }

		public string WaterAdjustDescription { get; set; }

		public int? WaterConsumption { get; set; }

		public int? WaterConsumptionOverride { get; set; }

		public int? SewerPercent { get; set; }

		public int? SewerTaxPercent { get; set; }

		public int? SewerType1 { get; set; }

		public int? SewerType2 { get; set; }

		public int? SewerType3 { get; set; }

		public int? SewerType4 { get; set; }

		public int? SewerType5 { get; set; }

		public double? SewerAmount1 { get; set; }

		public double? SewerAmount2 { get; set; }

		public double? SewerAmount3 { get; set; }

		public double? SewerAmount4 { get; set; }

		public double? SewerAmount5 { get; set; }

		public int? SewerKey1 { get; set; }

		public int? SewerKey2 { get; set; }

		public int? SewerKey3 { get; set; }

		public int? SewerKey4 { get; set; }

		public int? SewerKey5 { get; set; }

		public string SewerAccount1 { get; set; }

		public string SewerAccount2 { get; set; }

		public string SewerAccount3 { get; set; }

		public string SewerAccount4 { get; set; }

		public string SewerAccount5 { get; set; }

		public double? SewerAdjustAmount { get; set; }

		public int? SewerAdjustKey { get; set; }

		public string SewerAdjustAccount { get; set; }

		public string SewerAdjustDescription { get; set; }

		public int? SewerConsumption { get; set; }

		public int? SewerConsumptionOverride { get; set; }

		public int? PreviousReading { get; set; }

		public string PreviousCode { get; set; }

		public DateTime? PreviousReadingDate { get; set; }

		public int? CurrentReading { get; set; }

		public DateTime? CurrentReadingDate { get; set; }

		public string CurrentCode { get; set; }

		public DateTime? DateOfChange { get; set; }

		public string Remote { get; set; }

		public int? WCat { get; set; }

		public int? SCat { get; set; }

		public bool? Backflow { get; set; }

		public DateTime? BackflowDate { get; set; }

		public DateTime? SetDate { get; set; }

		public string BillingCode { get; set; }

		public string Comment { get; set; }

		public bool? FinalBilled { get; set; }

		public bool? NoBill { get; set; }

		public string BillingStatus { get; set; }

		public bool? UseRate1 { get; set; }

		public bool? UseRate2 { get; set; }

		public bool? UseRate3 { get; set; }

		public bool? UseRate4 { get; set; }

		public bool? UseRate5 { get; set; }

		public bool? UseAdjustRate { get; set; }

		public bool? Radio { get; set; }

		public bool? IncludeInExtract { get; set; }

		public int? BilledConsumption { get; set; }

		public bool? UseMeterChangeOut { get; set; }

		public int? ConsumptionOfChangedOutMeter { get; set; }

		public string XRef1 { get; set; }

		public string XRef2 { get; set; }

		public bool? NegativeConsumption { get; set; }

		public DateTime? FinalEndDate { get; set; }

		public DateTime? FinalStartDate { get; set; }

		public DateTime? FinalBillDate { get; set; }

		public bool? Final { get; set; }

		public string MXU { get; set; }

		public int? RadioAccessType { get; set; }

		public string Lat { get; set; }

		public string Long { get; set; }

		public bool? PrevChangeOut { get; set; }

		public string ReaderCode { get; set; }

		public int? PrevChangeOutConsumption { get; set; }

		public Master Master { get; set; }
		public CustomerParty CustomerParty { get; set; }
	}
}
