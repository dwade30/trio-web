﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class DischargeNeeded
	{
		public int Id { get; set; }

		public DateTime? DatePaid { get; set; }

		public DateTime? UpdatedDate { get; set; }

		public string Teller { get; set; }

		public int? LienId { get; set; }

		public int? BillId { get; set; }

		public bool? Printed { get; set; }

		public bool? Batch { get; set; }

		public bool? Cancelled { get; set; }

		public int? Book { get; set; }

		public int? Page { get; set; }
	}
}
