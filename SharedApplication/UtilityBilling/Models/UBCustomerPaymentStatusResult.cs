﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class UBCustomerPaymentStatusResult
	{
		public int Id { get; set; }
		public int AccountNumber { get; set; }
		public int StreetNumber { get; set; }
		public string StreetName { get; set; }
		public string BilledFullNameLF { get; set; }
		public string Billed2FullNameLF { get; set; }
		public int PartyId { get; set; }
		public string FullNameLF { get; set; }
		public string SecondOwnerFullNameLF { get; set; }
		public string BilledAddress1 { get; set; }
		public string BilledAddress2 { get; set; }
		public string BilledAddress3 { get; set; }
		public string BilledCity { get; set; }
		public string BilledState { get; set; }
		public string BilledZip { get; set; }
		public string OwnerAddress1 { get; set; }
		public string OwnerAddress2 { get; set; }
		public string OwnerAddress3 { get; set; }
		public string OwnerCity { get; set; }
		public string OwnerState { get; set; }
		public string OwnerZip { get; set; }
		public string MapLot { get; set; }
		public int REAccount { get; set; }
		public bool Deleted { get; set; }
		public bool PreviousOwner { get; set; }
	}
}
