﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class ControlLienProcess
	{
		public int Id { get; set; }
		public int? BillingYear { get; set; }
		public double? MinimumAmount { get; set; }
		public DateTime? FilingDate { get; set; }
		public string CollectorName { get; set; }
		public string Muni { get; set; }
		public string County { get; set; }
		public string State { get; set; }
		public string Signer { get; set; }
		public string Designation { get; set; }
		public double? FilingFee { get; set; }
		public double? CertMailFee { get; set; }
		public string MapPreparer { get; set; }
		public string MapPrepareDate { get; set; }
		public bool? PayCertMailFee { get; set; }
		public bool? SendCopyToMortHolder { get; set; }
		public bool? ChargeForMortHolder { get; set; }
		public string SendCopyToNewOwner { get; set; }
		public DateTime? DateCreated { get; set; }
		public string User { get; set; }
		public string OldCollector { get; set; }
		public DateTime? RecommitmentDate { get; set; }
		public DateTime? DateUpdated { get; set; }
		public double? MailTo { get; set; }
		public string DefaultSort { get; set; }
		public DateTime? CommissionExpirationDate { get; set; }
		public string LegalDescription { get; set; }
		public string MuniTitle { get; set; }
		public string CollectorTitle { get; set; }
	}
}
