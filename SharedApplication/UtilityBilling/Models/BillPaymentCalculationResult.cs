﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class BillPaymentCalculationResult
	{
		public decimal Principal { get; set; }
		public decimal Tax { get; set; }
		public decimal Cost { get; set; }
		public decimal Interest { get; set; }
		public decimal PreLienInterest { get; set; }
		public decimal RemainingAmount { get; set; }
	}
}
