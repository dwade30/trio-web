﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class Comment
	{
		public int Id { get; set; }
		public int? Account { get; set; }
		public string Text { get; set; }
		public int? Priority { get; set; }
		public string Type { get; set; }
		public DateTime? DateUpdated { get; set; }
	}
}
