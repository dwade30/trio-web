﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public partial class CommentRec
	{
		public int Id { get; set; }
		public int? AccountId { get; set; }
		public string Text { get; set; }
		public DateTime? Updated { get; set; }
		public Master Master { get; set; }
	}
}
