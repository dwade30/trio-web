﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class tblAcctACH
	{
		public int Id { get; set; }
		public int? AccountKey { get; set; }
		public string Service { get; set; }
		public string ACHBankRT { get; set; }
		public string ACHAcctNumber { get; set; }
		public string ACHAcctType { get; set; }
		public double? ACHLimit { get; set; }
		public bool? ACHActive { get; set; }
		public bool? ACHPrenote { get; set; }
	}
}
