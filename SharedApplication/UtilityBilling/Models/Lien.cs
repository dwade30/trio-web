﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public partial class Lien
	{
		public int ID { get; set; }
		public DateTime? IntPaidDate { get; set; }
		public double? Principal { get; set; }
		public double? Tax { get; set; }
		public double? Interest { get; set; }
		public double? IntAdded { get; set; }
		public double? Costs { get; set; }
		public double? PrinPaid { get; set; }
		public double? TaxPaid { get; set; }
		public double? PLIPaid { get; set; }
		public double? IntPaid { get; set; }
		public double? CostPaid { get; set; }
		public double? MaturityFee { get; set; }
		public DateTime? DateCreated { get; set; }
		public string Status { get; set; }
		public int? RateKeyId { get; set; }
		public bool? Water { get; set; }
		public int? Book { get; set; }
		public int? Page { get; set; }
		public bool? PrintedLDN { get; set; }
		public DateTime? LastUpdatedDate { get; set; }
		public bool? Uploaded { get; set; }
		public RateKey RateKey { get; set; }
		public DateTime? PreviousInterestPaidDate { get; set; }
		public ICollection<PaymentRec> Payments { get; set; } = new List<PaymentRec>();
	}
}
