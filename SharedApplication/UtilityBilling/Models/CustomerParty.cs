﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class CustomerParty
	{
		public int ID { get; set; }
		public int? AccountNumber { get; set; }
		public int? BillingPartyID { get; set; }
		public int? SecondBillingPartyID { get; set; }
		public int? SewerCategory { get; set; }
		public string SewerAccount { get; set; }
		public int? WaterCategory { get; set; }
		public string WaterAccount { get; set; }
		public int? REAccount { get; set; }
		public int? OwnerPartyID { get; set; }
		public int? SecondOwnerPartyID { get; set; }
		public DateTime? DateOfChange { get; set; }
		public string Comment { get; set; }
		public string MapLot { get; set; }
		public int? Code { get; set; }
		public bool? Deleted { get; set; }
		public string Directions { get; set; }
		public string BillMessage { get; set; }
		public string DataEntry { get; set; }
		public bool? UseREAccount { get; set; }
		public bool? UseMortgageHolder { get; set; }
		public bool? SameBillOwner { get; set; }
		public string BookPage { get; set; }
		public bool? FinalBill { get; set; }
		public bool? NoBill { get; set; }
		public decimal? Deposit { get; set; }
		public string Apt { get; set; }
		public string StreetNumber { get; set; }
		public string StreetName { get; set; }
		public bool? TaxAcquired { get; set; }
		public DateTime? TADate { get; set; }
		public string Telephone { get; set; }
		public string Email { get; set; }
		public bool? InBankruptcy { get; set; }
		public string RefAccountNumber { get; set; }
		public int? Book { get; set; }
		public int? Page { get; set; }
		public int? ResCode { get; set; }
		public int? UTBook { get; set; }
		public bool? EmailBill { get; set; }
		public bool? WBillToOwner { get; set; }
		public bool? SBillToOwner { get; set; }
		public decimal? ImpervSurfArea { get; set; }
		public string DeedName1 { get; set; }
		public string DeedName2 { get; set; }
		public Guid? PartyGuid { get; set; }
		public int? PartyType { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string Designation { get; set; }
		public string WebAddress { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Country { get; set; }
		public string OverrideName { get; set; }
		public string FullName { get; set; }
		public string FullNameLF { get; set; }
		public string BilledNameLF { get; set; }
		public string Billed2NameLF { get; set; }
		public string Owner2FullNameLF { get; set; }
		public string BilledAddress1 { get; set; }
		public string BilledAddress2 { get; set; }
		public string BilledAddress3 { get; set; }
		public string BilledCity { get; set; }
		public string BilledState { get; set; }
		public string BilledZip { get; set; }
		public string BilledCountry { get; set; }
		public ICollection<Bill> Bills { get; set; }
		public ICollection<MeterTable> MeterTables { get; set; }
	}
}
