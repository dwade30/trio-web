﻿using System;
using System.Collections.Generic;

namespace SharedApplication.UtilityBilling.Models
{
	
	public partial class Bill
	{
		public int ID { get; set; }
		public int? AccountId { get; set; } = 0;
		public int? ActualAccountNumber { get; set; } = 0;
		public int? BillNumber { get; set; } = 0;
		public int? BillingYear { get; set; } = 0;
		public int? MeterKey { get; set; } = 0;
		public int? Book { get; set; } = 0;
		public string Service { get; set; } = "";
		public string BillStatus { get; set; } = "";
		public int? RateKeyId { get; set; } = 0;
		public bool? NoBill { get; set; } = false;
		public int? WLienStatusEligibility { get; set; } = 0;
		public int? WLienProcessStatus { get; set; } = 0;
		public int? WLienRecordNumber { get; set; } = 0;
		public int? WCombinationLienKey { get; set; } = 0;
		public int? SLienStatusEligibility { get; set; } = 0;
		public int? SLienProcessStatus { get; set; } = 0;
		public int? SLienRecordNumber { get; set; } = 0;
		public int? SCombinationLienKey { get; set; } = 0;
		public string CombinationCode { get; set; } = "N";
		public DateTime? CurDate { get; set; }
		public int? CurReading { get; set; } = 0;
		public string CurCode { get; set; } = "";
		public DateTime? PrevDate { get; set; }
		public int? PrevReading { get; set; } = 0;
		public string PrevCode { get; set; } = "";
		public string BillingCode { get; set; } = "";
		public int? Consumption { get; set; } = 0;
		public int? WaterOverrideCons { get; set; } = 0;
		public double? WaterOverrideAmount { get; set; } = 0;
		public int? SewerOverrideCons { get; set; } = 0;
		public double? SewerOverrideAmount { get; set; } = 0;
		public double? WMiscAmount { get; set; } = 0;
		public double? WAdjustAmount { get; set; } = 0;
		public double? WDEAdjustAmount { get; set; } = 0;
		public double? WFlatAmount { get; set; } = 0;
		public double? WUnitsAmount { get; set; } = 0;
		public double? WConsumptionAmount { get; set; } = 0;
		public double? SMiscAmount { get; set; } = 0;
		public double? SAdjustAmount { get; set; } = 0;
		public double? SDEAdjustAmount { get; set; } = 0;
		public double? SFlatAmount { get; set; } = 0;
		public double? SUnitsAmount { get; set; } = 0;
		public double? SConsumptionAmount { get; set; } = 0;
		public double? WTax { get; set; } = 0;
		public double? STax { get; set; } = 0;
		public double? TotalWBillAmount { get; set; } = 0;
		public double? TotalSBillAmount { get; set; } = 0;
		public DateTime? WIntPaidDate { get; set; }
		public double? WPrinOwed { get; set; } = 0;
		public double? WTaxOwed { get; set; } = 0;
		public double? WIntOwed { get; set; } = 0;
		public double? WIntAdded { get; set; } = 0;
		public double? WCostOwed { get; set; } = 0;
		public double? WCostAdded { get; set; } = 0;
		public double? WPrinPaid { get; set; } = 0;
		public double? WTaxPaid { get; set; } = 0;
		public double? WIntPaid { get; set; } = 0;
		public double? WCostPaid { get; set; } = 0;
		public DateTime? SIntPaidDate { get; set; }
		public double? SPrinOwed { get; set; } = 0;
		public double? STaxOwed { get; set; } = 0;
		public double? SIntOwed { get; set; } = 0;
		public double? SIntAdded { get; set; } = 0;
		public double? SCostOwed { get; set; } = 0;
		public double? SCostAdded { get; set; } = 0;
		public double? SPrinPaid { get; set; } = 0;
		public double? STaxPaid { get; set; } = 0;
		public double? SIntPaid { get; set; } = 0;
		public double? SCostPaid { get; set; } = 0;
		public int? WRT1 { get; set; } = 0;
		public int? WRT2 { get; set; } = 0;
		public int? WRT3 { get; set; } = 0;
		public int? WRT4 { get; set; } = 0;
		public int? WRT5 { get; set; } = 0;
		public int? SRT1 { get; set; } = 0;
		public int? SRT2 { get; set; } = 0;
		public int? SRT3 { get; set; } = 0;
		public int? SRT4 { get; set; } = 0;
		public int? SRT5 { get; set; } = 0;
		public string Location { get; set; } = "";
		public DateTime? CreationDate { get; set; }
		public DateTime? LastUpdatedDate { get; set; }
		public string BName { get; set; } = "";
		public string BName2 { get; set; } = "";
		public string BAddress1 { get; set; } = "";
		public string BAddress2 { get; set; } = "";
		public string BAddress3 { get; set; } = "";
		public string BCity { get; set; } = "";
		public string BState { get; set; } = "";
		public string BZip { get; set; } = "";
		public string BZip4 { get; set; } = "";
		public string OName { get; set; } = "";
		public string OName2 { get; set; } = "";
		public string OAddress1 { get; set; } = "";
		public string OAddress2 { get; set; } = "";
		public string OAddress3 { get; set; } = "";
		public string OCity { get; set; } = "";
		public string OState { get; set; } = "";
		public string OZip { get; set; } = "";
		public string OZip4 { get; set; } = "";
		public int? Copies { get; set; } = 0;
		public string CertifiedMailNumber { get; set; } = "";
		public string Note { get; set; } = "";
		public string MapLot { get; set; } = "";
		public string BookPage { get; set; } = "";
		public string Telephone { get; set; } = "";
		public string Email { get; set; } = "";
		public int? WCat { get; set; } = 0;
		public int? SCat { get; set; } = 0;
		public bool? LoadBack { get; set; } = false;
		public string BillMessage { get; set; } = "";
		public DateTime? FinalEndDate { get; set; }
		public DateTime? FinalStartDate { get; set; }
		public DateTime? FinalBillDate { get; set; }
		public bool? Final { get; set; } = false;
		public bool? WBillOwner { get; set; } = false;
		public bool? SBillOwner { get; set; } = false;
		public DateTime? BillDate { get; set; }
		public int? ReadingUnits { get; set; } = 0;

		public double? WOrigBillAmount { get; set; } = 0;
		public double? SOrigBillAmount { get; set; } = 0;
		public bool? SHasOverride { get; set; } = false;
		public bool? WHasOverride { get; set; } = false;
		public int? SDemandGroupID { get; set; } = 0;
		public int? WDemandGroupID { get; set; } = 0;
		public bool? SendEBill { get; set; } = false;
		public bool? WinterBill { get; set; } = false;
		public bool? MeterChangedOut { get; set; } = false;
		public bool? SUploaded { get; set; } = false;
		public bool? WUploaded { get; set; } = false;
		public string IMPBTrackingNumber { get; set; } = "";
		public bool? LienProcessExclusion { get; set; } = false;
		public int? MeterChangedOutConsumption { get; set; } = 0;
		public DateTime? SPreviousInterestPaidDate { get; set; }
		public DateTime? WPreviousInterestPaidDate { get; set; }
		public RateKey RateKey { get; set; }
		public CustomerParty CustomerParty { get; set; }
		public ICollection<PaymentRec> Payments { get; set; } = new List<PaymentRec>();
	}
}
