﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public class ControlDischargeNotice
	{
		public int Id { get; set; }
		public string County { get; set; }
		public string Treasurer { get; set; }
		public bool? Male { get; set; }
		public string SignerName { get; set; }
		public string SignerDesignation { get; set; }
		public DateTime? CommissionExpiration { get; set; }
	}
}
