﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Models
{
	public partial class RateKey
	{
		public int ID { get; set; }

		public DateTime? Start { get; set; }

		public DateTime? End { get; set; }

		public DateTime? IntStart { get; set; }

		public DateTime? DateCreated { get; set; }

		public string Description { get; set; }

		public double? WIntRate { get; set; }

		public double? SIntRate { get; set; }

		public DateTime? BillDate { get; set; }

		public string RateType { get; set; }

		public DateTime? ThirtyDayNoticeDateW { get; set; }

		public DateTime? LienDateW { get; set; }

		public DateTime? MaturityDateW { get; set; }

		public DateTime? ThirtyDayNoticeDateS { get; set; }

		public DateTime? LienDateS { get; set; }

		public DateTime? MaturityDateS { get; set; }

		public DateTime? DueDate { get; set; }

		public ICollection<Bill> Bills { get; set; }

		public ICollection<Lien> Liens { get; set; }
	}
}
