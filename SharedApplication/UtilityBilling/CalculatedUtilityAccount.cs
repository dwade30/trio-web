﻿using System.Collections.Generic;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling
{
    public class CalculatedUtilityAccount
    {
        public UtilityAccount UtilityAccount { get; set; }
        public UtilityCalculationSummary AccountSummary { get; set; }


    }
}