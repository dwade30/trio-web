﻿using System;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling
{
    public class ViewUTAccountHandler :CommandHandler<ViewUTAccount>
    {
        private IView<IUtilityBillingCustomerSearchViewModel> searchView;
        private CommandDispatcher commandDispatcher;
        public ViewUTAccountHandler(IView<IUtilityBillingCustomerSearchViewModel> searchView,
            CommandDispatcher commandDispatcher)
        {
            this.searchView = searchView;
            this.commandDispatcher = commandDispatcher;
        }
        protected override void Handle(ViewUTAccount command)
        {
            searchView.ViewModel.ShowPaymentScreen = false;
            searchView.ViewModel.ShowAccountScreen = true;
            searchView.ViewModel.SetCorrelationId(Guid.Empty);
            searchView.ViewModel.StartedFromCashReceipts = false;
            searchView.ViewModel.AllowBatch = false;
            searchView.Show();
        }
    }
}