﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling
{
    public class UtilityAccount
    {
        public int Account { get;}
        private List<UBBill> bills = new List<UBBill>();
        public IEnumerable<UBBill> Bills
        {
            get => bills;
        }

        public void AddBill(UBBill bill)
        {
            if (bill != null)
            {
                bills.Add(bill);
            }
        }

        public void AddBills(IEnumerable<UBBill> bills)
        {
            if (bills != null && bills.Any())
            {
                this.bills.AddRange(bills);
            }
        }

        public UtilityAccount(int account)
        {
            Account = account;
        }
    }
}