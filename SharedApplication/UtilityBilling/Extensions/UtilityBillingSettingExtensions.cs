﻿using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Extensions
{
    public static class UtilityBillingSettingExtensions
    {
        public static GlobalUtilityBillingSettings ToGlobalUtilityBillingSettings(
            this UtilityBillingSetting utilityBillingSetting)
        {
            if (utilityBillingSetting == null)
            {
                return new GlobalUtilityBillingSettings();
            }
            var setting = new GlobalUtilityBillingSettings();
            switch (utilityBillingSetting.Service.ToLower())
            {
                case "b":
                    setting.HasSewerService = true;
                    setting.HasWaterService = true;
                    break;
                case "s":
                    setting.HasSewerService = true;
                    break;
                case "w":
                    setting.HasWaterService = true;
                    break;
            }

            setting.ShowLastUBAccountInCR = utilityBillingSetting.ShowLastUTAccountInCR ?? false;
            setting.gintBasis = utilityBillingSetting.Basis ?? 0;
            setting.TownService = utilityBillingSetting.Service;
            setting.OnlyCheckOldestBillForLien = utilityBillingSetting.OnlyCheckOldestBillForLien ?? false;
            setting.DisableAutoPaymentInsert = utilityBillingSetting.DisableAutoPmtFill ?? false;
            setting.DefaultInterestMethod = utilityBillingSetting.RegularIntMethod;
            setting.FlatInterestAmount = utilityBillingSetting.FlatInterestAmount ?? 0;
            setting.EarnedInterestRate = (decimal)(utilityBillingSetting.OverPayInterestRate ?? 0);
            setting.CheckRETaxAcquired = utilityBillingSetting.CheckRETaxAcquired ?? false;
            setting.PayWaterFirst = utilityBillingSetting.PayWaterFirst ?? false;
            setting.AutoPayOldestBillFirst = utilityBillingSetting.AutoPayOption == 0;
			return setting;
        }
    }
}