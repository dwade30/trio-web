﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;
using SharedApplication.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling
{
	public class BillCalculationService : IBillCalculationService
	{
		private GlobalUtilityBillingSettings globalUtilityBillingSettings;
		private IInterestCalculationService interestCalculationService;

		public BillCalculationService(GlobalUtilityBillingSettings globalUtilityBillingSettings, IInterestCalculationService interestCalculationService)
		{
			this.globalUtilityBillingSettings = globalUtilityBillingSettings;
			this.interestCalculationService = interestCalculationService;
		}

		public CalculateBillResult CalculateUtilityTotals(UBBillUtilityDetails utility, DateTime effectiveDate)
		{
			DateTime dtStartDate;
			bool boolPerDiem = false;

			int intDayAdded = 0;

			CalculateBillResult result = new CalculateBillResult();

			boolPerDiem = globalUtilityBillingSettings.DefaultInterestMethod == "P";

			if (utility.IntPaidDate != null && utility.IntPaidDate >= utility.InterestStartDate)
			{
				dtStartDate = (DateTime)utility.IntPaidDate;
				intDayAdded = 0;
			}
			else
			{
				dtStartDate = (DateTime)utility.InterestStartDate;
				intDayAdded = 1;
			}

			result.LastInterestDate = dtStartDate;
			result.CalculatedInterestDate = effectiveDate.Date;
			result.InterestCalculated = false;

			if (effectiveDate < dtStartDate)
			{
				result.CalculatedInterestResult.Interest = 0;
				result.CalculatedInterestDate = dtStartDate;
				result.CalculatedInterestResult.PerDiem = 0;
			}
			else
			{
				if (utility.PrincipalOwed - utility.PrincipalPaid > 0)
				{
					if (boolPerDiem)
					{
						result.CalculatedInterestResult = CalculateInterest((double)(utility.PrincipalOwed - utility.PrincipalPaid), dtStartDate, effectiveDate, utility.InterestRate, globalUtilityBillingSettings.gintBasis, intDayAdded, 0);
						result.InterestCalculated = true;
					}
					else
					{
						if (utility.IntAdded == 0)
						{
							result.CalculatedInterestResult.Interest = globalUtilityBillingSettings.FlatInterestAmount;
							result.InterestCalculated = true;
						}
					}
				}
				else
				{
					result.CalculatedInterestResult.Interest = 0;
					result.InterestCalculated = false;
				}
			}

			return result;
		}

		public CalculateInterestResult CalculateInterest(Double curAmount, DateTime dtInterestStartDate, DateTime dtDateNow, double dblRateAsDecimal, int intBasis, int usingStartDate, double dblOverPaymentIntRate = 0)
		{
			CalculateInterestResult result = new CalculateInterestResult();
			double dblPrincipalDue;
			int intDays;
			int lngPerDays;

			dblPrincipalDue = curAmount * -1;
			intDays = intBasis;
			if (intDays == 0)
				intDays = 360;

			TimeSpan difference = dtDateNow.Date - dtInterestStartDate;
			lngPerDays = difference.Days + usingStartDate;

			if (lngPerDays < 1)
			{
				result.Interest = 0;
				result.PerDiem = interestCalculationService.IPmt((dblRateAsDecimal / intDays), 1, 1, dblPrincipalDue);
			}
			else if (curAmount == 0)
			{
				result.Interest = 0;
				result.PerDiem = 0;
			}
			else
			{
				if (curAmount > 0)
				{
					result.PerDiem = interestCalculationService.IPmt((dblRateAsDecimal / intDays), 1, lngPerDays, dblPrincipalDue);
				}
				else
				{
					result.PerDiem = interestCalculationService.IPmt((dblOverPaymentIntRate / intDays), 1, lngPerDays, dblPrincipalDue);
				}
				result.Interest = (decimal)Math.Round(result.PerDiem * lngPerDays, 2, MidpointRounding.AwayFromZero);
			}
			return result;
		}
	}
}
