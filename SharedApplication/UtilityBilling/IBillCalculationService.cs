﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling
{
	public interface IBillCalculationService
	{
		CalculateBillResult CalculateUtilityTotals(UBBillUtilityDetails utility, DateTime effectiveDate);

		CalculateInterestResult CalculateInterest(Double curAmount, DateTime dtInterestStartDate, DateTime dtDateNow,
			double dblRateAsDecimal, int intBasis, int usingStartDate, double dblOverPaymentIntRate);
	}
}
