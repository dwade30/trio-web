﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling
{
    public class GetCalculatedUtilityAccountHandler : CommandHandler<GetCalculatedUtilityAccount,CalculatedUtilityAccount>
    {
        private IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler;
        private IUtilityBillingContext utilityContext;
        public GetCalculatedUtilityAccountHandler(IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler, IUtilityBillingContext utilityContext)
        {
            this.billQueryHandler = billQueryHandler;
            this.utilityContext = utilityContext;
        }
        protected override CalculatedUtilityAccount Handle(GetCalculatedUtilityAccount command)
        {
            var accountId = utilityContext.Masters.Where(u => u.AccountNumber == command.Account).Select(u => u.ID).FirstOrDefault();
            if (accountId == 0)
            {
                return null;
            }
            var bills = billQueryHandler.ExecuteQuery(new UBBillSearchCriteria()
            {
                EffectiveDate =  command.EffectiveDate,
                CalculateCurrentInterest = true,
                AccountSelection =  AccountStatusSelection.All,
                AccountId = accountId
            });
            var utilityAccount = new UtilityAccount(command.Account);
            utilityAccount.AddBills(bills);
            var calculatedAccount = new CalculatedUtilityAccount();
            calculatedAccount.AccountSummary = new UtilityCalculationSummary(command.Account);
            if (bills != null)
            {
                foreach (var bill in bills)
                {
                    foreach (var detail in bill.UtilityDetails)
                    {
                        var summary = new UtilityCalculationSummary(command.Account);
                        summary.Balance = detail.BalanceDue;
                        calculatedAccount.AccountSummary.AddTo(summary);
                    }
                }
            }

            return calculatedAccount;
        }
    }
}