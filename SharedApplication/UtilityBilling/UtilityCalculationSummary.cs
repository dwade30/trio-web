﻿namespace SharedApplication.UtilityBilling
{
    public class UtilityCalculationSummary
    {
        public int Account { get; }
        public decimal Balance { get; set; }
        //public decimal PerDiem { get; set; } = 0;
        //public decimal ChargedInterest { get; set; }
        //public decimal Charged { get; set; }
        //public decimal PrincipalPaid { get; set; }
        public UtilityCalculationSummary(int account)
        {
            Account = account;
        }

        public void AddTo(UtilityCalculationSummary billSummary)
        {
            //CostsCharged += billSummary.CostsCharged;
            //CostsPaid += billSummary.CostsPaid;
            //InterestPaid += billSummary.InterestPaid;
            //ChargedInterest += billSummary.ChargedInterest;
            //Charged += billSummary.Charged;
            //PrincipalPaid += billSummary.PrincipalPaid;
            Balance += billSummary.Balance;
            //CurrentInterest += billSummary.CurrentInterest;
            //PerDiem += billSummary.PerDiem;
        }
    }
}