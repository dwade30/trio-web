﻿using System.Linq;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling
{
    public interface IUtilityBillingContext
    {
        IQueryable<PaymentRec> PaymentRecs { get;  }
        IQueryable<Lien> Liens { get;  }
        IQueryable<Bill> Bills { get;  }
        IQueryable<RateKey> RateKeys { get;  }
        IQueryable<Master> Masters { get; }
        IQueryable<MeterTable> MeterTables { get; }
        IQueryable<CustomerParty> CustomerParties { get; }
        IQueryable<Comment> Comments { get; }
        IQueryable<CommentRec> CommentRecs { get; }
        IQueryable<tblAcctACH> tblAcctACHs { get; }
        IQueryable<DischargeNeeded> DischargeNeededs { get; }
        IQueryable<BatchRecover> BatchRecovers { get; }
		IQueryable<UtilityBillingSetting> UtilityBillingSettings { get; }

		IQueryable<SewerControlDischargeNotice> SewerControlDischargeNotices { get; }
		IQueryable<SewerControlLienProcess> SewerControlLienProcesses { get; }
		IQueryable<WaterControlDischargeNotice> WaterControlDischargeNotices { get; }
		IQueryable<WaterControlLienProcess> WaterControlLienProcesses { get; }
    }
}