﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling.Commands
{
	public class ShowLienDischargeNotice : Command
	{
		public DateTime PayDate { get;}
		public Lien LienPaid{ get;}

		public ShowLienDischargeNotice(DateTime payDate, Lien lienPaid)
		{
			PayDate = payDate;
			LienPaid = lienPaid;
		}
	}
}
