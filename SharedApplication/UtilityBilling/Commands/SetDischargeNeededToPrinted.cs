﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Commands
{
	public class SetDischargeNeededToPrinted : Command
	{
		public DateTime UpdatedDate { get; }
		public int LienId { get; }
		public DateTime DatePaid { get; }
		public SetDischargeNeededToPrinted(int lienId, DateTime updatedDate, DateTime datePaid)
		{
			UpdatedDate = updatedDate;
			LienId = lienId;
			DatePaid = datePaid;
		}
    }
}
