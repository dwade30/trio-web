﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling.Commands
{
	public class GetControlDischargeNotice : Command<ControlDischargeNotice>
	{
		public UtilityType Service { get; set; }
	}
}
