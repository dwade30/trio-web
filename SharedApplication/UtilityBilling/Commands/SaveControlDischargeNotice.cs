﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling.Commands
{
	public class SaveControlDischargeNotice : Command
	{
		public ControlDischargeNotice DischargeNotice { get; }
		public UtilityType Service { get; }

		public SaveControlDischargeNotice(ControlDischargeNotice dischargeNotice, UtilityType service)
		{
			this.DischargeNotice = dischargeNotice;
			this.Service = service;
		}
	}
}
