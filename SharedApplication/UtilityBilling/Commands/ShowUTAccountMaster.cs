﻿using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Commands
{
    public class ShowUTAccountMaster : Command
    {
        public int Account { get; }
        public int Id { get; }
        public ShowUTAccountMaster(int account,int id)
        {
            Account = account;
            Id = id;
        }
    }
}