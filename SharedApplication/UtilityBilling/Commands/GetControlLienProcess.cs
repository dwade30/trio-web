﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.UtilityBilling.Commands
{
	public class GetControlLienProcess : Command<ControlLienProcess>
	{
		public UtilityType Service { get; set; }
	}
}
