﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.UtilityBilling.Commands
{
    public class GetCalculatedUtilityAccount : Command<CalculatedUtilityAccount>
    {
        public int Account { get; }
        public DateTime EffectiveDate { get; }

        public GetCalculatedUtilityAccount(int account, DateTime effectiveDate)
        {
            Account = account;
            EffectiveDate = effectiveDate;
        }
    }
}