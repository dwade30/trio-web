﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UBBillUtilityDetails
	{
		public UtilityType Service { get; set; }
		public DateTime? RateCreatedDate { get; set; }
		public DateTime? BillDate { get; set; }
		public DateTime? RateStart { get; set; }
		public DateTime? RateEnd { get; set; }
		public DateTime? IntPaidDate { get; set; }
		public string RateDescription { get; set; }
		public bool IsLien { get; set; }
		public bool IsLiened { get; set; }
		public int LienId { get; set; } = 0;
		public bool LienMainBill { get; set; } = false;
		public decimal PrincipalOwed { get; set; } = 0;
		public decimal TaxOwed { get; set; } = 0;
		public decimal IntAdded { get; set; } = 0;
		public decimal IntOwed { get; set; } = 0;
		public decimal CostAdded { get; set; } = 0;
		public decimal CostOwed { get; set; } = 0;
		public decimal PrincipalPaid { get; set; } = 0;
		public decimal TaxPaid { get; set; } = 0;
		public decimal IntPaid { get; set; } = 0;
		public decimal PreLienInterestPaid { get; set; } = 0;
		public decimal CostPaid { get; set; } = 0;
		public decimal PendingPrincipalAmount { get; set; }
		public decimal PendingTaxAmount { get; set; }
		public decimal PendingPreLienInterestAmount { get; set; }
		public decimal PendingCurrentInterestAmount { get; set; }
		public decimal PendingCostAmount { get; set; }
		public decimal PendingChargedInterest { get; set; }
		public double InterestRate { get; set; }
		public decimal CurrentInterest { get; set; }
		public decimal MaturityFee { get; set; }
		public decimal PerDiem { get; set; }
		public DateTime? LastInterestDate { get; set; }
		public DateTime? CurrentInterestDate { get; set; }
		public DateTime? InterestStartDate { get; set; }
		public DateTime? PreviousInterestDate { get; set; }
		public DateTime? SavedPreviousInterestDate { get; set; }
		public List<UBPayment> Payments { get; set; } = new List<UBPayment>();
		public bool BillOwner { get; set; }
		public int DemandGroupId { get; set; } = 0;
		public decimal PrincipalDue
		{
			get { return IsLiened ? 0 : PrincipalOwed - PrincipalPaid - PendingPrincipalAmount; }
		}

		public decimal TaxDue
		{
			get { return IsLiened ? 0 : TaxOwed - TaxPaid - PendingTaxAmount; }
		}

		public decimal CostDue
		{
			get { return IsLiened ? 0 : CostOwed - CostAdded - MaturityFee - CostPaid - PendingCostAmount; }
		}

		public decimal InterestDue
		{
			get { return IsLiened ? 0 : IntOwed - IntAdded - IntPaid - PreLienInterestPaid - PendingPreLienInterestAmount - PendingCurrentInterestAmount - CurrentInterest; }
		}

		public decimal CurrentInterestDue
		{
			get
			{
				if (IsLien)
				{
					return (IntAdded * -1) - IntPaid - PendingCurrentInterestAmount - CurrentInterest;
				}
				else
				{
					return IsLiened ? 0 : InterestDue;
				}
			}
		}

		public decimal PreLienInterestDue
		{
			get
			{
				if (IsLien)
				{
					return IntOwed - PreLienInterestPaid - PendingPreLienInterestAmount;
				}
				else
				{
					return 0;
				}
			}
			
		}

		public decimal TotalInterest
		{
			get { return IntOwed - IntAdded - CurrentInterest; }
		}

		public decimal TotalCost
		{
			get { return CostOwed - CostAdded; }
		}

		public decimal TotalPayment
		{
			get { return PrincipalPaid + IntPaid + PreLienInterestPaid + CostPaid + TaxPaid + PendingPrincipalAmount + PendingCostAmount + PendingPreLienInterestAmount + PendingCurrentInterestAmount + PendingTaxAmount; }
		}

		public decimal BalanceDue
		{
			get { return PrincipalDue + TaxDue + InterestDue + CostDue; }
		}

		public bool IsIn30DayNoticeStatus
		{
			get { return DemandGroupId > 0 && !IsLiened; }
		}

		public string ServiceCode
		{
			get
			{
				return Service == UtilityType.Sewer ? "S" : "W";
			}
		}


		public decimal AbatementTotal
		{
			get
			{
				return Payments.Where(x => x.Code == "A").Select(y => y.Principal).DefaultIfEmpty(0).Sum();
			}
		}

		public decimal PendingAbatementTotal
		{
			get
			{
				return Payments.Where(x => x.Code == "A" && x.Pending).Select(y => y.Principal).DefaultIfEmpty(0).Sum();
			}
		}
	}
}
