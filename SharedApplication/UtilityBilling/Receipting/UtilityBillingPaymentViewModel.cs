﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountGroups;
using SharedApplication.AccountGroups.Commands;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Models;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Notes;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingPaymentViewModel : IUtilityBillingPaymentViewModel
	{

		private Guid correlationID;
		private UtilityBillingTransaction transaction;
		private List<UBBill> bills = new List<UBBill>();
		private bool isPaymentScreen;

		private EventPublisher eventPublisher;
		private CommandDispatcher commandDispatcher;
		private IGlobalActiveModuleSettings activeModuleSettings;
		private IBillCalculationService billCalculationService;
		private GlobalUtilityBillingSettings globalUtilityBillingSettings;

		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>
			customerPartyQueryHandler;

		private IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler;
		private IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler;
		private IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler;

		private IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>
			moduleAssociationQueryHandler;

		private IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>> commentQueryHandler;
		private IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>> commentRecQueryHandler;
		private IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler;
		private IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>> reMasterQueryHandler;
		private IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>> tblAcctACHQueryHandler;
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
		private IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>> dischargeNeededQueryHandler;
		private DataEnvironmentSettings environmentSettings;
		private IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>> lastYearReceiptQueryHandler;
		private IAccountValidationService accountValidationService;
		private IBudgetaryAccountingService budgetaryAccountingService;
		private CRTransactionGroupRepository crTransactionRepository;

		private UBCustomerPaymentStatusResult customer;
		private GroupMaster groupMaster;
		private Comment comment;
		private CommentRec commentRec;
		private UBPayment chargedInterestToReverse;
		private DateTime reversalEffectiveDate;
		private DateTime effectiveDate;
		private Master master;
		private tblAcctACH accountACHInfo;
		private ModuleAssociation moduleAssociation;

		public UtilityBillingPaymentViewModel(EventPublisher eventPublisher,
			GlobalUtilityBillingSettings globalUtilityBillingSettings, CommandDispatcher commandDispatcher,
			IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>
				customerPartyQueryHandler, IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler,
			IGlobalActiveModuleSettings activeModuleSettings,
			IQueryHandler<GroupMasterSearchCriteria, IEnumerable<GroupMaster>> groupMasterQueryHandler,
			IBillCalculationService billCalculationService,
			IQueryHandler<CommentSearchCriteria, IEnumerable<Comment>> commentQueryHandler,
			IAccountValidationService accountValidationService, IBudgetaryAccountingService budgetaryAccountingService,
			IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler,
			IQueryHandler<LastYearReceiptSearchCriteria, IEnumerable<LastYearReceipt>> lastYearReceiptQueryHandler,
			IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler,
			DataEnvironmentSettings environmentSettings,
			IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>> reMasterQueryHandler,
			IQueryHandler<tblAcctACHSearchCriteria, IEnumerable<tblAcctACH>> tblAcctACHQueryHandler,
			IQueryHandler<DischargeNeededSearchCriteria, IEnumerable<DischargeNeeded>> dischargeNeededQueryHandler,
			IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler,
			IQueryHandler<CommentSearchCriteria, IEnumerable<CommentRec>> commentRecQueryHandler,
			IQueryHandler<ModuleAssociationSearchCriteria, IEnumerable<ModuleAssociation>>
				moduleAssociationQueryHandler, CRTransactionGroupRepository crTransactionRepository)
		{
			this.eventPublisher = eventPublisher;
			this.globalUtilityBillingSettings = globalUtilityBillingSettings;
			this.commandDispatcher = commandDispatcher;
			this.customerPartyQueryHandler = customerPartyQueryHandler;
			this.billQueryHandler = billQueryHandler;
			this.activeModuleSettings = activeModuleSettings;
			this.groupMasterQueryHandler = groupMasterQueryHandler;
			this.billCalculationService = billCalculationService;
			this.commentQueryHandler = commentQueryHandler;
			this.accountValidationService = accountValidationService;
			this.budgetaryAccountingService = budgetaryAccountingService;
			this.receiptQueryHandler = receiptQueryHandler;
			this.lastYearReceiptQueryHandler = lastYearReceiptQueryHandler;
			this.masterQueryHandler = masterQueryHandler;
			this.environmentSettings = environmentSettings;
			this.reMasterQueryHandler = reMasterQueryHandler;
			this.tblAcctACHQueryHandler = tblAcctACHQueryHandler;
			this.dischargeNeededQueryHandler = dischargeNeededQueryHandler;
			this.receiptTypeQueryHandler = receiptTypeQueryHandler;
			this.commentRecQueryHandler = commentRecQueryHandler;
			this.moduleAssociationQueryHandler = moduleAssociationQueryHandler;
			this.crTransactionRepository = crTransactionRepository;

			EffectiveDate = DateTime.Now;

		}

		private void GetNote()
        {
            comment = commandDispatcher.Send(new GetUtilityNote(customer.AccountNumber)).Result;

			if (comment == null)
			{
				comment = new Comment
				{
					Text = "",
					Priority = 0,
					Account = customer.AccountNumber
				};
			}
        }

		private void GetCommentRec()
		{
			commentRec = this.commentRecQueryHandler.ExecuteQuery(new CommentSearchCriteria
			{
				AccountId = customer.Id
			}).FirstOrDefault();
		}

        

		private void GetModuleAssociationInfo()
		{
			moduleAssociation = this.moduleAssociationQueryHandler.ExecuteQuery(new ModuleAssociationSearchCriteria
			{
				Module = "UT",
				REMasterAccount = 0,
				AccountNumber = customer.AccountNumber
			}).FirstOrDefault();
		}

		public ModuleAssociation ModuleAssociationInfo
		{
			get => moduleAssociation;
			set => moduleAssociation = value;
		}

		public void UpdateCommentRec()
		{
			GetCommentRec();
		}

		public bool PayWaterFirst
		{
			get => globalUtilityBillingSettings.PayWaterFirst;
		}

		public string GetAbatementAccount(UtilityType service, int billNumber)
		{
			try
			{
				var bill = bills.Where(x => x.BillNumber == billNumber).FirstOrDefault();
				int receiptType;

				if (bill != null)
				{
					if (bill.IsLien)
					{
						if (service == UtilityType.Sewer)
						{
							receiptType = 95;
						}
						else
						{
							receiptType = 96;
						}
					}
					else
					{
						if (service == UtilityType.Water)
						{
							receiptType = 93;
						}
						else
						{
							receiptType = 94;
						}
					}
				}
				else
				{
					return "M ABATEMENT";
				}

				if (IsCashReceiptsActive() && BudgetaryExists)
				{
					var type = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
					{
						ReceiptType = receiptType
					}).FirstOrDefault();

					if (type == null)
					{
						return "M ABATEMENT";
					}
					else
					{
						return type.Account6;
					}
				}
				else
				{
					return "M ABATEMENT";
				}
			}
			catch (Exception ex)
			{
				return "M ABATEMENT";
			}
		}

		public bool LienDischargeExists()
		{
			return dischargeNeededQueryHandler.ExecuteQuery(new DischargeNeededSearchCriteria
			{
				Ids = bills.Where(x => x.IsLien).Select(y => y.Id).ToList()
			}) != null;
		}

		public DischargeNeeded LienDischarge(int lienId)
		{
			return dischargeNeededQueryHandler.ExecuteQuery(new DischargeNeededSearchCriteria
			{
				Ids = new List<int>
				{
					lienId
				}
			}).FirstOrDefault();
		}

		private void GetACHInfo()
		{
			accountACHInfo = this.tblAcctACHQueryHandler.ExecuteQuery(new tblAcctACHSearchCriteria
			{
				AccountId = customer.Id
			}).FirstOrDefault();
		}

		public tblAcctACH AccountACHInfo
		{
			get => accountACHInfo;
		}

		public bool IsTaxAcquired()
		{

			if (master.TaxAcquired ?? false)
			{
				return true;
			}

			if (activeModuleSettings.RealEstateIsActive && globalUtilityBillingSettings.CheckRETaxAcquired &&
			    master.REAccount != 0)
			{
				var result = reMasterQueryHandler.ExecuteQuery(new REMasterSearchCriteria
				{
					AccountNumber = master.REAccount ?? 0,
					deletedOption = REMasterSearchCriteria.DeletedSelection.NotDeleted
				}).FirstOrDefault();

				if (result != null)
				{
					return result.TaxAcquired ?? false;
				}
			}

			return false;
		}

		public void RemoveAllPendingPayments()
		{
			bills.RemoveAll(x => x.Pending);

			foreach (var bill in bills)
			{
				foreach (var utility in bill.UtilityDetails)
				{
					foreach (var payment in utility.Payments.Where(x => x.Pending).ToList())
					{
						RemovePendingPayment(payment);
					}
				}
			}
		}

		public bool ResetTransactionDate { get; set; }

		public bool DisableAutoPaymentInsert
		{
			get => globalUtilityBillingSettings.DisableAutoPaymentInsert;
		}

		public void GoToSearchScreen()
		{
			commandDispatcher.Send(new MakeUtilityBillingTransaction
			{
				StartedFromCashReceipts = StartedFromCashReceipts,
				ShowPaymentScreen = isPaymentScreen,
				CorrelationId = correlationID,
				AccountNumber = 0,
				Id = transaction.Id,
				AllowBatch = AllowBatch
			});
		}

		public void CompleteTransaction()
		{
			if (StartedFromCashReceipts)
			{
				var crTran = crTransactionRepository.Get(correlationID);

				if (crTran != null)
				{
					var transactions = crTran.UtilityBillingTransactions(customer.AccountNumber).ToList();
					if (transactions.Any())
					{
						transactions.ForEach(x => crTran.RemoveTransaction(x.Id));
					}
				}

				eventPublisher.Publish(new TransactionCompleted
				{
					CorrelationId = correlationID,
					CompletedTransactions = ConvertTransactionToUtilityBillingTransactionBase(transaction)
				});
			}
			else
			{
				commandDispatcher.Send(new FinalizeTransactions<UtilityBillingTransactionBase>
				{
					CorrelationIdentifier = correlationID,
					ReceiptId = 0,
					TellerId = "",
					Transactions = ConvertTransactionToUtilityBillingTransactionBase(transaction)
				});
			}
		}

		public bool AllowBatch { get; set; }

		private IEnumerable<UtilityBillingTransactionBase> ConvertTransactionToUtilityBillingTransactionBase(
			UtilityBillingTransaction transaction)
		{
			var result = new List<UtilityBillingTransactionBase>();
			foreach (var bill in bills.Where(x =>
				x.Pending || PendingPaymentsExistForSelectedBill(x.BillNumber.ToString())))
			{
				foreach (var utility in bill.UtilityDetails.Where(x => x.Payments.Any(y => y.Pending)))
				{
					var controls = new List<ControlItem>();

					controls.Add(new ControlItem
					{
						Name = "Control1",
						Description = "Period",
						Value = "1"
					});

					controls.Add(new ControlItem
					{
						Name = "Control2",
						Description = "Code",
						Value = ""
					});


					var payment = utility.Payments.Where(x => x.Pending).FirstOrDefault();
					
					var control = controls.FirstOrDefault(x => x.Name == "Control2");
					control.Value = payment.Code;

					var receiptType = StartedFromCashReceipts ? GetReceiptTypeInfoForPayment(bill, utility) : null;
					var cashReceiptsTransaction = new UtilityBillingTransactionBase
					{
						ActualDateTime = (DateTime) payment.ActualSystemDate,
						EffectiveDateTime = (DateTime) payment.EffectiveInterestDate,
						RecordedTransactionDate = (DateTime) payment.RecordedTransactionDate,
						AccountNumber = customer.AccountNumber,
						Comment = payment.Comments,
						Controls = controls,
						CorrelationIdentifier = transaction.CorrelationId,
						Id = Guid.NewGuid(),
						PayerName = customer.FullNameLF,
						PayerPartyId = customer.PartyId,
						AffectCashDrawer = payment.CashDrawer == "Y" ? true : false,
						IncludeInCashDrawerTotal = payment.CashDrawer == "Y" ? true : false,
						AffectCash = payment.GeneralLedger == "Y" ? true : false,
						Code = payment.Code,
						BudgetaryAccountNumber = payment.BudgetaryAccountNumber,
						DefaultCashAccount = receiptType?.DefaultAccount.Trim() ?? "",
						EPmtAccountID = receiptType?.EPmtAcctID ?? "",
						Reference = customer.AccountNumber + "-" + bill.BillNumber,
						TransactionTypeCode = receiptType?.TypeCode.ToString() ?? "" ,
						TransactionDetails = new UtilityBillingTransaction()
						{
							Id = Guid.NewGuid(),
							CorrelationId = transaction.CorrelationId,
							AccountNumber = customer.AccountNumber,
							Bills = new List<UBBill>
							{
								bill
							}
						},
						TransactionTypeDescription = receiptType?.TypeTitle ?? ""
					};

					cashReceiptsTransaction.TransactionDetails.Bills.FirstOrDefault().UtilityDetails =
					new List<UBBillUtilityDetails>
					{
						utility
					};

					cashReceiptsTransaction.AddTransactionAmounts(new List<TransactionAmountItem>
					{
						new TransactionAmountItem
						{
							Name = "Principal",
							Description = "Principal",
							Total = utility.PendingPrincipalAmount - utility.PendingAbatementTotal
						},
						new TransactionAmountItem
						{
							Name = "Interest",
							Description = "Interest",
							Total = utility.PendingCurrentInterestAmount
						},
						new TransactionAmountItem
						{
							Name = bill.IsLien ? "Lien Costs" : "Costs",
							Description = bill.IsLien ? "Lien Costs" : "Costs",
							Total = utility.PendingCostAmount
						},
						new TransactionAmountItem
						{
							Name = "Tax",
							Description = "Tax",
							Total = utility.PendingTaxAmount
						},
						new TransactionAmountItem
						{
							Name = "Abatement",
							Description = "Abatement",
							Total = utility.PendingAbatementTotal
						}
					});

					if (bill.IsLien)
					{
						cashReceiptsTransaction.AddTransactionAmount(
							new TransactionAmountItem
							{
								Name = "Pre Lien Interest",
								Description = "Pre Lien Interest",
								Total = utility.PendingPreLienInterestAmount
							}
						);
					}
					
					result.Add(cashReceiptsTransaction);
				}
			}

			return result;
		}

		private ReceiptType GetReceiptTypeInfoForPayment(UBBill bill, UBBillUtilityDetails utility)
		{
			int typeCode;

			if (IsTaxAcquired())
			{
				if (bill.IsLien)
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 896;
					}
					else
					{
						typeCode = 895;
					}
				}
				else
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 893;
					}
					else
					{
						typeCode = 894;
					}
				}
			}
			else
			{
				if (bill.IsLien)
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 96;
					}
					else
					{
						typeCode = 95;
					}
				}
				else
				{
					if (utility.Service == UtilityType.Water)
					{
						typeCode = 93;
					}
					else
					{
						typeCode = 94;
					}
				}
			}

			var type = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
			{
				ReceiptType = typeCode,
			}).FirstOrDefault();

			return type;
		}

        public AccountGroup GetGroupSummary(DateTime effectiveDate)
        {
            if (GroupInfo == null || GroupInfo.GroupNumber.GetValueOrDefault() == 0)
            {
                return new AccountGroup(0,new List<GroupAccountSummary>());
            }

            return commandDispatcher.Send(new CalculateAccountGroup(GroupInfo.GroupNumber.GetValueOrDefault(),effectiveDate)).Result;
        }

        public event EventHandler AccountChanged;
		public event EventHandler<UBPayment> PendingPaymentAdded;

		public void Cancel()
		{
			eventPublisher.Publish(new TransactionCancelled(correlationID, null));
		}

		public string ClientName
		{
			get => environmentSettings.ClientName;
		}

		public bool DontFireCollapse { get; set; }
		public bool NegativeBillsExist(UtilityType service)
		{ 
			return Bills(service).SelectMany(x => x.UtilityDetails).Any(y => y.BalanceDue < 0);
		}

		public void AdjustNegativeBillsToAddPayment(UtilityType service)
		{
			decimal totalAdjustment = 0;

			var utilityList = bills.Where(z => z.BillNumber != 0).SelectMany(x => x.UtilityDetails).Where(y =>
				y.BalanceDue > 0 && (service == UtilityType.All || y.Service == service));

			foreach (var bill in bills.Where(z => z.BillNumber != 0))
			{
				foreach (var utility in bill.UtilityDetails.Where(y =>
					y.BalanceDue < 0 && (service == UtilityType.All || y.Service == service)))
				{
					var billAmounts = CalculatePaymentAmountsForUtility(utility, 0, true);

					AddPendingPayment(bill.BillNumber.ToString(), utility.Service, "P", "AUTO", "Y", "Y", "", "", null,
						billAmounts.Principal, billAmounts.Tax, billAmounts.Interest, billAmounts.Cost, true);
					totalAdjustment += billAmounts.RemainingAmount;
				}
			}


			if (totalAdjustment > 0)
			{
				AddPendingPayment("Auto", service, "P", "AUTO", "Y", "Y", "", "", null, totalAdjustment, 0, 0, 0);
			}
		}

		public bool ChargedInterestExistsOnUtility(int id, UtilityType service)
		{
			return bills.FirstOrDefault(x => x.Id == id).UtilityDetails.FirstOrDefault(z => z.Service == service)
				.Payments.Any(y =>
					y.ChargedInterestRecord?.Reference == "CHGINT" || y.ChargedInterestRecord?.Reference == "EARNINT");
		}

		public bool BudgetaryExists
		{
			get => activeModuleSettings.BudgetaryIsActive;
		}

		public bool ValidManualAccount(string account, UtilityType service)
		{
			if (activeModuleSettings.BudgetaryIsActive)
			{
				if (!accountValidationService.AccountValidate(account))
				{
					return false;
				}

				if (activeModuleSettings.CashReceiptingIsActive && account.Left(1) != "M")
				{
					var receiptType = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
					{
						ReceiptType = service == UtilityType.Water ? 93 : 94
					}).FirstOrDefault();

					if (receiptType != null)
					{
						var tempFund = budgetaryAccountingService.GetFundFromAccount(account);
						if (tempFund == 0 || budgetaryAccountingService.GetFundFromAccount(account) !=
						    budgetaryAccountingService.GetFundFromAccount(receiptType.Account1))
						{
							return false;
						}
					}
				}
				
				return true;
			}
			else
			{
				return account.Left(1) == "M" && account.Length > 2;
			}
		}

		public string GetPaymentReceiptNumber(int receiptKey, DateTime? receiptDateTime)
		{
			if (receiptKey > 0)
			{
				if (activeModuleSettings.CashReceiptingIsActive)
				{
					var receipt = receiptQueryHandler.ExecuteQuery(new ReceiptSearchCriteria
					{
						ReceiptId = receiptKey,
						ReceiptDate = receiptDateTime
					}).FirstOrDefault();

					if (receipt != null)
					{
						return receipt.ReceiptNumber.ToString();
					}
					else
					{
						var lastReceipt = lastYearReceiptQueryHandler.ExecuteQuery(new LastYearReceiptSearchCriteria
						{
							ReceiptId = receiptKey,
							ReceiptDate = receiptDateTime
						}).FirstOrDefault();

						if (lastReceipt != null)
						{
							return lastReceipt.ReceiptNumber.ToString();
						}
						else
						{
							return receiptKey.ToString();
						}
					}
				}
				else
				{
					return receiptKey.ToString();
				}
			}
			else
			{
				return "0";
			}
		}

		public bool PaymentGreaterThanOwed(string billNumber, UtilityType service, decimal paymentTotal)
		{
			if (billNumber != "Auto")
			{
				var bill = GetBill(billNumber);

				if (bill != null)
				{
					return paymentTotal > bill.UtilityDetails
						       .Where(y => y.Service == service || service == UtilityType.All).Select(x => x.BalanceDue)
						       .DefaultIfEmpty(0).Sum();
				}
				else
				{
					return true;
				}
			}
			else
			{
				return paymentTotal > bills.SelectMany(y => y.UtilityDetails)
					       .Where(y => y.Service == service || service == UtilityType.All).Select(x => x.BalanceDue)
					       .DefaultIfEmpty(0).Sum();
			}
		}

		private BillPaymentCalculationResult CalculatePaymentAmountsForUtility(UBBillUtilityDetails utility,
			decimal paymentAmount, bool negativeBill = false)
		{
			var result = new BillPaymentCalculationResult();

			result.RemainingAmount = paymentAmount;

			decimal costNeeded = utility.CostDue;
			if (costNeeded < 0 && !negativeBill)
			{
				costNeeded = 0;
			}

			if (costNeeded >= result.RemainingAmount)
			{
				result.Cost = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Cost = costNeeded;
				result.RemainingAmount = result.RemainingAmount - costNeeded;
			}

			decimal intNeeded = utility.PreLienInterestDue;
			if (intNeeded < 0 && !negativeBill)
			{
				intNeeded = 0;
			}

			if (intNeeded >= result.RemainingAmount)
			{
				result.PreLienInterest = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.PreLienInterest = intNeeded;
				result.RemainingAmount = result.RemainingAmount - intNeeded;
			}

			intNeeded = utility.CurrentInterestDue;
			if (intNeeded < 0 && !negativeBill)
			{
				intNeeded = 0;
			}

			if (intNeeded >= result.RemainingAmount)
			{
				result.Interest = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Interest = intNeeded;
				result.RemainingAmount = result.RemainingAmount - intNeeded;
			}

			decimal taxNeeded = utility.TaxDue;
			if (taxNeeded < 0 && !negativeBill)
			{
				taxNeeded = 0;
			}

			if (taxNeeded >= result.RemainingAmount)
			{
				result.Tax = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Tax = taxNeeded;
				result.RemainingAmount = result.RemainingAmount - taxNeeded;
			}

			decimal principalNeeded = utility.PrincipalDue;
			if (principalNeeded < 0 && !negativeBill)
			{
				principalNeeded = 0;
			}

			if (principalNeeded >= result.RemainingAmount)
			{
				result.Principal = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Principal = principalNeeded;
				result.RemainingAmount = result.RemainingAmount - principalNeeded;
			}

			return result;
		}

		private UBBill GetPrepaymentBill(UtilityType service)
		{
			try
			{
				var bill = bills.FirstOrDefault(x => x.BillNumber == 0);

				if (bill == null)
				{
					bill = new UBBill
					{
						Pending = true,
						BillStatus = "D",
						BillNumber = 0,
						CurrentReading = 0,
						PreviousReading = 0,
						FinalBill = false,
						BilledAddress1 = customer.BilledAddress1,
						BilledAddress2 = customer.BilledAddress2,
						BilledAddress3 = customer.BilledAddress3,
						BilledCity = customer.BilledCity,
						BilledState = customer.BilledState,
						BilledZip = customer.BilledZip,
						BilledZip4 = "",
						OwnerAddress1 = customer.OwnerAddress1,
						OwnerAddress2 = customer.OwnerAddress2,
						OwnerAddress3 = customer.OwnerAddress3,
						OwnerCity = customer.OwnerCity,
						OwnerState = customer.OwnerState,
						OwnerZip = customer.OwnerZip,
						OwnerZip4 = "",
						MeterId = master.MeterTables.FirstOrDefault(x => x.MeterNumber == 1)?.ID ?? 0,
						Book = master.UTBook ?? 0,
						UtilityDetails = new List<UBBillUtilityDetails>()
					};

					var utility = new UBBillUtilityDetails
					{
						Service = service,
						LienId = 0,
						LienMainBill = false,
						BillOwner = service == UtilityType.Water
							? (master.WBillToOwner ?? false)
							: (master.SBillToOwner ?? false),
						CostAdded = 0,
						CostOwed = 0,
						CostPaid = 0,
						CurrentInterest = 0,
						DemandGroupId = 0,
						IntAdded = 0,
						IntOwed = 0,
						IntPaid = 0,
						InterestRate = 0,
						IsLien = false,
						IsLiened = false,
						MaturityFee = 0,
						PendingChargedInterest = 0,
						PendingCostAmount = 0,
						PendingCurrentInterestAmount = 0,
						PendingPreLienInterestAmount = 0,
						PendingPrincipalAmount = 0,
						PendingTaxAmount = 0,
						PerDiem = 0,
						PreLienInterestPaid = 0,
						PrincipalOwed = 0,
						PrincipalPaid = 0,
						RateDescription = "",
						TaxOwed = 0,
						TaxPaid = 0
					};

					bill.UtilityDetails.Add(utility);
					bills.Add(bill);
				}

				return bill;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return null;
			}
		}

		private UBPayment AddPendingChargedInterest(UBBillUtilityDetails utility, UBPayment linkedPayment)
		{
			var chargedInterest = new UBPayment();


			chargedInterest.Pending = true;
			chargedInterest.PendingId = Guid.NewGuid();
			chargedInterest.ActualSystemDate = linkedPayment.ActualSystemDate;
			chargedInterest.RecordedTransactionDate = linkedPayment.RecordedTransactionDate;
			chargedInterest.EffectiveInterestDate = linkedPayment.EffectiveInterestDate;
			chargedInterest.Code = "I";
			chargedInterest.Principal = 0;
			chargedInterest.Tax = 0;
			chargedInterest.Cost = 0;
			if (chargedInterestToReverse != null)
			{
				chargedInterest.CurrentInterest = chargedInterestToReverse.CurrentInterest * -1;
				chargedInterest.PreLienInterest = 0;
				chargedInterest.Reference = chargedInterest.CurrentInterest > 0 ? "Interest" : "CHGINT";
				chargedInterest.ChargedInterestDate = utility.PreviousInterestDate;
				var temp = utility.LastInterestDate;
				utility.LastInterestDate = utility.PreviousInterestDate;
				utility.PreviousInterestDate = temp;
			}
			else
			{
				chargedInterest.CurrentInterest = utility.CurrentInterest - utility.PendingChargedInterest;
				chargedInterest.PreLienInterest = 0;
				chargedInterest.Reference = chargedInterest.CurrentInterest > 0 ? "EARNINT" : "CHGINT";
				chargedInterest.ChargedInterestDate = utility.CurrentInterestDate;
				utility.PreviousInterestDate = utility.LastInterestDate;
				utility.LastInterestDate = utility.CurrentInterestDate;
			}

			chargedInterest.GeneralLedger = linkedPayment.GeneralLedger;
			chargedInterest.CashDrawer = linkedPayment.CashDrawer;
			chargedInterest.BudgetaryAccountNumber = linkedPayment.BudgetaryAccountNumber;

			utility.PendingChargedInterest = utility.PendingChargedInterest + chargedInterest.CurrentInterest;

			chargedInterestToReverse = null;

			return chargedInterest;
		}

		private void AddPendingRefundLine(UBBillUtilityDetails utility, UBPayment abatement)
		{
			try
			{
				var refund = new UBPayment
				{
					ActualSystemDate = abatement.ActualSystemDate,
					BillId = abatement.BillId,
					BudgetaryAccountNumber = StartedFromCashReceipts ? "" : "M NO ENTRY",
					Code = "R",
					CashDrawer = "N",
					GeneralLedger = "N",
					Comments = "",
					Cost = abatement.Cost * -1,
					CurrentInterest = abatement.CurrentInterest * -1,
					EffectiveInterestDate = abatement.EffectiveInterestDate,
					DailyCloseOut = 0,
					IsReversal = false,
					PaidBy = abatement.PaidBy,
					Pending = true,
					PreLienInterest = abatement.PreLienInterest * -1,
					Principal = abatement.Principal * -1,
					RecordedTransactionDate = abatement.RecordedTransactionDate,
					Reference = abatement.Reference,
					Tax = abatement.Tax * -1,
					Teller = abatement.Teller,
					TransactionId = abatement.TransactionId,
					PendingId = Guid.NewGuid(),
					ReceiptId = abatement.ReceiptId
				};

				AddPaymentToUtility(utility, refund);
			}
			catch
			{

			}
		}

		private void AddPendingOverPayment(decimal amount, string comment, string cashDrawer,
			string budgetaryAccountNumber, DateTime? transactionDate, bool isPrepayment, UtilityType service)
		{
			var payment = new UBPayment();
			var bill = new UBBill();
			var utility = new UBBillUtilityDetails();

			if (isPrepayment)
			{
				bill = GetPrepaymentBill(service);
				payment.Code = "Y";
			}
			else
			{
				payment.Code = "P";
				bill = bills.OrderByDescending(x => x.BillNumber).FirstOrDefault();
			}

			utility = GetUtilityDetails(bill, service);

			payment.ActualSystemDate = DateTime.Today;
			payment.Reference = "PREPAY-A";
			payment.EffectiveInterestDate = EffectiveDate;
			payment.GeneralLedger = "Y";
			payment.Principal = amount;
			payment.CashDrawer = cashDrawer;
			payment.Pending = true;
			payment.PendingId = Guid.NewGuid();
			if (transactionDate == null)
			{
				payment.RecordedTransactionDate = DateTime.Today;
			}
			else
			{
				payment.RecordedTransactionDate = transactionDate;
			}

			if (cashDrawer == "N")
			{
				payment.BudgetaryAccountNumber = budgetaryAccountNumber;
			}
			else
			{
				payment.BudgetaryAccountNumber = "";
			}

			payment.Comments = comment;
			AddPaymentToUtility(utility, payment);
		}


		public void AddPendingPayment(string billNumber, UtilityType service, string code, string reference,
			string cashDrawer, string generaslLedger,
			string budgetaryAccountNumber, string comment, DateTime? transactionDate, decimal principal, decimal tax,
			decimal interest, decimal cost, bool skipRecalc = false)
		{
			if (billNumber == "Auto")
			{
				decimal totalAmount = principal + tax + interest + cost;

				var utilityList = bills.SelectMany(x => x.UtilityDetails).Where(y => y.BalanceDue > 0);

				if (globalUtilityBillingSettings.AutoPayOldestBillFirst)
				{
					utilityList = utilityList.OrderBy(x =>
						globalUtilityBillingSettings.PayWaterFirst
							? x.Service == UtilityType.Sewer
							: x.Service == UtilityType.Water).ThenBy(y => y.BillDate);
				}
				else
				{
					utilityList = utilityList.OrderBy(x => x.BillDate).ThenBy(x =>
						globalUtilityBillingSettings.PayWaterFirst
							? x.Service == UtilityType.Sewer
							: x.Service == UtilityType.Water);
				}

				foreach (var utility in utilityList)
				{

					var billAmounts = CalculatePaymentAmountsForUtility(utility, totalAmount);
					var payment = new UBPayment();

					payment.PreLienInterest = billAmounts.PreLienInterest;
					payment.CurrentInterest = billAmounts.Interest;
					payment.Tax = billAmounts.Tax;
					payment.Principal = billAmounts.Principal;
					payment.Cost = billAmounts.Cost;
					payment.Pending = true;
					payment.PendingId = Guid.NewGuid();
					payment.ActualSystemDate = DateTime.Today;
					payment.CashDrawer = cashDrawer;
					payment.Code = code;
					payment.Comments = comment;
					payment.EffectiveInterestDate = EffectiveDate;
					payment.GeneralLedger = generaslLedger;
					if (transactionDate == null)
					{
						payment.RecordedTransactionDate = DateTime.Today;
					}
					else
					{
						payment.RecordedTransactionDate = transactionDate;
					}

					if (cashDrawer == "N")
					{
						payment.BudgetaryAccountNumber = budgetaryAccountNumber;
					}
					else
					{
						payment.BudgetaryAccountNumber = "";
					}

					payment.Reference = reference;
					if (utility.CurrentInterest - utility.PendingChargedInterest != 0)
					{
						payment.ChargedInterestRecord = AddPendingChargedInterest(utility, payment);
					}

					totalAmount = billAmounts.RemainingAmount;
					AddPaymentToUtility(utility, payment);

					if (totalAmount == 0)
					{
						break;
					}

				}

				if (totalAmount > 0)
				{
					AddPendingOverPayment(totalAmount, comment, cashDrawer, budgetaryAccountNumber, transactionDate,
						true,
						service == UtilityType.All
							? globalUtilityBillingSettings.PayWaterFirst ? UtilityType.Water : UtilityType.Sewer
							: service);
				}
			}
			else
			{
				UBBill bill;
				if ((code == "Y" || billNumber.Contains("*") || billNumber == "0") && reference != "REVERSE")
				{
					bill = GetPrepaymentBill(service);
				}
				else
				{
					bill = GetBill(billNumber);
				}

				decimal totalAmount = principal + tax + interest + cost;
				BillPaymentCalculationResult billAmounts = new BillPaymentCalculationResult();
				var utility = GetUtilityDetails(bill, service);

				if ((code == "P" || !(code == "C" && interest < 0)) &&
				    !(reference.ToUpper() == "REVERSE" || code == "C") && !skipRecalc)
				{
					billAmounts = CalculatePaymentAmountsForUtility(utility, totalAmount);
					if (billAmounts.RemainingAmount > 0)
					{
						billAmounts.Principal = billAmounts.Principal + billAmounts.RemainingAmount;
						billAmounts.RemainingAmount = 0;
					}
				}
				else
				{
					billAmounts.Principal = principal;
					billAmounts.Interest = interest;
					billAmounts.Tax = tax;
					billAmounts.Cost = cost;
					billAmounts.RemainingAmount = 0;
				}

				var payment = new UBPayment();

				payment.PreLienInterest = billAmounts.PreLienInterest;
				payment.CurrentInterest = billAmounts.Interest;
				payment.Tax = billAmounts.Tax;
				payment.Cost = billAmounts.Cost;
				payment.Principal = billAmounts.Principal;
				payment.Pending = true;
				payment.PendingId = Guid.NewGuid();
				payment.ActualSystemDate = DateTime.Today;
				payment.CashDrawer = cashDrawer;
				payment.Code = code;
				payment.Comments = comment;
				if (reference == "REVERSE")
				{
					if (utility.PreviousInterestDate != null)
					{
						payment.EffectiveInterestDate = utility.PreviousInterestDate;
					}
					else
					{
						payment.EffectiveInterestDate = reversalEffectiveDate;
					}
				}
				else
				{
					payment.EffectiveInterestDate = EffectiveDate;
				}

				payment.GeneralLedger = generaslLedger;
				if (transactionDate == null)
				{
					payment.RecordedTransactionDate = DateTime.Today;
				}
				else
				{
					payment.RecordedTransactionDate = transactionDate;
				}

				if (cashDrawer == "N")
				{
					payment.BudgetaryAccountNumber = budgetaryAccountNumber;
				}
				else
				{
					payment.BudgetaryAccountNumber = "";
				}

				payment.Reference = reference;
				if (reference == "REVERSE" && ChargedInterestToReverse != null)
				{
					payment.ChargedInterestRecord = AddPendingChargedInterest(utility, payment);
				}
				else
				{
					if (utility.CurrentInterest - utility.PendingChargedInterest != 0)
					{
						payment.ChargedInterestRecord = AddPendingChargedInterest(utility, payment);
					}
				}

				AddPaymentToUtility(utility, payment);
				if (code == "R")
				{
					AddPendingRefundLine(utility, payment);
				}
			}
		}

		private void AddPaymentToUtility(UBBillUtilityDetails utility, UBPayment payment)
		{
			utility.PendingCurrentInterestAmount = utility.PendingCurrentInterestAmount + payment.CurrentInterest;
			utility.PendingPreLienInterestAmount = utility.PendingPreLienInterestAmount + payment.PreLienInterest;
			utility.PendingTaxAmount = utility.PendingTaxAmount + payment.Tax;
			utility.PendingPrincipalAmount = utility.PendingPrincipalAmount + payment.Principal;
			utility.PendingCostAmount = utility.PendingCostAmount + payment.Cost;

			payment.PaidBy = customer.FullNameLF;

			utility.Payments.Add(payment);
			PendingPaymentAdded?.Invoke(this, payment);
		}

		public bool PendingPaymentsExistForSelectedBill(string billNumber)
		{

			return GetBill(billNumber)?.BillPayments(UtilityType.All).Any(x => x.Pending) ?? false;
		}

		public bool PendingPaymentsExist()
		{
			return bills.Any(x => x.BillPayments(UtilityType.All).Any(y => y.Pending));
		}

		public decimal TotalPendingPaymentsAmount(UtilityType service)
		{
			return bills.SelectMany(y => y.UtilityDetails)
				.Where(z => service == UtilityType.All || z.Service == service).Select(x =>
					x.PendingCurrentInterestAmount + x.PendingPreLienInterestAmount + x.PendingTaxAmount +
					x.PendingPrincipalAmount + x.PendingCostAmount).Sum();
		}

		public void SetCorrelationId(Guid correlationID)
		{
			this.correlationID = correlationID;
		}

		public bool PrepaymentExists(UtilityType service)
		{
			return bills.Any(x => (service == UtilityType.All || x.ContainsService(service)) && x.BillNumber == 0);
		}

		public UtilityType VisibleService { get; set; }

		public UtilityBillingTransaction Transaction
		{
			get => transaction;
			set
			{
				transaction = value;

				customer = this.customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
				{
					AccountNumber = transaction.AccountNumber
				}).FirstOrDefault();

				master = this.masterQueryHandler.ExecuteQuery(new MasterSearchCriteria
				{
					AccountNumber = customer.AccountNumber
				}).FirstOrDefault();

				GetNote();
				GetCommentRec();
				GetACHInfo();
				GetModuleAssociationInfo();

				groupMaster = this.groupMasterQueryHandler.ExecuteQuery(new GroupMasterSearchCriteria
				{
					AssociatedWith = new ModuleAssociationSearchCriteria
					{
						AccountNumber = customer?.AccountNumber ?? 0,
						Module = "UT"
					}
				}).FirstOrDefault();

				GetBillInfo();

				if (customer != null)
				{
					if (AccountChanged != null)
					{
						AccountChanged(this, new EventArgs());
					}
				}
			}
		}

		private void GetBillInfo()
		{
			bills = billQueryHandler.ExecuteQuery(new UBBillSearchCriteria
			{
				AccountId = customer.Id,
				VoidedSelection = VoidedStatusSelection.NotVoided
			}).ToList();

			CalculateCurrentInterest();

			if (transaction.Bills != null)
			{
				foreach (var existingBill in transaction.Bills)
				{
					if (existingBill.Pending)
					{
						bills.Add(existingBill);
					}
					else
					{
						foreach (var existingUtility in existingBill.UtilityDetails)
						{
							var utility = GetUtilityDetails(existingBill.BillNumber, existingUtility.Service);
							if (utility != null)
							{
								foreach (var payment in existingUtility.Payments.Where(x => x.Pending))
								{
									AddPaymentToUtility(utility, payment);
								}
							}
						}
					}
				}
			}
		}

		private void CalculateCurrentInterest()
		{
			foreach (var bill in bills.Where(x => x.BillNumber != 0))
			{
				CalculateBill(bill);
			}
		}

		public bool ShowAllBills { get; set; }

		public List<UBBill> Bills(UtilityType type)
		{
			List<UBBill> result = new List<UBBill>();

			if (bills != null)
			{
				if (type == UtilityType.All)
				{
					result = bills;
				}
				else
				{
					result = bills.Where(x => x.UtilityDetails.Any(y => y.Service == type)).ToList();
				}
			}

			return result.OrderByDescending(x => x.BillNumber == 0).ThenByDescending(y => y.BillDate)
				.ThenByDescending(z => z.BillNumber).ThenByDescending(a => a.Id).ToList();
		}

		public UBCustomerPaymentStatusResult Customer
		{
			get => customer;
		}

		public GroupMaster GroupInfo
		{
			get => groupMaster;
		}

		public decimal TotalPerDiem { get; }


        public void EditNote()
        {
            commandDispatcher.Send(new EditUtilityNote(customer.AccountNumber));
			GetNote();
        }

		

		public bool PopUpComment()
		{
			return (comment?.Priority ?? 0) > 0;
		}

		public bool StartedFromCashReceipts { get; set; }

		public bool IsCashReceiptsActive()
		{
			return activeModuleSettings.CashReceiptingIsActive;
		}

		public List<UBPayment> UtilityPayments(UBBillUtilityDetails utility)
		{
			return utility.Payments.OrderBy(d => d.Pending).ThenBy(a => a.RecordedTransactionDate)
                    .ThenBy(d => d.Id).ToList();
                //.ThenBy(b => b.ActualSystemDate).ThenBy(c => c.ReceiptId).ThenBy(d => d.Id).ToList();
		}

		public void CalculateBill(UBBill bill)
		{
			try
			{
				foreach (var utility in bill.UtilityDetails.Where(x => !x.IsLiened))
				{
					var result = billCalculationService.CalculateUtilityTotals(utility, EffectiveDate);

					//if (result.InterestCalculated)
					//{
						utility.LastInterestDate = result.LastInterestDate;
						utility.CurrentInterest = result.CalculatedInterestResult.Interest * -1;
						utility.CurrentInterestDate = result.CalculatedInterestDate;
						utility.PerDiem = (decimal) result.CalculatedInterestResult.PerDiem;
					//}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}

		public UBBill GetBill(int id)
		{
			return bills.Where(x => x.Id == id).FirstOrDefault();
		}

		public UBBill GetBill(string billNumber)
		{
			return bills.Where(x => x.BillNumber == billNumber.Replace("*", "").ToIntegerValue()).FirstOrDefault();
		}

		public UBBill GetBill(UBPayment payment)
		{
			return bills.FirstOrDefault(x => x.UtilityDetails.Any(y => y.Payments.Contains(payment)));
		}

		public UBBillUtilityDetails GetUtilityDetails(UBPayment payment)
		{
			return bills.SelectMany(x => x.UtilityDetails).FirstOrDefault(y => y.Payments.Contains(payment));
		}

		public UBBillUtilityDetails GetUtilityDetails(int billNumber, UtilityType service)
		{
			return bills.FirstOrDefault(x => x.BillNumber == billNumber).UtilityDetails
				.FirstOrDefault(y => y.Service == service);
		}

		public UBBillUtilityDetails GetUtilityDetails(UBBill bill, UtilityType service)
		{
			return bill.UtilityDetails.FirstOrDefault(y => y.Service == service);
		}

		public UBPayment GetPayment(int id)
		{
			return bills.SelectMany(x => x.UtilityDetails.SelectMany(y => y.Payments)).Where(z => z.Id == id)
				.FirstOrDefault();
		}

		public bool LatestPaymentOnUtility(UBPayment payment, UtilityType service)
		{
			var utility = GetUtilityDetails(payment);

			return !utility.Payments.Any(y => y.EffectiveInterestDate > payment.EffectiveInterestDate);
		}

		public void RemovePendingPayment(UBPayment payment)
		{
			
			var utility = GetUtilityDetails(payment);
			var bill = GetBill(payment);

			if (payment.ChargedInterestRecord != null)
			{
				utility.LastInterestDate = utility.PreviousInterestDate;
				utility.PreviousInterestDate = utility.SavedPreviousInterestDate;
				utility.PendingChargedInterest =
					utility.PendingChargedInterest - payment.ChargedInterestRecord.CurrentInterest;
			}

			utility.PendingCurrentInterestAmount = utility.PendingCurrentInterestAmount - payment.CurrentInterest;
			utility.PendingPreLienInterestAmount = utility.PendingPreLienInterestAmount - payment.PreLienInterest;
			utility.PendingTaxAmount = utility.PendingTaxAmount - payment.Tax;
			utility.PendingPrincipalAmount = utility.PendingPrincipalAmount - payment.Principal;
			utility.Payments.Remove(payment);

			if (bill.Pending)
			{
				if (!bill.UtilityDetails.Any(x => x.Payments.Any()))
				{
					bills.Remove(bill);
				}
			}
		}

		public UBPayment GetPendingPayment(Guid pendingId)
		{
			return bills.SelectMany(x => x.UtilityDetails.SelectMany(y => y.Payments))
				.Where(z => z.PendingId == pendingId && z.Pending).FirstOrDefault();
		}

		public List<UBPayment> GetPendingPayments(string billNumber)
		{
			return bills.Where(a => a.BillNumber == billNumber.ToIntegerValue()).SelectMany(x => x.UtilityDetails.SelectMany(y => y.Payments))
				.Where(z => z.Pending).ToList();
		}

		public bool IsPaymentScreen
		{
			get => isPaymentScreen;
			set => isPaymentScreen = value;
		}

		public DateTime EffectiveDate
		{
			get => effectiveDate;
			set
			{
				effectiveDate = value;
				CalculateCurrentInterest();
			}
		}

		public DateTime ReversalEffectiveDate
		{
			get => reversalEffectiveDate;
			set => reversalEffectiveDate = value;
		}

		public Comment AccountNote
		{
			get => comment;
			set => comment = value;
		}

		public CommentRec AccountCommentRec
		{
			get => commentRec;
		}

		public Master Master
		{
			get => master;
		}

		public UtilityType AccountServices()
		{
			bool hasWater = master.MeterTables.Any(x => x.Service == "W" || x.Service == "B");
			bool hasSewer = master.MeterTables.Any(x => x.Service == "S" || x.Service == "B");

			if (hasWater && hasSewer)
			{
				return UtilityType.All;
			}
			else if (hasWater)
			{
				return UtilityType.Water;
			}
			else if (hasSewer)
			{
				return UtilityType.Sewer;
			}
			else
			{
				return UtilityType.None;
			}
		}

		public bool HasWaterBill()
		{
			return bills.Any(x => x.ContainsService(UtilityType.Water));
		}

		public bool HasSewerBill()
		{
			return bills.Any(x => x.ContainsService(UtilityType.Sewer));
		}

		public UBPayment ChargedInterestToReverse
		{
			get => chargedInterestToReverse;
			set => chargedInterestToReverse = value;
		}

		public UtilityType TownServices
		{
			get => globalUtilityBillingSettings.TownService == "B" ? UtilityType.All :
				globalUtilityBillingSettings.TownService == "W" ? UtilityType.Water : UtilityType.Sewer;
		}

		

		
	}
}
