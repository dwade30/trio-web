﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UBLien
	{
		public int Id { get; set; }
		public int RateKey { get; set; }
		public DateTime? BillDate { get; set; }
		public decimal Principal { get; set; } = 0;
		public decimal Tax { get; set; } = 0;
		public decimal Interest { get; set; } = 0;
		public decimal Cost { get; set; } = 0;
		public decimal PrincipalPaid { get; set; } = 0;
		public decimal TaxPaid { get; set; } = 0;
		public decimal IntPaid { get; set; } = 0;
		public decimal CostPaid { get; set; } = 0;
		public decimal PendingPrincipalAmount { get; set; }
		public decimal PendingTaxAmount { get; set; }
		public decimal PendingInterestAmount { get; set; }
		public decimal PendingCostAmount { get; set; }
		public decimal PendingChargedInterest { get; set; }
		public double InterestRate { get; set; }
		public decimal FlatRate { get; set; }
		public string InterestMethod { get; set; }
		public decimal CurrentInterest { get; set; }
		public decimal PerDiem { get; set; }
		public DateTime? LastInterestDate { get; set; }
		public DateTime? CurrentInterestDate { get; set; }
		public DateTime? InterestStartDate { get; set; }
		public DateTime? PreviousInterestDate { get; set; }
		public DateTime? SavedPreviousInterestDate { get; set; }
		public List<UBPayment> Payments { get; set; } = new List<UBPayment>();

		public decimal TotalAmount
		{
			get { return Principal + Tax + Cost + Interest; }
		}
	}
}
