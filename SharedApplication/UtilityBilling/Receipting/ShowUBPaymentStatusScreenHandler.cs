﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts;
using SharedApplication.Messaging;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class ShowUBPaymentStatusScreenHandler : CommandHandler<ShowUBPaymentStatusScreen>
	{
		private IView<IUtilityBillingPaymentViewModel> paymentView;
		private CRTransactionGroupRepository crTransactionRepository;

		public ShowUBPaymentStatusScreenHandler(IView<IUtilityBillingPaymentViewModel> paymentView, CRTransactionGroupRepository crTransactionRepository)
		{
			this.paymentView = paymentView;
			this.crTransactionRepository = crTransactionRepository;
		}

		protected override void Handle(ShowUBPaymentStatusScreen command)
		{
			UtilityBillingTransaction transaction = new UtilityBillingTransaction();
			transaction.CorrelationId = command.CorrelationId;
			transaction.AccountNumber = command.AccountNumber;

			var crTran = crTransactionRepository.Get(command.CorrelationId);

			if (crTran != null)
			{
				var transactions = crTran.UtilityBillingTransactions(command.AccountNumber).ToList();
				if (transactions.Any())
				{
					List<UBBill> existingBills = new List<UBBill>();

					foreach (var trans in transactions)
					{
						existingBills.AddRange(trans.TransactionDetails.Bills);
					}
					transaction.Bills = existingBills;
				}
			}

			paymentView.ViewModel.SetCorrelationId(command.CorrelationId);
			paymentView.ViewModel.IsPaymentScreen = command.IsPaymentScreen;
			paymentView.ViewModel.StartedFromCashReceipts = command.StartedFromCashReceipts;
			paymentView.ViewModel.Transaction = transaction;
			paymentView.Show();

		}
	}

}
