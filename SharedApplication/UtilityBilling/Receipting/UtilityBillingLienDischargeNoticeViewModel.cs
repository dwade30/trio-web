﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingLienDischargeNoticeViewModel : IUtilityBillingLienDischargeNoticeViewModel
	{
        private CommandDispatcher commandDispatcher;
        private DataEnvironmentSettings envSettings;
        private IRegionalTownService regionalTownService;
        private IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler;
        private IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler;

        public UtilityBillingLienDischargeNoticeViewModel(CommandDispatcher commandDispatcher, DataEnvironmentSettings envSettings, IRegionalTownService regionalTownService, IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler, IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler)
        {
            this.commandDispatcher = commandDispatcher;
            this.envSettings = envSettings;
            this.regionalTownService = regionalTownService;
            this.billQueryHandler = billQueryHandler;
            this.masterQueryHandler = masterQueryHandler;
        }
        public ControlDischargeNotice GetControlDischargeNotice()
        {
            return commandDispatcher.Send(new GetControlDischargeNotice
            {
                Service = (LienToDischarge.Water ?? false) ? UtilityType.Water : UtilityType.Sewer
            }).Result ?? new ControlDischargeNotice();
        }

        public ControlLienProcess GetControlLienProcess()
        {
            return commandDispatcher.Send(new GetControlLienProcess
            {
                Service = (LienToDischarge.Water ?? false) ? UtilityType.Water : UtilityType.Sewer
            }).Result;
        }

        public UBBill GetLienBill()
        {
	        return billQueryHandler.ExecuteQuery(new UBBillSearchCriteria
	        {
		        LienId = LienToDischarge.ID,
	        }).FirstOrDefault();
        }

        public Master GetAccountDetails(int accountId)
        {
	        return masterQueryHandler.ExecuteQuery(new MasterSearchCriteria
	        {
		        Id = accountId,
	        }).FirstOrDefault();
        }

        public Lien LienToDischarge { get; set; }
        public DateTime PayDate { get; set; }
        public bool PrintAllSaved { get; set; }
        public bool PrintArchive { get; set; }
        public bool AbateDefault { get; set; }
        public void SaveNotice(ControlDischargeNotice dischargeNotice)
        {
            commandDispatcher.Send(new SaveControlDischargeNotice(dischargeNotice, (LienToDischarge.Water ?? false) ? UtilityType.Water : UtilityType.Sewer));
        }

        public void SetLienDischargePrinted()
        {
            commandDispatcher.Send(new SetLienDischargeNoticePrinted(LienToDischarge.ID));
        }

        public void SetDischargeNeededToPrinted()
        {
            commandDispatcher.Send(new SetDischargeNeededToPrinted(LienToDischarge.ID, DateTime.Today, PayDate));
        }

        public string TownName()
        {
	        return envSettings.CityTownClientName;
        }
    }
}
