﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UndeleteCustomer : SharedApplication.Messaging.Command<bool>
	{
		public int Account { get; set; }
	}
}
