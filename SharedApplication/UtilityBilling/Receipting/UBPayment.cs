﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UBPayment
	{
		public int Id { get; set; }
		public int? ReceiptId { get; set; }
		public DateTime? RecordedTransactionDate { get; set; }
		public DateTime? ActualSystemDate { get; set; }
		public string Code { get; set; }
		public string Reference { get; set; }
		public string Teller { get; set; }
		public string PaidBy { get; set; }
		public string Comments { get; set; }
		public Guid? TransactionId { get; set; }
		public DateTime? ChargedInterestDate { get; set; }
		public decimal Principal { get; set; } = 0;
		public decimal Tax { get; set; } = 0;
		public decimal PreLienInterest { get; set; } = 0;
		public decimal CurrentInterest { get; set; } = 0;
		public decimal Cost { get; set; } = 0;
		public string CashDrawer { get; set; }
		public string GeneralLedger { get; set; }
		public string BudgetaryAccountNumber { get; set; }
		public int? DailyCloseOut { get; set; }
		public int BillId { get; set; }
		public bool Pending { get; set; }
		public Guid PendingId { get; set; } = new Guid();
		public string EPaymentId { get; set; }
		public bool IsReversal { get; set; }
		public int ReversedPaymentId { get; set; }
		public DateTime? EffectiveInterestDate { get; set; }
		public UBPayment ChargedInterestRecord { get; set; } = null;

		public decimal InterestTotal
		{
			get { return PreLienInterest + CurrentInterest; }
		}

		public decimal PaymentTotal
		{
			get { return InterestTotal + Principal + Tax + Cost; }
		}

	}
}
