﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class BatchRecoverComboData
	{
		public string TellerId { get; set; }
		public DateTime? BatchRecoverDate { get; set; }
		public string PaidBy { get; set; }
	}
}
