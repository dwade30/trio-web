﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class MakeUtilityBillingTransaction : SharedApplication.Messaging.Command, MakeTransaction
	{
		public Guid CorrelationId { get; set; }
		public int AccountNumber { get; set; }
		public bool StartedFromCashReceipts { get; set; }
		public bool ShowPaymentScreen { get; set; }
		public bool AllowBatch { get; set; }
	}
}
