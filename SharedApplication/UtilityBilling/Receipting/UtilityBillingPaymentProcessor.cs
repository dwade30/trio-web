﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.RealEstate;
using SharedApplication.RealEstate.Models;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Extensions;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Extensions;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingPaymentProcessor : IUtilityBillingPaymentProcessor
	{
		private CommandDispatcher commandDispatcher;
		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>
			customerPartyQueryHandler;
		private IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler;
		private IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler;
		private IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>> reMasterQueryHandler;
		private GlobalUtilityBillingSettings settings;
		private IGlobalActiveModuleSettings activeModuleSettings;
		private string paidBy;
		private DateTime effectiveDate;
		private List<UBBill> bills;
		private UBCustomerPaymentStatusResult customer;
		private Master master;

		public UtilityBillingPaymentProcessor(CommandDispatcher commandDispatcher, IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>>
			customerPartyQueryHandler, IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler, IQueryHandler<UBBillSearchCriteria, IEnumerable<UBBill>> billQueryHandler,
			IQueryHandler<REMasterSearchCriteria, IEnumerable<REMaster>> reMasterQueryHandler)
		{
			this.customerPartyQueryHandler = customerPartyQueryHandler;
			this.masterQueryHandler = masterQueryHandler;
			this.billQueryHandler = billQueryHandler;
			this.reMasterQueryHandler = reMasterQueryHandler;

			settings = commandDispatcher.Send(new GetUtilityBillingSettings()).Result.ToGlobalUtilityBillingSettings();
			activeModuleSettings = commandDispatcher.Send(new GetModules()).Result.ToGlobalActiveModuleSettings();

		}

		public bool IsTaxAcquired()
		{

			if (master.TaxAcquired ?? false)
			{
				return true;
			}

			if (activeModuleSettings.RealEstateIsActive && settings.CheckRETaxAcquired &&
			    master.REAccount != 0)
			{
				var result = reMasterQueryHandler.ExecuteQuery(new REMasterSearchCriteria
				{
					AccountNumber = master.REAccount ?? 0,
					deletedOption = REMasterSearchCriteria.DeletedSelection.NotDeleted
				}).FirstOrDefault();

				if (result != null)
				{
					return result.TaxAcquired ?? false;
				}
			}

			return false;
		}

		public UtilityBillingPayPortTransaction ProcessPayment(ImportedPayment importedPayment, UtilityType service)
		{
			this.paidBy = importedPayment.PaidBy;
			this.effectiveDate = importedPayment.PaymentDate;

			customer = this.customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
			{
				AccountNumber = importedPayment.AccountNumber
			}).FirstOrDefault();

			master = this.masterQueryHandler.ExecuteQuery(new MasterSearchCriteria
			{
				AccountNumber = customer.AccountNumber
			}).FirstOrDefault();

			this.bills = billQueryHandler.ExecuteQuery(new UBBillSearchCriteria
			{
				AccountId = customer.Id,
				VoidedSelection = VoidedStatusSelection.NotVoided,
				CalculateCurrentInterest = true
			}).ToList();
			
			decimal totalAmount = importedPayment.PaymentAmount;

			var utilityList = bills.SelectMany(x => x.UtilityDetails).Where(y => y.BalanceDue > 0);

			if (settings.AutoPayOldestBillFirst)
			{
				utilityList = utilityList.OrderBy(x =>
					settings.PayWaterFirst
						? x.Service == UtilityType.Sewer
						: x.Service == UtilityType.Water).ThenBy(y => y.BillDate);
			}
			else
			{
                utilityList = utilityList.OrderBy(x => x.BillDate).ThenBy(x =>
					settings.PayWaterFirst
						? x.Service == UtilityType.Sewer
						: x.Service == UtilityType.Water);
			}

			foreach (var utility in utilityList)
			{
				var billAmounts = CalculatePaymentAmountsForUtility(utility, totalAmount);
				var payment = new UBPayment();

				payment.PreLienInterest = billAmounts.PreLienInterest;
				payment.CurrentInterest = billAmounts.Interest;
				payment.Tax = billAmounts.Tax;
				payment.Principal = billAmounts.Principal;
				payment.Cost = billAmounts.Cost;
				payment.Pending = true;
				payment.PendingId = Guid.NewGuid();
				payment.ActualSystemDate = DateTime.Today;
				payment.CashDrawer = "N";
				payment.Code = "P";
				payment.Comments = "";
				payment.EffectiveInterestDate = effectiveDate;
				payment.GeneralLedger = "Y";
				payment.RecordedTransactionDate = effectiveDate;
				payment.BudgetaryAccountNumber = "";
				payment.Reference = "EPYMT";
				payment.EPaymentId = importedPayment.PaymentIdentifier;
				if (utility.CurrentInterest - utility.PendingChargedInterest != 0)
				{
					payment.ChargedInterestRecord = AddPendingChargedInterest(utility, payment);
				}

				totalAmount = billAmounts.RemainingAmount;
				AddPaymentToUtility(utility, payment);

				if (totalAmount == 0)
				{
					break;
				}

				
			}

			if (totalAmount > 0)
			{
				AddPendingOverPayment(totalAmount, service == UtilityType.All
					? settings.PayWaterFirst ? UtilityType.Water : UtilityType.Sewer
					: service, importedPayment.PaymentIdentifier);
			}

			return new UtilityBillingPayPortTransaction
			{
				Id = Guid.NewGuid(),
				Customer = customer,
				TaxAcquired = IsTaxAcquired(),
				Bills = bills
			};
		}

		private BillPaymentCalculationResult CalculatePaymentAmountsForUtility(UBBillUtilityDetails utility,
			decimal paymentAmount, bool negativeBill = false)
		{
			var result = new BillPaymentCalculationResult();

			result.RemainingAmount = paymentAmount;

			decimal costNeeded = utility.CostDue;
			if (costNeeded < 0 && !negativeBill)
			{
				costNeeded = 0;
			}

			if (costNeeded >= result.RemainingAmount)
			{
				result.Cost = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Cost = costNeeded;
				result.RemainingAmount = result.RemainingAmount - costNeeded;
			}

			decimal intNeeded = utility.PreLienInterestDue;
			if (intNeeded < 0 && !negativeBill)
			{
				intNeeded = 0;
			}

			if (intNeeded >= result.RemainingAmount)
			{
				result.PreLienInterest = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.PreLienInterest = intNeeded;
				result.RemainingAmount = result.RemainingAmount - intNeeded;
			}

			intNeeded = utility.CurrentInterestDue;
			if (intNeeded < 0 && !negativeBill)
			{
				intNeeded = 0;
			}

			if (intNeeded >= result.RemainingAmount)
			{
				result.Interest = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Interest = intNeeded;
				result.RemainingAmount = result.RemainingAmount - intNeeded;
			}

			decimal taxNeeded = utility.TaxDue;
			if (taxNeeded < 0 && !negativeBill)
			{
				taxNeeded = 0;
			}

			if (taxNeeded >= result.RemainingAmount)
			{
				result.Tax = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Tax = taxNeeded;
				result.RemainingAmount = result.RemainingAmount - taxNeeded;
			}

			decimal principalNeeded = utility.PrincipalDue;
			if (principalNeeded < 0 && !negativeBill)
			{
				principalNeeded = 0;
			}

			if (principalNeeded >= result.RemainingAmount)
			{
				result.Principal = result.RemainingAmount;
				result.RemainingAmount = 0;
			}
			else
			{
				result.Principal = principalNeeded;
				result.RemainingAmount = result.RemainingAmount - principalNeeded;
			}

			return result;
		}

		private UBPayment AddPendingChargedInterest(UBBillUtilityDetails utility, UBPayment linkedPayment)
		{
			var chargedInterest = new UBPayment();
			
			chargedInterest.Pending = true;
			chargedInterest.PendingId = Guid.NewGuid();
			chargedInterest.ActualSystemDate = linkedPayment.ActualSystemDate;
			chargedInterest.RecordedTransactionDate = linkedPayment.RecordedTransactionDate;
			chargedInterest.EffectiveInterestDate = linkedPayment.EffectiveInterestDate;
			chargedInterest.EPaymentId = linkedPayment.EPaymentId;
			chargedInterest.Code = "I";
			chargedInterest.Principal = 0;
			chargedInterest.Tax = 0;
			chargedInterest.Cost = 0;
			
			chargedInterest.CurrentInterest = utility.CurrentInterest - utility.PendingChargedInterest;
			chargedInterest.PreLienInterest = 0;
			chargedInterest.Reference = chargedInterest.CurrentInterest > 0 ? "EARNINT" : "CHGINT";
			chargedInterest.ChargedInterestDate = utility.CurrentInterestDate;
			utility.PreviousInterestDate = utility.LastInterestDate;
			utility.LastInterestDate = utility.CurrentInterestDate;

			chargedInterest.GeneralLedger = linkedPayment.GeneralLedger;
			chargedInterest.CashDrawer = linkedPayment.CashDrawer;
			chargedInterest.BudgetaryAccountNumber = linkedPayment.BudgetaryAccountNumber;

			utility.PendingChargedInterest = utility.PendingChargedInterest + chargedInterest.CurrentInterest;

			return chargedInterest;
		}

		private void AddPaymentToUtility(UBBillUtilityDetails utility, UBPayment payment)
		{
			utility.PendingCurrentInterestAmount = utility.PendingCurrentInterestAmount + payment.CurrentInterest;
			utility.PendingPreLienInterestAmount = utility.PendingPreLienInterestAmount + payment.PreLienInterest;
			utility.PendingTaxAmount = utility.PendingTaxAmount + payment.Tax;
			utility.PendingPrincipalAmount = utility.PendingPrincipalAmount + payment.Principal;
			utility.PendingCostAmount = utility.PendingCostAmount + payment.Cost;

			payment.PaidBy = paidBy;

			utility.Payments.Add(payment);
		}

		private void AddPendingOverPayment(decimal amount, UtilityType service, string EPaymentId)
		{
			var payment = new UBPayment();
			var bill = new UBBill();
			var utility = new UBBillUtilityDetails();

			
			bill = GetPrepaymentBill(service);
			payment.Code = "Y";

			utility = GetUtilityDetails(bill, service);

			payment.ActualSystemDate = DateTime.Today;
			payment.Reference = "PREPAY-A";
			payment.EffectiveInterestDate = effectiveDate;
			payment.GeneralLedger = "Y";
			payment.Principal = amount;
			payment.CashDrawer = "N";
			payment.Pending = true;
			payment.PendingId = Guid.NewGuid();
			payment.RecordedTransactionDate = effectiveDate;
			payment.BudgetaryAccountNumber = "";
			payment.Comments = "";
			payment.EPaymentId = EPaymentId;
			AddPaymentToUtility(utility, payment);
		}

		private UBBill GetPrepaymentBill(UtilityType service)
		{
			try
			{
				var master = this.masterQueryHandler.ExecuteQuery(new MasterSearchCriteria
				{
					AccountNumber = customer.AccountNumber
				}).FirstOrDefault();

				var bill = bills.FirstOrDefault(x => x.BillNumber == 0);

				if (bill == null)
				{
					bill = new UBBill
					{
						Pending = true,
						BillStatus = "D",
						BillNumber = 0,
						CurrentReading = 0,
						PreviousReading = 0,
						FinalBill = false,
						BilledAddress1 = customer.BilledAddress1,
						BilledAddress2 = customer.BilledAddress2,
						BilledAddress3 = customer.BilledAddress3,
						BilledCity = customer.BilledCity,
						BilledState = customer.BilledState,
						BilledZip = customer.BilledZip,
						BilledZip4 = "",
						OwnerAddress1 = customer.OwnerAddress1,
						OwnerAddress2 = customer.OwnerAddress2,
						OwnerAddress3 = customer.OwnerAddress3,
						OwnerCity = customer.OwnerCity,
						OwnerState = customer.OwnerState,
						OwnerZip = customer.OwnerZip,
						OwnerZip4 = "",
						MeterId = master.MeterTables.FirstOrDefault(x => x.MeterNumber == 1).ID,
						Book = master.UTBook ?? 0,
						UtilityDetails = new List<UBBillUtilityDetails>()
					};

					var utility = new UBBillUtilityDetails
					{
						Service = service,
						LienId = 0,
						LienMainBill = false,
						BillOwner = service == UtilityType.Water
							? (master.WBillToOwner ?? false)
							: (master.SBillToOwner ?? false),
						CostAdded = 0,
						CostOwed = 0,
						CostPaid = 0,
						CurrentInterest = 0,
						DemandGroupId = 0,
						IntAdded = 0,
						IntOwed = 0,
						IntPaid = 0,
						InterestRate = 0,
						IsLien = false,
						IsLiened = false,
						MaturityFee = 0,
						PendingChargedInterest = 0,
						PendingCostAmount = 0,
						PendingCurrentInterestAmount = 0,
						PendingPreLienInterestAmount = 0,
						PendingPrincipalAmount = 0,
						PendingTaxAmount = 0,
						PerDiem = 0,
						PreLienInterestPaid = 0,
						PrincipalOwed = 0,
						PrincipalPaid = 0,
						RateDescription = "",
						TaxOwed = 0,
						TaxPaid = 0
					};

					bill.UtilityDetails.Add(utility);
					bills.Add(bill);
				}

				return bill;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return null;
			}
		}

		private UBBillUtilityDetails GetUtilityDetails(UBBill bill, UtilityType service)
		{
			return bill.UtilityDetails.FirstOrDefault(y => y.Service == service);
		}
	}
}
