﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingCustomerSearchViewModel : IUtilityBillingCustomerSearchViewModel
	{
		private Guid correlationID;
		private EventPublisher eventPublisher;
		private CommandDispatcher commandDispatcher;
		private Setting lastAccountAccessedSetting;
		private ISettingService settingService;
		private IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler;
		private IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>> customerPartyQueryHandler;
		private IGlobalActiveModuleSettings activeModuleSettings;
		private GlobalUtilityBillingSettings globalUtilityBillingSettings;
		private Master selectedCustomer = null;
		private IEnumerable<UBCustomerPaymentStatusResult> searchResults;
		
		public UtilityBillingCustomerSearchViewModel(EventPublisher eventPublisher, ISettingService settingService, IGlobalActiveModuleSettings activeModuleSettings, CommandDispatcher commandDispatcher, GlobalUtilityBillingSettings globalUtilityBillingSettings, IQueryHandler<CustomerPartySearchCriteria, IEnumerable<UBCustomerPaymentStatusResult>> customerPartyQueryHandler, IQueryHandler<MasterSearchCriteria, IEnumerable<Master>> masterQueryHandler)
		{
			this.eventPublisher = eventPublisher;
			this.settingService = settingService;
			this.activeModuleSettings = activeModuleSettings;
			this.commandDispatcher = commandDispatcher;
			this.globalUtilityBillingSettings = globalUtilityBillingSettings;
			this.customerPartyQueryHandler = customerPartyQueryHandler;
			this.masterQueryHandler = masterQueryHandler;


			lastAccountAccessedSetting = settingService.GetSettingValue(SettingOwnerType.User, "UTLastAccountNumber");

			if (lastAccountAccessedSetting == null)
			{
				lastAccountAccessedSetting = new Setting
				{
					SettingName = "UTLastAccountNumber",
					SettingValue = "0"
				};
			}

			SearchResultsVisible = false;
		}

		public int LastAccountAccessed
		{
			get => lastAccountAccessedSetting.SettingValue.ToIntegerValue();
			set
			{
				lastAccountAccessedSetting.SettingValue = value.ToString();
				settingService.SaveSetting(SettingOwnerType.User, lastAccountAccessedSetting.SettingName,lastAccountAccessedSetting.SettingValue);
			}
		}

		public void SetSelectedCustomer(int account, bool deletedOnly = false)
		{
			selectedCustomer = FindCustomer(account, deletedOnly);
		}

		public Master SelectedCustomer
		{
			get => selectedCustomer;
			set => selectedCustomer = value;
		}

		public Master FindCustomer(int accountNumber, bool deletedOnly)
		{
			return masterQueryHandler.ExecuteQuery(new MasterSearchCriteria
			{
				AccountNumber = accountNumber,
				deletedOption = deletedOnly ? DeletedSelection.OnlyDeleted : DeletedSelection.All
			}).FirstOrDefault();
		}

		public bool StartedFromCashReceipts { get; set; }

		public bool AllowBatch { get; set; }

		public bool SearchResultsVisible { get; set; }

		public bool ShowPaymentScreen { get; set; }
        public bool ShowAccountScreen { get; set; } = false;

        public IEnumerable<UBCustomerPaymentStatusResult> SearchResults
		{
			get => searchResults;
		}
		

		public bool ShowLastAccount()
		{
			return (StartedFromCashReceipts && globalUtilityBillingSettings.ShowLastUBAccountInCR) || !StartedFromCashReceipts;
		}

		public void Cancel()
		{
			eventPublisher.Publish(new TransactionCancelled(correlationID, null));
		}

		public void SetCorrelationId(Guid correlationID)
		{
			this.correlationID = correlationID;
		}

		public void ProceedToNextScreen(int account, int id)
		{
            if (ShowAccountScreen)
            {
                if (account == 0)
                {
                    id = -1;
                }
                commandDispatcher.Send(new ShowUTAccountMaster(account,id));
            }
            else
            {
                commandDispatcher.Send(new ShowUBPaymentStatusScreen
                {
                    AccountNumber = account,
                    CorrelationId = this.correlationID,
                    IsPaymentScreen = ShowPaymentScreen,
                    StartedFromCashReceipts = StartedFromCashReceipts
                });
            }
        }

		public void ProceedToBatchScreen()
		{
			commandDispatcher.Send(new CreateUBBatchPayment
			{
				CorrelationId = this.correlationID,
				StartedFromCashReceipts = StartedFromCashReceipts
			});
		}

		public bool UndeleteCustomer(int account)
		{
			return commandDispatcher.Send(new UndeleteCustomer()
			{
				Account = account
			}).Result;
		}

		public void Search(SearchType searchType, string criteria, string criteria2, SearchCriteriaMatchType matchType, bool IncludePreviousOwners)
		{
			try
			{
				searchResults = customerPartyQueryHandler.ExecuteQuery(new CustomerPartySearchCriteria
				{
					StreetName = searchType == SearchType.Location ? criteria : "",
					StreetNumber = searchType == SearchType.Location ? criteria2 : "",
					Name = searchType == SearchType.Name ? criteria : "",
					Address = searchType == SearchType.Address ? criteria : "",
					AccountNumber = searchType == SearchType.AccountNumber ? criteria.ToIntegerValue() : 0,
					XRef1 = searchType == SearchType.XRef1 ? criteria : "",
					SerialNumber = searchType == SearchType.SerialNumber ? criteria : "",
					MapLot = searchType == SearchType.MapLot ? criteria : "",
					Remote = searchType == SearchType.Remote ? criteria : "",
					REAccount = searchType == SearchType.REAccountNumber ? criteria.ToIntegerValue() : 0,
					MatchCriteriaType = matchType,
					IncludePreviousOwners = IncludePreviousOwners
				});
			}
			catch (Exception e)
			{
				searchResults = new List<UBCustomerPaymentStatusResult>();
			}
		}

		public enum SearchType
		{
			Name = 1,
			Address = 2,
			AccountNumber = 3,
			MapLot = 4,
			Location = 5,
			REAccountNumber = 6,
			SerialNumber = 7,
			Remote = 8,
			XRef1 = 9
		}
	}
}
