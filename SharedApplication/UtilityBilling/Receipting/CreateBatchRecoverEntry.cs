﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class CreateBatchRecoverEntry : SharedApplication.Messaging.Command<int>
	{
		public BatchRecover BatchRecoverData { get; set; }
	}
}
