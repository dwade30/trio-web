﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingBatchPaymentViewModel : IUtilityBillingBatchPaymentViewModel
	{
		private Guid correlationID;
		private DateTime effectiveDate;

		private EventPublisher eventPublisher;
		private CommandDispatcher commandDispatcher;
		private GlobalUtilityBillingSettings globalUtilityBillingSettings;
		private IQueryHandler<BatchRecoverSearchCriteria, IEnumerable<BatchRecover>> batchRecoverQueryHandler;

		public UtilityBillingBatchPaymentViewModel(EventPublisher eventPublisher, GlobalUtilityBillingSettings globalUtilityBillingSettings, CommandDispatcher commandDispatcher, IQueryHandler<BatchRecoverSearchCriteria, IEnumerable<BatchRecover>> batchRecoverQueryHandler)
		{
			this.eventPublisher = eventPublisher;
			this.globalUtilityBillingSettings = globalUtilityBillingSettings;
			this.commandDispatcher = commandDispatcher;
			this.batchRecoverQueryHandler = batchRecoverQueryHandler;

			BatchStarted = false;
			EffectiveDate = DateTime.Today;
			PaymentDate = DateTime.Today;
			TellerId = "";
			PaidBy = "";
		}

		public DateTime EffectiveDate { get; set; }

		public bool ValidTellerId(string tellerId)
		{
			if (tellerId.Trim() == "")
			{
				return false;
			}

			if (StartedFromCashReceipts)
			{
				var result = commandDispatcher.Send<TellerValidationResult>(new ValidateTellerId(tellerId, "")).Result;

				if (result == TellerValidationResult.Invalid)
				{
					return false;
				}
			}

			return true;
		}

		public string PaidBy { get; set; }

		public int AddBatchRecoverEntry(BatchRecover data)
		{
			return commandDispatcher.Send(new CreateBatchRecoverEntry
			{
				BatchRecoverData = data
			}).Result;
		}

		public IEnumerable<BatchRecoverComboData> GetBatchRecoverData()
		{
			var results = batchRecoverQueryHandler.ExecuteQuery(new BatchRecoverSearchCriteria());

			if (results != null)
			{
				return results.Select(x => new BatchRecoverComboData
				{
					BatchRecoverDate = x.BatchRecoverDate,
					PaidBy = x.PaidBy,
					TellerId = x.TellerId
				}).Distinct().ToList();
			}
			else
			{
				return null;
			}
		}

		public string TellerId { get; set; }

		public DateTime PaymentDate { get; set; }

		public bool BatchStarted { get; set; }
		public void SetCorrelationId(Guid correlationID)
		{
			this.correlationID = correlationID;
		}

		public bool StartedFromCashReceipts { get; set; }
	}
}
