﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class CreateUBBatchPaymentHandler : CommandHandler<CreateUBBatchPayment>
	{
		private IView<IUtilityBillingBatchPaymentViewModel> batchPaymentView;

		public CreateUBBatchPaymentHandler(IView<IUtilityBillingBatchPaymentViewModel> batchPaymentView)
		{
			this.batchPaymentView = batchPaymentView;
		}
		protected override void Handle(CreateUBBatchPayment command)
		{
			batchPaymentView.ViewModel.SetCorrelationId(command.CorrelationId);
			batchPaymentView.ViewModel.StartedFromCashReceipts = command.StartedFromCashReceipts;
			batchPaymentView.Show();

		}
	}
}
