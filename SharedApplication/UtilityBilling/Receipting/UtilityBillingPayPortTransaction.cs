﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingPayPortTransaction
	{
		public UBCustomerPaymentStatusResult Customer { get; set; }
		public Guid Id { get; set; } = Guid.NewGuid();
		public bool TaxAcquired { get; set; } = false;
		public IEnumerable<UBBill> Bills { get; set; } = new List<UBBill>();
	}
}
