﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.AccountsReceivable.Receipting.Interfaces;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class MakeUtilityBillingTransactionHandler : CommandHandler<MakeUtilityBillingTransaction>
	{
		private IView<IUtilityBillingCustomerSearchViewModel> customerSearchView;
		private CommandDispatcher commandDispatcher;

		public MakeUtilityBillingTransactionHandler(IView<IUtilityBillingCustomerSearchViewModel> searchView, CommandDispatcher commandDispatcher)
		{
			this.customerSearchView = searchView;
			this.commandDispatcher = commandDispatcher;
		}
		protected override void Handle(MakeUtilityBillingTransaction command)
		{
			UtilityBillingTransaction transaction = new UtilityBillingTransaction();
			transaction.CorrelationId = command.CorrelationId;

			if (command.AccountNumber == 0)
			{
				customerSearchView.ViewModel.ShowPaymentScreen = command.ShowPaymentScreen;
				customerSearchView.ViewModel.SetCorrelationId(command.CorrelationId);
				customerSearchView.ViewModel.StartedFromCashReceipts = command.StartedFromCashReceipts;
				customerSearchView.ViewModel.AllowBatch = command.AllowBatch;
				customerSearchView.Show();
			}
			else
			{
				commandDispatcher.Send(new ShowUBPaymentStatusScreen()
				{
					AccountNumber = command.AccountNumber,
					CorrelationId = command.CorrelationId,
					IsPaymentScreen = command.ShowPaymentScreen,
					StartedFromCashReceipts = command.StartedFromCashReceipts
				});
			}
		}
	}
}
