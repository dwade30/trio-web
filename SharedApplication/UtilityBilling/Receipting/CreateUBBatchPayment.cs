﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class CreateUBBatchPayment : SharedApplication.Messaging.Command, MakeTransaction
	{
		public Guid CorrelationId { get; set; }
		public bool StartedFromCashReceipts { get; set; }
	}
}
