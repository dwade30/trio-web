﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UtilityBillingTransaction
	{
		public Guid CorrelationId { get; set; }
		public int AccountNumber { get; set; }
		public Guid Id { get; set; } = Guid.NewGuid();
		public IEnumerable<UBBill> Bills { get; set; } = new List<UBBill>();
	}
}
