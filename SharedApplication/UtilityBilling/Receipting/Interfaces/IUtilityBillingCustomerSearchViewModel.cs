﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Enums;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Receipting.Interfaces
{
	public interface IUtilityBillingCustomerSearchViewModel
	{
		int LastAccountAccessed { get; set; }
		bool StartedFromCashReceipts { get; set; }
		bool ShowLastAccount();
		void Cancel();
		void SetCorrelationId(Guid correlationID);
		bool ShowPaymentScreen { get; set; }
        bool ShowAccountScreen { get; set; }
		void ProceedToNextScreen(int account, int id);
		void ProceedToBatchScreen();
		bool AllowBatch { get; set; }
		bool SearchResultsVisible { get; set; }
		Master FindCustomer(int accountNumber, bool deletedOnly);
		void SetSelectedCustomer(int account, bool deletedOnly = false);
		Master SelectedCustomer { get; set; }
		bool UndeleteCustomer(int account);
		void Search(UtilityBillingCustomerSearchViewModel.SearchType searchType, string criteria, string criteria2, SearchCriteriaMatchType matchType, bool IncludePreviousOwners);
		IEnumerable<UBCustomerPaymentStatusResult> SearchResults { get; }
	}
}
