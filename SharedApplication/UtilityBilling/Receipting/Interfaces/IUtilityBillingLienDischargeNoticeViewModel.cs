﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Receipting.Interfaces
{
	public interface IUtilityBillingLienDischargeNoticeViewModel
	{
		ControlDischargeNotice GetControlDischargeNotice();
		ControlLienProcess GetControlLienProcess();
		Lien LienToDischarge { get; set; }
		DateTime PayDate { get; set; }
		void SaveNotice(ControlDischargeNotice dischargeNotice);
		void SetLienDischargePrinted();
		void SetDischargeNeededToPrinted();
		string TownName();
		bool PrintAllSaved { get; set; }
		bool PrintArchive { get; set; }
		bool AbateDefault { get; set; }
		UBBill GetLienBill();
		Master GetAccountDetails(int accountNumber);
	}
}
