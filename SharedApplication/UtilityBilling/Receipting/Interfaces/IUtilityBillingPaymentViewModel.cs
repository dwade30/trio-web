﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountGroups;
using SharedApplication.CentralData.Models;
using SharedApplication.Models;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Receipting.Interfaces
{
	public interface IUtilityBillingPaymentViewModel
	{
		bool IsPaymentScreen { get; set; }
		void SetCorrelationId(Guid correlationID);
		UtilityBillingTransaction Transaction { get; set; }
		DateTime EffectiveDate { get; set; }
		DateTime ReversalEffectiveDate { get; set; }
		bool StartedFromCashReceipts { get; set; }
		bool IsCashReceiptsActive();
		bool ShowAllBills { get; set; }
		bool AllowBatch { get; set; }
		List<UBBill> Bills(UtilityType type);
		UBCustomerPaymentStatusResult Customer { get; }
		GroupMaster GroupInfo { get; }
		void CalculateBill(UBBill bill);
		decimal TotalPerDiem { get; }
		UBBill GetBill(int id);
		UBBill GetBill(string billNumber);
		UBBillUtilityDetails GetUtilityDetails(UBPayment payment);
		Comment AccountNote { get; set; }
		bool PopUpComment();
        void Cancel();
		bool DontFireCollapse { get; set; }
		bool NegativeBillsExist(UtilityType service);
		void AdjustNegativeBillsToAddPayment(UtilityType service);
		UBPayment GetPayment(int id);
		bool LatestPaymentOnUtility(UBPayment payment, UtilityType service);
		UBPayment ChargedInterestToReverse { get; set; }
		bool ChargedInterestExistsOnUtility(int id, UtilityType service);
		bool BudgetaryExists { get; }
		bool ValidManualAccount(string account, UtilityType service);
		string GetPaymentReceiptNumber(int receiptKey, DateTime? receiptDateTime);
		bool PaymentGreaterThanOwed(string billNumber, UtilityType service, decimal paymentTotal);
		void AddPendingPayment(string billNumber, UtilityType service, string code, string reference, string cashDrawer,
			string generaslLedger,
			string budgetaryAccountNumber, string comment, DateTime? transactionDate, decimal principal, decimal tax,
			decimal interest, decimal cost, bool skipRecalc = false);
		bool PendingPaymentsExistForSelectedBill(string billNumber);
		bool PendingPaymentsExist();
		decimal TotalPendingPaymentsAmount(UtilityType service);
		void RemoveAllPendingPayments();
		void RemovePendingPayment(UBPayment payment);
		UBPayment GetPendingPayment(Guid pendingId);
		List<UBPayment> GetPendingPayments(string billNumber);
		void GoToSearchScreen();
		void CompleteTransaction();
		UtilityType AccountServices();
		bool HasWaterBill();
		bool HasSewerBill();
		string ClientName { get; }
		UtilityType TownServices { get; }
		bool ResetTransactionDate { get; set; }
		bool PrepaymentExists(UtilityType service);
		UtilityType VisibleService { get; set; }
		bool DisableAutoPaymentInsert { get; }
		Master Master { get; }
		bool IsTaxAcquired();
		tblAcctACH AccountACHInfo { get; }
		bool LienDischargeExists();
		DischargeNeeded LienDischarge(int lienId);
		bool PayWaterFirst { get; }
		string GetAbatementAccount(UtilityType service, int billNumber);
		UBBillUtilityDetails GetUtilityDetails(int billNumber, UtilityType service);
		UBBillUtilityDetails GetUtilityDetails(UBBill bill, UtilityType service);
		void UpdateCommentRec();
		CommentRec AccountCommentRec { get; }
		UBBill GetBill(UBPayment payment);
		ModuleAssociation ModuleAssociationInfo { get; set; }
		List<UBPayment> UtilityPayments(UBBillUtilityDetails utility);
        void EditNote();
        AccountGroup GetGroupSummary(DateTime effectiveDate);
		event EventHandler AccountChanged;
		event EventHandler<UBPayment> PendingPaymentAdded;
	}
}
