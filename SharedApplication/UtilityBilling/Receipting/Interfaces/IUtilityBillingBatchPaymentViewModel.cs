﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.UtilityBilling.Models;

namespace SharedApplication.UtilityBilling.Receipting.Interfaces
{
	public interface IUtilityBillingBatchPaymentViewModel
	{
		void SetCorrelationId(Guid correlationID);
		bool StartedFromCashReceipts { get; set; }
		DateTime EffectiveDate { get; set; }
		bool BatchStarted { get; set; }
		string PaidBy { get; set; }
		string TellerId { get; set; }
		DateTime PaymentDate { get; set; }
		bool ValidTellerId(string tellerId);
		IEnumerable<BatchRecoverComboData> GetBatchRecoverData();
		int AddBatchRecoverEntry(BatchRecover data);

	}
}
