﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.UtilityBilling.Receipting
{
	public class UBBill
	{
		public DateTime? BillDate { get; set; }
		public DateTime? DueDate { get; set; }
		public int Id { get; set; }
		public string BillStatus { get; set; } = "";
		public int BillNumber { get; set; } = 0;
		public bool Pending { get; set; } = false;
		public string Bname { get; set; } = "";
		public string Bname2 { get; set; } = "";
		public string Oname { get; set; } = "";
		public string Oname2 { get; set; } = "";
		public string BilledCity { get; set; } = "";
		public string BilledState { get; set; } = "";
		public string BilledAddress1 { get; set; } = "";
		public string BilledAddress2 { get; set; } = "";
		public string BilledAddress3 { get; set; } = "";
		public string BilledZip { get; set; } = "";
		public string BilledZip4 { get; set; } = "";
		public string OwnerCity { get; set; } = "";
		public string OwnerState { get; set; } = "";
		public string OwnerAddress1 { get; set; } = "";
		public string OwnerAddress2 { get; set; } = "";
		public string OwnerAddress3 { get; set; } = "";
		public string OwnerZip { get; set; } = "";
		public string OwnerZip4 { get; set; } = "";
		public bool IsLien { get; set; } = false;
		public bool FinalBill { get; set; } = false;
		public DateTime? FinalBillDate { get; set; }
		public DateTime? FinalBillStartDate { get; set; }
		public DateTime? FinalBillEndDate { get; set; }
		public DateTime? CurrentReadingDate { get; set; }
		public DateTime? PreviousReadingDate { get; set; }
		public int CurrentReading { get; set; } = 0;
		public int PreviousReading { get; set; } = 0;
		public int Book { get; set; } = 0;
		public int Page { get; set; } = 0;
		public int MeterId { get; set; } = 0;
		public int AccountId { get; set; } = 0;
		public string MapLot { get; set; } = "";

		public List<UBBillUtilityDetails> UtilityDetails { get; set; } = new List<UBBillUtilityDetails>();

		public bool ContainsService(UtilityType service)
		{
			if (service == UtilityType.All)
			{
				return UtilityDetails.Count > 0;
			}
			else
			{
				return UtilityDetails.Any(x => x.Service == service);
			}
			
		}

		public string BilledZipFormatted
		{
			get
			{
				if (BilledZip4.Trim() != "")
				{
					return BilledZip.Trim() +"-" + BilledZip4.Trim();
				}
				else
				{
					return BilledZip.Trim();
				}
			}
		}

		public string OwnerZipFormatted
		{
			get
			{
				if (OwnerZip4.Trim() != "")
				{
					return OwnerZip.Trim() + "-" + OwnerZip4.Trim();
				}
				else
				{
					return OwnerZip.Trim();
				}
			}
		}

		public string BilledNameFormatted
		{
			get
			{
				if (Bname2.Trim() != "")
				{
					return Bname.Trim() + " and " + Bname2.Trim();
				}
				else
				{
					return Bname.Trim();
				}
			}
		}

		public string OwnerNameFormatted
		{
			get
			{
				if (Oname2.Trim() != "")
				{
					return Oname.Trim() + " and " + Oname2.Trim();
				}
				else
				{
					return Oname.Trim();
				}
			}
		}

		public List<UBPayment> BillPayments(UtilityType service)
		{
			if (service == UtilityType.All)
			{
				return UtilityDetails.SelectMany(x => x.Payments).ToList();
			}
			else
			{
				return UtilityDetails.Where(y => y.Service == service).SelectMany(x => x.Payments).ToList();
			}
		}

		


	}

	public enum UtilityType
	{
		None = 0,
		Water = 1,
		Sewer = 2,
		All = 3
	}
}
