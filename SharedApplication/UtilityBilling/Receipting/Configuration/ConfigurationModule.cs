﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace SharedApplication.UtilityBilling.Receipting.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<UtilityBillingCustomerSearchViewModel>().As<IUtilityBillingCustomerSearchViewModel>();
			builder.RegisterType<UtilityBillingPaymentViewModel>().As<IUtilityBillingPaymentViewModel>();
			builder.RegisterType<UtilityBillingBatchPaymentViewModel>().As<IUtilityBillingBatchPaymentViewModel>();
			builder.RegisterType<UtilityBillingPaymentProcessor>().As<IUtilityBillingPaymentProcessor>();
			builder.RegisterType<BillCalculationService>().As<IBillCalculationService>();
			builder.RegisterType<UtilityBillingLienDischargeNoticeViewModel>().As<IUtilityBillingLienDischargeNoticeViewModel>();
		}

	}

}
