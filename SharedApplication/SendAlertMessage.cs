﻿using SharedApplication.Messaging;

namespace SharedApplication
{
    public class SendAlertMessage : Command
    {
        public string Message { get;  }
        public string Title { get; }

        public SendAlertMessage(string message,string title)
        {
            this.Message = message;
            Title = title;
        }
    }
}