﻿using System.Collections.Generic;

namespace SharedApplication
{
    public interface IPurge
    {
        Dictionary<string,int> Purge(List<string> listYearsToPurge);
    }
}