﻿using System.Collections.Generic;

namespace SharedApplication.AppConfiguration
{
    public class TrioWebConfiguration
    {
        private List<TrioClientSetup> clientList = new List<TrioClientSetup>();
        public IEnumerable<TrioClientSetup> Clients { get => clientList;  }

        public void AddClient(TrioClientSetup client)
        {
            if (client != null)
            {
                clientList.Add(client);
            }
        }
    }

    public class TrioClientSetup
    {
        public string Name { get; set; } = "";
        public string DataSource { get; set; } = "";
        public string User { get; set; } = "";
        public string Password { get; set; } = "";
        public bool IsHarrisStaff { get; set; } = false;
        private List<TrioDataEnvironment> dataEnvironments = new List<TrioDataEnvironment>();
        public IEnumerable<TrioDataEnvironment> DataEnvironments
        {
            get => dataEnvironments;
        }

        public void AddDataEnvironment(TrioDataEnvironment dataEnvironment)
        {
            if (dataEnvironment != null)
            {
                dataEnvironments.Add(dataEnvironment);
            }
        }
    }

    public class TrioDataEnvironment
    {
        public string Name { get; set; } = "";
        public string DataDirectory { get; set; } = "";
    }
}