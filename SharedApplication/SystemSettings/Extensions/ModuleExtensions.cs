﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Clerk.Enums;
using SharedApplication.Clerk.Models;
using SharedApplication.Extensions;
using SharedApplication.SystemSettings.Models;

namespace SharedApplication.SystemSettings.Extensions
{
	public static class ModuleExtensions
	{
		public static GlobalActiveModuleSettings ToGlobalActiveModuleSettings(
			this Module module)
		{
			if (module == null)
			{
				return new GlobalActiveModuleSettings();
			}
			var setting = new GlobalActiveModuleSettings();

            if ((module.AR ?? false) && module.ARDate >= DateTime.Today)
            {
                setting.AccountsReceivableIsActive = true;
            }

            if ((module.BD ?? false) && module.BDDate >= DateTime.Today)
            {
                setting.BudgetaryIsActive = true;
            }

            if ((module.BL ?? false) && module.BLDate >= DateTime.Today)
            {
                setting.TaxBillingIsActive = true;
            }

            if ((module.CE ?? false) && module.CEDate >= DateTime.Today)
            {
                setting.CodeEnforcementIsActive = true;
            }

            if ((module.CL ?? false) && module.CLDate >= DateTime.Today)
            {
                setting.TaxCollectionsIsActive = true;
            }

            if ((module.CK ?? false) && module.CKDate >= DateTime.Today)
            {
                setting.ClerkIsActive = true;
            }

            if ((module.CR ?? false) && module.CRDate >= DateTime.Today)
            {
                setting.CashReceiptingIsActive = true;
            }

            if ((module.FA ?? false) && module.FADate >= DateTime.Today)
            {
                setting.FixedAssetsIsActive = true;
            }

            if ((module.MV ?? false) && module.MVDate >= DateTime.Today)
            {
                setting.MotorVehicleIsActive = true;
            }

            if ((module.MS ?? false) && module.MSDate >= DateTime.Today)
            {
                setting.MosesIsActive = true;
            }

            if ((module.PP ?? false) && module.PPDate >= DateTime.Today)
            {
                setting.PersonalPropertyIsActive = true;
            }

            if ((module.PY ?? false) && module.PYDate >= DateTime.Today)
            {
                setting.PayrollIsActive = true;
            }

            if ((module.RB ?? false) && module.RBDate >= DateTime.Today)
            {
                setting.RedbookIsActive = true;
            }

            if ((module.RE ?? false) && module.REDate >= DateTime.Today)
            {
                setting.RealEstateIsActive = true;
            }

            if ((module.TS ?? false) && module.TSDate >= DateTime.Today)
            {
                setting.TaxServiceIsActive = true;
            }

            if ((module.UT ?? false) && module.UTDate >= DateTime.Today)
            {
                setting.UtilityBillingIsActive = true;
            }

            if ((module.MR ?? false) && module.MRDate >= DateTime.Today)
            {
                setting.CRMyRecIsActive = true;
            }
            return setting;
		}

        public static ClerkLevelPermissions ToClerkLevelPermissions(
            this Module module)
        {
            if (module == null)
            {
                return new ClerkLevelPermissions();
            }

            var setting = new ClerkLevelPermissions();

            setting.AllowGroups = true;

            if ((module.CKLevel.ToString().ToIntegerValue() & (int)ClerkLevels.VitalsOnly) == (int)ClerkLevels.VitalsOnly)
            {
	            setting.AllowDogs = false;
	            setting.AllowGroups = false;
            }
            else
            {
	            setting.AllowDogs = true;
            }

            if ((module.CKLevel.ToString().ToIntegerValue() & (int)ClerkLevels.DogsOnly) == (int)ClerkLevels.DogsOnly)
            {
	            setting.AllowMarriages = false;
	            setting.AllowBirths = false;
	            setting.AllowDeaths = false;
                setting.AllowGroups = false;
            }
            else
            {
	            setting.AllowMarriages = true;
	            setting.AllowBirths = true;
	            setting.AllowDeaths = true;
            }

            return setting;
        }
    }
}
