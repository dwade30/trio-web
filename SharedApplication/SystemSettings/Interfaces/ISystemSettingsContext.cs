﻿using System.Linq;
using SharedApplication.SystemSettings.Models;

namespace SharedApplication.SystemSettings.Interfaces
{
    public interface ISystemSettingsContext
    {
        IQueryable<UserPermission> UserPermissions { get; }
        IQueryable<Archive> Archives { get;  }
        IQueryable<Security> Securities { get;  }
        IQueryable<Setting> Settings { get; }
        IQueryable<PasswordHistory> PasswordHistories { get; }
        IQueryable<GlobalVariable> GlobalVariables { get; }
        IQueryable<Operator> Operators { get; }
        IQueryable<Module> Modules { get; }
    }
}