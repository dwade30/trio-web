﻿using System.Collections.Generic;
using SharedApplication.SystemSettings.Models;

namespace SharedApplication.SystemSettings.Interfaces
{
	public interface IUserDataAccess
	{
		Models.Security GetUserByID(int lngUserID);
		Models.Security GetUserByUserId(string strUserId);
		IEnumerable<Models.Security> GetUsers();
		IEnumerable<PasswordHistory> GetPreviousPasswords(int userId);
		bool UpdatePassword(Models.Security user, string newPassword, string salt);
		bool SaveUser(Models.Security user);
		bool SaveChanges();
	}
}