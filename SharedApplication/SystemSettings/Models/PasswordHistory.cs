﻿using System;

namespace SharedApplication.SystemSettings.Models
{
	public partial class PasswordHistory
	{
		public int Id { get; set; }

		public int SecurityId { get; set; }

		public Security User { get; set; }

		public string Password { get; set; }

		public string Salt { get; set; }

		public DateTime? DateAdded { get; set; }
	}
}
