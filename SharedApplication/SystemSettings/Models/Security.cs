using System.Collections.Generic;

namespace SharedApplication.SystemSettings.Models
{
    using System;

    public partial class Security
    {
        public int ID { get; set; }

        public string UserID { get; set; }

        public string OPID { get; set; }

        public string UserName { get; set; }

        public int? Frequency { get; set; }

        public DateTime? DateChanged { get; set; }

        public bool? UseSecurity { get; set; }

        public DateTime? UpdateDate { get; set; }

        public string Password { get; set; }

        public bool? DefaultAdvancedSearch { get; set; }

        public bool? LockedOut { get; set; }

        public bool? Inactive { get; set; }

		public int? FailedAttempts { get; set; }

        public DateTime? LockoutDateTime { get; set; }

        public string Salt { get; set; }

        public ICollection<PasswordHistory> PreviousPasswords { get; set; }
	}
}
