﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.SystemSettings.Models
{
    public class Module
    {
        public int ID { get; set; }

        public bool? ET { get; set; }

        public DateTime? ETDate { get; set; }

        public bool? PW { get; set; }

        public DateTime? PWDate { get; set; }

        public bool? RE { get; set; }

        public DateTime? REDate { get; set; }

        public bool? WS { get; set; }

        public DateTime? WSDate { get; set; }

        public bool? RP { get; set; }

        public DateTime? RPDate { get; set; }

        public bool? PP { get; set; }

        public DateTime? PPDate { get; set; }

        public bool? BL { get; set; }

        public DateTime? BLDate { get; set; }

        public bool? CL { get; set; }

        public DateTime? CLDate { get; set; }

        public bool? CK { get; set; }

        public DateTime? CKDate { get; set; }

        public bool? VR { get; set; }

        public DateTime? VRDate { get; set; }

        public bool? CE { get; set; }

        public DateTime? CEDate { get; set; }

        public bool? BD { get; set; }

        public DateTime? BDDate { get; set; }

        public bool? CR { get; set; }

        public DateTime? CRDate { get; set; }

        public bool? PY { get; set; }

        public DateTime? PYDate { get; set; }

        public bool? UT { get; set; }

        public DateTime? UTDate { get; set; }

        public bool? MV { get; set; }

        public DateTime? MVDate { get; set; }

        public bool? TC { get; set; }

        public DateTime? TCDate { get; set; }

        public bool? E9 { get; set; }

        public DateTime? E9Date { get; set; }

        public string CheckDigits { get; set; }

        public string TownCheck { get; set; }

        public bool? CM { get; set; }

        public DateTime? CMDate { get; set; }

        public string MVRR { get; set; }

        public string CRMoses { get; set; }

        public bool? FA { get; set; }

        public DateTime? FADate { get; set; }

        public bool? TS { get; set; }

        public DateTime? TSDate { get; set; }

        public bool? AR { get; set; }

        public DateTime? ARDate { get; set; }

        public bool? NT { get; set; }

        public DateTime? NTDate { get; set; }

        public bool? IV { get; set; }

        public DateTime? IVDate { get; set; }

        public bool? RR { get; set; }

        public DateTime? RRDate { get; set; }

        public bool? MS { get; set; }

        public DateTime? MSDate { get; set; }

        public short? PPLevel { get; set; }

        public short? CLLevel { get; set; }

        public short? BDLevel { get; set; }

        public short? CELevel { get; set; }

        public short? VRLevel { get; set; }

        public short? PYLevel { get; set; }

        public short? UTLevel { get; set; }

        public short? FALevel { get; set; }

        public short? TSLevel { get; set; }

        public short? CKLevel { get; set; }

        public short? ARLevel { get; set; }

        public short? TCLevel { get; set; }

        public int? RE_MaxAccounts { get; set; }

        public short? RELevel { get; set; }

        public int? PP_MaxAccounts { get; set; }

        public short? MVLevel { get; set; }

        public bool? RB { get; set; }

        public DateTime? RBDate { get; set; }

        public short? RBLevel { get; set; }

        public string MV_StateLevel { get; set; }

        public string MV_RBLVL_AUTO { get; set; }

        public string MV_RBLVL_HVTK { get; set; }

        public string MV_RBLVL_MTCY { get; set; }

        public string MV_RBLVL_RCVH { get; set; }

        public string MV_RENTALS { get; set; }

        public string MV_RBLVL_TKBD { get; set; }

        public string MV_RBLVL_TRAL { get; set; }

        public short? BLLevel { get; set; }

        public short? CRLevel { get; set; }

        public short? E9Level { get; set; }

        public bool? HR { get; set; }

        public DateTime? HRDate { get; set; }

        public short? HRLevel { get; set; }

        public short? IVLevel { get; set; }

        public short? MSLevel { get; set; }

        public short? NTLevel { get; set; }

        public bool? RH { get; set; }

        public DateTime? RHDate { get; set; }

        public short? RHLevel { get; set; }

        public short? RRLevel { get; set; }

        public bool? SK { get; set; }

        public DateTime? SKDate { get; set; }

        public short? SKLevel { get; set; }

        public bool? XF { get; set; }

        public DateTime? XFDate { get; set; }

        public short? XFLevel { get; set; }

        public bool? RX { get; set; }

        public DateTime? RXDate { get; set; }

        public short? RXLevel { get; set; }

        public short? CMLevel { get; set; }

        public bool? ESPService { get; set; }

        public DateTime? ESPDate { get; set; }

        public short? TimeClockLevel { get; set; }
        public bool? MR { get; set; }
        public short? MRLevel { get; set; }
        public DateTime? MRDate { get; set; }
        public string CRMyRec { get; set; }
    }
}
