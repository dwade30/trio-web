﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.SystemSettings.Models
{
    public partial class GlobalVariable
    {
        public int ID { get; set; }

        public string MuniName { get; set; }
        public string CityTown { get; set; }
        public string TrioSDrive { get; set; }
        public string UseSecurity { get; set; }

        public DateTime? MaxDate { get; set; }

        public byte? Violations { get; set; }

        public bool? defaultpermission { get; set; }

        public string ReleaseNumber { get; set; }
        public string ArchiveYear { get; set; }

        public int? MenuBackColor { get; set; }

        public bool? UseRegistryOnly { get; set; }

        public string FTPSiteAddress { get; set; }
        
        public string FTPSitePassword { get; set; }

       
        public string FTPSiteUser { get; set; }
        public string BulkMail1 { get; set; }
        public string BulkMail2 { get; set; }

        public string BulkMail3 { get; set; }
        public string BulkMail4 { get; set; }
       
        public string BulkMail5 { get; set; }

        public bool? UseMultipleTown { get; set; }

        public string SMTPServer { get; set; }


        public string SMTPUser { get; set; }

   
        public string SMTPPassword { get; set; }

        public string UserEmail { get; set; }

        public DateTime? UpdateDate { get; set; }

        public bool? UpdateNotification { get; set; }

        public bool? SetUpdateNotification { get; set; }

        public int? AuthorizationType { get; set; }

        public bool? UsePassive { get; set; }

        public bool? UseSSL { get; set; }

       
        public string FromName { get; set; }

        public string DLLVersion { get; set; }

        public bool? CentralPartyCheck { get; set; }

        public int? ArchiveMenuBackColor { get; set; }

        public int? ServerPort { get; set; }
    }
}
