﻿namespace SharedApplication.SystemSettings.Models
{
    public class UserPermission
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public string ModuleName { get; set; }
        public int FunctionID { get; set; }
        public string Permission { get; set; }
        public bool? MovedToClientSettings { get; set; }
    }
}