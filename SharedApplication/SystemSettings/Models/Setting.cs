﻿namespace SharedApplication.SystemSettings.Models
{
    public partial class Setting
    {
        public int ID { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public string SettingType { get; set; }
        public string Owner { get; set; }
        public string OwnerType { get; set; }
    }
}