﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.SystemSettings.Models
{
    public partial class Archive
    {
        public int ID { get; set; }

        public string Description { get; set; }

        public string ArchiveType { get; set; }

        public int? ArchiveID { get; set; }

        public DateTime? CreationDate { get; set; }
    }
}
