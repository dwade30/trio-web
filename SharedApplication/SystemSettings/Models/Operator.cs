﻿namespace SharedApplication.SystemSettings.Models
{
    public partial class Operator
    {
        public int ID { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Level { get; set; }

        public short? PlateGroup { get; set; }

        public short? StickerGroup { get; set; }

        public short? MVR3Group { get; set; }

        public int? SecurityID { get; set; }

        public string Password { get; set; }

        public bool? LoggedIn { get; set; }

        public string DeptDiv { get; set; }

        public bool? MovedToClientSettings { get; set; }
    }
}