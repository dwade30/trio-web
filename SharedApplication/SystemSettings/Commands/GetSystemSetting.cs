﻿using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Models;

namespace SharedApplication.SystemSettings
{
    public class GetSystemSettingValue : Command<string>
    {
        public string SettingName { get; }
        public string SettingType { get; }
        public string Owner { get; }
        public SettingOwnerType OwnerType { get; }

        public GetSystemSettingValue(string settingName, string settingType, string owner, SettingOwnerType ownerType)
        {
            SettingName = settingName;
            SettingType = settingType;
            Owner = owner;
            OwnerType = ownerType;
        }
    }
}