﻿using SharedApplication.Enums;
using SharedApplication.Messaging;

namespace SharedApplication.SystemSettings.Commands
{
    public class SaveSystemSetting : Command
    {
        public string SettingName { get; }
        public string SettingType { get; }
        public string Owner { get; }
        public SettingOwnerType OwnerType { get; }
        public string Value { get; }

        public SaveSystemSetting(string name, string settingType, string owner, SettingOwnerType ownerType,
            string value)
        {
            SettingName = name;
            SettingType = settingType;
            Owner = owner;
            OwnerType = ownerType;
            Value = value;
        }
    }
}