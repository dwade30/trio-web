﻿using System;

namespace SharedApplication
{
    public interface IEditableRepository<TEntity,TIdType> : IRepository<TEntity, TIdType> where TEntity : class, IIdentifiableEntity<TIdType>
    {
        void Add(TEntity entity);
        void Remove(TIdType id);
    }

    public interface IIdentifiableEntity<TIdType>
    {//needed if doing things like creating a new object or accessing the id in a generic repository
        TIdType Id { get; set; }
    }
}