﻿namespace SharedApplication.CashReceipts
{
    public enum PaymentMethod
    {
		None = 0,
        Cash = 1,
        Check = 2,
        Credit = 3
    }
}