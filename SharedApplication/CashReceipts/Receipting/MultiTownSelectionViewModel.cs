﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MultiTownSelectionViewModel : IMultiTownSelectionViewModel
	{
		private IRegionalTownService regionalTownService;
		private IEnumerable<Region> regions = new List<Region>();
		
		public MultiTownSelectionViewModel(IRegionalTownService regionalTownService)
		{
			this.regionalTownService = regionalTownService;

			regions = regionalTownService.RegionalTowns();
		}

		public IEnumerable<Region> Regions()
		{
			return regions;
		}

		public int SelectedTownId { get; set; }
	}
}
