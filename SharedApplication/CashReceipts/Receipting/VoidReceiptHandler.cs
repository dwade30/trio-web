﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
	public class VoidReceiptHandler : CommandHandler<VoidReceipt>
	{
		private IModalView<IVoidReceiptViewModel> voidReceiptView;

		public VoidReceiptHandler(IModalView<IVoidReceiptViewModel> view)
		{
			voidReceiptView = view;
		}
		protected override void Handle(VoidReceipt command)
		{
			var transactionGroup = new CRTransactionGroup() { ReceiptIdentifier = Guid.NewGuid(), TellerId = command.TellerId, TransactionDate = DateTime.Now };
			voidReceiptView.ViewModel.TransactionGroup = transactionGroup;
			voidReceiptView.ViewModel.SetReceiptToVoid(command.ReceiptNumber);
			voidReceiptView.ViewModel.SetCorrelationId(command.CorrelationId);
			voidReceiptView.ShowModal();
		}
	}
}
