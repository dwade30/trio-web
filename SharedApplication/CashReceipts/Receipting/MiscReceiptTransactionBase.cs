﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MiscReceiptTransactionBase : TransactionBase
	{
		public MiscReceiptTransaction TransactionDetails { get; set; }
		public string TransactionTypeDescription { get; set; }
	}
}
