﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class AccountsReceivableTransactionBase : TransactionBase
	{
		public AccountsReceivableTransaction TransactionDetails { get; set; }
		public string Comment { get; set; }
		public int AccountNumber { get; set; }
		public DateTime RecordedTransactionDate { get; set; }
		public string TransactionTypeDescription { get; set; }
		public string Code { get; set; }
		public bool AffectCashDrawer { get; set; }
		public bool AffectCash { get; set; }
		public string BudgetaryAccountNumber { get; set; }
		public string DefaultCashAccount { get; set; }
		public string EPmtAccountID { get; set; }
        public string OverrideInterestAccount { get; set; }
        public string OverrideTaxAccount { get; set; }

	}
}
