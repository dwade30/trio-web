﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.CashReceipts.Receipting
{
	public class EditMiscReceiptTransaction : SharedApplication.Messaging.Command, MakeTransaction
	{
		public Guid CorrelationId { get; set; }
		public MiscReceiptTransaction Transaction { get; set; }
	}
}
