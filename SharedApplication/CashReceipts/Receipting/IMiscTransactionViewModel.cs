﻿using System;
using System.Collections.Generic;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData.Models;

namespace SharedApplication.CashReceipts
{
    public interface IMiscTransactionViewModel
    {
		void Cancel();
		void SetCorrelationId(Guid correlationId);
		void SetReceiptTypeInfo(int receiptType, int townKey, bool forceUpdate = false);
		ReceiptType ReceiptTypeInfo();
		IEnumerable<Region> Regions();
		bool ShowPercentage();
		bool ShowAccountEntry();
		bool ShowLine(int line);
		int TownKey { get; set;}
		event EventHandler ReceiptTypeChanged;
		bool IsBudgetaryActive();
		string LineTitle(int line);
		decimal LineDefaultAmount(int line);
		bool LinePercentage(int line);
		bool LineYear(int line);
		bool ReferenceVisible();
		bool ControlVisible(int index);
		string GetGLAccountDescription(string account);
		bool IsRegionalTown();
		void SetMiscReceiptTransaction(MiscReceiptTransaction transaction);
		MiscReceiptTransaction Transaction { get; set; }
		void UpdateControlItem(string name, string description, string value);
		void UpdateAmountItem(string name, string description, decimal amount, decimal percentageAmount = 0);
		bool IsValidPercentageTotal();
		bool IsZeroAmountReceipt();
		bool IsValidAccount(string account);
		void CompleteTransaction();
		bool VerifyPercentageTotal();
		bool IsExistingTransaction { get; set; }
		string GetControlItemValue(string name);
		decimal GetAmountItemValue(string name);

    }
}