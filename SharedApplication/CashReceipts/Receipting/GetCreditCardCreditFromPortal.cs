﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class GetCreditCardCreditFromPortal : Command<PaymentPortalCreditResponse>
    {
        public bool InTestMode { get; }
        public decimal TransactionAmount { get; }
        public string AccountId { get; }

        public GetCreditCardCreditFromPortal(string accountId, decimal transactionAmount, bool inTestMode)
        {
            AccountId = accountId;
            TransactionAmount = transactionAmount;
            InTestMode = inTestMode;
        }
    }
}