﻿using System.Collections.Generic;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
    public interface IReceiptTypesService
    {
        IEnumerable<ReceiptType> RealEstateTypes { get; }
        IEnumerable<ReceiptType> PersonalPropertyTypes { get; }
        IEnumerable<ReceiptType> AccountsReceivableTypes { get; }
        IEnumerable<ReceiptType> ClerkTypes { get; }
        IEnumerable<ReceiptType> UtilityTypes { get; }
        IEnumerable<ReceiptType> MotorVehicleTypes { get; }
        IEnumerable<ReceiptType> MosesTypes { get; }
        IEnumerable<ReceiptType> WaterTypes { get; }
        IEnumerable<ReceiptType> SewerTypes { get; }
        ReceiptType GetReceiptTypeByCode(int typeCode);
        int GetReceiptTypeCodeFromTransaction(TransactionBase transaction);
        ReceiptType GetReceiptTypeFromTransaction(TransactionBase transaction);
        IEnumerable<(string Title, string Account)> GetAccountsForReceiptType(int typeCode);
        IEnumerable<ReceiptType> ReceiptTypes { get; }
        IEnumerable<(string Title, string Account)> GetAccountsForTransaction(TransactionBase transaction);
        bool IsSystemDefinedType(int receiptTypeCode);
        string GetMerchantIdForTransaction(TransactionBase transaction);
    }
}