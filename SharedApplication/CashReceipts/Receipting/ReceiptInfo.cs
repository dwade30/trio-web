﻿using System;

namespace SharedApplication.CashReceipts.Receipting
{
    public class ReceiptInfo
    {
        public int Id { get; set; }
        public int ReceiptNumber { get; set; }
        public bool IsCurrentYear { get; set; }
        public DateTime ReceiptDate { get; set; }
        public string TellerID { get; set; }
        public string PaidBy { get; set; }
    }
}