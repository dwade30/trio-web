﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;

namespace SharedApplication.CashReceipts.Receipting
{
	public interface IMultiTownSelectionViewModel
	{
		IEnumerable<Region> Regions();
		int SelectedTownId { get; set; }
	}
}
