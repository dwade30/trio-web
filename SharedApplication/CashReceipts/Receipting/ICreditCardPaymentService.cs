﻿namespace SharedApplication.CashReceipts.Receipting
{
    public interface ICreditCardPaymentService
    {
        CreditTransactionFeeRequestInfo GetTransactionFee(decimal transactionAmount,string transactionId,string merchantId);
        string AuthorizePayment(decimal creditCardAmount);
        string ResponseURL { get; set; }
        string RawResponse { get; set; }
        PaymentPortalCreditResponse GiveCredit(decimal creditAmount, string transactionId,string merchantId);
        string GetEffectiveMerchantId(string merchantId);
    }
}