﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MotorVehicleTransactionBase : TransactionBase
	{
		public string TransactionTypeDescription { get; set; }
	}
}
