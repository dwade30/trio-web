﻿using System.Collections;
using System.Collections.Generic;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
    public class EmptyReceiptTypesService : IReceiptTypesService
    {
        public IEnumerable<ReceiptType> RealEstateTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> PersonalPropertyTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> AccountsReceivableTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> ClerkTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> UtilityTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> MotorVehicleTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> MosesTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> WaterTypes { get; } = new List<ReceiptType>();
        public IEnumerable<ReceiptType> SewerTypes { get; } = new List<ReceiptType>();
        public ReceiptType GetReceiptTypeByCode(int typeCode)
        {
            return null;
        }

        public int GetReceiptTypeCodeFromTransaction(TransactionBase transaction)
        {
            return 0;
        }

        public ReceiptType GetReceiptTypeFromTransaction(TransactionBase transaction)
        {
            return null;
        }

        public IEnumerable<(string Title, string Account)> GetAccountsForReceiptType(int typeCode)
        {
            return new List<(string Title, string Account)>();
        }

        public IEnumerable<ReceiptType> ReceiptTypes { get; }
        public IEnumerable<(string Title, string Account)> GetAccountsForTransaction(TransactionBase transaction)
        {
            return new List<(string Title, string Account)>();
        }

        public bool IsSystemDefinedType(int receiptTypeCode)
        {
            return false;
        }

        public string GetMerchantIdForTransaction(TransactionBase transaction)
        {
	        return "";
        }

	}
}