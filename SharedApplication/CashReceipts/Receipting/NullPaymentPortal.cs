﻿namespace SharedApplication.CashReceipts.Receipting
{
    public class NullPaymentPortal : ICreditCardPaymentService
    {
        public CreditTransactionFeeRequestInfo GetTransactionFee(decimal transactionAmount,string transactionId,string merchantId)
        {
            return new CreditTransactionFeeRequestInfo();
        }

        public string AuthorizePayment(decimal creditCardAmount)
        {
            return "";
        }

        public string ResponseURL { get; set; }
        public string RawResponse { get; set; }
        public PaymentPortalCreditResponse GiveCredit(decimal creditAmount,string transactionId, string merchantId)
        {
            return null;
        }

        public string GetEffectiveMerchantId(string merchantId)
        {
            return "";
        }
    }
}