﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MakeMosesTransaction : SharedApplication.Messaging.Command, MakeTransaction
	{
		public Guid CorrelationId { get; set; }

	}
}
