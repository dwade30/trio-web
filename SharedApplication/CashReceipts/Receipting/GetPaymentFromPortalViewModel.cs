﻿namespace SharedApplication.CashReceipts.Receipting
{
    /// <summary>
    /// Informe specific implementation
    /// if another provider is added, add a new implementation and let dependency injection and a factory choose the correct implementation
    /// </summary>
    public class GetPaymentFromPortalViewModel : IGetPaymentFromPortalViewModel
    {
        public string AccountId { get; set; } = "";
        public decimal TransactionAmount { get; set; } = 0;
        public bool IsTest { get; set; } = false;
        public CreditTransactionType TransactionType { get; set; } = CreditTransactionType.Sale;
        public decimal TransactionFee { get; set; } = 0;
        public bool Cancelled { get; set; } = true;
        public string OrderId { get; set; } = "";
        private string baseUrl = @"https://epayment.informe.org/xtrio/";
        private string baseDevUrl = @"https://epaydev.informe.org/xtrio/";
        public string SaleUrl
        {
            get
            {
                var baseSaleUrl = IsTest ? baseDevUrl + "sale" : baseUrl + "sale";
                return baseSaleUrl + @"?payor=" + Token;// + @"&purl=" + ResponseURL + responsePage;
               // return baseSaleUrl + @"?payor=" + Token + @"&purl=" + ResponseURL + responsePage;
            }
        }

        public string CreditUrl
        {

            get
            {
                var baseCreditUrl = IsTest ? baseDevUrl + "credit" : baseUrl + "credit";
                return baseCreditUrl + @"?payor=" + Token;
            }
        }

        public string Token { get; set; } = "";
        public string ResponseURL { get; set; } = "";
        public bool ShowAccountId { get; set; } = false;

        private bool inDevMode = true;
        
        private string responsePage = "paymentportalresponse.html";
        public PaymentPortalResponse PortalResponse { get; private set; } = new PaymentPortalResponse();
        
    }
}