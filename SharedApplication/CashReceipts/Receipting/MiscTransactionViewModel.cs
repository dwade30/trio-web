﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MiscTransactionViewModel : IMiscTransactionViewModel
	{
		private Guid correlationID;
		private EventPublisher eventPublisher;
		private ReceiptTypeSearchCriteria SearchCriteria { get; } = new ReceiptTypeSearchCriteria();
		private ReceiptType receiptTypeInfo = new ReceiptType();
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
		private IEnumerable<Region> regions = new List<Region>();
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private IGlobalActiveModuleSettings activeModuleSettings;
		private IAccountValidationService accountValidationService;
		private IAccountTitleService accountTitleService;
		private IRegionalTownService regionalTownService;
		private MiscReceiptTransaction transaction;

		public int TownKey
		{
			get => transaction.TownCode;
			set
			{
				if (value != transaction.TownCode)
				{
					transaction.TownCode = value;
					SearchCriteria.TownKey = value;
					GetReceiptTypeInfo();
				}
			}
		}

		public MiscTransactionViewModel(IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, EventPublisher eventPublisher, IGlobalActiveModuleSettings activeModuleSettings, IAccountValidationService accountValidationService, IAccountTitleService accountTitleService, IRegionalTownService regionalTownService, GlobalCashReceiptSettings globalCashReceiptSettings)   
		{
			this.receiptTypeQueryHandler = receiptTypeQueryHandler;
			this.eventPublisher = eventPublisher;
			this.activeModuleSettings = activeModuleSettings;
			this.accountValidationService = accountValidationService;
			this.accountTitleService = accountTitleService;
			this.regionalTownService = regionalTownService;
			this.globalCashReceiptSettings = globalCashReceiptSettings;

			regions = regionalTownService.RegionalTowns();
			this.transaction = new MiscReceiptTransaction();
		}

		public void Cancel()
		{
			eventPublisher.Publish(new TransactionCancelled(correlationID,null));
		}

		public void SetCorrelationId(Guid correlationID)
		{
			this.correlationID = correlationID;
		}

		public void SetMiscReceiptTransaction(MiscReceiptTransaction transaction)
		{
			this.transaction = transaction;
		}

		public void SetReceiptTypeInfo(int receiptType, int townKey, bool forceUpdate = false)
		{
			if (receiptType != transaction.TransactionTypeCode || townKey != transaction.TownCode || forceUpdate)
			{
				transaction.TransactionTypeCode = receiptType;
				transaction.TownCode = townKey;

				SearchCriteria.ReceiptType = receiptType;
				SearchCriteria.TownKey = townKey;
				GetReceiptTypeInfo();
			}
		}

		private void GetReceiptTypeInfo()
		{
			receiptTypeInfo = receiptTypeQueryHandler.ExecuteQuery(SearchCriteria).FirstOrDefault();
			if (ReceiptTypeChanged != null)
			{
				ReceiptTypeChanged(this, new EventArgs());
			}
		}

		public event EventHandler ReceiptTypeChanged;
		public event EventHandler CloseForm;

		public ReceiptType ReceiptTypeInfo()
		{
			return receiptTypeInfo;
		}

		public IEnumerable<Region> Regions()
		{
			return regions;
		}

		public bool ShowPercentage()
		{
			return (receiptTypeInfo.PercentageFee1 ?? false) || (receiptTypeInfo.PercentageFee2 ?? false) ||
			       (receiptTypeInfo.PercentageFee3 ?? false) || (receiptTypeInfo.PercentageFee4 ?? false) ||
			       (receiptTypeInfo.PercentageFee5 ?? false) || (receiptTypeInfo.PercentageFee6 ?? false);
		}

		public bool ShowAccountEntry()
		{
			return receiptTypeInfo.Account1 == "M I" || receiptTypeInfo.Account2 == "M I" ||
			       receiptTypeInfo.Account3 == "M I" || receiptTypeInfo.Account4 == "M I" ||
			       receiptTypeInfo.Account5 == "M I" || receiptTypeInfo.Account6 == "M I";
		}

		public bool IsBudgetaryActive()
		{
			return activeModuleSettings.BudgetaryIsActive;
		}

		public bool ShowLine(int line)
		{
			switch (line)
			{
				case 1:
					return receiptTypeInfo.Account1 != null && receiptTypeInfo.Account1.Trim() != "" && receiptTypeInfo.Title1.Trim() != "" && accountValidationService.AccountValidate(receiptTypeInfo.Account1);
				case 2:
					return receiptTypeInfo.Account2 != null && receiptTypeInfo.Account2.Trim() != "" && receiptTypeInfo.Title2.Trim() != "" && accountValidationService.AccountValidate(receiptTypeInfo.Account2);
				case 3:
					return receiptTypeInfo.Account3 != null && receiptTypeInfo.Account3.Trim() != "" && receiptTypeInfo.Title3.Trim() != "" && accountValidationService.AccountValidate(receiptTypeInfo.Account3);
				case 4:
					return receiptTypeInfo.Account4 != null && receiptTypeInfo.Account4.Trim() != "" && receiptTypeInfo.Title4.Trim() != "" && accountValidationService.AccountValidate(receiptTypeInfo.Account4);
				case 5:
					return receiptTypeInfo.Account5 != null && receiptTypeInfo.Account5.Trim() != "" && receiptTypeInfo.Title5.Trim() != "" && accountValidationService.AccountValidate(receiptTypeInfo.Account5);
				case 6:
					return receiptTypeInfo.Account6 != null && receiptTypeInfo.Account6.Trim() != "" && receiptTypeInfo.Title6.Trim() != "" && accountValidationService.AccountValidate(receiptTypeInfo.Account6);
				default:
					return false;
			}
		}

		public string LineTitle(int line)
		{
			switch (line)
			{
				case 1:
					return receiptTypeInfo.Title1?.Trim() ?? ""; 
				case 2:
					return receiptTypeInfo.Title2?.Trim() ?? ""; 
				case 3:
					return receiptTypeInfo.Title3?.Trim() ?? ""; 
				case 4:
					return receiptTypeInfo.Title4?.Trim() ?? "";
				case 5:
					return receiptTypeInfo.Title5?.Trim() ?? ""; 
				case 6:
					return receiptTypeInfo.Title6?.Trim() ?? "";
				default:
					return "";
			}
		}

		public decimal LineDefaultAmount(int line)
		{
			switch (line)
			{
				case 1:
					return (decimal)(receiptTypeInfo.DefaultAmount1 ?? 0);
				case 2:
					return (decimal)(receiptTypeInfo.DefaultAmount2 ?? 0);
				case 3:
					return (decimal)(receiptTypeInfo.DefaultAmount3 ?? 0);
				case 4:
					return (decimal)(receiptTypeInfo.DefaultAmount4 ?? 0);
				case 5:
					return (decimal)(receiptTypeInfo.DefaultAmount5 ?? 0);
				case 6:
					return (decimal)(receiptTypeInfo.DefaultAmount6 ?? 0);
				default:
					return 0;
			}
		}

		public bool LinePercentage(int line)
		{
			switch (line)
			{
				case 1:
					return receiptTypeInfo.PercentageFee1 ?? false;
				case 2:
					return receiptTypeInfo.PercentageFee2 ?? false;
				case 3:
					return receiptTypeInfo.PercentageFee3 ?? false;
				case 4:
					return receiptTypeInfo.PercentageFee4 ?? false;
				case 5:
					return receiptTypeInfo.PercentageFee5 ?? false;
				case 6:
					return receiptTypeInfo.PercentageFee6 ?? false;
				default:
					return false;
			}
		}

		public bool LineYear(int line)
		{
			switch (line)
			{
				case 1:
					return receiptTypeInfo.Year1 ?? false;
				case 2:
					return receiptTypeInfo.Year2 ?? false;
				case 3:
					return receiptTypeInfo.Year3 ?? false;
				case 4:
					return receiptTypeInfo.Year4 ?? false;
				case 5:
					return receiptTypeInfo.Year5 ?? false;
				case 6:
					return receiptTypeInfo.Year6 ?? false;
				default:
					return false;
			}
		}

		public bool ReferenceVisible()
		{
			return receiptTypeInfo.Reference.Trim() != "";
		}

		public bool ControlVisible(int index)
		{
			switch (index)
			{
				case 1:
					return (receiptTypeInfo.Control1?.Trim() ?? "") != "";
				case 2:
					return (receiptTypeInfo.Control2?.Trim() ?? "") != "";
				case 3:
					return (receiptTypeInfo.Control3?.Trim() ?? "") != "";
				default:
					return false;
			}
		}

		public string GetGLAccountDescription(string account)
		{
			if (account.Trim() != "" && activeModuleSettings.BudgetaryIsActive)
			{
				return accountTitleService.ReturnAccountDescription(account);
			}
			else
			{
				return "";
			}
		}

		public bool IsRegionalTown()
		{
			return regionalTownService.IsRegionalTown();
		}

		public void UpdateControlItem(string name, string description, string value)
		{
			var item = transaction.Controls.FirstOrDefault(x => x.Name == name);
			if (item != null)
			{
				item.Description = description;
				item.Value = value;
			}
			else
			{
				var newItem = new ControlItem();
				newItem.Name = name;
				newItem.Description = description;
				newItem.Value = value;
				transaction.Controls.Add(newItem);
			}
		}

		public string GetControlItemValue(string name)
		{
			var item = transaction.Controls.FirstOrDefault(x => x.Name == name);
			if (item != null)
			{
				return item.Value;
			}

			return "";
		}

		public decimal GetAmountItemValue(string name)
		{
			var item = transaction.TransactionAmounts.FirstOrDefault(x => x.Name == name);
			if (item != null)
			{
				if (item.PercentageAmount != 0)
				{
					return item.PercentageAmount;
				}
				else
				{
					return item.Total;
				}
			}

			return 0;
		}

		private decimal CalculatePercentageAmount(decimal percentage)
		{
			return Math.Round((percentage / 100) * transaction.PercentageAmount, 2, MidpointRounding.AwayFromZero);
		}

		public void UpdateAmountItem(string name, string description, decimal amount, decimal percentageAmount = 0)
		{
			var item = transaction.TransactionAmounts.FirstOrDefault(x => x.Name == name);
			if (item != null)
			{
				item.Description = description;
				if (percentageAmount == 0)
				{
					item.Total = amount;
					item.PercentageAmount = percentageAmount;
				}
				else
				{
					item.PercentageAmount = percentageAmount;
					item.Total = CalculatePercentageAmount(percentageAmount);
				}
			}
			else
			{
				var newItem = new MiscReceiptAmountItem();
				newItem.Name = name;
				newItem.Description = description;
				if (percentageAmount == 0)
				{
					newItem.Total = amount;
					newItem.PercentageAmount = percentageAmount;
				}
				else
				{
					newItem.PercentageAmount = percentageAmount;
					newItem.Total = CalculatePercentageAmount(percentageAmount);
				}
				transaction.AddMiscReceiptTransactionAmount(newItem);
			}
		}

		private decimal GetPercentTotal()
		{
			return transaction.TransactionAmounts.Select(x => x.PercentageAmount).DefaultIfEmpty(0).Sum();
		}

		private decimal GetAmountTotal()
		{
			return transaction.TransactionAmounts.Select(x => x.Total).DefaultIfEmpty(0).Sum();
		}

		private decimal GetPercentAmountTotal()
		{
			return transaction.TransactionAmounts.Where(y => y.PercentageAmount != 0).Select(x => x.Total).DefaultIfEmpty(0).Sum();
		}

		public bool IsExistingTransaction { get; set; }
		

		public bool VerifyPercentageTotal()
		{
			try
			{
				decimal usedPercentageAmount = 0;

				if (ShowPercentage())
				{
					if (GetPercentTotal() != 100m)
					{
						return false;
					}
					else
					{
						usedPercentageAmount = GetPercentAmountTotal();
					}

					if (transaction.PercentageAmount != usedPercentageAmount)
					{
						usedPercentageAmount = transaction.PercentageAmount - usedPercentageAmount;
						for (int line = 1; line <= 6; line++)
						{
							if (LinePercentage(line))
							{
								var amount = GetAmount(line)?.Total ?? 0;
								if (amount + usedPercentageAmount < 0)
								{
									usedPercentageAmount = usedPercentageAmount + amount;
									amount = 0;
								}
								else
								{
									usedPercentageAmount = 0;
								}

								UpdateAmountItem("Amount" + line, LineTitle(line), amount);

								if (usedPercentageAmount == 0)
								{
									break;
								}
							}
						}
					}
				}

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}

		public bool IsValidPercentageTotal()
		{
			if (ShowPercentage())
			{
				return GetPercentTotal() == 100m;
			}
			else
			{
				return true;
			}
		}

		private TransactionAmountItem GetAmount(int line)
		{
			return transaction.TransactionAmounts.FirstOrDefault(x => x.Name == "Amount" + line);
		}

		public bool IsZeroAmountReceipt()
		{
			if (GetAmountTotal() != 0 || GetPercentAmountTotal() != 0 && transaction.PercentageAmount != 0)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public bool IsValidAccount(string account)
		{
			return accountValidationService.AccountValidate(account);
		}

		public void CompleteTransaction()
		{
			eventPublisher.Publish(new TransactionCompleted
			{
				CorrelationId = correlationID,
				CompletedTransactions = ConvertTransactionToMiscReceiptTransactionBase(transaction)
			});
		}

		private IEnumerable<MiscReceiptTransactionBase> ConvertTransactionToMiscReceiptTransactionBase(MiscReceiptTransaction transaction)
		{
			return new List<MiscReceiptTransactionBase>
			{
				transaction.ToTransactionBase()
			};
		}


		public MiscReceiptTransaction Transaction { get => transaction; set => transaction = value; }
	}

	
}
