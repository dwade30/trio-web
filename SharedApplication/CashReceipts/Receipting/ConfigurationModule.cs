﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace SharedApplication.CashReceipts.Receipting
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<MiscTransactionViewModel>().As<IMiscTransactionViewModel>();
            builder.RegisterType<ReceiptTypesService>().AsSelf();
            RegisterReceiptTypeService(ref builder);
        }

        protected void RegisterReceiptTypeService(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var activeModules = f.Resolve<IGlobalActiveModuleSettings>();
                if (activeModules.CashReceiptingIsActive)
                {
                    return f.Resolve<ReceiptTypesService>();                    
                }
                IReceiptTypesService emptyService = new EmptyReceiptTypesService();
                return emptyService;
            }).As<IReceiptTypesService>();
        }
	}
}
