﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MakeMosesTransactionHandler : CommandHandler<MakeMosesTransaction>
	{
		private IModalView<IMultiTownSelectionViewModel> multiTownSelectionView;
		private DataEnvironmentSettings environmentSettings;
		private CommandDispatcher commandDispatcher;
		private EventPublisher eventPublisher;
		private IQueryHandler<MosesTypesQuery, IEnumerable<MOSESType>> mosesTypeQueryHandler;
		private IEnumerable<MOSESType> mosesTypes;
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;

		public MakeMosesTransactionHandler(IModalView<IMultiTownSelectionViewModel> view, DataEnvironmentSettings environmentSettings, CommandDispatcher commandDispatcher, EventPublisher eventPublisher, IQueryHandler<MosesTypesQuery, IEnumerable<MOSESType>> mosesTypeQueryHandler, IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler)
		{
			multiTownSelectionView = view;
			this.environmentSettings = environmentSettings;
			this.commandDispatcher = commandDispatcher;
			this.eventPublisher = eventPublisher;
			this.mosesTypeQueryHandler = mosesTypeQueryHandler;
			this.receiptTypeQueryHandler = receiptTypeQueryHandler;

		}

		protected override void Handle(MakeMosesTransaction command)
		{
			try
			{
				var filePath = commandDispatcher.Send(new UploadFile
				{
					DialogTitle = "Select file to import from"
				}).Result;

				if (File.Exists(filePath))
				{
					var mosesReceipts = ParseMosesReceipt(filePath);
					var townKey = 0;

					if ((mosesReceipts?.Count() ?? 0) > 0)
					{
						if (environmentSettings.IsMultiTown)
						{
							multiTownSelectionView.ShowModal();
							townKey = multiTownSelectionView.ViewModel.SelectedTownId;
						}
					}

					mosesTypes = mosesTypeQueryHandler.ExecuteQuery(new MosesTypesQuery());
					var transactions = new List<MiscReceiptTransaction>();

					foreach (var receipt in mosesReceipts)
					{
						var receiptTypeCode = mosesTypes.FirstOrDefault(x => x.MOSESTYPE == receipt.AuthItemCode.ToIntegerValue())?.CRType ?? 0;
						if (receiptTypeCode == 0)
						{
							receiptTypeCode = 190;
						}
						var type = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
						{
							ReceiptType = receiptTypeCode,
							TownKey = townKey
						}).FirstOrDefault();

						if (type != null)
						{
							MiscReceiptTransaction transaction = new MiscReceiptTransaction();
							transaction.CorrelationIdentifier = command.CorrelationId;
							transaction.TransactionTypeCode = receiptTypeCode;
							transaction.TransactionTypeDescription = type.TypeTitle;
							transaction.CollectionCode = receipt.AuthItemCode;
							transaction.RecordKey = receipt.AuthItemCode.ToIntegerValue();
							transaction.TownCode = townKey;
							transaction.Id = Guid.NewGuid();
							transaction.Reference = receipt.DocumentNumber;
							transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
							{
								Name = "Amount1",
								Description = "Excise Tax",
								Total = receipt.ExciseTax,
								PercentageAmount = 0
							});

							transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
							{
								Name = "Amount2",
								Description = "State Paid",
								Total = receipt.StateFee,
								PercentageAmount = 0
							});
							transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
							{
								Name = "Amount3",
								Description = "Agent Fee",
								Total = receipt.AgentFee,
								PercentageAmount = 0
							});
							transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
							{
								Name = "Amount4",
								Description = "Sales Tax",
								Total = receipt.SalesTax,
								PercentageAmount = 0
							});

							transaction.Controls.Add(new ControlItem
							{
								Name = "Control1",
								Description = type.Control1,
								Value = receipt.ReceiptNumber.ToString()
							});
							transaction.Controls.Add(new ControlItem
							{
								Name = "Control2",
								Description = type.Control2,
								Value = receipt.RegistrationNumber
							});
							var control3 = (receipt.InventorySerialNumber + " " + receipt.InventorySerialNumber2).Trim();
							transaction.Controls.Add(new ControlItem
							{
								Name = "Control3",
								Description = type.Control3,
								Value = control3
							});

							transaction.GlAccount = "";
							transaction.AffectCashDrawer = true;
							transaction.AffectCash = true;
							transaction.Quantity = 1;
							transaction.PaidBy = receipt.CustomerName;
							transaction.PercentageAmount = 0;
							transaction.Comment = "";

							transactions.Add(transaction);
						}
					}

					if (transactions.Count > 0)
					{
						eventPublisher.Publish(new TransactionCompleted
						{
							CorrelationId = command.CorrelationId,
							CompletedTransactions = transactions.ToTransactionBases()
						});
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		private IEnumerable<MosesReceipt> ParseMosesReceipt(string strFilename)
		{
			StreamReader ts = null;
			try
			{
				string txtLine;
				int strMaxPos;
				int strCurPos;
				int intCT = 0;
				bool boolFirstPass;
				bool boolFullLine = false;
				var result = new List<MosesReceipt>();

				if (strFilename != "")
				{
					ts = File.OpenText(strFilename);
					txtLine = ts.ReadLine();
					strMaxPos = txtLine.Length;
					boolFirstPass = true;
					while (!ts.EndOfStream || boolFirstPass || boolFullLine)
					{
						var receipt = new MosesReceipt();

						intCT += 1;
						boolFirstPass = false;
						// Receipt Number
                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.ReceiptNumber = Convert.ToInt32(Math.Round(Convert.ToDecimal(txtLine.Substring(0, strCurPos))));
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos- (strCurPos + 1));
						// Trans Date
						strMaxPos = txtLine.Length;
                        strCurPos = txtLine.IndexOf(",", 4);
						if (txtLine.Substring(0, strCurPos).Trim() != "")
						{
							receipt.TransactionDate = txtLine.Substring(0, strCurPos).Trim();
						}
						else
						{
							receipt.TransactionDate = Convert.ToString(DateTime.Today);
						}
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// TransType
						strMaxPos = txtLine.Length;
                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.TransactionType = txtLine.Substring(0, strCurPos).Trim();
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Athority Item Code
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",", 4);
						receipt.AuthItemCode = txtLine.Substring(0, strCurPos).Trim();
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Customer Name
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.CustomerName = txtLine.Substring(0, strCurPos).Trim();
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Document Number
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",");
						receipt.DocumentNumber = txtLine.Substring(0, strCurPos).Trim();
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Registration number for Registration
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.RegistrationNumber = txtLine.Substring(0, strCurPos).Trim();
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Inventory Item Name
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.InventoryItemName = txtLine.Substring(0, strCurPos);
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Inventory Serial Number
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",");
						receipt.InventorySerialNumber = txtLine.Substring(0, strCurPos).Trim();
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Inventory Serial Number2
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",");
						receipt.InventorySerialNumber2 = txtLine.Substring(0, strCurPos);
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Blank Field
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",");
						receipt.Blank1 = txtLine.Substring(0, strCurPos);
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// ClerkID
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",");
						receipt.ClerkId = txtLine.Substring(0, strCurPos);
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// State Fee
						strMaxPos = txtLine.Length;
						strCurPos = txtLine.IndexOf(",");
						receipt.StateFee = Convert.ToDecimal(txtLine.Substring(0, strCurPos));
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Agent Fee
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.AgentFee = Convert.ToDecimal(txtLine.Substring(0, strCurPos));
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Excise Tax
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.ExciseTax = Convert.ToDecimal(txtLine.Substring(0, strCurPos));
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Sales Tax
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.SalesTax = strCurPos != 0 ? Convert.ToDecimal(Convert.ToDouble(txtLine.Substring(0, strCurPos))) : 0;
						txtLine = txtLine.Substring( strCurPos + 1, strMaxPos - (strCurPos + 1));
						// Payment Type
						strMaxPos = txtLine.Length;
						                        strCurPos = txtLine.IndexOf(",", 4);
						receipt.PaymentType =txtLine.Substring(0, strMaxPos).Trim();

						boolFullLine = false;
						if (!ts.EndOfStream)
						{
							txtLine = ts.ReadLine();
							strMaxPos = txtLine.Length;
							if (txtLine != "")
							{
								boolFullLine = true;
							}
						}

						result.Add(receipt);
					}
				}

				return result;
			}
			catch (Exception ex)
			{
				return null;
			}
			finally
			{
				if (ts != null)
				{
					ts.Close();
				}
			}
		}
	}
}
