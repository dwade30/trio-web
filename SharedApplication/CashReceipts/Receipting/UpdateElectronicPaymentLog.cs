﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class UpdateElectronicPaymentLog : Command
    {
        public PaymentPortalResponse PaymentResponse { get; }
        public int LogId { get; }

        public UpdateElectronicPaymentLog(PaymentPortalResponse paymentResponse, int logId)
        {
            PaymentResponse = paymentResponse;
            LogId = logId;
        }
    }
}