﻿using System;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;

namespace SharedApplication.CashReceipts.Receipting
{
	public class PayportService
        : ICreditCardPaymentService
    {
		//private string _merchantId = "";
        private bool _isTesting = false;
        private string baseUrl = @"https://epayment.informe.org/xtrio/";
        private string baseDevUrl = @"https://epaydev.informe.org/xtrio/";
        private string testMerchantId = "Harris_PayPort";
        private string returnUrl = @"";
        private string responsePage = "paymentportalresponse.html";
        private string BaseHostUrl
        {
            get { return _isTesting ? baseDevUrl : baseUrl; }
        }

        public string ResponseURL { get; set; } = "";
        public string RawResponse { get; set; }


        public PayportService( bool isTesting)
        {

            _isTesting = isTesting;

        }

        public CreditTransactionFeeRequestInfo GetTransactionFee(decimal transactionAmount,string transactionId, string merchantId)
		{
			var ret = new CreditTransactionFeeRequestInfo();

			try
            {
                var merchantIdToUse = _isTesting ? testMerchantId : merchantId;
				var tUri = new Uri(BaseHostUrl + @"getPayportCharge");
				var contentType = "application/x-www-form-urlencoded";
				var requestString = "TransactionAmount=" + transactionAmount.ToString("0.00") + "&MerchantID=" + merchantIdToUse+"&OrderID=" + transactionId + @"&purl=" + ResponseURL + responsePage; ;
				var resp = SendRequest(tUri, requestString, contentType, "POST");
                RawResponse = resp;
				ret = ParseTransactionFee(resp);
			}
			catch (Exception ex)
			{
                //TODO log error
                RawResponse = ex.Message;
                throw;
            }

			return ret;
		}

        public PaymentPortalCreditResponse GiveCredit(decimal creditAmount,string transactionId, string merchantId)
        {
			var ret = new PaymentPortalCreditResponse();
            ret.ReferenceNumber = transactionId;
            try
            {
                var merchantIdToUse = _isTesting ? testMerchantId : merchantId;
                var testString = _isTesting ? "&TestType=full" : "";
                var tUri = new Uri(BaseHostUrl + @"credit");
                var paymentMechanism = "credit_card";
                var contentType = "application/x-www-form-urlencoded";
				var requestString = "PaymentMechanism=" + paymentMechanism + "&TransactionAmount=" + creditAmount.ToString("0.00") + "&TransactionID=" + transactionId + testString  ;
                var resp = SendRequest(tUri, requestString, contentType, "POST");
                RawResponse = resp;
                var parsedResponse = ParseVoidResponse(resp);
                parsedResponse.ReferenceNumber = transactionId;
                return parsedResponse;
            }
			catch (Exception ex)
            {
                RawResponse = ex.Message;
                throw;
            }

            return ret;
        }

        public string GetEffectiveMerchantId(string merchantId)
        {
            return _isTesting ? testMerchantId : merchantId;
        }

        public string AuthorizePayment(decimal creditCardAmount)
        {
            var ret = "";
           // var merchantIdToUse = _isTesting ? testMerchantId : merchantId;
            //try
            //{
            //    var tUri = new Uri(_baseHostUrl + "xxx");
            //    var contentType = "application/x-www-form-urlencoded";
            //    var requestString = "var1=" + creditCardAmount.ToString("0.00") + "&MerchantID=" + _merchantId;
            //    var resp = SendRequest(tUri, requestString, contentType, "POST");
            //    var code = ParseAuthCode(resp);
            //    ret = code;
            //}
            //catch (Exception ex)
            //{
            //    //TODO log error

            //    throw;
            //}

            return ret;
        }

		private String SendRequest(Uri url, String dataString, String contentType, String method)
		{
			var response = "";

            try
            {
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

                var req = System.Net.WebRequest.Create(url);
                req.ContentType = contentType;
                req.Method = method;
                var dataBytes = Encoding.UTF8.GetBytes(dataString);
                req.ContentLength = dataBytes.Length;

                using (var stream = req.GetRequestStream())
                {
                    stream.Write(dataBytes, 0, dataBytes.Length);
                }

                var wr = (HttpWebResponse) req.GetResponse();
                if (wr.StatusCode == HttpStatusCode.OK)
                {
                    using (var rs = wr.GetResponseStream())
                    {
                        var reader = new StreamReader(rs);
                        response = reader.ReadToEnd();
                        reader.Close();
                    }
                }
            }
            catch (WebException wex)
            {
                var returnedContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();
                return returnedContent;
            }
			catch(Exception ex)
            {
                //response = ex.Message;
				var xxx = ex.Message;
            }

			return response;
		}

		private CreditTransactionFeeRequestInfo ParseTransactionFee(String input)
		{
			var fee = "";
            var token = "";
			try
			{
				if (input == "")
				{
					// logMessage = "Empty Response";
				}
				else
				{
					var t = new XmlDocument();
					t.LoadXml(input);
					var tElemList = t.GetElementsByTagName("PayportCharge");
					if (tElemList != null)
					{
						if (tElemList.Count > 0)
						{
							if (tElemList[0].InnerXml != "")
							{
								// logMessage = "Success";
								fee = tElemList[0].InnerXml;
							}
							else
							{
								// logMessage = "List[0] = ''";
							}
						}
					}

                    tElemList = t.GetElementsByTagName("PayportToken");
                    if (tElemList != null)
                    {
                        if (tElemList.Count > 0)
                        {
                            token = tElemList[0].InnerXml;
                        }
                    }
                }
			}
			catch (XmlException xex)
			{
				// logMessage = "1: " + xex.Message;
			}
			catch (Exception ex)
			{
				// logMessage = "2: " + ex.Message;
			}
            return new CreditTransactionFeeRequestInfo()
            {
                Fee = fee.ToDecimalValue(),
                Token = token
            };
        }

		private String ParseAuthCode(String input)
		{
			var parsed = "";

			try
			{
				if (input == "")
				{
					// logMessage = "Empty Response";
				}
				else
				{
					var t = new XmlDocument();
					t.LoadXml(input);
					var tElemList = t.GetElementsByTagName("xxx");
					if (tElemList != null)
					{
						if (tElemList.Count > 0)
						{
							if (tElemList[0].InnerXml != "")
							{
								// logMessage = "Success";
								parsed = tElemList[0].InnerXml;
							}
							else
							{
								// logMessage = "List[0] = ''";
							}
						}
					}
				}
			}
			catch (XmlException xex)
			{
				// logMessage = "1: " + xex.Message;
			}
			catch (Exception ex)
			{
				// logMessage = "2: " + ex.Message;
			}

			return parsed;
		}

        private PaymentPortalCreditResponse ParseVoidResponse(string input)
        {
            var ret = new PaymentPortalCreditResponse();
            ret.ResponseData = input;
            try
            {
                if (!input.IsNullOrWhiteSpace())
                {
                    var t = new XmlDocument();
                    t.LoadXml(input);
                    var tElemList = t.GetElementsByTagName("ResultCode");
                    if (tElemList != null)
                    {
                        if (tElemList.Count > 0)
                        {
                            if (tElemList[0].InnerXml != "")
                            {
                                if (tElemList[0].InnerXml != "0")
                                {
                                    ret.Success = true;
                                }
                                else
                                {
                                    tElemList = t.GetElementsByTagName("ErrorDescription");
                                    if (tElemList.Count > 0)
                                    {
                                        if (tElemList[0].InnerXml != "")
                                        {
                                            ret.ErrorDescription = tElemList[0].InnerXml;
                                        }
                                    }
                                }
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }
            catch (XmlException xex)
            {

            }
            catch(Exception ex)
            {

            }
            return ret;

        }
	}
}
