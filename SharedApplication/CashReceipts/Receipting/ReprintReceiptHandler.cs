﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class ReprintReceiptHandler : CommandHandler<ReprintReceipt>
    {
        private IView<IReprintReceiptViewModel> reprintView;
        public ReprintReceiptHandler(IView<IReprintReceiptViewModel> reprintView)
        {
            this.reprintView = reprintView;
        }
        protected override void Handle(ReprintReceipt command)
        {
            reprintView.ViewModel.LoadReceipt(command.ReceiptId);
            reprintView.Show();
        }
    }
}