﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class AddElectronicPaymentLog : Command<int>
    {
        public ElectronicPaymentLog PaymentLog { get; }

        public AddElectronicPaymentLog(ElectronicPaymentLog paymentLog)
        {
            PaymentLog = paymentLog;
        }
    }
}