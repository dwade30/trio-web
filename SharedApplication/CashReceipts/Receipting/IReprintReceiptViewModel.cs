﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CashReceipts.Receipting
{
    public interface IReprintReceiptViewModel
    {
        int ReceiptId { get; }
        int ReceiptNumber { get; }
        void LoadReceipt(int receiptId);
        void ShowPaymentDetail(Guid transactionId);
        string PaidBy { get; }
        int PaidById { get; }
        CRTransactionGroup TransactionGroup { get; }
        decimal RemainingDue { get; }
        decimal ConvenienceFee { get; }
        decimal TotalAmount { get; }
        decimal TotalReceiptAmount { get; }
        decimal TotalPaid { get; }

        decimal TotalCashPaid { get; }

        decimal TotalCheckPaid { get; }

        decimal TotalCreditPaid { get; }

        decimal ChangeDue { get; }
        IEnumerable<ReceiptPayment> Payments { get; }
        (bool Success, string Message, string Title) PrintReceipt();

    }
}