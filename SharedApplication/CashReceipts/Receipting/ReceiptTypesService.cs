﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.AccountsReceivable;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.Budgetary.Models;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
    public class ReceiptTypesService : IReceiptTypesService
    {
        private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
        private IEnumerable<ReceiptType> receiptTypes;
        private DataEnvironmentSettings environmentSettings;
        private IQueryHandler<MosesTypesQuery, IEnumerable<MOSESType>> mosesQueryHandler;
        private IEnumerable<MOSESType> mosesTypes;
        private IAccountsReceivableContext arContext;
        private IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings;
        public ReceiptTypesService(IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, DataEnvironmentSettings environmentSettings, IQueryHandler<MosesTypesQuery, IEnumerable<MOSESType>> mosesQueryHandler, IAccountsReceivableContext arContext, IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings)
        {
            this.receiptTypeQueryHandler = receiptTypeQueryHandler;
            this.environmentSettings = environmentSettings;
            this.mosesQueryHandler = mosesQueryHandler;
            this.arContext = arContext;
            this.globalBudgetaryAccountSettings = globalBudgetaryAccountSettings;
            receiptTypes = this.receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
                {TownKey = environmentSettings.IsMultiTown ? 1 : 0}).OrderBy(x => x.TypeCode);
        }

        public bool IsSystemDefinedType(int receiptTypeCode)
        {
	        var type = receiptTypes.FirstOrDefault(x => x.TypeCode == receiptTypeCode);

	        if (type == null)
	        {
		        return false;
	        }
	        else
	        {
		        return type.IsSystemDefinedType();
	        }
        }

        public IEnumerable<ReceiptType> RealEstateTypes
        {
            get => receiptTypes.Where(t => t.IsRealEstateType()).ToList();
        }

        public IEnumerable<ReceiptType> PersonalPropertyTypes
        {
            get => receiptTypes.Where(t => t.IsPersonalPropertyType()).ToList();
        }

        public IEnumerable<ReceiptType> AccountsReceivableTypes
        {
            get => receiptTypes.Where(t => t.IsAccountsReceivableType()).ToList();
        }

        public IEnumerable<ReceiptType> ClerkTypes
        {
            get => receiptTypes.Where(t => t.IsClerkType()).ToList();
        }

        public IEnumerable<ReceiptType> UtilityTypes
        {
            get => receiptTypes.Where(t => t.IsUtilityType()).ToList();
        }

        public IEnumerable<ReceiptType> MotorVehicleTypes
        {
            get => receiptTypes.Where(t => t.IsMotorVehicleType()).ToList();
        }

        public IEnumerable<ReceiptType> MosesTypes
        {
            get
            {
                if (mosesTypes == null)
                {
                    mosesTypes = mosesQueryHandler.ExecuteQuery(new MosesTypesQuery());
                }

                var uniqueRTypes = mosesTypes.Select(m => m.CRType).Distinct().ToList();
                return receiptTypes.Where(r =>
                    r.TypeCode == 190 || uniqueRTypes.Contains(r.TypeCode.GetValueOrDefault()));
            }
        }

        public IEnumerable<ReceiptType> WaterTypes
        {
            get => receiptTypes.Where(t => t.IsWaterType()).ToList();
        }

        public IEnumerable<ReceiptType> SewerTypes
        {
            get => receiptTypes.Where(t => t.IsSewerType()).ToList();
        }

        public ReceiptType GetReceiptTypeByCode(int typeCode)
        {
            return receiptTypes.FirstOrDefault(r => r.TypeCode == typeCode);
        }



        public int GetReceiptTypeCodeFromTransaction(TransactionBase transaction)
        {
            switch (transaction)
            {
                case ClerkTransaction clerkTransaction:
                    return 800;
                case ClerkVitalTransaction vitalTransaction:
                    return GetClerkReceiptType(vitalTransaction.TransactionTypeCode);
                case PropertyTaxBatchTransaction propertyTaxBatchTransaction:
                case PropertyTaxTransaction propertyTaxTransaction:
                    return GetPropertyTaxReceiptType(transaction.TransactionTypeCode);
                default:
                    return transaction.TransactionTypeCode.ToIntegerValue();
            }
        }

        public ReceiptType GetReceiptTypeFromTransaction(TransactionBase transaction)
        {
            var code = GetReceiptTypeCodeFromTransaction(transaction);
            return GetReceiptTypeByCode(code);
        }

        public IEnumerable<(string Title, string Account)> GetAccountsForReceiptType(int typeCode)
        {
            var receiptType = GetReceiptTypeByCode(typeCode);
            if (receiptType == null)
            {
                return new List<(string Title, string Account)>();
            }

            var accounts = new List<(string Title, string Account)>()
            {
                (Title: receiptType.Title1, Account: receiptType.Account1),
                (Title: receiptType.Title2, Account: receiptType.Account2),
                (Title: receiptType.Title3, Account: receiptType.Account3),
                (Title: receiptType.Title4, Account: receiptType.Account4),
                (Title: receiptType.Title5, Account: receiptType.Account5),
                (Title: receiptType.Title6, Account: receiptType.Account6)
            };

            return accounts;
        }

        public IEnumerable<ReceiptType> ReceiptTypes
        {
            get => receiptTypes;
        }

        public IEnumerable<(string Title, string Account)> GetAccountsForTransaction(TransactionBase transaction)
        {
            switch (transaction)
            {
                case ClerkTransaction clerkTransaction:
                    return GetAccountsForReceiptType(800).Select(a => (Title: a.Title, Account: a.Account));
                    //return GetAccountsForTransaction(clerkTransaction).Select(t => (Title: t.Title, Account: t.Account));
                    break;
                case ClerkVitalTransaction clerkVitalTransaction:
                    return GetAccountsForReceiptType(GetClerkReceiptType(transaction.TransactionTypeCode)).Select(a => (Title: a.Title, Account: a.Account));
                    break;
                case MiscReceiptTransactionBase miscReceiptTransaction:
                    return GetAccountsForTransaction(miscReceiptTransaction)
                        .Select(t => (Title: t.Title, Account: t.Account));
                    break;
                case AccountsReceivableTransactionBase accountsReceivableTransaction:
                    return GetAccountsForTransaction(accountsReceivableTransaction)
                        .Select(t => (Title: t.Title, Account: t.Account));
                    break;
                case UtilityBillingTransactionBase autilityBillingTransaction:
	                return GetAccountsForTransaction(autilityBillingTransaction)
		                .Select(t => (Title: t.Title, Account: t.Account));
	                break;
                case MotorVehicleTransactionBase motorVehicleTransaction:
	                return GetAccountsForTransaction(motorVehicleTransaction)
		                .Select(t => (Title: t.Title, Account: t.Account));
	                break;
                case PropertyTaxTransaction propertyTaxTransaction:
                    var propertyAccounts = GetAccountsForReceiptType(GetPropertyTaxReceiptType(transaction.TransactionTypeCode))
                        .Select(a => (Title: a.Title, Account: a.Account)).ToList();
                    if (globalBudgetaryAccountSettings.SuffixExistsInLedger())
                    {
                        var modifiedAccounts = new List<(string Title, string Account)>();
                        foreach (var account in propertyAccounts)
                        {
                            if (account.Account.StartsWith("G"))
                            {
                                modifiedAccounts.Add((Title: account.Title, Account: account.Account.Substring(0, account.Account.Length - 2) +
                                                  propertyTaxTransaction.YearBill.ToString().Substring(2,2)));
                            }
                            else
                            {
                                modifiedAccounts.Add((Title: account.Title, Account: account.Account));
                            }
                        }

                        return modifiedAccounts;
                    }
                    else
                    {
                        return propertyAccounts;
                    }
                    
                    break;
                case PropertyTaxBatchTransaction propertyTaxBatchTransaction:
                    var batchAccounts = GetAccountsForReceiptType(GetPropertyTaxReceiptType(transaction.TransactionTypeCode))
                        .Select(a => (Title: a.Title, Account: a.Account)).ToList();                    
                    return batchAccounts;
                    break;
			}
            return new List<(string Title, string Account)>();
        }

        private int GetClerkReceiptType(string typeCode)
        {
            switch (typeCode.Trim().ToLower())
            {
                case "dog":
                    return 800;
                    break;
                case "bir":
                    return 802;
                    break;
                case "dea":
                    return 801;
                    break;
                case "mar":
                    return 803;
                case "bur":
                    return 804;
                default:
                    return 98;
                    break;
            }
        }

        private int GetPropertyTaxReceiptType(string typeCode)
        {
            switch (typeCode.Trim().ToLower())
            {
                case "taretaxlien":
                    return 891;
                    break;
                case "retaxlien":
                    return 91;
                    break;
                case "retax":
                    return 90;
                    break;
                case "taretax":
                    return 890;
                    break;
                case "pptax":
                    return 92;
                    break;
                default:
                    return 90;
                    break;
            }
        }

        public IEnumerable<(string Title, string Account)> GetAccountsForTransaction(
            MiscReceiptTransactionBase transaction)
        {
            var accounts = GetAccountsForReceiptType(transaction.TransactionTypeCode.ToIntegerValue()).ToList();
            accounts.Where(a => a.Account == "M I").ToList().ForEach(a => a.Account = transaction.TransactionDetails.GlAccount);
            return accounts.Select(a => (Title: a.Title,Account: a.Account));
        }

        public IEnumerable<(string Title, string Account)> GetAccountsForTransaction(
	        UtilityBillingTransactionBase transaction)
        {
	        var accounts = GetAccountsForReceiptType(transaction.TransactionTypeCode.ToIntegerValue()).ToList();
	        return accounts.Select(a => (Title: a.Title, Account: a.Account));
        }

        public IEnumerable<(string Title, string Account)> GetAccountsForTransaction(
	        MotorVehicleTransactionBase transaction)
        {
	        var accounts = GetAccountsForReceiptType(transaction.TransactionTypeCode.ToIntegerValue()).ToList();
	        return accounts.Select(a => (Title: a.Title, Account: a.Account));
        }

		public IEnumerable<(string Title, string Account)> GetAccountsForTransaction(
            AccountsReceivableTransactionBase transaction)
        {
            var accounts = GetAccountsForReceiptType(97).ToList();
            var billCode = transaction.TransactionDetails.Bills.FirstOrDefault()?.BillType;
            if (billCode.GetValueOrDefault() == 0)
            {
                var customize = arContext.Customize.FirstOrDefault();
                if (customize != null)
                {
                    accounts[0] = (Title: accounts[0].Title, Account: customize.PrePayReceivableAccount);
                }
            }
            else
            {
                var billType = arContext.DefaultBillTypes
                    .FirstOrDefault(d => d.TypeCode == billCode.GetValueOrDefault());
                if (billType != null)
                {
                    accounts[0] = (Title: accounts[0].Title, Account: billType.ReceivableAccount);
                    if (!string.IsNullOrWhiteSpace(billType.OverrideIntAccount))
                    {
                        accounts[1] = (Title: accounts[1].Title, Account: billType.OverrideIntAccount);
                    }

                    if (!string.IsNullOrWhiteSpace(billType.OverrideTaxAccount))
                    {
                        accounts[2] = (Title: accounts[2].Title, Account: billType.OverrideTaxAccount);
                    }
                }
            }

            return accounts.Select(a => (Title: a.Title, Account: a.Account));
        }

		public string GetMerchantIdForTransaction(TransactionBase transaction)
		{
			var code = GetReceiptTypeCodeFromTransaction(transaction);
			return GetMerchantIdForTransactionType(code);
		}

		public string GetMerchantIdForTransactionType(int typeCode)
		{
			string merchantId = "";
			var receiptType = GetReceiptTypeByCode(typeCode);
			if (receiptType != null)
			{
				merchantId = receiptType.EPmtAcctID ?? "";
			}

			return merchantId;
		}
    }
}