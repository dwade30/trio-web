﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;
using SharedApplication.UtilityBilling.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class UtilityBillingTransactionBase : TransactionBase
	{
		public UtilityBillingTransaction TransactionDetails { get; set; }
		public string Comment { get; set; }
		public int AccountNumber { get; set; }
		public DateTime RecordedTransactionDate { get; set; }
		public string TransactionTypeDescription { get; set; }
		public string Code { get; set; }
		public bool AffectCashDrawer { get; set; }
		public bool AffectCash { get; set; }
		public bool IsBatchPayment { get; set; } = false;
		public string BudgetaryAccountNumber { get; set; }
		public string DefaultCashAccount { get; set; }
		public string EPmtAccountID { get; set; }
	}
}
