﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Authorization;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class ReceiptService : IReceiptService
	{
		private CommandDispatcher commandDispatcher;
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private IBudgetaryAccountingService budgetaryAccountingService;
		private IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings;
		private IReceiptTypesService receiptTypesService;
		private DataEnvironmentSettings environmentSettings;
		private IUserPermissionSet currentPermissionSet;
		private Setting lastReceiptNumber;
		private ISettingService settingService;
        private IQueryHandler<CRReceiptQuery, Receipt> crReceiptQueryHandler;
        private ICashReceiptsContext crContext;
        private ICreditCardPaymentService creditCardPaymentService;
        public ReceiptService(CommandDispatcher commandDispatcher, GlobalCashReceiptSettings globalCashReceiptSettings, IBudgetaryAccountingService budgetaryAccountingService, IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings, IReceiptTypesService receiptTypesService, DataEnvironmentSettings environmentSettings, IUserPermissionSet currentPermissionSet, ISettingService settingService, ICreditCardPaymentService creditCardPaymentService, IQueryHandler<CRReceiptQuery, Receipt> crReceiptQueryHandler, ICashReceiptsContext crContext)
		{
			this.commandDispatcher = commandDispatcher;
			this.globalCashReceiptSettings = globalCashReceiptSettings;
			this.budgetaryAccountingService = budgetaryAccountingService;
			this.globalBudgetaryAccountSettings = globalBudgetaryAccountSettings;
			this.receiptTypesService = receiptTypesService;
			this.environmentSettings = environmentSettings;
			this.currentPermissionSet = currentPermissionSet;
			this.settingService = settingService;
            this.crReceiptQueryHandler = crReceiptQueryHandler;
            this.crContext = crContext;
            this.creditCardPaymentService = creditCardPaymentService;
            this.creditCardPaymentService.ResponseURL = globalCashReceiptSettings.PaymentResponseURL;
            lastReceiptNumber = settingService.GetSettingValue(SettingOwnerType.Machine,"LastReceiptGenerated");

			if (lastReceiptNumber == null)
			{
				lastReceiptNumber = new Setting
				{
					SettingName = "LastReceiptGenerated",
					SettingValue = "0"
                };
			}
		}

		public CRTransactionGroup TransactionGroup { get; set; }

        /// <summary>
        /// Loads a processed receipt for re-viewing
        /// </summary>
        /// <param name="receipt"></param>
        public void LoadFromReceipt(Receipt receipt)
        {
            if (receipt != null)
            {
                TransactionGroup = new CRTransactionGroup()
                {
                    PaidBy = receipt.PaidBy,
                    ReceiptIdentifier = receipt.ReceiptIdentifier.GetValueOrDefault(),
                    TransactionDate = receipt.ReceiptDate.GetValueOrDefault(),
                    TellerId = receipt.TellerID,
                    PaidById = receipt.PaidByPartyID.GetValueOrDefault()                    
                };
                
                TransactionGroup.AddTransactions(receipt.Archives.ToTransactions(receiptTypesService));
                if (receipt.CashAmount > 0)
                {
                    AddPayment(new ReceiptPayment()
                    {
                        Amount = receipt.CashAmount.GetValueOrDefault().ToDecimal(),
                        Id = Guid.NewGuid(),
                        PaymentType = PaymentMethod.Cash
                    });
                }

                foreach (var check in receipt.CheckMasters)
                {
                    AddPayment(new CheckReceiptPayment()
                    {
                        Amount = check.Amount.GetValueOrDefault().ToDecimal(),
                        Id = Guid.NewGuid(),
                        PaymentType = PaymentMethod.Check,
                        Reference = check.CheckNumber,
                        CheckNumber = check.CheckNumber,
                        EFT = check.EFT.GetValueOrDefault(),
                        BankId = check.BankId.GetValueOrDefault(),
                        EPaymentReferenceNumber = check.EPymtRefNumber
                    });
                }

                foreach (var creditCard in receipt.CreditCardMasters)
                {
                    var ccType = crContext.CreditCardTypes.FirstOrDefault(c =>
                        c.ID == creditCard.CCType.GetValueOrDefault());
                    var ccDescription = ccType?.Type ?? "";
                    AddPayment(new CreditCardReceiptPayment()
                    {
                        Amount = creditCard.Amount.GetValueOrDefault().ToDecimal(),
                        Id = Guid.NewGuid(),
                        PaymentType = PaymentMethod.Credit,
                        Reference = ccDescription,
                        ConvenienceFee = creditCard.ConvenienceFee.GetValueOrDefault(),
                        EPaymentReferenceNumber = creditCard.EPymtRefNumber,
                        AuthCode = creditCard.AuthCode,
                        EPaymentAccountId = creditCard.EPmtAcctID,
                        TypeId = creditCard.CCType.GetValueOrDefault(),
                        TypeDescription = ccDescription,
                        CCPaymentIdentifier = creditCard.CCPaymentIdentifier
                    });

                }
            }
        }
        public void LoadFromReceipt(int receiptId)
        {
            var receipt = GetReceipt(receiptId);
            LoadFromReceipt(receipt);
        }

        public Receipt GetReceipt(int id)
        {
            return crReceiptQueryHandler.ExecuteQuery(new CRReceiptQuery(id));
        }

        public int DefaultCloseout { get; set; } = 0;

        public decimal RemainingDue
		{
			get
			{
				var left = TotalAmount - TotalPaid;
				if (left != 0)
				{
					return left;
				}
				return 0;
			}
		}

		public decimal TotalAmount
		{
			get { return TransactionGroup.TotalAmount; }
		}

		public decimal TotalReceiptAmount
		{
			get { return TotalAmount + ConvenienceFee; }
		}

		public decimal TotalPaid
		{
			get { return payments.Sum(p => p.Amount); }
		}

		public decimal TotalCashPaid
		{
			get { return payments.Where(p => p.PaymentType == PaymentMethod.Cash).Sum(p => p.Amount); }
		}

		public decimal TotalCheckPaid
		{
			get { return payments.Where(p => p.PaymentType == PaymentMethod.Check).Sum(p => p.Amount); }
		}

		public decimal TotalCreditPaid
		{
			get { return payments.Where(p => p.PaymentType == PaymentMethod.Credit).Sum(p => p.Amount); }
		}

		public decimal ConvenienceFee
		{
			get
			{
				return payments.Where(p => p.PaymentType == PaymentMethod.Credit)
					.Select(x => (CreditCardReceiptPayment) x).Sum(a => a.ConvenienceFee);
			}
		}

		public decimal ChangeDue
		{
			get
			{
				var change = TotalPaid - TotalReceiptAmount;
				if (change > 0)
				{
					return change;
				}
				return 0;
			}
		}

		public bool IsReceiptPrinterSetUp()
		{
			return settingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName") != null;
		}

		private List<ReceiptPayment> payments = new List<ReceiptPayment>();
		public IEnumerable<ReceiptPayment> Payments
		{
			get => payments;
		}

		public int PaidById
		{
			get { return this.TransactionGroup.PaidById; }
			set { this.TransactionGroup.PaidById = value; }
		}

		public string PaidBy
		{
			get => TransactionGroup.PaidBy;
			set => TransactionGroup.PaidBy = value;
		}

		public void AddPayment(ReceiptPayment payment)
		{
			if (payment != null)
            {
                if (payment.PaymentType == PaymentMethod.Credit && globalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
                {
                    AddCreditPayment(payment);
                }
                else
                {
                    payments.Add(payment);
                }
            }
		}

		private void AddCreditPayment(ReceiptPayment payment)
		{
            //TODO PROCESS PAYMENT INTO payments IF NOT PAYPORT
           // if (globalCashReceiptSettings.ElectronicPaymentPortalType != ElectronicPaymentPortalOption.PayPort) return;

            var creditPayments = DistributeCreditPayment((CreditCardReceiptPayment)payment);
            if (globalCashReceiptSettings.ElectronicPaymentPortalType != ElectronicPaymentPortalOption.None)
            {
                GetConvenienceFee(creditPayments);
            }

            payments.AddRange(creditPayments);
        }

        public void AddExistingPayment(ReceiptPayment payment)
        {
			payments.Add(payment);
        }

		/// <summary>
		/// Because some towns can use separate merchant codes for specific transaction types
		/// we may need to distribute the credit card charge to different Merchant Ids
		/// This will create separate credit card payments for each merchant code
		/// </summary>
		private List<CreditCardReceiptPayment> DistributeCreditPayment(CreditCardReceiptPayment payment)
		{
			List <CreditCardReceiptPayment> creditCardPayments = new List<CreditCardReceiptPayment>();

			decimal newCardAmountRemaining = payment.Amount;
			decimal cardAmountRemaining = payments.Where(p => p.PaymentType == PaymentMethod.Credit).Sum(p => p.Amount);
			decimal checkAmountRemaining = payments.Where(p => p.PaymentType == PaymentMethod.Check).Sum(p => p.Amount);
			decimal cashAmountRemaining = payments.Where(p => p.PaymentType == PaymentMethod.Cash).Sum(p => p.Amount);

			decimal transactionCardPaidAmount = 0;
			decimal transactionNewCardPaidAmount = 0;
			decimal transactionCashPaidAmount = 0;
			decimal transactionCheckPaidAmount = 0;
			decimal transactionTotal = 0;

			string defaultMerchantId = GetDefaultMerchantId();

			var transactions = TransactionGroup.GetTransactions();

			foreach (var transaction in transactions.OrderBy(x => x.CashDrawerTotal))
			{
				transactionTotal = transaction.CashDrawerTotal;

				transactionCardPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref cardAmountRemaining);

				transactionNewCardPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref newCardAmountRemaining);

				transactionCashPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref cashAmountRemaining);

				transactionCheckPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref checkAmountRemaining);

				if (transactionNewCardPaidAmount != 0)
				{
					string typeMerchantId = receiptTypesService.GetMerchantIdForTransaction(transaction);
					if (String.IsNullOrEmpty(typeMerchantId))
					{
						typeMerchantId = defaultMerchantId;
					}

					CreditCardReceiptPayment ccpmt =
						creditCardPayments.FirstOrDefault(p => p.EPaymentAccountId == typeMerchantId);
					if (ccpmt == null)
					{
						ccpmt = new CreditCardReceiptPayment
						{
							Amount = transactionNewCardPaidAmount,
							TypeDescription = payment.TypeDescription,
							TypeId = payment.TypeId,
                            EPaymentAccountId = typeMerchantId
						};
						creditCardPayments.Add(ccpmt);
					}
					else
					{
						ccpmt.Amount += transactionNewCardPaidAmount;
					}
				}
            }
			return creditCardPayments;
		}

		private void GetConvenienceFee(List<CreditCardReceiptPayment> creditPayments)
		{
            if (globalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
            {
                foreach (var pmt in creditPayments)
                {
                    //TODO set up command and use dispatcher to send to proper payment system
                   // ICreditCardPaymentService ccService = new PayportService(pmt.EPaymentAccountId,globalCashReceiptSettings.EPaymentInTestMode);
                    var feeInfo = creditCardPaymentService.GetTransactionFee(pmt.Amount,pmt.CCPaymentIdentifier.ToString(),pmt.EPaymentAccountId);
                    //commandDispatcher.Send(new SendAlertMessage("Raw response " + creditCardPaymentService.RawResponse,
                    //    "Response"));
                    pmt.ConvenienceFee = feeInfo.Fee;
                    pmt.Token = feeInfo.Token;
                }
            }
        }

		private String GetDefaultMerchantId()
		{
			return globalCashReceiptSettings.EPaymentClientID;
		}

		public bool RemovePayment(Guid id)
		{
			try
			{
				var transactions = TransactionGroup.GetTransactions().Where(x => x.Id == id);

				foreach (var transaction in transactions)
				{
					string controlsDescription = "";

					switch (transaction)
					{
						case ClerkTransaction clerkTransaction:
							foreach (var control in clerkTransaction.Controls)
							{
								controlsDescription =
									controlsDescription + control.Description + ": " + control.Value + "  ";
							}
							commandDispatcher.Send(new AddCYAEntry
							{
								Description1 = "Deleted out an CK entry from the CR Summary grid",
								Description2 = "Name: " + clerkTransaction.PayerName,
								Description3 = "Reference: " + clerkTransaction.Reference + controlsDescription,
								Description4 = "Total Amount: " + clerkTransaction.Total.ToString("#,##0.00")
							});
							break;
						case ClerkVitalTransaction vitalTransaction:
							foreach (var control in vitalTransaction.Controls)
							{
								controlsDescription =
									controlsDescription + control.Description + ": " + control.Value + "  ";
							}
							commandDispatcher.Send(new AddCYAEntry
							{
								Description1 = "Deleted out an CK entry form the CR Summary grid",
								Description2 = "Name: " + vitalTransaction.PayerName,
								Description3 = "Reference: " + vitalTransaction.Reference + controlsDescription,
								Description4 = "Total Amount: " + vitalTransaction.Total.ToString("#,##0.00")
							});
							break;
						case MotorVehicleTransactionBase motorVehicleTransaction:
							foreach (var control in motorVehicleTransaction.Controls)
							{
								controlsDescription =
									controlsDescription + control.Description + ": " + control.Value + "  ";
							}
							commandDispatcher.Send(new AddCYAEntry
							{
								Description1 = "Deleted out an MV entry form the CR Summary grid",
								Description2 = "Owner: " + motorVehicleTransaction.PayerName,
								Description3 = "Reference: " + motorVehicleTransaction.Reference + controlsDescription,
								Description4 = "Total Amount: " + motorVehicleTransaction.Total.ToString("#,##0.00")
							});
							break;
						default:
							break;
					}
				}

				return payments.RemoveAll(p => p.Id == id) > 0;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
		}

		public bool RemoveTransaction(Guid id)
		{
			return TransactionGroup.RemoveTransaction(id);
		}

		public bool CanCashOut()
		{
			return TransactionGroup.GetTransactions().Any() && RemainingDue <= 0;
		}

        public (Receipt receipt, string errorMessage) VoidReceipt()
        {
            return CashOutByType(CashOutMethod.Void);
        }

        private (Receipt receipt,string errorMessage) CashOutByType(CashOutMethod cashoutType)
        {
            try
            {
                if (globalCashReceiptSettings.ElectronicPaymentPortalType != ElectronicPaymentPortalOption.None)
                {
                    switch (cashoutType)
                    {
                        case CashOutMethod.Void:
                            var voidReturn = GetCreditCardVoids();
							
                            if (!voidReturn.success)
                            {
                                return (receipt: null, voidReturn.errorMessage);
                            }
							break;
                        case CashOutMethod.CashOut:
                            if (!GetCreditCardPayments())
                            {
                                return (receipt: null, "");
                            }

                            break;
                    }
                }
                var receipt = SaveReceipt();
                if (receipt != null)
                {
                    if (FinalizeTransactions(receipt.Id))
                    {
                        commandDispatcher.Send(new RemoveCRTransactionGroup(TransactionGroup.ReceiptIdentifier));
                    }
                }

                lastReceiptNumber.SettingValue = receipt.ReceiptNumber.ToString();
                settingService.SaveSetting(SettingOwnerType.Machine, lastReceiptNumber.SettingName, lastReceiptNumber.SettingValue);

                if (globalCashReceiptSettings.CashDrawerConnected)
                {
                    OpenCashDrawer();
                }

                return (receipt: receipt,"");
			}
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

		public (Receipt receipt, string errorMessage) CashOut()
        {
           return CashOutByType(CashOutMethod.CashOut);
        }

        private bool GetCreditCardPayments()
        {
            var creditPayments = payments.Where(p => p.GetType() == typeof(CreditCardReceiptPayment));            
            foreach (CreditCardReceiptPayment creditPayment in creditPayments)
            {
                var logId = commandDispatcher.Send(new AddElectronicPaymentLog(new ElectronicPaymentLog()
                {
					Amount = creditPayment.Total - creditPayment.ConvenienceFee,
					Fee = creditPayment.ConvenienceFee,
					Success = false,
					StatusCode = "",
					ReferenceId = "",
					ResponseData = "",
					ErrorDescription = "",
					MerchantCode =  creditPayment.EPaymentAccountId,
					ElectronicPaymentIdentifier = creditPayment.CCPaymentIdentifier,
					InTestMode = globalCashReceiptSettings.EPaymentInTestMode,
					TransactionGroupId = TransactionGroup.ReceiptIdentifier,
					PaymentTime = DateTime.Now,
					TellerId = TransactionGroup.TellerId
                })).Result;
                var portalResponse = commandDispatcher.Send(new GetCreditCardPaymentFromPortal(creditPayment.EPaymentAccountId,
                    creditPayment.Amount, creditPayment.ConvenienceFee, globalCashReceiptSettings.EPaymentInTestMode,creditPayment.CCPaymentIdentifier.ToString(),creditPayment.Token,globalCashReceiptSettings.ShowPaymentPortalCodeOnPaymentScreen)).Result;
                LogResponse(portalResponse,logId);
                if (!portalResponse.Success)
                {
                    return false;
                }

                creditPayment.AuthCode = portalResponse.AuthorizationCode;
                creditPayment.EPaymentReferenceNumber = portalResponse.ReferenceNumber;
                creditPayment.CardExpiration = portalResponse.ExpirationDate;
            }

            return true;
        }

        private (bool success , string errorMessage) GetCreditCardVoids()
        {
           // var creditPayments =(IEnumerable<CreditCardReceiptPayment>) (payments.Where(p => p.GetType() == typeof(CreditCardReceiptPayment)));
            var creditPayments = payments.Where(p => p.GetType() == typeof(CreditCardReceiptPayment));
			foreach (CreditCardReceiptPayment creditPayment in creditPayments)
            {
                var logId = commandDispatcher.Send(new AddElectronicPaymentLog(new ElectronicPaymentLog()
                {
                    Amount = creditPayment.Total - creditPayment.ConvenienceFee,
                    Fee = creditPayment.ConvenienceFee,
                    Success = false,
                    StatusCode = "",
                    ReferenceId = creditPayment.EPaymentReferenceNumber,
                    ResponseData = "",
                    ErrorDescription = "",
                    MerchantCode = creditPayment.EPaymentAccountId,
                    ElectronicPaymentIdentifier = creditPayment.CCPaymentIdentifier,
                    InTestMode = globalCashReceiptSettings.EPaymentInTestMode,
                    TransactionGroupId = TransactionGroup.ReceiptIdentifier,
					TellerId = TransactionGroup.TellerId,
					PaymentTime = DateTime.Now
                })).Result;

				var portalResponse = creditCardPaymentService.GiveCredit(Math.Abs(creditPayment.Total),creditPayment.EPaymentReferenceNumber, creditPayment.EPaymentAccountId);
				
                LogResponse(portalResponse,  logId);
				if (!portalResponse.Success)
                {
                    return (success: false, errorMessage: "Reference " + creditPayment.EPaymentReferenceNumber + " error: " +  portalResponse.ErrorDescription);
                }

            }

            return (success: true, errorMessage: "");
        }

        public void PrintReceipt(int copies, Receipt receiptToPrint)
		{
            PrintReceipt(copies,receiptToPrint,false);
		}

        public void ReprintReceipt(Receipt receiptToPrint)
        {
            PrintReceipt(1,receiptToPrint,true);
        }

        private void PrintReceipt(int copies, Receipt receiptToPrint,bool isReprint)
        {            
            if (copies > 0)
            {
                TransactionGroup.GetTransactions().ToList().ForEach(x => x.Split = x.Split == 0 ? 1 : x.Split);
                commandDispatcher.Send(new PrintReceipt
                {
                    Copies = copies,
                    TransactionGroup = TransactionGroup,
                    Payments = payments,
                    ConvenienceFee = ConvenienceFee,
                    PaidBy = PaidBy,
                    Reprint = isReprint,
                    ReceiptNumber = receiptToPrint.ReceiptNumber ?? 0,
                    Change = (decimal)(receiptToPrint.Change ?? 0),
					ReceiptDate = receiptToPrint?.ReceiptDate ?? DateTime.Now
                });
            }
        }
		private void OpenCashDrawer()
		{
			var permission = currentPermissionSet.GetPermission("CR", (int) PermissionQueryFunctionId.OpenCashDrawer);

			if (permission == "F" || permission == "P")
			{
				commandDispatcher.Send(new OpenCashDrawer());
			}
		}

		private Receipt SaveReceipt()
		{
			var receipt = CreateReceipt();
			if (receipt != null)
			{
				if (commandDispatcher.Send(new CreateNewReceipt(receipt)).Result > 0)
				{
					return receipt;
				}
			}

			return null;
		}

		private Receipt CreateReceipt()
		{
			var receiptNumber = commandDispatcher.Send(new GetNewReceiptNumber()).Result;
			if (receiptNumber > 0)
			{
				var archives = CreateArchives();
				var creditCardMasters = CreateCreditCardMasters();
				var checkMasters = CreateCheckMasters();
				DateTime receiptDate = DateTime.Parse(DateTime.Now.ToString("MM/dd/yy hh:mm:ss"));

				var receipt = new Receipt()
				{
					CardAmount = TotalCreditPaid.ToDouble(),
					CashAmount = TotalCashPaid.ToDouble() - ChangeDue.ToDouble(),
					Change = ChangeDue.ToDouble(),
					CheckAmount = TotalCheckPaid.ToDouble(),
					ConvenienceFee = ConvenienceFee,
					EFT = false,
					IsEPayment = false,
					MultipleChecks = payments.Count(p => p.PaymentType == PaymentMethod.Check) > 1,
					MultipleCC = payments.Count(p => p.PaymentType == PaymentMethod.Credit) > 1,
					PaidBy = PaidBy,
					PaidByPartyID = PaidById,
					ReceiptIdentifier = TransactionGroup.ReceiptIdentifier,
					ReceiptNumber = receiptNumber,
					TellerID = TransactionGroup.TellerId,
					ReceiptDate = receiptDate,
					Archives = archives,
					CheckMasters = checkMasters,
					CreditCardMasters = creditCardMasters
				};

				return receipt;
			}

			return null;
		}

		private List<CreditCardMaster> CreateCreditCardMasters()
		{
			var result = new List<CreditCardMaster>();

			foreach (CreditCardReceiptPayment payment in payments.Where(x => x.PaymentType == PaymentMethod.Credit))
			{
				result.Add(new CreditCardMaster
				{
					Amount = payment.Amount.ToDouble(),
					CCType = payment.TypeId,
					EPymtRefNumber = payment.EPaymentReferenceNumber,
					AuthCode = payment.AuthCode,
					EPmtAcctID = creditCardPaymentService.GetEffectiveMerchantId(payment.EPaymentAccountId),
					ConvenienceFee = payment.ConvenienceFee,
                    CCPaymentIdentifier = payment.CCPaymentIdentifier
                    ,CryptCardExpiration =  payment.CardExpiration
				});
			}

			return result;
		}

		private List<CheckMaster> CreateCheckMasters()
		{
			var result = new List<CheckMaster>();

			foreach (CheckReceiptPayment payment in payments.Where(x => x.PaymentType == PaymentMethod.Check))
			{
				result.Add(new CheckMaster
				{
					Amount = payment.Amount.ToDouble(),
					BankId = payment.BankId,
					CheckNumber = payment.CheckNumber.ToString(),
					EPymtRefNumber = payment.EPaymentReferenceNumber,
					EFT = payment.EFT,
					Last4Digits = "",
					AccountType = "",
					OriginalReceiptKey = 0
				});
			}

			return result;
		}

		private List<Archive> CreateArchives()
		{
			var archives = new List<Archive>();
			decimal cardAmountRemaining = TotalCreditPaid;
			decimal checkAmountRemaining = TotalCheckPaid;
			decimal cashAmountRemaining = TotalCashPaid;

			decimal transactionCardPaidAmount = 0;
			decimal transactionCashPaidAmount = 0;
			decimal transactionCheckPaidAmount = 0;
			decimal transactionTotal = 0;

			var transactions = TransactionGroup.GetTransactions();

			foreach (var transaction in transactions.OrderBy(x => x.CashDrawerTotal))
			{
				transactionTotal = transaction.CashDrawerTotal;

				transactionCardPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref cardAmountRemaining);
				transactionCashPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref cashAmountRemaining);
				transactionCheckPaidAmount =
					CalculatePaymentForTransaction(ref transactionTotal, ref checkAmountRemaining);

				archives.Add(transaction.ToArchive(receiptTypesService, TransactionGroup.TellerId, transactionCardPaidAmount, transactionCashPaidAmount, transactionCheckPaidAmount, PaidBy));
			}

			foreach (var archive in archives)
			{
				archive.TownKey = archive.TownKey > 1 ? archive.TownKey : environmentSettings.IsMultiTown ? 1 : 0;
				archive.ReceiptIdentifier = TransactionGroup.ReceiptIdentifier;
                archive.DailyCloseOut = DefaultCloseout;
                archive.TellerCloseOut = DefaultCloseout;
				if (archive.CardPaidAmount != 0 && globalCashReceiptSettings.UseSeparateCreditCardGLAccount)
				{
					archive.SeperateCreditCardGLAccount = CalculateSeperateCreditCardGLAccount(archive);
				}
			}

			return archives;
		}

		private string CalculateSeperateCreditCardGLAccount(Archive archive)
		{
			string result = "";

			if (globalCashReceiptSettings.SeparateCreditCardGLAccount != "" )
			{
				if (archive.Account1 != "")
				{
					result = ReturnFormattedCreditCardAccount(archive.Account1);
                    return result;
                }
				if (archive.Account2 != "")
				{
					result = ReturnFormattedCreditCardAccount(archive.Account2);
                    return result;
                }
				if (archive.Account3 != "")
				{
					result = ReturnFormattedCreditCardAccount(archive.Account3);
                    return result;
                }
				if (archive.Account4 != "")
				{
					result = ReturnFormattedCreditCardAccount(archive.Account4);
                    return result;
                }
				if (archive.Account5 != "")
				{
					result = ReturnFormattedCreditCardAccount(archive.Account5);
                    return result;
                }
				if (archive.Account6 != "")
				{
					result = ReturnFormattedCreditCardAccount(archive.Account6);
                    return result;
                }
            }


            return "";
        }

		private string ReturnFormattedCreditCardAccount(string account)
		{
			if (!globalBudgetaryAccountSettings.SuffixExistsInLedger())
			{
				return "G " + budgetaryAccountingService.GetFundFromAccount(account).ToString().PadLeft(globalBudgetaryAccountSettings.LedgerFundLength(), '0') + "-" + globalCashReceiptSettings.SeparateCreditCardGLAccount;
			}
			else
			{
				return "G " + budgetaryAccountingService.GetFundFromAccount(account).ToString()
						   .PadLeft(globalBudgetaryAccountSettings.LedgerFundLength(), '0') + "-" +
					   globalCashReceiptSettings.SeparateCreditCardGLAccount + '-' + new string('0',
						   globalBudgetaryAccountSettings.LedgerSuffixLength());
			}
		}

		private decimal CalculatePaymentForTransaction(ref decimal TransactionTotal, ref decimal PaymentAmount)
		{
			decimal result = 0;

		    if (PaymentAmount != 0)
			{
				if (TransactionTotal > PaymentAmount || TransactionTotal < 0)
				{
					TransactionTotal = TransactionTotal - PaymentAmount;
					result = PaymentAmount;
					PaymentAmount = 0;
				}
				else
				{
					PaymentAmount = PaymentAmount - TransactionTotal;
					result = TransactionTotal;
					TransactionTotal = 0;
				}
			}

			return result;
		}

		private bool FinalizeTransactions(int receiptId)
		{
			var transactions = TransactionGroup.GetTransactions().GroupBy(t => t.GetType());

			foreach (var transactionGroup in transactions)
			{
				var command = GetFinalizeTransactionCommand(transactionGroup.ToList(), transactionGroup.Key, TransactionGroup.TellerId, receiptId);

				if (commandDispatcher.Send(command).Result)
				{

				}
				else
				{

				}
			}
			return true;
		}

		private Command<bool> GetFinalizeTransactionCommand(List<TransactionBase> transactions, Type type, string tellerId, int receiptId)
		{

			switch (type)
			{
				case Type t when t == typeof(ClerkTransaction):
					return new FinalizeTransactions<ClerkTransaction>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (ClerkTransaction)tran), TellerId = tellerId, ReceiptId = receiptId };

                case Type t when t == typeof(ClerkVitalTransaction):
					return new FinalizeTransactions<ClerkVitalTransaction>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (ClerkVitalTransaction)tran), TellerId = tellerId, ReceiptId = receiptId };
				
                case Type t when t == typeof(AccountsReceivableTransactionBase):
					return new FinalizeTransactions<AccountsReceivableTransactionBase>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (AccountsReceivableTransactionBase)tran), TellerId = tellerId, ReceiptId = receiptId };
				
                case Type t when t == typeof(UtilityBillingTransactionBase):
					return new FinalizeTransactions<UtilityBillingTransactionBase>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (UtilityBillingTransactionBase)tran), TellerId = tellerId, ReceiptId = receiptId };
				
                case Type t when t == typeof(MiscReceiptTransactionBase):
					return new FinalizeTransactions<MiscReceiptTransactionBase>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (MiscReceiptTransactionBase)tran), TellerId = tellerId, ReceiptId = receiptId };
				
                case Type t when t == typeof(PropertyTaxTransaction):
					return new FinalizeTransactions<PropertyTaxTransaction>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (PropertyTaxTransaction)tran), TellerId = tellerId, ReceiptId = receiptId };
                
                case Type t when t == typeof(PropertyTaxBatchTransaction):
                    return new FinalizeTransactions<PropertyTaxBatchTransaction>()
                    {
                        CorrelationIdentifier = TransactionGroup.ReceiptIdentifier,
                        Transactions = transactions.Select(tran => (PropertyTaxBatchTransaction) tran),
                        TellerId = tellerId, ReceiptId = receiptId
                    };
				
                case Type t when t == typeof(MotorVehicleTransactionBase):
					return new FinalizeTransactions<MotorVehicleTransactionBase>() { CorrelationIdentifier = TransactionGroup.ReceiptIdentifier, Transactions = transactions.Select(tran => (MotorVehicleTransactionBase)tran), TellerId = tellerId, ReceiptId = receiptId };

			}

			return null;
		}

		public bool ShouldPrintReceipt()
		{
			foreach (var transaction in TransactionGroup.GetTransactions())
			{
				if (receiptTypesService.GetReceiptTypeByCode(transaction.CashReceiptsReceiptTypeCode()).Print ?? false)
				{
					return true;
				}
			}

			return false;

		}

		public int CopiesToPrint()
		{
			var copies = 0;

			foreach (var transaction in TransactionGroup.GetTransactions())
			{
				var typeCopies = receiptTypesService.GetReceiptTypeByCode(transaction.CashReceiptsReceiptTypeCode()).Copies ?? 0;

				if (typeCopies > copies)
				{
					copies = typeCopies;
				}
			}

			return copies;
		}

        private void LogResponse(PaymentPortalResponse portalResponse, int logId)
        {
            commandDispatcher.Send(new UpdateElectronicPaymentLog(portalResponse,logId));
        }

        private void LogResponse(PaymentPortalCreditResponse portalResponse, int logId)
        {
            commandDispatcher.Send(new UpdateElectronicPaymentLog(new PaymentPortalResponse()
            {
				AuthorizationCode = portalResponse.AuthorizationCode,
				ErrorCode = portalResponse.ErrorCode,
				ErrorDescription = portalResponse.ErrorDescription,
				ReferenceNumber = portalResponse.ReferenceNumber,
				ResponseData = portalResponse.ResponseData,
				Success = portalResponse.Success
            }, logId));
        }
	}

    public enum CashOutMethod
    {
		CashOut = 0,
		Void = 1
    }
}
