﻿namespace SharedApplication.CashReceipts.Receipting
{
    public interface IGetPaymentFromPortalViewModel
    {
        string AccountId { get; set; }
        decimal TransactionAmount { get; set; }
        bool IsTest { get; set; }
        CreditTransactionType TransactionType { get; set; }
        decimal TransactionFee { get; set; }
        PaymentPortalResponse PortalResponse { get;  }
        bool Cancelled { get; set; }
        string OrderId { get; set; }
        string SaleUrl { get;  }
        string CreditUrl { get; }
        string Token { get; set; }
        string ResponseURL { get; set; }
        bool ShowAccountId { get; set; }
    }

    public enum CreditTransactionType
    {
        Sale = 0,
        Credit = 1
    }
}