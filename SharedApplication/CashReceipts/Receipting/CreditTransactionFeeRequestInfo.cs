﻿namespace SharedApplication.CashReceipts.Receipting
{
    public class CreditTransactionFeeRequestInfo
    {
        public decimal Fee { get; set; } = 0;
        public string Token { get; set; } = "";
    }
}