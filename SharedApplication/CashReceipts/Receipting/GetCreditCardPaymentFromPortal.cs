﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class GetCreditCardPaymentFromPortal : Command<PaymentPortalResponse>
    {
        public string AccountId { get; }
        public decimal TransactionAmount { get; }
        public decimal ConvenienceFee { get; }
        public bool InTestMode { get; }
        public string TransactionId { get; }
        public string Token { get; }
        public bool ShowAccountId { get; }
        public GetCreditCardPaymentFromPortal(string accountId,decimal transactionAmount, decimal convenienceFee,bool inTestMode,string transactionId,string token,bool showAccountId)
        {
            AccountId = accountId;
            TransactionAmount = transactionAmount;
            ConvenienceFee = convenienceFee;
            InTestMode = inTestMode;
            TransactionId = transactionId;
            Token = token;
            ShowAccountId = showAccountId;
        }
    }
}