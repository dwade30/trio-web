﻿namespace SharedApplication.CashReceipts.Receipting
{
    public class CheckReceiptPayment : ReceiptPayment
    {
        public CheckReceiptPayment()
        {
            this.PaymentType = PaymentMethod.Check;
        }

        public int BankId { get; set; } = 0;
        public bool EFT { get; set; } = false;
        public string BankDescription { get; set; } = "";
        public string EPaymentReferenceNumber { get; set; } = "";
		private string checkNumber = "";
        public string CheckNumber
        {
            get => checkNumber;
            set
            {
                checkNumber = value;
                Reference = checkNumber;
            }
        }
    }
}