﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
	public class EditMiscReceiptTransactionHandler : CommandHandler<EditMiscReceiptTransaction>
	{
		private IView<IMiscTransactionViewModel> miscReceiptEntryView;
		public EditMiscReceiptTransactionHandler(IView<IMiscTransactionViewModel> view)
		{
			miscReceiptEntryView = view;
		}
		protected override void Handle(EditMiscReceiptTransaction command)
		{
			miscReceiptEntryView.ViewModel.IsExistingTransaction = true;
			miscReceiptEntryView.ViewModel.SetMiscReceiptTransaction(command.Transaction);
			miscReceiptEntryView.ViewModel.SetCorrelationId(command.Transaction.CorrelationIdentifier);
			miscReceiptEntryView.ViewModel.SetReceiptTypeInfo(command.Transaction.TransactionTypeCode, command.Transaction.TownCode, true);
			miscReceiptEntryView.Show();
		}
	}
}
