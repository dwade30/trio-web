﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts.Receipting
{
	public interface IReceiptService
	{
		string PaidBy { get; set; }
		int PaidById { get; set; }
        int DefaultCloseout { get; set; }
		decimal RemainingDue { get; }
		decimal ConvenienceFee { get; }
		decimal TotalAmount { get; }
		decimal TotalReceiptAmount { get; }
		decimal TotalPaid { get; }
		decimal TotalCashPaid { get; }
		decimal TotalCheckPaid { get; }
		decimal TotalCreditPaid { get; }
		decimal ChangeDue { get; }
		IEnumerable<ReceiptPayment> Payments { get; }
		void AddPayment(ReceiptPayment payment);
        void AddExistingPayment(ReceiptPayment payment);
		bool RemovePayment(Guid id);
		bool RemoveTransaction(Guid id);
		bool CanCashOut();
		bool ShouldPrintReceipt();
		int CopiesToPrint();
        (Receipt receipt, string errorMessage) CashOut();
        (Receipt receipt, string errorMessage) VoidReceipt();
		bool IsReceiptPrinterSetUp();
		void PrintReceipt(int copies, Receipt receiptToPrint);
        void ReprintReceipt(Receipt receiptToPrint);
		CRTransactionGroup TransactionGroup { get; set; }
        Receipt GetReceipt(int id);
        void LoadFromReceipt(int receiptId);
        void LoadFromReceipt(Receipt receipt);
    }
}
