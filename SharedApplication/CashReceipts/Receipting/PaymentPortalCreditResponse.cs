﻿namespace SharedApplication.CashReceipts.Receipting
{
    public class PaymentPortalCreditResponse
    {
        public bool Success { get; set; } = false;
        public string AuthorizationCode { get; set; } = "";
        public string ReferenceNumber { get; set; } = "";
        public string ErrorCode { get; set; } = "";
        public string ErrorDescription { get; set; } = "";
        public string ResponseData { get; set; } = "";
    }
}