﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
	public class PrintReceipt : Command
	{
		public CRTransactionGroup TransactionGroup { get; set; }
		public int Copies { get; set; }
		public bool Reprint { get; set; } = false;
		public IEnumerable<ReceiptPayment> Payments { get; set; }
		public decimal ConvenienceFee { get; set; }
		public string PaidBy { get; set; }
		public int ReceiptNumber { get; set; }
		public decimal Change { get; set; }
		public DateTime ReceiptDate { get; set; } = DateTime.Now;
	}
}
