﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
	public class MiscReceiptTransactionHandler : CommandHandler<MakeMiscReceiptTransaction>
	{
		private IView<IMiscTransactionViewModel> miscReceiptEntryView;
		private DataEnvironmentSettings environmentSettings;

		public MiscReceiptTransactionHandler(IView<IMiscTransactionViewModel> view, DataEnvironmentSettings environmentSettings)  
		{
			miscReceiptEntryView = view;
			this.environmentSettings = environmentSettings;
		}
		protected override void Handle(MakeMiscReceiptTransaction command)
		{
			MiscReceiptTransaction transaction = new MiscReceiptTransaction();
			transaction.CorrelationIdentifier = command.CorrelationId;
			transaction.AffectCashDrawer = true;
			transaction.Quantity = 1;
            transaction.PaidBy = command.Name;
			miscReceiptEntryView.ViewModel.IsExistingTransaction = false;
			miscReceiptEntryView.ViewModel.SetMiscReceiptTransaction(transaction);
			miscReceiptEntryView.ViewModel.SetCorrelationId(command.CorrelationId);
			miscReceiptEntryView.ViewModel.SetReceiptTypeInfo(command.ReceiptType, environmentSettings.IsMultiTown ? 1 : 0, true);
			miscReceiptEntryView.Show();
		}
	}
}
