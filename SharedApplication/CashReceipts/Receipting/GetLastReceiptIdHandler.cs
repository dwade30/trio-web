﻿using System.Linq;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class GetLastReceiptIdHandler : CommandHandler<GetLastReceiptId,int>
    {
        private ICashReceiptsContext crContext;
        private ISettingService settingService;

        public GetLastReceiptIdHandler(ITrioContextInterfaceFactory factory, ISettingService settingService)
        {
            this.crContext = factory.GetCashReceiptsContextInterface();
            this.settingService = settingService;
        }
        protected override int Handle(GetLastReceiptId command)
        {
            var result = settingService.GetSettingValue(SettingOwnerType.Machine, "LastReceiptGenerated");

            if (result != null)
            {
                return crContext.Receipts.FirstOrDefault(r => r.ReceiptNumber == result.SettingValue.ToIntegerValue()).Id;
            }
            else
            {
                return 0;
            }
            
        }
    }
}