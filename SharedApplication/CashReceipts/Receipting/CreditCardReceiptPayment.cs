﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Receipting
{
	public class CreditCardReceiptPayment : ReceiptPayment
	{
		public CreditCardReceiptPayment()
		{
			this.PaymentType = PaymentMethod.Credit;
		}
		public int TypeId { get; set; } = 0;
		public string EPaymentReferenceNumber { get; set; } = "";
		public string AuthCode { get; set; } = "";
		public decimal ConvenienceFee { get; set; } = 0;
        public decimal OriginalAmount { get; set; } = 0;
		public string EPaymentAccountId { get; set; } = "";
		private string typeDescription = "";
        public string CardExpiration { get; set; } = "";
		public string TypeDescription
		{
			get => typeDescription;
			set
			{
				typeDescription = value;
				Reference = value;
			}
		}
        public Guid CCPaymentIdentifier { get; set; } = Guid.NewGuid();
        public string Token { get; set; } = "";
        public override decimal Total
        {
            get => Amount + ConvenienceFee;
        }
    }
}
