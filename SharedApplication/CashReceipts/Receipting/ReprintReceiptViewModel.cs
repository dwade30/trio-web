﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
    public class ReprintReceiptViewModel : IReprintReceiptViewModel
    {
        public int ReceiptId { get; }
        private IReceiptService receiptService;
        private CommandDispatcher commandDispatcher;
        private Receipt originalReceipt;
        private ISettingService settingService;
        public ReprintReceiptViewModel(IReceiptService receiptService, CommandDispatcher commandDispatcher,ISettingService settingService)
        {
            this.receiptService = receiptService;
            this.commandDispatcher = commandDispatcher;
            this.settingService = settingService;
        }

        public void LoadReceipt(int receiptId)
        {
            originalReceipt = receiptService.GetReceipt(receiptId);
            receiptService.LoadFromReceipt(originalReceipt);
        }

        public void ShowPaymentDetail(Guid transactionId)
        {
            var transaction = receiptService.TransactionGroup.GetTransactions().FirstOrDefault(t => t.Id == transactionId);
            if (transaction != null)
            {
                commandDispatcher.Send(new ShowReceiptTransactionDetail(transaction.ToReceiptDetailItem()));
            }
        }

        public string PaidBy
        {
            get { return receiptService.PaidBy; }
        }

        public int ReceiptNumber
        {
            get { return originalReceipt.ReceiptNumber ?? 0; }
        }

        public int PaidById
        {
            get { return receiptService.PaidById; }
        }

        public CRTransactionGroup TransactionGroup
        {
            get { return receiptService.TransactionGroup; }
        }

        public decimal RemainingDue => receiptService.RemainingDue;
        public decimal ConvenienceFee => receiptService.ConvenienceFee;
        public decimal TotalAmount => receiptService.TotalAmount;
        public decimal TotalReceiptAmount => receiptService.TotalReceiptAmount;
        public decimal TotalPaid => receiptService.TotalPaid;

        public decimal TotalCashPaid => receiptService.TotalCashPaid;

        public decimal TotalCheckPaid => receiptService.TotalCheckPaid;

        public decimal TotalCreditPaid => receiptService.TotalCreditPaid;

        public decimal ChangeDue => receiptService.ChangeDue;
        public IEnumerable<ReceiptPayment> Payments => receiptService.Payments;
        public (bool Success,string Message, string Title) PrintReceipt()
        {
            var cashReceiptPrinter = settingService.GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName");
            if (cashReceiptPrinter == null)
            {
                return (Success: false, Message: "You must set up a receipt printer before printing a receipt",Title: "No Receipt Printer");
            }
            receiptService.ReprintReceipt(originalReceipt);
            return (Success: true, Message: "", Title: "");
        }
    }
}