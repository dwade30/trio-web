﻿using System;

namespace SharedApplication.CashReceipts.Receipting
{
    public class ReceiptPayment
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public PaymentMethod PaymentType { get; set; } = PaymentMethod.Cash;
        public decimal Amount { get; set; } = 0;
        public string Reference { get; set; } = "";
        public virtual decimal Total
        {
            get => Amount;
        }
    }
}