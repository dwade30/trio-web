﻿using System;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    ///Might be provider specific implementation. Depends how different the providers work
    public class GetCreditCardPaymentFromPortalHandler : CommandHandler<GetCreditCardPaymentFromPortal,PaymentPortalResponse>
    {
        private IModalView<IGetPaymentFromPortalViewModel> paymentView;
        private GlobalCashReceiptSettings crSettings;
        public GetCreditCardPaymentFromPortalHandler(IModalView<IGetPaymentFromPortalViewModel> paymentView, GlobalCashReceiptSettings crSettings)
        {
            this.paymentView = paymentView;
            this.crSettings = crSettings;
        }
        protected override PaymentPortalResponse Handle(GetCreditCardPaymentFromPortal command)
        {
            paymentView.ViewModel.AccountId = command.AccountId;
            paymentView.ViewModel.TransactionAmount = command.TransactionAmount;
            paymentView.ViewModel.IsTest = command.InTestMode;
            paymentView.ViewModel.TransactionFee = command.ConvenienceFee;
            paymentView.ViewModel.Token = command.Token;
            paymentView.ViewModel.ResponseURL = crSettings.PaymentResponseURL;
            paymentView.ViewModel.ShowAccountId = command.ShowAccountId;
            paymentView.ViewModel.OrderId = command.TransactionId; // DateTime.Now.ToString("MMddyyhhmmtt" )+ command.TransactionId;
            paymentView.ShowModal();
            if (paymentView.ViewModel.Cancelled)
            {
                paymentView.ViewModel.PortalResponse.AuthorizationCode = "Cancelled";
            }
            return paymentView.ViewModel.PortalResponse;
        }
    }
}