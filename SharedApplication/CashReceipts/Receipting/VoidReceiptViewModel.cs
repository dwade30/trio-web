﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Receipting
{
	public class VoidReceiptViewModel : IVoidReceiptViewModel
	{
		private Guid correlationId;
		private Receipt receiptToVoid;

		private IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler;
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
		private IQueryHandler<CreditCardMasterSearchCriteria, IEnumerable<CreditCardMaster>> creditCardMasterQueryHandler;
		private IQueryHandler<CheckMasterSearchCriteria, IEnumerable<CheckMaster>> checkMasterQueryHandler;
		private IQueryHandler<CreditCardTypeSearchCriteria, IEnumerable<CreditCardType>> creditCardTypeQueryHandler;
		private GlobalCashReceiptSettings globalCashReceiptSettings;
		private IBudgetaryAccountingService budgetaryAccountingService;
		private IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings;
		private DataEnvironmentSettings environmentSettings;
		private CommandDispatcher commandDispatcher;
		private IReceiptTypesService receiptTypesService;
		private IReceiptService receiptService;
		public event EventHandler<(string message,string alertType)> AlertAdded;

		public VoidReceiptViewModel(IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler,
			IQueryHandler<CreditCardMasterSearchCriteria, IEnumerable<CreditCardMaster>> creditCardMasterQueryHandler,
			IQueryHandler<CheckMasterSearchCriteria, IEnumerable<CheckMaster>> checkMasterQueryHandler,
			IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler,
			IQueryHandler<CreditCardTypeSearchCriteria, IEnumerable<CreditCardType>> creditCardTypeQueryHandler,
			GlobalCashReceiptSettings globalCashReceiptSettings, IBudgetaryAccountingService budgetaryAccountingService, IGlobalBudgetaryAccountSettings globalBudgetaryAccountSettings,
			DataEnvironmentSettings environmentSettings, CommandDispatcher commandDispatcher, IReceiptTypesService receiptTypesService, IReceiptService receiptService)
		{
			this.receiptQueryHandler = receiptQueryHandler;
			this.checkMasterQueryHandler = checkMasterQueryHandler;
			this.creditCardMasterQueryHandler = creditCardMasterQueryHandler;
			this.receiptTypeQueryHandler = receiptTypeQueryHandler;
			this.creditCardTypeQueryHandler = creditCardTypeQueryHandler;
			this.globalCashReceiptSettings = globalCashReceiptSettings;
			this.globalBudgetaryAccountSettings = globalBudgetaryAccountSettings;
			this.budgetaryAccountingService = budgetaryAccountingService;
			this.environmentSettings = environmentSettings;
			this.commandDispatcher = commandDispatcher;
			this.receiptTypesService = receiptTypesService;
			this.receiptService = receiptService;
		}	

		public decimal TotalAmount
		{
			get => receiptService.TotalAmount;
		}

		public decimal TotalReceiptAmount
		{
			get => receiptService.TotalReceiptAmount;
		}

		public decimal TotalPaid
		{
			get => receiptService.TotalPaid; 
		}

		public decimal TotalCashPaid
		{
			get => receiptService.TotalCashPaid;
		}

		public decimal TotalCheckPaid
		{
			get => receiptService.TotalCheckPaid;
		}

		public decimal TotalCreditPaid
		{
			get => receiptService.TotalCreditPaid;
		}

		public decimal ConvenienceFee
		{
			get => receiptService.ConvenienceFee; 
		}

		public IEnumerable<ReceiptPayment> Payments
		{
			get => receiptService.Payments;
		}
		public CRTransactionGroup TransactionGroup { get => receiptService.TransactionGroup; set => receiptService.TransactionGroup = value; }

		public void SetCorrelationId(Guid correlationId)
		{
			this.correlationId = correlationId;
		}

		public void SetReceiptToVoid(int receiptNumber)
		{
			receiptToVoid = receiptQueryHandler.ExecuteQuery(new ReceiptSearchCriteria
			{
				ReceiptNumber = receiptNumber
			}).FirstOrDefault();

			if (receiptToVoid != null)
			{
				BuildTransactionGroupFromReceipt();
			}
		}

		private void BuildTransactionGroupFromReceipt()
		{
			foreach (var archive in receiptToVoid.Archives)
			{
				var type = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
				{
					ReceiptType = archive.ReceiptType ?? 0,
					TownKey = archive.TownKey ?? 0
				}).FirstOrDefault();

				var transaction = new MiscReceiptTransaction();

				if (archive.DailyCloseOut > 0)
				{ 
					transaction.AffectCashDrawer = false;
				}
				else
				{
					transaction.AffectCashDrawer = archive.AffectCashDrawer ?? false;
				}
				transaction.AffectCash = archive.AffectCash ?? false;
				transaction.Comment = "Void of Receipt Number " + receiptToVoid.ReceiptNumber;
				transaction.GlAccount = archive.DefaultAccount;
				transaction.RecordKey = archive.RecordKey ?? 0;
				transaction.Id = Guid.NewGuid();
				transaction.PaidBy = archive.Name;
				transaction.PercentageAmount = 0;
				transaction.CollectionCode = archive.CollectionCode;
				transaction.CorrelationIdentifier = correlationId;
				transaction.Quantity = archive.Quantity ?? 0;
				transaction.TownCode = archive.TownKey ?? 0;
				transaction.Reference = archive.Ref;
				transaction.TransactionTypeCode = archive.ReceiptType ?? 0;
				transaction.TransactionTypeDescription = type.TypeTitle;

				transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
				{
					Name = "Amount1",
					Description = type.Title1 ?? "",
					PercentageAmount = 0,
					Total = (archive.Amount1 ?? 0).ToDecimal() * -1
				});

				transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
				{
					Name = "Amount2",
					Description = type.Title2 ?? "",
					PercentageAmount = 0,
					Total = (archive.Amount2 ?? 0).ToDecimal() * -1
				});

				transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
				{
					Name = "Amount3",
					Description = type.Title3 ?? "",
					PercentageAmount = 0,
					Total = (archive.Amount3 ?? 0).ToDecimal() * -1
				});

				transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
				{
					Name = "Amount4",
					Description = type.Title4 ?? "",
					PercentageAmount = 0,
					Total = (archive.Amount4 ?? 0).ToDecimal() * -1
				});

				transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
				{
					Name = "Amount5",
					Description = type.Title5 ?? "",
					PercentageAmount = 0,
					Total = (archive.Amount5 ?? 0).ToDecimal() * -1
				});

				transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem
                {
					Name = "Amount6",
					Description =  type.Title6 ?? "",
					PercentageAmount = 0,
					Total = (archive.Amount6 ?? 0).ToDecimal() * -1
                });

				transaction.Controls.Add(new ControlItem
				{
					Name = "Control1",
					Description = type.Control1,
					Value = archive.Control1
				});

				transaction.Controls.Add(new ControlItem
				{
					Name = "Control2",
					Description = type.Control2,
					Value = archive.Control2
				});

				transaction.Controls.Add(new ControlItem
				{
					Name = "Control3",
					Description = type.Control3,
					Value = archive.Control3
				});

				var baseTrans = transaction.ToTransactionBase();
				baseTrans.IncludeInCashDrawerTotal = archive.AffectCashDrawer ?? false;

				TransactionGroup.AddTransaction(baseTrans);
				TransactionGroup.PaidBy = transaction.PaidBy;
				TransactionGroup.PaidById = archive.NamePartyID ?? 0;
			}

			if (receiptToVoid.CashAmount != 0)
			{
				receiptService.AddExistingPayment(new ReceiptPayment
				{
					Amount = (receiptToVoid.CashAmount ?? 0).ToDecimal() * -1,
					PaymentType = PaymentMethod.Cash,
					Reference = ""
				});
			}

			if (receiptToVoid.CheckAmount != 0)
			{
				var checkPayments = checkMasterQueryHandler.ExecuteQuery(new CheckMasterSearchCriteria
				{
					ReceiptId = receiptToVoid.Id
				});

				foreach (var checkPayment in checkPayments)
				{
					receiptService.AddExistingPayment(new CheckReceiptPayment
					{
						Amount = ((checkPayment.Amount ?? 0) * -1).ToDecimal(),
						BankId = checkPayment.BankId ?? 0,
						CheckNumber = checkPayment.CheckNumber,
						PaymentType = PaymentMethod.Check,
					});
				}
			}

			if (receiptToVoid.CardAmount != 0)
			{
				var cardPayments = creditCardMasterQueryHandler.ExecuteQuery(new CreditCardMasterSearchCriteria
				{
					ReceiptId = receiptToVoid.Id 
				});

				foreach (var cardPayment in cardPayments)
				{
					var cardType = creditCardTypeQueryHandler.ExecuteQuery(new CreditCardTypeSearchCriteria
					{
						Id = cardPayment.CCType ?? 0
					}).FirstOrDefault();

					receiptService.AddExistingPayment(new CreditCardReceiptPayment
					{
						Amount = ((cardPayment.Amount ?? 0) * -1).ToDecimal(),
						TypeId = cardPayment.CCType ?? 0,
						EPaymentReferenceNumber = cardPayment.EPymtRefNumber,
						TypeDescription = cardType.Type,
						PaymentType = PaymentMethod.Credit,
						Id = cardPayment.CCPaymentIdentifier,
						EPaymentAccountId = cardPayment.EPmtAcctID
					});
				}
			}
		}

		public void CashOut()
		{
            var voidResponse = receiptService.VoidReceipt();
            if (!voidResponse.errorMessage.IsNullOrWhiteSpace())
            {
				NotifyAlertAdded(voidResponse.errorMessage,"Error");
            }
            else
            {
                commandDispatcher.Send(new SetReceiptToVoided
                {
                    ReceiptNumber = receiptToVoid.ReceiptNumber ?? 0
                });
            }
        }

        private void NotifyAlertAdded(string message,string alertType)
        {
            if (AlertAdded != null)
            {
                AlertAdded(this, (message: message,alertType: alertType));
            }
        }
	}
}
