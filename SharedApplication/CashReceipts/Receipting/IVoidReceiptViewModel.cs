﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Receipting
{
	public interface IVoidReceiptViewModel
	{
		void SetReceiptToVoid(int receiptNumber);
		void SetCorrelationId(Guid correlationId);
		CRTransactionGroup TransactionGroup { get; set; }
		IEnumerable<ReceiptPayment> Payments { get; }
		decimal ConvenienceFee { get; }
		decimal TotalAmount { get; }
		decimal TotalReceiptAmount { get; }
		decimal TotalPaid { get; }
		decimal TotalCashPaid { get; }
		decimal TotalCheckPaid { get; }
		decimal TotalCreditPaid { get; }
		void CashOut();
        event EventHandler<(string message, string alertType)> AlertAdded;
	}
}
