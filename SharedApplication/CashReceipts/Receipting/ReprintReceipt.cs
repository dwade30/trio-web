﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Receipting
{
    public class ReprintReceipt : Command
    {
        public int ReceiptId { get; }

        public ReprintReceipt(int receiptId)
        {
            ReceiptId = receiptId;
        }
    }
}