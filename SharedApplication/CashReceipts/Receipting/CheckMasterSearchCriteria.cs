﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Receipting
{
	public class CheckMasterSearchCriteria
	{
		public string CheckNumber { get; set; } = "";
		public int BankId { get; set; } = 0;
		public int ReceiptId { get; set; } = 0;
	}
}
