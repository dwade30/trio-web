﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Enums;

namespace SharedApplication.CashReceipts
{
	public class ReceiptTypeSearchCriteria
	{
		public int ReceiptType { get; set; } = 0;
		public int TownKey { get; set; } = 0;
		public DeletedOption DeletedSearchOption { get; set; } = DeletedOption.All;
	}
}
