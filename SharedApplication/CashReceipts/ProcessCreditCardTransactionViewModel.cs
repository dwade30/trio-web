﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts
{
	public class ProcessCreditCardTransactionViewModel : IProcessCreditCardTransactionViewModel
	{
		//*************
		//************

		private ProcessCreditCardTransactionResult result = new ProcessCreditCardTransactionResult();
		public ProcessCreditCardTransactionViewModel()
		{

		}

		public ProcessCreditCardTransactionResult Result
		{
			get => result;
			set => result = value;
		}

	}
}
