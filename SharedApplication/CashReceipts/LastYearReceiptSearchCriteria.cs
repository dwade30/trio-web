﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts
{
	public class LastYearReceiptSearchCriteria
	{
		public int ReceiptId { get; set; }
		public DateTime? ReceiptDate { get; set; }
	}
}
