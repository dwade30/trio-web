﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.SystemSettings.Models;

namespace SharedApplication.CashReceipts
{
	public interface IReceiptSearchViewModel
	{
		IEnumerable<GenericDescriptionPair<SummaryDetailOption>> GetSummaryDetailOptions();
		IEnumerable<GenericDescriptionPair<SummaryDetailBothOption>> GetSummaryDetailBothOptions();
		IEnumerable<GenericDescriptionPair<TimePeriodOption>> GetTimePeriodOptions();
		IEnumerable<GenericDescriptionPair<ReceiptSearchSummarizeByOption>> GetSummarizeByOptions();
		IEnumerable<GenericDescriptionPair<NameSearchOptions>> GetNameSearchOptions();
		IEnumerable<GenericDescriptionPair<AmountSearchOptions>> GetAmountSearchOptions();
		IEnumerable<GenericDescriptionPair<DateToShowOptions>> GetDateToShowOptions();
		IEnumerable<GenericDescriptionPair<MotorVehicleTransactionTypes>> GetMotorVehicleTransactionTypeOptions();
		IEnumerable<GenericDescriptionPair<ReceiptSearchSortByOptions>> GetSortByOptions();
		IEnumerable<GenericDescriptionPair<PaymentMethod>> GetPaymentMethods();
		IEnumerable<ReceiptType> ReceiptTypes { get; }
		IEnumerable<SavedStatusReport> SavedReports { get; }
		void DeleteReport(int id);
		ReceiptSearchReportSetupInformation GetSavedReportInformation(int id);
		IEnumerable<Operator> TellerIds { get; }
		IEnumerable<CloseOut> CloseOuts { get; }
		IEnumerable<CRBank> Banks { get; }
		bool ValidateGLAccount(string account);
		bool ReportExists(string reportTitle);
		bool SaveReport(ReceiptSearchReportSetupInformation reportSetup);
		IReceiptSearchService ReceiptSearchReportService { get; }
	}
}
