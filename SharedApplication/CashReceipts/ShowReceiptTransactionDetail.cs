﻿namespace SharedApplication.CashReceipts
{
    public class ShowReceiptTransactionDetail : Messaging.Command
    {
        public ReceiptDetailItem DetailItem { get; set; }

        public ShowReceiptTransactionDetail(ReceiptDetailItem detailItem)
        {
            DetailItem = detailItem;
        }

    }
}