﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts
{
	public class ReceiptSearchReportSetupInformation
	{
		public string ReportName { get; set; } = "";
		public IEnumerable<ReceiptSearchSortByOptions> SortOptions { get; set; } = new List<ReceiptSearchSortByOptions>();
		public IEnumerable<int> ReceiptTypes { get; set; } = new List<int>();
		public int CloseOutDateStartId { get; set; } = 0;
		public int CloseOutDateEndId { get; set; } = 0;
		public string CloseOutDateStartDescription { get; set; } = "";
		public string CloseOutDateEndDescription { get; set; } = "";
		public DateTime? ActualDateStart { get; set; }
		public DateTime? ActualDateEnd { get; set; }
		public string TellerId { get; set; } = "";
		public int ReceiptTypeCodeStart { get; set; } = 0;
		public int ReceiptTypeCodeEnd { get; set; } = 0;
		public int ReceiptNumberStart { get; set; } = 0;
		public int ReceiptNumberEnd { get; set; } = 0;
		public string Reference { get; set; } = "";
		public NameSearchOptions NameSearchType { get; set; }
		public string Name { get; set; } = "";
		public string Control1 { get; set; } = "";
		public string Control2 { get; set; } = "";
		public string Control3 { get; set; } = "";
		public string GLAccount { get; set; } = "";
		public MotorVehicleTransactionTypes MotorVehicleTransactionType { get; set; }
		public decimal? ReceiptTotal { get; set; } = null;
		public AmountSearchOptions ReceiptTotalSearchType { get; set; }
		public decimal? TenderedTotal { get; set; } = null;
		public AmountSearchOptions TenderedTotalSearchType { get; set; }
		public NameSearchOptions PaidBySearchType { get; set; }
		public string PaidBy { get; set; } = "";
		public string CheckNumber { get; set; } = "";
		public DateToShowOptions DateToShow { get; set; }
		public int Bankid { get; set; } = 0;
		public string BankDescription { get; set; } = "";
		public string Comment { get; set; } = "";
		public PaymentMethod PaymentType { get; set; }
		public DateTime? ActualTimeStart { get; set; }
		public DateTime? ActualTimeEnd { get; set; }

		public SummaryDetailBothOption ReportDetailLevel { get; set; }
		public SummaryDetailOption ReceiptDetailLevel { get; set; }
		public TimePeriodOption TimePeriod { get; set; }
		public bool ShowTenderedAmounts { get; set; }
		public ReceiptSearchSummarizeByOption SummarizeBy { get; set; }
		public SummaryDetailOption SummaryDetailLevel { get; set; }

		
		
	}
}
