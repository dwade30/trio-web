﻿using System;
using System.Collections.Generic;
using SharedApplication.Clerk;

namespace SharedApplication.CashReceipts
{
    public interface IChooseClerkTransactionTypeViewModel
    {
        void SetCorrelationId(Guid correlationID);
        void StartTransaction(ClerkTransactionType transactionType);
        IEnumerable<GenericDescriptionPair<ClerkTransactionType>> GetAvailableTransactionTypes();

        void Cancel();
    }
}