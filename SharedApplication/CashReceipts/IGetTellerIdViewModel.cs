﻿using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
    public interface IGetTellerIdViewModel
    {

        string ChosenTeller { get; set; }
        TellerValidationResult ValidateTeller(string tellerId, string password);

        bool RequirePassword { get; }
        bool ShowDefaultTeller { get; }
        string DefaultTeller { get; }
    }
}