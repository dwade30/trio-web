﻿namespace SharedApplication.CashReceipts
{
    public class CRReceiptQuery
    {
        public int Id { get; }

        public CRReceiptQuery(int id)
        {
            Id = id;
        }
    }
}