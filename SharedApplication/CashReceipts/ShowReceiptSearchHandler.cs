﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
	public class ShowReceiptSearchHandler : CommandHandler<ShowReceiptSearch>
	{
		private IView<IReceiptSearchViewModel> receiptSearchView;
		
		public ShowReceiptSearchHandler(IView<IReceiptSearchViewModel> view)
		{
			receiptSearchView = view;
		}
		protected override void Handle(ShowReceiptSearch command)
		{
			receiptSearchView.Show();
		}
	}
}
