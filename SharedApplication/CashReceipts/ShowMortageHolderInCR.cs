﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class ShowMortgageHolderInCR : Command
    {
        public int Account { get; set; } = 0;
        public int AccountType { get; set; } = 0;

        public ShowMortgageHolderInCR(int account, int accountType)
        {
            Account = account;
            AccountType = accountType;
        }
    }
}