﻿using System.Linq;

namespace SharedApplication.CashReceipts
{
    public class NewCRBankViewModel : INewCRBankViewModel
    {
        private ICashReceiptsContext crContext;
        public NewCRBankViewModel(ICashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        public string Name { get; set; }
        public string RoutingTransitnumber { get; set; }
        public bool RoutingNumberIsUnique(string routingNumber)
        {
            return !crContext.Banks.Any(b => b.RoutingTransit == routingNumber);
        }
    }
}