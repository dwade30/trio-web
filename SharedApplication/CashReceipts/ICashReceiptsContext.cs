﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts
{
	public interface ICashReceiptsContext
	{
		IQueryable<Receipt> Receipts { get; }
		IQueryable<LastYearReceipt> LastYearReceipts { get; }
        IQueryable<LastYearArchive> LastYearArchives { get; }
        
        IQueryable<Archive> Archives { get; }
		IQueryable<ReceiptType> ReceiptTypes { get; }
		IQueryable<CRBank> Banks { get; }
        IQueryable<CreditCardType> CreditCardTypes { get; }
        IQueryable<CreditCardMaster> CreditCardMasters { get; }
        IQueryable<CheckMaster> CheckMasters { get; }
		IQueryable<MOSESType> MosesTypes { get; }
		IQueryable<Cya> CYAs { get; }
		IQueryable<CloseOut> CloseOuts { get; }
		IQueryable<SavedStatusReport> SavedStatusReports { get; }
		IQueryable<LastYearCreditCardMaster> LastYearCreditCardMasters { get; }
		IQueryable<LastYearCheckMaster> LastYearCheckMasters { get; }
		IQueryable<ElectronicPaymentLog> ElectronicPaymentLogs { get; }
		IQueryable<ServiceCode> ServiceCodes { get; }
    }
}
