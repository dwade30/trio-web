﻿using System.Collections.Generic;
using SharedApplication.CashReceipts.Receipting;

namespace SharedApplication.CashReceipts
{
    public class SelectReceiptFromListViewModel : ISelectReceiptFromListViewModel
    {
        public List<ReceiptInfo> ReceiptList { get; set; }
        public bool Cancelled { get; set; }
        public int SelectedReceiptId { get; set; }
    }


}