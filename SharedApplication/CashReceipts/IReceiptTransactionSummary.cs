﻿using System;

namespace SharedApplication.CashReceipts
{
    public interface IReceiptTransactionSummary
    {
	    CRTransactionGroup TransactionGroup { get; set; }
		void SetTransactionGroup(CRTransactionGroup transactionGroup);
        bool EditTransaction(Guid transactionId);
        void ShowPaymentDetail(Guid transactionId);
    }
}