﻿namespace SharedApplication.CashReceipts
{
    public enum TellerIDUsage
    {
        RequireEachTime = 0,
        RequirePassword = 1,
        RequireButDefault = 2,
        NotRequired = 3
    }
}