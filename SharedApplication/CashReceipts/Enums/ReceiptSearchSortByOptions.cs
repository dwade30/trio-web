﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Enums
{
	public enum ReceiptSearchSortByOptions
	{
		ActualSystemDate = 25,
		ActualSystemTime = 0,
		CloseOutDate = 1,
		TellerId = 2,
		ReceiptType = 3,
		ReceiptNumber = 4,
		Reference = 5,
		Name = 6,
		Control1 = 7,
		Control2 = 8,
		Control3 = 9
	}
}
