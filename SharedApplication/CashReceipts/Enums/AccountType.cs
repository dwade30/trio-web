﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Enums
{
    public enum AccountType
    {
        None,
        RealEstate,
        PersonalProperty,
        UtilityBilling,
        AccountsReceivable
    }

    public static class AccountBillingTypeExtensions
    {
        public static int ToInteger(this AccountType accountType)
        {
            return (int)accountType;
        }

        public static string ToAbbreviation(this AccountType accountType)
        {
            switch (accountType)
            {
                case AccountType.UtilityBilling:
                    return "UT";
                    break;
                case AccountType.PersonalProperty:
                    return "PP";
                    break;
                case AccountType.RealEstate:
                    return "RE";
                    break;
                case AccountType.AccountsReceivable:
                    return "AR";
                    break;
            }

            return "";
        }

    }
}
