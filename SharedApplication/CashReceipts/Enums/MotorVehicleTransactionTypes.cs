﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Enums
{
	public enum MotorVehicleTransactionTypes
	{
		NoSelection,
		All,
		DuplicateRegistration,
		DuplicateStickers,
		Correction,
		GrossVehicleWeightChange,
		LostPlateStickers,
		NewRegistration,
		ReRegistration,
		NewRegistrationTransfer,
		ReRegistrationTransfer
	}
}
