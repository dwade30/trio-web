﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Enums
{
	public enum DateToShowOptions
	{
		ActualSystemDate,
		RecordedTransactionDate
	}
}
