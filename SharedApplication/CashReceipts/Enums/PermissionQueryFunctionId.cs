﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Enums
{
	public enum PermissionQueryFunctionId
	{
		MainEntry = 1,
		ReceiptInput = 2,
		DisplayLastReceipt = 3,
		DailyReceiptAudit = 4,
		RecreateDailyAudit = 16,
		ReprintDailyAudit = 5,
		FileMaintenance = 6,
		SetupReceiptTypes = 7,
		SearchRoutines = 8,
		OpenCashDrawer = 9,
		EndOfYear = 10,
		ReprintReceipt = 11,
		TypeListing = 12,
		Mvr3Listing = 13,
		Customize = 14,
		ResetReceiptNumber = 15,
		PasswordProtectAudit = 17,
		CashDrawerCodes = 18,
		ChangeComments = 19,
		ReportAlignment = 20,
		TellerDefaults = 21,
		BankMaintenance = 22,
		RemoveLienMaturityNotice = 23,
		AutoPostJournal = 24,
	}
}
