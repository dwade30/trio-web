﻿namespace SharedApplication.CashReceipts
{
    public interface INewCRBankViewModel
    {
        string Name { get; set; }
        string RoutingTransitnumber { get; set; }
        bool RoutingNumberIsUnique(string routingNumber);
    }
}