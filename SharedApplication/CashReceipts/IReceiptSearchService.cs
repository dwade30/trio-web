﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts
{
	public interface IReceiptSearchService
	{
		ReceiptSearchReportSetupInformation GetSavedReportSetup(int id);
		bool SaveReport(ReceiptSearchReportSetupInformation reportSetup);
		bool DeleteReport(int id);
		IEnumerable<ReceiptSearchReportResult> GetReportData();
		ReceiptSearchReportSetupInformation ReportSetup { get; set; }
		IEnumerable<ReceiptType> ReceiptTypes { get; }

		IEnumerable<ReceiptSearchSummaryInformation> SummarizeData(
			IEnumerable<ReceiptSearchReportResult> reportData);

		IEnumerable<ReceiptSearchMotorVehicleSummaryInformation> SummarizeMotorVehicleData(
			IEnumerable<ReceiptSearchReportResult> reportData);

		IEnumerable<ReceiptSearchSummaryDetailInformation> GetSummaryDetailData(
			ReceiptSearchSummaryInformation groupRecord, IEnumerable<ReceiptSearchReportResult> reportData);

		IEnumerable<ReceiptSearchSummaryDetailInformation> GetMotorVehicleSummaryDetailData(
			ReceiptSearchMotorVehicleSummaryInformation groupRecord, IEnumerable<ReceiptSearchReportResult> reportData);
	}
}
