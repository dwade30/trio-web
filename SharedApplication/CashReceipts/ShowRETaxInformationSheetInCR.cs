﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class ShowRETaxInformationSheetInCR : Command
    {
        public int Account { get; set; }
        public DateTime EffectiveDate { get; set; }
        public ShowRETaxInformationSheetInCR(int account, DateTime effectiveDate)
        {
            Account = account;
            EffectiveDate = effectiveDate;
        }
    }
}