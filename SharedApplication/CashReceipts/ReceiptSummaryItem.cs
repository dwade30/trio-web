﻿using System;

namespace SharedApplication.CashReceipts
{
    public class ReceiptSummaryItem
    {
        public string TransactionType { get; set; }
        public string TypeDescription { get; set; }
        public int Account { get; set; }
        public string Info { get; set; }
        public string Name { get; set; }
       // public int Copies { get; set; } = 1;
        public decimal Amount { get; set; } = 0;
        public int Split { get; set; } = 1;
        public Guid TransactionId { get; set; } = Guid.Empty;
    }
}
