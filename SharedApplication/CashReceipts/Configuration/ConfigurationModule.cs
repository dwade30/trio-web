﻿using Autofac;
using SharedApplication.CashReceipts.CamdenMooringImport;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.MyRecImport;
using SharedApplication.CashReceipts.Receipting;


namespace SharedApplication.CashReceipts.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<ChooseTransactionTypeViewModel>().As<IChooseTransactionTypeViewModel>();
            builder.RegisterInstance(new InMemoryCRTransactionGroupRepository()).As<CRTransactionGroupRepository>();
           // builder.RegisterType<ReceiptTransactionSummary>().As<IReceiptProcessViewModel>();
            builder.RegisterType<ReceiptTransactionDetailViewModel>().As<IReceiptTransactionDetailViewModel>();
            builder.RegisterType<GetTellerIdViewModel>().As<IGetTellerIdViewModel>();
            builder.RegisterType<ProcessCreditCardTransactionViewModel>().As<IProcessCreditCardTransactionViewModel>();
			builder.RegisterType<CRCashOutViewModel>().As<ICRCashOutViewModel>();
            builder.RegisterType<NewCRBankViewModel>().As<INewCRBankViewModel>();
			builder.RegisterType<MultiTownSelectionViewModel>().As<IMultiTownSelectionViewModel>();
			builder.RegisterType<VoidReceiptViewModel>().As<IVoidReceiptViewModel>();
			builder.RegisterType<ReceiptService>().As<IReceiptService>();
			builder.RegisterType<ReceiptSearchViewModel>().As<IReceiptSearchViewModel>();
			builder.RegisterType<ReceiptSearchService>().As<IReceiptSearchService>();
            builder.RegisterType<ChooseExistingReceiptViewModel>().As<IChooseExistingReceiptViewModel>();
            builder.RegisterType<SelectReceiptFromListViewModel>().As<ISelectReceiptFromListViewModel>();
            builder.RegisterType<ReprintReceiptViewModel>().As<IReprintReceiptViewModel>();
           // builder.RegisterType<PayportService>().AsSelf();
            builder.RegisterType<GetPaymentFromPortalViewModel>().As<IGetPaymentFromPortalViewModel>();
            builder.RegisterType<MyRecReceiptImporter>().As<IMyRecReceiptImporter>();
            builder.RegisterType<CamdenMooringReceiptImporter>().As<ICamdenMooringReceiptImporter>();
            RegisterPaymentService(ref builder);
        }

        private void RegisterPaymentService(ref ContainerBuilder builder)
        {
            builder.Register(f =>
            {
                var crSettings = f.Resolve<GlobalCashReceiptSettings>();
                if (crSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort)
                {
                    var payportService = new PayportService(crSettings.EPaymentInTestMode);                    
                    return payportService;
                }
                //add other portal types here
                ICreditCardPaymentService nullService = new NullPaymentPortal();
                return nullService;
            }).As< ICreditCardPaymentService>();
        }

    }
}