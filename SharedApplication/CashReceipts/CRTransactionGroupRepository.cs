﻿using System;

namespace SharedApplication.CashReceipts
{
    public interface CRTransactionGroupRepository : IRepository<CRTransactionGroup, Guid>
    {
        void Add(CRTransactionGroup transactionGroup);
        void Remove(Guid id);
    }
}