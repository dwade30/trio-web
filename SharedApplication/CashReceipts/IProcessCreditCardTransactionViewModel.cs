﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts
{
	public interface IProcessCreditCardTransactionViewModel
	{
		ProcessCreditCardTransactionResult Result { get; set; }
	}
}
