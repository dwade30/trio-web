﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class ChooseExistingReceiptHandler : CommandHandler<ChooseExistingReceipt,int>
    {
        private IModalView<IChooseExistingReceiptViewModel> chooseView;
        public ChooseExistingReceiptHandler(IModalView<IChooseExistingReceiptViewModel> chooseView)
        {
            this.chooseView = chooseView;
        }
        protected override int Handle(ChooseExistingReceipt command)
        {
            chooseView.ShowModal();
            if (chooseView.ViewModel.Cancelled)
            {
                return 0;
            }

            return chooseView.ViewModel.Id;
        }
    }
}