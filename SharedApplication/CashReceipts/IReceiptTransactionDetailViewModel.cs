﻿namespace SharedApplication.CashReceipts
{
    public interface IReceiptTransactionDetailViewModel
    {
        ReceiptDetailItem DetailItem { get; set; }
    }
}