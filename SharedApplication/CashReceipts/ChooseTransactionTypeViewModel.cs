﻿using System;
using System.Collections.Generic;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Receipting;

namespace SharedApplication.CashReceipts
{
    public class ChooseTransactionTypeViewModel // : IChooseTransactionTypeViewModel
    {
        private CRTransactionGroup transactionGroup;
        private CommandDispatcher commandDispatcher;
        public ChooseTransactionTypeViewModel( CommandDispatcher commandDispatcher)
        {
            //this.transactionGroup = transactionGroup;
            this.commandDispatcher = commandDispatcher;
        }

        public void SetTransactionGroup(CRTransactionGroup transactionGroup)
        {
            this.transactionGroup = transactionGroup;
        }

        public void StartClerkTransaction()
        {
            commandDispatcher.Send(new InitializeClerkContext());
            commandDispatcher.Send(new MakeClerkTransaction() {CorrelationId = transactionGroup.ReceiptIdentifier, Id = Guid.NewGuid()});
        }

		public void StartMiscReceiptTransaction(int receiptType)
		{
			commandDispatcher.Send(new MakeMiscReceiptTransaction
			{
				CorrelationId = transactionGroup.ReceiptIdentifier,
				Id = Guid.NewGuid() ,
				ReceiptType = receiptType
			});
		}

        public IEnumerable<DescriptionIDPair> GetTransactionTypes()
        {
            throw new NotImplementedException();
        }

        public void StartRealEstateTransaction()
        {
            throw new NotImplementedException();
        }

        public void StartPersonalPropertyTransaction()
        {
            throw new NotImplementedException();
        }

        public void StartUtilityTransaction()
        {
            throw new NotImplementedException();
        }

        public bool IsValidTransactionCode(int transactionCode)
        {
            throw new NotImplementedException();
        }

        public bool IsValidTransactionChoice(string choice)
        {
            throw new NotImplementedException();
        }

        public void StartMotorVehicleTransaction()
        {
            commandDispatcher.Send(new MakeMotorVehicleTransaction()
                {CorrelationId = transactionGroup.ReceiptIdentifier, Id = Guid.NewGuid()});
        }
    }
}