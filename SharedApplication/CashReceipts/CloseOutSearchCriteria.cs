﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Enums;

namespace SharedApplication.CashReceipts
{
	public class CloseOutSearchCriteria
	{
		public CloseOutType Type { get; set; } = CloseOutType.All;
	}
}
