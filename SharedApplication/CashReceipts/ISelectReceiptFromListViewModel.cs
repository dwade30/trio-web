﻿using System.Collections.Generic;
using SharedApplication.CashReceipts.Receipting;

namespace SharedApplication.CashReceipts
{
    public interface ISelectReceiptFromListViewModel
    {
        List<ReceiptInfo> ReceiptList { get; set; }
        bool Cancelled { get; set; }
        int SelectedReceiptId { get; set; }
    }
}