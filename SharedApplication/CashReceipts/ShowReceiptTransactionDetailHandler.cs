﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class ShowReceiptTransactionDetailHandler : CommandHandler<ShowReceiptTransactionDetail>
    {
        private IView<IReceiptTransactionDetailViewModel> detailView;

        public ShowReceiptTransactionDetailHandler(IView<IReceiptTransactionDetailViewModel> detailView)
        {
            this.detailView = detailView;
        }
        protected override void Handle(ShowReceiptTransactionDetail command)
        {
            detailView.ViewModel.DetailItem = command.DetailItem;
            detailView.Show( );
        }
    }
}