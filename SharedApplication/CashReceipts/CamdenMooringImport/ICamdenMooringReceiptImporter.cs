﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.CamdenMooringImport
{
    public interface ICamdenMooringReceiptImporter
    {
        (IEnumerable<MiscReceiptTransaction> transactions, Exception exception) ImportFile(string fileName, Guid correlationId);
        Exception LastException { get; }
    }
}
