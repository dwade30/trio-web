﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.MyRecImport;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.CamdenMooringImport
{
    public class ImportCamdenMooringTransactionHandler : CommandHandler<ImportCamdenMooringTransaction>
    {
        private ICamdenMooringReceiptImporter receiptImporter;
        private EventPublisher eventPublisher;

        public ImportCamdenMooringTransactionHandler(ICamdenMooringReceiptImporter receiptImporter, EventPublisher eventPublisher)
        {
            this.receiptImporter = receiptImporter;
            this.eventPublisher = eventPublisher;
        }

        protected override void Handle(ImportCamdenMooringTransaction command)
        {
            var results = receiptImporter.ImportFile(command.FileName, command.CorrelationId);
            var error = results.exception;
            var transactions = results.transactions;
            if (error == null && transactions != null && transactions.Any())
            {
                eventPublisher.Publish(new TransactionCompleted
                {
                    CorrelationId = command.CorrelationId,
                    OverridePaidByName = "Import Moorings Batch " + DateTime.Now.ToString("MM/dd/yyyy h:mm tt"),
                    OverridePaidByPartyId = 0,
                    CompletedTransactions = transactions.Select(t => t.ToTransactionBase())
                });
            }
            else
            {
                eventPublisher.Publish(new TransactionCancelled(command.CorrelationId, new List<string>() { error.Message }));
            }
        }
    }
}
