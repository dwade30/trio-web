﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.CashReceipts.CamdenMooringImport
{
    public class ImportCamdenMooringTransaction : Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public string FileName { get; }
        public ImportCamdenMooringTransaction(Guid correlationId, Guid id, string fileName)
        {
            FileName = fileName;
            CorrelationId = correlationId;
            Id = id;
        }
    }
}
