﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.CamdenMooringImport
{
    public class CamdenMooringImportRecord
    {
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string PermitNumber { get; set; } = "";
        public DateTime PermitDate { get; set; }
        public decimal Amount { get; set; } = 0;
        public int ReceiptCode { get; set; } = 0;
        public int ReceiptFeeIndex { get; set; } = 0;
        public string FullName
        {
            get => LastName + ", " + FirstName;
        }
    }
}
