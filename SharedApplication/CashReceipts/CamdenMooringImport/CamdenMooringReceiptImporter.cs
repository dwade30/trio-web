﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using SharedApplication.CashReceipts.MyRecImport;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.CamdenMooringImport
{
    public class CamdenMooringReceiptImporter : ICamdenMooringReceiptImporter
    {
        private ITrioFileSystem fileSystem;
        private IReceiptTypesService receiptTypesService;
        public CamdenMooringReceiptImporter(ITrioFileSystem fileSystem, IReceiptTypesService receiptTypesService)
        {
            this.fileSystem = fileSystem;
            this.receiptTypesService = receiptTypesService;
        }
        public Exception LastException { get; private set; } = null;
        public (IEnumerable<MiscReceiptTransaction> transactions, Exception exception) ImportFile(string fileName, Guid correlationId)
        {
            LastException = null;
            var transactions = new List<MiscReceiptTransaction>();
            if (fileSystem.Exists(fileName))
            {

                using (var reader = new StreamReader(fileSystem.OpenFileStream(fileName, FileMode.Open, FileAccess.Read)))
                {
                    var records = new List<CamdenMooringImportRecord>();
                    bool firstRecord = true;
                    var line = "";

                    try
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (firstRecord)
                            {
                                firstRecord = false;
                            }
                            else
                            {
                                var data = line.Split('\t');

                                records.Add(new CamdenMooringImportRecord
                                {
                                    LastName = data[0].Trim(),
                                    FirstName = data[1].Trim(),
                                    PermitNumber = data[2].Trim(),
                                    PermitDate = data[3].ToDate(),
                                    Amount = data[4].ToDecimalValue(),
                                    ReceiptCode = data[5].ToIntegerValue(),
                                    ReceiptFeeIndex = data[6].ToIntegerValue()
                                });
                            }
                        }
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        LastException = ex;
                        return (transactions, LastException);
                    }

                    if (records.Any())
                    {
                        var receiptCodes = records.GroupBy(r => r.ReceiptCode, r => r.ReceiptCode, (code, codes) => code);
                        var receiptFeeCodes = records.GroupBy(r => r.ReceiptFeeIndex, r => r.ReceiptFeeIndex,
                            (feeCode, feeCodes) => feeCode);
                        var minCode = receiptFeeCodes.Min();
                        if (minCode < 1)
                        {
                            LastException = new InvalidReceiptFeeCode(minCode, 0)
                            {
                                ReceiptFeeCode = receiptFeeCodes.Min()
                            };
                            return (transactions, LastException);
                        }

                        var maxCode = receiptFeeCodes.Max();
                        if (maxCode > 6)
                        {
                            LastException = new InvalidReceiptFeeCode(maxCode, 0)
                            {
                                ReceiptFeeCode = receiptFeeCodes.Max()
                            };
                            return (transactions, LastException);
                        }

                        foreach (var receiptCode in receiptCodes)
                        {
                            var receiptType = receiptTypesService.ReceiptTypes.FirstOrDefault(r =>
                                r.TypeCode == receiptCode);
                            if (receiptType == null)
                            {
                                LastException = new UnknownReceiptCode(receiptCode.ToString());
                                return (transactions, LastException);
                            }
                        }

                        foreach (var camdenMooringImportRecord in records)
                        {
                            var receiptType =
                                receiptTypesService.ReceiptTypes.FirstOrDefault(r =>
                                    r.TypeCode == camdenMooringImportRecord.ReceiptCode);
                            var categoryTitle = receiptType.GetTitle(camdenMooringImportRecord.ReceiptFeeIndex);

                            if (categoryTitle.IsNullOrWhiteSpace())
                            {
                                LastException = new InvalidReceiptFeeCode(camdenMooringImportRecord.ReceiptFeeIndex, camdenMooringImportRecord.ReceiptCode);
                                return (transactions, LastException);
                            }

                            var transaction = new MiscReceiptTransaction()
                            {
                                TransactionTypeCode = receiptType.TypeCode.GetValueOrDefault(),
                                PaidBy = camdenMooringImportRecord.FullName,
                                AffectCash = true,
                                AffectCashDrawer = true,
                                Comment = "",
                                Quantity = 1,
                                CollectionCode = camdenMooringImportRecord.ReceiptCode.ToString(),
                                CorrelationIdentifier = correlationId,
                                Reference = camdenMooringImportRecord.PermitNumber,
                                TransactionTypeDescription = receiptType.TypeTitle
                            };
                            transaction.Controls.Add(new ControlItem() { Description = "Control1", Name = "Control1", Value = "" });
                            transaction.Controls.Add(new ControlItem() { Description = "Control2", Name = "Control2", Value = "" });
                            transaction.Controls.Add(new ControlItem() { Description = "Control3", Name = "Control3", Value = "" });

                            for (int x = 1; x < 7; x++)
                            {
                                decimal amount = 0;
                                if (x == camdenMooringImportRecord.ReceiptFeeIndex)
                                {
                                    amount = camdenMooringImportRecord.Amount;
                                }
                                transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem() { Description = "Amount" + x, Name = "Amount" + x, Total = amount });
                            }
                            transactions.Add(transaction);
                        }
                    }

                }
            }
            else
            {
                LastException = new FileNotFoundException(fileName);
            }

            return (transactions, LastException);
        }

        public class UnknownReceiptCode : Exception
        {
            public string ReceiptCode { get; set; } = "";

            public UnknownReceiptCode(string receiptCode) : base("Receipt " + receiptCode + " is invalid")
            {
                ReceiptCode = receiptCode;
            }
        }

        public class InvalidReceiptFeeCode : Exception
        {
            public InvalidReceiptFeeCode(int receiptFeeCode, int receiptCode) : base("Receipt " + receiptCode + " category " + receiptFeeCode + " is invalid")
            {
                ReceiptFeeCode = receiptFeeCode;
                ReceiptCode = receiptCode;
            }
            public int ReceiptFeeCode { get; set; } = 0;
            public int ReceiptCode { get; set; } = 0;

        }
    }
}
