﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts
{
	public class CreditCardTypeSearchCriteria
	{
		public bool SystemDefined { get; set; }
		public int Id { get; set; } = 0;
	}
}
