﻿using System;
using SharedApplication.Authorization;
using System.Collections.Generic;
using System.Linq;

using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.CentralParties.Commands;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.UtilityBilling;

namespace SharedApplication.CashReceipts
{
    public class CRCashOutViewModel : ReceiptTransactionSummary , ICRCashOutViewModel
    {
        private IQueryHandler<CRBanksQuery, IEnumerable<CRBank>> bankQueryHandler;
        private IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler;
		private IQueryHandler<CreditCardTypeSearchCriteria, IEnumerable<CreditCardType>> creditCardTypeQueryHandler;
        private IQueryHandler<CheckMasterSearchCriteria, IEnumerable<CheckMaster>> checkMasterQueryHandler;
		private GlobalCashReceiptSettings globalCashReceiptSettings;

		private List<CRBank> banks;
        private List<CreditCardType> creditCardTypes;
        private Receipt savedReceipt;

		public CRCashOutViewModel(CommandDispatcher commandDispatcher, IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, DataEnvironmentSettings environmentSettings, IGlobalActiveModuleSettings activeModuleSettings, IUserPermissionSet currentPermissionSet, IAccountValidationService accountValidationService, GlobalUtilityBillingSettings utilitySettings, IReceiptTypesService receiptTypesService, IQueryHandler<CRBanksQuery, IEnumerable<CRBank>> bankQueryHandler, IQueryHandler<CreditCardTypeSearchCriteria, IEnumerable<CreditCardType>> creditCardTypeQueryHandler, GlobalCashReceiptSettings globalCashReceiptSettings, IQueryHandler<CheckMasterSearchCriteria, IEnumerable<CheckMaster>> checkMasterQueryHandler, IQueryHandler<ReceiptSearchCriteria, IEnumerable<Receipt>> receiptQueryHandler, IReceiptService receiptService) : base(commandDispatcher, receiptTypeQueryHandler, environmentSettings, activeModuleSettings, currentPermissionSet, accountValidationService, utilitySettings, receiptTypesService, receiptService)        
		{
            this.bankQueryHandler = bankQueryHandler;
            this.creditCardTypeQueryHandler = creditCardTypeQueryHandler;
            this.globalCashReceiptSettings = globalCashReceiptSettings;
            this.checkMasterQueryHandler = checkMasterQueryHandler;
            this.receiptQueryHandler = receiptQueryHandler;
            MyRecImportEnabled = activeModuleSettings.CRMyRecIsActive;
            CamdenMooringImportEnabled = environmentSettings.ClientName.ToUpper().Trim() == "CAMDEN";
            PaymentMethods = FillPaymentMethods();
        }

        public IEnumerable<GenericDescriptionPair<PaymentMethod>> PaymentMethods { get; private set; }
        
        public IEnumerable<CRBank> Banks
        {
            get
            {
                if (banks == null)
                {
                    LoadBanks();
                }

                return banks;
            }
        }

        public IEnumerable<CreditCardType> CreditCardTypes
        {
	        get
	        {
		        if (creditCardTypes == null)
		        {
			        LoadCreditCardTypes();
		        }

		        return creditCardTypes;
	        }
        }

        public bool PrintExtraReceipt()
        {
	        return globalCashReceiptSettings.PrintExtraReceipt;
        }

		private void LoadBanks()
        {
             banks = new List<CRBank>( bankQueryHandler.ExecuteQuery(new CRBanksQuery()));
        }

		private void LoadCreditCardTypes()
		{
			creditCardTypes = new List<CreditCardType>(creditCardTypeQueryHandler.ExecuteQuery(new CreditCardTypeSearchCriteria
			{
				SystemDefined = globalCashReceiptSettings.ElectronicPaymentPortalType == ElectronicPaymentPortalOption.PayPort
			}));
		}

		public ValidReceiptToVoidResult ValidReceiptToVoid(int receiptNumber)
        {
            if (receiptNumber < 1)
            {
                return ValidReceiptToVoidResult.ReceiptDoesNotExist;
            }
	        var receipt = receiptQueryHandler.ExecuteQuery(new ReceiptSearchCriteria
	        {
				ReceiptNumber = receiptNumber
	        }).FirstOrDefault();

	        if (receipt == null)
	        {
		        return ValidReceiptToVoidResult.ReceiptDoesNotExist;
	        }

            if (receipt.IsVoided ?? false)
            {
                return ValidReceiptToVoidResult.ReceiptAlreadyVoided;
            }

	        foreach (var archive in receipt.Archives)
	        {
		        if (receiptTypesService.IsSystemDefinedType(archive.ReceiptType ?? 0))
		        {
			        return ValidReceiptToVoidResult.ReceiptContainsRestrictedTypes;
		        }
	        }

	        return ValidReceiptToVoidResult.OkToVoid;
        }

        public void VoidReceipt(int receiptNumber)
        {
	        commandDispatcher.Send(new VoidReceipt
	        {
		        ReceiptNumber = receiptNumber,
		        Id = Guid.NewGuid(),
				CorrelationId = Guid.NewGuid(),
				TellerId = receiptService.TransactionGroup.TellerId
			});
		}

        public bool MyRecImportEnabled { get; }

        public bool CamdenMooringImportEnabled { get; }

        public void AddPayment(ReceiptPayment payment)
		{
			if (payment != null)
			{
				receiptService.AddPayment(payment);
				NotifyPaymentsUpdated();
			}
		}
		
		public bool CheckAlreadyExistsForBank(string checkNumber, int bankId)
		{
			var result = checkMasterQueryHandler.ExecuteQuery(new CheckMasterSearchCriteria
			{
				CheckNumber = checkNumber,
				BankId = bankId
			}).FirstOrDefault();
			return result != null;
		}

		public void RemovePayment(Guid id)
		{
			if (receiptService.RemovePayment(id))
			{
				NotifyPaymentsUpdated();
			}
		}

		public void RemoveTransaction(Guid id)
		{
			if (receiptService.RemoveTransaction(id))
            {
                if (globalCashReceiptSettings.ElectronicPaymentPortalType != ElectronicPaymentPortalOption.None)
                {
                    var ccPayments = receiptService.Payments.Where(p => p.PaymentType == PaymentMethod.Credit).ToList();
                    foreach (var ccPayment in ccPayments)
                    {
                        receiptService.RemovePayment(ccPayment.Id);
                        NotifyPaymentsUpdated();
                    }
                }

                NotifyTransactionsUpdated();
			}
		}

		public int  AddBank()
        {
           var bank = commandDispatcher.Send(new GetNewCRBank()).Result;
           if (!String.IsNullOrWhiteSpace(bank.Name))
           {
               commandDispatcher.Send(new AddCRBank(bank));
               return bank.ID;
           }

           return 0;
        }

        public bool CanCashOut()
        {
	        return receiptService.CanCashOut();
        }

        public void ChooseParty()
        {
            var Id = commandDispatcher.Send(new ChooseCentralParty()).Result;
            if (PaidById != Id)
            {
                PaidById = Id;
                if (PaidById != 0)
                {
                    var party = commandDispatcher.Send(new GetCentralParty(PaidById)).Result;
                    if (party != null)
                    {
                        PaidBy = party.FullName();
                    }
                    NotifyPartyUpdated();
                }                                
            }
        }
		
        private void NotifyPaymentsUpdated()
        {
            if (PaymentsUpdated != null)
            {
                PaymentsUpdated(this,new EventArgs());
            }
        }

        private void NotifyTransactionsUpdated()
        {
	        if (TransactionsUpdated != null)
	        {
		        TransactionsUpdated(this, new EventArgs());
	        }
        }

		private void NotifyPartyUpdated()
        {
            if (PartyUpdated != null)
            {
                PartyUpdated(this, (PartyId: PaidById, PartyName: PaidBy));
            }
        }

        public event EventHandler PaymentsUpdated;
        public event EventHandler TransactionsUpdated;
		public event EventHandler<(int PartyId, string PartyName)> PartyUpdated;

        public string PaidBy {
            get { return this.receiptService.TransactionGroup.PaidBy; }
            set { this.receiptService.TransactionGroup.PaidBy = value; }
        }

		public int PaidById { get => receiptService.TransactionGroup.PaidById; set => receiptService.TransactionGroup.PaidById = value; }

		public decimal RemainingDue => receiptService.RemainingDue;
		public decimal ConvenienceFee => receiptService.ConvenienceFee;
		public decimal TotalAmount => receiptService.TotalAmount;
		public decimal TotalReceiptAmount => receiptService.TotalReceiptAmount;
		public decimal TotalPaid => receiptService.TotalPaid;

		public decimal TotalCashPaid => receiptService.TotalCashPaid;

		public decimal TotalCheckPaid => receiptService.TotalCheckPaid;

		public decimal TotalCreditPaid => receiptService.TotalCreditPaid;

		public decimal ChangeDue => receiptService.ChangeDue;

		public IEnumerable<ReceiptPayment> Payments => receiptService.Payments;

		private IEnumerable<GenericDescriptionPair<PaymentMethod>> FillPaymentMethods()
        {
            return new List<GenericDescriptionPair<PaymentMethod>>()
            {
                new GenericDescriptionPair<PaymentMethod>(){Description = "Cash", ID = PaymentMethod.Cash},
                new GenericDescriptionPair<PaymentMethod>(){Description = "Check", ID = PaymentMethod.Check},
                new GenericDescriptionPair<PaymentMethod>(){Description = "Credit Card", ID = PaymentMethod.Credit}
            };
        }

		public bool IsReceiptPrinterSetUp()
		{
			return receiptService.IsReceiptPrinterSetUp();
		}

		public int CashOut()
        {
            if (AsPendingTransaction)
            {
                receiptService.DefaultCloseout = -100;
            }
            else
            {
                receiptService.DefaultCloseout = 0;
            }
			var cashOutResponse = receiptService.CashOut();
            savedReceipt = cashOutResponse.receipt;
			return savedReceipt?.ReceiptNumber.GetValueOrDefault() ??  0;
        }

        public void PrintReceipt(int copies)
        {
			receiptService.PrintReceipt(copies, savedReceipt);
        }

        public bool ShouldPrintReceipt { get => receiptService.TransactionGroup.PrintReceipt; set => receiptService.TransactionGroup.PrintReceipt = value; }
        public int Copies { get => receiptService.TransactionGroup.Copies; set => receiptService.TransactionGroup.Copies = value; }

        public void StartNewTransaction()
        {
            commandDispatcher.Send(new StartCRTransaction(TransactionGroup.TellerId, false));
        }

        public void UpdateTransactionSplit(Guid transactionId, int split)
        {
	        var trans = receiptService.TransactionGroup.GetTransaction(transactionId);

	        if (trans.Split != split)
	        {
		        trans.Split = split;
                receiptService.TransactionGroup.AddOrUpdateTransaction(new List<TransactionBase>{trans});
	        }
        }
    }
}