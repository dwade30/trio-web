﻿using System;
using System.Linq;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
    public class TransactionCompletedEventHandler : ReceiptingEventHandlerBase<TransactionCompleted>
    {
        private CRTransactionGroupRepository transactionGroupRepository;
        private IView<ICRCashOutViewModel> transactionSummaryView;
        public TransactionCompletedEventHandler(CRTransactionGroupRepository transactionGroupRepository,IView<ICRCashOutViewModel> transactionSummaryView)
        {
            this.transactionGroupRepository = transactionGroupRepository;
            this.transactionSummaryView = transactionSummaryView;
            
        }
        protected override void Handle(TransactionCompleted completedEvent)
        {
            var crTransactionGroup = transactionGroupRepository.Get(completedEvent.CorrelationId);
            if (crTransactionGroup != null)
            {
	           crTransactionGroup.AddOrUpdateTransaction(completedEvent.CompletedTransactions);
				transactionSummaryView.ViewModel.SetTransactionGroup(crTransactionGroup);
               transactionSummaryView.ViewModel.AsPendingTransaction = crTransactionGroup.StartedAsPendingTransaction;
               if (completedEvent.OverridePaidByName != "")
               {
                   transactionSummaryView.ViewModel.PaidBy = completedEvent.OverridePaidByName;
                   transactionSummaryView.ViewModel.PaidById = completedEvent.OverridePaidByPartyId;
               }
               transactionSummaryView.Show();
            }
        }
    }
}