﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.Authorization;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CentralData;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
    public class GetTellerIdViewModel : IGetTellerIdViewModel
    {
        private CommandDispatcher commandDispatcher;
        private GlobalCashReceiptSettings crSettings;
        private IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>> settingsHandler;
        private IUserPermissionSet userPermissions;
        public GetTellerIdViewModel(CommandDispatcher commandDispatcher, GlobalCashReceiptSettings crSettings, IQueryHandler<SettingSearchCriteria, IEnumerable<Setting>> settingsHandler, IUserPermissionSet userPermissions)
        {
            this.commandDispatcher = commandDispatcher;
            this.crSettings = crSettings;
            RequirePassword = crSettings.TellerIdUsage == TellerIDUsage.RequirePassword;
            ShowDefaultTeller = crSettings.TellerIdUsage == TellerIDUsage.RequireButDefault;
            this.userPermissions = userPermissions;
            this.settingsHandler = settingsHandler;
            if (ShowDefaultTeller)
            {
                var lastTeller = settingsHandler.ExecuteQuery(new SettingSearchCriteria
                {
	                SettingName = "CRLastTellerID",
	                OwnerType = SettingOwnerType.User,
	                SettingType =  "",
                }).FirstOrDefault();
                if (lastTeller != null)
                {
                    DefaultTeller = lastTeller.SettingValue;
                }
            }
        }
        public string ChosenTeller { get; set; } = "";
        public TellerValidationResult ValidateTeller(string tellerId,string password)
        {
            return commandDispatcher.Send<TellerValidationResult>(new ValidateTellerId(tellerId,password)).Result;
        }

        public bool RequirePassword { get; private set; }
        public bool ShowDefaultTeller { get; private set; }
        public string DefaultTeller { get; private set; } = "";
    }
}