﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class CreditCardType
	{
		public int ID { get; set; }
		public string Type { get; set; }
		public bool? IsDebit { get; set; }
		public bool? IsVISA { get; set; }
		public bool? SystemDefined { get; set; }
	}
}
