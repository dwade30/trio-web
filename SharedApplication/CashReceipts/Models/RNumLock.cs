﻿namespace SharedApplication.CashReceipts.Models
{
    public class RNumLock
    {
        public int ID { get; set; }
        public bool? ReceiptNumberLock { get; set; }
        public int? NextReceiptNumber { get; set; }
        public string OpID { get; set; }
    }
}