﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Enums;

namespace SharedApplication.CashReceipts.Models
{
    public class AccountInfo
    {
        public int Account { get; set; } = 0;
        public AccountType Type { get; set; } = AccountType.None;
    }
}
