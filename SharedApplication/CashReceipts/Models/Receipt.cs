﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public partial class Receipt
	{
		public int Id { get; set; }

		public int? ReceiptNumber { get; set; }

		public string TellerID { get; set; }

		public DateTime? ReceiptDate { get; set; }

		public string PaidBy { get; set; }

		public double? Change { get; set; }

		public double? CashAmount { get; set; }

		public double? CheckAmount { get; set; }

		public double? CardAmount { get; set; }

		public bool? MultipleChecks { get; set; }

		public bool? MultipleCC { get; set; }

		public bool? EFT { get; set; }

		public bool? IsEPayment { get; set; }

		public decimal? ConvenienceFee { get; set; }

		public int? PaidByPartyID { get; set; }

		public Guid? ReceiptIdentifier { get; set; }
        public bool? IsVoided { get; set; } = false;

		public ICollection<Archive> Archives { get; set; }
		public ICollection<CheckMaster> CheckMasters { get; set; }
		public ICollection<CreditCardMaster> CreditCardMasters { get; set; }
	}
}
