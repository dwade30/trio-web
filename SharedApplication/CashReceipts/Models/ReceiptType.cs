﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public partial class ReceiptType

	{
		public int ID { get; set; }

		public int? TypeCode { get; set; }

		public string TypeTitle { get; set; }

		public string Title1 { get; set; }

		public string Title1Abbrev { get; set; }

		public string Account1 { get; set; }

		public double? DefaultAmount1 { get; set; }

		public string Title2 { get; set; }

		public string Title2Abbrev { get; set; }

		public string Account2 { get; set; }

		public double? DefaultAmount2 { get; set; }

		public string Title3 { get; set; }

		public string Title3Abbrev { get; set; }

		public string Account3 { get; set; }

		public double? DefaultAmount3 { get; set; }

		public string Title4 { get; set; }

		public string Title4Abbrev { get; set; }

		public string Account4 { get; set; }

		public double? DefaultAmount4 { get; set; }

		public string Title5 { get; set; }

		public string Title5Abbrev { get; set; }

		public string Account5 { get; set; }

		public double? DefaultAmount5 { get; set; }

		public string Title6 { get; set; }

		public string Title6Abbrev { get; set; }

		public string Account6 { get; set; }

		public double? DefaultAmount6 { get; set; }

		public bool? BMV { get; set; }

		public bool? ChangeType { get; set; }

		public bool? Year1 { get; set; }

		public bool? Year2 { get; set; }

		public bool? Year3 { get; set; }

		public bool? Year4 { get; set; }

		public bool? Year5 { get; set; }

		public bool? Year6 { get; set; }

		public string Reference { get; set; }

		public bool? ReferenceMandatory { get; set; }

		public string Control1 { get; set; }

		public bool? Control1Mandatory { get; set; }

		public string Control2 { get; set; }

		public bool? Control2Mandatory { get; set; }

		public string Control3 { get; set; }

		public bool? Control3Mandatory { get; set; }

		public short? Copies { get; set; }

		public bool? Print { get; set; }

		public string DefaultAccount { get; set; }

		public bool? ShowFeeDetailOnReceipt { get; set; }

		public bool? RBType { get; set; }

		public bool? PercentageFee1 { get; set; }

		public bool? PercentageFee2 { get; set; }

		public bool? PercentageFee3 { get; set; }

		public bool? PercentageFee4 { get; set; }

		public bool? PercentageFee5 { get; set; }

		public bool? PercentageFee6 { get; set; }

		public bool? EFT { get; set; }

		public int? TownKey { get; set; }

		public bool? MOSES { get; set; }

		public string Project1 { get; set; }

		public string Project2 { get; set; }

		public string Project3 { get; set; }

		public string Project4 { get; set; }

		public string Project5 { get; set; }

		public string Project6 { get; set; }

		public bool? Deleted { get; set; }

		public bool? ConvenienceFeeOverride { get; set; }

		public string ConvenienceFeeMethod { get; set; }

		public string ConvenienceFeeGLAccount { get; set; }

		public decimal? ConvenienceFlatAmount { get; set; }

		public double? ConveniencePercentage { get; set; }

		public string EPmtAcctID { get; set; }

        public string GetTitle(int titleNumber)
        {
            switch (titleNumber)
            {
				case 1:
                    return Title1;
                    break;
				case 2:
                    return Title2;
                    break;
				case 3:
                    return Title3;
                    break;
				case 4:
                    return Title4;
                    break;
				case 5:
                    return Title5;
                    break;
				case 6:
                    return Title6;
                    break;
            }

            return "";
        }

        public string GetAbbreviation(int titleNumber)
        {
            switch (titleNumber)
            {
                case 1:
                    return Title1Abbrev;
                    break;
                case 2:
                    return Title2Abbrev;
                    break;
                case 3:
                    return Title3Abbrev;
                    break;
                case 4:
                    return Title4Abbrev;
                    break;
                case 5:
                    return Title5Abbrev;
                    break;
                case 6:
                    return Title6Abbrev;
                    break;
            }

            return "";
		}

        public string GetAccount(int categoryNumber)
        {
            switch (categoryNumber)
            {
                case 1:
                    return Account1;
                    break;
                case 2:
                    return Account2;
                    break;
                case 3:
                    return Account3;
                    break;
                case 4:
                    return Account4;
                    break;
                case 5:
                    return Account5;
                    break;
                case 6:
                    return Account6;
                    break;
            }

            return "";
		}
	}
}
