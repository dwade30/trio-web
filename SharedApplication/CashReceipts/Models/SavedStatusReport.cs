﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class SavedStatusReport
	{
		public int Id { get; set; }
		public string ReportName { get; set; }
		public string Type { get; set; }
		public string SQL { get; set; }
		public DateTime? LastUpdated { get; set; }
		public string WhereSelection { get; set; }
		public string SortSelection { get; set; }
		public string FieldConstraint0 { get; set; }
		public string FieldConstraint1 { get; set; }
		public string FieldConstraint2 { get; set; }
		public string FieldConstraint3 { get; set; }
		public string FieldConstraint4 { get; set; }
		public string FieldConstraint5 { get; set; }
		public string FieldConstraint6 { get; set; }
		public string FieldConstraint7 { get; set; }
		public string FieldConstraint8 { get; set; }
		public string FieldConstraint9 { get; set; }
		public string FieldConstraint10 { get; set; }
		public string FieldConstraint11 { get; set; }
		public string FieldConstraint12 { get; set; }
		public string FieldConstraint13 { get; set; }
		public string FieldConstraint14 { get; set; }
		public string Ans0 { get; set; }
		public string Ans1 { get; set; }
		public string Ans2 { get; set; }
		public string Ans3 { get; set; }
		public string Ans4 { get; set; }
		public string Ans5 { get; set; }
		public string Ans6 { get; set; }
		public string Ans7 { get; set; }
		public string Ans8 { get; set; }
		public string Ans9 { get; set; }
	}
}
