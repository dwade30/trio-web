﻿namespace SharedApplication.CashReceipts.Models
{
    public class ServiceCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

        public void Copy(ServiceCode serviceCode)
        {
            this.Code = serviceCode.Code;
            this.Description = serviceCode.Description;
        }

        public void CopyAll(ServiceCode serviceCode)
        {
            this.Id = serviceCode.Id;
            Copy(serviceCode);
        }
    }
}