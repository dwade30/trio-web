﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ReceiptSearchSummaryDetailInformation
	{
		public string Title { get; set; }
		public string GLAccount { get; set; }
		public int Count { get; set; }
		public decimal Amount { get; set; }
	}
}
