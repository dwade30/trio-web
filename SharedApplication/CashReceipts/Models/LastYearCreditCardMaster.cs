﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class LastYearCreditCardMaster
	{
		public int ID { get; set; }
		public int? ReceiptId { get; set; }
		public int? CCType { get; set; }
		public double? Amount { get; set; }
		public string EPymtRefNumber { get; set; }
		public string Last4Digits { get; set; }
		public int? OriginalReceiptKey { get; set; }
		public string CryptCard { get; set; }
		public string CryptCardExpiration { get; set; }
		public decimal? ConvenienceFee { get; set; }
		public string MaskedCreditCardNumber { get; set; }
		public string AuthCode { get; set; }
		public bool? IsVISA { get; set; }
		public bool? IsDebit { get; set; }
		public decimal? NonTaxAmount { get; set; }
		public string AuthCodeConvenienceFee { get; set; }
		public string AuthCodeVISANonTax { get; set; }
		public decimal? NonTaxConvenienceFee { get; set; }
		public string AuthCodeNonTaxConvenienceFee { get; set; }
		public string SecureGUID { get; set; }
		public string EPmtAcctID { get; set; }
        public Guid CCPaymentIdentifier { get; set; } = Guid.NewGuid();
		public LastYearReceipt Receipt { get; set; }
	}
}
