﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ReceiptSearchSummaryInformation
	{
		public int TypeCode { get; set; }
		public string TypeTitle { get; set; }
		public string TellerId { get; set; }
		public DateTime? TransactionDate { get; set; }
		public int Count { get; set; }
		public decimal Amount { get; set; }

	}
}
