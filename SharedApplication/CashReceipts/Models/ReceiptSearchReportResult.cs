﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ReceiptSearchReportResult
	{
		public int DailyCloseOut { get; set; }
		public DateTime? ActualSystemDate { get; set; }
		public int ReceiptNumber { get; set; }
		public int ReceiptType { get; set; }
		public int AccountNumber { get; set; }
		public string Reference { get; set; }
		public string Control1 { get; set; }
		public string Control2 { get; set; }
		public string Control3 { get; set; }
		public string Control1Label { get; set; }
		public string Control2Label { get; set; }
		public string Control3Label { get; set; }
		public string TellerID { get; set; }
		public string Name { get; set; }
		public DateTime? RecordedTransactionDate { get; set; }
		public DateTime? ReceiptDate { get; set; }
		public string DefaultCashAccount { get; set; }
		public decimal Change { get; set; }
		public decimal CashAmount { get; set; }
		public decimal CheckAmount { get; set; }
		public decimal CardAmount { get; set; }
		public decimal Amount1 { get; set; }
		public decimal Amount2 { get; set; }
		public decimal Amount3 { get; set; }
		public decimal Amount4 { get; set; }
		public decimal Amount5 { get; set; }
		public decimal Amount6 { get; set; }
		public string Account1 { get; set; }
		public string Account2 { get; set; }
		public string Account3 { get; set; }
		public string Account4 { get; set; }
		public string Account5 { get; set; }
		public string Account6 { get; set; }
		public string Title1 { get; set; }
		public string Title2 { get; set; }
		public string Title3 { get; set; }
		public string Title4 { get; set; }
		public string Title5 { get; set; }
		public string Title6 { get; set; }
		public string Comment { get; set; }
		public string PaidBy { get; set; }
		public string DefaultAccount { get; set; }
		public int ARBillType { get; set; }
		public string ConvenienceFeeGLAccount { get; set; }
		public decimal ConvenienceFee { get; set; }
		public decimal CashPaidAmount { get; set; }
		public decimal CardPaidAmount { get; set; }
		public decimal CheckPaidAmount { get; set; }
		public bool MultipleLineItemsOnReceipt { get; set; }
		public IEnumerable<ReceiptSearchReportCheckResult> Checks { get; set; }
	}
}
