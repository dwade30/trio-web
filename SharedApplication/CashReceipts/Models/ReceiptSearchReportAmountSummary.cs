﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ReceiptSearchReportAmountSummary
	{
		public decimal Amount1Total { get; set; }
		public decimal Amount2Total { get; set; }
		public decimal Amount3Total { get; set; }
		public decimal Amount4Total { get; set; }
		public decimal Amount5Total { get; set; }
		public decimal Amount6Total { get; set; }
		public decimal CashTotal { get; set; }
		public decimal CheckTotal { get; set; }
		public decimal CardTotal { get; set; }

	}
}
