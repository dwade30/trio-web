﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class CheckMaster
	{
		public int ID { get; set; }
		public int? ReceiptId { get; set; }
		public string CheckNumber { get; set; }
		public int? BankId { get; set; }
		public double? Amount { get; set; }
		public bool? EFT { get; set; }
		public string EPymtRefNumber { get; set; }
		public string Last4Digits { get; set; }
		public string AccountType { get; set; }
		public int? OriginalReceiptKey { get; set; }
		public Receipt Receipt { get; set; }

	}
}
