﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ReceiptSearchMotorVehicleSummaryInformation
	{
		public string RegistrationType { get; set; }
		public int Count { get; set; }
		public decimal Amount { get; set; }
	}
}
