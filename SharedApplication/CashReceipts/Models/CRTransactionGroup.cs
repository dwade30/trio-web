﻿using System;
using System.Collections.Generic;
using System.Linq;

using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
//using SharedApplication.Clerk.Receipting;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
    public class CRTransactionGroup
    {
        public Guid ReceiptIdentifier { get; set; } = new Guid();
        public string TellerId { get; set; } = "";
        public string PaidBy { get; set; } = "";
        public int PaidById { get; set; } = 0;
        public bool PrintReceipt { get; set; } = true;
        public int Copies { get; set; } = 1;
        public DateTime TransactionDate { get; set; }
        public bool StartedAsPendingTransaction { get; set; } = false;
        public decimal TotalAmount {
            get { return transactions.Sum(s => s.CashDrawerTotal); }
        }

        private List<TransactionBase> transactions = new List<TransactionBase>();

        public void AddTransaction(TransactionBase transaction)
        {
            if (transaction != null)
            {
                transactions.Add(transaction);
                if (String.IsNullOrWhiteSpace(PaidBy))
                {
                    PaidBy = transaction.PayerName;
                    PaidById = transaction.PayerPartyId;
                }
            }
        }

        public void AddTransactions(IEnumerable<TransactionBase> transactionsToAdd)
        {
            if (transactionsToAdd != null)
            {
                if (transactionsToAdd.Any())
                {
                    transactions.AddRange(transactionsToAdd);
                    if (String.IsNullOrWhiteSpace(PaidBy))
                    {
                        var tran = transactions.FirstOrDefault();
                        PaidBy = tran.PayerName;
                        PaidById = tran.PayerPartyId;
                    }
                }
            }
        }

        public void AddOrUpdateTransaction(IEnumerable<TransactionBase> passedTransactions)
        {
	        if (passedTransactions != null)
	        {
                //separate loops because one transaction can have multiple parts.  Don't want to erase the item you just put in but do want to erase previous instances
		        foreach (var passedTransaction in passedTransactions)
		        {
			        var existingTransaction = transactions.FirstOrDefault(x => x.Id == passedTransaction.Id);

			        if (existingTransaction != null)
			        {
				        transactions.Remove(existingTransaction);
			        }
                }
                foreach (var passedTransaction in passedTransactions)
                {
                    AddTransaction(passedTransaction);
                }
            }
        }

        public bool RemoveTransaction(Guid transactionId)
        {
	        try
	        {
				transactions.RemoveAll(x => x.Id == transactionId);
				return true;
	        }
	        catch (Exception e)
	        {
		        Console.WriteLine(e);
		        return false;
	        }
	       
        }

		public IEnumerable<TransactionBase> GetTransactions()
        {
            return transactions;

        }

		public TransactionBase GetTransaction(Guid transactionId)
		{
			return transactions.FirstOrDefault(x => x.Id == transactionId);
		}

        public bool HasTransactions()
        {
            return transactions.Any();
        }
    }
}