﻿using System;

namespace SharedApplication.CashReceipts.Models
{
    public class ElectronicPaymentLog
    {
        public int Id { get; set; } = 0;
        public Guid TransactionGroupId { get; set; }
        public string StatusCode { get; set; } = "";
        public decimal Amount { get; set; } = 0;
        public decimal Fee { get; set; } = 0;
        public string ReferenceId { get; set; } = "";
        public string ResponseData { get; set; } = "";
        public bool Success { get; set; } = false;
        public string ErrorDescription { get; set; } = "";
        public string MerchantCode { get; set; } = "";
        public bool InTestMode { get; set; } = false;
        public Guid ElectronicPaymentIdentifier { get; set; }
        public string TellerId { get; set; } = "";
        public DateTime? PaymentTime { get; set; } = DateTime.Now;

        public decimal TotalAmount()
        {
            return Amount + Fee;
        }
    }
}