﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class CreditCardMaster
	{
		public int ID { get; set; }
        public int? ReceiptId { get; set; } = 0;
        public int? CCType { get; set; } = 0;
        public double? Amount { get; set; } = 0;
        public string EPymtRefNumber { get; set; } = "";
        public string Last4Digits { get; set; } = "";
        public int? OriginalReceiptKey { get; set; } = 0;
        public string CryptCard { get; set; } = "";
        public string CryptCardExpiration { get; set; } = "";
		public decimal? ConvenienceFee { get; set; }
        public string MaskedCreditCardNumber { get; set; } = "";
		public string AuthCode { get; set; }
        public bool? IsVISA { get; set; } = false;
        public bool? IsDebit { get; set; } = false;
        public decimal? NonTaxAmount { get; set; } = 0;
        public string AuthCodeConvenienceFee { get; set; } = "";
        public string AuthCodeVISANonTax { get; set; } = "";
        public decimal? NonTaxConvenienceFee { get; set; } = 0;
        public string AuthCodeNonTaxConvenienceFee { get; set; } = "";
        public string SecureGUID { get; set; } = "";
        public string EPmtAcctID { get; set; } = "";
        public Guid CCPaymentIdentifier { get; set; } = Guid.NewGuid();
		public Receipt Receipt { get; set; }
        
	}
}
