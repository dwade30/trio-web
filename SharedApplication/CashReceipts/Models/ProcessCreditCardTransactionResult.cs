﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ProcessCreditCardTransactionResult
	{
		public string ErrorCode { get; set; }
		public string Result { get; set; }
		public string ErrorDesc { get; set; }
		public string RefNum { get; set; }
		public string TransID { get; set; }
		public string AuthCode { get; set; }
		public string MaskedCreditCard { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Expiration { get; set; }
	}
}
