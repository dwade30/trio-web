﻿namespace SharedApplication.CashReceipts.Models
{
    public class CRBank
    {
        public int ID { get; set; }
        public string RoutingTransit { get; set; }
        public string Name { get; set; }
    }
}