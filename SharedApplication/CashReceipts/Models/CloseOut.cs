﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class CloseOut
	{
		public int Id { get; set; }
		public string Type { get; set; }
		public DateTime? CloseOutDate { get; set; }
		public string TellerId { get; set; }
	}
}
