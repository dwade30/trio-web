﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class MosesReceipt
	{
		public int ReceiptNumber { get; set; } = 0;
		public string TransactionDate { get; set; } = "";
		public string TransactionType { get; set; } = "";
		public string AuthItemCode { get; set; } = "";
		public string CustomerName { get; set; } = "";
		public string DocumentNumber { get; set; } = "";
		public string RegistrationNumber { get; set; } = "";
		public string InventoryItemName { get; set; } = "";
		public string InventorySerialNumber { get; set; } = "";
		public string InventorySerialNumber2 { get; set; } = "";
		public string Blank1 { get; set; } = "";
		public string ClerkId { get; set; } = "";
		public Decimal StateFee { get; set; } = 0;
		public Decimal AgentFee { get; set; } = 0;
		public Decimal ExciseTax { get; set; } = 0;
		public Decimal SalesTax { get; set; } = 0;
		public string PaymentType { get; set; }= "";
	}
}
