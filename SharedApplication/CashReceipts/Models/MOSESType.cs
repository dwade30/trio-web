﻿namespace SharedApplication.CashReceipts.Models
{
    public class MOSESType
    {
        public int ID { get; set; }
        public int MOSESTYPE { get; set; }
        public int? CRType { get; set; }
        public string MOSESTypeDescription { get; set; }
    }
}