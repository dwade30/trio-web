﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class ReceiptSearchReportCheckResult
	{
		public string CheckNumber { get; set; }
		public decimal Amount { get; set; }
	}
}
