﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public class Cya
	{
		public int Id { get; set; }
		public string UserID { get; set; }
		public DateTime? CYADate { get; set; } = DateTime.Today;
		public DateTime? CYATime { get; set; } = DateTime.Now;
		public string Description1 { get; set; } = "";
		public string Description2 { get; set; } = "";
		public string Description3 { get; set; } = "";
		public string Description4 { get; set; } = "";
		public string Description5 { get; set; } = "";
		public bool? boolUseSecurity { get; set; } = true;
	}

}
