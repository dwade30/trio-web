﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Models
{
	public partial class Archive
	{
		public int Id { get; set; }
		public int? ReceiptType { get; set; }
		public int? AccountNumber { get; set; }
		public string ReceiptTitle { get; set; }
		public string Account1 { get; set; }
		public double? Amount1 { get; set; }
		public string Account2 { get; set; }
		public double? Amount2 { get; set; }
		public string Account3 { get; set; }
		public double? Amount3 { get; set; }
		public string Account4 { get; set; }
		public double? Amount4 { get; set; }
		public string Account5 { get; set; }
		public double? Amount5 { get; set; }
		public string Account6 { get; set; }
		public double? Amount6 { get; set; }
		public string Comment { get; set; }
		public string Ref { get; set; }
		public string Control1 { get; set; }
		public string Control2 { get; set; }
		public string Control3 { get; set; }
		public int? ReceiptId { get; set; }
		public short? Split { get; set; }
		public string TellerID { get; set; }
		public string Name { get; set; }
		public DateTime? ArchiveDate { get; set; }
		public DateTime? ActualSystemDate { get; set; }
		public string CollectionCode { get; set; }
		public int? TellerCloseOut { get; set; }
		public int? DailyCloseOut { get; set; }
		public bool? AffectCashDrawer { get; set; }
		public bool? AffectCash { get; set; }
		public string DefaultAccount { get; set; }
		public int? ArchiveCode { get; set; }
		public string DefaultCashAccount { get; set; }
		public int? Quantity { get; set; }
		public string DefaultMIAccount { get; set; }
		public bool? EFT { get; set; }
		public int? RecordKey { get; set; }
		public int? TownKey { get; set; }
		public string Project1 { get; set; }
		public string Project2 { get; set; }
		public string Project3 { get; set; }
		public string Project4 { get; set; }
		public string Project5 { get; set; }
		public string Project6 { get; set; }
		public int? ARBillType { get; set; }
		public string SeperateCreditCardGLAccount { get; set; } = "";
		public decimal? ConvenienceFee { get; set; } = 0;
		public string ConvenienceFeeGLAccount { get; set; } = "";
		public decimal? CardPaidAmount { get; set; }
		public decimal? CheckPaidAmount { get; set; }
		public decimal? CashPaidAmount { get; set; }
		public int? NamePartyID { get; set; }
        public Guid? ReceiptIdentifier { get; set; }
        public Guid? TransactionIdentifier { get; set; }
        public Receipt Receipt { get; set; }

	}
}
