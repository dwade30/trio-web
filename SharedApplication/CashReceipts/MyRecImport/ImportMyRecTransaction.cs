﻿using System;
using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.CashReceipts.MyRecImport
{
    public class ImportMyRecTransaction : Command, MakeTransaction
    {
        public Guid CorrelationId { get; set; }
        public string FileName { get; }
        public ImportMyRecTransaction(Guid correlationId,Guid id,string fileName)
        {
            FileName = fileName;
            CorrelationId = correlationId;
            Id = id;
        }
    }
}