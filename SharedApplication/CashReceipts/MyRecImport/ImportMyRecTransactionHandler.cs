﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.MyRecImport
{
    public class ImportMyRecTransactionHandler : CommandHandler<ImportMyRecTransaction>
    {
        private IMyRecReceiptImporter receiptImporter;
        private EventPublisher eventPublisher;

        public ImportMyRecTransactionHandler(IMyRecReceiptImporter receiptImporter, EventPublisher eventPublisher)
        {
            this.receiptImporter = receiptImporter;
            this.eventPublisher = eventPublisher;
        }

        protected override void Handle(ImportMyRecTransaction command)
        {
           var results = receiptImporter.ImportFile(command.FileName,command.CorrelationId);
           var error = results.exception;
           var transactions = results.transactions;
           if (error == null && transactions != null && transactions.Any())
           {
               eventPublisher.Publish(new TransactionCompleted
               {
                   CorrelationId = command.CorrelationId,
                   CompletedTransactions = transactions.Select(t => t.ToTransactionBase())
               });
            }
           else
           {
                eventPublisher.Publish(new TransactionCancelled(command.CorrelationId, new List<string>(){error.Message}));
           }
        }
    }
}