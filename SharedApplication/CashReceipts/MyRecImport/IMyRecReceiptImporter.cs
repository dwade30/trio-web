﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CashReceipts.MyRecImport
{
    public interface IMyRecReceiptImporter
    {
        (IEnumerable<MiscReceiptTransaction> transactions, Exception exception) ImportFile(string fileName,Guid correlationId);
        Exception LastException { get;  }
    }
}