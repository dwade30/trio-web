﻿using System;

namespace SharedApplication.CashReceipts.MyRecImport
{
    public class MyRecImportRecord
    {
        public string RecordType { get; set; } = "";
        public int InvoiceID { get; set; } = 0;
        public string Item { get; set; }
        public string Code { get; set; }
        public string Member { get; set; }
        public int TransactionNumber { get; set; }
        public string Account { get; set; }
        public string BillingName { get; set; }
        public string Type { get; set; }
        public string AuthID { get; set; }
        public string AccountNumber { get; set; }
        public string BudgetGroup { get; set; }
        public DateTime Date { get; set; }
        public string CreatedBy { get; set; }
        public string Amount { get; set; }
        public string Disbursed { get; set; }
        public int ReceiptFeeCode { get; set; } = 0;
        public int ReceiptCode { get; set; } = 0;
    }
}