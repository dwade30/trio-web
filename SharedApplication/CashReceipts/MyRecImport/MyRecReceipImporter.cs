﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections;

namespace SharedApplication.CashReceipts.MyRecImport
{
    public class MyRecReceiptImporter : IMyRecReceiptImporter
    {
        private ITrioFileSystem fileSystem;
        private IReceiptTypesService receiptTypesService;
        public MyRecReceiptImporter(ITrioFileSystem fileSystem, IReceiptTypesService receiptTypesService)
        {
            this.fileSystem = fileSystem;
            this.receiptTypesService = receiptTypesService;
        }
        public Exception LastException { get; private set; } = null;
        public (IEnumerable<MiscReceiptTransaction> transactions, Exception exception) ImportFile(string fileName, Guid correlationId)
        {
            LastException = null;
            var transactions = new List<MiscReceiptTransaction>();
            if (fileSystem.Exists(fileName))
            {
                
                using (var reader = new StreamReader(fileSystem.OpenFileStream(fileName, FileMode.Open, FileAccess.Read)))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.RegisterClassMap<MyRecMap>();
                    List<MyRecImportRecord> records = new List<MyRecImportRecord>();
                    decimal totalPaid = 0;
                    decimal totalDisbursed = 0;
                    try
                    {
                        var rawRecords = csv.GetRecords<MyRecImportRecord>();
                        foreach (var myRecImport in rawRecords)
                        {
                            if (!myRecImport.RecordType.IsNullOrWhiteSpace())
                            {
                                var encodedReceiptInfo = myRecImport.AccountNumber.ToString();
                                myRecImport.ReceiptCode =
                                    encodedReceiptInfo.Substring(0, encodedReceiptInfo.Length - 1).ToIntegerValue();
                                myRecImport.ReceiptFeeCode = encodedReceiptInfo.Right(1).ToIntegerValue();
                                records.Add(myRecImport);
                            }
                        }
                    }
                    catch (CsvHelperException ex)
                    {
                        //do nothing, this is caused by the total line which doesn't match the format
                    }

                    if (records.Any())
                    {
                        var receiptCodes = records.GroupBy(r => r.ReceiptCode, r => r.ReceiptCode, (code, codes) => code);
                        var receiptFeeCodes = records.GroupBy(r => r.ReceiptFeeCode, r => r.ReceiptFeeCode,
                            (feeCode, feeCodes) => feeCode);
                        var minCode = receiptFeeCodes.Min();
                        if (minCode < 1 )
                        {
                            LastException = new InvalidReceiptFeeCode(minCode,0)
                            {
                                ReceiptFeeCode = receiptFeeCodes.Min()
                            };
                            return (transactions,LastException);
                        }

                        var maxCode = receiptFeeCodes.Max();
                        if (maxCode > 6)
                        {
                            LastException = new InvalidReceiptFeeCode(maxCode,0)
                            {
                                ReceiptFeeCode = receiptFeeCodes.Max()
                            };
                            return (transactions, LastException);
                        }

                        totalPaid = records.Sum(r => r.Amount.ToDecimalValue());
                        totalDisbursed = records.Sum(r => r.Disbursed.ToDecimalValue());
                        if (totalPaid != totalDisbursed)
                        {
                            LastException = new MyRecDisbursedDoesntEqualPaid(totalDisbursed, totalPaid);
                            return (transactions, LastException);
                        }
                        
                        foreach (var receiptCode in receiptCodes)
                        {
                            var receiptType = receiptTypesService.ReceiptTypes.FirstOrDefault(r =>
                                r.TypeCode == receiptCode);
                            if (receiptType == null)
                            {
                                LastException = new UnknownReceiptCode(receiptCode.ToString());
                                return (transactions, LastException);
                            }
                        }

                        foreach (var myRecImportRecord in records)
                        {
                            var receiptType =
                                receiptTypesService.ReceiptTypes.FirstOrDefault(r =>
                                    r.TypeCode == myRecImportRecord.ReceiptCode);
                            var categoryTitle = receiptType.GetTitle(myRecImportRecord.ReceiptFeeCode);

                            if (categoryTitle.IsNullOrWhiteSpace())
                            {
                                LastException = new InvalidReceiptFeeCode( myRecImportRecord.ReceiptFeeCode, myRecImportRecord.ReceiptCode);
                                return (transactions, LastException);
                            }

                            var transaction = new MiscReceiptTransaction()
                            {
                                TransactionTypeCode = receiptType.TypeCode.GetValueOrDefault(),
                                PaidBy = myRecImportRecord.BillingName,
                                AffectCash = true,
                                AffectCashDrawer = true,
                                Comment = myRecImportRecord.Item,
                                Quantity = 1,
                                CollectionCode = myRecImportRecord.RecordType,
                                CorrelationIdentifier = correlationId,
                                Reference =  myRecImportRecord.InvoiceID.ToString(),
                                TransactionTypeDescription = receiptType.TypeTitle
                            };
                            transaction.Controls.Add(new ControlItem(){Description = "Control1",Name="Control1",Value = myRecImportRecord.TransactionNumber.ToString()});
                            transaction.Controls.Add(new ControlItem() { Description = "Control2", Name = "Control2", Value = "" });
                            transaction.Controls.Add(new ControlItem() { Description = "Control3", Name = "Control3", Value = "" });

                            for (int x = 1; x < 7; x++)
                            {
                                decimal amount = 0;
                                if (x == myRecImportRecord.ReceiptFeeCode)
                                {
                                    amount = myRecImportRecord.Disbursed.ToDecimalValue();
                                }
                                transaction.AddMiscReceiptTransactionAmount(new MiscReceiptAmountItem() { Description = "Amount" + x, Name = "Amount" + x, Total = amount });
                            }
                            transactions.Add(transaction);
                        }
                    }

                }
            }
            else
            {
                LastException = new FileNotFoundException(fileName);
            }

            return (transactions, LastException);
        }

        public class MyRecMap : ClassMap<MyRecImportRecord>
        {
            public MyRecMap()
            {
                Map(m => m.RecordType).Index(0);
                Map(m => m.InvoiceID).Name("Invoice ID");
                Map(m => m.Item).Name("Item");
                Map(m => m.Code).Name("Code");
                Map(m => m.TransactionNumber).Name("Trans #");
                Map(m => m.Account).Name("Account");
                Map(m => m.BillingName).Name("Billing Name");
                Map(m => m.Type).Name("Type");
                Map(m => m.AuthID).Name(@"AuthID/Chk #");
                Map(m => m.AccountNumber).Name("Acct #");
                Map(m => m.BudgetGroup).Name("Budget Group");
                Map(m => m.Date).Name("Date");
                Map(m => m.CreatedBy).Name("Created By");
                Map(m => m.Amount).Name("Amount");
                Map(m => m.Disbursed).Name("Disbursed");
                Map(m => m.ReceiptFeeCode).Ignore();
                Map(m => m.ReceiptCode).Ignore();
            }
        }

        public class UnknownReceiptCode : Exception
        {
            public string ReceiptCode { get; set; } = "";

            public UnknownReceiptCode(string receiptCode) : base("Receipt " + receiptCode + " is invalid")
            {
                ReceiptCode = receiptCode;
            }
        }

        public class InvalidReceiptFeeCode : Exception
        {
            public InvalidReceiptFeeCode(int receiptFeeCode, int receiptCode) : base("Receipt " + receiptCode + " category " + receiptFeeCode + " is invalid")
            {
                ReceiptFeeCode = receiptFeeCode;
                ReceiptCode = receiptCode;
            }
            public int ReceiptFeeCode { get; set; } = 0;
            public int ReceiptCode { get; set; } = 0;
            
        }

        public class MyRecDisbursedDoesntEqualPaid : Exception
        {
            public decimal Disbursed { get; set; } = 0;
            public decimal Paid { get; set; } = 0;

            public MyRecDisbursedDoesntEqualPaid(decimal disbursed, decimal paid) : base(
                "Disbursed of " + disbursed.ToString() + " does not match paid of " + paid.ToString())
            {
                Disbursed = disbursed;
                Paid = paid;
            }
        }
    }
}