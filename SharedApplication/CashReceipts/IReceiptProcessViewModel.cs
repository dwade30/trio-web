﻿using System.Collections.Generic;

namespace SharedApplication.CashReceipts
{
    public interface IReceiptProcessViewModel : IChooseTransactionTypeViewModel, IReceiptTransactionSummary
    {
        new void  SetTransactionGroup(CRTransactionGroup transactionGroup);
       bool AsPendingTransaction { get; set; }
    }
}