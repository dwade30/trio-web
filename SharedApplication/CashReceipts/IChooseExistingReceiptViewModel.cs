﻿namespace SharedApplication.CashReceipts
{
    public interface IChooseExistingReceiptViewModel
    {
        int Id { get; set; }
        bool CurrentYear { get; set; }
        bool Cancelled { get; set; }
        int ReceiptNumber { get; set; }
        bool SelectReceipt(int receiptNumber, bool currentYear);
    }
}