﻿using System.Collections.Generic;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class SelectReceiptFromList : Command<int>
    {
        public List<ReceiptInfo> ReceiptList { get; }

        public SelectReceiptFromList(List<ReceiptInfo> list)
        {
            ReceiptList = list;
        }
    }
}