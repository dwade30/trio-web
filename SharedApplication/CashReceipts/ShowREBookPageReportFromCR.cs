﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class ShowREBookPageReportFromCR : Command
    {
        public int Account { get; set; }

        public ShowREBookPageReportFromCR(int account)
        {
            Account = account;
        }
    }
}