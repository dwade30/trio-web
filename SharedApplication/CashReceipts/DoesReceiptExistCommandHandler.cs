﻿using System.Linq;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class DoesReceiptExistCommandHandler : CommandHandler<DoesReceiptExist,bool>
    {
        private ICashReceiptsContext crContext;

        public DoesReceiptExistCommandHandler(ICashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        protected override bool Handle(DoesReceiptExist command)
        {
            if (command.IsCurrentYear)
            {
                return crContext.Receipts.Any(r => r.ReceiptNumber.GetValueOrDefault() == command.ReceiptNumber);
            }

            return crContext.LastYearReceipts.Any(r => r.ReceiptNumber.GetValueOrDefault() == command.ReceiptNumber);
        }
    }
}