﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.AccountsReceivable;
using SharedApplication.AccountsReceivable.Models;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
	public class ReceiptSearchService : IReceiptSearchService
	{
		private IQueryHandler<SavedStatusReportsSearchCriteria, IEnumerable<SavedStatusReport>> savedStatusReportQueryHandler;
		private CommandDispatcher commandDispatcher;
		private IQueryHandler<ReceiptSearchReportSetupInformation, IEnumerable<ReceiptSearchReportResult>> receiptSearchReportQueryHandler;
		private IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
		private DataEnvironmentSettings environmentSettings;

		private ReceiptSearchReportSetupInformation reportSetup;
		private IEnumerable<ReceiptType> receiptTypes = new List<ReceiptType>();

		public ReceiptSearchService(IQueryHandler<SavedStatusReportsSearchCriteria, IEnumerable<SavedStatusReport>> savedStatusReportQueryHandler, CommandDispatcher commandDispatcher, IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, IQueryHandler<ReceiptSearchReportSetupInformation, IEnumerable<ReceiptSearchReportResult>> receiptSearchReportQueryHandler, DataEnvironmentSettings environmentSettings)
		{
			this.savedStatusReportQueryHandler = savedStatusReportQueryHandler;
			this.commandDispatcher = commandDispatcher;
			this.receiptTypeQueryHandler = receiptTypeQueryHandler;
			this.receiptSearchReportQueryHandler = receiptSearchReportQueryHandler;
			this.environmentSettings = environmentSettings;

			GetReceiptTypes();
		}

		private void GetReceiptTypes()
		{
			receiptTypes = receiptTypeQueryHandler.ExecuteQuery(new ReceiptTypeSearchCriteria
			{
				TownKey = environmentSettings.IsMultiTown ? 1 : 0,
				DeletedSearchOption = DeletedOption.NotDeletedOnly
			}).ToList();
		}

		public IEnumerable<ReceiptSearchSummaryDetailInformation> GetSummaryDetailData(
			ReceiptSearchSummaryInformation groupRecord, IEnumerable<ReceiptSearchReportResult> reportData)
		{
			var result = new List<ReceiptSearchSummaryDetailInformation>();
			var dataToSummarize = new List<ReceiptSearchReportResult>();
			if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Type)
			{
				dataToSummarize = reportData.Where(x => x.ReceiptType == groupRecord.TypeCode).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Teller)
			{
				dataToSummarize = reportData.Where(x => x.TellerID == groupRecord.TellerId).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Date)
			{
				dataToSummarize = reportData.Where(x => reportSetup.DateToShow == DateToShowOptions.ActualSystemDate ? x.ActualSystemDate.Value.Date == groupRecord.TransactionDate.Value.Date : x.RecordedTransactionDate.Value.Date == groupRecord.TransactionDate.Value.Date).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.DateAndTeller)
			{
				dataToSummarize = reportData.Where(x => reportSetup.DateToShow == DateToShowOptions.ActualSystemDate ? x.ActualSystemDate.Value.Date == groupRecord.TransactionDate.Value.Date : x.RecordedTransactionDate.Value.Date == groupRecord.TransactionDate.Value.Date && x.TellerID == groupRecord.TellerId).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.TypeAndDate)
			{
				dataToSummarize = reportData.Where(x => reportSetup.DateToShow == DateToShowOptions.ActualSystemDate ? x.ActualSystemDate.Value.Date == groupRecord.TransactionDate.Value.Date : x.RecordedTransactionDate.Value.Date == groupRecord.TransactionDate.Value.Date && x.ReceiptType == groupRecord.TypeCode).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.TypeAndTeller)
			{
				dataToSummarize = reportData.Where(x => x.ReceiptType == groupRecord.TypeCode && x.TellerID == groupRecord.TellerId).ToList();
			}

			result.AddRange(dataToSummarize.GroupBy(x => new {x.Title1, x.Account1})
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title1,
					GLAccount = y.Key.Account1,
					Amount = y.Sum(z => z.Amount1),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title2, x.Account2 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title2,
					GLAccount = y.Key.Account2,
					Amount = y.Sum(z => z.Amount2),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title3, x.Account3 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title3,
					GLAccount = y.Key.Account3,
					Amount = y.Sum(z => z.Amount3),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title4, x.Account4 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title4,
					GLAccount = y.Key.Account4,
					Amount = y.Sum(z => z.Amount4),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title5, x.Account5 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title5,
					GLAccount = y.Key.Account5,
					Amount = y.Sum(z => z.Amount5),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title6, x.Account6 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title6,
					GLAccount = y.Key.Account6,
					Amount = y.Sum(z => z.Amount6),
					Count = y.Count()
				}));

			return result.Where(x => x.GLAccount.Trim() != "" || x.Amount != 0).OrderBy(y => y.Title);
		}

		public IEnumerable<ReceiptSearchSummaryDetailInformation> GetMotorVehicleSummaryDetailData(
			ReceiptSearchMotorVehicleSummaryInformation groupRecord, IEnumerable<ReceiptSearchReportResult> reportData)
		{
			var result = new List<ReceiptSearchSummaryDetailInformation>();
			var dataToSummarize = reportData.Where(x => x.ReceiptType == 99 && x.Comment == groupRecord.RegistrationType).ToList();

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title1, x.Account1 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title1,
					GLAccount = y.Key.Account1,
					Amount = y.Sum(z => z.Amount1),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title2, x.Account2 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title2,
					GLAccount = y.Key.Account2,
					Amount = y.Sum(z => z.Amount2),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title3, x.Account3 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title3,
					GLAccount = y.Key.Account3,
					Amount = y.Sum(z => z.Amount3),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title4, x.Account4 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title4,
					GLAccount = y.Key.Account4,
					Amount = y.Sum(z => z.Amount4),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title5, x.Account5 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title5,
					GLAccount = y.Key.Account5,
					Amount = y.Sum(z => z.Amount5),
					Count = y.Count()
				}));

			result.AddRange(dataToSummarize.GroupBy(x => new { x.Title6, x.Account6 })
				.Select(y => new ReceiptSearchSummaryDetailInformation
				{
					Title = y.Key.Title6,
					GLAccount = y.Key.Account6,
					Amount = y.Sum(z => z.Amount6),
					Count = y.Count()
				}));

			return result.Where(x => x.GLAccount.Trim() != "" || x.Amount != 0).OrderBy(y => y.Title);
		}

		public IEnumerable<ReceiptSearchMotorVehicleSummaryInformation> SummarizeMotorVehicleData(
			IEnumerable<ReceiptSearchReportResult> reportData)
		{
			var result = new List<ReceiptSearchMotorVehicleSummaryInformation>();
			
			result = reportData.Where(x => x.ReceiptType == 99).GroupBy(r => r.Comment)
				.Select(gr => new ReceiptSearchMotorVehicleSummaryInformation
				{
					RegistrationType = gr.Key,
					Count = gr.Count(),
					Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
				}).OrderBy(y => y.RegistrationType).ToList();

			return result;
		}

		public IEnumerable<ReceiptSearchSummaryInformation> SummarizeData(
			IEnumerable<ReceiptSearchReportResult> reportData)
		{
			var result = new List<ReceiptSearchSummaryInformation>();

			if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Type)
			{
				result = reportData.GroupBy(r => r.ReceiptType)
					.Select(gr => new ReceiptSearchSummaryInformation
						{
							TypeCode = (int)gr.Key,
							TypeTitle = receiptTypes.FirstOrDefault(x => x.TypeCode == (int)gr.Key)?.TypeTitle ?? "",
							Count = gr.Count(),
							TellerId = "",
							TransactionDate =  null,
							Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
						}).OrderBy(y => y.TypeCode).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Teller)
			{
				result = reportData.GroupBy(r => r.TellerID)
					.Select(gr => new ReceiptSearchSummaryInformation
					{
						TypeCode = 0,
						TypeTitle = "",
						Count = gr.Count(),
						TellerId = gr.Key,
						TransactionDate = null,
						Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
					}).OrderBy(y => y.TellerId).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Date)
			{
				result = reportData.GroupBy(r => reportSetup.DateToShow == DateToShowOptions.ActualSystemDate ?  r.ActualSystemDate.Value.Date : r.RecordedTransactionDate.Value.Date)
					.Select(gr => new ReceiptSearchSummaryInformation
					{
						TypeCode = 0,
						TypeTitle = "",
						Count = gr.Count(),
						TellerId = "",
						TransactionDate = (DateTime)gr.Key,
						Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
					}).OrderBy(y => y.TransactionDate).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.DateAndTeller)
			{
				result = reportData.GroupBy(r => new { TransactionDate = (reportSetup.DateToShow == DateToShowOptions.ActualSystemDate ? r.ActualSystemDate.Value.Date : r.RecordedTransactionDate.Value.Date), r.TellerID})
					.Select(gr => new ReceiptSearchSummaryInformation
					{
						TypeCode = 0,
						TypeTitle = "",
						Count = gr.Count(),
						TellerId = gr.Key.TellerID,
						TransactionDate = (DateTime)gr.Key.TransactionDate,
						Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
					}).OrderBy(y => y.TransactionDate).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.TypeAndDate)
			{
				result = reportData.GroupBy(r => new { r.ReceiptType, TransactionDate = (reportSetup.DateToShow == DateToShowOptions.ActualSystemDate ? r.ActualSystemDate.Value.Date : r.RecordedTransactionDate.Value.Date)})
					.Select(gr => new ReceiptSearchSummaryInformation
					{
						TypeCode = (int)gr.Key.ReceiptType,
						TypeTitle = receiptTypes.FirstOrDefault(x => x.TypeCode == (int)gr.Key.ReceiptType)?.TypeTitle ?? "",
						Count = gr.Count(),
						TellerId = "",
						TransactionDate = (DateTime)gr.Key.TransactionDate,
						Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
					}).OrderBy(y => y.TransactionDate).ToList();
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.TypeAndTeller)
			{
				result = reportData.GroupBy(r => new { r.ReceiptType, r.TellerID })
					.Select(gr => new ReceiptSearchSummaryInformation
					{
						TypeCode = (int)gr.Key.ReceiptType,
						TypeTitle = receiptTypes.FirstOrDefault(x => x.TypeCode == (int)gr.Key.ReceiptType)?.TypeTitle ?? "",
						Count = gr.Count(),
						TellerId = gr.Key.TellerID,
						TransactionDate = null,
						Amount = gr.Sum(x => x.Amount1 + x.Amount2 + x.Amount3 + x.Amount4 + x.Amount5 + x.Amount6)
					}).OrderBy(y => y.TransactionDate).ToList();
			}

			return result;
		}

		public IEnumerable<ReceiptType> ReceiptTypes { get => receiptTypes; }
		public IEnumerable<ReceiptSearchReportResult> GetReportData()
		{
			return receiptSearchReportQueryHandler.ExecuteQuery(reportSetup);
		}

		public ReceiptSearchReportSetupInformation ReportSetup {
			get => reportSetup;
			set => reportSetup = value;
		}
		public ReceiptSearchReportSetupInformation GetSavedReportSetup(int id)
		{
			var report = savedStatusReportQueryHandler.ExecuteQuery(
				new SavedStatusReportsSearchCriteria
				{
					Id = id
				}).FirstOrDefault();

			return ConvertSavedReportToReportInformation(report);
		}

		private List<int> ConvertCommaDelimitedListToListInt(string values)
		{
			var list = values.Split(',').ToList();
			var result = new List<int>();

			foreach (var item in list)
			{
				if (item != "")
				{
					result.Add(item.ToIntegerValue());
				}
			}

			return result;
		}

		private ReceiptSearchReportSetupInformation ConvertSavedReportToReportInformation(SavedStatusReport report)
		{
			if (report == null)
				return null;

			var result = new ReceiptSearchReportSetupInformation();

			if (report.Ans1 == "Current Year")
			{
				result.TimePeriod = TimePeriodOption.CurrentYear;
			}
			else if (report.Ans1 == "Archive Year")
			{
				result.TimePeriod = TimePeriodOption.PreviousYears;
			}
			else
			{
				result.TimePeriod = TimePeriodOption.Both;
			}

			if (report.Ans2 == "Summary")
			{
				result.ReportDetailLevel = SummaryDetailBothOption.Summary;
			}
			else if (report.Ans2 == "Detail")
			{
				result.ReportDetailLevel = SummaryDetailBothOption.Detail;
			}
			else
			{
				result.ReportDetailLevel = SummaryDetailBothOption.Both;
			}

			if (report.Ans3 == "Summary")
			{
				result.ReceiptDetailLevel = SummaryDetailOption.Summary;
			}
			else
			{
				result.ReceiptDetailLevel = SummaryDetailOption.Detail;
			}

			if (report.Ans4 == "Yes")
			{
				result.ShowTenderedAmounts = true;
			}
			else
			{
				result.ShowTenderedAmounts = false;
			}

			if (report.Ans5 == "Type")
			{
				result.SummarizeBy = ReceiptSearchSummarizeByOption.Type;
			}
			else if (report.Ans5 == "Date")
			{
				result.SummarizeBy = ReceiptSearchSummarizeByOption.Date;
			}
			else if (report.Ans5 == "Teller")
			{
				result.SummarizeBy = ReceiptSearchSummarizeByOption.Teller;
			}
			else if (report.Ans5 == "Type and Date")
			{
				result.SummarizeBy = ReceiptSearchSummarizeByOption.TypeAndDate;
			}
			else if (report.Ans5 == "Type and Teller")
			{
				result.SummarizeBy = ReceiptSearchSummarizeByOption.TypeAndTeller;
			}
			else 
			{
				result.SummarizeBy = ReceiptSearchSummarizeByOption.DateAndTeller;
			}

			if (report.Ans6 == "Summary")
			{
				result.SummaryDetailLevel = SummaryDetailOption.Summary;
			}
			else
			{
				result.SummaryDetailLevel = SummaryDetailOption.Detail;
			}

			result.ReportName = report.ReportName;

			result.ReceiptTypes = ConvertCommaDelimitedListToListInt(report.WhereSelection);

			result.SortOptions = ConvertCommaDelimitedListToListInt(report.SortSelection).Select(x => (ReceiptSearchSortByOptions)x).ToList();

			List<string> values = report.FieldConstraint0.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.ActualDateStart = values[0].ToDate();
				if (values.Count > 1)
				{
					result.ActualDateEnd = values[1].ToDate();
				}
				else
				{
					result.ActualDateEnd = values[0].ToDate();
				}
			}

			values = report.FieldConstraint1.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.CloseOutDateStartId = values[0].ToIntegerValue();
				if (values.Count > 1)
				{
					result.CloseOutDateEndId = values[1].ToIntegerValue();
				}
				else
				{
					result.CloseOutDateEndId = values[0].ToIntegerValue();
				}
			}

			values = report.FieldConstraint2.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.TellerId = values[0];
			}

			values = report.FieldConstraint3.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.ReceiptTypeCodeStart = values[0].ToIntegerValue();
				if (values.Count > 1)
				{
					result.ReceiptTypeCodeEnd = values[1].ToIntegerValue();
				}
				else
				{
					result.ReceiptTypeCodeEnd = values[0].ToIntegerValue();
				}
			}

			values = report.FieldConstraint4.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.ReceiptNumberStart = values[0].ToIntegerValue();
				if (values.Count > 1)
				{
					result.ReceiptNumberEnd = values[1].ToIntegerValue();
				}
				else
				{
					result.ReceiptNumberEnd = values[0].ToIntegerValue();
				}
			}

			values = report.FieldConstraint5.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.Reference = values[0];
			}

			values = report.FieldConstraint6.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 1)
			{
				if (values[0].Trim() == "Contains")
				{
					result.NameSearchType = NameSearchOptions.Contains;
				}
				else
				{
					result.NameSearchType = NameSearchOptions.BeginsWith;
				}
				result.Name = values[1];
			}

			values = report.FieldConstraint7.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.Control1 = values[0];
			}

			values = report.FieldConstraint8.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.Control2 = values[0];
			}

			values = report.FieldConstraint9.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.Control3 = values[0];
			}

			values = report.FieldConstraint10.Split(new string[] { "|||" }, StringSplitOptions.RemoveEmptyEntries).ToList();
			if (values.Count > 0)
			{
				result.GLAccount = values[0];
			}
			return result;
		}

		public bool SaveReport(ReceiptSearchReportSetupInformation reportSetup)
		{
			return commandDispatcher.Send(new SaveReceiptSearchReport
			{
				reportSetup = ConvertReportInformationToSavedReport(reportSetup)
			}).Result;
		}

		public bool DeleteReport(int id)
		{
			return commandDispatcher.Send(new DeleteReceiptSearchReport
			{
				Id = id
			}).Result;
		}

		private SavedStatusReport ConvertReportInformationToSavedReport(ReceiptSearchReportSetupInformation reportSetup)
		{
			if (reportSetup == null)
				return null;

			var savedReport = new SavedStatusReport();

			savedReport.Type = "SEARCH";
			if (reportSetup.TimePeriod == TimePeriodOption.CurrentYear)
			{
				savedReport.Ans1 = "Current Year";
			}
			else if (reportSetup.TimePeriod == TimePeriodOption.PreviousYears)
			{
				savedReport.Ans1 = "Archive Year";
			}
			else
			{
				savedReport.Ans1 = "Both Years";
			}

			if (reportSetup.ReportDetailLevel == SummaryDetailBothOption.Summary)
			{
				savedReport.Ans2 = "Summary";
			}
			else if (reportSetup.ReportDetailLevel == SummaryDetailBothOption.Detail)
			{
				savedReport.Ans2 = "Detail";
			}
			else
			{
				savedReport.Ans2 = "Both";
			}

			if (reportSetup.ReceiptDetailLevel == SummaryDetailOption.Summary)
			{
				savedReport.Ans3 = "Summary";
			}
			else
			{
				savedReport.Ans3 = "Detail";
			}

			if (reportSetup.ShowTenderedAmounts == true)
			{
				savedReport.Ans4 = "Yes";
			}
			else
			{
				savedReport.Ans4 = "No";
			}

			if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Type)
			{
				savedReport.Ans5 = "Type";
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Date)
			{
				savedReport.Ans5 = "Date";
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.Teller)
			{
				savedReport.Ans5 = "Teller";
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.TypeAndDate)
			{
				savedReport.Ans5 = "Type and Date";
			}
			else if (reportSetup.SummarizeBy == ReceiptSearchSummarizeByOption.TypeAndTeller)
			{
				savedReport.Ans5 = "Type and Teller";
			}
			else
			{
				savedReport.Ans5 = "Date and Teller";
			}

			if (reportSetup.SummaryDetailLevel == SummaryDetailOption.Summary)
			{
				savedReport.Ans6 = "Summary";
			}
			else
			{
				savedReport.Ans6 = "Detail";
			}

			savedReport.ReportName = reportSetup.ReportName;

			string receiptTypes = "";

			foreach (var type in reportSetup.ReceiptTypes)
			{
				receiptTypes = receiptTypes + type.ToString() + ",";
			}

			if (receiptTypes != "")
				receiptTypes = receiptTypes.Left(receiptTypes.Length - 1);

			savedReport.WhereSelection = receiptTypes;

			string sortOptions = "";

			foreach (var sort in reportSetup.SortOptions)
			{
				sortOptions = sortOptions + ((int)sort).ToString() + ",";
			}

			if (sortOptions != "")
				sortOptions = sortOptions.Left(sortOptions.Length - 1);

			savedReport.SortSelection = sortOptions;

			savedReport.FieldConstraint0 = BuildFieldConstraint(
				reportSetup.ActualDateStart != null ? reportSetup.ActualDateStart.Value.ToShortDateString() : "", 
				reportSetup.ActualDateEnd != null ? reportSetup.ActualDateEnd.Value.ToShortDateString() : "");
			
			savedReport.FieldConstraint1 = BuildFieldConstraint(
				reportSetup.CloseOutDateStartId > 0 ? reportSetup.CloseOutDateStartId.ToString() : "",
				reportSetup.CloseOutDateEndId > 0 ? reportSetup.CloseOutDateEndId.ToString() : "");

			savedReport.FieldConstraint2 = BuildFieldConstraint(reportSetup.TellerId.Trim());

			savedReport.FieldConstraint3 = BuildFieldConstraint(
				reportSetup.ReceiptTypeCodeStart > 0 ? reportSetup.ReceiptTypeCodeStart.ToString() : "",
				reportSetup.ReceiptTypeCodeEnd > 0 ? reportSetup.ReceiptTypeCodeEnd.ToString() : "");

			savedReport.FieldConstraint4 = BuildFieldConstraint(
				reportSetup.ReceiptNumberStart > 0 ? reportSetup.ReceiptNumberStart.ToString() : "",
				reportSetup.ReceiptNumberEnd > 0 ? reportSetup.ReceiptNumberEnd.ToString() : "");

			savedReport.FieldConstraint5 = BuildFieldConstraint(reportSetup.Reference.Trim());

			if (reportSetup.Name.Trim() != "")
			{
				savedReport.FieldConstraint6 = BuildFieldConstraint(reportSetup.NameSearchType == NameSearchOptions.Contains ? "Contains" : "Begins With", reportSetup.Name.Trim());
			}
			else
			{
				savedReport.FieldConstraint6 = BuildFieldConstraint();
			}

			savedReport.FieldConstraint7 = BuildFieldConstraint(reportSetup.Control1.Trim());
			savedReport.FieldConstraint8 = BuildFieldConstraint(reportSetup.Control2.Trim());
			savedReport.FieldConstraint9 = BuildFieldConstraint(reportSetup.Control3.Trim());
			savedReport.FieldConstraint10 = BuildFieldConstraint(reportSetup.GLAccount.Trim());
			
			return savedReport;
		}

		private string BuildFieldConstraint(string value1 = "", string value2 = "")
		{
			return value1 + "|||" + value2;
		}
	}
}
