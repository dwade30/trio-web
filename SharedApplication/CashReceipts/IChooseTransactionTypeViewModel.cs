﻿using System.Collections.Generic;

namespace SharedApplication.CashReceipts
{
    public interface IChooseTransactionTypeViewModel
    {
        void SetTransactionGroup(CRTransactionGroup transactionGroup);
        void StartClerkTransaction();
        void StartMiscReceiptTransaction(int receiptType, string name);
		(bool success,string message) StartMotorVehicleTransaction();
		void StartAccountsReceivableTransaction(int accountNumber);
        IEnumerable<DescriptionIDPair> GetTransactionTypes();
        void StartRealEstateTransaction(int accountNumber);
        void StartPersonalPropertyTransaction(int accountNumber);
        void StartUtilityTransaction(int accountNumber);
		void StartMosesTransaction();
        void StartMyRecTransaction(string fileName);
        void StartCamdenMooringTransaction(string fileName);
        bool IsValidTransactionCode(int transactionCode);
        bool IsValidTransactionChoice(string choice);
        //void StartDogTransaction();
        //void StartDeathTransaction();
        //void StartBurialTransaction();
        //void StartBirthTransaction();
        //void StartMarriageTransaction();
        
        IEnumerable<string> ValidationErrors { get; }
        IEnumerable<string> ProcessErrors { get; }
        void AddProcessErrors(IEnumerable<string> errors);
    }
}