﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Enums;

namespace SharedApplication.CashReceipts
{
	public class GlobalCashReceiptSettings
	{
		public ElectronicPaymentPortalOption ElectronicPaymentPortalType { get; set; } = ElectronicPaymentPortalOption.None;
        public bool EPaymentInTestMode { get; set; } = false;
        public string ConvenienceFeeMethod { get; set; } = "";
		public string ConvenienceFeeGLAccount { get; set; } = "";
		public string SeparateCreditCardGLAccount { get; set; } = "";
		public decimal ConvenienceFeeFlatAmount { get; set; } = 0;
		public decimal ConvenienceFeePercentage { get; set; } = 0;
		public bool CashDrawerConnected { get; set; } = false;
		public string CashDrawerCode { get; set; } = "";
		public int CashDrawerRepeatCount { get; set; } = 0;
		public string CashDrawerPortId { get; set; } = "";
		public bool PrintExtraReceipt { get; set; } = false;
		public bool SP200Printer { get; set; } = false;
		public bool NarrowReceipt { get; set; } = false;
		public bool WideReceiptNoMargins { get; set; } = false;
		public bool CompactedReceipt { get; set; } = false;
		public bool ThinReceipt { get; set; } = false;
		public int ReceiptLength { get; set; } = 0;
		public int ReceiptOffset { get; set; } = 0;
		public int ReceiptExtraLines { get; set; } = 0;
		public string TopComment { get; set; } = "";
		public string BottomComment { get; set; } = "";
		public PrintSignatureOption PrintSignature { get; set; } = PrintSignatureOption.No;
		public TellerIDUsage TellerIdUsage { get; set; } = TellerIDUsage.RequireEachTime;
		public bool ShowControlFields { get; set; } = false;
		public string EPaymentClientID { get; set; } = "";
        public string PaymentResponseURL { get; set; } //= @"https://development.trio-web.com/";
        public bool ShowPaymentPortalCodeOnPaymentScreen { get; set; } = false;
        public bool UseSeparateCreditCardGLAccount { get; set; } = false;
    }
}
