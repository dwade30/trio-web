﻿using System;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
    public class TransactionCancelledEventHandler : ReceiptingEventHandlerBase<TransactionCancelled>
    {
        private CRTransactionGroupRepository transactionGroupRepository;
        private IView<ICRCashOutViewModel> chooseTransactionView;
        public TransactionCancelledEventHandler(CRTransactionGroupRepository transactionGroupRepository,IView<ICRCashOutViewModel> chooseTransactionView)
        {
            this.transactionGroupRepository = transactionGroupRepository;
            this.chooseTransactionView = chooseTransactionView;
        }
        protected override void Handle(TransactionCancelled cancelledEvent)
        {
            if (cancelledEvent.CorrelationId != Guid.Empty)
            {
                var crTransactionGroup = transactionGroupRepository.Get(cancelledEvent.CorrelationId);
                if (crTransactionGroup != null)
                {
                    chooseTransactionView.ViewModel.SetTransactionGroup(crTransactionGroup);
                    chooseTransactionView.ViewModel.AddProcessErrors(cancelledEvent.Errors);
                    chooseTransactionView.Show();
                }
            }
        }
    }
}