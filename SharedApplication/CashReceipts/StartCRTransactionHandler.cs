﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class StartCRTransactionHandler : CommandHandler<StartCRTransaction>
    {
        private IView<ICRCashOutViewModel> chooseTransactionTypeView;
        private CRTransactionGroupRepository transactionGroupRepository;
        public StartCRTransactionHandler(IView<ICRCashOutViewModel> view, CRTransactionGroupRepository transactionGroupRepository)
        {
            chooseTransactionTypeView = view;
            this.transactionGroupRepository = transactionGroupRepository;
        }
        protected override void Handle(StartCRTransaction command)
        {   var transactionGroup = new CRTransactionGroup(){ReceiptIdentifier = Guid.NewGuid(),TellerId = command.TellerId, TransactionDate = DateTime.Now, StartedAsPendingTransaction = command.AsPendingTransaction};
            transactionGroupRepository.Add(transactionGroup);
            chooseTransactionTypeView.ViewModel.SetTransactionGroup(transactionGroup);
            chooseTransactionTypeView.ViewModel.AsPendingTransaction = command.AsPendingTransaction;
            chooseTransactionTypeView.Show();
        }
    }
}