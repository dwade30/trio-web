﻿using System;
using System.Linq;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class ChooseExistingReceiptViewModel : IChooseExistingReceiptViewModel
    {
        public int Id { get; set; }
        public bool CurrentYear { get; set; }
        public bool Cancelled { get; set; } = true;
        public int ReceiptNumber { get; set; }
        
        private ICashReceiptsContext crContext;
        private CommandDispatcher commandDispatcher;
        private ISettingService settingService;
        public ChooseExistingReceiptViewModel(ICashReceiptsContext crContext, CommandDispatcher commandDispatcher, ISettingService settingService)
        {
            this.crContext = crContext;
            this.commandDispatcher = commandDispatcher;
            this.settingService = settingService;

            var result = settingService.GetSettingValue(SettingOwnerType.Machine, "LastReceiptGenerated");

            if (result != null)
            {
                ReceiptNumber = result.SettingValue.ToIntegerValue();
            }
            else
            {
                ReceiptNumber = 0;
            }
            
        }
        public bool SelectReceipt(int receiptNumber, bool currentYear)
        {
            if (currentYear)
            {
                var choices = crContext.Receipts.Where(r => r.ReceiptNumber == receiptNumber);
                if (choices.Count() > 1)
                {
                    Id = commandDispatcher.Send(new SelectReceiptFromList(choices.Select(r => new ReceiptInfo()
                    {
                        Id = r.Id,
                        IsCurrentYear = true,
                        ReceiptNumber = r.ReceiptNumber.GetValueOrDefault(),
                        ReceiptDate = r.ReceiptDate.GetValueOrDefault(),
                        TellerID = r.TellerID,
                        PaidBy = r.PaidBy
                    }).OrderByDescending(r => r.ReceiptDate).ToList())).Result;
                }
                else
                {
                    Id = (choices.FirstOrDefault()?.Id).GetValueOrDefault();
                }
            }
            else
            {
                var choices = crContext.LastYearReceipts.Where(r => r.ReceiptNumber == receiptNumber);
                if (choices.Count() > 1)
                {
                     Id = commandDispatcher.Send(new SelectReceiptFromList(choices.Select(r => new ReceiptInfo()
                    {
                        Id = r.Id,
                        IsCurrentYear = false,
                        ReceiptNumber = r.ReceiptNumber.GetValueOrDefault(),
                        ReceiptDate = r.LastYearReceiptDate.GetValueOrDefault(),
                        TellerID = r.TellerID,
                        PaidBy = r.PaidBy
                    }).OrderByDescending(r => r.ReceiptDate).ToList())).Result;
                }
                else
                {
                    Id = (choices.FirstOrDefault()?.Id).GetValueOrDefault();
                }
                //Id = (crContext.LastYearReceipts.FirstOrDefault(r => r.ReceiptNumber == receiptNumber)?.ID).GetValueOrDefault();
            }

            if (Id == 0)
            {
                return false;
            }
            ReceiptNumber = receiptNumber;
            CurrentYear = currentYear;
            Cancelled = false;
            return true;
        }
    }
}