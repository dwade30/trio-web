﻿using SharedApplication.AccountsReceivable.Commands;
using SharedApplication.AccountsReceivable.Receipting;
using SharedApplication.Authorization;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts.Extensions;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Clerk.Commands;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections;
using SharedApplication.TaxCollections.Receipting;
using SharedApplication.UtilityBilling;
using SharedApplication.UtilityBilling.Receipting;
using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.CamdenMooringImport;
using SharedApplication.CashReceipts.MyRecImport;
using SharedApplication.Commands;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Extensions;

namespace SharedApplication.CashReceipts
{
    public abstract class ReceiptTransactionSummary : IReceiptProcessViewModel
    {
        protected CommandDispatcher commandDispatcher;
        protected IEnumerable<DescriptionIDPair> receiptTypes;
        protected IEnumerable<ReceiptType> receiptTypeDetails;
        protected IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler;
        protected DataEnvironmentSettings environmentSettings;
        protected IGlobalActiveModuleSettings activeModules;
        protected IUserPermissionSet currentPermissionSet;
        protected IAccountValidationService accountValidationService;
        protected GlobalUtilityBillingSettings utilityBillingSettings;
        protected IReceiptTypesService receiptTypesService;
        protected IReceiptService receiptService;

        protected List<string> validationList = new List<string>();
        public IEnumerable<string> ValidationErrors { get => validationList; }
        protected List<string> processErrorsList = new List<string>();
        public IEnumerable<string> ProcessErrors
        {
            get => processErrorsList;
        }


        public ReceiptTransactionSummary(CommandDispatcher commandDispatcher, IQueryHandler<ReceiptTypeSearchCriteria, IEnumerable<ReceiptType>> receiptTypeQueryHandler, DataEnvironmentSettings environmentSettings, IGlobalActiveModuleSettings activeModules, IUserPermissionSet currentPermissionSet, IAccountValidationService accountValidationService, GlobalUtilityBillingSettings utilityBillingSettings, IReceiptTypesService receiptTypesService, IReceiptService receiptService)
        {
            this.commandDispatcher = commandDispatcher;
            this.receiptTypeQueryHandler = receiptTypeQueryHandler;
            this.environmentSettings = environmentSettings;
            this.activeModules = activeModules;
            this.currentPermissionSet = currentPermissionSet;
            this.accountValidationService = accountValidationService;
            this.receiptTypesService = receiptTypesService;
            this.utilityBillingSettings = utilityBillingSettings;
            this.receiptService = receiptService;
        }

        public bool EditTransaction(Guid transactionId)
        {
            var transaction = receiptService.TransactionGroup.GetTransactions().FirstOrDefault(x => x.Id == transactionId);

            if (transaction != null)
            {
                switch (transaction)
                {
                    case MiscReceiptTransactionBase transactionDetails:
                        commandDispatcher.Send(new EditMiscReceiptTransaction
                        {
                            CorrelationId = transaction.CorrelationIdentifier,
                            Id = transaction.Id,
                            Transaction = ((MiscReceiptTransactionBase)transaction).TransactionDetails
                        });
                        return true;
                        break;
                    case AccountsReceivableTransactionBase transactionDetails:
                        commandDispatcher.Send(new ShowARPaymentStatusScreen()
                        {
                            AccountNumber = transactionDetails.AccountNumber,
                            CorrelationId = transaction.CorrelationIdentifier,
                            IsPaymentScreen = true,
                            StartedFromCashReceipts = true
                        });
                        return true;
                        break;
                    case UtilityBillingTransactionBase transactionDetails:
                        commandDispatcher.Send(new ShowUBPaymentStatusScreen()
                        {
                            AccountNumber = transactionDetails.AccountNumber,
                            CorrelationId = transaction.CorrelationIdentifier,
                            IsPaymentScreen = true,
                            StartedFromCashReceipts = true
                        });
                        return true;
                        break;
                    default:
                        return false;
                        break;
                }
            }

            return false;
        }

        public void ShowPaymentDetail(Guid transactionId)
        {
            var transaction = receiptService.TransactionGroup.GetTransactions().FirstOrDefault(t => t.Id == transactionId);
            if (transaction != null)
            {
                commandDispatcher.Send(new ShowReceiptTransactionDetail(transaction.ToReceiptDetailItem()));
            }
        }


        void IChooseTransactionTypeViewModel.SetTransactionGroup(CRTransactionGroup transactionGroup)
        {
            SetTransactionGroup(transactionGroup);
        }

        public bool AsPendingTransaction { get; set; }

        void IReceiptTransactionSummary.SetTransactionGroup(CRTransactionGroup transactionGroup)
        {
            SetTransactionGroup(transactionGroup);
        }

        public void SetTransactionGroup(CRTransactionGroup transactionGroup)
        {
            receiptService.TransactionGroup = transactionGroup;
        }

        public CRTransactionGroup TransactionGroup
        {
            get => receiptService.TransactionGroup;
            set => receiptService.TransactionGroup = value;
        }

        public void StartClerkTransaction()
        {
            commandDispatcher.Send(new InitializeClerkContext());
            commandDispatcher.Send(new MakeClerkTransaction() { CorrelationId = receiptService.TransactionGroup.ReceiptIdentifier, Id = Guid.NewGuid() });
        }

        //public void StartDogTransaction()
        //{
        //    commandDispatcher.Send(new InitializeClerkContext());
        //    commandDispatcher.Send(new MakeDogTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid()));
        //}

        //public void StartMarriageTransaction()
        //{
        //    commandDispatcher.Send(new InitializeClerkContext());
        //    commandDispatcher.Send(new MakeMarriageTransaction(receiptService.TransactionGroup.ReceiptIdentifier,
        //        Guid.NewGuid()));
        //}

        //public void StartDeathTransaction()
        //{
        //    commandDispatcher.Send(new InitializeClerkContext());
        //    commandDispatcher.Send(new MakeDeathTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid()));
        //}

        //public void StartBirthTransaction()
        //{
        //    commandDispatcher.Send(new InitializeClerkContext());
        //    commandDispatcher.Send(new MakeBirthTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid()));
        //}

        //public void StartBurialTransaction()
        //{
        //    commandDispatcher.Send(new InitializeClerkContext());
        //    commandDispatcher.Send(new MakeBurialTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid()));
        //}

        public void StartMiscReceiptTransaction(int receiptType, string name)
        {
            commandDispatcher.Send(new MakeMiscReceiptTransaction
            {
                CorrelationId = receiptService.TransactionGroup.ReceiptIdentifier,
                Id = Guid.NewGuid(),
                ReceiptType = receiptType,
                Name = name
            });
        }

        public (bool success,string message) StartMotorVehicleTransaction()
        {
            var formList = commandDispatcher.Send(new GetOpenFormsForModule("twmv0000")).Result;
            if (formList.Any())
            {
                var listString = formList.ToCRLFDelimitedList();
                var message = "Motor Vehicle is already started" + Environment.NewLine + "Please close the following forms:" + Environment.NewLine + listString;
                return (success: false, message: message);
            }
            commandDispatcher.Send(new MakeMotorVehicleTransaction()
            { CorrelationId = receiptService.TransactionGroup.ReceiptIdentifier, Id = Guid.NewGuid() });
            return (success: true, message: "");
        }

        public void StartMosesTransaction()
        {
            commandDispatcher.Send(new MakeMosesTransaction()
            { CorrelationId = receiptService.TransactionGroup.ReceiptIdentifier, Id = Guid.NewGuid() });
        }

        public void StartMyRecTransaction(string fileName)
        {
            commandDispatcher.Send(new ImportMyRecTransaction(receiptService.TransactionGroup.ReceiptIdentifier,Guid.NewGuid(),fileName));
        }

        public void StartCamdenMooringTransaction(string fileName)
        {
            commandDispatcher.Send(new ImportCamdenMooringTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid(), fileName));
        }

        public void StartAccountsReceivableTransaction(int accountNumber)
        {
            commandDispatcher.Send(new MakeAccountsReceivableTransaction
            {
                CorrelationId = receiptService.TransactionGroup.ReceiptIdentifier,
                Id = Guid.NewGuid(),
                AccountNumber = accountNumber,
                StartedFromCashReceipts = true,
                ShowPaymentScreen = true
            });
        }

        public IEnumerable<DescriptionIDPair> GetTransactionTypes()
        {
            //if (receiptTypes == null)
            //{
                LoadReceiptTypes();

            //}

            return receiptTypes;
        }

        protected void LoadReceiptTypes()
        {

            validationList.Clear();

            var detailList = new List<ReceiptType>();
            var rTypes = new List<DescriptionIDPair>();

            var typeDetails = receiptTypesService.ReceiptTypes.ToList();

            var reTypes = receiptTypesService.RealEstateTypes.ToList();
            var reTypesHaveAccounts = AllTypesHaveAccountsSetup(reTypes);

            var ppTypes = receiptTypesService.PersonalPropertyTypes.ToList();
            var ppTypesHaveAccounts = AllTypesHaveAccountsSetup(ppTypes);

            var arTypes = receiptTypesService.AccountsReceivableTypes.ToList();
            var arTypesHaveValidAccounts = ValidateARAccounts();

            var ckTypes = receiptTypesService.ClerkTypes.ToList();
            var ckTypesHaveAccounts = AllTypesHaveAccountsSetup(ckTypes);

            var waterTypes = receiptTypesService.WaterTypes.ToList();
            var waterTypesHaveAccounts = AllTypesHaveAccountsSetup(waterTypes);

            var sewerTypes = receiptTypesService.SewerTypes.ToList();
            var sewerTypesHaveAccounts = AllTypesHaveAccountsSetup(sewerTypes);

            var mosesTypes = receiptTypesService.MosesTypes.ToList();

            var noCLPermission =  currentPermissionSet.GetPermission("CL", 2) == "N";
            var noUTPermission = currentPermissionSet.GetPermission("UT", 9) == "N";
            var noARPermission =  currentPermissionSet.GetPermission("AR", 9) == "N";
            var noCKPermission = currentPermissionSet.GetPermission("CK", 1) == "N";
            var noMVPermission = currentPermissionSet.GetPermission("MV", 1) == "N";
            
            var modules = commandDispatcher.Send(new GetModules()).Result;
            var clerkPermissions = modules.ToClerkLevelPermissions();

            try
            {
                foreach (var detail in typeDetails.Where(x => !x.Deleted ?? false))
                {


                    var description = detail.TypeTitle;
                    var code = detail.TypeCode.GetValueOrDefault();

                    switch (code)
                    {
                        case 90:
                            if (!activeModules.TaxCollectionsIsActive || noCLPermission)
                            {
                                code = 0;
                            }
                            else
                            {
                                if (!reTypesHaveAccounts.Success)
                                {
                                    code = 0;
                                    validationList.AddRange(reTypesHaveAccounts.ValidationErrors);
                                }
                            }
                            break;
                        case 92:
                            if (!activeModules.TaxCollectionsIsActive || noCLPermission)
                            {
                                code = 0;
                            }
                            else
                            {
                                if (!ppTypesHaveAccounts.Success)
                                {
                                    code = 0;
                                    validationList.AddRange(ppTypesHaveAccounts.ValidationErrors);
                                }
                            }
                            break;
                        case 91:
                        case 890:
                        case 891:
                            code = 0;
                            break;
                        case 93:

                            if (!activeModules.UtilityBillingIsActive || noUTPermission)
                            {
                                code = 0;
                            }
                            else
                            {
                                if (utilityBillingSettings.HasSewerService)
                                {
                                    if (!sewerTypesHaveAccounts.Success)
                                    {
                                        code = 0;
                                        validationList.AddRange(sewerTypesHaveAccounts.ValidationErrors);
                                    }
                                }
                                if (utilityBillingSettings.HasWaterService && code > 0)
                                {
                                    if (!waterTypesHaveAccounts.Success)
                                    {
                                        code = 0;
                                        validationList.AddRange(waterTypesHaveAccounts.ValidationErrors);
                                    }
                                }
                            }


                            description = "Utility Payment";
                            break;
                        case 94:
                        case 95:
                        case 96:
                        case 893:
                        case 894:
                        case 895:
                        case 896:
                            code = 0;
                            break;
                        case 97:
                            if (!activeModules.AccountsReceivableIsActive || noARPermission)
                                code = 0;
                            else
                                if (!arTypesHaveValidAccounts.Success)
                            {
                                code = 0;
                                validationList.AddRange(arTypesHaveValidAccounts.ValidationErrors);
                            }

                            break;
                        case 98:
                        case 800:
                        case 801:
                        case 802:
                        case 803:
                        case 804:
                            if (!activeModules.ClerkIsActive || noCKPermission)
                                code = 0;
                            else
                                if (!ckTypesHaveAccounts.Success)
                            {
                                code = 0;
                                validationList.AddRange(ckTypesHaveAccounts.ValidationErrors);
                            }

                            break;
                        case 99:
                            if ((!activeModules.MotorVehicleIsActive && !activeModules.RedbookIsActive) || noMVPermission)
                            {
                                code = 0;
                            }

                            break;
                        case 190:
                            if (!activeModules.MosesIsActive)
                            {
                                code = 0;
                            }
                            break;
                        default:
                            var defaultResult = AllTypesHaveAccountsSetup(new List<ReceiptType> { detail });

                            if (!defaultResult.Success)
                            {
                                code = 0;
                                validationList.AddRange(defaultResult.ValidationErrors);
                            }

                            break;
                    }

                    if (code > 0) AddToReceiptTypeList(detailList, detail, description, rTypes);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                throw;
            }



            receiptTypeDetails = detailList;
            receiptTypes = rTypes;
        }

        private static void AddToReceiptTypeList(List<ReceiptType> detailList, ReceiptType detail, string description, List<DescriptionIDPair> rTypes)
        {
            detailList.Add(detail);
            var codePair = detail.ToDescriptionCodePair();
            codePair.Description = description;
            rTypes.Add(codePair);
        }

        protected (bool Success, IEnumerable<string> ValidationErrors) ValidateARAccounts()
        {
            if (!activeModules.BudgetaryIsActive)
            {
                return (true, new List<string>());
            }

            var result = commandDispatcher.Send(new ValidateAllARAccountsForPayments()).Result;
            return (Success: result.Success, ValidationErrors: result.ValidationErrors);
        }

        public void StartRealEstateTransaction(int accountNumber)
        {
            commandDispatcher.Send(new InitializeCollectionsStatics());
            commandDispatcher.Send(new MakeRealEstateTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid(), false,accountNumber));
        }

      

        public void StartPersonalPropertyTransaction(int accountNumber)
        {
            commandDispatcher.Send(new InitializeCollectionsStatics());
            commandDispatcher.Send(new MakePersonalPropertyTransaction(receiptService.TransactionGroup.ReceiptIdentifier, Guid.NewGuid(), false,accountNumber));
        }

        public void StartUtilityTransaction(int accountNumber)
        {
            commandDispatcher.Send(new InitializeUtilityBilling());
            commandDispatcher.Send(new MakeUtilityBillingTransaction
            {
                CorrelationId = receiptService.TransactionGroup.ReceiptIdentifier,
                Id = Guid.NewGuid(),
                AccountNumber = accountNumber,
                StartedFromCashReceipts = true,
                ShowPaymentScreen = true,
                AllowBatch = !receiptService.TransactionGroup.UtilityBillingBatchPaymentExists()
            });
        }

        public bool IsValidTransactionCode(int transactionCode)
        {
            return receiptTypes != null && receiptTypes.Any(r => r.ID == transactionCode);
        }

        public bool IsValidTransactionChoice(string choice)
        {
            if (choice.Length < 2)
            {
                return false;
            }

            switch (choice.Substring(0, 1).ToLower())
            {
                case "r":
                    if (choice.Substring(1).ToIntegerValue() > 0)
                    {
                        return IsValidTransactionCode(90);
                    }

                    break;
                case "p":
                    if (choice.Substring(1).ToIntegerValue() > 0)
                    {
                        return IsValidTransactionCode(92);
                    }

                    break;
                case "u":
                    if (choice.Substring(1).ToIntegerValue() > 0)
                    {
                        return IsValidTransactionCode(93);
                    }

                    break;
                case "a":
                    if (choice.Substring(1).ToIntegerValue() > 0)
                    {
                        return IsValidTransactionCode(97);
                    }

                    break;
                default:
                    return false;
            }

            return false;
        }

        protected bool AccountsAreSetup(ReceiptType receiptType)
        {
            return !AccountSetupNeeded(receiptType.Title1, receiptType.Account1) &&
                   !AccountSetupNeeded(receiptType.Title2, receiptType.Account2) &&
                   !AccountSetupNeeded(receiptType.Title3, receiptType.Account3) &&
                   !AccountSetupNeeded(receiptType.Title4, receiptType.Account4) &&
                   !AccountSetupNeeded(receiptType.Title5, receiptType.Account5) &&
                   !AccountSetupNeeded(receiptType.Title6, receiptType.Account6);
        }


        protected bool AccountSetupNeeded(string title, string account)
        {
            if (string.IsNullOrWhiteSpace(title)) return false;

            if (string.IsNullOrWhiteSpace(account)) return true;

            if (account.StartsWith("M") || !activeModules.BudgetaryIsActive) return false;

            return !accountValidationService.AccountValidate(account);
        }

        protected (bool Success, IEnumerable<string> ValidationErrors) AllTypesHaveAccountsSetup(IEnumerable<ReceiptType> receiptTypes)
        {
            var validationErrors = new List<string>();
            var result = true;

            foreach (var receiptType in receiptTypes.Where(receiptType => !AccountsAreSetup(receiptType)))
            {
                validationErrors.Add("Receipt type " + receiptType.TypeCode + " contains invalid accounts.");
                result = false;
            }

            return (Success: result, ValidationErrors: validationErrors);
        }


        public void AddProcessErrors(IEnumerable<string> errors)
        {
            if (errors.Any())
            {
                this.processErrorsList.AddRange(errors);
            }
        }
    }
}
