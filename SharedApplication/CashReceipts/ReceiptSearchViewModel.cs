﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Budgetary.Interfaces;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Enums;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings;
using SharedApplication.SystemSettings.Models;

namespace SharedApplication.CashReceipts
{
	public class ReceiptSearchViewModel : IReceiptSearchViewModel
	{
		private CommandDispatcher commandDispatcher;
		private IQueryHandler<SavedStatusReportsSearchCriteria, IEnumerable<SavedStatusReport>> savedStatusReportQueryHandler;
		private IQueryHandler<CloseOutSearchCriteria, IEnumerable<CloseOut>> closeOutQueryHandler;
		private IQueryHandler<OperatorQuery, IEnumerable<Operator>> tellerIdQueryHandler;
		private IQueryHandler<CRBanksQuery, IEnumerable<CRBank>> bankQueryHandler;
		private IReceiptSearchService receiptSearchService;
		private IAccountValidationService accountValidationService;

		private IEnumerable<SavedStatusReport> savedReports = new List<SavedStatusReport>();
		private IEnumerable<CloseOut> closeOuts = new List<CloseOut>();
		private IEnumerable<Operator> tellerIds = new List<Operator>();
		private IEnumerable<CRBank> banks = new List<CRBank>();

		public ReceiptSearchViewModel(CommandDispatcher commandDispatcher, IQueryHandler<SavedStatusReportsSearchCriteria, IEnumerable<SavedStatusReport>> savedStatusReportQueryHandler, IQueryHandler<CloseOutSearchCriteria, IEnumerable<CloseOut>> closeOutQueryHandler, IQueryHandler<OperatorQuery, IEnumerable<Operator>> tellerIdQueryHandler, IReceiptSearchService receiptSearchService, IAccountValidationService accountValidationService, IQueryHandler<CRBanksQuery, IEnumerable<CRBank>> bankQueryHandler)
		{
			this.commandDispatcher = commandDispatcher;
			this.savedStatusReportQueryHandler = savedStatusReportQueryHandler;
			this.closeOutQueryHandler = closeOutQueryHandler;
			this.tellerIdQueryHandler = tellerIdQueryHandler;
			this.receiptSearchService = receiptSearchService;
			this.accountValidationService = accountValidationService;
			this.bankQueryHandler = bankQueryHandler;

			
			GetSavedReports();
			GetCloseOuts();
			GetTellerIds();
			GetBanks();
		}

		public IEnumerable<GenericDescriptionPair<SummaryDetailOption>> GetSummaryDetailOptions()
		{
			return new List<GenericDescriptionPair<SummaryDetailOption>>()
			{
				new GenericDescriptionPair<SummaryDetailOption>(){ID = SummaryDetailOption.Summary,Description = "Summary"},
				new GenericDescriptionPair<SummaryDetailOption>(){ID = SummaryDetailOption.Detail,Description = "Detail"}
			};
		}

		public IEnumerable<GenericDescriptionPair<SummaryDetailBothOption>> GetSummaryDetailBothOptions()
		{
			return new List<GenericDescriptionPair<SummaryDetailBothOption>>()
			{
				new GenericDescriptionPair<SummaryDetailBothOption>(){ID = SummaryDetailBothOption.Summary,Description = "Summary"},
				new GenericDescriptionPair<SummaryDetailBothOption>(){ID = SummaryDetailBothOption.Detail,Description = "Detail"},
				new GenericDescriptionPair<SummaryDetailBothOption>(){ID = SummaryDetailBothOption.Both,Description = "Both"}
			};
		}

		public IEnumerable<GenericDescriptionPair<NameSearchOptions>> GetNameSearchOptions()
		{
			return new List<GenericDescriptionPair<NameSearchOptions>>()
			{
				new GenericDescriptionPair<NameSearchOptions>(){ID = NameSearchOptions.BeginsWith,Description = "Begins With"},
				new GenericDescriptionPair<NameSearchOptions>(){ID = NameSearchOptions.Contains,Description = "Contains"},
			};
		}

		public bool ValidateGLAccount(string account)
		{
			return accountValidationService.AccountValidate(account);
		}

		public IEnumerable<GenericDescriptionPair<AmountSearchOptions>> GetAmountSearchOptions()
		{
			return new List<GenericDescriptionPair<AmountSearchOptions>>()
			{
				new GenericDescriptionPair<AmountSearchOptions>(){ID = AmountSearchOptions.LessThan,Description = "Less Than"},
				new GenericDescriptionPair<AmountSearchOptions>(){ID = AmountSearchOptions.GreaterThan,Description = "Greater Than"},
				new GenericDescriptionPair<AmountSearchOptions>(){ID = AmountSearchOptions.Equals,Description = "Equals"},
				new GenericDescriptionPair<AmountSearchOptions>(){ID = AmountSearchOptions.NotEqual,Description = "Not Equal"}
			};
		}

		public IEnumerable<GenericDescriptionPair<DateToShowOptions>> GetDateToShowOptions()
		{
			return new List<GenericDescriptionPair<DateToShowOptions>>()
			{
				new GenericDescriptionPair<DateToShowOptions>(){ID = DateToShowOptions.ActualSystemDate,Description = "Actual System Date"},
				new GenericDescriptionPair<DateToShowOptions>(){ID = DateToShowOptions.RecordedTransactionDate,Description = "Recorded Transaction Date"},
			};
		}

		public IEnumerable<GenericDescriptionPair<MotorVehicleTransactionTypes>> GetMotorVehicleTransactionTypeOptions()
		{
			return new List<GenericDescriptionPair<MotorVehicleTransactionTypes>>()
			{
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.All,Description = "All Types w/Summary"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.Correction,Description = "Correction"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.DuplicateStickers,Description = "Duplicate Plate / Stickers"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.DuplicateRegistration,Description = "Duplicate Registration"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.GrossVehicleWeightChange,Description = "Gross Vehicle Weight Change"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.LostPlateStickers,Description = "Lost Plate / Stickers"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.NewRegistration,Description = "New Registration"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.NewRegistrationTransfer,Description = "New Registration Transfer"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.ReRegistration,Description = "Re Registration"},
				new GenericDescriptionPair<MotorVehicleTransactionTypes>(){ID = MotorVehicleTransactionTypes.ReRegistrationTransfer,Description = "Re Registration Transfer"},
			}.OrderBy(x => x.Description);
		}

		public IEnumerable<GenericDescriptionPair<TimePeriodOption>> GetTimePeriodOptions()
		{
			return new List<GenericDescriptionPair<TimePeriodOption>>()
			{
				new GenericDescriptionPair<TimePeriodOption>(){ID = TimePeriodOption.CurrentYear,Description = "Current Year"},
				new GenericDescriptionPair<TimePeriodOption>(){ID = TimePeriodOption.PreviousYears,Description = "Previous Years"},
				new GenericDescriptionPair<TimePeriodOption>(){ID = TimePeriodOption.Both,Description = "Both"}
			};
		}

		public IEnumerable<GenericDescriptionPair<ReceiptSearchSortByOptions>> GetSortByOptions()
		{
			return new List<GenericDescriptionPair<ReceiptSearchSortByOptions>>()
			{
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.ActualSystemDate,Description = "Actual System Date"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.CloseOutDate,Description = "Close Out Date"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.TellerId,Description = "Teller ID"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.ReceiptType,Description = "Receipt Type"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.ActualSystemTime,Description = "Actual System Time"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.Control1,Description = "Control 1"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.Control2,Description = "Control 2"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.Control3,Description = "Control 3"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.Name,Description = "Name"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.ReceiptNumber,Description = "Receipt Number"},
				new GenericDescriptionPair<ReceiptSearchSortByOptions>(){ID = ReceiptSearchSortByOptions.Reference,Description = "Reference"}
			}.OrderBy(x => x.Description);
		}

		public IEnumerable<GenericDescriptionPair<PaymentMethod>> GetPaymentMethods()
		{
			return new List<GenericDescriptionPair<PaymentMethod>>()
			{
				new GenericDescriptionPair<PaymentMethod>(){Description = "Cash", ID = PaymentMethod.Cash},
				new GenericDescriptionPair<PaymentMethod>(){Description = "Check", ID = PaymentMethod.Check},
				new GenericDescriptionPair<PaymentMethod>(){Description = "Credit Card", ID = PaymentMethod.Credit}
			};
		}

		public IEnumerable<GenericDescriptionPair<ReceiptSearchSummarizeByOption>> GetSummarizeByOptions()
		{
			return new List<GenericDescriptionPair<ReceiptSearchSummarizeByOption>>()
			{
				new GenericDescriptionPair<ReceiptSearchSummarizeByOption>(){ID = ReceiptSearchSummarizeByOption.Type,Description = "Type"},
				new GenericDescriptionPair<ReceiptSearchSummarizeByOption>(){ID = ReceiptSearchSummarizeByOption.Date,Description = "Date"},
				new GenericDescriptionPair<ReceiptSearchSummarizeByOption>(){ID = ReceiptSearchSummarizeByOption.Teller,Description = "Teller"},
				new GenericDescriptionPair<ReceiptSearchSummarizeByOption>(){ID = ReceiptSearchSummarizeByOption.TypeAndDate,Description = "Type and Date"},
				new GenericDescriptionPair<ReceiptSearchSummarizeByOption>(){ID = ReceiptSearchSummarizeByOption.TypeAndTeller,Description = "Type and Teller"},
				new GenericDescriptionPair<ReceiptSearchSummarizeByOption>(){ID = ReceiptSearchSummarizeByOption.DateAndTeller,Description = "Date and Teller"}
			};
		}

		public IEnumerable<ReceiptType> ReceiptTypes
		{
			get => receiptSearchService.ReceiptTypes;
		}
		
		public IEnumerable<SavedStatusReport> SavedReports
		{
			get => savedReports;
		}

		public IReceiptSearchService ReceiptSearchReportService {get => receiptSearchService;}

		public IEnumerable<CloseOut> CloseOuts
		{
			get => closeOuts;
		}

		public IEnumerable<Operator> TellerIds
		{
			get => tellerIds;
		}

		public IEnumerable<CRBank> Banks
		{
			get => banks;
		}

		public bool SaveReport(ReceiptSearchReportSetupInformation reportSetup)
		{
			if (receiptSearchService.SaveReport(reportSetup))
			{
				GetSavedReports();
				return true;
			}
			else
			{
				return false;
			}
		}

		public void DeleteReport(int id)
		{
			if (receiptSearchService.DeleteReport(id))
			{
				GetSavedReports();
			}
		}

		private void GetSavedReports()
		{
			savedReports = this.savedStatusReportQueryHandler.ExecuteQuery(new SavedStatusReportsSearchCriteria());
		}

		public bool ReportExists(string reportTitle)
		{
			return savedReports.Any(x => x.ReportName.ToUpper() == reportTitle.Trim().ToUpper());
		}

		private void GetCloseOuts()
		{
			closeOuts = this.closeOutQueryHandler.ExecuteQuery(new CloseOutSearchCriteria
			{
				Type = CloseOutType.FullCloseoutOnly
			});
		}

		private void GetTellerIds()
		{
			tellerIds = this.tellerIdQueryHandler.ExecuteQuery(new OperatorQuery());
		}

		private void GetBanks()
		{
			banks = this.bankQueryHandler.ExecuteQuery(new CRBanksQuery());
		}

		public ReceiptSearchReportSetupInformation GetSavedReportInformation(int id)
		{
			return receiptSearchService.GetSavedReportSetup(id);
		}
	}
}
