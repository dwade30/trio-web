﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class StartCRTransaction : Command
    {
        public string TellerId { get; set; } = "";
        public bool AsPendingTransaction { get; set; } = false;
        public StartCRTransaction(string tellerId,bool asPendingTransaction)
        {
            TellerId = tellerId;
            AsPendingTransaction = asPendingTransaction;
        }
    }
}