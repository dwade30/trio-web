﻿using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class RemoveCRTransactionGroupHandler : CommandHandler<RemoveCRTransactionGroup>
    {
        private CRTransactionGroupRepository transactionRepository;
        public RemoveCRTransactionGroupHandler(CRTransactionGroupRepository transactionRepository)
        {
            this.transactionRepository = transactionRepository;
        }
        protected override void Handle(RemoveCRTransactionGroup command)
        {
            transactionRepository.Remove(command.ReceiptIdentifier);
        }
    }
}