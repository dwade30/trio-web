﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using SharedApplication.CashReceipts;

namespace SharedApplication.CashReceipts
{
    public class InMemoryCRTransactionGroupRepository  : CRTransactionGroupRepository
    {
        private Dictionary<Guid, CRTransactionGroup> crTransactions = new Dictionary<Guid, CRTransactionGroup>();
        public CRTransactionGroup Get(Guid id)
        {
            if (crTransactions.ContainsKey(id))
            {
                return crTransactions[id];
            }
            return null;
        }

        public IEnumerable<CRTransactionGroup> GetIncluding(Expression<Func<CRTransactionGroup, bool>> whereProperty, params Expression<Func<CRTransactionGroup, object>>[] includeProperties)
        {
            if (whereProperty != null)
            {
                return crTransactions.Values.AsQueryable().Where(whereProperty);
            }
            return GetAll();
        }

        public IEnumerable<CRTransactionGroup> GetAll()
        {
            return crTransactions.Values.ToList();
        }

        public IEnumerable<CRTransactionGroup> Find(Expression<Func<CRTransactionGroup, bool>> predicate)
        {
            return crTransactions.Values.AsQueryable().Where(predicate).ToList();
        }

        public void Add(CRTransactionGroup transactionGroup)
        {
             crTransactions.Add(transactionGroup.ReceiptIdentifier, transactionGroup);
        }

        public void Remove(CRTransactionGroup transactionGroup)
        {
             crTransactions.Remove(transactionGroup.ReceiptIdentifier);
        }

        public void AddRange(IEnumerable<CRTransactionGroup> transactionGroups)
        {
            foreach (var transactionGroup in transactionGroups)
            {
                crTransactions.Add(transactionGroup.ReceiptIdentifier,transactionGroup);
            }
        }

        public void RemoveRange(IEnumerable<CRTransactionGroup> transactionGroups)
        {
            foreach (var transactionGroup in transactionGroups)
            {
                crTransactions.Remove(transactionGroup.ReceiptIdentifier);
            }
        }

        public void Remove(Guid id)
        {
            if (crTransactions.ContainsKey(id))
            {
                crTransactions.Remove(id);
            }
        }
    }
}