﻿using System;
using System.Collections.Generic;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;

namespace SharedApplication.CashReceipts
{
    public interface ICRCashOutViewModel : IReceiptProcessViewModel
    {
        IEnumerable<GenericDescriptionPair<PaymentMethod>> PaymentMethods { get; }
        IEnumerable<CRBank> Banks { get; }
        IEnumerable<CreditCardType> CreditCardTypes { get; }

		string PaidBy { get; set; } 
        int PaidById { get; set; }
        bool ShouldPrintReceipt { get; set; }
        int Copies { get; set; }

        decimal RemainingDue { get; }
        decimal ConvenienceFee { get; }
        decimal TotalAmount { get; }
        decimal TotalReceiptAmount { get; }
        decimal TotalPaid { get; }
        decimal TotalCashPaid { get; }
        decimal TotalCheckPaid { get; }
        decimal TotalCreditPaid { get; }
        decimal ChangeDue { get; }
        IEnumerable<ReceiptPayment> Payments { get; }
        bool MyRecImportEnabled { get; }
        bool CamdenMooringImportEnabled { get; }
        void AddPayment(ReceiptPayment payment);
        void RemovePayment(Guid id);
        void RemoveTransaction(Guid id);
		int AddBank();
        bool CanCashOut();
        bool PrintExtraReceipt();
		void ChooseParty();
        int CashOut();
		bool IsReceiptPrinterSetUp();
		void PrintReceipt(int copies);
        void StartNewTransaction();
        bool CheckAlreadyExistsForBank(string checkNumber, int bankId);
        ValidReceiptToVoidResult ValidReceiptToVoid(int receiptNumber);
        void VoidReceipt(int receiptNumber);
        void UpdateTransactionSplit(Guid transactionId, int split);

        event EventHandler PaymentsUpdated ;
		event EventHandler TransactionsUpdated;
		event EventHandler<(int PartyId,string PartyName)> PartyUpdated;
    }
}