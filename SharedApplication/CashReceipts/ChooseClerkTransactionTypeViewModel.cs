﻿using System;
using System.Collections.Generic;
using SharedApplication.Clerk;
using SharedApplication.Clerk.Births;
using SharedApplication.Clerk.Burials;
using SharedApplication.Clerk.Deaths;
using SharedApplication.Clerk.Enums;
using SharedApplication.Clerk.Marriages;
using SharedApplication.Extensions;
using SharedApplication.Messaging;
using SharedApplication.Receipting;
using SharedApplication.SystemSettings.Commands;
using SharedApplication.SystemSettings.Extensions;

namespace SharedApplication.CashReceipts
{
    public class ChooseClerkTransactionTypeViewModel : IChooseClerkTransactionTypeViewModel
    {
        private CommandDispatcher commandDispatcher;
        private Guid correlationID;
        private EventPublisher eventPublisher;
        public ChooseClerkTransactionTypeViewModel(CommandDispatcher commandDispatcher,EventPublisher eventPublisher)
        {
            //this.correlationID = correlationId;
            this.commandDispatcher = commandDispatcher;
            this.eventPublisher = eventPublisher;
        }

        public void SetCorrelationId(Guid correlationID)
        {
            this.correlationID = correlationID;
        }
        private void StartDogTransaction()
        {
            commandDispatcher.Send(new MakeDogTransaction(correlationID,Guid.NewGuid()) );
        }

        
        private void StartBirthTransaction()
        {
            commandDispatcher.Send(new MakeBirthTransaction(correlationID, Guid.NewGuid()));
        }

        private void StartDeathTransaction()
        {
            commandDispatcher.Send(new MakeDeathTransaction(correlationID, Guid.NewGuid()));
        }

        private void StartMarriageTransaction()
        {
            commandDispatcher.Send(new MakeMarriageTransaction(correlationID, Guid.NewGuid()));
        }

        private void StartBurialTransaction()
        {
            commandDispatcher.Send(new MakeBurialTransaction(correlationID, Guid.NewGuid()));
        }

        public void StartTransaction(ClerkTransactionType transactionType)
        {
            switch (transactionType)
            {
                case ClerkTransactionType.Birth:
                    StartBirthTransaction();
                    break;
                case ClerkTransactionType.Death:
                    StartDeathTransaction();
                    break;
                case ClerkTransactionType.Dog:
                    StartDogTransaction();
                    break;
                case ClerkTransactionType.Marriage:
                    StartMarriageTransaction();
                    break;
                case ClerkTransactionType.Burial:
                    StartBurialTransaction();
                    break;
            }
        }

        public IEnumerable<GenericDescriptionPair<ClerkTransactionType>> GetAvailableTransactionTypes()
        {
	        var clerkPermissions = commandDispatcher.Send(new GetModules()).Result.ToClerkLevelPermissions();

	        var result = new List<GenericDescriptionPair<ClerkTransactionType>>();

	        if (clerkPermissions.AllowDogs)
	        {
		        result.Add(new GenericDescriptionPair<ClerkTransactionType>() { ID = ClerkTransactionType.Dog, Description = "Dogs" });
	        }

            if (clerkPermissions.AllowBirths)
	        {
                result.Add(new GenericDescriptionPair<ClerkTransactionType>() { ID = ClerkTransactionType.Birth, Description = "Births" });
	        }

            if (clerkPermissions.AllowMarriages)
            {
	            result.Add(new GenericDescriptionPair<ClerkTransactionType>() { ID = ClerkTransactionType.Marriage, Description = "Marriages" });
            }

            if (clerkPermissions.AllowDeaths)
            {
	            result.Add(new GenericDescriptionPair<ClerkTransactionType>()
		            {ID = ClerkTransactionType.Death, Description = "Deaths"});
	            result.Add(new GenericDescriptionPair<ClerkTransactionType>()
		            {ID = ClerkTransactionType.Burial, Description = "Burials"});
            }

            return result;
        }

        public void Cancel()
        {
            eventPublisher.Publish(new TransactionCancelled(correlationID,null));
        }
    }
}