﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class SaveServiceCode : Command
    {
        public ServiceCode ServiceCode
        {
            get;
        }

        public SaveServiceCode(ServiceCode serviceCode)
        {
            ServiceCode = new ServiceCode()
            {
                Code =  serviceCode.Code,
                Description = serviceCode.Description,
                Id = serviceCode.Id
            };
        }
    }
}