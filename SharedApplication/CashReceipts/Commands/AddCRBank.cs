﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class AddCRBank : Command<int>
    {
        public CRBank Bank { get; set; }
        public AddCRBank(CRBank bank)
        {
            Bank = bank;
        }
    }
}