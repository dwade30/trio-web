﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class CreateNewReceipt : Command<int>
    {
        public Receipt Receipt { get; set; }
        public CreateNewReceipt(Receipt receipt)
        {
            this.Receipt = receipt;
        }
    }
}