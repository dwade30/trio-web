﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class DoesReceiptExist :Command<bool>
    {
        public int ReceiptNumber { get; }
        public bool IsCurrentYear { get; }

        public DoesReceiptExist(int receiptNumber, bool isCurrentYear)
        {
            ReceiptNumber = receiptNumber;
            IsCurrentYear = isCurrentYear;
        }
    }
}