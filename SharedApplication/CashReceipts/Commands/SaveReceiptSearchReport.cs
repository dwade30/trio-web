﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts.Commands
{
	public class SaveReceiptSearchReport : SharedApplication.Messaging.Command<bool>
	{
		public SavedStatusReport reportSetup { get; set; }
	}
}
