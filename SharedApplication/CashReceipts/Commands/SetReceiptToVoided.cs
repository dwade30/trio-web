﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class SetReceiptToVoided : Command
    {
        public int ReceiptNumber { get; set; } = 0;
    }
}
