﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class GetReceiptNumberFromID : Command<int>
    {
        public int ReceiptID { get; }

        public GetReceiptNumberFromID(int receiptId)
        {
            ReceiptID = receiptId;
        }
    }
}