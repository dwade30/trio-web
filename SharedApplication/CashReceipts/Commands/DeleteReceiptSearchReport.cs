﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CashReceipts.Commands
{
	public class DeleteReceiptSearchReport : SharedApplication.Messaging.Command<bool>
	{
		public int Id { get; set;}
	}
}
