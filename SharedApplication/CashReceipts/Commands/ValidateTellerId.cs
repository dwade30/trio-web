﻿using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Commands
{
    public class ValidateTellerId : Command<TellerValidationResult>
    {
        public ValidateTellerId(string tellerId, string password)
        {
            TellerId = tellerId;
            Password = password;
        }
        public string TellerId { get; set; }
        public string Password { get; set; }
    }
}