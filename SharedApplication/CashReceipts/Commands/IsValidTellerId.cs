﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class IsValidTellerId : Command<bool>
    {
        public string TellerId { get;  }

        public IsValidTellerId(string tellerId)
        {
            TellerId = tellerId;
        }
    }
}