﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class RemoveCRTransactionGroup : Command
    {
        public RemoveCRTransactionGroup(Guid receiptIdentifier)
        {
            ReceiptIdentifier = receiptIdentifier;
        }
        public Guid ReceiptIdentifier { get; set; }
    }
}