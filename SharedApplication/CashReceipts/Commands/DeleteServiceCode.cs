﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
    public class DeleteServiceCode : Command
    {
        public int Id { get; }

        public DeleteServiceCode(int id)
        {
            Id = id;
        }
    }
}