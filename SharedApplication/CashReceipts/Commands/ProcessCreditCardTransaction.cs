﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Autofac.Core;
using SharedApplication.CashReceipts.Enums;
using SharedApplication.CashReceipts.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts.Commands
{
	public class ProcessCreditCardTransaction : Command<ProcessCreditCardTransactionResult>
	{
		public string ServiceKey { get; set; }
		public decimal TransactionAmount { get; set; }
		public decimal ServiceFeeAmount { get; set; }
		public string SecureId { get; set; }
		public CreditCardProcessingMode ProcessingMode { get; set; }
		public string Caption { get; set; }
	}
}
