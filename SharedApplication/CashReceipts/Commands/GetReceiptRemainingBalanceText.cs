﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Commands
{
    public class GetReceiptRemainingBalanceText : Command<string>
    {
        public IEnumerable<TransactionBase> Transactions = new List<TransactionBase>();
        public bool IsFirstCopy;
        public bool IsReprint;
        public bool IsBatch;
    }
}
