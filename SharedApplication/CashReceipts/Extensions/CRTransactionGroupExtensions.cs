﻿using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;

namespace SharedApplication.CashReceipts.Extensions
{
    public static class CRTransactionGroupExtensions
    {
        public static Receipt ToReceipt(this CRTransactionGroup transactionGroup)
        {
            return new Receipt()
            {
                ReceiptIdentifier = transactionGroup.ReceiptIdentifier,
                TellerID =  transactionGroup.TellerId,
                
            };
        }


        public static bool UtilityBillingBatchPaymentExists(this CRTransactionGroup transactionGroup)
        {
            var transactions = transactionGroup.GetTransactions();
            foreach (var transaction in transactions)
            {
                switch (transaction)
                {
                    case UtilityBillingTransactionBase utilityBillingTransaction:
                        if (utilityBillingTransaction.IsBatchPayment)
                        {
                            return true;
                        }
                        break;
                }
            }

            return false;
        }
    }
}