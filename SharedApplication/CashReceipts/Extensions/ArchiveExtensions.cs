﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.Receipting;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.CashReceipts.Extensions
{
    public static class ArchiveExtensions
    {

        public static IEnumerable<TransactionBase> ToTransactions(this IEnumerable<Archive> archives,IReceiptTypesService receiptTypesService)
        {
            return archives.Select(a => a.ToTransaction(receiptTypesService));
        }
        public static TransactionBase ToTransaction(this Archive archive,IReceiptTypesService receiptTypesService)
        {
            var receiptType = receiptTypesService.GetReceiptTypeByCode(archive.ReceiptType.GetValueOrDefault());
            if (receiptType == null)
            {
                return null;
            }

            if (receiptType.IsMotorVehicleType())
            {
                return ArchiveToMVTransaction(archive);
            }

            if (receiptType.IsAccountsReceivableType())
            {
                return ArchiveToAccountsReceivableTransaction(archive);
            }

            if (receiptType.IsPropertyTaxType())
            {
                return ArchiveToPropertyTaxTransaction(archive);
            }

            if (receiptType.IsDogsType())
            {
                return ArchiveToClerkTransaction(archive);
            }

            if (receiptType.IsVitalsType())
            {
                return ArchiveToVitalTransaction(archive);
            }

            if (receiptType.IsUtilityType())
            {
                return ArchiveToUtilityBillingTransaction(archive,receiptType.IsLienType(), receiptType.TypeTitle);
            }

            return ArchiveToMiscReceiptTransaction(archive,receiptType);
        }

        private static MiscReceiptTransactionBase ArchiveToMiscReceiptTransaction(Archive archive,ReceiptType receiptType)
        {
            var transaction = new MiscReceiptTransactionBase
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = archive.ReceiptType.GetValueOrDefault().ToString(),                
                TownId = archive.TownKey.GetValueOrDefault(),                
                PayerName = archive.Name,
                Split = archive.Split.GetValueOrDefault(),
                TransactionTypeDescription = receiptType.TypeTitle
            };

            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount1", Description = receiptType.Title1, Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount2", Description = receiptType.Title2, Total = archive.Amount2.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount3", Description = receiptType.Title3, Total = archive.Amount3.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount4", Description = receiptType.Title4, Total = archive.Amount4.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount5", Description = receiptType.Title5, Total = archive.Amount5.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount6", Description = receiptType.Title6, Total = archive.Amount6.GetValueOrDefault().ToDecimal() });
            transaction.Controls.Add(new ControlItem() { Name = "Control1", Description = string.IsNullOrWhiteSpace(receiptType.Control1)? "Control1" : receiptType.Control1, Value = archive.Control1 });
            transaction.Controls.Add(new ControlItem() { Name = "Control2", Description = string.IsNullOrWhiteSpace(receiptType.Control2) ? "Control2" : receiptType.Control2, Value = archive.Control2 });
            transaction.Controls.Add(new ControlItem() { Name = "Control3", Description = string.IsNullOrWhiteSpace(receiptType.Control3) ? "Control3" : receiptType.Control3, Value = archive.Control3 });
            transaction.TransactionDetails = new MiscReceiptTransaction()
            {
                AffectCashDrawer = archive.AffectCashDrawer.GetValueOrDefault(),
                AffectCash = archive.AffectCash.GetValueOrDefault(),
                Comment = archive.Comment,
                Controls = transaction.Controls,
                CorrelationIdentifier = transaction.CorrelationIdentifier,
                CollectionCode = archive.CollectionCode,
                PaidBy = transaction.PayerName,
                Quantity = archive.Quantity.GetValueOrDefault(),
                Reference = archive.Ref,
                TownCode = archive.TownKey.GetHashCode(),
                TransactionTypeCode = transaction.TransactionTypeCode.ToIntegerValue(),
                TransactionTypeDescription = transaction.TransactionTypeDescription
                    
            };
            
            return transaction;
        }
        private static UtilityBillingTransactionBase ArchiveToUtilityBillingTransaction(Archive archive,bool isLienType,string typeTitle)
        {
            var transaction = new UtilityBillingTransactionBase
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = archive.ReceiptType.GetValueOrDefault().ToString(),
                AffectCash = archive.AffectCash.GetValueOrDefault(),
                AffectCashDrawer = archive.AffectCashDrawer.GetValueOrDefault(),
                TownId = archive.TownKey.GetValueOrDefault(),
                AccountNumber = archive.AccountNumber.GetValueOrDefault(),
                PayerName = archive.Name,
                Split = archive.Split.GetValueOrDefault(),
                Comment = archive.Comment,
                RecordedTransactionDate = archive.ArchiveDate.GetValueOrDefault(),
                Code = archive.ReceiptType.GetValueOrDefault().ToString(),
                TransactionTypeDescription = typeTitle
            };
            


            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Principal", Description = "Principal", Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Interest", Description = "Interest", Total = archive.Amount2.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Costs", Description = (isLienType?"Lien Costs": "Costs"), Total = archive.Amount3.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Tax", Description = "Tax", Total = archive.Amount4.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Abatement", Description = "Abatement", Total = archive.Amount5.GetValueOrDefault().ToDecimal() });
            if (archive.Amount6.GetValueOrDefault() != 0)
            {
                transaction.AddTransactionAmount(new TransactionAmountItem()
                {
                    Name = "Pre Lien Interest", Description = "Pre Lien Interest",
                    Total = archive.Amount6.GetValueOrDefault().ToDecimal()
                });
            }

            transaction.Controls.Add(new ControlItem() { Name = "Control1", Description = "Period", Value = archive.Control1 });
            transaction.Controls.Add(new ControlItem() { Name = "Control2", Description = "Code", Value = archive.Control2 });            

            return transaction;

        }
        private static AccountsReceivableTransactionBase ArchiveToAccountsReceivableTransaction(Archive archive)
        {
            var transaction = new AccountsReceivableTransactionBase()
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = TaxReceiptTypeCodeFromReceiptType(archive.ReceiptType.GetValueOrDefault()),
                AffectCash = archive.AffectCash.GetValueOrDefault(),
                AccountNumber = archive.AccountNumber.GetValueOrDefault(),
                AffectCashDrawer = archive.AffectCashDrawer.GetValueOrDefault(),
                TownId = archive.TownKey.GetValueOrDefault(),
                PayerName = archive.Name,
                Split = archive.Split.GetValueOrDefault(),
                Comment = archive.Comment,
                Code = archive.ReceiptType.GetValueOrDefault().ToString()
            };

            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount1", Description = "Amount1", Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount2", Description = "Amount2", Total = archive.Amount2.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount3", Description = "Amount3", Total = archive.Amount3.GetValueOrDefault().ToDecimal() });
            transaction.Controls.Add(new ControlItem() { Name = "Control1", Description = "Control1", Value = archive.Control1 });
            transaction.Controls.Add(new ControlItem() { Name = "Control2", Description = "Control2", Value = archive.Control2 });
            transaction.Controls.Add(new ControlItem() { Name = "Control3", Description = "Control3", Value = archive.Control3 });

            return transaction;
        }

        private static MotorVehicleTransactionBase ArchiveToMVTransaction(Archive archive)
        {
            var transaction = new MotorVehicleTransactionBase()
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = "99",
                TownId = archive.TownKey.GetValueOrDefault(),
                TransactionTypeDescription = "Motor Vehicle",
                Split = archive.Split.GetValueOrDefault(),
                PayerName = archive.Name
            };
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount1", Description = "Excise Tax", Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem(){Name = "Amount2", Description = "State Paid", Total = archive.Amount2.GetValueOrDefault().ToDecimal()});
            transaction.AddTransactionAmount(new TransactionAmountItem(){Name = "Amount3",Description = "Agent Fee", Total = archive.Amount3.GetValueOrDefault().ToDecimal()});
            transaction.AddTransactionAmount(new TransactionAmountItem(){Name = "Amount4",Description = "Sales Tax", Total = archive.Amount4.GetValueOrDefault().ToDecimal()});
            transaction.AddTransactionAmount(new TransactionAmountItem(){Name = "Amount5",Description = "Title Fee",Total = archive.Amount5.GetValueOrDefault().ToDecimal()});
            transaction.AddTransactionAmount(new TransactionAmountItem(){Name = "Amount6",Description = "Other Excise",Total = archive.Amount6.GetValueOrDefault().ToDecimal()});
            transaction.Controls.Add(new ControlItem(){Name = "Control1",Description = "Year Sticker", Value = archive.Control1});
            transaction.Controls.Add(new ControlItem(){Name = "Control2", Description = "Month Sticker", Value = archive.Control2});
            transaction.Controls.Add(new ControlItem(){Name = "Control3",Description = "MVR3",Value = archive.Control3});

            return transaction;
        }

        private static ClerkTransaction ArchiveToClerkTransaction(Archive archive)
        {
            var transaction = new ClerkTransaction()
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = ClerkTransactionCodeFromReceiptType(archive.ReceiptType.GetValueOrDefault()),
                TownId = archive.TownKey.GetValueOrDefault(),
                Split = archive.Split.GetValueOrDefault(),
                PayerName = archive.Name
            };
 
            
            //transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount1", Description = "Excise Tax", Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount2", Description = "Town Fee", Total = archive.Amount2.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount3", Description = "State Fee", Total = archive.Amount3.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount4", Description = "Clerk Tax", Total = archive.Amount4.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount5", Description = "Late Fee", Total = archive.Amount5.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount6", Description = "Misc Fee", Total = archive.Amount6.GetValueOrDefault().ToDecimal() });
            transaction.Controls.Add(new ControlItem() { Name = "Control1", Description = "Control1", Value = archive.Control1 });
            
            return transaction;
        }

        private static ClerkVitalTransaction ArchiveToVitalTransaction(Archive archive)
        {
            var transaction = new ClerkVitalTransaction()
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = ClerkTransactionCodeFromReceiptType(archive.ReceiptType.GetValueOrDefault()),
                TownId = archive.TownKey.GetValueOrDefault(),
                Split = archive.Split.GetValueOrDefault(),
                PayerName = archive.Name
            };
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount1", Description = "VitalRecords", Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount2", Description = "State Fee", Total = archive.Amount2.GetValueOrDefault().ToDecimal() });
            transaction.Controls.Add(new ControlItem() { Name = "Control1", Description = "Control1", Value = archive.Control1 });
            transaction.Controls.Add(new ControlItem() { Name = "Control2", Description = "Control2", Value = archive.Control1 });

            return transaction;
        }

        private static int YearBillFromReference(string reference)
        {
            if (string.IsNullOrWhiteSpace(reference))
            {
                return 0;
            }
            return reference.Split('-').LastOrDefault().ToIntegerValue();
        }

        private static string TaxReceiptTypeCodeFromReceiptType(int receiptType)
        {
            switch (receiptType)
            {
                case 90:
                    return "RETAX";
                case 91:
                    return "RETAXLIEN";
                case 92:
                    return "PPTAX";
                case 891:
                    return "TARETAXLIEN";
                case 890:
                    return "TARETAX";
            }

            return "RETAX";
        }

        private static PropertyTaxTransaction ArchiveToPropertyTaxTransaction(Archive archive)
        {
            var transaction = new PropertyTaxTransaction()
            {
                ActualDateTime = archive.ActualSystemDate.GetValueOrDefault(),
                Reference = archive.Ref,
                CorrelationIdentifier = archive.ReceiptIdentifier.GetValueOrDefault(),
                EffectiveDateTime = archive.ArchiveDate.GetValueOrDefault(),
                IncludeInCashDrawerTotal = archive.AffectCashDrawer.GetValueOrDefault(),
                Id = archive.TransactionIdentifier.GetValueOrDefault(),
                PayerPartyId = archive.NamePartyID.GetValueOrDefault(),
                TransactionTypeCode = TaxReceiptTypeCodeFromReceiptType(archive.ReceiptType.GetValueOrDefault()),
                AffectCash = archive.AffectCash.GetValueOrDefault(),
                BillType = archive.ReceiptType.GetValueOrDefault() == 92
                    ? PropertyTaxBillType.Personal
                    : PropertyTaxBillType.Real,
                TownId = archive.TownKey.GetValueOrDefault(),
                Account = archive.AccountNumber.GetValueOrDefault(),
                YearBill = YearBillFromReference(archive.Ref),
                PayerName =  archive.Name,
                Split = archive.Split.GetValueOrDefault()
            };
            transaction.IsLien = transaction.TransactionTypeCode == "RETAXLIEN" ||
                                 transaction.TransactionTypeCode == "TARETAXLIEN";
            transaction.IsTaxAcquired = transaction.TransactionTypeCode == "TARETAX" ||
                                        transaction.TransactionTypeCode == "TARETAXLIEN";
            

            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Principal", Description = "Principal", Total = archive.Amount1.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Interest", Description = "Interest", Total = archive.Amount2.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Costs", Description = "Costs", Total = archive.Amount4.GetValueOrDefault().ToDecimal() });
            transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Discount", Description = "Discount", Total = archive.Amount3.GetValueOrDefault().ToDecimal() });
            //transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount5", Description = "Title Fee", Total = archive.Amount5.GetValueOrDefault().ToDecimal() });
            //transaction.AddTransactionAmount(new TransactionAmountItem() { Name = "Amount6", Description = "Other Excise", Total = archive.Amount6.GetValueOrDefault().ToDecimal() });
            transaction.Controls.Add(new ControlItem() { Name = "Control1", Description = "Control1", Value = archive.Control1 });
            transaction.Controls.Add(new ControlItem() { Name = "Control2", Description = "Control2", Value = archive.Control2 });
            transaction.Controls.Add(new ControlItem() { Name = "Control3", Description = "Control3", Value = archive.Control3 });

            return transaction;
        }
        public static IEnumerable<Archive> ToArchives(this IEnumerable<LastYearArchive> lastYearArchives)
        {
            return lastYearArchives.Select(l => l.ToArchive());
        }

        public static Archive ToArchive(this LastYearArchive lastYearArchive)
        {
            return new Archive()
            {
                AffectCash = lastYearArchive.AffectCash.GetValueOrDefault(),
                Name = lastYearArchive.Name,
                ARBillType = lastYearArchive.ARBillType.GetValueOrDefault(),
                AffectCashDrawer = lastYearArchive.AffectCashDrawer.GetValueOrDefault(),
                ArchiveDate = lastYearArchive.ArchiveDate,
                Account6 = lastYearArchive.Account6,
                ActualSystemDate = lastYearArchive.ActualSystemDate,
                Amount3 = lastYearArchive.Amount3,
                Amount2 = lastYearArchive.Amount2,
                Amount5 = lastYearArchive.Amount5,
                Amount4 = lastYearArchive.Amount4,
                Amount6 = lastYearArchive.Amount6,
                Amount1 = lastYearArchive.Amount1,
                Account1 = lastYearArchive.Account1,
                Account2 = lastYearArchive.Account2,
                Account3 = lastYearArchive.Account3,
                Account4 = lastYearArchive.Account4,
                Account5 = lastYearArchive.Account5,
                AccountNumber = lastYearArchive.AccountNumber.GetValueOrDefault(),
                ArchiveCode = lastYearArchive.ArchiveCode.GetValueOrDefault(),
                ConvenienceFee = lastYearArchive.ConvenienceFee.GetValueOrDefault(),
                Comment = lastYearArchive.Comment,
                Control2 = lastYearArchive.Control2,
                Control3 = lastYearArchive.Control3,
                Control1 = lastYearArchive.Control1,
                CardPaidAmount = lastYearArchive.CardPaidAmount.GetValueOrDefault(),
                CashPaidAmount = lastYearArchive.CashPaidAmount.GetValueOrDefault(),
                CheckPaidAmount = lastYearArchive.CheckPaidAmount.GetValueOrDefault(),
                CollectionCode = lastYearArchive.CollectionCode,
                ConvenienceFeeGLAccount = lastYearArchive.ConvenienceFeeGLAccount,
                DailyCloseOut = lastYearArchive.DailyCloseOut.GetValueOrDefault(),
                DefaultAccount = lastYearArchive.DefaultAccount,
                DefaultCashAccount = lastYearArchive.DefaultCashAccount,
                DefaultMIAccount = lastYearArchive.DefaultMiaccount,
                EFT = lastYearArchive.Eft,
                NamePartyID = lastYearArchive.NamePartyId,
                Id = 0,
                Project1 = lastYearArchive.Project1,
                Project2 = lastYearArchive.Project2,
                Project3 = lastYearArchive.Project3,
                Project4 = lastYearArchive.Project4,
                Project5 = lastYearArchive.Project5,
                Project6 = lastYearArchive.Project6,
                Quantity = lastYearArchive.Quantity.GetValueOrDefault(),
                ReceiptId = 0,
                ReceiptIdentifier = lastYearArchive.ReceiptIdentifier,
                ReceiptTitle = lastYearArchive.ReceiptTitle,
                ReceiptType = lastYearArchive.ReceiptType.GetValueOrDefault(),
                RecordKey = 0,
                Ref = lastYearArchive.Ref,
                SeperateCreditCardGLAccount = lastYearArchive.SeperateCreditCardGlaccount,
                Split = lastYearArchive.Split.GetValueOrDefault(),
                TellerID = lastYearArchive.TellerId,
                TransactionIdentifier = lastYearArchive.TransactionIdentifier,
                TellerCloseOut = lastYearArchive.TellerCloseOut.GetValueOrDefault(),
                TownKey = lastYearArchive.TownKey.GetValueOrDefault()
            };
        }

        public static IEnumerable<CreditCardMaster> ToCreditCardMasters(
            this IEnumerable<LastYearCreditCardMaster> lastYearCreditCards)
        {
            return lastYearCreditCards.Select(c => c.ToCreditCardMaster());
        }

        public static CreditCardMaster ToCreditCardMaster(this LastYearCreditCardMaster lastYearCreditCard)
        {
            return new CreditCardMaster()
            {
                Amount = lastYearCreditCard.Amount.GetValueOrDefault(),
                ID = 0,
                AuthCode = lastYearCreditCard.AuthCode,
                AuthCodeConvenienceFee = lastYearCreditCard.AuthCodeConvenienceFee,
                AuthCodeNonTaxConvenienceFee = lastYearCreditCard.AuthCodeNonTaxConvenienceFee,
                AuthCodeVISANonTax = lastYearCreditCard.AuthCodeVISANonTax,
                CCType = lastYearCreditCard.CCType.GetValueOrDefault(),
                ConvenienceFee = lastYearCreditCard.ConvenienceFee.GetValueOrDefault(),
                CryptCard = lastYearCreditCard.CryptCard,
                CryptCardExpiration = lastYearCreditCard.CryptCardExpiration,
                EPmtAcctID = lastYearCreditCard.EPmtAcctID,
                EPymtRefNumber = lastYearCreditCard.EPymtRefNumber,
                IsDebit = lastYearCreditCard.IsDebit.GetValueOrDefault(),
                IsVISA = lastYearCreditCard.IsVISA.GetValueOrDefault(),
                Last4Digits = lastYearCreditCard.Last4Digits,
                MaskedCreditCardNumber = lastYearCreditCard.MaskedCreditCardNumber,
                NonTaxAmount = lastYearCreditCard.NonTaxAmount.GetValueOrDefault(),
                NonTaxConvenienceFee = lastYearCreditCard.NonTaxConvenienceFee.GetValueOrDefault(),
                OriginalReceiptKey = lastYearCreditCard.OriginalReceiptKey.GetValueOrDefault(),
                ReceiptId = lastYearCreditCard.ReceiptId.GetValueOrDefault(),
                SecureGUID = lastYearCreditCard.SecureGUID
            };
        }

        public static IEnumerable<CheckMaster> ToCheckMasters(this IEnumerable<LastYearCheckMaster> lastYearChecks)
        {
            return lastYearChecks.Select(c => c.ToCheckMaster());
        }

        public static CheckMaster ToCheckMaster(this LastYearCheckMaster lastYearCheck)
        {
            return new CheckMaster()
            {
                ID = 0,
                AccountType = lastYearCheck.AccountType,
                Amount = lastYearCheck.Amount.GetValueOrDefault(),
                BankId = lastYearCheck.BankId.GetValueOrDefault(),
                CheckNumber = lastYearCheck.CheckNumber,
                EFT = lastYearCheck.EFT.GetValueOrDefault(),
                EPymtRefNumber = lastYearCheck.EPymtRefNumber,
                Last4Digits = lastYearCheck.Last4Digits,
                OriginalReceiptKey = lastYearCheck.OriginalReceiptKey.GetValueOrDefault(),
                ReceiptId = lastYearCheck.ReceiptId.GetValueOrDefault()
            };
        }
        private static string ClerkTransactionCodeFromReceiptType(int receiptType)
        {
            switch (receiptType)
            {
                case 800:
                    return "DOG";
                case 801:
                    return "DEA";
                case 802:
                    return "BIR";
                case 803:
                    return "MAR";
                case 804:
                    return "BUR";
            }

            return "98";
        }
    }
}




