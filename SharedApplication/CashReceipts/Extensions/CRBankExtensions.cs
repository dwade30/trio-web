﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts.Extensions
{
    public static class CRBankExtensions
    {
        public static IEnumerable<DescriptionIDPair> ToDescriptionIDPairs(this IEnumerable<CRBank> banks)
        {
            return banks.Select(b => b.ToDescriptionIdPair());
        }

        public static DescriptionIDPair ToDescriptionIdPair(this CRBank bank)
        {
            if (bank != null)
            {
                return new DescriptionIDPair(){Description = bank.Name, ID = bank.ID};
            }
            return new DescriptionIDPair();
        }
    }
}