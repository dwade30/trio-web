﻿using System.Collections.Generic;
using System.Linq;
using SharedApplication.CashReceipts.Models;

namespace SharedApplication.CashReceipts.Extensions
{
    public static class ReceiptTypeExtensions
    {
        public static IEnumerable<DescriptionIDPair> ToDescriptionCodePairs(this IEnumerable<ReceiptType> receiptTypes)
        {
            if (receiptTypes != null && receiptTypes.Any())
            {
                return receiptTypes.Select(r => r.ToDescriptionCodePair());
            }
            return new List<DescriptionIDPair>();
        }

        public static DescriptionIDPair ToDescriptionCodePair(this ReceiptType receiptType)
        {
            return new DescriptionIDPair(){ID = receiptType.TypeCode.GetValueOrDefault(), Description = receiptType.TypeTitle};
        }

        public static bool IsRealEstateType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 90:
                case 91:
                case 890:
                case 891:
                    return true;
            }

            return false;
        }

        public static bool IsPersonalPropertyType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 92:
                    return true;
            }

            return false;
        }

        public static bool IsPropertyTaxType(this ReceiptType receiptType)
        {
            return receiptType.IsRealEstateType() || receiptType.IsPersonalPropertyType();
        }
        public static bool IsUtilityType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 93:
                case 94:
                case 95:
                case 96:
                case 893:
                case 894:
                case 895:
                case 896:
                    return true;
            }

            return false;
        }

        public static bool IsWaterType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 93:
                case 96:
                case 893:
                case 896:
                    return true;
            }

            return false;
        }

        public static bool IsSewerType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 94:
                case 95:
                case 894:
                case 895:
                    return true;
            }

            return false;
        }
        public static bool IsClerkType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 98:
                case 800:
                case 801:
                case 802:
                case 803:
                case 804:
                    return true;
            }
            return false;
        }

        public static bool IsVitalsType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 801:
                case 802:
                case 803:
                case 804:
                    return true;
            }
            return false;
        }

        public static bool IsDogsType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 800:
                    return true;
            }
            return false;
        }

        public static bool IsDeathsType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }
            switch (receiptType.TypeCode)
            {
                case 801:
                    return true;
            }
            return false;
        }

        public static bool IsBurialType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }
            switch (receiptType.TypeCode)
            {
                case 804:
                    return true;
            }
            return false;
        }

        public static bool IsMarriageType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }
            switch (receiptType.TypeCode)
            {
                case 803:
                    return true;
            }
            return false;
        }

        public static bool IsBirthType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }
            switch (receiptType.TypeCode)
            {
                case 804:
                    return true;
            }
            return false;
        }

        public static bool IsAccountsReceivableType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 97:
                    return true;
            }
            return false;
        }

        public static bool IsMotorVehicleType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 99:
                    return true;
            }
            return false;
        }

        public static bool IsMoses(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 190:
                    return true;
            }
            return false;
        }

        public static bool IsSystemDefinedType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            return receiptType.IsAccountsReceivableType() || receiptType.IsClerkType() || receiptType.IsMoses() ||
                   receiptType.IsMotorVehicleType() || receiptType.IsPersonalPropertyType() ||
                   receiptType.IsRealEstateType() || receiptType.IsUtilityType();
        }

        public static bool IsLienType(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 91:
                case 891:
                case 95:
                case 96:
                case 895:
                case 896:
                    return true;
            }

            return false;
        }

        public static bool IsTaxAcquired(this ReceiptType receiptType)
        {
            if (receiptType == null)
            {
                return false;
            }

            switch (receiptType.TypeCode)
            {
                case 890:
                case 891:
                case 892:
                case 893:
                case 894:
                case 895:
                case 896:
                    return true;
            }

            return false;
        }
    }
}