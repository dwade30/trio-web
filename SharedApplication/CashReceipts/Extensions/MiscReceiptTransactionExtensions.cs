﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts.Extensions
{
	public static class MiscReceiptTransactionExtensions
	{
		public static MiscReceiptTransactionBase ToTransactionBase(this MiscReceiptTransaction transaction)
		{
			var result =  new MiscReceiptTransactionBase
			{
				ActualDateTime = DateTime.Today,
				EffectiveDateTime = DateTime.Today,
				Controls = transaction.Controls,
				CorrelationIdentifier = transaction.CorrelationIdentifier,
				IncludeInCashDrawerTotal = transaction.AffectCashDrawer,
				Id = transaction.Id,
				
				PayerName = transaction.PaidBy,
				Reference = transaction.Reference,
				TransactionTypeCode = transaction.TransactionTypeCode.ToString(),
				TransactionDetails = transaction,
				TransactionTypeDescription = transaction.TransactionTypeDescription,
			};

			result.AddTransactionAmounts(transaction.TransactionAmounts.ToTransactionItems());

			return result;
		}

		public static IEnumerable<MiscReceiptTransactionBase> ToTransactionBases(
			this IEnumerable<MiscReceiptTransaction> transactions)
		{
			return transactions.Select(x => x.ToTransactionBase());
		}
	}

	public static class TransactionAmountExtensions
	{
		public static TransactionAmountItem ToTransactionAmountItem(this MiscReceiptAmountItem miscReceiptAmountItem)
		{
			return new TransactionAmountItem
			{
				Total = miscReceiptAmountItem.Total,
				Name = miscReceiptAmountItem.Name,
				Description = miscReceiptAmountItem.Description
			};
		}

		public static IEnumerable<TransactionAmountItem> ToTransactionItems(this IEnumerable<MiscReceiptAmountItem> miscReceiptTransactions)
		{
			return miscReceiptTransactions.Select(ToTransactionAmountItem);
		}
	}
}
