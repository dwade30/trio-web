﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
	public class MiscReceiptTransaction
	{
		public Guid CorrelationIdentifier { get; set; }
		public int Quantity { get; set; }
		public bool AffectCashDrawer { get; set; }
		public bool AffectCash { get; set; }
		public decimal PercentageAmount { get; set; }
		public int TownCode { get; set; }
		public string GlAccount { get; set; }
		public string Comment { get; set; }
		public int RecordKey { get; set; } = 0;
		public string CollectionCode { get; set; } = "";
		public string PaidBy { get; set; } = "";

		protected new List<MiscReceiptAmountItem> Amounts = new List<MiscReceiptAmountItem>();
		public IEnumerable<MiscReceiptAmountItem> TransactionAmounts
		{
			get { return Amounts; }
		}

		public void AddMiscReceiptTransactionAmount(MiscReceiptAmountItem amountItem)
		{
			if (amountItem != null)
			{
				Amounts.Add(amountItem);
			}
		}

		public Guid Id { get; set; } = Guid.NewGuid();
		public string Reference { get; set; } = "";
		public List<ControlItem> Controls = new List<ControlItem>();
		public int TransactionTypeCode { get; set; }
		public string TransactionTypeDescription { get; set; }
        public string DefaultCashAccount { get; set; }
	}
}
