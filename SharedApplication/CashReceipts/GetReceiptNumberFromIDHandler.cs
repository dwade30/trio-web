﻿using System.Linq;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class GetReceiptNumberFromIDHandler : CommandHandler<GetReceiptNumberFromID,int>
    {
        private ICashReceiptsContext crContext;
        public GetReceiptNumberFromIDHandler(ICashReceiptsContext crContext)
        {
            this.crContext = crContext;
        }
        protected override int Handle(GetReceiptNumberFromID command)
        {
            var receiptNumber = crContext.Receipts.Where(r => r.Id == command.ReceiptID).Select(r => r.ReceiptNumber)
                .FirstOrDefault().GetValueOrDefault();
            if (receiptNumber > 0)
            {
                return receiptNumber;
            }

            receiptNumber = crContext.LastYearReceipts.Where(r => r.Id == command.ReceiptID)
                .Select(r => r.ReceiptNumber).FirstOrDefault().GetValueOrDefault();
            return receiptNumber;
        }
    }
}