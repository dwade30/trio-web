﻿using System;
using System.Collections.Generic;
using SharedApplication.Receipting;

namespace SharedApplication.CashReceipts
{
    public class ReceiptDetailItem
    {
        public string TypeCode { get; set; }
        public string TypeDescription { get; set; }
        //public string Name { get; set; }
        public string Reference { get; set; }
        public List<ControlItem> Controls { get; set; }
        public IEnumerable<TransactionAmountItem> TransactionAmounts { get; set; }
        public IEnumerable<(string Title, string Value)> TransactionSpecificDetails { get; set; }
    }
}