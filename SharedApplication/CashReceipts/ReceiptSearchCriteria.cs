﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CashReceipts.Enums;

namespace SharedApplication.CashReceipts
{
	public class ReceiptSearchCriteria
	{
		public int ReceiptId { get; set; } = 0;
		public int ReceiptNumber { get; set; } = 0;
		public DateTime? ReceiptDate { get; set; }
        public VoidedSearchOption VoidedCriteria { get; set; } = VoidedSearchOption.All;
    }
}
