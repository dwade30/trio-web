﻿using SharedApplication.Messaging;

namespace SharedApplication.CashReceipts
{
    public class SelectReceiptFromListHandler : CommandHandler<SelectReceiptFromList,int>
    {
        private IModalView<ISelectReceiptFromListViewModel> selectView;
        public SelectReceiptFromListHandler(IModalView<ISelectReceiptFromListViewModel> selectView)
        {
            this.selectView = selectView;
        }
        protected override int Handle(SelectReceiptFromList command)
        {
            selectView.ViewModel.ReceiptList = command.ReceiptList;
            selectView.ShowModal();
            if (selectView.ViewModel.Cancelled)
            {
                return 0;
            }

            return selectView.ViewModel.SelectedReceiptId;
        }
    }
}