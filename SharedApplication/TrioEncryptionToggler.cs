﻿using System;
using System.Linq;
using System.Text;

namespace SharedApplication
{
    public class TrioEncryptionToggler : ITrioEncryptionToggler
    {
        public string ToggleEncryptionCode(string code)
        {
            StringBuilder toggledCode = new StringBuilder();
   
            if (code != "")
            {
                var coded = code.ToUpper().Reverse();
                foreach (var codedChar in coded)
                {                    
                    toggledCode.Append(DecodeCharacter(codedChar));
                }

            }

            return toggledCode.ToString();
        }

        private char DecodeCharacter(char codedChar)
        {
            return Convert.ToChar(Convert.ToByte(codedChar) ^ 240);
        }
    }
}