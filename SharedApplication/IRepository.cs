﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;


namespace SharedApplication
{
    public interface IEntity : IIdentifiableEntity<int>
    {

    }

    public interface IRepository<TEntity> : IRepository<TEntity,int> where TEntity: class
    {
    }

    public interface IRepository<TEntity, TIdType> where TEntity : class
    {
        TEntity Get(TIdType id);

        IEnumerable<TEntity> GetIncluding(Expression<Func<TEntity, bool>> whereProperty,
            params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        void Add(TEntity entity);
        //void Remove(TIdType id);
        void Remove(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
       // void RemoveRange(IEnumerable<TIdType> entityIds);
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}
