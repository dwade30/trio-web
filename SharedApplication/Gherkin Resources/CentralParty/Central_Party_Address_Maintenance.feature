﻿Feature: Central_Party_Address_Maintenance
	In order to keep addresses usable throughout TRIO
	As a central party admin
	I want automatic help keeping a single Primary address defined per party

Scenario: Adding new Primary address to list of addresses
	Given 0-to-many existing party addresses	
	When add a new address marked as Primary
	Then existing addresses will all be set as Override addresses
	And new address will be set as the Primary address

Scenario: Adding new Override address to list of addresses already having Primary
	Given existing party addresses	
	And addresses contains Primary address
	When add a new address as Override
	Then new address will be set as an Override address

Scenario: Adding new Override address to list of addresses without a Primary
	Given 0-to-many existing party addresses
	And addresses do not contain Primary address
	When add a new address as Override
	Then new address will be saved as the Primary address


Scenario: Update Primary address to be an Override address when another Primary address does not exist
	Given a party address marked as Primary
	And the party address is the only Primary address
	When marking the existing party address as an Override address
	Then call an error because the action would give party no Primary address

Scenario: Update Primary address to be an Override address when another Primary address exists
	Given a party address marked as Primary
	And the party address is not the only Primary address
	When marking the address as an Override address
	Then updated address will be set as an Override address
	

Scenario: Update Primary address record data when no other Primary address exists
	Given a party address marked as Primary
	And the party address is the only Primary address
	When changing the address data
	Then save the address changes

Scenario: Update Primary address record data when another Primary address exists
	Given a party address marked as Primary
	And the party address is not the only Primary address
	When changing the address data
	Then existing addresses will all be set as Override addresses
	And save the address changes

Scenario: Update Override address record data when no Primary address exists
	Given a party address marked as Override
	And no Primary address exists
	When changing the address data
	Then mark the address as Primary
	And save the address changes

Scenario: Update Override address record data when a single Primary address exists
	Given a party address marked as Override
	And a single Primary address exists
	When changing the address data
	Then save the address changes

Scenario: Update Override address record data when multiple Primary addresses exists
	Given a party address marked as Override
	And multiple Primary addresses exists
	When changing the address data
	Then call an error because the action would give party multiple Primary addresses


Scenario: Deleting party address with no other addresses available
	Given a party address
	And no other addresses exist
	When deleting the address
	Then delete the address

Scenario: Deleting party address with only Override addresses available
	Given a party address
	And all other addresses are Override addresses 
	When deleting the address
	Then delete the address
	And set first remaining address as Primary

Scenario: Deleting party address with 1 Primary address available
	Given a party address
	And 1 remaining address is Primary 
	When deleting the address
	Then delete the address

Scenario: Delete party address with multiple Primary addresses available
	Given a party address 
	And multiple Primary addresses exists
	When deleting the address
	Then call an error because the action would give party multiple Primary addresses
