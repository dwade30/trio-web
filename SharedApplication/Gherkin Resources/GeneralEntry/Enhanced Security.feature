﻿Feature: Enhanced Security
	In order to make the system more secure for users
	As a software system
	I want to protect user accounts

@Login
Scenario: User must enter their own username
	Given a user wants access to the system
	When they log into the system
	Then they will be required to type in their user name

Scenario: User is locked out after 3 failed attempts to login
	Given a user is trying to log in
	When a user fails to enter proper credentials to login in on 3 consecutive attempts
	Then they will be locked out of the system
	And the user will be notified that their account has been locked and to either wait 10 minutes or have someone with security access unlock their account

Scenario: User account is unlocked automatically
	Given a user is locked out
	When 10 minutes has passed
	Then they will be able to log into the system

Scenario: User account is unlocked manually
	Given a user is locked out
	When a user with permissions to set security goes into the user account and unlocks it
	Then they will be able to log into the system

Scenario: User names are not case sensitive
	Given a user is trying to log in
	When a user enters a user name in a different case from what is stored in the system
	And they enter a valid password
	Then they will be able to log in successfully

Scenario: Passwords are case sensitive
	Given a user is trying to log in
	When a user enters a valid user name
	And they enter a password in a different case from what was stored in the system
	Then they will not be able to log in
	And a message will be displayed notifying the user they entered an invalid user name or password
	And a failed attempt will be recorded for that user

Scenario: Inactive users are not allowed into the system
	Given a user is trying to log in
	When a user enters a user name that is marked as inactive in the system
	Then they will not be able to log in

Scenario: Password does not meet minimum requirements
	Given a user is trying to log in
	When a user enters correct credentials but the entered password does not meet the required password length
	Then they will be notified that the password does not meet minimum system requirements 
	And they will be required to immediately change their password to one that does meet minimum system requirements


@Change Password
Scenario: Minimum password length
	Given a user is trying to update their password
	When they enter a password that does not meet the 8 character minimum length
	Then they will be notified of the 8 character length needed for a password
	And they will not be allowed to save this entered password

Scenario: Common word password requirement
	Given a user is trying to update their password
	When they enter a password that is included in a list of common passwords
	Then they will be notified that they can't choose to use what they entered for a password
	And they will not be allowed to save this entered password

Scenario: Password can't match last 5 passwords
	Given a user is trying to update their password
	When they enter a password that they have previously used in their last 5 password changes
	Then they will be notified that they can't use a previously used password
	And they will not be allowed to save this entered password

Scenario: Password strength indicator
	Given a user is trying to update their password
	When they entering information for the password change
	Then they will see a password strength indicator that will change as they enter characters

Scenario: Current password entry
	Given a user is trying to update their password
	When they entering information for the password change
	Then they will be required to enter their current password as well

Scenario: Current password failed check
	Given a user is trying to update their password
	When the existing password they entered does not match what we have stored for the user
	Then they will be notified that they have entered an invalid password
	And they will not be allowed to update their password

Scenario: Valid new password
	Given a user is trying to update their password
	When the existing password they entered matches what we have in the system
	And the new password the entered meets the password length requirements
	And the new password is not in the list of common passwords the system checks against
	And the new password is not a password that has been used in the last 5 password changes
	Then they will be notified that they have successfully updated their password
	And the password will be updated in the system


@Security Maintenance
Scenario: Unlock user account
	Given the user has access to the security setup screen
	When the user selects an active user in the system
	And the user chooses the option to unlock the user
	Then that selected users account should now be allowed to log into the system

Scenario: Inactivate user account
	Given the user has access to the security setup screen
	When the user selects an active user in the system
	And the user sets that account to inactive
	Then user account will no longer be allowed to log into the system

Scenario: Minimum password length
	Given a user with rights to the security maintenance screen is trying to update the password of a selected user
	When they enter a password that does not meet the 8 character minimum length
	Then they will be notified of the 8 character length needed for a password
	And they will not be allowed to save this entered password

Scenario: Common word password requirement
	Given a user with rights to the security maintenance screen is trying to update the password of a selected user
	When they enter a password that is included in a list of common passwords
	Then they will be notified that they can't choose to use what they entered for a password
	And they will not be allowed to save this entered password

Scenario: Password can't match last 5 passwords
	Given a user with rights to the security maintenance screen is trying to update the password of a selected user
	When they enter a password that they have previously used in their last 5 password changes
	Then they will be notified that they can't use a previously used password
	And they will not be allowed to save this entered password

Scenario: Valid new password
	Given a user is trying to update their password
	When the existing password they entered matches what we have in the system
	And the new password the entered meets the password length requirements
	And the new password is not in the list of common passwords the system checks against
	And the new password is not a password that has been used in the last 5 password changes
	Then they will be notified that they have successfully updated their password
	And the password will be updated in the system

Scenario: Password strength indicator
	Given a user is trying to update their password
	When they entering information for the password change
	Then they will see a password strength indicator that will change as they enter characters
