﻿namespace SharedApplication.Messaging
{
    public class BaseEndpointController
    {
        public CommandDispatcher Dispatcher { get; set; }

        public BaseEndpointController()
        {
            
        }

        public BaseEndpointController(CommandDispatcher dispatcher)
        {
           
            Dispatcher = dispatcher;
        }
    }
}
