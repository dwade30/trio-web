﻿using MediatR;

namespace SharedApplication.Messaging
{
    public abstract class DomainEventHandler<TEvent> : NotificationHandler<TEvent> where TEvent : DomainEvent
    {
        
    }
}