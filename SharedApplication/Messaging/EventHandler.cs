﻿using MediatR;

namespace SharedApplication.Messaging
{
    public abstract class EventHandler<TEvent> : NotificationHandler<TEvent> where TEvent : Message
    {
        
    }
}