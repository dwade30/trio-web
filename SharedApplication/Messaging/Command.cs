﻿using System;
using MediatR;

namespace SharedApplication.Messaging
{
    public abstract class Command<TResponseType> : ICommand<TResponseType>
    {
        public Guid Id { get; set; } = Guid.NewGuid();
    }

    public abstract class Command : Command<Unit> , SharedApplication.Messaging.ICommand
    {
        protected Command()
        {
           
        }
    }
}