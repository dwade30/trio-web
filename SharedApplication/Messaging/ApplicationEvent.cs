﻿using System;

namespace SharedApplication.Messaging
{
    public class ApplicationEvent : Message
    {
        public Guid Id { get; set; }
    }
}