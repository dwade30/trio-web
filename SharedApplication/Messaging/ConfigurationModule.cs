﻿using Autofac;

namespace SharedApplication.Messaging
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MediatrCommandDispatcher>().As<CommandDispatcher>();
            builder.RegisterType<MediatrCommandDispatcher>().As<EventPublisher>();
        }
    }
}