﻿using System;

namespace SharedApplication.Messaging
{
    public abstract class DomainEvent : Message
    {
        public Guid Id { get; set; }
        public int Version { get; set; }
        
    }
}