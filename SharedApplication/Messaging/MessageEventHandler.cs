﻿using MediatR;

namespace SharedApplication.Messaging
{
    public abstract class MessageEventHandler<TEvent> : NotificationHandler<TEvent> where TEvent : Message
    {
        
    }
}