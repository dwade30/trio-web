﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SharedApplication.Messaging
{
    public abstract class CommandHandler<TCommand, TResponse> : ICommandHandler<TCommand, TResponse> where TCommand : ICommand<TResponse>
    {
        public Task<TResponse> Handle(TCommand command, CancellationToken cancellationToken)
        {
            return Task.FromResult(Handle(command));
        }

        protected abstract TResponse Handle(TCommand command);
    }

    public abstract class CommandHandler<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        public Task<Unit> Handle(TCommand command, CancellationToken cancellationToken)
        {
            Handle(command);
            return Task.FromResult(new Unit());
        }

        protected abstract void Handle(TCommand command);
    }
}