﻿namespace SharedApplication.Messaging
{
    public interface EventPublisher
    {
        void Publish<TEvent>(TEvent eventNotification) where TEvent : Message;
    }
}