﻿using System;
using MediatR;

namespace SharedApplication.Messaging
{
    public interface Message : INotification
    {
        Guid Id { get; }
    }
}