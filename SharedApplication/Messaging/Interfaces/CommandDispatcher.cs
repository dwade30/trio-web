﻿using System.Threading.Tasks;

namespace SharedApplication.Messaging
{
    public interface CommandDispatcher
    {
        void Send(Command command);
        Task<TResponseType> Send<TResponseType>(Command<TResponseType> command);
    }

}