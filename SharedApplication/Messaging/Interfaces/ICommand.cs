﻿using MediatR;
using System;

namespace SharedApplication.Messaging
{
    public interface ICommand<TResponseType> : IRequest<TResponseType>
    {
        Guid Id { get; set; }
    }

    public interface ICommand : ICommand<Unit>
    {

    }
}