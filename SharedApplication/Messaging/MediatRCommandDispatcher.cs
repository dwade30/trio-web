﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SharedApplication.Telemetry;

namespace SharedApplication.Messaging
{
    public class MediatrCommandDispatcher : CommandDispatcher, EventPublisher
    {
        private IMediator mediator;
        private ITelemetryService telemetry;

        public MediatrCommandDispatcher(IMediator mediator, ITelemetryService telemetry)
        {
            this.mediator = mediator;
            this.telemetry = telemetry;
        }

        public async void Send(Command command)
        {
            try
            {
                await mediator.Send(command);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                telemetry.TrackException(e);
                throw;
            }
        }

        public async Task<TResponseType> Send<TResponseType>(Command<TResponseType> command)
        {

            try
            {
                var ret = await mediator.Send<TResponseType>(command);
                return ret;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                telemetry.TrackException(e);
                throw;
            }
        }

        public void Publish<TEvent>(TEvent eventNotification) where TEvent : Message
        {

            mediator.Publish<TEvent>(eventNotification, new CancellationToken());
        }
    }
}