﻿using System;

namespace SharedApplication
{
    public interface IEffectiveDateChangeViewModel
    {
        DateTime EffectiveDate { get; set; }
        bool UpdateRecordedDate { get; set; }
        bool AllowChangingRecordedDate { get; set; }
        bool Cancelled { get; set; }
        void SetEffectiveDate(EffectiveDateChoice choice);
        void Cancel();
    }
}