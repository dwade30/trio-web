﻿using System;

namespace SharedApplication
{
    public interface IBillPaymentUtility
    {
        decimal CalculateInterest(decimal currentAmount, DateTime interestStartDate, DateTime asOfDate, decimal interestRateAsDecimal, decimal overPaymentInterestRate, int periodsInYear);
        (decimal interest, decimal perDiem) CalculateInterestAndPerDiem(decimal currentAmount, DateTime interestStartDate, DateTime asOfDate, decimal interestRateAsDecimal, decimal overPaymentInterestRate, int periodsInYear);
    }
}