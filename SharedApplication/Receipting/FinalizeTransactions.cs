﻿using System;
using SharedApplication.Messaging;
using System.Collections.Generic;

namespace SharedApplication.Receipting
{
    public class FinalizeTransactions<TTransactionType> : Command<bool> where TTransactionType : TransactionBase
    {
        public Guid CorrelationIdentifier { get; set; }
        public IEnumerable<TTransactionType> Transactions { get; set; }
        public int ReceiptId { get; set; }
		public string TellerId { get; set; }
        public bool AllowUIInteraction { get; set; } = true;
    }
}