﻿using System.Collections.Generic;

namespace SharedApplication.Receipting
{
    public class TransactionCompleted : ReceiptingEventBase
    {
        public IEnumerable<TransactionBase> CompletedTransactions { get; set; }
        public string OverridePaidByName { get; set; } = "";
        public int OverridePaidByPartyId { get; set; } = 0;
    }
}