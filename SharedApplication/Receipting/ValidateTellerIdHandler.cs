﻿using System.Linq;
using SharedApplication.Authorization;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Interfaces;

namespace SharedApplication.Receipting
{
    public class ValidateTellerIdHandler : CommandHandler<ValidateTellerId,TellerValidationResult>
    {
        private ISystemSettingsContext systemSettingsContext;
        private GlobalCashReceiptSettings crSettings;
        private ITrioEncryptionToggler encryptionToggler;
        private IUserPermissionSet currentUserPermissionSet;

        public ValidateTellerIdHandler(ISystemSettingsContext systemSettingsContext, GlobalCashReceiptSettings crSettings, ITrioEncryptionToggler encryptionToggler, IUserPermissionSet currentUserPermissionSet)
        {
            this.systemSettingsContext = systemSettingsContext;
            this.crSettings = crSettings;
            this.encryptionToggler = encryptionToggler;
            this.currentUserPermissionSet = currentUserPermissionSet;
        }

        protected override TellerValidationResult Handle(ValidateTellerId command)
        {
	        if (command.TellerId == "617" && currentUserPermissionSet.UserId != -1)
	        {
		        return TellerValidationResult.Invalid;
			}

	        var teller = systemSettingsContext.Operators.FirstOrDefault(o => o.Code == command.TellerId);
			if (teller == null)
			{
				return TellerValidationResult.Invalid;
			}

			if (crSettings.TellerIdUsage == TellerIDUsage.RequirePassword && currentUserPermissionSet.UserId != -1)
			{
				if (encryptionToggler.ToggleEncryptionCode(teller.Password) != command.Password.ToUpper())
				{
					return TellerValidationResult.Invalid;
				}
			}

			if (teller.LoggedIn.GetValueOrDefault())
			{
				return TellerValidationResult.LoggedIn;
			}
            
            return TellerValidationResult.Valid;
        }
    }
}