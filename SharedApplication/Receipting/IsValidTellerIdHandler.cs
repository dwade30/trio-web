﻿using System.Linq;
using SharedApplication.Authorization;
using SharedApplication.CashReceipts.Commands;
using SharedApplication.Messaging;
using SharedApplication.SystemSettings.Interfaces;

namespace SharedApplication.Receipting
{
    public class IsValidTellerIdHandler : CommandHandler<IsValidTellerId,bool>
    {
        private ISystemSettingsContext systemSettingsContext;
        private IUserPermissionSet currentUserPermissionSet;
        public IsValidTellerIdHandler(ISystemSettingsContext systemSettingsContext, IUserPermissionSet currentUserPermissionSet)
        {
            this.systemSettingsContext = systemSettingsContext;
            this.currentUserPermissionSet = currentUserPermissionSet;
        }
        protected override bool Handle(IsValidTellerId command)
        {
            if (command.TellerId == "617" && currentUserPermissionSet.UserId != -1)
            {
                return false;
            }
            var teller = systemSettingsContext.Operators.FirstOrDefault(o => o.Code == command.TellerId);
            if (teller == null)
            {
                return false;
            }

            return true;
        }
    }
}