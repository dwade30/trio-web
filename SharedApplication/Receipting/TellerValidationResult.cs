﻿namespace SharedApplication.Receipting
{
    public enum TellerValidationResult
    {
        Valid = 0,
        LoggedIn = 1,
        Invalid = 2
    }
}