﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SharedApplication.CashReceipts;
using SharedApplication.CashReceipts.Models;
using SharedApplication.CashReceipts.Receipting;
using SharedApplication.Clerk.Models;
using SharedApplication.Clerk.Receipting;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Receipting;
using SharedApplication.TaxCollections.Receipting;

namespace SharedApplication.Receipting
{
    public static class TransactionExtensions
    {

        public static ReceiptSummaryItem ToReceiptSummaryItem(this TransactionBase transaction)
        {
            switch (transaction)
            {
                case ClerkTransaction clerkTransaction:
                    return clerkTransaction.ToReceiptSummaryItem();
                    break;
                case ClerkVitalTransaction vitalTransaction:
                    return vitalTransaction.ToReceiptSummaryItem();
                    break;
                case MiscReceiptTransactionBase miscReceiptTransaction:
	                return miscReceiptTransaction.ToReceiptSummaryItem();
	                break;
                case AccountsReceivableTransactionBase accountsReceivableTransaction:
	                return accountsReceivableTransaction.ToReceiptSummaryItem();
	                break;
                case UtilityBillingTransactionBase utilityBillingTransaction:
	                return utilityBillingTransaction.ToReceiptSummaryItem();
	                break;
                case MotorVehicleTransactionBase motorVehicleTransaction:
	                return motorVehicleTransaction.ToReceiptSummaryItem();
	                break;
                case PropertyTaxTransaction propertyTaxTransaction:
                    return propertyTaxTransaction.ToReceiptSummaryItem();
                    break;
                case PropertyTaxBatchTransaction propertyTaxBatchTransaction:
                    return propertyTaxBatchTransaction.ToReceiptSummaryItem();
				default:
                    return null;
                    break;
            }
        }
        public static ReceiptSummaryItem ToReceiptSummaryItem(this ClerkTransaction transaction)
        {
            return new ReceiptSummaryItem()
            {
                Amount = transaction.Total,
                Account = 0,
                Info = transaction.Reference,               
                Split = 1,
                TransactionType = "800",
                TypeDescription = "Dog Transaction",
                TransactionId = transaction.Id,
                Name = transaction.PayerName
            };
        }
		
		public static ReceiptSummaryItem ToReceiptSummaryItem(this MiscReceiptTransactionBase transaction)
		{
			return new ReceiptSummaryItem()
			{
				Amount = transaction.TransactionAmounts.Sum(t => t.Total),
				Account = 0,
				Info = transaction.Reference,
				Split = transaction.Split,
				TransactionType = transaction.TransactionTypeCode,
				TypeDescription = transaction.TransactionTypeDescription,
				Name = transaction.PayerName,
				TransactionId = transaction.Id
			};
		}

		public static ReceiptSummaryItem ToReceiptSummaryItem(this MotorVehicleTransactionBase transaction)
		{
			return new ReceiptSummaryItem()
			{
				Amount = transaction.TransactionAmounts.Sum(t => t.Total),
				Account = 0,
				Info = transaction.Reference,
				Split = 1,
				TransactionType = transaction.TransactionTypeCode,
				TypeDescription = transaction.TransactionTypeDescription,
				Name = transaction.PayerName,
				TransactionId = transaction.Id
			};
		}

		public static ReceiptSummaryItem ToReceiptSummaryItem(this AccountsReceivableTransactionBase transaction)
		{
			return new ReceiptSummaryItem()
			{
				Amount = transaction.TransactionAmounts.Sum(t => t.Total),
				Account = transaction.AccountNumber,
				Info = transaction.Reference,
				Split = 1,
				TransactionType = transaction.TransactionTypeCode,
				TypeDescription = transaction.TransactionTypeDescription,
				Name = transaction.PayerName,
				TransactionId = transaction.Id
			};
		}

		public static ReceiptSummaryItem ToReceiptSummaryItem(this UtilityBillingTransactionBase transaction)
		{
			return new ReceiptSummaryItem()
			{
				Amount = transaction.TransactionAmounts.Sum(t => t.Total),
				Account = transaction.AccountNumber,
				Info = transaction.Reference,
				Split = 1,
				TransactionType = transaction.TransactionTypeCode,
				TypeDescription = transaction.TransactionTypeDescription,
				Name = transaction.PayerName,
				TransactionId = transaction.Id
			};
		}

        public static ReceiptSummaryItem ToReceiptSummaryItem(this PropertyTaxTransaction transaction)
        {
            var transactionTypeInfo = GetPropertyTaxTransactionTypeInfo(transaction.TransactionTypeCode);
            return new ReceiptSummaryItem()
            {
                Amount =  transaction.TransactionAmounts.Sum(t => t.Total),
                Account = transaction.Account,
                Info = transaction.Reference,
                Split = 1,
                TransactionType = transactionTypeInfo.Code,
                TypeDescription = transactionTypeInfo.Description,
                Name = transaction.PayerName,
                TransactionId = transaction.Id
            };
        }

        public static ReceiptSummaryItem ToReceiptSummaryItem(this PropertyTaxBatchTransaction transaction)
        {
            var transactionTypeInfo = GetPropertyTaxBatchTransactionTypeInfo(transaction.TransactionTypeCode);
            return new ReceiptSummaryItem()
            {
                Amount = transaction.TransactionAmounts.Sum(t => t.Total),
                Info = transaction.TaxYear.ToString(),
                Split = 1,
                TransactionType = transactionTypeInfo.Code,
                TypeDescription = transactionTypeInfo.Description,
                Name = transaction.PayerName,
                TransactionId =  transaction.Id
            };
        }
		public static ReceiptSummaryItem ToReceiptSummaryItem(this ClerkVitalTransaction transaction)
        {
            var transactionTypeInfo = GetClerkTransactionTypeInfo(transaction.TransactionTypeCode);
            return new ReceiptSummaryItem()
            {
                Amount =  transaction.Total,
                Account = 0,
                Info = transaction.Reference,
                Split = 1,
                TransactionType = transactionTypeInfo.Code,
                TypeDescription = transactionTypeInfo.Description,
                Name = transaction.PayerName,
                TransactionId = transaction.Id
            };
        }

		public static int CashReceiptsReceiptTypeCode(this TransactionBase transaction)
		{
			return transaction.ToReceiptSummaryItem().TransactionType.ToIntegerValue();
		}

		public static (string Code,string Description) GetClerkTransactionTypeInfo(string typeCode)
        {
            switch (typeCode.Trim().ToLower())
            {
                case "dog":
                    return ("800", "Dog Transaction");
                    break;
                case "bir":
                    return ("802", "Birth");
                    break;
                case "dea":
                    return ("801", "Death");
                    break;
                case "mar":
                    return ("803", "Marriage");
                case "bur":
                    return ("804", "");
                default:
                    return ("98", "Clerk Transaction");
                    break;
            }
        }

        private static (string Code, string Description) GetPropertyTaxTransactionTypeInfo(string typeCode)
        {
            switch (typeCode.Trim().ToLower())
            {
                case "taretaxlien":
                    return ("891", "Tax Lien");
                    break;
                case "retaxlien":
                    return ("91", "Tax Lien");
                    break;
                case "retax":
                    return ("90", "Real Estate");
                    break;
                case "taretax":
                    return ("890", "Real Estate");
                    break;
                case "pptax":
                    return ("92", "Personal Property");
                    break;
                default:
                    return ("90", "Real Estate");
                    break;
            }
        }

        private static (string Code, string Description) GetPropertyTaxBatchTransactionTypeInfo(string typeCode)
        {
            switch (typeCode.Trim().ToLower())
            {
                case "pp":
                    return ("92", "Personal Property Batch Update");
                    break;
                default:
                    return ("90", "Real Estate Batch Update");
                    break;
            }
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this TransactionBase transaction)
        {
            switch (transaction)
            {
                case ClerkTransaction clerkTransaction:
                    return clerkTransaction.ToReceiptDetailItem();
                    break;
                case ClerkVitalTransaction vitalTransaction:
                    return vitalTransaction.ToReceiptDetailItem();
                    break;
                case MiscReceiptTransactionBase miscReceiptTransaction:
                    return miscReceiptTransaction.ToReceiptDetailItem();
                    break;
                case AccountsReceivableTransactionBase accountsReceivableTransaction:
	                return accountsReceivableTransaction.ToReceiptDetailItem();
	                break;
                case UtilityBillingTransactionBase utilityBillingTransaction:
	                return utilityBillingTransaction.ToReceiptDetailItem();
	                break;
                case MotorVehicleTransactionBase motorVehicleTransaction:
	                return motorVehicleTransaction.ToReceiptDetailItem();
	                break;
                case PropertyTaxTransaction propertyTaxTransaction:
                    return propertyTaxTransaction.ToReceiptDetailItem();
                    break;
				default:
                    return null;
                    break;
            }
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this ClerkTransaction transaction)
        {
            var transactionTypeInfo = GetClerkTransactionTypeInfo(transaction.TransactionTypeCode);
            var detailItem = new ReceiptDetailItem()
            {
             // Name   =  transaction.PayerName,
              Controls = transaction.Controls,
              TransactionAmounts = transaction.TransactionAmounts,
              Reference =  transaction.Reference,
              TypeCode = transactionTypeInfo.Code,
              TypeDescription = transactionTypeInfo.Description,
              TransactionSpecificDetails = new List<(string Title, string Value)>()
              {
                  (Title: "Name",Value: transaction.PayerName),
                  (Title: "Date", Value: transaction.EffectiveDateTime.FormatAndPadShortDate())
              }
            };
            return detailItem;
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this ClerkVitalTransaction transaction)
        {
            var transactionTypeInfo = GetClerkTransactionTypeInfo(transaction.TransactionTypeCode);
            var detailItem = new ReceiptDetailItem()
            {
                //Name = transaction.PayerName,
                Controls = transaction.Controls,
                TransactionAmounts = transaction.TransactionAmounts,
                Reference = transaction.Reference,
                TypeCode = transactionTypeInfo.Code,
                TypeDescription = transactionTypeInfo.Description,
                TransactionSpecificDetails = new List<(string Title, string Value)>()
                {
	                (Title: "Name",Value: transaction.PayerName),
                    (Title: "Date", Value: transaction.EffectiveDateTime.FormatAndPadShortDate())
                }
            };
            return detailItem;
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this MotorVehicleTransactionBase transaction)
        {
	        var detailItem = new ReceiptDetailItem()
	        {
		        Controls = transaction.Controls,
		        TransactionAmounts = transaction.TransactionAmounts,
		        Reference = transaction.Reference,
		        TypeCode = transaction.TransactionTypeCode,
				TypeDescription = transaction.TransactionTypeDescription,
				TransactionSpecificDetails = new List<(string Title, string Value)>()
		        {
			        (Title: "Name",Value: transaction.PayerName),
		        }
	        };
	        return detailItem;
        }

		public static ReceiptDetailItem ToReceiptDetailItem(this MiscReceiptTransactionBase transaction)
        {
            
            var detailItem = new ReceiptDetailItem()
            {
                Controls = transaction.Controls,
                TransactionAmounts = transaction.TransactionAmounts,
                Reference =  transaction.Reference,
                TypeCode =  transaction.TransactionTypeCode.ToString(),
				TypeDescription = transaction.TransactionTypeDescription,
                TransactionSpecificDetails = new List<(string Title, string Value)>()
                {
                    (Title: "Quantity", Value: transaction.TransactionDetails.Quantity.ToString()),
                    (Title:"Comment", Value: transaction.TransactionDetails.Comment),
                    (Title:"Affect Cash Drawer", Value: transaction.TransactionDetails.AffectCashDrawer.ToString())
                }
            };
            
            return detailItem;
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this AccountsReceivableTransactionBase transaction)
        {

	        var detailItem = new ReceiptDetailItem()
	        {
		        Controls = transaction.Controls,
		        TransactionAmounts = transaction.TransactionAmounts,
		        Reference = transaction.Reference,
		        TypeCode = transaction.TransactionTypeCode.ToString(),
		        TransactionSpecificDetails = new List<(string Title, string Value)>()
		        {
					(Title: "Name",Value: transaction.PayerName),
					(Title: "Date", Value: transaction.EffectiveDateTime.FormatAndPadShortDate()),
					(Title: "Alternative Cash Account", Value: transaction.BudgetaryAccountNumber),
					(Title: "Default Cash Account",Value: transaction.DefaultCashAccount),
					(Title: "Collection Code",Value: transaction.Code),
					(Title: "Comment", Value: transaction.Comment),
					(Title: "Affect Cash",Value: transaction.AffectCash.ToString()),
					(Title: "Affect Cash Drawer", Value: transaction.AffectCashDrawer.ToString())
				}
	        };

	        return detailItem;
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this UtilityBillingTransactionBase transaction)
        {

	        var detailItem = new ReceiptDetailItem()
	        {
		        Controls = transaction.Controls,
		        TransactionAmounts = transaction.TransactionAmounts,
		        Reference = transaction.Reference,
		        TypeCode = transaction.TransactionTypeCode.ToString(),
				TypeDescription = transaction.TransactionTypeDescription,
		        TransactionSpecificDetails = new List<(string Title, string Value)>()
		        {
			        (Title: "Name",Value: transaction.PayerName),
			        (Title: "Date", Value: transaction.EffectiveDateTime.FormatAndPadShortDate()),
			        (Title: "Alternative Cash Account", Value: transaction.BudgetaryAccountNumber),
			        (Title: "Default Cash Account",Value: transaction.DefaultCashAccount),
			        (Title: "Collection Code",Value: transaction.Code),
			        (Title: "Comment", Value: transaction.Comment),
			        (Title: "Affect Cash",Value: transaction.AffectCash.ToString()),
			        (Title: "Affect Cash Drawer", Value: transaction.AffectCashDrawer.ToString())
		        }
	        };

	        return detailItem;
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this PropertyTaxTransaction transaction)
        {
            var transactionTypeInfo = GetPropertyTaxTransactionTypeInfo(transaction.TransactionTypeCode);
            var detailItem = new ReceiptDetailItem()
            {
                Controls = transaction.Controls,
                TransactionAmounts = transaction.TransactionAmounts,
                Reference = transaction.Reference,
                TypeCode = transactionTypeInfo.Code,
                TypeDescription = transactionTypeInfo.Description,
                TransactionSpecificDetails = new List<(string Title, string Value)>()
                {
                    (Title: "Name",Value: transaction.PayerName),
                    (Title: "Date", Value: transaction.EffectiveDateTime.FormatAndPadShortDate())
                }
            };
            return detailItem;
        }

        public static ReceiptDetailItem ToReceiptDetailItem(this PropertyTaxBatchTransaction transaction)
        {
            var transactionTypeInfo = GetPropertyTaxBatchTransactionTypeInfo(transaction.TransactionTypeCode);
            var detailItem = new ReceiptDetailItem()
            {
                Controls = transaction.Controls,
                TransactionAmounts = transaction.TransactionAmounts,
                Reference = transaction.Reference,
                TypeCode = transactionTypeInfo.Code,
                TypeDescription = transactionTypeInfo.Description,
                TransactionSpecificDetails = new List<(string Title, string Value)>()
                {
                    (Title: "Name",Value: transaction.PayerName),
                    (Title: "Date", Value: transaction.EffectiveDateTime.FormatAndPadShortDate())
                }
            };
            return detailItem;
        }
        public static Archive ToArchive(this TransactionBase transaction, IReceiptTypesService receiptTypesService, string tellerId, decimal cardPaidAmount, decimal cashPaidAmount, decimal checkPaidAmount, string paidBy)
        {
	        var result = new Archive();

	        if (transaction.PayerName.IsNullOrWhiteSpace())
	        {
		        transaction.PayerName = paidBy;
	        }

			switch (transaction)
            {
				case ClerkTransaction clerkTransaction:
                    result = clerkTransaction.ToArchive(receiptTypesService);
                    break;
                case ClerkVitalTransaction vitalTransaction:
	                result = vitalTransaction.ToArchive(receiptTypesService);
                    break;
                case MiscReceiptTransactionBase miscReceiptTransaction:
	                result = miscReceiptTransaction.ToArchive(receiptTypesService);
                    break;
                case AccountsReceivableTransactionBase accountsReceivableTransaction:
	                result = accountsReceivableTransaction.ToArchive(receiptTypesService);
                    break;
                case UtilityBillingTransactionBase utilityBillingTransaction:
	                result = utilityBillingTransaction.ToArchive(receiptTypesService);
	                break;
				case MotorVehicleTransactionBase motorVehicleTransaction:
					result = motorVehicleTransaction.ToArchive(receiptTypesService);
					break;
                case PropertyTaxTransaction propertyTaxTransaction:
                    result = propertyTaxTransaction.ToArchive(receiptTypesService);
                    break;
                case PropertyTaxBatchTransaction propertyTaxBatchTransaction:
                    result = propertyTaxBatchTransaction.ToArchive(receiptTypesService);
                    break;
				default:
	                result = null;
                    break;
            }

			result.TellerID = tellerId;
			result.CardPaidAmount = cardPaidAmount;
			result.CashPaidAmount = cashPaidAmount;
			result.CheckPaidAmount = checkPaidAmount;
            return result;
        }

        public static Archive ToArchive(this ClerkTransaction transaction, IReceiptTypesService receiptTypesService)
        {            
            double amount1 = 0;
            double amount2 = 0;
            double amount3 = 0;
            double amount4 = 0;
            double amount5 = 0;
            double amount6 = 0;
            var amountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Town Fee");
            if (amountItem != null)
            {
                amount2 = amountItem.Total.ToDouble();
            }

            amountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "State Fee");
            if (amountItem != null)
            {
                amount3 = amountItem.Total.ToDouble();
            }

            amountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Clerk Fee");
            if (amountItem != null)
            {
                amount4 = amountItem.Total.ToDouble();
            }

            amountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Late Fee");
            if (amountItem != null)
            {
                amount5 = amountItem.Total.ToDouble();
            }

            amountItem  = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Misc Fee");
            if (amountItem != null)
            {
                amount6 = amountItem.Total.ToDouble();
            }

            string control1 = "";
            string control2 = "";
            var accounts = receiptTypesService.GetAccountsForReceiptType(800).ToList();


            if (transaction.Controls.FirstOrDefault() != null)
            {
                control1 = transaction.Controls.FirstOrDefault().Value;
            }
            return new Archive()
           {
               ReceiptType = 800,
               AccountNumber = 0,
               ARBillType = 0,
               ActualSystemDate = DateTime.Now,
               Account1 = "",
               Account2 = accounts.FirstOrDefault(a => a.Title == "Town Fee").Account,
               Account3 = accounts.FirstOrDefault(a => a.Title == "State Fee").Account,
               Account4 = accounts.FirstOrDefault(a => a.Title == "Clerk Fee").Account,
               Account5 = accounts.FirstOrDefault(a => a.Title == "Late Fee").Account,
               Account6 = accounts.FirstOrDefault(a => a.Title == "Misc Fee").Account,
               Amount1 = 0,
               Amount2 = amount2,
               Amount3 = amount3,
               Amount4 = amount4,
               Amount5 = amount5,
               Amount6 = amount6,
               AffectCashDrawer = true,
               AffectCash = true,
               CollectionCode = "DOG",
               ArchiveCode =  0,
               ArchiveDate = transaction.EffectiveDateTime.Date,
               Split =  1,
               Name = transaction.PayerName,
               TellerCloseOut =  0,
               DailyCloseOut = 0,
               NamePartyID = transaction.PayerPartyId,
               Quantity = 1,
               Project1 = "",
               Project2 = "",
               Project3 = "",
               Project4 = "",
               Project5 = "",
               Project6 = "",
               Ref = transaction.Reference,
               Comment = "DOG",
               ReceiptTitle = "",
               ReceiptIdentifier = transaction.CorrelationIdentifier,
               TransactionIdentifier = transaction.Id,
               Control1 = control1,
               Control2 =  control2,
               Control3 = "",
               EFT = false,
               DefaultAccount = "",
               DefaultCashAccount = "",
               DefaultMIAccount = ""
           };
        }

        public static Archive ToArchive(this ClerkVitalTransaction transaction, IReceiptTypesService receiptTypesService)
        {
            var transactionTypeInfo = GetClerkTransactionTypeInfo(transaction.TransactionTypeCode);
            decimal amount1 = 0;
            decimal amount2 = 0;
            var amountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Vital Records");
            if (amountItem != null)
            {
                amount1 = amountItem.Total;
            }

            amountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "State Fee");
            if (amountItem != null)
            {
                amount2 = amountItem.Total;
            }

            string control1 = "";
            string control2 = "";
            


            if (transaction.Controls.FirstOrDefault() != null)
            {
                control1 = transaction.Controls.FirstOrDefault().Value;
            }

            if (transaction.Controls.Count > 1)
            {
                control2 = transaction.Controls[1].Value;
            }
            var accounts = receiptTypesService.GetAccountsForReceiptType(transactionTypeInfo.Code.ToIntegerValue()).ToList();
            return new Archive()
            {
                ReceiptType = transactionTypeInfo.Code.ToIntegerValue(),
                AccountNumber = 0,
                ARBillType = 0,
                ActualSystemDate = DateTime.Now,
                Account1 = accounts.FirstOrDefault(a => a.Title == "Vital Records").Account,
                Account2 = accounts.FirstOrDefault(a => a.Title == "State Fee").Account,
                Account3 = "",
                Account4 = "",
                Account5 = "",
                Account6 = "",
                Amount1 = amount1.ToDouble(),
                Amount2 = amount2.ToDouble(),
                Amount3 = 0,
                Amount4 = 0,
                Amount5 = 0,
                Amount6 = 0,
                AffectCashDrawer = true,
                AffectCash = true,
                CollectionCode = transaction.TransactionTypeCode,
                ArchiveCode = 0,
                ArchiveDate = transaction.EffectiveDateTime.Date,
                Split = 1,
                Name = transaction.PayerName,
                TellerCloseOut = 0,
                DailyCloseOut = 0,
                NamePartyID = transaction.PayerPartyId,
                Quantity = 1,
                Project1 = "",
                Project2 = "",
                Project3 = "",
                Project4 = "",
                Project5 = "",
                Project6 = "",
                Ref = transaction.Reference,
                Comment = transaction.TransactionTypeCode,
                ReceiptTitle = "",
                ReceiptIdentifier = transaction.CorrelationIdentifier,
                TransactionIdentifier = transaction.Id,
                Control1 = control1,
                Control2 = control2,
                Control3 = "",
                EFT = false,
                DefaultAccount = "",
                DefaultCashAccount = "",
                DefaultMIAccount = ""
            };
        }

		public static Archive ToArchive(this MotorVehicleTransactionBase transaction, IReceiptTypesService receiptTypesService)
		{
			decimal[] amounts = { 0, 0, 0, 0, 0, 0 };
			string[] controls = { "", "", "" };
			string[] accounts = { "", "", "", "", "", "" };
			var accountTuples = receiptTypesService.GetAccountsForTransaction(transaction).ToList();
			var accountCount = 0;
			foreach (var accountTuple in accountTuples)
			{
				accounts[accountCount] = accountTuple.Account;
				accountCount += 1;
			}
			var controlCount = 0;
			foreach (var control in transaction.Controls)
			{
				controls[controlCount] = control.Value;
				controlCount += 1;
			}

			var amountCount = 0;
			foreach (var amount in transaction.TransactionAmounts)
			{
				amounts[amountCount] = amount.Total;
				amountCount += 1;
			}

			return new Archive()
			{
				ReceiptType = transaction.TransactionTypeCode.ToIntegerValue(),
				AccountNumber = 0,
				ARBillType = 0,
				ActualSystemDate = DateTime.Now,
				Account1 = amounts[0] != 0 ? accounts[0] : "",
				Account2 = amounts[1] != 0 ? accounts[1] : "",
				Account3 = amounts[2] != 0 ? accounts[2] : "",
				Account4 = amounts[3] != 0 ? accounts[3] : "",
				Account5 = amounts[4] != 0 ? accounts[4] : "",
				Account6 = amounts[5] != 0 ? accounts[5] : "",
				Amount1 = amounts[0].ToDouble(),
				Amount2 = amounts[1].ToDouble(),
				Amount3 = amounts[2].ToDouble(),
				Amount4 = amounts[3].ToDouble(),
				Amount5 = amounts[4].ToDouble(),
				Amount6 = amounts[5].ToDouble(),
				AffectCashDrawer = true,
				AffectCash = true,
				CollectionCode = "",
				ArchiveCode = 0,
				ArchiveDate = transaction.EffectiveDateTime.Date,
				Split = 1,
				Name = transaction.PayerName,
				TellerCloseOut = 0,
				DailyCloseOut = 0,
				NamePartyID = transaction.PayerPartyId,
				Quantity = 1,
				Project1 = "",
				Project2 = "",
				Project3 = "",
				Project4 = "",
				Project5 = "",
				Project6 = "",
				Ref = transaction.Reference,
				Comment = "",
				ReceiptTitle = "",
				ReceiptIdentifier = transaction.CorrelationIdentifier,
				TransactionIdentifier = transaction.Id,
				Control1 = controls[0],
				Control2 = controls[1],
				Control3 = controls[2],
				EFT = false,
				DefaultAccount = "",
				DefaultCashAccount = "",
				DefaultMIAccount = ""
			};
		}


		public static Archive ToArchive(this MiscReceiptTransactionBase transaction, IReceiptTypesService receiptTypesService)
        {
            decimal[] amounts = {0,0,0,0,0,0};
            string[] controls = {"", "", ""};
            string[] accounts = {"", "", "", "", "", ""};
            var accountTuples = receiptTypesService.GetAccountsForTransaction(transaction).ToList();
            var accountCount = 0;
            foreach (var accountTuple in accountTuples)
            {
                if (accountTuple.Account != "M I")
                {
                    accounts[accountCount] = accountTuple.Account;
                }
                else
                {
                    accounts[accountCount] = transaction.TransactionDetails.GlAccount;
                }
                accountCount += 1;
            }
            var controlCount = 0;
            foreach (var control in transaction.TransactionDetails.Controls)
            {
                controls[controlCount] = control.Value;
                controlCount += 1;
            }

            var amountCount = 0;
            foreach (var amount in transaction.TransactionAmounts)
            {
                amounts[amountCount] = amount.Total;
                amountCount += 1;
            }
            
            return new Archive()
            {
                ReceiptType = transaction.TransactionTypeCode.ToIntegerValue(),
                AccountNumber = 0,
                ARBillType = 0,
                ActualSystemDate = DateTime.Now,
                Account1 = amounts[0] != 0  ? accounts[0] : "",
                Account2 = amounts[1] != 0 ? accounts[1] : "",
                Account3 = amounts[2] != 0 ? accounts[2] : "",
                Account4 = amounts[3] != 0 ? accounts[3] : "",
                Account5 = amounts[4] != 0 ? accounts[4] : "",
                Account6 = amounts[5] != 0 ? accounts[5] : "",
                Amount1 = amounts[0].ToDouble(),
                Amount2 = amounts[1].ToDouble(),
                Amount3 = amounts[2].ToDouble(),
                Amount4 = amounts[3].ToDouble(),
                Amount5 = amounts[4].ToDouble(),
                Amount6 = amounts[5].ToDouble(),
                AffectCashDrawer = transaction.TransactionDetails.AffectCashDrawer,
                AffectCash = transaction.TransactionDetails.AffectCash,
                CollectionCode = transaction.TransactionDetails.CollectionCode,
				RecordKey = transaction.TransactionDetails.RecordKey,
                ArchiveCode = 0,
                ArchiveDate = transaction.EffectiveDateTime.Date,
                Split = 1,
                Name = transaction.PayerName,
                TellerCloseOut = 0,
                DailyCloseOut = 0,
                NamePartyID = transaction.PayerPartyId,
                Quantity = transaction.TransactionDetails.Quantity,
                Project1 = "",
                Project2 = "",
                Project3 = "",
                Project4 = "",
                Project5 = "",
                Project6 = "",
                Ref = transaction.Reference,
                Comment = transaction.TransactionDetails.Comment,
                ReceiptTitle = "",
                ReceiptIdentifier = transaction.CorrelationIdentifier,
                TransactionIdentifier = transaction.Id,
                Control1 = controls[0],
                Control2 = controls[1],
                Control3 = controls[2],
                EFT = false,
                DefaultAccount = "",
                DefaultCashAccount = transaction.TransactionDetails.DefaultCashAccount,
                DefaultMIAccount = ""
            };
        }

        public static Archive ToArchive(this AccountsReceivableTransactionBase transaction, IReceiptTypesService receiptTypesService)
        {
            decimal[] amounts = { 0, 0, 0, 0, 0, 0 };
            string[] controls = { "", "", "" };
            string[] accounts = { "", "", "", "", "", "" };
            var accountTuples = receiptTypesService.GetAccountsForTransaction(transaction).ToList();
            var accountCount = 0;
            foreach (var accountTuple in accountTuples)
            {
                accounts[accountCount] = accountTuple.Account;
                accountCount += 1;
            }
            var controlCount = 0;
            foreach (var control in transaction.Controls)
            {
                controls[controlCount] = control.Value;
                controlCount += 1;
            }
            var amountCount = 0;
            foreach (var amount in transaction.TransactionAmounts)
            {
                amounts[amountCount] = amount.Total;
                amountCount += 1;
            }
            return new Archive()
            {
                ReceiptType = 97,
                AccountNumber = transaction.AccountNumber,
                ARBillType = transaction.TransactionDetails.Bills.FirstOrDefault()?.BillType,
                ActualSystemDate = DateTime.Now,
				Account1 = amounts[0] != 0 ? accounts[0] : "",
				Account2 = amounts[1] != 0 ? accounts[1] : "",
				Account3 = amounts[2] != 0 ? accounts[2] : "",
				Account4 = amounts[3] != 0 ? accounts[3] : "",
				Account5 = amounts[4] != 0 ? accounts[4] : "",
				Account6 = amounts[5] != 0 ? accounts[5] : "",
				Amount1 = amounts[0].ToDouble(),
                Amount2 = amounts[1].ToDouble(),
                Amount3 = amounts[2].ToDouble(),
                Amount4 = amounts[3].ToDouble(),
                Amount5 = amounts[4].ToDouble(),
                Amount6 = amounts[5].ToDouble(),
                AffectCashDrawer = transaction.AffectCashDrawer,
                AffectCash = transaction.AffectCash,
                CollectionCode = "",
                ArchiveCode = 0,
                ArchiveDate = transaction.EffectiveDateTime.Date,
                Split = 1,
                Name = transaction.PayerName,
                TellerCloseOut = 0,
                DailyCloseOut = 0,
                NamePartyID = transaction.PayerPartyId,
                Quantity = 1,
                Project1 = "",
                Project2 = "",
                Project3 = "",
                Project4 = "",
                Project5 = "",
                Project6 = "",
                Ref = transaction.Reference,
                Comment = transaction.Comment,
                ReceiptTitle = "",
                ReceiptIdentifier = transaction.CorrelationIdentifier,
                TransactionIdentifier = transaction.Id,
                Control1 = controls[0],
                Control2 = controls[1],
                Control3 = controls[2],
                EFT = false,
                DefaultAccount = transaction.BudgetaryAccountNumber,
                DefaultCashAccount = "",
                DefaultMIAccount = ""
            };
        }

		public static Archive ToArchive(this UtilityBillingTransactionBase transaction, IReceiptTypesService receiptTypesService)
		{
			decimal[] amounts = { 0, 0, 0, 0, 0, 0 };
			string[] controls = { "", "", "" };
			string[] accounts = { "", "", "", "", "", "" };
			var accountTuples = receiptTypesService.GetAccountsForTransaction(transaction).ToList();
			var accountCount = 0;
			foreach (var accountTuple in accountTuples)
			{
				accounts[accountCount] = accountTuple.Account;
				accountCount += 1;
			}
			var controlCount = 0;
			foreach (var control in transaction.Controls)
			{
				controls[controlCount] = control.Value;
				controlCount += 1;
			}
			var amountCount = 0;
			foreach (var amount in transaction.TransactionAmounts)
			{
				if (amount.Total != 0)
				{
					for (int counter = 0; counter < accountCount; counter++)
					{
						if (amount.Description == accountTuples[counter].Title || 
						    (accountTuples[counter].Title == "Lien Interest" && amount.Description == "Pre Lien Interest") ||
						    (accountTuples[counter].Title == "Costs" && amount.Description == "Lien Costs"))
						{
							amounts[counter] = amount.Total;
							amountCount += 1;
							break;
						}
                    }
                }
			}
			return new Archive()
			{
				ReceiptType = transaction.TransactionTypeCode.ToIntegerValue(),
				AccountNumber = transaction.AccountNumber,
				ARBillType = 0,
				ActualSystemDate = DateTime.Now,
				Account1 = amounts[0] != 0 ? accounts[0] : "",
				Account2 = amounts[1] != 0 ? accounts[1] : "",
				Account3 = amounts[2] != 0 ? accounts[2] : "",
				Account4 = amounts[3] != 0 ? accounts[3] : "",
				Account5 = amounts[4] != 0 ? accounts[4] : "",
				Account6 = amounts[5] != 0 ? accounts[5] : "",
				Amount1 = amounts[0].ToDouble(),
				Amount2 = amounts[1].ToDouble(),
				Amount3 = amounts[2].ToDouble(),
				Amount4 = amounts[3].ToDouble(),
				Amount5 = amounts[4].ToDouble(),
				Amount6 = amounts[5].ToDouble(),
				AffectCashDrawer = transaction.AffectCashDrawer,
				AffectCash = transaction.AffectCash,
				CollectionCode = "",
				ArchiveCode = 0,
				ArchiveDate = transaction.EffectiveDateTime.Date,
				Split = 1,
				Name = transaction.PayerName,
				TellerCloseOut = 0,
				DailyCloseOut = 0,
				NamePartyID = transaction.PayerPartyId,
				Quantity = 1,
				Project1 = "",
				Project2 = "",
				Project3 = "",
				Project4 = "",
				Project5 = "",
				Project6 = "",
				Ref = transaction.Reference,
				Comment = transaction.Comment,
				ReceiptTitle = "",
				ReceiptIdentifier = transaction.CorrelationIdentifier,
				TransactionIdentifier = transaction.Id,
				Control1 = controls[0],
				Control2 = controls[1],
				Control3 = controls[2],
				EFT = false,
                DefaultAccount = transaction.BudgetaryAccountNumber,
                DefaultCashAccount = "",
				DefaultMIAccount = ""
			};
		}

        public static Archive ToArchive(this PropertyTaxTransaction transaction,
            IReceiptTypesService receiptTypesService)
        {
            var transactionTypeInfo = GetPropertyTaxTransactionTypeInfo(transaction.TransactionTypeCode);
            //var accounts = receiptTypesService.GetAccountsForReceiptType(transactionTypeInfo.Code.ToIntegerValue()).ToList();
            var accounts = receiptTypesService.GetAccountsForTransaction(transaction);
            var principalItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Principal");
            var abatementItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Abatement");
            var interestItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Interest");            
            var abatement = abatementItem?.Total ?? 0;
            var principal = principalItem?.Total ?? 0;
            var interest = interestItem?.Total ?? 0;
            decimal costs = 0;
            decimal discount = 0;
            decimal preLienInterest = 0;
            var interestAccount = accounts.FirstOrDefault(a => a.Title == "Interest").Account ?? "";
            string preLienOrDiscountAccount = "";
            var costsAccount = accounts.FirstOrDefault(a => a.Title == "Costs").Account ?? "";
            var principalAccount = accounts.FirstOrDefault(a => a.Title == "Principal").Account;
            var abatementAccount = accounts.FirstOrDefault(a => a.Title == "Abatement").Account;
            if (transaction.IsLien)
            {
                var costItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Lien Costs");
                var preLienInterestItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Pre Lien Interest");
                costs = costItem?.Total ?? 0;
                preLienInterest = preLienInterestItem?.Total ?? 0;               
                preLienOrDiscountAccount = accounts.FirstOrDefault(a => a.Title == "Lien Interest").Account ?? "";
            }
            else
            {
                var costItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Costs");
                var discountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Discount");
                costs = costItem?.Total ?? 0;
                discount = discountItem?.Total ?? 0;
                preLienOrDiscountAccount = accounts.FirstOrDefault(a => a.Title == "Discount").Account ?? "";
            }

            string control1 = "";
            string control2 = "";
            string control3 = "";
            if (transaction.Controls.FirstOrDefault() != null)
            {
                control1 = transaction.Controls.FirstOrDefault().Value;
            }

            if (transaction.Controls.Count > 1)
            {
                control2 = transaction.Controls[1].Value;
            }

            if (transaction.Controls.Count > 2)
            {
                control3 = transaction.Controls[2].Value;
            }

            var preLienOrDiscount = preLienInterest + discount;
            return new Archive()
            {
                ReceiptType = transactionTypeInfo.Code.ToIntegerValue(),
                AccountNumber = transaction.Account,
                ARBillType = 0,
                ActualSystemDate = DateTime.Now,
                Account1 = principalAccount,
                Account2 = interestAccount,
                Account3 = preLienOrDiscountAccount,
                Account4 = costsAccount,
                Account5 = "",
                Account6 = abatementAccount,
                Amount1 = principal.ToDouble() + abatement.ToDouble(),
                Amount2 = interest.ToDouble(),
                Amount3 = preLienOrDiscount.ToDouble(),
                Amount4 = costs.ToDouble(),
                Amount5 = 0,
                Amount6 = 0,
                AffectCashDrawer = transaction.IncludeInCashDrawerTotal,
                AffectCash = transaction.AffectCash,
                CollectionCode = control2,
                ArchiveCode = 0,
                ArchiveDate = transaction.EffectiveDateTime.Date,
                Split = 1,
                Name = transaction.PayerName,
                TellerCloseOut = 0,
                DailyCloseOut = 0,
                NamePartyID = transaction.PayerPartyId,
                Quantity = 1,
                Project1 = "",
                Project2 = "",
                Project3 = "",
                Project4 = "",
                Project5 = "",
                Project6 = "",
                Ref =  transaction.Reference,
                Comment = "",
                ReceiptTitle = "",
                ReceiptIdentifier = transaction.CorrelationIdentifier,
                TransactionIdentifier = transaction.Id,
                Control1 = control1,
                Control2 = control2,
                Control3 = control3,
                EFT = false,
                DefaultAccount = transaction.OverrideAccount,
                DefaultCashAccount = "",
                DefaultMIAccount = ""
            };
        }

        public static Archive ToArchive(this PropertyTaxBatchTransaction transaction,
            IReceiptTypesService receiptTypesService)
        {
            var transactionTypeInfo = GetPropertyTaxBatchTransactionTypeInfo(transaction.TransactionTypeCode);
            var accounts = receiptTypesService.GetAccountsForReceiptType(transactionTypeInfo.Code.ToIntegerValue()).ToList();
            var principalItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Principal");
            var abatementItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Abatement");
            var interestItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Interest");
            var abatement = abatementItem?.Total ?? 0;
            var principal = principalItem?.Total ?? 0;
            var interest = interestItem?.Total ?? 0;
            decimal costs = 0;
            decimal discount = 0;
            decimal preLienInterest = 0;
            var interestAccount = accounts.FirstOrDefault(a => a.Title == "Interest").Account ?? "";
            string preLienOrDiscountAccount = "";
            var costsAccount = accounts.FirstOrDefault(a => a.Title == "Costs").Account ?? "";
            var principalAccount = accounts.FirstOrDefault(a => a.Title == "Principal").Account;
            var abatementAccount = accounts.FirstOrDefault(a => a.Title == "Abatement").Account;
            var costItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Costs");
            var discountItem = transaction.TransactionAmounts.FirstOrDefault(a => a.Name == "Discount");
            costs = costItem?.Total ?? 0;
            discount = discountItem?.Total ?? 0;
            preLienOrDiscountAccount = accounts.FirstOrDefault(a => a.Title == "Discount").Account ?? "";
            var preLienOrDiscount = preLienInterest + discount;
            return new Archive()
            {
                ReceiptType = transactionTypeInfo.Code.ToIntegerValue(),
                AccountNumber = 0,
                ARBillType = 0,
                ActualSystemDate = DateTime.Now,
                Account1 = principalAccount,
                Account2 = interestAccount,
                Account3 = preLienOrDiscountAccount,
                Account4 = costsAccount,
                Account5 = "",
                Account6 = abatementAccount,
                Amount1 = principal.ToDouble() + abatement.ToDouble(),
                Amount2 = interest.ToDouble(),
                Amount3 = preLienOrDiscount.ToDouble(),
                Amount4 = costs.ToDouble(),
                Amount5 = 0,
                Amount6 = 0,
                AffectCashDrawer = true,
                AffectCash = true,
                CollectionCode = transaction.TransactionTypeCode,
                ArchiveCode = 0,
                ArchiveDate = transaction.EffectiveDateTime.Date,
                Split = 1,
                Name = transaction.PayerName,
                TellerCloseOut = 0,
                DailyCloseOut = 0,
                NamePartyID = transaction.PayerPartyId,
                Quantity = 1,
                Project1 = "",
                Project2 = "",
                Project3 = "",
                Project4 = "",
                Project5 = "",
                Project6 = "",
                Ref = transaction.Reference,
                Comment = "",
                ReceiptTitle = "",
                ReceiptIdentifier = transaction.CorrelationIdentifier,
                TransactionIdentifier = transaction.Id,
                Control1 = "",
                Control2 = "",
                Control3 = "",
                EFT = false,
                DefaultAccount = "",
                DefaultCashAccount = "",
                DefaultMIAccount = ""
            };
        }
	}
}