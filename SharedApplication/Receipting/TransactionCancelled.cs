﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SharedApplication.Messaging;

namespace SharedApplication.Receipting
{
    public class TransactionCancelled : ReceiptingEventBase
    {
        public IEnumerable<string> Errors { get; }

        public TransactionCancelled(Guid correlationId ,IEnumerable<string> errors)
        {
            this.CorrelationId = correlationId;
            if (errors != null && errors.Any())
            {
                Errors = errors.ToList();
            }
            else
            {
                Errors = new List<string>();
            }
        }
    }
}