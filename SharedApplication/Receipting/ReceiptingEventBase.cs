﻿using System;
using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Receipting
{
    public abstract class ReceiptingEventBase : ReceiptingEvent
    {
        public Guid Id { get; }
        public Guid CorrelationId { get; set; }
    }
}