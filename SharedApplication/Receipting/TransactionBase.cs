﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.Receipting
{

    public abstract class TransactionBase : IIdentifiableEntity<Guid>
    {
        public Guid CorrelationIdentifier { get; set; }
        protected List<TransactionAmountItem> Amounts  = new List<TransactionAmountItem>();
        public IEnumerable<TransactionAmountItem> TransactionAmounts
        {
            get { return Amounts; }
        }

        public void AddTransactionAmount(TransactionAmountItem amountItem)
        {
            if (amountItem != null)
            {
                Amounts.Add(amountItem);
            }
        }

        public void AddTransactionAmounts(IEnumerable<TransactionAmountItem> amountItems)
        {
            if (amountItems != null && amountItems.Any())
            {
                Amounts.AddRange(amountItems);
            }
        }

        public Guid Id { get; set; } = Guid.NewGuid();
		public DateTime ActualDateTime { get; set; }
        public DateTime EffectiveDateTime { get; set; }
		public string Reference { get; set; } = "";
        public string PayerName { get; set; } = "";
        public int PayerPartyId { get; set; } = 0;
        public int TownId { get; set; } = 0;
        public int Split { get; set; } = 1;
		public bool IncludeInCashDrawerTotal { get; set; } = true;

		public List<ControlItem> Controls = new List<ControlItem>();
        public string TransactionTypeCode { get; set; }
		public decimal Total {
            get
            {
                if (Amounts.Any())
                {
                    return Amounts.Sum(a => a.Total);
                }
                else
                {
                    return 0;
                }
            }
        }
		public decimal CashDrawerTotal
        {
	        get
	        {
		        if (Amounts.Any() && IncludeInCashDrawerTotal)
		        {
			        return Amounts.Sum(a => a.Total);
		        }
		        else
		        {
			        return 0;
		        }
	        }
        }

	}

}