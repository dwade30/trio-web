﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.Receipting
{
    public class FinalizeTransaction<TTransactionType> : Command where TTransactionType : TransactionBase
    {
        public Guid CorrelationIdentifier { get; set; }
        public TTransactionType Transaction { get; set; }
        public int ReceiptId { get; set; }
    }
}