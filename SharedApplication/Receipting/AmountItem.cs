﻿namespace SharedApplication.Receipting
{
    public class TransactionAmountItem
    {
        public string Description { get; set; } = "";
        public string Name { get; set; } = "";
        public decimal Total { get; set; } = 0;
    }
}