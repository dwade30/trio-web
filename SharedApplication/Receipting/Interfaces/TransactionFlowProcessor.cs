﻿using System;

namespace SharedApplication.Receipting.Interfaces
{
    public interface TransactionFlowProcessor
    {
        Guid CorrelationId { get; set; }
        Guid TransactionId { get; set; }
        void Search();
    }
}