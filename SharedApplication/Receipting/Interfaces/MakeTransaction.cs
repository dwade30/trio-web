﻿using System;
using System.Windows.Input;
using SharedApplication.Messaging;

namespace SharedApplication.Receipting.Interfaces
{
    public interface MakeTransaction : SharedApplication.Messaging.ICommand 
    {
        Guid CorrelationId { get; set; }
    }
}