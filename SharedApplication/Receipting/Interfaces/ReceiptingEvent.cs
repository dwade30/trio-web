﻿using System;
using SharedApplication.Messaging;

namespace SharedApplication.Receipting.Interfaces
{
    public interface ReceiptingEvent : Message
    {
        Guid CorrelationId { get; set; }
    }
}