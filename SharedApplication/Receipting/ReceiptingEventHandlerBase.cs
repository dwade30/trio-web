﻿using SharedApplication.Messaging;
using SharedApplication.Receipting.Interfaces;

namespace SharedApplication.Receipting
{
    public abstract class ReceiptingEventHandlerBase<TEvent> : MessageEventHandler<TEvent> where TEvent : ReceiptingEvent
    {

    }
}