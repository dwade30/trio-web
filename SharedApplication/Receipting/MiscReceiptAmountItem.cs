﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Receipting
{
	public class MiscReceiptAmountItem : TransactionAmountItem
	{
		public decimal PercentageAmount { get; set; } = 0;
	}
}
