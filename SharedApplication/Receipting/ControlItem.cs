﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.Receipting
{
	public class ControlItem
	{
		public string Description { get; set; } = "";
		public string Name { get; set; } = "";
		public string Value { get; set; } = "";
	}
}
