﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SharedApplication
{
    public class InMemoryEditableRepository<TEntity,TIdType> : IEditableRepository<TEntity, TIdType> where TEntity: class, IIdentifiableEntity<TIdType>
    {   private Dictionary<TIdType,TEntity> repositoryEntries = new Dictionary<TIdType, TEntity>();
        public void Add(TEntity entity)
        {
            repositoryEntries.Add(entity.Id,entity);
        }

        public void Remove(TEntity entity)
        {
            repositoryEntries.Remove(entity.Id);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                repositoryEntries.Add(entity.Id,entity);
            }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                repositoryEntries.Remove(entity.Id);
            }
        }


        public System.Collections.Generic.IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<System.Func<TEntity, bool>> predicate)
        {
            return repositoryEntries.Values.AsQueryable().Where(predicate).ToList();
        }

        public TEntity Get(TIdType id)
        {
            if (repositoryEntries.ContainsKey(id))
            {
                return repositoryEntries[id];
            }

            return null;
        }

        public IEnumerable<TEntity> GetIncluding(Expression<Func<TEntity, bool>> whereProperty, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            if (whereProperty != null)
            {
                return repositoryEntries.Values.AsQueryable().Where(whereProperty);
            }

            return repositoryEntries.Values.ToList();
        }

        public System.Collections.Generic.IEnumerable<TEntity> GetAll()
        {
            return repositoryEntries.Values.ToList();
        }

        public void Remove(TIdType id)
        {
            if (repositoryEntries.ContainsKey(id))
            {
                repositoryEntries.Remove(id);
            }
        }
    }
}