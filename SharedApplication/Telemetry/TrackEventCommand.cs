﻿namespace SharedApplication.Telemetry
{
    public class TrackEventCommand : Messaging.Command
    {
        public string EventName { get; set; }

        public TrackEventCommand()
        {
            
        }

        public TrackEventCommand(string eventName)
        {
            EventName = eventName;
        }
    }
}
