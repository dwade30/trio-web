﻿using System;

namespace SharedApplication.Telemetry
{
    public class TrackTimingCommand : Messaging.Command
    {
        public TrackTimingCommand()
        {
            
        }

        public TrackTimingCommand(string title, long milliseconds, DateTime startDateTime, DateTime endDateTime)
        {
            Title = title;
            Milliseconds = milliseconds;
            StartDateTime = startDateTime;
            EndDateTime = endDateTime;
        }

        public long Milliseconds { get; set; }
        public string Title { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime StartDateTime { get; set; }
    }
}
