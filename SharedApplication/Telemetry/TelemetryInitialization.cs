﻿namespace SharedApplication.Telemetry
{
    public class TelemetryInitialization
    {
        public string Username { get; set; }
        public string Environment { get; set; }
        public string ASPNETSessionId { get; set; }
        public string WiseJSessionId { get; set; }
        public string DeviceId { get; set; }
        public string LocationIp { get; set; }
        public string Browser { get; set; }
        public string UserHostName { get; set; }
        public string UserHostAddress { get; set; }
        public string UserAgent { get; set; }
        public string AppVersion { get; set; }
        public string DeviceType { get; set; }
        public string IsHarrisStaffEnvironment { get; set; }
        public string IsHosted { get; set; }
    }
}