﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Telemetry
{
    public class TelemetryDummy : ITelemetryService
    {
        public void TrackException(Exception exception)
        {            
        }

        public void TrackTrace(string traceText)
        {            
        }

        public void TrackEvent(string eventName)
        {
        }

        public void TrackTiming(string title, long milliseconds)
        {         
        }

        public void TrackTiming(string title, DateTime startDateTime, DateTime endDateTime)
        {
        }

        public void TrackHit(string pageName)
        {
        }
        public void TrackException(Exception exception, IDictionary<string, string> extraProperties)
        {

        }

        public void TrackTrace(string traceText, IDictionary<string, string> extraProperties)
        {

        }

        public void TrackEvent(string eventName, IDictionary<string, string> extraProperties)
        {

        }
    }
}