﻿using System;
using System.Collections.Generic;

namespace SharedApplication.Telemetry
{
    public interface ITelemetryService
    {
        void TrackException(Exception exception);
        void TrackTrace(string traceText);
        void TrackEvent(string eventName);
        void TrackTiming(string title, long milliseconds);
        void TrackTiming(string title, DateTime startDateTime, DateTime endDateTime);
        void TrackHit(string pageName);
        void TrackException(Exception exception, IDictionary<string, string> extraProperties);
        void TrackTrace(string traceText, IDictionary<string, string> extraProperties);
        void TrackEvent(string eventName, IDictionary<string, string> extraProperties);
    }
}