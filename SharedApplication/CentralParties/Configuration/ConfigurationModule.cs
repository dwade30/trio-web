﻿using Autofac;

namespace SharedApplication.CentralParties.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CentralPartySearchViewModel>().As<ICentralPartySearchViewModel>();
        }
    }
}