﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CentralParties.Models
{
    public partial class Contact
    {
        public Contact()
        {
            ContactPhoneNumbers = new HashSet<ContactPhoneNumber>();
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        public int? PartyId { get; set; }

        public virtual CentralParty Party { get; set; }
        public virtual ICollection<ContactPhoneNumber> ContactPhoneNumbers { get; set; }
    }
}