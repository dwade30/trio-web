﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CentralParties.Models
{
    public partial class CentralComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string ProgModule { get; set; }
        public int? PartyId { get; set; }
        public string EnteredBy { get; set; }
        public DateTime? LastModified { get; set; }

        public virtual CentralParty Party { get; set; }
    }
}