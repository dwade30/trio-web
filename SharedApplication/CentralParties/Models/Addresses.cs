﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CentralParties.Models
{
    public partial class PartyAddress
    {
        public int Id { get; set; }
        public int? PartyId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string OverrideName { get; set; }
        public string AddressType { get; set; }
        public bool? Seasonal { get; set; }
        public string ProgModule { get; set; }
        public string Comment { get; set; }
        public int? StartMonth { get; set; }
        public int? StartDay { get; set; }
        public int? EndMonth { get; set; }
        public int? EndDay { get; set; }
        public int? ModAccountId { get; set; }

        public virtual CentralParty Party { get; set; }

        public string GetFormattedAddress()
        {
            string strReturn = "";
            string strCRLF = "";
            string strComma = "";
            //FC:FINAL:MSH - issue #905: replace "\r\n" for correct formatting string in labels
            if (!String.IsNullOrWhiteSpace(Address1))
            {
                strReturn = strCRLF + Address1;
                strCRLF = "\r\n";
            }
            
            if (!String.IsNullOrWhiteSpace(Address2))
            {
                strReturn += strCRLF + Address2;
                strCRLF = "\r\n";
            }
            if (!String.IsNullOrWhiteSpace(Address3))
            {
                strReturn += strCRLF + Address3;
                strCRLF = "\r\n";
            }
            if (!String.IsNullOrWhiteSpace(City) || !String.IsNullOrWhiteSpace(State) || !String.IsNullOrWhiteSpace(Zip))
            {                
                if (!String.IsNullOrWhiteSpace(City))
                    strComma = ",";
                strReturn += strCRLF + ((City + strComma + " " + State).Trim() + " " + Zip);
                strCRLF = "\r\n";
            }
            if (!String.IsNullOrWhiteSpace(Country))
            {
                if (Country.Trim().ToLower() != "united states")
                {
                    strReturn += strCRLF + Country;
                    strCRLF = "\r\n";
                }
            }
            return strReturn;            
        }
    }
}