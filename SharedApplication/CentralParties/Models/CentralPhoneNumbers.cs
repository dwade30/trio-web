﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CentralParties.Models
{
    public partial class CentralPhoneNumber
    {
        public int Id { get; set; }
        public int? PartyId { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
        public int? PhoneOrder { get; set; }
        public string Extension { get; set; }

        public virtual CentralParty Party { get; set; }
    }
}