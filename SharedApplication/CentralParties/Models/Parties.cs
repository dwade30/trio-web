﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SharedApplication.CentralParties.Models
{ 
    public partial class CentralParty
    {
        public CentralParty()
        {
            Addresses = new HashSet<PartyAddress>();
            CentralComments = new HashSet<CentralComment>();
            CentralPhoneNumbers = new HashSet<CentralPhoneNumber>();
            Contacts = new HashSet<Contact>();
        }

        public int Id { get; set; }
        public Guid? PartyGuid { get; set; }
        public int? PartyType { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Designation { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        public DateTime? DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public bool? Merged { get; set; }
        public bool? AutoMerged { get; set; }

        public virtual ICollection<PartyAddress> Addresses { get; set; }
        public virtual ICollection<CentralComment> CentralComments { get; set; }
        public virtual ICollection<CentralPhoneNumber> CentralPhoneNumbers { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }

        public bool HasSameAddress(CentralParty comparisonParty)
        {
            if (comparisonParty != null)
            {
                    if (this.Addresses.Any()  && comparisonParty.Addresses.Any())
                    {
                        foreach (PartyAddress Addr1 in Addresses)
                        {
                            foreach (PartyAddress Addr2 in comparisonParty.Addresses)
                            {
                                if (Addr1.Address1.ToLower() == Addr2.Address1.ToLower())
                                {
                                    if (Addr1.Address2.ToLower() == Addr2.Address2.ToLower())
                                    {
                                        if (Addr1.Address3.ToLower() == Addr2.Address3.ToLower())
                                        {
                                            if (Addr1.City.ToLower() == Addr2.City.ToLower())
                                            {
                                                if (Addr1.State.ToLower() == Addr2.State.ToLower())
                                                {
                                                    if (Addr1.Zip.ToLower() == Addr2.Zip.ToLower())
                                                    {
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

            }
            return false;
        }

        public string FullName()
        {
            return  (((FirstName.Trim() + " " + MiddleName).Trim() + " " + LastName).Trim() + " " + Designation).Trim();
        }

        public string FullNameLastFirst()
        {
            if (LastName != "")
            {
                return  ((LastName.Trim() + ", " + FirstName + " " + MiddleName).ToString() + " " + Designation).ToString();
            }
            else
            {
                return ((FirstName + " " + MiddleName).Trim() + " " + Designation).Trim();
            }
        }

        public PartyAddress PrimaryAddress()
        {
            return Addresses.FirstOrDefault(a => a.AddressType == "Primary");
        }
    }
}