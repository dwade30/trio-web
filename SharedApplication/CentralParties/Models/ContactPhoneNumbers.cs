﻿using System;
using System.Collections.Generic;

namespace SharedApplication.CentralParties.Models
{
    public partial class ContactPhoneNumber
    {
        public int Id { get; set; }
        public int? ContactId { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
        public int? PhoneOrder { get; set; }
        public string Extension { get; set; }

        public virtual Contact Contact { get; set; }
    }
}