﻿using SharedApplication.CentralParties.Commands;
using SharedApplication.Messaging;

namespace SharedApplication.CentralParties
{
    public class ChooseCentralPartyHandler : CommandHandler<ChooseCentralParty,int>
    {
        private IModalView<ICentralPartySearchViewModel> searchView;
        public ChooseCentralPartyHandler(IModalView<ICentralPartySearchViewModel> searchView)
        {
            this.searchView = searchView;
        }
        protected override int Handle(ChooseCentralParty command)
        {
            searchView.ShowModal();
            return searchView.ViewModel.PartyId;
        }
    }
}