﻿using SharedApplication.CentralParties.Models;
using SharedApplication.Messaging;

namespace SharedApplication.CentralParties.Commands
{
    public class GetCentralParty : Command<CentralParty>
    {
        public GetCentralParty(int id)
        {
            Id = id;
        }
        public int Id { get; set; } = 0;
        public bool IncludeChildren { get; set; } = false;
    }
}