﻿namespace SharedApplication.CentralParties
{
    public interface ICentralPartySearchViewModel
    {
        int PartyId { get; set; }        
    }
}