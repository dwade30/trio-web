﻿namespace SharedApplication.CentralParties
{
    public enum CentralPartySearchMethod
    {
        Contains = 0,
        StartsWith = 1,
        Equals = 2
    }
}