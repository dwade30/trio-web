﻿using System.Linq;
using SharedApplication.CentralParties.Models;

namespace SharedApplication.CentralParties
{
    public interface ICentralPartyContext
    {
       IQueryable<PartyAddress> Addresses { get; }
        IQueryable<CentralComment> CentralComments { get;  }
        IQueryable<CentralPhoneNumber> CentralPhoneNumbers { get;  }
        IQueryable<ContactPhoneNumber> ContactPhoneNumbers { get;  }
        IQueryable<Contact> Contacts { get; }
        IQueryable<Dbversion> Dbversion { get;  }
        IQueryable<CentralParty> Parties { get; }
    }
}