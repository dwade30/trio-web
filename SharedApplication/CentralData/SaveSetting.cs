﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;

namespace SharedApplication.CentralData
{
	public class SaveSetting : SharedApplication.Messaging.Command
	{
		public Setting setting { get; set; }
		public SettingOwnerType type {get; set; }
	}
}
