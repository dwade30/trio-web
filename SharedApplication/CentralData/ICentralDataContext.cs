﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;

namespace SharedApplication.CentralData
{
	public interface ICentralDataContext
	{
		IQueryable<BankIndex> DefaultBanks { get; }
		IQueryable<Region> Regions { get; }
		IQueryable<RegionalTownBreakdown> RegionalTownBreakdowns { get; }
		IQueryable<Setting> Settings { get; }
		IQueryable<GroupMaster> GroupMasters { get; }
		IQueryable<ModuleAssociation> ModuleAssociations { get; }
		IQueryable<ImportedPayment> ImportedPayments { get; }
	}
}
