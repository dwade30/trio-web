﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Enums;

namespace SharedApplication.CentralData
{
	public class SettingSearchCriteria
	{
		public string SettingName { get; set; }
		public string SettingType { get; set; }
		public SettingOwnerType OwnerType { get; set; }
		public string Owner { get; set; }
	}
}
