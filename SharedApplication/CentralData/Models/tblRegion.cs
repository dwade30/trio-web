﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Extensions;

namespace SharedApplication.CentralData.Models
{
    public partial class Region
    {
        public int ID { get; set; }
        public string TownName { get; set; }
        public string TownAbbreviation { get; set; }
        public int? TownNumber { get; set; }
        public string ResCode { get; set; }
        public string Fund { get; set; }
        public string RECommitmentAccount { get; set; }
        public string PPCommitmentAccount { get; set; }
        public string RESupplementalAccount { get; set; }
        public string PPSupplementalAccount { get; set; }
    }
}
