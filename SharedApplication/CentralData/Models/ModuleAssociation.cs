﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData.Models
{
	public partial class ModuleAssociation
	{
		public int ID { get; set; }

		public int? GroupNumber { get; set; }

		public string Module { get; set; }

		public int? Account { get; set; }

		public bool? GroupPrimary { get; set; }

		public bool? PrimaryAssociate { get; set; }

		public int? REMasterAcct { get; set; }
	}
}
