﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData.Models
{
	public partial class GroupMaster
	{
		public int ID { get; set; }

		public int? GroupNumber { get; set; }

		public DateTime? DateCreated { get; set; }

		public string Comment { get; set; }
	}
}
