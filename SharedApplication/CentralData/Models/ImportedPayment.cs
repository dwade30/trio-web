﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData.Models
{
	public class ImportedPayment
	{
		public int Id { get; set; }
		public int BillId { get; set; }
		public string BillType { get; set; }
		public string BillNumber { get; set; }
		public int AccountNumber { get; set; }
		public DateTime PaymentDate { get; set; }
		public decimal PaymentAmount { get; set; }
		public string PaidBy { get; set; }
		public DateTime? AppliedDateTime { get; set; }
		public bool? Processed { get; set; }
		public string PaymentIdentifier { get; set; }
	}
}
