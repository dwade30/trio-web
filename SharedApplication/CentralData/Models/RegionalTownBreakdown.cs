﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData.Models
{

	public partial class RegionalTownBreakdown
	{
		public int ID { get; set; }

		public int? Code { get; set; }

		public int? TownNumber { get; set; }

		public double? Percentage { get; set; }
	}
}
