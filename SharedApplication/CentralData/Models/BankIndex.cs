﻿namespace SharedApplication.CentralData.Models
{
    public class BankIndex
    {
        public int Id { get; set; } = 0;
        public int APBank { get; set; } = 0;
        public int PayrollBank { get; set; } = 0;
        public short CRBank { get; set; } = 0;
    }
}