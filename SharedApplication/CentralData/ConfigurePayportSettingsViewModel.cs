﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SharedApplication;
using SharedApplication.Enums;

namespace SharedApplication.CentralData
{
	public class ConfigurePayportSettingsViewModel : IConfigurePayportSettingsViewModel
	{
		private ISettingService settingService;
		private List<string> combinedServiceDescriptionOptions = new List<string>();
		public ConfigurePayportSettingsViewModel(ISettingService settingService)
		{
			this.settingService = settingService;

			combinedServiceDescriptionOptions.Add("Sewer");
			combinedServiceDescriptionOptions.Add("Utility");
			combinedServiceDescriptionOptions.Add("Water");
		}
		public bool CombineUtilityServices { get; set; }
		public string CombinedUtilityType { get; set; } = "";
		public bool SyncRealEstate { get; set; }
		public bool SyncPersonalProperty { get; set; }
		public bool SyncUtilityBilling { get; set; }
		public string GeoCode { get; set; } = "";
		
		public List<string> CombinesServiceDescriptionOptions { get => combinedServiceDescriptionOptions; }

		public void LoadSettings()
		{
			SyncRealEstate = settingService.GetSettingValue(SettingOwnerType.Global, "PayPortExport", "SyncRE").SettingValue.ToLower() == "true";
			SyncPersonalProperty = settingService.GetSettingValue(SettingOwnerType.Global, "PayPortExport", "SyncPP").SettingValue.ToLower() == "true";
			SyncUtilityBilling = settingService.GetSettingValue(SettingOwnerType.Global, "PayPortExport", "SyncUB").SettingValue.ToLower() == "true";
			CombineUtilityServices = settingService.GetSettingValue(SettingOwnerType.Global, "PayPortExport", "CombineUBBills").SettingValue.ToLower() == "true";
			CombinedUtilityType = settingService.GetSettingValue(SettingOwnerType.Global, "PayPortExport", "CombinedUBBillType").SettingValue.Trim();
			GeoCode = settingService.GetSettingValue(SettingOwnerType.Global, "PayPortExport", "GeoCode").SettingValue.Trim();
		}

		public bool SaveSettings()
		{
			try
			{
				settingService.SaveSetting(SettingOwnerType.Global, "PayPortExport", "SyncRE", SyncRealEstate ? "true" : "false");
				settingService.SaveSetting(SettingOwnerType.Global, "PayPortExport", "SyncPP", SyncPersonalProperty ? "true" : "false");
				settingService.SaveSetting(SettingOwnerType.Global, "PayPortExport", "SyncUB", SyncUtilityBilling ? "true" : "false");
				settingService.SaveSetting(SettingOwnerType.Global, "PayPortExport", "CombineUBBills", CombineUtilityServices ? "true" : "false");
				settingService.SaveSetting(SettingOwnerType.Global, "PayPortExport", "CombinedUBBillType", CombinedUtilityType.Trim());
				settingService.SaveSetting(SettingOwnerType.Global, "PayPortExport", "GeoCode", GeoCode.Trim());

				return true;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
			
		}
	}
}
