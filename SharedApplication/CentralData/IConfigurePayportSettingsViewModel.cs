﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData
{
	public interface IConfigurePayportSettingsViewModel
	{
		bool CombineUtilityServices { get; set; }
		string CombinedUtilityType { get; set; } 
		bool SyncRealEstate { get; set; }
		bool SyncPersonalProperty { get; set; }
		bool SyncUtilityBilling { get; set; }
		string GeoCode { get; set; }
		List<string> CombinesServiceDescriptionOptions { get; }
		void LoadSettings();
		bool SaveSettings();
	}
}
