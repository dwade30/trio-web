﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;

namespace SharedApplication.CentralData
{
	public interface IRegionalTownService
	{
		bool IsRegionalTown();
		string TownAbbreviation(int townId);
		string TownName(int townId);
		IEnumerable<Region> RegionalTowns();
	}
}
