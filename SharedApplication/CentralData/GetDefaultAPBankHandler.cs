﻿using System.Linq;
using SharedApplication.Messaging;

namespace SharedApplication.CentralData
{
    public class GetDefaultAPBankHandler: CommandHandler<GetDefaultAPBank,int>
    {
        private ICentralDataContext cdContext;
        public GetDefaultAPBankHandler(ICentralDataContext cdContext)
        {
            this.cdContext = cdContext;
        }
        protected override int Handle(GetDefaultAPBank command)
        {
            var banks = cdContext.DefaultBanks.FirstOrDefault();
            if (banks == null)
            {
                return 0;
            }

            return banks.APBank;
        }
    }
}