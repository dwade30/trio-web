﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.CentralData.Models;
using SharedApplication.Enums;

namespace SharedApplication.CentralData.Extensions
{
    public static class IEnumberableExtensions
    {
        public static IEnumerable<DescriptionIDPair> MapToDescriptionIDPairsShowingTownNumberAndTownName(
            this IEnumerable<Region> towns)
        {
            return towns.Select(town => new DescriptionIDPair()
            {
                Description = town.TownNumber + " - " + town.TownName,
                ID = town.ID
            }).ToList();
        }

        public static string AsString(this SettingOwnerType ownerType)
        {
            switch (ownerType)
            {
                case SettingOwnerType.Machine:
                    return "Machine";
                    break;
                case SettingOwnerType.User:
                    return "User";
                    break;
                default:
                    return "";
                    break;
            }
        }
    }
}
