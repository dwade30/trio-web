﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData
{
	public class GroupMasterSearchCriteria
	{
		public int GroupNumber { get; set; }
		public ModuleAssociationSearchCriteria AssociatedWith { get; set; }
	}

	
}
