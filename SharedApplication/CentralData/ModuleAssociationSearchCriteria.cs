﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.CentralData
{
	public class ModuleAssociationSearchCriteria
	{
		public string Module { get; set; }
		public int AccountNumber { get; set; }
		public int REMasterAccount { get; set; }
	}
}
