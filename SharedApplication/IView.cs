﻿namespace SharedApplication
{
    public interface IView<TViewModelType>
    {
        void Show();
        TViewModelType ViewModel { get; set; }
    }
}