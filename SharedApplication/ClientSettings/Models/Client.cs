﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings.Models
{
	public class Client
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public Guid ClientIdentifier { get; set; }
	}
}
