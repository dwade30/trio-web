﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings.Models
{
	public class DBVersion
	{
		public int Id { get; set; }
		public int? Build { get; set; }
		public int? Major { get; set; }
		public int? Minor { get; set; }
		public int? Revision { get; set; }
	}
}
