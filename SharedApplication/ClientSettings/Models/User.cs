﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings.Models
{
	public class User
	{
		public int Id { get; set; }

		public string UserID { get; set; }

		public string OPID { get; set; }

		public string UserName { get; set; }

		public int? Frequency { get; set; }

		public DateTime? DateChanged { get; set; }

		public DateTime? UpdateDate { get; set; }

		public string Password { get; set; }

		public bool? DefaultAdvancedSearch { get; set; }

		public bool? LockedOut { get; set; }

		public bool? Inactive { get; set; }

		public int? FailedAttempts { get; set; }

		public DateTime? LockoutDateTime { get; set; }

		public string Salt { get; set; }

		public bool? CanUpdateSite { get; set; }

		public Guid ClientIdentifier { get; set; }

		public Guid UserIdentifier { get; set; }

		public ICollection<PasswordHistory> PreviousPasswords { get; set; }
    }
}
