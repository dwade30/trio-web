﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings.Models
{
	public class ScheduledUpdate
	{
		public int Id { get; set; }
		public int ScheduledByUserId { get; set; }
		public DateTime ScheduledDateTime { get; set; }
		public bool Completed { get; set; }
		public string UpdateResult { get; set; }
	}
}
