﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings.Models
{
	public class SiteVersion
	{
		public int Id { get; set; }
		public string Version { get; set; }
	}
}
