﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings.Enums
{
	public enum ScheduledUpdateCompletedOptions
	{
		All = 0,
		CompletedOnly = 1,
		NotCompletedOnly = 2
	}
}
