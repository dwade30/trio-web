﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedApplication.ClientSettings
{
	public class UserSearchCriteria
	{
		public int UserId { get; set; } = 0;
		public string ClientId { get; set; } = "";
	}
}
