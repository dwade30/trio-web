﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Messaging;

namespace SharedApplication.ClientSettings.Commands
{
	public class SaveScheduledUpdate : Command<bool>
	{
		public ScheduledUpdate updateToSave { get; set; } = new ScheduledUpdate();
		public string Version { get; set; } = "";
	}
}
