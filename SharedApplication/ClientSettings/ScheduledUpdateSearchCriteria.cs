﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Enums;

namespace SharedApplication.ClientSettings
{
	public class ScheduledUpdateSearchCriteria
	{
		public ScheduledUpdateCompletedOptions CompletedSelection { get; set; } = ScheduledUpdateCompletedOptions.All;

	}
}
