﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Models;

namespace SharedApplication.ClientSettings.ScheduleUpdate
{
	public interface IScheduleUpdateViewModel
	{
		ScheduledUpdate UpcomingScheduledUpdate { get; }
		bool UpdateScheduled { get; }
		bool IsNewVersionAvailable();
		bool SaveScheduledUpdate(DateTime updateTime);
		bool CancelScheduledUpdate();
		string LatestVersion { get; set; }
		string CurrentVersion { get; set; }

	}
}
