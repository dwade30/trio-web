﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using SharedApplication.ClientSettings.Commands;
using SharedApplication.ClientSettings.Enums;
using SharedApplication.ClientSettings.Models;
using SharedApplication.Messaging;

namespace SharedApplication.ClientSettings.ScheduleUpdate
{
	public class ScheduleUpdateViewModel : IScheduleUpdateViewModel
	{
		private CommandDispatcher commandDispatcher;
		private UserInformation userInformation;
		private IQueryHandler<ScheduledUpdateSearchCriteria, IEnumerable<ScheduledUpdate>> scheduledUpdatesQueryHandler;
		private ScheduledUpdate upcomingUpdate;
		private string currentVersion = "";
		private string latestVersion = "";
		public ScheduleUpdateViewModel(
			IQueryHandler<ScheduledUpdateSearchCriteria, IEnumerable<ScheduledUpdate>> scheduledUpdatesQueryHandler, CommandDispatcher commandDispatcher, UserInformation userInformation)
		{
			this.scheduledUpdatesQueryHandler = scheduledUpdatesQueryHandler;
			this.commandDispatcher = commandDispatcher;
			this.userInformation = userInformation;

			GetUpcomingUpdate();
			SetVersions();
		}

		private void SetVersions()
		{
			WebRequest request = WebRequest.Create("http://azurefunctions20200514132729.azurewebsites.net/api/GetAvailableVersion?code=37I4HfEitJZY43Vizrj4O/H3/apGHcBtrliPaYI9WXLZmWFmskhrYw==");
			WebResponse response = request.GetResponse();

			using (Stream dataStream = response.GetResponseStream())
			{
				StreamReader reader = new StreamReader(dataStream);
				latestVersion = reader.ReadToEnd();
			}

			response.Close();

			currentVersion = commandDispatcher.Send(new GetSiteVersion()).Result;
		}

		private void GetUpcomingUpdate()
		{
			upcomingUpdate = scheduledUpdatesQueryHandler.ExecuteQuery(new ScheduledUpdateSearchCriteria
			{
				CompletedSelection = ScheduledUpdateCompletedOptions.NotCompletedOnly
			}).FirstOrDefault();
		}

		public ScheduledUpdate UpcomingScheduledUpdate { get => upcomingUpdate; }

		public bool UpdateScheduled
		{
			get => upcomingUpdate != null;
		}

		public bool IsNewVersionAvailable()
		{
			return LatestVersion != CurrentVersion;
		}

		public string LatestVersion { 
			get => latestVersion;
			set => latestVersion = value;
		}

		public string CurrentVersion
		{
			get => currentVersion;
			set => currentVersion = value;
		}
		public bool SaveScheduledUpdate(DateTime updateTime)
		{
			var command = new SaveScheduledUpdate();

			command.updateToSave.ScheduledDateTime = updateTime;
			command.updateToSave.ScheduledByUserId = userInformation.Id;
			command.updateToSave.Completed = false;
			command.updateToSave.UpdateResult = "";

			var result = commandDispatcher.Send(command).Result;

			if (result)
			{
				GetUpcomingUpdate();
			}

			return result;
		}

		public bool CancelScheduledUpdate()
		{
			var result = commandDispatcher.Send(new CancelScheduledUpdate()).Result;
			if (result)
			{
				GetUpcomingUpdate();
			}
			
			return result;
		}
	}
}
