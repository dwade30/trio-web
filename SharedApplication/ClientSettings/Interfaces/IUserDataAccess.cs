﻿using System.Collections.Generic;
using SharedApplication.ClientSettings.Models;

namespace SharedApplication.ClientSettings.Interfaces
{
	public interface IUserDataAccess
	{
		User GetUserByID(int lngUserID);
		User GetUserByUserId(string strUserId);
		IEnumerable<User> GetUsers();
		IEnumerable<PasswordHistory> GetPreviousPasswords(int userId);
		bool UpdatePassword(User user, string newPassword, string salt);
		bool SaveUser(User user);
		bool DuplicateUserIdExists(string strUserId);
		bool SaveChanges();
	}
}