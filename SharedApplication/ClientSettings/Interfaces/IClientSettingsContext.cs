﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.ClientSettings.Models;

namespace SharedApplication.ClientSettings.Interfaces
{
	public interface IClientSettingsContext
	{
		IQueryable<User> Users { get; }
		IQueryable<SiteVersion> SiteVersions { get; }
		IQueryable<DBVersion> DBVersions { get; }
		IQueryable<PasswordHistory> PasswordHistories { get; }
		IQueryable<Client> Clients { get; }
		IQueryable<ScheduledUpdate> ScheduledUpdates { get; }
	}
}
