﻿/**
 * wisej.web.extender.CustomProperties
 *
 * This extender class manages a collection of CustomProperties.
 */
qx.Class.define("wisej.web.extender.CustomProperties", {

	extend: qx.core.Object,

	// All Wisej components must include this mixin
	// to provide services to the Wisej core.
	include: [wisej.mixin.MWisejComponent],

	construct: function () {

		this.base(arguments);
	},

	properties: {

		/**
		 * CustomProperties property.
		 *
		 * Collection of widgets and relative values. The items
		 * in the collection use this format: { id: [widget-id], value: [CustomProperties value] }.
		 */
        customProperties: { init: null, nullable: true, check: "Array", apply: "_applyCustomProperties" },

	},

	members: {

		_applyCustomProperties: function (value, old) {

			if (old != null && old.length > 0) {
				for (var i = 0; i < old.length; i++) {

					var id = old[i].id;

					// don't remove the CustomProperties if it is in the
					// new values list.
					if (value && value.length > 0) {
						for (var j = 0; j < value.length; j++) {
							if (value[j].id == id) {
								id = null;
								break;
							}
						}
					}

					if (!id)
						continue;
				}
            }

            if (value != null && value.length > 0) {
                for (var i = 0; i < value.length; i++) {

                    var id = value[i].id;
                    var comp = Wisej.Core.getComponent(id);
                    if (comp) {
                        comp.customProperties = value[i].value;
                    }                    
                }
            }
		}
	}
});