﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using Wisej.Base;
using Wisej.Core;

namespace Wisej.Web.Ext.CustomProperties
{
	/// <summary>
	/// Represents a dynamic Custom properties ehich can be added to a control, and used on client side.
	/// </summary>
	[ToolboxItem(true)]
	public class CustomProperties : Wisej.Web.Component, IExtenderProvider
	{
        // collection of controls with the related CustomProperties value.
        private Dictionary<Control, Properties> properties;

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Wisej.Web.Ext.CustomProperties.CustomProperties" /> without a specified container.
        /// </summary>
        public CustomProperties()
		{
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Wisej.Web.Ext.CustomProperties.CustomProperties" /> class with a specified container.
        /// </summary>
        /// <param name="container">An <see cref="T:System.ComponentModel.IContainer" />container. </param>
        public CustomProperties(IContainer container)
			: this()
		{
			if (container == null)
				throw new ArgumentNullException("container");

			container.Add(this);
		}

		#endregion

		#region Events
		#endregion

		#region Properties

		/// <summary>
		/// Returns or sets the object that contains programmer-supplied data associated with this component.
		/// </summary>
		/// <returns>An <see cref="T:System.Object" /> that contains user data. The default is null.</returns>
		[DefaultValue(null)]
		[Localizable(false)]
		[SRCategory("CatData")]
		[SRDescription("ControlTagDescr")]
		[TypeConverter(typeof(StringConverter))]
		public object Tag
		{
			get { return this._tag; }
			set { this._tag = value; }
		}
		private object _tag;

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if <see cref="T:Wisej.Web.Ext.CustomProperties.CustomProperties" /> can offer an extender property to the specified target component.
        /// </summary>
        /// <returns>true if the <see cref="T:Wisej.Web.Ext.CustomProperties.CustomProperties" /> class can offer one or more extender properties; otherwise, false.</returns>
        /// <param name="target">The target object to add an extender property to. </param>
        public bool CanExtend(object target)
		{
			return (target is Control);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				Clear();
			}

			base.Dispose(disposing);
		}

		/// <summary>
		/// Removes all custom properties.
		/// </summary>
		public void Clear()
		{
			if (this.properties != null)
			{
				this.properties.ToList().ForEach((o) => {
					o.Key.Disposed -= this.Control_Disposed;
					o.Key.ControlCreated -= this.Control_Created;
				});

				this.properties.Clear();

				Update();
			}
		}

        /// <summary>
        /// Returns the notification value associated with the specified control.
        /// </summary>
        /// <param name="control">The <see cref="T:Wisej.Web.Control" /> for which to retrieve the <see cref="T:Wisej.Web.Ext.CustomProperties.CustomProperties" /> value. </param>
        [DefaultValue(0)]
		[DisplayName("CustomProperties")]
		public DynamicObject GetCustomPropertiesValue(Control control)
		{
			if (!HasCustomPropertiesEntry(control))
				return new DynamicObject();

			return GetCustomProperties(control).Value;
		}

        /// <summary>
        /// Set the CustomProperties value with the specified control.
        /// </summary>
        /// <param name="control">The <see cref="T:Wisej.Web.Control" /> to associate the JavaScript with. </param>
        /// <param name="value">The value to set in the CustomProperties</param>
        public void SetCustomPropertiesValue(Control control, DynamicObject value)
		{
			if (value == null)
				throw new ArgumentOutOfRangeException("value");

            GetCustomProperties(control).Value = value;
			Update();
		}
        
		/// <summary>
		/// Returns if the control is associated to the extender.
		/// </summary>
		/// <param name="control"></param>
		/// <returns></returns>
		private bool HasCustomPropertiesEntry(Control control)
		{
			if (control == null)
				throw new ArgumentNullException("control");

			return this.properties != null && this.properties.ContainsKey(control);

		}

		/// <summary>
		/// Creates or retrieves the extender data entry associated with the control.
		/// </summary>
		/// <param name="control"></param>
		/// <returns></returns>
		private Properties GetCustomProperties(Control control)
		{
			if (control == null)
				throw new ArgumentNullException("control");

			if (this.properties == null)
				this.properties = new Dictionary<Control, Properties>();

            Properties property = null;
			if (!this.properties.TryGetValue(control, out property))
			{
				property = new Properties() { Widget = control };
				this.properties[control] = property;

				// remove the control from the extender when it's disposed.
				control.Disposed -= this.Control_Disposed;
				control.Disposed += this.Control_Disposed;

			}
			return property;
		}

		private void Control_Disposed(object sender, EventArgs e)
		{
			Control control = (Control)sender;
			control.Disposed -= this.Control_Disposed;
			control.ControlCreated -= this.Control_Created;

			// remove the extender values associated with the disposed control.
			if (this.properties != null)
				this.properties.Remove(control);
		}

		private void Control_Created(object sender, EventArgs e)
		{
			// handle the delayed registration of this extender for a control
			// that was not created (not visible) when the extender tried to register it.
			Control control = (Control)sender;
			control.ControlCreated -= this.Control_Created;

			// update the extender, now it will send also this newly created control.
			Update();
		}

        /// <summary>
        /// Returns a string representation for this control.
        /// </summary>
        /// <returns>A <see cref="T:System.String" /> containing a description of the <see cref="T:Wisej.Web.Ext.CustomProperties.CustomProperties" />.</returns>
        public override string ToString()
		{
			return base.ToString();
		}

		#endregion

		#region Wisej Implementation
		/// <summary>
		/// Renders the client component.
		/// </summary>
		/// <param name="config">Dynamic configuration object.</param>
		protected override void OnWebRender(dynamic config)
		{
			base.OnWebRender((object)config);

			config.className = "wisej.web.extender.CustomProperties";

			if (this.properties != null)
			{
				List<object> list = new List<object>();
				foreach (var entry in this.properties)
				{
					var control = entry.Key;
					var settings = entry.Value;

					// skip controls that are not yet created.
					if (!control.Created)
						continue;

					if (settings.Value.Count > 0)
					{
						list.Add(new {
							id = ((IWisejComponent)control).Id,
							value = settings.Value
						});
					}
				}
				config.customProperties = list.ToArray();

				// register non-created control for delayed registration.
				this.properties.Where(o => !o.Key.Created).ToList().ForEach(o => o.Key.ControlCreated += this.Control_Created);
			}
		}

        #endregion

        #region Properties

        /// <summary>
        /// Represents a Properties component.
        /// </summary>
        private class Properties
		{
			public DynamicObject Value;
            public Control Widget;
		}

		#endregion

	}
}
