//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWCE0000
{
	public class clsAttachedDocuments
	{
		//=========================================================
		private FCCollection dlDocList = new FCCollection();
		private int intCurrentIndex;

		public int Clear()
		{
			int Clear = 0;
			// returns error number
			Clear = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!FCUtils.IsEmpty(dlDocList))
				{
					if (dlDocList.Count > 0)
					{
						int x;
                        //FC:FINAL:MSH - i.issue #1729: wrong list erasing. Will be removed only part of list elements
                        int count = dlDocList.Count;
                        //for (x = 1; x <= dlDocList.Count; x++)
                        for (x = 1; x <= count; x++)
						{
							dlDocList.Remove(1);
						}
						// x
					}
				}
				intCurrentIndex = 0;
				return Clear;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				Clear = fecherFoundation.Information.Err(ex).Number;
			}
			return Clear;
		}

		public int Count
		{
			get
			{
				int Count = 0;
				if (!FCUtils.IsEmpty(dlDocList))
				{
					Count = dlDocList.Count;
				}
				else
				{
					Count = 0;
				}
				return Count;
			}
		}
		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		public void DeleteByID(int lngID, string strDatabase = "")
		{
            if (lngID <= 0) return;

            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            docService.DeleteCentralDocument(new DeleteCommand {Id = lngID});
        }

		public int LoadDocuments(int lngID, string referenceType, string strDatabase = "", string strWhere = "")
		{
			int ret = 0;
			// returns the error number
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ret = 0;
				
                var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
                var docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand {DataGroup = "Code Enforcement", ReferenceType = referenceType, ReferenceId = lngID});

				Clear();

                foreach (var centralDocumentHeader in docList)
                {
                    AddDoc(lngID, referenceType, centralDocumentHeader.ItemName, centralDocumentHeader.ItemDescription, centralDocumentHeader.ID);
                }
				
				return ret;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ret = fecherFoundation.Information.Err(ex).Number;
			}
			return ret;
		}

		public int AddDoc(int lngID, string referenceType, string strFileName, string strDescription, int lngAutoID)
		{
			int ret = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngReturn;
				clsAttachedDoc tDoc = new clsAttachedDoc();
				
				tDoc.ID = lngAutoID;
				tDoc.Description = strDescription;
				tDoc.FileName = strFileName;
				tDoc.ReferenceID = lngID;
				tDoc.ReferenceType = referenceType;
				tDoc.Changed = false;
				lngReturn = InsertDoc(tDoc);
				ret = lngReturn;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ret = fecherFoundation.Information.Err(ex).Number;
			}
			return ret;
		}

		private int InsertDoc(clsAttachedDoc tDoc)
		{
			int ret = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				dlDocList.Add(tDoc, "ID" + tDoc.ID);
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				ret = fecherFoundation.Information.Err(ex).Number;
			}
			return ret;
		}

		public int MoveFirst()
		{
			int ret = 0;
            intCurrentIndex = 0;
			var intReturn = MoveNext();
			ret = intReturn;
			return ret;
		}

		public int MoveNext()
		{
			int ret = 0;
            var intReturn = -1;
			if (!FCUtils.IsEmpty(dlDocList))
			{
				if (dlDocList.Count >= intCurrentIndex + 1 && dlDocList.Count > 0)
				{
					while (intCurrentIndex <= dlDocList.Count)
					{
						intCurrentIndex += 1;

                        if (dlDocList[intCurrentIndex] == null) continue;

                        intReturn = intCurrentIndex;
                        break;
                    }
					if (intCurrentIndex > dlDocList.Count)
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			ret = intReturn;
			return ret;
		}

		public int MovePrevious()
		{
			int ret = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(dlDocList))
			{
				if (dlDocList.Count > 0)
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;

                        if (intCurrentIndex <= 0) continue;

                        if (dlDocList[intCurrentIndex] != null)
                        {
                            intReturn = intCurrentIndex;
                        }
                    }
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			ret = intReturn;
			return ret;
		}


		public clsAttachedDoc GetCurrentDoc()
		{
			clsAttachedDoc GetCurrentDoc = null;
			clsAttachedDoc tDoc;
			tDoc = null;
			if (!FCUtils.IsEmpty(dlDocList) && intCurrentIndex > 0 && dlDocList[intCurrentIndex] != null)
			{
                tDoc = dlDocList[intCurrentIndex];
            }
			GetCurrentDoc = tDoc;
			return GetCurrentDoc;
		}


		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		public void RemovebyID(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!FCUtils.IsEmpty(dlDocList))
				{
					dlDocList.Remove("ID" + FCConvert.ToString(lngID));
				}
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}
	}
}
