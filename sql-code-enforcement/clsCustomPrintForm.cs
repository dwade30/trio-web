//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWCE0000
{
	public class clsCustomPrintForm
	{
		//=========================================================
		private FCCollection ControlList = new FCCollection();
		private int intCurrentIndex;

		public void LoadControlTypes()
		{
			clsCustomControlParameters tcp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strJust = "";
				// vbPorter upgrade warning: intType As int	OnWrite(DAO.DataTypeEnum)
				int intType = 0;
				AddControlType("General", 12, 50, "Return Address", "", "Left", 1, 2, "", "", "", "Return Address", 1, "", true, "", "", 10, false);
				AddControlType("Owner", 1, 1, "Full Name", "", "Left", 0.1667, 2, "", "", "", "Owner's Full Name", 1, "", true, "", "", 10, false);
				AddControlType("Owner", 1, 2, "2nd Full Name", "", "Left", 0.1667, 2, "", "", "", "Second Owner's Full Name", 2, "", true, "", "", 10, false);
				AddControlType("Owner", 1, 41, "Last, First", "", "Left", 0.1667, 2, "", "", "", "Owner's Last, First", 3, "", true, "", "", 10, false);
				AddControlType("Owner", 1, 42, "2nd Last, First", "", "Left", 0.1667, 2, "", "", "", "Second Owner's Last, First", 4, "", true, "", "", 10, false);
				AddControlType("Owner", 1, 3, "First Name", "twce0000.vb1", "Left", 0.1667, 1, "", "First", "", "Owner's First Name", 5, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 4, "Middle Name", "twce0000.vb1", "Left", 0.1667, 1, "", "Middle", "", "Owner's Middle Name", 6, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 5, "Last Name", "twce0000.vb1", "Left", 0.1667, 1, "", "Last", "", "Owner's Last Name", 7, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 6, "Designation", "twce0000.vb1", "Left", 0.1667, 1, "", "Desig", "", "Owner's Designation", 8, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 7, "2nd First Name", "twce0000.vb1", "Left", 0.1667, 1, "", "SecFirst", "", "Second Owner's First Name", 9, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 8, "2nd Middle Name", "twce0000.vb1", "Left", 0.1667, 1, "", "SecMiddle", "", "Second Owner's Middle Name", 10, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 9, "2nd Last Name", "twce0000.vb1", "Left", 0.1667, 1, "", "SecLast", "", "Second Owner's Last Name", 11, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 10, "2nd Designation", "twce0000.vb1", "Left", 0.1667, 1, "", "SecDesig", "", "Second Owner's Designation", 12, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 11, "Mailing Address", "", "Left", 1, 2, "", "", "", "Mailing Address", 13, "", true, "", "", 10, false);
				AddControlType("Owner", 1, 12, "Address 1", "twce0000.vb1", "Left", 0.1667, 2, "", "Address1", "", "Address Line 1", 14, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 13, "Address 2", "twce0000.vb1", "Left", 0.1667, 2, "", "Address2", "", "Address Line 2", 15, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 14, "City", "twce0000.vb1", "Left", 0.1667, 1, "", "City", "", "City", 16, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 15, "State", "twce0000.vb1", "Left", 0.1667, 1, "", "State", "", "State", 17, "", false, "CEMaster", "", 10, false);
				AddControlType("Owner", 1, 16, "Zip", "twce0000.vb1", "Left", 0.1667, 1, "", "Zip", "", "Zip", 18, "", true, "CEMaster", "", 10, false);
				AddControlType("Property", 2, 17, "Account", "twce0000.vb1", "Right", 0.1667, 1, "", "CEAccount", "", "Account Number", 1, "", false, "CEMaster", "", 4, false);
				AddControlType("Property", 2, 18, "Map/Lot", "twce0000.vb1", "Left", 0.1667, 1, "", "MapLot", "", "Map and Lot", 2, "", false, "CEMaster", "", 10, false);
				AddControlType("Property", 2, 19, "Street Number", "twce0000.vb1", "Right", 0.1667, 0.5, "", "StreetNumber", "", "Street Number", 3, "", false, "CEMaster", "", 4, false);
				AddControlType("Property", 2, 20, "Apartment", "twce0000.vb1", "Left", 0.1667, 0.5, "", "Apt", "", "Apartment", 4, "", false, "CEMaster", "", 10, false);
				AddControlType("Property", 2, 21, "Street Name", "twce0000.vb1", "Left", 0.1667, 1, "", "Streetname", "", "Street Name", 5, "", false, "CEMaster", "", 10, false);
				AddControlType("Property", 2, 22, "Acreage", "twce0000.vb1", "Right", 0.1667, 1, "", "Acres", "0.00", "Acreage", 6, "", false, "CEMaster", "", 7, false);
				AddControlType("Property", 2, 23, "Frontage", "twce0000.vb1", "Right", 0.1667, 1, "", "Frontage", "0.00", "Frontage", 7, "", false, "CEMaster", "", 7, false);
				AddControlType("Property", 2, 24, "Neighborhood", "twce0000.vb1", "Left", 0.1667, 2, "", "Neighborhood", "", "Neighborhood", 8, "", false, "CEMaster", "", 10, false);
				AddControlType("Property", 2, 25, "Zone", "twce0000.vb1", "Left", 0.1667, 2, "", "Zone", "", "Zone", 9, "", false, "CEMaster", "", 9, false);
				rsLoad.OpenRecordset("SELECT * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					while (!rsLoad.EndOfFile())
					{
						strJust = "Left";
						if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strJust = "Right";
							intType = FCConvert.ToInt32(DataTypeEnum.dbDouble);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
						{
							strJust = "Right";
							intType = FCConvert.ToInt32(DataTypeEnum.dbLong);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbBoolean);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDATE)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbDate);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPETEXT)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbText);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbText);
						}
						//end switch
						AddControlType("Property User Data", 6, 43, rsLoad.Get_Fields_String("Description"), "twce0000.vb1", strJust, 0.1667, 1, "", "", "", "", 1, "", false, "CEMaster", "", intType, true, rsLoad.Get_Fields_Int32("ID"));
						rsLoad.MoveNext();
					}
				}
				AddControlType("Permit", 3, 26, "Permit Year", "twce0000.vb1", "Right", 0.1667, 1, "", "PermitYear", "", "Permit Year", 1, "", false, "Permit", "", 4, false);
				AddControlType("Permit", 3, 27, "Permit", "twce0000.vb1", "Left", 0.1667, 1, "", "", "", "Permit", 2, "", true, "Permit", "", 10, false);
				AddControlType("Permit", 3, 52, "Applicant Name", "twce0000.vb1", "Left", 0.1667, 2, "", "FiledBy", "", "Name of filer", 3, "", false, "Permit", "", 10, false);
				AddControlType("Permit", 3, 28, "Application Date", "twce0000.vb1", "Left", 0.1667, 1, "", "ApplicationDate", "", "Application Date", 4, "", false, "Permit", "", 8, false);
				AddControlType("Permit", 3, 29, "Type", "twce0000.vb1", "Left", 0.1667, 1, "", "", "", "Type of Permit", 5, "", true, "Permit", "", 10, false);
				AddControlType("Permit", 3, 51, "Category", "twce0000.vb1", "Left", 0.1667, 1, "", "", "", "Permit Category", 6, "", true, "Permit", "", 10, false);
				AddControlType("Permit", 3, 30, "Status", "twce0000.vb1", "Left", 0.1667, 1, "", "", "", "Status of Permit", 7, "", true, "Permit", "", 10, false);
				AddControlType("Permit", 3, 31, "Description", "twce0000.vb1", "Left", 0.1667, 2, "", "Description", "", "Permit Description", 8, "", false, "Permit", "", 10, false);
				AddControlType("Permit", 3, 32, "Plan", "twce0000.vb1", "Left", 0.1667, 1, "", "Plan", "", "Plan", 9, "", false, "Permit", "", 10, false);
				AddControlType("Permit", 3, 33, "Fee", "twce0000.vb1", "Right", 0.1667, 1, "", "Fee", "#,###,#0.00", "Permit Fee", 10, "", false, "Permit", "", 7, false);
				AddControlType("Permit", 3, 40, "Permit Contractor", "twce0000.vb1", "Left", 0.1667, 2, "", "PermitContractorName", "", "Contractor Name on Permit", 11, "", false, "PermitContractors", "", 10, false);
				AddControlType("Permit", 3, 47, "Start Date", "twce0000.vb1", "Left", 0.1667, 1, "", "StartDate", "", "Start Date", 12, "", false, "Permit", "", 8, false);
				AddControlType("Permit", 3, 48, "End Date", "twce0000.vb1", "Left", 0.1667, 1, "", "CompletionDate", "", "End Date", 13, "", false, "Permit", "", 8, false);
				AddControlType("Permit", 3, 53, "Value", "twce0000.vb1", "Right", 0.1667, 1, "", "EstimatedValue", "#,###,##0", "Value", 14, "", false, "Permit", "", 7, false);
				AddControlType("Permit", 3, 54, "Contact Name", "twce0000.vb1", "Left", 0.1667, 2, "", "ContactName", "", "Contact Name", 15, "", false, "Permit", "", 10, false);
				AddControlType("Permit", 3, 55, "Contact Email", "twce0000.vb1", "Left", 0.1667, 2, "", "ContactEmail", "", "Contact Email", 16, "", false, "Permit", "", 10, false);
				AddControlType("Permit", 3, 56, "Comm/Res", "twce0000.vb1", "Left", 0.1667, 1.5, "", "CommResCode", "", "Commercial / Residential", 17, "", true, "Permit", "", 10, false);
				AddControlType("Permit", 3, 57, "Cert. of Occ. Date", "twce0000.vb1", "Left", 0.1667, 1, "", "CertificateOfOCCDate", "", "Certificate of Occupancy Date", 18, "", false, "Permit", "", 8, false);
				rsTemp.OpenRecordset("select * FROM systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!rsTemp.EndOfFile())
				{
					rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " AND SUBTYPE = " + rsTemp.Get_Fields_Int32("ID") + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
					if (!rsLoad.EndOfFile())
					{
						while (!rsLoad.EndOfFile())
						{
							strJust = "Left";
							if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL)
							{
								strJust = "Right";
								intType = FCConvert.ToInt32(DataTypeEnum.dbDouble);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
							{
								strJust = "Right";
								intType = FCConvert.ToInt32(DataTypeEnum.dbLong);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEBOOLEAN)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbBoolean);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDATE)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbDate);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPETEXT)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbText);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDROPDOWN)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbText);
							}
							AddControlType(rsTemp.Get_Fields_String("description") + " Permit User Data", 7, 44, rsLoad.Get_Fields_String("Description"), "twce0000.vb1", strJust, 0.1667, 1, "", "", "", "", 1, "", false, "CEMaster", "", intType, true, rsLoad.Get_Fields_Int32("ID"));
							rsLoad.MoveNext();
						}
					}
					rsTemp.MoveNext();
				}
				AddControlType("Inspection", 4, 34, "Type", "twce0000.vb1", "Left", 0.1667, 1, "", "InspectionType", "", "Inspection Type", 1, "", true, "Inspections", "", 10, false);
				AddControlType("Inspection", 4, 35, "Status", "twce0000.vb1", "Left", 0.1667, 1, "", "InspectionStatus", "", "Inspection Status", 2, "", true, "Inpsections", "", 10, false);
				AddControlType("Inspection", 4, 36, "Date", "twce0000.vb1", "Left", 0.1667, 1, "", "InspectionDate", "", "Inspection Date", 3, "", false, "Inspections", "", 8, false);
				AddControlType("Inspection", 4, 37, "Inspector", "twce0000.vb1", "Left", 0.1667, 2, "", "InspectorName", "", "Inspector", 4, "", false, "Inspectors", "", 10, false);
				// Call rsLoad.OpenRecordset("select * from usercodes where codetype = " & CNSTCODETYPEINSPECTION & " order by orderno", strCEDatabase)
				// If Not rsLoad.EndOfFile Then
				// Do While Not rsLoad.EndOfFile
				// strJust = "Left"
				// Select Case rsLoad.Fields("valueformat")
				// Case CNSTVALUETYPEDECIMAL
				// strJust = "Right"
				// intType = dbDouble
				// Case CNSTVALUETYPELONG
				// strJust = "Right"
				// intType = dbLong
				// Case CNSTVALUETYPEBOOLEAN
				// intType = dbBoolean
				// Case CNSTVALUETYPEDATE
				// intType = dbDate
				// Case CNSTVALUETYPETEXT
				// intType = dbText
				// Case CNSTVALUETYPEDROPDOWN
				// intType = dbText
				// End Select
				// Call AddControlType("Inspection User Data", 8, 45, rsLoad.Fields("Description"), "twce0000.vb1", strJust, 0.1667, 1, "", "", "", "", 1, "", False, "CEMaster", "", intType, True, rsLoad.Fields("ID"))
				// rsLoad.MoveNext
				// Loop
				// End If
				rsTemp.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " and not booloption = 1 order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!rsTemp.EndOfFile())
				{
					rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " and subtype = " + rsTemp.Get_Fields_Int32("ID") + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
					if (!rsLoad.EndOfFile())
					{
						while (!rsLoad.EndOfFile())
						{
							strJust = "Left";
							if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL)
							{
								strJust = "Right";
								intType = FCConvert.ToInt32(DataTypeEnum.dbDouble);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
							{
								strJust = "Right";
								intType = FCConvert.ToInt32(DataTypeEnum.dbLong);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEBOOLEAN)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbBoolean);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDATE)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbDate);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPETEXT)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbText);
							}
							else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDROPDOWN)
							{
								intType = FCConvert.ToInt32(DataTypeEnum.dbText);
							}
							AddControlType(rsTemp.Get_Fields_String("description") + " Inspection User Data", 8, 45, rsLoad.Get_Fields_String("Description"), "twce0000.vb1", strJust, 0.1667, 1, "", "", "", "", 1, "", false, "CEMaster", "", intType, true, rsLoad.Get_Fields_Int32("ID"));
							rsLoad.MoveNext();
						}
					}
					rsTemp.MoveNext();
				}
				AddControlType("Contractor", 5, 38, "Name", "twce0000.vb1", "Left", 0.1667, 2, "", "ContractorName1", "", "Contractor Name", 1, "", false, "Contractors", "", 10, false);
				AddControlType("Contractor", 5, 39, "Name 2", "twce0000.vb1", "Left", 0.1667, 2, "", "ContractorName2", "", "Contractor Name 2", 2, "", false, "Contractors", "", 10, false);
				rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					while (!rsLoad.EndOfFile())
					{
						strJust = "Left";
						if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strJust = "Right";
							intType = FCConvert.ToInt32(DataTypeEnum.dbDouble);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
						{
							strJust = "Right";
							intType = FCConvert.ToInt32(DataTypeEnum.dbLong);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbBoolean);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDATE)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbDate);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPETEXT)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbText);
						}
						else if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							intType = FCConvert.ToInt32(DataTypeEnum.dbText);
						}
						AddControlType("Contractor User Data", 9, 46, rsLoad.Get_Fields_String("Description"), "twce0000.vb1", strJust, 0.1667, 1, "", "", "", "", 1, "", false, "CEMaster", "", intType, true, rsLoad.Get_Fields_Int32("ID"));
						rsLoad.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadControlTypes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddControlType(string strCategory, int intCategoryNumber, int lngCode, string strName, string strDBName, string strAlign, double dblHeight, double dblWidth, string strEParams, string strFieldName, string strFormat, string strHelp, int intOrder, string strParamsTip, bool boolSpecial, string strTable, string strToolTip, int intDataType, bool boolUserDefined, int lngOpenID = 0)
		{
			clsCustomControlParameters tcp = new clsCustomControlParameters();
			int intAlign = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (fecherFoundation.Strings.LCase(strAlign) == "right")
				{
					intAlign = 1;
				}
				else if (fecherFoundation.Strings.LCase(strAlign) == "center")
				{
					intAlign = 2;
				}
				else
				{
					// left
					intAlign = 0;
				}
				tcp.Category = strCategory;
				tcp.CategoryNumber = intCategoryNumber;
				tcp.Code = lngCode;
				tcp.ControlName = strName;
				tcp.DBName = strDBName;
				tcp.DefaultAlign = FCConvert.ToString(intAlign);
				tcp.DefaultHeight = dblHeight;
				tcp.DefaultWidth = dblWidth;
				tcp.ExtraParameters = strEParams;
				tcp.FieldID = lngOpenID;
				tcp.FieldName = strFieldName;
				tcp.FormatString = strFormat;
				tcp.HelpString = strHelp;
				tcp.OrderNumber = intOrder;
				tcp.ParametersToolTip = strParamsTip;
				tcp.SpecialCase = boolSpecial;
				tcp.TableName = strTable;
				tcp.ToolTipString = strToolTip;
				tcp.TypeOfData = intDataType;
				tcp.UserDefined = boolUserDefined;
				InsertControlType(ref tcp);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddControlType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void InsertControlType(ref clsCustomControlParameters ccParam)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				ControlList.Add(ccParam, "Code" + ccParam.Code + "ID" + ccParam.FieldID);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertControlType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public clsCustomPrintForm() : base()
		{
			intCurrentIndex = -1;
		}

		public int MoveFirst()
		{
			int MoveFirst = 0;
			int intReturn;
			intCurrentIndex = 0;
			intReturn = MoveNext();
			MoveFirst = intReturn;
			return MoveFirst;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(ControlList))
			{
				if (ControlList.Count >= intCurrentIndex + 1)
				{
					while (intCurrentIndex <= ControlList.Count)
					{
						intCurrentIndex += 1;
						if (!(ControlList[intCurrentIndex] == null))
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
					if (intCurrentIndex > ControlList.Count)
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MoveNext = intReturn;
			return MoveNext;
		}

		public int MovePrevious()
		{
			int MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(ControlList))
			{
				if (ControlList.Count > 0)
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex > 0)
						{
							if (!(ControlList[intCurrentIndex] == null))
							{
								intReturn = intCurrentIndex;
							}
						}
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MovePrevious = intReturn;
			return MovePrevious;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			if (!FCUtils.IsEmpty(ControlList))
			{
				if (intCurrentIndex > ControlList.Count)
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
			GetCurrentIndex = intCurrentIndex;
			return GetCurrentIndex;
		}

		public clsCustomControlParameters GetCurrentControl()
		{
			clsCustomControlParameters GetCurrentControl = null;
			clsCustomControlParameters tcp;
			tcp = null;
			if (!FCUtils.IsEmpty(ControlList))
			{
				if (intCurrentIndex > 0 && intCurrentIndex <= ControlList.Count)
				{
					tcp = ControlList[intCurrentIndex];
				}
			}
			GetCurrentControl = tcp;
			return GetCurrentControl;
		}

		public clsCustomControlParameters GetControlByIndex(ref int intIndex)
		{
			clsCustomControlParameters GetControlByIndex = null;
			clsCustomControlParameters tcp;
			tcp = null;
			if (!FCUtils.IsEmpty(ControlList))
			{
				if (ControlList.Count >= intIndex)
				{
					tcp = ControlList[intIndex];
				}
			}
			GetControlByIndex = tcp;
			return GetControlByIndex;
		}

		public clsCustomControlParameters GetControlByCode(int lngCode, int lngFieldID)
		{
			clsCustomControlParameters GetControlByCode = null;
			clsCustomControlParameters tcp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				tcp = null;
				if (!FCUtils.IsEmpty(ControlList))
				{
                    //FC:FINAL:AM:#1967 - improve performance
                    //if (ControlList.Count > 0)
                    string key = "Code" + lngCode + "ID" + lngFieldID;
                    if (ControlList.Count > 0 && ControlList.Contains(key))
                    {
						tcp = (clsCustomControlParameters)ControlList[key];
					}
				}
				GetControlByCode = tcp;
				return GetControlByCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				tcp = null;
				GetControlByCode = tcp;
			}
			return GetControlByCode;
		}
	}
}
