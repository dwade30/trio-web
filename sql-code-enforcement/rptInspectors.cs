﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptInspectors.
	/// </summary>
	public partial class rptInspectors : BaseSectionReport
	{
		public rptInspectors()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptInspectors InstancePtr
		{
			get
			{
				return (rptInspectors)Sys.GetInstance(typeof(rptInspectors));
			}
		}

		protected rptInspectors _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInspectors	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLSHORTNAME = 2;
		int lngRow;

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Inspectors");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = lngRow >= frmInspectors.InstancePtr.Grid.Rows;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			lngRow = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmInspectors.InstancePtr.Grid.Rows)
			{
				txtName.Text = frmInspectors.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME);
				txtShort.Text = frmInspectors.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTNAME);
				lngRow += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
