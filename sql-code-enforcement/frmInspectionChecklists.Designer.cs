//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmInspectionChecklists.
	/// </summary>
	partial class frmInspectionChecklists
	{
		public fecherFoundation.FCComboBox cmbPermitType;
		public FCGrid Grid;
		public FCGrid GridDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddItem;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbPermitType = new fecherFoundation.FCComboBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.GridDelete = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddItem = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddItem = new fecherFoundation.FCButton();
            this.cmdDeleteItem = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteItem)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1023, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbPermitType);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.GridDelete);
            this.ClientArea.Size = new System.Drawing.Size(1023, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddItem);
            this.TopPanel.Controls.Add(this.cmdDeleteItem);
            this.TopPanel.Size = new System.Drawing.Size(1023, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteItem, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddItem, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(203, 30);
            this.HeaderText.Text = "Permit Checklists";
            // 
            // cmbPermitType
            // 
            this.cmbPermitType.AutoSize = false;
            this.cmbPermitType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPermitType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPermitType.FormattingEnabled = true;
            this.cmbPermitType.Location = new System.Drawing.Point(30, 30);
            this.cmbPermitType.Name = "cmbPermitType";
            this.cmbPermitType.Size = new System.Drawing.Size(203, 40);
            this.cmbPermitType.TabIndex = 0;
            this.cmbPermitType.SelectedIndexChanged += new System.EventHandler(this.cmbPermitType_SelectedIndexChanged);
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 5;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
            this.Grid.ExtendLastCol = true;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 90);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(915, 440);
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 1;
            this.Grid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEdit);
            this.Grid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.Grid_MouseDownEvent);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // GridDelete
            // 
            this.GridDelete.AllowSelection = false;
            this.GridDelete.AllowUserToResizeColumns = false;
            this.GridDelete.AllowUserToResizeRows = false;
            this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
            this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
            this.GridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridDelete.Cols = 1;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridDelete.ColumnHeadersHeight = 30;
            this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridDelete.ColumnHeadersVisible = false;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridDelete.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridDelete.DragIcon = null;
            this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridDelete.FixedCols = 0;
            this.GridDelete.FixedRows = 0;
            this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.FrozenCols = 0;
            this.GridDelete.GridColor = System.Drawing.Color.Empty;
            this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.Location = new System.Drawing.Point(24, 256);
            this.GridDelete.Name = "GridDelete";
            this.GridDelete.OutlineCol = 0;
            this.GridDelete.ReadOnly = true;
            this.GridDelete.RowHeadersVisible = false;
            this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridDelete.RowHeightMin = 0;
            this.GridDelete.Rows = 0;
            this.GridDelete.ScrollTipText = null;
            this.GridDelete.ShowColumnVisibilityMenu = false;
            this.GridDelete.ShowFocusCell = false;
            this.GridDelete.Size = new System.Drawing.Size(17, 26);
            this.GridDelete.StandardTab = true;
            this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridDelete.TabIndex = 2;
            this.GridDelete.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddItem,
            this.mnuDelete,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddItem
            // 
            this.mnuAddItem.Index = 0;
            this.mnuAddItem.Name = "mnuAddItem";
            this.mnuAddItem.Text = "Add Item";
            this.mnuAddItem.Click += new System.EventHandler(this.mnuAddItem_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Item";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 2;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 3;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 4;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 5;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 6;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(479, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddItem
            // 
            this.cmdAddItem.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddItem.AppearanceKey = "toolbarButton";
            this.cmdAddItem.Location = new System.Drawing.Point(829, 29);
            this.cmdAddItem.Name = "cmdAddItem";
            this.cmdAddItem.Size = new System.Drawing.Size(72, 24);
            this.cmdAddItem.TabIndex = 1;
            this.cmdAddItem.Text = "Add Item";
            this.cmdAddItem.Click += new System.EventHandler(this.mnuAddItem_Click);
            // 
            // cmdDeleteItem
            // 
            this.cmdDeleteItem.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteItem.AppearanceKey = "toolbarButton";
            this.cmdDeleteItem.Location = new System.Drawing.Point(907, 29);
            this.cmdDeleteItem.Name = "cmdDeleteItem";
            this.cmdDeleteItem.Size = new System.Drawing.Size(88, 24);
            this.cmdDeleteItem.TabIndex = 2;
            this.cmdDeleteItem.Text = "Delete Item";
            this.cmdDeleteItem.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // frmInspectionChecklists
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1023, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmInspectionChecklists";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Permit Checklists";
            this.Load += new System.EventHandler(this.frmInspectionChecklists_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmInspectionChecklists_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteItem)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdDeleteItem;
		private FCButton cmdAddItem;
	}
}