//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmEditRecurrence.
	/// </summary>
	partial class frmEditRecurrence
	{
		public fecherFoundation.FCComboBox cmbPatternNoEnd;
		public fecherFoundation.FCComboBox cmbYearlyDay;
		public fecherFoundation.FCComboBox cmbMonthlyDay_1;
		public fecherFoundation.FCComboBox cmbDailyEveryNDays;
		public fecherFoundation.FCLabel lblDailyEveryNDays;
		public fecherFoundation.FCComboBox cmbRecYearly;
		public fecherFoundation.FCLabel lblRecYearly;
		public fecherFoundation.FCButton btnRemoveRecurrence;
		public fecherFoundation.FCButton btnCancel;
		public fecherFoundation.FCButton btnOK;
		public fecherFoundation.FCFrame frameRangeOfRecurrence;
		public fecherFoundation.FCTextBox txtPatternEndAfter;
		public fecherFoundation.FCComboBox ddPatternEndDate;
		public fecherFoundation.FCComboBox ddPatternStartDate;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame frameRecurrencePattern;
		public fecherFoundation.FCFrame pageYearly;
		public fecherFoundation.FCComboBox cmdYearlyThePartOfWeek;
		public fecherFoundation.FCComboBox cmbYearlyTheDay;
		public fecherFoundation.FCComboBox cmbYearlyEveryDate;
		public fecherFoundation.FCComboBox cmbYearlyDate;
		public fecherFoundation.FCComboBox cmbYearlyTheMonth;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCFrame pageMonthly;
		public fecherFoundation.FCTextBox txtMonthlyEveryMonth;
		public fecherFoundation.FCComboBox cmbMonthlyDayOfMonth;
		public fecherFoundation.FCComboBox cmbMonthlyDayOfWeek;
		public fecherFoundation.FCComboBox cmbMonthlyDay;
		public fecherFoundation.FCTextBox txtMonthlyOfEveryTheMonths;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCFrame pageWeekly;
		public fecherFoundation.FCCheckBox chkWeeklySunday;
		public fecherFoundation.FCCheckBox chkWeeklySaturday;
		public fecherFoundation.FCCheckBox chkWeeklyFriday;
		public fecherFoundation.FCCheckBox chkWeeklyThursday;
		public fecherFoundation.FCCheckBox chkWeeklyWednesday;
		public fecherFoundation.FCCheckBox chkWeeklyTusday;
		public fecherFoundation.FCCheckBox chkWeeklyMonday;
		public fecherFoundation.FCTextBox txtWeeklyNWeeks;
		public fecherFoundation.FCLabel Recurr;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame pageDaily;
		public fecherFoundation.FCTextBox txtDailyEveryNdays;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCFrame frameApointmentTime;
		public fecherFoundation.FCTextBox txtMinutes;
		public fecherFoundation.FCTextBox txtHours;
		public fecherFoundation.FCTextBox txtDays;
		public FCDateTimePicker dtpEventStart;
		//public AxXtremeSuiteControls.AxUpDown UpDownDays;
		//public AxXtremeSuiteControls.AxUpDown UpDownHours;
		//public AxXtremeSuiteControls.AxUpDown UpDownMinutes;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel txtEventEndTime;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCButton cmdSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbPatternNoEnd = new fecherFoundation.FCComboBox();
            this.cmbYearlyDay = new fecherFoundation.FCComboBox();
            this.cmbMonthlyDay_1 = new fecherFoundation.FCComboBox();
            this.cmbDailyEveryNDays = new fecherFoundation.FCComboBox();
            this.lblDailyEveryNDays = new fecherFoundation.FCLabel();
            this.cmbRecYearly = new fecherFoundation.FCComboBox();
            this.lblRecYearly = new fecherFoundation.FCLabel();
            this.btnRemoveRecurrence = new fecherFoundation.FCButton();
            this.btnCancel = new fecherFoundation.FCButton();
            this.btnOK = new fecherFoundation.FCButton();
            this.frameRangeOfRecurrence = new fecherFoundation.FCFrame();
            this.txtPatternEndAfter = new fecherFoundation.FCTextBox();
            this.ddPatternStartDate = new fecherFoundation.FCComboBox();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.ddPatternEndDate = new fecherFoundation.FCComboBox();
            this.frameRecurrencePattern = new fecherFoundation.FCFrame();
            this.pageDaily = new fecherFoundation.FCFrame();
            this.txtDailyEveryNdays = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.pageWeekly = new fecherFoundation.FCFrame();
            this.chkWeeklySunday = new fecherFoundation.FCCheckBox();
            this.chkWeeklySaturday = new fecherFoundation.FCCheckBox();
            this.chkWeeklyFriday = new fecherFoundation.FCCheckBox();
            this.chkWeeklyThursday = new fecherFoundation.FCCheckBox();
            this.chkWeeklyWednesday = new fecherFoundation.FCCheckBox();
            this.chkWeeklyTusday = new fecherFoundation.FCCheckBox();
            this.chkWeeklyMonday = new fecherFoundation.FCCheckBox();
            this.txtWeeklyNWeeks = new fecherFoundation.FCTextBox();
            this.Recurr = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.pageMonthly = new fecherFoundation.FCFrame();
            this.txtMonthlyEveryMonth = new fecherFoundation.FCTextBox();
            this.cmbMonthlyDayOfMonth = new fecherFoundation.FCComboBox();
            this.cmbMonthlyDayOfWeek = new fecherFoundation.FCComboBox();
            this.cmbMonthlyDay = new fecherFoundation.FCComboBox();
            this.txtMonthlyOfEveryTheMonths = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.pageYearly = new fecherFoundation.FCFrame();
            this.cmdYearlyThePartOfWeek = new fecherFoundation.FCComboBox();
            this.cmbYearlyTheDay = new fecherFoundation.FCComboBox();
            this.cmbYearlyEveryDate = new fecherFoundation.FCComboBox();
            this.cmbYearlyDate = new fecherFoundation.FCComboBox();
            this.cmbYearlyTheMonth = new fecherFoundation.FCComboBox();
            this.Label12 = new fecherFoundation.FCLabel();
            this.frameApointmentTime = new fecherFoundation.FCFrame();
            this.txtMinutes = new fecherFoundation.FCTextBox();
            this.txtHours = new fecherFoundation.FCTextBox();
            this.txtDays = new fecherFoundation.FCTextBox();
            this.dtpEventStart = new fecherFoundation.FCDateTimePicker();
            this.Label2 = new fecherFoundation.FCLabel();
            this.txtEventEndTime = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveExit = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveRecurrence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameRangeOfRecurrence)).BeginInit();
            this.frameRangeOfRecurrence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frameRecurrencePattern)).BeginInit();
            this.frameRecurrencePattern.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageDaily)).BeginInit();
            this.pageDaily.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageWeekly)).BeginInit();
            this.pageWeekly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklySunday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklySaturday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyFriday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyThursday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyWednesday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyTusday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyMonday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageMonthly)).BeginInit();
            this.pageMonthly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageYearly)).BeginInit();
            this.pageYearly.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frameApointmentTime)).BeginInit();
            this.frameApointmentTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 576);
            this.BottomPanel.Size = new System.Drawing.Size(1072, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnRemoveRecurrence);
            this.ClientArea.Controls.Add(this.btnCancel);
            this.ClientArea.Controls.Add(this.btnOK);
            this.ClientArea.Controls.Add(this.frameRangeOfRecurrence);
            this.ClientArea.Controls.Add(this.frameRecurrencePattern);
            this.ClientArea.Controls.Add(this.frameApointmentTime);
            this.ClientArea.Size = new System.Drawing.Size(1072, 516);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1072, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(189, 30);
            this.HeaderText.Text = "Edit Recurrence";
            // 
            // cmbPatternNoEnd
            // 
            this.cmbPatternNoEnd.AutoSize = false;
            this.cmbPatternNoEnd.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPatternNoEnd.FormattingEnabled = true;
            this.cmbPatternNoEnd.Items.AddRange(new object[] {
            "No End Date",
            "End After:",
            "End By:"});
            this.cmbPatternNoEnd.Location = new System.Drawing.Point(255, 30);
            this.cmbPatternNoEnd.Name = "cmbPatternNoEnd";
            this.cmbPatternNoEnd.Size = new System.Drawing.Size(147, 40);
            this.cmbPatternNoEnd.TabIndex = 60;
            this.cmbPatternNoEnd.SelectedIndexChanged += cmbPatternNoEnd_SelectedIndexChanged;
            // 
            // cmbYearlyDay
            // 
            this.cmbYearlyDay.AutoSize = false;
            this.cmbYearlyDay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYearlyDay.FormattingEnabled = true;
            this.cmbYearlyDay.Items.AddRange(new object[] {
            "Day",
            "The"});
            this.cmbYearlyDay.Location = new System.Drawing.Point(20, 30);
            this.cmbYearlyDay.Name = "cmbYearlyDay";
            this.cmbYearlyDay.Size = new System.Drawing.Size(147, 40);
            this.cmbYearlyDay.TabIndex = 23;
            // 
            // cmbMonthlyDay_1
            // 
            this.cmbMonthlyDay_1.AutoSize = false;
            this.cmbMonthlyDay_1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMonthlyDay_1.FormattingEnabled = true;
            this.cmbMonthlyDay_1.Items.AddRange(new object[] {
            "Day",
            "The"});
            this.cmbMonthlyDay_1.Location = new System.Drawing.Point(20, 30);
            this.cmbMonthlyDay_1.Name = "cmbMonthlyDay_1";
            this.cmbMonthlyDay_1.Size = new System.Drawing.Size(147, 40);
            this.cmbMonthlyDay_1.TabIndex = 32;
            // 
            // cmbDailyEveryNDays
            // 
            this.cmbDailyEveryNDays.AutoSize = false;
            this.cmbDailyEveryNDays.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbDailyEveryNDays.FormattingEnabled = true;
            this.cmbDailyEveryNDays.Items.AddRange(new object[] {
            "Every",
            "Every Work Day"});
            this.cmbDailyEveryNDays.Location = new System.Drawing.Point(81, 30);
            this.cmbDailyEveryNDays.Name = "cmbDailyEveryNDays";
            this.cmbDailyEveryNDays.Size = new System.Drawing.Size(147, 40);
            this.cmbDailyEveryNDays.TabIndex = 40;
            // 
            // lblDailyEveryNDays
            // 
            this.lblDailyEveryNDays.AutoSize = true;
            this.lblDailyEveryNDays.Location = new System.Drawing.Point(20, 44);
            this.lblDailyEveryNDays.Name = "lblDailyEveryNDays";
            this.lblDailyEveryNDays.Size = new System.Drawing.Size(33, 15);
            this.lblDailyEveryNDays.TabIndex = 41;
            this.lblDailyEveryNDays.Text = "DAY";
            // 
            // cmbRecYearly
            // 
            this.cmbRecYearly.AutoSize = false;
            this.cmbRecYearly.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRecYearly.FormattingEnabled = true;
            this.cmbRecYearly.Items.AddRange(new object[] {
            "Yearly",
            "Monthly",
            "Weekly",
            "Daily"});
            this.cmbRecYearly.Location = new System.Drawing.Point(104, 30);
            this.cmbRecYearly.Name = "cmbRecYearly";
            this.cmbRecYearly.Size = new System.Drawing.Size(147, 40);
            this.cmbRecYearly.TabIndex = 16;
            this.cmbRecYearly.SelectedIndexChanged += new System.EventHandler(this.cmbRecYearly_SelectedIndexChanged);
            // 
            // lblRecYearly
            // 
            this.lblRecYearly.AutoSize = true;
            this.lblRecYearly.Location = new System.Drawing.Point(20, 44);
            this.lblRecYearly.Name = "lblRecYearly";
            this.lblRecYearly.Size = new System.Drawing.Size(65, 15);
            this.lblRecYearly.TabIndex = 17;
            this.lblRecYearly.Text = "PATTERN";
            // 
            // btnRemoveRecurrence
            // 
            this.btnRemoveRecurrence.AppearanceKey = "actionButton";
            this.btnRemoveRecurrence.Location = new System.Drawing.Point(230, 552);
            this.btnRemoveRecurrence.Name = "btnRemoveRecurrence";
            this.btnRemoveRecurrence.Size = new System.Drawing.Size(200, 40);
            this.btnRemoveRecurrence.TabIndex = 63;
            this.btnRemoveRecurrence.Text = "Remove Recurrence";
            this.btnRemoveRecurrence.Click += new System.EventHandler(this.btnRemoveRecurrence_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.Location = new System.Drawing.Point(115, 552);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(95, 40);
            this.btnCancel.TabIndex = 62;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.AppearanceKey = "actionButton";
            this.btnOK.Location = new System.Drawing.Point(30, 552);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(65, 40);
            this.btnOK.TabIndex = 61;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // frameRangeOfRecurrence
            // 
            this.frameRangeOfRecurrence.Controls.Add(this.txtPatternEndAfter);
            this.frameRangeOfRecurrence.Controls.Add(this.cmbPatternNoEnd);
            this.frameRangeOfRecurrence.Controls.Add(this.ddPatternStartDate);
            this.frameRangeOfRecurrence.Controls.Add(this.Label7);
            this.frameRangeOfRecurrence.Controls.Add(this.Label3);
            this.frameRangeOfRecurrence.Controls.Add(this.ddPatternEndDate);
            this.frameRangeOfRecurrence.Location = new System.Drawing.Point(30, 442);
            this.frameRangeOfRecurrence.Name = "frameRangeOfRecurrence";
            this.frameRangeOfRecurrence.Size = new System.Drawing.Size(601, 90);
            this.frameRangeOfRecurrence.TabIndex = 52;
            this.frameRangeOfRecurrence.Text = "Range Of Recurrence";
            // 
            // txtPatternEndAfter
            // 
            this.txtPatternEndAfter.AutoSize = false;
            this.txtPatternEndAfter.BackColor = System.Drawing.SystemColors.Window;
            this.txtPatternEndAfter.LinkItem = null;
            this.txtPatternEndAfter.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtPatternEndAfter.LinkTopic = null;
            this.txtPatternEndAfter.Location = new System.Drawing.Point(422, 30);
            this.txtPatternEndAfter.Name = "txtPatternEndAfter";
            this.txtPatternEndAfter.Size = new System.Drawing.Size(46, 40);
            this.txtPatternEndAfter.TabIndex = 59;
            this.txtPatternEndAfter.Text = "10";
            // 
            // ddPatternStartDate
            // 
            this.ddPatternStartDate.AutoSize = false;
            this.ddPatternStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.ddPatternStartDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.ddPatternStartDate.FormattingEnabled = true;
            this.ddPatternStartDate.Location = new System.Drawing.Point(88, 30);
            this.ddPatternStartDate.Name = "ddPatternStartDate";
            this.ddPatternStartDate.Size = new System.Drawing.Size(147, 40);
            this.ddPatternStartDate.TabIndex = 53;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(502, 44);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(90, 17);
            this.Label7.TabIndex = 60;
            this.Label7.Text = "OCURRENCES";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(40, 18);
            this.Label3.TabIndex = 54;
            this.Label3.Text = "START";
            // 
            // ddPatternEndDate
            // 
            this.ddPatternEndDate.AutoSize = false;
            this.ddPatternEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.ddPatternEndDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.ddPatternEndDate.FormattingEnabled = true;
            this.ddPatternEndDate.Location = new System.Drawing.Point(422, 30);
            this.ddPatternEndDate.Name = "ddPatternEndDate";
            this.ddPatternEndDate.Size = new System.Drawing.Size(110, 40);
            this.ddPatternEndDate.TabIndex = 58;
            // 
            // frameRecurrencePattern
            // 
            this.frameRecurrencePattern.AppearanceKey = "groupBoxLeftBorder";
            this.frameRecurrencePattern.Controls.Add(this.cmbRecYearly);
            this.frameRecurrencePattern.Controls.Add(this.lblRecYearly);
            this.frameRecurrencePattern.Controls.Add(this.pageDaily);
            this.frameRecurrencePattern.Controls.Add(this.pageWeekly);
            this.frameRecurrencePattern.Controls.Add(this.pageMonthly);
            this.frameRecurrencePattern.Controls.Add(this.pageYearly);
            this.frameRecurrencePattern.Location = new System.Drawing.Point(30, 140);
            this.frameRecurrencePattern.Name = "frameRecurrencePattern";
            this.frameRecurrencePattern.Size = new System.Drawing.Size(641, 282);
            this.frameRecurrencePattern.TabIndex = 10;
            this.frameRecurrencePattern.Text = "Recurrence Pattern";
            // 
            // pageDaily
            // 
            this.pageDaily.Controls.Add(this.txtDailyEveryNdays);
            this.pageDaily.Controls.Add(this.cmbDailyEveryNDays);
            this.pageDaily.Controls.Add(this.lblDailyEveryNDays);
            this.pageDaily.Controls.Add(this.Label5);
            this.pageDaily.Location = new System.Drawing.Point(20, 90);
            this.pageDaily.Name = "pageDaily";
            this.pageDaily.Size = new System.Drawing.Size(367, 90);
            this.pageDaily.TabIndex = 36;
            this.pageDaily.Text = "Daily";
            // 
            // txtDailyEveryNdays
            // 
            this.txtDailyEveryNdays.AutoSize = false;
            this.txtDailyEveryNdays.BackColor = System.Drawing.SystemColors.Window;
            this.txtDailyEveryNdays.LinkItem = null;
            this.txtDailyEveryNdays.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDailyEveryNdays.LinkTopic = null;
            this.txtDailyEveryNdays.Location = new System.Drawing.Point(250, 30);
            this.txtDailyEveryNdays.Name = "txtDailyEveryNdays";
            this.txtDailyEveryNdays.Size = new System.Drawing.Size(41, 40);
            this.txtDailyEveryNdays.TabIndex = 39;
            this.txtDailyEveryNdays.Text = "1";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(310, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 17);
            this.Label5.TabIndex = 40;
            this.Label5.Text = "DAY(S)";
            // 
            // pageWeekly
            // 
            this.pageWeekly.Controls.Add(this.chkWeeklySunday);
            this.pageWeekly.Controls.Add(this.chkWeeklySaturday);
            this.pageWeekly.Controls.Add(this.chkWeeklyFriday);
            this.pageWeekly.Controls.Add(this.chkWeeklyThursday);
            this.pageWeekly.Controls.Add(this.chkWeeklyWednesday);
            this.pageWeekly.Controls.Add(this.chkWeeklyTusday);
            this.pageWeekly.Controls.Add(this.chkWeeklyMonday);
            this.pageWeekly.Controls.Add(this.txtWeeklyNWeeks);
            this.pageWeekly.Controls.Add(this.Recurr);
            this.pageWeekly.Controls.Add(this.Label6);
            this.pageWeekly.Location = new System.Drawing.Point(20, 90);
            this.pageWeekly.Name = "pageWeekly";
            this.pageWeekly.Size = new System.Drawing.Size(473, 162);
            this.pageWeekly.TabIndex = 41;
            this.pageWeekly.Text = "Weekly";
            // 
            // chkWeeklySunday
            // 
            this.chkWeeklySunday.Location = new System.Drawing.Point(233, 121);
            this.chkWeeklySunday.Name = "chkWeeklySunday";
            this.chkWeeklySunday.Size = new System.Drawing.Size(83, 27);
            this.chkWeeklySunday.TabIndex = 51;
            this.chkWeeklySunday.Text = "Sunday";
            // 
            // chkWeeklySaturday
            // 
            this.chkWeeklySaturday.Location = new System.Drawing.Point(124, 121);
            this.chkWeeklySaturday.Name = "chkWeeklySaturday";
            this.chkWeeklySaturday.Size = new System.Drawing.Size(93, 27);
            this.chkWeeklySaturday.TabIndex = 50;
            this.chkWeeklySaturday.Text = "Saturday";
            // 
            // chkWeeklyFriday
            // 
            this.chkWeeklyFriday.Location = new System.Drawing.Point(20, 121);
            this.chkWeeklyFriday.Name = "chkWeeklyFriday";
            this.chkWeeklyFriday.Size = new System.Drawing.Size(73, 27);
            this.chkWeeklyFriday.TabIndex = 49;
            this.chkWeeklyFriday.Text = "Friday";
            // 
            // chkWeeklyThursday
            // 
            this.chkWeeklyThursday.Location = new System.Drawing.Point(364, 80);
            this.chkWeeklyThursday.Name = "chkWeeklyThursday";
            this.chkWeeklyThursday.Size = new System.Drawing.Size(96, 27);
            this.chkWeeklyThursday.TabIndex = 48;
            this.chkWeeklyThursday.Text = "Thursday";
            // 
            // chkWeeklyWednesday
            // 
            this.chkWeeklyWednesday.Location = new System.Drawing.Point(233, 80);
            this.chkWeeklyWednesday.Name = "chkWeeklyWednesday";
            this.chkWeeklyWednesday.Size = new System.Drawing.Size(114, 27);
            this.chkWeeklyWednesday.TabIndex = 47;
            this.chkWeeklyWednesday.Text = "Wednesday";
            // 
            // chkWeeklyTusday
            // 
            this.chkWeeklyTusday.Location = new System.Drawing.Point(124, 80);
            this.chkWeeklyTusday.Name = "chkWeeklyTusday";
            this.chkWeeklyTusday.Size = new System.Drawing.Size(90, 27);
            this.chkWeeklyTusday.TabIndex = 46;
            this.chkWeeklyTusday.Text = "Tuesday";
            // 
            // chkWeeklyMonday
            // 
            this.chkWeeklyMonday.Checked = true;
            this.chkWeeklyMonday.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkWeeklyMonday.Location = new System.Drawing.Point(20, 80);
            this.chkWeeklyMonday.Name = "chkWeeklyMonday";
            this.chkWeeklyMonday.Size = new System.Drawing.Size(85, 27);
            this.chkWeeklyMonday.TabIndex = 45;
            this.chkWeeklyMonday.Text = "Monday";
            // 
            // txtWeeklyNWeeks
            // 
            this.txtWeeklyNWeeks.AutoSize = false;
            this.txtWeeklyNWeeks.BackColor = System.Drawing.SystemColors.Window;
            this.txtWeeklyNWeeks.LinkItem = null;
            this.txtWeeklyNWeeks.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtWeeklyNWeeks.LinkTopic = null;
            this.txtWeeklyNWeeks.Location = new System.Drawing.Point(133, 30);
            this.txtWeeklyNWeeks.Name = "txtWeeklyNWeeks";
            this.txtWeeklyNWeeks.Size = new System.Drawing.Size(54, 40);
            this.txtWeeklyNWeeks.TabIndex = 42;
            this.txtWeeklyNWeeks.Text = "1";
            // 
            // Recurr
            // 
            this.Recurr.Location = new System.Drawing.Point(20, 44);
            this.Recurr.Name = "Recurr";
            this.Recurr.Size = new System.Drawing.Size(90, 17);
            this.Recurr.TabIndex = 44;
            this.Recurr.Text = "RECUR EVERY";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(205, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(80, 18);
            this.Label6.TabIndex = 43;
            this.Label6.Text = "WEEK(S) ON";
            // 
            // pageMonthly
            // 
            this.pageMonthly.Controls.Add(this.txtMonthlyEveryMonth);
            this.pageMonthly.Controls.Add(this.cmbMonthlyDay_1);
            this.pageMonthly.Controls.Add(this.cmbMonthlyDayOfMonth);
            this.pageMonthly.Controls.Add(this.cmbMonthlyDayOfWeek);
            this.pageMonthly.Controls.Add(this.cmbMonthlyDay);
            this.pageMonthly.Controls.Add(this.txtMonthlyOfEveryTheMonths);
            this.pageMonthly.Controls.Add(this.Label8);
            this.pageMonthly.Controls.Add(this.Label9);
            this.pageMonthly.Controls.Add(this.Label10);
            this.pageMonthly.Controls.Add(this.Label11);
            this.pageMonthly.Location = new System.Drawing.Point(20, 90);
            this.pageMonthly.Name = "pageMonthly";
            this.pageMonthly.Size = new System.Drawing.Size(591, 139);
            this.pageMonthly.TabIndex = 24;
            this.pageMonthly.Text = "Monthly";
            // 
            // txtMonthlyEveryMonth
            // 
			this.txtMonthlyEveryMonth.MaxLength = 3;
            this.txtMonthlyEveryMonth.AutoSize = false;
            this.txtMonthlyEveryMonth.BackColor = System.Drawing.SystemColors.Window;
            this.txtMonthlyEveryMonth.LinkItem = null;
            this.txtMonthlyEveryMonth.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMonthlyEveryMonth.LinkTopic = null;
            this.txtMonthlyEveryMonth.Location = new System.Drawing.Point(455, 30);
            this.txtMonthlyEveryMonth.Name = "txtMonthlyEveryMonth";
            this.txtMonthlyEveryMonth.Size = new System.Drawing.Size(41, 40);
            this.txtMonthlyEveryMonth.TabIndex = 31;
            this.txtMonthlyEveryMonth.Text = "1";
            // 
            // cmbMonthlyDayOfMonth
            // 
            this.cmbMonthlyDayOfMonth.AutoSize = false;
            this.cmbMonthlyDayOfMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMonthlyDayOfMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMonthlyDayOfMonth.FormattingEnabled = true;
            this.cmbMonthlyDayOfMonth.Location = new System.Drawing.Point(186, 30);
            this.cmbMonthlyDayOfMonth.Name = "cmbMonthlyDayOfMonth";
            this.cmbMonthlyDayOfMonth.Size = new System.Drawing.Size(147, 40);
            this.cmbMonthlyDayOfMonth.TabIndex = 30;
            // 
            // cmbMonthlyDayOfWeek
            // 
            this.cmbMonthlyDayOfWeek.AutoSize = false;
            this.cmbMonthlyDayOfWeek.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMonthlyDayOfWeek.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMonthlyDayOfWeek.FormattingEnabled = true;
            this.cmbMonthlyDayOfWeek.Location = new System.Drawing.Point(186, 80);
            this.cmbMonthlyDayOfWeek.Name = "cmbMonthlyDayOfWeek";
            this.cmbMonthlyDayOfWeek.Size = new System.Drawing.Size(147, 40);
            this.cmbMonthlyDayOfWeek.TabIndex = 29;
            // 
            // cmbMonthlyDay
            // 
            this.cmbMonthlyDay.AutoSize = false;
            this.cmbMonthlyDay.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMonthlyDay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMonthlyDay.FormattingEnabled = true;
            this.cmbMonthlyDay.Location = new System.Drawing.Point(20, 80);
            this.cmbMonthlyDay.Name = "cmbMonthlyDay";
            this.cmbMonthlyDay.Size = new System.Drawing.Size(147, 40);
            this.cmbMonthlyDay.TabIndex = 28;
            // 
            // txtMonthlyOfEveryTheMonths
            // 
            this.txtMonthlyOfEveryTheMonths.AutoSize = false;
            this.txtMonthlyOfEveryTheMonths.BackColor = System.Drawing.SystemColors.Window;
            this.txtMonthlyOfEveryTheMonths.LinkItem = null;
            this.txtMonthlyOfEveryTheMonths.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMonthlyOfEveryTheMonths.LinkTopic = null;
            this.txtMonthlyOfEveryTheMonths.Location = new System.Drawing.Point(455, 80);
            this.txtMonthlyOfEveryTheMonths.Name = "txtMonthlyOfEveryTheMonths";
            this.txtMonthlyOfEveryTheMonths.Size = new System.Drawing.Size(41, 40);
            this.txtMonthlyOfEveryTheMonths.TabIndex = 27;
            this.txtMonthlyOfEveryTheMonths.Text = "1";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(515, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(70, 17);
            this.Label8.TabIndex = 35;
            this.Label8.Text = "MONTH(S)";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(366, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(60, 18);
            this.Label9.TabIndex = 34;
            this.Label9.Text = "OF EVERY";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(515, 94);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(70, 17);
            this.Label10.TabIndex = 33;
            this.Label10.Text = "MONTH(S)";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(366, 94);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(60, 18);
            this.Label11.TabIndex = 32;
            this.Label11.Text = "OF EVERY";
            // 
            // pageYearly
            // 
            this.pageYearly.Controls.Add(this.cmdYearlyThePartOfWeek);
            this.pageYearly.Controls.Add(this.cmbYearlyDay);
            this.pageYearly.Controls.Add(this.cmbYearlyTheDay);
            this.pageYearly.Controls.Add(this.cmbYearlyEveryDate);
            this.pageYearly.Controls.Add(this.cmbYearlyDate);
            this.pageYearly.Controls.Add(this.cmbYearlyTheMonth);
            this.pageYearly.Controls.Add(this.Label12);
            this.pageYearly.Location = new System.Drawing.Point(20, 90);
            this.pageYearly.Name = "pageYearly";
            this.pageYearly.Size = new System.Drawing.Size(554, 140);
            this.pageYearly.TabIndex = 15;
            this.pageYearly.Text = "Yearly";
            // 
            // cmdYearlyThePartOfWeek
            // 
            this.cmdYearlyThePartOfWeek.AutoSize = false;
            this.cmdYearlyThePartOfWeek.BackColor = System.Drawing.SystemColors.Window;
            this.cmdYearlyThePartOfWeek.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmdYearlyThePartOfWeek.FormattingEnabled = true;
            this.cmdYearlyThePartOfWeek.Location = new System.Drawing.Point(20, 80);
            this.cmdYearlyThePartOfWeek.Name = "cmdYearlyThePartOfWeek";
            this.cmdYearlyThePartOfWeek.Size = new System.Drawing.Size(147, 40);
            this.cmdYearlyThePartOfWeek.TabIndex = 22;
            // 
            // cmbYearlyTheDay
            // 
            this.cmbYearlyTheDay.AutoSize = false;
            this.cmbYearlyTheDay.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYearlyTheDay.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYearlyTheDay.FormattingEnabled = true;
            this.cmbYearlyTheDay.Location = new System.Drawing.Point(187, 80);
            this.cmbYearlyTheDay.Name = "cmbYearlyTheDay";
            this.cmbYearlyTheDay.Size = new System.Drawing.Size(147, 40);
            this.cmbYearlyTheDay.TabIndex = 21;
            // 
            // cmbYearlyEveryDate
            // 
            this.cmbYearlyEveryDate.AutoSize = false;
            this.cmbYearlyEveryDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYearlyEveryDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYearlyEveryDate.FormattingEnabled = true;
            this.cmbYearlyEveryDate.Location = new System.Drawing.Point(187, 30);
            this.cmbYearlyEveryDate.Name = "cmbYearlyEveryDate";
            this.cmbYearlyEveryDate.Size = new System.Drawing.Size(147, 40);
            this.cmbYearlyEveryDate.TabIndex = 20;
            // 
            // cmbYearlyDate
            // 
            this.cmbYearlyDate.AutoSize = false;
            this.cmbYearlyDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYearlyDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYearlyDate.FormattingEnabled = true;
            this.cmbYearlyDate.Location = new System.Drawing.Point(356, 30);
            this.cmbYearlyDate.Name = "cmbYearlyDate";
            this.cmbYearlyDate.Size = new System.Drawing.Size(147, 40);
            this.cmbYearlyDate.TabIndex = 19;
            // 
            // cmbYearlyTheMonth
            // 
            this.cmbYearlyTheMonth.AutoSize = false;
            this.cmbYearlyTheMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYearlyTheMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbYearlyTheMonth.FormattingEnabled = true;
            this.cmbYearlyTheMonth.Location = new System.Drawing.Point(387, 80);
            this.cmbYearlyTheMonth.Name = "cmbYearlyTheMonth";
            this.cmbYearlyTheMonth.Size = new System.Drawing.Size(147, 40);
            this.cmbYearlyTheMonth.TabIndex = 18;
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(352, 94);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(17, 18);
            this.Label12.TabIndex = 23;
            this.Label12.Text = "OF";
            // 
            // frameApointmentTime
            // 
            this.frameApointmentTime.Controls.Add(this.txtMinutes);
            this.frameApointmentTime.Controls.Add(this.txtHours);
            this.frameApointmentTime.Controls.Add(this.txtDays);
            this.frameApointmentTime.Controls.Add(this.dtpEventStart);
            this.frameApointmentTime.Controls.Add(this.Label2);
            this.frameApointmentTime.Controls.Add(this.txtEventEndTime);
            this.frameApointmentTime.Controls.Add(this.Label4);
            this.frameApointmentTime.Controls.Add(this.Label1);
            this.frameApointmentTime.Location = new System.Drawing.Point(30, 30);
            this.frameApointmentTime.Name = "frameApointmentTime";
            this.frameApointmentTime.Size = new System.Drawing.Size(769, 90);
            this.frameApointmentTime.TabIndex = 5;
            this.frameApointmentTime.Text = "Appointment Time";
            // 
            // txtMinutes
            // 
            this.txtMinutes.AutoSize = false;
            this.txtMinutes.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinutes.LinkItem = null;
            this.txtMinutes.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMinutes.LinkTopic = null;
            this.txtMinutes.Location = new System.Drawing.Point(689, 30);
            this.txtMinutes.LockedOriginal = true;
            this.txtMinutes.Name = "txtMinutes";
            this.txtMinutes.ReadOnly = true;
            this.txtMinutes.Size = new System.Drawing.Size(60, 40);
            this.txtMinutes.TabIndex = 65;
            this.txtMinutes.TabStop = false;
            this.txtMinutes.Text = "0";
            // 
            // txtHours
            // 
            this.txtHours.AutoSize = false;
            this.txtHours.BackColor = System.Drawing.SystemColors.Window;
            this.txtHours.LinkItem = null;
            this.txtHours.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtHours.LinkTopic = null;
            this.txtHours.Location = new System.Drawing.Point(609, 30);
            this.txtHours.LockedOriginal = true;
            this.txtHours.Name = "txtHours";
            this.txtHours.ReadOnly = true;
            this.txtHours.Size = new System.Drawing.Size(60, 40);
            this.txtHours.TabIndex = 64;
            this.txtHours.TabStop = false;
            this.txtHours.Text = "0";
            // 
            // txtDays
            // 
            this.txtDays.AutoSize = false;
            this.txtDays.BackColor = System.Drawing.SystemColors.Window;
            this.txtDays.LinkItem = null;
            this.txtDays.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDays.LinkTopic = null;
            this.txtDays.Location = new System.Drawing.Point(527, 30);
            this.txtDays.LockedOriginal = true;
            this.txtDays.Name = "txtDays";
            this.txtDays.ReadOnly = true;
            this.txtDays.Size = new System.Drawing.Size(60, 40);
            this.txtDays.TabIndex = 4;
            this.txtDays.TabStop = false;
            this.txtDays.Text = "0";
            // 
            // dtpEventStart
            // 
            this.dtpEventStart.AutoSize = false;
            this.dtpEventStart.Format = Wisej.Web.DateTimePickerFormat.Time;
            this.dtpEventStart.Location = new System.Drawing.Point(89, 30);
            this.dtpEventStart.Name = "dtpEventStart";
            this.dtpEventStart.ShowUpDown = true;
            this.dtpEventStart.Size = new System.Drawing.Size(136, 40);
            this.dtpEventStart.TabIndex = 0;
            this.dtpEventStart.Value = new System.DateTime(2018, 9, 12, 12, 47, 23, 742);
            this.dtpEventStart.Leave += new System.EventHandler(this.dtpEventStart_Leave);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(244, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(30, 17);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "END";
            // 
            // txtEventEndTime
            // 
            this.txtEventEndTime.BorderStyle = 1;
            this.txtEventEndTime.Location = new System.Drawing.Point(303, 44);
            this.txtEventEndTime.Name = "txtEventEndTime";
            this.txtEventEndTime.Size = new System.Drawing.Size(87, 17);
            this.txtEventEndTime.TabIndex = 8;
            this.txtEventEndTime.Text = "LABEL3";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(429, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(70, 18);
            this.Label4.TabIndex = 7;
            this.Label4.Text = "DURATION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(45, 18);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "START";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // Seperator
            // 
            this.Seperator.Index = 0;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveExit
            // 
            this.cmdSaveExit.AppearanceKey = "acceptButton";
            this.cmdSaveExit.Location = new System.Drawing.Point(471, 30);
            this.cmdSaveExit.Name = "cmdSaveExit";
            this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveExit.Size = new System.Drawing.Size(129, 48);
            this.cmdSaveExit.TabIndex = 0;
            this.cmdSaveExit.Text = "Save & Exit";
            // 
            // frmEditRecurrence
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1072, 684);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmEditRecurrence";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Recurrence";
            this.Load += new System.EventHandler(this.frmEditRecurrence_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditRecurrence_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemoveRecurrence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameRangeOfRecurrence)).EndInit();
            this.frameRangeOfRecurrence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frameRecurrencePattern)).EndInit();
            this.frameRecurrencePattern.ResumeLayout(false);
            this.frameRecurrencePattern.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageDaily)).EndInit();
            this.pageDaily.ResumeLayout(false);
            this.pageDaily.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageWeekly)).EndInit();
            this.pageWeekly.ResumeLayout(false);
            this.pageWeekly.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklySunday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklySaturday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyFriday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyThursday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyWednesday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyTusday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkWeeklyMonday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageMonthly)).EndInit();
            this.pageMonthly.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pageYearly)).EndInit();
            this.pageYearly.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.frameApointmentTime)).EndInit();
            this.frameApointmentTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
