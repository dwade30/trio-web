//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmPrintScheduledEvents.
	/// </summary>
	partial class frmPrintScheduledEvents
	{
		public fecherFoundation.FCComboBox cmbUser;
		public fecherFoundation.FCLabel lblUser;
		public fecherFoundation.FCComboBox cmbtNoPageBreaks;
		public fecherFoundation.FCLabel lbltNoPageBreaks;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCListBox lstUsers;
		public fecherFoundation.FCCheckBox chkIncludePrivate;
		public FCDateTimePicker dtpFrom;
		public FCDateTimePicker dtpTo;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbUser = new fecherFoundation.FCComboBox();
            this.lblUser = new fecherFoundation.FCLabel();
            this.cmbtNoPageBreaks = new fecherFoundation.FCComboBox();
            this.lbltNoPageBreaks = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.lstUsers = new fecherFoundation.FCListBox();
            this.chkIncludePrivate = new fecherFoundation.FCCheckBox();
            this.dtpFrom = new fecherFoundation.FCDateTimePicker();
            this.dtpTo = new fecherFoundation.FCDateTimePicker();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludePrivate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 358);
            this.BottomPanel.Size = new System.Drawing.Size(609, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbUser);
            this.ClientArea.Controls.Add(this.lblUser);
            this.ClientArea.Controls.Add(this.cmbtNoPageBreaks);
            this.ClientArea.Controls.Add(this.lbltNoPageBreaks);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.chkIncludePrivate);
            this.ClientArea.Controls.Add(this.dtpFrom);
            this.ClientArea.Controls.Add(this.dtpTo);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(609, 298);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(609, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(211, 30);
            this.HeaderText.Text = "Scheduled Events";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbUser
            // 
            this.cmbUser.AutoSize = false;
            this.cmbUser.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Items.AddRange(new object[] {
            "User",
            "Date"});
            this.cmbUser.Location = new System.Drawing.Point(126, 161);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(220, 40);
            this.cmbUser.TabIndex = 0;
            this.cmbUser.Text = "Date";
            this.ToolTip1.SetToolTip(this.cmbUser, null);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(30, 175);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(72, 15);
            this.lblUser.TabIndex = 1;
            this.lblUser.Text = "ORDER BY";
            this.ToolTip1.SetToolTip(this.lblUser, null);
            // 
            // cmbtNoPageBreaks
            // 
            this.cmbtNoPageBreaks.AutoSize = false;
            this.cmbtNoPageBreaks.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbtNoPageBreaks.FormattingEnabled = true;
            this.cmbtNoPageBreaks.Items.AddRange(new object[] {
            "No page breaks",
            "New page per user"});
            this.cmbtNoPageBreaks.Location = new System.Drawing.Point(126, 221);
            this.cmbtNoPageBreaks.Name = "cmbtNoPageBreaks";
            this.cmbtNoPageBreaks.Size = new System.Drawing.Size(220, 40);
            this.cmbtNoPageBreaks.TabIndex = 2;
            this.cmbtNoPageBreaks.Text = "No page breaks";
            this.ToolTip1.SetToolTip(this.cmbtNoPageBreaks, null);
            // 
            // lbltNoPageBreaks
            // 
            this.lbltNoPageBreaks.AutoSize = true;
            this.lbltNoPageBreaks.Location = new System.Drawing.Point(30, 235);
            this.lbltNoPageBreaks.Name = "lbltNoPageBreaks";
            this.lbltNoPageBreaks.Size = new System.Drawing.Size(50, 15);
            this.lbltNoPageBreaks.TabIndex = 3;
            this.lbltNoPageBreaks.Text = "PAGES";
            this.ToolTip1.SetToolTip(this.lbltNoPageBreaks, null);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.lstUsers);
            this.Frame1.Location = new System.Drawing.Point(376, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(205, 264);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Users";
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // lstUsers
            // 
            this.lstUsers.Appearance = 0;
            this.lstUsers.BackColor = System.Drawing.SystemColors.Window;
			this.lstUsers.CheckBoxes = true;
            this.lstUsers.Location = new System.Drawing.Point(20, 30);
            this.lstUsers.MultiSelect = 0;
            this.lstUsers.Name = "lstUsers";
            this.lstUsers.Size = new System.Drawing.Size(165, 214);
            this.lstUsers.Sorted = true;
            this.lstUsers.Sorting = Wisej.Web.SortOrder.Ascending;
            this.lstUsers.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.lstUsers, null);
            // 
            // chkIncludePrivate
            // 
            this.chkIncludePrivate.Location = new System.Drawing.Point(30, 30);
            this.chkIncludePrivate.Name = "chkIncludePrivate";
            this.chkIncludePrivate.Size = new System.Drawing.Size(187, 27);
            this.chkIncludePrivate.TabIndex = 0;
            this.chkIncludePrivate.Text = "Include private events";
            this.ToolTip1.SetToolTip(this.chkIncludePrivate, "Only your own private events will show detail");
            // 
            // dtpFrom
            // 
            this.dtpFrom.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.dtpFrom.Location = new System.Drawing.Point(126, 77);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(136, 22);
            this.dtpFrom.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.dtpFrom, null);
            this.dtpFrom.Value = new System.DateTime(2018, 9, 12, 17, 3, 42, 432);
            // 
            // dtpTo
            // 
            this.dtpTo.Format = Wisej.Web.DateTimePickerFormat.Short;
            this.dtpTo.Location = new System.Drawing.Point(126, 119);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(136, 22);
            this.dtpTo.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.dtpTo, null);
            this.dtpTo.Value = new System.DateTime(2018, 9, 12, 17, 3, 42, 437);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(30, 124);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(25, 15);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "TO";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 83);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(44, 15);
            this.Label1.TabIndex = 8;
            this.Label1.Text = "FROM";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Print";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(259, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(75, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Print";
            this.ToolTip1.SetToolTip(this.cmdSaveContinue, null);
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmPrintScheduledEvents
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(609, 466);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmPrintScheduledEvents";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Scheduled Events";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmPrintScheduledEvents_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPrintScheduledEvents_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludePrivate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdSaveContinue;
    }
}