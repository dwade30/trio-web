//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmUserCodes.
	/// </summary>
	partial class frmUserCodes
	{
		public FCGrid GridPermitType;
		public FCGrid GridCodeDescs;
		public FCGrid GridExample;
		public FCGrid GridParameters;
		public FCGrid GridCodes;
		public fecherFoundation.FCComboBox cmbCodeGroup;
		public FCGrid gridDelete;
		public FCGrid GridDeletedCodeDescs;
		public FCGrid gridViolationType;
		public fecherFoundation.FCLabel lblViolationType;
		public fecherFoundation.FCLabel lblPermitType;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddCode;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCode;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuAddCodeDescription;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCodeDescription;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle13 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle14 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle15 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle16 = new Wisej.Web.DataGridViewCellStyle();
			this.GridPermitType = new fecherFoundation.FCGrid();
			this.GridCodeDescs = new fecherFoundation.FCGrid();
			this.GridExample = new fecherFoundation.FCGrid();
			this.GridParameters = new fecherFoundation.FCGrid();
			this.GridCodes = new fecherFoundation.FCGrid();
			this.cmbCodeGroup = new fecherFoundation.FCComboBox();
			this.gridDelete = new fecherFoundation.FCGrid();
			this.GridDeletedCodeDescs = new fecherFoundation.FCGrid();
			this.gridViolationType = new fecherFoundation.FCGrid();
			this.lblViolationType = new fecherFoundation.FCLabel();
			this.lblPermitType = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteCode = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddCodeDescription = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteCodeDescription = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdDeleteCodeDescription = new fecherFoundation.FCButton();
			this.cmdAddCodeDescription = new fecherFoundation.FCButton();
			this.cmdDeleteCode = new fecherFoundation.FCButton();
			this.cmdAddCode = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridPermitType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridCodeDescs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExample)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridParameters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedCodeDescs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViolationType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCodeDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCodeDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(899, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridCodeDescs);
			this.ClientArea.Controls.Add(this.GridExample);
			this.ClientArea.Controls.Add(this.GridCodes);
			this.ClientArea.Controls.Add(this.cmbCodeGroup);
			this.ClientArea.Controls.Add(this.gridDelete);
			this.ClientArea.Controls.Add(this.GridDeletedCodeDescs);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.GridPermitType);
			this.ClientArea.Controls.Add(this.GridParameters);
			this.ClientArea.Controls.Add(this.gridViolationType);
			this.ClientArea.Controls.Add(this.lblViolationType);
			this.ClientArea.Controls.Add(this.lblPermitType);
			this.ClientArea.Size = new System.Drawing.Size(899, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAddCode);
			this.TopPanel.Controls.Add(this.cmdDeleteCode);
			this.TopPanel.Controls.Add(this.cmdAddCodeDescription);
			this.TopPanel.Controls.Add(this.cmdDeleteCodeDescription);
			this.TopPanel.Size = new System.Drawing.Size(899, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCodeDescription, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddCodeDescription, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCode, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddCode, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(156, 30);
			this.HeaderText.Text = "User Defined";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// GridPermitType
			// 
			this.GridPermitType.AllowSelection = false;
			this.GridPermitType.AllowUserToResizeColumns = false;
			this.GridPermitType.AllowUserToResizeRows = false;
			this.GridPermitType.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridPermitType.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridPermitType.BackColorBkg = System.Drawing.Color.Empty;
			this.GridPermitType.BackColorFixed = System.Drawing.Color.Empty;
			this.GridPermitType.BackColorSel = System.Drawing.Color.Empty;
			this.GridPermitType.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridPermitType.Cols = 1;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridPermitType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.GridPermitType.ColumnHeadersHeight = 30;
			this.GridPermitType.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridPermitType.ColumnHeadersVisible = false;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridPermitType.DefaultCellStyle = dataGridViewCellStyle12;
			this.GridPermitType.DragIcon = null;
			this.GridPermitType.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridPermitType.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridPermitType.ExtendLastCol = true;
			this.GridPermitType.FixedCols = 0;
			this.GridPermitType.FixedRows = 0;
			this.GridPermitType.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridPermitType.FrozenCols = 0;
			this.GridPermitType.GridColor = System.Drawing.Color.Empty;
			this.GridPermitType.GridColorFixed = System.Drawing.Color.Empty;
			this.GridPermitType.Location = new System.Drawing.Point(562, 65);
			this.GridPermitType.Name = "GridPermitType";
			this.GridPermitType.OutlineCol = 0;
			this.GridPermitType.RowHeadersVisible = false;
			this.GridPermitType.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridPermitType.RowHeightMin = 0;
			this.GridPermitType.Rows = 1;
			this.GridPermitType.ScrollTipText = null;
			this.GridPermitType.ShowColumnVisibilityMenu = false;
			this.GridPermitType.ShowFocusCell = false;
			this.GridPermitType.Size = new System.Drawing.Size(370, 42);
			this.GridPermitType.StandardTab = true;
			this.GridPermitType.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridPermitType.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridPermitType.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.GridPermitType, null);
			this.GridPermitType.ComboCloseUp += new System.EventHandler(this.GridPermitType_ComboCloseUp);
			this.GridPermitType.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridPermitType_AfterEdit);
			// 
			// GridCodeDescs
			// 
			this.GridCodeDescs.AllowSelection = false;
			this.GridCodeDescs.AllowUserToResizeColumns = false;
			this.GridCodeDescs.AllowUserToResizeRows = false;
			this.GridCodeDescs.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridCodeDescs.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridCodeDescs.BackColorBkg = System.Drawing.Color.Empty;
			this.GridCodeDescs.BackColorFixed = System.Drawing.Color.Empty;
			this.GridCodeDescs.BackColorSel = System.Drawing.Color.Empty;
			this.GridCodeDescs.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridCodeDescs.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridCodeDescs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridCodeDescs.ColumnHeadersHeight = 30;
			this.GridCodeDescs.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridCodeDescs.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridCodeDescs.DragIcon = null;
			this.GridCodeDescs.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridCodeDescs.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridCodeDescs.ExtendLastCol = true;
			this.GridCodeDescs.FixedCols = 0;
			this.GridCodeDescs.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridCodeDescs.FrozenCols = 0;
			this.GridCodeDescs.GridColor = System.Drawing.Color.Empty;
			this.GridCodeDescs.GridColorFixed = System.Drawing.Color.Empty;
			this.GridCodeDescs.Location = new System.Drawing.Point(562, 307);
			this.GridCodeDescs.Name = "GridCodeDescs";
			this.GridCodeDescs.OutlineCol = 0;
			this.GridCodeDescs.RowHeadersVisible = false;
			this.GridCodeDescs.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridCodeDescs.RowHeightMin = 0;
			this.GridCodeDescs.Rows = 1;
			this.GridCodeDescs.ScrollTipText = null;
			this.GridCodeDescs.ShowColumnVisibilityMenu = false;
			this.GridCodeDescs.ShowFocusCell = false;
			this.GridCodeDescs.Size = new System.Drawing.Size(370, 188);
			this.GridCodeDescs.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridCodeDescs.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridCodeDescs.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.GridCodeDescs, null);
			this.GridCodeDescs.Visible = false;
			this.GridCodeDescs.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridCodeDescs_AfterEdit);
			this.GridCodeDescs.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridCodeDescs_MouseDownEvent);
			this.GridCodeDescs.KeyDown += new Wisej.Web.KeyEventHandler(this.GridCodeDescs_KeyDownEvent);
			// 
			// GridExample
			// 
			this.GridExample.AllowSelection = false;
			this.GridExample.AllowUserToResizeColumns = false;
			this.GridExample.AllowUserToResizeRows = false;
			this.GridExample.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridExample.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridExample.BackColorBkg = System.Drawing.Color.Empty;
			this.GridExample.BackColorFixed = System.Drawing.Color.Empty;
			this.GridExample.BackColorSel = System.Drawing.Color.Empty;
			this.GridExample.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridExample.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridExample.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridExample.ColumnHeadersHeight = 30;
			this.GridExample.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridExample.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridExample.DragIcon = null;
			this.GridExample.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridExample.ExtendLastCol = true;
			this.GridExample.FixedCols = 0;
			this.GridExample.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridExample.FrozenCols = 0;
			this.GridExample.GridColor = System.Drawing.Color.Empty;
			this.GridExample.GridColorFixed = System.Drawing.Color.Empty;
			this.GridExample.Location = new System.Drawing.Point(30, 546);
			this.GridExample.Name = "GridExample";
			this.GridExample.OutlineCol = 0;
			this.GridExample.ReadOnly = true;
			this.GridExample.RowHeadersVisible = false;
			this.GridExample.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridExample.RowHeightMin = 0;
			this.GridExample.Rows = 2;
			this.GridExample.ScrollTipText = null;
			this.GridExample.ShowColumnVisibilityMenu = false;
			this.GridExample.ShowFocusCell = false;
			this.GridExample.Size = new System.Drawing.Size(513, 152);
			this.GridExample.StandardTab = true;
			this.GridExample.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridExample.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridExample.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.GridExample, null);
			this.GridExample.Visible = false;
			// 
			// GridParameters
			// 
			this.GridParameters.AllowSelection = false;
			this.GridParameters.AllowUserToResizeColumns = false;
			this.GridParameters.AllowUserToResizeRows = false;
			this.GridParameters.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridParameters.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridParameters.BackColorBkg = System.Drawing.Color.Empty;
			this.GridParameters.BackColorFixed = System.Drawing.Color.Empty;
			this.GridParameters.BackColorSel = System.Drawing.Color.Empty;
			this.GridParameters.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridParameters.Cols = 2;
			dataGridViewCellStyle13.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridParameters.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
			this.GridParameters.ColumnHeadersHeight = 30;
			this.GridParameters.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridParameters.ColumnHeadersVisible = false;
			dataGridViewCellStyle14.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridParameters.DefaultCellStyle = dataGridViewCellStyle14;
			this.GridParameters.DragIcon = null;
			this.GridParameters.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridParameters.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridParameters.ExtendLastCol = true;
			this.GridParameters.FixedRows = 0;
			this.GridParameters.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridParameters.FrozenCols = 0;
			this.GridParameters.GridColor = System.Drawing.Color.Empty;
			this.GridParameters.GridColorFixed = System.Drawing.Color.Empty;
			this.GridParameters.Location = new System.Drawing.Point(562, 125);
			this.GridParameters.Name = "GridParameters";
			this.GridParameters.OutlineCol = 0;
			this.GridParameters.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridParameters.RowHeightMin = 0;
			this.GridParameters.Rows = 4;
			this.GridParameters.ScrollTipText = null;
			this.GridParameters.ShowColumnVisibilityMenu = false;
			this.GridParameters.ShowFocusCell = false;
			this.GridParameters.Size = new System.Drawing.Size(370, 164);
			this.GridParameters.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridParameters.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridParameters.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.GridParameters, null);
			this.GridParameters.ComboCloseUp += new System.EventHandler(this.GridParameters_ComboCloseUp);
			this.GridParameters.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridParameters_BeforeEdit);
			this.GridParameters.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridParameters_AfterEdit);
			this.GridParameters.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridParameters_ValidateEdit);
			// 
			// GridCodes
			// 
			this.GridCodes.AllowSelection = false;
			this.GridCodes.AllowUserToResizeColumns = false;
			this.GridCodes.AllowUserToResizeRows = false;
			this.GridCodes.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridCodes.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridCodes.BackColorBkg = System.Drawing.Color.Empty;
			this.GridCodes.BackColorFixed = System.Drawing.Color.Empty;
			this.GridCodes.BackColorSel = System.Drawing.Color.Empty;
			this.GridCodes.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridCodes.Cols = 13;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridCodes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.GridCodes.ColumnHeadersHeight = 30;
			this.GridCodes.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridCodes.DefaultCellStyle = dataGridViewCellStyle6;
			this.GridCodes.DragIcon = null;
			this.GridCodes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridCodes.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridCodes.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
			this.GridCodes.ExtendLastCol = true;
			this.GridCodes.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridCodes.FrozenCols = 0;
			this.GridCodes.GridColor = System.Drawing.Color.Empty;
			this.GridCodes.GridColorFixed = System.Drawing.Color.Empty;
			this.GridCodes.Location = new System.Drawing.Point(30, 125);
			this.GridCodes.Name = "GridCodes";
			this.GridCodes.OutlineCol = 0;
			this.GridCodes.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridCodes.RowHeightMin = 0;
			this.GridCodes.Rows = 1;
			this.GridCodes.ScrollTipText = null;
			this.GridCodes.ShowColumnVisibilityMenu = false;
			this.GridCodes.ShowFocusCell = false;
			this.GridCodes.Size = new System.Drawing.Size(513, 371);
			this.GridCodes.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridCodes.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridCodes.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.GridCodes, "Use Insert and Delete keys to add and delete items");
			this.GridCodes.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridCodes_AfterEdit);
			this.GridCodes.CurrentCellChanged += new System.EventHandler(this.GridCodes_RowColChange);
			this.GridCodes.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridCodes_MouseDownEvent);
			this.GridCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.GridCodes_KeyDownEvent);
			// 
			// cmbCodeGroup
			// 
			this.cmbCodeGroup.AutoSize = false;
			this.cmbCodeGroup.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCodeGroup.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCodeGroup.FormattingEnabled = true;
			this.cmbCodeGroup.Location = new System.Drawing.Point(30, 65);
			this.cmbCodeGroup.Name = "cmbCodeGroup";
			this.cmbCodeGroup.Size = new System.Drawing.Size(513, 40);
			this.cmbCodeGroup.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.cmbCodeGroup, null);
			this.cmbCodeGroup.SelectedIndexChanged += new System.EventHandler(this.cmbCodeGroup_SelectedIndexChanged);
			// 
			// gridDelete
			// 
			this.gridDelete.AllowSelection = false;
			this.gridDelete.AllowUserToResizeColumns = false;
			this.gridDelete.AllowUserToResizeRows = false;
			this.gridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.gridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.gridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.gridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridDelete.Cols = 3;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.gridDelete.ColumnHeadersHeight = 30;
			this.gridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridDelete.ColumnHeadersVisible = false;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridDelete.DefaultCellStyle = dataGridViewCellStyle8;
			this.gridDelete.DragIcon = null;
			this.gridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridDelete.FixedCols = 0;
			this.gridDelete.FixedRows = 0;
			this.gridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridDelete.FrozenCols = 0;
			this.gridDelete.GridColor = System.Drawing.Color.Empty;
			this.gridDelete.GridColorFixed = System.Drawing.Color.Empty;
			this.gridDelete.Location = new System.Drawing.Point(23, 47);
			this.gridDelete.Name = "gridDelete";
			this.gridDelete.OutlineCol = 0;
			this.gridDelete.ReadOnly = true;
			this.gridDelete.RowHeadersVisible = false;
			this.gridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridDelete.RowHeightMin = 0;
			this.gridDelete.Rows = 0;
			this.gridDelete.ScrollTipText = null;
			this.gridDelete.ShowColumnVisibilityMenu = false;
			this.gridDelete.ShowFocusCell = false;
			this.gridDelete.Size = new System.Drawing.Size(34, 18);
			this.gridDelete.StandardTab = true;
			this.gridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridDelete.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.gridDelete, null);
			this.gridDelete.Visible = false;
			// 
			// GridDeletedCodeDescs
			// 
			this.GridDeletedCodeDescs.AllowSelection = false;
			this.GridDeletedCodeDescs.AllowUserToResizeColumns = false;
			this.GridDeletedCodeDescs.AllowUserToResizeRows = false;
			this.GridDeletedCodeDescs.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDeletedCodeDescs.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.BackColorSel = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDeletedCodeDescs.Cols = 3;
			dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDeletedCodeDescs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
			this.GridDeletedCodeDescs.ColumnHeadersHeight = 30;
			this.GridDeletedCodeDescs.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDeletedCodeDescs.ColumnHeadersVisible = false;
			dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDeletedCodeDescs.DefaultCellStyle = dataGridViewCellStyle10;
			this.GridDeletedCodeDescs.DragIcon = null;
			this.GridDeletedCodeDescs.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDeletedCodeDescs.FixedCols = 0;
			this.GridDeletedCodeDescs.FixedRows = 0;
			this.GridDeletedCodeDescs.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.FrozenCols = 0;
			this.GridDeletedCodeDescs.GridColor = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDeletedCodeDescs.Location = new System.Drawing.Point(0, 24);
			this.GridDeletedCodeDescs.Name = "GridDeletedCodeDescs";
			this.GridDeletedCodeDescs.OutlineCol = 0;
			this.GridDeletedCodeDescs.ReadOnly = true;
			this.GridDeletedCodeDescs.RowHeadersVisible = false;
			this.GridDeletedCodeDescs.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDeletedCodeDescs.RowHeightMin = 0;
			this.GridDeletedCodeDescs.Rows = 0;
			this.GridDeletedCodeDescs.ScrollTipText = null;
			this.GridDeletedCodeDescs.ShowColumnVisibilityMenu = false;
			this.GridDeletedCodeDescs.ShowFocusCell = false;
			this.GridDeletedCodeDescs.Size = new System.Drawing.Size(34, 18);
			this.GridDeletedCodeDescs.StandardTab = true;
			this.GridDeletedCodeDescs.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDeletedCodeDescs.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDeletedCodeDescs.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.GridDeletedCodeDescs, null);
			this.GridDeletedCodeDescs.Visible = false;
			// 
			// gridViolationType
			// 
			this.gridViolationType.AllowSelection = false;
			this.gridViolationType.AllowUserToResizeColumns = false;
			this.gridViolationType.AllowUserToResizeRows = false;
			this.gridViolationType.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.gridViolationType.BackColorAlternate = System.Drawing.Color.Empty;
			this.gridViolationType.BackColorBkg = System.Drawing.Color.Empty;
			this.gridViolationType.BackColorFixed = System.Drawing.Color.Empty;
			this.gridViolationType.BackColorSel = System.Drawing.Color.Empty;
			this.gridViolationType.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.gridViolationType.Cols = 1;
			dataGridViewCellStyle15.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.gridViolationType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this.gridViolationType.ColumnHeadersHeight = 30;
			this.gridViolationType.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.gridViolationType.ColumnHeadersVisible = false;
			dataGridViewCellStyle16.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.gridViolationType.DefaultCellStyle = dataGridViewCellStyle16;
			this.gridViolationType.DragIcon = null;
			this.gridViolationType.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridViolationType.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.gridViolationType.ExtendLastCol = true;
			this.gridViolationType.FixedCols = 0;
			this.gridViolationType.FixedRows = 0;
			this.gridViolationType.ForeColorFixed = System.Drawing.Color.Empty;
			this.gridViolationType.FrozenCols = 0;
			this.gridViolationType.GridColor = System.Drawing.Color.Empty;
			this.gridViolationType.GridColorFixed = System.Drawing.Color.Empty;
			this.gridViolationType.Location = new System.Drawing.Point(562, 65);
			this.gridViolationType.Name = "gridViolationType";
			this.gridViolationType.OutlineCol = 0;
			this.gridViolationType.RowHeadersVisible = false;
			this.gridViolationType.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.gridViolationType.RowHeightMin = 0;
			this.gridViolationType.Rows = 1;
			this.gridViolationType.ScrollTipText = null;
			this.gridViolationType.ShowColumnVisibilityMenu = false;
			this.gridViolationType.ShowFocusCell = false;
			this.gridViolationType.Size = new System.Drawing.Size(370, 42);
			this.gridViolationType.StandardTab = true;
			this.gridViolationType.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.gridViolationType.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.gridViolationType.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.gridViolationType, null);
			this.gridViolationType.ComboCloseUp += new System.EventHandler(this.gridViolationType_ComboCloseUp);
			this.gridViolationType.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.gridViolationType_AfterEdit);
			// 
			// lblViolationType
			// 
			this.lblViolationType.Location = new System.Drawing.Point(562, 30);
			this.lblViolationType.Name = "lblViolationType";
			this.lblViolationType.Size = new System.Drawing.Size(177, 15);
			this.lblViolationType.TabIndex = 11;
			this.lblViolationType.Text = "VIOLATION TYPE";
			this.ToolTip1.SetToolTip(this.lblViolationType, null);
			// 
			// lblPermitType
			// 
			this.lblPermitType.Location = new System.Drawing.Point(608, 30);
			this.lblPermitType.Name = "lblPermitType";
			this.lblPermitType.Size = new System.Drawing.Size(177, 15);
			this.lblPermitType.TabIndex = 9;
			this.lblPermitType.Text = "PERMIT TYPE";
			this.lblPermitType.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.lblPermitType, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 516);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(66, 15);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "EXAMPLE";
			this.ToolTip1.SetToolTip(this.Label1, null);
			this.Label1.Visible = false;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddCode,
            this.mnuDeleteCode,
            this.mnuSepar2,
            this.mnuAddCodeDescription,
            this.mnuDeleteCodeDescription,
            this.mnuSepar3,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddCode
			// 
			this.mnuAddCode.Index = 0;
			this.mnuAddCode.Name = "mnuAddCode";
			this.mnuAddCode.Text = "Add Item";
			this.mnuAddCode.Click += new System.EventHandler(this.mnuAddCode_Click);
			// 
			// mnuDeleteCode
			// 
			this.mnuDeleteCode.Index = 1;
			this.mnuDeleteCode.Name = "mnuDeleteCode";
			this.mnuDeleteCode.Text = "Delete Item";
			this.mnuDeleteCode.Click += new System.EventHandler(this.mnuDeleteCode_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 2;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuAddCodeDescription
			// 
			this.mnuAddCodeDescription.Index = 3;
			this.mnuAddCodeDescription.Name = "mnuAddCodeDescription";
			this.mnuAddCodeDescription.Text = "Add Code Description";
			this.mnuAddCodeDescription.Click += new System.EventHandler(this.mnuAddCodeDescription_Click);
			// 
			// mnuDeleteCodeDescription
			// 
			this.mnuDeleteCodeDescription.Index = 4;
			this.mnuDeleteCodeDescription.Name = "mnuDeleteCodeDescription";
			this.mnuDeleteCodeDescription.Text = "Delete Code Description";
			this.mnuDeleteCodeDescription.Click += new System.EventHandler(this.mnuDeleteCodeDescription_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 5;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 6;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 7;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 8;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 9;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdDeleteCodeDescription
			// 
			this.cmdDeleteCodeDescription.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteCodeDescription.AppearanceKey = "toolbarButton";
			this.cmdDeleteCodeDescription.Location = new System.Drawing.Point(705, 29);
			this.cmdDeleteCodeDescription.Name = "cmdDeleteCodeDescription";
			this.cmdDeleteCodeDescription.Size = new System.Drawing.Size(166, 24);
			this.cmdDeleteCodeDescription.TabIndex = 1;
			this.cmdDeleteCodeDescription.Text = "Delete Code Description";
			this.ToolTip1.SetToolTip(this.cmdDeleteCodeDescription, null);
			this.cmdDeleteCodeDescription.Click += new System.EventHandler(this.mnuDeleteCodeDescription_Click);
			// 
			// cmdAddCodeDescription
			// 
			this.cmdAddCodeDescription.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddCodeDescription.AppearanceKey = "toolbarButton";
			this.cmdAddCodeDescription.Location = new System.Drawing.Point(545, 29);
			this.cmdAddCodeDescription.Name = "cmdAddCodeDescription";
			this.cmdAddCodeDescription.Size = new System.Drawing.Size(154, 24);
			this.cmdAddCodeDescription.TabIndex = 2;
			this.cmdAddCodeDescription.Text = "Add Code Description";
			this.ToolTip1.SetToolTip(this.cmdAddCodeDescription, null);
			this.cmdAddCodeDescription.Click += new System.EventHandler(this.mnuAddCodeDescription_Click);
			// 
			// cmdDeleteCode
			// 
			this.cmdDeleteCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteCode.AppearanceKey = "toolbarButton";
			this.cmdDeleteCode.Location = new System.Drawing.Point(451, 29);
			this.cmdDeleteCode.Name = "cmdDeleteCode";
			this.cmdDeleteCode.Size = new System.Drawing.Size(88, 24);
			this.cmdDeleteCode.TabIndex = 3;
			this.cmdDeleteCode.Text = "Delete Item";
			this.ToolTip1.SetToolTip(this.cmdDeleteCode, null);
			this.cmdDeleteCode.Click += new System.EventHandler(this.mnuDeleteCode_Click);
			// 
			// cmdAddCode
			// 
			this.cmdAddCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddCode.AppearanceKey = "toolbarButton";
			this.cmdAddCode.Location = new System.Drawing.Point(371, 29);
			this.cmdAddCode.Name = "cmdAddCode";
			this.cmdAddCode.Size = new System.Drawing.Size(74, 24);
			this.cmdAddCode.TabIndex = 4;
			this.cmdAddCode.Text = "Add Item";
			this.ToolTip1.SetToolTip(this.cmdAddCode, null);
			this.cmdAddCode.Click += new System.EventHandler(this.mnuAddCode_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(296, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(79, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmUserCodes
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(899, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUserCodes";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "User Defined";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmUserCodes_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUserCodes_KeyDown);
			this.Resize += new System.EventHandler(this.frmUserCodes_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridPermitType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridCodeDescs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridExample)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridParameters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedCodeDescs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridViolationType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCodeDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCodeDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdAddCodeDescription;
        private FCButton cmdDeleteCodeDescription;
        private FCButton cmdDeleteCode;
        private FCButton cmdAddCode;
        private FCButton cmdSave;
    }
}