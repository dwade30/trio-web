﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptScheduledEvents.
	/// </summary>
	partial class rptScheduledEvents
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptScheduledEvents));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEventTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEventDuration = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEventDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtUser = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventDuration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtDescription,
				this.txtEventTime,
				this.txtEventDuration,
				this.txtLocation,
				this.txtEventDate,
				this.txtUser
			});
			this.Detail.Height = 0.1770833F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label1,
				this.txtRange,
				this.Field1,
				this.Field2,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8
			});
			this.PageHeader.Height = 0.78125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Scheduled Events";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.9375F;
			// 
			// txtRange
			// 
			this.txtRange.Height = 0.2083333F;
			this.txtRange.Left = 1.25F;
			this.txtRange.Name = "txtRange";
			this.txtRange.Text = null;
			this.txtRange.Top = 0.25F;
			this.txtRange.Width = 4.9375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1666667F;
			this.Field1.Left = 4.03125F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-weight: bold";
			this.Field1.Text = "Event";
			this.Field1.Top = 0.6041667F;
			this.Field1.Width = 1.104167F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1666667F;
			this.Field2.Left = 6F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-weight: bold";
			this.Field2.Text = "Location";
			this.Field2.Top = 0.6041667F;
			this.Field2.Width = 1.104167F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1666667F;
			this.Field5.Left = 1.625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-weight: bold";
			this.Field5.Text = "Duration";
			this.Field5.Top = 0.6041667F;
			this.Field5.Width = 1.104167F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1666667F;
			this.Field6.Left = 0F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-weight: bold";
			this.Field6.Text = "Date";
			this.Field6.Top = 0.6041667F;
			this.Field6.Width = 0.7916667F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1666667F;
			this.Field7.Left = 0.84375F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-weight: bold";
			this.Field7.Text = "Time";
			this.Field7.Top = 0.6041667F;
			this.Field7.Width = 0.7916667F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1666667F;
			this.Field8.Left = 2.875F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-weight: bold";
			this.Field8.Text = "User";
			this.Field8.Top = 0.6041667F;
			this.Field8.Width = 0.875F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1666667F;
			this.txtDescription.Left = 4.041667F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 1.916667F;
			// 
			// txtEventTime
			// 
			this.txtEventTime.Height = 0.1666667F;
			this.txtEventTime.Left = 0.84375F;
			this.txtEventTime.Name = "txtEventTime";
			this.txtEventTime.Text = null;
			this.txtEventTime.Top = 0F;
			this.txtEventTime.Width = 0.78125F;
			// 
			// txtEventDuration
			// 
			this.txtEventDuration.Height = 0.1666667F;
			this.txtEventDuration.Left = 1.645833F;
			this.txtEventDuration.Name = "txtEventDuration";
			this.txtEventDuration.Text = null;
			this.txtEventDuration.Top = 0F;
			this.txtEventDuration.Width = 1.21875F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1666667F;
			this.txtLocation.Left = 6F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0F;
			this.txtLocation.Width = 1.395833F;
			// 
			// txtEventDate
			// 
			this.txtEventDate.Height = 0.1666667F;
			this.txtEventDate.Left = 0F;
			this.txtEventDate.Name = "txtEventDate";
			this.txtEventDate.Text = null;
			this.txtEventDate.Top = 0F;
			this.txtEventDate.Width = 0.8333333F;
			// 
			// txtUser
			// 
			this.txtUser.Height = 0.1666667F;
			this.txtUser.Left = 2.875F;
			this.txtUser.Name = "txtUser";
			this.txtUser.Text = null;
			this.txtUser.Top = 0F;
			this.txtUser.Width = 1.135417F;
			// 
			// rptScheduledEvents
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventDuration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEventDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEventTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEventDuration;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEventDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtUser;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
