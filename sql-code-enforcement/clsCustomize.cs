//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsCustomize
	{
		//=========================================================
		bool boolUsesRE;
		string strMasterTable = "";
		// which table are we using, from RE or CE
		string strMapLotTable = "";
		// which table are we using, from RE or CE
		bool boolUseRE;
		// vbPorter upgrade warning: intPermitIdentifierValidation As int	OnWrite(int, IdentifierValidation)
		private int intPermitIdentifierValidation;
		private bool boolGeneratePermitIdentifiers;
		// vbPorter upgrade warning: intPermitIdentifierFormat As int	OnWrite(int, IdentifierFormat)
		private int intPermitIdentifierFormat;
		// vbPorter upgrade warning: intViolationIdentifierValidation As int	OnWrite(int, IdentifierValidation)
		private int intViolationIdentifierValidation;
		private bool boolGenerateViolationIdentifiers;
		// vbPorter upgrade warning: intViolationIdentifierFormat As int	OnWrite(int, IdentifierFormat)
		private int intViolationIdentifierFormat;
		private bool boolPromptInspectionSchedule;
		private bool boolAutoAddInspectionsToCalendar;
		private bool boolPermitNumberReset;
		private bool boolUseTwoDigitYear;
		private int intStartMonth;
		private int intYearAdd;
		private int intPermitFormatPlaces;
		private int intViolationFormatPlaces;

		public enum IdentifierValidation
		{
			TotallyUnique = 0,
			TypeUnique = 1,
			NotUnique = 2,
		}

		public enum IdentifierFormat
		{
			NumberOnly = 0,
			yeardashnumber = 1,
			NumberDashYear = 2,
		}

		const string CNSTREMASTERTABLE = "REMaster";
		const string CNSTCEMASTERTABLE = "CEMaster";
		const string CNSTREMAPLOTTABLE = "REMapLot";
		const string CNSTCEMAPLOTTABLE = "CEMapLot";

		public int ViolationFormatPlaces
		{
			set
			{
				intViolationFormatPlaces = value;
			}
			get
			{
				int ViolationFormatPlaces = 0;
				ViolationFormatPlaces = intViolationFormatPlaces;
				return ViolationFormatPlaces;
			}
		}

		public int PermitFormatPlaces
		{
			set
			{
				intPermitFormatPlaces = value;
			}
			get
			{
				int PermitFormatPlaces = 0;
				PermitFormatPlaces = intPermitFormatPlaces;
				return PermitFormatPlaces;
			}
		}

		public int AddToYear
		{
			set
			{
				intYearAdd = value;
			}
			get
			{
				int AddToYear = 0;
				AddToYear = intYearAdd;
				return AddToYear;
			}
		}

		public int StartMonth
		{
			set
			{
				intStartMonth = value;
			}
			get
			{
				int StartMonth = 0;
				StartMonth = intStartMonth;
				return StartMonth;
			}
		}

		public bool UseTwoDigitYear
		{
			set
			{
				boolUseTwoDigitYear = value;
			}
			get
			{
				bool UseTwoDigitYear = false;
				UseTwoDigitYear = boolUseTwoDigitYear;
				return UseTwoDigitYear;
			}
		}

		public bool PermitNumberReset
		{
			set
			{
				boolPermitNumberReset = value;
			}
			get
			{
				bool PermitNumberReset = false;
				PermitNumberReset = boolPermitNumberReset;
				return PermitNumberReset;
			}
		}

		public bool PromptInspectionSchedule
		{
			set
			{
				boolPromptInspectionSchedule = value;
			}
			get
			{
				bool PromptInspectionSchedule = false;
				PromptInspectionSchedule = boolPromptInspectionSchedule;
				return PromptInspectionSchedule;
			}
		}

		public bool AutoAddInspectionsToCalendar
		{
			set
			{
				boolAutoAddInspectionsToCalendar = value;
			}
			get
			{
				bool AutoAddInspectionsToCalendar = false;
				AutoAddInspectionsToCalendar = boolAutoAddInspectionsToCalendar;
				return AutoAddInspectionsToCalendar;
			}
		}

		public int PermitIdentifierValidation
		{
			set
			{
				intPermitIdentifierValidation = value;
			}
			get
			{
				int PermitIdentifierValidation = 0;
				PermitIdentifierValidation = intPermitIdentifierValidation;
				return PermitIdentifierValidation;
			}
		}

		public int PermitIdentifierFormat
		{
			set
			{
				intPermitIdentifierFormat = value;
			}
			get
			{
				int PermitIdentifierFormat = 0;
				PermitIdentifierFormat = intPermitIdentifierFormat;
				return PermitIdentifierFormat;
			}
		}

		public bool GeneratePermitIdentifiers
		{
			set
			{
				boolGeneratePermitIdentifiers = value;
			}
			get
			{
				bool GeneratePermitIdentifiers = false;
				GeneratePermitIdentifiers = boolGeneratePermitIdentifiers;
				return GeneratePermitIdentifiers;
			}
		}

		public int ViolationIdentifierValidation
		{
			set
			{
				intViolationIdentifierValidation = value;
			}
			get
			{
				int ViolationIdentifierValidation = 0;
				ViolationIdentifierValidation = intViolationIdentifierValidation;
				return ViolationIdentifierValidation;
			}
		}

		public int ViolationIdentifierFormat
		{
			set
			{
				intViolationIdentifierFormat = value;
			}
			get
			{
				int ViolationIdentifierFormat = 0;
				ViolationIdentifierFormat = intViolationIdentifierFormat;
				return ViolationIdentifierFormat;
			}
		}

		public bool GenerateViolationIdentifiers
		{
			set
			{
				boolGenerateViolationIdentifiers = value;
			}
			get
			{
				bool GenerateViolationIdentifiers = false;
				GenerateViolationIdentifiers = boolGenerateViolationIdentifiers;
				return GenerateViolationIdentifiers;
			}
		}

		public string MasterTable
		{
			get
			{
				string MasterTable = "";
				MasterTable = strMasterTable;
				return MasterTable;
			}
		}

		public string MapLotTable
		{
			get
			{
				string MapLotTable = "";
				MapLotTable = strMapLotTable;
				return MapLotTable;
			}
		}

		public bool GetDataFromRE
		{
			get
			{
				bool GetDataFromRE = false;
				GetDataFromRE = boolUsesRE && boolUseRE;
				return GetDataFromRE;
			}
		}

		public bool UseREData
		{
			set
			{
				boolUseRE = value;
			}
			get
			{
				bool UseREData = false;
				UseREData = boolUseRE;
				return UseREData;
			}
		}

		public void Save()
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsSave.OpenRecordset("select * from customize", modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("UseREInfo", boolUseRE);
				rsSave.Set_Fields("permitidentifierformat", intPermitIdentifierFormat);
				rsSave.Set_Fields("permitidentifiervalidation", intPermitIdentifierValidation);
				rsSave.Set_Fields("violationidentifierformat", intViolationIdentifierFormat);
				rsSave.Set_Fields("violationidentifiervalidation", intViolationIdentifierValidation);
				rsSave.Set_Fields("GeneratePermitIdentifiers", boolGeneratePermitIdentifiers);
				rsSave.Set_Fields("GenerateViolationIdentifiers", boolGenerateViolationIdentifiers);
				rsSave.Set_Fields("PromptInspectionSchedule", boolPromptInspectionSchedule);
				rsSave.Set_Fields("AutoAddInspectionToCalendar", boolAutoAddInspectionsToCalendar);
				rsSave.Update();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Save", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public clsCustomize() : base()
		{
			ReLoad();
		}

		public void ReLoad()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
				{
					boolUsesRE = true;
					strMasterTable = CNSTREMASTERTABLE;
					strMapLotTable = CNSTREMAPLOTTABLE;
				}
				else
				{
					boolUsesRE = false;
					strMasterTable = CNSTCEMASTERTABLE;
					strMapLotTable = CNSTCEMAPLOTTABLE;
				}
				clsLoad.OpenRecordset("Select * from customize", modGlobalVariables.Statics.strCEDatabase);
				if (!clsLoad.EndOfFile())
				{
					boolUseRE = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("UseREInfo"));
					intPermitIdentifierFormat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("PermitIdentifierFormat"))));
					intPermitIdentifierValidation = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("PermitIdentifierValidation"))));
					intViolationIdentifierFormat = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("ViolationIdentifierFormat"))));
					intViolationIdentifierValidation = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("ViolationIdentifierValidation"))));
					boolGeneratePermitIdentifiers = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("GeneratePermitIdentifiers"));
					boolGenerateViolationIdentifiers = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("GenerateViolationIdentifiers"));
					boolPromptInspectionSchedule = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("PromptInspectionSchedule"));
					boolAutoAddInspectionsToCalendar = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("AutoAddInspectionToCalendar"));
				}
				else
				{
					boolUseRE = false;
					intPermitIdentifierFormat = FCConvert.ToInt32(IdentifierFormat.NumberOnly);
					intPermitIdentifierValidation = FCConvert.ToInt32(IdentifierValidation.TotallyUnique);
					boolGeneratePermitIdentifiers = false;
					boolGenerateViolationIdentifiers = true;
					boolPromptInspectionSchedule = false;
					boolAutoAddInspectionsToCalendar = false;
					intViolationIdentifierFormat = FCConvert.ToInt32(IdentifierFormat.yeardashnumber);
					intViolationIdentifierValidation = FCConvert.ToInt32(IdentifierValidation.TotallyUnique);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReLoad", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
