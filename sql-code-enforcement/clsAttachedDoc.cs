﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWCE0000
{
	public class clsAttachedDoc
	{
		//=========================================================
		private string strFileName = "";
		private string strDescription = "";
		private int lngAutoID;
		private int lngRefID;
		private string referenceType;
		private bool boolChanged;

		public string FileName
		{
			set
			{
				if (strFileName != value)
					boolChanged = true;
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public string Description
		{
			set
			{
				if (strDescription != value)
					boolChanged = true;
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int ID
		{
			set
			{
				if (lngAutoID != value)
					boolChanged = true;
				lngAutoID = value;
			}
			get
			{
				int ID = 0;
				ID = lngAutoID;
				return ID;
			}
		}

		public int ReferenceID
		{
			set
			{
				if (lngRefID != value)
					boolChanged = true;
				lngRefID = value;
			}
			get
			{
				int ReferenceID = 0;
				ReferenceID = lngRefID;
				return ReferenceID;
			}
		}

		public string ReferenceType
		{
			set
			{
				if (referenceType != value)
					boolChanged = true;
				referenceType = value;
			}
			get
			{
				return referenceType;
			}
		}

		public bool Changed
		{
			set
			{
				boolChanged = value;
			}
			get
			{
				return boolChanged;
			}
		}
	}
}
