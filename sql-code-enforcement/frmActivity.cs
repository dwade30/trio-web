//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using SharedApplication;
using SharedApplication.CentralDocuments.Commands;
using TWSharedLibrary;

namespace TWCE0000
{
	public partial class frmActivity : BaseForm
	{
		public frmActivity()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmActivity InstancePtr
		{
			get
			{
				return (frmActivity)Sys.GetInstance(typeof(frmActivity));
			}
		}

		protected frmActivity _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private clsActivity alActivity = new clsActivity();
		private bool boolCantEdit;
		private clsAttachedDocuments dlAttachedDocs = new clsAttachedDocuments();
		const int CNSTGRIDDOCSCOLAUTOID = 0;
		const int CNSTGRIDDOCSCOLDESCRIPTION = 1;
		const int CNSTGRIDDOCSCOLFILENAME = 2;

		private void FillcmbActivity()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp;
				strTemp = "";
				cmbActivityType.Clear();
				cmbActivityType.AddItem("Other");
				cmbActivityType.ItemData(cmbActivityType.NewIndex, modCEConstants.CNSTACTIVITYTYPEUNKNOWN);
				cmbActivityType.AddItem("Initial");
				cmbActivityType.ItemData(cmbActivityType.NewIndex, modCEConstants.CNSTACTIVITYTYPEPERMITCREATION);
				cmbActivityType.AddItem("Inspection");
				cmbActivityType.ItemData(cmbActivityType.NewIndex, modCEConstants.CNSTACTIVITYTYPEINSPECTION);
				rsLoad.OpenRecordset("select * from systemcodes where editable = 1 and CODETYpe = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEACTIVITYTYPE) + " order by code ", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					cmbActivityType.AddItem(rsLoad.Get_Fields_String("Description"));
					cmbActivityType.ItemData(cmbActivityType.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")));
					rsLoad.MoveNext();
				}
				cmbActivityType.SelectedIndex = 0;
				txtType.Text = "Other";
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillcmbActivity", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmbActivityType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbActivityType.ItemData(cmbActivityType.SelectedIndex) == modCEConstants.CNSTACTIVITYTYPEUNKNOWN)
			{
				lblType.Visible = true;
				txtType.Visible = true;
			}
			else
			{
				lblType.Visible = false;
				txtType.Text = cmbActivityType.Text;
				txtType.Visible = false;
			}
		}

		private void frmActivity_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmActivity_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmActivity properties;
			//frmActivity.FillStyle	= 0;
			//frmActivity.ScaleWidth	= 9300;
			//frmActivity.ScaleHeight	= 7920;
			//frmActivity.LinkTopic	= "Form2";
			//frmActivity.LockControls	= -1  'True;
			//frmActivity.PaletteMode	= 1  'UseZOrder;
			//rtbDetail properties;
			//rtbDetail.BorderStyle	= 0;
			//rtbDetail.ScrollBars	= 2;
			//rtbDetail.MaxLength	= 63000;
			//rtbDetail.AutoVerbMenu	= -1  'True;
			//rtbDetail.TextRTF	= $"frmActivity.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridDocuments();
			if (boolCantEdit)
			{
				txtType.Enabled = false;
				txtDescription.Enabled = false;
				t2kDate.Enabled = false;
				rtbDetail.Locked = true;
				cmdAttachDocument.Visible = false;
				cmdRemoveDocument.Visible = false;
			}
			FillcmbActivity();
			int x;
			for (x = 0; x <= cmbActivityType.Items.Count - 1; x++)
			{
				if (cmbActivityType.ItemData(x) == alActivity.ReferenceType)
				{
					cmbActivityType.SelectedIndex = x;
					break;
				}
			}
			// x
			FillGridDocuments();
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(double, int)
		public int Init(ref bool boolReadOnly, int lngID = 0, int lngParentID = 0, int lngParentType = 0, int lngReferenceID = 0, int lngReferenceType = 0, int lngAccount = 0)
		{
			int Init = 0;
			boolCantEdit = boolReadOnly;
			if (lngID > 0)
			{
				alActivity.ID = lngID;
				alActivity.LoadActivity(lngID);
				LoadActivity();
				if (!boolCantEdit)
				{
					cmdAttachDocument.Enabled = true;
				}
			}
			else
			{
				alActivity.Clear();
				alActivity.ParentID = lngParentID;
				alActivity.ParentType = lngParentType;
				alActivity.ReferenceID = lngReferenceID;
				alActivity.ReferenceType = lngReferenceType;
				alActivity.Account = lngAccount;
				cmdAttachDocument.Enabled = false;
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = alActivity.ID;
			return Init;
		}

		private void LoadActivity()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				t2kDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtDescription.Text = "";
				txtType.Text = "";
				rtbDetail.Text = "";
				if (alActivity.ActivityDate.ToOADate() != 0)
				{
					t2kDate.Text = Strings.Format(alActivity.ActivityDate, "MM/dd/yyyy");
				}
				else
				{
					t2kDate.Text = "";
				}
				txtType.Text = alActivity.ActivityType;
				txtDescription.Text = alActivity.Description;
				rtbDetail.Text = alActivity.Detail;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadActivity", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveActivity()
		{
			bool SaveActivity = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: lngReturn As int	OnWriteFCConvert.ToInt32(
				int lngReturn = 0;
				SaveActivity = false;
				if (!boolCantEdit)
				{
					if (!Information.IsDate(t2kDate.Text))
					{
						MessageBox.Show("The activity date must be a valid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveActivity;
					}
					// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
					int x;
					if (gridDelete.Rows > 0)
					{
						for (x = 0; x <= (gridDelete.Rows - 1); x++)
						{
							dlAttachedDocs.DeleteByID(FCConvert.ToInt32(Conversion.Val(gridDelete.TextMatrix(x, 0))));
						}
						// x
						gridDelete.Rows = 0;
					}
					alActivity.ActivityDate = FCConvert.ToDateTime(t2kDate.Text);
					alActivity.ActivityType = txtType.Text;
					alActivity.Description = txtDescription.Text;
					alActivity.Detail = rtbDetail.Text;
					if (alActivity.ReferenceType != cmbActivityType.ItemData(cmbActivityType.SelectedIndex))
						alActivity.ReferenceID = 0;
					alActivity.ReferenceType = cmbActivityType.ItemData(cmbActivityType.SelectedIndex);
					lngReturn = alActivity.SaveActivity();
					if (lngReturn != 0)
					{
						fecherFoundation.Information.Err().Raise(lngReturn, null, null, null, null);
					}
				}
				SaveActivity = true;
				return SaveActivity;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveActivity", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveActivity;
		}

		private bool CheckForChanges()
		{
			bool CheckForChanges = false;
			CheckForChanges = false;
			bool boolReturn;
			boolReturn = false;
			if (txtDescription.Text != alActivity.Description)
			{
				boolReturn = true;
			}
			else if (txtType.Text != alActivity.ActivityType)
			{
				boolReturn = true;
			}
			else if (rtbDetail.Text != alActivity.Detail)
			{
				boolReturn = true;
			}
			else
			{
				if (Information.IsDate(t2kDate.Text))
				{
					if (alActivity.ActivityDate.ToOADate() != Convert.ToDateTime(t2kDate.Text).ToOADate())
					{
						boolReturn = true;
					}
				}
				else
				{
					if (alActivity.ActivityDate.ToOADate() != 0)
					{
						boolReturn = true;
					}
				}
			}
			CheckForChanges = boolReturn;
			return CheckForChanges;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (!boolCantEdit)
			{
				if (CheckForChanges())
				{
					if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (!SaveActivity())
						{
							e.Cancel = true;
						}
					}
				}
			}
		}

		private void frmActivity_Resize(object sender, System.EventArgs e)
		{
			ResizeGridDocuments();
		}

		private void gridDocuments_DblClick(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = gridDocuments.MouseRow;
			if (lngRow > 0)
			{
                FCFileSystem fso = new FCFileSystem();
				if (FCFileSystem.FileExists(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLFILENAME)))
				{
					//frmViewDocuments.InstancePtr.Unload();
                    //App.DoEvents();
                    var id = (int) Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID));

                    //frmViewDocuments.InstancePtr.Init((int) Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID)), "", modCEConstants.CNSTCODETYPEVIOLATION.ToString(), alActivity.ID, "", modGlobalVariables.Statics.strCEDatabase, FCForm.FormShowEnum.Modal);
                    StaticSettings.GlobalCommandDispatcher.Send(new ShowSingleDocument(id, "", "CodeEnforcement", "Activity",
                        alActivity.ID, "", true, false));
                }
				else
				{
					MessageBox.Show("Unable to locate file", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void gridDocuments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddDoc();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (gridDocuments.Row > 0)
						{
							RemoveDoc();
						}
						break;
					}
			}
			//end switch
		}

		private void RemoveDoc()
		{
			int lngRow;
			lngRow = gridDocuments.Row;
			if (lngRow > 0)
			{
				dlAttachedDocs.RemovebyID(FCConvert.ToInt32(Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID))));
				if (Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID)) > 0)
				{
					gridDelete.AddItem(FCConvert.ToString(Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID))));
				}
				gridDocuments.RemoveItem(lngRow);
			}
		}

		private void mnuAttachDoc_Click(object sender, System.EventArgs e)
		{
			AddDoc();
		}

		private void AddDoc()
		{
			if (!boolCantEdit)
			{
				if (alActivity.ID > 0)
				{
                    StaticSettings.GlobalCommandDispatcher.Send(new ShowAddCentralDocument("CodeEnforcement", "Activity", alActivity.ID, ""));
					FillGridDocuments();                    
                }
				else
				{
					MessageBox.Show("You must save this activity before you can add documents", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptActivity.InstancePtr.Init(0, 0, true, "", 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal), alActivity);
		}

		private void mnuRemoveDocument_Click(object sender, System.EventArgs e)
		{
			if (!boolCantEdit)
			{
				RemoveDoc();
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (SaveActivity())
			{
				if (!boolCantEdit)
				{
					cmdAttachDocument.Enabled = true;
				}
				MessageBox.Show("Activity Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveActivity())
			{
				Close();
			}
		}

		private void FillGridDocuments()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngReturn;
				int lngRow;
				clsAttachedDoc tDoc;
				lngReturn = dlAttachedDocs.LoadDocuments(alActivity.ID, modCEConstants.CNSTCODETYPEACTIVITY.ToString(), modGlobalVariables.Statics.strCEDatabase);
				if (lngReturn != 0)
				{
					fecherFoundation.Information.Err().Raise(lngReturn, null, null, null, null);
				}
				gridDocuments.Rows = 1;
				dlAttachedDocs.MoveFirst();
				dlAttachedDocs.MovePrevious();
				while (dlAttachedDocs.MoveNext() > 0)
				{
					tDoc = dlAttachedDocs.GetCurrentDoc();
					if (!(tDoc == null))
					{
						gridDocuments.Rows += 1;
						lngRow = gridDocuments.Rows - 1;
						gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID, FCConvert.ToString(tDoc.ID));
						gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLDESCRIPTION, tDoc.Description);
						gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLFILENAME, tDoc.FileName);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridDocuments", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridDocuments()
		{
			gridDocuments.ColHidden(CNSTGRIDDOCSCOLAUTOID, true);
			gridDocuments.TextMatrix(0, CNSTGRIDDOCSCOLDESCRIPTION, "Description");
			gridDocuments.TextMatrix(0, CNSTGRIDDOCSCOLFILENAME, "File Name");
		}

		private void ResizeGridDocuments()
		{
			int GridWidth = 0;
			GridWidth = gridDocuments.WidthOriginal;
			gridDocuments.ColWidth(CNSTGRIDDOCSCOLDESCRIPTION, FCConvert.ToInt32(0.6 * GridWidth));
		}

		private void mnuViewDocuments_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		private void ViewDocs()
		{
			//frmViewDocuments.InstancePtr.Unload();
			////App.DoEvents();
			//frmViewDocuments.InstancePtr.Init(0, "", modCEConstants.CNSTCODETYPEVIOLATION.ToString(), alActivity.ID, "", modGlobalVariables.Statics.strCEDatabase, FCForm.FormShowEnum.Modal, boolCantEdit);
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer( "", "CodeEnforcement", "Activity",
                alActivity.ID, "", true, boolCantEdit));
            FillGridDocuments();
		}
	}
}
