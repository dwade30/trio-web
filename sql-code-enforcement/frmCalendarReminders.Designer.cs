//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmCalendarReminders.
	/// </summary>
	partial class frmCalendarReminders
	{
		public fecherFoundation.FCButton btnDismissAll;
		public fecherFoundation.FCButton btnOpenItem;
		public fecherFoundation.FCButton btnDismiss;
		public fecherFoundation.FCButton btnSnooze;
		public fecherFoundation.FCComboBox cmbSnooze;
		public fecherFoundation.FCListView ctrlReminders;
		private Wisej.Web.ColumnHeader ctrlRemindersColumnHeader0;
		private Wisej.Web.ColumnHeader ctrlRemindersColumnHeader1;
		private Wisej.Web.ColumnHeader ctrlRemindersColumnHeader2;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel txtDescription1;
		public fecherFoundation.FCLabel txtDescription2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnDismissAll = new fecherFoundation.FCButton();
            this.btnOpenItem = new fecherFoundation.FCButton();
            this.btnDismiss = new fecherFoundation.FCButton();
            this.btnSnooze = new fecherFoundation.FCButton();
            this.cmbSnooze = new fecherFoundation.FCComboBox();
            this.ctrlReminders = new fecherFoundation.FCListView();
            this.ctrlRemindersColumnHeader1 = new Wisej.Web.ColumnHeader();
            this.ctrlRemindersColumnHeader2 = new Wisej.Web.ColumnHeader();
            this.ctrlRemindersColumnHeader0 = new Wisej.Web.ColumnHeader();
            this.Label2 = new fecherFoundation.FCLabel();
            this.txtDescription1 = new fecherFoundation.FCLabel();
            this.txtDescription2 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDismissAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDismiss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSnooze)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 490);
            this.BottomPanel.Size = new System.Drawing.Size(791, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnDismissAll);
            this.ClientArea.Controls.Add(this.btnOpenItem);
            this.ClientArea.Controls.Add(this.btnDismiss);
            this.ClientArea.Controls.Add(this.btnSnooze);
            this.ClientArea.Controls.Add(this.cmbSnooze);
            this.ClientArea.Controls.Add(this.ctrlReminders);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.txtDescription1);
            this.ClientArea.Controls.Add(this.txtDescription2);
            this.ClientArea.Size = new System.Drawing.Size(791, 430);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(791, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(131, 30);
            this.HeaderText.Text = "Reminders";
            // 
            // btnDismissAll
            // 
            this.btnDismissAll.AppearanceKey = "actionButton";
            this.btnDismissAll.Location = new System.Drawing.Point(30, 280);
            this.btnDismissAll.Name = "btnDismissAll";
            this.btnDismissAll.Size = new System.Drawing.Size(131, 40);
            this.btnDismissAll.TabIndex = 7;
            this.btnDismissAll.Text = "Dismiss All";
            this.btnDismissAll.Click += new System.EventHandler(this.btnDismissAll_Click);
            // 
            // btnOpenItem
            // 
            this.btnOpenItem.AppearanceKey = "actionButton";
            this.btnOpenItem.Location = new System.Drawing.Point(181, 280);
            this.btnOpenItem.Name = "btnOpenItem";
            this.btnOpenItem.Size = new System.Drawing.Size(126, 40);
            this.btnOpenItem.TabIndex = 6;
            this.btnOpenItem.Text = "Open Item";
            this.btnOpenItem.Click += new System.EventHandler(this.btnOpenItem_Click);
            // 
            // btnDismiss
            // 
            this.btnDismiss.AppearanceKey = "actionButton";
            this.btnDismiss.Location = new System.Drawing.Point(327, 280);
            this.btnDismiss.Name = "btnDismiss";
            this.btnDismiss.Size = new System.Drawing.Size(100, 40);
            this.btnDismiss.TabIndex = 5;
            this.btnDismiss.Text = "Dismiss";
            this.btnDismiss.Click += new System.EventHandler(this.btnDismiss_Click);
            // 
            // btnSnooze
            // 
            this.btnSnooze.AppearanceKey = "actionButton";
            this.btnSnooze.Location = new System.Drawing.Point(353, 367);
            this.btnSnooze.Name = "btnSnooze";
            this.btnSnooze.Size = new System.Drawing.Size(100, 40);
            this.btnSnooze.TabIndex = 4;
            this.btnSnooze.Text = "Snooze";
            this.btnSnooze.Click += new System.EventHandler(this.btnSnooze_Click);
            // 
            // cmbSnooze
            // 
            this.cmbSnooze.AutoSize = false;
            this.cmbSnooze.BackColor = System.Drawing.SystemColors.Window;
            this.cmbSnooze.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSnooze.FormattingEnabled = true;
            this.cmbSnooze.Location = new System.Drawing.Point(30, 367);
            this.cmbSnooze.Name = "cmbSnooze";
            this.cmbSnooze.Size = new System.Drawing.Size(303, 40);
            this.cmbSnooze.TabIndex = 3;
            // 
            // ctrlReminders
            // 
            this.ctrlReminders.BackColor = System.Drawing.SystemColors.Window;
            this.ctrlReminders.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
            this.ctrlRemindersColumnHeader1,
            this.ctrlRemindersColumnHeader2});
            this.ctrlReminders.HeaderStyle = Wisej.Web.ColumnHeaderStyle.None;
            this.ctrlReminders.Location = new System.Drawing.Point(30, 150);
            this.ctrlReminders.Name = "ctrlReminders";
            this.ctrlReminders.OLEDragMode = fecherFoundation.FCListView.OLEDragConstants.ccOLEDragManual;
            this.ctrlReminders.OLEDropMode = fecherFoundation.FCListView.OLEDragConstants.ccOLEDragManual;
            this.ctrlReminders.Size = new System.Drawing.Size(590, 110);
            this.ctrlReminders.SortOrder = Wisej.Web.SortOrder.None;
            this.ctrlReminders.TabIndex = 0;
            this.ctrlReminders.View = Wisej.Web.View.Details;
            this.ctrlReminders.SelectedIndexChanged += new System.EventHandler(this.ctrlReminders_SelectedIndexChanged);
            // 
            // ctrlRemindersColumnHeader1
            // 
            this.ctrlRemindersColumnHeader1.Name = "ctrlRemindersColumnHeader1";
            this.ctrlRemindersColumnHeader1.Text = "Subject";
            this.ctrlRemindersColumnHeader1.Width = 204;
            // 
            // ctrlRemindersColumnHeader2
            // 
            this.ctrlRemindersColumnHeader2.Name = "ctrlRemindersColumnHeader2";
            this.ctrlRemindersColumnHeader2.Text = "Due In";
            this.ctrlRemindersColumnHeader2.Width = 195;
            // 
            // ctrlRemindersColumnHeader0
            // 
            this.ctrlRemindersColumnHeader0.Name = "ctrlRemindersColumnHeader0";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 340);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(267, 17);
            this.Label2.TabIndex = 8;
            this.Label2.Text = "CLICK SNOOZE TO BE REMINDED AGAIN IN: ";
            // 
            // txtDescription1
            // 
            this.txtDescription1.Location = new System.Drawing.Point(30, 30);
            this.txtDescription1.Name = "txtDescription1";
            this.txtDescription1.Size = new System.Drawing.Size(373, 40);
            this.txtDescription1.TabIndex = 2;
            this.txtDescription1.Text = "LABEL1";
            // 
            // txtDescription2
            // 
            this.txtDescription2.Location = new System.Drawing.Point(30, 90);
            this.txtDescription2.Name = "txtDescription2";
            this.txtDescription2.Size = new System.Drawing.Size(373, 40);
            this.txtDescription2.TabIndex = 1;
            this.txtDescription2.Text = "LABEL1";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Text = "Save & Continue";
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmCalendarReminders
            // 
            this.AcceptButton = this.btnSnooze;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(791, 490);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCalendarReminders";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reminders";
            this.Load += new System.EventHandler(this.frmCalendarReminders_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCalendarReminders_KeyDown);
            this.Resize += new System.EventHandler(this.frmCalendarReminders_Resize);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnDismissAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnDismiss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSnooze)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}