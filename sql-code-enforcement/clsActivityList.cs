//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCE0000
{
	public class clsActivityList
	{
		//=========================================================
		private FCCollection alList = new FCCollection();
		private int intCurrentIndex;

		public void Clear()
		{
			int x;
			//FC:FINAL:MSH - i.issue #1746: wrong removing items from list (each time will be deleted only half items)
			int count = alList.Count;
			if (!FCUtils.IsEmpty(alList))
			{
				//for (x = 1; x <= alList.Count; x++)
				for (x = 1; x <= count; x++)
				{
					alList.Remove(1);
				}
				// x
			}
			intCurrentIndex = -1;
		}

		public int LoadList(int lngAccount = 0, int lngParentType = 0, int lngParentID = 0, int lngRefType = 0, string strActivityType = "", DateTime? tempdtStart = null, DateTime? tempdtEnd = null)
		{
			//FC:FINAL:DDU:#i2092 - changed default paramenter because run-time error
			if (tempdtStart == null)
			{
				tempdtStart = DateTime.FromOADate(0);
			}
			DateTime dtStart = tempdtStart.Value;
			if (tempdtEnd == null)
			{
				tempdtEnd = DateTime.FromOADate(0);
			}
			DateTime dtEnd = tempdtEnd.Value;
			int LoadList = 0;
			LoadList = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Clear();
				string strSQL;
				string strWhere;
				string strAnd;
				strWhere = "";
				strSQL = "";
				strAnd = "";
				if (lngAccount > 0)
				{
					strWhere += strAnd + " account = " + FCConvert.ToString(lngAccount);
					strAnd = " and ";
				}
				if (lngParentID > 0)
				{
					strWhere += strAnd + " parenttype = " + FCConvert.ToString(lngParentType) + " and parentid = " + FCConvert.ToString(lngParentID);
					strAnd = " and ";
				}
				else if (lngParentType > 0)
				{
					strWhere += strAnd + " parenttype = " + FCConvert.ToString(lngParentType);
					strAnd = " and ";
				}
				if (lngRefType > 0)
				{
					strWhere += strAnd + " referencetype = " + FCConvert.ToString(lngRefType);
					strAnd = " and ";
				}
				if (strActivityType != "")
				{
					strWhere += strAnd + " activitytype = '" + strActivityType + "'";
					strAnd = " and ";
				}
				if (dtStart.ToOADate() != 0 || dtEnd.ToOADate() != 0)
				{
					if (dtStart.ToOADate() != 0)
					{
						if (dtEnd.ToOADate() != 0)
						{
							strWhere += strAnd + " activitydate >= '" + FCConvert.ToString(dtStart) + "' and activitydate <= '" + FCConvert.ToString(dtEnd) + "'";
						}
						else
						{
							strWhere += strAnd + " activitydate >= '" + FCConvert.ToString(dtStart) + "'";
						}
					}
					else
					{
						strWhere += strAnd + " activitydate <= '" + FCConvert.ToString(dtEnd) + "'";
					}
					strAnd = " and ";
				}
				if (strWhere != string.Empty)
				{
					strWhere = " where " + strWhere;
				}
				strSQL = "select * from activitylog " + strWhere + " order by activitydate desc";
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					AddActivity(rsLoad.Get_Fields_Int32("ID"), rsLoad.Get_Fields("account"), rsLoad.Get_Fields("parentid"), rsLoad.Get_Fields_Int32("parenttype"), rsLoad.Get_Fields_DateTime("activitydate"), rsLoad.Get_Fields_String("activitytype"), rsLoad.Get_Fields_String("description"), rsLoad.Get_Fields_String("detail"), rsLoad.Get_Fields_Int32("referenceid"), rsLoad.Get_Fields("referencetype"));
					rsLoad.MoveNext();
				}
				return LoadList;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				LoadList = fecherFoundation.Information.Err(ex).Number;
			}
			return LoadList;
		}

		public void AddActivity(int lngAutoID, int lngAccount, int lngParentID, int lngParentType, DateTime dtActivityDate, string strType, string strDescription, string strDetail, int lngReferenceID, int lngRefType)
		{
			clsActivity tAct = new clsActivity();
			tAct.Account = lngAccount;
			tAct.ActivityDate = dtActivityDate;
			tAct.ActivityType = strType;
			tAct.ID = lngAutoID;
			tAct.Description = strDescription;
			tAct.Detail = strDetail;
			tAct.ParentID = lngParentID;
			tAct.ParentType = lngParentType;
			tAct.ReferenceID = lngReferenceID;
			tAct.ReferenceType = lngRefType;
			InsertActivity(ref tAct);
		}

		private void InsertActivity(ref clsActivity tAct)
		{
			alList.Add(tAct, "ID" + tAct.ID);
		}

		public int MoveFirst()
		{
			int MoveFirst = 0;
			int intReturn;
			intCurrentIndex = 0;
			intReturn = MoveNext();
			MoveFirst = intReturn;
			return MoveFirst;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(alList))
			{
				if (alList.Count >= intCurrentIndex + 1 && alList.Count > 0)
				{
					while (intCurrentIndex <= alList.Count)
					{
						intCurrentIndex += 1;
						if (!(alList[intCurrentIndex] == null))
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
					if (intCurrentIndex > alList.Count)
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MoveNext = intReturn;
			return MoveNext;
		}

		public int MovePrevious()
		{
			int MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(alList))
			{
				if (alList.Count > 0)
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex > 0)
						{
							if (!(alList[intCurrentIndex] == null))
							{
								intReturn = intCurrentIndex;
							}
						}
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MovePrevious = intReturn;
			return MovePrevious;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = intCurrentIndex;
			return GetCurrentIndex;
		}

		public clsActivity GetCurrentActivity()
		{
			clsActivity GetCurrentActivity = null;
			clsActivity tAct;
			tAct = null;
			if (!FCUtils.IsEmpty(alList))
			{
				if (intCurrentIndex > 0)
				{
					if (!(alList[intCurrentIndex] == null))
					{
						tAct = alList[intCurrentIndex];
					}
				}
			}
			GetCurrentActivity = tAct;
			return GetCurrentActivity;
		}
		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		public clsActivity GetActivityByID(int lngID)
		{
			clsActivity GetActivityByID = null;
			clsActivity tAct;
			tAct = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!FCUtils.IsEmpty(alList))
				{
					tAct = (clsActivity)alList["ID" + lngID];
				}
				GetActivityByID = tAct;
				return GetActivityByID;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				GetActivityByID = null;
			}
			return GetActivityByID;
		}

		public void RemoveByIndex(ref int intIndex)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!FCUtils.IsEmpty(alList))
				{
					if (alList.Count >= intIndex)
					{
						alList.Remove(intIndex);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}
		// vbPorter upgrade warning: lngID As int	OnWriteFCConvert.ToDouble(
		public void RemovebyID(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!FCUtils.IsEmpty(alList))
				{
					alList.Remove("ID" + FCConvert.ToString(lngID));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		~clsActivityList()
		{
			Clear();
		}
	}
}
