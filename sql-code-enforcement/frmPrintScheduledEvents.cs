//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmPrintScheduledEvents : BaseForm
	{
		public frmPrintScheduledEvents()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPrintScheduledEvents InstancePtr
		{
			get
			{
				return (frmPrintScheduledEvents)Sys.GetInstance(typeof(frmPrintScheduledEvents));
			}
		}

		protected frmPrintScheduledEvents _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmPrintScheduledEvents_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPrintScheduledEvents_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrintScheduledEvents properties;
			//frmPrintScheduledEvents.FillStyle	= 0;
			//frmPrintScheduledEvents.ScaleWidth	= 5880;
			//frmPrintScheduledEvents.ScaleHeight	= 4425;
			//frmPrintScheduledEvents.LinkTopic	= "Form2";
			//frmPrintScheduledEvents.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			dtpTo.Value = DateTime.Now;
			dtpFrom.Value = dtpTo.Value;
			FillUserList();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillUserList()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			lstUsers.Clear();
			rsInfo.OpenRecordset("SELECT * FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' ORDER BY UserID", "ClientSettings");
			while (!rsInfo.EndOfFile())
			{
				lstUsers.AddItem(FCConvert.ToString(rsInfo.Get_Fields("UserID")));
				if (rsInfo.Get_Fields("userid") == modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID())
				{
					lstUsers.SetSelected(lstUsers.ListCount - 1, true);
				}
				rsInfo.MoveNext();
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			// print
			if (dtpTo.Value.Year < dtpFrom.Value.Year)
			{
				MessageBox.Show("Invalid Date Range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				if (dtpTo.Value.Year == dtpFrom.Value.Year)
				{
					if (dtpTo.Value.Month < dtpFrom.Value.Month)
					{
						MessageBox.Show("Invalid Date Range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else
					{
						if (dtpTo.Value.Month == dtpFrom.Value.Month)
						{
							if (dtpTo.Value.Day < dtpFrom.Value.Day)
							{
								MessageBox.Show("Invalid Date Range", "Invalid Date Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
					}
				}
			}
			FCCollection clUsers = new FCCollection();
			int x;
			for (x = 0; x <= lstUsers.Items.Count - 1; x++)
			{
				if (lstUsers.Selected(x))
				{
					clUsers.Add(lstUsers.Items[x].Text);
				}
			}
			// x
			if (clUsers.Count < 1)
			{
				MessageBox.Show("You must select at least one user", "No Users Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			bool boolIncludePrivate = false;
			if (chkIncludePrivate.CheckState == Wisej.Web.CheckState.Checked)
			{
				boolIncludePrivate = true;
			}
			else
			{
				boolIncludePrivate = false;
			}
			string strFrom;
			string strTo;
			strFrom = FCConvert.ToString(dtpFrom.Value.Month) + "/" + FCConvert.ToString(dtpFrom.Value.Day) + "/" + FCConvert.ToString(dtpFrom.Value.Year) + " 12:00:00 AM";
			strTo = FCConvert.ToString(dtpTo.Value.Month) + "/" + FCConvert.ToString(dtpTo.Value.Day) + "/" + FCConvert.ToString(dtpTo.Value.Year) + " 11:59:59 PM";
			string strOrder = "";
			if (cmbUser.Text == "User")
			{
				strOrder = "User";
			}
			else
			{
				strOrder = "Date";
			}
			rptScheduledEvents.InstancePtr.Init(ref clUsers, strFrom, strTo, boolIncludePrivate, strOrder);
		}
	}
}
