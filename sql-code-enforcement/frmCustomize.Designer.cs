//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmCustomize.
	/// </summary>
	partial class frmCustomize
	{
		public fecherFoundation.FCComboBox cmbIdentifier;
		public fecherFoundation.FCComboBox cmbIdentifierType;
		public fecherFoundation.FCLabel lblIdentifierType;
		public fecherFoundation.FCComboBox cmbPermitIdentifier;
		public fecherFoundation.FCLabel lblPermitIdentifier;
		public fecherFoundation.FCComboBox cmbPermitIdentFormat;
		public fecherFoundation.FCLabel lblPermitIdentFormat;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkPermitNumber;
		public fecherFoundation.FCCheckBox chkPermitIdentifier;
		public fecherFoundation.FCCheckBox chkPlan;
		public fecherFoundation.FCCheckBox chkStart;
		public fecherFoundation.FCCheckBox chkComplete;
		public fecherFoundation.FCCheckBox chkPermitStatus;
		public fecherFoundation.FCCheckBox chkApplicationDate;
		public fecherFoundation.FCCheckBox chkPermitValue;
		public fecherFoundation.FCCheckBox chkPermitFee;
		public fecherFoundation.FCCheckBox chkPermitType;
		public FCGrid GridPermits;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkGenerateViolationIdentifier;
		public fecherFoundation.FCComboBox cmbFormatViolationNumber;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkAddInspectionsToCalendar;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCCheckBox chkGeneratePermitIdentifier;
		public fecherFoundation.FCComboBox cmbFormatPermitNumber;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCCheckBox chkInspectionPrompt;
		public fecherFoundation.FCComboBox cmbFirstMonth;
		public fecherFoundation.FCComboBox cmbYearCalc;
		public fecherFoundation.FCCheckBox chkResetNumber;
		public fecherFoundation.FCCheckBox chk2DigitYear;
		public fecherFoundation.FCCheckBox chkUseRE;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbIdentifier = new fecherFoundation.FCComboBox();
			this.cmbIdentifierType = new fecherFoundation.FCComboBox();
			this.lblIdentifierType = new fecherFoundation.FCLabel();
			this.cmbPermitIdentifier = new fecherFoundation.FCComboBox();
			this.lblPermitIdentifier = new fecherFoundation.FCLabel();
			this.cmbPermitIdentFormat = new fecherFoundation.FCComboBox();
			this.lblPermitIdentFormat = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkPermitNumber = new fecherFoundation.FCCheckBox();
			this.chkPermitIdentifier = new fecherFoundation.FCCheckBox();
			this.chkPlan = new fecherFoundation.FCCheckBox();
			this.chkStart = new fecherFoundation.FCCheckBox();
			this.chkComplete = new fecherFoundation.FCCheckBox();
			this.chkPermitStatus = new fecherFoundation.FCCheckBox();
			this.chkApplicationDate = new fecherFoundation.FCCheckBox();
			this.chkPermitValue = new fecherFoundation.FCCheckBox();
			this.chkPermitFee = new fecherFoundation.FCCheckBox();
			this.chkPermitType = new fecherFoundation.FCCheckBox();
			this.GridPermits = new fecherFoundation.FCGrid();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.lblIdentifier = new fecherFoundation.FCLabel();
			this.chkGenerateViolationIdentifier = new fecherFoundation.FCCheckBox();
			this.cmbFormatViolationNumber = new fecherFoundation.FCComboBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.chkAddInspectionsToCalendar = new fecherFoundation.FCCheckBox();
			this.Frame6 = new fecherFoundation.FCFrame();
			this.chkGeneratePermitIdentifier = new fecherFoundation.FCCheckBox();
			this.cmbFormatPermitNumber = new fecherFoundation.FCComboBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.chkInspectionPrompt = new fecherFoundation.FCCheckBox();
			this.cmbFirstMonth = new fecherFoundation.FCComboBox();
			this.cmbYearCalc = new fecherFoundation.FCComboBox();
			this.chkResetNumber = new fecherFoundation.FCCheckBox();
			this.chk2DigitYear = new fecherFoundation.FCCheckBox();
			this.chkUseRE = new fecherFoundation.FCCheckBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitIdentifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPlan)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkComplete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApplicationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPermits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkGenerateViolationIdentifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAddInspectionsToCalendar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
			this.Frame6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkGeneratePermitIdentifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInspectionPrompt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkResetNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chk2DigitYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseRE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 621);
			this.BottomPanel.Size = new System.Drawing.Size(1110, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.Frame5);
			this.ClientArea.Controls.Add(this.cmbFirstMonth);
			this.ClientArea.Controls.Add(this.cmbYearCalc);
			this.ClientArea.Controls.Add(this.chkResetNumber);
			this.ClientArea.Controls.Add(this.chk2DigitYear);
			this.ClientArea.Controls.Add(this.chkUseRE);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Size = new System.Drawing.Size(1110, 561);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1110, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(128, 30);
			this.HeaderText.Text = "Customize";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbIdentifier
			// 
			this.cmbIdentifier.AutoSize = false;
			this.cmbIdentifier.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbIdentifier.FormattingEnabled = true;
			this.cmbIdentifier.Items.AddRange(new object[] {
            "Are unique",
            "Are unique to each type"});
			this.cmbIdentifier.Location = new System.Drawing.Point(125, 30);
			this.cmbIdentifier.Name = "cmbIdentifier";
			this.cmbIdentifier.Size = new System.Drawing.Size(224, 40);
			this.cmbIdentifier.TabIndex = 2;
			this.cmbIdentifier.Text = "Are unique";
			this.ToolTip1.SetToolTip(this.cmbIdentifier, null);
			// 
			// cmbIdentifierType
			// 
			this.cmbIdentifierType.AutoSize = false;
			this.cmbIdentifierType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbIdentifierType.FormattingEnabled = true;
			this.cmbIdentifierType.Items.AddRange(new object[] {
            "Number",
            "Year - Number",
            "Number - Year"});
			this.cmbIdentifierType.Location = new System.Drawing.Point(125, 80);
			this.cmbIdentifierType.Name = "cmbIdentifierType";
			this.cmbIdentifierType.Size = new System.Drawing.Size(224, 40);
			this.cmbIdentifierType.TabIndex = 0;
			this.cmbIdentifierType.Text = "Number";
			this.ToolTip1.SetToolTip(this.cmbIdentifierType, null);
			// 
			// lblIdentifierType
			// 
			this.lblIdentifierType.AutoSize = true;
			this.lblIdentifierType.Location = new System.Drawing.Point(20, 94);
			this.lblIdentifierType.Name = "lblIdentifierType";
			this.lblIdentifierType.Size = new System.Drawing.Size(60, 15);
			this.lblIdentifierType.TabIndex = 1;
			this.lblIdentifierType.Text = "FORMAT";
			this.ToolTip1.SetToolTip(this.lblIdentifierType, null);
			// 
			// cmbPermitIdentifier
			// 
			this.cmbPermitIdentifier.AutoSize = false;
			this.cmbPermitIdentifier.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPermitIdentifier.FormattingEnabled = true;
			this.cmbPermitIdentifier.Items.AddRange(new object[] {
            "Are unique",
            "Are unique to each type"});
			this.cmbPermitIdentifier.Location = new System.Drawing.Point(125, 30);
			this.cmbPermitIdentifier.Name = "cmbPermitIdentifier";
			this.cmbPermitIdentifier.Size = new System.Drawing.Size(224, 40);
			this.cmbPermitIdentifier.TabIndex = 2;
			this.cmbPermitIdentifier.Text = "Are unique";
			this.ToolTip1.SetToolTip(this.cmbPermitIdentifier, null);
			// 
			// lblPermitIdentifier
			// 
			this.lblPermitIdentifier.AutoSize = true;
			this.lblPermitIdentifier.Location = new System.Drawing.Point(20, 48);
			this.lblPermitIdentifier.Name = "lblPermitIdentifier";
			this.lblPermitIdentifier.Size = new System.Drawing.Size(84, 15);
			this.lblPermitIdentifier.TabIndex = 3;
			this.lblPermitIdentifier.Text = "IDENTIFIERS";
			this.ToolTip1.SetToolTip(this.lblPermitIdentifier, null);
			// 
			// cmbPermitIdentFormat
			// 
			this.cmbPermitIdentFormat.AutoSize = false;
			this.cmbPermitIdentFormat.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPermitIdentFormat.FormattingEnabled = true;
			this.cmbPermitIdentFormat.Items.AddRange(new object[] {
            "Number",
            "Year - Number",
            "Number - Year"});
			this.cmbPermitIdentFormat.Location = new System.Drawing.Point(125, 80);
			this.cmbPermitIdentFormat.Name = "cmbPermitIdentFormat";
			this.cmbPermitIdentFormat.Size = new System.Drawing.Size(224, 40);
			this.cmbPermitIdentFormat.TabIndex = 0;
			this.cmbPermitIdentFormat.Text = "Number";
			this.ToolTip1.SetToolTip(this.cmbPermitIdentFormat, null);
			// 
			// lblPermitIdentFormat
			// 
			this.lblPermitIdentFormat.AutoSize = true;
			this.lblPermitIdentFormat.Location = new System.Drawing.Point(20, 98);
			this.lblPermitIdentFormat.Name = "lblPermitIdentFormat";
			this.lblPermitIdentFormat.Size = new System.Drawing.Size(60, 15);
			this.lblPermitIdentFormat.TabIndex = 1;
			this.lblPermitIdentFormat.Text = "FORMAT";
			this.ToolTip1.SetToolTip(this.lblPermitIdentFormat, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkPermitNumber);
			this.Frame1.Controls.Add(this.chkPermitIdentifier);
			this.Frame1.Controls.Add(this.chkPlan);
			this.Frame1.Controls.Add(this.chkStart);
			this.Frame1.Controls.Add(this.chkComplete);
			this.Frame1.Controls.Add(this.chkPermitStatus);
			this.Frame1.Controls.Add(this.chkApplicationDate);
			this.Frame1.Controls.Add(this.chkPermitValue);
			this.Frame1.Controls.Add(this.chkPermitFee);
			this.Frame1.Controls.Add(this.chkPermitType);
			this.Frame1.Controls.Add(this.GridPermits);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(872, 359);
			this.Frame1.TabIndex = 31;
			this.Frame1.Text = "Property Screen Permit List";
			this.ToolTip1.SetToolTip(this.Frame1, "Customize the permit list display on the property screen");
			// 
			// chkPermitNumber
			// 
			this.chkPermitNumber.Location = new System.Drawing.Point(20, 30);
			this.chkPermitNumber.Name = "chkPermitNumber";
			this.chkPermitNumber.Size = new System.Drawing.Size(85, 27);
			this.chkPermitNumber.TabIndex = 41;
			this.chkPermitNumber.Text = "Number";
			this.ToolTip1.SetToolTip(this.chkPermitNumber, "System generated number made up of year and creation order number");
			this.chkPermitNumber.CheckedChanged += new System.EventHandler(this.chkPermitNumber_CheckedChanged);
			// 
			// chkPermitIdentifier
			// 
			this.chkPermitIdentifier.Location = new System.Drawing.Point(192, 30);
			this.chkPermitIdentifier.Name = "chkPermitIdentifier";
			this.chkPermitIdentifier.Size = new System.Drawing.Size(90, 27);
			this.chkPermitIdentifier.TabIndex = 40;
			this.chkPermitIdentifier.Text = "Identifier";
			this.ToolTip1.SetToolTip(this.chkPermitIdentifier, null);
			this.chkPermitIdentifier.CheckedChanged += new System.EventHandler(this.chkPermitIdentifier_CheckedChanged);
			// 
			// chkPlan
			// 
			this.chkPlan.Location = new System.Drawing.Point(322, 30);
			this.chkPlan.Name = "chkPlan";
			this.chkPlan.Size = new System.Drawing.Size(60, 27);
			this.chkPlan.TabIndex = 39;
			this.chkPlan.Text = "Plan";
			this.ToolTip1.SetToolTip(this.chkPlan, null);
			this.chkPlan.CheckedChanged += new System.EventHandler(this.chkPlan_CheckedChanged);
			// 
			// chkStart
			// 
			this.chkStart.Location = new System.Drawing.Point(192, 67);
			this.chkStart.Name = "chkStart";
			this.chkStart.Size = new System.Drawing.Size(101, 27);
			this.chkStart.TabIndex = 38;
			this.chkStart.Text = "Start Date";
			this.ToolTip1.SetToolTip(this.chkStart, null);
			this.chkStart.CheckedChanged += new System.EventHandler(this.chkStart_CheckedChanged);
			// 
			// chkComplete
			// 
			this.chkComplete.Location = new System.Drawing.Point(322, 67);
			this.chkComplete.Name = "chkComplete";
			this.chkComplete.Size = new System.Drawing.Size(150, 27);
			this.chkComplete.TabIndex = 37;
			this.chkComplete.Text = "Completion Date";
			this.ToolTip1.SetToolTip(this.chkComplete, null);
			this.chkComplete.CheckedChanged += new System.EventHandler(this.chkComplete_CheckedChanged);
			// 
			// chkPermitStatus
			// 
			this.chkPermitStatus.Location = new System.Drawing.Point(192, 104);
			this.chkPermitStatus.Name = "chkPermitStatus";
			this.chkPermitStatus.Size = new System.Drawing.Size(74, 27);
			this.chkPermitStatus.TabIndex = 36;
			this.chkPermitStatus.Text = "Status";
			this.ToolTip1.SetToolTip(this.chkPermitStatus, null);
			this.chkPermitStatus.CheckedChanged += new System.EventHandler(this.chkPermitStatus_CheckedChanged);
			// 
			// chkApplicationDate
			// 
			this.chkApplicationDate.Location = new System.Drawing.Point(20, 67);
			this.chkApplicationDate.Name = "chkApplicationDate";
			this.chkApplicationDate.Size = new System.Drawing.Size(147, 27);
			this.chkApplicationDate.TabIndex = 35;
			this.chkApplicationDate.Text = "Application Date";
			this.ToolTip1.SetToolTip(this.chkApplicationDate, null);
			this.chkApplicationDate.CheckedChanged += new System.EventHandler(this.chkApplicationDate_CheckedChanged);
			// 
			// chkPermitValue
			// 
			this.chkPermitValue.Location = new System.Drawing.Point(322, 104);
			this.chkPermitValue.Name = "chkPermitValue";
			this.chkPermitValue.Size = new System.Drawing.Size(69, 27);
			this.chkPermitValue.TabIndex = 34;
			this.chkPermitValue.Text = "Value";
			this.ToolTip1.SetToolTip(this.chkPermitValue, "Estimated Value");
			this.chkPermitValue.CheckedChanged += new System.EventHandler(this.chkPermitValue_CheckedChanged);
			// 
			// chkPermitFee
			// 
			this.chkPermitFee.Location = new System.Drawing.Point(20, 141);
			this.chkPermitFee.Name = "chkPermitFee";
			this.chkPermitFee.Size = new System.Drawing.Size(55, 27);
			this.chkPermitFee.TabIndex = 33;
			this.chkPermitFee.Text = "Fee";
			this.ToolTip1.SetToolTip(this.chkPermitFee, null);
			this.chkPermitFee.CheckedChanged += new System.EventHandler(this.chkPermitFee_CheckedChanged);
			// 
			// chkPermitType
			// 
			this.chkPermitType.Location = new System.Drawing.Point(20, 104);
			this.chkPermitType.Name = "chkPermitType";
			this.chkPermitType.Size = new System.Drawing.Size(63, 27);
			this.chkPermitType.TabIndex = 32;
			this.chkPermitType.Text = "Type";
			this.ToolTip1.SetToolTip(this.chkPermitType, null);
			this.chkPermitType.CheckedChanged += new System.EventHandler(this.chkPermitType_CheckedChanged);
			// 
			// GridPermits
			// 
			this.GridPermits.AllowSelection = false;
			this.GridPermits.AllowUserToResizeColumns = true;
			this.GridPermits.AllowUserToResizeRows = false;
			this.GridPermits.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridPermits.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridPermits.BackColorBkg = System.Drawing.Color.Empty;
			this.GridPermits.BackColorFixed = System.Drawing.Color.Empty;
			this.GridPermits.BackColorSel = System.Drawing.Color.Empty;
			this.GridPermits.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridPermits.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridPermits.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridPermits.ColumnHeadersHeight = 30;
			this.GridPermits.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridPermits.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridPermits.DragIcon = null;
			this.GridPermits.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridPermits.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.GridPermits.FixedCols = 0;
			this.GridPermits.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridPermits.FrozenCols = 0;
            this.GridPermits.FrozenRows = 0;
			this.GridPermits.GridColor = System.Drawing.Color.Empty;
			this.GridPermits.GridColorFixed = System.Drawing.Color.Empty;
			this.GridPermits.Location = new System.Drawing.Point(20, 177);
			this.GridPermits.Name = "GridPermits";
			this.GridPermits.OutlineCol = 0;
			this.GridPermits.ReadOnly = true;
			this.GridPermits.RowHeadersVisible = false;
			this.GridPermits.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridPermits.RowHeightMin = 0;
			this.GridPermits.Rows = 3;
			this.GridPermits.ScrollTipText = null;
			this.GridPermits.ShowColumnVisibilityMenu = false;
			this.GridPermits.ShowFocusCell = false;
			this.GridPermits.Size = new System.Drawing.Size(833, 133);
			this.GridPermits.StandardTab = true;
			this.GridPermits.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridPermits.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridPermits.TabIndex = 42;
			this.ToolTip1.SetToolTip(this.GridPermits, null);
			this.GridPermits.Resize += new System.EventHandler(this.GridPermits_AfterUserResize);
            this.GridPermits.AfterMoveColumn += new EventHandler<AfterMoveColumnEventArgs>(this.GridPermits_AfterMoveColumn);
			// 
			// Frame2
			// 
			this.Frame2.AppearanceKey = "groupBoxLeftBorder";
			this.Frame2.Controls.Add(this.Frame3);
			this.Frame2.Location = new System.Drawing.Point(30, 832);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(564, 273);
			this.Frame2.TabIndex = 19;
			this.Frame2.Text = "Violations";
			this.ToolTip1.SetToolTip(this.Frame2, null);
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.lblIdentifier);
			this.Frame3.Controls.Add(this.cmbIdentifierType);
			this.Frame3.Controls.Add(this.lblIdentifierType);
			this.Frame3.Controls.Add(this.cmbIdentifier);
			this.Frame3.Controls.Add(this.chkGenerateViolationIdentifier);
			this.Frame3.Controls.Add(this.cmbFormatViolationNumber);
			this.Frame3.Controls.Add(this.Label5);
			this.Frame3.Controls.Add(this.Label6);
			this.Frame3.Location = new System.Drawing.Point(20, 30);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(371, 224);
			this.Frame3.TabIndex = 20;
			this.Frame3.Text = "Identifiers";
			this.ToolTip1.SetToolTip(this.Frame3, null);
			// 
			// lblIdentifier
			// 
			this.lblIdentifier.AutoSize = true;
			this.lblIdentifier.Location = new System.Drawing.Point(20, 44);
			this.lblIdentifier.Name = "lblIdentifier";
			this.lblIdentifier.Size = new System.Drawing.Size(84, 15);
			this.lblIdentifier.TabIndex = 31;
			this.lblIdentifier.Text = "IDENTIFIERS";
			this.ToolTip1.SetToolTip(this.lblIdentifier, null);
			// 
			// chkGenerateViolationIdentifier
			// 
			this.chkGenerateViolationIdentifier.Location = new System.Drawing.Point(20, 130);
			this.chkGenerateViolationIdentifier.Name = "chkGenerateViolationIdentifier";
			this.chkGenerateViolationIdentifier.Size = new System.Drawing.Size(276, 27);
			this.chkGenerateViolationIdentifier.TabIndex = 26;
			this.chkGenerateViolationIdentifier.Text = "Generate when creating violations";
			this.ToolTip1.SetToolTip(this.chkGenerateViolationIdentifier, "Generate a default identifier for new violations");
			// 
			// cmbFormatViolationNumber
			// 
			this.cmbFormatViolationNumber.AutoSize = false;
			this.cmbFormatViolationNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFormatViolationNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormatViolationNumber.FormattingEnabled = true;
			this.cmbFormatViolationNumber.Location = new System.Drawing.Point(125, 167);
			this.cmbFormatViolationNumber.Name = "cmbFormatViolationNumber";
			this.cmbFormatViolationNumber.Size = new System.Drawing.Size(104, 40);
			this.cmbFormatViolationNumber.TabIndex = 21;
			this.ToolTip1.SetToolTip(this.cmbFormatViolationNumber, null);
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(260, 181);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(58, 15);
			this.Label5.TabIndex = 30;
			this.Label5.Text = "PLACES";
			this.ToolTip1.SetToolTip(this.Label5, null);
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(20, 181);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(70, 15);
			this.Label6.TabIndex = 29;
			this.Label6.Text = "FORMAT TO";
			this.ToolTip1.SetToolTip(this.Label6, null);
			// 
			// Frame5
			// 
			this.Frame5.AppearanceKey = "groupBoxLeftBorder";
			this.Frame5.Controls.Add(this.chkAddInspectionsToCalendar);
			this.Frame5.Controls.Add(this.Frame6);
			this.Frame5.Controls.Add(this.chkInspectionPrompt);
			this.Frame5.Location = new System.Drawing.Point(30, 497);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(619, 315);
			this.Frame5.TabIndex = 5;
			this.Frame5.Text = "Permits";
			this.ToolTip1.SetToolTip(this.Frame5, null);
			// 
			// chkAddInspectionsToCalendar
			// 
			this.chkAddInspectionsToCalendar.Location = new System.Drawing.Point(300, 30);
			this.chkAddInspectionsToCalendar.Name = "chkAddInspectionsToCalendar";
			this.chkAddInspectionsToCalendar.Size = new System.Drawing.Size(264, 27);
			this.chkAddInspectionsToCalendar.TabIndex = 6;
			this.chkAddInspectionsToCalendar.Text = "Add new inspections to calendar";
			this.ToolTip1.SetToolTip(this.chkAddInspectionsToCalendar, "When creating new inspections, add them to the calendar");
			// 
			// Frame6
			// 
			this.Frame6.Controls.Add(this.lblPermitIdentFormat);
			this.Frame6.Controls.Add(this.cmbPermitIdentFormat);
			this.Frame6.Controls.Add(this.cmbPermitIdentifier);
			this.Frame6.Controls.Add(this.lblPermitIdentifier);
			this.Frame6.Controls.Add(this.chkGeneratePermitIdentifier);
			this.Frame6.Controls.Add(this.cmbFormatPermitNumber);
			this.Frame6.Controls.Add(this.Label3);
			this.Frame6.Controls.Add(this.Label4);
			this.Frame6.Location = new System.Drawing.Point(20, 67);
			this.Frame6.Name = "Frame6";
			this.Frame6.Size = new System.Drawing.Size(371, 229);
			this.Frame6.TabIndex = 8;
			this.Frame6.Text = "Identifiers";
			this.ToolTip1.SetToolTip(this.Frame6, null);
			// 
			// chkGeneratePermitIdentifier
			// 
			this.chkGeneratePermitIdentifier.Location = new System.Drawing.Point(20, 134);
			this.chkGeneratePermitIdentifier.Name = "chkGeneratePermitIdentifier";
			this.chkGeneratePermitIdentifier.Size = new System.Drawing.Size(261, 27);
			this.chkGeneratePermitIdentifier.TabIndex = 14;
			this.chkGeneratePermitIdentifier.Text = "Generate when creating permits";
			this.ToolTip1.SetToolTip(this.chkGeneratePermitIdentifier, "Generate a default identifier for new permits");
			// 
			// cmbFormatPermitNumber
			// 
			this.cmbFormatPermitNumber.AutoSize = false;
			this.cmbFormatPermitNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFormatPermitNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormatPermitNumber.FormattingEnabled = true;
			this.cmbFormatPermitNumber.Location = new System.Drawing.Point(125, 167);
			this.cmbFormatPermitNumber.Name = "cmbFormatPermitNumber";
			this.cmbFormatPermitNumber.Size = new System.Drawing.Size(104, 40);
			this.cmbFormatPermitNumber.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.cmbFormatPermitNumber, null);
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 181);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(75, 15);
			this.Label3.TabIndex = 18;
			this.Label3.Text = "FORMAT TO";
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(260, 181);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(58, 15);
			this.Label4.TabIndex = 17;
			this.Label4.Text = "PLACES";
			this.ToolTip1.SetToolTip(this.Label4, null);
			// 
			// chkInspectionPrompt
			// 
			this.chkInspectionPrompt.Location = new System.Drawing.Point(20, 30);
			this.chkInspectionPrompt.Name = "chkInspectionPrompt";
			this.chkInspectionPrompt.Size = new System.Drawing.Size(253, 27);
			this.chkInspectionPrompt.TabIndex = 7;
			this.chkInspectionPrompt.Text = "Prompt for first Inspection Date";
			this.ToolTip1.SetToolTip(this.chkInspectionPrompt, "When creating a permit that has inspections, prompt to schedule the first inspect" +
        "ion.");
			// 
			// cmbFirstMonth
			// 
			this.cmbFirstMonth.AutoSize = false;
			this.cmbFirstMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cmbFirstMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFirstMonth.FormattingEnabled = true;
			this.cmbFirstMonth.Location = new System.Drawing.Point(227, 397);
			this.cmbFirstMonth.Name = "cmbFirstMonth";
			this.cmbFirstMonth.Size = new System.Drawing.Size(172, 40);
			this.cmbFirstMonth.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.cmbFirstMonth, null);
			// 
			// cmbYearCalc
			// 
			this.cmbYearCalc.AutoSize = false;
			this.cmbYearCalc.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYearCalc.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYearCalc.FormattingEnabled = true;
			this.cmbYearCalc.Location = new System.Drawing.Point(227, 447);
			this.cmbYearCalc.Name = "cmbYearCalc";
			this.cmbYearCalc.Size = new System.Drawing.Size(172, 40);
			this.cmbYearCalc.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.cmbYearCalc, null);
			// 
			// chkResetNumber
			// 
			this.chkResetNumber.Location = new System.Drawing.Point(414, 405);
			this.chkResetNumber.Name = "chkResetNumber";
			this.chkResetNumber.Size = new System.Drawing.Size(215, 27);
			this.chkResetNumber.TabIndex = 2;
			this.chkResetNumber.Text = "Reset numbers each year";
			this.ToolTip1.SetToolTip(this.chkResetNumber, null);
			// 
			// chk2DigitYear
			// 
			this.chk2DigitYear.Location = new System.Drawing.Point(414, 455);
			this.chk2DigitYear.Name = "chk2DigitYear";
			this.chk2DigitYear.Size = new System.Drawing.Size(141, 27);
			this.chk2DigitYear.TabIndex = 1;
			this.chk2DigitYear.Text = "Use 2 digit year";
			this.ToolTip1.SetToolTip(this.chk2DigitYear, null);
			// 
			// chkUseRE
			// 
			this.chkUseRE.Location = new System.Drawing.Point(30, 30);
			this.chkUseRE.Name = "chkUseRE";
			this.chkUseRE.Size = new System.Drawing.Size(359, 27);
			this.chkUseRE.TabIndex = 0;
			this.chkUseRE.Text = "Use Real Estate data for associated accounts";
			this.ToolTip1.SetToolTip(this.chkUseRE, "Use information from Real Estate on the master screen");
			this.chkUseRE.Visible = false;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 411);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(167, 21);
			this.Label1.TabIndex = 44;
			this.Label1.Text = "YEAR BEGINS ON THE 1ST OF";
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 461);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(47, 17);
			this.Label2.TabIndex = 43;
			this.Label2.Text = "AND IS";
			this.ToolTip1.SetToolTip(this.Label2, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(414, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(172, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save & Continue";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmCustomize
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1110, 729);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomize";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Customize";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmCustomize_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomize_KeyDown);
			this.Resize += new System.EventHandler(this.frmCustomize_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitIdentifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPlan)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkComplete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkApplicationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPermitType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridPermits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkGenerateViolationIdentifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			this.Frame5.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkAddInspectionsToCalendar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
			this.Frame6.ResumeLayout(false);
			this.Frame6.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkGeneratePermitIdentifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkInspectionPrompt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkResetNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chk2DigitYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseRE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		public FCLabel lblIdentifier;
	}
}