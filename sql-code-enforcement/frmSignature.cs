﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmSignature : BaseForm
	{
		public frmSignature()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSignature InstancePtr
		{
			get
			{
				return (frmSignature)Sys.GetInstance(typeof(frmSignature));
			}
		}

		protected frmSignature _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private string strSigPath = string.Empty;
		private bool boolDataChanged;

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			strSigPath = "";
			imgSignature.Image = null;
			boolDataChanged = true;
		}

		private void frmSignature_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSignature_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSignature properties;
			//frmSignature.FillStyle	= 0;
			//frmSignature.ScaleWidth	= 5880;
			//frmSignature.ScaleHeight	= 3975;
			//frmSignature.LinkTopic	= "Form2";
			//frmSignature.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			LoadSignature();
			boolDataChanged = false;
		}

		private void LoadSignature()
		{
			clsSignature t = new clsSignature();
			t.LoadCurrentUserSignatureInfo();
			strSigPath = t.SignatureFile;
			imgSignature.Image = t.GetSignature();
			// Dim rsLoad As New clsdrwrapper
			// Dim lngUser As Long
			// lngUser = clsSecurityClass.Get_UserID
			// Call rsLoad.OpenRecordset("select * from signatures where userid = " & lngUser, strCEDatabase)
			// If Not rsLoad.EndOfFile Then
			// If rsLoad.Fields("signature") <> "" Then
			// strSigPath = rsLoad.Fields("Signature")
			// If Not LoadSigPicture(strSigPath) Then
			// strSigPath = ""
			// Set imgSignature.Picture = Nothing
			// End If
			// Else
			// strSigPath = ""
			// Set imgSignature.Picture = Nothing
			// End If
			// Else
			// strSigPath = ""
			// Set imgSignature.Picture = Nothing
			// End If
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strFileName = "";
				MDIParent.InstancePtr.CommonDialog1.Filter = "Image Files|*.bmp;*.gif;*.jpg;*.sig";
                // MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                //FC:FINAL:CHN - i.issue #1667: Fix incorrect property using converting.
                // MDIParent.InstancePtr.CommonDialog1_Open.ShowOpen();
                MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
				if (fecherFoundation.Strings.Trim(strFileName) != string.Empty)
				{
					if (LoadSigPicture(ref strFileName))
					{
						// strNewPicture = strFileName
						strSigPath = strFileName;
						boolDataChanged = true;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number != 32755)
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Browse_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private bool LoadSigPicture(ref string strFileName)
		{
			bool LoadSigPicture = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				double dblRatio;
				int lngWidth;
				int lngHeight;
				int lngMaxWidth;
				int lngMaxHeight;
				// vbPorter upgrade warning: lngNewHeight As int	OnWriteFCConvert.ToDouble(
				int lngNewHeight;
				// vbPorter upgrade warning: lngNewWidth As int	OnWriteFCConvert.ToDouble(
				int lngNewWidth = 0;
				LoadSigPicture = false;
				imgSignature.Image = FCUtils.LoadPicture(strFileName);
				lngMaxHeight = Shape1.HeightOriginal;
				lngMaxWidth = Shape1.WidthOriginal;
                //FC:FINAL:CHN - i.issue #1667: Missing converting.
                // dblRatio = imgSignature.Image.Height / imgSignature.Image.Width;
                dblRatio = FCConvert.ToDouble(imgSignature.Image.Height) / imgSignature.Image.Width;
				lngNewHeight = FCConvert.ToInt32(dblRatio * lngMaxWidth);
				if (lngNewHeight > lngMaxHeight)
				{
					imgSignature.HeightOriginal = lngMaxHeight;
					lngNewWidth = FCConvert.ToInt32(lngMaxHeight / dblRatio);
					imgSignature.WidthOriginal = lngNewWidth;
				}
				else
				{
					imgSignature.WidthOriginal = lngMaxWidth;
					imgSignature.HeightOriginal = lngNewHeight;
				}
				LoadSigPicture = true;
				return LoadSigPicture;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (fecherFoundation.Information.Err(ex).Number == 481)
				{
					MessageBox.Show("You have selected an invalid image file for your signature.  Please try again.", "Invalid Signature File", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadSigPicture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return LoadSigPicture;
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngUser;
				string strUser;
				lngUser = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				strUser = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
				rsSave.OpenRecordset("select * from signatures where userid = " + FCConvert.ToString(lngUser), modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("UserID", lngUser);
					rsSave.Set_Fields("User", strUser);
				}
				rsSave.Set_Fields("Signature", strSigPath);
				rsSave.Update();
				boolDataChanged = false;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE #1926 save button should not close form 
            //if (SaveInfo())
            //{
            //	Close();
            //}
            SaveInfo();
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveInfo())
					{
						e.Cancel = true;
					}
				}
			}
		}
	}
}
