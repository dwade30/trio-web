﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWCE0000
{
	public class clsSQLStatement
	{
		//=========================================================
		private string strSQLStatement = string.Empty;
		private string strWhereStatement = string.Empty;
		private string strOrderBy = string.Empty;
		private string strSelectStatement = string.Empty;
		private int intTables;
		private string strParameterDescription = string.Empty;

		public string ParameterDescription
		{
			set
			{
				strParameterDescription = value;
			}
			get
			{
				string ParameterDescription = "";
				ParameterDescription = strParameterDescription;
				return ParameterDescription;
			}
		}

		public string SQLStatement
		{
			set
			{
				strSQLStatement = value;
			}
			get
			{
				string SQLStatement = "";
				SQLStatement = strSQLStatement;
				return SQLStatement;
			}
		}

		public string WhereStatement
		{
			set
			{
				strWhereStatement = value;
			}
			get
			{
				string WhereStatement = "";
				WhereStatement = strWhereStatement;
				return WhereStatement;
			}
		}

		public string OrderByStatement
		{
			set
			{
				strOrderBy = value;
			}
			get
			{
				string OrderByStatement = "";
				OrderByStatement = strOrderBy;
				return OrderByStatement;
			}
		}

		public string SelectStatement
		{
			set
			{
				strSelectStatement = value;
			}
			get
			{
				string SelectStatement = "";
				SelectStatement = strSelectStatement;
				return SelectStatement;
			}
		}

		public string ParameterDescriptionText
		{
			get
			{
				string ParameterDescriptionText = "";
				ParameterDescriptionText = GetRangeText(strParameterDescription);
				return ParameterDescriptionText;
			}
		}

		private string GetRangeText(string strText)
		{
			string GetRangeText = "";
			string[] strAry = null;
			string strReturn;
			string strSep;
			strSep = "";
			strReturn = "";
			if (fecherFoundation.Strings.Trim(strText) != "")
			{
				strAry = Strings.Split(strText, "|", -1, CompareConstants.vbBinaryCompare);
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					strReturn += strSep + strAry[x];
					strSep = ", ";
				}
				// x
			}
			GetRangeText = strReturn;
			return GetRangeText;
		}
	}
}
