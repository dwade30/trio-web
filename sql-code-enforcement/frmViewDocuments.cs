using fecherFoundation;
using Global;
using SharedApplication.CentralDocuments;
using SharedApplication.Extensions;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using TWSharedLibrary;
using Wisej.Web;
using Conversion = fecherFoundation.Conversion;
using Strings = fecherFoundation.Strings;

namespace TWCE0000
{
    public partial class frmViewDocuments : BaseForm
    {
        public frmViewDocuments()
        {
            //
            // required for windows form designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// default instance for form
        /// </summary>
        public static frmViewDocuments InstancePtr
        {
            get
            {
                return (frmViewDocuments)Sys.GetInstance(typeof(frmViewDocuments));
            }
        }

        protected frmViewDocuments _InstancePtr = null;
        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation
        // Written By     Dave Wade
        // Date           3/20/2008
        // This form will be used to view linked documents.  This is a global
        // form that can be used as long as the programmaer has used the
        // modLinkedDocuments to create a LinkedDocuments table in their database.
        // The programmer will need to call this form by using the init function.
        // The 4 arguments that need to be passed in are the SQL statement to be used
        // to select the correct documents, the Title to be shown on the screen,
        // the link field name that the programmer has added into their LinkedDocuments table,
        // and the value to place in the link field when adding new documents
        // ********************************************************
        string strFileToShow = "";
        string strPath = "";

        string strFieldToLink = "";
        int lngLinkKeyValue;
        string strFieldToLink2 = "";
        int lngLinkKeyValue2;
        string strDB = "";
        bool boolCantEdit;
        CentralDocumentService docService;
        public Collection<CentralDocumentHeader> docList { get; set; }
        private string _referenceType;
        private int _referenceId;
        private string _altReferenceId;

        enum Columns
        {
            FilenameCol = 0,
            SizeCol = 1,
            Typecol = 2,
            DescriptionCol = 3,
            KeyCol = 4
        }

        public void Init(int docId, string title, string referenceType, int referenceId, string altReferenceId, string strDBase = modMain.DEFAULTDATABASE, FCForm.FormShowEnum intModal = FCForm.FormShowEnum.Modeless, bool boolReadOnly = false)
        {
            strDB = strDBase;
            _referenceType = referenceType;
            _referenceId = referenceId;
            _altReferenceId = altReferenceId;

            GetDocumentList(docId);

            Show(intModal);

            Text = $"Attached Documents - {title}";
        }

        private void GetDocumentList(int id)
        {
           
            docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);

            if (id > 0)
            {
                docList.Add(docService.GetCentralDocument(new GetCommand {Id = id}).ToHeader());
            }
            else
            {
                docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand {DataGroup = "Code Enforcement",ReferenceId = _referenceId, ReferenceType = _referenceType});
            }
        }

        private void frmViewDocuments_Activated(object sender, System.EventArgs e)
        {
            if (FCConvert.ToBoolean(modMDIParent.FormExist(this)))
            {
                return;
            }
            if (ListView1.Visible)
            {
                ListView1.Focus();
            }
            Refresh();
        }

        private void frmViewDocuments_Load(object sender, System.EventArgs e)
        {

            mnuFileAddDocument.Enabled = !boolCantEdit;
            mnuFileDeleteDocument.Enabled = !boolCantEdit;

            SetupListView();

            LoadListView();

            modGlobalFunctions.SetFixedSize(this);
            modGlobalFunctions.SetTRIOColors(this);
        }

        private void frmViewDocuments_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            FormUtilities.KeyPressHandler(e,this);
        }

        private void ListView1_Click(object sender, System.EventArgs e)
        {
            try
            {
                fecherFoundation.Information.Err().Clear();
                var selectedItem = ListView1.FocusedItem;

                if (selectedItem == null) return;

                // get the whole document including binary data
                var documentId = selectedItem.SubItems[(int)Columns.KeyCol].Text.ToIntegerValue();
                var docBytes = docService.GetCentralDocument(new GetCommand { Id = documentId }).ItemData;

                if (docBytes == null || docBytes.Length == 0) throw new FileNotFoundException("No file content could be found for that file");

                ImageViewer1.LoadFileFromApplication("", docBytes, selectedItem.SubItems[(int)Columns.Typecol].Text);

                lblCurrentPage.Text = "1";

                if (SelectedFileExtension() != "PDF")
                {
                    lblTotalPage.Text = "1";
                }

                strFileToShow = selectedItem.Text;
                lblDescription.Text = strFileToShow;
                if (ListView1.Visible)
                {
                    // Set focus onto the listview control as long as this event is not firing in the Form_Load event
                    ListView1.Focus();
                }
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show($"Error {FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)} {fecherFoundation.Information.Err(ex).Description}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private string SelectedFileExtension()
        {
            return fecherFoundation.Strings.UCase(Strings.Right(ListView1.FocusedItem.Text, 3));
        }

        public void ListView1_Click()
        {
            ListView1_Click(ListView1, new System.EventArgs());
        }

        private void ListView1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // Force the click event when someone moves to a different item in the listview using the arrow keys
            ListView1_Click();
        }

        private void ListView1_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
        {

        }

        private void mnuFileAddDocument_Click(object sender, System.EventArgs e)
        {
            // add the document using frmAddDocument...returns false if no document added, telling us to return as well
            if (frmAddDocument.InstancePtr.Init(strFieldToLink, lngLinkKeyValue, strFieldToLink2, lngLinkKeyValue2, strDB) == false) return;

            LoadListView();

        }

        private void mnuFileDeleteDocument_Click(object sender, System.EventArgs e)
        {
            ListViewItem itm;
            DialogResult Ans = 0;
            int intIndex = 0;

            if (strFileToShow == "") return;

            Ans = MessageBox.Show("Are you sure you wish to remove this document?", "Remove Document?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Ans != DialogResult.Yes) return;

            itm = ListView1.SelectedItems.First();

            if (itm == null) return;

            var id = itm.SubItems[(int)Columns.KeyCol].Text.ToIntegerValue();
            docService.DeleteCentralDocument(new DeleteCommand { Id = id });

            LoadListView();
        }

        private void mnuFileNextPage_Click(object sender, System.EventArgs e)
        {
            var currentPageNumber = Conversion.Val(lblCurrentPage.Text);
            var totalPageCount = Conversion.Val(lblTotalPage.Text);

            // if we are not on the last page
            if (currentPageNumber < totalPageCount)
            {
                lblCurrentPage.Text = (currentPageNumber + 1).ToString();
            }
        }

        private void mnuFilePreviousPage_Click(object sender, System.EventArgs e)
        {
            var currentPageNumber = Conversion.Val(lblCurrentPage.Text);

            // if the page is not the first one
            if (currentPageNumber > 1)
            {
                lblCurrentPage.Text = (currentPageNumber - 1).ToString();
            }
        }

        private void mnuFilePrint_Click(object sender, System.EventArgs e)
        {
            // print the image we are currently viewing

            if (ImageViewer1.Controls.Count <= 0 || !(ImageViewer1.Controls[0] is PictureBox)) return;

            var viewer = ImageViewer1.Controls[0];
            string script = $"var divToPrint=document.getElementById('id_{viewer.Handle}');var backImage = divToPrint.currentStyle || window.getComputedStyle(divToPrint, false);backImage = backImage.backgroundImage.slice(4, -1).replace(/\"/g, \"\");var newHtml = '<img src=\\''+backImage+'\\'>';var newWin = window.open('', 'Print-Window');var header = '<html><body onload=\\'window.print();\\'>';var footer = '</body></html>'; newWin.document.open();newWin.document.write(header + newHtml + footer);newWin.document.close();setTimeout(function(){{ newWin.close(); }},10); ";
            ImageViewer1.Eval(script);
        }

        private void mnuFileRotate_Click(object sender, System.EventArgs e)
        {
            rotation1.GetRotation(ImageViewer1).RotateZ += 90;
        }

        private void mnuFileZoomAspectRatio_Click(object sender, System.EventArgs e)
        {
            ImageViewer1.Focus();
        }

        private void mnuFileZoomFitToWindow_Click(object sender, System.EventArgs e)
        {
            rotation1.GetRotation(ImageViewer1).ScaleX = 100;
            rotation1.GetRotation(ImageViewer1).ScaleY = 100;
            rotation1.GetRotation(ImageViewer1).ScaleZ = 100;
            ImageViewer1.Focus();
        }

        private void mnuFileZoomZoomIn_Click(object sender, System.EventArgs e)
        {

            rotation1.GetRotation(ImageViewer1).ScaleX += 10;
            rotation1.GetRotation(ImageViewer1).ScaleY += 10;
            rotation1.GetRotation(ImageViewer1).ScaleZ += 10;
        }

        private void mnuFileZoomZoomOut_Click(object sender, System.EventArgs e)
        {

            rotation1.GetRotation(ImageViewer1).ScaleX -= 10;
            rotation1.GetRotation(ImageViewer1).ScaleY -= 10;
            rotation1.GetRotation(ImageViewer1).ScaleZ -= 10;
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }


        private void SetupListView()
        {
            ListView1.View = View.Details;

            ListView1.SelectionMode = SelectionMode.One;
            ListView1.GridLines = true;
            ListView1.LabelEdit = false;
            ListView1.Sorted = true;
            ListView1.Columns.Clear();
            ListView1.Columns.Add("name", "Name", 250);
            ListView1.Columns.Add("size", "Size", 80);
            ListView1.Columns.Add("type", "Type", 195);
            ListView1.Columns.Add("description", "Description", 0);
            ListView1.Columns.Add("key", "Key", 0);
        }

        private void LoadListView()
        {
            try
            {
                fecherFoundation.Information.Err().Clear();
                ListView1.Items.Clear();

                // faster loading if not 'refreshed' with each addition
                ListView1.Visible = false;

                // clear out the images/icons
                FormUtilities.LV_ClearImageListsAndRelinkToListView(ref ListView1, ref imgLarge, ref imgSmall);

                // note that these do NOT hold image data; byte array is grabbed when an item is clicked
                foreach (var centralDocument in docList)
                {
                    var sKey = $"@{centralDocument.ID}";
                    var item = ListView1.Items.Add(sKey, centralDocument.ItemName);
                    FCListView.SubItemsSetText(item, (int) Columns.SizeCol, FormatLength(centralDocument.ItemLength));
                    FCListView.SubItemsSetText(item, (int) Columns.Typecol, centralDocument.MediaType);
                    FCListView.SubItemsSetText(item, (int) Columns.DescriptionCol, centralDocument.ItemDescription);
                    FCListView.SubItemsSetText(item, (int) Columns.KeyCol, centralDocument.ID.ToString());
                    item.ToolTipText = centralDocument.ItemDescription;
                }

                ListView1.Visible = true;

                if (ListView1.Items.Count <= 0) return;

                ListView1.Items[0].Selected = true;
                ListView1.FocusedItem = ListView1.Items[0];

                // Force it to show in the image viewer control
                ListView1_Click();
            }
            catch (Exception ex)
            {
                MessageBox.Show(fecherFoundation.Information.Err(ex).Description);
                ListView1.Visible = false;
            }
        }

        private string FormatLength(long length)
        {
            var sLength = Strings.Format(length, "###,###,###,###");
            var formatLength = sLength.PadLeft(12, ' ');
            
            return formatLength;
        }
    }
}
