//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptViolations.
	/// </summary>
	public partial class rptViolations : BaseSectionReport
	{
		public rptViolations()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptViolations InstancePtr
		{
			get
			{
				return (rptViolations)Sys.GetInstance(typeof(rptViolations));
			}
		}

		protected rptViolations _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
				rsType.Dispose();
				rsStatus.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptViolations	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		clsDRWrapper rsType = new clsDRWrapper();
		clsDRWrapper rsStatus = new clsDRWrapper();

		public void Init(int lngAccount, int lngPermit, int lnginspection, bool modalDialog)
		{
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                if (lnginspection > 0)
                {
                    rsReport.OpenRecordset(
                        "select * from violations where violationaccount = " + FCConvert.ToString(lngAccount) +
                        " and violationpermit = " + FCConvert.ToString(lngPermit) + " and violationinspection = " +
                        FCConvert.ToString(lnginspection) + " order by dateviolationissued desc",
                        modGlobalVariables.Statics.strCEDatabase);
                }
                else if (lngPermit > 0)
                {
                    rsReport.OpenRecordset(
                        "select * from violations where violationaccount = " + FCConvert.ToString(lngAccount) +
                        " and violationpermit = " + FCConvert.ToString(lngPermit) +
                        " order by dateviolationissued desc", modGlobalVariables.Statics.strCEDatabase);
                }
                else
                {
                    rsReport.OpenRecordset(
                        "select * from violations where violationaccount = " + FCConvert.ToString(lngAccount) +
                        "  order by dateviolationissued desc", modGlobalVariables.Statics.strCEDatabase);
                }

                if (!rsReport.EndOfFile())
                {
                    rsLoad.OpenRecordset("select * from cemaster where ceaccount = " + FCConvert.ToString(lngAccount),
                        modGlobalVariables.Statics.strCEDatabase);
                    if (rsLoad.EndOfFile())
                    {
                        MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }

                    txtName.Text = fecherFoundation.Strings.Trim(
                        fecherFoundation.Strings.Trim(rsLoad.Get_Fields("FIRST") + " " + rsLoad.Get_Fields("Middle")) +
                        " " + rsLoad.Get_Fields("last") + " " + rsLoad.Get_Fields_String("desig"));
                    txtDoingBusinessAs.Text = rsLoad.Get_Fields_String("doingbusinessas");
                    if (Conversion.Val(rsLoad.Get_Fields("streetnumber")) > 0)
                    {
                        txtLocation.Text = rsLoad.Get_Fields("streetnumber") + rsLoad.Get_Fields_String("apt") + " " +
                                           rsLoad.Get_Fields_String("streetname");
                    }
                    else
                    {
                        txtLocation.Text = rsLoad.Get_Fields_String("streetname");
                    }

                    this.Fields.Add("grpHeader");
                    this.Fields["grpHeader"].Value = lngAccount.ToString();
                }
                else
                {
                    MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtGroupMuni.Text = txtMuni.Text;
			txtGroupPage.Text = "Page 1";
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtGroupDate.Text = txtDate.Text;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtGroupTime.Text = txtTime.Text;
			rsType.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
			rsStatus.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONSTATUS) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				txtDateIssued.Text = "";
				if (Information.IsDate(rsReport.Get_Fields("dateviolationissued")))
				{
					if (Convert.ToDateTime(rsReport.Get_Fields_DateTime("dateviolationissued")).ToOADate() != 0)
					{
						txtDateIssued.Text = Strings.Format(rsReport.Get_Fields_DateTime("dateviolationissued"),"MM/dd/yyyy");
					}
				}
				txtIdentifier.Text = rsReport.Get_Fields_String("violationidentifier");
				if (rsType.FindFirstRecord("ID", rsReport.Get_Fields_Int32("violationtype")))
				{
					txtType.Text = rsType.Get_Fields_String("description");
				}
				else
				{
					txtType.Text = "";
				}
				if (rsStatus.FindFirstRecord("ID", rsReport.Get_Fields_Int32("violationstatus")))
				{
					txtStatus.Text = rsStatus.Get_Fields_String("description");
				}
				else
				{
					txtStatus.Text = "";
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
			if (PageNumber > 1)
				PageHeader.Visible = true;
		}

		
	}
}
