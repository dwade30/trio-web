﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptContractors.
	/// </summary>
	public partial class rptContractors : BaseSectionReport
	{
		public rptContractors()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Contractors";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptContractors InstancePtr
		{
			get
			{
				return (rptContractors)Sys.GetInstance(typeof(rptContractors));
			}
		}

		protected rptContractors _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptContractors	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref clsSQLStatement clsStatement)
		{
			string strSQL = "";
			if (!(clsStatement == null))
			{
				// 
				strSQL = "Select distinct ContractorAutoID,ContractorNumber, ContractorName1, ContractorName2,ContractorAddress1,";
				strSQL += "ContractorAddress2, ContractorCity,ContractorState,ContractorZip, ContractorZip4,";
				strSQL += " ContractorEmail,contractorstatus, ContractorDeleted, ContractorComment from ";
				strSQL += "(" + clsStatement.SelectStatement + " " + clsStatement.WhereStatement + " " + clsStatement.OrderByStatement + ") abc";
				// strSQL = clsStatement.SQLStatement
			}
			else
			{
				strSQL = "Select  contractors.ID as ContractorAutoID,ContractorNumber,contractors.name1 as ContractorName1,contractors.name2 as ContractorName2,Contractors.address1 as ContractorAddress1,";
				strSQL += "contractors.address2 as ContractorAddress2,contractors.city as ContractorCity,contractors.state as ContractorState,contractors.zip as ContractorZip,contractors.zip4 as ContractorZip4,";
				strSQL += "contractors.email as ContractorEmail,contractorstatus,contractors.deleted as ContractorDeleted,contractors.comment as ContractorComment from contractors where not deleted = 1 order by name1";
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			txtRange.Text = clsStatement.ParameterDescriptionText;
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "Contractors");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				if (!(Conversion.Val(rsReport.Get_Fields_String("contractorAutoID")) <= 0))
				{
					txtNo.Text = rsReport.Get_Fields_String("CONTRACTORNUMBER");
					string strTemp = "";
					string strnewline = "";
					string strLicense = "";
					clsDRWrapper rsTemp = new clsDRWrapper();
					// txtLicense.Text = rsReport.Fields("license")
					strTemp = "";
					strnewline = "";
					if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("Contractorname1")) != string.Empty)
					{
						strTemp = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractorname1"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("Contractorname2")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractorname2"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractoraddress1")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractoraddress1"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractoraddress2")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractoraddress2"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractorcity")) != string.Empty || rsReport.Get_Fields_String("contractorstate") != string.Empty || rsReport.Get_Fields_String("contractorzip") != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractorcity")) + "  " + rsReport.Get_Fields_String("contractorstate") + " " + rsReport.Get_Fields_String("contractorzip") + " " + rsReport.Get_Fields_String("contractorzip4"));
						strnewline = "\r\n";
					}
					txtName.Text = strTemp;
					strTemp = "";
					strnewline = "";
					strLicense = "";
					rsTemp.OpenRecordset("select * from classcodes inner join systemcodes on (systemcodes.code = classcodes.code) where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORCLASS) + " and  contractorid = " + rsReport.Get_Fields_String("contractorAutoID"), modGlobalVariables.Statics.strCEDatabase);
					while (!rsTemp.EndOfFile())
					{
						strTemp += strnewline + rsTemp.Get_Fields_String("description");
						strLicense += strnewline + rsTemp.Get_Fields_String("license");
						strnewline = "\r\n";
						rsTemp.MoveNext();
					}
					txtClass.Text = strTemp;
					txtLicense.Text = strLicense;
					rsTemp.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORSTATUS) + " and code = " + rsReport.Get_Fields_String("contractorstatus"), modGlobalVariables.Statics.strCEDatabase);
					if (!rsTemp.EndOfFile())
					{
						txtStatus.Text = rsTemp.Get_Fields_String("description");
					}
					else
					{
						txtStatus.Text = "";
					}
					rsTemp.Dispose();
				}
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
