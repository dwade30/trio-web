//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.CodeEnforcement;
using SharedApplication.Messaging;
using SharedApplication.Models;
using TWSharedLibrary;
using Wisej.Web;
using Wisej.Web.Ext.FullCalendar;

namespace TWCE0000
{
	public class modMain
	{
		//=========================================================
		public const string DEFAULTDATABASE = "TWCE0000.vb1";

		public static void Main(CommandDispatcher commandDispatcher)
		{
			// Dim clsLoad As New clsdrwrapper
			clsDRWrapper clsTemp = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modGlobalVariables.Statics.strREDatabase = "TWRE0000.vb1";
				modGlobalVariables.Statics.strCEDatabase = "TWCE0000.vb1";
				modGlobalVariables.Statics.strGNDatabase = "SystemSettings";
				modGlobalVariables.Statics.strCECostFilesDatabase = "TWCE0000.vb1";
				string strDataPath;
				strDataPath = Environment.CurrentDirectory;
				if (Strings.Right(strDataPath, 1) != "\\")
				{
					strDataPath += "\\";
				}
				// kk04132015 troges-39  Change to single config file
				if (!FCConvert.ToBoolean(modGlobalFunctions.LoadSQLConfig()))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				// Dim s                           As New SettingsInfo
				// S.LoadSettingsAndArchives
				// If Not s.LoadSettingsAndArchives(strDataPath & "trioworkstationsetup.xml") Then
				// Call MsgBox("Unable to load settings file", vbCritical, "Cannot Continue")
				// End
				// Exit Sub
				// End If
				// If Not s.LoadSettings(strDataPath & "trioworkstationsetup.xml") Then
				// Call MsgBox("Unable to load settings file", vbCritical, "Cannot Continue")
				// End
				// Exit Sub
				// End If
				// clsTemp.DefaultGroup = "Live"
				// If clsTemp.MegaGroupsCount > 0 Then
				// clsTemp.DefaultMegaGroup = clsTemp.AvailableMegaGroups(0).GroupName
				// End If
				//FC:FINAL:SBE - get user id from session
                //modReplaceWorkFiles.Statics.gintSecurityID = (FCConvert.ToString(modRegistry.GetRegistryKey("SecurityID")) != string.Empty ? FCConvert.ToInt32(Math.Round(Conversion.Val(modReplaceWorkFiles.Statics.gstrReturn)) : 0);
                modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;
                modReplaceWorkFiles.EntryFlagFile(true, "");
                //FC:FINAL:IPI - not needed on web
                //if (App.PrevInstance())
                //{
                //	MessageBox.Show("Warning. There is already a version of this program running on this computer.", "Already Running", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //	Application.Exit();
                //	return;
                //}
                // gintSecurityID = IIf(GetRegistryKey("SecurityID") <> vbNullString, Val(gstrReturn), "0")
                modReplaceWorkFiles.GetGlobalVariables();
				modGlobalFunctions.UpdateUsedModules();
				// Call clsLoad.OpenRecordset("SELECT * from globalvariables", "SystemSettings")
				// 
				// MuniName = clsLoad.Fields("muniname")
				CheckVersion(commandDispatcher);
				modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
				modGlobalConstants.Statics.clsSecurityClass.Init("CE", "SystemSettings");
				modGlobalVariables.Statics.CECustom = new clsCustomize();
				MDIParent.InstancePtr.Init();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Main", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Application.Exit();
			}
		}

		public static string NameFromParts(string strFirst, string strMiddle, string strLast, string strDesig, bool boolLastFirst = false)
		{
			string NameFromParts = "";
			string strReturn = "";
			if (!boolLastFirst)
			{
				strReturn = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strFirst + " " + strMiddle) + " " + strLast) + " " + strDesig);
			}
			else
			{
				strReturn = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strFirst + " " + strMiddle) + " " + strDesig);
				if (strReturn != string.Empty)
				{
					if (fecherFoundation.Strings.Trim(strLast) != string.Empty)
					{
						strReturn = fecherFoundation.Strings.Trim(strLast + ", " + strReturn);
					}
				}
				else
				{
					strReturn = fecherFoundation.Strings.Trim(strLast);
				}
			}
			NameFromParts = strReturn;
			return NameFromParts;
		}

		private static void CheckVersion(CommandDispatcher commandDispatcher, bool boolCheckDBStructure = false)
		{
			try
            {
                var dbInfo = new SQLConfigInfo()
                {
                    ServerInstance = StaticSettings.gGlobalSettings.DataSource,
                    UserName = StaticSettings.gGlobalSettings.UserName,
                    Password = StaticSettings.gGlobalSettings.Password
                };
				StaticSettings.GlobalCommandDispatcher.Send(new UpdateCodeEnforcementDatabase(dbInfo,StaticSettings.gGlobalSettings.DataEnvironment));
				//var dbChecker = new cCodeEnforcementDB();
    //            dbChecker.CheckVersion();
				//return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show(ex.Message + "\r\n" + "In CheckVersion", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		public static void CheckDatabaseStructure(CommandDispatcher commandDispatcher)
		{
			CheckVersion(commandDispatcher,true);
			MessageBox.Show("Check database structure complete", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		public static object ReleaseLock(int intLockType)
		{
			object ReleaseLock = null;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strField = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				switch (intLockType)
				{
					case modCEConstants.CNSTCONTRACTLOCK:
						{
							strField = "Contractor";
							break;
						}
					case modCEConstants.CNSTPERMITLOCK:
						{
							strField = "Permit";
							break;
						}
					case modCEConstants.CNSTMASTERLOCK:
						{
							strField = "Master";
							break;
						}
					case modCEConstants.CNSTVIOLATIONLOCK:
						{
							strField = "Violation";
							break;
						}
				}
				//end switch
				clsSave.Execute("update accountlocks set " + strField + "Lock = 0", modGlobalVariables.Statics.strCEDatabase);
				clsSave = null;
				return ReleaseLock;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ReleaseLock", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ReleaseLock;
		}

		public static void ClearLocks()
		{
			int x;
			for (x = 0; x <= 3; x++)
			{
				ReleaseLock(x);
			}
			// x
		}

		public static bool AttemptLock(ref string strUser, int intLockType)
		{
			bool AttemptLock = false;
			try
			{
				int intTryCount;
				string strField = "";
				string strTemp = "";
				AttemptLock = false;
				intTryCount = 0;
				switch (intLockType)
				{
					case modCEConstants.CNSTCONTRACTLOCK:
						{
							strField = "Contractor";
							break;
						}
					case modCEConstants.CNSTPERMITLOCK:
						{
							strField = "Permit";
							break;
						}
					case modCEConstants.CNSTMASTERLOCK:
						{
							strField = "Master";
							break;
						}
					case modCEConstants.CNSTVIOLATIONLOCK:
						{
							strField = "Violation";
							break;
						}
				}
				//end switch
				while (intTryCount < 10)
				{
					if (GetLock(ref strField, ref strTemp))
					{
						AttemptLock = true;
						return AttemptLock;
					}
					else
					{
						strUser = strTemp;
					}
				}
				if (MessageBox.Show("The program has timed out trying to obtain the " + strField + " lock" + "\r\n" + "Do you wish to try again?", "Try Lock Again?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					intTryCount = 0;
					while (intTryCount < 10)
					{
						if (GetLock(ref strField, ref strTemp))
						{
							AttemptLock = true;
							return AttemptLock;
						}
						else
						{
							strUser = strTemp;
						}
					}
				}
				return AttemptLock;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AttemptLock", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return AttemptLock;
			}
		}

		private static bool GetLock(ref string strField, ref string strUser)
		{
			bool GetLock = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GetLock = false;
				clsLoad.OpenRecordset("select * from accountlocks", modGlobalVariables.Statics.strCEDatabase);
				if (clsLoad.EndOfFile())
				{
					clsLoad.AddNew();
				}
				strUser = FCConvert.ToString(clsLoad.Get_Fields(strField + "User"));
				if (FCConvert.ToBoolean(clsLoad.Get_Fields(strField + "Lock")))
				{
					return GetLock;
				}
				else
				{
					if (!clsLoad.Edit())
					{
						return GetLock;
					}
					clsLoad.Set_Fields(strField + "user", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
					clsLoad.Set_Fields(strField + "Lock", true);
					if (!clsLoad.Update())
					{
						return GetLock;
					}
					GetLock = true;
				}
				clsLoad = null;
				return GetLock;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetLock", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetLock;
		}

		public static string EscapeQuote(ref string a)
		{
			string EscapeQuote = "";
			// searches for a singlequote and puts another next to it
			string tstring = "";
			string t2string = "";
			int stringindex;
			// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
			int intpos = 0;
			EscapeQuote = a;
			if (a == string.Empty)
				return EscapeQuote;
			stringindex = 1;
			while (stringindex <= a.Length)
			{
				intpos = Strings.InStr(stringindex, a, "'", CompareConstants.vbBinaryCompare);
				if (intpos == 0)
					break;
				tstring = Strings.Mid(a, 1, intpos);
				t2string = Strings.Mid(a, intpos + 1);
				if (intpos == a.Length)
				{
					a = Strings.Mid(a, 1, intpos - 1);
					break;
				}
				else
				{
					a = tstring + "'" + t2string;
				}
				stringindex = intpos + 2;
			}
			EscapeQuote = a;
			return EscapeQuote;
		}

		public static bool DeleteAccount(ref int lngAccountNumber)
		{
			bool DeleteAccount = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				DeleteAccount = false;
				
				modGlobalFunctions.AddCYAEntry_6("CE", "Deleted Account " + FCConvert.ToString(lngAccountNumber));
				clsLoad.OpenRecordset("select * from permits where account = " + FCConvert.ToString(lngAccountNumber), modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					DeletePermit(FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))), false);
					clsLoad.MoveNext();
				}
				clsSave.Execute("update cemaster set deleted = 1 where ceaccount = " + FCConvert.ToString(lngAccountNumber), modGlobalVariables.Statics.strCEDatabase);
				// Else
				// don't do anything. It doesn't exist
				// End If
				DeleteAccount = true;
				return DeleteAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DeleteAccount;
		}
		// vbPorter upgrade warning: lngPermitID As int	OnWrite(double, int)
		public static bool DeletePermit(int lngPermitID, bool boolPermanentDelete = false)
		{
			bool DeletePermit = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsDRWrapper clsSave = new clsDRWrapper();
				DeletePermit = false;
				if (!boolPermanentDelete)
				{
					modGlobalFunctions.AddCYAEntry_6("CE", "Deleted permit " + FCConvert.ToString(lngPermitID));
					clsSave.Execute("update permits set deleted = 1 where ID = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
					clsSave.Execute("update inspections set deleted = 1 where permitid = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
				}
				else
				{
					clsLoad.OpenRecordset("select * from inspections where permitid = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
					while (!clsLoad.EndOfFile())
					{
						DeleteInspection(clsLoad.Get_Fields_Int32("ID"));
						clsLoad.MoveNext();
					}
					clsSave.Execute("delete from usercodevalues where account = " + FCConvert.ToString(lngPermitID) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT), modGlobalVariables.Statics.strCEDatabase);
					clsSave.Execute("delete from phonenumbers where parentid = " + FCConvert.ToString(lngPermitID) + " and (phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPEPERMIT) + " or phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPEPERMITCONTRACTOR) + ")", modGlobalVariables.Statics.strCEDatabase);
					clsSave.Execute("delete from permits where ID = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
				}
				DeletePermit = true;
				return DeletePermit;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeletePermit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DeletePermit;
		}

		public static bool DeleteInspection(int lngInspectionID, bool boolPermanentDelete = false)
		{
			bool DeleteInspection = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				DeleteInspection = false;
				if (!boolPermanentDelete)
				{
					modGlobalFunctions.AddCYAEntry_6("CE", "Deleted Inspection " + FCConvert.ToString(lngInspectionID));
					clsSave.Execute("update inspections set deleted = 1 where ID = " + FCConvert.ToString(lngInspectionID), modGlobalVariables.Statics.strCEDatabase);
				}
				else
				{
					clsSave.Execute("delete from usercodevalues where account = " + FCConvert.ToString(lngInspectionID) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION), modGlobalVariables.Statics.strCEDatabase);
					clsSave.Execute("delete from inspections where ID = " + FCConvert.ToString(lngInspectionID), modGlobalVariables.Statics.strCEDatabase);
				}
				DeleteInspection = true;
				return DeleteInspection;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DeleteInspection;
		}

		public static bool DeleteContractor(int lngContractorID, bool boolPermanentDelete = false)
		{
			bool DeleteContractor = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				DeleteContractor = false;
				if (!boolPermanentDelete)
				{
					modGlobalFunctions.AddCYAEntry_6("CE", "Deleted Contractor " + FCConvert.ToString(lngContractorID));
					clsSave.Execute("update contractors set deleted = 1 where ID = " + FCConvert.ToString(lngContractorID), modGlobalVariables.Statics.strCEDatabase);
				}
				else
				{
					clsSave.Execute("delete from usercodevalues where account = " + FCConvert.ToString(lngContractorID) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR), modGlobalVariables.Statics.strCEDatabase);
					clsSave.Execute("delete from phonenumbers where PARENTid = " + FCConvert.ToString(lngContractorID) + " and phonecode = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR), modGlobalVariables.Statics.strCEDatabase);
					clsSave.Execute("delete from contractors where ID = " + FCConvert.ToString(lngContractorID), modGlobalVariables.Statics.strCEDatabase);
				}
				clsSave.Execute("update permits set contractorid = " + FCConvert.ToString(modCEConstants.CNSTPERMITCONTRACTOROTHER) + " where contractorid = " + FCConvert.ToString(lngContractorID), modGlobalVariables.Statics.strCEDatabase);
				DeleteContractor = true;
				return DeleteContractor;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteContractor", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DeleteContractor;
		}
		// vbPorter upgrade warning: lngAccount As int	OnWrite(int, double)
		public static bool UndeleteMaster(int lngAccount)
		{
			bool UndeleteMaster = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsLoad = new clsDRWrapper();
				UndeleteMaster = false;
				modGlobalFunctions.AddCYAEntry_6("CE", "Undeleted Account " + FCConvert.ToString(lngAccount));
				clsSave.Execute("Update cemaster set deleted = 0 where ceaccount = " + FCConvert.ToString(lngAccount), modGlobalVariables.Statics.strCEDatabase);
				clsLoad.OpenRecordset("select * from permits where account = " + FCConvert.ToString(lngAccount), modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					UndeletePermit(clsLoad.Get_Fields_Int32("ID"));
					clsLoad.MoveNext();
				}
				UndeleteMaster = true;
				return UndeleteMaster;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UndeleteMaster", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UndeleteMaster;
		}
		// vbPorter upgrade warning: lngContractorID As int	OnWrite(int, double)
		public static bool UndeleteContractor(int lngContractorID)
		{
			bool UndeleteContractor = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				UndeleteContractor = false;
				modGlobalFunctions.AddCYAEntry_6("CE", "Undeleted Contractor " + FCConvert.ToString(lngContractorID));
				clsSave.Execute("update contractors set deleted = 0 where ID = " + FCConvert.ToString(lngContractorID), modGlobalVariables.Statics.strCEDatabase);
				UndeleteContractor = true;
				return UndeleteContractor;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UndeleteContractor", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UndeleteContractor;
		}
		// vbPorter upgrade warning: lngPermitID As object	OnWrite(object, int, double)
		public static bool UndeletePermit(object lngPermitID)
		{
			bool UndeletePermit = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				UndeletePermit = false;
				modGlobalFunctions.AddCYAEntry_6("CE", "Undeleted Permit " + lngPermitID);
				clsSave.Execute("update permits set deleted = 0 where ID = " + lngPermitID, modGlobalVariables.Statics.strCEDatabase);
				clsSave.Execute("update inspections set deleted = 0 where permitid = " + lngPermitID, modGlobalVariables.Statics.strCEDatabase);
				UndeletePermit = true;
				return UndeletePermit;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UndeletePermit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UndeletePermit;
		}
		// vbPorter upgrade warning: lngInspectionID As object	OnWriteFCConvert.ToDouble(
		public static bool UndeleteInspection(object lngInspectionID)
		{
			bool UndeleteInspection = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				UndeleteInspection = false;
				modGlobalFunctions.AddCYAEntry_6("CE", "Undeleted Inspection " + lngInspectionID);
				clsSave.Execute("update inspections set deleted = 0 where ID = " + lngInspectionID, modGlobalVariables.Statics.strCEDatabase);
				UndeleteInspection = true;
				return UndeleteInspection;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UndeleteInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UndeleteInspection;
		}

		public static string GetCEDBRegistryEntry(string strKeyName, string strSubPath, int lngUserID)
		{
			string GetCEDBRegistryEntry = "";
			string strReturn;
			strReturn = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("select * from RegistryEntries where userid = " + FCConvert.ToString(lngUserID) + " and Keyname = '" + strKeyName + "' and SubPath = '" + strSubPath + "'", modGlobalVariables.Statics.strCEDatabase);
			if (!rsTemp.EndOfFile())
			{
				strReturn = FCConvert.ToString(rsTemp.Get_Fields_String("KeyValue"));
			}
			GetCEDBRegistryEntry = strReturn;
			return GetCEDBRegistryEntry;
		}
		// vbPorter upgrade warning: strKeyValue As string	OnWrite(int, string)
		public static void SaveCEDBRegistryEntry(string strKeyName, string strKeyValue, string strSubPath, int lngUserID)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("select * from RegistryEntries where userid = " + FCConvert.ToString(lngUserID) + " and Keyname = '" + strKeyName + "' and SubPath = '" + strSubPath + "'", modGlobalVariables.Statics.strCEDatabase);
			if (!rsTemp.EndOfFile())
			{
				rsTemp.Edit();
			}
			else
			{
				rsTemp.AddNew();
				rsTemp.Set_Fields("KeyName", strKeyName);
				rsTemp.Set_Fields("SubPath", strSubPath);
				rsTemp.Set_Fields("UserID", lngUserID);
			}
			rsTemp.Set_Fields("KeyValue", strKeyValue);
			rsTemp.Update();
		}

		public static void AddInspectionToCalendar(int lnginspection, string strDescription, string strLocation, string strSubject, DateTime dtDate)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// Dim rsLoad As New clsDataConnection
				// Call rsLoad.OpenRecordset("select * from inspections where autoid = " & lngInspection, strCEDatabase)
				// If Not rsLoad.EndOfFile Then
				//FC:TODO
				Event tEvent = new Event();
				//tEvent = frmCalendar.InstancePtr.CalendarControl.DataProvider.CreateEvent;
				tEvent.UserData.Label = modCEConstants.CNSTCalendarEventLabelInspection;
				tEvent.UserData.body = strDescription;
				tEvent.UserData.location = strLocation;
				tEvent.Title = strSubject;
				if (Information.IsDate(dtDate)) {
					if (dtDate.ToOADate()!=0) {
						tEvent.Start = dtDate;
						tEvent.End = fecherFoundation.DateAndTime.DateAdd("n", 15, dtDate);
					}
				}
                tEvent.UserData.User = modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID();
				frmEditEvent.InstancePtr.ModifyEvent(ref tEvent, true);
				frmEditEvent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddInspectionToCalendar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
