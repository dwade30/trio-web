﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWCE0000
{
	public class CustomFieldInfo
	{
		//=========================================================
		// Private Type CustomFieldInfo
		public string Title = "";
		public int Code;
		public int FieldID;
		// if it is an user defined field, need code and fieldid
		public string ExtraParameters = "";
		// End Type
	}
}
