//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.CentralDocuments.Commands;
using TWSharedLibrary;

namespace TWCE0000
{
	public partial class frmViolation : BaseForm
	{
		public frmViolation()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmViolation InstancePtr
		{
			get
			{
				return (frmViolation)Sys.GetInstance(typeof(frmViolation));
			}
		}

		protected frmViolation _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDACTIVITYCOLAUTOID = 0;
		const int CNSTGRIDACTIVITYCOLDATE = 1;
		const int CNSTGRIDACTIVITYCOLTYPE = 2;
		const int CNSTGRIDACTIVITYCOLDESC = 3;
		const int CNSTGRIDDOCSCOLAUTOID = 0;
		const int CNSTGRIDDOCSCOLDESCRIPTION = 1;
		const int CNSTGRIDDOCSCOLFILENAME = 2;
		const int CNSTGRIDOPENSCOLAUTOID = 0;
		const int CNSTGRIDOPENSCOLDESC = 1;
		const int CNSTGRIDOPENSCOLVALUE = 2;
		const int CNSTGRIDOPENSCOLVALUEFORMAT = 3;
		const int CNSTGRIDOPENSCOLMANDATORY = 4;
		const int CNSTGRIDOPENSCOLMIN = 5;
		const int CNSTGRIDOPENSCOLMAX = 6;
		private clsViolation vViolation = new clsViolation();
		private bool boolCantEdit;
		private clsActivityList alList = new clsActivityList();
		private clsAttachedDocuments dlAttachedDocs = new clsAttachedDocuments();

		private void cmbViolationType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			CheckType(cmbViolationType.ItemData(cmbViolationType.SelectedIndex));
		}

		private void frmViolation_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmViolation_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmViolation properties;
			//frmViolation.FillStyle	= 0;
			//frmViolation.ScaleWidth	= 9300;
			//frmViolation.ScaleHeight	= 7815;
			//frmViolation.LinkTopic	= "Form2";
			//frmViolation.LockControls	= -1  'True;
			//frmViolation.Moveable	= 0   'False;
			//frmViolation.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SetTRIOColors Me
			SetupcmbStatus();
			SetupcmbViolationType();
			SetupGridActivity();
			SetupGridPhones();
			SetupGridDocuments();
			SetupGridOpens(0);
			FillViolation();
			if (boolCantEdit)
			{
				cmdAddActivity.Visible = false;
				cmdAttachDoc.Visible = false;
				cmdRemoveDocument.Visible = false;
				cmdSave.Enabled = false;
				//mnuSaveExit.Enabled = false;
				cmdDeleteViolation.Enabled = false;
			}
		}

		private void SetupcmbStatus()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			cmbStatus.Clear();
			rsLoad.OpenRecordset("select * from SYSTEMCODES where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONSTATUS), modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				cmbStatus.AddItem(rsLoad.Get_Fields_String("description"));
				cmbStatus.ItemData(cmbStatus.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")));
				rsLoad.MoveNext();
			}
			cmbStatus.AddItem("Closed");
			cmbStatus.ItemData(cmbStatus.NewIndex, modCEConstants.CNSTVIOLATIONSTATUSCLOSED);
		}

		private void SetupcmbViolationType()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			cmbViolationType.Clear();
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE), modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				cmbViolationType.AddItem(rsLoad.Get_Fields_String("description"));
				cmbViolationType.ItemData(cmbViolationType.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")));
				rsLoad.MoveNext();
			}
		}

		private void FillViolation()
		{
			txtDescription.Text = vViolation.Description;
			rtbDetail.Text = vViolation.Detail;
			if (vViolation.DateIssued.ToOADate() != 0)
			{
                //FC:FINAL:MSH - i.issue #1710: use full date format, because masketTextBox automatically parse dates incorrectly (WiseJ problem)
				//t2kDateIssued.Text = FCConvert.ToString(vViolation.DateIssued);
				t2kDateIssued.Text = Strings.Format(vViolation.DateIssued, "MM/dd/yyyy");
			}
			else
			{
				t2kDateIssued.Text = "";
			}
			if (vViolation.ID == 0)
			{
				t2kDateIssued.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			if (vViolation.DateOccurred.ToOADate() != 0)
			{
                //FC:FINAL:MSH - i.issue #1710: use full date format, because masketTextBox automatically parse dates incorrectly (WiseJ problem)
				//T2KDateOccurred.Text = FCConvert.ToString(vViolation.DateOccurred);
				T2KDateOccurred.Text = Strings.Format(vViolation.DateOccurred, "MM/dd/yyyy");
            }
			else
			{
				T2KDateOccurred.Text = "";
			}
			if (vViolation.DateResolved.ToOADate() != 0)
			{
                //FC:FINAL:MSH - i.issue #1710: use full date format, because masketTextBox automatically parse dates incorrectly (WiseJ problem)
				//T2KDateResolved.Text = FCConvert.ToString(vViolation.DateResolved);
				T2KDateResolved.Text = Strings.Format(vViolation.DateResolved, "MM/dd/yyyy");
            }
			else
			{
				T2KDateResolved.Text = "";
			}
			txtContactName.Text = vViolation.ContactName;
			txtEmail.Text = vViolation.ContactEmail;
			txtName.Text = vViolation.FirstName;
			txtMiddle.Text = vViolation.MiddleName;
			txtLast.Text = vViolation.LastName;
			txtDesig.Text = vViolation.Desig;
			txtDoingBusinessAs.Text = vViolation.DBA;
			txtAddress1.Text = vViolation.Address1;
			txtAddress2.Text = vViolation.Address2;
			txtCity.Text = vViolation.City;
			txtState.Text = vViolation.State;
			txtZip.Text = vViolation.Zip;
			txtZip4.Text = vViolation.Zip4;
			txtFee.Text = FCConvert.ToString(vViolation.Fee);
			txtBusinessLicense.Text = vViolation.BusinessLicense;
			if (vViolation.StreetNumber > 0)
			{
				txtStreetNum.Text = FCConvert.ToString(vViolation.StreetNumber);
			}
			else
			{
				txtStreetNum.Text = "";
			}
			txtApt.Text = vViolation.Apt;
			txtStreet.Text = vViolation.Street;
			int x;
			if (cmbViolationType.Items.Count > 0)
			{
				cmbViolationType.SelectedIndex = 0;
				for (x = 0; x <= cmbViolationType.Items.Count - 1; x++)
				{
					if (cmbViolationType.ItemData(x) == vViolation.ViolationType)
					{
						cmbViolationType.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			txtIdentifier.Text = vViolation.Identifier;
			if (cmbStatus.Items.Count > 0)
			{
				cmbStatus.SelectedIndex = 0;
				for (x = 0; x <= cmbStatus.Items.Count - 1; x++)
				{
					if (cmbStatus.ItemData(x) == vViolation.Status)
					{
						cmbStatus.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			clsPhoneList PhoneList;
			int lngRow;
			PhoneList = vViolation.PhoneNumberList;
			if (!(PhoneList == null))
			{
				clsPhoneNumber tPN;
				PhoneList.MoveFirst();
				while (PhoneList.GetCurrentIndex >= 0)
				{
					tPN = PhoneList.GetCurrentPhoneNumber;
					if (!(tPN == null))
					{
						GridPhones.Rows += 1;
						lngRow = GridPhones.Rows - 1;
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(tPN.ID));
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC, tPN.Description);
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER, tPN.FormattedPhoneNumber);
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(PhoneList.GetCurrentIndex));
					}
					PhoneList.MoveNext();
				}
			}
			else
			{
				GridPhones.Rows = 1;
			}
			if (vViolation.Account > 0)
			{
				lblAccountLabel.Visible = true;
				lblAccount.Text = FCConvert.ToString(vViolation.Account);
			}
			else
			{
				lblAccountLabel.Visible = false;
				lblAccount.Text = "";
			}
			if (vViolation.Permit > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select permitidentifier from permits where ID = " + FCConvert.ToString(vViolation.Permit), modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					lblPermitLabel.Visible = true;
					lblPermit.Text = FCConvert.ToString(rsLoad.Get_Fields_String("permitidentifier"));
				}
				else
				{
					lblPermitLabel.Visible = false;
					lblPermit.Text = "";
				}
			}
			else
			{
				lblPermitLabel.Visible = false;
				lblPermit.Text = "";
			}
			if (vViolation.ID > 0)
			{
				if (!boolCantEdit)
				{
					cmdAddActivity.Enabled = true;
				}
				else
				{
					cmdAddActivity.Enabled = false;
				}
				gridActivity.Enabled = true;
				mnuPrintActivity.Enabled = true;
                mnuPrintActivityWithDetail.Enabled = true;
				alList.Clear();
				alList.LoadList(vViolation.Account, modCEConstants.CNSTCODETYPEVIOLATION, vViolation.ID);
				FillActivityGrid();
				dlAttachedDocs.Clear();
				dlAttachedDocs.LoadDocuments(vViolation.Account, modCEConstants.CNSTCODETYPEVIOLATION.ToString(), modGlobalVariables.Statics.strCEDatabase);
				FillGridDocuments();
				LoadGridOpens();
			}
			else
			{
				gridActivity.Enabled = false;
				cmdAddActivity.Enabled = false;
                mnuPrintActivity.Enabled = false;
                mnuPrintActivityWithDetail.Enabled = false;
				alList.Clear();
				FillActivityGrid();
				dlAttachedDocs.Clear();
				FillGridDocuments();
				LoadGridOpens();
			}
		}
		// vbPorter upgrade warning: lngVID As int	OnWrite(double, int)
		public void Init(int lngVID, bool boolReadOnly = false, int lngPermit = 0, int lngAccount = 0, int lnginspection = 0)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngReturn;
				boolCantEdit = boolReadOnly;
				vViolation.Clear();
				if (lnginspection > 0)
				{
					vViolation.Inspection = lnginspection;
				}
				if (lngPermit > 0)
				{
					vViolation.Permit = lngPermit;
				}
				if (lngAccount > 0)
				{
					vViolation.Account = lngAccount;
				}
				lngReturn = vViolation.LoadViolation(lngVID);
				if (lngReturn != 0)
				{
					fecherFoundation.Information.Err().Raise(lngReturn, null, null, null, null);
				}
				//FC:FINAL:DDU:#i1681 - view form in tab to don't overlap over others forms
				//this.Show(FCForm.FormShowEnum.Modal);
				this.Show(FCForm.FormShowEnum.Modal);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmViolation_Resize(object sender, System.EventArgs e)
		{
			ResizeGridActivity();
			ResizeGridDocuments();
			ResizeGridPhones();
			ResizeGridOpens();
		}

		private void ResizeGridOpens()
		{
			int GridWidth = 0;
			GridWidth = GridOpens.WidthOriginal;
			GridOpens.ColWidth(CNSTGRIDOPENSCOLDESC, FCConvert.ToInt32(0.6 * GridWidth));
			// .ColWidth(CNSTGRIDOPENSCOLVALUE) = 0.35 * GridWidth
		}

		private void gridDocuments_DblClick(object sender, System.EventArgs e)
		{
			int lngRow;
			lngRow = gridDocuments.MouseRow;
			if (lngRow > 0)
			{
                FCFileSystem fso = new FCFileSystem();
				//if (FCFileSystem.FileExists(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLFILENAME)))
				//{
					//frmViewDocuments.InstancePtr.Unload();
					//App.DoEvents();
                    var id = (int) Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID));
                    //frmViewDocuments.InstancePtr.Init(id, "", modCEConstants.CNSTCODETYPEVIOLATION.ToString(), vViolation.ID, "", modGlobalVariables.Statics.strCEDatabase, FCForm.FormShowEnum.Modal);
                    StaticSettings.GlobalCommandDispatcher.Send(new ShowSingleDocument(id,"", "CodeEnforcement", "Violation",
                        vViolation.ID, "", true, true));                    
    //            }
				//else
				//{
				//	MessageBox.Show("Unable to locate file", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				//}
			}
		}

		private void GridPhones_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridPhones.MouseRow;
			if (mRow < 0)
			{
				GridPhones.Row = 0;
				GridPhones.Focus();
			}
		}

		private void GridPhones_RowColChange(object sender, System.EventArgs e)
		{
			if (GridPhones.Row == GridPhones.Rows - 1)
			{
				if (GridPhones.Col == modCEConstants.CNSTGRIDPHONESCOLDESC)
				{
					GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void mnuDeleteViolation_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("This will permanently delete this violation" + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				vViolation.DeleteViolation();
				Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuPrintViolation_Click(object sender, System.EventArgs e)
		{
			vViolation.Description = txtDescription.Text;
			vViolation.Detail = rtbDetail.Text;
			if (cmbStatus.SelectedIndex >= 0)
			{
				vViolation.Status = cmbStatus.ItemData(cmbStatus.SelectedIndex);
			}
			if (cmbViolationType.SelectedIndex >= 0)
			{
				vViolation.ViolationType = cmbViolationType.ItemData(cmbViolationType.SelectedIndex);
			}
			if (Information.IsDate(t2kDateIssued.Text))
			{
				vViolation.DateIssued = FCConvert.ToDateTime(t2kDateIssued.Text);
			}
			else
			{
				vViolation.DateIssued = DateTime.FromOADate(0);
			}
			if (Information.IsDate(T2KDateOccurred.Text))
			{
				vViolation.DateOccurred = FCConvert.ToDateTime(T2KDateOccurred.Text);
			}
			else
			{
				vViolation.DateOccurred = DateTime.FromOADate(0);
			}
			if (Information.IsDate(T2KDateResolved.Text))
			{
				vViolation.DateResolved = FCConvert.ToDateTime(T2KDateResolved.Text);
			}
			else
			{
				vViolation.DateResolved = DateTime.FromOADate(0);
			}
			vViolation.Fee = Conversion.Val(txtFee.Text);
			vViolation.FirstName = txtName.Text;
			vViolation.MiddleName = txtMiddle.Text;
			vViolation.LastName = txtLast.Text;
			vViolation.Desig = txtDesig.Text;
			vViolation.StreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStreetNum.Text)));
			vViolation.Apt = txtApt.Text;
			vViolation.BusinessLicense = txtBusinessLicense.Text;
			vViolation.DBA = txtDoingBusinessAs.Text;
			vViolation.Address1 = txtAddress1.Text;
			vViolation.Address2 = txtAddress2.Text;
			vViolation.City = txtCity.Text;
			vViolation.Zip = txtZip.Text;
			vViolation.Zip4 = txtZip4.Text;
			vViolation.State = txtState.Text;
			vViolation.ContactEmail = txtEmail.Text;
			vViolation.ContactName = txtContactName.Text;
			rptViolation.InstancePtr.Init(vViolation.ID, vViolation);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridOpens.Row = 0;
			if (SaveViolation())
			{
				alList.LoadList(0, modCEConstants.CNSTCODETYPEVIOLATION, vViolation.ID);
				FillActivityGrid();
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridOpens.Row = 0;
			if (SaveViolation())
			{
				Close();
			}
		}

		private bool SaveViolation()
		{
			bool SaveViolation = false;
			SaveViolation = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngReturn;
				string strSQL = "";
				bool boolOK;
				boolOK = false;
				if (fecherFoundation.Strings.Trim(txtIdentifier.Text) == "")
				{
					if (vViolation.ID == 0 && !modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers)
					{
						MessageBox.Show("You have not provided an identifier and have chosen not to auto-generate identifiers" + "\r\n" + "An identifier must be provided before saving", "Identifier Empty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveViolation;
					}
					else if (vViolation.ID > 0)
					{
						MessageBox.Show("Identifiers can not be blank", "Identifier Empty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveViolation;
					}
				}
				if (!Information.IsDate(t2kDateIssued.Text))
				{
					MessageBox.Show("Date Issued cannot be blank", "Invalid Date Issued", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveViolation;
				}
				if (!(modGlobalVariables.Statics.CECustom.ViolationIdentifierValidation == FCConvert.ToInt32(clsCustomize.IdentifierValidation.NotUnique)))
				{
					if (!modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers)
					{
						// if auto generate then don't warn, just fix it when you save
						strSQL = "select id from violations where id <> " + FCConvert.ToString(vViolation.ID) + " and violationidentifier = '" + txtIdentifier.Text + "' ";
						if (modGlobalVariables.Statics.CECustom.ViolationIdentifierValidation == FCConvert.ToInt32(clsCustomize.IdentifierValidation.TypeUnique))
						{
							if (cmbStatus.SelectedIndex >= 0)
							{
								strSQL += " and violationtype = " + FCConvert.ToString(cmbStatus.ItemData(cmbStatus.SelectedIndex));
							}
						}
						clsDRWrapper rsTemp = new clsDRWrapper();
						rsTemp.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
						if (!rsTemp.EndOfFile())
						{
							MessageBox.Show("There is already another violation with this identifier", "Identifier Already Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SaveViolation;
						}
					}
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				if (!modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers || vViolation.ID > 0)
				{
					vViolation.Identifier = fecherFoundation.Strings.Trim(txtIdentifier.Text);
				}
				for (lngRow = GridDeletedPhones.Rows - 1; lngRow >= 0; lngRow--)
				{
					rsSave.Execute("delete from phonenumbers where ID = " + GridDeletedPhones.TextMatrix(lngRow, 0), modGlobalVariables.Statics.strCEDatabase);
					GridDeletedPhones.RemoveItem(lngRow);
				}
				// lngRow
				clsPhoneList tPList;
				tPList = vViolation.PhoneNumberList;
				clsPhoneNumber tPN;
				for (lngRow = 1; lngRow <= GridPhones.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX)) > 0)
					{
						tPN = tPList.Get_GetPhoneNumberByIndex(FCConvert.ToInt32(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX)));
						if (!(tPN == null))
						{
							tPN.Description = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC));
							tPN.PhoneNumber = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
						}
						else
						{
							tPN = new clsPhoneNumber();
							tPN.ID = 0;
							tPN.Deleted = false;
							tPN.Description = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC));
							tPN.PhoneNumber = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
							GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(tPList.InsertPhoneNumber(ref tPN)));
						}
					}
					else
					{
						tPN = new clsPhoneNumber();
						tPN.ID = 0;
						tPN.Deleted = false;
						tPN.Description = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC));
						tPN.PhoneNumber = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(tPList.InsertPhoneNumber(ref tPN)));
					}
				}
				// lngRow
				vViolation.Description = txtDescription.Text;
				vViolation.Detail = rtbDetail.Text;
				if (cmbStatus.SelectedIndex >= 0)
				{
					vViolation.Status = cmbStatus.ItemData(cmbStatus.SelectedIndex);
				}
				if (cmbViolationType.SelectedIndex >= 0)
				{
					vViolation.ViolationType = cmbViolationType.ItemData(cmbViolationType.SelectedIndex);
				}
				if (Information.IsDate(t2kDateIssued.Text))
				{
					vViolation.DateIssued = FCConvert.ToDateTime(t2kDateIssued.Text);
				}
				else
				{
					vViolation.DateIssued = DateTime.FromOADate(0);
				}
				if (Information.IsDate(T2KDateOccurred.Text))
				{
					vViolation.DateOccurred = FCConvert.ToDateTime(T2KDateOccurred.Text);
				}
				else
				{
					vViolation.DateOccurred = DateTime.FromOADate(0);
				}
				if (Information.IsDate(T2KDateResolved.Text))
				{
					vViolation.DateResolved = FCConvert.ToDateTime(T2KDateResolved.Text);
				}
				else
				{
					vViolation.DateResolved = DateTime.FromOADate(0);
				}
				vViolation.Fee = Conversion.Val(txtFee.Text);
				vViolation.FirstName = txtName.Text;
				vViolation.MiddleName = txtMiddle.Text;
				vViolation.LastName = txtLast.Text;
				vViolation.Desig = txtDesig.Text;
				vViolation.StreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStreetNum.Text)));
				vViolation.Apt = txtApt.Text;
				vViolation.BusinessLicense = txtBusinessLicense.Text;
				vViolation.DBA = txtDoingBusinessAs.Text;
				vViolation.Address1 = txtAddress1.Text;
				vViolation.Address2 = txtAddress2.Text;
				vViolation.City = txtCity.Text;
				vViolation.Zip = txtZip.Text;
				vViolation.Zip4 = txtZip4.Text;
				vViolation.State = txtState.Text;
				vViolation.ContactEmail = txtEmail.Text;
				vViolation.ContactName = txtContactName.Text;
				int lngOrigID;
				lngOrigID = vViolation.ID;
				lngReturn = vViolation.SaveViolation();
				if (lngOrigID == 0 && vViolation.ID > 0)
				{
					// add activity
					clsActivity tAct = new clsActivity();
					tAct.Account = vViolation.Account;
					tAct.ActivityDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					tAct.ActivityType = "Initial";
					tAct.Description = "Created Violation";
					tAct.Detail = "";
					tAct.ParentID = vViolation.ID;
					tAct.ParentType = modCEConstants.CNSTCODETYPEVIOLATION;
					tAct.ReferenceID = 0;
					tAct.ReferenceType = modCEConstants.CNSTACTIVITYTYPEINITIAL;
					tAct.SaveActivity();
				}
				boolOK = SaveGridOpens();
				if (lngReturn != 0)
				{
					fecherFoundation.Information.Err().Raise(lngReturn, null, null, null, null);
				}
				else if (!boolOK)
				{
				}
				else
				{
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveViolation = true;
					txtIdentifier.Text = vViolation.Identifier;
				}
				return SaveViolation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveViolation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveViolation;
		}

		private void AddActivity()
		{
			if (frmActivity.InstancePtr.Init(ref boolCantEdit, 0, vViolation.ID, modCEConstants.CNSTCODETYPEVIOLATION, 0, 0, vViolation.Account) > 0)
			{
				alList.LoadList(vViolation.Account, modCEConstants.CNSTCODETYPEVIOLATION, vViolation.ID);
				FillActivityGrid();
			}
		}

		private void FillActivityGrid()
		{
			gridActivity.Rows = 1;
			int lngRow;
			alList.MoveFirst();
			alList.MovePrevious();
			clsActivity tAct;
			clsAttachedDocuments dlAttachedDocs = new clsAttachedDocuments();
			while (alList.MoveNext() > 0)
			{
				tAct = alList.GetCurrentActivity();
				if (!(tAct == null))
				{
					gridActivity.Rows += 1;
					lngRow = gridActivity.Rows - 1;
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLAUTOID, FCConvert.ToString(tAct.ID));
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLDATE, Strings.Format(tAct.ActivityDate, "MM/dd/yyyy"));
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLDESC, tAct.Description);
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLTYPE, tAct.ActivityType);
					dlAttachedDocs.LoadDocuments(tAct.ID, modCEConstants.CNSTCODETYPEACTIVITY.ToString(), modGlobalVariables.Statics.strCEDatabase);
					if (dlAttachedDocs.Count > 0)
					{
                        //FC:FINAL:IPI - index correction: ImageList starts with index 1 in VB6
                        //gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDACTIVITYCOLDESC, MDIParent.InstancePtr.CEImageList.Images[2]);
                        gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDACTIVITYCOLDESC, MDIParent.InstancePtr.CEImageList.Images[1]);
                    }
					gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDACTIVITYCOLDESC, FCGrid.PictureAlignmentSettings.flexPicAlignRightCenter);
				}
			}
		}

		private void gridActivity_DblClick(object sender, System.EventArgs e)
		{
			if (gridActivity.MouseRow > 0)
			{
				frmActivity.InstancePtr.Init(ref boolCantEdit, FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.MouseRow, CNSTGRIDACTIVITYCOLAUTOID))), vViolation.ID, modCEConstants.CNSTCODETYPEVIOLATION);
				if (!boolCantEdit)
				{
					alList.LoadList(vViolation.Account, modCEConstants.CNSTCODETYPEVIOLATION, vViolation.ID);
					FillActivityGrid();
				}
			}
		}

		private void gridActivity_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (!boolCantEdit)
			{
				switch (e.KeyCode)
				{
					case Keys.Delete:
						{
							KeyCode = 0;
							if (gridActivity.Rows > 0)
							{
								if (MessageBox.Show("This will delete this activity entry" + "\r\n" + "Do you want to continue?", "Delete Activity?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
								{
									clsActivity tAct;
									tAct = alList.GetActivityByID(FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.Row, CNSTGRIDACTIVITYCOLAUTOID))));
									if (!(tAct == null))
									{
										tAct.DeleteActivity();
									}
									alList.RemovebyID(FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.Row, CNSTGRIDACTIVITYCOLAUTOID))));
									gridActivity.RemoveItem(gridActivity.Row);
								}
							}
							break;
						}
					case Keys.Insert:
						{
							if (!boolCantEdit)
							{
								AddActivity();
							}
							break;
						}
				}
				//end switch
			}
		}

		private void mnuAddActivity_Click(object sender, System.EventArgs e)
		{
			if (!boolCantEdit)
			{
				AddActivity();
			}
			else
			{
				MessageBox.Show("You cannot add activity until this violation has been created", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void mnuPrintActivity_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			rptActivity.InstancePtr.Init(vViolation.ID, modCEConstants.CNSTCODETYPEVIOLATION, false, strTemp, 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void mnuPrintActivityWithDetail_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			rptActivity.InstancePtr.Init(vViolation.ID, modCEConstants.CNSTCODETYPEVIOLATION, true, strTemp, 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void SetupGridActivity()
		{
			gridActivity.ColHidden(CNSTGRIDACTIVITYCOLAUTOID, true);
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLDATE, "Date");
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLDESC, "Description");
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLTYPE, "Type");
		}

		private void ResizeGridActivity()
		{
			int GridWidth = 0;
			GridWidth = gridActivity.WidthOriginal;
			gridActivity.ColWidth(CNSTGRIDACTIVITYCOLDATE, FCConvert.ToInt32(0.17 * GridWidth));
			gridActivity.ColWidth(CNSTGRIDACTIVITYCOLTYPE, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void SetupGridPhones()
		{
			GridPhones.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "Telephone");
			GridPhones.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLDESC, "Description");
			GridPhones.ColHidden(modCEConstants.CNSTGRIDPHONESCOLAUTOID, true);
			GridPhones.ColHidden(modCEConstants.CNSTGRIDPHONESCOLINDEX, true);
			GridPhones.ColEditMask(modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
			GridPhones.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void FillGridDocuments()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngReturn;
				int lngRow;
				clsAttachedDoc tDoc;
				lngReturn = dlAttachedDocs.LoadDocuments(vViolation.ID, modCEConstants.CNSTCODETYPEVIOLATION.ToString(), modGlobalVariables.Statics.strCEDatabase);
				if (lngReturn != 0)
				{
					fecherFoundation.Information.Err().Raise(lngReturn, null, null, null, null);
				}
				gridDocuments.Rows = 1;
				dlAttachedDocs.MoveFirst();
				dlAttachedDocs.MovePrevious();
				while (dlAttachedDocs.MoveNext() > 0)
				{
					tDoc = dlAttachedDocs.GetCurrentDoc();
					if (!(tDoc == null))
					{
						gridDocuments.Rows += 1;
						lngRow = gridDocuments.Rows - 1;
						gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID, FCConvert.ToString(tDoc.ID));
						gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLDESCRIPTION, tDoc.Description);
						gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLFILENAME, tDoc.FileName);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridDocuments", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RemoveDoc()
		{
			int lngRow;
			lngRow = gridDocuments.Row;
			if (lngRow > 0)
			{
				dlAttachedDocs.RemovebyID(FCConvert.ToInt32(Conversion.Val(gridDocuments.TextMatrix(lngRow, CNSTGRIDDOCSCOLAUTOID))));
				gridDocuments.RemoveItem(lngRow);
			}
		}

		private void mnuAttachDoc_Click(object sender, System.EventArgs e)
		{
			AddDoc();
		}

		private void AddDoc()
		{
			if (!boolCantEdit)
			{
                StaticSettings.GlobalCommandDispatcher.Send(new ShowAddCentralDocument("CodeEnforcement", "Violation", vViolation.ID, ""));
					FillGridDocuments();				
			}
		}

		private void SetupGridDocuments()
		{
			gridDocuments.ColHidden(CNSTGRIDDOCSCOLAUTOID, true);
			gridDocuments.TextMatrix(0, CNSTGRIDDOCSCOLDESCRIPTION, "Description");
			gridDocuments.TextMatrix(0, CNSTGRIDDOCSCOLFILENAME, "File Name");
		}

		private void ResizeGridDocuments()
		{
			int GridWidth = 0;
			GridWidth = gridDocuments.WidthOriginal;
			gridDocuments.ColWidth(CNSTGRIDDOCSCOLDESCRIPTION, FCConvert.ToInt32(0.6 * GridWidth));
		}

		private void ViewDocs()
		{
			//frmViewDocuments.InstancePtr.Unload();
			//frmViewDocuments.InstancePtr.Init(0, "", modCEConstants.CNSTCODETYPEVIOLATION.ToString(), vViolation.ID, "", modGlobalVariables.Statics.strCEDatabase, FCForm.FormShowEnum.Modal, boolCantEdit);
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer( "", "CodeEnforcement", "Violation",
                vViolation.ID, "", true, boolCantEdit));
            FillGridDocuments();
		}

		private void mnuViewDocuments_Click(object sender, System.EventArgs e)
		{
			ViewDocs();
		}

		private void mnuRemoveDocument_Click(object sender, System.EventArgs e)
		{
			if (!boolCantEdit)
			{
				RemoveDoc();
			}
		}

		private void GridPhones_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						GridPhones.Rows += 1;
						GridPhones.TopRow = GridPhones.Rows - 1;
						GridPhones.TextMatrix(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
						GridPhones.TextMatrix(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(0));
						GridPhones.Select(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER);
						GridPhones.EditCell();
						GridPhones.EditSelLength = 0;
						GridPhones.EditSelStart = 1;
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridPhones.Row < 1)
							return;
						DeletePhone(GridPhones.Row);
						break;
					}
			}
			//end switch
		}

		private void DeletePhone(int lngRow)
		{
			if (boolCantEdit)
				return;
			GridDeletedPhones.Rows += 1;
			GridDeletedPhones.TextMatrix(GridDeletedPhones.Rows - 1, 0, FCConvert.ToString(Conversion.Val(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID))));
			GridPhones.RemoveItem(lngRow);
		}

		private void ResizeGridPhones()
		{
			int GridWidth = 0;
			GridWidth = GridPhones.WidthOriginal;
			GridPhones.ColWidth(modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCConvert.ToInt32(0.4 * GridWidth));
		}

		private void gridDocuments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (!boolCantEdit)
			{
				switch (KeyCode)
				{
					case Keys.Insert:
						{
							KeyCode = 0;
							AddDoc();
							break;
						}
					case Keys.Delete:
						{
							KeyCode = 0;
							if (gridDocuments.Row > 0)
							{
								RemoveDoc();
							}
							break;
						}
				}
				//end switch
			}
		}

		private void GridOpens_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			Row = GridOpens.MouseRow;
			Col = GridOpens.MouseCol;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDOPENSCOLVALUE:
						{
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEVALUE)) Then
							if (!(FCConvert.ToString(GridOpens.Tag) == "UNAUTHORIZED") && !boolCantEdit)
							{
								GridOpens.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(Row));
							if (Conversion.Val(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
							{
								GridOpens.EditMask = "00/00/0000";
							}
							else
							{
								GridOpens.EditMask = "";
							}
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							// Case CNSTGRIDOPENSCOLDATE
							// GridOpens.ComboList = ""
							// GridOpens.EditMask = "00/00/0000"
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEDATE)) Then
							// GridOpens.Editable = flexEDKbdMouse
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							break;
						}
				}
				//end switch
			}
		}

		private void GridOpens_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridOpens.GetFlexRowIndex(e.RowIndex);
            int col = GridOpens.GetFlexColIndex(e.ColumnIndex);

            if (row < 1)
				return;
			string strTemp = "";
			string[] strAry = null;
			GridOpens.EditMask = "";
			if (col == CNSTGRIDOPENSCOLVALUE)
			{
				if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					strTemp = GridOpens.EditText;
					if (Information.IsDate(strTemp))
					{
						strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					}
					else
					{
						strTemp = "";
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strAry = Strings.Split(strTemp, ".", -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Format(strAry[0], "#,###,###,##0");
					if (Information.UBound(strAry, 1) > 0)
					{
						strTemp += "." + strAry[1];
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					strTemp = GridOpens.ComboData();
					GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, Conversion.Val(strTemp));
				}
				// Case CNSTGRIDOPENSCOLDATE
			}
		}

		private void SetupGridOpens(int lngSubType)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsCodes = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			GridOpens.Cols = 7;
			GridOpens.Rows = 1;
			GridOpens.ColHidden(CNSTGRIDOPENSCOLAUTOID, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLVALUEFORMAT, true);
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLDESC, "Item");
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLVALUE, "Value");
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMANDATORY, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMAX, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMIN, true);
			//GridOpens.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLVALUE, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			clsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATION) + " AND SUBTYPE = " + FCConvert.ToString(lngSubType) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				GridOpens.Rows += 1;
				lngRow = GridOpens.Rows - 1;
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(false));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("MINVALUE"))));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("maxvalue"))));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("valueformat"))));
				if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					// get the list of drop downs
					strTemp = "";
					clsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + clsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATION) + " order by code", modGlobalVariables.Statics.strCEDatabase);
					while (!clsCodes.EndOfFile())
					{
						strTemp += "#" + clsCodes.Get_Fields("code") + ";" + clsCodes.Get_Fields_String("description") + "|";
						clsCodes.MoveNext();
					}
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					}
					GridOpens.RowData(lngRow, strTemp);
				}
				else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("VALUEFORMAT")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
					GridOpens.RowData(lngRow, "Yes|No");
				}
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("mandatory")))
				{
					GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(true));
				}
				clsLoad.MoveNext();
			}
		}

		private bool SaveGridOpens()
		{
			bool SaveGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				bool boolMatch = false;
				string strTemp = "";
				int lngTemp = 0;
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				// shouldn't be able to get to this point without a permit number
				SaveGridOpens = false;
				if (vViolation.ID > 0)
				{
					clsSave.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(vViolation.ID) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATION) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
					{
						boolMatch = false;
						if (!clsSave.EndOfFile())
						{
							if (clsSave.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								boolMatch = true;
							}
						}
						if (boolMatch)
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
						clsSave.Set_Fields("ACCOUNT", vViolation.ID);
						clsSave.Set_Fields("codetype", modCEConstants.CNSTCODETYPEVIOLATION);
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))));
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveGridOpens;
							}
						}
						dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
						dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
						if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
						{
							// not allowing type date at this time
							if (Information.IsDate(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE)))
							{
								clsSave.Set_Fields("datevalue", Strings.Format(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), "MM/dd/yyyy"));
							}
							else
							{
								clsSave.Set_Fields("datevalue", 0);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == "Yes")
							{
								clsSave.Set_Fields("booleanvalue", true);
							}
							else
							{
								clsSave.Set_Fields("booleanvalue", false);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							dblTemp = Conversion.Val(strTemp);
							if (dblTemp != 0)
							{
								if (dblMin != 0 && dblMin > dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (dblTemp == 0)
								{
									if (dblMin > dblTemp || dblMax < dblTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", dblTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							clsSave.Set_Fields("dropdownvalue", FCConvert.ToString(Conversion.Val(GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE))));
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							if (lngTemp != 0)
							{
								if (dblMin != 0 && dblMin > lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (lngTemp == 0)
								{
									if (dblMin > lngTemp || dblMax < lngTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", lngTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
						{
							clsSave.Set_Fields("textvalue", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE));
						}
						// End If
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
						// If IsDate(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE)) Then
						// clsSave.Fields("datevalue") = Format(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE), "MM/dd/yyyy")
						// End If
						// Else
						// clsSave.Fields("datevalue") = 0
						// End If
						clsSave.Update();
					}
					// lngRow
				}
				SaveGridOpens = true;
				return SaveGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Eror Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridOpens;
		}

		private void LoadGridOpens()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				string[] strAry = null;
				string[] strAry2 = null;
				if (vViolation.ID > 0)
				{
					clsLoad.OpenRecordset("select * from USERcodevalues where account = " + FCConvert.ToString(vViolation.ID) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATION) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
						{
							if (clsLoad.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
								GridOpens.ComboList = "";
								if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
								{
									// not allowing type date at this time
									if (Information.IsDate(clsLoad.Get_Fields("datevalue")))
									{
										if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("datevalue")).ToOADate() != 0)
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(clsLoad.Get_Fields_DateTime("datevalue"), "MM/dd/yyyy"));
										}
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
								{
									if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("booleanvalue")))
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "Yes");
									}
									else
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "No");
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
								{
									GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(lngRow));
									// .TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) = Val(clsLoad.Fields("dropdownvalue"))
									GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE, Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")));
									strAry = Strings.Split(Strings.Replace(FCConvert.ToString(GridOpens.RowData(lngRow)), "#", "", 1, -1, CompareConstants.vbTextCompare), "|", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
									{
										strAry2 = Strings.Split(strAry[x], ";", -1, CompareConstants.vbTextCompare);
										if (Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")) == Conversion.Val(strAry2[0]))
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, strAry2[1]);
										}
									}
									// x
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))), "#,###,###,##0"));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(clsLoad.Get_Fields_String("textvalue")));
								}
							}
						}
						// lngRow
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CheckType(int lngType)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				SetupGridOpens(lngType);
				LoadGridOpens();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
