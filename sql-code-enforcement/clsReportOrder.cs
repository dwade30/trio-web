﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWCE0000
{
	public class clsReportOrder
	{
		//=========================================================
		private int lngAutoID;
		private int lngOrder;
		private string strOrderFields = "";
		private bool boolDescending;
		private string strDescription = string.Empty;
		private string strOrderByString = "";
		private bool boolUnused;

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public int ID
		{
			set
			{
				lngAutoID = value;
			}
			get
			{
				int ID = 0;
				ID = lngAutoID;
				return ID;
			}
		}

		public int Order
		{
			set
			{
				lngOrder = value;
			}
			get
			{
				int Order = 0;
				Order = lngOrder;
				return Order;
			}
		}

		public string Fields
		{
			set
			{
				if (value != strOrderFields)
				{
					strOrderFields = value;
					MakeOrderByString();
				}
			}
			get
			{
				string Fields = "";
				Fields = strOrderFields;
				return Fields;
			}
		}

		public bool Descending
		{
			set
			{
				if (value != boolDescending)
				{
					boolDescending = value;
					MakeOrderByString();
				}
			}
			get
			{
				bool Descending = false;
				Descending = boolDescending;
				return Descending;
			}
		}

		public bool Ascending
		{
			set
			{
				if (value == boolDescending)
				{
					boolDescending = !value;
					MakeOrderByString();
				}
			}
			get
			{
				bool Ascending = false;
				Ascending = !boolDescending;
				return Ascending;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string OrderByString
		{
			get
			{
				string OrderByString = "";
				OrderByString = strOrderByString;
				return OrderByString;
			}
		}

		private void MakeOrderByString()
		{
			string[] strAry = null;
			string strTemp = "";
			if (strOrderFields == string.Empty)
			{
				strOrderByString = "";
				return;
			}
			if (Descending)
			{
				strAry = Strings.Split(strOrderFields, ",", -1, CompareConstants.vbTextCompare);
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				strTemp = "";
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					if (x < Information.UBound(strAry, 1))
					{
						strTemp += strAry[x] + " desc,";
					}
					else
					{
						strTemp += strAry[x] + " desc";
					}
				}
				// x
				strOrderByString = strTemp;
			}
			else
			{
				strOrderByString = strOrderFields + " ";
			}
		}
	}
}
