//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptAccountPermitActivity.
	/// </summary>
	public partial class rptAccountPermitActivity : BaseSectionReport
	{
		public rptAccountPermitActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Account Permit History";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptAccountPermitActivity InstancePtr
		{
			get
			{
				return (rptAccountPermitActivity)Sys.GetInstance(typeof(rptAccountPermitActivity));
			}
		}

		protected rptAccountPermitActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAccountPermitActivity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		private bool boolDetail;

		public void Init(int lngAccountID, bool modalDialog)
		{
			rsReport.OpenRecordset("select * from cemaster where ID = " + FCConvert.ToString(lngAccountID) + " and not deleted = 1 order by ceaccount", modGlobalVariables.Statics.strCEDatabase);
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), strAttachmentName: "Account History", showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				SubReport1.Report = new srptAccountPermitActivity();
				SubReport1.Report.UserData = rsReport.Get_Fields_Int32("ceaccount") + "|" + rsReport.Get_Fields_String("maplot");
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
