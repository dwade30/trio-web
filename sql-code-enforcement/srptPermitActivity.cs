//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for srptPermitActivity.
	/// </summary>
	public partial class srptPermitActivity : FCSectionReport
	{
		public srptPermitActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptPermitActivity InstancePtr
		{
			get
			{
				return (srptPermitActivity)Sys.GetInstance(typeof(srptPermitActivity));
			}
		}

		protected srptPermitActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptPermitActivity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngPermitID;
		private clsActivityList alList = new clsActivityList();
		private bool boolDetail;

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngPermitID = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			boolDetail = true;
			int lngReturn;
			lngReturn = alList.LoadList(0, modCEConstants.CNSTCODETYPEPERMIT, lngPermitID);
			if (lngReturn == 0)
			{
				alList.MoveFirst();
			}
			else
			{
				this.Detail.Visible = false;
			}
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from permits where ID = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
			txtDateFiled.Text = "";
			txtPermitType.Text = "";
			txtPermit.Text = "";
			this.Fields["grpHeader"].Value = lngPermitID.ToString();
			if (!rsLoad.EndOfFile())
			{
				if (Information.IsDate(rsLoad.Get_Fields("applicationdate")))
				{
					if (rsLoad.Get_Fields_DateTime("applicationdate").ToOADate() != 0)
					{
						txtDateFiled.Text = Strings.Format(rsLoad.Get_Fields_DateTime("applicationdate"), "MM/dd/yyyy");
					}
				}
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("permitidentifier"))) != string.Empty)
				{
					txtPermit.Text = rsLoad.Get_Fields_String("permitidentifier");
				}
				else
				{
					txtPermit.Text = rsLoad.Get_Fields_Int32("permityear") + "-" + rsLoad.Get_Fields("permitnumber");
				}
				if (Conversion.Val(rsLoad.Get_Fields("PermitType")) > 0)
				{
					clsDRWrapper rsType = new clsDRWrapper();
					rsType.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " and ID = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("permittype"))), modGlobalVariables.Statics.strCEDatabase);
					if (!rsType.EndOfFile())
					{
						txtPermitType.Text = rsType.Get_Fields_String("Description");
					}
				}
			}
			rsLoad.Dispose();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = alList.GetCurrentIndex() < 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsActivity tAct;
			tAct = alList.GetCurrentActivity();
			if (!(tAct == null))
			{
				txtActivityDate.Text = Strings.Format(tAct.ActivityDate, "MM/dd/yyyy");
				txtDescription.Text = tAct.Description;
				txtType.Text = tAct.ActivityType;
				if (boolDetail)
				{
					txtDetail.Text = tAct.Detail;
				}
			}
			else
			{
				txtDetail.Text = "";
				txtDescription.Text = "";
				txtActivityDate.Text = "";
				txtType.Text = "";
			}
			alList.MoveNext();
		}

		

		private void ActiveReports_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("grpHeader");
		}
	}
}
