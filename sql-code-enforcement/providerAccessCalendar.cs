//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Diagnostics;
using Wisej.Web.Ext.FullCalendar;

namespace TWCE0000
{
	public class providerAccessCalendar
	{
		//=========================================================
		//AxXtremeCalendarControl.AxCalendarControl m_pCalendar = new AxXtremeCalendarControl.AxCalendarControl();
		FCCalendarControl m_pCalendar = new FCCalendarControl();
		SQLDataHelper m_pHelper = new SQLDataHelper();
		private bool boolShowAll;

		public bool ShowAllEvents
		{
			set
			{
				boolShowAll = value;
			}
			get
			{
				bool ShowAllEvents = false;
				ShowAllEvents = boolShowAll;
				return ShowAllEvents;
			}
		}

		// ======================================================================
		//FC:TODO
		// vbPorter upgrade warning: pCalendar As CalendarControl	OnWrite(XtremeCalendarControl.CalendarControl)
		public void SetCalendar(FCCalendarControl pCalendar)
		{
			m_pCalendar = pCalendar;
			m_pCalendar.Events.CollectionChanged -= CalendarEvents_CollectionChanged;
			m_pCalendar.Events.CollectionChanged += CalendarEvents_CollectionChanged;
			m_pHelper.SetCalendar(pCalendar);
		}

		private void CalendarEvents_CollectionChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e)
		{
			Event pEvent = e.Element as Event;
			bool bResult = true;
			int NewEventID = 0;

			switch (e.Action)
			{
				case System.ComponentModel.CollectionChangeAction.Add:

					m_pCalendar_DoCreateEvent(pEvent, ref NewEventID, ref bResult);

					break;
				case System.ComponentModel.CollectionChangeAction.Remove:
					m_pCalendar_DoDeleteEvent(pEvent, ref bResult);
					break;
			}
		}

		private void m_pCalendar_DoCreateEvent(Event pEvent, ref int NewEventID, ref bool bResult)
		{
			string strSQL;
			clsDRWrapper rsEvent = new clsDRWrapper();
			bResult = false;
			try
			{   // On Error GoTo err1
				fecherFoundation.Information.Err().Clear();
				strSQL = "SELECT * FROM NewTasks WHERE ID = 0";
				rsEvent.OpenRecordset(strSQL, modGlobalVariables.Statics.strGNDatabase);
				rsEvent.AddNew();
				string user = pEvent.UserData.User;
				m_pHelper.PutEventToRS(ref pEvent, rsEvent, ref user);
				rsEvent.Update();
				NewEventID = FCConvert.ToInt32(rsEvent.Get_Fields_Int32("ID"));
				// --------------------------------------------
				rsEvent.Reset();
				bResult = true;
				return;
			}
			catch (Exception ex)
			{   // err1:
				if (fecherFoundation.Information.Err(ex).Number != 0)
				{
					Debug.WriteLine("Cannot CreateEvent in DB: " + fecherFoundation.Information.Err(ex).Description);
					Debug.Assert(false);
				}
			}
		}

		//private void m_pCalendar_DoCreateRPattern(XtremeCalendarControl.CalendarRecurrencePattern pPatternref, int NewPatternID, ref bool bResult)
		//{
		//	bResult = false;
		//	try
		//	{	// On Error GoTo err1
		//		fecherFoundation.Information.Err().Clear();
		//		string strSQL;
		//		strSQL = "SELECT * FROM RecurrencePattern ";
		//		strSQL += " WHERE RecurrencePatternID = 0 ";
		//		clsDRWrapper rsPattern = new clsDRWrapper();
		//		rsPattern.OpenRecordset(strSQL, modGlobalVariables.strGNDatabase);
		//		rsPattern.AddNew();
		//		m_pHelper.PutRPatternToRS(ref pPattern, ref rsPattern);
		//		rsPattern.Update();
		//		NewPatternID = FCConvert.ToInt32(rsPattern.Get_Fields("RecurrencePatternID"));
		//		rsPattern.Reset();
		//		bResult = true;
		//		return;
		//	}
		//	catch (Exception ex)
		//	{	// err1:
		//		if (fecherFoundation.Information.Err(ex).Number!=0) {
		//			Debug.WriteLine("Cannot UpdateEvent in DB: "+fecherFoundation.Information.Err(ex).Description);
		//			Debug.Assert(false);
		//		}
		//	}
		//}
		private void m_pCalendar_DoDeleteEvent(Event pEvent, ref bool bResult)
		{
			string strSQL;
			clsDRWrapper rsDelete = new clsDRWrapper();
			strSQL = "DELETE FROM NewTasks WHERE ID = " + pEvent.Id.Substring(pEvent.Id.IndexOf("_") + 1);
            rsDelete.Execute(strSQL, modGlobalVariables.Statics.strGNDatabase);
			rsDelete.Reset();
			bResult = true;
		}
		//private void m_pCalendar_DoDeleteRPattern(XtremeCalendarControl.CalendarRecurrencePattern pPattern, ref bool bResult)
		//{
		//	string strSQL;
		//	clsDRWrapper rsDelete = new clsDRWrapper();
		//	strSQL = "DELETE FROM RecurrencePattern WHERE RecurrencePatternID = "+pPattern.ID;
		//	rsDelete.Execute(strSQL, modGlobalVariables.strGNDatabase);
		//	rsDelete.Reset();
		//	bResult = true;
		//}
		//private void m_pCalendar_DoGetUpcomingEvents(DateTime dtFrom, int PeriodMinutes, XtremeCalendarControl.CalendarEvents pEvents)
		//{
		//	string strSQL;
		//	clsDRWrapper rsEvents = new clsDRWrapper();
		//	strSQL = FCConvert.ToString(m_pHelper.MakeGetUpcomingEventsSQL(dtFrom, PeriodMinutes));
		//	// Debug.Print strSQL
		//	// ----------------------------------------------------------
		//	rsEvents.OpenRecordset(strSQL, modGlobalVariables.strGNDatabase);
		//	CalendarEvent pEvent = new CalendarEvent();
		//	while (!rsEvents.EndOfFile()) {
		//		pEvent = m_pHelper.CreateEventFromRS(ref rsEvents, false);
		//		if (!(pEvent==null)) {
		//			pEvents.Add(pEvent);
		//		}
		//		rsEvents.MoveNext();
		//	}
		//}
		//// vbPorter upgrade warning: pEvent As XtremeCalendarControl.CalendarEvent	OnWrite(CalendarEvent)
		//private void m_pCalendar_DoReadEvent(int EventID, ref XtremeCalendarControl.CalendarEvent pEvent)
		//{
		//	string strSQL;
		//	strSQL = "SELECT * FROM NewTasks WHERE ID = "+FCConvert.ToString(EventID);
		//	clsDRWrapper rsEvent = new clsDRWrapper();
		//	rsEvent.OpenRecordset(strSQL, modGlobalVariables.strGNDatabase);
		//	pEvent = m_pHelper.CreateEventFromRS(ref rsEvent, false);
		//}
		//// vbPorter upgrade warning: pPattern As XtremeCalendarControl.CalendarRecurrencePattern	OnWrite(CalendarRecurrencePattern)
		//private void m_pCalendar_DoReadRPattern(int PatternID, ref XtremeCalendarControl.CalendarRecurrencePattern pPattern)
		//{
		//	string strSQL;
		//	strSQL = "SELECT * FROM RecurrencePattern ";
		//	strSQL = strSQL+" WHERE RecurrencePatternID = "+FCConvert.ToString(PatternID);
		//	clsDRWrapper rsRPattern = new clsDRWrapper();
		//	rsRPattern.OpenRecordset(strSQL, modGlobalVariables.strGNDatabase);
		//	pPattern = m_pHelper.CreateRPatternFromRS(ref rsRPattern);
		//}
		// Public Function DoDeleteEvent(ByVal pEvent As XtremeCalendarControl.CalendarEvent) As Boolean
		// Dim strSQL As String
		// Dim rsDelete As New clsdrwrapper
		// Dim excepEv As XtremeCalendarControl.CalendarEvent
		//
		// If pEvent.RecurrenceState = xtpCalendarRecurrenceOccurrence Then
		// strSQL = "SELECT * FROM Tasks WHERE ID = " & pEvent.RecurrencePattern.MasterEventId
		//
		// rsDelete.OpenRecordset strSQL, strgndatabase
		//
		// Set excepEv = m_pCalendar.DataProvider.CreateEventEx(pEvent.EventID)
		// excepEv.StartTime = pEvent.RExceptionStartTimeOrig
		// excepEv.EndTime = pEvent.RExceptionEndTimeOrig
		// excepEv.MakeAsRExceptionEx rsDelete.Fields("RecurrencePatternID")
		// With excepEv
		// .Subject = pEvent.Subject
		// .Location = pEvent.Location
		// .Body = pEvent.Body
		// .MeetingFlag = pEvent.IsMeeting
		// .PrivateFlag = pEvent.IsPrivate
		// .Label = pEvent.LabelID
		// .BusyStatus = pEvent.BusyStatus
		// .Importance = pEvent.ImportanceLevel
		// .StartTime = pEvent.StartDateTime
		// .EndTime = pEvent.EndDateTime
		// .AllDayEvent = pEvent.IsAllDayEvent
		// .Reminder = pEvent.IsReminder
		// .ReminderMinutesBeforeStart = pEvent.ReminderMinutesBeforeStart
		// .ReminderSoundFile = pEvent.ReminderSoundFile
		// .CustomProperties.LoadFromString pEvent.CustomPropertiesXMLData
		// .ScheduleID = pEvent.ScheduleID
		// If pEvent.IsRecurrenceExceptionDeleted = True Then
		// .SetRExceptionDeleted True
		// End If
		// End With
		// DoCreateEvent excepEv
		// m_pCalendar.DataProvider.ChangeEvent excepEv
		// m_pCalendar.Populate
		// Else
		// strSQL = "DELETE FROM Tasks WHERE ID = " & pEvent.ID
		//
		// rsDelete.Execute strSQL, strgndatabase
		// End If
		// rsDelete.Reset
		// DoDeleteEvent = True
		// End Function
		//
		//FC:TODO
		//private void m_pCalendar_DoRetrieveDayEvents(DateTime dtDay, XtremeCalendarControl.CalendarEvents pEvents)
		//{
		//	string strSQL;
		//	strSQL = m_pHelper.MakeRetrieveDayEventsSQL(dtDay, modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID(), boolShowAll);
		//	clsDRWrapper rsEvents = new clsDRWrapper();
		//	rsEvents.OpenRecordset(strSQL, modGlobalVariables.strGNDatabase);
		//	CalendarEvent pEvent = new CalendarEvent();
		//	while (!rsEvents.EndOfFile()) {
		//		pEvent = m_pHelper.CreateEventFromRS(ref rsEvents, false);
		//		if (!(pEvent==null)) {
		//			pEvents.Add(pEvent);
		//		}
		//		rsEvents.MoveNext();
		//	}
		//}
		public void DoUpdateEvent(Event pEvent)
		{
			try
			{   // On Error GoTo err1
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				strSQL = "SELECT * FROM NewTasks WHERE ID = " + pEvent.Id.Substring(pEvent.Id.IndexOf("_") + 1);
				clsDRWrapper rsEvent = new clsDRWrapper();
				rsEvent.OpenRecordset(strSQL, modGlobalVariables.Statics.strGNDatabase);
				rsEvent.Edit();
                string user = pEvent.UserData.User;
                m_pHelper.PutEventToRS(ref pEvent, rsEvent, ref user);
				rsEvent.Update();
				rsEvent.Reset();
				return;
			}
			catch (Exception ex)
			{   // err1:
				if (fecherFoundation.Information.Err(ex).Number != 0)
				{
					Debug.WriteLine("Cannot UpdateEvent in DB: " + fecherFoundation.Information.Err(ex).Description);
					Debug.Assert(false);
				}
			}
		}
		//private void m_pCalendar_DoUpdateRPattern(XtremeCalendarControl.CalendarRecurrencePattern pPatternref, bool bResult)
		//{
		//	bResult = false;
		//	try
		//	{	// On Error GoTo err1
		//		fecherFoundation.Information.Err().Clear();
		//		string strSQL;
		//		strSQL = "SELECT * FROM RecurrencePattern ";
		//		strSQL = strSQL+" WHERE RecurrencePatternID = "+pPattern.ID;
		//		clsDRWrapper rsPattern = new clsDRWrapper();
		//		rsPattern.OpenRecordset(strSQL, modGlobalVariables.strGNDatabase);
		//		Debug.Assert(!rsPattern.EndOfFile() && !rsPattern.BeginningOfFile());
		//		rsPattern.Edit();
		//		m_pHelper.PutRPatternToRS(ref pPattern, ref rsPattern);
		//		rsPattern.Update();
		//		rsPattern.Reset();
		//		bResult = true;
		//		return;
		//	}
		//	catch (Exception ex)
		//	{	// err1:
		//		if (fecherFoundation.Information.Err(ex).Number!=0) {
		//			Debug.WriteLine("Cannot UpdateEvent in DB: "+fecherFoundation.Information.Err(ex).Description);
		//			Debug.Assert(false);
		//		}
		//	}
		//}
		//private void m_pCalendar_PrePopulate(XtremeCalendarControl.CalendarViewGroup ViewGroup, XtremeCalendarControl.CalendarEvents Events)
		//{
		//	int counter;
		//	if (Events.Count>0) {
		//		for(counter=Events.Count-1; counter>=0; counter--) {
		//			if (Events(counter).CustomProperties.Property("User")!=modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID() && Events(counter).PrivateFlag==true && !boolShowAll) {
		//				Events.Remove(counter);
		//			}
		//		}
		//	}
		//}
	}
}
