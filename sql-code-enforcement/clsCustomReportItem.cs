﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation.DataBaseLayer;
using fecherFoundation;
using System;

namespace TWCE0000
{
	public class clsCustomReportItem
	{
		//=========================================================
		private int intRow;
		private int intCol;
		private int intCodeType;
		private int lngOpenCode;
		private int intDataType;
		private int intTotalOption;

		public int TotalOption
		{
			set
			{
				intTotalOption = value;
			}
			get
			{
				int TotalOption = 0;
				TotalOption = intTotalOption;
				return TotalOption;
			}
		}

		public int Row
		{
			set
			{
				intRow = value;
			}
			get
			{
				int Row = 0;
				Row = intRow;
				return Row;
			}
		}

		public int Col
		{
			set
			{
				intCol = value;
			}
			get
			{
				int Col = 0;
				Col = intCol;
				return Col;
			}
		}

		public int CodeType
		{
			set
			{
				intCodeType = value;
			}
			get
			{
				int CodeType = 0;
				CodeType = intCodeType;
				return CodeType;
			}
		}

		public int OpenCode
		{
			set
			{
				lngOpenCode = value;
			}
			get
			{
				int OpenCode = 0;
				OpenCode = lngOpenCode;
				return OpenCode;
			}
		}

		public int TypeOfData
		{
			set
			{
				intDataType = value;
			}
			get
			{
				int TypeOfData = 0;
				TypeOfData = intDataType;
				return TypeOfData;
			}
		}

		public object Get_TypeFromCodes(int intCode, int lngOCode)
		{
			object TypeFromCodes = null;
			// vbPorter upgrade warning: intReturn As int	OnWrite(DAO.DataTypeEnum)
			int intReturn;
			intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			if (intCode == 1)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbLong);
			}
			else if (intCode >= 2 && intCode <= 6)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			}
			else if (intCode == 7)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbLong);
			}
			else if (intCode >= 8 && intCode <= 10)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			}
			else if (intCode >= 11 && intCode <= 12)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbDouble);
			}
			else if (intCode == 13)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbLong);
			}
			else if (intCode == 14)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			}
			else if (intCode == 15)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbDate);
			}
			else if (intCode >= 16 && intCode <= 17)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbLong);
			}
			else if (intCode >= 18 && intCode <= 19)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			}
			else if (intCode >= 20 && intCode <= 21)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbLong);
			}
			else if (intCode == 22)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbDate);
			}
			else if (intCode == 23)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			}
			else if (intCode >= 24 && intCode <= 26)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbText);
			}
			else if (intCode >= 27 && intCode <= 28)
			{
				intReturn = FCConvert.ToInt32(DataTypeEnum.dbLong);
			}
			else if (intCode == 29)
			{
				// property open code
			}
			else if (intCode == 30)
			{
				// contractor open codes
			}
			return TypeFromCodes;
		}
	}
}
