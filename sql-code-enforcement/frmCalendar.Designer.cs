﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmCalendar.
	/// </summary>
	partial class frmCalendar
	{
		public fecherFoundation.FCTimer TimerRMDForm;
		public fecherFoundation.FCCalendarControl CalendarControl;
		//public AxXtremeCommandBars.AxCommandBarsFrame CommandBarsFrame;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuContextMenu;
		public fecherFoundation.FCToolStripMenuItem mnuContextEditEvent;
		public fecherFoundation.FCToolStripMenuItem mnuOpenEvent;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteEvent;
		public fecherFoundation.FCToolStripMenuItem mnuContextNewEvent;
		public fecherFoundation.FCToolStripMenuItem mnuNewEvent;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.TimerRMDForm = new fecherFoundation.FCTimer(this.components);
            this.CalendarControl = new fecherFoundation.FCCalendarControl();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContextMenu = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContextEditEvent = new fecherFoundation.FCToolStripMenuItem();
            this.mnuOpenEvent = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteEvent = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContextNewEvent = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNewEvent = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdPrintEventListing = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.contextMenuDelete = new Wisej.Web.ContextMenu();
            this.menuDeleteEvent = new Wisej.Web.MenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintEventListing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 546);
            this.BottomPanel.Size = new System.Drawing.Size(1008, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.CalendarControl);
            this.ClientArea.Size = new System.Drawing.Size(1008, 486);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdPrintEventListing);
            this.TopPanel.Size = new System.Drawing.Size(1008, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintEventListing, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(112, 30);
            this.HeaderText.Text = "Calendar";
            // 
            // TimerRMDForm
            // 
            this.TimerRMDForm.Enabled = false;
            this.TimerRMDForm.Interval = 15000;
            this.TimerRMDForm.Location = new System.Drawing.Point(0, 0);
            this.TimerRMDForm.Name = null;
            this.TimerRMDForm.Tick += new System.EventHandler(this.TimerRMDForm_Tick);
            // 
            // CalendarControl
            // 
            this.CalendarControl.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.CalendarControl.Location = new System.Drawing.Point(30, 30);
            this.CalendarControl.Name = "CalendarControl";
            this.CalendarControl.Size = new System.Drawing.Size(948, 446);
            this.CalendarControl.EventClick += new Wisej.Web.Ext.FullCalendar.EventClickEventHandler(this.CalendarControl_EventClick);
            this.CalendarControl.EventDoubleClick += new Wisej.Web.Ext.FullCalendar.EventClickEventHandler(this.CalendarControl_EventDoubleClick);
            this.CalendarControl.DayDoubleClick += new Wisej.Web.Ext.FullCalendar.DayClickEventHandler(this.CalendarControl_DayDoubleClick);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            this.mnuFile.Visible = false;
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 0;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuContextMenu
            // 
            this.mnuContextMenu.Index = -1;
            this.mnuContextMenu.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuContextEditEvent,
            this.mnuContextNewEvent});
            this.mnuContextMenu.Name = "mnuContextMenu";
            this.mnuContextMenu.Text = "Context Menu";
            this.mnuContextMenu.Visible = false;
            // 
            // mnuContextEditEvent
            // 
            this.mnuContextEditEvent.Index = 0;
            this.mnuContextEditEvent.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuOpenEvent,
            this.mnuDeleteEvent});
            this.mnuContextEditEvent.Name = "mnuContextEditEvent";
            this.mnuContextEditEvent.Text = "Edit Event Context Menu";
            // 
            // mnuOpenEvent
            // 
            this.mnuOpenEvent.Index = 0;
            this.mnuOpenEvent.Name = "mnuOpenEvent";
            this.mnuOpenEvent.Text = "Open";
            this.mnuOpenEvent.Click += new System.EventHandler(this.mnuOpenEvent_Click);
            // 
            // mnuDeleteEvent
            // 
            this.mnuDeleteEvent.Index = 1;
            this.mnuDeleteEvent.Name = "mnuDeleteEvent";
            this.mnuDeleteEvent.Text = "Delete";
            this.mnuDeleteEvent.Click += new System.EventHandler(this.mnuDeleteEvent_Click);
            // 
            // mnuContextNewEvent
            // 
            this.mnuContextNewEvent.Index = 1;
            this.mnuContextNewEvent.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNewEvent});
            this.mnuContextNewEvent.Name = "mnuContextNewEvent";
            this.mnuContextNewEvent.Text = "New Event Context Menu";
            // 
            // mnuNewEvent
            // 
            this.mnuNewEvent.Index = 0;
            this.mnuNewEvent.Name = "mnuNewEvent";
            this.mnuNewEvent.Text = "New Event";
            this.mnuNewEvent.Click += new System.EventHandler(this.mnuNewEvent_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(801, 10);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdPrint.Size = new System.Drawing.Size(48, 26);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // cmdPrintEventListing
            // 
            this.cmdPrintEventListing.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintEventListing.Location = new System.Drawing.Point(855, 10);
            this.cmdPrintEventListing.Name = "cmdPrintEventListing";
            this.cmdPrintEventListing.Size = new System.Drawing.Size(128, 26);
            this.cmdPrintEventListing.TabIndex = 4;
            this.cmdPrintEventListing.Text = "Print Event Listing";
            this.cmdPrintEventListing.Click += new System.EventHandler(this.cmdPrintEventListing_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(432, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(140, 48);
            this.cmdPrintPreview.TabIndex = 3;
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
            // 
            // contextMenuDelete
            // 
            this.contextMenuDelete.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.menuDeleteEvent});
            this.contextMenuDelete.Name = "contextMenuDelete";
            // 
            // menuDeleteEvent
            // 
            this.menuDeleteEvent.Index = 0;
            this.menuDeleteEvent.Name = "menuDeleteEvent";
            this.menuDeleteEvent.Text = "Delete Event";
            this.menuDeleteEvent.Click += new System.EventHandler(this.menuDeleteEvent_Click);
            // 
            // frmCalendar
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1008, 654);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCalendar";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Calendar";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCalendar_Load);
            this.Activated += new System.EventHandler(this.frmCalendar_Activated);
            this.Resize += new System.EventHandler(this.frmCalendar_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCalendar_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintEventListing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
		//private fecherFoundation.FCButton cmdPageSetup;
		private fecherFoundation.FCButton cmdPrint;
		private fecherFoundation.FCButton cmdPrintPreview;
		private fecherFoundation.FCButton cmdPrintEventListing;
        private ContextMenu contextMenuDelete;
        private MenuItem menuDeleteEvent;
    }
}
