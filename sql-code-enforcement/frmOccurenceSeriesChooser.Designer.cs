//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmOccurrenceSeriesChooser.
	/// </summary>
	partial class frmOccurrenceSeriesChooser
	{
		public fecherFoundation.FCComboBox cmbOccurrence;
		public fecherFoundation.FCButton btnOK;
		public fecherFoundation.FCButton btnCancel;
		public fecherFoundation.FCLabel txtDescription;
		public fecherFoundation.FCLabel txtAction;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbOccurrence = new fecherFoundation.FCComboBox();
            this.btnOK = new fecherFoundation.FCButton();
            this.btnCancel = new fecherFoundation.FCButton();
            this.txtDescription = new fecherFoundation.FCLabel();
            this.txtAction = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 274);
            this.BottomPanel.Size = new System.Drawing.Size(438, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbOccurrence);
            this.ClientArea.Controls.Add(this.btnOK);
            this.ClientArea.Controls.Add(this.btnCancel);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtAction);
            this.ClientArea.Size = new System.Drawing.Size(438, 214);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(438, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(206, 30);
            this.HeaderText.Text = "Select Occurence";
            // 
            // cmbOccurrence
            // 
            this.cmbOccurrence.AutoSize = false;
            this.cmbOccurrence.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbOccurrence.FormattingEnabled = true;
            this.cmbOccurrence.Items.AddRange(new object[] {
            "Open the Occurence",
            "Open the Series"});
            this.cmbOccurrence.Location = new System.Drawing.Point(30, 105);
            this.cmbOccurrence.Name = "cmbOccurrence";
            this.cmbOccurrence.Size = new System.Drawing.Size(219, 40);
            this.cmbOccurrence.TabIndex = 0;
            // 
            // btnOK
            // 
            this.btnOK.AppearanceKey = "actionButton";
            this.btnOK.Location = new System.Drawing.Point(30, 166);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(90, 40);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.AppearanceKey = "actionButton";
            this.btnCancel.Location = new System.Drawing.Point(139, 166);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(90, 40);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(30, 30);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(331, 18);
            this.txtDescription.TabIndex = 1;
            this.txtDescription.Text = "\"RECURRENCE EVENT - LUNCH\" IS A RECURRING EVENT";
            // 
            // txtAction
            // 
            this.txtAction.Location = new System.Drawing.Point(30, 68);
            this.txtAction.Name = "txtAction";
            this.txtAction.Size = new System.Drawing.Size(386, 18);
            this.txtAction.TabIndex = 0;
            this.txtAction.Text = "DO YOU WANT TO OPEN ONLY THIS OCCURRENCE OR THE SERIES?";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(129, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(172, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            // 
            // frmOccurrenceSeriesChooser
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(438, 382);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmOccurrenceSeriesChooser";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Select Occurence";
            this.Load += new System.EventHandler(this.frmOccurrenceSeriesChooser_Load);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}