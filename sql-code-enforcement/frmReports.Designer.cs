//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmReports.
	/// </summary>
	partial class frmReports
	{
		public FCGrid GridOrderBy;
		public FCGrid GridParameters;
		public FCGrid GridReport;
		public FCGrid GridPropertySetup;
		public FCGrid GridContractorSetup;
		public FCGrid GridPermitSetup;
		public FCGrid GridInspectionSetup;
		public FCGrid gridCustomReports;
		public FCGrid gridCustomForms;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdLoadParameters;
		public fecherFoundation.FCButton cmdSaveParameters;
		public fecherFoundation.FCButton cmdSaveAs;
		public fecherFoundation.FCButton cmdDeleteParameters;
		public fecherFoundation.FCButton cmdSaveExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.GridOrderBy = new fecherFoundation.FCGrid();
            this.GridParameters = new fecherFoundation.FCGrid();
            this.GridReport = new fecherFoundation.FCGrid();
            this.GridPropertySetup = new fecherFoundation.FCGrid();
            this.GridContractorSetup = new fecherFoundation.FCGrid();
            this.GridPermitSetup = new fecherFoundation.FCGrid();
            this.GridInspectionSetup = new fecherFoundation.FCGrid();
            this.gridCustomReports = new fecherFoundation.FCGrid();
            this.gridCustomForms = new fecherFoundation.FCGrid();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.cmdLoadParameters = new fecherFoundation.FCButton();
            this.cmdSaveParameters = new fecherFoundation.FCButton();
            this.cmdSaveAs = new fecherFoundation.FCButton();
            this.cmdDeleteParameters = new fecherFoundation.FCButton();
            this.cmdSaveExit = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOrderBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPropertySetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridContractorSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPermitSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridInspectionSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomReports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomForms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteParameters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(708, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridOrderBy);
            this.ClientArea.Controls.Add(this.GridParameters);
            this.ClientArea.Controls.Add(this.GridReport);
            this.ClientArea.Controls.Add(this.GridPropertySetup);
            this.ClientArea.Controls.Add(this.GridContractorSetup);
            this.ClientArea.Controls.Add(this.GridPermitSetup);
            this.ClientArea.Controls.Add(this.GridInspectionSetup);
            this.ClientArea.Controls.Add(this.gridCustomReports);
            this.ClientArea.Controls.Add(this.gridCustomForms);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(708, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteParameters);
            this.TopPanel.Controls.Add(this.cmdSaveAs);
            this.TopPanel.Controls.Add(this.cmdSaveParameters);
            this.TopPanel.Controls.Add(this.cmdLoadParameters);
            this.TopPanel.Size = new System.Drawing.Size(708, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdLoadParameters, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveParameters, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveAs, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteParameters, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(98, 30);
            this.HeaderText.Text = "Reports";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // GridOrderBy
            // 
            this.GridOrderBy.AllowDrag = true;
            this.GridOrderBy.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridOrderBy.Cols = 4;
            this.GridOrderBy.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridOrderBy.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
            this.GridOrderBy.ExtendLastCol = true;
            this.GridOrderBy.Location = new System.Drawing.Point(30, 506);
            this.GridOrderBy.Name = "GridOrderBy";
            this.GridOrderBy.ReadOnly = false;
            this.GridOrderBy.Rows = 1;
            this.GridOrderBy.ShowFocusCell = false;
            this.GridOrderBy.Size = new System.Drawing.Size(530, 190);
            this.GridOrderBy.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.GridOrderBy, null);
            // 
            // GridParameters
            // 
            this.GridParameters.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridParameters.Cols = 6;
            this.GridParameters.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridParameters.ExtendLastCol = true;
            this.GridParameters.Location = new System.Drawing.Point(30, 201);
            this.GridParameters.Name = "GridParameters";
            this.GridParameters.ReadOnly = false;
            this.GridParameters.Rows = 50;
            this.GridParameters.ShowFocusCell = false;
            this.GridParameters.Size = new System.Drawing.Size(530, 260);
            this.GridParameters.StandardTab = false;
            this.GridParameters.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridParameters.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.GridParameters, "Use Insert and Delete to add and delete parameters");
            this.GridParameters.ComboCloseUp += new System.EventHandler(this.GridParameters_ComboCloseUp);
            this.GridParameters.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridParameters_CellChanged);
            this.GridParameters.CurrentCellChanged += new System.EventHandler(this.GridParameters_RowColChange);
            this.GridParameters.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridParameters_MouseDownEvent);
            this.GridParameters.KeyDown += new Wisej.Web.KeyEventHandler(this.GridParameters_KeyDownEvent);
            // 
            // GridReport
            // 
            this.GridReport.ColumnHeadersVisible = false;
            this.GridReport.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridReport.ExtendLastCol = true;
            this.GridReport.FixedCols = 0;
            this.GridReport.FixedRows = 0;
            this.GridReport.Location = new System.Drawing.Point(30, 52);
            this.GridReport.Name = "GridReport";
            this.GridReport.ReadOnly = false;
            this.GridReport.RowHeadersVisible = false;
            this.GridReport.Rows = 1;
            this.GridReport.ShowFocusCell = false;
            this.GridReport.Size = new System.Drawing.Size(249, 42);
            this.ToolTip1.SetToolTip(this.GridReport, null);
            this.GridReport.ComboCloseUp += new System.EventHandler(this.GridReport_ComboCloseUp);
            this.GridReport.Enter += new System.EventHandler(this.GridReport_Enter);
            this.GridReport.Click += new System.EventHandler(this.GridReport_ClickEvent);
            // 
            // GridPropertySetup
            // 
            this.GridPropertySetup.Cols = 4;
            this.GridPropertySetup.ColumnHeadersVisible = false;
            this.GridPropertySetup.FixedCols = 0;
            this.GridPropertySetup.FixedRows = 0;
            this.GridPropertySetup.Location = new System.Drawing.Point(0, 24);
            this.GridPropertySetup.Name = "GridPropertySetup";
            this.GridPropertySetup.RowHeadersVisible = false;
            this.GridPropertySetup.Rows = 0;
            this.GridPropertySetup.ShowFocusCell = false;
            this.GridPropertySetup.Size = new System.Drawing.Size(29, 10);
            this.GridPropertySetup.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.GridPropertySetup, null);
            this.GridPropertySetup.Visible = false;
            // 
            // GridContractorSetup
            // 
            this.GridContractorSetup.Cols = 4;
            this.GridContractorSetup.ColumnHeadersVisible = false;
            this.GridContractorSetup.FixedCols = 0;
            this.GridContractorSetup.FixedRows = 0;
            this.GridContractorSetup.Location = new System.Drawing.Point(0, 24);
            this.GridContractorSetup.Name = "GridContractorSetup";
            this.GridContractorSetup.RowHeadersVisible = false;
            this.GridContractorSetup.Rows = 0;
            this.GridContractorSetup.ShowFocusCell = false;
            this.GridContractorSetup.Size = new System.Drawing.Size(29, 10);
            this.GridContractorSetup.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.GridContractorSetup, null);
            this.GridContractorSetup.Visible = false;
            // 
            // GridPermitSetup
            // 
            this.GridPermitSetup.Cols = 4;
            this.GridPermitSetup.ColumnHeadersVisible = false;
            this.GridPermitSetup.FixedCols = 0;
            this.GridPermitSetup.FixedRows = 0;
            this.GridPermitSetup.Location = new System.Drawing.Point(0, 24);
            this.GridPermitSetup.Name = "GridPermitSetup";
            this.GridPermitSetup.RowHeadersVisible = false;
            this.GridPermitSetup.Rows = 0;
            this.GridPermitSetup.ShowFocusCell = false;
            this.GridPermitSetup.Size = new System.Drawing.Size(29, 10);
            this.GridPermitSetup.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.GridPermitSetup, null);
            this.GridPermitSetup.Visible = false;
            // 
            // GridInspectionSetup
            // 
            this.GridInspectionSetup.Cols = 4;
            this.GridInspectionSetup.ColumnHeadersVisible = false;
            this.GridInspectionSetup.FixedCols = 0;
            this.GridInspectionSetup.FixedRows = 0;
            this.GridInspectionSetup.Location = new System.Drawing.Point(0, 24);
            this.GridInspectionSetup.Name = "GridInspectionSetup";
            this.GridInspectionSetup.RowHeadersVisible = false;
            this.GridInspectionSetup.Rows = 0;
            this.GridInspectionSetup.ShowFocusCell = false;
            this.GridInspectionSetup.Size = new System.Drawing.Size(29, 10);
            this.GridInspectionSetup.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.GridInspectionSetup, null);
            this.GridInspectionSetup.Visible = false;
            // 
            // gridCustomReports
            // 
            this.gridCustomReports.Cols = 1;
            this.gridCustomReports.ColumnHeadersVisible = false;
            this.gridCustomReports.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridCustomReports.ExtendLastCol = true;
            this.gridCustomReports.FixedCols = 0;
            this.gridCustomReports.FixedRows = 0;
            this.gridCustomReports.Location = new System.Drawing.Point(30, 114);
            this.gridCustomReports.Name = "gridCustomReports";
            this.gridCustomReports.ReadOnly = false;
            this.gridCustomReports.RowHeadersVisible = false;
            this.gridCustomReports.Rows = 1;
            this.gridCustomReports.ShowFocusCell = false;
            this.gridCustomReports.Size = new System.Drawing.Size(282, 42);
            this.gridCustomReports.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.gridCustomReports, null);
            this.gridCustomReports.ComboCloseUp += new System.EventHandler(this.gridCustomReports_ComboCloseUp);
            this.gridCustomReports.Click += new System.EventHandler(this.gridCustomReports_ClickEvent);
            // 
            // gridCustomForms
            // 
            this.gridCustomForms.Cols = 1;
            this.gridCustomForms.ColumnHeadersVisible = false;
            this.gridCustomForms.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridCustomForms.ExtendLastCol = true;
            this.gridCustomForms.FixedCols = 0;
            this.gridCustomForms.FixedRows = 0;
            this.gridCustomForms.Location = new System.Drawing.Point(30, 114);
            this.gridCustomForms.Name = "gridCustomForms";
            this.gridCustomForms.ReadOnly = false;
            this.gridCustomForms.RowHeadersVisible = false;
            this.gridCustomForms.Rows = 1;
            this.gridCustomForms.ShowFocusCell = false;
            this.gridCustomForms.Size = new System.Drawing.Size(249, 42);
            this.gridCustomForms.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.gridCustomForms, null);
            this.gridCustomForms.Visible = false;
            this.gridCustomForms.ComboCloseUp += new System.EventHandler(this.gridCustomForms_ComboCloseUp);
            this.gridCustomForms.Click += new System.EventHandler(this.gridCustomForms_ClickEvent);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 478);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(485, 16);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "ORDER BY";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 173);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(485, 16);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "PARAMETERS";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 26);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(450, 16);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "REPORT";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // cmdLoadParameters
            // 
            this.cmdLoadParameters.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdLoadParameters.Location = new System.Drawing.Point(163, 29);
            this.cmdLoadParameters.Name = "cmdLoadParameters";
            this.cmdLoadParameters.Size = new System.Drawing.Size(118, 24);
            this.cmdLoadParameters.TabIndex = 4;
            this.cmdLoadParameters.Text = "Load Parameters";
            this.ToolTip1.SetToolTip(this.cmdLoadParameters, null);
            this.cmdLoadParameters.Click += new System.EventHandler(this.mnuLoadParameters_Click);
            // 
            // cmdSaveParameters
            // 
            this.cmdSaveParameters.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveParameters.Location = new System.Drawing.Point(284, 29);
            this.cmdSaveParameters.Name = "cmdSaveParameters";
            this.cmdSaveParameters.Size = new System.Drawing.Size(119, 24);
            this.cmdSaveParameters.TabIndex = 3;
            this.cmdSaveParameters.Text = "Save Parameters";
            this.ToolTip1.SetToolTip(this.cmdSaveParameters, null);
            this.cmdSaveParameters.Click += new System.EventHandler(this.mnuSaveParameters_Click);
            // 
            // cmdSaveAs
            // 
            this.cmdSaveAs.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveAs.Location = new System.Drawing.Point(406, 29);
            this.cmdSaveAs.Name = "cmdSaveAs";
            this.cmdSaveAs.Size = new System.Drawing.Size(140, 24);
            this.cmdSaveAs.TabIndex = 2;
            this.cmdSaveAs.Text = "Save Parameters As";
            this.ToolTip1.SetToolTip(this.cmdSaveAs, null);
            this.cmdSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // cmdDeleteParameters
            // 
            this.cmdDeleteParameters.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteParameters.Location = new System.Drawing.Point(549, 29);
            this.cmdDeleteParameters.Name = "cmdDeleteParameters";
            this.cmdDeleteParameters.Size = new System.Drawing.Size(130, 24);
            this.cmdDeleteParameters.TabIndex = 1;
            this.cmdDeleteParameters.Text = "Delete Parameters";
            this.ToolTip1.SetToolTip(this.cmdDeleteParameters, null);
            this.cmdDeleteParameters.Click += new System.EventHandler(this.mnuDeleteParameters_Click);
            // 
            // cmdSaveExit
            // 
            this.cmdSaveExit.AppearanceKey = "acceptButton";
            this.cmdSaveExit.Location = new System.Drawing.Point(262, 30);
            this.cmdSaveExit.Name = "cmdSaveExit";
            this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveExit.Size = new System.Drawing.Size(100, 48);
            this.cmdSaveExit.Text = "Continue";
            this.ToolTip1.SetToolTip(this.cmdSaveExit, null);
            this.cmdSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(708, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmReports";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Reports";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmReports_Load);
            this.Resize += new System.EventHandler(this.frmReports_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReports_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOrderBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPropertySetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridContractorSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPermitSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridInspectionSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomReports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridCustomForms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteParameters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}