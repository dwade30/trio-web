//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	public partial class frmContractors : BaseForm
	{
		public frmContractors()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmContractors InstancePtr
		{
			get
			{
				return (frmContractors)Sys.GetInstance(typeof(frmContractors));
			}
		}

		protected frmContractors _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By Corey
		// Date       03/30/2005
		// ********************************************************
		const int CNSTGRIDCLASSESCOLUSE = 1;
		const int CNSTGRIDCLASSESCOLDESCRIPTION = 0;
		const int CNSTGRIDCLASSESCOLLICENSE = 2;
		const int CNSTGRIDCLASSESCOLCODE = 3;
		const int CNSTGRIDOPENSCOLAUTOID = 0;
		const int CNSTGRIDOPENSCOLDESC = 1;
		const int CNSTGRIDOPENSCOLVALUE = 2;
		// Private Const CNSTGRIDOPENSCOLDATE = 3
		// Private Const CNSTGRIDOPENSCOLUSEVALUE = 4
		// Private Const CNSTGRIDOPENSCOLUSEDATE = 5
		const int CNSTGRIDOPENSCOLVALUEFORMAT = 3;
		const int CNSTGRIDOPENSCOLMANDATORY = 4;
		const int CNSTGRIDOPENSCOLMIN = 5;
		const int CNSTGRIDOPENSCOLMAX = 6;
		int lngCurrentContractor;
		bool boolDataChanged;
		bool boolLoading;
		bool boolViewOnly;
		bool boolCantEdit;
		// vbPorter upgrade warning: lngContractor As int	OnWrite(int, string)
		public void Init(int lngContractor)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			FillcmbStatus();
			SetupGridClasses();
			SetupGridPhones();
			SetupGridOpens();
			LoadGridClasses();
			boolDataChanged = false;
			lngCurrentContractor = 0;
			if (lngContractor > 0)
			{
				clsLoad.OpenRecordset("select * from contractors where ID = " + FCConvert.ToString(lngContractor), modGlobalVariables.Statics.strCEDatabase);
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("Contractor not found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					return;
				}
				else
				{
					lngCurrentContractor = lngContractor;
					LoadContractor();
				}
			}
			else
			{
				ClearContractor();
			}
			//FC:FINAL:DDU:#1968 - set boolean during form show
			boolLoading = true;
			this.Show(App.MainForm);
			boolLoading = false;
		}

		private void FillcmbStatus()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORSTATUS) + " and code <> " + FCConvert.ToString(modCEConstants.CNSTCONTRACTORSTATUSWARNING) + " order by code", modGlobalVariables.Statics.strCEDatabase);
			cmbStatus.Clear();
			while (!clsLoad.EndOfFile())
			{
				cmbStatus.AddItem(clsLoad.Get_Fields_String("description"));
				cmbStatus.ItemData(cmbStatus.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("code")));
				clsLoad.MoveNext();
			}
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORSTATUS) + " and code = " + FCConvert.ToString(modCEConstants.CNSTCONTRACTORSTATUSWARNING), modGlobalVariables.Statics.strCEDatabase);
			if (!clsLoad.EndOfFile())
			{
				cmbStatus.AddItem(clsLoad.Get_Fields_String("description"));
				cmbStatus.ItemData(cmbStatus.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("code")));
			}
		}

		private void SetupGridClasses()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridClasses.Cols = 4;
				GridClasses.Rows = 1;
				GridClasses.ColHidden(CNSTGRIDCLASSESCOLCODE, true);
				GridClasses.ColDataType(CNSTGRIDCLASSESCOLUSE, FCGrid.DataTypeSettings.flexDTBoolean);
				GridClasses.TextMatrix(0, CNSTGRIDCLASSESCOLDESCRIPTION, "Class");
				GridClasses.TextMatrix(0, CNSTGRIDCLASSESCOLLICENSE, "License");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridClasses", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGridClasses()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORCLASS) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					GridClasses.Rows += 1;
					lngRow = GridClasses.Rows - 1;
					GridClasses.TextMatrix(lngRow, CNSTGRIDCLASSESCOLUSE, FCConvert.ToString(false));
					GridClasses.TextMatrix(lngRow, CNSTGRIDCLASSESCOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))));
					GridClasses.TextMatrix(lngRow, CNSTGRIDCLASSESCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					// GridClasses.TextMatrix(lngRow, CNSTGRIDCLASSESCOLLICENSE) = clsLoad.Fields("license")
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridClasses", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridClasses()
		{
			int GridWidth = 0;
			GridWidth = GridClasses.WidthOriginal;
			GridClasses.ColWidth(CNSTGRIDCLASSESCOLDESCRIPTION, FCConvert.ToInt32(0.5 * GridWidth));
			GridClasses.ColWidth(CNSTGRIDCLASSESCOLUSE, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void UpdateChanges()
		{
			if (!boolLoading)
			{
				boolDataChanged = true;
			}
		}

		private void cmbStatus_TextChanged(object sender, System.EventArgs e)
		{
			UpdateChanges();
		}

		private void frmContractors_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				default:
					{
						if (KeyCode >= Keys.A && KeyCode <= Keys.Z)
						{
							UpdateChanges();
						}
						else if (KeyCode >= Keys.D0 && KeyCode <= Keys.D9)
						{
							UpdateChanges();
						}
						else if (KeyCode >= Keys.NumPad0 && KeyCode <= Keys.NumPad9)
						{
							UpdateChanges();
						}
						else if ((KeyCode == Keys.Add) || (KeyCode == Keys.Subtract) || (KeyCode == Keys.Space))
						{
							UpdateChanges();
						}
						else if ((KeyCode == Keys.Separator) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Decimal) || (KeyCode == Keys.Divide))
						{
							UpdateChanges();
						}
						else if ((KeyCode == Keys.Multiply) || (KeyCode == Keys.Back))
						{
							UpdateChanges();
						}
						else if ((KeyCode == (Keys)219) || (KeyCode == (Keys)221) || (KeyCode == (Keys)220) || (KeyCode == (Keys)192) || (KeyCode == (Keys)186) || (KeyCode == (Keys)222) || (KeyCode == (Keys)191) || (KeyCode == (Keys)187) || (KeyCode == (Keys)190) || (KeyCode == (Keys)188))
						{
							// keys:[]\~;'/=.,
							UpdateChanges();
						}
						break;
					}
			}
			//end switch
		}

		private void frmContractors_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmContractors properties;
			//frmContractors.FillStyle	= 0;
			//frmContractors.ScaleWidth	= 9300;
			//frmContractors.ScaleHeight	= 7500;
			//frmContractors.LinkTopic	= "Form2";
			//frmContractors.LockControls	= -1  'True;
			//frmContractors.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// Set ImgComment.Picture = MDIParent.ImageList2.ListImages(1).Picture
			txtContractorNumber.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITCONTRACTORSSECURITY, false))
			{
				boolCantEdit = true;
				DisableControls();
			}
		}

		private void DisableControls()
		{
			int x;
			for (x = 0; x <= this.Controls.Count - 1; x++)
			{
				if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "    ", 4)) != "GRID" && !(this.Controls[x] is FCToolStripMenuItem) && !(this.Controls[x] is FCLabel) && !(this.Controls[x] is FCTabControl))
				{
					this.Controls[x].Enabled = false;
				}
				else if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "    ", 4)) == "GRID")
				{
					(this.Controls[x] as FCGrid).Editable = FCGrid.EditableSettings.flexEDNone;
					this.Controls[x].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
					if ((this.Controls[x] as FCGrid).Rows > 0 && (this.Controls[x] as FCGrid).Cols > 0)
					{
						(this.Controls[x] as FCGrid).Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, (this.Controls[x] as FCGrid).Rows - 1, (this.Controls[x] as FCGrid).Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
					}
				}
			}
			// x
			cmdDelete.Enabled = false;
			cmdSave.Enabled = false;
			mnuSaveExit.Enabled = false;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveContractor())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmContractors_Resize(object sender, System.EventArgs e)
		{
			ResizeGridClasses();
			ResizeGridPhones();
			ResizeGridOpens();
		}

		private void DeletePhone(int lngRow)
		{
			GridDeletedPhones.Rows += 1;
			GridDeletedPhones.TextMatrix(GridDeletedPhones.Rows - 1, 0, FCConvert.ToString(Conversion.Val(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID))));
			GridPhones.RemoveItem(lngRow);
		}

		private void GridPhones_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						GridPhones.Rows += 1;
						GridPhones.TopRow = GridPhones.Rows - 1;
						GridPhones.TextMatrix(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
						GridPhones.Select(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER);
						GridPhones.EditCell();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridPhones.Row < 1)
							return;
						DeletePhone(GridPhones.Row);
						break;
					}
			}
			//end switch
		}

		private void GridPhones_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridPhones.MouseRow;
			if (mRow < 0)
			{
				GridPhones.Row = 0;
				GridPhones.Focus();
			}
		}

		private void GridPhones_RowColChange(object sender, System.EventArgs e)
		{
			if (GridPhones.Col == modCEConstants.CNSTGRIDPHONESCOLDESC && GridPhones.Row == GridPhones.Rows - 1)
			{
				GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void ImgComment_Click(object sender, System.EventArgs e)
		{
			mnuComment_Click();
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentContractor > 0)
			{
				if (!frmGlobalComment.InstancePtr.Init("CE", "Contractors", "Comment", "ID", lngCurrentContractor, "Comment", "", 0, boolCantEdit, true))
				{
					ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
				}
				else
				{
					ImgComment.Image = null;
				}
			}
			else
			{
				MessageBox.Show("You must save and create the contractor before you can assign it a comment", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void mnuComment_Click()
		{
			mnuComment_Click(mnuComment, new System.EventArgs());
		}

		private void mnuDeleteContractor_Click(object sender, System.EventArgs e)
		{
			if (boolCantEdit)
				return;
			if (lngCurrentContractor > 0)
			{
				if (MessageBox.Show("Are you sure you want to delete this contractor?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (modMain.DeleteContractor(lngCurrentContractor))
					{
						MessageBox.Show("Contractor Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
						mnuExit_Click();
					}
				}
			}
			else
			{
				// hasn't been created yet
				mnuExit_Click();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridOpens.Row = 0;
			GridPhones.Row = 0;
			SaveContractor();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridOpens.Row = 0;
			GridPhones.Row = 0;
			if (SaveContractor())
			{
				mnuExit_Click();
			}
		}

		private void LoadContractor()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngStatus = 0;
			boolLoading = true;
			ImgComment.Image = null;
			if (lngCurrentContractor > 0)
			{
				clsLoad.OpenRecordset("select * from contractors where ID = " + FCConvert.ToString(lngCurrentContractor), modGlobalVariables.Statics.strCEDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("comment"))) != string.Empty)
					{
						ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
					}
					txtAddress1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address1"));
					txtAddress2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address2"));
					txtCity.Text = FCConvert.ToString(clsLoad.Get_Fields_String("city"));
					txtContractorNumber.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("contractornumber")));
					txtEmail.Text = FCConvert.ToString(clsLoad.Get_Fields_String("email"));
					txtName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("name1"));
					txtName2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("name2"));
					// strTemp = Val(clsLoad.Fields("TELEPHONE1"))
					// strTemp = Format(strTemp, "0000000000")
					// strTemp = "(" & Mid(strTemp, 1, 3) & ")" & Mid(strTemp, 4, 3) & "-" & Mid(strTemp, 7, 4)
					// txtPhone.Text = strTemp
					// strTemp = Val(clsLoad.Fields("Telephone2"))
					// strTemp = Format(strTemp, "0000000000")
					// strTemp = "(" & Mid(strTemp, 1, 3) & ")" & Mid(strTemp, 4, 3) & "-" & Mid(strTemp, 7, 4)
					// txtPhone2.Text = strTemp
					// txtPhone1Desc.Text = clsLoad.Fields("TelephoneDesc1")
					// txtPhone2Desc.Text = clsLoad.Fields("TelephoneDesc2")
					txtState.Text = FCConvert.ToString(clsLoad.Get_Fields("state"));
					txtZip.Text = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
					txtZip4.Text = FCConvert.ToString(clsLoad.Get_Fields_String("zip4"));
					lngStatus = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("contractorstatus"))));
					for (x = 0; x <= cmbStatus.Items.Count - 1; x++)
					{
						if (cmbStatus.ItemData(x) == lngStatus)
						{
							cmbStatus.SelectedIndex = x;
							break;
						}
					}
					// x
					clsLoad.OpenRecordset("select * from classcodes where contractorid = " + FCConvert.ToString(lngCurrentContractor), modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						for (x = 1; x <= (GridClasses.Rows - 1); x++)
						{
							if (clsLoad.FindFirstRecord("code", Conversion.Val(GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLCODE))))
							{
								GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLUSE, FCConvert.ToString(true));
								GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLLICENSE, FCConvert.ToString(clsLoad.Get_Fields_String("license")));
							}
						}
						// x
					}
					int lngRow;
					clsLoad.OpenRecordset("select * from phonenumbers where phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPECONTRACTOR) + " and parentid = " + FCConvert.ToString(lngCurrentContractor), modGlobalVariables.Statics.strCEDatabase);
					GridPhones.Rows = 1;
					while (!clsLoad.EndOfFile())
					{
						GridPhones.Rows += 1;
						lngRow = GridPhones.Rows - 1;
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("PHONEdescription")));
						strTemp = Strings.Right(Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("Phonenumber"), 10);
						strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER, strTemp);
						clsLoad.MoveNext();
					}
					LoadGridOpens();
				}
				else
				{
					ClearContractor();
				}
			}
			else
			{
				ClearContractor();
			}
			boolLoading = false;
		}

		private bool SaveContractor()
		{
			bool SaveContractor = false;
			bool boolLocked = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				string strTemp = "";
				// vbPorter upgrade warning: lngTemp As int	OnWrite(double, int)
				int lngTemp = 0;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				int lngContractorNumber;
				int lngRow;
				bool boolOK;
				SaveContractor = false;
				boolOK = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				GridClasses.Col = 0;
				for (lngRow = GridDeletedPhones.Rows - 1; lngRow >= 0; lngRow--)
				{
					clsSave.Execute("delete from phonenumbers where ID = " + GridDeletedPhones.TextMatrix(lngRow, 0), modGlobalVariables.Statics.strCEDatabase);
					GridDeletedPhones.RemoveItem(lngRow);
				}
				// lngRow
				if (lngCurrentContractor <= 0)
				{
					if (modMain.AttemptLock(ref strTemp, modCEConstants.CNSTCONTRACTLOCK))
					{
						// use the lock and close it as quickly as possible
						// don't keep the lock while saving everything
						boolLocked = true;
						clsSave.OpenRecordset("select max(contractornumber) as maxnum from contractors", modGlobalVariables.Statics.strCEDatabase);
						if (!clsSave.EndOfFile())
						{
							lngTemp = FCConvert.ToInt32(Conversion.Val(clsSave.Get_Fields("maxnum")) + 1);
						}
						else
						{
							lngTemp = 1;
						}
						clsSave.OpenRecordset("select * from contractors where ID = -1", modGlobalVariables.Statics.strCEDatabase);
						clsSave.AddNew();
						clsSave.Set_Fields("contractornumber", lngTemp);
						lngContractorNumber = lngTemp;
						clsSave.Update();
						lngCurrentContractor = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
						modMain.ReleaseLock(modCEConstants.CNSTCONTRACTLOCK);
						boolLocked = false;
					}
					else
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Couldn't obtain lock to assign new contractor number" + "\r\n" + "Locked out by " + strTemp, "Locked Out", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveContractor;
					}
				}
				clsSave.OpenRecordset("select * from contractors where ID = " + FCConvert.ToString(lngCurrentContractor), modGlobalVariables.Statics.strCEDatabase);
				clsSave.Edit();
				lngContractorNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields_Int32("contractornumber"))));
				clsSave.Set_Fields("Name1", txtName.Text);
				clsSave.Set_Fields("Name2", txtName2.Text);
				clsSave.Set_Fields("address1", txtAddress1.Text);
				clsSave.Set_Fields("address2", txtAddress2.Text);
				clsSave.Set_Fields("city", txtCity.Text);
				clsSave.Set_Fields("state", fecherFoundation.Strings.Trim(Strings.Mid(txtState.Text + "  ", 1, 2)));
				clsSave.Set_Fields("zip", txtZip.Text);
				clsSave.Set_Fields("zip4", txtZip4.Text);
				// strTemp = txtPhone.Text
				// strTemp = Replace(strTemp, "(", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, ")", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
				// strTemp = Format(strTemp, "0000000000")
				// clsSave.Fields("telephone1") = strTemp
				// strTemp = txtPhone2.Text
				// strTemp = Replace(strTemp, "(", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, ")", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
				// strTemp = Format(strTemp, "0000000000")
				// clsSave.Fields("telephone2") = strTemp
				// clsSave.Fields("telephonedesc1") = txtPhone1Desc.Text
				// clsSave.Fields("telephonedesc2") = txtPhone2Desc.Text
				clsSave.Set_Fields("email", txtEmail.Text);
				if (cmbStatus.SelectedIndex >= 0)
				{
					clsSave.Set_Fields("contractorSTATUS", cmbStatus.ItemData(cmbStatus.SelectedIndex));
				}
				else
				{
					clsSave.Set_Fields("contractorstatus", 0);
				}
				clsSave.Update();
				bool boolUse = false;
				clsSave.OpenRecordset("select * from classcodes where contractorid = " + FCConvert.ToString(lngContractorNumber) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				for (x = 1; x <= (GridClasses.Rows - 1); x++)
				{
					if (FCConvert.CBool(GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLUSE)))
					{
						boolUse = true;
					}
					else
					{
						boolUse = false;
					}
					if (!clsSave.EndOfFile())
					{
						if (clsSave.FindFirstRecord("Code", Conversion.Val(GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLCODE))))
						{
							if (boolUse)
							{
								// no need to do anything
								clsSave.Edit();
							}
							else
							{
								clsSave.Delete();
							}
						}
						else
						{
							if (boolUse)
							{
								clsSave.AddNew();
							}
						}
					}
					else
					{
						if (boolUse)
						{
							clsSave.AddNew();
						}
					}
					if (boolUse)
					{
						clsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLCODE))));
						clsSave.Set_Fields("Contractorid", lngContractorNumber);
						clsSave.Set_Fields("license", GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLLICENSE));
						clsSave.Update();
					}
				}
				// x
				for (lngRow = 1; lngRow <= GridPhones.Rows - 1; lngRow++)
				{
					strTemp = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Right(Strings.StrDup(10, "0") + strTemp, 10);
					clsSave.OpenRecordset("select * from PHONENUMBERS where ID = " + FCConvert.ToString(Conversion.Val(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID))), modGlobalVariables.Statics.strCEDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
					}
					clsSave.Set_Fields("phonenumber", strTemp);
					clsSave.Set_Fields("phonecode", modCEConstants.CNSTPHONETYPECONTRACTOR);
					clsSave.Set_Fields("parentid", lngCurrentContractor);
					clsSave.Set_Fields("phonedescription", fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC)));
					clsSave.Update();
					GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
				}
				// lngRow
				boolOK = boolOK && SaveGridOpens();
				boolDataChanged = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				SaveContractor = boolOK;
				if (boolOK)
				{
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return SaveContractor;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (boolLocked)
				{
					modMain.ReleaseLock(modCEConstants.CNSTCONTRACTLOCK);
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveContractor", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveContractor;
		}

		private void ClearContractor()
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			boolLoading = true;
			txtAddress1.Text = "";
			txtAddress2.Text = "";
			txtCity.Text = "";
			txtContractorNumber.Text = "";
			txtName.Text = "";
			txtName2.Text = "";
			// txtPhone.Text = "(000)000-0000"
			// txtPhone1Desc.Text = ""
			// txtPhone2.Text = "(000)000-0000"
			// txtPhone2Desc.Text = ""
			txtState.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			txtEmail.Text = "";
			for (x = 1; x <= (GridClasses.Rows - 1); x++)
			{
				GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLUSE, FCConvert.ToString(false));
				GridClasses.TextMatrix(x, CNSTGRIDCLASSESCOLLICENSE, "");
			}
			// x
			boolLoading = false;
		}

		private void txtAddress1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtAddress2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtCity_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtEmail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtLicense_Validate(ref bool Cancel)
		{
			UpdateChanges();
		}

		private void txtName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtName2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtPhone_Validate(ref bool Cancel)
		{
			UpdateChanges();
		}

		private void txtPhone1Desc_Validate(ref bool Cancel)
		{
			UpdateChanges();
		}

		private void txtPhone2_Validate(ref bool Cancel)
		{
			UpdateChanges();
		}

		private void txtPhone2Desc_Validate(ref bool Cancel)
		{
			UpdateChanges();
		}

		private void txtState_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtZip_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void txtZip4_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			UpdateChanges();
		}

		private void SetupGridPhones()
		{
			GridPhones.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "Telephone");
			GridPhones.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLDESC, "Description");
			GridPhones.ColHidden(modCEConstants.CNSTGRIDPHONESCOLAUTOID, true);
			GridPhones.ColEditMask(modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
			//GridPhones.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void ResizeGridPhones()
		{
			int GridWidth = 0;
			GridWidth = GridPhones.WidthOriginal;
			GridPhones.ColWidth(modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCConvert.ToInt32(0.4 * GridWidth));
		}

		private void GridOpens_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			Row = GridOpens.MouseRow;
			Col = GridOpens.MouseCol;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDOPENSCOLVALUE:
						{
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEVALUE)) Then
							if (!boolCantEdit)
							{
								GridOpens.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(Row));
							if (Conversion.Val(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
							{
								GridOpens.EditMask = "00/00/0000";
							}
							else
							{
								GridOpens.EditMask = "";
							}
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							// Case CNSTGRIDOPENSCOLDATE
							// GridOpens.ComboList = ""
							// GridOpens.EditMask = "00/00/0000"
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEDATE)) Then
							// GridOpens.Editable = flexEDKbdMouse
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							break;
						}
				}
				//end switch
			}
		}

		private void GridOpens_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridOpens.GetFlexRowIndex(e.RowIndex);
            int col = GridOpens.GetFlexColIndex(e.ColumnIndex);

			if (row < 1)
				return;
			string strTemp = "";
			string[] strAry = null;
			GridOpens.EditMask = "";
			if (col == CNSTGRIDOPENSCOLVALUE)
			{

				if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					strTemp = GridOpens.EditText;
					if (Information.IsDate(strTemp))
					{
						strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					}
					else
					{
						strTemp = "";
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strAry = Strings.Split(strTemp, ".", -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Format(strAry[0], "#,###,###,##0");
					if (Information.UBound(strAry, 1) > 0)
					{
						strTemp += "." + strAry[1];
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					strTemp = GridOpens.ComboData();
					GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, Conversion.Val(strTemp));
				}
			}
		}

		private void SetupGridOpens()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsCodes = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			GridOpens.Cols = 7;
			GridOpens.Rows = 1;
			GridOpens.ColHidden(CNSTGRIDOPENSCOLAUTOID, true);
			// .ColHidden(CNSTGRIDOPENSCOLUSEVALUE) = True
			// .ColHidden(CNSTGRIDOPENSCOLUSEDATE) = True
			GridOpens.ColHidden(CNSTGRIDOPENSCOLVALUEFORMAT, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMANDATORY, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMIN, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMAX, true);
			GridOpens.ColDataType(CNSTGRIDOPENSCOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLDESC, "Item");
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLVALUE, "Value");
			// .TextMatrix(0, CNSTGRIDOPENSCOLDATE) = "Date"
			//GridOpens.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLVALUE, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLDATE) = flexAlignCenterCenter
			clsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				GridOpens.Rows += 1;
				lngRow = GridOpens.Rows - 1;
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(false));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("MINVALUE"))));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("maxvalue"))));
				// If Not clsLoad.Fields("usevalue") Then
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLVALUE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = False
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = True
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("valueformat"))));
				if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					// get the list of drop downs
					strTemp = "";
					clsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + clsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by code", modGlobalVariables.Statics.strCEDatabase);
					while (!clsCodes.EndOfFile())
					{
						strTemp += "#" + clsCodes.Get_Fields("code") + ";" + clsCodes.Get_Fields_String("description") + "|";
						clsCodes.MoveNext();
					}
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					}
					GridOpens.RowData(lngRow, strTemp);
				}
				else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
					GridOpens.RowData(lngRow, "Yes|No");
				}
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("mandatory")))
				{
					GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(true));
				}
				// If Not clsLoad.Fields("usedate") Then
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = False
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLDATE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = True
				// End If
				clsLoad.MoveNext();
			}
		}

		private void ResizeGridOpens()
		{
			int GridWidth = 0;
			GridWidth = GridOpens.WidthOriginal;
			GridOpens.ColWidth(CNSTGRIDOPENSCOLDESC, FCConvert.ToInt32(0.4 * GridWidth));
			GridOpens.ColWidth(CNSTGRIDOPENSCOLVALUE, FCConvert.ToInt32(0.35 * GridWidth));
		}

		private void LoadGridOpens()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				string[] strAry = null;
				string[] strAry2 = null;
				if (lngCurrentContractor > 0)
				{
					clsLoad.OpenRecordset("select * from USERcodevalues where account = " + FCConvert.ToString(lngCurrentContractor) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
						{
							if (clsLoad.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
								GridOpens.ComboList = "";
								if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
								{
									// not allowing type date at this time
									if (Information.IsDate(clsLoad.Get_Fields("datevalue")))
									{
										if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("datevalue")).ToOADate() != 0)
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(clsLoad.Get_Fields_DateTime("datevalue"), "MM/dd/yyyy"));
										}
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
								{
									if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("booleanvalue")))
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "Yes");
									}
									else
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "No");
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
								{
									GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(lngRow));
									// .TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) = Val(clsLoad.Fields("dropdownvalue"))
									GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE, Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")));
									strAry = Strings.Split(Strings.Replace(FCConvert.ToString(GridOpens.RowData(lngRow)), "#", "", 1, -1, CompareConstants.vbTextCompare), "|", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
									{
										strAry2 = Strings.Split(strAry[x], ";", -1, CompareConstants.vbTextCompare);
										if (Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")) == Conversion.Val(strAry2[0]))
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, strAry2[1]);
										}
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))), "#,###,###,##0"));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(clsLoad.Get_Fields_String("textvalue")));
								}
								// End If
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
								// If IsDate(clsLoad.Fields("DateValue")) Then
								// If clsLoad.Fields("datevalue") <> 0 Then
								// .TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE) = Format(clsLoad.Fields("datevalue"), "MM/dd/yyyy")
								// End If
								// End If
								// End If
							}
						}
						// lngRow
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveGridOpens()
		{
			bool SaveGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				bool boolMatch = false;
				string strTemp = "";
				int lngTemp = 0;
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				// shouldn't be able to get to this point without an account number
				SaveGridOpens = false;
				if (lngCurrentContractor > 0)
				{
					clsSave.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(lngCurrentContractor) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
					{
						boolMatch = false;
						if (!clsSave.EndOfFile())
						{
							if (clsSave.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								boolMatch = true;
							}
						}
						if (boolMatch)
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
						clsSave.Set_Fields("ACCOUNT", lngCurrentContractor);
						clsSave.Set_Fields("codetype", modCEConstants.CNSTCODETYPECONTRACTOR);
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))));
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveGridOpens;
							}
						}
						dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
						dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
						if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
						{
							// not allowing type date at this time
							if (Information.IsDate(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE)))
							{
								clsSave.Set_Fields("datevalue", Strings.Format(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), "MM/dd/yyyy"));
							}
							else
							{
								clsSave.Set_Fields("datevalue", 0);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == "Yes")
							{
								clsSave.Set_Fields("booleanvalue", true);
							}
							else
							{
								clsSave.Set_Fields("booleanvalue", false);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							dblTemp = Conversion.Val(strTemp);
							if (dblTemp != 0)
							{
								if (dblMin != 0 && dblMin > dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (dblTemp == 0)
								{
									if (dblMin > dblTemp || dblMax < dblTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", dblTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							clsSave.Set_Fields("dropdownvalue", FCConvert.ToString(Conversion.Val(GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE))));
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							if (lngTemp != 0)
							{
								if (dblMin != 0 && dblMin > lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (lngTemp == 0)
								{
									if (dblMin > lngTemp || dblMax < lngTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", lngTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
						{
							clsSave.Set_Fields("textvalue", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE));
						}
						//end switch
						// End If
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
						// If IsDate(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE)) Then
						// clsSave.Fields("datevalue") = Format(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE), "MM/dd/yyyy")
						// End If
						// Else
						// clsSave.Fields("datevalue") = 0
						// End If
						clsSave.Update();
					}
					// lngRow
				}
				SaveGridOpens = true;
				return SaveGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Eror Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridOpens;
		}
	}
}
