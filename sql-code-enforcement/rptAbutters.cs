﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptAbutters.
	/// </summary>
	public partial class rptAbutters : BaseSectionReport
	{
		public rptAbutters()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Abutters";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptAbutters InstancePtr
		{
			get
			{
				return (rptAbutters)Sys.GetInstance(typeof(rptAbutters));
			}
		}

		protected rptAbutters _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAcct.Dispose();
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAbutters	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		clsDRWrapper rsAcct = new clsDRWrapper();

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtPage.Text = "Page 1";
		}

		public void Init(ref clsSQLStatement clsStatement)
		{
			string strSQL = "";
			string strMasterJoin;
			strMasterJoin = modCE.GetCEMasterJoin();
			if (!(clsStatement == null))
			{
				// strSQL = clsStatement.SQLStatement
				strSQL = "select distinct ceaccount,property1,property2  from (" + clsStatement.SelectStatement + " " + clsStatement.WhereStatement + " " + clsStatement.OrderByStatement + ") as tbl1 inner join abutters on (abutters.property1 = tbl1.ceaccount) or (abutters.property2 = tbl1.ceaccount)";
			}
			else
			{
				strSQL = "Select ceaccount from cemaster inner join abutters on (abutters.property1 = cemaster.ceaccount) or (abutters.property2 = cemaster.ceaccount) where not isnull(deleted,0) = 1 order by ceaccount";
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			rsAcct.OpenRecordset(strMasterJoin + " order by ceaccount", modGlobalVariables.Statics.strCEDatabase);
			Fields.Add("ceaccount");
			this.Fields["ceaccount"].Value = "0";
			txtRange.Text = clsStatement.ParameterDescriptionText;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Properties");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!rsReport.EndOfFile())
			{
				eArgs.EOF = false;
				this.Fields["ceaccount"].Value = rsReport.Get_Fields_String("ceaccount");
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				string strTemp = "";
				string strnewline = "";
				int lngAbuttAccount = 0;
				if (rsReport.Get_Fields_Int32("ceaccount") != rsReport.Get_Fields_Int32("property1"))
				{
					lngAbuttAccount = rsReport.Get_Fields_Int32("property1");
				}
				else
				{
					lngAbuttAccount = rsReport.Get_Fields_Int32("property2");
				}
				if (rsAcct.FindFirstRecord("ceaccount", lngAbuttAccount))
				{
					txtAbutterAccount.Text = lngAbuttAccount.ToString();
					txtAbutterMaplot.Text = rsAcct.Get_Fields_String("maplot");
					strTemp = fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("apt") + " " + rsAcct.Get_Fields_String("streetname"));
					if (Conversion.Val(rsAcct.Get_Fields_String("streetnumber")) > 0)
					{
						strTemp = rsAcct.Get_Fields_String("streetnumber") + " " + strTemp;
					}
					txtAbutterLocation.Text = strTemp;
					strnewline = "";
					strTemp = fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("last"));
					if (strTemp != string.Empty)
					{
						if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("first")) != string.Empty || fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("middle")) != string.Empty)
						{
							strTemp = fecherFoundation.Strings.Trim(strTemp + ", " + rsAcct.Get_Fields_String("first") + " " + rsAcct.Get_Fields_String("middle") + " " + rsAcct.Get_Fields_String("desig"));
						}
					}
					else
					{
						strTemp = fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("first") + " " + rsAcct.Get_Fields_String("middle") + " " + rsAcct.Get_Fields_String("desig"));
					}
					strnewline = "\r\n";
					if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address1")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address1"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address2")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address2"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("city")) != string.Empty || rsAcct.Get_Fields_String("state") != string.Empty || rsAcct.Get_Fields_String("zip") != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("city")) + "  " + rsAcct.Get_Fields_String("state") + " " + rsAcct.Get_Fields_String("zip") + " " + rsAcct.Get_Fields_String("zip4"));
						strnewline = "\r\n";
					}
					txtAbutterName.Text = strTemp;
				}
				else
				{
					txtAbutterAccount.Text = "";
					txtAbutterMaplot.Text = "";
					txtAbutterName.Text = "";
					txtAbutterLocation.Text = "";
				}
				rsReport.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (Conversion.Val(this.Fields["ceaccount"].Value) > 0)
			{
				if (rsAcct.FindFirstRecord("ceaccount", Conversion.Val(this.Fields["ceaccount"].Value)))
				{
					txtAccount.Text = this.Fields["ceaccount"].Value.ToString();
					txtMapLot.Text = rsAcct.Get_Fields_String("maplot");
					string strTemp = "";
					strTemp = fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("apt") + " " + rsAcct.Get_Fields_String("streetname"));
					if (Conversion.Val(rsAcct.Get_Fields_String("streetnumber")) > 0)
					{
						strTemp = rsAcct.Get_Fields_String("streetnumber") + " " + strTemp;
					}
					txtLocation.Text = strTemp;
					string strnewline = "";
					strnewline = "";
					strTemp = fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("last"));
					if (strTemp != string.Empty)
					{
						if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("first")) != string.Empty || fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("middle")) != string.Empty)
						{
							strTemp = fecherFoundation.Strings.Trim(strTemp + ", " + rsAcct.Get_Fields_String("first") + " " + rsAcct.Get_Fields_String("middle") + " " + rsAcct.Get_Fields_String("desig"));
						}
					}
					else
					{
						strTemp = fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("first") + " " + rsAcct.Get_Fields_String("middle") + " " + rsAcct.Get_Fields_String("desig"));
					}
					strnewline = "\r\n";
					if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address1")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address1"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address2")) != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("address2"));
						strnewline = "\r\n";
					}
					if (fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("city")) != string.Empty || rsAcct.Get_Fields_String("state") != string.Empty || rsAcct.Get_Fields_String("zip") != string.Empty)
					{
						strTemp += strnewline + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsAcct.Get_Fields_String("city")) + "  " + rsAcct.Get_Fields_String("state") + " " + rsAcct.Get_Fields_String("zip") + " " + rsAcct.Get_Fields_String("zip4"));
						strnewline = "\r\n";
					}
					txtName.Text = strTemp;
				}
			}
		}

		
	}
}
