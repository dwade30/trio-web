//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for srptAccountPermitActivity.
	/// </summary>
	public partial class srptAccountPermitActivity : FCSectionReport
	{
		public srptAccountPermitActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptAccountPermitActivity InstancePtr
		{
			get
			{
				return (srptAccountPermitActivity)Sys.GetInstance(typeof(srptAccountPermitActivity));
			}
		}

		protected srptAccountPermitActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAccountPermitActivity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		int lngAccount;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}
		// Private Sub ActiveReport_ReportEnd()
		// Unload SubReport1.object
		// Set SubReport1 = Nothing
		// End Sub
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string[] strAry = null;
			strAry = Strings.Split(FCConvert.ToString(this.UserData), "|", -1, CompareConstants.vbBinaryCompare);
			lngAccount = 0;
			if (Information.UBound(strAry, 1) >= 0)
			{
				txtAccount.Text = strAry[0];
				lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
				if (Information.UBound(strAry, 1) > 0)
				{
					txtMapLot.Text = strAry[1];
				}
			}
			rsReport.OpenRecordset("select * from permits where account = " + FCConvert.ToString(lngAccount) + " and not deleted = 1", modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.RecordCount() > 0)
			{
				SubReport1.Report = new srptPermitActivity();
				SubReport1.Report.UserData = 0;
			}
			else
			{
				SubReport1.Report = null;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				int lngPermitID = 0;
				lngPermitID = FCConvert.ToInt32(rsReport.Get_Fields_Int32("ID"));
				SubReport1.Report.UserData = lngPermitID;
				rsReport.MoveNext();
			}
		}


	}
}
