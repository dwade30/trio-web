//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsPhoneList
	{
		//=========================================================
		private FCCollection PhoneList = new FCCollection();
		private int intPhoneType;
		private int lngParentID;
		private int intCurrentIndex;

		public clsPhoneList Copy()
		{
			clsPhoneList Copy = null;
			clsPhoneList tpL = new clsPhoneList();
			clsPhoneNumber tPN;
			clsPhoneNumber nPN;
			int intIndex;
			if (!FCUtils.IsEmpty(PhoneList))
			{
				for (intIndex = 1; intIndex <= PhoneList.Count; intIndex++)
				{
					tPN = Get_GetPhoneNumberByIndex(intIndex);
					if (!(tPN == null))
					{
						nPN = new clsPhoneNumber();
						nPN.ID = tPN.ID;
						nPN.Deleted = tPN.Deleted;
						nPN.Description = tPN.Description;
						nPN.PhoneNumber = tPN.PhoneNumber;
						tpL.InsertPhoneNumber(ref nPN);
					}
				}
				// intIndex
			}
			Copy = tpL;
			return Copy;
		}

		public int PhoneCode
		{
			set
			{
				intPhoneType = value;
			}
			get
			{
				int PhoneCode = 0;
				PhoneCode = intPhoneType;
				return PhoneCode;
			}
		}

		public int ParentID
		{
			set
			{
				lngParentID = value;
			}
			get
			{
				int ParentID = 0;
				ParentID = lngParentID;
				return ParentID;
			}
		}

		public void AddPhoneNumber(string strPhoneNumber, string strDescription, int lngAutoID)
		{
			clsPhoneNumber tPN = new clsPhoneNumber();
			tPN.PhoneNumber = strPhoneNumber;
			tPN.Deleted = false;
			tPN.Description = strDescription;
			tPN.ID = lngAutoID;
			InsertPhoneNumber(ref tPN);
		}

		public int InsertPhoneNumber(ref clsPhoneNumber tPN)
		{
			int InsertPhoneNumber = 0;
			if (!(tPN == null))
			{
				PhoneList.Add(tPN);
				InsertPhoneNumber = PhoneList.Count;
			}
			return InsertPhoneNumber;
		}

		public void LoadNumbers()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				Clear();
				rsLoad.OpenRecordset("select * from phonenumbers where phonecode =" + FCConvert.ToString(intPhoneType) + " and parentid = " + FCConvert.ToString(lngParentID) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					AddPhoneNumber(rsLoad.Get_Fields_String("phonenumber"), rsLoad.Get_Fields_String("PhoneDescription"), rsLoad.Get_Fields_Int32("ID"));
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadNumbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveNumbers()
		{
			bool SaveNumbers = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				clsPhoneNumber tPN;
				int intIndex;
				if (!FCUtils.IsEmpty(PhoneList))
				{
					for (intIndex = 1; intIndex <= PhoneList.Count; intIndex++)
					{
						tPN = Get_GetPhoneNumberByIndex(intIndex);
						if (!(tPN == null))
						{
							if (!tPN.Deleted)
							{
								rsSave.OpenRecordset("select * from phonenumbers where ID = " + FCConvert.ToString(tPN.ID), modGlobalVariables.Statics.strCEDatabase);
								if (!rsSave.EndOfFile())
								{
									rsSave.Edit();
								}
								else
								{
									rsSave.AddNew();
								}
								rsSave.Set_Fields("phonenumber", tPN.PhoneNumber);
								rsSave.Set_Fields("PhoneDescription", tPN.Description);
								rsSave.Set_Fields("PhoneCode", intPhoneType);
								rsSave.Set_Fields("ParentID", lngParentID);
								rsSave.Update();
								tPN.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
							}
							else
							{
								if (tPN.ID > 0)
								{
									// delete it from the table
									rsSave.Execute("delete from phonenumbers where ID = " + FCConvert.ToString(tPN.ID), modGlobalVariables.Statics.strCEDatabase);
								}
							}
						}
					}
					// intIndex
					ClearDeleted();
					MoveFirst();
				}
				return SaveNumbers;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveNumbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveNumbers;
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public int GetCount
		{
			get
			{
				int GetCount = 0;
				int intReturn;
				intReturn = 0;
				if (!FCUtils.IsEmpty(PhoneList))
				{
					intReturn = PhoneList.Count;
				}
				GetCount = intReturn;
				return GetCount;
			}
		}

		public clsPhoneNumber GetCurrentPhoneNumber
		{
			get
			{
				clsPhoneNumber GetCurrentPhoneNumber = null;
				clsPhoneNumber tPN;
				tPN = null;
				if (!FCUtils.IsEmpty(PhoneList))
				{
					if (intCurrentIndex > 0)
					{
						if (!(PhoneList[intCurrentIndex] == null))
						{
							tPN = PhoneList[intCurrentIndex];
						}
					}
				}
				GetCurrentPhoneNumber = tPN;
				return GetCurrentPhoneNumber;
			}
		}

		public clsPhoneNumber Get_GetPhoneNumberByIndex(int intValue)
		{
			clsPhoneNumber GetPhoneNumberByIndex = null;
			clsPhoneNumber tPN;
			tPN = null;
			if (intValue >= 0)
			{
				if (!FCUtils.IsEmpty(PhoneList))
				{
					if (intValue <= PhoneList.Count)
					{
						tPN = PhoneList[intValue];
					}
				}
			}
			GetPhoneNumberByIndex = tPN;
			return GetPhoneNumberByIndex;
		}

		public void Clear()
		{
			int x;
			if (!FCUtils.IsEmpty(PhoneList))
			{
				for (x = 1; x <= PhoneList.Count; x++)
				{
					PhoneList.Remove(1);
				}
				// x
			}
			intCurrentIndex = -1;
		}

		public void ClearDeleted()
		{
			int x;
			if (!FCUtils.IsEmpty(PhoneList))
			{
				for (x = PhoneList.Count; x >= 1; x--)
				{
					if (!(PhoneList[x] == null))
					{
						if (PhoneList[x].Deleted)
						{
							PhoneList.Remove(x);
						}
					}
				}
				// x
			}
		}

		public void MoveFirst()
		{
			if (!(PhoneList == null))
			{
				if (!FCUtils.IsEmpty(PhoneList))
				{
					if (PhoneList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(PhoneList))
			{
				if (intCurrentIndex > PhoneList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= PhoneList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > PhoneList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (PhoneList[intCurrentIndex] == null)
						{
						}
						else if (PhoneList[intCurrentIndex].Deleted)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public int MovePrevious()
		{
			int MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(PhoneList))
			{
				if (PhoneList.Count > 0)
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex > 0)
						{
							if (!(PhoneList[intCurrentIndex] == null))
							{
								intReturn = intCurrentIndex;
							}
						}
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MovePrevious = intReturn;
			return MovePrevious;
		}
	}
}
