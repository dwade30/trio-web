﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptPermits.
	/// </summary>
	partial class rptPermits
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPermits));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFiler = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiler)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtID,
            this.txtFiler,
            this.txtDateFiled,
            this.txtType,
            this.txtStatus,
            this.txtLocation});
			this.Detail.Height = 0.3958333F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtID
			// 
			this.txtID.Height = 0.1770833F;
			this.txtID.Left = 0.02083333F;
			this.txtID.Name = "txtID";
			this.txtID.Text = null;
			this.txtID.Top = 0.03125F;
			this.txtID.Width = 0.96875F;
			// 
			// txtFiler
			// 
			this.txtFiler.Height = 0.1770833F;
			this.txtFiler.Left = 1.010417F;
			this.txtFiler.Name = "txtFiler";
			this.txtFiler.Text = null;
			this.txtFiler.Top = 0.03125F;
			this.txtFiler.Width = 2.395833F;
			// 
			// txtDateFiled
			// 
			this.txtDateFiled.Height = 0.1770833F;
			this.txtDateFiled.Left = 3.427083F;
			this.txtDateFiled.Name = "txtDateFiled";
			this.txtDateFiled.Text = null;
			this.txtDateFiled.Top = 0.03125F;
			this.txtDateFiled.Width = 0.8125F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1770833F;
			this.txtType.Left = 4.239583F;
			this.txtType.Name = "txtType";
			this.txtType.Text = null;
			this.txtType.Top = 0.03125F;
			this.txtType.Width = 1.729167F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1770833F;
			this.txtStatus.Left = 5.989583F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Text = null;
			this.txtStatus.Top = 0.03125F;
			this.txtStatus.Width = 1.416667F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1770833F;
			this.txtLocation.Left = 1.010417F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.2083333F;
			this.txtLocation.Width = 4.958333F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Label5,
            this.Label6,
            this.txtRange});
			this.PageHeader.Height = 0.687F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.15625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Permits";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1770833F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.010417F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Filer";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 2.395833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1770833F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.427083F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Date Filed";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.187F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.239583F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Type";
			this.Label4.Top = 0.5F;
			this.Label4.Width = 1.729F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1770833F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 5.989583F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Status";
			this.Label5.Top = 0.5F;
			this.Label5.Width = 1.416667F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1770833F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.02083333F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: left";
			this.Label6.Text = "Permit";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 0.96875F;
			// 
			// txtRange
			// 
			this.txtRange.Height = 0.2083333F;
			this.txtRange.Left = 1.25F;
			this.txtRange.Name = "txtRange";
			this.txtRange.Text = null;
			this.txtRange.Top = 0.25F;
			this.txtRange.Width = 4.9375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptPermits
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.447917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFiler)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFiler;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
