//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmUndelete : BaseForm
	{
		public frmUndelete()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUndelete InstancePtr
		{
			get
			{
				return (frmUndelete)Sys.GetInstance(typeof(frmUndelete));
			}
		}

		protected frmUndelete _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 12/27/2005
		// ********************************************************
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLCODETYPE = 1;
		const int CNSTGRIDCOLUNDELETE = 2;
		const int CNSTGRIDCOLPERMITNUM = 3;
		const int CNSTGRIDCOLACCOUNT = 4;
		const int CNSTGRIDCOLMAPLOT = 5;
		const int CNSTGRIDCOLLICENSE = 6;
		const int CNSTGRIDCOLNAME = 7;
		const int CNSTGRIDCOLDATE = 8;
		const int CNSTGRIDCOLPLAN = 9;
		const int CNSTGRIDCOLFILER = 10;
		const int CNSTGRIDCOLINSPECTIONTYPE = 11;
		const int CNSTGRIDCOLINSPECTOR = 12;

		private void frmUndelete_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmUndelete_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUndelete properties;
			//frmUndelete.FillStyle	= 0;
			//frmUndelete.ScaleWidth	= 9300;
			//frmUndelete.ScaleHeight	= 7935;
			//frmUndelete.LinkTopic	= "Form2";
			//frmUndelete.LockControls	= -1  'True;
			//frmUndelete.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			FillGrid();
		}

		private void frmUndelete_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuAccounts_Click(object sender, System.EventArgs e)
		{
			Grid.Tag = (System.Object)(modCEConstants.CNSTCODETYPEPROPERTY);
			ShowGrid();
		}

		private void mnuContractors_Click(object sender, System.EventArgs e)
		{
			Grid.Tag = (System.Object)(modCEConstants.CNSTCODETYPECONTRACTOR);
			ShowGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuInspections_Click(object sender, System.EventArgs e)
		{
			Grid.Tag = (System.Object)(modCEConstants.CNSTCODETYPEINSPECTION);
			ShowGrid();
		}

		private void mnuPermits_Click(object sender, System.EventArgs e)
		{
			Grid.Tag = (System.Object)(modCEConstants.CNSTCODETYPEPERMIT);
			ShowGrid();
		}

		private void ShowGrid()
		{
			int lngRow;
			if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPEPROPERTY)
			{
				framRecords.Text = "Accounts";
				Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Account");
				Grid.ColHidden(CNSTGRIDCOLACCOUNT, false);
				Grid.ColHidden(CNSTGRIDCOLDATE, true);
				Grid.ColHidden(CNSTGRIDCOLFILER, true);
				Grid.ColHidden(CNSTGRIDCOLINSPECTIONTYPE, true);
				Grid.ColHidden(CNSTGRIDCOLINSPECTOR, true);
				Grid.ColHidden(CNSTGRIDCOLLICENSE, true);
				Grid.ColHidden(CNSTGRIDCOLMAPLOT, false);
				Grid.ColHidden(CNSTGRIDCOLNAME, false);
				Grid.ColHidden(CNSTGRIDCOLPERMITNUM, true);
				Grid.ColHidden(CNSTGRIDCOLPLAN, true);
			}
			else if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPECONTRACTOR)
			{
				framRecords.Text = "Contractors";
				Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Number");
				Grid.ColHidden(CNSTGRIDCOLACCOUNT, false);
				Grid.ColHidden(CNSTGRIDCOLDATE, true);
				Grid.ColHidden(CNSTGRIDCOLFILER, true);
				Grid.ColHidden(CNSTGRIDCOLINSPECTIONTYPE, true);
				Grid.ColHidden(CNSTGRIDCOLINSPECTOR, true);
				Grid.ColHidden(CNSTGRIDCOLLICENSE, false);
				Grid.ColHidden(CNSTGRIDCOLMAPLOT, true);
				Grid.ColHidden(CNSTGRIDCOLNAME, false);
				Grid.ColHidden(CNSTGRIDCOLPERMITNUM, true);
				Grid.ColHidden(CNSTGRIDCOLPLAN, true);
			}
			else if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPEINSPECTION)
			{
				framRecords.Text = "Inspections";
				Grid.ColHidden(CNSTGRIDCOLACCOUNT, true);
				Grid.ColHidden(CNSTGRIDCOLDATE, false);
				Grid.ColHidden(CNSTGRIDCOLFILER, true);
				Grid.ColHidden(CNSTGRIDCOLINSPECTIONTYPE, false);
				Grid.ColHidden(CNSTGRIDCOLINSPECTOR, false);
				Grid.ColHidden(CNSTGRIDCOLLICENSE, true);
				Grid.ColHidden(CNSTGRIDCOLMAPLOT, true);
				Grid.ColHidden(CNSTGRIDCOLNAME, true);
				Grid.ColHidden(CNSTGRIDCOLPERMITNUM, false);
				Grid.ColHidden(CNSTGRIDCOLPLAN, true);
			}
			else if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPEPERMIT)
			{
				framRecords.Text = "Permits";
				Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Account");
				Grid.ColHidden(CNSTGRIDCOLACCOUNT, false);
				Grid.ColHidden(CNSTGRIDCOLDATE, true);
				Grid.ColHidden(CNSTGRIDCOLFILER, false);
				Grid.ColHidden(CNSTGRIDCOLINSPECTIONTYPE, true);
				Grid.ColHidden(CNSTGRIDCOLINSPECTOR, true);
				Grid.ColHidden(CNSTGRIDCOLLICENSE, true);
				Grid.ColHidden(CNSTGRIDCOLMAPLOT, true);
				Grid.ColHidden(CNSTGRIDCOLNAME, true);
				Grid.ColHidden(CNSTGRIDCOLPERMITNUM, false);
				Grid.ColHidden(CNSTGRIDCOLPLAN, false);
			}
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == Conversion.Val(Grid.Tag))
				{
					Grid.RowHidden(lngRow, false);
				}
				else
				{
					Grid.RowHidden(lngRow, true);
				}
			}
			// lngRow
			ResizeGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLUNDELETE, FCConvert.ToInt32(0.12 * GridWidth));
			if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPEPROPERTY)
			{
				Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(0.2 * GridWidth));
				Grid.ColWidth(CNSTGRIDCOLMAPLOT, FCConvert.ToInt32(0.2 * GridWidth));
			}
			else if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPEPERMIT)
			{
				Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(0.15 * GridWidth));
				Grid.ColWidth(CNSTGRIDCOLPERMITNUM, FCConvert.ToInt32(0.2 * GridWidth));
				Grid.ColWidth(CNSTGRIDCOLPLAN, FCConvert.ToInt32(0.2 * GridWidth));
			}
			else if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPEINSPECTION)
			{
				Grid.ColWidth(CNSTGRIDCOLDATE, FCConvert.ToInt32(0.13 * GridWidth));
                //FC:FINAL:BSE #1916 increase column width to fit text 
				Grid.ColWidth(CNSTGRIDCOLPERMITNUM, FCConvert.ToInt32(0.2 * GridWidth));
				Grid.ColWidth(CNSTGRIDCOLINSPECTIONTYPE, FCConvert.ToInt32(0.25 * GridWidth));
			}
			else if (Conversion.Val(Grid.Tag) == modCEConstants.CNSTCODETYPECONTRACTOR)
			{
				Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(0.2 * GridWidth));
				Grid.ColWidth(CNSTGRIDCOLLICENSE, FCConvert.ToInt32(0.2 * GridWidth));
			}
		}

		private void SetupGrid()
		{
			Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Account");
			Grid.TextMatrix(0, CNSTGRIDCOLDATE, "Date");
			Grid.TextMatrix(0, CNSTGRIDCOLFILER, "Filed By");
			Grid.TextMatrix(0, CNSTGRIDCOLINSPECTIONTYPE, "Inspection Type");
			Grid.TextMatrix(0, CNSTGRIDCOLINSPECTOR, "Inspector");
			Grid.TextMatrix(0, CNSTGRIDCOLLICENSE, "License");
			Grid.TextMatrix(0, CNSTGRIDCOLMAPLOT, "Map Lot");
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLPERMITNUM, "Permit Number");
			Grid.TextMatrix(0, CNSTGRIDCOLPLAN, "Plan");
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			Grid.ColHidden(CNSTGRIDCOLCODETYPE, true);
			Grid.ColDataType(CNSTGRIDCOLUNDELETE, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.TextMatrix(0, CNSTGRIDCOLUNDELETE, "Undelete");
		}

		private void FillGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Grid.Rows = 1;
				strSQL = "select * from cemaster where deleted = 1 ";
				strSQL += " order by ceaccount";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(clsLoad.Get_Fields_Int32("ceaccount")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(clsLoad.Get_Fields_String("maplot")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("name")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUNDELETE, FCConvert.ToString(false));
					clsLoad.MoveNext();
				}
				// don't allow to undelete any permits that belong to a deleted account
				strSQL = "select * from permits inner join cemaster on (permits.account = cemaster.ceaccount) where permits.deleted  = 1 and not cemaster.deleted = 1 ";
				strSQL += " order by permityear desc,permitnumber";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields("permits.ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("account"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPLAN, FCConvert.ToString(clsLoad.Get_Fields_String("plan")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLFILER, FCConvert.ToString(clsLoad.Get_Fields_String("filedby")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPERMITNUM, Strings.Right(Strings.Format(Conversion.Val(clsLoad.Get_Fields_Int32("permityear")), "0000"), 2) + "-" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Permitnumber"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUNDELETE, FCConvert.ToString(false));
					clsLoad.MoveNext();
				}
				strSQL = "select * from inspections inner join permits on (permits.ID = inspections.permitid) where inspections.deleted = 1 and not permits.deleted = 1 order by inspectiondate ";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields("inspections.ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDATE, FCConvert.ToString(clsLoad.Get_Fields_DateTime("inspectiondate")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLINSPECTOR, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectorid"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLINSPECTIONTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectiontype"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPERMITNUM, Strings.Right(Strings.Format(Conversion.Val(clsLoad.Get_Fields_Int32("permityear")), "0000"), 2) + "-" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Permitnumber"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUNDELETE, FCConvert.ToString(false));
					clsLoad.MoveNext();
				}
				strSQL = "select * from contractors where deleted = 1";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("contractornumber"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("name1")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLICENSE, FCConvert.ToString(clsLoad.Get_Fields_String("license")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLUNDELETE, FCConvert.ToString(false));
					clsLoad.MoveNext();
				}
				Grid.Tag = (System.Object)(modCEConstants.CNSTCODETYPEPROPERTY);
				ShowGrid();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuUndelete_Click(object sender, System.EventArgs e)
		{
			// undelete the records and then remove them from the grid
			int lngRow;
			bool boolSuccessful;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modGlobalFunctions.AddCYAEntry_6("CE", "Undeleted Records from Undelete screen");
				for (lngRow = Grid.Rows - 1; lngRow >= 1; lngRow--)
				{
					if (FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLUNDELETE)))
					{
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == modCEConstants.CNSTCODETYPEPROPERTY)
						{
							if (modMain.UndeleteMaster(FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT)))))
							{
								Grid.RemoveItem(lngRow);
							}
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
						{
							if (modMain.UndeleteContractor(FCConvert.ToInt32(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)))))
							{
								Grid.RemoveItem(lngRow);
							}
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == modCEConstants.CNSTCODETYPEINSPECTION)
						{
							if (modMain.UndeleteInspection(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))))
							{
								Grid.RemoveItem(lngRow);
							}
						}
						else if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == modCEConstants.CNSTCODETYPEPERMIT)
						{
							if (modMain.UndeletePermit(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))))
							{
								Grid.RemoveItem(lngRow);
							}
						}
					}
				}
				// lngRow
				MessageBox.Show("Record(s) Undeleted", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In mnuUndelete", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
