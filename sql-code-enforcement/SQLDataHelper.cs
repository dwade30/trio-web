//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Diagnostics;
using Wisej.Web.Ext.FullCalendar;

namespace TWCE0000
{
	public class SQLDataHelper
	{
		//=========================================================
		//FC:TODO
		fecherFoundation.FCCalendarControl m_pCalendar = new fecherFoundation.FCCalendarControl();
		// ======================================================================
		// vbPorter upgrade warning: pCalendar As CalendarControl	OnWrite(XtremeCalendarControl.CalendarControl)
		public void SetCalendar(fecherFoundation.FCCalendarControl pCalendar)
		{
			m_pCalendar = pCalendar;
		}
        // vbPorter upgrade warning: bRException As object	OnWrite(bool)
        //public CalendarEvent CreateEventFromRS(ref clsDRWrapper pEventRS, object bRException)
        public Event CreateEventFromRS(ref clsDRWrapper pEventRS, object bRException)
        {
            try
            {   // On Error GoTo err1
                fecherFoundation.Information.Err().Clear();
                clsDRWrapper rsRecurrence = new clsDRWrapper();
                //		XtremeCalendarControl.CalendarRecurrencePattern mRecurr = new XtremeCalendarControl.CalendarRecurrencePattern();
                //		CalendarEvent pEvent = new CalendarEvent();
                Event pEvent = new Event();
                int nEventID;
                //		if (pEventRS.EndOfFile()) {
                //			return CreateEventFromRS;
                //		}
                nEventID = pEventRS.Get_Fields_Int32("ID");
                pEvent.Id = nEventID.ToString();
                //		pEvent = m_pCalendar.DataProvider.CreateEventEx(nEventID);
                //		if (pEvent==null) {
                //			return CreateEventFromRS;
                //		}
                //		pEvent.Subject = pEventRS.Get_Fields("Subject");
                pEvent.Title = pEventRS.Get_Fields_String("Subject");
                pEvent.UserData.location = pEventRS.Get_Fields_String("Location");
                pEvent.UserData.body = pEventRS.Get_Fields_String("Body");
                var meetingFlag = pEventRS.Get_Fields_Boolean("IsMeeting");
                pEvent.UserData.MeetingFlag = (meetingFlag != null ? meetingFlag : false);
                var privateFlag = pEventRS.Get_Fields_Boolean("IsPrivate");
                pEvent.UserData.PrivateFlag = (privateFlag != null ? privateFlag : false);
                pEvent.UserData.Label = pEventRS.Get_Fields_Int32("LabelID");
                pEvent.UserData.BusyStatus = pEventRS.Get_Fields_Int32("BusyStatus");
                //		pEvent.Importance = pEventRS.Get_Fields("ImportanceLevel");
                //		pEvent.StartTime = pEventRS.Get_Fields("StartDateTime");
                if (Information.IsDate(pEventRS.Get_Fields("StartDateTime")))
                {
                    pEvent.Start = pEventRS.Get_Fields_DateTime("StartDateTime");
                }
                //		pEvent.EndTime = pEventRS.Get_Fields("EndDateTime");
                if (Information.IsDate(pEventRS.Get_Fields("EndDateTime")))
                {
                    pEvent.End = pEventRS.Get_Fields_DateTime("EndDateTime");
                }
                //		pEvent.AllDayEvent = (pEventRS.Get_Fields("IsAllDayEvent")!=0 ? true : false);
                var allDayEvent = pEventRS.Get_Fields_Boolean("IsAllDayEvent");
                pEvent.AllDay = allDayEvent != null ? allDayEvent : false;
                var reminder = pEventRS.Get_Fields_Boolean("TaskReminder");
                pEvent.UserData.Reminder = reminder != null ? reminder : false;
                var reminderMinutesBeforeStart = pEventRS.Get_Fields_Int32("ReminderMinutesBeforeStart");
                if (reminderMinutesBeforeStart != null) {
                    pEvent.UserData.ReminderMinutesBeforeStart = reminderMinutesBeforeStart;
                }
                CalendarEventCustomProperties customProperties = new CalendarEventCustomProperties();
                if (pEventRS.Get_Fields_String("CustomPropertiesXMLData") != null)
                {
                    customProperties.LoadFromString(pEventRS.Get_Fields_String("CustomPropertiesXMLData"));
                }
                pEvent.UserData.CustomProperties = customProperties;
                pEvent.UserData.ScheduleID = pEventRS.Get_Fields_Int32("ScheduleID");
                //		// If pEventRS.Fields("RecurrencePatternID") <> 0 Then
                //		// rsRecurrence.OpenRecordset "SELECT * FROM RecurrencePattern WHERE RecurrencePatternID = " & pEventRS.Fields("RecurrencePatternID"), strgndatabase
                //		// If rsRecurrence.EndOfFile <> True And rsRecurrence.BeginningOfFile <> True Then
                //		// Set mRecurr = CreateRPatternFromRS(rsRecurrence)
                //		// pEvent.RecurrencePatt
                //		// pEvent.CreateRecurrence mRecurr
                //		// End If
                //		// End If
                //		if (FCConvert.ToBoolean(bRException)) {
                //			pEvent.MakeAsRException();
                //			pEvent.RExceptionStartTimeOrig = pEventRS.Get_Fields("RExceptionStartTimeOrig");
                //			pEvent.RExceptionEndTimeOrig = pEventRS.Get_Fields("RExceptionEndTimeOrig");
                //			pEvent.RExceptionDeleted = (pEventRS.Get_Fields("ISRecurrenceExceptionDeleted")!=0 ? true : false);
                //		}
                //		// If Not bRException Then
                //		//
                //		// "process_RecurrenceState" and "process_RecurrencePatternID" properties
                //		// are used to process master events.
                //		//
                //		// If they are set and RecurrenceStaie is Master Data provider will
                //		// fier DoReadRPattern event and make event as Master.
                //		// And it will also generate ocurrences for RetrieveDayEvents method.
                //		//
                //		// Thise properties are temporary and they will be removed by data provider.
                //		//
                //		// If these properties are not set data provider expect that master event
                //		// is already compleated - CreateRecurrence method is called and
                //		// Recurrence pattern is set.
                //		//
                //		// This mechanism is usefull for DB data providers, when events and patterns
                //		// are stored separately (in tables).
                //		// But if events stored in some memory collection or array
                //		// it should not be used because master event store recurrence pattern inside.
                //		//
                //		pEvent.CustomProperties("process_RecurrenceState") = pEventRS.Get_Fields("RecurrenceState");
                //		pEvent.CustomProperties("process_RecurrencePatternID") = pEventRS.Get_Fields("RecurrencePatternID");
                //		// End If
                return pEvent;
            }
            catch (Exception ex)
            {   // err1:
                if (fecherFoundation.Information.Err(ex).Number != 0)
                {
                    Debug.WriteLine("Cannot read event from recordset: " + fecherFoundation.Information.Err(ex).Description);
                    Debug.Assert(false);
                }
                return null;
            }
        }
		// vbPorter upgrade warning: pEvent As CalendarEvent	OnWrite(XtremeCalendarControl.CalendarEvent)
		public void PutEventToRS(ref Event pEvent, clsDRWrapper pEventRS, ref string strUser)
		{
			try
			{   // On Error GoTo err1
				fecherFoundation.Information.Err().Clear();
				pEventRS.Set_Fields("TaskPerson", strUser);
				pEventRS.Set_Fields("Subject", pEvent.Title);
				pEventRS.Set_Fields("Location", pEvent.UserData.location);
				pEventRS.Set_Fields("Body", pEvent.UserData.body);
				pEventRS.Set_Fields("IsMeeting", (pEvent.UserData.MeetingFlag ? 1 : 0));
				pEventRS.Set_Fields("IsPrivate", (pEvent.UserData.PrivateFlag ? 1 : 0));
				pEventRS.Set_Fields("LabelID", pEvent.UserData.Label);
				pEventRS.Set_Fields("BusyStatus", pEvent.UserData.BusyStatus);
				pEventRS.Set_Fields("ImportanceLevel", pEvent.UserData.Importance);
				pEventRS.Set_Fields("StartDateTime", pEvent.Start);
				pEventRS.Set_Fields("EndDateTime", pEvent.End);
				pEventRS.Set_Fields("IsAllDayEvent", (pEvent.AllDay ? 1 : 0));
				pEventRS.Set_Fields("TaskReminder", (pEvent.UserData.Reminder ? 1 : 0));
				pEventRS.Set_Fields("ReminderMinutesBeforeStart", pEvent.UserData.ReminderMinutesBeforeStart);
				pEventRS.Set_Fields("RecurrenceState", pEvent.UserData.RecurrenceState);
				//if (pEvent.RecurrenceState == xtpCalendarRecurrenceMaster || pEvent.RecurrenceState == xtpCalendarRecurrenceException)
				//{
				//	pEventRS.Set_Fields("RecurrencePatternID", pEvent.RecurrencePattern.ID);
				//}
				//else
				//{
				//	pEventRS.Set_Fields("RecurrencePatternID", 0);
				//}
				//pEventRS.Set_Fields("RExceptionStartTimeOrig", pEvent.RExceptionStartTimeOrig);
				//pEventRS.Set_Fields("RExceptionEndTimeOrig", pEvent.RExceptionEndTimeOrig);
				//pEventRS.Set_Fields("ISRecurrenceExceptionDeleted", (pEvent.RExceptionDeleted ? 1 : 0));
				CalendarEventCustomProperties customProperties = pEvent.UserData.CustomProperties as CalendarEventCustomProperties;
				if (customProperties != null)
				{
					pEventRS.Set_Fields("CustomPropertiesXMLData", customProperties.SaveToString());
				}
				//
				pEventRS.Set_Fields("ScheduleID", pEvent.UserData.ScheduleID);
			}
			catch (Exception ex)
			{   // err1:
				if (fecherFoundation.Information.Err(ex).Number != 0)
				{
					Debug.WriteLine("Cannot put event to recordset: " + fecherFoundation.Information.Err(ex).Description);
					Debug.Assert(false);
				}
			}
		}
		//public CalendarRecurrencePattern CreateRPatternFromRS(ref clsDRWrapper pRPatternRS)
		//{
		//	try
		//	{	// On Error GoTo err1
		//		fecherFoundation.Information.Err().Clear();
		//		CreateRPatternFromRS = null;
		//		if (pRPatternRS.EndOfFile()) return CreateRPatternFromRS;
		//		CalendarRecurrencePattern pRPattern = new CalendarRecurrencePattern();
		//		int nPatternID;
		//		nPatternID = pRPatternRS.Get_Fields("RecurrencePatternID");
		//		pRPattern = m_pCalendar.DataProvider.CreateRecurrencePattern(nPatternID);
		//		if (pRPattern==null) {
		//			return CreateRPatternFromRS;
		//		}
		//		pRPattern.MasterEventId = pRPatternRS.Get_Fields("MasterEventID");
		//		pRPattern.StartTime = pRPatternRS.Get_Fields("EventStartTime");
		//		pRPattern.DurationMinutes = pRPatternRS.Get_Fields("EventDuration");
		//		pRPattern.StartDate = pRPatternRS.Get_Fields("PatternStartDate");
		//		pRPattern.EndMethod = pRPatternRS.Get_Fields("PatternEndMethod");
		//		if (pRPattern.EndMethod==xtpCalendarPatternEndDate) {
		//			pRPattern.EndDate = pRPatternRS.Get_Fields("PatternEndDate");
		//		} else if (pRPattern.EndMethod==xtpCalendarPatternEndAfterOccurrences) {
		//			pRPattern.EndAfterOccurrences = pRPatternRS.Get_Fields("PatternEndAfterOccurrences");
		//		} else {
		//			Debug.Assert(pRPattern.EndMethod==xtpCalendarPatternEndNoDate);
		//		}
		//		pRPattern.Options.data1 = pRPatternRS.Get_Fields("OptionsData1");
		//		pRPattern.Options.Data2 = pRPatternRS.Get_Fields("OptionsData2");
		//		pRPattern.Options.Data3 = pRPatternRS.Get_Fields("OptionsData3");
		//		pRPattern.Options.Data4 = pRPatternRS.Get_Fields("OptionsData4");
		//		pRPattern.CustomProperties.LoadFromString(pRPatternRS.Get_Fields("CustomPropertiesXMLData"));
		//		ReadRPatternExceptions(ref pRPattern);
		//		CreateRPatternFromRS = pRPattern;
		//		return CreateRPatternFromRS;
		//	}
		//	catch (Exception ex)
		//	{	// err1:
		//		if (fecherFoundation.Information.Err(ex).Number!=0) {
		//			Debug.WriteLine("Cannot read recurrence pattern from recordset: "+fecherFoundation.Information.Err(ex).Description);
		//			Debug.Assert(false);
		//		}
		//	}
		//}
		//// vbPorter upgrade warning: pRPattern As CalendarRecurrencePattern	OnWrite(XtremeCalendarControl.CalendarRecurrencePattern)
		//public void PutRPatternToRS(ref CalendarRecurrencePattern pRPatternref, clsDRWrapper pRPatternRS)
		//{
		//	try
		//	{	// On Error GoTo err1
		//		fecherFoundation.Information.Err().Clear();
		//		pRPatternRS.Set_Fields("MasterEventID", pRPattern.MasterEventId);
		//		pRPatternRS.Set_Fields("EventStartTime", pRPattern.StartTime);
		//		pRPatternRS.Set_Fields("EventDuration", pRPattern.DurationMinutes);
		//		pRPatternRS.Set_Fields("PatternStartDate", pRPattern.StartDate);
		//		pRPatternRS.Set_Fields("PatternEndMethod", pRPattern.EndMethod);
		//		pRPatternRS.Set_Fields("PatternEndDate", pRPattern.EndDate);
		//		pRPatternRS.Set_Fields("PatternEndAfterOccurrences", pRPattern.EndAfterOccurrences);
		//		pRPatternRS.Set_Fields("OptionsData1", pRPattern.Options.data1);
		//		pRPatternRS.Set_Fields("OptionsData2", pRPattern.Options.Data2);
		//		pRPatternRS.Set_Fields("OptionsData3", pRPattern.Options.Data3);
		//		pRPatternRS.Set_Fields("OptionsData4", pRPattern.Options.Data4);
		//		pRPatternRS.Set_Fields("CustomPropertiesXMLData", pRPattern.CustomProperties.SaveToString);
		//		return;
		//	}
		//	catch (Exception ex)
		//	{	// err1:
		//		if (fecherFoundation.Information.Err(ex).Number!=0) {
		//			Debug.WriteLine("Cannot read recurrence pattern from recordset: "+fecherFoundation.Information.Err(ex).Description);
		//			Debug.Assert(false);
		//		}
		//	}
		//}
		//public void ReadRPatternExceptions(ref CalendarRecurrencePattern pPattern)
		//{
		//	clsDRWrapper rsEvents = new clsDRWrapper();
		//	int nPatternID;
		//	string strSQL = "";
		//	CalendarEvent pEvent = new CalendarEvent();
		//	nPatternID = pPattern.ID;
		//	rsEvents.OpenRecordset("SELECT * FROM NewTasks WHERE RecurrenceState = "+xtpCalendarRecurrenceException+" AND RecurrencePatternID = "+FCConvert.ToString(nPatternID), modGlobalVariables.strGNDatabase);
		//	if (rsEvents.EndOfFile()!=true && rsEvents.BeginningOfFile()!=true) {
		//		do {
		//			pEvent = CreateEventFromRS(ref rsEvents, true);
		//			if (!(pEvent==null)) {
		//				pPattern.SetException(pEvent);
		//			}
		//			rsEvents.MoveNext();
		//		} while (!rsEvents.EndOfFile());
		//	}
		//}
		public string MakeRetrieveDayEventsSQL(DateTime dtDay, string strUser = "", bool boolShowAllEvents = false)
		{
			string MakeRetrieveDayEventsSQL = "";
			int nYear, nMonth, nDay;
			string strSQL;
			nYear = dtDay.Year;
			nMonth = dtDay.Month;
			nDay = dtDay.Day;
			strSQL = "SELECT * FROM NewTasks WHERE " + "\r\n";
			//strSQL = strSQL+" ( RecurrenceState = "+xtpCalendarRecurrenceNotRecurring+" OR "+"\r\n";
			//strSQL = strSQL+"   RecurrenceState = "+xtpCalendarRecurrenceMaster+") AND "+"\r\n";
			strSQL = strSQL + "( YEAR(StartDateTime) < " + FCConvert.ToString(nYear) + "\r\n";
			strSQL = strSQL + "  OR ( YEAR(StartDateTime) = " + FCConvert.ToString(nYear) + " AND " + "\r\n";
			strSQL = strSQL + "       ( MONTH(StartDateTime) < " + FCConvert.ToString(nMonth) + " OR ";
			strSQL = strSQL + "         MONTH(StartDateTime) = " + FCConvert.ToString(nMonth) + " AND " + "\r\n";
			strSQL = strSQL + "         DAY(StartDateTime) <= " + FCConvert.ToString(nDay) + "\r\n";
			strSQL = strSQL + "     ) ) )AND " + "\r\n";
			strSQL = strSQL + "( YEAR(EndDateTime) > " + FCConvert.ToString(nYear) + "\r\n";
			strSQL = strSQL + "  OR ( YEAR(EndDateTime) = " + FCConvert.ToString(nYear) + " AND " + "\r\n";
			strSQL = strSQL + "       (MONTH(EndDateTime) > " + FCConvert.ToString(nMonth) + " OR ";
			strSQL = strSQL + "        MONTH(EndDateTime) = " + FCConvert.ToString(nMonth) + " AND " + "\r\n";
			strSQL = strSQL + "        DAY(EndDateTime) >= " + FCConvert.ToString(nDay) + "\r\n";
			strSQL = strSQL + "     ) ) ) " + "\r\n";
			if (!boolShowAllEvents)
			{
				if (strUser != "")
				{
					// If Not boolShowAllEvents Then
					strSQL += " and (IsPrivate = 0 OR TaskPerson = '" + strUser + "')";
					// Else
					// End If
				}
				else
				{
					strSQL += " and isprivate = 0 or TaskPerson = ''";
				}
			}
			// Debug.Print strSQL
			MakeRetrieveDayEventsSQL = strSQL;
			return MakeRetrieveDayEventsSQL;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public string MakeGetUpcomingEventsSQL(DateTime dtFrom, int PeriodMinutes)
		{
			// The SQL script below is read all events from DB
			// RemindersManager will select only reminder which will be Fiered
			// untill dtFrom + PeriodMinutes. (generally dtFrom is Now)
			// 
			// Of cause you can write this script more optimized.
			// 
			string strSQL = "";
			//strSQL = "SELECT * FROM NewTasks WHERE (TaskPerson = '"+modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID()+"') AND (RecurrenceState = "+xtpCalendarRecurrenceNotRecurring+" OR RecurrenceState = "+xtpCalendarRecurrenceMaster+") ";
			// ----------------------------------------------------------
			DateTime dtUntil;
			DateTime dtReminder;
			dtUntil = fecherFoundation.DateAndTime.DateAdd("n", PeriodMinutes, dtFrom);
			string strReminderStartDateSQL;
			strReminderStartDateSQL = " DATEADD(\"n\", -1 * ReminderMinutesBeforeStart, StartDateTime) ";
			//strSQL = strSQL+" AND TaskReminder <> 0 AND "+strReminderStartDateSQL+" <= '"+FCConvert.ToString(dtUntil)+"' UNION ALL SELECT * FROM NewTasks WHERE (TaskPerson = '"+modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID()+"') AND (RecurrenceState = "+xtpCalendarRecurrenceException+") AND TaskReminder <> 0 AND "+strReminderStartDateSQL+" <= '"+FCConvert.ToString(dtUntil)+"'";
			return strSQL;
		}
	}
}
