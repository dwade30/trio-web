﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsReportParameters
	{
		//=========================================================
		private FCCollection ParameterList = new FCCollection();
		private int intCurrentIndex;
		private int lngNextGroupID;

		public void Clear()
		{
			int x;
			for (x = 0; x <= ParameterList.Count - 1; x++)
			{
				ParameterList.Remove(0);
			}
			// x
			intCurrentIndex = -1;
			lngNextGroupID = 0;
		}

		public void InsertParameter(ref clsReportParameter Parm)
		{
			int x;
			bool boolAdded;
			clsReportParameter tParm;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolAdded = false;
				for (x = 1; x <= ParameterList.Count; x++)
				{
					if (!(ParameterList[x] == null))
					{
						tParm = ParameterList[x];
						if (Parm.CodeType < tParm.CodeType)
						{
							lngNextGroupID += 1;
							Parm.ParameterGroup = lngNextGroupID;
							ParameterList.Add(Parm, null, x);
							boolAdded = true;
							return;
						}
						else if (Parm.CodeType == tParm.CodeType)
						{
							if (Parm.Code < tParm.Code)
							{
								lngNextGroupID += 1;
								Parm.ParameterGroup = lngNextGroupID;
								ParameterList.Add(Parm, null, x);
								boolAdded = true;
								return;
							}
							else if (Parm.Code == tParm.Code && tParm.ID == Parm.ID)
							{
								Parm.ParameterGroup = tParm.ParameterGroup;
								tParm.InGroup = true;
								Parm.InGroup = true;
								ParameterList.Add(Parm, null, null, x);
								boolAdded = true;
								return;
							}
						}
					}
				}
				// x
				if (!boolAdded)
				{
					lngNextGroupID += 1;
					Parm.ParameterGroup = lngNextGroupID;
					ParameterList.Add(Parm);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertParameter", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngCodeType As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intDBType As int	OnWriteFCConvert.ToDouble(
		public void AddParameter(int lngCodeType, ref int lngCode, ref bool boolIs, ref string strTo, ref string strFrom, ref int lngAutoID, int intDBType)
		{
			clsReportParameter tParm = new clsReportParameter();
			tParm.Code = lngCode;
			tParm.CodeType = lngCodeType;
			tParm.FromParameter = strFrom;
			tParm.ToParameter = strTo;
			tParm.ID = lngAutoID;
			tParm.IsTrue = boolIs;
			tParm.DBType = intDBType;
			InsertParameter(ref tParm);
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsReportParameter GetCurrentParameter
		{
			get
			{
				clsReportParameter GetCurrentParameter = null;
				clsReportParameter tParm;
				tParm = null;
				if (!FCUtils.IsEmpty(ParameterList))
				{
					if (intCurrentIndex > 0)
					{
						if (!(ParameterList[intCurrentIndex] == null))
						{
							tParm = ParameterList[intCurrentIndex];
						}
					}
				}
				GetCurrentParameter = tParm;
				return GetCurrentParameter;
			}
		}

		public void MoveFirst()
		{
			if (!(ParameterList == null))
			{
				if (!FCUtils.IsEmpty(ParameterList))
				{
					if (ParameterList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(ParameterList))
			{
				if (intCurrentIndex > ParameterList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= ParameterList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > ParameterList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (ParameterList[intCurrentIndex] == null)
						{
						}
						else if (ParameterList[intCurrentIndex].Unused)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}
	}
}
