//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsCEMaster
	{
		//=========================================================
		clsDRWrapper rsMast = new clsDRWrapper();
		private int lngCEAccount;
		private int lngRSAccount;
		private string strFirst = string.Empty;
		private string strMiddle = string.Empty;
		private string strLast = string.Empty;
		private string strDesig = string.Empty;
		private string strSecFirst = string.Empty;
		private string strSecMiddle = string.Empty;
		private string strSecLast = string.Empty;
		private string strSecDesig = string.Empty;
		private string strName = string.Empty;
		private string strSecOwner = string.Empty;
		private string strAddr1 = string.Empty;
		private string strAddr2 = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZip4 = string.Empty;
		private string strMapLot = string.Empty;
		private string strStreetNum = string.Empty;
		private string strApt = string.Empty;
		private string strStreet = string.Empty;
		private string strRef1 = string.Empty;
		private string strRef2 = string.Empty;
		private double dblAcres;
		private double dblFrontage;
		private string strEmail = string.Empty;
		private bool boolDeleted;
		private string strNeighborhood = string.Empty;
		private string strZone = string.Empty;
		// Private aryPhone() As PhoneRecord
		// Private aryAbutters() As Abutter
		private int lngPartyID1;
		private int lngPartyID2;
		private cParty pOwner;
		private cParty pSecOwner;
		private string strGuid = "";

		public string CEGUID
		{
			get
			{
				string CEGUID = "";
				CEGUID = strGuid;
				return CEGUID;
			}
			set
			{
				strGuid = value;
			}
		}

		public cParty OwnerParty()
		{
			cParty OwnerParty = null;
			OwnerParty = pOwner;
			return OwnerParty;
		}

		public cParty SecOwnerParty()
		{
			cParty SecOwnerParty = null;
			SecOwnerParty = pSecOwner;
			return SecOwnerParty;
		}

		public int PartyID2
		{
			set
			{
				lngPartyID2 = value;
			}
			get
			{
				int PartyID2 = 0;
				PartyID2 = lngPartyID2;
				return PartyID2;
			}
		}

		public int PartyID1
		{
			set
			{
				lngPartyID1 = value;
			}
			get
			{
				int PartyID1 = 0;
				PartyID1 = lngPartyID1;
				return PartyID1;
			}
		}

		public int CEAccount
		{
			set
			{
				lngCEAccount = value;
			}
			get
			{
				int CEAccount = 0;
				CEAccount = lngCEAccount;
				return CEAccount;
			}
		}

		public int RSAccount
		{
			set
			{
				lngRSAccount = value;
			}
			get
			{
				int RSAccount = 0;
				RSAccount = lngRSAccount;
				return RSAccount;
			}
		}

		public string OwnerName
		{
			set
			{
				strName = value;
			}
			get
			{
				string OwnerName = "";
				OwnerName = strName;
				return OwnerName;
			}
		}

		public string OwnerFirst
		{
			set
			{
				strFirst = value;
				// ReFigureNameStrings
			}
			get
			{
				string OwnerFirst = "";
				OwnerFirst = strFirst;
				return OwnerFirst;
			}
		}

		public string OwnerMiddle
		{
			set
			{
				strMiddle = value;
				// ReFigureNameStrings
			}
			get
			{
				string OwnerMiddle = "";
				OwnerMiddle = strMiddle;
				return OwnerMiddle;
			}
		}

		public string OwnerLast
		{
			set
			{
				strLast = value;
				// ReFigureNameStrings
			}
			get
			{
				string OwnerLast = "";
				OwnerLast = strLast;
				return OwnerLast;
			}
		}

		public string OwnerDesig
		{
			set
			{
				strDesig = value;
				// ReFigureNameStrings
			}
			get
			{
				string OwnerDesig = "";
				OwnerDesig = strDesig;
				return OwnerDesig;
			}
		}

		public string SecOwner
		{
			set
			{
				strSecOwner = value;
			}
			get
			{
				string SecOwner = "";
				SecOwner = strSecOwner;
				return SecOwner;
			}
		}

		public string SecOwnerFirst
		{
			set
			{
				strSecFirst = value;
				// ReFigureNameStrings
			}
			get
			{
				string SecOwnerFirst = "";
				SecOwnerFirst = strSecFirst;
				return SecOwnerFirst;
			}
		}

		public string SecOwnerMiddle
		{
			set
			{
				strSecMiddle = value;
				// ReFigureNameStrings
			}
			get
			{
				string SecOwnerMiddle = "";
				SecOwnerMiddle = strSecMiddle;
				return SecOwnerMiddle;
			}
		}

		public string SecOwnerLast
		{
			set
			{
				strSecLast = value;
				// ReFigureNameStrings
			}
			get
			{
				string SecOwnerLast = "";
				SecOwnerLast = strSecLast;
				return SecOwnerLast;
			}
		}

		public string SecOwnerDesig
		{
			set
			{
				strSecDesig = value;
				// ReFigureNameStrings
			}
			get
			{
				string SecOwnerDesig = "";
				SecOwnerDesig = strSecDesig;
				return SecOwnerDesig;
			}
		}

		public string Address1
		{
			set
			{
				strAddr1 = value;
			}
			get
			{
				string Address1 = "";
				Address1 = strAddr1;
				return Address1;
			}
		}

		public string Address2
		{
			set
			{
				strAddr2 = value;
			}
			get
			{
				string Address2 = "";
				Address2 = strAddr2;
				return Address2;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string Zip4
		{
			set
			{
				strZip4 = value;
			}
			get
			{
				string Zip4 = "";
				Zip4 = strZip4;
				return Zip4;
			}
		}

		public string MapLot
		{
			set
			{
				strMapLot = value;
			}
			get
			{
				string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
		}

		public string StreetNum
		{
			set
			{
				strStreetNum = value;
			}
			get
			{
				string StreetNum = "";
				StreetNum = strStreetNum;
				return StreetNum;
			}
		}

		public string Apt
		{
			set
			{
				strApt = value;
			}
			get
			{
				string Apt = "";
				Apt = strApt;
				return Apt;
			}
		}

		public string StreetName
		{
			set
			{
				strStreet = value;
			}
			get
			{
				string StreetName = "";
				StreetName = strStreet;
				return StreetName;
			}
		}

		public string Ref1
		{
			set
			{
				strRef1 = value;
			}
			get
			{
				string Ref1 = "";
				Ref1 = strRef1;
				return Ref1;
			}
		}

		public string Ref2
		{
			set
			{
				strRef2 = Ref2;
			}
			get
			{
				string Ref2 = "";
				Ref2 = strRef2;
				return Ref2;
			}
		}

		public double Acres
		{
			set
			{
				dblAcres = value;
			}
			get
			{
				double Acres = 0;
				Acres = dblAcres;
				return Acres;
			}
		}

		public double Frontage
		{
			set
			{
				dblFrontage = value;
			}
			get
			{
				double Frontage = 0;
				Frontage = dblFrontage;
				return Frontage;
			}
		}

		public string Email
		{
			set
			{
				strEmail = value;
			}
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
		}

		public bool Deleted
		{
			set
			{
				boolDeleted = value;
			}
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
		}

		public string Neighborhood
		{
			set
			{
				strNeighborhood = value;
			}
			get
			{
				string Neighborhood = "";
				Neighborhood = strNeighborhood;
				return Neighborhood;
			}
		}

		public string Zone
		{
			set
			{
				strZone = value;
			}
			get
			{
				string Zone = "";
				Zone = strZone;
				return Zone;
			}
		}

		public void Clear()
		{
			lngCEAccount = 0;
			lngRSAccount = 0;
			strName = "";
			strSecOwner = "";
			strFirst = "";
			strMiddle = "";
			strLast = "";
			strDesig = "";
			strSecFirst = "";
			strSecMiddle = "";
			strSecLast = "";
			strSecDesig = "";
			strAddr1 = "";
			strAddr2 = "";
			strCity = "";
			strState = "";
			strZip = "";
			strZip4 = "";
			strMapLot = "";
			strStreetNum = "";
			strApt = "";
			strStreet = "";
			strRef1 = "";
			strRef2 = "";
			dblAcres = 0;
			dblFrontage = 0;
			strEmail = "";
			boolDeleted = false;
			strNeighborhood = "";
			strZone = "";
			pOwner = new cParty();
			pSecOwner = new cParty();
			lngPartyID1 = 0;
			lngPartyID2 = 0;
			cCreateGUID tGUID = new cCreateGUID();
			strGuid = tGUID.CreateGUID();
			// Erase aryPhone
			// Erase aryAbutters
		}

		private int CreateNewAccount()
		{
			int CreateNewAccount = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngAcct;
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngAcctToUse;
				lngAcct = 0;
				CreateNewAccount = 0;
				clsLoad.OpenRecordset("select ceaccount FROM cemaster order by ceaccount", modGlobalVariables.Statics.strCEDatabase);
				lngAcctToUse = 1;
				while (!clsLoad.EndOfFile())
				{
					if (lngAcctToUse < clsLoad.Get_Fields_Int32("ceaccount"))
					{
						break;
					}
					lngAcctToUse += 1;
					clsLoad.MoveNext();
				}
				lngAcct = lngAcctToUse;
				CreateNewAccount = lngAcct;
				clsLoad = null;
				return CreateNewAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateNewAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateNewAccount;
		}

		public int Save()
		{
			int Save = 0;
			bool boolGotLock = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngAcct = 0;
				clsDRWrapper rsSave = new clsDRWrapper();
				Save = 0;
				boolGotLock = false;
				if (lngCEAccount == 0)
				{
					// must assign a new one
					if (strGuid == "")
					{
						cCreateGUID tGUID = new cCreateGUID();
						strGuid = tGUID.CreateGUID();
						tGUID = null;
					}
					string strUser = modGlobalConstants.Statics.clsSecurityClass.Get_UserID().ToStringES();
					if (modMain.AttemptLock(ref strUser, modCEConstants.CNSTMASTERLOCK))
					{
						lngAcct = CreateNewAccount();
						boolGotLock = true;
					}
					else
					{
						MessageBox.Show("Could not lock master table at this time" + "\r\n" + "Cannot create a new account", "Cannot Create Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return Save;
					}
					if (lngAcct > 0)
					{
						lngCEAccount = lngAcct;
					}
					else
					{
						modMain.ReleaseLock(modCEConstants.CNSTMASTERLOCK);
						return Save;
					}
				}
				rsSave.OpenRecordset("select * from cemaster where ceaccount = " + FCConvert.ToString(lngCEAccount), modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("ceaccount", lngCEAccount);
				}
				rsSave.Set_Fields("rsaccount", lngRSAccount);
				// rsSave.Fields("name") = strName
				rsSave.Set_Fields("Owner1ID", lngPartyID1);
				rsSave.Set_Fields("Owner2ID", lngPartyID2);
				rsSave.Set_Fields("ceguid", strGuid);
				// rsSave.Fields("first") = strFirst
				// rsSave.Fields("middle") = strMiddle
				// rsSave.Fields("last") = strLast
				// rsSave.Fields("desig") = strDesig
				// rsSave.Fields("secfirst") = strSecFirst
				// rsSave.Fields("secmiddle") = strSecMiddle
				// rsSave.Fields("seclast") = strSecLast
				// rsSave.Fields("secdesig") = strSecDesig
				// rsSave.Fields("secowner") = strSecOwner
				// rsSave.Fields("Address1") = strAddr1
				// rsSave.Fields("Address2") = strAddr2
				// rsSave.Fields("City") = strCity
				// rsSave.Fields("state") = strState
				// rsSave.Fields("zip") = strZip
				// rsSave.Fields("zip4") = strZip4
				rsSave.Set_Fields("StreetNumber", FCConvert.ToString(Conversion.Val(strStreetNum)));
				rsSave.Set_Fields("apt", strApt);
				rsSave.Set_Fields("StreetName", strStreet);
				rsSave.Set_Fields("ref1", strRef1);
				rsSave.Set_Fields("ref2", strRef2);
				rsSave.Set_Fields("acres", dblAcres);
				rsSave.Set_Fields("Zone", strZone);
				rsSave.Set_Fields("Neighborhood", strNeighborhood);
				rsSave.Set_Fields("frontage", dblFrontage);
				rsSave.Set_Fields("maplot", strMapLot);
				// rsSave.Fields("email") = strEmail
				rsSave.Update();
				if (boolGotLock)
				{
					modMain.ReleaseLock(modCEConstants.CNSTMASTERLOCK);
				}
				Save = lngCEAccount;
				rsSave = null;
				return Save;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolGotLock)
				{
					modMain.ReleaseLock(modCEConstants.CNSTMASTERLOCK);
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Save", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Save;
		}

		public int Load(int lngAccount, bool boolCE = true, bool boolLoadParties = false)
		{
			int Load = 0;
			// returns a negative number on error, 0 if there is not ce equivalent and + if there is a CE account
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strSQL = "";
				string strDB = "";
				clsDRWrapper rsZone = new clsDRWrapper();
				clsDRWrapper rsNeighborhood = new clsDRWrapper();
				int intZone = 0;
				int intNeighborhood = 0;
				string strCEMasterJoin;
				string strREMasterJoin;
				strCEMasterJoin = modCE.GetCEMasterJoin();
				strREMasterJoin = modCE.GetREMasterJoin();
				Load = -1;
				Clear();
				if (boolCE)
				{
					// strSQL = "Select * from cemaster where ceaccount = " & lngAccount
					strSQL = strCEMasterJoin + " where ceaccount = " + FCConvert.ToString(lngAccount);
					strDB = modGlobalVariables.Statics.strCEDatabase;
				}
				else
				{
					// strSQL = "Select * from master where rsaccount = " & lngAccount & " and rscard = 1"
					strSQL = strREMasterJoin + " where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1";
					strDB = modGlobalVariables.Statics.strREDatabase;
				}
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					strGuid = FCConvert.ToString(rsLoad.Get_Fields_String("ceguid"));
					strSecFirst = "";
					strSecMiddle = "";
					strSecLast = "";
					strSecDesig = "";
					lngRSAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("rsaccount"))));
					if (!boolCE)
					{
						strName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rsname")));
						strSecOwner = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rssecowner")));
						// ParseName strName
						// If strSecOwner <> vbNullString Then
						// ParseName strSecOwner, True
						// End If
						lngPartyID1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("OwnerPartyID"))));
						lngPartyID2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("SecOwnerPartyID"))));
						strAddr1 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rsaddr1")));
						strAddr2 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rsaddr2")));
						strCity = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rsaddr3")));
						strState = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rsstate")));
						strZip = FCConvert.ToString(rsLoad.Get_Fields_String("rszip"));
						strZip4 = FCConvert.ToString(rsLoad.Get_Fields_String("rszip4"));
						strRef1 = FCConvert.ToString(rsLoad.Get_Fields_String("rsref1"));
						strRef2 = FCConvert.ToString(rsLoad.Get_Fields_String("rsref2"));
						strMapLot = FCConvert.ToString(rsLoad.Get_Fields_String("rsmaplot"));
						strStreetNum = FCConvert.ToString(rsLoad.Get_Fields_String("rslocnumalph"));
						strApt = FCConvert.ToString(rsLoad.Get_Fields_String("rslocapt"));
						strStreet = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("rslocstreet")));
						dblAcres = Conversion.Val(rsLoad.Get_Fields_Double("piacres"));
						strEmail = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("email")));
						intZone = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("pizone"))));
						intNeighborhood = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("pineighborhood"))));
						strSQL = "select * from zones order by code";
						rsZone.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
						strSQL = "select * from neighborhood order by code";
						rsNeighborhood.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
						if (rsZone.FindFirstRecord("code", intZone))
						{
							strZone = FCConvert.ToString(rsZone.Get_Fields_String("description"));
						}
						else
						{
							strZone = "";
						}
						if (rsNeighborhood.FindFirstRecord("code", intNeighborhood))
						{
							strNeighborhood = FCConvert.ToString(rsNeighborhood.Get_Fields_String("description"));
						}
						else
						{
							strNeighborhood = "";
						}
					}
					else if (lngRSAccount <= 0 || !modGlobalVariables.Statics.CECustom.GetDataFromRE)
					{
						strName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("name")));
						strSecOwner = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("secowner")));
						// strFirst = Trim(rsLoad.Fields("first"))
						// strMiddle = Trim(rsLoad.Fields("middle"))
						// strLast = Trim(rsLoad.Fields("last"))
						// strDesig = Trim(rsLoad.Fields("desig"))
						// strSecFirst = Trim(rsLoad.Fields("SecFirst"))
						// strSecMiddle = Trim(rsLoad.Fields("secmiddle"))
						// strSecLast = Trim(rsLoad.Fields("seclast"))
						// strSecDesig = Trim(rsLoad.Fields("secdesig"))
						lngPartyID1 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("Owner1ID"))));
						lngPartyID2 = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("Owner2ID"))));
						strAddr1 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("Address1")));
						strAddr2 = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("Address2")));
						strCity = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("City")));
						strState = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("State")));
						strZip = FCConvert.ToString(rsLoad.Get_Fields_String("zip"));
						strZip4 = FCConvert.ToString(rsLoad.Get_Fields_String("zip4"));
						strRef1 = FCConvert.ToString(rsLoad.Get_Fields_String("ref1"));
						strRef2 = FCConvert.ToString(rsLoad.Get_Fields_String("ref2"));
						strMapLot = FCConvert.ToString(rsLoad.Get_Fields_String("maplot"));
						strStreetNum = FCConvert.ToString(rsLoad.Get_Fields("StreetNumber"));
						strApt = FCConvert.ToString(rsLoad.Get_Fields_String("apt"));
						strStreet = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("StreetName")));
						dblAcres = Conversion.Val(rsLoad.Get_Fields_Double("acres"));
						strEmail = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields_String("email")));
						strZone = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("zone")));
						strNeighborhood = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("neighborhood")));
						if (lngPartyID1 > 0 || lngPartyID2 > 0)
						{
							if (boolLoadParties)
							{
								cPartyController tCont = new cPartyController();
								if (lngPartyID1 > 0)
								{
									tCont.LoadParty(ref pOwner, lngPartyID1);
								}
								if (lngPartyID2 > 0)
								{
									tCont.LoadParty(ref pSecOwner, lngPartyID2);
								}
							}
						}
					}
					if (boolCE)
					{
						boolDeleted = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("deleted"));
						lngCEAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ceaccount"))));
						dblFrontage = Conversion.Val(rsLoad.Get_Fields_Double("frontage"));
					}
					else
					{
						rsLoad.OpenRecordset("select * from cemaster where rsaccount = " + FCConvert.ToString(lngAccount), modGlobalVariables.Statics.strCEDatabase);
						if (!rsLoad.EndOfFile())
						{
							lngCEAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ceaccount"))));
							dblFrontage = Conversion.Val(rsLoad.Get_Fields_Double("frontage"));
						}
					}
					// ReFigureNameStrings
				}
				Load = lngCEAccount;
				rsLoad = null;
				rsZone = null;
				rsNeighborhood = null;
				return Load;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Load;
		}
		// Private Sub ReFigureNameStrings()
		// If Trim(strMiddle & strLast & strDesig) = vbNullString Then
		// strName = strFirst
		// Else
		// If Trim(strLast) <> vbNullString Then
		// If Trim(strFirst & strMiddle) <> vbNullString Then
		// strName = Trim(Trim(Trim(Trim(strLast) & ", " & strFirst) & " " & strMiddle) & " " & strDesig)
		// Else
		// strName = strLast
		// End If
		// Else
		// strName = Trim(Trim(Trim(strFirst) & " " & strMiddle) & " " & strDesig)
		// End If
		// End If
		// If Trim(strSecMiddle & strSecLast & strSecDesig) = vbNullString Then
		// strSecOwner = strSecFirst
		// Else
		// If Trim(strSecLast) <> vbNullString Then
		// If Trim(strSecFirst & strSecMiddle) <> vbNullString Then
		// strSecOwner = Trim(Trim(Trim(Trim(strSecLast) & ", " & strSecFirst) & " " & strSecMiddle) & " " & strSecDesig)
		// Else
		// strSecOwner = strSecLast
		// End If
		// Else
		// strSecOwner = Trim(Trim(Trim(strSecFirst) & " " & strSecMiddle) & " " & strSecDesig)
		// End If
		// End If
		// End Sub
		// vbPorter upgrade warning: 'Return' As bool	OnWrite(bool, string)
		private bool ParsePartialName(string strOriginal, ref string strFName, ref string strMName, ref string strDName, ref string strLName, bool boolLast = false)
		{
			bool ParsePartialName = false;
			string strString;
			string[] strAry = null;
			// vbPorter upgrade warning: boolParse As string	OnWrite(bool)
			string boolParse;
			// vbPorter upgrade warning: intLimit As int	OnWriteFCConvert.ToInt32(
			int intLimit = 0;
			int x;
			ParsePartialName = false;
			strFName = "";
			strMName = "";
			strDName = "";
			strString = fecherFoundation.Strings.Trim(strOriginal);
			strString = Strings.Replace(strString, ".", "", 1, -1, CompareConstants.vbTextCompare);
			if (!boolLast)
			{
				if (Strings.InStr(1, strString, ",", CompareConstants.vbTextCompare) > 0)
				{
					strFName = strOriginal;
					return ParsePartialName;
				}
			}
			else
			{
				if (Strings.InStr(1, strString, ",", CompareConstants.vbTextCompare) > 0)
				{
					strAry = Strings.Split(strString, ",", -1, CompareConstants.vbTextCompare);
					strLName = strAry[0];
					strString = fecherFoundation.Strings.Trim(strAry[1]);
				}
			}
			boolParse = FCConvert.ToString(true);
			strAry = Strings.Split(strString, " ", -1, CompareConstants.vbTextCompare);
			if (Information.UBound(strAry, 1) > 2 || Information.UBound(strAry, 1) == 0)
			{
				strFName = strString;
				strMName = "";
				strDName = "";
			}
			else
			{
				intLimit = Information.UBound(strAry, 1);
				if (intLimit < 0)
					return ParsePartialName;
				if ((fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "SR") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "JR") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "II") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "III") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "IV") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "VI") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "VII") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "VIII") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "IX"))
				{
					strDName = strAry[Information.UBound(strAry, 1)];
					intLimit = (Information.UBound(strAry, 1) - 1);
				}
				else if ((fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "I") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "V") || (fecherFoundation.Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "X"))
				{
					if (Information.UBound(strAry, 1) == 2 && Strings.InStr(1, strOriginal, "&", CompareConstants.vbBinaryCompare) <= 0)
					{
						// if it is obviously not a middle initial
						strDName = strAry[Information.UBound(strAry, 1)];
						intLimit = (Information.UBound(strAry, 1) - 1);
					}
				}
				for (x = 0; x <= intLimit; x++)
				{
					if ((fecherFoundation.Strings.UCase(strAry[x]) == "TRUSTEE") || (fecherFoundation.Strings.UCase(strAry[x]) == "TRUSTEES"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "INC") || (fecherFoundation.Strings.UCase(strAry[x]) == "CORP") || (fecherFoundation.Strings.UCase(strAry[x]) == "COMPANY") || (fecherFoundation.Strings.UCase(strAry[x]) == "AT") || (fecherFoundation.Strings.UCase(strAry[x]) == "LLC") || (fecherFoundation.Strings.UCase(strAry[x]) == "ASSN"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "PARTNERS") || (fecherFoundation.Strings.UCase(strAry[x]) == "OWNERS") || (fecherFoundation.Strings.UCase(strAry[x]) == "ASSOC") || (fecherFoundation.Strings.UCase(strAry[x]) == "PROPERTIES") || (fecherFoundation.Strings.UCase(strAry[x]) == "APARTMENTS") || (fecherFoundation.Strings.UCase(strAry[x]) == "UTILITY") || (fecherFoundation.Strings.UCase(strAry[x]) == "UTILITIES") || (fecherFoundation.Strings.UCase(strAry[x]) == "UTIL"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "ASSOCIATES") || (fecherFoundation.Strings.UCase(strAry[x]) == "ASSOCIATION") || (fecherFoundation.Strings.UCase(strAry[x]) == "CONSERVANCY") || (fecherFoundation.Strings.UCase(strAry[x]) == "PRESERVE") || (fecherFoundation.Strings.UCase(strAry[x]) == "ASSOCIATE") || (fecherFoundation.Strings.UCase(strAry[x]) == "REVOCABLE"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "CORPORATION") || (fecherFoundation.Strings.UCase(strAry[x]) == "HOMEOWNERS") || (fecherFoundation.Strings.UCase(strAry[x]) == "ESTATE") || (fecherFoundation.Strings.UCase(strAry[x]) == "ESTATES") || (fecherFoundation.Strings.UCase(strAry[x]) == "BANK") || (fecherFoundation.Strings.UCase(strAry[x]) == "PRIVATE"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "MAINE") || (fecherFoundation.Strings.UCase(strAry[x]) == "BANGOR") || (fecherFoundation.Strings.UCase(strAry[x]) == "TRUST") || (fecherFoundation.Strings.UCase(strAry[x]) == "INVESTMENTS") || (fecherFoundation.Strings.UCase(strAry[x]) == "PORTLAND") || (fecherFoundation.Strings.UCase(strAry[x]) == "PUBLIC") || (fecherFoundation.Strings.UCase(strAry[x]) == "STATE"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "TOWN") || (fecherFoundation.Strings.UCase(strAry[x]) == "CITY") || (fecherFoundation.Strings.UCase(strAry[x]) == "COMPANY") || (fecherFoundation.Strings.UCase(strAry[x]) == "BUSINESS") || (fecherFoundation.Strings.UCase(strAry[x]) == "INCORPORATED") || (fecherFoundation.Strings.UCase(strAry[x]) == "HYDRO") || (fecherFoundation.Strings.UCase(strAry[x]) == "ELECTRIC") || (fecherFoundation.Strings.UCase(strAry[x]) == "LIBRARY"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "UNITED") || (fecherFoundation.Strings.UCase(strAry[x]) == "US") || (fecherFoundation.Strings.UCase(strAry[x]) == "GOVERNMENT") || (fecherFoundation.Strings.UCase(strAry[x]) == "GOV") || (fecherFoundation.Strings.UCase(strAry[x]) == "GOVT") || (fecherFoundation.Strings.UCase(strAry[x]) == "FEDERAL"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "MUNICIPAL") || (fecherFoundation.Strings.UCase(strAry[x]) == "MUNICIPALITY"))
					{
						boolParse = FCConvert.ToString(false);
					}
					else if ((fecherFoundation.Strings.UCase(strAry[x]) == "AL") || (fecherFoundation.Strings.UCase(strAry[x]) == "ALS"))
					{
						if (x > 0)
						{
							if (fecherFoundation.Strings.UCase(strAry[x - 1]) == "ET")
							{
								boolParse = FCConvert.ToString(false);
							}
						}
					}
				}
				// x
				if (FCConvert.CBool(boolParse))
				{
					if (Strings.InStr(1, strString, "&", CompareConstants.vbTextCompare) > 0)
					{
						strFName = "";
						for (x = 0; x <= intLimit; x++)
						{
							strFName += strAry[x] + " ";
						}
						// x
						strFName = fecherFoundation.Strings.Trim(strFName);
						strMName = "";
					}
					else if (intLimit <= 1)
					{
						strFName = strAry[0];
						if (intLimit > 0)
						{
							strMName = strAry[1];
						}
					}
					else
					{
						strFName = "";
						for (x = 0; x <= intLimit; x++)
						{
							strFName += strAry[x] + " ";
						}
						// x
						strFName = fecherFoundation.Strings.Trim(strFName);
						strMName = "";
					}
				}
			}
			ParsePartialName = FCConvert.CBool(boolParse);
			return ParsePartialName;
		}

		public object ParseName(ref string strOriginal, bool boolSecondOwner = false)
		{
			object ParseName = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strF;
				string strL;
				string strM;
				string strD;
				string strsecL;
				string strOtherHalf = "";
				bool boolParse;
				// vbPorter upgrade warning: intIndex As int	OnWriteFCConvert.ToInt32(
				int intIndex;
				int x;
				string[] strAry = null;
				int intLimit;
				strF = "";
				strL = "";
				strM = "";
				strD = "";
				strsecL = "";
				intIndex = Strings.InStr(1, strOriginal, ",", CompareConstants.vbTextCompare);
				if (intIndex > 0)
				{
					// search for special words that would indicate this isn't a name
					strL = Strings.Mid(strOriginal, 1, intIndex - 1);
					if (intIndex < strOriginal.Length)
					{
						strOtherHalf = Strings.Mid(strOriginal, intIndex + 1);
						// If InStr(1, strOtherHalf, ",") <= 0 Then
						// strOtherHalf = Trim(Replace(strOtherHalf, ".", "", , , vbTextCompare))
						// boolParse = True
						// strAry = Split(strOtherHalf, " ", , vbTextCompare)
						// If UBound(strAry) > 0 Then
						// intLimit = UBound(strAry)
						// Select Case UCase(strAry(UBound(strAry)))
						// Case "SR", "JR", "II", "III", "IV", "VI", "VII", "VIII", "IX"
						// strD = strAry(UBound(strAry))
						// intLimit = UBound(strAry) - 1
						// Case "I", "V", "X"
						// If UBound(strAry) = 2 And InStr(1, strOtherHalf, "&") <= 0 Then
						// if it is obviously not a middle initial
						// strD = strAry(UBound(strAry))
						// intLimit = UBound(strAry) - 1
						// End If
						// End Select
						// 
						// For x = 0 To intLimit
						// Select Case UCase(strAry(x))
						// Case "TRUSTEE", "TRUSTEES"
						// boolParse = False
						// Case "INC", "CORP", "COMPANY", "AT", "LLC", "ASSN"
						// boolParse = False
						// Case "PARTNERS", "OWNERS", "ASSOC", "PROPERTIES", "APARTMENTS", "UTILITY", "UTILITIES", "UTIL"
						// boolParse = False
						// Case "ASSOCIATES", "ASSOCIATION", "CONSERVANCY", "PRESERVE", "ASSOCIATE", "REVOCABLE"
						// boolParse = False
						// Case "CORPORATION", "HOMEOWNERS", "ESTATE", "ESTATES", "BANK", "PRIVATE"
						// boolParse = False
						// Case "MAINE", "BANGOR", "TRUST", "INVESTMENTS", "PORTLAND", "PUBLIC", "STATE"
						// boolParse = False
						// Case "TOWN", "CITY", "COMPANY", "BUSINESS", "INCORPORATED", "HYDRO", "ELECTRIC", "LIBRARY"
						// boolParse = False
						// Case "UNITED", "US", "GOVERNMENT", "GOV", "GOVT", "FEDERAL"
						// boolParse = False
						// Case "MUNICIPAL", "MUNICIPALITY"
						// boolParse = False
						// Case "AL", "ALS"
						// If x > 0 Then
						// If UCase(strAry(x - 1)) = "ET" Then
						// boolParse = False
						// End If
						// End If
						// 
						// End Select
						// Next x
						// 
						// If boolParse Then
						// If InStr(1, strOtherHalf, "&", vbTextCompare) > 0 Then
						// strF = ""
						// For x = 0 To intLimit
						// strF = strF & strAry(x) & " "
						// Next x
						// strF = Trim(strF)
						// strM = ""
						// ElseIf intLimit <= 1 Then
						// strF = strAry(0)
						// If intLimit > 0 Then
						// strM = strAry(1)
						// End If
						// Else
						// strF = ""
						// For x = 0 To intLimit
						// strF = strF & strAry(x) & " "
						// Next x
						// strF = Trim(strF)
						// strM = ""
						// End If
						// End If
						// Else
						// strF = strAry(0)
						// End If
						// If boolParse Then
						// 
						// Else
						// strF = strOriginal
						// strL = ""
						// strM = ""
						// strD = ""
						// End If
						// Else
						// can't parse
						// strF = strOriginal
						// strL = ""
						// strM = ""
						// strD = ""
						// End If
						bool boolAnd = false;
						string strAnd = "";
						strAnd = "";
						boolAnd = false;
						strOtherHalf = fecherFoundation.Strings.Trim(Strings.Replace(strOtherHalf, "  ", " ", 1, -1, CompareConstants.vbTextCompare));
						intIndex = Strings.InStr(1, strOtherHalf, "&", CompareConstants.vbTextCompare);
						if (intIndex < 1 && !boolSecondOwner)
						{
							intIndex = Strings.InStr(1, strOtherHalf, " and ", CompareConstants.vbTextCompare);
							if (intIndex > 0)
							{
								strOtherHalf = Strings.Replace(strOtherHalf, " and ", " & ", 1, -1, CompareConstants.vbTextCompare);
								boolAnd = true;
								strAnd = strOtherHalf;
							}
						}
						if (!boolSecondOwner && intIndex > 0)
						{
							strAry = Strings.Split(strOtherHalf, "&", -1, CompareConstants.vbTextCompare);
							strAry[1] = fecherFoundation.Strings.Trim(strAry[1]);
							strsecL = "";
							if (ParsePartialName(strAry[1], ref strF, ref strM, ref strD, ref strsecL, true))
							{
								strSecFirst = strF;
								strSecMiddle = strM;
								if (strsecL == "")
								{
									strSecLast = strL;
								}
								else
								{
									strSecLast = strsecL;
								}
								strSecDesig = strD;
								strAry[0] = fecherFoundation.Strings.Trim(strAry[0]);
								if (ParsePartialName(strAry[0], ref strF, ref strM, ref strD, ref strsecL))
								{
								}
								else
								{
									strSecFirst = "";
									strSecMiddle = "";
									strSecLast = "";
									strSecDesig = "";
								}
							}
							else
							{
								boolParse = false;
								if (boolAnd)
									strOtherHalf = strAnd;
								strF = strOtherHalf;
								strM = "";
								strD = "";
							}
						}
						else
						{
							if (boolAnd)
								strOtherHalf = strAnd;
							if (ParsePartialName(strOtherHalf, ref strF, ref strM, ref strD, ref strsecL))
							{
								if (boolAnd && strOtherHalf == strF)
								{
									strOtherHalf = strAnd;
								}
								boolParse = true;
							}
							else
							{
								boolParse = false;
								strF = strOriginal;
								strM = "";
								strD = "";
								strL = "";
							}
						}
					}
					else
					{
						strF = strOriginal;
						strL = "";
						strM = "";
						strD = "";
					}
				}
				else
				{
					strF = strOriginal;
				}
				if (!boolSecondOwner)
				{
					strFirst = strF;
					strMiddle = strM;
					strLast = strL;
					strDesig = strD;
				}
				else
				{
					strSecFirst = strF;
					strSecMiddle = strM;
					strSecLast = strL;
					strSecDesig = strD;
				}
				return ParseName;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ParseName", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ParseName;
		}

		public clsCEMaster() : base()
		{
			Clear();
		}
	}
}
