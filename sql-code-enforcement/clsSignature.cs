//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;

namespace TWCE0000
{
	public class clsSignature
	{
		//=========================================================
		private string strSignaturePath = "";
		private int lngID;
		private Image theImage;

		public void RELoadCurrentUserSignatureInfo()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lngID = FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				rsLoad.OpenRecordset("select * from signatures where userid = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					strSignaturePath = FCConvert.ToString(rsLoad.Get_Fields_String("signature"));
				}
				else
				{
					strSignaturePath = "";
				}
				if (strSignaturePath != "")
				{
					theImage = FCUtils.LoadPicture(strSignaturePath);
				}
				else
				{
					theImage = null;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		public void LoadCurrentUserSignatureInfo()
		{
			if (lngID != FCConvert.ToInt32(modGlobalConstants.Statics.clsSecurityClass.Get_UserID()))
			{
				RELoadCurrentUserSignatureInfo();
			}
		}

		public void LoadSignatureInfo(int lngUserID)
		{
			if (lngUserID != lngID)
			{
				ReLoadSignatureInfo(lngUserID);
			}
		}

		public void ReLoadSignatureInfo(int lngUserID)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lngID = lngUserID;
				rsLoad.OpenRecordset("select * from signatures where userid = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					strSignaturePath = FCConvert.ToString(rsLoad.Get_Fields_String("signature"));
				}
				else
				{
					strSignaturePath = "";
				}
				if (strSignaturePath != "")
				{
					theImage = FCUtils.LoadPicture(strSignaturePath);
				}
				else
				{
					theImage = null;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		public string SignatureFile
		{
			get
			{
				string SignatureFile = "";
				SignatureFile = strSignaturePath;
				return SignatureFile;
			}
			set
			{
				strSignaturePath = value;
				theImage = null;
			}
		}

		public bool SaveSignatureInfo()
		{
			bool SaveSignatureInfo = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			if (lngID > 0)
			{
				rsSave.OpenRecordset("select * from signatures where userid = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.Update();
					rsSave.Set_Fields("UserID", lngID);
				}
				rsSave.Set_Fields("Signature", strSignaturePath);
				rsSave.Update();
				SaveSignatureInfo = true;
			}
			else
			{
				SaveSignatureInfo = false;
			}
			return SaveSignatureInfo;
		}

		public Image GetSignature()
		{
			Image GetSignature = null;
			if (strSignaturePath != "")
			{
				if (!(theImage == null))
				{
					GetSignature = theImage;
				}
				else
				{
					FCFileSystem fs = new FCFileSystem();
					if (FCFileSystem.FileExists(strSignaturePath))
					{
						GetSignature = FCUtils.LoadPicture(strSignaturePath);
					}
					else
					{
						GetSignature = null;
					}
				}
			}
			else
			{
				GetSignature = null;
			}
			return GetSignature;
		}

		public Image GetCurrentSignature()
		{
			Image GetCurrentSignature = null;
			LoadCurrentUserSignatureInfo();
			GetCurrentSignature = GetSignature();
			return GetCurrentSignature;
		}

		public clsSignature() : base()
		{
			theImage = null;
		}
	}
}
