//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmReports : BaseForm
	{
		public frmReports()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReports InstancePtr
		{
			get
			{
				return (frmReports)Sys.GetInstance(typeof(frmReports));
			}
		}

		protected frmReports _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDynamicDocument ddDefault;
		const int CNSTREPORTTYPEINSPECTORS = 0;
		const int CNSTREPORTTYPECONTRACTORS = 1;
		const int CNSTREPORTTYPEPERMITS = 2;
		const int CNSTREPORTTYPEPROPERTY = 3;
		const int CNSTREPORTTYPEPROPERTYLIST = 4;
		const int CNSTREPORTTYPECUSTOMREPORT = 5;
		const int CNSTREPORTTYPECUSTOMFORMS = 6;
		const int CNSTREPORTTYPEOPENPERMITS = 7;
		const int CNSTReportTypePermitApplicationSummary = 8;
		const int CNSTReportTypeEnvelopes = 9;
		const int CNSTReportTypeAbutters = 10;
		// Private Const CNSTEnvelope10 As Integer = 0
		// Private Const CNSTEnvelopeB5 As Integer = 1
		// Private Const CNSTEnvelopeC5 As Integer = 2
		// Private Const CNSTEnvelopeDL As Integer = 3
		// Private Const CNSTEnvelopeMonarch As Integer = 4
		const int CNSTGRIDPARAMETERSCOLOUTLINE = 0;
		const int CNSTGRIDPARAMETERSCOLTABLE = 1;
		const int CNSTGRIDPARAMETERSCOLDESC = 2;
		const int CNSTGRIDPARAMETERSCOLNOT = 3;
		const int CNSTGRIDPARAMETERSCOLFROMVALUE = 4;
		const int CNSTGRIDPARAMETERSCOLTOVALUE = 5;
		const int CNSTGRIDPARAMETERSCOLVALUEFORMAT = 6;
		const int CNSTGRIDSETUPCOLAUTOID = 0;
		const int CNSTGRIDSETUPCOLDESC = 1;
		const int CNSTGRIDSETUPCOLVALUEFORMAT = 2;
		const int CNSTGRIDPROPERTYROWDELETED = -2;
		const int CNSTGRIDPROPERTYROWOWNERLAST = 0;
		const int CNSTGRIDPROPERTYROWOWNERFIRST = 1;
		const int CNSTGRIDPROPERTYROWMAPLOT = 2;
		const int CNSTGRIDPROPERTYROWACCOUNT = 3;
		const int CNSTGRIDPROPERTYROWLOCATION = 4;
		const int CNSTGRIDPROPERTYROWNEIGHBORHOOD = 5;
		const int CNSTGRIDPROPERTYROWZONE = 6;
		const int CNSTGRIDPERMITROWYEAR = 0;
		const int CNSTGRIDPERMITROWIDENTIFIER = 1;
		const int CNSTGRIDPERMITROWAPPLYDATE = 2;
		const int CNSTGRIDPERMITROWFILER = 5;
		const int CNSTGRIDPERMITROWTYPE = 3;
		const int CNSTGRIDPERMITROWCATEGORY = 4;
		const int CNSTGRIDPERMITROWSTATUS = 6;
		const int CNSTGRIDPERMITROWFEE = 7;
		const int CNSTGRIDCONTRACTORROWNAME = 0;
		const int CNSTGRIDCONTRACTORROWNAME2 = 1;
		const int CNSTGRIDCONTRACTORROWLICENSE = 2;
		const int CNSTGRIDCONTRACTORROWSTATUS = 3;
		const int CNSTGRIDCONTRACTORROWCLASS = 4;
		const int CNSTGRIDINSPECTIONROWTYPE = 0;
		const int CNSTGRIDINSPECTIONROWSTATUS = 1;
		const int CNSTGRIDINSPECTIONROWDATE = 2;
		const int CNSTGRIDINSPECTIONROWINSPECTOR = 3;
		const int CNSTGRIDORDERBYCOLDESC = 0;
		const int CNSTGRIDORDERBYCOLUSE = 1;
		const int CNSTGRIDORDERBYCOLDIRECTION = 2;
		const int CNSTGRIDORDERBYCOLFIELDNAME = 3;
		private string strPropertyList = "";
		private string strContractorList = "";
		private string strInspectionList = "";
		private string strPermitList = "";
		// Private Sub cmbReportType_Click()
		// Select Case cmbReportType.ItemData(cmbReportType.ListIndex)
		// Case CNSTCODETYPECONTRACTOR
		// FillContractorReports
		// Case CNSTCODETYPEPERMIT
		// FillPermitReports
		// Case CNSTCODETYPEINSPECTION
		// FillInspectionReports
		// Case CNSTCODETYPEPROPERTY
		// FillPropertyReports
		// End Select
		// End Sub
		private void frmReports_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReports properties;
			//frmReports.FillStyle	= 0;
			//frmReports.ScaleWidth	= 9300;
			//frmReports.ScaleHeight	= 8055;
			//frmReports.LinkTopic	= "Form2";
			//frmReports.LockControls	= -1  'True;
			//frmReports.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridOrderBy();
			FillReports();
			SetupGridParameters();
			SetupGridProperty();
			SetupGridPermit();
			SetupGridContractor();
			SetupGridInspection();
			RefillOrderByGrid(FCConvert.ToInt32(GridReport.TextMatrix(0, 0)));
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPRINTINGEDITSECURITY))
			{
				cmdDeleteParameters.Enabled = false;
				cmdSaveAs.Enabled = false;
				cmdSaveParameters.Enabled = false;
			}
		}
		// Private Sub FillReportTypes()
		// With cmbReportType
		// .Clear
		// .AddItem "Contractors"
		// .ItemData(.NewIndex) = CNSTCODETYPECONTRACTOR
		// .AddItem "Inspections"
		// .ItemData(.NewIndex) = CNSTCODETYPEINSPECTION
		// .AddItem "Permits"
		// .ItemData(.NewIndex) = CNSTCODETYPEPERMIT
		// .AddItem "Property"
		// .ItemData(.NewIndex) = CNSTCODETYPEPROPERTY
		// .ListIndex = 0
		// End With
		// End Sub
		private void FillReports()
		{
			string strTemp;
			GridReport.Rows = 1;
			strTemp = "#" + FCConvert.ToString(CNSTREPORTTYPECONTRACTORS) + ";Contractors|#" + FCConvert.ToString(CNSTREPORTTYPEINSPECTORS) + ";Inspectors|#" + FCConvert.ToString(CNSTREPORTTYPEPERMITS) + ";Permits|#" + FCConvert.ToString(CNSTREPORTTYPEPROPERTY) + ";Properties|#" + FCConvert.ToString(CNSTREPORTTYPEPROPERTYLIST) + ";Property List|#" + FCConvert.ToString(CNSTREPORTTYPEOPENPERMITS) + ";Open Permits|#" + FCConvert.ToString(CNSTReportTypePermitApplicationSummary) + ";Permit Application Summary|#" + FCConvert.ToString(CNSTReportTypeAbutters) + ";Abutters|#" + FCConvert.ToString(CNSTReportTypeEnvelopes) + ";Envelopes";
			if (FillGridCustomReports())
			{
				strTemp += "|#5;Custom Reports";
			}
			if (FillGridCustomForms())
			{
				strTemp += "|#6;Custom Forms";
			}
			GridReport.ColComboList(0, strTemp);
			GridReport.TextMatrix(0, 0, FCConvert.ToString(CNSTREPORTTYPECONTRACTORS));
			GridReport.ColWidth(0, FCConvert.ToInt32(GridReport.Width * 0.97));
			GridReport.ColWidth(1, 0);
		}

		private bool FillGridCustomReports()
		{
			bool FillGridCustomReports = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FillGridCustomReports = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp;
				int lngFirst;
				strTemp = "";
				lngFirst = 0;
				rsLoad.OpenRecordset("select * from customreports order by title", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					if (lngFirst == 0)
					{
						lngFirst = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					}
					strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields("title") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				else
				{
					return FillGridCustomReports;
				}
				gridCustomReports.ColComboList(0, strTemp);
				gridCustomReports.TextMatrix(0, 0, FCConvert.ToString(lngFirst));
				FillGridCustomReports = true;
				return FillGridCustomReports;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridCustomReports", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillGridCustomReports;
		}

		private bool FillGridCustomForms()
		{
			bool FillGridCustomForms = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				FillGridCustomForms = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strTemp;
				int lngFirst;
				strTemp = "";
				lngFirst = 0;
				rsLoad.OpenRecordset("select * from custombills order by formatname", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					if (lngFirst == 0)
					{
						lngFirst = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					}
					strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("FormatName") + "\t" + rsLoad.Get_Fields_String("Description") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				else
				{
					return FillGridCustomForms;
				}
				gridCustomForms.ColComboList(0, strTemp);
				gridCustomForms.TextMatrix(0, 0, FCConvert.ToString(lngFirst));
				FillGridCustomForms = true;
				return FillGridCustomForms;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridCustomForms", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillGridCustomForms;
		}

		private void frmReports_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
		}

		private void gridCustomForms_ClickEvent(object sender, System.EventArgs e)
		{
			gridCustomForms.EditCell();
		}

		private void gridCustomForms_ComboCloseUp(object sender, EventArgs e)
		{
			// setup the order by grid
			string strLast;
			string strNow;
			strLast = gridCustomForms.TextMatrix(0, 0);
			strNow = gridCustomForms.ComboData();
			gridCustomForms.EndEdit();
			//App.DoEvents();
			if (strLast != strNow)
			{
				RefillOrderByGrid(CNSTREPORTTYPECUSTOMFORMS);
			}
		}

		private void gridCustomReports_ClickEvent(object sender, System.EventArgs e)
		{
			gridCustomReports.EditCell();
		}

		private void gridCustomReports_ComboCloseUp(object sender, EventArgs e)
		{
			// setup the order by grid
			string strLast;
			string strNow;
			strLast = gridCustomReports.TextMatrix(0, 0);
			strNow = gridCustomReports.ComboData();
			gridCustomReports.EndEdit();
			//App.DoEvents();
			if (strLast != strNow)
			{
				RefillOrderByGrid(CNSTREPORTTYPECUSTOMREPORT);
			}
		}
		// Private Sub GridContractor_CellChanged(ByVal Row As Long, ByVal Col As Long)
		// Dim lngRow As Long
		// If Row > 0 Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLDESC
		// lngRow = Val(GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC))
		// GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = ""
		// GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTOVALUE) = ""
		// GridContractor.RowData(Row) = GridContractorSetup.RowData(lngRow)
		// GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
		// End Select
		// End If
		// End Sub
		//
		//
		//
		// Private Sub GridContractor_ComboCloseUp(ByVal Row As Long, ByVal Col As Long, FinishEdit As Boolean)
		// Dim lngRow As Long
		// Dim lngCol As Long
		// lngRow = GridContractor.Row
		// lngCol = GridContractor.Col
		// If lngCol = CNSTGRIDPARAMETERSCOLFROMVALUE Or lngCol = CNSTGRIDPARAMETERSCOLTOVALUE Then
		// GridContractor.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol) = GridContractor.ComboData
		// End If
		// GridContractor.Col = 4
		// End Sub
		// Private Sub GridContractor_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKeyInsert
		// KeyCode = 0
		// AddContractorCode
		// Case vbKeyDelete
		// End Select
		// End Sub
		//
		// Private Sub GridContractor_RowColChange()
		// Dim Row As Long
		// Dim Col As Long
		//
		// Row = GridContractor.MouseRow
		// Col = GridContractor.MouseCol
		// If Row > 0 Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// GridContractor.Editable = flexEDKbdMouse
		// GridContractor.ComboList = GridContractor.RowData(Row)
		//
		// If Val(GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT)) = CNSTVALUETYPEDATE Then
		// GridContractor.EditMask = "00/00/0000"
		// Else
		// GridContractor.EditMask = ""
		// End If
		// End Select
		// End If
		// End Sub
		// Private Sub GridContractor_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Row < 1 Then Exit Sub
		// Dim strTemp As String
		// Dim strAry() As String
		// Dim intTemp As Integer
		// intTemp = GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC)
		// If Val(GridContractorSetup.TextMatrix(intTemp, CNSTGRIDSETUPCOLAUTOID)) = 0 Then
		// Exit Sub
		// End If
		// GridContractor.EditMask = ""
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// Select Case Val(GridContractor.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT))
		// Case CNSTVALUETYPEDATE
		// strTemp = GridContractor.EditText
		// If IsDate(strTemp) Then
		// strTemp = Format(strTemp, "MM/dd/yyyy")
		// Else
		// strTemp = ""
		// End If
		// GridContractor.EditText = strTemp
		// Case CNSTVALUETYPEDECIMAL
		// strTemp = GridContractor.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strAry = Split(strTemp, ".", , vbTextCompare)
		// strTemp = Format(strAry(0), "#,###,###,##0")
		// If UBound(strAry) > 0 Then
		// strTemp = strTemp & "." & strAry(1)
		// End If
		// GridContractor.EditText = strTemp
		// End If
		// Case CNSTVALUETYPELONG
		// strTemp = GridContractor.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strTemp = Format(strTemp, "#,###,###,###,##0")
		// GridContractor.EditText = strTemp
		// End If
		// Case CNSTVALUETYPEDROPDOWN
		// strTemp = GridContractor.ComboData
		// GridContractor.Cell(FCGrid.CellPropertySettings.flexcpData, Row, Col) = Val(strTemp)
		// End Select
		// Case CNSTGRIDOPENSCOLDATE
		// End Select
		// End Sub
		// Private Sub GridInspection_CellChanged(ByVal Row As Long, ByVal Col As Long)
		// Dim lngRow As Long
		// If Row > 0 Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLDESC
		// lngRow = Val(GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC))
		// GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = ""
		// GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTOVALUE) = ""
		// GridInspection.RowData(Row) = GridInspectionSetup.RowData(lngRow)
		// Set GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = Nothing
		// Set GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, Row, CNSTGRIDPARAMETERSCOLTOVALUE) = Nothing
		// GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
		// End Select
		// End If
		// End Sub
		// Private Sub GridInspection_ComboCloseUp(ByVal Row As Long, ByVal Col As Long, FinishEdit As Boolean)
		// Dim lngRow As Long
		// Dim lngCol As Long
		// lngRow = GridInspection.Row
		// lngCol = GridInspection.Col
		// If lngCol = CNSTGRIDPARAMETERSCOLFROMVALUE Or lngCol = CNSTGRIDPARAMETERSCOLTOVALUE Then
		// GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol) = GridInspection.ComboData
		// End If
		// GridInspection.Col = 4
		// End Sub
		//
		// Private Sub GridInspection_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKeyInsert
		// KeyCode = 0
		// AddInspectionCode
		// Case vbKeyDelete
		// End Select
		// End Sub
		// Private Sub GridInspection_RowColChange()
		// Dim Row As Long
		// Dim Col As Long
		//
		// Row = GridInspection.MouseRow
		// Col = GridInspection.MouseCol
		// If Row > 0 Then
		// If Val(GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC)) > CNSTGRIDINSPECTIONROWINSPECTOR Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// GridInspection.Editable = flexEDKbdMouse
		// GridInspection.ComboList = GridInspection.RowData(Row)
		//
		// If Val(GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT)) = CNSTVALUETYPEDATE Then
		// GridInspection.EditMask = "00/00/0000"
		// GridInspection.ComboList = ""
		// Else
		// GridInspection.EditMask = ""
		// End If
		// End Select
		// Else
		// If Col = CNSTGRIDPARAMETERSCOLFROMVALUE Or Col = CNSTGRIDPARAMETERSCOLTOVALUE Then
		// GridInspection.Editable = flexEDKbdMouse
		// GridInspection.ComboList = GridInspection.RowData(Row)
		// If Row = CNSTGRIDinspectionROWAPPLYDATE Then
		// GridInspection.EditMask = "00/00/0000"
		// Else
		// GridInspection.EditMask = ""
		// End If
		// End If
		// End If
		//
		// End If
		// End Sub
		//
		// Private Sub GridInspection_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Row < 1 Then Exit Sub
		// Dim strTemp As String
		// Dim strAry() As String
		// Dim intTemp As Integer
		// intTemp = GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC)
		// If Val(GridInspectionSetup.TextMatrix(intTemp, CNSTGRIDSETUPCOLAUTOID)) = 0 Then
		// Exit Sub
		// End If
		// GridInspection.EditMask = ""
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// Select Case Val(GridInspection.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT))
		// Case CNSTVALUETYPEDATE
		// strTemp = GridInspection.EditText
		// If IsDate(strTemp) Then
		// strTemp = Format(strTemp, "MM/dd/yyyy")
		// Else
		// strTemp = ""
		// End If
		// GridInspection.EditText = strTemp
		// Case CNSTVALUETYPEDECIMAL
		// strTemp = GridInspection.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strAry = Split(strTemp, ".", , vbTextCompare)
		// strTemp = Format(strAry(0), "#,###,###,##0")
		// If UBound(strAry) > 0 Then
		// strTemp = strTemp & "." & strAry(1)
		// End If
		// GridInspection.EditText = strTemp
		// End If
		// Case CNSTVALUETYPELONG
		// strTemp = GridInspection.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strTemp = Format(strTemp, "#,###,###,###,##0")
		// GridInspection.EditText = strTemp
		// End If
		// Case CNSTVALUETYPEDROPDOWN
		// strTemp = GridInspection.ComboData
		// GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, Row, Col) = Val(strTemp)
		// End Select
		// Case CNSTGRIDOPENSCOLDATE
		// End Select
		// End Sub
		private void CheckForGroups()
		{
			int lngRow;
			int tempRow;
			int lngCat = 0;
			int lngType;
			int tRow = 0;
			int lngAutoID = 0;
			int lngCode = 0;
			for (lngRow = 1; lngRow <= GridParameters.Rows - 1; lngRow++)
			{
				GridParameters.IsSubtotal(lngRow, true);
				lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE))));
				tRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC))));
				switch (lngCat)
				{
					case modCEConstants.CNSTCODETYPECONTRACTOR:
						{
							lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContractorSetup.TextMatrix(tRow, CNSTGRIDSETUPCOLAUTOID))));
							break;
						}
					case modCEConstants.CNSTCODETYPEINSPECTION:
						{
							lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridInspectionSetup.TextMatrix(tRow, CNSTGRIDSETUPCOLAUTOID))));
							break;
						}
					case modCEConstants.CNSTCODETYPEPERMIT:
						{
							lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPermitSetup.TextMatrix(tRow, CNSTGRIDSETUPCOLAUTOID))));
							break;
						}
					case modCEConstants.CNSTCODETYPEPROPERTY:
						{
							lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPropertySetup.TextMatrix(tRow, CNSTGRIDSETUPCOLAUTOID))));
							break;
						}
				}
				//end switch
				if (lngAutoID == 0)
				{
					lngCode = tRow;
				}
				else
				{
					lngCode = 0;
				}
				if (lngRow > 1)
				{
					if (lngCat != Conversion.Val(GridParameters.TextMatrix(lngRow - 1, CNSTGRIDPARAMETERSCOLTABLE)) || lngAutoID != DetermineAutoID(lngRow - 1) || lngCode != DetermineCode(lngRow - 1))
					{
						GridParameters.RowOutlineLevel(lngRow, 0);
						GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLOUTLINE, "");
					}
				}
				else
				{
					GridParameters.RowOutlineLevel(lngRow, 0);
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLOUTLINE, "");
				}
				if (GridParameters.RowOutlineLevel(lngRow) == 0 && lngRow < GridParameters.Rows - 1)
				{
					for (tempRow = lngRow + 1; tempRow <= GridParameters.Rows - 1; tempRow++)
					{
						if (lngCat == Conversion.Val(GridParameters.TextMatrix(tempRow, CNSTGRIDPARAMETERSCOLTABLE)) && lngAutoID == DetermineAutoID(tempRow) && lngCode == DetermineCode(tempRow))
						{
							GridParameters.RowOutlineLevel(tempRow, 1);
							GridParameters.TextMatrix(tempRow, CNSTGRIDPARAMETERSCOLOUTLINE, "OR");
							if (tempRow > lngRow + 1)
							{
								if (lngCat != Conversion.Val(GridParameters.TextMatrix(tempRow - 1, CNSTGRIDPARAMETERSCOLTABLE)) || lngAutoID != DetermineAutoID(tempRow - 1) || lngCode != DetermineCode(tempRow - 1))
								{
									// move the row
									GridParameters.RowPosition(tempRow, lngRow + 1);
								}
							}
						}
						GridParameters.IsSubtotal(tempRow, true);
					}
					// tempRow
				}
			}
			// lngRow
		}

		private int DetermineAutoID(int lngRow)
		{
			int DetermineAutoID = 0;
			int lngAutoID = 0;
			int lngSetupRow;
			int lngCat;
			lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE))));
			lngSetupRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC))));
			switch (lngCat)
			{
				case modCEConstants.CNSTCODETYPECONTRACTOR:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContractorSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
				case modCEConstants.CNSTCODETYPEINSPECTION:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridInspectionSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
				case modCEConstants.CNSTCODETYPEPERMIT:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPermitSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
				case modCEConstants.CNSTCODETYPEPROPERTY:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPropertySetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
			}
			//end switch
			DetermineAutoID = lngAutoID;
			return DetermineAutoID;
		}

		private int DetermineCode(int lngRow)
		{
			int DetermineCode = 0;
			int lngAutoID = 0;
			int lngSetupRow;
			int lngCat;
			lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE))));
			lngSetupRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC))));
			switch (lngCat)
			{
				case modCEConstants.CNSTCODETYPECONTRACTOR:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContractorSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
				case modCEConstants.CNSTCODETYPEINSPECTION:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridInspectionSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
				case modCEConstants.CNSTCODETYPEPERMIT:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPermitSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
				case modCEConstants.CNSTCODETYPEPROPERTY:
					{
						lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPropertySetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
						break;
					}
			}
			//end switch
			if (lngAutoID == 0)
			{
				DetermineCode = lngSetupRow;
			}
			else
			{
				DetermineCode = 0;
			}
			return DetermineCode;
		}

		private void GridParameters_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
            int GridParametersRow = GridParameters.GetFlexRowIndex(e.RowIndex);
            int GridParametersCol = GridParameters.GetFlexColIndex(e.ColumnIndex);

            int lngRow = 0;
			if (GridParametersRow > 0)
			{
				switch (GridParametersCol)
				{
					case CNSTGRIDPARAMETERSCOLDESC:
						{
							lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, GridParametersRow, CNSTGRIDPARAMETERSCOLDESC))));
							GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLFROMVALUE, "");
							GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTOVALUE, "");
							if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
							{
								GridParameters.RowData(GridParametersRow, GridContractorSetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							}
							else if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEINSPECTION)
							{
								GridParameters.RowData(GridParametersRow, GridInspectionSetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							}
							else if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPERMIT)
							{
								GridParameters.RowData(GridParametersRow, GridPermitSetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							}
							else if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPROPERTY)
							{
								GridParameters.RowData(GridParametersRow, GridPropertySetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							}
							// Set GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = Nothing
							// Set GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, Row, CNSTGRIDPARAMETERSCOLTOVALUE) = Nothing
							GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, GridParametersRow, CNSTGRIDPARAMETERSCOLFROMVALUE, "");
							GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, GridParametersRow, CNSTGRIDPARAMETERSCOLTOVALUE, "");
							CheckForGroups();
							break;
						}
					case CNSTGRIDPARAMETERSCOLTABLE:
						{
							GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLFROMVALUE, "");
							GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTOVALUE, "");
							lngRow = 0;
							if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
							{
								GridParameters.RowData(GridParametersRow, GridContractorSetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLDESC, GridContractorSetup.TextMatrix(CNSTGRIDCONTRACTORROWNAME, CNSTGRIDSETUPCOLDESC));
							}
							else if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEINSPECTION)
							{
								GridParameters.RowData(GridParametersRow, GridInspectionSetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLDESC, GridInspectionSetup.TextMatrix(CNSTGRIDINSPECTIONROWTYPE, CNSTGRIDSETUPCOLDESC));
							}
							else if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPERMIT)
							{
								GridParameters.RowData(GridParametersRow, GridPermitSetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLDESC, GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWYEAR, CNSTGRIDSETUPCOLDESC));
							}
							else if (Conversion.Val(GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPROPERTY)
							{
								GridParameters.RowData(GridParametersRow, GridPropertySetup.RowData(lngRow));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT));
								GridParameters.TextMatrix(GridParametersRow, CNSTGRIDPARAMETERSCOLDESC, GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWOWNERLAST, CNSTGRIDSETUPCOLDESC));
							}
							// Set GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = Nothing
							// Set GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, Row, CNSTGRIDPARAMETERSCOLTOVALUE) = Nothing
							GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, GridParametersRow, CNSTGRIDPARAMETERSCOLFROMVALUE, "");
							GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, GridParametersRow, CNSTGRIDPARAMETERSCOLTOVALUE, "");
							CheckForGroups();
							break;
						}
				}
				//end switch
			}
		}

		private void GridParameters_ComboCloseUp(object sender, EventArgs e)
		{
			int lngRow;
			int lngCol;
			lngRow = GridParameters.Row;
			lngCol = GridParameters.Col;
			if (lngCol == CNSTGRIDPARAMETERSCOLFROMVALUE || lngCol == CNSTGRIDPARAMETERSCOLTOVALUE)
			{
				GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol, GridParameters.ComboData());
			}
			else if (lngCol == CNSTGRIDPARAMETERSCOLDESC)
			{
				GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol, GridParameters.ComboData());
			}
			else if (lngCol == CNSTGRIDPARAMETERSCOLTABLE)
			{
				// GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = ""
				// GridParameters.TextMatrix(Row, cnstgridparameterstovalue) = ""
				// lngRow = 0
				// Select Case Val(GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTABLE))
				// Case CNSTCODETYPECONTRACTOR
				// GridParameters.RowData(Row) = GridContractorSetup.RowData(lngRow)
				// GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
				// Case CNSTCODETYPEINSPECTION
				// GridParameters.RowData(Row) = GridInspectionSetup.RowData(lngRow)
				// GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
				// Case CNSTCODETYPEPERMIT
				// GridParameters.RowData(Row) = GridPermitSetup.RowData(lngRow)
				// GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
				// Case CNSTCODETYPEPROPERTY
				// GridParameters.RowData(Row) = GridPropertySetup.RowData(lngRow)
				// GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
				// End Select
			}
			GridParameters.Col = CNSTGRIDPARAMETERSCOLVALUEFORMAT;
		}

		private void GridParameters_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						AddParameter();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteParameter();
						break;
					}
			}
			//end switch
		}

		private void GridParameters_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridParameters.MouseRow;
			if (mRow < 0)
			{
				GridParameters.Row = 0;
				GridParameters.Focus();
			}
		}

		private void GridParameters_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			// Row = GridParameters.MouseRow
			// Col = GridParameters.MouseCol
			Row = GridParameters.Row;
			Col = GridParameters.Col;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDPARAMETERSCOLFROMVALUE:
					case CNSTGRIDPARAMETERSCOLTOVALUE:
						{
							GridParameters.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							GridParameters.ComboList = FCConvert.ToString(GridParameters.RowData(Row));
							if (Conversion.Val(GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
							{
								GridParameters.EditMask = "00/00/0000";
							}
							else
							{
								GridParameters.EditMask = "";
							}
							break;
						}
					case CNSTGRIDPARAMETERSCOLDESC:
						{
							GridParameters.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							if (Conversion.Val(GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
							{
								GridParameters.ComboList = strContractorList;
							}
							else if (Conversion.Val(GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEINSPECTION)
							{
								GridParameters.ComboList = strInspectionList;
							}
							else if (Conversion.Val(GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPERMIT)
							{
								GridParameters.ComboList = strPermitList;
							}
							else if (Conversion.Val(GridParameters.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPROPERTY)
							{
								GridParameters.ComboList = strPropertyList;
							}
							GridParameters.EditMask = "";
							break;
						}
				}
				//end switch
			}
		}
		// Private Sub GridPermit_ComboCloseUp(ByVal Row As Long, ByVal Col As Long, FinishEdit As Boolean)
		// Dim lngRow As Long
		// Dim lngCol As Long
		// lngRow = GridPermit.Row
		// lngCol = GridPermit.Col
		// If lngCol = CNSTGRIDPARAMETERSCOLFROMVALUE Or lngCol = CNSTGRIDPARAMETERSCOLTOVALUE Then
		// GridPermit.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol) = GridPermit.ComboData
		// End If
		// GridPermit.Col = 4
		// End Sub
		//
		// Private Sub GridPermit_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKeyInsert
		// KeyCode = 0
		// AddPermitCode
		// Case vbKeyDelete
		// End Select
		// End Sub
		// Private Sub GridPermit_RowColChange()
		// Dim Row As Long
		// Dim Col As Long
		//
		// Row = GridPermit.MouseRow
		// Col = GridPermit.MouseCol
		// If Row > 0 Then
		// If Val(GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC)) > CNSTGRIDPERMITROWSTATUS Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// GridPermit.Editable = flexEDKbdMouse
		// GridPermit.ComboList = GridPermit.RowData(Row)
		//
		// If Val(GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT)) = CNSTVALUETYPEDATE Then
		// GridPermit.EditMask = "00/00/0000"
		// Else
		// GridPermit.EditMask = ""
		// End If
		// End Select
		// Else
		// If Col = CNSTGRIDPARAMETERSCOLFROMVALUE Or Col = CNSTGRIDPARAMETERSCOLTOVALUE Then
		// GridPermit.Editable = flexEDKbdMouse
		// GridPermit.ComboList = GridPermit.RowData(Row)
		// If Row = CNSTGRIDPERMITROWAPPLYDATE Then
		// GridPermit.EditMask = "00/00/0000"
		// Else
		// GridPermit.EditMask = ""
		// End If
		// End If
		// End If
		//
		// End If
		// End Sub
		//
		// Private Sub GridPermit_CellChanged(ByVal Row As Long, ByVal Col As Long)
		// Dim lngRow As Long
		// If Row > 0 Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLDESC
		// lngRow = Val(GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC))
		// GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = ""
		// GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTOVALUE) = ""
		// GridPermit.RowData(Row) = GridPermitSetup.RowData(lngRow)
		// GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
		// End Select
		// End If
		// End Sub
		//
		// Private Sub GridPermit_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Row < 1 Then Exit Sub
		// Dim strTemp As String
		// Dim strAry() As String
		// Dim intTemp As Integer
		// intTemp = GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC)
		// If Val(GridPermitSetup.TextMatrix(intTemp, CNSTGRIDSETUPCOLAUTOID)) = 0 Then
		// Exit Sub
		// End If
		// GridPermit.EditMask = ""
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// Select Case Val(GridPermit.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT))
		// Case CNSTVALUETYPEDATE
		// strTemp = GridPermit.EditText
		// If IsDate(strTemp) Then
		// strTemp = Format(strTemp, "MM/dd/yyyy")
		// Else
		// strTemp = ""
		// End If
		// GridPermit.EditText = strTemp
		// Case CNSTVALUETYPEDECIMAL
		// strTemp = GridPermit.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strAry = Split(strTemp, ".", , vbTextCompare)
		// strTemp = Format(strAry(0), "#,###,###,##0")
		// If UBound(strAry) > 0 Then
		// strTemp = strTemp & "." & strAry(1)
		// End If
		// GridPermit.EditText = strTemp
		// End If
		// Case CNSTVALUETYPELONG
		//
		// strTemp = GridPermit.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strTemp = Format(strTemp, "#,###,###,###,##0")
		// GridPermit.EditText = strTemp
		// End If
		// Case CNSTVALUETYPEDROPDOWN
		// strTemp = GridPermit.ComboData
		// GridPermit.Cell(FCGrid.CellPropertySettings.flexcpData, Row, Col) = Val(strTemp)
		// End Select
		// Case CNSTGRIDOPENSCOLDATE
		// End Select
		// End Sub
		//
		// Private Sub GridProperty_CellChanged(ByVal Row As Long, ByVal Col As Long)
		// Dim lngRow As Long
		// If Row > 0 Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLDESC
		// lngRow = Val(GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC))
		// GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLFROMVALUE) = ""
		// GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLTOVALUE) = ""
		// GridProperty.RowData(Row) = GridPropertySetup.RowData(lngRow)
		// GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT) = GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT)
		// End Select
		// End If
		// End Sub
		// Private Sub GridProperty_ComboCloseUp(ByVal Row As Long, ByVal Col As Long, FinishEdit As Boolean)
		// Dim lngRow As Long
		// Dim lngCol As Long
		// lngRow = GridProperty.Row
		// lngCol = GridProperty.Col
		// If lngCol = CNSTGRIDPARAMETERSCOLFROMVALUE Or lngCol = CNSTGRIDPARAMETERSCOLTOVALUE Then
		// GridProperty.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol) = GridProperty.ComboData
		// End If
		// GridProperty.Col = 4
		// End Sub
		//
		// Private Sub GridProperty_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKeyInsert
		// AddPropertyCode
		// Case vbKeyDelete
		// End Select
		// End Sub
		// Private Sub AddPropertyCode()
		// GridProperty.Rows = GridProperty.Rows + 1
		// GridProperty.TextMatrix(GridProperty.Rows - 1, CNSTGRIDPARAMETERSCOLDESC) = 0
		// GridProperty.TopRow = GridProperty.Rows - 1
		// GridProperty.TextMatrix(GridProperty.Rows - 1, CNSTGRIDPARAMETERSCOLNOT) = 0
		//
		// End Sub
		//
		// Private Sub AddPermitCode()
		// GridPermit.Rows = GridPermit.Rows + 1
		// GridPermit.TextMatrix(GridPermit.Rows - 1, CNSTGRIDPARAMETERSCOLDESC) = 0
		// GridPermit.TopRow = GridPermit.Rows - 1
		// GridPermit.TextMatrix(GridPermit.Rows - 1, CNSTGRIDPARAMETERSCOLNOT) = 0
		// End Sub
		//
		// Private Sub AddInspectionCode()
		// GridInspection.Rows = GridInspection.Rows + 1
		// GridInspection.TextMatrix(GridInspection.Rows - 1, CNSTGRIDPARAMETERSCOLDESC) = 0
		// GridInspection.TopRow = GridInspection.Rows - 1
		// GridInspection.TextMatrix(GridInspection.Rows - 1, CNSTGRIDPARAMETERSCOLNOT) = 0
		// End Sub
		//
		// Private Sub AddContractorCode()
		// GridContractor.Rows = GridContractor.Rows + 1
		// GridContractor.TextMatrix(GridContractor.Rows - 1, CNSTGRIDPARAMETERSCOLDESC) = 0
		// GridContractor.TopRow = GridContractor.Rows - 1
		// GridContractor.TextMatrix(GridContractor.Rows - 1, CNSTGRIDPARAMETERSCOLNOT) = 0
		// End Sub
		private void AddParameter()
		{
			GridParameters.Rows += 1;
			GridParameters.TopRow = GridParameters.Rows - 1;
			GridParameters.TextMatrix(GridParameters.Rows - 1, CNSTGRIDPARAMETERSCOLNOT, FCConvert.ToString(0));
			GridParameters.TextMatrix(GridParameters.Rows - 1, CNSTGRIDPARAMETERSCOLTABLE, FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR));
		}

		private void DeleteParameter()
		{
			if (GridParameters.Row < 1)
				return;
			int lngRow;
			lngRow = GridParameters.Row;
			GridParameters.RemoveItem(lngRow);
			CheckForGroups();
		}

		private void GridReport_ClickEvent(object sender, System.EventArgs e)
		{
			GridReport.EditCell();
		}

		private void GridReport_ComboCloseUp(object sender, EventArgs e)
		{
			// setup the order by grid
			string strLast;
			string strNow;
			strLast = GridReport.TextMatrix(GridReport.Row, 0);
			strNow = GridReport.ComboData();
			GridReport.EndEdit();
			//App.DoEvents();
			if (strLast != strNow)
			{
				RefillOrderByGrid(FCConvert.ToInt32(Conversion.Val(strNow)));
			}
		}

		private void GridReport_Enter(object sender, System.EventArgs e)
		{
			GridReport.EditCell();
		}

		private void mnuDeleteParameters_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strList;
			string strReturn;
			string[] strAry = null;
			strSQL = "select * from ParameterGroups order by description";
			rsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsLoad.EndOfFile())
			{
				MessageBox.Show("No saved parameters found", "No Parameters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strList = "";
			while (!rsLoad.EndOfFile())
			{
				strList += rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			strList = Strings.Mid(strList, 1, strList.Length - 1);
			strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strList, "Load Parameters");
			if (strReturn == string.Empty)
				return;
			strAry = Strings.Split(strReturn, ",", -1, CompareConstants.vbTextCompare);
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
			{
				rsLoad.Execute("delete from savedparameters where parametergroup = " + strAry[x], modGlobalVariables.Statics.strCEDatabase);
				rsLoad.Execute("delete from parametergroups where ID = " + strAry[x], modGlobalVariables.Statics.strCEDatabase);
			}
			// x
			MessageBox.Show("Parameters deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuLoadParameters_Click(object sender, System.EventArgs e)
		{
			LoadParameters();
		}

		private void mnuSaveAs_Click(object sender, System.EventArgs e)
		{
			SaveParameters(true);
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsSQLStatement SQLStatement;
			int lngID = 0;
			string strTemp = "";
			bool boolTemp = false;
			// If cmbReportType.ListIndex >= 0 Then
			GridReport.Row = -1;
			GridOrderBy.Row = 0;
			GridParameters.Row = -1;
			gridCustomReports.Row = -1;
			gridCustomForms.Row = -1;
			//App.DoEvents();
			switch (FCConvert.ToInt32(GridReport.TextMatrix(0, 0)))
			{
				case CNSTREPORTTYPEINSPECTORS:
					{
						SQLStatement = GetReportSQL("Inspectors");
						rptInspectorsList.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTREPORTTYPECONTRACTORS:
					{
						SQLStatement = GetReportSQL("Contractors");
						rptContractors.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTREPORTTYPEPERMITS:
					{
						SQLStatement = GetReportSQL("permits");
						rptPermits.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTReportTypeAbutters:
					{
						SQLStatement = GetReportSQL("cemaster");
						rptAbutters.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTREPORTTYPEPROPERTY:
					{
						SQLStatement = GetReportSQL("cemaster");
						rptProperty.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTREPORTTYPEPROPERTYLIST:
					{
						SQLStatement = GetReportSQL("cemaster");
						rptPropertyList.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTREPORTTYPEOPENPERMITS:
					{
						SQLStatement = GetReportSQL("permits");
						rptOpenPermits.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTReportTypePermitApplicationSummary:
					{
						SQLStatement = GetReportSQL("permits");
						rptPermitFees.InstancePtr.Init(ref SQLStatement);
						break;
					}
				case CNSTReportTypeEnvelopes:
					{
						SQLStatement = GetReportSQL("CEMaster");
						lngID = frmChooseEnvelope.InstancePtr.Init();
						strTemp = modMain.GetCEDBRegistryEntry("UseReturnAddress", "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
						boolTemp = true;
						if (fecherFoundation.Strings.LCase(strTemp) == "false")
						{
							boolTemp = false;
						}
						if (lngID >= 0)
						{
							rptEnvelope10.InstancePtr.Init(ref SQLStatement, boolTemp, lngID);
						}
						break;
					}
				case CNSTREPORTTYPECUSTOMREPORT:
					{
						lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridCustomReports.TextMatrix(0, 0))));
						clsDRWrapper rsLoad = new clsDRWrapper();
						rsLoad.OpenRecordset("select * from customreports where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
						if (!rsLoad.EndOfFile())
						{
							strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("TableList"));
						}
						SQLStatement = GetReportSQL(strTemp);
						rptCECustomReport.InstancePtr.Init(lngID, ref SQLStatement, true);
						break;
					}
				case CNSTREPORTTYPECUSTOMFORMS:
					{
						lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(gridCustomForms.TextMatrix(0, 0))));
						strTemp = GetTableListFromCustomForm(ref lngID);
						SQLStatement = GetReportSQL(strTemp);
						clsCustomBill clsbill = new clsCustomBill();
						clsbill.DBFile = "TWCE0000.vb1";
						clsbill.DotMatrixFormat = false;
						clsbill.FormatID = lngID;
						clsbill.SQL = SQLStatement.SQLStatement;
						if (fecherFoundation.Strings.Trim(clsbill.SQL) == "" || fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(clsbill.SQL)) == "from")
						{
							clsbill.SQL = "select * from cemaster";
						}
						clsbill.ReportTitle = FCConvert.ToString(gridCustomForms.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 0, 0));
						rptCustomBill.InstancePtr.Init(clsbill, true);
						break;
					}
			}
			//end switch
			// Else
			// MsgBox "You have not selected a report type", vbExclamation, "No Report Selected"
			// End If
		}

		private string GetTableListFromCustomForm(ref int lngID)
		{
			string GetTableListFromCustomForm = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				bool boolPropTable = false;
				bool boolPermitTable = false;
				bool boolInspection = false;
				bool boolInspector = false;
				bool boolContractor = false;
				bool boolPermitContractors = false;
				bool boolPermitOpens;
				string strTemp;
				int x;
				strTemp = "";
				// Call rsLoad.OpenRecordset("select * from custombillfields where fieldid > 0 and  formatid = " & lngID & " order by fieldid", strCEDatabase)
				// Do While Not rsLoad.EndOfFile
				// Select Case rsLoad.Fields("fieldid")
				// Case 1 To 25, 41, 42, 43
				// boolPropTable = True
				// Case 26 To 33, 44
				// boolPermitTable = True
				// Case 40
				// boolPermitTable = True
				// boolPermitContractors = True
				// Case 34 To 36, 45
				// boolInspection = True
				// Case 37
				// boolInspection = True
				// boolInspector = True
				// Case 38, 39, 46
				// boolContractor = True
				// End Select
				// rsLoad.MoveNext
				// Loop
				FCCollection cdList = new FCCollection();
				clsDynamicDocumentTag tTag;
				GetCodeListFromCustomForm(lngID, ref cdList);
				if (!FCUtils.IsEmpty(cdList))
				{
					if (cdList.Count > 0)
					{
						for (x = 1; x <= cdList.Count; x++)
						{
							tTag = cdList[x];
							if (!(tTag == null))
							{
								int vbPorterVar = tTag.Code;
								if ((vbPorterVar >= 1 && vbPorterVar <= 25) || (vbPorterVar == 41) || (vbPorterVar == 42) || (vbPorterVar == 43))
								{
									boolPropTable = true;
								}
								else if (vbPorterVar >= 26 && vbPorterVar <= 33)
								{
									boolPermitTable = true;
								}
								else if (vbPorterVar == 44)
								{
									boolPermitOpens = true;
									boolPermitTable = true;
								}
								else if (vbPorterVar == 40)
								{
									boolPermitTable = true;
									boolPermitContractors = true;
								}
								else if ((vbPorterVar >= 34 && vbPorterVar <= 36) || (vbPorterVar == 45))
								{
									boolInspection = true;
								}
								else if (vbPorterVar == 37)
								{
									boolInspection = true;
									boolInspector = true;
								}
								else if ((vbPorterVar == 38) || (vbPorterVar == 39) || (vbPorterVar == 46))
								{
									boolContractor = true;
								}
							}
						}
						// x
					}
				}
				for (x = 1; x <= cdList.Count; x++)
				{
					cdList.Remove(1);
				}
				// x
				cdList = null;
				if (boolPropTable)
				{
					strTemp += "CEMaster,";
				}
				if (boolPermitTable)
				{
					strTemp += "Permits,";
				}
				if ((boolPermitTable && boolContractor) || boolPermitContractors)
				{
					strTemp += "PermitContractors,";
				}
				if (boolContractor)
				{
					strTemp += "Contractors,";
				}
				if (boolInspection)
				{
					strTemp += "Inspections,";
				}
				if (boolInspector)
				{
					strTemp += "Inspectors,";
				}
				if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GetTableListFromCustomForm = strTemp;
				return GetTableListFromCustomForm;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetTableListFromCustomForm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetTableListFromCustomForm;
		}
		// Private Function GetPropertySQL() As clsSQLStatement
		// Dim tSQL As New clsReportSQLCreator
		// Dim tList As New clsReportParameters
		// Dim boolTemp As Boolean
		// Dim lngCode As Long
		// Dim lngAutoID As Long
		// Dim lngSetupRow As Long
		// Dim lngRow As Long
		// Dim tSQLStatement As New clsSQLStatement
		//
		// For lngRow = 1 To GridProperty.Rows - 1
		// If Val(GridProperty.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT)) = 1 Then
		// boolTemp = False
		// Else
		// boolTemp = True
		// End If
		// lngSetupRow = GridProperty.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLDESC)
		// lngAutoID = Val(GridPropertySetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))
		// If lngAutoID = 0 Then
		// lngCode = lngSetupRow
		// Else
		// lngCode = 0
		// End If
		// Call tList.AddParameter(CNSTCODETYPEPROPERTY, lngCode, boolTemp, GridProperty.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE), GridProperty.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE), lngAutoID)
		// Next lngRow
		// If GridProperty.Rows < 2 Then
		// Call tList.AddParameter(CNSTCODETYPEPROPERTY, CNSTGRIDPROPERTYROWDELETED, False, "true", "true", 0)
		// End If
		// tSQL.ListOfParameters = tList
		// Dim strTemp As String
		// strTemp = tSQL.SQLStatement(tSQLStatement, "CEMaster")
		// Set GetPropertySQL = tSQLStatement
		// End Function
		private string GetParametersDescription()
		{
			string GetParametersDescription = "";
			int lngRow;
			string strReturn;
			string strSep;
			string strTemp = "";
			int lngCode;
			int lngAutoID;
			int lngSetupRow;
			string strTo = "";
			string strFrom = "";
			string strIs = "";
			int intCount;
			string strOr = "";
			strSep = "";
			strReturn = "";
			intCount = 0;
			for (lngRow = 1; lngRow <= GridParameters.Rows - 1; lngRow++)
			{
				if (intCount < 4)
				{
					strTemp = "";
					strIs = "Is ";
					strOr = "";
					if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT)) == 1)
					{
						strIs = "Is Not ";
					}
					strOr = GridParameters.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDPARAMETERSCOLOUTLINE) + " ";
					strTemp = strSep;
					if (fecherFoundation.Strings.Trim(strOr) == "")
					{
						strTemp += GridParameters.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDPARAMETERSCOLTABLE) + " ";
						strTemp += GridParameters.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDPARAMETERSCOLDESC) + " ";
					}
					else
					{
						strTemp += "or ";
					}
					strTemp += strIs;
					// strTemp = strTemp & GridParameters.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDPARAMETERSCOLTABLE) & " "
					// strTemp = strTemp & GridParameters.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, lngRow, CNSTGRIDPARAMETERSCOLDESC) & " "
					// strTemp = strTemp & strIs
					strTo = "";
					strFrom = "";
					strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE);
					strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE);
					if (fecherFoundation.Strings.Trim(strFrom) != fecherFoundation.Strings.Trim(strTo))
					{
						if (fecherFoundation.Strings.Trim(strFrom) != "")
						{
							if (fecherFoundation.Strings.Trim(strTo) != "")
							{
								strTemp += strFrom + " to " + strTo;
							}
							else
							{
								strTemp += strFrom;
							}
						}
						else
						{
							if (fecherFoundation.Strings.Trim(strTo) != "")
							{
								strTemp += "to " + strTo;
							}
							else
							{
							}
						}
					}
					else
					{
						if (fecherFoundation.Strings.Trim(strFrom) != "")
						{
							strTemp += strFrom;
						}
						else
						{
							strTemp = "";
						}
					}
					if (!(fecherFoundation.Strings.Trim(strTemp) == ""))
					{
						strTemp = fecherFoundation.Strings.Trim(strTemp);
						intCount += 1;
						strReturn += strTemp;
						strSep = "|";
					}
				}
				else
				{
					break;
				}
			}
			// lngRow
			GetParametersDescription = strReturn;
			return GetParametersDescription;
		}

		private clsSQLStatement GetReportSQL(string strDefaultTables)
		{
			clsSQLStatement GetReportSQL = null;
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			clsReportParameters tList = new clsReportParameters();
			clsReportOrderClause tOrdList = new clsReportOrderClause();
			bool boolTemp = false;
			int lngCode = 0;
			int lngAutoID = 0;
			int lngSetupRow = 0;
			int lngRow;
			clsSQLStatement tSQLStatement = new clsSQLStatement();
			string strTo = "";
			string strFrom = "";
			tSQLStatement.ParameterDescription = GetParametersDescription();
			for (lngRow = 1; lngRow <= GridParameters.Rows - 1; lngRow++)
			{
				if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT)) == 1)
				{
					boolTemp = false;
				}
				else
				{
					boolTemp = true;
				}
				lngSetupRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC))));
				if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContractorSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				else if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEINSPECTION)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridInspectionSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				else if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPERMIT)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPermitSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				else if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPROPERTY)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPropertySetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				if (lngAutoID == 0)
				{
					lngCode = lngSetupRow;
				}
				else
				{
					lngCode = 0;
				}
				strTo = "";
				strFrom = "";
				if (!FCUtils.IsEmpty(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)))
				{
                    //FC:FINAL:MSH - i.issue #1693: wrong comparing (result will be wrong if value equal null)
					//if (!(FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)) == ""))
					if (!string.IsNullOrEmpty(FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE))))
					{
						strTo = FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE));
					}
					else
					{
						strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE);
					}
				}
				else
				{
					strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE);
				}
				if (!FCUtils.IsEmpty(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)))
				{
                    //FC:FINAL:MSH - i.issue #1693: wrong comparing (result will be wrong if value equal null)
                    //if (!(FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)) == ""))
                    if (!string.IsNullOrEmpty(FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE))))
					{
						strFrom = FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE));
					}
					else
					{
						strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE);
					}
				}
				else
				{
					strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE);
				}
				tList.AddParameter(FCConvert.ToInt32(Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE))), ref lngCode, ref boolTemp, ref strTo, ref strFrom, ref lngAutoID, FCConvert.ToInt16(Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT))));
			}
			// lngRow
			// Call tList.AddParameter(CNSTCODETYPEPROPERTY, CNSTGRIDPROPERTYROWDELETED, False, "true", "true", 0)
			tSQL.ListOfParameters = tList;
			// now figure order by clause
			for (lngRow = 1; lngRow <= GridOrderBy.Rows - 1; lngRow++)
			{
				if (FCConvert.CBool(GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE)))
				{
					tOrdList.AddOrder(0, Conversion.Val(GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION)) == 1, GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC), GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME), ref lngRow);
				}
			}
			// lngRow
			tSQL.ListOfOrders = tOrdList;
			string strTemp;
			strTemp = tSQL.Get_SQLStatement(tSQLStatement, strDefaultTables);
			GetReportSQL = tSQLStatement;
			return GetReportSQL;
		}
		// Private Function GetInspectionSQL()
		// Dim tSQL As New clsReportSQLCreator
		// Dim tList As New clsReportParameters
		// Dim boolTemp As Boolean
		// Dim lngCode As Long
		// Dim lngAutoID As Long
		// Dim lngSetupRow As Long
		// Dim lngRow As Long
		// Dim tSQLStatement As New clsSQLStatement
		// Dim strTo As String
		// Dim strFrom As String
		//
		// For lngRow = 1 To GridInspection.Rows - 1
		// If Val(GridInspection.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT)) = 1 Then
		// boolTemp = False
		// Else
		// boolTemp = True
		// End If
		// lngSetupRow = GridInspection.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLDESC)
		// lngAutoID = Val(GridInspectionSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))
		// If lngAutoID = 0 Then
		// lngCode = lngSetupRow
		// Else
		// lngCode = 0
		// End If
		// strTo = ""
		// strFrom = ""
		// If Not IsEmpty(GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)) Then
		// If Not GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE) Is Nothing Then
		// strTo = GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)
		// Else
		// strTo = GridInspection.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)
		// End If
		// Else
		// strTo = GridInspection.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)
		// End If
		// If Not IsEmpty(GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)) Then
		// If Not GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE) Is Nothing Then
		// strFrom = GridInspection.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)
		// Else
		// strFrom = GridInspection.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)
		// End If
		// Else
		// strFrom = GridInspection.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)
		// End If
		//
		// Call tList.AddParameter(CNSTCODETYPEINSPECTION, lngCode, boolTemp, strTo, strFrom, lngAutoID)
		// Next lngRow
		// tSQL.ListOfParameters = tList
		// Dim strTemp As String
		// strTemp = tSQL.SQLStatement(tSQLStatement, "Inspections")
		// Set GetInspectionSQL = tSQLStatement
		// End Function
		private void SetupGridProperty()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsCodes = new clsDRWrapper();
				int lngRow = 0;
				string strTemp = "";
				string strList = "";
				GridPropertySetup.Rows = 7;
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWACCOUNT, CNSTGRIDSETUPCOLDESC, "Account");
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWOWNERLAST, CNSTGRIDSETUPCOLDESC, "Owner Last");
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWOWNERFIRST, CNSTGRIDSETUPCOLDESC, "Owner First");
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWMAPLOT, CNSTGRIDSETUPCOLDESC, "Map/Lot");
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWLOCATION, CNSTGRIDSETUPCOLDESC, "Location");
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWNEIGHBORHOOD, CNSTGRIDSETUPCOLDESC, "Neighborhood");
				GridPropertySetup.TextMatrix(CNSTGRIDPROPERTYROWZONE, CNSTGRIDSETUPCOLDESC, "Zone");
				strList = "";
				rsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				strList = "#" + FCConvert.ToString(CNSTGRIDPROPERTYROWOWNERLAST) + ";Owner Last|#" + FCConvert.ToString(CNSTGRIDPROPERTYROWOWNERFIRST) + ";Owner First|#" + FCConvert.ToString(CNSTGRIDPROPERTYROWMAPLOT) + ";Map/Lot|#" + FCConvert.ToString(CNSTGRIDPROPERTYROWACCOUNT) + ";Account|#" + FCConvert.ToString(CNSTGRIDPROPERTYROWLOCATION) + ";Location|#" + FCConvert.ToString(CNSTGRIDPROPERTYROWNEIGHBORHOOD) + ";Neighborhood|#" + FCConvert.ToString(CNSTGRIDPROPERTYROWZONE) + ";Zone|";
				while (!rsLoad.EndOfFile())
				{
					GridPropertySetup.Rows += 1;
					lngRow = GridPropertySetup.Rows - 1;
					strList += "#" + FCConvert.ToString(lngRow) + ";" + rsLoad.Get_Fields_String("description") + "|";
					GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
					GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("Description")));
					GridPropertySetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("Valueformat"))));
					if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
					{
						strTemp = "";
						rsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + rsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by code", modGlobalVariables.Statics.strCEDatabase);
						while (!rsCodes.EndOfFile())
						{
							strTemp += "#" + rsCodes.Get_Fields("code") + ";" + rsCodes.Get_Fields_String("description") + "|";
							rsCodes.MoveNext();
						}
						if (strTemp != string.Empty)
						{
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						}
						GridPropertySetup.RowData(lngRow, strTemp);
					}
					else if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
					{
						GridPropertySetup.RowData(lngRow, "Yes|No");
					}
					rsLoad.MoveNext();
				}
				strPropertyList = strList;
				// With GridProperty
				// .Rows = 1
				// .Cols = 6
				// .ColHidden(CNSTGRIDPARAMETERSCOLVALUEFORMAT) = True
				// .ColComboList(CNSTGRIDPARAMETERSCOLDESC) = strList
				// .ColComboList(CNSTGRIDPARAMETERSCOLNOT, "#0;Is|#1;Is Not"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLDESC) = "Description"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLNOT) = "Is/Not"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLFROMVALUE) = "From"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLTOVALUE) = "To"
				// End With
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridProperty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridParameters()
		{
			string strList;
			strList = "#" + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + ";Contractor|#" + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + ";Inspection|#" + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + ";Permit|#" + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + ";Property";
			GridParameters.Rows = 1;
			GridParameters.Cols = 7;
			GridParameters.ColHidden(CNSTGRIDPARAMETERSCOLVALUEFORMAT, true);
			GridParameters.ColComboList(CNSTGRIDPARAMETERSCOLNOT, "#0;Is|#1;Is Not");
			GridParameters.ColComboList(CNSTGRIDPARAMETERSCOLTABLE, strList);
			GridParameters.TextMatrix(0, CNSTGRIDPARAMETERSCOLNOT, "Is/Not");
			GridParameters.TextMatrix(0, CNSTGRIDPARAMETERSCOLDESC, "Description");
			GridParameters.TextMatrix(0, CNSTGRIDPARAMETERSCOLTABLE, "Type");
			GridParameters.TextMatrix(0, CNSTGRIDPARAMETERSCOLFROMVALUE, "From");
			GridParameters.TextMatrix(0, CNSTGRIDPARAMETERSCOLTOVALUE, "To");
		}

		private void SetupGridOrderBy()
		{
			GridOrderBy.Rows = 1;
			GridOrderBy.Cols = 4;
			GridOrderBy.ColHidden(CNSTGRIDORDERBYCOLFIELDNAME, true);
			GridOrderBy.ColComboList(CNSTGRIDORDERBYCOLDIRECTION, "#0;Ascending|#1;Descending");
			GridOrderBy.ColDataType(CNSTGRIDORDERBYCOLUSE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridOrderBy.TextMatrix(0, CNSTGRIDORDERBYCOLDIRECTION, "Order");
			GridOrderBy.TextMatrix(0, CNSTGRIDORDERBYCOLDESC, "Description");
		}
		// vbPorter upgrade warning: lngRpt As int	OnWrite(string, int, double)
		private void RefillOrderByGrid(int lngRpt)
		{
			// Dim lngRpt As Long
			int lngRow;
			//App.DoEvents();
			// lngRpt = GridReport.TextMatrix(0, 0)
			GridOrderBy.Rows = 1;
			gridCustomReports.Visible = false;
			gridCustomForms.Visible = false;
			switch (lngRpt)
			{
				case CNSTREPORTTYPECONTRACTORS:
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Number");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.contractornumber");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Name");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.name1");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Status");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.CONTRACTORstatus");
						break;
					}
				case CNSTREPORTTYPEINSPECTORS:
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspector");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "inspectors.name");
						break;
					}
				case CNSTREPORTTYPEPERMITS:
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permitidentifier,permityear,permitnumber");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Applicant");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "filedby");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Date Filed");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "applicationdate");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Type");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permittype");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Status");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "appdencode");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Location");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "originalstreet,originalstreetnum");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Fee");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "Fee");
						break;
					}
				case CNSTREPORTTYPEPROPERTY:
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Account");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "ceaccount");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Owner");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.last,cemaster.first,cemaster.middle,cemaster.desig");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Location");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.streetname,cemaster.streetnumber");
						break;
					}
				case CNSTREPORTTYPEPROPERTYLIST:
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Account");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "ceaccount");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Owner");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.last,cemaster.first,cemaster.middle,cemaster.desig");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Location");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.streetname,cemaster.streetnumber");
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Map/Lot");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.maplot");
						break;
					}
				case CNSTREPORTTYPEOPENPERMITS:
					{
						break;
					}
				case CNSTREPORTTYPECUSTOMREPORT:
					{
						gridCustomReports.Visible = true;
						FillOrderFromCustom();
						break;
					}
				case CNSTREPORTTYPECUSTOMFORMS:
					{
						gridCustomForms.Visible = true;
						FillOrderFromCustomForms();
						break;
					}
			}
			//end switch
		}

		private void FillOrderFromCustomForms()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				bool boolLastFirst;
				bool boolSecLastFirst;
				bool boolFirst;
				bool boolSecFirst;
				int x;
				boolLastFirst = false;
				boolSecLastFirst = false;
				boolFirst = false;
				boolSecFirst = false;
				FCCollection cdList = new FCCollection();
				clsDynamicDocumentTag tTag;
				GetCodeListFromCustomForm(FCConvert.ToInt32(Conversion.Val(gridCustomForms.TextMatrix(0, 0))), ref cdList);
				if (!FCUtils.IsEmpty(cdList))
				{
					if (cdList.Count > 0)
					{
						for (x = 1; x <= cdList.Count; x++)
						{
							tTag = cdList[x];
							if (!(tTag == null))
							{
								switch (tTag.Code)
								{
									case 1:
									case 3:
										{
											// first and first,last
											if (!boolFirst)
											{
												boolFirst = true;
												GridOrderBy.Rows += 1;
												lngRow = GridOrderBy.Rows - 1;
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Owner (First, Middle, Last)");
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.first,cemaster.middle,cemaster.last,cemaster.desig");
											}
											break;
										}
									case 2:
									case 7:
										{
											if (!boolSecFirst)
											{
												boolSecFirst = true;
												GridOrderBy.Rows += 1;
												lngRow = GridOrderBy.Rows - 1;
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Second Owner (First, Middle, Last)");
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.secfirst,cemaster.secmiddle,cemaster.seclast,cemaster.secdesig");
											}
											break;
										}
									case 5:
									case 41:
										{
											if (!boolLastFirst)
											{
												boolLastFirst = true;
												GridOrderBy.Rows += 1;
												lngRow = GridOrderBy.Rows - 1;
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Owner");
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.last,cemaster.first,cemaster.middle,cemaster.desig");
											}
											break;
										}
									case 9:
									case 42:
										{
											if (!boolSecLastFirst)
											{
												boolSecLastFirst = true;
												GridOrderBy.Rows += 1;
												lngRow = GridOrderBy.Rows - 1;
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Second Owner");
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
												GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.seclast,cemaster.secfirst,cemaster.secmiddle,cemaster.secdesig");
											}
											break;
										}
									case 17:
										{
											// account
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Account");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "ceaccount");
											break;
										}
									case 18:
										{
											// maplot
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Map/Lot");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.maplot");
											break;
										}
									case 21:
										{
											// location
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Location");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.streetname,cemaster.streetnumber");
											break;
										}
									case 24:
										{
											// neighborhood
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Neighborhood");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Neighborhood");
											break;
										}
									case 25:
										{
											// zone
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Zone");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Zone");
											break;
										}
									case 22:
										{
											// acreage
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Acreage");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Acres");
											break;
										}
									case 23:
										{
											// frontage
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Frontage");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Frontage");
											break;
										}
									case 33:
										{
											// permit fee
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Fee");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "fee");
											break;
										}
									case 26:
										{
											// permit year
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Year");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permityear");
											break;
										}
									case 27:
										{
											// permit
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permitidentifier,permityear,permitnumber");
											break;
										}
									case 28:
										{
											// application date
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Date Filed");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "applicationdate");
											break;
										}
									case 32:
										{
											// plan
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Plan");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "Plan");
											break;
										}
									case 29:
										{
											// permit type
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Type");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permittype");
											break;
										}
									case 30:
										{
											// status
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Status");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "appdencode");
											break;
										}
									case 40:
										{
											// permit contractor
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Contractor");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "PermitContractorName");
											break;
										}
									case 36:
										{
											// inspection date
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspection Date");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectionDate");
											break;
										}
									case 37:
										{
											// inspector
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspector");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectorName");
											break;
										}
									case 34:
										{
											// Inspection type
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspection Type");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectionType");
											break;
										}
									case 35:
										{
											// status
											// Inspection Status
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspection Status");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectionStatus");
											break;
										}
									case 38:
										{
											// contractor name
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Name");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.name1");
											break;
										}
									case 39:
										{
											// contractor name 2
											GridOrderBy.Rows += 1;
											lngRow = GridOrderBy.Rows - 1;
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Name 2");
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
											GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.name2");
											break;
										}
								}
								//end switch
							}
						}
						// x
					}
				}
				for (x = 1; x <= cdList.Count; x++)
				{
					cdList.Remove(1);
				}
				// x
				cdList = null;
				// Call rsLoad.OpenRecordset("select * from custombillfields where fieldid > 0 and formatid = " & Val(gridCustomForms.TextMatrix(0, 0)) & " order by fieldid", strCEDatabase)
				// Do While Not rsLoad.EndOfFile
				// Select Case rsLoad.Fields("fieldid")
				// Case 1, 3
				// first and first,last
				// If Not boolFirst Then
				// boolFirst = True
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Owner (First, Middle, Last)"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.first,cemaster.middle,cemaster.last,cemaster.desig"
				// End If
				// Case 2, 7
				// If Not boolSecFirst Then
				// boolSecFirst = True
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Second Owner (First, Middle, Last)"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.secfirst,cemaster.secmiddle,cemaster.seclast,cemaster.secdesig"
				// End If
				// Case 5, 41
				// If Not boolLastFirst Then
				// boolLastFirst = True
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Owner"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.last,cemaster.first,cemaster.middle,cemaster.desig"
				// End If
				// Case 9, 42
				// If Not boolSecLastFirst Then
				// boolSecLastFirst = True
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Second Owner"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.seclast,cemaster.secfirst,cemaster.secmiddle,cemaster.secdesig"
				// End If
				// Case 17
				// account
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Account"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "ceaccount"
				// Case 18
				// maplot
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Map/Lot"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.maplot"
				// Case 21
				// location
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Location"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.streetname,cemaster.streetnumber"
				// Case 24
				// neighborhood
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Neighborhood"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.Neighborhood"
				// Case 25
				// zone
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Zone"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.Zone"
				// Case 22
				// acreage
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Acreage"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.Acres"
				// Case 23
				// frontage
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Frontage"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "cemaster.Frontage"
				// Case 33
				// permit fee
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Permit Fee"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "fee"
				// Case 26
				// permit year
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Permit Year"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "permityear"
				// Case 27
				// permit
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Permit"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "permitidentifier,permityear,permitnumber"
				// Case 28
				// application date
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Date Filed"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "applicationdate"
				// Case 32
				// plan
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Plan"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "Plan"
				// Case 29
				// permit type
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Permit Type"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "permittype"
				// Case 30
				// status
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Permit Status"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "appdencode"
				// Case 40
				// permit contractor
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Permit Contractor"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "PermitContractorName"
				// Case 36
				// inspection date
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Inspection Date"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "InspectionDate"
				// Case 37
				// inspector
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Inspector"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "InspectorName"
				// Case 34
				// Inspection type
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Inspection Type"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "InspectionType"
				// Case 35
				// status
				// Inspection Status
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Inspection Status"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "InspectionStatus"
				// Case 38
				// contractor name
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Contractor Name"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "contractors.name1"
				// Case 39
				// contractor name 2
				// GridOrderBy.Rows = GridOrderBy.Rows + 1
				// lngRow = GridOrderBy.Rows - 1
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC) = "Contractor Name 2"
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION) = 0
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) = False
				// GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME) = "contractors.name2"
				// End Select
				// rsLoad.MoveNext
				// Loop
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "FillOrderFromCustomForms", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillOrderFromCustom()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				rsLoad.OpenRecordset("select * from customreportlayouts where reportid = " + FCConvert.ToString(Conversion.Val(gridCustomReports.TextMatrix(0, 0))) + " order by codetype,opencode", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 1)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Account");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "ceaccount");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 2)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Owner (First,Middle,Last)");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.first,cemaster.middle,cemaster.last,cemaster.desig");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 3)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Owner");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.last,cemaster.first,cemaster.middle,cemaster.desig");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 4)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Second Owner (First,Middle,Last)");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.secfirst,cemaster.secmiddle,cemaster.seclast,cemaster.secdesig");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 5)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Second Owner");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.seclast,cemaster.secfirst,cemaster.secmiddle,cemaster.secdesig");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 6)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Map/Lot");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.maplot");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 7)
					{
							}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 8)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Location");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.streetname,cemaster.streetnumber");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 9)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Neighborhood");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Neighborhood");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 10)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Zone");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Zone");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 11)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Acreage");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Acres");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 12)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Frontage");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "cemaster.Frontage");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 13)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Year");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permityear");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 14)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permitidentifier,permityear,permitnumber");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 15)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Date Filed");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "applicationdate");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 16)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Type");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "permittype");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 17)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Status");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "appdencode");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 18)
					{
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 19)
					{
						// plan
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Plan");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "Plan");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 20)
					{
						// Inspection type
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspection Type");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectionType");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 21)
					{
						// Inspection Status
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspection Status");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectionStatus");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 22)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspection Date");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectionDate");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 23)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Inspector");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "InspectorName");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 24)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Name");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.name1");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 25)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Name 2");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.name2");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 26)
					{
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 27)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Contractor Status");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "contractors.status");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 33)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Fee");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "fee");
					}
					else if (Conversion.Val(rsLoad.Get_Fields_Int32("codetype")) == 34)
					{
						GridOrderBy.Rows += 1;
						lngRow = GridOrderBy.Rows - 1;
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC, "Permit Contractor");
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION, FCConvert.ToString(0));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE, FCConvert.ToString(false));
						GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME, "PermitContractorName");
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillOrderFromCustom", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridOrderBy()
		{
			int GridWidth = 0;
			GridWidth = GridOrderBy.WidthOriginal;
			GridOrderBy.ColWidth(CNSTGRIDORDERBYCOLDESC, FCConvert.ToInt32(0.5 * GridWidth));
			GridOrderBy.ColWidth(CNSTGRIDORDERBYCOLUSE, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void SetupGridContractor()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsCodes = new clsDRWrapper();
				int lngRow = 0;
				string strTemp = "";
				string strList = "";
				GridContractorSetup.Rows = 5;
				GridContractorSetup.TextMatrix(CNSTGRIDCONTRACTORROWNAME, CNSTGRIDSETUPCOLDESC, "Name");
				GridContractorSetup.TextMatrix(CNSTGRIDCONTRACTORROWNAME2, CNSTGRIDSETUPCOLDESC, "Name 2");
				GridContractorSetup.TextMatrix(CNSTGRIDCONTRACTORROWLICENSE, CNSTGRIDSETUPCOLDESC, "License");
				GridContractorSetup.TextMatrix(CNSTGRIDCONTRACTORROWSTATUS, CNSTGRIDSETUPCOLDESC, "Status");
				GridContractorSetup.TextMatrix(CNSTGRIDCONTRACTORROWCLASS, CNSTGRIDSETUPCOLDESC, "Class");
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORCLASS) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				strTemp = "#0; |";
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridContractorSetup.RowData(CNSTGRIDCONTRACTORROWCLASS, strTemp);
				strTemp = "#0; |";
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORSTATUS) + " and code <> " + FCConvert.ToString(modCEConstants.CNSTCONTRACTORSTATUSWARNING) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORSTATUS) + " and code = " + FCConvert.ToString(modCEConstants.CNSTCONTRACTORSTATUSWARNING) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridContractorSetup.RowData(CNSTGRIDCONTRACTORROWSTATUS, strTemp);
				strList = "";
				strList = "#" + FCConvert.ToString(CNSTGRIDCONTRACTORROWNAME) + ";Name|#" + FCConvert.ToString(CNSTGRIDCONTRACTORROWNAME2) + ";Name 2|#" + FCConvert.ToString(CNSTGRIDCONTRACTORROWLICENSE) + ";License|#" + FCConvert.ToString(CNSTGRIDCONTRACTORROWSTATUS) + ";Status|#" + FCConvert.ToString(CNSTGRIDCONTRACTORROWCLASS) + ";Class|";
				rsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					GridContractorSetup.Rows += 1;
					lngRow = GridContractorSetup.Rows - 1;
					strList += "#" + FCConvert.ToString(lngRow) + ";" + rsLoad.Get_Fields_String("description") + "|";
					GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
					GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("Description")));
					GridContractorSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("Valueformat"))));
					if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
					{
						strTemp = "";
						rsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + rsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by code", modGlobalVariables.Statics.strCEDatabase);
						while (!rsCodes.EndOfFile())
						{
							strTemp += "#" + rsCodes.Get_Fields("code") + ";" + rsCodes.Get_Fields_String("description") + "|";
							rsCodes.MoveNext();
						}
						if (strTemp != string.Empty)
						{
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						}
						GridContractorSetup.RowData(lngRow, strTemp);
					}
					else if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
					{
						GridContractorSetup.RowData(lngRow, "Yes|No");
					}
					rsLoad.MoveNext();
				}
				strList = Strings.Mid(strList, 1, strList.Length - 1);
				strContractorList = strList;
				// With GridContractor
				// .Rows = 1
				// .Cols = 6
				// .ColHidden(CNSTGRIDPARAMETERSCOLVALUEFORMAT) = True
				// .ColComboList(CNSTGRIDPARAMETERSCOLDESC) = strList
				// .ColComboList(CNSTGRIDPARAMETERSCOLNOT, "#0;Is|#1;Is Not"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLDESC) = "Description"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLNOT) = "Is/Not"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLFROMVALUE) = "From"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLTOVALUE) = "To"
				// End With
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridContractor", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string InspectionSQL()
		{
			string InspectionSQL = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn;
				strReturn = "";
				InspectionSQL = "";
				strReturn = "";
				InspectionSQL = strReturn;
				return InspectionSQL;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InspectionSQL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return InspectionSQL;
		}

		private void SetupGridInspection()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsCodes = new clsDRWrapper();
				int lngRow = 0;
				string strTemp = "";
				string strList = "";
				GridInspectionSetup.Rows = 4;
				GridInspectionSetup.TextMatrix(CNSTGRIDINSPECTIONROWTYPE, CNSTGRIDSETUPCOLDESC, "Type");
				GridInspectionSetup.TextMatrix(CNSTGRIDINSPECTIONROWSTATUS, CNSTGRIDSETUPCOLDESC, "Status");
				GridInspectionSetup.TextMatrix(CNSTGRIDINSPECTIONROWDATE, CNSTGRIDSETUPCOLDESC, "Date");
				GridInspectionSetup.TextMatrix(CNSTGRIDINSPECTIONROWINSPECTOR, CNSTGRIDSETUPCOLDESC, "Inspector");
				GridInspectionSetup.TextMatrix(CNSTGRIDINSPECTIONROWDATE, CNSTGRIDSETUPCOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPEDATE));
				strTemp = "#0; |";
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONTYPE), modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + rsLoad.Get_Fields("code") + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				GridInspectionSetup.RowData(CNSTGRIDINSPECTIONROWTYPE, strTemp);
				// strtemp = "#0; |"
				strTemp = "";
				strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONPENDING) + ";Scheduled|#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONCOMPLETE) + ";Completed (Last Inspection)|";
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONSTATUS), modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + rsLoad.Get_Fields("code") + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				GridInspectionSetup.RowData(CNSTGRIDINSPECTIONROWSTATUS, strTemp);
				rsLoad.OpenRecordset("select * from inspectors order by NAME", modGlobalVariables.Statics.strCEDatabase);
				strTemp = "#-2; |#0;Unknown|";
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("name") + "|";
					rsLoad.MoveNext();
				}
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				GridInspectionSetup.RowData(CNSTGRIDINSPECTIONROWINSPECTOR, strTemp);
				strList = "#" + FCConvert.ToString(CNSTGRIDINSPECTIONROWTYPE) + ";Type|#" + FCConvert.ToString(CNSTGRIDINSPECTIONROWSTATUS) + ";Status|#" + FCConvert.ToString(CNSTGRIDINSPECTIONROWDATE) + ";Date|#" + FCConvert.ToString(CNSTGRIDINSPECTIONROWINSPECTOR) + ";Inspector|";
				rsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					GridInspectionSetup.Rows += 1;
					lngRow = GridInspectionSetup.Rows - 1;
					strList += "#" + FCConvert.ToString(lngRow) + ";" + rsLoad.Get_Fields_String("description") + "|";
					GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
					GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("Description")));
					GridInspectionSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("Valueformat"))));
					if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
					{
						strTemp = "";
						rsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + rsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " order by code", modGlobalVariables.Statics.strCEDatabase);
						while (!rsCodes.EndOfFile())
						{
							strTemp += "#" + rsCodes.Get_Fields("code") + ";" + rsCodes.Get_Fields_String("description") + "|";
							rsCodes.MoveNext();
						}
						if (strTemp != string.Empty)
						{
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						}
						GridInspectionSetup.RowData(lngRow, strTemp);
					}
					else if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
					{
						GridInspectionSetup.RowData(lngRow, "Yes|No");
					}
					rsLoad.MoveNext();
				}
				strInspectionList = strList;
				// With GridInspection
				// .Rows = 1
				// .Cols = 6
				// .ColHidden(CNSTGRIDPARAMETERSCOLVALUEFORMAT) = True
				// .ColComboList(CNSTGRIDPARAMETERSCOLDESC) = strList
				// .ColComboList(CNSTGRIDPARAMETERSCOLNOT, "#0;Is|#1;Is Not"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLDESC) = "Description"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLNOT) = "Is/Not"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLFROMVALUE) = "From"
				// .TextMatrix(0, CNSTGRIDPARAMETERSCOLTOVALUE) = "To"
				// End With
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridPermit()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsCodes = new clsDRWrapper();
				int lngRow = 0;
				string strTemp = "";
				string strList = "";
				GridPermitSetup.Rows = 8;
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWYEAR, CNSTGRIDSETUPCOLDESC, "Permit Year");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWIDENTIFIER, CNSTGRIDSETUPCOLDESC, "Identifier");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWAPPLYDATE, CNSTGRIDSETUPCOLDESC, "Application Date");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWTYPE, CNSTGRIDSETUPCOLDESC, "Type");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWCATEGORY, CNSTGRIDSETUPCOLDESC, "Category");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWFILER, CNSTGRIDSETUPCOLDESC, "Filer");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWSTATUS, CNSTGRIDSETUPCOLDESC, "Status");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWFEE, CNSTGRIDSETUPCOLDESC, "Fee");
				GridPermitSetup.TextMatrix(CNSTGRIDPERMITROWAPPLYDATE, CNSTGRIDSETUPCOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPEDATE));
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				strTemp = "";
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("code"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTPERMITCOMPLETE) + ";Complete";
				GridPermitSetup.RowData(CNSTGRIDPERMITROWSTATUS, strTemp);
				strTemp = "";
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				if (!(strTemp == string.Empty))
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					GridPermitSetup.RowData(CNSTGRIDPERMITROWTYPE, strTemp);
				}
				strTemp = "#0;None|";
				rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITCATEGORIES) + " order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))) + ";" + rsLoad.Get_Fields_String("description") + "|";
					rsLoad.MoveNext();
				}
				if (!(strTemp == string.Empty))
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					GridPermitSetup.RowData(CNSTGRIDPERMITROWCATEGORY, strTemp);
				}
				strList = "";
				rsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				strList = "#" + FCConvert.ToString(CNSTGRIDPERMITROWYEAR) + ";Permit Year|#" + FCConvert.ToString(CNSTGRIDPERMITROWIDENTIFIER) + ";Identifier|#" + FCConvert.ToString(CNSTGRIDPERMITROWAPPLYDATE) + ";Application Date|#" + FCConvert.ToString(CNSTGRIDPERMITROWTYPE) + ";Type|#" + FCConvert.ToString(CNSTGRIDPERMITROWCATEGORY) + ";Category|#" + FCConvert.ToString(CNSTGRIDPERMITROWFILER) + ";Filer|#" + FCConvert.ToString(CNSTGRIDPERMITROWSTATUS) + ";Status|#" + FCConvert.ToString(CNSTGRIDPERMITROWFEE) + ";Fee|";
				while (!rsLoad.EndOfFile())
				{
					GridPermitSetup.Rows += 1;
					lngRow = GridPermitSetup.Rows - 1;
					strList += "#" + FCConvert.ToString(lngRow) + ";" + rsLoad.Get_Fields_String("description") + "|";
					GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
					GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("Description")));
					GridPermitSetup.TextMatrix(lngRow, CNSTGRIDSETUPCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("Valueformat"))));
					if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
					{
						strTemp = "";
						rsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + rsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " order by code", modGlobalVariables.Statics.strCEDatabase);
						while (!rsCodes.EndOfFile())
						{
							strTemp += "#" + rsCodes.Get_Fields("code") + ";" + rsCodes.Get_Fields_String("description") + "|";
							rsCodes.MoveNext();
						}
						if (strTemp != string.Empty)
						{
							strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						}
						GridPermitSetup.RowData(lngRow, strTemp);
					}
					else if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
					{
						GridPermitSetup.RowData(lngRow, "Yes|No");
					}
					rsLoad.MoveNext();
				}
				strPermitList = strList;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridPermit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Private Sub GridProperty_RowColChange()
		// Dim Row As Long
		// Dim Col As Long
		//
		// Row = GridProperty.MouseRow
		// Col = GridProperty.MouseCol
		// If Row > 0 Then
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// GridProperty.Editable = flexEDKbdMouse
		// GridProperty.ComboList = GridProperty.RowData(Row)
		//
		// If Val(GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT)) = CNSTVALUETYPEDATE Then
		// GridProperty.EditMask = "00/00/0000"
		// Else
		// GridProperty.EditMask = ""
		// End If
		// End Select
		//
		// End If
		//
		// End Sub
		//
		// Private Sub GridProperty_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Row < 1 Then Exit Sub
		// Dim strTemp As String
		// Dim strAry() As String
		// Dim intTemp As Integer
		// intTemp = GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLDESC)
		// If Val(GridPropertySetup.TextMatrix(intTemp, CNSTGRIDSETUPCOLAUTOID)) = 0 Then
		// Exit Sub
		// End If
		// GridProperty.EditMask = ""
		// Select Case Col
		// Case CNSTGRIDPARAMETERSCOLFROMVALUE, CNSTGRIDPARAMETERSCOLTOVALUE
		// Select Case Val(GridProperty.TextMatrix(Row, CNSTGRIDPARAMETERSCOLVALUEFORMAT))
		// Case CNSTVALUETYPEDATE
		// strTemp = GridProperty.EditText
		// If IsDate(strTemp) Then
		// strTemp = Format(strTemp, "MM/dd/yyyy")
		// Else
		// strTemp = ""
		// End If
		// GridProperty.EditText = strTemp
		// Case CNSTVALUETYPEDECIMAL
		// strTemp = GridProperty.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strAry = Split(strTemp, ".", , vbTextCompare)
		// strTemp = Format(strAry(0), "#,###,###,##0")
		// If UBound(strAry) > 0 Then
		// strTemp = strTemp & "." & strAry(1)
		// End If
		// GridProperty.EditText = strTemp
		// End If
		// Case CNSTVALUETYPELONG
		// strTemp = GridProperty.EditText
		// If Trim(strTemp) <> vbNullString Then
		// strTemp = Trim(Replace(strTemp, ",", "", , , vbTextCompare))
		// strTemp = Val(strTemp)
		// strTemp = Format(strTemp, "#,###,###,###,##0")
		// GridProperty.EditText = strTemp
		// End If
		// Case CNSTVALUETYPEDROPDOWN
		// strTemp = GridProperty.ComboData
		// GridProperty.Cell(FCGrid.CellPropertySettings.flexcpData, Row, Col) = Val(strTemp)
		// End Select
		// Case CNSTGRIDOPENSCOLDATE
		// End Select
		// End Sub
		private void ResizeGrids()
		{
			int GridWidth = 0;
			GridWidth = GridParameters.WidthOriginal;
			GridParameters.ColWidth(CNSTGRIDPARAMETERSCOLOUTLINE, FCConvert.ToInt32(0.07 * GridWidth));
			GridParameters.ColWidth(CNSTGRIDPARAMETERSCOLTABLE, FCConvert.ToInt32(0.2 * GridWidth));
			GridParameters.ColWidth(CNSTGRIDPARAMETERSCOLDESC, FCConvert.ToInt32(0.3 * GridWidth));
			GridParameters.ColWidth(CNSTGRIDPARAMETERSCOLNOT, FCConvert.ToInt32(0.09 * GridWidth));
			GridParameters.ColWidth(CNSTGRIDPARAMETERSCOLFROMVALUE, FCConvert.ToInt32(0.19 * GridWidth));
			ResizeGridOrderBy();
			// With GridProperty
			// GridWidth = .Width
			// .ColWidth(CNSTGRIDPARAMETERSCOLDESC) = 0.4 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLNOT) = 0.12 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLFROMVALUE) = 0.22 * GridWidth
			// End With
			// With GridPermit
			// GridWidth = .Width
			// .ColWidth(CNSTGRIDPARAMETERSCOLDESC) = 0.4 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLNOT) = 0.12 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLFROMVALUE) = 0.22 * GridWidth
			// End With
			// With GridContractor
			// GridWidth = .Width
			// .ColWidth(CNSTGRIDPARAMETERSCOLDESC) = 0.4 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLNOT) = 0.12 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLFROMVALUE) = 0.22 * GridWidth
			// End With
			// With GridInspection
			// GridWidth = .Width
			// .ColWidth(CNSTGRIDPARAMETERSCOLDESC) = 0.4 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLNOT) = 0.12 * GridWidth
			// .ColWidth(CNSTGRIDPARAMETERSCOLFROMVALUE) = 0.22 * GridWidth
			// End With
		}

		private void mnuSaveParameters_Click(object sender, System.EventArgs e)
		{
			SaveParameters();
		}

		private void SaveParameters(bool boolSaveAs = false)
		{
			object strTitle = "";
			int lngID;
			string strTo = "";
			string strFrom = "";
			if (boolSaveAs)
			{
				GridParameters.Tag = (System.Object)(0);
			}
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Tag)));
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.OpenRecordset("select * from ParameterGroups where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
			if (!rsSave.EndOfFile())
			{
				rsSave.Execute("delete from SavedParameters where ParameterGroup = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
			}
			else
			{
				if (!frmInput.InstancePtr.Init(ref strTitle, "Description", "Enter Description", 2880))
					return;
				rsSave.AddNew();
				rsSave.Set_Fields("Description", strTitle);
				rsSave.Update();
				lngID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
				GridParameters.Tag = (System.Object)(lngID);
			}
			rsSave.OpenRecordset("select * from savedparameters where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
			int lngRow;
			int lngSetupRow = 0;
			int lngCode = 0;
			int lngAutoID = 0;
			for (lngRow = 1; lngRow <= GridParameters.Rows - 1; lngRow++)
			{
				rsSave.AddNew();
				rsSave.Set_Fields("ParameterOrder", lngRow);
				rsSave.Set_Fields("ParameterGroup", lngID);
				rsSave.Set_Fields("ReportType", FCConvert.ToString(Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE))));
				if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT)) != 1)
				{
					rsSave.Set_Fields("IsIsNot", true);
				}
				else
				{
					rsSave.Set_Fields("IsIsNot", false);
				}
				strTo = "";
				strFrom = "";
				if (!FCUtils.IsEmpty(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)))
				{
					if (!(FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)) == ""))
					{
						strTo = FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE));
						rsSave.Set_Fields("ToDesc", GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE));
					}
					else
					{
						strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE);
						rsSave.Set_Fields("ToDesc", "");
					}
				}
				else
				{
					strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE);
					rsSave.Set_Fields("ToDesc", "");
				}
				if (!FCUtils.IsEmpty(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)))
				{
					if (!(FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)) == ""))
					{
						strFrom = FCConvert.ToString(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE));
						rsSave.Set_Fields("FromDesc", GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE));
					}
					else
					{
						strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE);
						rsSave.Set_Fields("fromdesc", "");
					}
				}
				else
				{
					strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE);
					rsSave.Set_Fields("fromdesc", "");
				}
				rsSave.Set_Fields("FromValue", strFrom);
				rsSave.Set_Fields("ToValue", strTo);
				lngSetupRow = FCConvert.ToInt32(Math.Round(Conversion.Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC))));
				if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridContractorSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				else if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEINSPECTION)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridInspectionSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				else if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPERMIT)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPermitSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				else if (Conversion.Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)) == modCEConstants.CNSTCODETYPEPROPERTY)
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPropertySetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))));
				}
				if (lngAutoID == 0)
				{
					lngCode = lngSetupRow;
				}
				else
				{
					lngCode = 0;
				}
				rsSave.Set_Fields("ParameterType", lngAutoID);
				rsSave.Set_Fields("parameterrow", lngCode);
				rsSave.Update();
			}
			// lngRow
			MessageBox.Show("Parameters saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void LoadParameters()
		{
			int lngID;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strSQL;
			string strReturn = "";
			int lngRow = 0;
			string strList;
			int lngParmRow = 0;
			int lngParmID;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			strSQL = "select * from ParameterGroups order by description";
			rsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsLoad.EndOfFile())
			{
				MessageBox.Show("No saved parameters found", "No Parameters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			strList = "";
			while (!rsLoad.EndOfFile())
			{
				strList += rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			strList = Strings.Mid(strList, 1, strList.Length - 1);
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(frmListChoiceWithIDs.InstancePtr.Init(ref strList, "Load Parameters"))));
			if (lngID == 0)
				return;
			rsLoad.OpenRecordset("select * from savedparameters where parametergroup = " + FCConvert.ToString(lngID) + " order by parameterorder", modGlobalVariables.Statics.strCEDatabase);
			if (rsLoad.EndOfFile())
			{
				MessageBox.Show("No parameters found", "No Parameters", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			GridParameters.Rows = 1;
			GridParameters.Tag = (System.Object)(lngID);
			while (!rsLoad.EndOfFile())
			{
				GridParameters.Rows += 1;
				lngRow = GridParameters.Rows - 1;
				GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE, FCConvert.ToString(rsLoad.Get_Fields("reporttype")));
				lngParmRow = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("parameterrow"))));
				lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("parametertype"))));
				if (lngParmRow == 0)
				{
					switch (FCConvert.ToInt32(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)))
					{
						case modCEConstants.CNSTCODETYPECONTRACTOR:
							{
								for (x = 0; x <= (GridContractorSetup.Rows - 1); x++)
								{
									if (Conversion.Val(GridContractorSetup.TextMatrix(x, CNSTGRIDSETUPCOLAUTOID)) == lngID)
									{
										lngParmRow = x;
										break;
									}
								}
								// x
								break;
							}
						case modCEConstants.CNSTCODETYPEPERMIT:
							{
								for (x = 0; x <= (GridPermitSetup.Rows - 1); x++)
								{
									if (Conversion.Val(GridPermitSetup.TextMatrix(x, CNSTGRIDSETUPCOLAUTOID)) == lngID)
									{
										lngParmRow = x;
										break;
									}
								}
								// x
								break;
							}
						case modCEConstants.CNSTCODETYPEINSPECTION:
							{
								for (x = 0; x <= (GridInspectionSetup.Rows - 1); x++)
								{
									if (Conversion.Val(GridInspectionSetup.TextMatrix(x, CNSTGRIDSETUPCOLAUTOID)) == lngID)
									{
										lngParmRow = x;
										break;
									}
								}
								// x
								break;
							}
						case modCEConstants.CNSTCODETYPEPROPERTY:
							{
								for (x = 0; x <= (GridPropertySetup.Rows - 1); x++)
								{
									if (Conversion.Val(GridPropertySetup.TextMatrix(x, CNSTGRIDSETUPCOLAUTOID)) == lngID)
									{
										lngParmRow = x;
										break;
									}
								}
								// x
								break;
							}
					}
					//end switch
				}
				// If lngParmRow > 0 Then
				switch (FCConvert.ToInt32(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)))
				{
					case modCEConstants.CNSTCODETYPECONTRACTOR:
						{
							GridParameters.RowData(lngRow, GridContractorSetup.RowData(lngParmRow));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridContractorSetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLDESC, GridContractorSetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLDESC));
							break;
						}
					case modCEConstants.CNSTCODETYPEPERMIT:
						{
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridPermitSetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLDESC, GridPermitSetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLDESC));
							GridParameters.RowData(lngRow, GridPermitSetup.RowData(lngParmRow));
							break;
						}
					case modCEConstants.CNSTCODETYPEINSPECTION:
						{
							GridParameters.RowData(lngRow, GridInspectionSetup.RowData(lngParmRow));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridInspectionSetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLDESC, GridInspectionSetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLDESC));
							break;
						}
					case modCEConstants.CNSTCODETYPEPROPERTY:
						{
							GridParameters.RowData(lngRow, GridPropertySetup.RowData(lngParmRow));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT, GridPropertySetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLVALUEFORMAT));
							GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLDESC, GridPropertySetup.TextMatrix(lngParmRow, CNSTGRIDSETUPCOLDESC));
							break;
						}
				}
				//end switch
				if (FCConvert.ToString(GridParameters.RowData(lngRow)) != string.Empty)
				{
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE, FCConvert.ToString(rsLoad.Get_Fields_String("todesc")));
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE, FCConvert.ToString(rsLoad.Get_Fields_String("fromdesc")));
					GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC, lngParmRow);
					GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE, rsLoad.Get_Fields_String("FROMVALUE"));
					GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE, rsLoad.Get_Fields_String("tovalue"));
				}
				else
				{
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE, FCConvert.ToString(rsLoad.Get_Fields_String("tovalue")));
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE, FCConvert.ToString(rsLoad.Get_Fields_String("fromvalue")));
					// GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC) = ""
					GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC, lngParmRow);
				}
				if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("isisnot")))
				{
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT, FCConvert.ToString(0));
				}
				else
				{
					GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT, FCConvert.ToString(1));
				}
				// Else
				// End If
				rsLoad.MoveNext();
			}
            modColorScheme.ColorGrid(GridParameters);
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(int, double)
		private void GetCodeListFromCustomForm(int lngID, ref FCCollection cdList)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int x;
				clsDynamicDocumentTag tTag;
				string strTemp = "";
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from custombillfields where (fieldid = -4 or fieldid > 0) and formatid = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					if (rsLoad.Get_Fields("fieldid") > 0)
					{
						if (cdList.Count > 0)
						{
							tTag = GetCodeByCode("Code" + Conversion.Val(rsLoad.Get_Fields("fieldid")) + "ID" + Conversion.Val(rsLoad.Get_Fields_Int32("openid")), ref cdList);
							if (tTag == null)
							{
								tTag = new clsDynamicDocumentTag();
								tTag.Code = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("fieldid"))));
								tTag.MiscID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
								cdList.Add(tTag, "Code" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fieldid"))) + "ID" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
							}
						}
						else
						{
							tTag = new clsDynamicDocumentTag();
							tTag.Code = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("fieldid"))));
							tTag.MiscID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
							cdList.Add(tTag, "Code" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fieldid"))) + "ID" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
						}
					}
					else
					{
						rsTemp.OpenRecordset("select * from dynamicreports where ID = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("usertext"))), modGlobalVariables.Statics.strCEDatabase);
						if (!rsTemp.EndOfFile())
						{
							strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("text"));
							if (strTemp != "")
							{
								ParseDDTextForCodes(ref strTemp, ref cdList);
							}
						}
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCodeListFromCustomForm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public clsDynamicDocumentTag GetCodeByCode(string strTag, ref FCCollection cdList)
		{
			clsDynamicDocumentTag GetCodeByCode = null;
			clsDynamicDocumentTag tDDT;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				tDDT = null;
				if (!FCUtils.IsEmpty(cdList))
				{
					if (cdList.Count > 0)
					{
						tDDT = (clsDynamicDocumentTag)cdList[strTag];
					}
				}
				GetCodeByCode = tDDT;
				return GetCodeByCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				tDDT = null;
				GetCodeByCode = tDDT;
			}
			return GetCodeByCode;
		}

		private void ParseDDTextForCodes(ref string strText, ref FCCollection cdList)
		{
			try
			{
				string strTemp = "";
				// this is a temporary sting that will be used to store and transfer string segments
				int lngNextVariable;
				// this is the position of the beginning of the next variable (0 = no more variables)
				int lngEndOfLastVariable;
				// this is the position of the end of the last variable
				string strTag = "";
				int lngMiscID;
				int lngCode = 0;
				clsDynamicDocumentTag tTag;
				if (ddDefault == null)
				{
					ddDefault = new clsDynamicDocument();
					ddDefault.SetupTags();
				}
				lngMiscID = 0;
				// ParseDynamicDocumentString = ""
				lngEndOfLastVariable = 0;
				lngNextVariable = 1;
				if (strText.Length < 1)
					return;
				// priming read
				if (Strings.InStr(1, strText, "<", CompareConstants.vbBinaryCompare) > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strText, "<", CompareConstants.vbBinaryCompare);
					// do until there are no more variables left
					do
					{
						// set the end pointer
						if (Strings.InStr(lngNextVariable, strText, ">", CompareConstants.vbBinaryCompare) > 0)
						{
							lngEndOfLastVariable = Strings.InStr(lngNextVariable, strText, ">", CompareConstants.vbBinaryCompare);
							// replace the variable
							strTag = Strings.Mid(strText, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1);
							if (fecherFoundation.Strings.UCase(strTag) == "DATE")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "TIME")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "YEAR")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "BUSNAME")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "CRLF")
							{
							}
							else
							{
								tTag = ddDefault.GetTagByTag(ref strTag);
								if (!(tTag == null))
								{
									lngCode = tTag.Code;
									lngMiscID = tTag.MiscID;
									// If intReportType = CNSTDYNAMICREPORTTYPECUSTOMBILL Then
									// lngCode = GetCodeFromDynamicTag(strTag, intReportType, lngMiscID)
									tTag = GetCodeByCode("Code" + lngCode + "ID" + lngMiscID, ref cdList);
									if (tTag == null)
									{
										tTag = new clsDynamicDocumentTag();
										tTag.Code = lngCode;
										tTag.MiscID = lngMiscID;
										cdList.Add(tTag, "Code" + FCConvert.ToString(lngCode) + "ID" + FCConvert.ToString(lngMiscID));
									}
									// Else
									// lngCode = GetCodeFromDynamicTag(strTag, intReportType, lngMiscID)
									// strBuildString = strBuildString & GetDataByCode(lngCode, lngMiscID)
									// End If
								}
							}
							// check for another variable
							lngNextVariable = Strings.InStr(lngEndOfLastVariable, strText, "<", CompareConstants.vbBinaryCompare);
						}
						else
						{
							lngNextVariable = 0;
						}
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				// If lngEndOfLastVariable < Len(strText) Then
				// strBuildString = strBuildString & Mid(strOriginal, lngEndOfLastVariable + 1)
				// End If
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ParseDDTextForCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}
	}
}
