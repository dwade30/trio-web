﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for srptPermitActivity.
	/// </summary>
	partial class srptPermitActivity
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptPermitActivity));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPermit = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPermitType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtActivityDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDetail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPermit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPermitType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActivityDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtActivityDate,
				this.txtType,
				this.txtDescription,
				this.txtDetail
			});
			this.Detail.Height = 0.46875F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.txtPermit,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label9,
				this.Label10,
				this.txtPermitType,
				this.txtDateFiled
			});
			this.GroupHeader1.Height = 0.4895833F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPageIncludeNoDetail;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.9375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Permit";
			this.Label2.Top = 0F;
			this.Label2.Width = 0.625F;
			// 
			// txtPermit
			// 
			this.txtPermit.Height = 0.1875F;
			this.txtPermit.Left = 1.5F;
			this.txtPermit.Name = "txtPermit";
			this.txtPermit.Text = null;
			this.txtPermit.Top = 0F;
			this.txtPermit.Width = 1.4375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.5F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Date";
			this.Label3.Top = 0.25F;
			this.Label3.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Type";
			this.Label4.Top = 0.25F;
			this.Label4.Width = 0.8125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 3.958333F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Description";
			this.Label5.Top = 0.25F;
			this.Label5.Width = 1.375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1770833F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.1875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "Date Filed";
			this.Label9.Top = 0F;
			this.Label9.Width = 0.8125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1770833F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Type";
			this.Label10.Top = 0F;
			this.Label10.Width = 0.4791667F;
			// 
			// txtPermitType
			// 
			this.txtPermitType.Height = 0.1770833F;
			this.txtPermitType.Left = 5.75F;
			this.txtPermitType.Name = "txtPermitType";
			this.txtPermitType.Text = null;
			this.txtPermitType.Top = 0F;
			this.txtPermitType.Width = 1.729167F;
			// 
			// txtDateFiled
			// 
			this.txtDateFiled.Height = 0.1875F;
			this.txtDateFiled.Left = 4F;
			this.txtDateFiled.Name = "txtDateFiled";
			this.txtDateFiled.Text = null;
			this.txtDateFiled.Top = 0F;
			this.txtDateFiled.Width = 1.125F;
			// 
			// txtActivityDate
			// 
			this.txtActivityDate.Height = 0.1666667F;
			this.txtActivityDate.Left = 1.5F;
			this.txtActivityDate.Name = "txtActivityDate";
			this.txtActivityDate.Style = "text-align: left";
			this.txtActivityDate.Text = null;
			this.txtActivityDate.Top = 0F;
			this.txtActivityDate.Width = 0.8645833F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1666667F;
			this.txtType.Left = 2.375F;
			this.txtType.Name = "txtType";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 1.552083F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1666667F;
			this.txtDescription.Left = 3.958333F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 3.46875F;
			// 
			// txtDetail
			// 
			this.txtDetail.CanShrink = true;
			this.txtDetail.Height = 0.1666667F;
			this.txtDetail.Left = 1.5F;
			this.txtDetail.Name = "txtDetail";
			this.txtDetail.Text = null;
			this.txtDetail.Top = 0.2083333F;
			this.txtDetail.Visible = false;
			this.txtDetail.Width = 5.916667F;
			// 
			// srptPermitActivity
			//
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReports_DataInitialize);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.458333F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPermit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPermitType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActivityDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActivityDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDetail;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPermit;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPermitType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFiled;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
