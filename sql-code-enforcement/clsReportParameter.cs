﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWCE0000
{
	public class clsReportParameter
	{
		//=========================================================
		private bool boolIs;
		private int lngType;
		private int lngCode;
		private string strFrom = string.Empty;
		private string strTo = string.Empty;
		private int lngAutoID;
		private int lngGroup;
		private bool boolInGroup;
		private bool boolUnused;
		private int intDBType;

		public int DBType
		{
			set
			{
				intDBType = value;
			}
			get
			{
				int DBType = 0;
				DBType = intDBType;
				return DBType;
			}
		}

		public bool IsTrue
		{
			set
			{
				boolIs = value;
			}
			get
			{
				bool IsTrue = false;
				IsTrue = boolIs;
				return IsTrue;
			}
		}

		public bool Unused
		{
			set
			{
				boolUnused = value;
			}
			get
			{
				bool Unused = false;
				Unused = boolUnused;
				return Unused;
			}
		}

		public int ID
		{
			set
			{
				lngAutoID = value;
			}
			get
			{
				int ID = 0;
				ID = lngAutoID;
				return ID;
			}
		}

		public string ToParameter
		{
			set
			{
				strTo = value;
			}
			get
			{
				string ToParameter = "";
				ToParameter = strTo;
				return ToParameter;
			}
		}

		public string FromParameter
		{
			set
			{
				strFrom = value;
			}
			get
			{
				string FromParameter = "";
				FromParameter = strFrom;
				return FromParameter;
			}
		}

		public int CodeType
		{
			set
			{
				lngType = value;
			}
			get
			{
				int CodeType = 0;
				CodeType = lngType;
				return CodeType;
			}
		}

		public int Code
		{
			set
			{
				lngCode = value;
			}
			get
			{
				int Code = 0;
				Code = lngCode;
				return Code;
			}
		}

		public int ParameterGroup
		{
			set
			{
				lngGroup = value;
			}
			get
			{
				int ParameterGroup = 0;
				ParameterGroup = lngGroup;
				return ParameterGroup;
			}
		}

		public bool InGroup
		{
			set
			{
				boolInGroup = value;
			}
			get
			{
				bool InGroup = false;
				InGroup = boolInGroup;
				return InGroup;
			}
		}
	}
}
