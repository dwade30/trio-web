//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWCE0000
{
	public class clsCustomBill
	{
		//=========================================================
		// This is set up so that you can have as many classes
		// as you want per module.  You do not have to handle
		// all custom bill/reports the same way
		// You can have multiple classes, or just one class
		// that alters its behaviour based on a variable
		// that determines which type you are doing.
		// The properties and functions are set up to be used
		// like recordsets from the report.  They do not have
		// to actually work this way.  EndOfFile could return
		// clsreport.endoffile, or it could return whether an
		// index is pointing past the end of an array or grid
		// Any functions or variables can be changed or added
		// as long as the interface that the report expects
		// is unchanged. I.E. add as much as you want or change an existing function that
		// the report calls, but don't rename or delete it.
		private string strSQLReport = string.Empty;
		private clsDRWrapper clsReport = new clsDRWrapper();
		private int lngCurrentFormatID;
		private string strDisplayTitle = string.Empty;
		private string strPrinterName = string.Empty;
		private string strThisModule = string.Empty;
		private string strThisDB = string.Empty;
		private string strDataDB = string.Empty;
		// Private clsCustomCodes As New clsdrwrapper
		private clsCustomPrintForm clsCustomCodes = new clsCustomPrintForm();
		private clsDynamicDocument ddDefault;
		private int lngVerticalAlignment;
		private bool boolUsePrinterFonts;
		private bool boolDotMatrix;
		private clsDRWrapper rsUserCodes;
		private clsDRWrapper rsSetup;
		private string[] aryReturnAddress = new string[4 + 1];

		public bool DotMatrixFormat
		{
			set
			{
				boolDotMatrix = value;
			}
			get
			{
				bool DotMatrixFormat = false;
				DotMatrixFormat = boolDotMatrix;
				return DotMatrixFormat;
			}
		}

		public bool UsePrinterFonts
		{
			set
			{
				boolUsePrinterFonts = value;
			}
			get
			{
				bool UsePrinterFonts = false;
				UsePrinterFonts = boolUsePrinterFonts;
				return UsePrinterFonts;
			}
		}

		public int VerticalAlignment
		{
			set
			{
				// adjust up or down by lngtwips
				lngVerticalAlignment = value;
			}
			get
			{
				int VerticalAlignment = 0;
				VerticalAlignment = lngVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public string Module
		{
			set
			{
				// I.E. BL or UT etc.
				strThisModule = value;
			}
			get
			{
				string Module = "";
				Module = strThisModule;
				return Module;
			}
		}

		public string DBFile
		{
			set
			{
				// The filename.  Usually TW[Mod abbreviation]0000.vb1
				strThisDB = value;
			}
			get
			{
				string DBFile = "";
				DBFile = strThisDB;
				return DBFile;
			}
		}

		public string DataDBFile
		{
			set
			{
				// The filename of the db that has the data
				strDataDB = value;
			}
			get
			{
				string DataDBFile = "";
                //FC:FINAL:MSH - i.issue #1703: strDataDB can be equal to a null (not only to an empty string)
                //if (strDataDB != string.Empty)
                if (!string.IsNullOrEmpty(strDataDB))
				{
					DataDBFile = strDataDB;
				}
				else
				{
					DataDBFile = strThisDB;
				}
				return DataDBFile;
			}
		}

		public string SQL
		{
			set
			{
				// The sql statement.  This is not accessed directly
				// by the report since the programmer might use an array of types or a grid
				// instead of a recordset
				strSQLReport = value;
			}
			get
			{
				string SQL = "";
				SQL = strSQLReport;
				return SQL;
			}
		}

		public int FormatID
		{
			set
			{
				// The ID of the report
				// Should be set before the report is run
				lngCurrentFormatID = value;
			}
			get
			{
				int FormatID = 0;
				FormatID = lngCurrentFormatID;
				return FormatID;
			}
		}

		public string ReportTitle
		{
			set
			{
				// What you want displayed if the report is previewed
				strDisplayTitle = value;
			}
			get
			{
				string ReportTitle = "";
				ReportTitle = strDisplayTitle;
				return ReportTitle;
			}
		}

		public string PrinterName
		{
			set
			{
				// If you need a specific printer and don't want the user to
				// be able to choose one, fill this in before calling the report
				strPrinterName = value;
			}
			get
			{
				string PrinterName = "";
				PrinterName = strPrinterName;
				return PrinterName;
			}
		}

		public bool EndOfFile
		{
			get
			{
				bool EndOfFile = false;
				// end of file, or more generally, end of the data
				EndOfFile = clsReport.EndOfFile();
				return EndOfFile;
			}
		}

		public bool BeginningOfFile
		{
			get
			{
				bool BeginningOfFile = false;
				BeginningOfFile = clsReport.BeginningOfFile();
				return BeginningOfFile;
			}
		}

		public void MoveNext()
		{
			clsReport.MoveNext();
		}

		public void MoveLast()
		{
			clsReport.MoveLast();
		}

		public void MovePrevious()
		{
			// most likely not used
			clsReport.MovePrevious();
		}

		public void MoveFirst()
		{
			clsReport.MoveFirst();
		}

		public bool LoadData()
		{
			bool LoadData = false;
			// this function returns false if it fails
			// It loads the data.  In the example class, this loads a recordset
			// the function calls all assume a recordset, but the programmer can have movenext
			// add 1 to an index to an array.  The class controls how the data is loaded and read
			// The report will use it like a recordset, but the implementation is irrelevant
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadData = false;
				// load the data
				clsReport.OpenRecordset("select * from customize", modGlobalVariables.Statics.strCEDatabase);
				if (!clsReport.EndOfFile())
				{
					int x;
					for (x = 1; x <= 4; x++)
					{
						aryReturnAddress[x - 1] = FCConvert.ToString(clsReport.Get_Fields_Boolean("returnaddress" + FCConvert.ToString(x)));
					}
					// x
				}
				clsReport.OpenRecordset(strSQLReport, DataDBFile);
				// load the codes
				// Call clsCustomCodes.OpenRecordset("select * from custombillcodes order by fieldid", strThisDB)
				clsCustomCodes.LoadControlTypes();
				LoadData = true;
				return LoadData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In clsCustomBill.LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadData;
		}
		// Public Sub LoadForSub(ByRef aRecSet As DAO.Recordset)
		// On Error GoTo ErrorHandler
		// Set clsReport = aRecSet
		// clsCustomCodes.LoadControlTypes
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In clsCustomBill.LoadForSub", vbCritical, "Error"
		// End Sub
		public string GetDataByCode(int lngCode, int lngFieldID, string strExtraParameters = "")
		{
			string GetDataByCode = "";
			// the code corresponds to the data in the custombillcodes table
			bool boolSpecialCase;
			GetDataByCode = "";
			// Dim CurCode As CustomBillCodeType
			clsCustomControlParameters tcp;
			if (lngCode < 0)
			{
				tcp = new clsCustomControlParameters();
				tcp.Category = "";
				tcp.CategoryNumber = 0;
				tcp.TypeOfData = 0;
				tcp.DBName = "";
				// .FieldID = lngCode
				tcp.Code = lngCode;
				tcp.FieldName = "";
				tcp.FormatString = "";
				tcp.SpecialCase = true;
				tcp.TableName = "";
				// .ExtraParameters = strExtraParameters
				// With CurCode
				// .Category = 0
				// .Datatype = 0
				// .DBName = ""
				// .FieldID = lngCode
				// .FieldName = ""
				// .FormatString = ""
				// .SpecialCase = True
				// .TableName = ""
				// End With
				GetDataByCode = HandleSpecialCase(tcp, strExtraParameters);
				// GetDataByCode = HandleSpecialCase(clsReport, CurCode, strExtraParameters)
			}
			else
			{
				tcp = clsCustomCodes.GetControlByCode(lngCode, lngFieldID);
				if (!(tcp == null))
				{
					// If clsCustomCodes.FindFirstRecord("FieldID", lngCode) Then
					boolSpecialCase = tcp.SpecialCase;
					// boolSpecialCase = clsCustomCodes.Fields("SpecialCase")
					// With CurCode
					// .Category = Val(clsCustomCodes.Fields("Category"))
					// .Datatype = Val(clsCustomCodes.Fields("datatype"))
					// .DBName = clsCustomCodes.Fields("dbname")
					// .FieldID = lngCode
					// .FieldName = clsCustomCodes.Fields("FieldName")
					// .FormatString = clsCustomCodes.Fields("FormatString")
					// .SpecialCase = boolSpecialCase
					// .TableName = clsCustomCodes.Fields("tablename")
					// End With
					if (!tcp.UserDefined)
					{
						if (boolSpecialCase == true)
						{
							// it is a special case and can't be simply loaded straight from the database
							GetDataByCode = FCConvert.ToString(HandleSpecialCase(tcp, strExtraParameters));
						}
						else
						{
							// is a simple code that just takes the data
							GetDataByCode = FCConvert.ToString(HandleRegularCase(ref tcp));
						}
					}
					else
					{
						GetDataByCode = FCConvert.ToString(HandleUserDefined(ref tcp));
					}
				}
			}
			return GetDataByCode;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object HandleRegularCase(ref clsCustomControlParameters tcp)
		{
			object HandleRegularCase = null;
			string strTemp;
			// Takes a code number and looks up the data based on the field name in the database
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				HandleRegularCase = "";
				strTemp = FCConvert.ToString(clsReport.Get_Fields(tcp.FieldName));
				switch ((DataTypeEnum)tcp.TypeOfData)
				{
					case DataTypeEnum.dbDate:
						{
                            //FC:FINAL:MSH - i.issue #1703: replace wrong comparing
							//if (FCConvert.ToInt32(clsReport.Get_Fields(tcp.FieldName)) == 0)
							if (clsReport.Get_Fields_DateTime(tcp.FieldName).ToOADate() == 0)
							{
								strTemp = "";
							}
							else
							{
                                //FC:FINAL:MSH - i.issue #1703: replace wrong comparing (can be equal not only to an empty string)
								//if (tcp.FormatString != string.Empty)
								if (!string.IsNullOrEmpty(tcp.FormatString))
								{
									strTemp = Strings.Format(strTemp, tcp.FormatString);
								}
								else
								{
									strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
								}
							}
							break;
						}
					default:
						{
                            //FC:FINAL:MSH - i.issue #1703: replace wrong comparing (can be equal not only to an empty string)
                            //if (fecherFoundation.Strings.Trim(tcp.FormatString) != string.Empty)
                            if (!string.IsNullOrEmpty(fecherFoundation.Strings.Trim(tcp.FormatString)))
							{
								strTemp = Strings.Format(strTemp, tcp.FormatString);
							}
							break;
						}
				}
				//end switch
				HandleRegularCase = strTemp;
				return HandleRegularCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In HandleRegularCase with code " + tcp.Code, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleRegularCase;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		private object HandleUserDefined(ref clsCustomControlParameters tcp)
		{
			object HandleUserDefined = null;
			string strTemp;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strTemp = "";
				HandleUserDefined = "";
				switch (tcp.Code)
				{
					case 43:
						{
							// property user defined
							strTemp = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Int32("ceaccount")))), modCEConstants.CNSTCODETYPEPROPERTY, tcp.FieldID);
							break;
						}
					case 44:
						{
							// permit user defined
							strTemp = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("permitautoid")))), modCEConstants.CNSTCODETYPEPERMIT, tcp.FieldID);
							break;
						}
					case 45:
						{
							// inspection user defined
							strTemp = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("inspectionautoid")))), modCEConstants.CNSTCODETYPEINSPECTION, tcp.FieldID);
							break;
						}
					case 46:
						{
							// contractor user defined
							strTemp = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsReport.Get_Fields("contractorautoid")))), modCEConstants.CNSTCODETYPECONTRACTOR, tcp.FieldID);
							break;
						}
				}
				//end switch
				HandleUserDefined = strTemp;
				return HandleUserDefined;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In HandleUserDefined with code " + tcp.Code, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleUserDefined;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		private string HandleSpecialCase(clsCustomControlParameters tcp, string strExtraParameters)
		{
			string HandleSpecialCase = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strTemp = "";
				string strTemp2 = "";
				int intTemp;
				HandleSpecialCase = "";
				int vbPorterVar = tcp.Code;
				if (vbPorterVar == modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE)
				{
					// barcode
					HandleSpecialCase = "R00002004";
				}
				else if (vbPorterVar == 1)
				{
					// owner full name
					strTemp = "";
					strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(clsReport.Get_Fields("first") + " " + clsReport.Get_Fields("middle")) + " " + clsReport.Get_Fields("last") + " " + clsReport.Get_Fields_String("desig"));
					HandleSpecialCase = fecherFoundation.Strings.Trim(strTemp);
				}
				else if (vbPorterVar == 2)
				{
					// second owner full name
					strTemp = "";
					strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(clsReport.Get_Fields("secfirst") + " " + clsReport.Get_Fields("secmiddle")) + " " + clsReport.Get_Fields("seclast") + " " + clsReport.Get_Fields("secdesig"));
					HandleSpecialCase = fecherFoundation.Strings.Trim(strTemp);
				}
				else if (vbPorterVar == 11)
				{
					// mailing address
					strTemp = "";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("address1"))) != string.Empty)
						strTemp += fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("address1"))) + "\r\n";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("address2"))) != string.Empty)
						strTemp += fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("address2"))) + "\r\n";
					strTemp += fecherFoundation.Strings.Trim(clsReport.Get_Fields_String("city") + " " + clsReport.Get_Fields("state") + " " + clsReport.Get_Fields_String("zip") + " " + clsReport.Get_Fields_String("zip4")) + "\r\n";
					HandleSpecialCase = fecherFoundation.Strings.Trim(strTemp);
				}
				else if (vbPorterVar == 16)
				{
					// zip and zip ext
					strTemp = "";
					strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("zip")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("zip4"))) != "")
					{
						strTemp += " " + fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("zip4")));
					}
					HandleSpecialCase = fecherFoundation.Strings.Trim(strTemp);
				}
				else if (vbPorterVar == 27)
				{
					// permit
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields_String("permitidentifier"))) != "")
					{
						HandleSpecialCase = clsReport.Get_Fields_String("permitidentifier");
					}
					else
					{
						HandleSpecialCase = clsReport.Get_Fields_Int32("permityear") + "-" + clsReport.Get_Fields("permitnumber");
					}
				}
				else if (vbPorterVar == 29)
				{
					// permit type
					if (rsSetup == null)
					{
						rsSetup = new clsDRWrapper();
						rsSetup.OpenRecordset("select * from SYSTEMCODES order by codetype,code", modGlobalVariables.Statics.strCEDatabase);
					}
					if (rsSetup.FindFirstRecord("ID", Conversion.Val(clsReport.Get_Fields("permittype"))))
					{
						HandleSpecialCase = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSetup.Get_Fields_String("description")));
					}
				}
				else if (vbPorterVar == 51)
				{
					// permit category
					if (rsSetup == null)
					{
						rsSetup = new clsDRWrapper();
						rsSetup.OpenRecordset("select * from SYSTEMCODES order by codetype,code", modGlobalVariables.Statics.strCEDatabase);
					}
					if (Conversion.Val(clsReport.Get_Fields_Int32("permitsubtype")) == 0)
					{
						HandleSpecialCase = "None";
					}
					else
					{
						if (rsSetup.FindFirstRecord("ID", Conversion.Val(clsReport.Get_Fields_Int32("permitsubtype"))))
						{
							HandleSpecialCase = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSetup.Get_Fields_String("description")));
						}
						else
						{
							HandleSpecialCase = "Unknown";
						}
					}
				}
				else if (vbPorterVar == 56)
				{
					// permit comm/res code
					if (Conversion.Val(clsReport.Get_Fields_Int32("CommResCode")) == 1)
					{
						HandleSpecialCase = "Commercial";
					}
					else
					{
						HandleSpecialCase = "Residential";
					}
				}
				else if (vbPorterVar == 30)
				{
					// permit status
					if (Conversion.Val(clsReport.Get_Fields_Int32("appdencode")) > 0)
					{
						if (rsSetup == null)
						{
							rsSetup = new clsDRWrapper();
							rsSetup.OpenRecordset("select * from SYSTEMCODES order by codetype,code", modGlobalVariables.Statics.strCEDatabase);
						}
						// If rsSetup.FindFirstRecord(, , "codetype = " & CNSTCODETYPEAPPROVALDENIAL & " and code = " & Val(clsReport.Fields("appdencode"))) Then
						if (rsSetup.FindFirstRecord2("CodeType,Code", FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + "," + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Int32("appdencode"))), ","))
						{
							HandleSpecialCase = fecherFoundation.Strings.Trim(FCConvert.ToString(rsSetup.Get_Fields_String("description")));
						}
					}
					else
					{
						if (Conversion.Val(clsReport.Get_Fields_Int32("appdencode")) == modCEConstants.CNSTPERMITCOMPLETE)
						{
							HandleSpecialCase = "Complete";
						}
					}
				}
				else if (vbPorterVar == 34)
				{
					// inspection type
					if (rsSetup == null)
					{
						rsSetup = new clsDRWrapper();
						rsSetup.OpenRecordset("select * from SYSTEMCODES order by codetype,code", modGlobalVariables.Statics.strCEDatabase);
					}
					// If rsSetup.FindFirstRecord(, , "codetype = " & CNSTCODETYPEINSPECTIONTYPE & " and code = " & Val(clsReport.Fields("InspectionType"))) Then
					if (rsSetup.FindFirstRecord2("CodeType,code", FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONTYPE) + "," + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Int32("InspectionType"))), ","))
					{
						HandleSpecialCase = rsSetup.Get_Fields_String("description");
					}
				}
				else if (vbPorterVar == 35)
				{
					// inspection status
					if (rsSetup == null)
					{
						rsSetup = new clsDRWrapper();
						rsSetup.OpenRecordset("select * from SYSTEMCODES order by codetype,code", modGlobalVariables.Statics.strCEDatabase);
					}
					if (Conversion.Val(clsReport.Get_Fields_Int32("InspectionStatus")) == modCEConstants.CNSTINSPECTIONPENDING)
					{
						HandleSpecialCase = "Pending";
					}
					else if (Conversion.Val(clsReport.Get_Fields_Int32("InspectionStatus")) == modCEConstants.CNSTINSPECTIONCOMPLETE)
					{
						HandleSpecialCase = "Complete";
					}
					else if (Conversion.Val(clsReport.Get_Fields_Int32("InspectionStatus")) == modCEConstants.CNSTINSPECTIONVIOLATION)
					{
						HandleSpecialCase = "Violation";
					}
					else
					{
						// If rsSetup.FindFirstRecord("codetype = " & CNSTCODETYPEINSPECTIONSTATUS & " and code = " & Val(clsReport.Fields("inspectionstatus"))) Then
						if (rsSetup.FindFirstRecord2("CodeType,code", FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONSTATUS) + "," + FCConvert.ToString(Conversion.Val(clsReport.Get_Fields_Int32("inspectionstatus"))), ","))
						{
							HandleSpecialCase = rsSetup.Get_Fields_String("description");
						}
					}
				}
				else if (vbPorterVar == 41)
				{
					// owner last,first
					strTemp = "";
					strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(clsReport.Get_Fields("first") + " " + clsReport.Get_Fields("middle")) + " " + clsReport.Get_Fields("last") + " " + clsReport.Get_Fields_String("desig"));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields("last"))) != "")
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields("first"))) != "")
						{
							strTemp = fecherFoundation.Strings.Trim(clsReport.Get_Fields("last") + ", " + clsReport.Get_Fields("first") + " " + clsReport.Get_Fields("middle") + clsReport.Get_Fields_String("desig"));
						}
						else
						{
							strTemp = FCConvert.ToString(clsReport.Get_Fields("Last"));
						}
					}
					else
					{
						strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields("first")));
					}
					HandleSpecialCase = fecherFoundation.Strings.Trim(strTemp);
				}
				else if (vbPorterVar == 42)
				{
					// 2nd owner last,first
					strTemp = "";
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields("Seclast"))) != "")
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsReport.Get_Fields("secfirst"))) != "")
						{
							strTemp = fecherFoundation.Strings.Trim(clsReport.Get_Fields("seclast") + ", " + clsReport.Get_Fields("secfirst") + " " + clsReport.Get_Fields("secmiddle") + clsReport.Get_Fields("secdesig"));
						}
						else
						{
							strTemp = FCConvert.ToString(clsReport.Get_Fields("seclast"));
						}
					}
					else
					{
						strTemp = FCConvert.ToString(clsReport.Get_Fields("secfirst"));
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 50)
				{
					// return address
					strTemp = "";
					strTemp2 = "";
					for (intTemp = 0; intTemp <= 3; intTemp++)
					{
						if (fecherFoundation.Strings.Trim(aryReturnAddress[intTemp]) != string.Empty)
						{
							strTemp += strTemp2 + aryReturnAddress[intTemp];
							strTemp2 = "\r\n";
						}
					}
					// intTemp
					HandleSpecialCase = fecherFoundation.Strings.Trim(strTemp);
				}
				return HandleSpecialCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In HandleSpecialCase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleSpecialCase;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int CentimetersToTwips(ref double dblCentimeters)
		{
			int CentimetersToTwips = 0;
			// returns twips
			CentimetersToTwips = FCConvert.ToInt32(dblCentimeters * 567);
			return CentimetersToTwips;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int InchesToTwips(ref double dblInches)
		{
			int InchesToTwips = 0;
			// returns twips
			InchesToTwips = FCConvert.ToInt32(dblInches * 1440);
			return InchesToTwips;
		}

		public double TwipsToInches(ref int lngTwips)
		{
			double TwipsToInches = 0;
			TwipsToInches = lngTwips / 1440.0;
			return TwipsToInches;
		}

		public double TwipsToCentimeters(ref int lngTwips)
		{
			double TwipsToCentimeters = 0;
			TwipsToCentimeters = lngTwips / 567.0;
			return TwipsToCentimeters;
		}

		public clsCustomBill() : base()
		{
			rsSetup = null;
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(string)
		private string GetValueForUserCode(int lngID, int lngType, int lngField)
		{
			string GetValueForUserCode = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsCode = new clsDRWrapper();
			string strTemp;
			strTemp = "";
			if (rsUserCodes == null)
			{
				rsUserCodes = new clsDRWrapper();
				rsUserCodes.OpenRecordset("select * from usercodes order by ID", modGlobalVariables.Statics.strCEDatabase);
			}
			rsTemp.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(lngID) + " and codetype = " + FCConvert.ToString(lngType) + " and fieldid = " + FCConvert.ToString(lngField), modGlobalVariables.Statics.strCEDatabase);
			if (!rsTemp.EndOfFile())
			{
				if (rsUserCodes.FindFirstRecord("ID", lngField))
				{
					if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEBOOLEAN)
					{
						if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("booleanvalue")))
						{
							strTemp = "Yes";
						}
						else
						{
							strTemp = "No";
						}
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDATE)
					{
						if (Information.IsDate(rsTemp.Get_Fields("datevalue")))
						{
							if (Convert.ToDateTime(rsTemp.Get_Fields_DateTime("datevalue")).ToOADate() != 0)
							{
								strTemp = FCConvert.ToString(rsTemp.Get_Fields_DateTime("datevalue"));
							}
							else
							{
								strTemp = "";
							}
						}
						else
						{
							strTemp = "";
						}
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL)
					{
						strTemp = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Double("numericvalue")));
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDROPDOWN)
					{
						rsCode.OpenRecordset("select * from codedescriptions where code = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("dropdownvalue"))) + " and fieldid = " + FCConvert.ToString(lngField), modGlobalVariables.Statics.strCEDatabase);
						if (!rsCode.EndOfFile())
						{
							strTemp = FCConvert.ToString(rsCode.Get_Fields_String("description"));
						}
						else
						{
							strTemp = "";
						}
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
					{
						strTemp = Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Double("numericvalue"))), "#,###,###,##0");
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPETEXT)
					{
						strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("textvalue"));
					}
				}
			}
			GetValueForUserCode = strTemp;
			return GetValueForUserCode;
		}

		public string ParseDynamicDocumentString(string strModDB, string strOriginal, int intReportType)
		{
			string ParseDynamicDocumentString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			string strTag = "";
			int lngMiscID;
			int lngCode = 0;
			try
			{
				if (ddDefault == null)
				{
					ddDefault = new clsDynamicDocument();
					ddDefault.SetupTags();
				}
				lngMiscID = 0;
				ParseDynamicDocumentString = "";
				lngEndOfLastVariable = 0;
				lngNextVariable = 1;
				strBuildString = "";
				if (strOriginal.Length < 1)
					return ParseDynamicDocumentString;
				// priming read
				if (Strings.InStr(1, strOriginal, "<", CompareConstants.vbBinaryCompare) > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<", CompareConstants.vbBinaryCompare);
					// do until there are no more variables left
					do
					{
						// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
						strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
						// set the end pointer
						if (Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare) > 0)
						{
							lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare);
							// replace the variable
							strTag = Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1);
							if (fecherFoundation.Strings.UCase(strTag) == "DATE")
							{
								strBuildString += Strings.Format(DateTime.Today, "MM/dd/yyyy");
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "TIME")
							{
								strBuildString += Strings.Format(DateTime.Now, "hh:mm tt");
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "YEAR")
							{
								strBuildString += FCConvert.ToString(DateTime.Today.Year);
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "BUSNAME")
							{
								strBuildString += modGlobalConstants.Statics.MuniName;
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "CRLF")
							{
								// ignore
							}
							else
							{
								clsDynamicDocumentTag tTag;
								tTag = ddDefault.GetTagByTag(ref strTag);
								if (!(tTag == null))
								{
									lngCode = tTag.Code;
									lngMiscID = tTag.MiscID;
									// If intReportType = CNSTDYNAMICREPORTTYPECUSTOMBILL Then
									// lngCode = GetCodeFromDynamicTag(strTag, intReportType, lngMiscID)
									strBuildString += GetDataByCode(lngCode, lngMiscID);
									// Else
									// lngCode = GetCodeFromDynamicTag(strTag, intReportType, lngMiscID)
									// strBuildString = strBuildString & GetDataByCode(lngCode, lngMiscID)
									// End If
								}
							}
							// check for another variable
							lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<", CompareConstants.vbBinaryCompare);
						}
						else
						{
							lngNextVariable = 0;
						}
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				if (lngEndOfLastVariable < strOriginal.Length)
				{
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
				}
				ParseDynamicDocumentString = strBuildString;
				return ParseDynamicDocumentString;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ParseDynamicDocumentString", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return ParseDynamicDocumentString;
			}
		}

		public string EmailTag()
		{
			string EmailTag = "";
			EmailTag = "";
			return EmailTag;
		}
	}
}
