﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWCE0000
{
	public class clsDynamicDocumentTag
	{
		//=========================================================
		private string strTag = string.Empty;
		private int lngCode;
		private string strName = string.Empty;
		private string strDescription = string.Empty;
		private string strCategory = string.Empty;
		private int lngCategoryNO;
		private int lngReportType;
		private int lngMiscID;
		private string strBaseTag = "";

		public string BaseTag
		{
			get
			{
				string BaseTag = "";
				BaseTag = strBaseTag;
				return BaseTag;
			}
		}

		public string Tag
		{
			set
			{
				strTag = value;
				string strTemp;
				strTemp = Strings.Replace(strTag, "<", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, ">", "", 1, -1, CompareConstants.vbTextCompare);
				// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
				int intTemp;
				intTemp = Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare);
				if (intTemp < 1)
				{
					strBaseTag = strTemp;
				}
				else
				{
					strBaseTag = Strings.Mid(strTemp, 1, intTemp - 1);
				}
			}
			get
			{
				string Tag = "";
				Tag = strTag;
				return Tag;
			}
		}

		public int Code
		{
			set
			{
				lngCode = value;
			}
			get
			{
				int Code = 0;
				Code = lngCode;
				return Code;
			}
		}

		public string TagName
		{
			set
			{
				strName = value;
			}
			get
			{
				string TagName = "";
				TagName = strName;
				return TagName;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Category
		{
			set
			{
				strCategory = value;
			}
			get
			{
				string Category = "";
				Category = strCategory;
				return Category;
			}
		}

		public int CategoryNO
		{
			set
			{
				lngCategoryNO = value;
			}
			get
			{
				int CategoryNO = 0;
				CategoryNO = lngCategoryNO;
				return CategoryNO;
			}
		}

		public int MiscID
		{
			set
			{
				lngMiscID = value;
			}
			get
			{
				int MiscID = 0;
				MiscID = lngMiscID;
				return MiscID;
			}
		}
	}
}
