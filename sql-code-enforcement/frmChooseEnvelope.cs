﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmChooseEnvelope : BaseForm
	{
		public frmChooseEnvelope()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmChooseEnvelope InstancePtr
		{
			get
			{
				return (frmChooseEnvelope)Sys.GetInstance(typeof(frmChooseEnvelope));
			}
		}

		protected frmChooseEnvelope _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: intEnvelopeType As int	OnWriteFCConvert.ToInt32(
		int intEnvelopeType;
		// Private Const CNSTEnvelope10 As Integer = 0
		// Private Const CNSTEnvelopeB5 As Integer = 1
		// Private Const CNSTEnvelopeC5 As Integer = 2
		// Private Const CNSTEnvelopeDL As Integer = 3
		// Private Const CNSTEnvelopeMonarch As Integer = 4
		private void FillInfo()
		{
			string strTemp;
			strTemp = "";
			if (cmbEnvelopeType.SelectedIndex >= 0)
			{
				switch (cmbEnvelopeType.ItemData(cmbEnvelopeType.SelectedIndex))
				{
					case modCEConstants.CNSTEnvelope10:
						{
							strTemp = "4.13 in. X 9.5 in." + "\r\n" + "10.48 cm X 24.13 cm";
							break;
						}
					case modCEConstants.CNSTEnvelopeB5:
						{
							strTemp = "6.93 in. X 9.84 in." + "\r\n" + "17.6 cm X 25 cm";
							break;
						}
					case modCEConstants.CNSTEnvelopeC5:
						{
							strTemp = "6.38 in. X 9.02 in." + "\r\n" + "16.2 cm X 22.9 cm";
							break;
						}
					case modCEConstants.CNSTEnvelopeDL:
						{
							strTemp = "4.33 in. X 8.66 in." + "\r\n" + "11 cm X 22 cm";
							break;
						}
					case modCEConstants.CNSTEnvelopeMonarch:
						{
							strTemp = "3.88 in. X 7.5 in." + "\r\n" + "9.84 cm X 19.05 cm";
							break;
						}
				}
				//end switch
			}
			lblInfo.Text = strTemp;
		}

		private void cmbEnvelopeType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			FillInfo();
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (cmbEnvelopeType.SelectedIndex >= 0)
			{
				intEnvelopeType = cmbEnvelopeType.ItemData(cmbEnvelopeType.SelectedIndex);
				modMain.SaveCEDBRegistryEntry("EnvelopeType", FCConvert.ToString(intEnvelopeType), "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				if (chkReturnAddress.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					modMain.SaveCEDBRegistryEntry("UseReturnAddress", "False", "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				}
				else
				{
					modMain.SaveCEDBRegistryEntry("UseReturnAddress", "True", "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
				}
				Close();
			}
		}

		private void frmChooseEnvelope_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmChooseEnvelope_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseEnvelope properties;
			//frmChooseEnvelope.FillStyle	= 0;
			//frmChooseEnvelope.ScaleWidth	= 3885;
			//frmChooseEnvelope.ScaleHeight	= 2235;
			//frmChooseEnvelope.LinkTopic	= "Form2";
			//frmChooseEnvelope.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			SetupCombo();
		}

		private void SetupCombo()
		{
			cmbEnvelopeType.Clear();
			cmbEnvelopeType.AddItem("#10");
			cmbEnvelopeType.ItemData(cmbEnvelopeType.NewIndex, modCEConstants.CNSTEnvelope10);
			cmbEnvelopeType.AddItem("B5");
			cmbEnvelopeType.ItemData(cmbEnvelopeType.NewIndex, modCEConstants.CNSTEnvelopeB5);
			cmbEnvelopeType.AddItem("C5");
			cmbEnvelopeType.ItemData(cmbEnvelopeType.NewIndex, modCEConstants.CNSTEnvelopeC5);
			cmbEnvelopeType.AddItem("DL");
			cmbEnvelopeType.ItemData(cmbEnvelopeType.NewIndex, modCEConstants.CNSTEnvelopeDL);
			cmbEnvelopeType.AddItem("Monarch");
			cmbEnvelopeType.ItemData(cmbEnvelopeType.NewIndex, modCEConstants.CNSTEnvelopeMonarch);
			cmbEnvelopeType.SelectedIndex = 0;
			string strTemp;
			int x;
			strTemp = modMain.GetCEDBRegistryEntry("EnvelopeType", "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			if (Conversion.Val(strTemp) >= 0)
			{
				for (x = 0; x <= cmbEnvelopeType.Items.Count - 1; x++)
				{
					if (cmbEnvelopeType.ItemData(x) == Conversion.Val(strTemp))
					{
						cmbEnvelopeType.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			strTemp = modMain.GetCEDBRegistryEntry("UseReturnAddress", "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			if (strTemp != "")
			{
				if (fecherFoundation.Strings.LCase(fecherFoundation.Strings.Trim(strTemp)) == "false")
				{
					chkReturnAddress.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				else
				{
					chkReturnAddress.CheckState = Wisej.Web.CheckState.Checked;
				}
			}
			FillInfo();
		}

		public int Init()
		{
			int Init = 0;
			intEnvelopeType = -1;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = intEnvelopeType;
			return Init;
		}
	}
}
