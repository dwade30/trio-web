//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Wisej.Web.Ext.FullCalendar;
using System.Linq;
using TWSharedLibrary;

namespace TWCE0000
{
	public partial class frmEditEvent : BaseForm
	{
		public frmEditEvent()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

			//FC:FINAL:DDU:#1903 - allow only numerics
			this.dtpStart.AllowOnlyNumericInput();
			this.dtpEnd.AllowOnlyNumericInput();
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditEvent InstancePtr
		{
			get
			{
				return (frmEditEvent)Sys.GetInstance(typeof(frmEditEvent));
			}
		}

		protected frmEditEvent _InstancePtr = null;
		//=========================================================
		[DllImport("user32", EntryPoint = "SendMessageA")]
		private static extern int SendMessage(int hwnd, int wMsg, int wParam, int lParam);

		const int CB_SETDROPPEDWIDTH = 0x160;
		Event m_pEditingEvent = new Event();
		bool m_bAddEvent;
		int SelectCol;
		int DescriptionCol;
		bool m_bNewEvent;

		private void btnRecurrence_Click(object sender, System.EventArgs e)
		{
			UpdateEventFromControls();
			//frmEditRecurrence.InstancePtr.m_pMasterEvent = m_pEditingEvent.CloneEvent;
			frmEditRecurrence.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			bool bRecurrenceStateChanged;
			//bRecurrenceStateChanged = m_pEditingEvent.RecurrenceState!=frmEditRecurrence.InstancePtr.m_pMasterEvent.RecurrenceState;
			m_pEditingEvent = frmEditRecurrence.InstancePtr.m_pMasterEvent;
			//if (frmEditRecurrence.InstancePtr.m_bUpdateFromEvent || bRecurrenceStateChanged) 
			//{
			//UpdateControlsFromEvent();
			//}
		}

		private void chkAllDayEvent_CheckedChanged(object sender, System.EventArgs e)
		{
			dtpStart.Visible = (chkAllDayEvent.CheckState == Wisej.Web.CheckState.Checked ? false : true);
			dtpEnd.Visible = (chkAllDayEvent.CheckState == Wisej.Web.CheckState.Checked ? false : true);
		}

		public void chkAllDayEvent_Click()
		{
			chkAllDayEvent_CheckedChanged(chkAllDayEvent, new System.EventArgs());
		}

		private void chkPublic_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			if (chkPublic.CheckState == Wisej.Web.CheckState.Checked)
			{
				// If m_bAddEvent Then
				// For counter = 0 To vsInstallers.Rows - 1
				// If vsInstallers.TextMatrix(counter, DescriptionCol) = clsSecurityClass.Get_UsersUserID Then
				// vsInstallers.TextMatrix(counter, SelectCol) = True
				// Else
				// vsInstallers.TextMatrix(counter, SelectCol) = False
				// End If
				// Next
				// fraInstallers.Enabled = False
				// End If
				// Else
				// If m_bAddEvent Then
				// fraInstallers.Enabled = True
				// End If
			}
		}

		private void chkReminder_CheckedChanged(object sender, System.EventArgs e)
		{
			cmbReminder.Enabled = (chkReminder.CheckState == Wisej.Web.CheckState.Checked ? true : false);
			cmbReminder.BackColor = ColorTranslator.FromOle((chkReminder.CheckState == Wisej.Web.CheckState.Checked ? Information.RGB(255, 255, 255) : Information.RGB(210, 210, 210)));
		}

		public void chkReminder_Click()
		{
			chkReminder_CheckedChanged(chkReminder, new System.EventArgs());
		}
        // Private Sub cmbEndTime_Click()
        // Dim Index As Long
        // Index = InStr(1, cmbEndTime.Text, "(")
        // If Index > 0 Then
        // cmbEndTime.Text = Left(cmbEndTime.Text, Index - 2)
        // End If
        // End Sub
        private void cmbLabel_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            //FC:TODO
            CalendarLabel pLabel = new CalendarLabel();
            int nLabelID;
            nLabelID = cmbLabel.ItemData(cmbLabel.SelectedIndex);
            pLabel = frmCalendar.InstancePtr.CalendarControl.LabelList.Where(label => label.LabelID == nLabelID).FirstOrDefault();
            if (!(pLabel == null))
            {
                txtColor.BackColor = pLabel.Color;
            }
        }
		// Private Sub cmbStartTime_LostFocus()
		// UpdateEndTimeCombo
		// End Sub
		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}
		// vbPorter upgrade warning: DatePart As string	OnRead(DateTime)
		// vbPorter upgrade warning: TimePart As string	OnRead(DateTime)
		// vbPorter upgrade warning: 'Return' As DateTime	OnWriteFCConvert.ToDouble(
		public DateTime DateFromString(ref string DatePart, ref string TimePart)
		{
			DateTime DateFromString = System.DateTime.Now;
			// vbPorter upgrade warning: dtDatePart As DateTime	OnWrite(string)
			// vbPorter upgrade warning: dtTimePart As DateTime	OnWrite(string)
			DateTime dtDatePart, dtTimePart;
			dtDatePart = FCConvert.ToDateTime(DatePart);
			dtTimePart = FCConvert.ToDateTime(TimePart);
			DateFromString = DateTime.FromOADate(dtDatePart.ToOADate() + dtTimePart.ToOADate());
			return DateFromString;
		}
		// vbPorter upgrade warning: DatePart As string	OnRead(DateTime)
		public bool IsDateValid(string DatePart)
		{
			bool IsDateValid = false;
			IsDateValid = false;
			try
			{
				// On Error GoTo Error
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
				DateTime dtDate;
				dtDate = FCConvert.ToDateTime(DatePart);
				IsDateValid = true;
			}
			catch (Exception ex)
			{
				// Error:
			}
			return IsDateValid;
		}

		private bool CheckDates()
		{
			bool CheckDates = false;
			CheckDates = true;
			if (!IsDateValid(cmbStartDate.Text))
			{
				cmbStartDate.Focus();
				CheckDates = false;
				return CheckDates;
			}
			// If (Not IsDateValid(cmbStartTime.Text)) Then
			// cmbStartTime.SetFocus
			// CheckDates = False
			// Exit Function
			// End If
			if (!IsDateValid(cmbEndDate.Text))
			{
				cmbEndDate.Focus();
				CheckDates = false;
				return CheckDates;
			}
			// If (Not IsDateValid(cmbEndTime.Text)) Then
			// cmbEndTime.SetFocus
			// CheckDates = False
			// Exit Function
			// End If
			if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(cmbStartDate.Text), FCConvert.ToDateTime(cmbEndDate.Text)) < 0)
			{
				CheckDates = false;
				MessageBox.Show("The end date is before the start date", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return CheckDates;
			}
			if (cmbStartDate.Text == cmbEndDate.Text && !(chkAllDayEvent.CheckState == Wisej.Web.CheckState.Checked))
			{
				if (fecherFoundation.DateAndTime.DateDiff("n", (DateTime)dtpEnd.Value, (DateTime)dtpStart.Value) > 0)
				{
					CheckDates = false;
					MessageBox.Show("Then end time is before the start time", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return CheckDates;
				}
			}
			return CheckDates;
		}

		private bool UpdateEventFromControls()
		{
			bool UpdateEventFromControls = false;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			string strTemp = "";
			// vbPorter upgrade warning: EndTime As DateTime	OnWrite(DateTime, int)
			DateTime StartTime, EndTime;
			UpdateEventFromControls = true;
			// StartTime = dtpStart.Value
			// EndTime = dtpEnd.Value
			//FC:TODO
			StartTime = FCConvert.ToDateTime(cmbStartDate.Text + " " + dtpStart.Text);
			EndTime = FCConvert.ToDateTime(cmbEndDate.Text + " " + dtpEnd.Text);
			//StartTime = DateFromString(cmbStartDate.Text, cmbStartTime.Text)
			//EndTime = DateFromString(cmbEndDate.Text, cmbEndTime.Text)
			if (chkAllDayEvent.CheckState==Wisej.Web.CheckState.Checked) {
				if (fecherFoundation.DateAndTime.DateDiff("s", DateAndTime.TimeValue(EndTime), DateTime.FromOADate(0))==0) {
					EndTime = EndTime.AddDays(1);
				}
			}
			//FC:TODO
			//if (m_pEditingEvent.RecurrenceState!=xtpCalendarRecurrenceMaster) {
				m_pEditingEvent.Start = StartTime;
				m_pEditingEvent.End = EndTime;
			//}
			m_pEditingEvent.Title = txtSubject.Text;
			m_pEditingEvent.UserData.location = txtLocation.Text;
			m_pEditingEvent.UserData.body = txtBody.Text;
			m_pEditingEvent.AllDay = chkAllDayEvent.CheckState==Wisej.Web.CheckState.Checked;
			if (cmbLabel.SelectedIndex>=0) {
				m_pEditingEvent.UserData.Label = cmbLabel.ItemData( cmbLabel.SelectedIndex);
			}
			m_pEditingEvent.UserData.ScheduleID = 0;
			m_pEditingEvent.UserData.PrivateFlag = chkPublic.CheckState==Wisej.Web.CheckState.Unchecked;
			m_pEditingEvent.UserData.MeetingFlag = chkMeeting.CheckState==Wisej.Web.CheckState.Checked;
			if (!((chkReminder.CheckState==Wisej.Web.CheckState.Checked)==m_pEditingEvent.UserData.Reminder)) {
				m_pEditingEvent.UserData.Reminder = chkReminder.CheckState==Wisej.Web.CheckState.Checked;
				// m_pEditingEvent.ReminderSoundFile = "D:\Backup_10_12\Desktop\mustbuild.wav"
			}
			if (chkReminder.CheckState==Wisej.Web.CheckState.Checked) {
				if (!(Conversion.Val(cmbReminder.Text)==m_pEditingEvent.UserData.ReminderMinutesBeforeStart)) {
					m_pEditingEvent.UserData.ReminderMinutesBeforeStart = modCalendarFunctions.CalcStandardDurations_0m_2wLong(cmbReminder.Text);
				}
			}
			if (m_bAddEvent)
			{
				strTemp = "";
				for (counter = 0; counter <= (vsInstallers.Rows - 1); counter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsInstallers.TextMatrix(counter, SelectCol)) == true)
					if (FCConvert.CBool(vsInstallers.TextMatrix(counter, SelectCol)) == true)
					{
						strTemp += vsInstallers.TextMatrix(counter, DescriptionCol) + "|";
					}
				}
                // If Not chkPublic.Value = vbChecked Then
                // If strtemp <> "" Then
                // strtemp = Left(strtemp, Len(strtemp) - 1)
                // Else
                // UpdateEventFromControls = False
                // MsgBox "You must select at least 1 user before you may continue.", vbInformation, "Select User"
                // End If
                m_pEditingEvent.UserData.User = strTemp;
				// End If
				if (strTemp != "")
				{
					strTemp = Strings.Left(strTemp, strTemp.Length - 1);
				}
				else
				{
					// If Not chkPublic.Value = vbChecked Then
					UpdateEventFromControls = false;
					MessageBox.Show("You must select at least 1 user before you may continue.", "Select User", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// End If
				}
                m_pEditingEvent.UserData.User = strTemp;
            }
			else
			{
                m_pEditingEvent.UserData.User = cboUser.Text;
				if (cboUser.SelectedIndex < 0)
				{
					UpdateEventFromControls = false;
					MessageBox.Show("You must select a user before you may continue.", "Invalid User", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			return UpdateEventFromControls;
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			if (!CheckDates())
				return;
			if (!UpdateEventFromControls())
			{
				return;
			}
			if (m_bAddEvent)
			{
				// frmCalendar.m_pCustomDataHandler.DoCreateEvent m_pEditingEvent
				for (counter = 0; counter <= (vsInstallers.Rows - 1); counter++)
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsInstallers.TextMatrix(counter, SelectCol)) == true)
					if (FCConvert.CBool(vsInstallers.TextMatrix(counter, SelectCol)) == true)
					{
                        m_pEditingEvent.UserData.User = vsInstallers.TextMatrix(counter, DescriptionCol);
						frmCalendar.InstancePtr.CalendarControl.Events.Add(m_pEditingEvent);
					}
				}
			}
			else if (m_bNewEvent)
			{
				frmCalendar.InstancePtr.CalendarControl.Events.Add(m_pEditingEvent);
			}
			else
			{
				frmCalendar.InstancePtr.m_pCustomDataHandler.DoUpdateEvent(m_pEditingEvent);
				//frmCalendar.InstancePtr.CalendarControl.DataProvider.ChangeEvent(m_pEditingEvent);
			}
			//frmCalendar.InstancePtr.CalendarControl.Populate();
			Close();
		}

		private void UpdateEndTimePicker()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (IsDateValid(cmbEndDate.Text))
				{
					if (IsDateValid(cmbStartDate.Text))
					{
						if (fecherFoundation.DateAndTime.DateDiff("d", FCConvert.ToDateTime(cmbStartDate.Text), FCConvert.ToDateTime(cmbStartDate.Text)) == 0)
						{
							if (fecherFoundation.DateAndTime.DateDiff("n", (DateTime)dtpStart.Value, FCConvert.ToDateTime("11:59:00 PM")) >= 15)
							{
								if (fecherFoundation.DateAndTime.DateDiff("n", (DateTime)dtpEnd.Value, (DateTime)dtpStart.Value) > 0)
								{
									dtpEnd.Value = fecherFoundation.DateAndTime.DateAdd("n", 15, (DateTime)dtpStart.Value);
								}
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}
		// Private Sub UpdateEndTimeCombo()
		// On Error GoTo Error
		//
		// Dim i As Long
		// For i = 1 To cmbEndTime.ListCount - 1
		// cmbEndTime.RemoveItem 0
		// Next i
		//
		// Dim BeginTime As Date
		// BeginTime = TimeValue(cmbStartTime.Text)
		//
		// cmbEndTime.AddItem BeginTime & " (0 minutes)"
		// cmbEndTime.AddItem TimeValue(BeginTime + 1 / 24 / 2) & " (30 minutes)"
		// cmbEndTime.AddItem TimeValue(BeginTime + 1 / 24) & " (1 hour)"
		//
		// For i = 3 To 47
		// cmbEndTime.AddItem TimeValue(BeginTime + i / 24 / 2) & " (" & i / 2 & " hours)"
		// Next i
		//
		// Call SendMessage(cmbEndTime.hwnd, CB_SETDROPPEDWIDTH, 200, 0)
		// Error:
		//
		// End Sub
		// Private Sub InitStartTimeCombo()
		// On Error GoTo Error
		//
		// Dim i As Long
		// For i = 1 To cmbStartTime.ListCount - 1
		// cmbStartTime.RemoveItem 0
		// Next i
		//
		// Dim BeginTime As Date
		// BeginTime = #12:00:00 AM#
		//
		// For i = 1 To 47
		// cmbStartTime.AddItem TimeValue(BeginTime + i / 24 / 2)
		// Next i
		//
		// Error:
		//
		// End Sub
		private void dtpStart_Leave(object sender, System.EventArgs e)
		{
			UpdateEndTimePicker();
		}

		private void frmEditEvent_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditEvent properties;
			//frmEditEvent.ScaleWidth	= 9300;
			//frmEditEvent.ScaleHeight	= 5880;
			//frmEditEvent.LinkTopic	= "Form1";
			//End Unmaped Properties
			clsDRWrapper rsTaskInfo = new clsDRWrapper();
			SelectCol = 0;
			DescriptionCol = 1;
			vsInstallers.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsInstallers.ColWidth(SelectCol, FCConvert.ToInt32(vsInstallers.WidthOriginal * 0.2));
			modGlobalFunctions.SetTRIOColors(this);
			// InitStartTimeCombo
			// Fill Labels Combobox
			//FC:FINAL:DSE:#1899 Call frmCalendar Load event (if Calendar window was closed)
			frmCalendar.InstancePtr.LoadForm();
			//FC:TODO
			//CalendarEventLabel pLabel = new CalendarEventLabel();
			foreach (var pLabel in frmCalendar.InstancePtr.CalendarControl.LabelList) {
				cmbLabel.AddItem(pLabel.Name);
				cmbLabel.ItemData( cmbLabel.NewIndex, pLabel.LabelID);
			//	// None 0
			//	// Important 1
			//	// Business 2
			//	// personal 3
			//	// vacation 4
			//	// Must Atterd 5
			//	// Travel Required 6
			//	// Needs Preparation 7
			//	// Birthday 8
			//	// Anniversary 9
			//	// Phone Call 10
			//	// Inspection 11
			}
			LoadUsers();
			LoadInstallersToGrid();
            // Populate controls with Event properties values
            //FC:TODO
            if (!m_bAddEvent)
            {
                //if (m_pEditingEvent.RecurrenceState==xtpCalendarRecurrenceOccurrence) {
                //	m_pEditingEvent.MakeAsRException();
                //}
                UpdateControlsFromEvent();
                //fraEditStaff.Visible = true;
                //fraInstallers.Visible = false;

                string user = m_pEditingEvent.UserData.User;
                if (user != null)
                {
                    SetUserCombo(user);
                }

            }
            else
            {
                //fraEditStaff.Visible = false;
                //fraInstallers.Visible = true;
            }
			// Fill reminders durations combobox
			modCalendarFunctions.FillStandardDurations_0m_2w(ref cmbReminder, false);
			frmCalendar.InstancePtr.IncrementModalFormsRunning();
			// frmCalendar.ModalFormsRunningCounter = frmCalendar.ModalFormsRunningCounter + 1
			int nCaptionHeght;
			nCaptionHeght = this.HeightOriginal - this.ClientSize.Height;
		}

		private void LoadInstallersToGrid()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			vsInstallers.Rows = 0;
			rsInfo.OpenRecordset("SELECT * FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' ORDER BY UserID", "ClientSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// If Not blnEditUserOnly Then
				do
				{
					vsInstallers.AddItem("");
					vsInstallers.TextMatrix(vsInstallers.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsInstallers.TextMatrix(vsInstallers.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields("UserID")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void vsInstallers_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsInstallers.Row >= 0)
			{
				//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
				//if (FCConvert.ToBoolean(vsInstallers.TextMatrix(vsInstallers.Row, SelectCol)) == false)
				if (FCConvert.CBool(vsInstallers.TextMatrix(vsInstallers.Row, SelectCol)) == false)
				{
					vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsInstallers_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				if (vsInstallers.Row >= 0)
				{
					KeyCode = 0;
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsInstallers.TextMatrix(vsInstallers.Row, SelectCol)) == false)
					if (FCConvert.CBool(vsInstallers.TextMatrix(vsInstallers.Row, SelectCol)) == false)
					{
						vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsInstallers.TextMatrix(vsInstallers.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		public void SetInstallersGrid(string strSearch)
		{
			string[] strInstallers = null;
			// vbPorter upgrade warning: counter As int	OnWriteFCConvert.ToInt32(
			int counter;
			// vbPorter upgrade warning: intRowCounter As int	OnWriteFCConvert.ToInt32(
			int intRowCounter;
			for (counter = 0; counter <= (vsInstallers.Rows - 1); counter++)
			{
				vsInstallers.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
			strInstallers = Strings.Split(strSearch, ",", -1, CompareConstants.vbBinaryCompare);
			for (counter = 0; counter <= (Information.UBound(strInstallers, 1)); counter++)
			{
				for (intRowCounter = 0; intRowCounter <= (vsInstallers.Rows - 1); intRowCounter++)
				{
					if (fecherFoundation.Strings.Trim(Strings.Left(vsInstallers.TextMatrix(intRowCounter, DescriptionCol), strInstallers[counter].Length)) == fecherFoundation.Strings.Trim(strInstallers[counter]))
					{
						vsInstallers.TextMatrix(intRowCounter, SelectCol, FCConvert.ToString(true));
					}
				}
			}
		}

		private void LoadUsers()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			cboUser.Clear();
			rsInfo.OpenRecordset("SELECT * FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' ORDER BY UserID", "ClientSettings");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// If Not blnEditUserOnly Then
				do
				{
					cboUser.AddItem(FCConvert.ToString(rsInfo.Get_Fields("UserID")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		public void SetUserCombo(string strSearch)
		{
			int counter;
			if (strSearch == "")
			{
				cboUser.SelectedIndex = -1;
			}
			else
			{
				for (counter = 0; counter <= cboUser.Items.Count - 1; counter++)
				{
					if (Strings.Left(cboUser.Items[counter].ToString(), strSearch.Length) == strSearch)
					{
						cboUser.SelectedIndex = counter;
						break;
					}
				}
			}
		}

		public void SetStartEnd(DateTime BeginSelection, DateTime EndSelection, bool AllDay)
		{
			// vbPorter upgrade warning: EndDate As DateTime	OnWrite
			DateTime StartDate, StartTime, EndDate, EndTime;
			StartDate = DateAndTime.DateValue(BeginSelection.ToString());
			StartTime = DateAndTime.TimeValue(BeginSelection);
			EndDate = DateAndTime.DateValue(EndSelection.ToString());
			EndTime = DateAndTime.TimeValue(EndSelection);
			if (AllDay)
			{
				// cmbEndTime.Visible = False
				// cmbStartTime.Visible = False
				dtpStart.Visible = false;
				dtpEnd.Visible = false;
				if (fecherFoundation.DateAndTime.DateDiff("s", EndTime, DateTime.FromOADate(0)) == 0)
				{
					EndDate = EndDate.AddDays(-1);
				}
			}
			cmbStartDate.Text = StartDate.ToShortDateString();
			// cmbStartTime.Text = StartTime
			dtpStart.Value = StartTime > DateTimePicker.MaximumDateTime ? DateTimePicker.MaximumDateTime : StartTime;
			// UpdateEndTimeCombo
			cmbEndDate.Text = EndDate.ToShortDateString();
			// cmbEndTime.Text = EndTime
			dtpEnd.Value = EndTime > DateTimePicker.MaximumDateTime ? DateTimePicker.MaximumDateTime : EndTime;
		}

		public void NewEvent()
		{
            vsInstallers.Visible = true;
            cboUser.Visible = false;
			//FC:FINAL:DSE:#1901 Fix window text
			this.Text = "Untitled - Event";
			//m_pEditingEvent = frmCalendar.InstancePtr.CalendarControl.DataProvider.CreateEvent;
			m_bAddEvent = true;
			m_bNewEvent = true;
			DateTime BeginSelection = DateTime.FromOADate(0), EndSelection = DateTime.FromOADate(0);
			bool AllDay = false;
			frmCalendar.InstancePtr.CalendarControl.GetSelection(ref BeginSelection, ref EndSelection, ref AllDay);
			//FC:TODO
			//m_pEditingEvent.StartTime = BeginSelection;
			//m_pEditingEvent.EndTime = EndSelection;
			SetStartEnd(BeginSelection, EndSelection, AllDay);
			//FC:FINAL:DSE:#1901 Fire Load event to load combo boxes
			this.LoadForm();
			chkAllDayEvent.CheckState = (AllDay ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			txtSubject.Text = "New Event";
			cmbLabel.SelectedIndex = 0;
			chkReminder_Click();
			cmbReminder.Text = "15 minutes";
			SetInstallersGrid(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
		}
        public void ModifyEvent(ref Event ModEvent, bool boolIsNewEvent = false)
        {
            vsInstallers.Visible = false;
            cboUser.Visible = true;

            m_pEditingEvent = ModEvent;
            m_bNewEvent = boolIsNewEvent;
            m_bAddEvent = false;
            if (m_pEditingEvent.Title != "")
            {
                this.Text = m_pEditingEvent.Title + " - Event";
                this.HeaderText.Text = this.Text;
            }
        }

        public void UpdateControlsFromEvent()
        {
            //	// Restore Event base properties
            //FC:FINAL:BSE:#3680 text should not be uppercased 
            txtSubject.Text = System.Globalization.CultureInfo.InvariantCulture.TextInfo.ToTitleCase(m_pEditingEvent.Title.ToLower());
            txtBody.Text = m_pEditingEvent.UserData.body != null ? m_pEditingEvent.UserData.body : string.Empty;
            txtLocation.Text = m_pEditingEvent.UserData.location;
            chkAllDayEvent.CheckState = (m_pEditingEvent.AllDay ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
            //	// Restore Event Label value
            int I;
            for (I = 0; I <= cmbLabel.Items.Count - 1; I++)
            {
                if (cmbLabel.ItemData(I) == m_pEditingEvent.UserData.Label)
                {
                    cmbLabel.SelectedIndex = I;
                    break;
                }
            }
            if (m_pEditingEvent.UserData.PrivateFlag == null)
            {
                m_pEditingEvent.UserData.PrivateFlag = false;
            }
            chkPublic.CheckState = (m_pEditingEvent.UserData.PrivateFlag ? Wisej.Web.CheckState.Unchecked : Wisej.Web.CheckState.Checked);
            if (m_pEditingEvent.UserData.MeetingFlag == null)
            {
                m_pEditingEvent.UserData.MeetingFlag = false;
            }
            chkMeeting.CheckState = (m_pEditingEvent.UserData.MeetingFlag ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
            SetStartEnd(m_pEditingEvent.Start, m_pEditingEvent.End, m_pEditingEvent.AllDay);
            if (m_pEditingEvent.Title != "")
            {
                this.Text = m_pEditingEvent.Title + " - Event";
            }
            //	bool bDatesVisible;
            //	bDatesVisible = m_pEditingEvent.RecurrenceState!=xtpCalendarRecurrenceMaster;
            //	lblStartTime.Visible = bDatesVisible;
            //	lblEndTime.Visible = bDatesVisible;
            //	cmbStartDate.Visible = bDatesVisible;
            //	dtpStart.Visible = bDatesVisible;
            //	cmbEndDate.Visible = bDatesVisible;
            //	dtpEnd.Visible = bDatesVisible;
            //	chkAllDayEvent.Visible = bDatesVisible;
            //	if (bDatesVisible) {
            //		chkAllDayEvent_Click();
            //	}
            if (m_pEditingEvent.UserData.Reminder != null && m_pEditingEvent.UserData.Reminder == true)
            {
                chkReminder.CheckState = Wisej.Web.CheckState.Checked;
                if (m_pEditingEvent.UserData.ReminderMinutesBeforeStart != null)
                {
                    cmbReminder.Text = modCalendarFunctions.CalcStandardDurations_0m_2wString(m_pEditingEvent.UserData.ReminderMinutesBeforeStart);
                }
                else
                {
                    cmbReminder.Text = "";
                }
            }
            else
            {
                chkReminder.CheckState = Wisej.Web.CheckState.Unchecked;
                cmbReminder.Text = "";
            }
        }
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			frmCalendar.InstancePtr.DecrementModalFormsRunning();
			// frmCalendar.ModalFormsRunningCounter = frmCalendar.ModalFormsRunningCounter - 1
		}
	}
}
