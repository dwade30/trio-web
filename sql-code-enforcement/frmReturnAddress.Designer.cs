﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmReturnAddress.
	/// </summary>
	partial class frmReturnAddress
	{
		public FCGrid gridAddress;
		//public fecherFoundation.FCButton cmdSave;
		public fecherFoundation.FCButton cmdSaveContinue;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.gridAddress = new fecherFoundation.FCGrid();
            //this.cmdSave = new fecherFoundation.FCButton();
            this.cmdSaveContinue = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAddress)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveContinue);
            this.BottomPanel.Location = new System.Drawing.Point(0, 348);
            this.BottomPanel.Size = new System.Drawing.Size(520, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.gridAddress);
            this.ClientArea.Size = new System.Drawing.Size(520, 288);
            // 
            // TopPanel
            // 
            //this.TopPanel.Controls.Add(this.cmdSave);
            this.TopPanel.Size = new System.Drawing.Size(520, 60);
            //this.TopPanel.Controls.SetChildIndex(this.cmdSave, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(183, 30);
            this.HeaderText.Text = "Return Address";
            // 
            // gridAddress
            // 
            this.gridAddress.AllowSelection = false;
            this.gridAddress.AllowUserToResizeColumns = false;
            this.gridAddress.AllowUserToResizeRows = false;
            this.gridAddress.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridAddress.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridAddress.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridAddress.BackColorBkg = System.Drawing.Color.Empty;
            this.gridAddress.BackColorFixed = System.Drawing.Color.Empty;
            this.gridAddress.BackColorSel = System.Drawing.Color.Empty;
            this.gridAddress.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridAddress.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridAddress.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridAddress.ColumnHeadersHeight = 30;
            this.gridAddress.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridAddress.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridAddress.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridAddress.DragIcon = null;
            this.gridAddress.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.gridAddress.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridAddress.ExtendLastCol = true;
            this.gridAddress.FixedCols = 0;
            this.gridAddress.FixedRows = 0;
            this.gridAddress.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridAddress.FrozenCols = 0;
            this.gridAddress.GridColor = System.Drawing.Color.Empty;
            this.gridAddress.GridColorFixed = System.Drawing.Color.Empty;
            this.gridAddress.Location = new System.Drawing.Point(30, 30);
            this.gridAddress.Name = "gridAddress";
            this.gridAddress.OutlineCol = 0;
            this.gridAddress.RowHeadersVisible = false;
            this.gridAddress.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridAddress.RowHeightMin = 0;
            this.gridAddress.Rows = 4;
            this.gridAddress.ScrollTipText = null;
            this.gridAddress.ShowColumnVisibilityMenu = false;
            this.gridAddress.ShowFocusCell = false;
            this.gridAddress.Size = new System.Drawing.Size(461, 229);
            this.gridAddress.StandardTab = true;
            this.gridAddress.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridAddress.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.gridAddress.TabIndex = 0;
            this.gridAddress.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.gridAddress_AfterEdit);
            // 
            // cmdSave
            // 
            //this.cmdSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
           // this.cmdSave.AppearanceKey = "toolbarButton";
            //this.cmdSave.Location = new System.Drawing.Point(444, 29);
            //this.cmdSave.Name = "cmdSave";
            //this.cmdSave.Size = new System.Drawing.Size(47, 24);
            //this.cmdSave.TabIndex = 1;
            //this.cmdSave.Text = "Save";
            //this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdSaveContinue
            // 
            this.cmdSaveContinue.AppearanceKey = "acceptButton";
            this.cmdSaveContinue.Location = new System.Drawing.Point(158, 30);
            this.cmdSaveContinue.Name = "cmdSaveContinue";
            this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveContinue.Size = new System.Drawing.Size(173, 48);
            this.cmdSaveContinue.TabIndex = 0;
            this.cmdSaveContinue.Text = "Save";
            this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmReturnAddress
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(520, 456);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmReturnAddress";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Return Address";
            this.Load += new System.EventHandler(this.frmReturnAddress_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReturnAddress_KeyDown);
            this.Resize += new System.EventHandler(this.frmReturnAddress_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridAddress)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
