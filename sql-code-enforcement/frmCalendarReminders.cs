﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmCalendarReminders : BaseForm
	{
		public frmCalendarReminders()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCalendarReminders InstancePtr
		{
			get
			{
				return (frmCalendarReminders)Sys.GetInstance(typeof(frmCalendarReminders));
			}
		}

		protected frmCalendarReminders _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		//FC:TODO
		//public void OnReminders(XtremeCalendarControl.CalendarRemindersAction Action, XtremeCalendarControl.CalendarReminder Reminder)
		//{
		//	//App.DoEvents();
		//	if (Action==xtpCalendarRemindersFire || Action==xtpCalendarReminderSnoozed || Action==xtpCalendarReminderDismissed || Action==xtpCalendarReminderDismissedAll) {
		//		UpdateFromManager();
		//		UpdateControlsBySelection();
		//	} else if (Action==xtpCalendarRemindersMonitoringStopped) {
		//		ctrlReminders.Items.Clear();
		//		UpdateControlsBySelection();
		//	}
		//	if (ctrlReminders.Items.Count==0) {
		//		Close();
		//	}
		//}
		public void CheckReminders()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				UpdateFromManager();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
		}

		private void UpdateFromManager()
		{
			ctrlReminders.Items.Clear();
			//FC:TODO
			//CalendarReminder pRemI;
			//CalendarEvent pEventI;
			//ListViewItem pItemI;
			//bool boolUse = false;
			//foreach (CalendarReminder pRemI in frmCalendar.InstancePtr.CalendarControl.Reminders)
			//{
			//	pEventI = pRemI.Event;
			//	boolUse = false;
			//	if (fecherFoundation.DateAndTime.DateDiff("n", DateTime.Now, pEventI.StartTime)<=pEventI.ReminderMinutesBeforeStart) {
			//		boolUse = true;
			//	} else if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Now, pEventI.StartTime)==0 && pEventI.AllDayEvent) {
			//		boolUse = true;
			//	} else {
			//	}
			//	if (boolUse) {
			//		pItemI = ctrlReminders.Items.Add("");
			//		pItemI.Text = pEventI.Subject;
			//		// ctrlReminders.ListItems.Add , , pEventI.Subject
			//		// vbPorter upgrade warning: nMinutes As int	OnWrite(long)
			//		int nMinutes = 0; string strDueIn = "";
			//		nMinutes = FCConvert.ToInt32((fecherFoundation.DateAndTime.DateDiff("n", DateTime.Now, pEventI.StartTime));
			//		if (nMinutes>0) {
			//			strDueIn = modCalendarFunctions.FormatTimeDuration(nMinutes, true);
			//		} else {
			//			strDueIn = modCalendarFunctions.FormatTimeDuration(-1*nMinutes, true)+" overdue";
			//		}
			//		pItemI.SubItems[1].Text = strDueIn;
			//		// If ctrlReminders.ListItems(ctrlReminders.ListItems.Count).ListSubItems.Count > 0 Then
			//		// ctrlReminders.ListItems(ctrlReminders.ListItems.Count).SubItems(1) = strDueIn
			//		// End If
			//	}
			//}
		}

		private void UpdateControlsBySelection()
		{
			bool bEnabled;
			bEnabled = false;
			if (ctrlReminders.FocusedItem == null)
			{
				txtDescription1.Text = "";
				if (ctrlReminders.Items.Count > 0)
				{
					txtDescription2.Text = "0 reminders are selected";
				}
				else
				{
					txtDescription2.Text = "There are no reminders to show.";
				}
			}
			else
			{
				bEnabled = true;
			}
			btnDismissAll.Enabled = bEnabled;
			btnDismiss.Enabled = bEnabled;
			btnOpenItem.Enabled = bEnabled;
			btnSnooze.Enabled = bEnabled;
			cmbSnooze.Enabled = bEnabled;
			//FC:TODO
			//CalendarReminder pRem = new CalendarReminder();
			//if (bEnabled) {
			//	pRem = frmCalendar.InstancePtr.CalendarControl.Reminders((ctrlReminders.FocusedItem.Index+1)-1);
			//	txtDescription1.Text = pRem.Event.Subject;
			//	txtDescription2.Text = "Start time:  "+Strings.FormatDateTime(pRem.Event.StartTime, Microsoft.VisualBasic.DateFormat.GeneralDate);
			//	if (pRem.MinutesBeforeStart<5) {
			//		cmbSnooze.Text = "5 minutes";
			//	} else {
			//		cmbSnooze.Text = modCalendarFunctions.FormatTimeDuration(pRem.MinutesBeforeStart, false);
			//	}
			//}
			this.Text = FCConvert.ToString(ctrlReminders.Items.Count) + " Reminder" + (ctrlReminders.Items.Count > 1 ? "s" : "");
		}

		private void btnDismiss_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			if (ctrlReminders.FocusedItem == null)
			{
				return;
			}
			//FC:TODO
			//CalendarReminder pRem = new CalendarReminder();
			//int nIndex;
			//nIndex = (ctrlReminders.FocusedItem.Index+1);
			//pRem = frmCalendar.InstancePtr.CalendarControl.Reminders(nIndex-1);
			//pRem.Dismiss();
			//rs.Execute("UPDATE NewTasks SET TaskReminder = 0 WHERE ID = "+pRem.Event.ID, modGlobalVariables.strGNDatabase);
		}

		private void btnDismissAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rs = new clsDRWrapper();
			//FC:TODO
			//frmCalendar.InstancePtr.CalendarControl.Reminders.DismissAll();
			//for(counter=0; counter<=frmCalendar.InstancePtr.CalendarControl.Reminders.Count-1; counter++) {
			//	rs.Execute("UPDATE NewTasks SET TaskReminder = 0 WHERE ID = "+frmCalendar.InstancePtr.CalendarControl.Reminders(counter).Event.ID, modGlobalVariables.strGNDatabase);
			//}
		}

		private void btnOpenItem_Click(object sender, System.EventArgs e)
		{
			if (ctrlReminders.FocusedItem == null)
			{
				return;
			}
			//FC:TODO
			//CalendarReminder pRem = new CalendarReminder();
			//int nIndex;
			//nIndex = (ctrlReminders.FocusedItem.Index+1);
			//pRem = frmCalendar.InstancePtr.CalendarControl.Reminders(nIndex-1);
			//frmEditEvent frmProperties = new frmEditEvent();
			//frmProperties.ModifyEvent(ref pRem.Event);
			//frmProperties.Show(FCForm.FormShowEnum.Modal);
		}

		private void btnSnooze_Click(object sender, System.EventArgs e)
		{
			if (ctrlReminders.FocusedItem == null)
			{
				return;
			}
			int nMinutes = 0;
			modCalendarFunctions.ParseTimeDuration(cmbSnooze.Text, ref nMinutes);
			//FC:TODO
			//CalendarReminder pRem = new CalendarReminder();
			//int nIndex;
			//nIndex = (ctrlReminders.FocusedItem.Index+1);
			//pRem = frmCalendar.InstancePtr.CalendarControl.Reminders(nIndex-1);
			//pRem.Snooze(nMinutes);
		}

		private void ctrlReminders_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			UpdateControlsBySelection();
		}

		private void frmCalendarReminders_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCalendarReminders_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCalendarReminders properties;
			//frmCalendarReminders.FillStyle	= 0;
			//frmCalendarReminders.ScaleWidth	= 5880;
			//frmCalendarReminders.ScaleHeight	= 4110;
			//frmCalendarReminders.LinkTopic	= "Form2";
			//frmCalendarReminders.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modCalendarFunctions.FillStandardDurations_0m_2w(ref cmbSnooze, true);
			// frmCalendar.IncrementModalFormsRunning
			// frmCalendar.ModalFormsRunningCounter = frmCalendar.ModalFormsRunningCounter + 1
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCalendarReminders_Resize(object sender, System.EventArgs e)
		{
			ctrlReminders.Columns[0].Width = FCConvert.ToInt32(0.4 * ctrlReminders.WidthOriginal);
			ctrlReminders.Columns[1].Width = FCConvert.ToInt32(0.59 * ctrlReminders.WidthOriginal);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// frmCalendar.DecrementModalFormsRunning
			// frmCalendar.ModalFormsRunningCounter = frmCalendar.ModalFormsRunningCounter - 1
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
	}
}
