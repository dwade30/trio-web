﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWCE0000
{
	public class modGlobalVariables
	{
		//=========================================================
		public const uint HKEY_LOCAL_MACHINE = 0x80000001;
		public const uint HKEY_MACHINE = 0x80000002;
		// Global Const REG_SZ = 1                         ' Unicode nul terminated string
		// Global Const REG_DWORD = 4
		public class StaticVariables
		{
            public bool moduleInitialized = false;
            public clsCustomize CECustom;
			public string strREDatabase = string.Empty;
			public string strCEDatabase = string.Empty;
			public string strGNDatabase = string.Empty;
			public string strCECostFilesDatabase = string.Empty;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
