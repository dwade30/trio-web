//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptPermits.
	/// </summary>
	public partial class rptPermits : BaseSectionReport
	{
		public rptPermits()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPermits InstancePtr
		{
			get
			{
				return (rptPermits)Sys.GetInstance(typeof(rptPermits));
			}
		}

		protected rptPermits _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsType.Dispose();
				rsStatus.Dispose();
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPermits	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		private clsDRWrapper rsStatus = new clsDRWrapper();
		private clsDRWrapper rsType = new clsDRWrapper();

		public void Init(ref clsSQLStatement clsStatement, bool boolDistinct = false)
		{
			string strSQL = "";
			string strWhere;
			strWhere = "not permitS.deleted = 1";
			if (!(clsStatement == null))
			{
				// strSQL = clsStatement.SQLStatement
				// If boolDistinct Then
				if (clsStatement.WhereStatement != "")
				{
					strWhere = clsStatement.WhereStatement + " and " + strWhere;
				}
				else
				{
					strWhere = " where " + strWhere;
				}
				strSQL = "select distinct applicationdate,filedby,permitidentifier,permityear,permitnumber,originalstreetnum,originalstreet,permittype,appdencode from (" + clsStatement.SelectStatement + " " + strWhere + ") abc " + clsStatement.OrderByStatement;
				// End If
			}
			else
			{
				strSQL = "Select * from cemaster where not deleted = 1 order by ceaccount";
				strSQL = "select permits.ID as PermitAutoid,account,PermitIdentifier,PermitYear,AppDenCode,ApplicationDate,Plan,FiledBy,ContactName,ContactEmail,StartDate,CompletionDate,EstimatedValue,Fee,PermitType,";
				strSQL += "PermitSubType,PERMITS.ContractorID AS PermitContractorID,CertificateOfOCCRequired,CertificateOfOCCDate,PermitNumber,Units,DwellUnits,NumBuildings,CommResCode,Description,OMBCode,PermitContractorName,";
				strSQL += "PermitContractorAddress1,PermitContractorAddress2,PermitContractorCity,PermitContractorState,PermitContractorZip,PermitContractorZip4,PermitContractorEmail,Completed,";
				strSQL += "permits.deleted as PermitDeleted,permits.comment as PermitComment,OriginalOwner,OriginalMapLot,OriginalStreetNum,OriginalStreet,OriginalOwnerFirst,OriginalOwnerLast,OriginalOwnerMiddle,OriginalOwnerDesig from permits where " + strWhere + " order by applicationdate,permitidentifier";
			}
			txtRange.Text = clsStatement.ParameterDescriptionText;
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "Permits");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			rsType.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
			rsStatus.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by code", modGlobalVariables.Statics.strCEDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				if (Information.IsDate(rsReport.Get_Fields("applicationdate")))
				{
                    //FC:FINAL:MSH - i.issue #1693: replace Convert by Get_Fields_DateTime to avoid exceptions
                    //if (Convert.ToDateTime(rsReport.Get_Fields("applicationdate")).ToOADate() != 0)
                    if (rsReport.Get_Fields_DateTime("applicationdate").ToOADate() != 0)
					{
                        //FC:FINAL:MSH - i.issue #1693: get data in Short format (as in original)
						//txtDateFiled.Text = rsReport.Get_Fields("applicationdate");
						txtDateFiled.Text = rsReport.Get_Fields_DateTime("applicationdate").ToShortDateString();
					}
					else
					{
						txtDateFiled.Text = "";
					}
				}
				else
				{
					txtDateFiled.Text = "";
				}
				txtFiler.Text = rsReport.Get_Fields_String("filedby");
				if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("permitidentifier")) != string.Empty)
				{
					txtID.Text = rsReport.Get_Fields_String("permitidentifier");
				}
				else
				{
					txtID.Text = rsReport.Get_Fields_Int32("permityear") + "-" + rsReport.Get_Fields("permitnumber");
				}
				txtLocation.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("originalstreetnum") + " " + rsReport.Get_Fields_String("originalstreet"));
				if (rsType.FindFirstRecord("ID", Conversion.Val(rsReport.Get_Fields_String("permittype"))))
				{
					txtType.Text = rsType.Get_Fields_String("description");
				}
				else
				{
					txtType.Text = "";
				}
				if (rsStatus.FindFirstRecord("Code", Conversion.Val(rsReport.Get_Fields_String("appdencode"))))
				{
					txtStatus.Text = rsStatus.Get_Fields_String("description");
				}
				else if (Conversion.Val(rsReport.Get_Fields_String("appdencode")) == modCEConstants.CNSTPERMITCOMPLETE)
				{
					txtStatus.Text = "Complete";
				}
				else
				{
					txtStatus.Text = "";
				}
				rsReport.MoveNext();
			}
		}

		
	}
}
