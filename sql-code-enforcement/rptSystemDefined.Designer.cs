﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptSystemDefined.
	/// </summary>
	partial class rptSystemDefined
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptSystemDefined));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtShort = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtGroup = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.txtPermitDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPermitShort = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermitDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermitShort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtDesc,
            this.txtShort});
            this.Detail.Height = 0.21875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtDesc
            // 
            this.txtDesc.Height = 0.1770833F;
            this.txtDesc.Left = 0.6979167F;
            this.txtDesc.Name = "txtDesc";
            this.txtDesc.Text = null;
            this.txtDesc.Top = 0.02083333F;
            this.txtDesc.Width = 2.489583F;
            // 
            // txtShort
            // 
            this.txtShort.Height = 0.1770833F;
            this.txtShort.Left = 3.270833F;
            this.txtShort.Name = "txtShort";
            this.txtShort.Text = null;
            this.txtShort.Top = 0.02083333F;
            this.txtShort.Width = 1.739583F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage,
            this.Label1});
            this.PageHeader.Height = 0.65625F;
            this.PageHeader.Name = "PageHeader";
            // 
            // txtMuni
            // 
            this.txtMuni.Height = 0.1875F;
            this.txtMuni.HyperLink = null;
            this.txtMuni.Left = 0F;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Style = "";
            this.txtMuni.Text = null;
            this.txtMuni.Top = 0.03125F;
            this.txtMuni.Width = 1.25F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.HyperLink = null;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.21875F;
            this.txtTime.Width = 1.25F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.HyperLink = null;
            this.txtDate.Left = 6.1875F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.03125F;
            this.txtDate.Width = 1.25F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.HyperLink = null;
            this.txtPage.Left = 6.1875F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.21875F;
            this.txtPage.Width = 1.25F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.25F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.Label1.Text = "System Defined Types";
            this.Label1.Top = 0.03125F;
            this.Label1.Width = 4.9375F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroup});
            this.GroupHeader1.DataField = "grpHeader";
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // txtGroup
            // 
            this.txtGroup.Height = 0.1925F;
            this.txtGroup.HyperLink = null;
            this.txtGroup.Left = 0.0625F;
            this.txtGroup.Name = "txtGroup";
            this.txtGroup.Style = "";
            this.txtGroup.Text = null;
            this.txtGroup.Top = 0.03125F;
            this.txtGroup.Width = 2.958333F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.CanGrow = false;
            this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtPermitDesc,
            this.txtPermitShort});
            this.GroupHeader2.Height = 0F;
            this.GroupHeader2.Name = "GroupHeader2";
            this.GroupHeader2.Visible = false;
            this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
            // 
            // txtPermitDesc
            // 
            this.txtPermitDesc.Height = 0.1770833F;
            this.txtPermitDesc.Left = 0.6979167F;
            this.txtPermitDesc.Name = "txtPermitDesc";
            this.txtPermitDesc.Text = null;
            this.txtPermitDesc.Top = 0.02083333F;
            this.txtPermitDesc.Width = 2.489583F;
            // 
            // txtPermitShort
            // 
            this.txtPermitShort.Height = 0.1770833F;
            this.txtPermitShort.Left = 3.270833F;
            this.txtPermitShort.Name = "txtPermitShort";
            this.txtPermitShort.Text = null;
            this.txtPermitShort.Top = 0.02083333F;
            this.txtPermitShort.Width = 1.739583F;
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Height = 0F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // rptSystemDefined
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.447917F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.DataInitialize += new System.EventHandler(this.rptSystemDefined_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermitDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPermitShort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtShort;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtGroup;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPermitDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPermitShort;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
	}
}
