//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	public partial class frmMaster : BaseForm
	{
		public frmMaster()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmMaster InstancePtr
		{
			get
			{
				return (frmMaster)Sys.GetInstance(typeof(frmMaster));
			}
		}

		protected frmMaster _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// ********************************************************
		int lngCurrentAccount;
		int lngREAccount;
		const int CNSTGRIDPREVIOUSMAPSCOLMAPLOT = 0;
		const int CNSTGRIDPREVIOUSMAPSCOLDATE = 1;
		const int CNSTGRIDOPENSCOLAUTOID = 0;
		const int CNSTGRIDOPENSCOLDESC = 1;
		const int CNSTGRIDOPENSCOLVALUE = 2;
		const int CNSTGRIDOPENSCOLVALUEFORMAT = 3;
		const int CNSTGRIDOPENSCOLMANDATORY = 4;
		const int CNSTGRIDOPENSCOLMIN = 5;
		const int CNSTGRIDOPENSCOLMAX = 6;
		const int CNSTGRIDABUTTERSCOLAUTOID = 0;
		const int CNSTGRIDABUTTERSCOLACCOUNT = 1;
		const int CNSTGRIDABUTTERSCOLMAPLOT = 2;
		const int CNSTGRIDPERMITSCOLAUTOID = 0;
		const int CNSTGRIDPERMITSCOLPLAN = 2;
		const int CNSTGRIDPERMITSCOLPERMITNUMBER = 1;
		const int CNSTGRIDPERMITSCOLSTARTDATE = 3;
		const int CNSTGRIDPERMITSCOLENDDATE = 4;
		const int CNSTGRIDPERMITSCOLSTATUS = 5;
		const int CNSTGRIDPERMITSCOLPERMITIDENTIFIER = 6;
		const int CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE = 7;
		const int CNSTGRIDPERMITSCOLPERMITVALUE = 8;
		const int CNSTGRIDPERMITSCOLPERMITFEE = 9;
		const int CNSTGRIDPERMITSCOLTYPE = 10;
		const int CNSTGRIDVIOLATIONSCOLAUTOID = 0;
		const int CNSTGRIDVIOLATIONSCOLIDENTIFIER = 1;
		const int CNSTGRIDVIOLATIONSCOLTYPE = 2;
		const int CNSTGRIDVIOLATIONSCOLSTATUS = 4;
		const int CNSTGRIDVIOLATIONSCOLISSUED = 3;
		private clsCEMaster MastRec = new clsCEMaster();
		private bool boolCantEdit;
		const int BPBookCol = 1;
		const int BPPageCol = 2;
		// Private Const BPSaleDateCol = 3
		const int BPCheckCol = 0;
		const int BPIDCol = 3;
		const int CNSTGRIDACTIVITYCOLAUTOID = 0;
		const int CNSTGRIDACTIVITYCOLDATE = 1;
		const int CNSTGRIDACTIVITYCOLTYPE = 2;
		const int CNSTGRIDACTIVITYCOLDESC = 3;
		private clsGridPermitSetup gps = new clsGridPermitSetup();
		private bool boolDataChanged;
		private clsActivityList alList = new clsActivityList();

		private void cmdEditOwner_Click(object sender, System.EventArgs e)
		{
			if (!(MastRec == null))
			{
				if (MastRec.PartyID1 > 0)
				{
					int lngReturn;
					int lngID = MastRec.PartyID1;
					lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngID);
					cPartyController tCont = new cPartyController();
					cParty temp = MastRec.OwnerParty();
					tCont.LoadParty(ref temp, MastRec.PartyID1);
					ShowOwner();
					ShowAddress();
				}
			}
		}

		private void cmdEditSecOwner_Click(object sender, System.EventArgs e)
		{
			if (!(MastRec == null))
			{
				if (MastRec.PartyID2 > 0)
				{
					int lngReturn;
					int lngID = MastRec.PartyID2;
					lngReturn = frmEditCentralParties.InstancePtr.Init(ref lngID);
					cPartyController tCont = new cPartyController();
					cParty temp = MastRec.SecOwnerParty();
					tCont.LoadParty(ref temp, MastRec.PartyID2);
					ShowSecOwner();
				}
			}
		}

		private void mnuAddActivity_Click(object sender, System.EventArgs e)
		{
			if (!boolCantEdit)
			{
				AddActivity();
			}
			else
			{
				// MsgBox "You cannot add activity until this property has been created", vbExclamation, "Not Saved"
				return;
			}
		}

		private void cmdRemoveOwner_Click(object sender, System.EventArgs e)
		{
			MastRec.PartyID1 = 0;
			if (!(MastRec.OwnerParty() == null))
			{
				MastRec.OwnerParty().Clear();
			}
			ShowOwner();
			ShowAddress();
		}

		private void cmdRemoveSecOwner_Click(object sender, System.EventArgs e)
		{
			MastRec.PartyID2 = 0;
			if (!(MastRec.SecOwnerParty() == null))
			{
				MastRec.SecOwnerParty().Clear();
			}
			ShowSecOwner();
		}

		private void txtOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtOwnerID.Text) != MastRec.PartyID1)
			{
				cParty tParty = new cParty();
				cPartyController tCont = new cPartyController();
				if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text))))
				{
					e.Cancel = true;
					MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtOwnerID.Text = FCConvert.ToString(MastRec.PartyID1);
					return;
				}
				MastRec.PartyID1 = FCConvert.ToInt32(Math.Round(Conversion.Val(txtOwnerID.Text)));
				cParty temp = MastRec.SecOwnerParty();
				tCont.LoadParty(ref temp, FCConvert.ToInt32(Conversion.Val(txtOwnerID.Text)));
				ShowOwner();
				ShowAddress();
			}
		}

		private void txt2ndOwnerID_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txt2ndOwnerID.Text) != MastRec.PartyID2)
			{
				cParty tParty = new cParty();
				cPartyController tCont = new cPartyController();
				if (!tCont.LoadParty(ref tParty, FCConvert.ToInt32(Conversion.Val(txt2ndOwnerID.Text))))
				{
					e.Cancel = true;
					MessageBox.Show("That party doesn't exist.", "Invalid Party", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txt2ndOwnerID.Text = FCConvert.ToString(MastRec.PartyID2);
					return;
				}
				MastRec.PartyID2 = FCConvert.ToInt32(Math.Round(Conversion.Val(txt2ndOwnerID.Text)));
				cParty temp = MastRec.SecOwnerParty();
				tCont.LoadParty(ref temp, FCConvert.ToInt32(Conversion.Val(txt2ndOwnerID.Text)));
				ShowSecOwner();
			}
		}

		private void ResizeGridActivity()
		{
			int GridWidth = 0;
			GridWidth = gridActivity.WidthOriginal;
			gridActivity.ColWidth(CNSTGRIDACTIVITYCOLDATE, FCConvert.ToInt32(0.17 * GridWidth));
			gridActivity.ColWidth(CNSTGRIDACTIVITYCOLTYPE, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void gridActivity_DblClick(object sender, System.EventArgs e)
		{
			if (gridActivity.MouseRow > 0)
			{
				frmActivity.InstancePtr.Init(ref boolCantEdit, FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.MouseRow, CNSTGRIDACTIVITYCOLAUTOID))), lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY);
				if (!boolCantEdit)
				{
					alList.LoadList(lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY, lngCurrentAccount);
					FillActivityGrid();
				}
			}
		}

		private void SetupGridActivity()
		{
			gridActivity.ColHidden(CNSTGRIDACTIVITYCOLAUTOID, true);
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLDATE, "Date");
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLDESC, "Description");
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLTYPE, "Type");
		}

		private void gridActivity_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (!boolCantEdit)
			{
				switch (e.KeyCode)
				{
					case Keys.Delete:
						{
							KeyCode = 0;
							if (gridActivity.Rows > 0)
							{
								if (MessageBox.Show("This will delete this activity entry" + "\r\n" + "Do you want to continue?", "Delete Activity?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
								{
									clsActivity tAct;
									tAct = alList.GetActivityByID(FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.Row, CNSTGRIDACTIVITYCOLAUTOID))));
									if (!(tAct == null))
									{
										tAct.DeleteActivity();
									}
									alList.RemovebyID(FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.Row, CNSTGRIDACTIVITYCOLAUTOID))));
									gridActivity.RemoveItem(gridActivity.Row);
								}
							}
							break;
						}
					case Keys.Insert:
						{
							if (!boolCantEdit)
							{
								AddActivity();
							}
							break;
						}
				}
				//end switch
			}
		}

		private void gridActivity_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = gridActivity.MouseRow;
			if (mRow < 0)
			{
				gridActivity.Row = 0;
				gridActivity.Focus();
			}
		}

		private void AddActivity()
		{
			if (frmActivity.InstancePtr.Init(ref boolCantEdit, 0, lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY, 0, 0, lngCurrentAccount) > 0)
			{
				alList.LoadList(lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY, lngCurrentAccount);
				FillActivityGrid();
			}
		}

		private void FillActivityGrid()
		{
			gridActivity.Rows = 1;
			int lngRow;
			alList.MoveFirst();
			alList.MovePrevious();
			clsActivity tAct;
			clsAttachedDocuments dlAttachedDocs = new clsAttachedDocuments();
			while (alList.MoveNext() > 0)
			{
				tAct = alList.GetCurrentActivity();
				if (!(tAct == null))
				{
					gridActivity.Rows += 1;
					lngRow = gridActivity.Rows - 1;
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLAUTOID, FCConvert.ToString(tAct.ID));
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLDATE, Strings.Format(tAct.ActivityDate, "MM/dd/yyyy"));
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLDESC, tAct.Description);
					// check if there are attached documents
					dlAttachedDocs.LoadDocuments(tAct.ID, modCEConstants.CNSTCODETYPEACTIVITY.ToString(), modGlobalVariables.Statics.strCEDatabase);
					if (dlAttachedDocs.Count > 0)
					{
                        //FC:FINAL:IPI - index correction: ImageList starts with index 1 in VB6
                        //gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDACTIVITYCOLDESC, MDIParent.InstancePtr.CEImageList.Images[2]);
                        gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDACTIVITYCOLDESC, MDIParent.InstancePtr.CEImageList.Images[1]);
                    }
					gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDACTIVITYCOLDESC, FCGrid.PictureAlignmentSettings.flexPicAlignRightCenter);
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLTYPE, tAct.ActivityType);
				}
			}
		}

		private void FillGridAbutters()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				int lngAcct = 0;
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strSQL = "";
				if (lngCurrentAccount != 0)
				{
					clsLoad.OpenRecordset("select * from abutters where property1 = " + FCConvert.ToString(lngCurrentAccount) + " or property2 = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
					while (!clsLoad.EndOfFile())
					{
						GridAbutters.Rows += 1;
						lngRow = GridAbutters.Rows - 1;
						GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
						if (Conversion.Val(clsLoad.Get_Fields_Int32("property1")) != lngCurrentAccount)
						{
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("property1"))));
						}
						else
						{
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("property2"))));
						}
						GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLACCOUNT, FCConvert.ToString(lngAcct));
						strSQL = "select maplot from cemaster where ceaccount = " + FCConvert.ToString(lngAcct);
						clsTemp.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
						if (!clsTemp.EndOfFile())
						{
							GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLMAPLOT, FCConvert.ToString(clsTemp.Get_Fields_String("maplot")));
						}
						clsLoad.MoveNext();
					}
				}
				ForceAbuttersLastBlankRow();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridAbutters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridAbutters()
		{
			GridAbutters.Cols = 3;
			GridAbutters.ColHidden(CNSTGRIDABUTTERSCOLAUTOID, true);
			GridAbutters.Rows = 1;
			GridAbutters.TextMatrix(0, CNSTGRIDABUTTERSCOLMAPLOT, "Abutters");
			GridAbutters.TextMatrix(0, CNSTGRIDABUTTERSCOLACCOUNT, "Acct");
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPROPERTYEDITSECURITY, false))
			{
				GridAbutters.Editable = FCGrid.EditableSettings.flexEDNone;
				GridAbutters.Tag = (System.Object)("UNAUTHORIZED");
			}
		}

		private void ResizeGridAbutters()
		{
			int GridWidth = 0;
			GridWidth = GridAbutters.WidthOriginal;
			GridAbutters.ColWidth(CNSTGRIDABUTTERSCOLACCOUNT, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void SetupGridViolations()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLIDENTIFIER, "Identifier");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLISSUED, "Issued");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLSTATUS, "Status");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLTYPE, "Type");
			gridViolations.ColHidden(CNSTGRIDVIOLATIONSCOLAUTOID, true);
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONSTATUS) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			strTemp = "";
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTVIOLATIONSTATUSCLOSED) + ";Closed";
			gridViolations.ColComboList(CNSTGRIDVIOLATIONSCOLSTATUS, strTemp);
			strTemp = "";
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != "")
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			gridViolations.ColComboList(CNSTGRIDVIOLATIONSCOLTYPE, strTemp);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTVIOLATIONSSECURITY, false))
			{
				gridViolations.Tag = (System.Object)("UNAUTHORIZED");
				cmdAddViolation.Enabled = false;
			}
			else
			{
				gridViolations.Tag = (System.Object)("");
				if (modSecurity.ValidPermissions_6(this, modCEConstants.CNSTVIOLATIONSEDITSECURITY, false))
				{
					cmdAddViolation.Enabled = true;
				}
				else
				{
					cmdAddViolation.Enabled = false;
				}
			}
		}

		private void SetupGridPermits()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			int x;
			int GridWidth = 0;
			// .Cols = 6
			GridWidth = GridPermits.WidthOriginal;
			GridPermits.Rows = 1;
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
				GridPermits.ExtendLastCol = true;
			}
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLENDDATE, "Complete");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLSTARTDATE, "Start");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPLAN, "Plan");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITNUMBER, "Number");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLSTATUS, "Status");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, "App Date");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITFEE, "Fee");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITIDENTIFIER, "Identifier");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITVALUE, "Value");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPLAN, "Plan");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLTYPE, "Type");
			GridPermits.ColDataType(CNSTGRIDPERMITSCOLPLAN, FCGrid.DataTypeSettings.flexDTString);
			GridPermits.ColDataType(CNSTGRIDPERMITSCOLPERMITIDENTIFIER, FCGrid.DataTypeSettings.flexDTString);
			GridPermits.ColDataType(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, FCGrid.DataTypeSettings.flexDTDate);
			GridPermits.ColDataType(CNSTGRIDPERMITSCOLENDDATE, FCGrid.DataTypeSettings.flexDTDate);
			GridPermits.ColDataType(CNSTGRIDPERMITSCOLSTARTDATE, FCGrid.DataTypeSettings.flexDTDate);
			GridPermits.ColAlignment(CNSTGRIDPERMITSCOLPLAN, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridPermits.ColHidden(CNSTGRIDPERMITSCOLAUTOID, true);
			for (x = 1; x <= 10; x++)
			{
				GridPermits.ColData(x, x);
				GridPermits.ColWidth(x, FCConvert.ToInt32(gps.Get_ColWidth(x) * GridWidth));
				GridPermits.ColHidden(x, !gps.Get_ColShow(x));
			}
			// x
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPERMITSECURITY, false))
			{
				GridPermits.Tag = (System.Object)("UNAUTHORIZED");
				cmdAddPermit.Enabled = false;
			}
			else
			{
				GridPermits.Tag = (System.Object)("");
				if (modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITPERMITSECURITY, false))
				{
					cmdAddPermit.Enabled = true;
				}
				else
				{
					cmdAddPermit.Enabled = false;
				}
			}
			clsLoad.OpenRecordset("select * from SYSTEMCODES where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by code", modGlobalVariables.Statics.strCEDatabase);
			strTemp = "";
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "|";
				clsLoad.MoveNext();
			}
			strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTPERMITCOMPLETE) + ";Complete";
			GridPermits.ColComboList(CNSTGRIDPERMITSCOLSTATUS, strTemp);
			strTemp = "";
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by code", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + clsLoad.Get_Fields_Int32("ID") + ";" + clsLoad.Get_Fields_String("description") + "|";
				clsLoad.MoveNext();
			}
			if (fecherFoundation.Strings.Trim(strTemp) != "")
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			GridPermits.ColComboList(CNSTGRIDPERMITSCOLTYPE, strTemp);
			for (x = 1; x <= 10; x++)
			{
				MoveCol(ref x, gps.Get_ColOrder(x));
			}
			// x
		}

		private void MoveCol(ref int intType, int intCol)
		{
			int x;
			for (x = 1; x <= 10; x++)
			{
				if (FCConvert.ToInt32(GridPermits.ColData(x)) == intType)
				{
					GridPermits.ColPosition(x, intCol);
					break;
				}
			}
			// x
		}
		// Private Sub ResizeGridPermits()
		// Dim GridWidth As Long
		// With GridPermits
		// GridWidth = .Width
		// .ColWidth(CNSTGRIDPERMITSCOLPLAN) = 0.18 * GridWidth
		// .ColWidth(CNSTGRIDPERMITSCOLSTARTDATE) = 0.18 * GridWidth
		// .ColWidth(CNSTGRIDPERMITSCOLPERMITNUMBER) = 0.18 * GridWidth
		// .ColWidth(CNSTGRIDPERMITSCOLENDDATE) = 0.18 * GridWidth
		// End With
		// End Sub
		private void ResizeGridPermits()
		{
			int GridWidth;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			GridWidth = GridPermits.WidthOriginal;
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				if (!GridPermits.ColHidden(x))
				{
					GridPermits.ColWidth(x, FCConvert.ToInt32(gps.Get_ColWidth(FCConvert.ToInt32(GridPermits.ColData(x))) * GridWidth));
				}
			}
			// x
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
				GridPermits.ExtendLastCol = true;
			}
		}

		private void ResizeGridViolations()
		{
			int GridWidth = 0;
			GridWidth = gridViolations.WidthOriginal;
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLIDENTIFIER, FCConvert.ToInt32(0.15 * GridWidth));
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLTYPE, FCConvert.ToInt32(0.32 * GridWidth));
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLISSUED, FCConvert.ToInt32(0.17 * GridWidth));
		}

		private void FillGridViolations()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				int x;
				gridViolations.Rows = 1;
				rsLoad.OpenRecordset("select * from violations where violationaccount = " + FCConvert.ToString(lngCurrentAccount) + " and violationaccount > 0 order by dateviolationissued desc,violationidentifier", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					gridViolations.Rows += 1;
					lngRow = gridViolations.Rows - 1;
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLIDENTIFIER, FCConvert.ToString(rsLoad.Get_Fields_String("violationidentifier")));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus"))));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLTYPE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("violationtype"))));
					if (Information.IsDate(rsLoad.Get_Fields("dateviolationissued")))
					{
                        //FC:FINAL:MSH - i.issue #1710: replace Convert by Get_Fields_DateTime to avoid converting exceptions
                        //if (Convert.ToDateTime(rsLoad.Get_Fields("dateviolationissued")).ToOADate() != 0)
                        if (rsLoad.Get_Fields_DateTime("dateviolationissued").ToOADate() != 0)
						{
                            //FC:FINAL:MSH - i.issue #1710: display only date in short format (as in original)
							//gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLISSUED, FCConvert.ToString(rsLoad.Get_Fields("dateviolationissued")));
							gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLISSUED, rsLoad.Get_Fields_DateTime("dateviolationissued").ToShortDateString());
						}
					}
					if (cmbViolations.Text == "Show Current")
					{
						if ((Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus")) == modCEConstants.CNSTVIOLATIONSTATUSCLOSED) || (Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus")) == modCEConstants.CNSTVIOLATIONSTATUSRESOLVED))
						{
							gridViolations.RowHidden(lngRow, true);
						}
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridViolations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillGridPermits()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				int x;
				GridPermits.Rows = 1;
				clsLoad.OpenRecordset("select * from permits where account = " + FCConvert.ToString(lngCurrentAccount) + " and not deleted = 1 order by applicationdate", modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					GridPermits.Rows += 1;
					lngRow = GridPermits.Rows - 1;
					GridPermits.TextMatrix(lngRow, CNSTGRIDPERMITSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					for (x = 1; x <= 10; x++)
					{
						if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLSTARTDATE)
						{
							if (Information.IsDate(clsLoad.Get_Fields("startdate")))
							{
								if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("startdate")).ToOADate() != 0)
								{
									GridPermits.TextMatrix(lngRow, x, Strings.Format(clsLoad.Get_Fields_DateTime("startdate"), "MM/dd/yyyy"));
								}
							}
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLENDDATE)
						{
							if (Information.IsDate(clsLoad.Get_Fields("completiondate")))
							{
								if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("completiondate")).ToOADate() != 0)
								{
									GridPermits.TextMatrix(lngRow, x, Strings.Format(clsLoad.Get_Fields_DateTime("completiondate"), "MM/dd/yyyy"));
								}
							}
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLPLAN)
						{
							GridPermits.TextMatrix(lngRow, x, FCConvert.ToString(clsLoad.Get_Fields_String("plan")));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLSTATUS)
						{
							GridPermits.TextMatrix(lngRow, x, FCConvert.ToString(clsLoad.Get_Fields_Int32("APPDENCODE")));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLPERMITNUMBER)
						{
							GridPermits.TextMatrix(lngRow, x, Strings.Right(FCConvert.ToString(clsLoad.Get_Fields_Int32("PermitYear")), 2) + "-" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("permitnumber"))));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLPERMITIDENTIFIER)
						{
							GridPermits.TextMatrix(lngRow, x, FCConvert.ToString(clsLoad.Get_Fields_String("permitidentifier")));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLPERMITFEE)
						{
							GridPermits.TextMatrix(lngRow, x, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fee"))));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLPERMITVALUE)
						{
							GridPermits.TextMatrix(lngRow, x, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("estimatedvalue"))));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLTYPE)
						{
							GridPermits.TextMatrix(lngRow, x, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("permittype"))));
						}
						else if (FCConvert.ToInt32(GridPermits.ColData(x)) == CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE)
						{
							if (Information.IsDate(clsLoad.Get_Fields("applicationdate")))
							{
								if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("applicationdate")).ToOADate() != 0)
								{
									GridPermits.TextMatrix(lngRow, x, Strings.Format(clsLoad.Get_Fields_DateTime("applicationdate"), "MM/dd/yyyy"));
								}
							}
						}
					}
					// x
					if (cmbPermits.Text == "Show Current")
					{
						if (Conversion.Val(clsLoad.Get_Fields_Int32("APPDENCODE")) == modCEConstants.CNSTPERMITCOMPLETE)
						{
							GridPermits.RowHidden(lngRow, true);
						}
					}
					clsLoad.MoveNext();
				}
				// CheckForViolations
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridPermits", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridOpens()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsCodes = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			GridOpens.Cols = 7;
			GridOpens.Rows = 1;
			GridOpens.ColHidden(CNSTGRIDOPENSCOLAUTOID, true);
			// .ColHidden(CNSTGRIDOPENSCOLUSEVALUE) = True
			// .ColHidden(CNSTGRIDOPENSCOLUSEDATE) = True
			GridOpens.ColHidden(CNSTGRIDOPENSCOLVALUEFORMAT, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMANDATORY, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMIN, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMAX, true);
			GridOpens.ColDataType(CNSTGRIDOPENSCOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
            int GridWidth = 0;
            GridWidth = GridOpens.WidthOriginal;
            GridOpens.ColWidth(CNSTGRIDOPENSCOLDESC, FCConvert.ToInt32(0.45 * GridWidth));
            GridOpens.ColWidth(CNSTGRIDOPENSCOLVALUE, FCConvert.ToInt32(0.5 * GridWidth));
            GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLDESC, "Item");
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLVALUE, "Value");
			// .TextMatrix(0, CNSTGRIDOPENSCOLDATE) = "Date"
			//GridOpens.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLVALUE, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLDATE) = flexAlignCenterCenter
			clsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				GridOpens.Rows += 1;
				lngRow = GridOpens.Rows - 1;
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(false));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("MINVALUE"))));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("maxvalue"))));
				// If Not clsLoad.Fields("usevalue") Then
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLVALUE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = False
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = True
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("valueformat"))));
				if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					// get the list of drop downs
					strTemp = "";
					clsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + clsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by code", modGlobalVariables.Statics.strCEDatabase);
					while (!clsCodes.EndOfFile())
					{
						strTemp += "#" + clsCodes.Get_Fields("code") + ";" + clsCodes.Get_Fields_String("description") + "|";
						clsCodes.MoveNext();
					}
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					}
					GridOpens.RowData(lngRow, strTemp);
				}
				else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
					GridOpens.RowData(lngRow, "Yes|No");
				}
				// End If
				// If Not clsLoad.Fields("usedate") Then
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = False
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLDATE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = True
				// End If
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("mandatory")))
				{
					GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(true));
				}
				clsLoad.MoveNext();
			}
			if (modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPROPERTYEDITSECURITY, false))
			{
				GridOpens.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				GridOpens.Tag = (System.Object)("");
			}
			else
			{
				GridOpens.Editable = FCGrid.EditableSettings.flexEDNone;
				GridOpens.Tag = (System.Object)("UNAUTHORIZED");
			}
		}

		private void LoadGridOpens()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				string[] strAry = null;
				string[] strAry2 = null;
				if (lngCurrentAccount != 0)
				{
					clsLoad.OpenRecordset("select * from USERcodevalues where account = " + FCConvert.ToString(lngCurrentAccount) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
						{
							if (clsLoad.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
								GridOpens.ComboList = "";
								if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
								{
									// not allowing type date at this time
									if (Information.IsDate(clsLoad.Get_Fields("datevalue")))
									{
										if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("DateValue")).ToOADate() != 0)
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(clsLoad.Get_Fields_DateTime("datevalue"), "MM/dd/yyyy"));
										}
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
								{
									if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("BooleanValue")))
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "Yes");
									}
									else
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "No");
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
								{
									GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(lngRow));
									// .TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) = Val(clsLoad.Fields("dropdownvalue"))
									GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE, Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")));
									strAry = Strings.Split(Strings.Replace(FCConvert.ToString(GridOpens.RowData(lngRow)), "#", "", 1, -1, CompareConstants.vbTextCompare), "|", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
									{
										strAry2 = Strings.Split(strAry[x], ";", -1, CompareConstants.vbTextCompare);
										if (Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")) == Conversion.Val(strAry2[0]))
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, strAry2[1]);
										}
									}
									// x
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))), "#,###,###,##0"));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(clsLoad.Get_Fields_String("textvalue")));
								}
								// End If
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
								// If IsDate(clsLoad.Fields("DateValue")) Then
								// If clsLoad.Fields("datevalue") <> 0 Then
								// .TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE) = Format(clsLoad.Fields("datevalue"), "MM/dd/yyyy")
								// End If
								// End If
								// End If
							}
						}
						// lngRow
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveGridOpens()
		{
			bool SaveGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				bool boolMatch = false;
				string strTemp = "";
				int lngTemp = 0;
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				// shouldn't be able to get to this point without an account number
				SaveGridOpens = false;
				if (lngCurrentAccount != 0)
				{
					clsSave.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(lngCurrentAccount) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
					{
						boolMatch = false;
						if (!clsSave.EndOfFile())
						{
							if (clsSave.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								boolMatch = true;
							}
						}
						if (boolMatch)
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
						clsSave.Set_Fields("ACCOUNT", lngCurrentAccount);
						clsSave.Set_Fields("codetype", modCEConstants.CNSTCODETYPEPROPERTY);
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))));
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveGridOpens;
							}
						}
						dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
						dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
						if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
						{
							// not allowing type date at this time
							if (Information.IsDate(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE)))
							{
								clsSave.Set_Fields("datevalue", Strings.Format(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), "MM/dd/yyyy"));
							}
							else
							{
								clsSave.Set_Fields("datevalue", 0);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == "Yes")
							{
								clsSave.Set_Fields("booleanvalue", true);
							}
							else
							{
								clsSave.Set_Fields("booleanvalue", false);
							}
							// clsSave.Fields("booleanvalue") = CBool(.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE))
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							dblTemp = Conversion.Val(strTemp);
							if (dblTemp != 0)
							{
								if (dblMin != 0 && dblMin > dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (dblTemp == 0)
								{
									if (dblMin > dblTemp || dblMax < dblTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", dblTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							clsSave.Set_Fields("dropdownvalue", FCConvert.ToString(Conversion.Val(GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE))));
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							if (lngTemp != 0)
							{
								if (dblMin != 0 && dblMin > lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (lngTemp == 0)
								{
									if (dblMin > lngTemp || dblMax < lngTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", lngTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
						{
							clsSave.Set_Fields("textvalue", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE));
						}
						// End If
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
						// If IsDate(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE)) Then
						// clsSave.Fields("datevalue") = Format(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE), "MM/dd/yyyy")
						// Else
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY) Then
						// MsgBox "Item " & .TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) & " is a mandatory field" & vbNewLine & "You must enter a valid date in this field to save", vbExclamation, "Invalid Date"
						// Exit Function
						// End If
						// End If
						// Else
						// clsSave.Fields("datevalue") = 0
						// End If
						clsSave.Update();
					}
					// lngRow
				}
				SaveGridOpens = true;
				return SaveGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Eror Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridOpens;
		}

		private void ResizeGridOpens()
		{
			int GridWidth = 0;
			GridWidth = GridOpens.WidthOriginal;
			GridOpens.ColWidth(CNSTGRIDOPENSCOLDESC, FCConvert.ToInt32(0.45 * GridWidth));
			GridOpens.ColWidth(CNSTGRIDOPENSCOLVALUE, FCConvert.ToInt32(0.5 * GridWidth));
		}

		private void frmMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMaster properties;
			//frmMaster.FillStyle	= 0;
			//frmMaster.ScaleWidth	= 9300;
			//frmMaster.ScaleHeight	= 7890;
			//frmMaster.LinkTopic	= "Form2";
			//frmMaster.LockControls	= -1  'True;
			//frmMaster.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			gps.LoadCols();
			boolDataChanged = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SetTRIOColors Me
			lblViewOnly.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblViewOnly.Font = FCUtils.FontChangeSize(lblViewOnly.Font, 14);
			// lblViolation.ForeColor = TRIOCOLORRED
			// lblViolation.FontSize = 16
			boolCantEdit = false;
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPROPERTYEDITSECURITY))
			{
				DisableREControls();
				mnuChangeREAssoc.Enabled = false;
				cmdDeleteAccount.Enabled = false;
				cmdSave.Enabled = false;
				mnuSaveExit.Enabled = false;
				cmdAddActivity.Enabled = false;
				boolCantEdit = true;
			}
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPERMITSECURITY, false) || !modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITPERMITSECURITY, false))
			{
				cmdAddPermit.Enabled = false;
			}
			// If CECustom.GetDataFromRE And lngREAccount > 0 Then
			// If lngREAccount > 0 Then
			// DisableREControls
			// Else
			// re doesn't have a frontage field
			// txtFrontage.Enabled = False
			// End If
			// End If
		}

		private void InitializeScreen()
		{
			SetupGridAbutters();
			SetupGridPreviousMaps();
			SetupGridOpens();
			SetupGridPermits();
			SetupGridViolations();
			SetupBPGrid();
			SetupGridActivity();
			LoadInfo();
		}

		private void frmMaster_Resize(object sender, System.EventArgs e)
		{
			ResizeGridPreviousMaps();
			ResizeGridOpens();
			ResizeGridAbutters();
			ResizeGridPermits();
			ResizeGridViolations();
			ResizeBPGrid();
			ResizeGridActivity();
		}

		private void GridAbutters_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			int lngAcct;
			if (FCConvert.ToString(GridAbutters.Tag) != "UNAUTHORIZED")
			{
				lngRow = GridAbutters.MouseRow;
				if (GridAbutters.Rows > 0)
				{
					if (lngRow == GridAbutters.Rows - 1)
					{
						if (Conversion.Val(GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLACCOUNT)) == 0)
						{
							// look for new
							GetAbutterAccount();
						}
					}
				}
			}
		}

		private void GetAbutterAccount()
		{
			int lngAcct;
			int lngRow;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			lngAcct = 0;
			try
			{
				lngAcct = frmSearchAccounts.InstancePtr.Init("Choose Abutter");
				if (lngAcct != 0)
				{
					ForceAbuttersLastBlankRow();
					lngRow = GridAbutters.Rows - 1;
					GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLACCOUNT, FCConvert.ToString(lngAcct));
					strSQL = "Select maplot from cemaster where ceaccount = " + FCConvert.ToString(lngAcct);
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLMAPLOT, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("maplot"))));
					}
					ForceAbuttersLastBlankRow();
				}
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetAbutterAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void GridAbutters_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridAbutters.Row < 1)
							return;
						GridAbutters.RemoveItem(GridAbutters.Row);
						ForceAbuttersLastBlankRow();
						break;
					}
				case Keys.Return:
					{
						KeyCode = 0;
						if (GridAbutters.Rows > 0)
						{
							if (GridAbutters.Row == GridAbutters.Rows - 1)
							{
								if (Conversion.Val(GridAbutters.TextMatrix(GridAbutters.Row, CNSTGRIDABUTTERSCOLACCOUNT)) == 0)
								{
									// look for new
									GetAbutterAccount();
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void GridOpens_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			Row = GridOpens.MouseRow;
			Col = GridOpens.MouseCol;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDOPENSCOLVALUE:
						{
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEVALUE)) Then
							if (!boolCantEdit)
							{
								GridOpens.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(Row));
							if (Conversion.Val(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
							{
								GridOpens.EditMask = "00/00/0000";
							}
							else
							{
								GridOpens.EditMask = "";
							}
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							// Case CNSTGRIDOPENSCOLDATE
							// GridOpens.ComboList = ""
							// GridOpens.EditMask = "00/00/0000"
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEDATE)) Then
							// GridOpens.Editable = flexEDKbdMouse
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							break;
						}
				}
				//end switch
			}
		}

		private void GridOpens_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridOpens.GetFlexRowIndex(e.RowIndex);
            int col = GridOpens.GetFlexColIndex(e.ColumnIndex);

            if (row < 1)
				return;
			string strTemp = "";
			string[] strAry = null;
			GridOpens.EditMask = "";
			if (col == CNSTGRIDOPENSCOLVALUE)
			{
				if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					strTemp = GridOpens.EditText;
					if (Information.IsDate(strTemp))
					{
						strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					}
					else
					{
						strTemp = "";
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strAry = Strings.Split(strTemp, ".", -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Format(strAry[0], "#,###,###,##0");
					if (Information.UBound(strAry, 1) > 0)
					{
						strTemp += "." + strAry[1];
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					strTemp = GridOpens.ComboData();
					GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, Conversion.Val(strTemp));
				}
				// Case CNSTGRIDOPENSCOLDATE
			}
		}

		private void GridPermits_DblClick(object sender, System.EventArgs e)
		{
			int lngID;
			if (GridPermits.MouseRow < 1)
				return;
			if (FCConvert.ToString(GridPermits.Tag) == "UNAUTHORIZED")
				return;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridPermits.TextMatrix(GridPermits.MouseRow, CNSTGRIDPERMITSCOLAUTOID))));
			frmPermits.InstancePtr.Init(lngID, lngCurrentAccount);
			// may be changes, update the grid
			FillGridPermits();
			FillGridViolations();
		}
		// Private Sub GridPhones_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
		// Dim mRow As Long
		// mRow = GridPhones.MouseRow
		// If mRow < 0 Then
		// GridPhones.Row = 0
		// GridPhones.SetFocus
		// End If
		// End Sub
		//
		// Private Sub GridPhones_RowColChange()
		// If GridPhones.Col = CNSTGRIDPHONESCOLDESC And GridPhones.Row = GridPhones.Rows - 1 Then
		// GridPhones.TabBehavior = flexTabControls
		// Else
		// GridPhones.TabBehavior = flexTabCells
		// End If
		// End Sub
		private void GridPreviousMaps_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			ForceMapsLastBlankRow();
		}

		private void GridPreviousMaps_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridPreviousMaps.Row > 0)
						{
							GridPreviousMaps.RemoveItem(GridPreviousMaps.Row);
							ForceMapsLastBlankRow();
						}
						break;
					}
			}
			//end switch
		}

		private void ForceMapsLastBlankRow()
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = (GridPreviousMaps.Rows - 1); x >= 1; x--)
			{
				if (fecherFoundation.Strings.Trim(GridPreviousMaps.TextMatrix(x, CNSTGRIDPREVIOUSMAPSCOLMAPLOT)) == string.Empty)
				{
					GridPreviousMaps.RemoveItem(x);
				}
			}
			// x
			GridPreviousMaps.Rows += 1;
		}

		private void ForceAbuttersLastBlankRow()
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = (GridAbutters.Rows - 1); x >= 1; x--)
			{
				if (Conversion.Val(GridAbutters.TextMatrix(x, CNSTGRIDABUTTERSCOLACCOUNT)) == 0)
				{
					GridAbutters.RemoveItem(x);
				}
			}
			// x
			GridAbutters.Rows += 1;
		}

		private void gridViolations_DblClick(object sender, System.EventArgs e)
		{
			if (gridViolations.MouseRow > 0)
			{
				if (!(fecherFoundation.Strings.LCase(FCConvert.ToString(gridViolations.Tag)) == "unauthorized"))
				{
					bool boolRonly = false;
					boolRonly = !cmdAddViolation.Enabled;
					frmViolation.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(gridViolations.TextMatrix(gridViolations.MouseRow, CNSTGRIDVIOLATIONSCOLAUTOID))), boolRonly);
					FillGridViolations();
				}
			}
		}

		private void ImgComment_Click(object sender, System.EventArgs e)
		{
			if (cmdComment.Enabled)
			{
				mnuComment_Click();
			}
		}

		private void mnuAddPermit_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngID;
			if (lngCurrentAccount == 0)
			{
				MessageBox.Show("This account must be saved before you can add a permit", "Cannot Add", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			lngID = frmPermits.InstancePtr.Init(0, lngCurrentAccount);
			if (lngID > 0)
			{
				// add it to the grid
				FillGridPermits();
			}
		}

		private void mnuAddViolation_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentAccount == 0)
			{
				MessageBox.Show("This account must be saved before you can add a violation", "Cannot Add", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmViolation.InstancePtr.Init(0, false, 0, lngCurrentAccount);
			FillGridViolations();
		}

		private void mnuChangeREAssoc_Click(object sender, System.EventArgs e)
		{
			int lngReturn;
			lngReturn = frmSearchAccounts.InstancePtr.Init("Associated Real Estate Account", true, true, lngCurrentAccount);
			if (lngReturn != MastRec.RSAccount)
			{
				if (lngReturn > 0)
				{
					MastRec.RSAccount = lngReturn;
					if (modGlobalVariables.Statics.CECustom.GetDataFromRE)
					{
						MastRec.Load(lngReturn, false);
						MastRec.CEAccount = lngCurrentAccount;
						LoadInfo(true);
					}
					else
					{
						if (MessageBox.Show("Do you want to replace the current information with that from Real Estate?", "Replace Information?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							MastRec.Load(lngReturn, false);
							MastRec.CEAccount = lngCurrentAccount;
							LoadInfo(true);
						}
					}
				}
				else if (lngReturn == 0)
				{
					MastRec.RSAccount = lngReturn;
				}
			}
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			// If frmComment.Init("CE", "Comments", "Comment", "Account", lngCurrentAccount) Then
			// Set ImgComment.Picture = Nothing
			// Else
			// Set ImgComment.Picture = MDIParent.ImageList2.ListImages(1).Picture
			// End If
			bool boolNonEditable = false;
			if (modSecurity.ValidPermissions_8(this, modCEConstants.CNSTPROPERTYEDITSECURITY, false))
			{
				boolNonEditable = false;
			}
			else
			{
				boolNonEditable = true;
			}
			if (lngCurrentAccount != 0)
			{
				if (!frmGlobalComment.InstancePtr.Init("CE", "Comments", "Comment", "Account", lngCurrentAccount, "Comment", "", 0, boolNonEditable, true))
				{
					ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
				}
				else
				{
					ImgComment.Image = null;
				}
			}
			else
			{
				MessageBox.Show("You must save and create the account before you can assign it a comment", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void mnuComment_Click()
		{
			mnuComment_Click(mnuComment, new System.EventArgs());
		}

		private void mnuCreateDocument_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentAccount < 1)
			{
				MessageBox.Show("You must create the account before you can create documents for it.", "Save Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmRTBDocument.InstancePtr.Init(lngCurrentAccount, lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY);
		}

		private void mnuDeleteAccount_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			if (lngCurrentAccount < 1)
			{
				// not a permanent account yet
				if (lngCurrentAccount == 0)
				{
					// not even an account yet
					mnuExit_Click();
					return;
				}
				else
				{
					// don't mark deleted, just delete it
					if (MessageBox.Show("This account is a pending account" + "\r\n" + "It will be permanently deleted not marked as deleted" + "\r\n" + "Do you want to delete it?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (modMain.DeleteAccount(ref lngCurrentAccount))
						{
							MessageBox.Show("Account Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
							mnuExit_Click();
							return;
						}
					}
				}
			}
			else
			{
				if (MessageBox.Show("Are you sure you want to delete this account?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (modMain.DeleteAccount(ref lngCurrentAccount))
					{
						MessageBox.Show("Account Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
						mnuExit_Click();
						return;
					}
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}
		// vbPorter upgrade warning: lngAccountToLoad As int	OnWrite(int, string)
		public void Init(int lngAccountToLoad = 0, bool boolShowPermits = false)
		{
			if (lngAccountToLoad != 0)
			{
				lngCurrentAccount = lngAccountToLoad;
				lblAccount.Text = FCConvert.ToString(lngCurrentAccount);
			}
			else
			{
				// create a new one
				lngCurrentAccount = 0;
				lngREAccount = 0;
				lblAccount.Text = "";
			}
			//FC:FINAL:MSH - Form_Load in original app will be executed on changing  lblAccount.Text value
			this.LoadForm();
			InitializeScreen();
			if (boolShowPermits)
			{
				SSTab1.SelectedIndex = 1;
			}
			else
			{
				SSTab1.SelectedIndex = 0;
			}
			this.Show(App.MainForm);
		}

		private void SetupGridPreviousMaps()
		{
			GridPreviousMaps.Rows = 1;
			GridPreviousMaps.TextMatrix(0, CNSTGRIDPREVIOUSMAPSCOLMAPLOT, "Map/Lot");
			GridPreviousMaps.TextMatrix(0, CNSTGRIDPREVIOUSMAPSCOLDATE, "Date");
			GridPreviousMaps.ColAlignment(CNSTGRIDPREVIOUSMAPSCOLMAPLOT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridPreviousMaps()
		{
			int GridWidth = 0;
			GridWidth = GridPreviousMaps.WidthOriginal;
			GridPreviousMaps.ColWidth(CNSTGRIDPREVIOUSMAPSCOLMAPLOT, FCConvert.ToInt32(0.55 * GridWidth));
		}

		private void LoadInfo(bool boolReload = false)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			// Dim strMTable As String
			string strSQL = "";
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!boolReload)
				{
					MastRec.Clear();
					lblRE.Visible = false;
					lblREAccount.Text = "";
					if (lngCurrentAccount == 0)
					{
						ImgComment.Image = null;
						return;
					}
					// strMTable = CECustom.MasterTable
					clsLoad.OpenRecordset("select * from comments where account = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
					if (clsLoad.EndOfFile())
					{
						ImgComment.Image = null;
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("comment"))) != string.Empty)
						{
							ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
						}
						else
						{
							ImgComment.Image = null;
						}
					}
					if (MastRec.Load(lngCurrentAccount, true, true) != lngCurrentAccount)
					{
						return;
					}
					modCE.SaveCERegistryKey("CELastAccountNumber", FCConvert.ToString(lngCurrentAccount));
				}
				if (MastRec.RSAccount > 0)
				{
					if (modGlobalVariables.Statics.CECustom.GetDataFromRE)
					{
						DisableREControls();
					}
					lblRE.Visible = true;
					lblREAccount.Text = FCConvert.ToString(MastRec.RSAccount);
				}
				else
				{
					lblRE.Visible = false;
					lblREAccount.Text = "";
				}
				if (MastRec.Deleted)
				{
					lblViewOnly.Visible = true;
					lblViewOnly.Text = "Deleted - View Only";
				}
				lngREAccount = MastRec.RSAccount;
				// txtName.Text = MastRec.OwnerName
				// txtSecOwner.Text = MastRec.SecOwner
				ShowOwner();
				ShowAddress();
				ShowSecOwner();
				// txtName.Text = MastRec.OwnerFirst
				// txtSecOwner.Text = MastRec.SecOwnerFirst
				// txtMiddle.Text = MastRec.OwnerMiddle
				// txtLast.Text = MastRec.OwnerLast
				// txtDesig.Text = MastRec.OwnerDesig
				// txtSecMiddle.Text = MastRec.SecOwnerMiddle
				// txtSecLast.Text = MastRec.SecOwnerLast
				// txtSecDesig.Text = MastRec.SecOwnerDesig
				// 
				// txtAddress1.Text = MastRec.Address1
				// txtAddress2.Text = MastRec.Address2
				// txtCity.Text = MastRec.City
				// txtState.Text = MastRec.State
				// txtZip.Text = MastRec.Zip
				// txtZip4.Text = MastRec.Zip4
				txtStreetNum.Text = MastRec.StreetNum;
				txtApt.Text = MastRec.Apt;
				txtStreet.Text = MastRec.StreetName;
				// txtRef1.Text = MastRec.Ref1
				// txtRef2.Text = MastRec.Ref2
				txtAcres.Text = FCConvert.ToString(MastRec.Acres);
				txtNeighborhood.Text = MastRec.Neighborhood;
				txtZone.Text = MastRec.Zone;
				// txtEmail.Text = MastRec.Email
				txtMapLot.Text = MastRec.MapLot;
				txtFrontage.Text = FCConvert.ToString(MastRec.Frontage);
				LoadMapLots();
				if (!boolCantEdit)
				{
					cmdAddActivity.Enabled = true;
				}
				else
				{
					cmdAddActivity.Enabled = false;
				}
				gridActivity.Enabled = true;
				mnuPrintActivity.Enabled = true;
				mnuPrintActivityWithDetail.Enabled = true;
				if (!boolReload)
				{
					FillGridAbutters();
					LoadGridOpens();
					FillGridPermits();
					FillGridViolations();
					FillBPGrid();
					alList.Clear();
					alList.LoadList(lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY, lngCurrentAccount);
					FillActivityGrid();
				}
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadMapLots()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				string strMTable;
				string strSQL = "";
				int lngRow;
				string strDB = "";
				if (lngCurrentAccount == 0)
				{
					return;
				}
				GridPreviousMaps.Rows = 1;
				strMTable = modGlobalVariables.Statics.CECustom.MapLotTable;
				if (modGlobalVariables.Statics.CECustom.GetDataFromRE && MastRec.RSAccount > 0)
				{
					strSQL = "Select * from maplot where account = " + FCConvert.ToString(MastRec.RSAccount) + " order by line";
					strDB = modGlobalVariables.Statics.strREDatabase;
				}
				else
				{
					strSQL = "Select * from cemaplot where account = " + FCConvert.ToString(lngCurrentAccount) + " order by line";
					strDB = modGlobalVariables.Statics.strCEDatabase;
				}
				clsLoad.OpenRecordset(strSQL, strDB);
				while (!clsLoad.EndOfFile())
				{
					GridPreviousMaps.Rows += 1;
					lngRow = GridPreviousMaps.Rows - 1;
					GridPreviousMaps.TextMatrix(lngRow, CNSTGRIDPREVIOUSMAPSCOLMAPLOT, FCConvert.ToString(clsLoad.Get_Fields_String("maplot")));
					GridPreviousMaps.TextMatrix(lngRow, CNSTGRIDPREVIOUSMAPSCOLDATE, FCConvert.ToString(clsLoad.Get_Fields_String("MPdate")));
					clsLoad.MoveNext();
				}
				GridPreviousMaps.Rows += 1;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadMapLots", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Private Function CreateNewPendingAccount() As Long
		// On Error GoTo ErrorHandler
		//
		//
		// Dim clsLoad As New clsdrwrapper
		// Dim lngAcctToUse As Long
		//
		//
		// CreateNewPendingAccount = 0
		// Call clsLoad.OpenRecordset("select rsaccount from " & CECustom.MasterTable & " where rsaccount < 0 order by rsaccount", strCEDatabase)
		// lngAcctToUse = -1
		// If Not clsLoad.EndOfFile Then
		// lngAcctToUse = Val(clsLoad.Fields("rsaccount")) - 1
		// End If
		//
		// CreateNewPendingAccount = lngAcctToUse
		// Exit Function
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In CreateNewPendingAccount", vbCritical, "Error"
		// End Function
		private bool SaveInfo()
		{
			bool SaveInfo = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			string strTemp = "";
			int lngAcct;
			bool boolGotLock;
			bool boolOK;
			int lngRet;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveInfo = false;
				GridOpens.Row = 0;
				GridAbutters.Row = 0;
				GridPreviousMaps.Row = 0;
				BPGrid.Row = 0;
				boolGotLock = false;
				boolOK = true;
				// MastRec.OwnerFirst = Trim(txtName.Text)
				// MastRec.OwnerMiddle = Trim(txtMiddle.Text)
				// MastRec.OwnerLast = Trim(txtLast.Text)
				// MastRec.OwnerDesig = Trim(txtDesig.Text)
				// MastRec.OwnerName = Trim(txtName.Text)
				// MastRec.SecOwner = Trim(txtSecOwner.Text)
				// MastRec.SecOwnerFirst = Trim(txtSecOwner.Text)
				// MastRec.SecOwnerMiddle = Trim(txtSecMiddle.Text)
				// MastRec.SecOwnerLast = Trim(txtSecLast.Text)
				// MastRec.SecOwnerDesig = Trim(txtSecDesig.Text)
				// MastRec.Address1 = Trim(txtAddress1.Text)
				// MastRec.Address2 = Trim(txtAddress2.Text)
				// MastRec.City = Trim(txtCity.Text)
				// MastRec.State = Trim(txtState.Text)
				// MastRec.Zip = txtZip.Text
				// MastRec.Zip4 = txtZip4.Text
				MastRec.StreetNum = FCConvert.ToString(Conversion.Val(txtStreetNum.Text));
				MastRec.Apt = fecherFoundation.Strings.Trim(txtApt.Text);
				MastRec.StreetName = fecherFoundation.Strings.Trim(txtStreet.Text);
				// MastRec.Ref1 = txtRef1.Text
				// MastRec.Ref2 = txtRef2.Text
				MastRec.Acres = Conversion.Val(txtAcres.Text);
				MastRec.Zone = fecherFoundation.Strings.Trim(txtZone.Text);
				MastRec.Neighborhood = fecherFoundation.Strings.Trim(txtNeighborhood.Text);
				MastRec.Frontage = Conversion.Val(txtFrontage.Text);
				MastRec.MapLot = fecherFoundation.Strings.Trim(txtMapLot.Text);
				// MastRec.Email = txtEmail.Text
				lngRet = MastRec.Save();
				if (lngRet > 0)
				{
					lngCurrentAccount = lngRet;
				}
				else
				{
					return SaveInfo;
				}
				lblAccount.Text = FCConvert.ToString(lngCurrentAccount);
				boolOK = boolOK && SaveGridPreviousMaps();
				// End If
				boolOK = boolOK && SaveGridOpens();
				boolOK = boolOK && SaveGridAbutters();
				// boolOK = boolOK And SaveGridPhones
				boolOK = boolOK && SaveBPGrid();
				SaveInfo = boolOK;
				if (boolOK)
				{
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private bool SaveGridPreviousMaps()
		{
			bool SaveGridPreviousMaps = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int x;
				int lngRow;
				string strTemp = "";
				string strSQL = "";
				SaveGridPreviousMaps = false;
				clsSave.Execute("delete from CEMaplot where account = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
				x = 0;
				for (lngRow = 1; lngRow <= GridPreviousMaps.Rows - 1; lngRow++)
				{
					if (fecherFoundation.Strings.Trim(GridPreviousMaps.TextMatrix(lngRow, CNSTGRIDPREVIOUSMAPSCOLMAPLOT)) != string.Empty)
					{
						x += 1;
						strTemp = fecherFoundation.Strings.Trim(GridPreviousMaps.TextMatrix(lngRow, CNSTGRIDPREVIOUSMAPSCOLMAPLOT));
						strTemp = Strings.Replace(strTemp, "'", "", 1, -1, CompareConstants.vbTextCompare);
						strSQL = "insert into cemaplot (account,maplot,mpdate,line) values (";
						strSQL += FCConvert.ToString(lngCurrentAccount);
						strSQL += ",'" + strTemp + "'";
						strTemp = fecherFoundation.Strings.Trim(GridPreviousMaps.TextMatrix(lngRow, CNSTGRIDPREVIOUSMAPSCOLDATE));
						if (Information.IsDate(strTemp))
						{
							strSQL += ",'" + strTemp + "'";
						}
						else
						{
							strSQL += ",''";
						}
						strSQL += "," + FCConvert.ToString(x);
						strSQL += ")";
						clsSave.Execute(strSQL, modGlobalVariables.Statics.strCEDatabase);
					}
				}
				// lngRow
				SaveGridPreviousMaps = true;
				return SaveGridPreviousMaps;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridPreviousMaps", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridPreviousMaps;
		}

		private bool SaveGridAbutters()
		{
			bool SaveGridAbutters = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				string strTemp = "";
				string strSQL = "";
				SaveGridAbutters = false;
				if (lngCurrentAccount != 0)
				{
					clsSave.Execute("delete from abutters where property1 = " + FCConvert.ToString(lngCurrentAccount) + " or property2 = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridAbutters.Rows - 1; lngRow++)
					{
						if (Conversion.Val(GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLACCOUNT)) > 0)
						{
							strSQL = "Insert into abutters (property1,property2) values (";
							strSQL += FCConvert.ToString(lngCurrentAccount);
							strSQL += "," + FCConvert.ToString(Conversion.Val(GridAbutters.TextMatrix(lngRow, CNSTGRIDABUTTERSCOLACCOUNT)));
							strSQL += ")";
							clsSave.Execute(strSQL, modGlobalVariables.Statics.strCEDatabase);
						}
					}
					// lngRow
				}
				SaveGridAbutters = true;
				return SaveGridAbutters;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridAbutters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridAbutters;
		}

		private void DisableREControls()
		{
			int x;
			for (x = 0; x <= this.Controls.Count - 1; x++)
			{
				if (FCConvert.ToString(this.Controls[x].Tag) == "RE")
				{
					this.Controls[x].Enabled = false;
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Controls[x].Tag)) == "REGRID")
				{
					(this.Controls[x] as FCGrid).Editable = FCGrid.EditableSettings.flexEDNone;
					this.Controls[x].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
					(this.Controls[x] as FCGrid).Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, (this.Controls[x] as FCGrid).Rows - 1, (this.Controls[x] as FCGrid).Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
				}
			}
			// x
		}

		private void EnableREControls()
		{
			int x;
			for (x = 0; x <= this.Controls.Count - 1; x++)
			{
				if (FCConvert.ToString(this.Controls[x].Tag) == "RE")
				{
					this.Controls[x].Enabled = true;
				}
				else if (fecherFoundation.Strings.UCase(FCConvert.ToString(this.Controls[x].Tag)) == "REGRID")
				{
					(this.Controls[x] as FCGrid).Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					this.Controls[x].ForeColor = Color.Black;
					(this.Controls[x] as FCGrid).Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, (this.Controls[x] as FCGrid).Rows - 1, (this.Controls[x] as FCGrid).Cols - 1, Color.Black);
				}
			}
			// x
		}

		private void mnuPrintAbutters_Click(object sender, System.EventArgs e)
		{
			clsSQLStatement SQLStatement;
			SQLStatement = GetReportSQL("CEMaster");
			rptAbutters.InstancePtr.Init(ref SQLStatement);
		}

		private void mnuPrintActivity_Click(object sender, System.EventArgs e)
		{
			string strTemp;
			strTemp = "Account " + FCConvert.ToString(lngCurrentAccount);
			rptActivity.InstancePtr.Init(lngCurrentAccount, modCEConstants.CNSTCODETYPEPROPERTY, false, strTemp, 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void mnuPrintActivityWithDetail_Click(object sender, System.EventArgs e)
		{
			string strTemp;
			strTemp = "Account " + FCConvert.ToString(lngCurrentAccount);
			rptActivity.InstancePtr.Init(lngCurrentAccount, modCEConstants.CNSTCODETYPEPERMIT, true, strTemp, 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void mnuPrintEnvelope_Click(object sender, System.EventArgs e)
		{
			int lngID;
			string strTemp;
			bool boolTemp;
			clsSQLStatement SQLStatement;
			SQLStatement = GetReportSQL("CEMaster");
			lngID = frmChooseEnvelope.InstancePtr.Init();
			strTemp = modMain.GetCEDBRegistryEntry("UseReturnAddress", "CE", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
			boolTemp = true;
			if (fecherFoundation.Strings.LCase(strTemp) == "false")
			{
				boolTemp = false;
			}
			if (lngID >= 0)
			{
				rptEnvelope10.InstancePtr.Init(ref SQLStatement, boolTemp, lngID);
			}
		}

		private clsSQLStatement GetReportSQL(string strDefaultTables)
		{
			clsSQLStatement GetReportSQL = null;
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			clsReportParameters tList = new clsReportParameters();
			clsReportOrderClause tOrdList = new clsReportOrderClause();
			bool boolTemp;
			int lngCode;
			int lngAutoID;
			int lngSetupRow;
			int lngRow;
			clsSQLStatement tSQLStatement = new clsSQLStatement();
			string strTo = "";
			string strFrom = "";
			// 
			tSQL.ListOfParameters = tList;
			tSQL.ListOfOrders = tOrdList;
			string strTemp;
			strTemp = tSQL.Get_SQLStatement(tSQLStatement, strDefaultTables);
			tSQLStatement.WhereStatement = "Where mj.ceaccount = " + FCConvert.ToString(lngCurrentAccount);
			GetReportSQL = tSQLStatement;
			return GetReportSQL;
		}

		private void mnuPrintHistory_Click(object sender, System.EventArgs e)
		{
			rptAccountPermitActivity.InstancePtr.Init(lngCurrentAccount, this.Modal);
		}

		private void mnuPrintViolations_Click(object sender, System.EventArgs e)
		{
			rptViolations.InstancePtr.Init(lngCurrentAccount, 0, 0, this.Modal);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}
		// Private Sub CheckForViolations()
		// On Error GoTo ErrorHandler
		// Dim rsLoad As New clsdrwrapper
		// Dim rsInsp As New clsdrwrapper
		//
		// Dim boolViolation As Boolean
		//
		// boolViolation = False
		// Call rsLoad.OpenRecordset("select * from permits where not deleted and account = " & lngCurrentAccount, strCEDatabase)
		// Do While Not rsLoad.EndOfFile
		// Call rsInsp.OpenRecordset("select * from inspections where permitid = " & rsLoad.Fields("ID") & " and not deleted", strCEDatabase)
		// Do While Not rsInsp.EndOfFile
		// If Val(rsInsp.Fields("inspectionstatus")) = CNSTINSPECTIONVIOLATION Then
		// boolViolation = True
		// Exit Do
		// End If
		// rsInsp.MoveNext
		// Loop
		// If boolViolation Then Exit Do
		// rsLoad.MoveNext
		// Loop
		//
		// If boolViolation Then
		// lblViolation.Visible = True
		// Else
		// lblViolation.Visible = False
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In CheckForViolations", vbCritical, "Error"
		// End Sub
		private void BPGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			BPGrid.RowData(BPGrid.Row, true);
			if (BPGrid.Rows < 2)
			{
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
				AutoPositionBPGrid();
			}
			else if ((fecherFoundation.Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (fecherFoundation.Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty))
			{
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
				AutoPositionBPGrid();
			}
		}

		private void AutoPositionBPGrid()
		{
			BPGrid.ShowCell(BPGrid.Rows - 1, 0);
		}

		private void BPGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Delete)
			{
				// delete key
				if (BPGrid.Row > 0)
				{
					if (Conversion.Val(BPGrid.TextMatrix(BPGrid.Row, BPIDCol)) > 0)
					{
						gridBPDelete.Rows += 1;
						gridBPDelete.TextMatrix(gridBPDelete.Rows - 1, 0, FCConvert.ToString(Conversion.Val(BPGrid.TextMatrix(BPGrid.Row, BPIDCol))));
						boolDataChanged = true;
					}
					BPGrid.RemoveItem(BPGrid.Row);
					if (BPGrid.Rows < 2)
					{
						BPGrid.AddItem(FCConvert.ToString(true));
						AutoPositionBPGrid();
						BPGrid.RowData(BPGrid.Rows - 1, false);
					}
					else if ((fecherFoundation.Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (fecherFoundation.Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty))
					{
						BPGrid.AddItem(FCConvert.ToString(true));
						AutoPositionBPGrid();
						BPGrid.RowData(BPGrid.Rows - 1, false);
					}
				}
			}
		}

		private void BPGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii < 48) || (KeyAscii > 57))
			{
				// not a number
				if (((KeyAscii >= 58) && (KeyAscii <= 90)) || ((KeyAscii >= 97) && (KeyAscii <= 126)))
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void BPGrid_RowColChange(object sender, System.EventArgs e)
		{
			BPGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (BPGrid.Row == BPGrid.Rows - 1)
			{
				if (BPGrid.Col == BPGrid.Cols - 1)
				{
					BPGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
		}

		private void BPGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = BPGrid.GetFlexRowIndex(e.RowIndex);
            int col = BPGrid.GetFlexColIndex(e.ColumnIndex);

            boolDataChanged = true;
			if (row > 0)
			{
				BPGrid.RowData(row, true);
			}
			if (BPGrid.Rows < 2)
			{
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
				AutoPositionBPGrid();
			}
			else if ((fecherFoundation.Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPBookCol)) != string.Empty) || (fecherFoundation.Strings.Trim(BPGrid.TextMatrix(BPGrid.Rows - 1, BPPageCol)) != string.Empty))
			{
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
				AutoPositionBPGrid();
			}
		}

		private void SetupBPGrid()
		{
			BPGrid.Rows = 1;
			BPGrid.ColHidden(BPIDCol, true);
			BPGrid.TextMatrix(0, BPCheckCol, "");
			BPGrid.TextMatrix(0, BPBookCol, "Book");
			BPGrid.TextMatrix(0, BPPageCol, "Page");
			// .TextMatrix(0, BPSaleDateCol) = "Date"
			// .ColEditMask(BPSaleDateCol) = "##/##/####"
			BPGrid.ColDataType(BPCheckCol, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void ResizeBPGrid()
		{
			int GridWidth = 0;
			GridWidth = BPGrid.WidthOriginal;
			BPGrid.ColWidth(BPCheckCol, FCConvert.ToInt32(0.1 * GridWidth));
			BPGrid.ColWidth(BPBookCol, FCConvert.ToInt32(0.42 * GridWidth));
			// .ColWidth(BPPageCol) = 0.243 * GridWidth
		}

		private void FillBPGrid()
		{
			clsDRWrapper clsBP = new clsDRWrapper();
			string strWhere = "";
			string strBP = "";
			clsBP.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngCurrentAccount) + " order by book , page", modGlobalVariables.Statics.strCEDatabase);
			if (!clsBP.EndOfFile())
			{
				BPGrid.Rows = 1;
				while (!clsBP.EndOfFile())
				{
					BPGrid.AddItem(clsBP.Get_Fields_Boolean("Current") + "\t" + FCConvert.ToString(Conversion.Val(clsBP.Get_Fields("book") + "")) + "\t" + FCConvert.ToString(Conversion.Val(clsBP.Get_Fields("Page") + "")) + "\t" + clsBP.Get_Fields_Int32("ID"));
					BPGrid.RowData(BPGrid.Rows - 1, false);
					clsBP.MoveNext();
				}
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
			}
			else
			{
				BPGrid.Rows = 1;
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
			}
			BPGrid.Col = 1;
			BPGrid.Row = BPGrid.Rows - 1;
			AutoPositionBPGrid();
		}

		private bool SaveBPGrid()
		{
			bool SaveBPGrid = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveBPGrid = false;
				int lngRow;
				clsDRWrapper rsSave = new clsDRWrapper();
				bool boolDelRows;
				boolDelRows = false;
				for (lngRow = 0; lngRow <= gridBPDelete.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from bookpage where ID = " + FCConvert.ToString(Conversion.Val(gridBPDelete.TextMatrix(lngRow, 0))), modGlobalVariables.Statics.strCEDatabase);
				}
				// lngRow
				gridBPDelete.Rows = 0;
				for (lngRow = 1; lngRow <= BPGrid.Rows - 1; lngRow++)
				{
					if (FCConvert.CBool(BPGrid.RowData(lngRow)))
					{
						if (Conversion.Val(BPGrid.TextMatrix(lngRow, BPBookCol)) > 0 && Conversion.Val(BPGrid.TextMatrix(lngRow, BPPageCol)) > 0)
						{
							if (Conversion.Val(BPGrid.TextMatrix(lngRow, BPIDCol)) > 0)
							{
								rsSave.Execute("update bookpage set book = " + FCConvert.ToString(Conversion.Val(BPGrid.TextMatrix(lngRow, BPBookCol))) + ",page = " + FCConvert.ToString(Conversion.Val(BPGrid.TextMatrix(lngRow, BPPageCol))) + " where account = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
							}
							else
							{
								rsSave.OpenRecordset("select top 1 * from bookpage", modGlobalVariables.Statics.strCEDatabase);
								rsSave.AddNew();
								rsSave.Set_Fields("book", FCConvert.ToString(Conversion.Val(BPGrid.TextMatrix(lngRow, BPBookCol))));
								rsSave.Set_Fields("Page", FCConvert.ToString(Conversion.Val(BPGrid.TextMatrix(lngRow, BPPageCol))));
								rsSave.Set_Fields("account", lngCurrentAccount);
								rsSave.Set_Fields("current", FCConvert.CBool(BPGrid.TextMatrix(lngRow, BPCheckCol)));
								rsSave.Update();
								BPGrid.TextMatrix(lngRow, BPIDCol, FCConvert.ToString(rsSave.Get_Fields_Int32("ID")));
							}
							BPGrid.RowData(lngRow, false);
						}
						else
						{
							if (Conversion.Val(BPGrid.TextMatrix(lngRow, BPIDCol)) > 0)
							{
								boolDelRows = true;
							}
						}
					}
				}
				// lngRow
				// If boolDelRows Then
				for (lngRow = BPGrid.Rows - 1; lngRow >= 1; lngRow--)
				{
					if (Conversion.Val(BPGrid.TextMatrix(lngRow, BPBookCol)) <= 0 || Conversion.Val(BPGrid.TextMatrix(lngRow, BPPageCol)) <= 0)
					{
						BPGrid.RemoveItem(lngRow);
					}
				}
				// lngRow
				// End If
				BPGrid.AddItem(FCConvert.ToString(true));
				BPGrid.RowData(BPGrid.Rows - 1, false);
				SaveBPGrid = true;
				return SaveBPGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveBPGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveBPGrid;
		}

		private void optPermits_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			int lngRow;
			switch (Index)
			{
				case 0:
					{
						// just current
						for (lngRow = 1; lngRow <= GridPermits.Rows - 1; lngRow++)
						{
							if (Conversion.Val(GridPermits.TextMatrix(lngRow, gps.Get_ColOrder(CNSTGRIDPERMITSCOLSTATUS))) == modCEConstants.CNSTPERMITCOMPLETE)
							{
								GridPermits.RowHidden(lngRow, true);
							}
						}
						// lngRow
						break;
					}
				case 1:
					{
						// all
						for (lngRow = 1; lngRow <= GridPermits.Rows - 1; lngRow++)
						{
							GridPermits.RowHidden(lngRow, false);
						}
						// lngRow
						break;
					}
			}
			//end switch
		}

		private void optPermits_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbPermits.SelectedIndex;
			optPermits_CheckedChanged(index, sender, e);
		}

		private void optViolations_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			int lngRow;
			switch (Index)
			{
				case 0:
					{
						// just current
						for (lngRow = 1; lngRow <= gridViolations.Rows - 1; lngRow++)
						{
							if (FCConvert.ToDouble(gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS)) == FCConvert.ToDouble(modCEConstants.CNSTVIOLATIONSTATUSCLOSED) || FCConvert.ToDouble(gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS)) == FCConvert.ToDouble(modCEConstants.CNSTVIOLATIONSTATUSRESOLVED))
							{
								gridViolations.RowHidden(lngRow, true);
							}
						}
						// lngRow
						break;
					}
				case 1:
					{
						// all
						for (lngRow = 1; lngRow <= gridViolations.Rows - 1; lngRow++)
						{
							gridViolations.RowHidden(lngRow, false);
						}
						// lngRow
						break;
					}
			}
			//end switch
		}

		private void optViolations_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbViolations.SelectedIndex;
			optViolations_CheckedChanged(index, sender, e);
		}

		private void cmdSearchOwner_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsGroup = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngReturnID;
				cPartyController tCont = new cPartyController();
				lngReturnID = frmCentralPartySearch.InstancePtr.Init();
				if (lngReturnID != MastRec.PartyID1 && lngReturnID > 0)
				{
					// Dim tCont As New cPartyController
					cParty temp = MastRec.OwnerParty();
					tCont.LoadParty(ref temp, lngReturnID);
					MastRec.PartyID1 = lngReturnID;
					ShowOwner();
					ShowAddress();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SearchOwner", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdSearchSecOwner_Click(object sender, System.EventArgs e)
		{
			int lngReturnID;
			lngReturnID = frmCentralPartySearch.InstancePtr.Init();
			if (lngReturnID != MastRec.PartyID2 && lngReturnID > 0)
			{
				cPartyController tCont = new cPartyController();
				cParty temp = MastRec.SecOwnerParty();
				tCont.LoadParty(ref temp, lngReturnID);
				MastRec.PartyID2 = lngReturnID;
				ShowSecOwner();
			}
		}

		private void ShowOwner()
		{
			if (MastRec.PartyID1 > 0)
			{
				if (!(MastRec.OwnerParty() == null))
				{
					lblOwner1.Text = MastRec.OwnerParty().FullName;
				}
				else
				{
					lblOwner1.Text = "";
				}
				txtOwnerID.Text = FCConvert.ToString(MastRec.PartyID1);
			}
			else
			{
				lblOwner1.Text = "";
				txtOwnerID.Text = FCConvert.ToString(0);
			}
		}

		private void ShowAddress()
		{
			if (!(MastRec.OwnerParty() == null))
			{
				cPartyAddress tAdd;
				tAdd = MastRec.OwnerParty().GetPrimaryAddress();
				if (!(tAdd == null))
				{
					lblAddress.Text = tAdd.GetFormattedAddress();
				}
				else
				{
					lblAddress.Text = "";
				}
			}
			else
			{
				lblAddress.Text = "";
			}
		}

		private void ShowSecOwner()
		{
			if (MastRec.PartyID2 > 0)
			{
				if (!(MastRec.SecOwnerParty() == null))
				{
					lbl2ndOwner.Text = MastRec.SecOwnerParty().FullName;
				}
				else
				{
					lbl2ndOwner.Text = "";
				}
				txt2ndOwnerID.Text = FCConvert.ToString(MastRec.PartyID2);
			}
			else
			{
				lbl2ndOwner.Text = "";
				txt2ndOwnerID.Text = FCConvert.ToString(0);
			}
		}
	}
}
