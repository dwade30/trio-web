﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;

namespace TWCE0000
{
	public class clsPhoneNumber
	{
		//=========================================================
		private string strPhoneNumber = string.Empty;
		private string strDescription = string.Empty;
		private int lngID;
		private bool boolDeleted;

		public string PhoneNumber
		{
			set
			{
				string strTemp;
				strTemp = value;
				strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Replace(strTemp, " ", "", 1, -1, CompareConstants.vbTextCompare);
				strTemp = Strings.Right("0000000000" + strTemp, 10);
				strPhoneNumber = strTemp;
			}
			get
			{
				string PhoneNumber = "";
				PhoneNumber = Strings.Right("0000000000" + strPhoneNumber, 10);
				return PhoneNumber;
			}
		}

		public string FormattedPhoneNumber
		{
			get
			{
				string FormattedPhoneNumber = "";
				string strTemp;
				strTemp = Strings.Right("0000000000" + strPhoneNumber, 10);
				strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7);
				FormattedPhoneNumber = strTemp;
				return FormattedPhoneNumber;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public bool Deleted
		{
			set
			{
				boolDeleted = value;
			}
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
		}
	}
}
