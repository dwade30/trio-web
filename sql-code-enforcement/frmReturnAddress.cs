//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmReturnAddress : BaseForm
	{
		public frmReturnAddress()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmReturnAddress InstancePtr
		{
			get
			{
				return (frmReturnAddress)Sys.GetInstance(typeof(frmReturnAddress));
			}
		}

		protected frmReturnAddress _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolDataChanged;

		private void frmReturnAddress_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmReturnAddress_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReturnAddress properties;
			//frmReturnAddress.FillStyle	= 0;
			//frmReturnAddress.ScaleWidth	= 5880;
			//frmReturnAddress.ScaleHeight	= 3960;
			//frmReturnAddress.LinkTopic	= "Form2";
			//frmReturnAddress.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			LoadData();
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from customize", modGlobalVariables.Statics.strCEDatabase);
				int x;
				for (x = 1; x <= 4; x++)
				{
					gridAddress.TextMatrix(x - 1, 0, FCConvert.ToString(rsLoad.Get_Fields("ReturnAddress" + FCConvert.ToString(x))));
				}
				// x
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				gridAddress.Row = -1;
				//App.DoEvents();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from customize", modGlobalVariables.Statics.strCEDatabase);
				int x;
				rsSave.Edit();
				for (x = 1; x <= 4; x++)
				{
					rsSave.Set_Fields("ReturnAddress" + x, gridAddress.TextMatrix(x - 1, 0));
				}
				// x
				rsSave.Update();
				boolDataChanged = false;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void ResizeGrid()
		{
			//FC:FINAL:CHN - i.issue #1666: Change using "Height" property on "HeightOriginal" to get correct value.
			// gridAddress.Height = gridAddress.RowHeight(0) * 4 + 30;
			//gridAddress.HeightOriginal = gridAddress.RowHeight(0) * 4 + 30;
		}

		private void frmReturnAddress_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void gridAddress_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
            //FC:FINAL:BSE #1924 save should not close tab
            //if (SaveData())
            //{
            //	Close();
            //}
            SaveData();
		}
	}
}
