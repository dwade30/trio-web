//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmInspection.
	/// </summary>
	partial class frmInspection
	{
		public fecherFoundation.FCComboBox cmbViolations;
		public fecherFoundation.FCLabel lblViolations;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridOpens;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid gridViolations;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCLabel lblIdentifier;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel lblApplicationDate;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel lblPlan;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel lblPermitType;
		public fecherFoundation.FCLabel lblCommRes;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel lblPermit;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCRichTextBox rtbComments;
		public fecherFoundation.FCComboBox cmbStatus;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCComboBox cmbInspector;
		public Global.T2KDateBox t2kDate;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddViolation;
		public fecherFoundation.FCToolStripMenuItem mnuPrintViolations;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteInspection;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmbViolations = new fecherFoundation.FCComboBox();
            this.lblViolations = new fecherFoundation.FCLabel();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.GridOpens = new fecherFoundation.FCGrid();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.gridViolations = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.lblIdentifier = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.lblApplicationDate = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.lblPlan = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.lblPermitType = new fecherFoundation.FCLabel();
            this.lblCommRes = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.lblPermit = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblMapLot = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.rtbComments = new fecherFoundation.FCRichTextBox();
            this.cmbStatus = new fecherFoundation.FCComboBox();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.cmbInspector = new fecherFoundation.FCComboBox();
            this.t2kDate = new Global.T2KDateBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddViolation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintViolations = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteInspection = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddViolation = new fecherFoundation.FCButton();
            this.cmdPrintViolations = new fecherFoundation.FCButton();
            this.cmdDeleteInspection = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViolations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddViolation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintViolations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteInspection)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(927, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.rtbComments);
            this.ClientArea.Controls.Add(this.cmbStatus);
            this.ClientArea.Controls.Add(this.cmbType);
            this.ClientArea.Controls.Add(this.cmbInspector);
            this.ClientArea.Controls.Add(this.t2kDate);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1_1);
            this.ClientArea.Size = new System.Drawing.Size(927, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddViolation);
            this.TopPanel.Controls.Add(this.cmdPrintViolations);
            this.TopPanel.Controls.Add(this.cmdDeleteInspection);
            this.TopPanel.Size = new System.Drawing.Size(927, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteInspection, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintViolations, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddViolation, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(137, 30);
            this.HeaderText.Text = "Inspections";
            // 
            // cmbViolations
            // 
            this.cmbViolations.Items.AddRange(new object[] {
            "Show Current",
            "Show All"});
            this.cmbViolations.Location = new System.Drawing.Point(102, 30);
            this.cmbViolations.Name = "cmbViolations";
            this.cmbViolations.Size = new System.Drawing.Size(194, 40);
            this.cmbViolations.TabIndex = 35;
            this.cmbViolations.Text = "Show Current";
            this.cmbViolations.SelectedIndexChanged += new System.EventHandler(this.optViolations_CheckedChanged);
            // 
            // lblViolations
            // 
            this.lblViolations.AutoSize = true;
            this.lblViolations.Location = new System.Drawing.Point(30, 44);
            this.lblViolations.Name = "lblViolations";
            this.lblViolations.Size = new System.Drawing.Size(46, 15);
            this.lblViolations.TabIndex = 36;
            this.lblViolations.Text = "SHOW";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Location = new System.Drawing.Point(30, 503);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(863, 371);
            this.SSTab1.TabIndex = 29;
            this.SSTab1.Text = "User Defined Data";
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.GridOpens);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(861, 323);
            this.SSTab1_Page1.Text = "User Defined Data";
            // 
            // GridOpens
            // 
            this.GridOpens.Cols = 7;
            this.GridOpens.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridOpens.ExtendLastCol = true;
            this.GridOpens.FixedCols = 2;
            this.GridOpens.Location = new System.Drawing.Point(20, 20);
            this.GridOpens.Name = "GridOpens";
            this.GridOpens.ReadOnly = false;
            this.GridOpens.Rows = 1;
            this.GridOpens.ShowFocusCell = false;
            this.GridOpens.Size = new System.Drawing.Size(824, 282);
            this.GridOpens.TabIndex = 30;
            this.GridOpens.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridOpens_ValidateEdit);
            this.GridOpens.CurrentCellChanged += new System.EventHandler(this.GridOpens_RowColChange);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.gridViolations);
            this.SSTab1_Page2.Controls.Add(this.cmbViolations);
            this.SSTab1_Page2.Controls.Add(this.lblViolations);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(861, 323);
            this.SSTab1_Page2.Text = "Violations";
            // 
            // gridViolations
            // 
            this.gridViolations.Cols = 5;
            this.gridViolations.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.gridViolations.ExtendLastCol = true;
            this.gridViolations.FixedCols = 0;
            this.gridViolations.Location = new System.Drawing.Point(20, 91);
            this.gridViolations.Name = "gridViolations";
            this.gridViolations.RowHeadersVisible = false;
            this.gridViolations.Rows = 1;
            this.gridViolations.ShowFocusCell = false;
            this.gridViolations.Size = new System.Drawing.Size(821, 204);
            this.gridViolations.TabIndex = 34;
            this.gridViolations.DoubleClick += new System.EventHandler(this.gridViolations_DblClick);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.lblIdentifier);
            this.Frame2.Controls.Add(this.Label1_5);
            this.Frame2.Controls.Add(this.lblApplicationDate);
            this.Frame2.Controls.Add(this.Label1_4);
            this.Frame2.Controls.Add(this.lblPlan);
            this.Frame2.Controls.Add(this.Label1_3);
            this.Frame2.Controls.Add(this.lblPermitType);
            this.Frame2.Controls.Add(this.lblCommRes);
            this.Frame2.Controls.Add(this.Label1_2);
            this.Frame2.Controls.Add(this.lblPermit);
            this.Frame2.Location = new System.Drawing.Point(30, 154);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(863, 107);
            this.Frame2.TabIndex = 18;
            this.Frame2.Text = "Permit";
            // 
            // lblIdentifier
            // 
            this.lblIdentifier.Location = new System.Drawing.Point(205, 30);
            this.lblIdentifier.Name = "lblIdentifier";
            this.lblIdentifier.Size = new System.Drawing.Size(124, 22);
            this.lblIdentifier.TabIndex = 28;
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(149, 30);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(48, 22);
            this.Label1_5.TabIndex = 27;
            this.Label1_5.Text = "PERMIT";
            // 
            // lblApplicationDate
            // 
            this.lblApplicationDate.Location = new System.Drawing.Point(527, 30);
            this.lblApplicationDate.Name = "lblApplicationDate";
            this.lblApplicationDate.Size = new System.Drawing.Size(106, 22);
            this.lblApplicationDate.TabIndex = 26;
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(486, 30);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(35, 22);
            this.Label1_4.TabIndex = 25;
            this.Label1_4.Text = "DATE";
            // 
            // lblPlan
            // 
            this.lblPlan.Location = new System.Drawing.Point(394, 30);
            this.lblPlan.Name = "lblPlan";
            this.lblPlan.Size = new System.Drawing.Size(84, 22);
            this.lblPlan.TabIndex = 24;
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(351, 30);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(35, 22);
            this.Label1_3.TabIndex = 23;
            this.Label1_3.Text = "PLAN";
            // 
            // lblPermitType
            // 
            this.lblPermitType.Location = new System.Drawing.Point(137, 74);
            this.lblPermitType.Name = "lblPermitType";
            this.lblPermitType.Size = new System.Drawing.Size(508, 17);
            this.lblPermitType.TabIndex = 22;
            // 
            // lblCommRes
            // 
            this.lblCommRes.Location = new System.Drawing.Point(30, 74);
            this.lblCommRes.Name = "lblCommRes";
            this.lblCommRes.Size = new System.Drawing.Size(85, 17);
            this.lblCommRes.TabIndex = 21;
            this.lblCommRes.Text = "RESIDENTIAL";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(30, 30);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(56, 22);
            this.Label1_2.TabIndex = 20;
            this.Label1_2.Text = "PERMIT";
            // 
            // lblPermit
            // 
            this.lblPermit.Location = new System.Drawing.Point(90, 30);
            this.lblPermit.Name = "lblPermit";
            this.lblPermit.Size = new System.Drawing.Size(47, 22);
            this.lblPermit.TabIndex = 19;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.Label1_0);
            this.Frame1.Controls.Add(this.lblAccount);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.lblMapLot);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.lblLocation);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.lblName);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(863, 111);
            this.Frame1.TabIndex = 9;
            this.Frame1.Text = "Property";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(30, 30);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(70, 18);
            this.Label1_0.TabIndex = 17;
            this.Label1_0.Text = "ACCOUNT";
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(112, 30);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(71, 22);
            this.lblAccount.TabIndex = 16;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(201, 30);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(56, 22);
            this.Label7.TabIndex = 15;
            this.Label7.Text = "MAP LOT";
            // 
            // lblMapLot
            // 
            this.lblMapLot.Location = new System.Drawing.Point(273, 30);
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Size = new System.Drawing.Size(140, 22);
            this.lblMapLot.TabIndex = 14;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(30, 73);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(70, 18);
            this.Label6.TabIndex = 13;
            this.Label6.Text = "LOCATION";
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(118, 73);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(717, 22);
            this.lblLocation.TabIndex = 12;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(422, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(49, 22);
            this.Label5.TabIndex = 11;
            this.Label5.Text = "NAME";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(483, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(352, 22);
            this.lblName.TabIndex = 10;
            // 
            // rtbComments
            // 
            this.rtbComments.Location = new System.Drawing.Point(30, 386);
            this.rtbComments.Name = "rtbComments";
            this.rtbComments.Size = new System.Drawing.Size(863, 102);
            this.rtbComments.TabIndex = 4;
            // 
            // cmbStatus
            // 
            this.cmbStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.Location = new System.Drawing.Point(427, 332);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(261, 40);
            this.cmbStatus.TabIndex = 3;
            // 
            // cmbType
            // 
            this.cmbType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbType.Location = new System.Drawing.Point(97, 332);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(188, 40);
            this.cmbType.TabIndex = 2;
            // 
            // cmbInspector
            // 
            this.cmbInspector.BackColor = System.Drawing.SystemColors.Window;
            this.cmbInspector.Location = new System.Drawing.Point(427, 279);
            this.cmbInspector.Name = "cmbInspector";
            this.cmbInspector.Size = new System.Drawing.Size(261, 40);
            this.cmbInspector.TabIndex = 1;
            // 
            // t2kDate
            // 
            this.t2kDate.Location = new System.Drawing.Point(97, 280);
            this.t2kDate.Mask = "##/##/####";
            this.t2kDate.Name = "t2kDate";
            this.t2kDate.Size = new System.Drawing.Size(120, 40);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(321, 346);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(68, 16);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "STATUS";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 346);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(46, 16);
            this.Label3.TabIndex = 7;
            this.Label3.Text = "TYPE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(321, 293);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(70, 16);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "INSPECTOR";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(30, 293);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(46, 16);
            this.Label1_1.TabIndex = 5;
            this.Label1_1.Text = "DATE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddViolation,
            this.mnuPrintViolations,
            this.mnuSepar3,
            this.mnuDeleteInspection,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddViolation
            // 
            this.mnuAddViolation.Index = 0;
            this.mnuAddViolation.Name = "mnuAddViolation";
            this.mnuAddViolation.Text = "Add Violation";
            this.mnuAddViolation.Click += new System.EventHandler(this.mnuAddViolation_Click);
            // 
            // mnuPrintViolations
            // 
            this.mnuPrintViolations.Index = 1;
            this.mnuPrintViolations.Name = "mnuPrintViolations";
            this.mnuPrintViolations.Text = "Print Violations";
            this.mnuPrintViolations.Click += new System.EventHandler(this.mnuPrintViolations_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 2;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuDeleteInspection
            // 
            this.mnuDeleteInspection.Index = 3;
            this.mnuDeleteInspection.Name = "mnuDeleteInspection";
            this.mnuDeleteInspection.Text = "Delete Inspection";
            this.mnuDeleteInspection.Click += new System.EventHandler(this.mnuDeleteInspection_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 4;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 5;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 6;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 7;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(424, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddViolation
            // 
            this.cmdAddViolation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddViolation.Location = new System.Drawing.Point(557, 29);
            this.cmdAddViolation.Name = "cmdAddViolation";
            this.cmdAddViolation.Size = new System.Drawing.Size(98, 24);
            this.cmdAddViolation.TabIndex = 1;
            this.cmdAddViolation.Text = "Add Violation";
            this.cmdAddViolation.Click += new System.EventHandler(this.mnuAddViolation_Click);
            // 
            // cmdPrintViolations
            // 
            this.cmdPrintViolations.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintViolations.Location = new System.Drawing.Point(661, 29);
            this.cmdPrintViolations.Name = "cmdPrintViolations";
            this.cmdPrintViolations.Size = new System.Drawing.Size(108, 24);
            this.cmdPrintViolations.TabIndex = 2;
            this.cmdPrintViolations.Text = "Print Violations";
            this.cmdPrintViolations.Click += new System.EventHandler(this.mnuPrintViolations_Click);
            // 
            // cmdDeleteInspection
            // 
            this.cmdDeleteInspection.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteInspection.Location = new System.Drawing.Point(775, 29);
            this.cmdDeleteInspection.Name = "cmdDeleteInspection";
            this.cmdDeleteInspection.Size = new System.Drawing.Size(124, 24);
            this.cmdDeleteInspection.TabIndex = 3;
            this.cmdDeleteInspection.Text = "Delete Inspection";
            this.cmdDeleteInspection.Click += new System.EventHandler(this.mnuDeleteInspection_Click);
            // 
            // frmInspection
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(927, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmInspection";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Inspections";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmInspection_Load);
            this.Resize += new System.EventHandler(this.frmInspection_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmInspection_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            this.SSTab1_Page2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViolations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtbComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddViolation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintViolations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteInspection)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdAddViolation;
		private FCButton cmdPrintViolations;
		private FCButton cmdDeleteInspection;
    }
}