//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptCECustomReport.
	/// </summary>
	public partial class rptCECustomReport : BaseSectionReport
	{
		public rptCECustomReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCECustomReport InstancePtr
		{
			get
			{
				return (rptCECustomReport)Sys.GetInstance(typeof(rptCECustomReport));
			}
		}

		protected rptCECustomReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
				rsUserCodes.Dispose();
				rsPermitCategories?.Dispose();
				rsInspectionStatus?.Dispose();
				rsInspectionTypes?.Dispose();
				rsPermitStatus?.Dispose();
				rsPermitTypes?.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCECustomReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		private clsCustomReportItemList ListOfItems = new clsCustomReportItemList();
		private clsDRWrapper rsUserCodes = new clsDRWrapper();
		private clsDRWrapper rsPermitTypes;
		private clsDRWrapper rsInspectionTypes;
		private clsDRWrapper rsPermitStatus;
		private clsDRWrapper rsInspectionStatus;
		private clsDRWrapper rsPermitCategories;

		public void Init(int lngAutoID, ref clsSQLStatement clsStatement, bool boolPreview = true)
		{
			string strSQL;
            using (clsDRWrapper rsLoad = new clsDRWrapper())
            {
                bool boolDup = false;
                rsLoad.OpenRecordset("select * from customreports where ID = " + FCConvert.ToString(lngAutoID),
                    modGlobalVariables.Statics.strCEDatabase);
                if (!rsLoad.EndOfFile())
                {
                    lblTitle.Text = fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("Title"));
                    this.Name = lblTitle.Text;
                }
                else
                {
                    this.Name = "Custom Report";
                }

                // have to build the sql statement
                // strSQL = MakeSQLStatement(lngAutoID)
                strSQL = clsStatement.SQLStatement;
                rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
                if (rsReport.EndOfFile())
                {
                    MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                txtRange.Text = clsStatement.ParameterDescriptionText;
                rsUserCodes.OpenRecordset("select * from usercodes order by ID",
                    modGlobalVariables.Statics.strCEDatabase);
                CreateLabelsAndTextboxes(lngAutoID);
                if (boolPreview)
                {
                    boolDup = false;

                    if (this.PageSettings.Orientation ==
                        GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape)
                    {
                        frmReportViewer.InstancePtr.Init(this, "", 0, boolDup, false, "Pages", true, "",
                            "TRIO Software", false, true, "Custom");
                    }
                    else
                    {
                        frmReportViewer.InstancePtr.Init(this, "", 0, boolDup, false, boolAllowEmail: true,
                            strAttachmentName: "Custom");
                    }
                }
                else
                {
                    this.PrintReport(true);
                }
            }
        }

		private bool CreateLabelsAndTextboxes(int lngAutoID)
		{
			bool CreateLabelsAndTextboxes = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			int lngCol = 0;
			float lngCurrentLabelX;
			float lngCurrentLabelY;
			float lngCurrentBoxX;
			float lngCurrentBoxY;
			float lngHeight;
			float lngWidth = 0;
			int intDetailSpace = 0;
			int intReturn;
			string strTemp = "";
			GrapeCity.ActiveReports.SectionReportModel.ARControl ctl = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			int intAlign = 0;
			int lngFields;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                CreateLabelsAndTextboxes = false;
                lngFields = 0;
                clsLoad.OpenRecordset(
                    "select * from customreportlayouts where reportid = " + FCConvert.ToString(lngAutoID) +
                    " order by rownumber,colnumber", modGlobalVariables.Statics.strCEDatabase);
                if (clsLoad.EndOfFile())
                    return CreateLabelsAndTextboxes;
                lngCurrentLabelX = 0;
                lngCurrentLabelY = 720 / 1440F;
                lngCurrentBoxX = 0;
                lngCurrentBoxY = 0;
                lngHeight = 270 / 1440F;
                lngRow = 1;
                while (!clsLoad.EndOfFile())
                {
                    // create a label for the header and a textbox for the detail section
                    // the labels tops will be 720 + (label height * row)
                    // boxes will be 240 + (box height * col)
                    lngFields += 1;
                    if (clsLoad.Get_Fields_Int32("rownumber") > lngRow)
                    {
                        lngCurrentLabelX = 0;
                        lngCurrentLabelY += lngHeight + 720 / 1440F;
                        lngCurrentBoxX = 0;
                        lngCurrentBoxY += 240 / 1440F;
                    }

                    lngRow = clsLoad.Get_Fields_Int32("rownumber");
                    //FC:FINAL:MSH - i.issue #1753: wrong width
                    //lngWidth = clsLoad.Get_Fields_Int32("colWidth");
                    lngWidth = clsLoad.Get_Fields_Int32("colWidth") / 1440f;
                    lngCol = clsLoad.Get_Fields_Int32("COlnumber");
                    if (Conversion.Val(clsLoad.Get_Fields_String("justification")) == 1)
                    {
                        intAlign = FCConvert.ToInt32(GrapeCity.ActiveReports.Document.Section.TextAlignment.Right);
                    }
                    else if (Conversion.Val(clsLoad.Get_Fields_String("justification")) == 2)
                    {
                        intAlign = FCConvert.ToInt32(GrapeCity.ActiveReports.Document.Section.TextAlignment.Center);
                    }
                    else
                    {
                        intAlign = FCConvert.ToInt32(GrapeCity.ActiveReports.Document.Section.TextAlignment.Left);
                    }

                    //FC:FINAL:MSH - i.issue #1753: wrong method call
                    //ListOfItems.AddItem(ref lngRow, ref lngCol, FCConvert.ToInt16(Conversion.Val(clsLoad.Get_Fields("codetype")))), FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("opencode")))), FCConvert.ToInt16(Conversion.Val(clsLoad.Get_Fields("totaloption")))));
                    int codeType = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("codetype")));
                    int openCode = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("opencode")));
                    int totalOption = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("totaloption")));
                    ListOfItems.AddItem(lngRow, lngCol, codeType, openCode, totalOption);
                    // now that we have the info we need to create the controls
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Label();
                    ctl.Top = lngCurrentLabelY;
                    ctl.Width = lngWidth;
                    ctl.Left = lngCurrentLabelX;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Alignment =
                        (GrapeCity.ActiveReports.Document.Section.TextAlignment) intAlign;
                    ctl.Height = lngHeight + 0.02F;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Text =
                        clsLoad.Get_Fields_String("Header");
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new System.Drawing.Font(
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font, System.Drawing.FontStyle.Bold);
                    ctl.Name = "lblRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol);
                    //FC:FINAL:MSH - i.issue #1753: add labels into PageHeader
                    //Detail.Controls.Add(ctl);
                    PageHeader.Controls.Add(ctl);
                    lngCurrentLabelX += lngWidth;
                    // now the textbox
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                    ctl.Top = lngCurrentBoxY;
                    ctl.Width = lngWidth;
                    ctl.Left = lngCurrentBoxX;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.TextBox).Alignment =
                        (GrapeCity.ActiveReports.Document.Section.TextAlignment) intAlign;
                    ctl.Height = lngHeight;
                    ctl.Name = "txtRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol);
                    strTemp = ctl.Name;
                    Detail.Controls.Add(ctl);
                    if (Conversion.Val(clsLoad.Get_Fields_String("totaloption")) > 0)
                    {
                        ReportFooter.Visible = true;
                        ctl = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                        ctl.Height = lngHeight;
                        // total
                        // ReDim Preserve aryTots(UBound(aryTots) + 1) As String
                        // aryTots(UBound(aryTots)) = strTemp
                        ctl.Top = (lngRow * lngHeight) + Line1.Y1 + 120 / 1440F;
                        ctl.Left = lngCurrentBoxX;
                        ctl.Width = lngWidth;
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.TextBox).Alignment =
                            (GrapeCity.ActiveReports.Document.Section.TextAlignment) intAlign;
                        ctl.Name = "TotalFor" + strTemp;
                        if (ReportFooter.Height < ctl.Top + ctl.Height)
                        {
                            ReportFooter.Height = ctl.Top + ctl.Height + 20 / 1440F;
                        }

                        ReportFooter.Controls.Add(ctl);
                    }

                    lngCurrentBoxX += lngWidth;
                    clsLoad.MoveNext();
                }

                Detail.Height = lngCurrentBoxY + intDetailSpace;
                if (lngCurrentBoxX > 7.5F)
                {
                    // this must be landscape or to a wide printer
                    // intReturn = MsgBox("This report is wider than normal. Do you wish to print landscape?" & vbNewLine & "If you answer no the page will be formatted for a wide printer.", vbQuestion + vbYesNo, "Print Landscape?")
                    // If intReturn = vbYes Then
                    this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                    txtPage.Left = 10F - txtPage.Width - 20 / 1440F;
                    txtDate.Left = 10F - txtDate.Width - 20 / 1440F;
                    lblTitle.Width = txtDate.Left - lblTitle.Left;
                    // Else
                    // Me.PageSettings.PaperWidth = 13.5 * 1440 '14.5 inches
                    // End If
                }

                //FC:FINAL:MSH - i.issue #1753: wrong calculating PageHeader height
                //PageHeader.Height = lngCurrentLabelY + 1;
                PageHeader.Height = lngCurrentLabelY + 1 / 1440f;
                PageHeader.CanShrink = false;
                CreateLabelsAndTextboxes = true;
                return CreateLabelsAndTextboxes;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateLabelsAndTextboxes", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
			return CreateLabelsAndTextboxes;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			clsCustomReportItem tItem;
			if (!FCUtils.IsEmpty(ListOfItems))
			{
				ListOfItems.MoveFirst();
				while (ListOfItems.GetCurrentIndex >= 0)
				{
					tItem = ListOfItems.GetCurrentReportItem;
					if (!(tItem == null))
					{
						if (tItem.TotalOption > 0)
						{
							strTemp = "TotalFortxtRow" + FCConvert.ToString(tItem.Row) + "Col" + FCConvert.ToString(tItem.Col);
							if (Strings.InStr(1, (ReportFooter.Controls[strTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, ".", CompareConstants.vbTextCompare) > 0)
							{
								(ReportFooter.Controls[strTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls[strTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0.00");
							}
							else
							{
								(ReportFooter.Controls[strTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format((ReportFooter.Controls[strTemp] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text, "#,###,###,##0");
							}
						}
					}
					ListOfItems.MoveNext();
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int x;
			int lngRow = 0;
			int lngCol = 0;
			int Y;
			string strTemp = "";
			if (!FCUtils.IsEmpty(ListOfItems))
			{
				ListOfItems.MoveFirst();
				clsCustomReportItem tItem;
				while (ListOfItems.GetCurrentIndex >= 0)
				{
					tItem = ListOfItems.GetCurrentReportItem;
					if (!(tItem == null))
					{
						lngRow = tItem.Row;
						lngCol = tItem.Col;
						strTemp = GetValueFromCode(tItem);
						(Detail.Controls["txtRow" + lngRow + "Col" + lngCol] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strTemp;
						if (tItem.TotalOption == 1)
						{
							if (fecherFoundation.Strings.Trim(strTemp) == "")
							{
								strTemp = "0";
							}
							(ReportFooter.Controls["TotalFortxtRow" + lngRow + "Col" + lngCol] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((ReportFooter.Controls["TotalFortxtRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text) + FCConvert.ToDouble(strTemp));
						}
						else if (tItem.TotalOption == 2)
						{
							(ReportFooter.Controls["TotalFortxtRow" + lngRow + "Col" + lngCol] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = FCConvert.ToString(Conversion.Val((ReportFooter.Controls["TotalFortxtRow" + FCConvert.ToString(lngRow) + "Col" + FCConvert.ToString(lngCol)] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text) + 1);
						}
					}
					ListOfItems.MoveNext();
				}
			}
			rsReport.MoveNext();
		}

		private string GetValueFromCode(clsCustomReportItem tItem)
		{
			string GetValueFromCode = "";
			string strReturn;
			strReturn = "";
			if (!(tItem == null))
			{
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strTemp = "";
				switch (tItem.CodeType)
				{
					case 1:
						{
							// account
							strReturn = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("CEAccount")));
							break;
						}
					case 2:
						{
							// name
							strReturn = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("First") + " " + rsReport.Get_Fields_String("Middle")) + " " + rsReport.Get_Fields_String("Last") + " " + rsReport.Get_Fields_String("Desig"));
							break;
						}
					case 3:
						{
							// last,first
							if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("first")) != string.Empty && fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("last")) != string.Empty)
							{
								strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("last") + ", " + fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("first") + " " + rsReport.Get_Fields_String("middle")) + " " + rsReport.Get_Fields_String("desig"));
							}
							else
							{
								strReturn = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("First") + " " + rsReport.Get_Fields_String("Middle")) + " " + rsReport.Get_Fields_String("Last") + " " + rsReport.Get_Fields_String("Desig"));
							}
							break;
						}
					case 4:
						{
							// sec owner
							strReturn = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("secFirst") + " " + rsReport.Get_Fields_String("secMiddle")) + " " + rsReport.Get_Fields_String("secLast") + " " + rsReport.Get_Fields_String("secDesig"));
							break;
						}
					case 5:
						{
							// last,first
							if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("secfirst")) != string.Empty && fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("seclast")) != string.Empty)
							{
								strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("seclast") + ", " + fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("secfirst") + " " + rsReport.Get_Fields_String("secmiddle")) + " " + rsReport.Get_Fields_String("secdesig"));
							}
							else
							{
								strReturn = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("secFirst") + " " + rsReport.Get_Fields_String("secMiddle")) + " " + rsReport.Get_Fields_String("secLast") + " " + rsReport.Get_Fields_String("secDesig"));
							}
							break;
						}
					case 6:
						{
							// maplot
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("maplot"));
							break;
						}
					case 7:
						{
							// streetnumber
							if (Conversion.Val(rsReport.Get_Fields_String("streetnumber")) > 0)
							{
								strReturn = rsReport.Get_Fields_String("streetnumber");
							}
							else
							{
								strReturn = "";
							}
							break;
						}
					case 8:
						{
							// streetname
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("streetname"));
							break;
						}
					case 9:
						{
							// neigh
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("neighborhood"));
							break;
						}
					case 10:
						{
							// zone
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("zone"));
							break;
						}
					case 11:
						{
							strReturn = Strings.Format(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("acreage"))), "#,###,##0.00");
							break;
						}
					case 12:
						{
							strReturn = Strings.Format(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("frontage"))), "#,###,##0.00");
							break;
						}
					case 13:
						{
							strReturn = FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("permityear")));
							break;
						}
					case 14:
						{
							if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("permitidentifier")) != string.Empty)
							{
								strReturn = rsReport.Get_Fields_String("permitidentifier");
							}
							else
							{
								strReturn = rsReport.Get_Fields_String("Permityear") + "-" + rsReport.Get_Fields_String("permitnumber");
							}
							break;
						}
					case 15:
						{
							if (Information.IsDate(rsReport.Get_Fields("ApplicationDate")))
							{
                                //FC:FINAL:MSH - i.issue #1693: replace Convert by Get_Fields_DateTime to avoid conversion errors
                                //if (Convert.ToDateTime(rsReport.Get_Fields("applicationdate")).ToOADate() != 0)
                                if (rsReport.Get_Fields_DateTime("applicationdate").ToOADate() != 0)
								{
									strReturn = Strings.Format(rsReport.Get_Fields_DateTime("applicationdate"), "MM/dd/yyyy");
								}
								else
								{
									strReturn = "";
								}
							}
							else
							{
								strReturn = "";
							}
							break;
						}
					case 16:
						{
							// permit type
							if (rsPermitTypes == null)
							{
								rsPermitTypes = new clsDRWrapper();
								rsPermitTypes.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
							}
							if (rsPermitTypes.FindFirstRecord("ID", Conversion.Val(rsReport.Get_Fields_String("permittype"))))
							{
								strReturn = rsPermitTypes.Get_Fields_String("description");
							}
							break;
						}
					case 51:
						{
							if (rsPermitCategories == null)
							{
								rsPermitCategories = new clsDRWrapper();
								rsPermitCategories.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITCATEGORIES) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
							}
							if (Conversion.Val(rsReport.Get_Fields_String("permitsubtype")) == 0)
							{
								strReturn = "None";
							}
							else
							{
								if (rsPermitCategories.FindFirstRecord("ID", Conversion.Val(rsReport.Get_Fields_String("permitsubtype"))))
								{
									strReturn = rsPermitCategories.Get_Fields_String("Description");
								}
								else
								{
									strReturn = "Unknown";
								}
							}
							break;
						}
					case 52:
						{
							strReturn = rsReport.Get_Fields_String("FiledBy");
							break;
						}
					case 53:
						{
							strReturn = Strings.Format(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("EstimatedValue"))), "#,###,###,##0");
							break;
						}
					case 54:
						{
							strReturn = rsReport.Get_Fields_String("ContactName");
							break;
						}
					case 55:
						{
							strReturn = rsReport.Get_Fields_String("ContactEmail");
							break;
						}
					case 56:
						{
							if (Conversion.Val(rsReport.Get_Fields_String("Commrescode")) > 0)
							{
								strReturn = "Commercial";
							}
							else
							{
								strReturn = "Residential";
							}
							break;
						}
					case 57:
						{
							if (Information.IsDate(rsReport.Get_Fields("certificateofoccdate")))
							{
                                //FC:FINAL:MSH - i.issue #1693: replace Convert by Get_Fields_DateTime to avoid conversion errors
								//if (!(Convert.ToDateTime(rsReport.Get_Fields("certificateofoccdate")).ToOADate() == 0))
								if (!(rsReport.Get_Fields_DateTime("certificateofoccdate").ToOADate() == 0))
								{
									strReturn = Strings.Format(rsReport.Get_Fields_DateTime("certificateofoccdate"), "MM/dd/yyyy");
								}
							}
							break;
						}
					case 17:
						{
							// permit status
							if (rsPermitStatus == null)
							{
								rsPermitStatus = new clsDRWrapper();
								rsPermitStatus.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
							}
							if (rsPermitStatus.FindFirstRecord("ID", Conversion.Val(rsReport.Get_Fields_String("permitstatus"))))
							{
								strReturn = rsPermitStatus.Get_Fields_String("description");
							}
							break;
						}
					case 18:
						{
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("Description"));
							break;
						}
					case 19:
						{
							strReturn = rsReport.Get_Fields_String("plan");
							break;
						}
					case 20:
						{
							// inspection type
							if (rsInspectionTypes == null)
							{
								rsInspectionTypes = new clsDRWrapper();
								rsInspectionTypes.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONTYPE) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
							}
							if (rsInspectionTypes.FindFirstRecord("code", Conversion.Val(rsReport.Get_Fields_String("inspectiontype"))))
							{
								strReturn = rsInspectionTypes.Get_Fields_String("description");
							}
							break;
						}
					case 21:
						{
							// inspection status
							if (Conversion.Val(rsReport.Get_Fields_String("inspectionstatus")) == modCEConstants.CNSTINSPECTIONPENDING)
							{
								strReturn = "Scheduled";
							}
							else if (Conversion.Val(rsReport.Get_Fields_String("inspectionstatus")) == modCEConstants.CNSTINSPECTIONCOMPLETE)
							{
								strReturn = "Completed (Last Inspection)";
							}
							else
							{
								if (rsInspectionStatus == null)
								{
									rsInspectionStatus = new clsDRWrapper();
									rsInspectionStatus.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONSTATUS) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
								}
								if (rsInspectionStatus.FindFirstRecord("code", Conversion.Val(rsReport.Get_Fields_String("inspectionstatus"))))
								{
									strReturn = rsInspectionStatus.Get_Fields_String("description");
								}
							}
							break;
						}
					case 22:
						{
							if (Information.IsDate(rsReport.Get_Fields("InspectionDate")))
							{
                                //FC:FINAL:MSH - i.issue #1693: replace Convert by Get_Fields_DateTime to avoid conversion errors
								//if (Convert.ToDateTime(rsReport.Get_Fields("InspectionDate")).ToOADate() != 0)
								if (rsReport.Get_Fields_DateTime("InspectionDate").ToOADate() != 0)
								{
									strReturn = Strings.Format(rsReport.Get_Fields_DateTime("InspectionDate"), "MM/dd/yyyy");
								}
								else
								{
									strReturn = "";
								}
							}
							else
							{
								strReturn = "";
							}
							break;
						}
					case 23:
						{
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("InspectorName"));
							break;
						}
					case 24:
						{
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("ContractorName1"));
							break;
						}
					case 25:
						{
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("contractorname2"));
							break;
						}
					case 26:
						{
							// contractor license
							strReturn = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("License"));
							break;
						}
					case 27:
						{
							rsTemp.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTORSTATUS) + " and code = " + FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("ContractorStatus"))), modGlobalVariables.Statics.strCEDatabase);
							if (!rsTemp.EndOfFile())
							{
								strReturn = rsTemp.Get_Fields_String("description");
							}
							break;
						}
					case 28:
						{
							// contractor class
							break;
						}
					case 29:
						{
							// property opens
							strReturn = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("ceaccount")))), modCEConstants.CNSTCODETYPEPROPERTY, tItem.OpenCode);
							break;
						}
					case 30:
						{
							// contractor
							strReturn = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("ContractorAutoID")))), modCEConstants.CNSTCODETYPECONTRACTOR, tItem.OpenCode);
							break;
						}
					case 31:
						{
							// permit opens
							strReturn = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("PermitAutoID")))), modCEConstants.CNSTCODETYPEPERMIT, tItem.OpenCode);
							break;
						}
					case 32:
						{
							// inspection opens
							strReturn = GetValueForUserCode(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("InspectionAutoID")))), modCEConstants.CNSTCODETYPEINSPECTION, tItem.OpenCode);
							break;
						}
					case 33:
						{
							// permit fee
							if (Conversion.Val(rsReport.Get_Fields_String("fee")) > 0)
							{
								strReturn = Strings.Format(FCConvert.ToString(Conversion.Val(rsReport.Get_Fields_String("fee"))), "#,###,##0.00");
							}
							break;
						}
					case 34:
						{
							// permit contractor name
							strReturn = rsReport.Get_Fields_String("PermitContractorName");
							break;
						}
				}
				//end switch
				rsTemp.Dispose();
			}
			GetValueFromCode = strReturn;
			return GetValueFromCode;
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(string)
		private string GetValueForUserCode(int lngID, int lngType, int lngField)
		{
			string GetValueForUserCode = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsCode = new clsDRWrapper();
			string strTemp;
			strTemp = "";
			rsTemp.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(lngID) + " and codetype = " + FCConvert.ToString(lngType) + " and fieldid = " + FCConvert.ToString(lngField), modGlobalVariables.Statics.strCEDatabase);
			if (!rsTemp.EndOfFile())
			{
				if (rsUserCodes.FindFirstRecord("ID", lngField))
				{
					if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEBOOLEAN)
					{
						if (rsTemp.Get_Fields_Boolean("booleanvalue"))
						{
							strTemp = "Yes";
						}
						else
						{
							strTemp = "No";
						}
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDATE)
					{
						if (Information.IsDate(rsTemp.Get_Fields("datevalue")))
						{
                            //FC:FINAL:MSH - i.issue #1693: replace Convert by Get_Fields_DateTime to avoid conversion errors
							//if (Convert.ToDateTime(rsTemp.Get_Fields("datevalue")).ToOADate() != 0)
							if (rsTemp.Get_Fields_DateTime("datevalue").ToOADate() != 0)
							{
                                //FC:FINAL:MSH - i.issue #1693: get data in short format (as in original)
                                strTemp = rsTemp.Get_Fields_DateTime("datevalue").ToShortDateString();
							}
							else
							{
								strTemp = "";
							}
						}
						else
						{
							strTemp = "";
						}
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL)
					{
						strTemp = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("numericvalue")));
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDROPDOWN)
					{
						rsCode.OpenRecordset("select * from codedescriptions where code = " + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("dropdownvalue"))) + " and fieldid = " + FCConvert.ToString(lngField), modGlobalVariables.Statics.strCEDatabase);
						if (!rsCode.EndOfFile())
						{
							strTemp = rsCode.Get_Fields_String("description");
						}
						else
						{
							strTemp = "";
						}
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
					{
						strTemp = Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("numericvalue"))), "#,###,###,##0");
					}
					else if (rsUserCodes.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPETEXT)
					{
						strTemp = rsTemp.Get_Fields_String("textvalue");
					}
				}
			}
			GetValueForUserCode = strTemp;
			rsTemp.Dispose();
			rsCode.Dispose();
			return GetValueForUserCode;
		}

		
	}
}
