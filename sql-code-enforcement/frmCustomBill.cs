//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWCE0000
{
	public partial class frmCustomBill : BaseForm
	{
		public frmCustomBill()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomBill InstancePtr
		{
			get
			{
				return (frmCustomBill)Sys.GetInstance(typeof(frmCustomBill));
			}
		}

		protected frmCustomBill _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 08/19/2004
		// ********************************************************
		// vbPorter upgrade warning: intCharWid As int	OnWrite(int, double)
		float intCharWid;
		// vbPorter upgrade warning: intLineHite As int	OnWrite(int, double)
		float intLineHite;
		int lngCustomBillID;
		// the id number of the bill.  Used to identify the unique bill
		string strThisModule = "";
		// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
		double dblSizeRatio;
		// ratio of original size to what form has been resized to
		// vbPorter upgrade warning: lngPageWidth As int	OnWrite(double, int)
		float lngPageWidth;
		// The pagewidth in twips (actual, not what is seen)
		// vbPorter upgrade warning: lngPageHeight As int	OnWrite(int, double)
		float lngPageHeight;
		// The pageheight in twips (actual, not what is seen)
		double dblPageRatio;
		// The ratio of actual twips to what the shown page is sized (dblPageRatio * shownsize = actual size)
		// vbPorter upgrade warning: lngScreenPageWidth As int	OnWriteFCConvert.ToDouble(
		int lngScreenPageWidth;
		// The actual twips of the shown page (not including dblSizeRatio)
		// vbPorter upgrade warning: lngScreenPageHeight As int	OnWriteFCConvert.ToDouble(
		int lngScreenPageHeight;
		// The actual twips of the shown page (not including dblsizeratio)
		// vbPorter upgrade warning: lngOffsetY As int	OnWrite(int, double)
		float lngOffsetY;
		// The offset from the frames 0 to where the picture1.top is visibly 0
		// vbPorter upgrade warning: lngOffsetX As int	OnWrite(int, double)
		float lngOffsetX;
		// The offset from the frames 0 to where the picture1.left is visibly 0
		double dblTwipsPerUnit;
		// 1440 per inch ,567 per centimeter, 56.7 per millimeter
		// vbPorter upgrade warning: lngClickXOffset As int	OnWrite(int, float)
		int lngClickXOffset;
		// twips from left the mouse click is
		// vbPorter upgrade warning: lngClickYOffset As int	OnWrite(int, float)
		int lngClickYOffset;
		// twips from top the mouse click is
		bool boolDragging;
		bool boolInHighlightMode;
		bool boolInLassoMode;
		// vbPorter upgrade warning: lngLassoY As int	OnWriteFCConvert.ToSingle(
		int lngLassoY;
		// vbPorter upgrade warning: lngLassoX As int	OnWriteFCConvert.ToSingle(
		int lngLassoX;
		int lngGroupBoxLeft;
		int lngGroupBoxRight;
		int lngGroupBoxTop;
		int lngGroupBoxBottom;
		// constants to make changing things simple.  Don't need to change code. Can just change these constants
		const int CNSTGRIDFIELDSCOLAUTOID = 0;
		const int CNSTGRIDFIELDSCOLFIELDID = 1;
		const int CNSTGRIDFIELDSCOLFIELDNUM = 2;
		const int CNSTGRIDFIELDSCOLDESCRIPTION = 3;
		const int CNSTGRIDFIELDSCOLTOP = 4;
		const int CNSTGRIDFIELDSCOLLEFT = 5;
		const int CNSTGRIDFIELDSCOLHEIGHT = 6;
		const int CNSTGRIDFIELDSCOLWIDTH = 7;
		const int CNSTGRIDFIELDSCOLALIGNMENT = 8;
		const int CNSTGRIDFIELDSCOLCONTROL = 10;
		const int CNSTGRIDFIELDSCOLTEXT = 11;
		const int CNSTGRIDFIELDSCOLFONT = 9;
		const int CNSTGRIDFIELDSCOLFONTSIZE = 12;
		const int CNSTGRIDFIELDSCOLFONTSTYLE = 13;
		const int CNSTGRIDFIELDSCOLEXTRAPARAMETERS = 14;
		const int CNSTGRIDFIELDSCOLMISC = 15;
		const int CNSTGRIDFIELDSCOLOPENID = 16;
		const int CNSTGRIDBILLSIZECOLDATA = 1;
		const int CNSTGRIDBILLSIZECOLDESCRIPTION = 0;
		const int CNSTGRIDBILLSIZEROWNAME = 0;
		const int CNSTGRIDBILLSIZEROWUNITS = 1;
		const int CNSTGRIDBILLSIZEROWPAGEHEIGHT = 2;
		const int CNSTGRIDBILLSIZEROWPAGEWIDTH = 3;
		const int CNSTGRIDBILLSIZEROWTOPMARGIN = 4;
		const int CNSTGRIDBILLSIZEROWBOTTOMMARGIN = 5;
		const int CNSTGRIDBILLSIZEROWLEFTMARGIN = 6;
		const int CNSTGRIDBILLSIZEROWRIGHTMARGIN = 7;
		const int CNSTGRIDBILLSIZEROWSTYLE = 8;
		const int CNSTGRIDBILLSIZEROWDESCRIPTION = 9;
		const int CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE = 10;
		const int CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE = 11;
		const int CNSTCUSTOMBILLALIGNLEFT = 0;
		const int CNSTCUSTOMBILLALIGNRIGHT = 1;
		const int CNSTCUSTOMBILLALIGNCENTER = 2;
		const int CNSTCUSTOMBILLUNITSINCHES = 0;
		const int CNSTCUSTOMBILLUNITSCENTIMETERS = 1;
		const int CNSTCUSTOMBILLFONTSTYLEREGULAR = 0;
		const int CNSTCUSTOMBILLFONTSTYLEBOLD = 1;
		const int CNSTCUSTOMBILLFONTSTYLEITALIC = 2;
		const int CNSTCUSTOMBILLFONTSTYLEBOLDITALIC = 3;
		const int CNSTCUSTOMBILLMAXCONTROLS = 200;
		// not used currently.  Use if we need to not show all of the fields
		// that are created since only 255 controls can be on a form
		const int CNSTGRIDCONTROLINFOCOLDATA = 1;
		const int CNSTGRIDCONTROLINFOCOLDESCRIPTION = 0;
		const int CNSTGRIDCONTROLINFOCOLTOOLTIP = 2;
		const int CNSTGRIDCONTROLINFOROWCODE = 0;
		const int CNSTGRIDCONTROLINFOROWTYPE = 1;
		const int CNSTGRIDCONTROLINFOROWTEXT = 2;
		const int CNSTGRIDCONTROLINFOROWALIGN = 3;
		const int CNSTGRIDCONTROLINFOROWFONT = 4;
		const int CNSTGRIDCONTROLINFOROWFONTSIZE = 5;
		const int CNSTGRIDCONTROLINFOROWFONTSTYLE = 6;
		const int CNSTGRIDCONTROLINFOROWOPEN = 7;
		const int CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS = 8;
		// these rows must always be the last rows
		// since there are a variable number of them and we need to know if we are on those types of rows or not
		// the string to describe these is delimited by semi-colons.  Each parameters info is separated by commas
		// and if the parameter needs a dropdown, then that info is separated by |'s
		// Ex: 3 parameters Description 1,Label and Type.  Description and label can be anything but type needs
		// to be chosen from a drop down.  The paramaters string would look like this:
		// Description 1;Label;Type,Choice1|Choice2|Choice3      The first two have no other data associated since they
		// take whatever the user types in.  The Type includes a string to be made into a drop down list
		// The first item is used as the text to show to the user.  When saved, all info from the user will be delimited
		// by semi-colons.  This means that the only invalid character to be entered by the user is a semi-colon
		const int CNSTGRIDCONTROLTYPESCOLOUTLINE = 0;
		const int CNSTGRIDCONTROLTYPESCOLCODE = 1;
		const int CNSTGRIDCONTROLTYPESCOLCATEGORY = 2;
		const int CNSTGRIDCONTROLTYPESCOLNAME = 3;
		const int CNSTGRIDCONTROLTYPESCOLDESCRIPTION = 4;
		const int CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN = 5;
		const int CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH = 6;
		const int CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT = 7;
		const int CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS = 8;
		const int CNSTGRIDCONTROLTYPESCOLTOOLTIP = 9;
		const int CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP = 10;
		const int CNSTGRIDCONTROLTYPESCOLOPENID = 11;
		const int CNSTGRIDLOADDELETECOLAUTOID = 0;
		const int CNSTGRIDLOADDELETECOLCHECK = 1;
		const int CNSTGRIDLOADDELETECOLNAME = 2;
		const int CNSTGRIDLOADDELETECOLDESCRIPTION = 3;
		const int CNSTGRIDLOADDELETELOAD = 0;
		const int CNSTGRIDLOADDELETEDELETE = 1;
		const int CNSTGRIDLOADDELETEEXPORT = 2;
        int originalHeight = 0;

		private struct CustomBillInfo
		{
			public int lngBillID;
			// ID
			public int lngBillFormat;
			// Type Number
			public string strFormatName;
			// Formats Name
			public int intUnits;
			// 0 is inches,1 is centimeters
			public double dblPageHeight;
			public double dblPageWidth;
			public double dblTopMargin;
			public double dblBottomMargin;
			public double dblLeftMargin;
			public double dblRightMargin;
			public bool boolWide;
			// currently not used
			public int lngBillType;
			// currently not used.  Module defined
			public bool boolIsLaser;
			public bool boolIsDeletable;
			// false if it is shipped with TRIO
			public string strDescription;
			// text describing the bill format
			public string strBackgroundImage;
			// background image to display to help line up fields
			public int intDefaultFontSize;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CustomBillInfo(int unusedParam)
			{
				this.lngBillID = 0;
				this.lngBillFormat = 0;
				this.strFormatName = string.Empty;
				this.intUnits = 0;
				this.dblPageHeight = 0;
				this.dblPageWidth = 0;
				this.dblTopMargin = 0;
				this.dblBottomMargin = 0;
				this.dblLeftMargin = 0;
				this.dblRightMargin = 0;
				this.boolWide = false;
				this.lngBillType = 0;
				this.boolIsLaser = false;
				this.boolIsDeletable = false;
				this.strDescription = string.Empty;
				this.strBackgroundImage = string.Empty;
				this.intDefaultFontSize = 0;
			}
		};

		CustomBillInfo CurrentLoadedFormat = new CustomBillInfo(0);

		private void BillImage_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			string strTemp = "";
			// vbPorter upgrade warning: lngRow As int	OnWrite(string, int)
			int lngRow = 0;
			double dblRatio = 0;
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			// vbPorter upgrade warning: intLoop2 As int	OnWriteFCConvert.ToInt32(
			int intLoop2;
			boolDragging = false;
            var source = e.Data.GetData("fecherFoundation.FCLabel") as FCLabel;
            if (source.GetIndex() == 0)
            {
                // group control
                //if (e.Y - lngClickYOffset < 0)
                //{
                //    e.Source.TopOriginal = 0;
                //}
                //else
                //{
                //    if ((e.Y - lngClickYOffset) + e.Source.HeightOriginal > Picture1.HeightOriginal)
                //    {
                //        e.Source.TopOriginal = Picture1.HeightOriginal - e.Source.HeightOriginal;
                //    }
                //    else
                //    {
                //        e.Source.TopOriginal = FCConvert.ToInt32(e.Y - lngClickYOffset);
                //    }
                //}
                //if (e.X - lngClickXOffset < 0)
                //{
                //    e.Source.LeftOriginal = 0;
                //}
                //else
                //{
                //    if ((e.X - lngClickXOffset) + e.Source.WidthOriginal > Picture1.WidthOriginal)
                //    {
                //        e.Source.LeftOriginal = Picture1.WidthOriginal - e.Source.WidthOriginal;
                //    }
                //    else
                //    {
                //        e.Source.LeftOriginal = FCConvert.ToInt32(e.X - lngClickXOffset);
                //    }
                //}
                // now move all controls the same amount
                int lngLeftMove = 0;
                int lngTopMove = 0;
                lngTopMove = source.TopOriginal - ctlGroupControl.TopOriginal;
                lngLeftMove = source.LeftOriginal - ctlGroupControl.LeftOriginal;
                for (intloop = 0; intloop <= (GridHighlighted.Rows - 1); intloop++)
                {
                    (this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).LeftOriginal = (this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).LeftOriginal + lngLeftMove;
                    (this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).TopOriginal = (this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).TopOriginal + lngTopMove;
                    lngRow = FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0));
                    for (intLoop2 = 1; intLoop2 <= (GridFields.Rows - 1); intLoop2++)
                    {
                        if (Conversion.Val(GridFields.TextMatrix(intLoop2, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
                        {
                            lngRow = intLoop2;
                            break;
                        }
                    } // intLoop2
                    dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
                    dblTemp = FCConvert.ToDouble(Strings.Format((this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).LeftOriginal * dblRatio, "0.0000"));
                    GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
                    dblTemp = FCConvert.ToDouble(Strings.Format((this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).TopOriginal * dblRatio, "0.0000"));
                    GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
                    GridFields.RowData(lngRow, true); // mark as changed
                } // intloop
                lngClickXOffset = 0;
                lngClickYOffset = 0;
                this.Picture1.Controls["CustomLabel_0"].Visible = false;
                MakeGroupBoxFromList();
                return;
            }
            else
            {
                // Source.Left = Source.Left - (X - lngClickXOffset)
                // Source.Top = Source.Top - (Y - lngClickYOffset)
                //if (e.Y - lngClickYOffset < 0)
                //{
                //    e.Source.TopOriginal = 0;
                //}
                //else
                //{
                //    if ((e.Y - lngClickYOffset) + e.Source.HeightOriginal > Picture1.HeightOriginal)
                //    {
                //        e.Source.TopOriginal = Picture1.HeightOriginal - e.Source.HeightOriginal;
                //    }
                //    else
                //    {
                //        e.Source.TopOriginal = FCConvert.ToInt32(e.Y - lngClickYOffset);
                //    }
                //}
                //if (e.X - lngClickXOffset < 0)
                //{
                //    e.Source.LeftOriginal = 0;
                //}
                //else
                //{
                //    if ((e.X - lngClickXOffset) + e.Source.WidthOriginal > Picture1.WidthOriginal)
                //    {
                //        e.Source.LeftOriginal = Picture1.WidthOriginal - e.Source.WidthOriginal;
                //    }
                //    else
                //    {
                //        e.Source.LeftOriginal = FCConvert.ToInt32(e.X - lngClickXOffset);
                //    }
                //}
                lngClickXOffset = 0;
                lngClickYOffset = 0;
                // Source.Left = X
                // Source.Top = Y
                // strTemp = Source.Name
                // strTemp = Mid(strTemp, 12) 'chop off the CustomLabel part before the number
                // lngRow = Val(strTemp)
                lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(source.Tag)));
                for (intloop = 1; intloop <= (GridFields.Rows - 1); intloop++)
                {
                    if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
                    {
                        lngRow = intloop;
                        break;
                    }
                } // intloop
                dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
                dblTemp = FCConvert.ToDouble(Strings.Format(source.LeftOriginal * dblRatio, "0.0000"));
                GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
                dblTemp = FCConvert.ToDouble(Strings.Format(source.TopOriginal * dblRatio, "0.0000"));
                GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
                GridFields.RowData(lngRow, true); // mark as changed
                GridFields.Row = lngRow;
            }
        }

		private void BillImage_DragOver(object sender, Wisej.Web.DragEventArgs e)
		{
			// If this covers the picture box, the picture box cant get this event
			// If Not boolDragging Then
			// boolDragging = True
			// lngClickXOffset = X
			// lngClickYOffset = Y
			// End If
		}

		private void BillImage_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)((FCConvert.ToInt32(e.Button) / 0x100000));
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (Button == MouseButtonConstants.LeftButton)
			{
				// if they click in empty space, unhighlight all
				UnhighlightAllControls();
				// start lasso mode
				boolInLassoMode = true;
				lngLassoX = FCConvert.ToInt32(x);
				lngLassoY = FCConvert.ToInt32(Y);
				ctlGroupControl.Left = FCConvert.ToInt32(x);
				ctlGroupControl.Top = FCConvert.ToInt32(Y);
				ctlGroupControl.Width = 0;
				ctlGroupControl.Height = 0;
				ctlGroupControl.Visible = true;
			}
			else
			{
				// only show the copy and paste stuff
				mnuAlignBottoms.Visible = false;
				mnuAlignLefts.Visible = false;
				mnuAlignRights.Visible = false;
				mnuAlignTops.Visible = false;
				mnuBringToFront.Visible = false;
				mnuSendToBack.Visible = false;
				mnuPopSepar1.Visible = false;
				mnuPopSepar2.Visible = false;
				PopupMenu(mnuPop);
			}
		}

		private void BillImage_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int lngX = 0;
			int lngY = 0;
			if (boolInLassoMode)
			{
				lngX = lngLassoX;
				lngY = lngLassoY;
				if (lngX < x)
				{
					ctlGroupControl.WidthOriginal = FCConvert.ToInt32(x - lngX);
					ctlGroupControl.LeftOriginal = lngX;
				}
				else
				{
					ctlGroupControl.WidthOriginal = FCConvert.ToInt32(lngX - x);
					ctlGroupControl.LeftOriginal = FCConvert.ToInt32(x);
				}
				if (lngY < Y)
				{
					ctlGroupControl.HeightOriginal = FCConvert.ToInt32(Y - lngY);
					ctlGroupControl.TopOriginal = lngY;
				}
				else
				{
					ctlGroupControl.HeightOriginal = FCConvert.ToInt32(lngY - Y);
					ctlGroupControl.TopOriginal = FCConvert.ToInt32(Y);
				}
			}
		}

		private void BillImage_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			// vbPorter upgrade warning: lngX1 As int	OnWrite(float, int)
			int lngX1 = 0;
			// vbPorter upgrade warning: lngX2 As int	OnWrite(int, float)
			int lngX2 = 0;
			// vbPorter upgrade warning: lngY1 As int	OnWrite(float, int)
			int lngY1 = 0;
			// vbPorter upgrade warning: lngY2 As int	OnWrite(int, float)
			int lngY2 = 0;
			ctlGroupControl.Visible = false;
			if (boolInLassoMode)
			{
				if (x < lngLassoX)
				{
					lngX1 = FCConvert.ToInt32(x);
					lngX2 = lngLassoX;
				}
				else
				{
					lngX1 = lngLassoX;
					lngX2 = FCConvert.ToInt32(x);
				}
				if (Y < lngLassoY)
				{
					lngY1 = FCConvert.ToInt32(Y);
					lngY2 = lngLassoY;
				}
				else
				{
					lngY1 = lngLassoY;
					lngY2 = FCConvert.ToInt32(Y);
				}
				boolInLassoMode = false;
				ctlGroupControl.Visible = false;
				// now select all that were in the lasso
				for (intloop = 1; intloop <= (GridFields.Rows - 1); intloop++)
				{
					if (FieldCollisionTest(FCConvert.ToInt32(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)), ref lngX1, ref lngY1, ref lngX2, ref lngY2))
					{
						boolInHighlightMode = true;
						HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)));
						GridHighlighted.Rows += 1;
						GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM));
						GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDID));
						GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 2, GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLOPENID));
					}
				}
				// intloop
				HighlightAllInList();
			}
		}
		// vbPorter upgrade warning: intFnum As int	OnWrite(string)
		private bool FieldCollisionTest(int intFnum, ref int lngX1, ref int lngY1, ref int lngX2, ref int lngY2)
		{
			bool FieldCollisionTest = false;
			// tests for a collision between this field and the bounding box
			int lngTestX1;
			int lngTestX2;
			int lngTestY1;
			int lngTestY2;
			FieldCollisionTest = false;
			lngTestX1 = this.Picture1.Controls["CustomLabel_" + intFnum].Left;
			lngTestX2 = this.Picture1.Controls["CustomLabel_" + intFnum].Left + this.Picture1.Controls["CustomLabel_" + intFnum].Width;
			lngTestY1 = this.Picture1.Controls["CustomLabel_" + intFnum].Top;
			lngTestY2 = this.Picture1.Controls["CustomLabel_" + intFnum].Top + this.Picture1.Controls["CustomLabel_" + intFnum].Height;
			// easy tests.
			if (lngTestX1 > lngX2)
				return FieldCollisionTest;
			if (lngTestX2 < lngX1)
				return FieldCollisionTest;
			if (lngTestY1 > lngY2)
				return FieldCollisionTest;
			if (lngTestY2 < lngY1)
				return FieldCollisionTest;
			// something is within the range, test to see if there is true overlap though
			if ((lngTestX1 >= lngX1 && lngTestX1 <= lngX2) || (lngTestX2 >= lngX1 && lngTestX2 <= lngX2) || (lngTestX1 <= lngX1 && lngTestX2 >= lngX2))
			{
				// the widths over lap now see if they also overlap vertically
				if ((lngTestY1 >= lngY1 && lngTestY1 <= lngY2) || (lngTestY2 >= lngY1 && lngTestY2 <= lngY2) || (lngTestY1 <= lngY1 && lngTestY2 >= lngY2))
				{
					FieldCollisionTest = true;
				}
			}
			return FieldCollisionTest;
		}

		private void cmdLoadDeleteCancel_Click(object sender, System.EventArgs e)
		{
			framLoadDelete.Visible = false;
			if (Conversion.Val(GridLoadDelete.Tag) == CNSTGRIDLOADDELETEEXPORT)
			{
				ExportFormat();
			}
		}

		private void ExportFormat()
		{
			// export any of the formats checked in gridloaddelete
			string strTemp = "";
			string strFile = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			bool boolChoseSome;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolChoseSome = false;
				for (x = 1; x <= (GridLoadDelete.Rows - 1); x++)
				{
					if (GridLoadDelete.TextMatrix(x, CNSTGRIDLOADDELETECOLCHECK) != string.Empty)
					{
						if (FCConvert.CBool(GridLoadDelete.TextMatrix(x, CNSTGRIDLOADDELETECOLCHECK)))
						{
							boolChoseSome = true;
						}
					}
				}
				// x
				if (!boolChoseSome)
					return;
				// first see what file they want to save this to
				//MDIParent.InstancePtr.CommonDialog1.DefaultExt = "json";
				//MDIParent.InstancePtr.CommonDialog1.FileName = "CECustomFormExport.json";
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNOverwritePrompt+vbPorterConverter.cdlOFNPathMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//MDIParent.InstancePtr.CommonDialog1.Filter = "JSON|*.json";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Export File To Create";
				//MDIParent.InstancePtr.CommonDialog1.InitDir = Environment.CurrentDirectory;
				//MDIParent.InstancePtr.CommonDialog1.Show();
				//strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				strFile = Path.Combine(FCFileSystem.Statics.UserDataFolder, "CECustomFormExport.json");
				if (strFile == "")
					return;
				if (CreateFormatDatabase(strFile))
				{
					if (!FillFormatDatabase(strFile))
					{
						MessageBox.Show("Could not save data in file", "File not updated", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					MessageBox.Show("Could not create file", "File not created", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				MessageBox.Show("Export complete", "Exported", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //FC:FINAL:MSH - i.issue #1734: wrong variable (strTemp is always empty)
                //FCUtils.Download(strTemp);				return;
                using(var stream = new FileStream(strFile, FileMode.Open))
                {
                    FCUtils.Download(stream, "CECustomFormExport.json");
                }
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (fecherFoundation.Information.Err(ex).Number == 32755/*vbPorterConverter.cdlCancel*/)
					return;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ExportFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: strFile As object	OnWrite(string)
		private bool FillFormatDatabase(string strFile)
		{
			bool FillFormatDatabase = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strDBPath;
			string strDBFile;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngFID = 0;
			int lngNewFID;
            FCFileSystem fso = new FCFileSystem();
			GenericJSONTableSerializer tJSONSer = new GenericJSONTableSerializer();
			TableCollection tTabColl = new TableCollection();
			string strList;
			string strFieldList;
			tTabColl.DBName = "CustomForms";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strDBPath = System.IO.Path.GetDirectoryName(FCConvert.ToString(strFile));
				if (strDBPath != string.Empty)
				{
					if (Strings.Right(strDBPath, 1) != "\\")
					{
						strDBPath += "\\";
					}
				}
				strDBFile = System.IO.Path.GetFileName(FCConvert.ToString(strFile));
				FillFormatDatabase = false;
				strList = "";
				strFieldList = "";
				string strComma;
				string strComma2;
				strComma = "";
				strComma2 = "";
				for (x = 1; x <= (GridLoadDelete.Rows - 1); x++)
				{
					// if row is checked, then export it
					if (GridLoadDelete.TextMatrix(x, CNSTGRIDLOADDELETECOLCHECK) != string.Empty)
					{
						if (FCConvert.CBool(GridLoadDelete.TextMatrix(x, CNSTGRIDLOADDELETECOLCHECK)))
						{
							lngFID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridLoadDelete.TextMatrix(x, CNSTGRIDLOADDELETECOLAUTOID))));
							// copy the format first
							strList += strComma + FCConvert.ToString(lngFID);
							strComma = ",";
							// now save all the fields that go with this format
							clsLoad.OpenRecordset("select * from custombillfields where formatid = " + FCConvert.ToString(lngFID), "tw" + strThisModule + "0000.vb1");
							while (!clsLoad.EndOfFile())
							{
								strFieldList += strComma2 + clsLoad.Get_Fields_Int32("ID");
								strComma2 = ",";
								clsLoad.MoveNext();
							}
						}
					}
				}
				// x
				if (strList != "")
				{
					clsLoad.OpenRecordset("select * from custombills where id in (" + strList + ")", modGlobalVariables.Statics.strCEDatabase);
					string strJSON = "";
					TableItem tTab = new TableItem();
					tTab = clsLoad.TableToTableItem();
					if (!(tTab == null))
					{
						tTabColl.AddItem(tTab);
					}
					if (strFieldList != "")
					{
						clsLoad.OpenRecordset("select * from custombillfields where id in (" + strFieldList + ")", modGlobalVariables.Statics.strCEDatabase);
						tTab = clsLoad.TableToTableItem();
						if (!(tTab == null))
						{
							tTabColl.AddItem(tTab);
						}
					}
					strJSON = tJSONSer.GetJSONFromCollection(ref tTabColl);
					StreamWriter ts;
                    ts = File.CreateText(FCConvert.ToString(strFile));//, true, false);
					ts.Write(strJSON);
					ts.Close();
				}
				FillFormatDatabase = true;
				return FillFormatDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillFormatDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillFormatDatabase;
		}

		private bool CreateFormatDatabase(string strFile)
		{
			bool CreateFormatDatabase = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
                FCFileSystem fso = new FCFileSystem();
				CreateFormatDatabase = false;
				if (FCFileSystem.FileExists(strFile))
				{
                    FCFileSystem.DeleteFile(strFile);//, true);
				}
				CreateFormatDatabase = true;
				return CreateFormatDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateFormatDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateFormatDatabase;
		}

		private void cmdOKControlInfo_Click(object sender, System.EventArgs e)
		{
			// fill gridfields with this new info
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strTemp = "";
			cmdSave.Enabled = true;
			framPage.Enabled = true;
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT))));
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT))));
			}
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLCONTROL, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLALIGNMENT, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONT, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSIZE, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDATA));
			if (fecherFoundation.Strings.UCase(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA)) == "BOLD")
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEBOLD));
			}
			else if (fecherFoundation.Strings.UCase(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA)) == "BOLD ITALIC")
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEBOLDITALIC));
			}
			else if (fecherFoundation.Strings.UCase(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA)) == "ITALIC")
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEITALIC));
			}
			else
			{
				// regular
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEREGULAR));
			}
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLOPENID, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA));
			if (GridControlInfo.Rows > CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS)
			{
				// there were extra parameters
				strTemp = "";
				for (x = CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS; x <= (GridControlInfo.Rows - 1); x++)
				{
					strTemp += GridControlInfo.TextMatrix(x, CNSTGRIDCONTROLINFOCOLDATA) + ";";
				}
				// x
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					// drop the last semi-colon
				}
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLEXTRAPARAMETERS, strTemp);
			}
			GridFields.RowData(GridFields.Row, true);
			// mark as edited
			framControlInfo.Visible = false;
			ReDrawField(GridFields.Row);
			if (!boolInHighlightMode)
			{
				UnhighlightAllControls();
				GridHighlighted.Rows = 1;
				GridHighlighted.TextMatrix(0, 0, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM));
				GridHighlighted.TextMatrix(0, 1, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID));
				GridHighlighted.TextMatrix(0, 2, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLOPENID));
				HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM)));
			}
			GridFields.Focus();
		}

		public void cmdOKControlInfo_Click()
		{
			cmdOKControlInfo_Click(cmdOKControlInfo, new System.EventArgs());
		}

		private void cmdOKControlInfoNew_Click(object sender, System.EventArgs e)
		{
			double dblRatio;
			double dblDefWidth;
			double dblDefHeight;
			// vbPorter upgrade warning: lngW As int	OnWriteFCConvert.ToDouble(
			int lngW;
			// vbPorter upgrade warning: lngH As int	OnWriteFCConvert.ToDouble(
			int lngH;
			int lngFNum;
			int lngRow;
			int lngTemp;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			lngRow = GridFields.Row;
			lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM))));
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			if (Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLCODE)) != Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) || Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA)) != Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLOPENID)))
			{
				// lngTemp = GridControlTypes.FindRow(Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)), , CNSTGRIDCONTROLTYPESCOLCODE)
				// If lngTemp >= 0 Then
				// GridControlTypes.Row = lngTemp
				// End If
				for (x = 1; x <= (GridControlTypes.Rows - 1); x++)
				{
					if (Conversion.Val(GridControlTypes.TextMatrix(x, CNSTGRIDCONTROLTYPESCOLCODE)) == Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)))
					{
						if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA)) == Conversion.Val(GridControlTypes.TextMatrix(x, CNSTGRIDCONTROLTYPESCOLOPENID)))
						{
							GridControlTypes.Row = x;
							break;
						}
					}
				}
				// x
			}
			dblDefWidth = Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH));
			dblDefHeight = Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT));
			if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "CENTIMETERS")
			{
				// defaults are in inches
				dblDefWidth *= 2.54;
				dblDefHeight *= 2.54;
			}
			else
			{
				// inches, do nothing
			}
			lngW = FCConvert.ToInt32(dblDefWidth * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			lngH = FCConvert.ToInt32(dblDefHeight * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				// don't put against top side or it wont be easy to see
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(1));
				// doesn't matter what units we use, just need to see this
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblDefWidth));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = lngW;
                (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = 0;
                (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = FCConvert.ToInt32(dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				// 1 unit
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				// don't put against left side or it wont be easy to see
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(0));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblDefHeight));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(1));
                // doesn't matter what units
                (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = 0;
                (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = lngH;
                (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = 0;
                (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal = FCConvert.ToInt32(dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			}
			else
			{
				if (dblDefWidth > 0)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblDefWidth));
                    (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = lngW;
				}
				if (dblDefHeight > 0)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblDefHeight));
                    (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = lngH;
				}
				if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT))));
				}
				else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT))));
				}
			}
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION, GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLNAME));
			cmdOKControlInfoNew.Visible = false;
			cmdOKControlInfo.Visible = true;
			//FC:FINAL:BBE:#321 - bring the visible button to the front, otherwise the click event will not be raised.
			cmdOKControlInfo.BringToFront();			
			cmdOKControlInfo_Click();
		}
		// Private Sub cmdOKFont_Click()
		// Dim lngFNum As Long
		//
		//
		// GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONT) = GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLNAME)
		// GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSIZE) = GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLSIZE)
		//
		// lngFNum = Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))
		// Me.Controls("CustomLabel" & lngFNum).Font = GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLNAME)
		// Me.Controls("CustomLabel" & lngFNum).Font.Size = Val(GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLSIZE)) * dblSizeRatio / dblPageRatio
		//
		// framFont.Visible = False
		// End Sub
		private void cmdUserDefinedTextOK_Click(object sender, System.EventArgs e)
		{
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, rtbUserText.Text);
			framUserText.Visible = false;
		}

		private void CustomLabel_Click(int Index, object sender, System.EventArgs e)
		{
			// give this focus
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			int lngRow = 0;
			// lngClickXOffset = CustomLabel(Index).Left
			// lngClickYOffset = CustomLabel(Index).Top
			for (intloop = 1; intloop <= (GridFields.Rows - 1); intloop++)
			{
				if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == Index)
				{
					lngRow = intloop;
					break;
				}
			}
			// intloop
			GridFields.Row = lngRow;
			GridFields.TopRow = lngRow;
			GridFields.Focus();
		}

		private void CustomLabel_Click(object sender, System.EventArgs e)
		{
			int index = 0/*CustomLabel.GetIndex((FCLabel)sender)*/;
			CustomLabel_Click(index, sender, e);
		}

		private void CustomLabel_DragDrop(int Index, object sender, Wisej.Web.DragEventArgs e)
		{
			double dblRatio = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			// vbPorter upgrade warning: lngRow As int	OnWrite(string, int)
			int lngRow = 0;
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			// vbPorter upgrade warning: intLoop2 As int	OnWriteFCConvert.ToInt32(
			int intLoop2;
			//FC:TODO
			//if (fecherFoundation.Strings.UCase(e.Source.GetName())=="CUSTOMLABEL" && vbPorterConverter.GetIndex(e.Source)==0) {
			//	// in group mode
			//	if (e.Y-lngClickYOffset+CustomLabel[Index].TopOriginal<0) {
			//		e.Source.TopOriginal = 0;
			//	} else {
			//		if ((e.Y-lngClickYOffset+CustomLabel[Index].TopOriginal)+e.Source.HeightOriginal>Picture1.HeightOriginal) {
			//			e.Source.TopOriginal = Picture1.HeightOriginal-e.Source.HeightOriginal;
			//		} else {
			//			e.Source.TopOriginal = FCConvert.ToInt32(e.Y-lngClickYOffset+CustomLabel[Index].TopOriginal);
			//		}
			//	}
			//	if (e.X - lngClickXOffset+CustomLabel[Index].LeftOriginal<0) {
			//		e.Source.LeftOriginal = 0;
			//	} else {
			//		if ((e.X - lngClickXOffset+CustomLabel[Index].LeftOriginal)+e.Source.WidthOriginal>Picture1.WidthOriginal) {
			//			e.Source.LeftOriginal = Picture1.WidthOriginal-e.Source.WidthOriginal;
			//		} else {
			//			e.Source.LeftOriginal = FCConvert.ToInt32(e.X - lngClickXOffset+CustomLabel[Index].LeftOriginal);
			//		}
			//	}
			//	// now move all controls the same amount
			//	int lngLeftMove = 0;
			//	int lngTopMove = 0;
			//	lngTopMove = e.Source.TopOriginal-ctlGroupControl.TopOriginal;
			//	lngLeftMove = e.Source.LeftOriginal-ctlGroupControl.LeftOriginal;
			//	for(intloop=0; intloop<=(GridHighlighted.Rows-1); intloop++) {
			//		CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].LeftOriginal = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].LeftOriginal+lngLeftMove;
			//		CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].TopOriginal = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].TopOriginal+lngTopMove;
			//		lngRow = FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0));
			//		for(intLoop2=1; intLoop2<=(GridFields.Rows-1); intLoop2++) {
			//			if (Conversion.Val(GridFields.TextMatrix(intLoop2, CNSTGRIDFIELDSCOLFIELDNUM))==lngRow) {
			//				lngRow = intLoop2;
			//				break;
			//			}
			//		} // intLoop2
			//		dblRatio = (dblPageRatio/(dblTwipsPerUnit*dblSizeRatio));
			//		dblTemp = FCConvert.ToDouble(Strings.Format(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].LeftOriginal*dblRatio, "0.0000"));
			//		GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
			//		dblTemp = FCConvert.ToDouble(Strings.Format(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].TopOriginal*dblRatio, "0.0000"));
			//		GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
			//		GridFields.RowData(lngRow, true); // mark as changed
			//	} // intloop
			//	lngClickXOffset = 0;
			//	lngClickYOffset = 0;
			//	CustomLabel[0].Visible = false;
			//	MakeGroupBoxFromList();
			//	return;
			//} else {
			//	if (e.Y-lngClickYOffset+CustomLabel[Index].TopOriginal<0) {
			//		e.Source.TopOriginal = 0;
			//	} else {
			//		if ((e.Y-lngClickYOffset+CustomLabel[Index].TopOriginal)+e.Source.HeightOriginal>Picture1.HeightOriginal) {
			//			e.Source.TopOriginal = Picture1.HeightOriginal-e.Source.HeightOriginal;
			//		} else {
			//			e.Source.TopOriginal = FCConvert.ToInt32(e.Y-lngClickYOffset+CustomLabel[Index].TopOriginal);
			//		}
			//	}
			//	if (e.X - lngClickXOffset+CustomLabel[Index].LeftOriginal<0) {
			//		e.Source.LeftOriginal = 0;
			//	} else {
			//		if ((e.X - lngClickXOffset+CustomLabel[Index].LeftOriginal)+e.Source.WidthOriginal>Picture1.WidthOriginal) {
			//			e.Source.LeftOriginal = Picture1.WidthOriginal-e.Source.WidthOriginal;
			//		} else {
			//			e.Source.LeftOriginal = FCConvert.ToInt32(e.X - lngClickXOffset+CustomLabel[Index].LeftOriginal);
			//		}
			//	}
			//	lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(e.Source.Tag)));
			//	for(intloop=1; intloop<=(GridFields.Rows-1); intloop++) {
			//		if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM))==lngRow) {
			//			lngRow = intloop;
			//			break;
			//		}
			//	} // intloop
			//	dblRatio = (dblPageRatio/(dblTwipsPerUnit*dblSizeRatio));
			//	dblTemp = FCConvert.ToDouble(Strings.Format(e.Source.LeftOriginal*dblRatio, "0.0000"));
			//	GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
			//	dblTemp = FCConvert.ToDouble(Strings.Format(e.Source.TopOriginal*dblRatio, "0.0000"));
			//	GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
			//	GridFields.RowData(lngRow, true); // mark as changed
			//	// Source.Left = X - lngClickXOffset + CustomLabel(Index).Left
			//	// Source.Top = Y - lngClickYOffset + CustomLabel(Index).Top
			//	lngClickXOffset = 0;
			//	lngClickYOffset = 0;
			//	// lngClickXOffset = X
			//	// lngClickYOffset = Y
			//}
		}

		private void CustomLabel_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			int index = 0/*CustomLabel.GetIndex((FCLabel)sender)*/;
			CustomLabel_DragDrop(index, sender, e);
		}

		private void CustomLabel_MouseDown(int Index, object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float x = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			int lngRow = 0;
			int lngTemp = 0;
			// lngClickXOffset = CustomLabel(Index).Left
			// lngClickYOffset = CustomLabel(Index).Top
			if (Button == MouseButtonConstants.LeftButton)
			{
				lngClickXOffset = FCConvert.ToInt32(x);
				lngClickYOffset = FCConvert.ToInt32(Y);
				for (intloop = 1; intloop <= (GridFields.Rows - 1); intloop++)
				{
					if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == Index)
					{
						lngRow = intloop;
						break;
					}
				}
				// intloop
				if (Shift != FCConvert.ToInt32(ShiftConstants.CtrlMask))
				{
					if (!IsInHighlightList(ref Index) || !boolInHighlightMode || (IsInHighlightList(ref Index) && GridHighlighted.Rows <= 1))
					{
						boolInHighlightMode = false;
						UnhighlightAllControls();
						GridHighlighted.Rows = 1;
						GridHighlighted.TextMatrix(0, 0, FCConvert.ToString(Index));
						GridHighlighted.TextMatrix(0, 1, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
						GridHighlighted.TextMatrix(0, 2, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLOPENID))));
						ShowGroupBox(false);
					}
					else
					{
						if (GridHighlighted.Rows > 0)
						{
							lngTemp = GridHighlighted.FindRow(Index, 0, 0);
							if (lngTemp >= 0)
							{
								GridHighlighted.RemoveItem(lngTemp);
								GridHighlighted.Rows += 1;
								GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, FCConvert.ToString(Index));
								GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
								GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 2, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLOPENID))));
							}
							MakeGroupBoxFromList();
						}
					}
				}
				else
				{
					boolInHighlightMode = true;
					if (GridHighlighted.Rows > 0)
					{
						// don't want duplicate entries
						lngTemp = GridHighlighted.FindRow(Index, 0, 0);
						if (lngTemp >= 0)
						{
							GridHighlighted.RemoveItem(lngTemp);
						}
					}
					GridHighlighted.Rows += 1;
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, FCConvert.ToString(Index));
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 2, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLOPENID))));
					ShowGroupBox();
				}
				(this.Picture1.Controls["CustomLabel_" + Index] as FCLabel).BackStyle = 1;
				this.Picture1.Controls["CustomLabel_" + Index].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				HighlightAllInList();
				GridFields.Row = lngRow;
				GridFields.TopRow = lngRow;
				GridFields.Focus();
				if (Shift != FCConvert.ToInt32(ShiftConstants.CtrlMask))
				{
					if (!boolInHighlightMode)
					{
						CustomLabel_0.Visible = false;
						CustomLabel_0.BorderStyle = FCConvert.ToInt32(BorderStyle.None);
						(this.Picture1.Controls["CustomLabel_" + Index] as FCLabel).Drag();
					}
					else
					{
						CustomLabel_0.Visible = true;
						(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).BackStyle = 0;
						CustomLabel_0.BorderStyle = FCConvert.ToInt32(BorderStyle.None);
						// CustomLabel(Index).Drag
						MakeGroupBoxFromList();
						CustomLabel_0.LeftOriginal = ctlGroupControl.LeftOriginal;
						CustomLabel_0.TopOriginal = ctlGroupControl.TopOriginal;
						CustomLabel_0.HeightOriginal = ctlGroupControl.HeightOriginal;
						CustomLabel_0.WidthOriginal = ctlGroupControl.WidthOriginal;
						// adjust the offsets since we clicked on a field not the customlabel(0) ctl
						lngClickYOffset = this.Picture1.Controls["CustomLabel_" + Index].Top - CustomLabel_0.TopOriginal + lngClickYOffset;
						lngClickXOffset = this.Picture1.Controls["CustomLabel_" + Index].Left - CustomLabel_0.LeftOriginal + lngClickXOffset;
						CustomLabel_0.Drag();
					}
				}
			}
			else if (Button == MouseButtonConstants.RightButton)
			{
				// lets pop up a menu
				// show all of the options
				mnuAlignBottoms.Visible = true;
				mnuAlignLefts.Visible = true;
				mnuAlignRights.Visible = true;
				mnuAlignTops.Visible = true;
				mnuBringToFront.Visible = true;
				mnuSendToBack.Visible = true;
				mnuPopSepar1.Visible = true;
				mnuPopSepar2.Visible = true;
				PopupMenu(mnuPop);
			}
		}

		private void CustomLabel_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
            string name = ((FCLabel)sender).Name;
            int index = FCConvert.ToInt32(name.Substring(name.LastIndexOf("_") + 1));
            CustomLabel_MouseDown(index, sender, e);
		}

        private void CustomLabel_0_MouseUp(object sender, MouseEventArgs e)
        {
            BillImage_DragDrop(sender, new DragEventArgs(DragDropEffects.Move, DragDropEffects.Move, sender, e.Location));
        }

        private bool IsInHighlightList(ref int intFieldNum)
		{
			bool IsInHighlightList = false;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			IsInHighlightList = false;
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				if (FCConvert.ToDouble(GridHighlighted.TextMatrix(x, 0)) == intFieldNum)
				{
					IsInHighlightList = true;
					return IsInHighlightList;
				}
			}
			// x
			return IsInHighlightList;
		}

		private object HighlightAllInList()
		{
			object HighlightAllInList = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				this.Picture1.Controls["CustomLabel_" +FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].BackColor = ColorTranslator.FromOle(Information.RGB(155, 193, 223));
			}
			// x
			if (GridHighlighted.Rows > 0)
			{
				this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0))].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			}
			return HighlightAllInList;
		}

		private void UnhighlightAllControls(bool boolLeaveLast = false)
		{
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			int lngRow;
			ShowGroupBox(false);
			for (intloop = (GridHighlighted.Rows - 1); intloop >= 0; intloop--)
			{
				if (!boolLeaveLast || intloop < GridHighlighted.Rows - 1)
				{
					if (Conversion.Val(GridHighlighted.TextMatrix(intloop, 1)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
					{
						(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).BackStyle = 1;
						this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].BackColor = Color.Black;
					}
					else
					{
						this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].BackColor = Color.White;
						(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))] as FCLabel).BackStyle = 0;
					}
					// If Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM)) > 0 Then
					// If Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDID)) = CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED Then
					// CustomLabel(Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM))).BackStyle = 1
					// CustomLabel(Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM))).BackColor = vbBlack
					// Else
					// CustomLabel(Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM))).BackStyle = vbTransparent
					// End If
					// End If
					GridHighlighted.RemoveItem(intloop);
				}
			}
			// intloop
			//App.DoEvents();
		}

		private void MakeGroupBoxFromList()
		{
			int lngL;
			int lngR;
			int lngT;
			int lngB;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngTemp = 0;
			// make a bounding box for all selected controls
			// initialize so it wont fail
			lngB = 0;
			lngT = 200000;
			lngR = 0;
			lngL = 200000;
			if (GridHighlighted.Rows > 0)
			{
				for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
				{
					if (this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Left < lngL)
					{
						lngL = this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Left;
					}
					if (this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Top < lngT)
					{
						lngT = this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Top;
					}
					lngTemp = this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Left + this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Width;
					if (lngTemp > lngR)
					{
						lngR = lngTemp;
					}
					lngTemp = this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Top + this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].Height;
					if (lngTemp > lngB)
					{
						lngB = lngTemp;
					}
				}
				// x
			}
			else
			{
				lngT = 0;
				lngL = 0;
			}
			ctlGroupControl.LeftOriginal = lngL;
			ctlGroupControl.TopOriginal = lngT;
			ctlGroupControl.HeightOriginal = lngB - lngT;
			ctlGroupControl.WidthOriginal = lngR - lngL;
			lngGroupBoxBottom = lngB;
			lngGroupBoxLeft = lngL;
			lngGroupBoxRight = lngR;
			lngGroupBoxTop = lngT;
		}

		private void ShowGroupBox(bool boolMakeVisible = true)
		{
			if (boolMakeVisible)
			{
				// ctlGroupControl.Visible = True
				ctlGroupControl.Visible = false;
				MakeGroupBoxFromList();
			}
			else
			{
				ctlGroupControl.Visible = false;
				MakeGroupBoxFromList();
			}
		}
		// vbPorter upgrade warning: lngCNum As int	OnWrite(string, int)
		private void HighlightControl(int lngCNum)
		{
            (this.Picture1.Controls["CustomLabel_" + lngCNum] as FCLabel).BackStyle = 1;
            this.Picture1.Controls["CustomLabel_" + lngCNum].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			//App.DoEvents();
		}

		private void frmCustomBill_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int lngFNum;
			int lngPosition;
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						if (cmdSave.Enabled)
						{
							if (MessageBox.Show("Are you sure you want to exit the screen?", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								mnuExit_Click();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmCustomBill_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomBill properties;
			//frmCustomBill.FillStyle	= 0;
			//frmCustomBill.ScaleWidth	= 9225;
			//frmCustomBill.ScaleHeight	= 7935;
			//frmCustomBill.LinkTopic	= "Form2";
			//frmCustomBill.LockControls	= -1  'True;
			//frmCustomBill.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
            //FC:FINAL:MSH - i.issue #1683: restore calculating dblSizeRatio for next calculations
            dblSizeRatio = RatioLine.X2 / 1440F;
			SetupGridControlTypes();
			SetupGridControlInfo();
			SetupGridBillSize();
			SetupGridFields();
			SetupGridLoadDelete();
			InitializeValues();
			ResizePage();
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("CustomBillHorizontalIncrements"));
			if (Conversion.Val(strTemp) == 0)
			{
                // default to full
                intCharWid = 144; 
			}
			else
			{
				intCharWid = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			}
			strTemp = FCConvert.ToString(modRegistry.GetRegistryKey("CustomBillVerticalIncrements"));
			if (Conversion.Val(strTemp) == 0)
			{
				// default to full
				intLineHite = 240;
			}
			else
			{
				intLineHite = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			}
			if (intCharWid == 72)
			{
				mnuFullCharFullLine.Checked = false;
				mnuMoveHalfCharHalfLine.Checked = true;
				mnuMoveQuarterCharQuarterLine.Checked = false;
			}
			else if (intCharWid == 36)
			{
				mnuFullCharFullLine.Checked = false;
				mnuMoveHalfCharHalfLine.Checked = false;
				mnuMoveQuarterCharQuarterLine.Checked = true;
			}
			else
			{
				mnuFullCharFullLine.Checked = true;
				mnuMoveQuarterCharQuarterLine.Checked = false;
				mnuMoveHalfCharHalfLine.Checked = false;
			}
			LoadCustomBill(CurrentLoadedFormat.lngBillID, "twUT0000.vb1");
		}

		private void InitializeValues()
		{
			// initializes things like the pageratio etc.
			// this defaults to letter size
			lngPageWidth = 8.5F * 1440;
			// assuming no margins
			lngPageHeight = 11F * 1440;
            // assuming no margins
            //FC:FINAL:AM - make preview larger 50%
            //dblPageRatio = 1.5;
            dblPageRatio = 0.8;
            // The beginning size will be half (not including dblSizeRatio when the form is resized) of the page size
            dblTwipsPerUnit = 1440;
			// inches
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Inches");
			lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
			lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
			lngOffsetY = 240;
			lngOffsetX = 120;
			//VScroll1.Maximum = 100;
			//HScroll1.Maximum = 100;
			Picture1.BackColor = Color.White;
		}

		private void ResizePage()
		{
			// resizes the picture box
			double dblTemp;
			int lngTemp;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			Picture1.WidthOriginal = FCConvert.ToInt32(lngScreenPageWidth * dblSizeRatio);
			Picture1.HeightOriginal = FCConvert.ToInt32(lngScreenPageHeight * dblSizeRatio);
            //FC:FINAL:DSE:#i1719 Adjust frame height to fit the picture
            //framPage.HeightOriginal = Picture1.TopOriginal + Picture1.HeightOriginal;
            for (x = 1; x <= (GridFields.Rows - 1); x++)
			{
				if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)) > 0)
				{
					this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))].Font, FCConvert.ToInt32((dblSizeRatio / dblPageRatio * Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFONTSIZE)))));
					(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))] as FCLabel).LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
					(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))] as FCLabel).HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
					(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))] as FCLabel).WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
					(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))] as FCLabel).TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				}
			}
			// x
			BillImage.Width = Picture1.Width;
			BillImage.Height = Picture1.Height;
			// lngTemp = Picture1.Height
			// need to make the 0 to max of the scroll bar the height of the page - the visible part of the page
			// so when you scroll all the way it doesn't scroll the page out of sight
			// lngTemp = (lngTemp - (framPage.Height)) / 20   'subtract the visible frame height (leave the bottom offset)
			// VScroll1.Max = lngTemp + lngOffsetY
			VScroll1_Change();
			HScroll1_Change();
		}

		private void frmCustomBill_Resize(object sender, System.EventArgs e)
		{
            //FC:FINAL:MSH - i.issue #1683: restore calculating dblSizeRatio for next calculations
            dblSizeRatio = RatioLine.X2 / 1440F;
			// line is originally 1440 wide (1 inch)
			lngOffsetY = 240;
			lngOffsetX = 120;
            originalHeight = GridControlInfo.Height;
            ResizePage();
			ResizeGridFields();
			ResizeGridBillSize();
			ResizeGridControlInfo();
			ResizeGridControlTypes();
			ResizeGridLoadDelete();
			cmdOKControlInfo.TopOriginal = (framControlInfo.HeightOriginal) - cmdOKControlInfo.HeightOriginal - 50;
			cmdOKControlInfoNew.TopOriginal = cmdOKControlInfo.TopOriginal;
        }

		private void GridBillSize_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			switch (GridBillSize.Row)
			{
				case CNSTGRIDBILLSIZEROWSTYLE:
					{
						GridBillSize.ComboList = "Laser";
						break;
					}
				case CNSTGRIDBILLSIZEROWUNITS:
					{
						GridBillSize.ComboList = "Centimeters|Inches";
						break;
					}
				case CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE:
					{
						if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)) != "NONE" && fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)) != "CHOOSE NEW")
						{
							GridBillSize.ComboList = fecherFoundation.Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)) + "|None|Choose New";
						}
						else
						{
							GridBillSize.ComboList = "None|Choose New";
						}
						break;
					}
				case CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE:
					{
						GridBillSize.ComboList = "8|9|10|12|14|16";
						break;
					}
				default:
					{
						GridBillSize.ComboList = string.Empty;
						// don't want a combo
						break;
					}
			}
			//end switch
		}

		private void GridBillSize_ChangeEdit(object sender, System.EventArgs e)
		{
			string strTemp = "";
			string strOld = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (GridBillSize.Row == CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE)
				{
					if (fecherFoundation.Strings.UCase(GridBillSize.EditText) == "NONE")
					{
						// BillImage.Visible = False
						BillImage.Image = null;
						GridBillSize.ComboList = "None|Choose New";
						GridBillSize.EditText = "None";
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, "None");
						GridBillSize.Col = CNSTGRIDBILLSIZECOLDESCRIPTION;
						GridBillSize.Refresh();
						GridBillSize.Col = CNSTGRIDBILLSIZECOLDATA;
					}
					else if (fecherFoundation.Strings.UCase(GridBillSize.EditText) == "CHOOSE NEW")
					{
						strOld = GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA);
						// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
						MDIParent.InstancePtr.CommonDialog1_Open.DialogTitle = "Load Image";
						MDIParent.InstancePtr.CommonDialog1_Open.FileName = "";
						MDIParent.InstancePtr.CommonDialog1_Open.Filter = "Images (*.bmp,*.gif,*.jpg)|*.bmp;*.gif;*.jpg";
						MDIParent.InstancePtr.CommonDialog1_Open.InitDir = Environment.CurrentDirectory;
						//- MDIParent.InstancePtr.CommonDialog1.CancelError = false;
						MDIParent.InstancePtr.CommonDialog1_Open.ShowOpen();
						strTemp = MDIParent.InstancePtr.CommonDialog1_Open.FileName;
						if (strTemp != string.Empty)
						{
							BillImage.Image = FCUtils.LoadPicture(strTemp);
							BillImage.WidthOriginal = Picture1.WidthOriginal;
							BillImage.HeightOriginal = Picture1.HeightOriginal;
							BillImage.Visible = true;
							GridBillSize.ComboList = fecherFoundation.Strings.Trim(strTemp) + "|None|Choose New";
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, fecherFoundation.Strings.Trim(strTemp));
							GridBillSize.EditText = strTemp;
						}
						else
						{
							GridBillSize.EditText = strOld;
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, strOld);
						}
					}
					else
					{
						// don't need to do anything
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GridBillSize_ChangeEdit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridBillSize_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            // vbPorter upgrade warning: dblTemp As double	OnWrite(double, string)
			double dblTemp = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			// vbPorter upgrade warning: lngPHeight As int	OnWriteFCConvert.ToDouble(
			int lngPHeight;
			// vbPorter upgrade warning: lngLMargin As int	OnWriteFCConvert.ToDouble(
			int lngLMargin;
			// vbPorter upgrade warning: lngRMargin As int	OnWriteFCConvert.ToDouble(
			int lngRMargin;
			// vbPorter upgrade warning: lngTMargin As int	OnWriteFCConvert.ToDouble(
			int lngTMargin;
			// vbPorter upgrade warning: lngBMargin As int	OnWriteFCConvert.ToDouble(
			int lngBMargin;
			// vbPorter upgrade warning: lngPWidth As int	OnWriteFCConvert.ToDouble(
			int lngPWidth;
			string strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;

            //FC:FINAL:MSH - i.issue #1683: added saving and using correct cell indexes
            int row = GridBillSize.GetFlexRowIndex(e.RowIndex);
            int col = GridBillSize.GetFlexColIndex(e.ColumnIndex);

			switch (row)
			{
				case CNSTGRIDBILLSIZEROWUNITS:
					{
						strTemp = fecherFoundation.Strings.UCase(GridBillSize.EditText);
						if (strTemp != fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) && fecherFoundation.Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) != string.Empty)
						{
							// changed units so convert
							if (strTemp == "CENTIMETERS")
							{
								// from inch to cent
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp * 2.54, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp * 2.54, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								for (x = 1; x <= (GridFields.Rows - 1); x++)
								{
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTOP));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLLEFT));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLHEIGHT));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLWIDTH));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
								}
								// x
							}
							else if (strTemp == "INCHES")
							{
								// from cent to inch
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								for (x = 1; x <= (GridFields.Rows - 1); x++)
								{
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTOP));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLLEFT));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLHEIGHT));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLWIDTH));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
								}
								// x
							}
						}
						else
						{
							strTemp = fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA));
						}
						break;
					}
				default:
					{
						strTemp = fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA));
						break;
					}
			}
			//end switch
			if (strTemp == "CENTIMETERS")
			{
				dblTwipsPerUnit = 567;
			}
			else if (strTemp == "INCHES")
			{
				dblTwipsPerUnit = 1440;
			}
			else
			{
			}
			dblTemp = Conversion.Val(GridBillSize.EditText);
			lngTemp = FCConvert.ToInt32(dblTemp * dblTwipsPerUnit);
			lngPHeight = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngPWidth = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngLMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngRMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngTMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngBMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			switch (row)
			{
				case CNSTGRIDBILLSIZEROWBOTTOMMARGIN:
					{
						lngPageHeight = lngPHeight - lngTMargin - lngTemp;
						lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWTOPMARGIN:
					{
						lngPageHeight = lngPHeight - lngTemp - lngBMargin;
						lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWPAGEHEIGHT:
					{
						lngPageHeight = lngTemp - lngTMargin - lngBMargin;
						lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWPAGEWIDTH:
					{
						lngPageWidth = lngTemp - lngLMargin - lngRMargin;
						lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWLEFTMARGIN:
					{
						lngPageWidth = lngPWidth - lngTemp - lngRMargin;
						lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWRIGHTMARGIN:
					{
						lngPageWidth = lngPWidth - lngLMargin - lngTemp;
						lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
						break;
					}
			}
			//end switch
			ResizePage();
		}

		private void GridControlInfo_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// change dropdowns
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			GridControlInfo.ComboList = "";
			switch (GridControlInfo.Row)
			{
				case CNSTGRIDCONTROLINFOROWTEXT:
					{
						if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
						{
							clsLoad.OpenRecordset("select * from custombills where ID <> " + FCConvert.ToString(lngCustomBillID) + " order by formatname", "twce0000.vb1");
							while (!clsLoad.EndOfFile())
							{
								strTemp += "#" + clsLoad.Get_Fields_Int32("ID") + ";" + clsLoad.Get_Fields_String("FormatName") + "|";
								clsLoad.MoveNext();
							}
							if (strTemp != string.Empty)
							{
								strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
							}
							GridControlInfo.ComboList = strTemp;
						}
						else
						{
							GridControlInfo.ComboList = "...";
						}
						break;
					}
				case CNSTGRIDCONTROLINFOROWTYPE:
					{
						GridControlInfo.ComboList = "...";
						break;
					}
				case CNSTGRIDCONTROLINFOROWALIGN:
					{
						GridControlInfo.ComboList = "Left|Right|Center";
						break;
					}
				case CNSTGRIDCONTROLINFOROWFONT:
					{
						GridControlInfo.ComboList = "Courier New|Tahoma";
						break;
					}
				case CNSTGRIDCONTROLINFOROWFONTSIZE:
					{
						GridControlInfo.ComboList = "8|9|10|12|14|16";
						break;
					}
				case CNSTGRIDCONTROLINFOROWFONTSTYLE:
					{
						GridControlInfo.ComboList = "Regular|Bold|Italic|Bold Italic";
						break;
					}
				default:
					{
						if (GridControlInfo.Row >= CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS)
						{
							GridControlInfo.ComboList = FCConvert.ToString(GridControlInfo.RowData(GridControlInfo.Row));
						}
						break;
					}
			}
			//end switch
		}

		private void ReshowimgControlImage(string strFileName)
		{
			double dblPicSizeRatio = 0;
			if (fecherFoundation.Strings.Trim(strFileName) != string.Empty)
			{
				imgControlImage.Image = FCUtils.LoadPicture(strFileName);
				//imgControlImage.WidthOriginal = framControlInfo.WidthOriginal - imgControlImage.LeftOriginal - 100;
				// make 100 pixels less than border
				//imgControlImage.HeightOriginal = framControlInfo.HeightOriginal - imgControlImage.TopOriginal - 45;
                imgControlImage.Size = new System.Drawing.Size(336, 177);
                // now make the image proportional, not stretched funny
                dblPicSizeRatio = imgControlImage.Image.Width / imgControlImage.Image.Height;
				if (imgControlImage.HeightOriginal * dblPicSizeRatio > imgControlImage.WidthOriginal)
				{
					// shape is the other way
					imgControlImage.HeightOriginal = FCConvert.ToInt32(imgControlImage.WidthOriginal / dblPicSizeRatio);
				}
				else
				{
					imgControlImage.WidthOriginal = FCConvert.ToInt32(imgControlImage.HeightOriginal * dblPicSizeRatio);
				}
			}
			else
			{
				imgControlImage.Image = null;
			}
		}

		private void GridControlInfo_CellButtonClick(object sender, EventArgs e)
		{
			string strTemp = "";
			int lngTemp = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
            framUserText.CenterToContainer(this.ClientArea);
            switch (GridControlInfo.Row)
			{
				case CNSTGRIDCONTROLINFOROWTEXT:
					{
						if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
						{
							// get image path and file name
							MDIParent.InstancePtr.CommonDialog1_Open.DialogTitle = "Load Image";
							MDIParent.InstancePtr.CommonDialog1_Open.FileName = "";
							MDIParent.InstancePtr.CommonDialog1_Open.Filter = "Images (*.bmp,*.gif,*.jpg)|*.bmp;*.gif;*.jpg";
							// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
							MDIParent.InstancePtr.CommonDialog1_Open.InitDir = Environment.CurrentDirectory;
							MDIParent.InstancePtr.CommonDialog1_Open.ShowOpen();
							strTemp = MDIParent.InstancePtr.CommonDialog1_Open.FileName;
							if (strTemp != string.Empty)
							{
								GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
							}
							ReshowimgControlImage(strTemp);
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							// must size the control so text will wrap the same
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMDATE)
						{
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
						{
							// must select a predefined report
							// lngTemp = frmChooseCustomBillType.Init(strThisModule, "tw" & strThisModule & "0000.vb1")
							// If lngTemp < 0 Then
							// strTemp = "Sub Report"
							// lngTemp = 0
							// GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA) = strTemp
							// GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT) = lngTemp
							// Else
							// strTemp = "Sub Report"
							// Call clsLoad.OpenRecordset("select formatname from custombills where ID = " & lngTemp, "tw" & strThisModule & "0000.vb1")
							// If Not clsLoad.EndOfFile Then
							// strTemp = clsLoad.Fields("formatname")
							// End If
							// GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA) = strTemp
							// GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT) = lngTemp
							// End If
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
						{
							lngTemp = frmChooseDynamicReport.InstancePtr.Init(strThisModule, modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL);
							if (lngTemp == 0)
							{
								// cancelled
								strTemp = GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA);
								if (fecherFoundation.Strings.Trim(strTemp) == string.Empty)
								{
									strTemp = "Dynamic Report";
								}
							}
							else if (lngTemp < 0)
							{
								// no reports
								MessageBox.Show("There are no dynamic documents to load");
								strTemp = "Dynamic Report";
								lngTemp = 0;
								GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
								GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, lngTemp);
							}
							else
							{
								strTemp = "Dynamic Report";
								clsLoad.OpenRecordset("select text from DYNAMICREPORTS where reporttype = " + FCConvert.ToString(modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL) + " and ID = " + FCConvert.ToString(lngTemp), "tw" + strThisModule + "0000.vb1");
								if (!clsLoad.EndOfFile())
								{
									strTemp = FCConvert.ToString(clsLoad.Get_Fields_String("text"));
								}
								GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
								GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, lngTemp);
							}
							GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT)));
						}
						break;
					}
				case CNSTGRIDCONTROLINFOROWTYPE:
					{
						GridControlTypes.Visible = true;
						//framControlInfo.HeightOriginal = (framPage.TopOriginal + framPage.HeightOriginal) - framControlInfo.TopOriginal;
						// framControlInfo.Height = (framPage.Top) - framControlInfo.Top
						GridControlTypes.BringToFront();
						break;
					}
			}
			//end switch
		}

		private void GridControlInfo_ComboCloseUp(object sender, EventArgs e)
		{
			switch (GridControlInfo.Row)
			{
				case CNSTGRIDCONTROLINFOROWTEXT:
					{
						if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
						{
							GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, GridControlInfo.ComboData());
						}
						break;
					}
			}
			//end switch
		}

		private void GridControlInfo_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridControlInfo[e.ColumnIndex, e.RowIndex];
            int lngX;
			int lngY;
			lngX = GridControlInfo.GetFlexColIndex(e.ColumnIndex);
			lngY = GridControlInfo.GetFlexRowIndex(e.RowIndex);
			if (lngY < 0)
			{
                //ToolTip1.SetToolTip(GridControlInfo, "");
                cell.ToolTipText = "";
				return;
			}
			//ToolTip1.SetToolTip(GridControlInfo, GridControlInfo.TextMatrix(lngY, CNSTGRIDCONTROLINFOCOLTOOLTIP));
			cell.ToolTipText = GridControlInfo.TextMatrix(lngY, CNSTGRIDCONTROLINFOCOLTOOLTIP);
		}

		private void GridControlInfo_RowColChange(object sender, System.EventArgs e)
		{
			int lngTemp;
			GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			if (GridControlInfo.Col == CNSTGRIDCONTROLINFOCOLDATA)
			{
				switch (GridControlInfo.Row)
				{
					case CNSTGRIDCONTROLINFOROWTEXT:
						{
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA))));
							if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
							{
								// GridControlInfo.Editable = flexEDKbdMouse
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
							{
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMDATE)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLSIGNATURE)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							break;
						}
				}
				//end switch
			}
		}

		private void GridControlTypes_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow;
			// vbPorter upgrade warning: lngCode As int	OnWrite(string)
			int lngCode;
            FCFileSystem fso = new FCFileSystem();
			lngRow = GridControlTypes.Row;
			if (lngRow < 1)
				return;
			lngCode = FCConvert.ToInt32(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLCODE));
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) != lngCode)
			{
				// clear the extra parameters since they belong to a different code
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLEXTRAPARAMETERS, "");
			}
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA, FCConvert.ToString(lngCode));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLNAME));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLOPENID));
			if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
			{
				imgControlImage.Visible = true;
				if (fecherFoundation.Strings.Trim(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA)) != string.Empty)
				{
					if (FCFileSystem.FileExists(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA)))
					{
						imgControlImage.Image = FCUtils.LoadPicture(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA));
					}
					else
					{
						imgControlImage.Visible = false;
					}
				}
				else
				{
					imgControlImage.Image = null;
				}
			}
			else
			{
				imgControlImage.Image = null;
				imgControlImage.Visible = false;
			}
			ResizeGridControlInfo();
			//framControlInfo.HeightOriginal = framPage.TopOriginal - framControlInfo.TopOriginal;
			GridControlTypes.Visible = false;
		}

		private void GridControlTypes_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridControlTypes[e.ColumnIndex, e.RowIndex];
            int lngX;
			int lngY;
			lngX = GridControlTypes.GetFlexColIndex(e.ColumnIndex);
			lngY = GridControlTypes.GetFlexRowIndex(e.RowIndex);
            if (lngY < 0)
            {
                return;
            }
			//ToolTip1.SetToolTip(GridControlTypes, GridControlTypes.TextMatrix(lngY, CNSTGRIDCONTROLTYPESCOLTOOLTIP));
			cell.ToolTipText = GridControlTypes.TextMatrix(lngY, CNSTGRIDCONTROLTYPESCOLTOOLTIP);
		}

		private void GridFields_CellButtonClick(object sender, EventArgs e)
		{
			string strTemp = "";
			int lngTemp;
            FCFileSystem fso = new FCFileSystem();
			framPage.Enabled = false;
			cmdSave.Enabled = false;
			boolInHighlightMode = false;
			UnhighlightAllControls();
			HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM)));
			switch (GridFields.Col)
			{
				case CNSTGRIDFIELDSCOLCONTROL:
					{
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLALIGNMENT));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONT));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSIZE));
						if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEBOLD)
						{
							strTemp = "Bold";
						}
						else if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEBOLDITALIC)
						{
							strTemp = "Bold Italic";
						}
						else if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEITALIC)
						{
							strTemp = "Italic";
						}
						else
						{
							// regular
							strTemp = "Regular";
						}
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLOPENID));
						// lngTemp = GridControlTypes.FindRow(Val(GridFields.TextMatrix(Row, CNSTGRIDFIELDSCOLFIELDID)), , CNSTGRIDCONTROLTYPESCOLCODE)
						// If lngTemp >= 0 Then
						// GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA) = GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME)
						// Else
						// shouldn't be able to happen
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, "");
						// End If
						// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
						int x;
						for (x = 1; x <= (GridControlTypes.Rows - 1); x++)
						{
							if (Conversion.Val(GridControlTypes.TextMatrix(x, CNSTGRIDCONTROLTYPESCOLCODE)) == Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)))
							{
								if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA)) == Conversion.Val(GridControlTypes.TextMatrix(x, CNSTGRIDCONTROLTYPESCOLOPENID)))
								{
									GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(x, CNSTGRIDCONTROLTYPESCOLNAME));
									break;
								}
							}
						}
						// x
						framControlInfo.Visible = true;
						ResizeGridControlInfo();
						if (FCConvert.ToDouble(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
						{
							if (FCFileSystem.FileExists(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT)))
							{
								ReshowimgControlImage(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT));
							}
							else
							{
								imgControlImage.Image = null;
							}
						}
						framControlInfo.BringToFront();
						break;
					}
			}
			//end switch
		}

		private void GridFields_ClickEvent(object sender, System.EventArgs e)
		{
			if (GridFields.Row < 1)
				return;
			boolInHighlightMode = false;
			UnhighlightAllControls();
			HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM)));
			GridHighlighted.Rows = 1;
			GridHighlighted.TextMatrix(0, 0, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM));
			GridHighlighted.TextMatrix(0, 1, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID));
			GridHighlighted.TextMatrix(0, 2, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLOPENID));
		}

		private void GridFields_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string, int)
			int lngFNum = 0;
			// vbPorter upgrade warning: lngPosition As int	OnWrite(double, int)
			int lngPosition = 0;
			double dblRatio = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			int intloop;
			// vbPorter upgrade warning: lngWidth As int	OnWrite(double, int)
			float lngWidth = 0;
			// vbPorter upgrade warning: lngHeight As int	OnWriteFCConvert.ToDouble(
			int lngHeight = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp = 0;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						if (boolInHighlightMode)
							return;
						mnuAddField_Click();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (!boolInHighlightMode)
						{
							mnuDeleteField_Click();
						}
						else
						{
							if (MessageBox.Show("Are you sure you want to delete all fields highlighted?", "Delete All Selected?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
							{
								for (x = (GridHighlighted.Rows - 1); x >= 0; x--)
								{
									lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
									this.Picture1.Controls.Remove(this.Picture1.Controls["CustomLabel_" + lngFNum]);
									//CustomLabel.Unload(lngFNum);
									// now get rid of the row
									for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
									{
										if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLAUTOID)) > 0)
											{
												// if its < 0 then it was never saved, so don't have to worry about it
												GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLAUTOID))));
											}
											GridFields.RemoveItem(Y);
											break;
										}
									}
									// Y
									GridHighlighted.RemoveItem(x);
								}
								// x
								boolInHighlightMode = false;
								ShowGroupBox(false);
							}
						}
						break;
					}
				case Keys.Left:
					{
						// move control left one grid unit (character width?)
						if (Shift == FCConvert.ToInt32(ShiftConstants.CtrlMask))
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal - (intCharWid * dblSizeRatio / dblPageRatio));
										if (lngPosition < 0)
											lngPosition = 0;
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngLeftMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.LeftOriginal - (intCharWid * dblSizeRatio / dblPageRatio));
								if (lngPosition < 0)
									lngPosition = 0;
								lngLeftMove = ctlGroupControl.LeftOriginal - lngPosition;
								if (lngLeftMove > 0)
								{
									for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
										lngPosition = (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal - lngLeftMove;
										if (lngPosition <= 0)
											lngPosition = 0;
										(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))] as FCLabel).LeftOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// x
								}
								ShowGroupBox();
							}
						}
						else if (Shift == FCConvert.ToInt32(ShiftConstants.ShiftMask))
						{
							// make it smaller by a character width
							KeyCode = 0;
							if (boolInHighlightMode)
							{
								// can't resize more than one at a time
								return;
							}
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngWidth = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal - (intCharWid * dblSizeRatio / dblPageRatio));
									if (lngWidth < intCharWid)
										lngWidth = intCharWid;
									(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = FCConvert.ToInt32(lngWidth);
									dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
									dblTemp = FCConvert.ToDouble(Strings.Format(lngWidth * dblRatio, "0.0000"));
									GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
									GridFields.RowData(GridFields.Row, true);
								}
							}
						}
						break;
					}
				case Keys.Right:
					{
						// move control right one grid unit (Character width?)
						if (Shift == FCConvert.ToInt32(ShiftConstants.CtrlMask))
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum]as FCLabel).LeftOriginal + (intCharWid * dblSizeRatio / dblPageRatio));
										if (lngPosition + (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal > Picture1.WidthOriginal)
											lngPosition = Picture1.WidthOriginal - (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal;
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngRightMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.LeftOriginal + (intCharWid * dblSizeRatio / dblPageRatio));
								if (lngPosition + ctlGroupControl.WidthOriginal > Picture1.WidthOriginal)
									lngPosition = Picture1.WidthOriginal - ctlGroupControl.WidthOriginal;
								lngRightMove = lngPosition - ctlGroupControl.LeftOriginal;
								if (lngRightMove > 0)
								{
									for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
										lngPosition = (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal + lngRightMove;
										if (lngPosition + (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal > Picture1.WidthOriginal)
											lngPosition = Picture1.WidthOriginal - (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal;
										this.Picture1.Controls["CustomLabel_" + lngFNum].Left = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// x
								}
								ShowGroupBox();
							}
						}
						else if (Shift == FCConvert.ToInt32(ShiftConstants.ShiftMask))
						{
							// make Wider by a character
							KeyCode = 0;
							if (boolInHighlightMode)
								return;
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngWidth = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal + (intCharWid * dblSizeRatio / dblPageRatio));
									if ((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal + lngWidth <= Picture1.WidthOriginal)
									{
										// only make wider if there is room
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = FCConvert.ToInt32(lngWidth);
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngWidth * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
						}
						break;
					}
				case Keys.Up:
					{
						// move control up one grid unit (Text Line height?)
						if (Shift == FCConvert.ToInt32(ShiftConstants.CtrlMask))
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal - (intLineHite * dblSizeRatio / dblPageRatio));
										if (lngPosition < 0)
											lngPosition = 0;
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngUpMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.TopOriginal - (intLineHite * dblSizeRatio / dblPageRatio));
								if (lngPosition < 0)
									lngPosition = 0;
								lngUpMove = ctlGroupControl.TopOriginal - lngPosition;
								if (lngUpMove > 0)
								{
									for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
										lngPosition = (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal - lngUpMove;
										if (lngPosition < 0)
											lngPosition = 0;
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// x
								}
								ShowGroupBox();
							}
						}
						else if (Shift == FCConvert.ToInt32(ShiftConstants.ShiftMask))
						{
							KeyCode = 0;
							if (boolInHighlightMode)
								return;
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngHeight = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal - (intLineHite * dblSizeRatio / dblPageRatio));
									lngTemp = FCConvert.ToInt32(intLineHite * dblSizeRatio / dblPageRatio);
									// If lngHeight < 240 Then lngHeight = 240
									if (lngHeight >= lngTemp)
									{
										// only make a change if it is at least 240 high
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = lngHeight;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngHeight * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
						}
						break;
					}
				case Keys.Down:
					{
						// move control down one grid unit (Text Line height?)
						if (Shift == FCConvert.ToInt32(ShiftConstants.CtrlMask))
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal + (intLineHite * dblSizeRatio / dblPageRatio));
										if (lngPosition + (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal > Picture1.HeightOriginal)
											lngPosition = Picture1.HeightOriginal - (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal;
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngDownMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.TopOriginal + (intLineHite * dblSizeRatio / dblPageRatio));
								if (lngPosition + ctlGroupControl.HeightOriginal > Picture1.HeightOriginal)
									lngPosition = Picture1.HeightOriginal - ctlGroupControl.HeightOriginal;
								lngDownMove = lngPosition - ctlGroupControl.TopOriginal;
								if (lngDownMove > 0)
								{
									for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
										lngPosition = (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal + lngDownMove;
										if (lngPosition + (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal > Picture1.HeightOriginal)
											lngPosition = Picture1.HeightOriginal - (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal;
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// x
								}
								ShowGroupBox();
							}
						}
						else if (Shift == FCConvert.ToInt32(ShiftConstants.ShiftMask))
						{
							KeyCode = 0;
							if (boolInHighlightMode)
								return;
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngHeight = FCConvert.ToInt32((this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal + (intLineHite * dblSizeRatio / dblPageRatio));
									if (lngHeight + (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal <= Picture1.HeightOriginal)
									{
										(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = lngHeight;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngHeight * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void GridFields_KeyUpEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.ControlKey:
					{
						// UnhighlightAllControls (True)
						break;
					}
			}
			//end switch
		}

		private void GridFields_RowColChange(object sender, System.EventArgs e)
		{
			// With GridFields
			// Select Case .Col
			// Case CNSTGRIDFIELDSCOLTEXT
			// 
			// If Val(.TextMatrix(.Row, CNSTGRIDFIELDSCOLFIELDID)) = CNSTCUSTOMBILLCUSTOMTEXT Or Val(.TextMatrix(.Row, CNSTGRIDFIELDSCOLFIELDID)) = CNSTCUSTOMBILLCUSTOMRICHEDIT Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case Else
			// .Editable = flexEDKbdMouse
			// End Select
			// End With
		}

		private void GridFields_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - i.issue #1683: save and use correct indexes of the cell
            int row = GridFields.GetFlexRowIndex(e.RowIndex);
            int col = GridFields.GetFlexColIndex(e.ColumnIndex);

			// mark as changed
			GridFields.RowData(row, true);
			GridFields.TextMatrix(row, col, GridFields.EditText);
			switch (col)
			{
				case CNSTGRIDFIELDSCOLWIDTH:
					{
						if (Conversion.Val(GridFields.TextMatrix(row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
						{
							GridFields.TextMatrix(row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
							GridFields.EditText = FCConvert.ToString(0);
							// Cancel = True
						}
						break;
					}
				case CNSTGRIDFIELDSCOLHEIGHT:
					{
						if (Conversion.Val(GridFields.TextMatrix(row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
						{
							GridFields.TextMatrix(row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
							GridFields.EditText = FCConvert.ToString(0);
							// Cancel = True
						}
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
			ReDrawField(row);
		}

		private void GridLoadDelete_DblClick(object sender, System.EventArgs e)
		{
			// loads or deletes the format in the current row
			int lngRow;
			int lngID = 0;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (Conversion.Val(GridLoadDelete.Tag) == CNSTGRIDLOADDELETEEXPORT)
				{
					return;
				}
				lngRow = GridLoadDelete.MouseRow;
				if (Conversion.Val(GridLoadDelete.Tag) == CNSTGRIDLOADDELETELOAD)
				{
					// loading
					LoadCustomBill(FCConvert.ToInt32(Conversion.Val(GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID))), "tw" + strThisModule + "0000.vb1");
					GridHighlighted.Rows = 0;
					framLoadDelete.Visible = false;
				}
				else
				{
					// deleting.  Check to see if it is the one we are currently on
					lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID))));
					if (lngID == CurrentLoadedFormat.lngBillID)
					{
						// will have to save as new since it will be deleted now
						// don't clear the screen in case they made a mistake, so they can save it again
						CurrentLoadedFormat.lngBillID = 0;
					}
					modGlobalFunctions.AddCYAEntry_8(strThisModule, "Deleted custom bill format", "Format " + GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID), GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLNAME));
					clsSave.Execute("delete from custombillfields where formatid = " + FCConvert.ToString(lngID), "tw" + strThisModule + "0000.vb1");
					clsSave.Execute("delete from custombills where ID = " + FCConvert.ToString(lngID), "tw" + strThisModule + "0000.vb1");
					MessageBox.Show("Format deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
					framLoadDelete.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GridLoadDelete_DblClick", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridLoadDelete_RowColChange(object sender, System.EventArgs e)
		{
			switch (GridLoadDelete.Col)
			{
				case CNSTGRIDLOADDELETECOLCHECK:
					{
						GridLoadDelete.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						GridLoadDelete.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = HScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.WidthOriginal - framPage.WidthOriginal + (lngOffsetX * 2)) * dblTemp);
			//Picture1.LeftOriginal = -lngTemp + lngOffsetX;
		}

		public void HScroll1_Change()
		{
			//HScroll1_ValueChanged(HScroll1, new System.EventArgs());
		}

		private void HScroll1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Up:
					{
						KeyCode = (Keys)0;
						//VScroll1.Focus();
						break;
					}
				case Keys.Down:
					{
						KeyCode = (Keys)0;
						//VScroll1.Focus();
						break;
					}
			}
			//end switch
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = HScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.WidthOriginal - framPage.WidthOriginal + (lngOffsetX * 2)) * dblTemp);
			//Picture1.LeftOriginal = -lngTemp + (lngOffsetX);
		}

		private int AddANewfield(ref int lngRow)
		{
			int AddANewfield = 0;
			// Accepts the row in gridfields to get its info from
			// Returns the control number so we can store that in gridfields
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngFNum;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				AddANewfield = -1;
				// get the next control number
				lngFNum = 0;
				for (x = 1; x <= (GridFields.Rows - 1); x++)
				{
					if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)) > lngFNum)
					{
						lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))));
			}
				}
				// x
				lngFNum += 1;
				string name = "CustomLabel_" + lngFNum;
				FCLabel y = new FCLabel();
				y.Name = name;
				//y.Click += new System.EventHandler(this.CustomLabel_Click);
				//y.DragDrop += new Wisej.Web.DragEventHandler(this.CustomLabel_DragDrop);
				y.MouseDown += new Wisej.Web.MouseEventHandler(this.CustomLabel_MouseDown);
                y.MouseUp += CustomLabel_0_MouseUp;
                y.AllowDrag = true;
                y.AllowDrop = true;
                y.Movable = true;
                this.Picture1.Controls.Add(y);
				//CustomLabel.Load(lngFNum);
				this.Picture1.Controls["CustomLabel_" + lngFNum].Tag = lngFNum;
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				this.Picture1.Controls["CustomLabel_" + lngFNum].Visible = true;
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).UseMnemonic = false;
				this.Picture1.Controls["CustomLabel_" + lngFNum].BringToFront();
				this.Picture1.Controls["CustomLabel_" + lngFNum].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFNum].ForeColor = Color.Black;
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).Appearance = 0;
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).BorderStyle = 1;
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).BackStyle = 1;
				this.Picture1.Controls["CustomLabel_" + lngFNum].Font = FCUtils.FontChangeName(this.Picture1.Controls["CustomLabel_" + lngFNum].Font, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT));
				this.Picture1.Controls["CustomLabel_" + lngFNum].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + lngFNum].Font, FCConvert.ToInt32((Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE)) * dblSizeRatio / dblPageRatio)));
				(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).Alignment = CNSTCUSTOMBILLALIGNLEFT;
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToString(lngFNum));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(0));
				GridFields.RowData(lngRow, true);
				ReDrawField(lngRow);
				AddANewfield = lngFNum;
				return AddANewfield;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddANewField", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddANewfield;
		}

		private void mnuAddField_Click(object sender, System.EventArgs e)
		{
			// add a new row,default it to something and make them choose a type so you don't ever have
			// bad data
			int lngRow;
			int lngFNum;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strTemp = "";
			int lngTemp;
			cmdSave.Enabled = false;
			framPage.Enabled = false;
			UnhighlightAllControls();
			boolInHighlightMode = false;
			lngFNum = 0;
			for (x = 1; x <= (GridFields.Rows - 1); x++)
			{
				if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)) > lngFNum)
				{
					lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))));
				}
			}
			// x
			lngFNum += 1;
			// create a row
			GridFields.Rows += 1;
			lngRow = GridFields.Rows - 1;
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToString(lngFNum));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLOPENID, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT, "Left");
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLCONTROL, "Label");
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION, "Label");
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT, "Label");
			if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "CENTIMETERS")
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(2.54));
				// 1 inch
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0.4233));
				// 1 line high
			}
			else if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "INCHES")
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(1));
				// 1 inch
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0.1667));
				// 1 line high
			}
			else
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(1));
				// 1 inch
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0.1667));
				// 1 line high
			}
			if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA)) == "DOT MATRIX")
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT, "Courier New");
				// .TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE) = "12"
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEREGULAR));
			}
			else
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT, "Tahoma");
				// .TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE) = "10"
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEREGULAR));
			}
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE, GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA));
			GridFields.Row = lngRow;
			GridFields.Col = CNSTGRIDFIELDSCOLDESCRIPTION;
			// create a control
			// Call frmCustomBill.Controls.Add("VB.Label", "CustomLabel" & lngFnum, Picture1)
			string name = "CustomLabel_" + lngFNum;
			FCLabel y = new FCLabel();
			y.Name = name;
			//y.Click += new System.EventHandler(this.CustomLabel_Click);
			//y.DragDrop += new Wisej.Web.DragEventHandler(this.CustomLabel_DragDrop);
			y.MouseDown += new Wisej.Web.MouseEventHandler(this.CustomLabel_MouseDown);
            y.MouseUp += CustomLabel_0_MouseUp;
            y.AllowDrag = true;
            y.AllowDrop = true;
            y.Movable = true;
			this.Picture1.Controls.Add(y);
			//CustomLabel.Load(lngFNum);
			// With frmCustomBill.Controls("CustomLabel" & lngFnum)
			this.Picture1.Controls["CustomLabel_" + lngFNum].Tag = lngFNum;
            (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            (this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            this.Picture1.Controls["CustomLabel_" + lngFNum].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
            this.Picture1.Controls["CustomLabel_" + lngFNum].Visible = true;
			(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).UseMnemonic = false;
			this.Picture1.Controls["CustomLabel_" + lngFNum].BringToFront();
			this.Picture1.Controls["CustomLabel_" + lngFNum].BackColor = Color.White;
			this.Picture1.Controls["CustomLabel_" + lngFNum].ForeColor = Color.Black;
			(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).Appearance = 0;
			(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).BorderStyle = 1;
			(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).BackStyle = 0;
			this.Picture1.Controls["CustomLabel_" + lngFNum].Font = FCUtils.FontChangeName(this.Picture1.Controls["CustomLabel_" + lngFNum].Font, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT));
			this.Picture1.Controls["CustomLabel_" + lngFNum].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + lngFNum].Font, FCConvert.ToSingle(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE)) * dblSizeRatio / dblPageRatio));
			(this.Picture1.Controls["CustomLabel_" + lngFNum] as FCLabel).Alignment = CNSTCUSTOMBILLALIGNLEFT;
			GridFields.TopRow = GridFields.Rows - 1;
			GridFields.Row = GridFields.Rows - 1;
			// have them choose the type
			cmdOKControlInfo.Visible = false;
			cmdOKControlInfoNew.Visible = true;
            //FC:FINAL:AM:#1757 - bring to front the visible control
            cmdOKControlInfoNew.BringToFront();
            GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWOPEN, CNSTGRIDCONTROLINFOCOLDATA, FCConvert.ToString(0));
			// do this the same as if you click on gridfields to edit the information
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE));
			if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEBOLD)
			{
				strTemp = "Bold";
			}
			else if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEBOLDITALIC)
			{
				strTemp = "Bold Italic";
			}
			else if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEITALIC)
			{
				strTemp = "Italic";
			}
			else
			{
				// regular
				strTemp = "Regular";
			}
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID));
			lngTemp = GridControlTypes.FindRow(FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID)))), -1, CNSTGRIDCONTROLTYPESCOLCODE);
			if (lngTemp >= 0)
			{
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME));
			}
			else
			{
				// shouldn't be able to happen
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, "");
			}
			framControlInfo.Visible = true;
			ResizeGridControlInfo();
			// If GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID) = CNSTCUSTOMBILLCUSTOMIMAGE Then
			// If fso.FileExists(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) Then
			// ReshowimgControlImage (GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT))
			// Else
			// Set imgControlImage.Picture = Nothing
			// End If
			// End If
			GridHighlighted.Rows = 1;
			GridHighlighted.TextMatrix(0, 0, FCConvert.ToString(lngFNum));
			GridHighlighted.TextMatrix(0, 1, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT));
			GridHighlighted.TextMatrix(0, 2, FCConvert.ToString(0));
			framControlInfo.BringToFront();
		}

		public void mnuAddField_Click()
		{
			mnuAddField_Click(mnuAddField, new System.EventArgs());
		}

		private void mnuAlignBottoms_Click(object sender, System.EventArgs e)
		{
			int lngB;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			double dblRatio = 0;
			int lngPosition = 0;
			lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0));
			lngB = this.Picture1.Controls["CustomLabel_" + lngFNum].Top + this.Picture1.Controls["CustomLabel_" + lngFNum].Height;
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
				this.Picture1.Controls["CustomLabel_" + lngFNum].Top = lngB - this.Picture1.Controls["CustomLabel_" + lngFNum].Height;
				lngPosition = this.Picture1.Controls["CustomLabel_" + lngFNum].Top;
				dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
				dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
				for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// x
		}

		private void mnuAlignLefts_Click(object sender, System.EventArgs e)
		{
			// align lefts with the left of the last in the list
			int lngL;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp;
			double dblRatio;
			lngL = this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0))].Left;
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			dblTemp = FCConvert.ToDouble(Strings.Format(lngL * dblRatio, "0.0000"));
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
				this.Picture1.Controls["CustomLabel_" + lngFNum].Left = lngL;
				for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// x
		}

		private void mnuAlignRights_Click(object sender, System.EventArgs e)
		{
			// align rights with the right of the last in the list
			int lngR;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			double dblRatio = 0;
			int lngPosition = 0;
			lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0));
			lngR = this.Picture1.Controls["CustomLabel_" + lngFNum].Left + this.Picture1.Controls["CustomLabel_" + lngFNum].Width;
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
				this.Picture1.Controls["CustomLabel_" + lngFNum].Left = lngR - this.Picture1.Controls["CustomLabel_" + lngFNum].Width;
				lngPosition = this.Picture1.Controls["CustomLabel_" + lngFNum].Left;
				dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
				dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
				for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// x
		}

		private void mnuAlignTops_Click(object sender, System.EventArgs e)
		{
			int lngT;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp;
			double dblRatio;
			int lngPosition;
			lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0));
			lngT = this.Picture1.Controls["CustomLabel_" + lngFNum].Top;
			lngPosition = this.Picture1.Controls["CustomLabel_" + lngFNum].Top;
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0));
				this.Picture1.Controls["CustomLabel_" + lngFNum].Top = lngT;
				for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// x
		}

		private void mnuBringToFront_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))].BringToFront();
			}
			// x
		}

		private void mnuCopyControls_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			int lngRow = 0;
			if (GridHighlighted.Rows > 0)
			{
				GridCopy.Rows = 0;
				// loop through each highlighted control
				for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
				{
					lngRow = 0;
					for (Y = 1; Y <= (GridFields.Rows - 1); Y++)
					{
						if (GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM) == GridHighlighted.TextMatrix(x, 0))
						{
							// matched
							lngRow = Y;
							break;
						}
					}
					// Y
					if (lngRow > 0)
					{
						GridCopy.Rows += 1;
						for (Y = 0; Y <= (GridFields.Cols - 1); Y++)
						{
							// copy each column
							GridCopy.TextMatrix(GridCopy.Rows - 1, Y, GridFields.TextMatrix(lngRow, Y));
						}
						// Y
					}
				}
				// x
				if (GridCopy.Rows > 0)
				{
					mnuPasteControls.Enabled = true;
				}
			}
		}

		private void mnuDeleteField_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngFldNum;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// delete the row and the control
			if (GridFields.Row < 1)
				return;
			lngRow = GridFields.Row;
			// get rid of the control first
			lngFldNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM))));
			// frmCustomBill.Controls.Remove ("CustomLabel" & lngFldNum)
			this.Picture1.Controls.Remove(this.Picture1.Controls["CustomLabel_" + lngFldNum]);
			//CustomLabel.Unload(lngFldNum);
			// now get rid of the row
			if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID)) > 0)
			{
				// if its < 0 then it was never saved, so don't have to worry about it
				GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID))));
			}
			GridFields.RemoveItem(lngRow);
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				if (FCConvert.ToDouble(GridHighlighted.TextMatrix(x, 0)) == lngFldNum)
				{
					GridHighlighted.RemoveItem(x);
					break;
				}
			}
			// x
		}

		public void mnuDeleteField_Click()
		{
			mnuDeleteField_Click(mnuDeleteField, new System.EventArgs());
		}

		private void mnuDeleteFormat_Click(object sender, System.EventArgs e)
		{
			LoadGridLoadDelete(true);
			GridLoadDelete.Tag = (System.Object)(CNSTGRIDLOADDELETEDELETE);
			framLoadDelete.Visible = true;
			framLoadDelete.BringToFront();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGridBillSize()
		{
			GridBillSize.Cols = 2;
			GridBillSize.Rows = 12;
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDESCRIPTION, "Name");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDESCRIPTION, "Page Height");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDESCRIPTION, "Page Width");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Bottom Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Left Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Right Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Top Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDESCRIPTION, "Style");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDESCRIPTION, "Description");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDESCRIPTION, "Units");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDESCRIPTION, "Background");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDESCRIPTION, "Default Font Size");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, "10");
		}

		private void ResizeGridBillSize()
		{
			int GridWidth = 0;
			GridWidth = GridBillSize.WidthOriginal;
			GridBillSize.ColWidth(CNSTGRIDBILLSIZECOLDESCRIPTION, FCConvert.ToInt32(0.5 * GridWidth));
		}

		private void SetupGridLoadDelete()
		{
			GridLoadDelete.Cols = 4;
			GridLoadDelete.Rows = 1;
			GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLAUTOID, true);
			GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK, true);
			GridLoadDelete.ColDataType(CNSTGRIDLOADDELETECOLCHECK, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoadDelete.TextMatrix(0, CNSTGRIDLOADDELETECOLDESCRIPTION, "Description");
			GridLoadDelete.TextMatrix(0, CNSTGRIDLOADDELETECOLNAME, "Format Name");
			GridLoadDelete.TextMatrix(0, CNSTGRIDLOADDELETECOLCHECK, "Export");
			GridLoadDelete.ColAlignment(CNSTGRIDLOADDELETECOLCHECK, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridLoadDelete()
		{
			int GridWidth = 0;
			GridWidth = GridLoadDelete.WidthOriginal;
			if (!GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK))
			{
				GridLoadDelete.ColWidth(CNSTGRIDLOADDELETECOLNAME, FCConvert.ToInt32(0.06 * GridWidth));
			}
			GridLoadDelete.ColWidth(CNSTGRIDLOADDELETECOLNAME, FCConvert.ToInt32(0.15 * GridWidth));
		}

		private void LoadGridLoadDelete(bool boolForDeleting = false, bool boolForExporting = false)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			bool boolShowUnDeletables = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// load with the current formats
				// if deleting, don't bother to show the ones that are permanent and can't be deleted
				if (boolForDeleting)
				{
					boolShowUnDeletables = false;
				}
				else
				{
					boolShowUnDeletables = true;
				}
				if (boolForExporting)
				{
					GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK, false);
					cmdLoadDeleteCancel.Text = "OK";
				}
				else
				{
					GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK, true);
					cmdLoadDeleteCancel.Text = "Cancel";
				}
				ResizeGridLoadDelete();
				GridLoadDelete.Rows = 1;
				if (!boolShowUnDeletables)
				{
					clsLoad.OpenRecordset("select * from custombills where isdeletable = 1 order by formatname", "tw" + strThisModule + "0000.vb1");
				}
				else
				{
					clsLoad.OpenRecordset("select * from custombills order by formatname", "tw" + strThisModule + "0000.vb1");
				}
				while (!clsLoad.EndOfFile())
				{
					GridLoadDelete.Rows += 1;
					lngRow = GridLoadDelete.Rows - 1;
					GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLDESCRIPTION, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
					GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLNAME, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("FormatName"))));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridLoadDelete", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridControlInfo()
		{
			string strTemp = "";
			GridControlInfo.Cols = 3;
			GridControlInfo.FixedCols = 1;
			GridControlInfo.Rows = 8;
			GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Type");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Text");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Alignment");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Font Name");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Font Size");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Font Style");
			GridControlInfo.ColHidden(CNSTGRIDCONTROLINFOCOLTOOLTIP, true);
			GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWCODE, true);
			GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWOPEN, true);
		}

		private void ResizeGridControlInfo()
		{
			int GridWidth = 0;
			string[] strParms = null;
			// holds different parameters
			string[] strAry = null;
			// holds one parameters information
			string[] strFparms = null;
			// holds the parameters saved in the fields table
			string[] strCList = null;
			// if a parameter has a combolist, this will hold the choices so we can have a default choice
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// loop counter
			int lngRow = 0;
			string strTemp = "";
			string[] strParmsToolTips = null;
            GridControlInfo.Rows = CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS;
			// this will be all rows but the extra parameters
			GridWidth = GridControlInfo.WidthOriginal;
            GridControlInfo.Height = originalHeight;
            GridControlInfo.ColWidth(CNSTGRIDCONTROLINFOCOLDESCRIPTION, FCConvert.ToInt32(0.6 * GridWidth));
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
                GridControlInfo.Height = 90;
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "File");
				imgControlImage.Visible = true;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLSIGNATURE)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				//FC:FINAL:DDU:#i1682 - managed by design part
				//GridControlInfo.HeightOriginal = GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) * 2 + 80;
				imgControlImage.Visible = false;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE || Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE || Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, true);
				//FC:FINAL:DDU:#i1682 - managed by design part
				//GridControlInfo.HeightOriginal = GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				imgControlImage.Visible = false;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Report Type");
				//FC:FINAL:DDU:#i1682 - managed by design part
				//GridControlInfo.HeightOriginal = GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) * 2 + 80;
				imgControlImage.Visible = false;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				// .Height = (.Rows - 1) * .RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80
				//FC:FINAL:DDU:#i1682 - managed by design part
				//GridControlInfo.HeightOriginal = (GridControlInfo.Rows - 2) * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Document");
				imgControlImage.Visible = false;
			}
			else
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				// check for extra parameters
				lngRow = GridControlTypes.FindRow(FCConvert.ToInt32(Math.Round(Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)))), 1, CNSTGRIDCONTROLTYPESCOLCODE);
				if (lngRow > 0)
				{
					if (fecherFoundation.Strings.Trim(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS)) != string.Empty)
					{
						strParms = Strings.Split(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS), ";", -1, CompareConstants.vbTextCompare);
						strTemp = GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLEXTRAPARAMETERS);
						if (strTemp != string.Empty)
						{
							strFparms = Strings.Split(strTemp, ";", -1, CompareConstants.vbTextCompare);
						}
						for (x = 0; x <= (Information.UBound(strParms, 1)); x++)
						{
							GridControlInfo.Rows += 1;
							strAry = Strings.Split(strParms[x], ",", -1, CompareConstants.vbTextCompare);
							GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDESCRIPTION, strAry[0]);
							if (strTemp != string.Empty)
							{
								if (Information.UBound(strFparms, 1) >= x)
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strFparms[x]);
								}
							}
							if (Information.UBound(strAry, 1) > 0)
							{
								// extra information
								GridControlInfo.RowData(GridControlInfo.Rows - 1, strAry[1]);
								// store the dropdown list
								strCList = Strings.Split(strAry[1], "|", -1, CompareConstants.vbTextCompare);
								if (strTemp == string.Empty)
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strCList[0]);
									// default to first choice
								}
								else if (Information.UBound(strFparms, 1) < x)
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strCList[0]);
									// default to first choice
								}
								else if (strFparms[x] == "")
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strCList[0]);
									// default to first choice
								}
							}
						}
						// x
						if (fecherFoundation.Strings.Trim(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP)) != string.Empty)
						{
							strParmsToolTips = Strings.Split(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP), ";", -1, CompareConstants.vbTextCompare);
							for (x = 0; x <= (Information.UBound(strParmsToolTips, 1)); x++)
							{
								GridControlInfo.TextMatrix(x + CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS, CNSTGRIDCONTROLINFOCOLTOOLTIP, strParmsToolTips[x]);
							}
							// x
						}
					}
				}
				//FC:FINAL:DDU:#i1682 - managed by design part
				//if (GridControlInfo.Rows >= CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS)
				//{
				//	GridControlInfo.HeightOriginal = (CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS - 2) * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				//}
				//else
				//{
				//	GridControlInfo.HeightOriginal = (GridControlInfo.Rows - 2) * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				//}
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Text");
				if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
				{
					GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Description");
				}
				imgControlImage.Visible = false;
				if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
				{
					GridControlInfo.RowHidden(GridControlInfo.Rows - 2, true);
				}
			}
		}

		private void SetupGridFields()
		{
			GridFields.Cols = 17;
			GridFields.FixedCols = CNSTGRIDFIELDSCOLFIELDNUM + 1;
			GridFields.Rows = 3;
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLFIELDNUM, "Field");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLDESCRIPTION, "Name");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLHEIGHT, "Height");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLLEFT, "Left");
			// .TextMatrix(0, CNSTGRIDFIELDSCOLTEXT) = "Text"
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLTOP, "Top");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLWIDTH, "Width");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLALIGNMENT, "Align");
			// .TextMatrix(0, CNSTGRIDFIELDSCOLFONT) = "Font"
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLCONTROL, "Field Info");
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLTOP, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLLEFT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLWIDTH, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLHEIGHT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLAUTOID, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFIELDID, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFONTSIZE, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFONT, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLTEXT, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLALIGNMENT, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFONTSTYLE, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLEXTRAPARAMETERS, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLMISC, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLOPENID, true);
			GridFields.ColComboList(CNSTGRIDFIELDSCOLCONTROL, "...");
			// .ColComboList(CNSTGRIDFIELDSCOLALIGNMENT) = "Left|Right|Center"
			// .ColComboList(CNSTGRIDFIELDSCOLFONT) = "Courier New|Tahoma"
			// .ColComboList(CNSTGRIDFIELDSCOLFONT) = "..."
			GridFields.CellButtonPicture = null;
			GridFields.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDFIELDSCOLFIELDNUM, GridFields.Rows - 1, CNSTGRIDFIELDSCOLFIELDNUM, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//FC:FINAL:DDU:#i1682 - don't extend the last col because the width becomes too small
			//GridFields.ExtendLastCol = true;
			// now do same thing for gridcopy except for any visual or user interactive stuff
			GridCopy.Cols = 17;
			GridCopy.Rows = 0;
		}

		private void ResizeGridFields()
		{
			// resizes the columns for the grid
			int GridWidth = 0;
			GridWidth = GridFields.WidthOriginal;
			GridFields.ColWidth(CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToInt32(0.07 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLTOP, FCConvert.ToInt32(0.1 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLLEFT, FCConvert.ToInt32(0.1 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToInt32(0.11 * GridWidth));
			// .ColWidth(CNSTGRIDFIELDSCOLALIGNMENT) = 0.09 * GridWidth
			// .ColWidth(CNSTGRIDFIELDSCOLFONT) = 0.12 * GridWidth
			GridFields.ColWidth(CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToInt32(0.11 * GridWidth));
            // .ColWidth(CNSTGRIDFIELDSCOLTEXT) = 0.25 * GridWidth

            //FC:FINAL:AKV:#3773 - Increase width of grid and FIELD INFO column
            GridFields.ColWidth(CNSTGRIDFIELDSCOLCONTROL, FCConvert.ToInt32(0.22 * GridWidth));

        }

		private void SetupGridControlTypes()
		{
			GridControlTypes.Rows = 1;
			GridControlTypes.Cols = 12;
			GridControlTypes.TextMatrix(0, CNSTGRIDCONTROLTYPESCOLCATEGORY, "Category");
			GridControlTypes.TextMatrix(0, CNSTGRIDCONTROLTYPESCOLNAME, "Type");
			GridControlTypes.TextMatrix(0, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "Description");
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLCODE, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLTOOLTIP, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLOPENID, true);
			GridControlTypes.Editable = FCGrid.EditableSettings.flexEDNone;
			LoadGridControlTypes();
		}

		private void ResizeGridControlTypes()
		{
			int GridWidth = 0;
			GridWidth = GridControlTypes.WidthOriginal;
			// .ColWidth(CNSTGRIDCONTROLTYPESCOLCATEGORY) = 0.2 * GridWidth
			GridControlTypes.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			GridControlTypes.AutoSize(CNSTGRIDCONTROLTYPESCOLCATEGORY);
			GridControlTypes.ColWidth(CNSTGRIDCONTROLTYPESCOLNAME, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void LoadGridControlTypes()
		{
			// loads the custom bill codes and stores them in the grid
			// Dim clsLoad As New clsdrwrapper
			string strSQL = "";
			string strLastCategory;
			int lngCurRow;
			string strTemp = "";
			bool boolUseDynamicDocuments;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// boolUseDynamicDocuments = False
				// If clsLoad.UpdateDatabaseTable("DynamicReportCodes", "tw" & strThisModule & "0000.vb1") Then
				// Call clsLoad.OpenRecordset("select * from dynamicreportcodes where reporttype = " & CNSTDYNAMICREPORTTYPECUSTOMBILL, "tw" & strThisModule & "0000.vb1")
				// If Not clsLoad.EndOfFile Then
				// boolUseDynamicDocuments = True
				// End If
				// End If
				boolUseDynamicDocuments = true;
				// strSQL = "Select * from custombillcodes order by categoryno,orderno"
				// Call clsLoad.OpenRecordset(strSQL, "tw" & strThisModule & "0000.vb1")
				strLastCategory = "";
				GridControlTypes.Rows = 1;
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "Misc");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Label");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "User defined text field");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.1667));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 0);
				GridControlTypes.IsSubtotal(lngCurRow, true);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Image");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "User defined Image");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Rectangle");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "An unfilled rectangle");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Rectangle (Filled)");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "An filled rectangle.  Text will be in white.");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Horizontal Line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A horizontal line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Vertical Line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A vertical line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(0));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "LEFT");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Sub Report");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A form within a form");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.1667));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(6));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, FCConvert.ToString(CNSTCUSTOMBILLALIGNLEFT));
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Bar Code");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A bar code");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.5));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(2));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Center");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "Barcode Caption,Show Caption|Don't Show");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Prompt Text");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A text field that the user is prompted to supply a value for");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.1667));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "User Text,Don't Save|Save;Default Text");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				// If boolUseDynamicDocuments Then
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Dynamic Document");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A user defined document");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(2));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				// End If
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMDATE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Date");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "Current Date");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.1667));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLSIGNATURE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Signature");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "Signature Image");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.5));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(2));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				clsCustomControlParameters objParams = new clsCustomControlParameters();
				clsCustomPrintForm objList = new clsCustomPrintForm();
				int intLastCat;
				string strLastCat;
				strLastCat = "";
				intLastCat = 0;
				objList.LoadControlTypes();
				objList.MoveFirst();
				objList.MovePrevious();
				while (objList.MoveNext() > 0)
				{
					objParams = objList.GetCurrentControl();
					if (!(objParams == null))
					{
						if (objParams.CategoryNumber != intLastCat || objParams.Category != strLastCat)
						{
							objParams.OutlineLevel = 0;
							objParams.IsSubTotal = true;
							intLastCat = objParams.CategoryNumber;
							strLastCat = objParams.Category;
						}
						else
						{
							objParams.OutlineLevel = 1;
							objParams.IsSubTotal = false;
						}
						AddControlType(ref objParams);
					}
				}
				GridControlTypes.AutoSize(CNSTGRIDCONTROLTYPESCOLCATEGORY);
				// Do While Not clsLoad.EndOfFile
				// 
				// 
				// 
				// GridControlTypes.Rows = GridControlTypes.Rows + 1
				// lngCurRow = GridControlTypes.Rows - 1
				// If clsLoad.Fields("category") <> strLastCategory Then
				// strLastCategory = Trim(clsLoad.Fields("category"))
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY) = strLastCategory
				// objParams.Category = strLastCategory
				// objParams.OutlineLevel = 0
				// objParams.IsSubTotal = True
				// 
				// GridControlTypes.RowOutlineLevel(lngCurRow) = 0
				// GridControlTypes.IsSubTotal(lngCurRow) = True
				// Else
				// objParams.Category = ""
				// objParams.OutlineLevel = 1
				// objParams.IsSubTotal = False
				// GridControlTypes.RowOutlineLevel(lngCurRow) = 1
				// GridControlTypes.IsSubTotal(lngCurRow) = False
				// End If
				// objParams.Code = Val(clsLoad.Fields("fieldid"))
				// objParams.ControlName = Trim(clsLoad.Fields("description"))
				// objParams.HelpString = Trim(clsLoad.Fields("helpdescription"))
				// objParams.DefaultHeight = Val(clsLoad.Fields("defaultheight"))
				// objParams.DefaultWidth = Val(clsLoad.Fields("defaultwidth"))
				// objParams.ExtraParameters = clsLoad.Fields("extraparameters")
				// objParams.ToolTipString = clsLoad.Fields("tooltiptext")
				// objParams.ParametersToolTip = clsLoad.Fields("parameterstooltip")
				// 
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE) = Val(clsLoad.Fields("fieldid"))
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME) = Trim(clsLoad.Fields("Description"))
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION) = Trim(clsLoad.Fields("helpdescription"))
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT) = Val(clsLoad.Fields("defaultheight"))
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH) = Val(clsLoad.Fields("defaultwidth"))
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS) = clsLoad.Fields("extraparameters")
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLTOOLTIP) = clsLoad.Fields("tooltiptext")
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP) = clsLoad.Fields("ParametersToolTip")
				// Select Case Val(clsLoad.Fields("defaultalignment"))
				// Case CNSTCUSTOMBILLALIGNRIGHT
				// strTemp = "Right"
				// Case CNSTCUSTOMBILLALIGNCENTER
				// strTemp = "Center"
				// Case Else
				// strTemp = "Left"
				// End Select
				// objParams.DefaultAlign = strTemp
				// Call AddControlType(objParams)
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN) = strTemp
				// clsLoad.MoveNext
				// Loop
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridControlTypes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddControlType(ref clsCustomControlParameters objParams)
		{
			int lngCurRow;
			GridControlTypes.Rows += 1;
			lngCurRow = GridControlTypes.Rows - 1;
			if (objParams.OutlineLevel == 0)
			{
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, objParams.Category);
			}
			else
			{
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
			}
			GridControlTypes.RowOutlineLevel(lngCurRow, objParams.OutlineLevel);
			GridControlTypes.IsSubtotal(lngCurRow, objParams.IsSubTotal);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, objParams.Code);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, objParams.ControlName);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, objParams.HelpString);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, objParams.DefaultHeight);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, objParams.DefaultWidth);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, objParams.ExtraParameters);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLTOOLTIP, objParams.ToolTipString);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP, objParams.ParametersToolTip);
			GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLOPENID, objParams.FieldID);
			switch (FCConvert.ToInt32(objParams.DefaultAlign))
			{
				case CNSTCUSTOMBILLALIGNRIGHT:
					{
						GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Right");
						break;
					}
				case CNSTCUSTOMBILLALIGNCENTER:
					{
						GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Center");
						break;
					}
				default:
					{
						GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
						break;
					}
			}
			//end switch
		}

		private void MakeNewFormat()
		{
			// Clears out the current one
			// to get a new one without clearing, you would do a Save As instead
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridHighlighted.Rows = 0;
				// clear out the highlighted fields since there aren't any
				for (x = (GridFields.Rows - 1); x >= 1; x--)
				{
					// step backward since the row numbers will change when you delete rows
					if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)) > 0 && Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDID)) != 0)
					{
						// Me.Controls.Remove ("CustomLabel" & Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)))
						this.Picture1.Controls.Remove(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))]);
						//CustomLabel.Unload(FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)));
					}
					GridFields.RemoveItem(x);
				}
				// x
				GridDeleted.Rows = 0;
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA, "");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, "New Type");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, "11");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, "8.5");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA, "Laser");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Inches");
				CurrentLoadedFormat.boolIsDeletable = true;
				CurrentLoadedFormat.boolIsLaser = true;
				CurrentLoadedFormat.dblBottomMargin = 0;
				CurrentLoadedFormat.dblLeftMargin = 0;
				CurrentLoadedFormat.dblPageHeight = 11;
				CurrentLoadedFormat.dblPageWidth = 8.5;
				CurrentLoadedFormat.dblRightMargin = 0;
				CurrentLoadedFormat.dblTopMargin = 0;
				CurrentLoadedFormat.intUnits = CNSTCUSTOMBILLUNITSINCHES;
				CurrentLoadedFormat.lngBillID = 0;
				CurrentLoadedFormat.strDescription = "";
				CurrentLoadedFormat.strFormatName = "New Type";
                dblTwipsPerUnit = 1440;
				InitializeValues();
				ResizePage();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeNewFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExportFormat_Click(object sender, System.EventArgs e)
		{
			// get a list of formats to export
			LoadGridLoadDelete(false, true);
			GridLoadDelete.Tag = (System.Object)(CNSTGRIDLOADDELETEEXPORT);
			framLoadDelete.Visible = true;
			framLoadDelete.BringToFront();
		}

		private void mnuFillCharFullLine_Click()
		{
		}

		private void mnuFullCharFullLine_Click(object sender, System.EventArgs e)
		{
			mnuFullCharFullLine.Checked = true;
			mnuMoveHalfCharHalfLine.Checked = false;
			mnuMoveQuarterCharQuarterLine.Checked = false;
			intCharWid = 144;
			intLineHite = 240;
			modRegistry.SaveRegistryKey("CustomBillHorizontalIncrements", FCConvert.ToString(intCharWid));
			modRegistry.SaveRegistryKey("CustomBillVerticalIncrements", FCConvert.ToString(intLineHite));
		}

		private void mnuImportFormat_Click(object sender, System.EventArgs e)
		{
			ImportFormat();
		}

		private void ImportFormat()
		{
			// loads custom formats from a database
			FCFileSystem fso = new FCFileSystem();
			string strTemp = "";
			// Dim strDBPath As String
			// Dim strDBFile As String
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsSave = new clsDRWrapper();
			// this modules database
			int lngSrcID;
			// formats ID in the mdb file
			int lngDestID;
			// formats ID in this modules database
			// Dim clsFields As New clsDRWrapper  'the fields table in the mdb file
			string strFile;
			string strJSON = "";
			GenericJSONTableSerializer tJSONSer = new GenericJSONTableSerializer();
			TableCollection tTabColl = new TableCollection();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// get the file to load
				strFile = "";
				MDIParent.InstancePtr.CommonDialog1_Open.DefaultExt = "json";
				// .FileName = "*.mdb"
				// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNFileMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1_Open.Filter = "JSON|*.json";
				//FC:FINAL:MSH - i.issue #1741: restore canceling executing on closing file dialog without selected file
				MDIParent.InstancePtr.CommonDialog1_Open.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1_Open.DialogTitle = "File To Import";
				MDIParent.InstancePtr.CommonDialog1_Open.InitDir = Environment.CurrentDirectory;
				MDIParent.InstancePtr.CommonDialog1_Open.ShowOpen();
				strFile = MDIParent.InstancePtr.CommonDialog1_Open.FileName;
				if (fecherFoundation.Strings.Trim(strFile) == string.Empty)
					return;
				if (!FCFileSystem.FileExists(strFile))
				{
					MessageBox.Show("File doesn't exist", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				int lngParentID;
				StreamReader ts;
				// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
				int Y;
                // Dim strTemp As String
                //ts = FCFileSystem.OpenTextFile(strFile, IOMode.ForReading, false, Tristate.TristateUseDefault);
                ts = FCFileSystem.OpenText(strFile);
                if (!(ts == null))
				{
					strJSON = ts.ReadToEnd();
					//FC:FINAL:DSE:#1983 Close the file after using it. Otherwise, if the same file is imported again, it will throw an exception
					ts.Close();
					tTabColl = tJSONSer.GetCollectionFromJSON(strJSON);
					Dictionary<object, object> idColl = new Dictionary<object, object>();
					if (!(tTabColl == null))
					{
						TableItem tTabItem = new TableItem();
						tTabItem = tTabColl.Item("CustomBills");
						if (!(tTabItem == null))
						{
							RecordItem tRec = new RecordItem();
							// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
							int x;
							clsSave.Execute("update custombills set billformat = 0", modGlobalVariables.Statics.strCEDatabase);
							clsSave.OpenRecordset("select * from CustomBills where id = -1", modGlobalVariables.Statics.strCEDatabase);
							// vbPorter upgrade warning: lngID As int	OnWrite(string)
							int lngID = 0;
							// vbPorter upgrade warning: lngOldID As int	OnWrite(string)
							int lngOldID = 0;
							for (x = 0; x <= (tTabItem.RecordCount() - 1); x++)
							{
								tRec = tTabItem.GetRecord(x);
								if (!(tRec == null))
								{
									clsSave.AddNew();
									for (Y = 0; Y <= (tRec.FieldsCount() - 1); Y++)
									{
										strTemp = tRec.FieldName(Y);
										if (fecherFoundation.Strings.LCase(strTemp) != "id")
										{
											clsSave.Set_Fields(strTemp, tRec.GetItem(strTemp));
										}
										else
										{
											lngOldID = FCConvert.ToInt32(tRec.GetItem(strTemp));
										}
									}
									// Y
									// clsSave.Fields("BillFormat") = Val(TRec.getItem("ID"))
									// clsSave.Fields("FormatName") = TRec.getItem("FormatName")
									// clsSave.Fields("Units") = TRec.getItem("Units")
									// clsSave.Fields("PageHeight") = TRec.getItem("PageHeight")
									// clsSave.Fields("PageWidth") = TRec.getItem("PageWidth")
									// clsSave.Fields("TopMargin") = TRec.getItem("TopMargin")
									// clsSave.Fields("BottomMargin") = TRec.getItem("BottomMargin")
									// clsSave.Fields("LeftMargin") = TRec.getItem("LeftMargin")
									// clsSave.Fields("RightMargin") = TRec.getItem("RightMargin")
									// clsSave.Fields("boolWide") = TRec.getItem("boolWide")
									// clsSave.Fields("IsLaser") = TRec.getItem("IsLaser")
									// clsSave.Fields("IsDeletable") = TRec.getItem("IsDeletable")
									// clsSave.Fields("Description") = TRec.getItem("Description")
									// clsSave.Fields("BackgroundImage") = TRec.getItem("BackgroundImage")
									// clsSave.Fields("DefaultFontSize") = TRec.getItem("DefaultFontSize")
									clsSave.Update();
									idColl.Add(lngOldID, clsSave.Get_Fields_Int32("ID"));
								}
							}
							// x
							tTabItem = tTabColl.Item("custombillfields");
							if (!(tTabItem == null))
							{
								clsSave.OpenRecordset("select * from custombillfields where id = -1", modGlobalVariables.Statics.strCEDatabase);
								clsLoad.OpenRecordset("select * from custombills where billformat > 0", modGlobalVariables.Statics.strCEDatabase);
								for (x = 0; x <= (tTabItem.RecordCount() - 1); x++)
								{
									tRec = tTabItem.GetRecord(x);
									if (!(tRec == null))
									{
										clsSave.AddNew();
										for (Y = 0; Y <= (tRec.FieldsCount() - 1); Y++)
										{
											strTemp = tRec.FieldName(Y);
											if (fecherFoundation.Strings.LCase(strTemp) != "id")
											{
												clsSave.Set_Fields(strTemp, tRec.GetItem(strTemp));
											}
										}
										// Y
										lngID = FCConvert.ToInt32(tRec.GetItem("FormatID"));
										if (idColl.ContainsKey(lngID))
										{
											clsSave.Set_Fields("formatid", idColl[lngID]);
											clsSave.Update();
										}
										// If clsLoad.FindFirstRecord("BillFormat", TRec.getItem("FormatID")) Then
										// clsSave.Fields("FormatID") = clsLoad.Fields("ID")
										// Else
										// clsSave.Fields("FormatID") = 0
										// End If
										// clsSave.Update
									}
								}
								// x
							}
						}
						else
						{
							MessageBox.Show("The file did not contain the correct data", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}
					}
					else
					{
						MessageBox.Show("The file contained no valid data", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return;
					}
				}
				else
				{
					MessageBox.Show("Unable to open the file", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				MessageBox.Show("Import Complete", "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (fecherFoundation.Information.Err(ex).Number == 32755/*vbPorterConverter.cdlCancel*/)
				{
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ImportFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuLoadFormat_Click(object sender, System.EventArgs e)
		{
			LoadGridLoadDelete(false);
			GridLoadDelete.Tag = (System.Object)(CNSTGRIDLOADDELETELOAD);
			framLoadDelete.Visible = true;
			framLoadDelete.BringToFront();
		}

		private void mnuMoveHalfCharHalfLine_Click(object sender, System.EventArgs e)
		{
			mnuFullCharFullLine.Checked = false;
			mnuMoveHalfCharHalfLine.Checked = true;
			mnuMoveQuarterCharQuarterLine.Checked = false;
			intCharWid = 144.0F / 2;
			intLineHite = 240.0F / 2;
			modRegistry.SaveRegistryKey("CustomBillHorizontalIncrements", FCConvert.ToString(intCharWid));
			modRegistry.SaveRegistryKey("CustomBillVerticalIncrements", FCConvert.ToString(intLineHite));
		}

		private void mnuMoveQuarterCharQuarterLine_Click(object sender, System.EventArgs e)
		{
			mnuFullCharFullLine.Checked = false;
			mnuMoveHalfCharHalfLine.Checked = false;
			mnuMoveQuarterCharQuarterLine.Checked = true;
			intCharWid = 144.0F / 4;
			intLineHite = 240.0F / 4;
			modRegistry.SaveRegistryKey("CustomBillHorizontalIncrements", FCConvert.ToString(intCharWid));
			modRegistry.SaveRegistryKey("CustomBillVerticalIncrements", FCConvert.ToString(intLineHite));
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("This will clear the current format from the screen and start with a new blank format." + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			else
			{
				MakeNewFormat();
			}
		}

		private void mnuPasteControls_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			// vbPorter upgrade warning: Y As int	OnWriteFCConvert.ToInt32(
			int Y;
			int lngFRow = 0;
			int lngCtrlNum = 0;
			// unhighlight because the pasted ones will be the selected ones
			UnhighlightAllControls();
			// loop through each control
			for (x = 0; x <= (GridCopy.Rows - 1); x++)
			{
				// loop through each column and copy
				GridFields.Rows += 1;
				lngFRow = GridFields.Rows - 1;
				for (Y = 0; Y <= (GridCopy.Cols - 1); Y++)
				{
					GridFields.TextMatrix(lngFRow, Y, GridCopy.TextMatrix(x, Y));
				}
				// Y
				// now add the new field
				lngCtrlNum = AddANewfield(ref lngFRow);
				if (lngCtrlNum > 0)
				{
					GridHighlighted.Rows += 1;
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, FCConvert.ToString(lngCtrlNum));
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, GridFields.TextMatrix(lngFRow, CNSTGRIDFIELDSCOLFIELDID));
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 2, GridFields.TextMatrix(lngFRow, CNSTGRIDFIELDSCOLOPENID));
					HighlightControl(lngCtrlNum);
				}
			}
			// x
			// highlight and select all of the ones we just pasted
			HighlightAllInList();
			GridFields.Row = GridFields.Rows - 1;
			GridFields.TopRow = GridFields.Row;
			GridFields.Focus();
		}

		private void mnuPrintSample_Click(object sender, System.EventArgs e)
		{
			clsCustomBill testClass = new clsCustomBill();
			if (CurrentLoadedFormat.lngBillID < 1)
			{
				testClass.FormatID = frmChooseCustomBillType.InstancePtr.Init(strThisModule, "TW" + strThisModule + "0000.vb1");
			}
			else
			{
				testClass.FormatID = CurrentLoadedFormat.lngBillID;
			}
			if (testClass.FormatID > 0)
			{
				testClass.PrinterName = "";
				testClass.DBFile = "TW" + strThisModule + "0000.vb1";
				testClass.Module = strThisModule;
				testClass.SQL = "";
				testClass.UsePrinterFonts = true;
				rptCustomBillTest.InstancePtr.Init(testClass);
			}
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// UnhighlightAllControls (True)
			// boolInHighlightMode = False
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (!CurrentLoadedFormat.boolIsDeletable)
			{
				SaveCustomBillAs();
				return;
			}
			if (CurrentLoadedFormat.lngBillID == 0)
			{
				SaveCustomBillAs();
				return;
			}
			// Isn't new and is editable
			SaveCustomBill();
		}

		private bool SaveCustomBillAs()
		{
			bool SaveCustomBillAs = false;
			// save, but create a new format
			object strInput;
			string strTemp;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveCustomBillAs = false;
				if (CurrentLoadedFormat.lngBillID > 0)
				{
					if (MessageBox.Show("This will save the current format as a new format." + "\r\n" + "Continue?", "Save as New?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						return SaveCustomBillAs;
					}
				}
				CurrentLoadedFormat.lngBillID = 0;
				CurrentLoadedFormat.boolIsDeletable = true;
				strInput = "";
				strTemp = fecherFoundation.Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA));
				if (frmInput.InstancePtr.Init(ref strInput, "Format", "Enter a name for the new format", 2880, false, modGlobalConstants.InputDTypes.idtString, strTemp))
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(strInput)) == string.Empty)
					{
						MessageBox.Show("Cannot save with a blank format name", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveCustomBillAs;
					}
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, strInput);
					for (x = 1; x <= (GridFields.Rows - 1); x++)
					{
						// doing a save as so even the unchanged ones need to be saved.  Mark as updated
						GridFields.RowData(x, true);
					}
					// x
					if (SaveCustomBill())
					{
						SaveCustomBillAs = true;
					}
				}
				return SaveCustomBillAs;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveCustomBillAs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCustomBillAs;
		}

		private void mnuSaveAs_Click(object sender, System.EventArgs e)
		{
			SaveCustomBillAs();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (!CurrentLoadedFormat.boolIsDeletable)
			{
				if (SaveCustomBillAs())
				{
					Close();
				}
				return;
			}
			if (CurrentLoadedFormat.lngBillID == 0)
			{
				if (SaveCustomBillAs())
				{
					Close();
				}
				return;
			}
			// Isn't new and is editable
			if (SaveCustomBill())
			{
				Close();
			}
		}

		private void mnuSendToBack_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (GridHighlighted.Rows - 1); x++)
			{
				//Support.ZOrder(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(x, 0))], 1);
			}
			// x
			// make sure the background image is actually in background
			//Support.ZOrder(BillImage, 1);
		}

		private void mnuZoomIn_Click(object sender, System.EventArgs e)
		{
			if (dblPageRatio <= 0.6)
				return;
			dblPageRatio -= 0.2;
			lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
			lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
			ResizePage();
		}

		private void mnuZoomOut_Click(object sender, System.EventArgs e)
		{
			if (dblPageRatio >= 3)
				return;
			dblPageRatio += 0.2;
			lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
			lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
			ResizePage();
		}
		//
		private void Picture1_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp;
			string strTemp = "";
			int lngRow;
			double dblRatio;
			// vbPorter upgrade warning: intloop As int	OnWriteFCConvert.ToInt32(
			int intloop;
			boolDragging = false;
			// Source.Left = Source.Left + (X - lngClickXOffset)
			// Source.Top = Source.Top + (Y - lngClickYOffset)
			//FC:TODO
			//if (e.Y-lngClickYOffset<0) {
			//	e.Source.TopOriginal = 0;
			//} else {
			//	if ((e.Y-lngClickYOffset)+e.Source.HeightOriginal>Picture1.HeightOriginal) {
			//		e.Source.TopOriginal = Picture1.HeightOriginal-e.Source.HeightOriginal;
			//	} else {
			//		e.Source.TopOriginal = FCConvert.ToInt32(e.Y-lngClickYOffset);
			//	}
			//}
			//if (e.X-lngClickXOffset<0) {
			//	e.Source.LeftOriginal = 0;
			//} else {
			//	if ((e.X - lngClickXOffset)+e.Source.WidthOriginal>Picture1.WidthOriginal) {
			//		e.Source.LeftOriginal = Picture1.WidthOriginal-e.Source.WidthOriginal;
			//	} else {
			//		e.Source.LeftOriginal = FCConvert.ToInt32(e.X - lngClickXOffset);
			//	}
			//}
			//lngClickXOffset = 0;
			//lngClickYOffset = 0;
			//// strTemp = Source.Name
			//// strTemp = Mid(strTemp, 12) 'chop off the CustomLabel part before the number
			//// lngRow = Val(strTemp)
			//lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(e.Source.Tag)));
			//for(intloop=1; intloop<=(GridFields.Rows-1); intloop++) {
			//	if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM))==lngRow) {
			//		lngRow = intloop;
			//		break;
			//	}
			//} // intloop
			//dblRatio = (dblPageRatio/(dblTwipsPerUnit*dblSizeRatio));
			//dblTemp = FCConvert.ToDouble(Strings.Format(e.Source.LeftOriginal*dblRatio, "0.0000"));
			//GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
			//dblTemp = FCConvert.ToDouble(Strings.Format(e.Source.TopOriginal*dblRatio, "0.0000"));
			//GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
			//GridFields.RowData(lngRow, true); // mark as changed
			//GridFields.Row = lngRow;
			//// Source.Left = X
			//// Source.Top = Y
		}

		private void Picture1_DragOver(object sender, Wisej.Web.DragEventArgs e)
		{
			// If Not boolDragging Then
			// boolDragging = True
			// lngClickXOffset = X
			// lngClickYOffset = Y
			// End If
		}

		private void VScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = VScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.HeightOriginal - framPage.HeightOriginal + (lngOffsetY * 2)) * dblTemp);
			//Picture1.TopOriginal = -lngTemp + (lngOffsetY);
		}

		public void VScroll1_Change()
		{
			//VScroll1_ValueChanged(VScroll1, new System.EventArgs());
		}

		private void VScroll1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						KeyCode = (Keys)0;
						//HScroll1.Focus();
						break;
					}
				case Keys.Right:
					{
						KeyCode = (Keys)0;
						//HScroll1.Focus();
						break;
					}
			}
			//end switch
		}

		private void VScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = VScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.HeightOriginal - framPage.HeightOriginal + (lngOffsetY * 2)) * dblTemp);
			//Picture1.TopOriginal = -lngTemp + (lngOffsetY);
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(int, double)
		private void LoadCustomBill(int lngID, string strModule)
		{
			// opens the record with lngID as the ID
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngFldCount;
			int lngFID = 0;
			int lngFormatNum;
			int lngTemp;
            FCFileSystem fso = new FCFileSystem();
			int x;
			string strTemp = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			if (lngID <= 0)
			{
				MakeNewFormat();
				return;
			}
			// first clear out what is already there
			for (x = GridFields.Rows - 1; x >= 1; x--)
			{
				// step backward since the row numbers will change when you delete rows
				if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)) > 0 && Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDID)) != 0)
				{
					// Me.Controls.Remove ("CustomLabel" & Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)))
					this.Picture1.Controls.Remove(this.Picture1.Controls["CustomLabel_" + FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))]);
					//CustomLabel.Unload(FCConvert.ToInt32(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)));
				}
				GridFields.RemoveItem(x);
			}
			// x
			GridDeleted.Rows = 0;
			// take care of page size stuff first
			clsLoad.OpenRecordset("SELECT * from custombills where ID = " + FCConvert.ToString(lngID), strModule);
			if (!clsLoad.EndOfFile())
			{
				CurrentLoadedFormat.boolIsDeletable = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("isdeletable"));
				CurrentLoadedFormat.lngBillID = lngID;
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("formatname"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("BottomMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("LeftMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("RightMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("TopMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PageHeight"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(clsLoad.Get_Fields_String("backgroundimage")));
				if (Conversion.Val(clsLoad.Get_Fields("Units")) == CNSTCUSTOMBILLUNITSCENTIMETERS)
				{
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Centimeters");
                    dblTwipsPerUnit = 567;
				}
				else if (Conversion.Val(clsLoad.Get_Fields("Units")) == CNSTCUSTOMBILLUNITSINCHES)
				{
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Inches");
                    dblTwipsPerUnit = 1440;
				}

				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA, "Laser");
				if (Conversion.Val(clsLoad.Get_Fields_Double("DefaultFontSize")) > 0)
				{
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("DefaultFontSize"))));
				}
				else
				{
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, "10");
				}
				lngPageHeight = FCConvert.ToInt32((Conversion.Val(clsLoad.Get_Fields_Double("PageHeight")) - Conversion.Val(clsLoad.Get_Fields_Double("TopMargin")) - Conversion.Val(clsLoad.Get_Fields_Double("BottomMargin"))) * dblTwipsPerUnit);
				lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
				lngPageWidth = FCConvert.ToInt32((Conversion.Val(clsLoad.Get_Fields_Double("PageWidth")) - Conversion.Val(clsLoad.Get_Fields_Double("LeftMargin")) - Conversion.Val(clsLoad.Get_Fields_Double("RightMargin"))) * dblTwipsPerUnit);
				lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
				strTemp = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("backgroundimage")));
				if (strTemp != string.Empty)
				{
					if (FCFileSystem.FileExists(strTemp))
					{
						BillImage.Image = FCUtils.LoadPicture(strTemp);
						BillImage.WidthOriginal = Picture1.WidthOriginal;
						BillImage.HeightOriginal = Picture1.HeightOriginal;
						BillImage.Visible = true;
						GridBillSize.ComboList = fecherFoundation.Strings.Trim(strTemp) + "|None|Choose New";
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, fecherFoundation.Strings.Trim(strTemp));
					}
					else
					{
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, "");
						BillImage.Image = null;
					}
				}
				else
				{
					BillImage.Image = null;
				}
			}
			else
			{
				CurrentLoadedFormat.lngBillID = 0;
				MakeNewFormat();
				return;
			}
			// now load the fields
			clsLoad.OpenRecordset("select * from CustomBillFields where formatid = " + FCConvert.ToString(lngID) + " order by fieldnumber", strModule);
			GridFields.Rows = 1;
			lngFldCount = 0;
			while (!clsLoad.EndOfFile())
			{
				GridFields.Rows += 1;
				lngFldCount += 1;
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				if (Conversion.Val(clsLoad.Get_Fields_Int16("Alignment")) == CNSTCUSTOMBILLALIGNLEFT)
				{
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLALIGNMENT, "Left");
				}
				else if (Conversion.Val(clsLoad.Get_Fields_Int16("Alignment")) == CNSTCUSTOMBILLALIGNRIGHT)
				{
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLALIGNMENT, "Right");
				}
				else if (Conversion.Val(clsLoad.Get_Fields_Int16("Alignment")) == CNSTCUSTOMBILLALIGNCENTER)
				{
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLALIGNMENT, "Center");
				}
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFIELDID, FCConvert.ToString(clsLoad.Get_Fields("FieldID")));
				lngFID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("fieldid"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToString(lngFldCount));
				if (lngFldCount != Conversion.Val(clsLoad.Get_Fields_Int32("fieldnumber")))
				{
					// we changed the field number so mark this as a changed row
					GridFields.RowData(GridFields.Rows - 1, true);
				}
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Height"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Left"))));
				if (Conversion.Val(clsLoad.Get_Fields("fieldid")) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
				{
					if (Conversion.Val(clsLoad.Get_Fields_String("usertext")) > 0)
					{
						clsTemp.OpenRecordset("select * from DynamicReports where ID = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("usertext"))), "tw" + strThisModule + "0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT, FCConvert.ToString(clsTemp.Get_Fields_String("text")));
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("UserText"))));
						}
					}
					GridFields.RowData(GridFields.Rows - 1, 0);
				}
				else if (Conversion.Val(clsLoad.Get_Fields("fieldid")) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
				{
					if (Conversion.Val(clsLoad.Get_Fields_String("usertext")) > 0)
					{
						clsTemp.OpenRecordset("select * from custombills where ID = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("usertext"))), "tw" + strThisModule + "0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT, FCConvert.ToString(clsTemp.Get_Fields_String("FormatName")));
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("UserText"))));
						}
					}
					GridFields.RowData(GridFields.Rows - 1, 0);
				}
				else
				{
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT, FCConvert.ToString(clsLoad.Get_Fields_String("UserText")));
				}
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("top"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("width"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFONT, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Font"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFONTSIZE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fontsize"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLEXTRAPARAMETERS, FCConvert.ToString(clsLoad.Get_Fields_String("extraparameters")));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLOPENID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("openid"))));
				// lngTemp = GridControlTypes.FindRow(Val(clsLoad.Fields("fieldid")), , CNSTGRIDCONTROLTYPESCOLCODE)
				// If lngTemp >= 0 Then
				// .TextMatrix(.Rows - 1, CNSTGRIDFIELDSCOLCONTROL) = GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME)
				// End If
				for (lngTemp = 1; lngTemp <= GridControlTypes.Rows - 1; lngTemp++)
				{
					if (Conversion.Val(GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLCODE)) == Conversion.Val(clsLoad.Get_Fields("fieldid")))
					{
						if (Conversion.Val(clsLoad.Get_Fields_Int32("openid")) == Conversion.Val(GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLOPENID)))
						{
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLCONTROL, GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME));
							break;
						}
					}
				}
				// lngTemp
				// now create a field on the form to represnt this field
				// Select Case lngFID
				// Case CNSTCUSTOMBILLCUSTOMIMAGE
				// Call frmCustomBill.Controls.Add("VB.Image", "CustomLabel" & lngFldCount, Picture1)
				// With frmCustomBill.Controls("CustomLabel" & lngFldCount)
				// .Left = Val(clsLoad.Fields("Left")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// .Height = Val(clsLoad.Fields("height")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// .Width = Val(clsLoad.Fields("width")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// .Top = Val(clsLoad.Fields("Top")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// If Trim(clsLoad.Fields("description")) <> vbNullString Then
				// .Text = Trim(clsLoad.Fields("description"))
				// Else
				// .Text = .Name
				// End If
				// .Visible = True
				// .ZOrder
				// .BackColor = vbWhite
				// .Appearance = 0
				// .BorderStyle = 1
				// If Trim(clsLoad.Fields("Font")) <> vbNullString Then
				// .Font = Trim(clsLoad.Fields("Font"))
				// .Font.Size = Val(clsLoad.Fields("fontsize")) * dblSizeRatio / dblPageRatio
				// Else
				// If UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA)) = "DOT MATRIX" Then
				// .Font = "COURIER NEW"
				// .Font.Size = 12 * dblSizeRatio / dblPageRatio   '12 point is 10 CPI
				// Else
				// .Font = "Tahoma"
				// .Font.Size = 10 * dblSizeRatio / dblPageRatio
				// End If
				// 
				// End If
				// .Alignment = 2  'Center
				// 
				// End With
				// Case Else
				// Call frmCustomBill.Controls.Add("VB.Label", "CustomLabel" & lngFldCount, Picture1)
				string name = "CustomLabel_" + lngFldCount;
				FCLabel y = new FCLabel();
				y.Name = name;
				//y.Click += new System.EventHandler(this.CustomLabel_Click);
				//y.DragDrop += new Wisej.Web.DragEventHandler(this.CustomLabel_DragDrop);
				y.MouseDown += new Wisej.Web.MouseEventHandler(this.CustomLabel_MouseDown);
                y.MouseUp += CustomLabel_0_MouseUp;
                y.AllowDrag = true;
                y.AllowDrop = true;
                y.Movable = true;
                this.Picture1.Controls.Add(y);
				//CustomLabel.Load(lngFldCount);
				// With frmCustomBill.Controls("CustomLabel" & lngFldCount)
				// .DragMode = 1
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Tag = lngFldCount;
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).LeftOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Double("Left")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).HeightOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Double("height")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).WidthOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("width")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).TopOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Double("Top")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
                //this.Picture1.Controls["CustomLabel_" + lngFldCount].SetStyle(ControlStyles.ContainerControl, true);
                (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).UseMnemonic = false;
                (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BorderStyle = 1;
                this.Picture1.Controls["CustomLabel_" + lngFldCount].BringToFront();
                if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("usertext"))) != string.Empty)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = FCConvert.ToString(clsLoad.Get_Fields_String("usertext"));
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
						{
							this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
						}
						else
						{
							this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = this.Picture1.Controls["CustomLabel_" + lngFldCount].GetName();
						}
					}
                    (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Appearance = 0;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 1;
                }
				else if ((lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT) || (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT))
				{
					if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT);
					}
					else
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
						{
							this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
						}
						else
						{
							this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = this.Picture1.Controls["CustomLabel_" + lngFldCount].GetName();
						}
					}
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Appearance(0);
					this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
					//this.Picture1.Controls["CustomLabel_" + lngFldCount].BackStyle = 0;
				}
				else if ((lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE) || (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED))
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Appearance(0);
					if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
					{
						if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("UserText"))) != string.Empty)
						{
							this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = FCConvert.ToString(clsLoad.Get_Fields_String("Usertext"));
						}
						else
						{
							this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
						}
						(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 1;
						this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.Black;
						this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.White;
					}
					else
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
						this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
						this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
						(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
					}
				}
				else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
				{
					//(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Appearance = 0;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
					this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.Black;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.White;
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
                    this.Picture1.Controls["CustomLabel_" + lngFldCount].Height = 0;
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
				}
				else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
				{
					//(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Appearance = 0;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
					this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.Black;
					this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.White;
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
                    (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Width = 0;
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
				}
				else
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					}
					else
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = this.Picture1.Controls["CustomLabel_" + lngFldCount].GetName();
					}
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Appearance = 0;
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
				}
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Visible = true;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BringToFront();
				if (lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE && lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Font"))) != string.Empty)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeName(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Font"))));
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, FCConvert.ToInt32((Conversion.Val(clsLoad.Get_Fields("fontsize")) * dblSizeRatio / dblPageRatio)));
					}
					else
					{
						// If optLaser(0).Value Then
						// .Font = "COURIER NEW"
						// Else
						// .Font = "Tahoma"
						// End If
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, FCConvert.ToInt32((12 * dblSizeRatio / dblPageRatio)));
					}
					if (Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle")) == CNSTCUSTOMBILLFONTSTYLEBOLD)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
					}
					else if (Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle")) == CNSTCUSTOMBILLFONTSTYLEBOLDITALIC)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
					}
					else if (Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle")) == CNSTCUSTOMBILLFONTSTYLEITALIC)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
					}
					else
					{
						// regular
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
					}
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Alignment = (HorizontalAlignment)Conversion.Val(clsLoad.Get_Fields_Int16("alignment"));
				}
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Refresh();
				// End Select
				clsLoad.MoveNext();
			}
			GridFields.Col = 0;
			GridFields.Row = 0;
			ResizePage();
		}

		private void ReDrawField(int lngRow)
		{
			int lngFldCount;
			int lngFID = 0;
			lngFldCount = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM))));
			// With frmCustomBill.Controls("CustomLabel" & lngFldCount)
			(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
            (this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			lngFID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
			if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
			{
				if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT);
				}
				else
				{
					if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION)) != string.Empty)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
					}
					else
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = this.Picture1.Controls["CustomLabel_" + lngFldCount].GetName();
					}
				}
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
			}
			else if ((lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT) || (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT))
			{
				if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT);
				}
				else
				{
					if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION)) != string.Empty)
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
					}
					else
					{
						this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = this.Picture1.Controls["CustomLabel_" + lngFldCount].GetName();
					}
				}
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Height = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Width = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE)
			{
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
			{
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 1;
				// opaque
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.Black;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.White;
				if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT);
				}
				else
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = "";
				}
			}
			else
			{
				if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION)) != string.Empty)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
				}
				else
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Text = this.Picture1.Controls["CustomLabel_" + lngFldCount].GetName();
				}
				this.Picture1.Controls["CustomLabel_" + lngFldCount].BackColor = Color.White;
				this.Picture1.Controls["CustomLabel_" + lngFldCount].ForeColor = Color.Black;
				(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).BackStyle = 0;
			}
			if (lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE && lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				if (fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT)) != string.Empty)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeName(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, fecherFoundation.Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT)));
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, FCConvert.ToInt32((Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE)) * dblSizeRatio / dblPageRatio)));
				}
				else
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeSize(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, FCConvert.ToInt32((12 * dblSizeRatio / dblPageRatio)));
				}
				if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEBOLD)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
				}
				else if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEBOLDITALIC)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
				}
				else if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)) == CNSTCUSTOMBILLFONTSTYLEITALIC)
				{
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, true);
				}
				else
				{
					// regular
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeBold(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
					this.Picture1.Controls["CustomLabel_" + lngFldCount].Font = FCUtils.FontChangeItalic(this.Picture1.Controls["CustomLabel_" + lngFldCount].Font, false);
				}
				if (fecherFoundation.Strings.UCase(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT)) == "RIGHT")
				{
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Alignment = (HorizontalAlignment)CNSTCUSTOMBILLALIGNRIGHT;
				}
				else if (fecherFoundation.Strings.UCase(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT)) == "CENTER")
				{
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Alignment = (HorizontalAlignment)CNSTCUSTOMBILLALIGNCENTER;
				}
				else
				{
					(this.Picture1.Controls["CustomLabel_" + lngFldCount] as FCLabel).Alignment = (HorizontalAlignment)CNSTCUSTOMBILLALIGNLEFT;
				}
			}
		}

		public void Init(string strMod, string strTitle = "Custom Bill", int lngIDToLoad = 0)
		{
			strThisModule = fecherFoundation.Strings.UCase(strMod);
			CurrentLoadedFormat.lngBillID = lngIDToLoad;
			this.Text = strTitle;
			this.Show(App.MainForm);
		}

		private bool SaveCustomBill()
		{
			bool SaveCustomBill = false;
			// returns true if there are no errors saving
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strSQL = "";
			int lngLastRowChecked = 0;
			// counter used to go through the grid
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveCustomBill = false;
				if (fecherFoundation.Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA)) == string.Empty)
				{
					MessageBox.Show("The format name cannot be blank", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveCustomBill;
				}
				clsSave.OpenRecordset("Select * from custombills where ID = " + FCConvert.ToString(CurrentLoadedFormat.lngBillID), "TW" + strThisModule + "0000.vb1");
				if (CurrentLoadedFormat.lngBillID == 0 || clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				clsSave.Set_Fields("IsDeletable", CurrentLoadedFormat.boolIsDeletable);
				clsSave.Set_Fields("PageHeight", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("PageWidth", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("TopMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("BottomMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("LeftMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("RightMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "CENTIMETERS")
				{
					clsSave.Set_Fields("Units", CNSTCUSTOMBILLUNITSCENTIMETERS);
				}
				else if (fecherFoundation.Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "INCHES")
				{
					clsSave.Set_Fields("Units", CNSTCUSTOMBILLUNITSINCHES);
				}
				else
				{
					clsSave.Set_Fields("Units", CNSTCUSTOMBILLUNITSINCHES);
				}
				clsSave.Set_Fields("IsLaser", true);
				clsSave.Set_Fields("Description", GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA));
				clsSave.Set_Fields("FormatName", GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA));
				clsSave.Set_Fields("BackgroundImage", fecherFoundation.Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)));
				clsSave.Set_Fields("DefaultFontSize", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("BillFormat", CurrentLoadedFormat.lngBillFormat);
				clsSave.Set_Fields("BillType", CurrentLoadedFormat.lngBillType);
				if (!clsSave.Update())
				{
					return SaveCustomBill;
				}
				CurrentLoadedFormat.lngBillID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				// Main record is saved, now update each field
				// take care of deleted rows first
				if (GridDeleted.Rows > 0)
				{
					for (x = 0; x <= (GridDeleted.Rows - 1); x++)
					{
						clsSave.Execute("delete from custombillfields where ID = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(x, 0))), "TW" + strThisModule + "0000.vb1");
					}
					// x
				}
				GridDeleted.Rows = 0;
				// now take care of new and updated ones
				clsSave.OpenRecordset("select * from custombillfields where formatid = " + FCConvert.ToString(CurrentLoadedFormat.lngBillID), "TW" + strThisModule + "0000.vb1");
				lngLastRowChecked = 1;
				x = GridFields.FindRow(true, lngLastRowChecked);
				while (x > 0)
				{
					// make sure there is data and this isn't somehow a blank grid row
					if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDID)) != 0)
					{
						if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLAUTOID)) == 0)
						{
							// ID of 0 is a new one
							clsSave.AddNew();
							// .TextMatrix(x, CNSTGRIDFIELDSCOLAUTOID) = clsSave.Fields("ID")
						}
						else
						{
							// just updating
							if (clsSave.FindFirstRecord("ID", Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLAUTOID))))
							{
								clsSave.Edit();
							}
							else
							{
								clsSave.AddNew();
							}
						}
						clsSave.Set_Fields("openid", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLOPENID))));
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDID))));
						clsSave.Set_Fields("Top", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTOP))));
						clsSave.Set_Fields("Left", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLLEFT))));
						clsSave.Set_Fields("Height", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLHEIGHT))));
						clsSave.Set_Fields("Width", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLWIDTH))));
						clsSave.Set_Fields("Description", fecherFoundation.Strings.Trim(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLDESCRIPTION)));
						clsSave.Set_Fields("BillType", CurrentLoadedFormat.lngBillID);
						if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
						{
							clsSave.Set_Fields("usertext", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLMISC))));
						}
						else if (Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
						{
							clsSave.Set_Fields("usertext", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLMISC))));
						}
						else
						{
							clsSave.Set_Fields("usertext", GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLTEXT));
						}
						clsSave.Set_Fields("Font", GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFONT));
						clsSave.Set_Fields("Fontsize", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFONTSIZE))));
						clsSave.Set_Fields("FontStyle", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFONTSTYLE))));
						clsSave.Set_Fields("ExtraParameters", GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLEXTRAPARAMETERS));
						clsSave.Set_Fields("FormatID", CurrentLoadedFormat.lngBillID);
						clsSave.Set_Fields("FieldNumber", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM))));
						if (fecherFoundation.Strings.UCase(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLALIGNMENT)) == "RIGHT")
						{
							clsSave.Set_Fields("Alignment", CNSTCUSTOMBILLALIGNRIGHT);
						}
						else if (fecherFoundation.Strings.UCase(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLALIGNMENT)) == "CENTER")
						{
							clsSave.Set_Fields("Alignment", CNSTCUSTOMBILLALIGNCENTER);
						}
						else
						{
							clsSave.Set_Fields("Alignment", CNSTCUSTOMBILLALIGNLEFT);
						}
						if (!clsSave.Update())
						{
							return SaveCustomBill;
						}
						GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
					}
					GridFields.RowData(x, false);
					x = GridFields.FindRow(true, lngLastRowChecked);
				}
				SaveCustomBill = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Format Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveCustomBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveCustomBill", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCustomBill;
		}
	}
}
