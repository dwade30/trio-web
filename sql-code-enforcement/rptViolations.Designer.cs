﻿using System;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptViolations.
	/// </summary>
	partial class rptViolations
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptViolations));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblParent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDoingBusinessAs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroupMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroupTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroupDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtGroupPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGroupParent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtIdentifier = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDateIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblParent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDoingBusinessAs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupParent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIdentifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtIdentifier,
				this.txtType,
				this.txtDateIssued,
				this.txtStatus
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label2,
				this.Label3,
				this.Label4,
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label12,
				this.lblParent
			});
			this.PageHeader.Height = 0.84375F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Visible = false;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.DataField = "grpHeader";
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label5,
				this.txtName,
				this.Label6,
				this.txtDoingBusinessAs,
				this.Label7,
				this.txtLocation,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.txtGroupMuni,
				this.txtGroupTime,
				this.txtGroupDate,
				this.txtGroupPage,
				this.Label17,
				this.lblGroupParent
			});
			this.GroupHeader1.Height = 1.583333F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.1770833F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.03125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-weight: bold";
			this.Label1.Text = "Identifier";
			this.Label1.Top = 0.6041667F;
			this.Label1.Width = 0.9583333F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1770833F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.447917F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Type";
			this.Label2.Top = 0.6041667F;
			this.Label2.Width = 2.197917F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1770833F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.833333F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Date Issued";
			this.Label3.Top = 0.6041667F;
			this.Label3.Width = 0.9583333F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1770833F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.84375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Status";
			this.Label4.Top = 0.6041667F;
			this.Label4.Width = 2.145833F;
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 2.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.25F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.21875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.25F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label12.Text = "Violations";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 4.9375F;
			// 
			// lblParent
			// 
			this.lblParent.Height = 0.1875F;
			this.lblParent.HyperLink = null;
			this.lblParent.Left = 1.25F;
			this.lblParent.Name = "lblParent";
			this.lblParent.Style = "text-align: center";
			this.lblParent.Text = null;
			this.lblParent.Top = 0.25F;
			this.lblParent.Width = 4.9375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Name";
			this.Label5.Top = 0.625F;
			this.Label5.Width = 0.75F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1.40625F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 0.625F;
			this.txtName.Width = 4.697917F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold";
			this.Label6.Text = "Doing Business As";
			this.Label6.Top = 0.8125F;
			this.Label6.Width = 1.333333F;
			// 
			// txtDoingBusinessAs
			// 
			this.txtDoingBusinessAs.Height = 0.1875F;
			this.txtDoingBusinessAs.Left = 1.40625F;
			this.txtDoingBusinessAs.Name = "txtDoingBusinessAs";
			this.txtDoingBusinessAs.Text = null;
			this.txtDoingBusinessAs.Top = 0.8125F;
			this.txtDoingBusinessAs.Width = 4.697917F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Location";
			this.Label7.Top = 1F;
			this.Label7.Width = 1.333333F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1.40625F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1F;
			this.txtLocation.Width = 4.697917F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1770833F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.03125F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Identifier";
			this.Label8.Top = 1.354167F;
			this.Label8.Width = 0.9583333F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1770833F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.447917F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-weight: bold";
			this.Label9.Text = "Type";
			this.Label9.Top = 1.354167F;
			this.Label9.Width = 2.197917F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1770833F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.833333F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Date Issued";
			this.Label10.Top = 1.354167F;
			this.Label10.Width = 0.9583333F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1770833F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.84375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-weight: bold";
			this.Label11.Text = "Status";
			this.Label11.Top = 1.354167F;
			this.Label11.Width = 2.145833F;
			// 
			// txtGroupMuni
			// 
			this.txtGroupMuni.Height = 0.1875F;
			this.txtGroupMuni.HyperLink = null;
			this.txtGroupMuni.Left = 0F;
			this.txtGroupMuni.Name = "txtGroupMuni";
			this.txtGroupMuni.Style = "";
			this.txtGroupMuni.Text = null;
			this.txtGroupMuni.Top = 0.03125F;
			this.txtGroupMuni.Width = 2.625F;
			// 
			// txtGroupTime
			// 
			this.txtGroupTime.Height = 0.1875F;
			this.txtGroupTime.HyperLink = null;
			this.txtGroupTime.Left = 0F;
			this.txtGroupTime.Name = "txtGroupTime";
			this.txtGroupTime.Style = "";
			this.txtGroupTime.Text = null;
			this.txtGroupTime.Top = 0.25F;
			this.txtGroupTime.Width = 1.25F;
			// 
			// txtGroupDate
			// 
			this.txtGroupDate.Height = 0.1875F;
			this.txtGroupDate.HyperLink = null;
			this.txtGroupDate.Left = 6.1875F;
			this.txtGroupDate.Name = "txtGroupDate";
			this.txtGroupDate.Style = "text-align: right";
			this.txtGroupDate.Text = null;
			this.txtGroupDate.Top = 0.03125F;
			this.txtGroupDate.Width = 1.25F;
			// 
			// txtGroupPage
			// 
			this.txtGroupPage.Height = 0.1875F;
			this.txtGroupPage.HyperLink = null;
			this.txtGroupPage.Left = 6.1875F;
			this.txtGroupPage.Name = "txtGroupPage";
			this.txtGroupPage.Style = "text-align: right";
			this.txtGroupPage.Text = null;
			this.txtGroupPage.Top = 0.25F;
			this.txtGroupPage.Width = 1.25F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.21875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 1.25F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label17.Text = "Violations";
			this.Label17.Top = 0.03125F;
			this.Label17.Width = 4.9375F;
			// 
			// lblGroupParent
			// 
			this.lblGroupParent.Height = 0.1875F;
			this.lblGroupParent.HyperLink = null;
			this.lblGroupParent.Left = 1.25F;
			this.lblGroupParent.Name = "lblGroupParent";
			this.lblGroupParent.Style = "text-align: center";
			this.lblGroupParent.Text = null;
			this.lblGroupParent.Top = 0.25F;
			this.lblGroupParent.Width = 4.9375F;
			// 
			// txtIdentifier
			// 
			this.txtIdentifier.Height = 0.1875F;
			this.txtIdentifier.Left = 0.03125F;
			this.txtIdentifier.Name = "txtIdentifier";
			this.txtIdentifier.Text = "Field1";
			this.txtIdentifier.Top = 0.01041667F;
			this.txtIdentifier.Width = 1.333333F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 1.447917F;
			this.txtType.Name = "txtType";
			this.txtType.Text = "Field2";
			this.txtType.Top = 0.01041667F;
			this.txtType.Width = 2.322917F;
			// 
			// txtDateIssued
			// 
			this.txtDateIssued.Height = 0.1875F;
			this.txtDateIssued.Left = 3.833333F;
			this.txtDateIssued.Name = "txtDateIssued";
			this.txtDateIssued.Text = "Field3";
			this.txtDateIssued.Top = 0.01041667F;
			this.txtDateIssued.Width = 0.9583333F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1875F;
			this.txtStatus.Left = 4.84375F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Text = "Field4";
			this.txtStatus.Top = 0.01041667F;
			this.txtStatus.Width = 2.583333F;
			// 
			// rptViolations
			//
			//
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblParent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDoingBusinessAs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupParent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIdentifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}

		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIdentifier;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateIssued;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblParent;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDoingBusinessAs;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtGroupMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtGroupTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtGroupDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtGroupPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroupParent;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
