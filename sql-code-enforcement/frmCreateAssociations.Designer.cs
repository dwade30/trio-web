//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmCreateAssociations.
	/// </summary>
	partial class frmCreateAssociations
	{
		public fecherFoundation.FCComboBox cmbMatch;
		public fecherFoundation.FCLabel lblMatch;
		public fecherFoundation.FCButton cmdDeselectAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public fecherFoundation.FCCheckBox chkCreateNew;
		public FCGrid Grid;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuReload;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbMatch = new fecherFoundation.FCComboBox();
            this.lblMatch = new fecherFoundation.FCLabel();
            this.cmdDeselectAll = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.chkCreateNew = new fecherFoundation.FCCheckBox();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuReload = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdReload = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeselectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 517);
            this.BottomPanel.Size = new System.Drawing.Size(953, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdDeselectAll);
            this.ClientArea.Controls.Add(this.cmdSelectAll);
            this.ClientArea.Controls.Add(this.chkCreateNew);
            this.ClientArea.Controls.Add(this.cmbMatch);
            this.ClientArea.Controls.Add(this.lblMatch);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Size = new System.Drawing.Size(953, 457);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdReload);
            this.TopPanel.Size = new System.Drawing.Size(953, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdReload, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(421, 30);
            this.HeaderText.Text = "Create Associations from Real Estate";
            // 
            // cmbMatch
            // 
            this.cmbMatch.AutoSize = false;
            this.cmbMatch.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbMatch.FormattingEnabled = true;
            this.cmbMatch.Items.AddRange(new object[] {
            "Map / Lot",
            "Location"});
            this.cmbMatch.Location = new System.Drawing.Point(123, 30);
            this.cmbMatch.Name = "cmbMatch";
            this.cmbMatch.Size = new System.Drawing.Size(183, 40);
            this.cmbMatch.TabIndex = 7;
            this.cmbMatch.Text = "Map / Lot";
            this.cmbMatch.SelectedIndexChanged += new System.EventHandler(this.optMatch_CheckedChanged);
            // 
            // lblMatch
            // 
            this.lblMatch.AutoSize = true;
            this.lblMatch.Location = new System.Drawing.Point(30, 44);
            this.lblMatch.Name = "lblMatch";
            this.lblMatch.Size = new System.Drawing.Size(71, 15);
            this.lblMatch.TabIndex = 8;
            this.lblMatch.Text = "MATCH BY";
            // 
            // cmdDeselectAll
            // 
            this.cmdDeselectAll.AppearanceKey = "actionButton";
            this.cmdDeselectAll.Location = new System.Drawing.Point(165, 131);
            this.cmdDeselectAll.Name = "cmdDeselectAll";
            this.cmdDeselectAll.Size = new System.Drawing.Size(106, 40);
            this.cmdDeselectAll.TabIndex = 6;
            this.cmdDeselectAll.Text = "Clear All";
            this.cmdDeselectAll.Click += new System.EventHandler(this.cmdDeselectAll_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.Location = new System.Drawing.Point(30, 131);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(115, 40);
            this.cmdSelectAll.TabIndex = 5;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // chkCreateNew
            // 
            this.chkCreateNew.Location = new System.Drawing.Point(30, 90);
            this.chkCreateNew.Name = "chkCreateNew";
            this.chkCreateNew.Size = new System.Drawing.Size(451, 27);
            this.chkCreateNew.TabIndex = 4;
            this.chkCreateNew.Text = "Create new accounts for unmatched Real Estate accounts";
            this.chkCreateNew.CheckedChanged += new System.EventHandler(this.chkCreateNew_CheckedChanged);
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 8;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 191);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(878, 408);
            this.Grid.StandardTab = true;
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 0;
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            this.Grid.Sorted += new System.EventHandler(this.Grid_AfterSort);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuReload,
            this.mnuSepar2,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuReload
            // 
            this.mnuReload.Index = 0;
            this.mnuReload.Name = "mnuReload";
            this.mnuReload.Text = "Reload";
            this.mnuReload.Click += new System.EventHandler(this.mnuReload_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 2;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 3;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdReload
            // 
            this.cmdReload.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdReload.AppearanceKey = "toolbarButton";
            this.cmdReload.Location = new System.Drawing.Point(864, 29);
            this.cmdReload.Name = "cmdReload";
            this.cmdReload.Size = new System.Drawing.Size(61, 24);
            this.cmdReload.TabIndex = 1;
            this.cmdReload.Text = "Reload";
            this.cmdReload.Click += new System.EventHandler(this.mnuReload_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(439, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmCreateAssociations
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(953, 625);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmCreateAssociations";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Create Associations from Real Estate";
            this.Load += new System.EventHandler(this.frmCreateAssociations_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCreateAssociations_KeyDown);
            this.Resize += new System.EventHandler(this.frmCreateAssociations_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeselectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCreateNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdReload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdReload;
		private FCButton cmdSave;
	}
}