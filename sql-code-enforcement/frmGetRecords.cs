//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmGetRecords : BaseForm
	{
		public frmGetRecords()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmGetRecords InstancePtr
		{
			get
			{
				return (frmGetRecords)Sys.GetInstance(typeof(frmGetRecords));
			}
		}

		protected frmGetRecords _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 02/14/2005
		// This is the search screen for all records
		// ********************************************************
		const int CNSTGRIDSEARCHCOLAUTOID = 0;
		const int CNSTGRIDSEARCHPROPERTY = 0;
		const int CNSTGRIDSEARCHCONTRACTOR = 1;
		const int CNSTGRIDSEARCHINSPECTION = 2;
		const int CNSTGRIDSEARCHPERMIT = 3;
		const int CNSTGRIDSEARCHVIOLATION = 4;
		const int CNSTGRIDSEARCHPROPERTYCOLACCT = 1;
		const int CNSTGRIDSEARCHPROPERTYCOLNAME = 2;
		const int CNSTGRIDSEARCHPROPERTYCOLMAPLOT = 3;
		const int CNSTGRIDSEARCHPROPERTYCOLSTNUM = 4;
		const int CNSTGRIDSEARCHPROPERTYCOLSTREET = 5;
		const int CNSTGRIDSEARCHPROPERTYCOLEXTRA = 6;
		const int CNSTGRIDSEARCHCONTRACTORCOLNUMBER = 1;
		const int CNSTGRIDSEARCHCONTRACTORCOLNAME = 2;
		const int CNSTGRIDSEARCHCONTRACTORCOLNAME2 = 3;
		const int CNSTGRIDSEARCHCONTRACTORCOLLICENSE = 4;
		const int CNSTGRIDSEARCHCONTRACTORCOLEXTRA = 5;
		const int CNSTGRIDSEARCHPERMITCOLNUMBER = 1;
		const int CNSTGRIDSEARCHPERMITCOLIDENTIFIER = 2;
		// Private Const CNSTGRIDSEARCHPERMITCOLPLAN = 2
		const int CNSTGRIDSEARCHPERMITCOLSTATUS = 3;
		const int CNSTGRIDSEARCHPERMITCOLAPPDATE = 4;
		const int CNSTGRIDSEARCHPERMITCOLNAME = 5;
		const int CNSTGRIDSEARCHPERMITCOLEXTRA = 6;
		const int CNSTGRIDSEARCHPERMITCOLSTNUM = 6;
		const int CNSTGRIDSEARCHPERMITCOLSTREET = 7;
		const int CNSTGRIDSEARCHINSPECTIONCOLDATE = 1;
		const int CNSTGRIDSEARCHINSPECTIONCOLPERMIT = 2;
		const int CNSTGRIDSEARCHINSPECTIONCOLTYPE = 3;
		const int CNSTGRIDSEARCHINSPECTIONCOLSTATUS = 4;
		const int CNSTGRIDSEARCHINSPECTIONCOLINSPECTOR = 5;
		const int CNSTGRIDSEARCHINSPECTIONCOLEXTRA = 6;
		const int CNSTGRIDSEARCHINSPECTIONCOLSTNUM = 6;
		const int CNSTGRIDSEARCHINSPECTIONCOLSTREET = 7;
		const int CNSTGRIDSEARCHVIOLATIONCOLIDENTIFIER = 1;
		const int CNSTGRIDSEARCHVIOLATIONCOLNAME = 2;
		const int CNSTGRIDSEARCHVIOLATIONCOLTYPE = 3;
		const int CNSTGRIDSEARCHVIOLATIONCOLISSUED = 4;
		const int CNSTGRIDSEARCHVIOLATIONCOLRESOLVED = 5;
		const int CNSTGRIDSEARCHVIOLATIONCOLEXTRA = 6;
		const int CNSTGRIDSEARCHVIOLATIONCOLSTNUM = 6;
		const int CNSTGRIDSEARCHVIOLATIONCOLSTREET = 7;

		public void Init(int lngSearchType = 0)
		{
			chkShowDel.Visible = true;
			lblViolationSearchType.Visible = false;
			cmbViolationSearchType.Visible = false;
			//FC:FINAL:CHN - i.issue #1695: Redesign: at original app this Label + ComboBox was at Frame at back of SearchType Frame.
			this.cmbtsearchtype.Visible = true;
			this.lbltsearchtype.Visible = true;
            cmdClear.Visible = true;
            btnSearch.Visible = true;
            ToolTip1.SetToolTip(txtGetAccountNumber, "");
            //FC:FINAL:AM:#1669 - initialize Tag to avoid wrong comparisons when the Tag is null
            GridSearch.Tag = 999;
			switch (lngSearchType)
			{
				case modCEConstants.CNSTSEARCHCONTRACTOR:
					{
                        //FC:FINAL:MSH - i.issue #1673: remove deleting items from combobox to avoid handling SelectedIndexChanged with wrong index value
						//cmbtrecordtype.Items.Clear();
						//cmbtrecordtype.Items.Add("Contractors");
						this.Text = "Contractor Search";
						chkShowDel.Text = "Include Deleted Contractors";
						txtGetAccountNumber.Text = FCConvert.ToString(Conversion.Val(modCE.GetCERegistryKey("CELastContractorNumber")));
						Label2.Text = "Last Contractor Accessed";
						cmbtrecordtype.Text = "Contractors";
                        //FC:FINAL:SGA:#1964 - set col data type                        
                        GridSearch.ColDataType(CNSTGRIDSEARCHCONTRACTORCOLNUMBER, FCGrid.DataTypeSettings.flexDTLong);
                        break;
					}
				case modCEConstants.CNSTSEARCHPROPERTY:
					{
                        //FC:FINAL:MSH - i.issue #1673: remove deleting items from combobox to avoid handling SelectedIndexChanged with wrong index value
						//cmbtrecordtype.Items.Clear();
						//cmbtrecordtype.Items.Add("Property Records");
						this.Text = "Property Search";
						chkShowDel.Text = "Include Deleted Properties";
						txtGetAccountNumber.Text = FCConvert.ToString(Conversion.Val(modCE.GetCERegistryKey("CELastAccountNumber")));
						Label2.Text = "Last Account Accessed";
						cmbtrecordtype.Text = "Property Records";
						break;
					}
				case modCEConstants.CNSTSEARCHPERMIT:
					{
						ToolTip1.SetToolTip(txtGetAccountNumber, "Enter permit in yyyy-Number or yy-Number format");
                        //FC:FINAL:MSH - i.issue #1673: remove deleting items from combobox to avoid handling SelectedIndexChanged with wrong index value
                        //cmbtrecordtype.Items.Clear();
						//cmbtrecordtype.Items.Add("Permits");
						this.Text = "Permit Search";
						chkShowDel.Text = "Include Deleted Permits";
						txtGetAccountNumber.Text = FCConvert.ToString(modCE.GetCERegistryKey("CELastPermitNumber"));
						Label2.Text = "Last Permit Accessed";
						cmbtrecordtype.Text = "Permits";
						break;
					}
				case modCEConstants.CNSTSEARCHINSPECTION:
					{
                        //FC:FINAL:MSH - i.issue #1673: remove deleting items from combobox to avoid handling SelectedIndexChanged with wrong index value
                        //cmbtrecordtype.Items.Clear();
						//cmbtrecordtype.Items.Add("Inspections");
						this.Text = "Inspection Search";
						chkShowDel.Text = "Include Deleted Inspections";
						Label2.Text = "";
						cmbtrecordtype.Text = "Inspections";
                        //FC:FINAL:SGA:#2795 - set col data type
                        GridSearch.ColDataType(CNSTGRIDSEARCHINSPECTIONCOLDATE, FCGrid.DataTypeSettings.flexDTDate);
                        break;
					}
				case modCEConstants.CNSTSEARCHVIOLATION:
					{
						chkShowDel.Visible = false;
						lblViolationSearchType.Visible = true;
						cmbViolationSearchType.Visible = true;
						//FC:FINAL:CHN - i.issue #1695: Redesign: at original app this Label + ComboBox was at Frame at back of SearchType Frame.
						this.cmbtsearchtype.Visible = false;
						this.lbltsearchtype.Visible = false;
                        //FC:FINAL:MSH - i.issue #1673: remove deleting items from combobox to avoid handling SelectedIndexChanged with wrong index value
                        //cmbtrecordtype.Items.Clear();
						//cmbtrecordtype.Items.Add("Violations");
						this.Text = "Violation Search";
						chkShowDel.Text = "Include Deleted Properties";
						txtGetAccountNumber.Text = FCConvert.ToString(modCE.GetCERegistryKey("CELastViolationIdentifier"));
						Label2.Text = "Last Violation Accessed";
						cmbtrecordtype.Text = "Violations";
						break;
					}
			}
			//end switch
			this.Show(App.MainForm);
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			txtSearch.Text = "";
			txtSearch2.Text = "";
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngTemp = 0;
			string[] strAry = null;
			string strSQL = "";
			if (cmbtrecordtype.Text == "Property Records")
			{
				// property
				strTemp = "You must enter a valid account number";
			}
			else if (cmbtrecordtype.Text == "Contractors")
			{
				// contractor
				strTemp = "You must enter a valid contractor number";
			}
			else if (cmbtrecordtype.Text == "Permits")
			{
				// permit
				strTemp = "You must enter a valid permit number in yy-number format";
			}
			else if (cmbtrecordtype.Text == "Violations")
			{
				strTemp = "You must enter a valid violation identifier";
			}
			if (Conversion.Val(txtGetAccountNumber.Text) == 0)
			{
				// If (Optrecordtype(CNSTGRIDSEARCHPROPERTY).Value) Then
				// If MsgBox("This will create a PENDING account which must be finalized or created in Real Estate to be used in Real Estate" & vbNewLine & "Do you wish to continue?", vbQuestion + vbYesNo, "Create Pending Account?") = vbNo Then
				// Exit Sub
				// End If
				// Else
				if (cmbtrecordtype.Text == "Permits")
				{
					MessageBox.Show(strTemp, "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				if (cmdAddNew.Enabled == false)
				{
					MessageBox.Show("You do not have permissions to create a new record", "No Permission", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (Conversion.Val(txtGetAccountNumber.Text) < 0)
			{
				if ((cmbtrecordtype.Text == "Property Records" && !modGlobalVariables.Statics.CECustom.GetDataFromRE) || cmbtrecordtype.Text != "Property Records")
				{
					MessageBox.Show(strTemp, "Invalid Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (cmbtrecordtype.Text == "Property Records")
			{
				// property
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(txtGetAccountNumber.Text)));
				if (lngTemp != 0)
				{
					clsLoad.OpenRecordset("select * from cemaster where ceaccount = " + FCConvert.ToString(lngTemp), modGlobalVariables.Statics.strCEDatabase);
					if (clsLoad.EndOfFile())
					{
						if (Conversion.Val(txtGetAccountNumber.Text) < 0)
						{
							MessageBox.Show("Bad Account Number", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
							// If MsgBox("Account " & lngTemp & " does not exist" & vbNewLine & "Do you want to create it?", vbQuestion + vbYesNo + vbDefaultButton2, "Create?") = vbNo Then
							// Exit Sub
							// Else
							// Call AddCYAEntry("CE", "Requested to Create " & lngTemp, "From Search Screen")
							// End If
						}
						else
						{
							// If CECustom.GetDataFromRE Then
							// MsgBox "This account doesn't exist", vbExclamation, "Invalid Account"
							// Exit Sub
							// Else
							if (MessageBox.Show("Account " + txtGetAccountNumber.Text + " does not exist" + "\r\n" + "Do you want to create it?", "Create?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
							{
								return;
							}
							else
							{
								modGlobalFunctions.AddCYAEntry_6("CE", "Requested to Create " + FCConvert.ToString(lngTemp), "From Search Screen");
							}
							// End If
						}
					}
					else
					{
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("deleted")))
						{
							// If CECustom.GetDataFromRE And Val(clsLoad.Fields("rsaccount")) > 0 Then
							// MsgBox "This is a deleted account" & vbNewLine & "Cannot open"
							// Exit Sub
							// Else
							// is a pending account or they don't have real estate
							lngTemp = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ceaccount"));
							if (MessageBox.Show("This account is deleted" + "\r\n" + "Do you want to undelete it?", "Undelete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								if (modMain.UndeleteMaster(lngTemp))
								{
									MessageBox.Show("Account " + FCConvert.ToString(lngTemp) + " undeleted", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									return;
								}
							}
							else
							{
								return;
							}
							// End If
						}
						modCE.SaveCERegistryKey("CELastAccountNumber", FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
					}
				}
				frmMaster.InstancePtr.Init(lngTemp);
			}
			else if (cmbtrecordtype.Text == "Contractors")
			{
				// contractor
				if (Conversion.Val(txtGetAccountNumber.Text) > 0)
				{
					clsLoad.OpenRecordset("select ID,contractornumber,deleted from contractors where contractornumber = " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)), modGlobalVariables.Statics.strCEDatabase);
					if (clsLoad.EndOfFile())
					{
						if (MessageBox.Show("This contractor does not exist." + "\r\n" + "Do you want to create it?", "Create?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
						{
							if (modMain.AttemptLock(ref strTemp, modCEConstants.CNSTCONTRACTLOCK))
							{
								clsLoad.AddNew();
								clsLoad.Set_Fields("contractornumber", FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
								clsLoad.Update();
								lngTemp = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID"));
								modMain.ReleaseLock(modCEConstants.CNSTCONTRACTLOCK);
								modGlobalFunctions.AddCYAEntry_6("CE", "Created contractor from search screen", "Requested number " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
							}
							else
							{
								MessageBox.Show("Could not create contractor number " + FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)) + " the lock is held by " + strTemp + "\r\n" + "Please try again in a moment", "Cannot Get Lock", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else
						{
							return;
						}
					}
					else
					{
						lngTemp = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID"));
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("deleted")))
						{
							if (MessageBox.Show("This contractor is deleted" + "\r\n" + "Do you want to undelete it?", "Undelete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								if (modMain.UndeleteContractor(lngTemp))
								{
									MessageBox.Show("Contractor " + txtGetAccountNumber.Text + " undeleted", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									return;
								}
							}
							else
							{
								return;
							}
						}
					}
					modCE.SaveCERegistryKey("CELastContractorNumber", FCConvert.ToString(Conversion.Val(txtGetAccountNumber.Text)));
				}
				else
				{
					lngTemp = 0;
				}
				frmContractors.InstancePtr.Init(lngTemp);
			}
			else if (cmbtrecordtype.Text == "Permits")
			{
				// permit
				int lngPermYear = 0;
				int lngPermNumber = 0;
				int lngAcct = 0;
				if (Strings.InStr(1, txtGetAccountNumber.Text, "-", CompareConstants.vbTextCompare) > 0)
				{
					strAry = Strings.Split(fecherFoundation.Strings.Trim(txtGetAccountNumber.Text), "-", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(strAry, 1) < 1)
					{
						MessageBox.Show(strTemp, "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					lngPermYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					if (lngPermYear < 100)
					{
						if (lngPermYear < 85)
						{
							lngPermYear = 2000 + lngPermYear;
						}
						else
						{
							lngPermYear = 1900 + lngPermYear;
						}
					}
					lngPermNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[1])));
					clsLoad.OpenRecordset("select * from permits where permityear = " + FCConvert.ToString(lngPermYear) + " and permitnumber = " + FCConvert.ToString(lngPermNumber), modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						lngTemp = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID"));
						lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("account"))));
						if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("deleted")))
						{
							strSQL = "select * from cemaster where ceaccount = " + FCConvert.ToString(lngAcct);
							clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
							if (!clsLoad.EndOfFile())
							{
								if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("rsdeleted")))
								{
									MessageBox.Show("Permit " + txtGetAccountNumber.Text + " belongs to a deleted account", "Deleted Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
							}
							else
							{
								MessageBox.Show("Permit " + txtGetAccountNumber.Text + " does not exist", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							if (MessageBox.Show("Permit " + txtGetAccountNumber.Text + " is deleted" + "\r\n" + "Do you wish to undelete it?", "Deleted Permit", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
							{
								if (modMain.UndeletePermit(lngTemp))
								{
									MessageBox.Show("Permit " + txtGetAccountNumber.Text + " undeleted", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									return;
								}
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						MessageBox.Show("Permit " + txtGetAccountNumber.Text + " does not exist", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					modCE.SaveCERegistryKey("CELastPermitNumber", fecherFoundation.Strings.Trim(txtGetAccountNumber.Text));
					frmPermits.InstancePtr.Init(lngTemp, lngAcct);
				}
				else
				{
					MessageBox.Show(strTemp, "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbtrecordtype.Text == "Violations")
			{
				clsLoad.OpenRecordset("select * from violations where violationidentifier = '" + txtGetAccountNumber.Text + "'", modGlobalVariables.Statics.strCEDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (clsLoad.RecordCount() > 0)
					{
						GridSearch.Rows = 1;
						GridSearch.Tag = (System.Object)(CNSTGRIDSEARCHVIOLATION);
						SetupGridSearch();
						FillGrid(ref clsLoad);
					}
				}
				else
				{
					MessageBox.Show("Violation " + txtGetAccountNumber.Text + " could not be found", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			SearchRecords();
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(btnSearch, new System.EventArgs());
		}

		private void frmGetRecords_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGetRecords_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetRecords properties;
			//frmGetRecords.FillStyle	= 0;
			//frmGetRecords.ScaleWidth	= 9300;
			//frmGetRecords.ScaleHeight	= 7875;
			//frmGetRecords.LinkTopic	= "Form2";
			//frmGetRecords.LockControls	= -1  'True;
			//frmGetRecords.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmGetRecords_Resize(object sender, System.EventArgs e)
		{
			ResizeGridSearch();
		}

		private void GridSearch_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPROPERTY)
			{
				if (GridSearch.Col == CNSTGRIDSEARCHPROPERTYCOLSTNUM || GridSearch.Col == CNSTGRIDSEARCHPROPERTYCOLSTREET)
				{
					GridSearch.Col = CNSTGRIDSEARCHPROPERTYCOLSTNUM;
					GridSearch.ColSel = CNSTGRIDSEARCHPROPERTYCOLSTREET;
					if (GridSearch.SortOrder == SortOrder.Ascending)
					{
						GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
					}
					else if (GridSearch.SortOrder == SortOrder.Descending)
					{
						GridSearch.Sort = FCGrid.SortSettings.flexSortStringDescending;
					}
				}
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPERMIT)
			{
				if (GridSearch.Col == CNSTGRIDSEARCHPERMITCOLSTNUM || GridSearch.Col == CNSTGRIDSEARCHPERMITCOLSTREET)
				{
					if (cmbtsearchtype.Text == "Location")
					{
						GridSearch.Col = CNSTGRIDSEARCHPERMITCOLSTNUM;
						GridSearch.ColSel = CNSTGRIDSEARCHPERMITCOLSTREET;
						if (GridSearch.SortOrder == SortOrder.Ascending)
						{
							GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
						}
						else if (GridSearch.SortOrder == SortOrder.Descending)
						{
							GridSearch.Sort = FCGrid.SortSettings.flexSortStringDescending;
						}
					}
				}
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHINSPECTION)
			{
				if (GridSearch.Col == CNSTGRIDSEARCHINSPECTIONCOLSTNUM || GridSearch.Col == CNSTGRIDSEARCHINSPECTIONCOLSTREET)
				{
					if (cmbtsearchtype.Text == "Location")
					{
						GridSearch.Col = CNSTGRIDSEARCHINSPECTIONCOLSTNUM;
						GridSearch.ColSel = CNSTGRIDSEARCHINSPECTIONCOLSTREET;
					}
					if (GridSearch.SortOrder == SortOrder.Ascending)
					{
						GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
					}
					else if (GridSearch.SortOrder == SortOrder.Descending)
					{
						GridSearch.Sort = FCGrid.SortSettings.flexSortStringDescending;
					}
				}
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHVIOLATION)
			{
				if (GridSearch.Col == CNSTGRIDSEARCHVIOLATIONCOLSTNUM || GridSearch.Col == CNSTGRIDSEARCHVIOLATIONCOLSTREET)
				{
					GridSearch.Col = CNSTGRIDSEARCHVIOLATIONCOLSTNUM;
					GridSearch.ColSel = CNSTGRIDSEARCHVIOLATIONCOLSTREET;
					if (GridSearch.SortOrder == SortOrder.Ascending)
					{
						GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
					}
					else if (GridSearch.SortOrder == SortOrder.Descending)
					{
						GridSearch.Sort = FCGrid.SortSettings.flexSortStringDescending;
					}
				}
			}
		}

		private void GridSearch_DblClick(object sender, System.EventArgs e)
		{
			if (GridSearch.MouseRow < 1)
				return;
			if (FCConvert.ToBoolean(GridSearch.RowData(GridSearch.MouseRow)))
			{
				MessageBox.Show("Record is deleted" + "\r\n" + "Cannot load", "Deleted Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPROPERTY)
			{
				frmMaster.InstancePtr.Init(FCConvert.ToInt32(GridSearch.TextMatrix(GridSearch.MouseRow, CNSTGRIDSEARCHPROPERTYCOLACCT)));
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHCONTRACTOR)
			{
				frmContractors.InstancePtr.Init(FCConvert.ToInt32(GridSearch.TextMatrix(GridSearch.MouseRow, CNSTGRIDSEARCHCOLAUTOID)));
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPERMIT)
			{
				frmPermits.InstancePtr.Init(FCConvert.ToInt32(GridSearch.TextMatrix(GridSearch.MouseRow, CNSTGRIDSEARCHCOLAUTOID)));
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHINSPECTION)
			{
				frmInspection.InstancePtr.Init(FCConvert.ToInt32(GridSearch.TextMatrix(GridSearch.MouseRow, CNSTGRIDSEARCHCOLAUTOID)));
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHVIOLATION)
			{
				bool boolRonly = false;
				if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modCEConstants.CNSTVIOLATIONSEDITSECURITY)) != "F")
				{
					boolRonly = true;
				}
				else
				{
					boolRonly = false;
				}
				frmViolation.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(GridSearch.TextMatrix(GridSearch.MouseRow, CNSTGRIDSEARCHCOLAUTOID))), boolRonly);
			}
		}

		private void mnuAddNew_Click(object sender, System.EventArgs e)
		{
			if (cmbtrecordtype.Text == "Property Records")
			{
				frmMaster.InstancePtr.Init(0);
			}
			else if (cmbtrecordtype.Text == "Contractors")
			{
				frmContractors.InstancePtr.Init(0);
			}
			else if (cmbtrecordtype.Text == "Violations")
			{
				frmViolation.InstancePtr.Init(0);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNewSearch_Click(object sender, System.EventArgs e)
		{
			GridSearch.Rows = 1;
			mnuSaveExit.Visible = true;
			cmdNewSearch.Visible = false;
            cmdClear.Visible = true;
            btnSearch.Visible = true;
            framSearch.Visible = false;
            Frame2.Visible = true;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			SearchRecords();
		}

		private void Optrecordtype_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			Label1.Visible = false;
			lblViolationSearchType.Visible = false;
			cmbViolationSearchType.Visible = false;
			//FC:FINAL:CHN - i.issue #1695: Redesign: at original app this Label + ComboBox was at Frame at back of SearchType Frame.
			this.cmbtsearchtype.Visible = true;
			this.lbltsearchtype.Visible = true;
			switch (Index)
			{
				case CNSTGRIDSEARCHPROPERTY:
					{
						// property
						Label1.Visible = true;
						Label1.Text = "Enter an account number or 0 to add a new account";
						cmdGetAccountNumber.Text = "Get Account";
						cmdGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Text = FCConvert.ToString(Conversion.Val(modCE.GetCERegistryKey("CELastAccountNumber")));
						cmdAddNew.Visible = true;
						mnuSepar2.Visible = true;
						if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modCEConstants.CNSTPROPERTYEDITSECURITY)) == "F")
						{
							cmdAddNew.Enabled = true;
						}
						else
						{
							cmdAddNew.Enabled = false;
						}
						this.Text = "Property Search";
						cmdAddNew.Text = "New Property";
						//FC:FINAL:DDU:#i1674 - fixed size of buton
						cmdAddNew.Size = new System.Drawing.Size(110, cmdAddNew.Height);
						break;
					}
				case CNSTGRIDSEARCHCONTRACTOR:
					{
						// contractor
						Label1.Visible = true;
                        Label1.Text = "Enter a contractor number or 0 to add a new contractor";
						cmdGetAccountNumber.Text = "Get Contractor";
						cmdGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Text = FCConvert.ToString(Conversion.Val(modCE.GetCERegistryKey("CELastContractorNumber")));
						this.Text = "Contractor Search";
						cmdAddNew.Visible = true;
						mnuSepar2.Visible = true;
						cmdAddNew.Text = "New Contractor";
						//FC:FINAL:DDU:#i1674 - fixed size of buton
						cmdAddNew.Size = new System.Drawing.Size(110, cmdAddNew.Height);
						if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modCEConstants.CNSTEDITCONTRACTORSSECURITY)) == "F")
						{
							cmdAddNew.Enabled = true;
                        }
						else
						{
							cmdAddNew.Enabled = false;
                        }
                        //FC:FINAL:SGA:#1964 - set col data type
                        GridSearch.ColDataType(CNSTGRIDSEARCHCONTRACTORCOLNUMBER, FCGrid.DataTypeSettings.flexDTLong);
                        break;
					}
				case CNSTGRIDSEARCHPERMIT:
					{
						// permit
						Label1.Visible = false;
						cmdGetAccountNumber.Text = "Get Permit";
						cmdGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Text = FCConvert.ToString(modCE.GetCERegistryKey("CELastPermitNumber"));
						this.Text = "Permit Search";
						cmdAddNew.Visible = false;
						mnuSepar2.Visible = false;
                        break;
					}
				case CNSTGRIDSEARCHINSPECTION:
					{
						// inspection
						Label1.Visible = false;
						cmdGetAccountNumber.Enabled = false;
						txtGetAccountNumber.Enabled = false;
						this.Text = "Inspection Search";
						cmdAddNew.Visible = false;
						mnuSepar2.Visible = false;
                        //FC:FINAL:SGA:#2795 - set col data type
                        GridSearch.ColDataType(CNSTGRIDSEARCHINSPECTIONCOLDATE, FCGrid.DataTypeSettings.flexDTDate);
                        break;
					}
				case CNSTGRIDSEARCHVIOLATION:
					{
						// violations
						Label1.Visible = true;
						Label1.Text = "Enter a violation identifier";
						cmdGetAccountNumber.Text = "Get Violation";
						cmdGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Enabled = true;
						txtGetAccountNumber.Text = FCConvert.ToString(modCE.GetCERegistryKey("CELastViolationIdentifier"));
						this.Text = "Violation Search";
						lblViolationSearchType.Visible = true;
						cmbViolationSearchType.Visible = true;
						//FC:FINAL:CHN - i.issue #1695: Redesign: at original app this Label + ComboBox was at Frame at back of SearchType Frame.
						this.cmbtsearchtype.Visible = false;
						this.lbltsearchtype.Visible = false;
						cmdAddNew.Visible = false;
						mnuSepar2.Visible = false;
						cmdAddNew.Text = "New Violation";
						//FC:FINAL:DDU:#i1674 - fixed size of buton
						cmdAddNew.Size = new System.Drawing.Size(110, cmdAddNew.Height);
						if (FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modCEConstants.CNSTVIOLATIONSEDITSECURITY)) == "F")
						{
							cmdAddNew.Enabled = true;
						}
						else
						{
							cmdAddNew.Enabled = false;
						}
						break;
					}
			}
			//end switch
			if (cmdGetAccountNumber.Enabled)
			{
				cmdGetAccountNumber.Visible = true;
				txtGetAccountNumber.Visible = true;
			}
			else
			{
				cmdGetAccountNumber.Visible = false;
				txtGetAccountNumber.Visible = false;
			}
		}

		private void Optrecordtype_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtrecordtype.SelectedIndex;
			Optrecordtype_CheckedChanged(index, sender, e);
		}

		private void SearchRecords()
		{
			if (cmbtrecordtype.Text == "Property Records")
			{
				SearchProperty();
			}
			else if (cmbtrecordtype.Text == "Contractors")
			{
				SearchContractor();
			}
			else if (cmbtrecordtype.Text == "Permits")
			{
				SearchPermit();
			}
			else if (cmbtrecordtype.Text == "Inspections")
			{
				SearchInspection();
			}
			else if (cmbtrecordtype.Text == "Violations")
			{
				SearchViolation();
			}
		}

		private void SearchProperty()
		{
			// search property records
			string strSQL;
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strAnd;
			string strWhere;
			string strOrderBy = "";
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp = 0;
			string[] strAry = null;
			string strTemp2 = "";
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			bool boolContractors;
			bool boolPermits;
			bool boolPermitContractors;
			bool boolInspections;
			string strMasterJoin;
			string strMasterJoinJoin;
			boolContractors = false;
			boolPermits = false;
			boolPermitContractors = false;
			boolInspections = false;
			strMasterJoin = modCE.GetCEMasterJoin();
			strMasterJoinJoin = modCE.GetCEMasterJoinForJoin();
			strAnd = "";
			// strSQL = "select * from cemaster "
			strSQL = "select " + tSQL.GetTableFields("CEMaster");
			strWhere = " where ";
			// If CECustom.GetDataFromRE Then
			// strWhere = strWhere & strAnd & " rscard = 1 "
			// strAnd = " and "
			// End If
			if (chkShowDel.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				// strWhere = strWhere & strAnd & " not cemaster.deleted = 1 "
				strWhere += strAnd + " not mj.deleted = 1 ";
				strAnd = " and ";
			}
			if (cmbtsearchtype.Text == "Owner Name")
			{
				// name
				// If CECustom.GetDataFromRE Then
				// strSQL = "select * from cemaster left join (select rsaccount,rsname,rssecowner,rslocstreet,rslocnumalph,rslocapt,rsmaplot,rsdeleted from remaster where rscard = 1) as query1 on (val(cemaster.rsaccount & '') = query1.rsaccount) "
				// End If
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " ((cemaster.rsaccount = 0 and name like '*" & strTemp & "*') "
					// strWhere = strWhere & " or  (cemaster.rsaccount > 0 and rsname like '*" & strTemp & "*') "
					// Else
					strWhere += strAnd + " (name like '%" + strTemp + "%' ";
					// End If
					strWhere += ") ";
				}
				else
				{
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " ((cemaster.rsaccount = 0 and name between '" & strTemp & "' and '" & strTemp & "ZZZZZ') "
					// strWhere = strWhere & "or (cemaster.rsaccount > 0 and rsname between '" & strTemp & "' and '" & strTemp & "ZZZZZ') "
					// strWhere = strWhere & ") "
					// Else
					strWhere += strAnd + " name between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					// End If
				}
				strAnd = " and ";
				strOrderBy = " Order by name,ceaccount ";
			}
			else if (cmbtsearchtype.Text == "Location")
			{
				// location
				strTemp2 = fecherFoundation.Strings.Trim(txtSearch.Text);
				int lngTemp = 0;
				lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp2)));
				strTemp = fecherFoundation.Strings.Trim(txtSearch2.Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				// strWhere = strWhere & " (NOT usere and (locationstreet like '*" & strSearch & "*')) "
				// If gboolRE Or gboolBL Then
				// strWhere = strWhere & " or (usere and (rslocstreet like '*" & strSearch & "*'))"
				// End If
				// If CECustom.GetDataFromRE Then
				// strSQL = "select * from cemaster left join (select rsaccount,rsname,rssecowner,rslocstreet,rslocnumalph,rslocapt,rsmaplot,rsdeleted from remaster where rscard = 1) as query1 on (val(cemaster.rsaccount & '') = query1.rsaccount) "
				// End If
				if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
				{
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						// strWhere = strWhere & strAnd & " rslocstreet like '*" & strTemp & "*' "
						// If CECustom.GetDataFromRE Then
						// strWhere = strWhere & strAnd & " ((cemaster.rsaccount  = 0 and streetname LIKE '*" & strTemp & "*' "
						// If lngTemp > 0 Then
						// strWhere = strWhere & " and streetnumber = '" & lngTemp & "' "
						// End If
						// strWhere = strWhere & ") "
						// strWhere = strWhere & " or (cemaster.rsaccount > 0 and rslocstreet like '*" & strTemp & "*' "
						// If lngTemp > 0 Then
						// strWhere = strWhere & strAnd & " rslocnumalph = '" & lngTemp & "' "
						// End If
						// strWhere = strWhere & ") "
						// Else
						strWhere += strAnd + " (streetname LIKE '%" + strTemp + "%' ";
						if (lngTemp > 0)
						{
							strWhere += " and streetnumber = " + FCConvert.ToString(lngTemp) + " ";
						}
						// End If
						strWhere += ") ";
					}
					else
					{
						// If CECustom.GetDataFromRE Then
						// strWhere = strWhere & strAnd & " ((cemaster.rsaccount = 0 and streetname between '" & strTemp & "' and '" & strTemp & "ZZZZZ' "
						// 
						// If lngTemp > 0 Then
						// strWhere = strWhere & " and streetnumber = '" & lngTemp & "' "
						// End If
						// strWhere = strWhere & ") "
						// strWhere = strWhere & " or (cemaster.rsaccount > 0 and rslocstreet between '" & strTemp & "' and '" & strTemp & "ZZZZZ' "
						// If lngTemp > 0 Then
						// strWhere = strWhere & " and rslocnumalph = '" & lngTemp & "' "
						// End If
						// strWhere = strWhere & ") "
						// Else
						strWhere += strAnd + " (streetname between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
						if (lngTemp > 0)
						{
							strWhere += " and streetnumber = " + FCConvert.ToString(lngTemp) + " ";
						}
						// End If
						strWhere += ") ";
					}
				}
				else
				{
					if (lngTemp > 0)
					{
						// If CECustom.GetDataFromRE Then
						// strWhere = strWhere & strAnd & " ((cemaster.rsaccount = 0 and streetnumber = '" & lngTemp & "') "
						// strWhere = strWhere & " and (cemaster.rsaccount > 0 and rslocnumalph = '" & lngTemp & "') "
						// Else
						strWhere += strAnd + " (streetnumber = " + FCConvert.ToString(lngTemp) + " ";
						// End If
						strWhere += ") ";
						strAnd = " and ";
					}
				}
				strAnd = " and ";
				strOrderBy = " order by streetname,streetnumber,ceaccount ";
			}
			else if (cmbtsearchtype.Text == "Map / Lot")
			{
				// maplot
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " ((cemaster.rsaccount = 0 and maplot like '*" & strTemp & "*') "
					// strWhere = strWhere & " and (cemaster.rsaccount > 0 and rsmaplot like '*" & strTemp & "*') "
					// strWhere = strWhere & ") "
					// Else
					strWhere += strAnd + " maplot like '%" + strTemp + "%' ";
					// End If
				}
				else
				{
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " ((cemaster.rsaccount = 0 and maplot between '" & strTemp & "' and '" & strTemp & "ZZZZZ') "
					// strWhere = strWhere & " and  (cemaster.rsaccount > 0 and rsmaplot between '" & strTemp & "' and '" & strTemp & "ZZZZZ') "
					// strWhere = strWhere & ") "
					// Else
					strWhere += strAnd + " maplot between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					// End If
				}
				strAnd = " and ";
				strOrderBy = " order by maplot,ceaccount ";
			}
			else if (cmbtsearchtype.Text == "Contractor Number")
			{
				// contractor num
				boolContractors = true;
				boolPermits = true;
				boolPermitContractors = true;
				strSQL += ", " + tSQL.GetTableFields("permits") + ", " + tSQL.GetTableFields("permitcontractors") + ", " + tSQL.GetTableFields("contractors");
				// strSQL = strSQL & " inner join (permits inner join contractors on (permits.contractorid = contractors.ID)) on (permits.account = cemaster.ceaccount) "
				strWhere += strAnd + " contractors.ID = " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
				strAnd = " and ";
				strOrderBy = " order by contractors.ID,name,ceaccount";
			}
			else if (cmbtsearchtype.Text == "Permit")
			{
				// permit
				// search for permit number
				int lngPermitNumber;
				int lngPermitYear;
				boolPermits = true;
				strSQL += ", " + tSQL.GetTableFields("permits");
				intTemp = Strings.InStr(1, txtSearch.Text, "-", CompareConstants.vbTextCompare);
				if (intTemp > 0)
				{
					strAry = Strings.Split(fecherFoundation.Strings.Trim(txtSearch.Text), "-", -1, CompareConstants.vbTextCompare);
					// strSQL = strSQL & " inner join permits on (permits.account = cemaster.ceaccount) "
					if (strAry[0].Length > 0)
					{
						if (strAry[0].Length == 2)
						{
							if (Conversion.Val(strAry[0]) > 85)
							{
								strWhere += strAnd + " permits.permityear = 19" + strAry[0];
							}
							else
							{
								strWhere += strAnd + " permits.permityear = 20" + strAry[0] + " ";
							}
						}
						else
						{
							strWhere += strAnd + " permits.permityear = " + FCConvert.ToString(FCConvert.ToDateTime(strAry[0]).Year) + " ";
						}
						strAnd = " and ";
					}
					if (strAry[1].Length > 0)
					{
						strWhere += strAnd + " permits.permitnumber = " + FCConvert.ToString(Conversion.Val(strAry[1])) + " ";
						strAnd = " and ";
					}
					strOrderBy = " order by permityear desc,permitnumber ";
				}
				else
				{
					MessageBox.Show("Permit numbers must be entered in yy-number format where yy and number can be omitted (Dash must be included)", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbtsearchtype.Text == "Permit Identifier")
			{
				boolPermits = true;
				strSQL += ", " + tSQL.GetTableFields("permits");
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				strTemp = modMain.EscapeQuote(ref strTemp);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					strWhere += strAnd + " permits.permitidentifier like '%" + strTemp + "%' ";
				}
				else
				{
					strWhere += strAnd + " permits.permitidentifier between '" + strTemp + "' and '" + strTemp + "zzzzzz' ";
				}
				strOrderBy = " order by permitidentifier ";
				strAnd = " and ";
			}
			else if (cmbtsearchtype.Text == "Contractor Name")
			{
				// contractor name
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				// boolPermitContractors = True
				boolPermits = true;
				boolPermitContractors = true;
				strSQL += ", " + tSQL.GetTableFields("permits") + ", " + tSQL.GetTableFields("permitcontractors");
				// strSQL = strSQL & " inner join (permits inner join contractors on (permits.contractorid = contractors.ID)) on (permits.account = cemaster.ceaccount) "
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					strWhere += strAnd + " permitcontractorname like '%" + strTemp + "%' ";
				}
				else
				{
					strWhere += strAnd + " permitcontractorname between '" + strTemp + "' and '" + strTemp + "zzzz' ";
				}
				strAnd = " and ";
				strOrderBy = " order by permitcontractorname,name,ceaccount ";
			}
			else if (cmbtsearchtype.Text == "Inspection Date")
			{
				// inspection date
				if (Information.IsDate(t2kSearchDate.Text))
				{
					boolPermits = true;
					boolInspections = true;
					strSQL += ", " + tSQL.GetTableFields("Permits") + ", " + tSQL.GetTableFields("inspections");
					// strSQL = strSQL & " inner join (permits inner join inspections on (permits.ID = inspections.permitid)) on (permits.account = cemaster.ceaccount) "
					strWhere += strAnd + " inspections.inspectiondate = '" + t2kSearchDate.Text + "' ";
					strAnd = " and ";
					strOrderBy = " order by inspectiondate desc,ceaccount";
				}
				else
				{
					MessageBox.Show("You must enter a valid inspection date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			strSQL += " from " + tSQL.GetInnerJoin(boolContractors, boolInspections, true, boolPermits, false, false, boolPermitContractors, false);
			strSQL += strWhere + strOrderBy;
			SetupGridSearch();
			clsLoad.OpenRecordset(strSQL, "twce0000.vb1");
			GridSearch.Rows = 1;
			GridSearch.Tag = (System.Object)(CNSTGRIDSEARCHPROPERTY);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			FillGrid(ref clsLoad);
		}

		private void FillGrid(ref clsDRWrapper clsLoad)
		{
			int lngRow = 0;
			string strTemp = "";
			clsCEMaster MastRec = new clsCEMaster();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cmdNewSearch.Visible = true;
                //FC:FINAL:AKV:#3699 - Search Result screen extra buttons
                cmdClear.Visible = false;
                btnSearch.Visible = false;
				mnuSaveExit.Visible = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				while (!clsLoad.EndOfFile())
				{
					//App.DoEvents();
					GridSearch.Rows += 1;
					lngRow = GridSearch.Rows - 1;
					GridSearch.RowData(lngRow, false);
					if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPROPERTY)
					{
						if (cmbtsearchtype.Text == "Owner Name" || cmbtsearchtype.Text == "Location" || cmbtsearchtype.Text == "Map / Lot")
						{
							if (clsLoad.Get_Fields("propertydeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else
						{
							if (clsLoad.Get_Fields("propertydeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						if (cmbtsearchtype.Text == "Owner Name" || cmbtsearchtype.Text == "Location" || cmbtsearchtype.Text == "Map / Lot")
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("propertyautoid"));
						}
						else
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("propertyautoid"));
						}
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLACCT, clsLoad.Get_Fields_Int32("ceaccount"));
						// If Not CECustom.GetDataFromRE Then
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLNAME, clsLoad.Get_Fields_String("name"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLMAPLOT, clsLoad.Get_Fields_String("maplot"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTNUM, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetnumber"))));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTREET, clsLoad.Get_Fields_String("streetname"));
						// ElseIf (Not Optsearchtype(0).Value And Not Optsearchtype(1).Value And Not Optsearchtype(2).Value) Then
						// If clsLoad.Fields("rsaccount") <> 0 Then
						// have to get it
						// If MastRec.Load(clsLoad.Fields("rsaccount"), False) = clsLoad.Fields("rsaccount") Then
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLNAME) = MastRec.OwnerName
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLMAPLOT) = MastRec.MapLot
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTNUM) = MastRec.StreetNum
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTREET) = MastRec.StreetName
						// End If
						// Else
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLNAME) = clsLoad.Fields("name")
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLMAPLOT) = clsLoad.Fields("maplot")
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTNUM) = Val(clsLoad.Fields("streetnumber"))
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTREET) = clsLoad.Fields("streetname")
						// End If
						// Else
						// If clsLoad.Fields("cemaster.rsaccount") <> 0 Then
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLNAME) = clsLoad.Fields("rsname")
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLMAPLOT) = clsLoad.Fields("rsmaplot")
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTNUM) = Val(clsLoad.Fields("rslocnumalph"))
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTREET) = clsLoad.Fields("rslocstreet")
						// Else
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLNAME) = clsLoad.Fields("name")
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLMAPLOT) = clsLoad.Fields("maplot")
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTNUM) = Val(clsLoad.Fields("streetnumber"))
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLSTREET) = clsLoad.Fields("streetname")
						// End If
						// End If
						// If clsLoad.Fields("deleted") Then
						// GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1) = TRIOCOLORRED
						// GridSearch.RowData(lngRow) = True
						// End If
						if (cmbtsearchtype.Text == "Owner Name")
						{
							// name
							GridSearch.Col = CNSTGRIDSEARCHPROPERTYCOLNAME;
							GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
						}
						else if (cmbtsearchtype.Text == "Location")
						{
							// location
							GridSearch.Col = CNSTGRIDSEARCHPROPERTYCOLSTNUM;
							GridSearch.Sort = FCGrid.SortSettings.flexSortNumericAscending;
							GridSearch.Col = CNSTGRIDSEARCHPROPERTYCOLSTREET;
							GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
						}
						else if (cmbtsearchtype.Text == "Map / Lot")
						{
							// maplot
							GridSearch.Col = CNSTGRIDSEARCHPROPERTYCOLMAPLOT;
							GridSearch.Sort = FCGrid.SortSettings.flexSortStringAscending;
						}
						else if (cmbtsearchtype.Text == "Contractor Number")
						{
							// contractor number
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLEXTRA, clsLoad.Get_Fields_String("permitcontractorname"));
							if (clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("contractorautoid") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("contractorautoid");
						}
						else if (cmbtsearchtype.Text == "Contractor Name")
						{
							// contractor name
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLEXTRA, clsLoad.Get_Fields_String("permitcontractorname"));
							if (clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields_String("permitcontractorname") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields_String("permitcontractorname");
						}
						else if (cmbtsearchtype.Text == "Permit")
						{
							// permit
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLEXTRA, Strings.Right("0000" + clsLoad.Get_Fields_Int32("permityear"), 2) + "-" + clsLoad.Get_Fields("permitnumber"));
							if (clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("permitautoid") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("permitautoid");
						}
						else if (cmbtsearchtype.Text == "Permit Identifier")
						{
							// permit identifier
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLEXTRA, clsLoad.Get_Fields_String("permitidentifier"));
							if (clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("permitautoid") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("permitautoid");
						}
						else if (cmbtsearchtype.Text == "Inspection Date")
						{
							// inspection date
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPROPERTYCOLEXTRA,Strings.Format(clsLoad.Get_Fields_DateTime("inspectiondate"), "MM/dd/yyyy"));
							if (clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("inspectionautoid") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("propertyautoid") + " " + clsLoad.Get_Fields("inspectionautoid");
						}
					}
					else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHCONTRACTOR)
					{
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLNUMBER, clsLoad.Get_Fields_Int32("contractornumber"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLNAME, clsLoad.Get_Fields("contractorname1"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLNAME2, clsLoad.Get_Fields("contractorname2"));
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLLICENSE) = clsLoad.Fields("license")
						if (cmbtsearchtype.Text == "Location")
						{
							// location
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, fecherFoundation.Strings.Trim(clsLoad.Get_Fields_String("originalstreetnum") + " " + clsLoad.Get_Fields_String("originalstreet")));
							GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Location");
							if (clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields_String("originalstreetnum") + " " + clsLoad.Get_Fields_String("originalstreet") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields_String("originalstreetnum") + " " + clsLoad.Get_Fields_String("originalstreet");
						}
						else if (cmbtsearchtype.Text == "Map / Lot")
						{
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Map/Lot");
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, clsLoad.Get_Fields_String("originalmaplot"));
							if (clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields_String("originalmaplot") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields_String("originalmaplot");
						}
						else if (cmbtsearchtype.Text == "Permit")
						{
							GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Permit");
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, Strings.Right("0000" + clsLoad.Get_Fields_Int32("permityear"), 2) + "-" + clsLoad.Get_Fields("permitnumber"));
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Permit Identifier")
						{
							GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Permit Identifier");
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, clsLoad.Get_Fields_String("permitidentifier"));
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Inspection Date")
						{
							GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Inspection Date");
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, Strings.Format(clsLoad.Get_Fields_DateTime("Inspectiondate"), "MM/dd/yyyy"));
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
							if (clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields_DateTime("inspectiondate") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields_DateTime("inspectiondate");
						}
						else if (cmbtsearchtype.Text == "Owner Name")
						{
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, clsLoad.Get_Fields_String("originalowner"));
							GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Owner");
							if (clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields("account") == strTemp)
							{
								GridSearch.RemoveItem(lngRow);
							}
							strTemp = clsLoad.Get_Fields("contractorautoid") + " " + clsLoad.Get_Fields("account");
						}
						else
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("contractorautoid"));
							if (clsLoad.Get_Fields("contractordeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
					}
					else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPERMIT)
					{
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLNUMBER, Strings.Right(FCConvert.ToString(clsLoad.Get_Fields_Int32("permityear")), 2) + "-" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("permitnumber"))));
						// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLPLAN) = clsLoad.Fields("plan")
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLIDENTIFIER, clsLoad.Get_Fields_String("PERMITIDENTIFIER"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLNAME, clsLoad.Get_Fields_String("filedby"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLSTATUS, clsLoad.Get_Fields_Int32("appdencode"));
						if (Information.IsDate(clsLoad.Get_Fields("applicationdate")))
						{
							if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("applicationdate")).ToOADate() != 0)
							{
								GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLAPPDATE, Strings.Format(clsLoad.Get_Fields_DateTime("applicationdate"), "MM/dd/yyyy"));
							}
						}
						if (cmbtsearchtype.Text == "Owner Name")
						{
							// owner
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLEXTRA, clsLoad.Get_Fields_String("originalowner"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Location")
						{
							// location
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLSTNUM, clsLoad.Get_Fields_String("originalstreetnum"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLSTREET, clsLoad.Get_Fields_String("originalstreet"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Map / Lot")
						{
							// maplot
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLEXTRA, clsLoad.Get_Fields_String("originalmaplot"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Contractor Number")
						{
							// contractor number
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLEXTRA, clsLoad.Get_Fields_Int32("contractornumber"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Permit")
						{
							// permit
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Permit")
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Permit Identifier")
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Contractor Name")
						{
							// contractor name
							// GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID) = clsLoad.Fields("ID")
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLEXTRA, clsLoad.Get_Fields_String("permitcontractorname"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
						else if (cmbtsearchtype.Text == "Inspection Date")
						{
							// inspection date
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("permitautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHPERMITCOLEXTRA, Strings.Format(clsLoad.Get_Fields_DateTime("inspectiondate"), "MM/dd/yyyy"));
							if (clsLoad.Get_Fields("permitdeleted"))
							{
								GridSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, GridSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORRED);
								GridSearch.RowData(lngRow, true);
							}
						}
					}
					else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHINSPECTION)
					{
						if (Information.IsDate(clsLoad.Get_Fields("inspectiondate")))
						{
							if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("inspectiondate")).ToOADate() != 0)
							{
								GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLDATE, Strings.Format(clsLoad.Get_Fields_DateTime("inspectiondate"),"MM/dd/yyyy"));
							}
						}
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLINSPECTOR, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectorid"))));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLSTATUS, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectionstatus"))));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectiontype"))));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLPERMIT, Strings.Right("0000" + clsLoad.Get_Fields_Int32("permityear"), 2) + "-" + clsLoad.Get_Fields("permitnumber"));
						if (cmbtsearchtype.Text == "Owner Name")
						{
							// owner name
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, clsLoad.Get_Fields_String("originalowner"));
						}
						else if (cmbtsearchtype.Text == "Location")
						{
							// location
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLSTNUM, clsLoad.Get_Fields_String("originalstreetnum"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLSTREET, clsLoad.Get_Fields_String("originalstreet"));
						}
						else if (cmbtsearchtype.Text == "Map / Lot")
						{
							// maplot
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, clsLoad.Get_Fields_String("originalmaplot"));
						}
						else if (cmbtsearchtype.Text == "Contractor Number")
						{
							// contractor number
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, clsLoad.Get_Fields_Int32("contractornumber"));
						}
						else if (cmbtsearchtype.Text == "Permit")
						{
							// permit
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, clsLoad.Get_Fields_String("permitidentifier"));
						}
						else if (cmbtsearchtype.Text == "Permit Identifier")
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, clsLoad.Get_Fields_String("permitidentifier"));
						}
						else if (cmbtsearchtype.Text == "Contractor Name")
						{
							// contractor name
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, clsLoad.Get_Fields_String("permitcontractorname"));
						}
						else if (cmbtsearchtype.Text == "Inspection Date")
						{
							// inspection date
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, clsLoad.Get_Fields("inspectionautoid"));
						}
					}
					else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHVIOLATION)
					{
						if (Information.IsDate(clsLoad.Get_Fields("dateviolationissued")))
						{
							if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("dateviolationissued")).ToOADate() != 0)
							{
								GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLISSUED, Strings.Format(clsLoad.Get_Fields_DateTime("dateviolationissued"), "MM/dd/yyyy"));
							}
						}
						if (Information.IsDate(clsLoad.Get_Fields("dateviolationresolved")))
						{
							if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("dateviolationresolved")).ToOADate() != 0)
							{
								GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLRESOLVED, Strings.Format(clsLoad.Get_Fields_DateTime("dateviolationresolved"), "MM/dd/yyyy"));
							}
						}
						if (Conversion.Val(clsLoad.Get_Fields_String("violationstreetnumber")) > 0)
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLSTNUM, clsLoad.Get_Fields_String("violationstreetnumber"));
						}
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLSTREET, clsLoad.Get_Fields_String("violationstreet"));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("violationtype"))));
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLIDENTIFIER, clsLoad.Get_Fields_String("violationidentifier"));
						if (cmbViolationSearchType.Text != "Doing Business As")
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLNAME, clsLoad.Get_Fields_String("violationname"));
						}
						else
						{
							GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHVIOLATIONCOLNAME, clsLoad.Get_Fields_String("violationdba"));
						}
						GridSearch.TextMatrix(lngRow, CNSTGRIDSEARCHCOLAUTOID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("id"))));
					}
					clsLoad.MoveNext();
				}
				framSearch.Visible = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                Frame2.Visible = false;
				framSearch.BringToFront();
				ResizeGridSearch();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridSearch()
		{
			string strTemp = "";
			clsDRWrapper clsLoad;
			GridSearch.Rows = 1;
			
			
			GridSearch.ColHidden(CNSTGRIDSEARCHCOLAUTOID, true);
			if (cmbtrecordtype.Text == "Property Records")
			{
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLACCT, "Acct");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLNAME, "Name");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLMAPLOT, "Map Lot");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLSTNUM, "St Num");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLSTREET, "Street Name");
				if (cmbtsearchtype.Text == "Owner Name" || cmbtsearchtype.Text == "Location" || cmbtsearchtype.Text == "Map / Lot")
				{
					GridSearch.Cols = 6;
				}
				else
				{
					GridSearch.Cols = 7;
					// need to add another column
					if (cmbtsearchtype.Text == "Contractor Number")
					{
						// contractor number
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLEXTRA, "Contractor");
					}
					else if (cmbtsearchtype.Text == "Contractor Name")
					{
						// contractor name
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLEXTRA, "Contractor");
					}
					else if (cmbtsearchtype.Text == "Permit")
					{
						// permit
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLEXTRA, "Permit");
					}
					else if (cmbtsearchtype.Text == "Inspection Date")
					{
						// inspection date
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLEXTRA, "Inspection Date");
					}
					else if (cmbtsearchtype.Text == "Permit Identifier")
					{
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHPROPERTYCOLEXTRA, "Permit Identifier");
					}
				}
			}
			else if (cmbtrecordtype.Text == "Contractors")
			{
				// contractors
				GridSearch.Rows = 1;
				GridSearch.ColHidden(CNSTGRIDSEARCHCOLAUTOID, true);
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLNUMBER, "Number");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLNAME, "Name");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLNAME2, "Name2");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLLICENSE, "License");
				GridSearch.ColHidden(CNSTGRIDSEARCHCONTRACTORCOLLICENSE, true);
				if (cmbtsearchtype.Text == "Contractor Number" || cmbtsearchtype.Text == "Contractor Name")
				{
					GridSearch.Cols = 5;
				}
				else
				{
					GridSearch.Cols = 6;
					if (cmbtsearchtype.Text == "Location")
					{
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Location");
					}
					else if (cmbtsearchtype.Text == "Map / Lot")
					{
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Map Lot");
					}
					else if (cmbtsearchtype.Text == "Permit Identifier")
					{
						GridSearch.TextMatrix(0, CNSTGRIDSEARCHCONTRACTORCOLEXTRA, "Permit Identifier");
					}
				}
			}
			else if (cmbtrecordtype.Text == "Permits")
			{
				// permits
				GridSearch.Rows = 1;
				GridSearch.Cols = 7;
				GridSearch.ColHidden(CNSTGRIDSEARCHCOLAUTOID, true);
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLNUMBER, "Number");
				// .TextMatrix(0, CNSTGRIDSEARCHPERMITCOLPLAN) = "Plan"
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLIDENTIFIER, "Identifier");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLSTATUS, "Status");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLAPPDATE, "App Date");
				GridSearch.ColAlignment(CNSTGRIDSEARCHPERMITCOLAPPDATE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLNAME, "Filer");
				if (cmbtsearchtype.Text == "Owner Name")
				{
					// owner name
					GridSearch.Cols = 7;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLEXTRA, "Owner");
				}
				else if (cmbtsearchtype.Text == "Location")
				{
					// location
					GridSearch.Cols = 8;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLSTREET, "Street Name");
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLSTNUM, "St Num");
				}
				else if (cmbtsearchtype.Text == "Map / Lot")
				{
					// maplot
					GridSearch.Cols = 7;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLEXTRA, "Map Lot");
				}
				else if (cmbtsearchtype.Text == "Contractor Number")
				{
					// contractor number
					GridSearch.Cols = 7;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLEXTRA, "Contractor");
				}
				else if (cmbtsearchtype.Text == "Permit")
				{
					// permit
					GridSearch.Cols = 6;
				}
				else if (cmbtsearchtype.Text == "Permit Identifier")
				{
					// permit identifier
					GridSearch.Cols = 6;
				}
				else if (cmbtsearchtype.Text == "Contractor Name")
				{
					// contractor name
					GridSearch.Cols = 7;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLEXTRA, "Contractor");
				}
				else if (cmbtsearchtype.Text == "Inspection Date")
				{
					// inspection date
					GridSearch.Cols = 7;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHPERMITCOLEXTRA, "Inspection Date");
				}
				clsLoad = new clsDRWrapper();
				clsLoad.OpenRecordset("select * from SYSTEMCODES where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				strTemp = "";
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields("code") + ";" + clsLoad.Get_Fields_String("description") + "|";
					clsLoad.MoveNext();
				}
				strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTPERMITCOMPLETE) + ";Complete";
				GridSearch.ColComboList(CNSTGRIDSEARCHPERMITCOLSTATUS, strTemp);
			}
			else if (cmbtrecordtype.Text == "Violations")
			{
				GridSearch.Rows = 1;
				GridSearch.Cols = 8;
				GridSearch.ColHidden(CNSTGRIDSEARCHCOLAUTOID, true);
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLIDENTIFIER, "Identifier");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLISSUED, "Date Issued");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLRESOLVED, "Resolved");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLNAME, "Name");
				//FC:FINAL:CHN - i.issue #1696: Set col data format to show Date only.
				GridSearch.ColFormat(CNSTGRIDSEARCHVIOLATIONCOLISSUED, "MM/dd/yyyy");
				GridSearch.ColFormat(CNSTGRIDSEARCHVIOLATIONCOLRESOLVED, "MM/dd/yyyy");
				if (cmbViolationSearchType.Text == "Doing Business As")
				{
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLNAME, "Doing Business As");
				}
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLTYPE, "Type");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLSTNUM, "St Num");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHVIOLATIONCOLSTREET, "Street");
				clsLoad = new clsDRWrapper();
				clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE) + " order by code", modGlobalVariables.Statics.strCEDatabase);
				strTemp = "";
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields_Int32("ID") + ";" + clsLoad.Get_Fields_String("description") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != "")
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridSearch.ColComboList(CNSTGRIDSEARCHVIOLATIONCOLTYPE, strTemp);
			}
			else if (cmbtrecordtype.Text == "Inspections")
			{
				GridSearch.Rows = 1;
				GridSearch.Cols = 7;
				GridSearch.ColHidden(CNSTGRIDSEARCHCOLAUTOID, true);
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLDATE, "Date");
				GridSearch.ColAlignment(CNSTGRIDSEARCHINSPECTIONCOLDATE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLINSPECTOR, "Inspector");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLTYPE, "Type");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLSTATUS, "Status");
				GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLPERMIT, "Permit");
				if (cmbtsearchtype.Text == "Owner Name")
				{
					// owner name
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, "Owner");
				}
				else if (cmbtsearchtype.Text == "Location")
				{
					// location
					GridSearch.Cols = 8;
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLSTNUM, "St Num");
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLSTREET, "Street");
				}
				else if (cmbtsearchtype.Text == "Map / Lot")
				{
					// maplot
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, "Map Lot");
				}
				else if (cmbtsearchtype.Text == "Contractor Number")
				{
					// contractor number
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, "Contractor");
				}
				else if (cmbtsearchtype.Text == "Permit" || cmbtsearchtype.Text == "Permit Identifier")
				{
					// permit
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, "Indentifier");
				}
				else if (cmbtsearchtype.Text == "Contractor Name")
				{
					// contractor name
					GridSearch.TextMatrix(0, CNSTGRIDSEARCHINSPECTIONCOLEXTRA, "Contractor");
				}
				else if (cmbtsearchtype.Text == "Inspection Date")
				{
					// inspection date
					GridSearch.Cols = 6;
				}
				strTemp = "0#;Unknown|#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONPENDING) + ";Scheduled|#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONCOMPLETE) + ";Completed (Last)|";
				clsLoad = new clsDRWrapper();
				clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONSTATUS), modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Code"))) + ";" + clsLoad.Get_Fields_String("description") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridSearch.ColComboList(CNSTGRIDSEARCHINSPECTIONCOLSTATUS, strTemp);
				strTemp = "";
				clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONTYPE), modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("CODE"))) + ";" + clsLoad.Get_Fields_String("description") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridSearch.ColComboList(CNSTGRIDSEARCHINSPECTIONCOLTYPE, strTemp);
				strTemp = "#0;Unknown|";
				clsLoad.OpenRecordset("select * from inspectors order by NAME", modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))) + ";" + clsLoad.Get_Fields_String("name") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GridSearch.ColComboList(CNSTGRIDSEARCHINSPECTIONCOLINSPECTOR, strTemp);
			}
			ResizeGridSearch();
		}

		private void ResizeGridSearch()
		{
			int GridWidth = 0;
			GridWidth = GridSearch.WidthOriginal;
			if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPROPERTY)
			{
				GridSearch.ColWidth(CNSTGRIDSEARCHPROPERTYCOLACCT, FCConvert.ToInt32(0.07 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHPROPERTYCOLNAME, FCConvert.ToInt32(0.3 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHPROPERTYCOLMAPLOT, FCConvert.ToInt32(0.15 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHPROPERTYCOLSTNUM, FCConvert.ToInt32(0.08 * GridWidth));
				if (cmbtsearchtype.Text != "Owner Name" && cmbtsearchtype.Text != "Location" && cmbtsearchtype.Text != "Map / Lot")
				{
					// another column
					GridSearch.ColWidth(CNSTGRIDSEARCHPROPERTYCOLSTREET, FCConvert.ToInt32(0.2 * GridWidth));
				}
				else
				{
				}
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHCONTRACTOR)
			{
				GridSearch.ColWidth(CNSTGRIDSEARCHCONTRACTORCOLNUMBER, FCConvert.ToInt32(0.08 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHCONTRACTORCOLNAME, FCConvert.ToInt32(0.3 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHCONTRACTORCOLNAME2, FCConvert.ToInt32(0.3 * GridWidth));
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHPERMIT)
			{
				GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLNUMBER, FCConvert.ToInt32(0.09 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLIDENTIFIER, FCConvert.ToInt32(0.15 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLAPPDATE, FCConvert.ToInt32(0.11 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLSTATUS, FCConvert.ToInt32(0.19 * GridWidth));
				if (cmbtsearchtype.Text != "Permit" && cmbtsearchtype.Text != "Location")
				{
					GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLNAME, FCConvert.ToInt32(0.24 * GridWidth));
				}
				else if (cmbtsearchtype.Text == "Location")
				{
					GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLNAME, FCConvert.ToInt32(0.21 * GridWidth));
					GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLSTATUS, FCConvert.ToInt32(0.18 * GridWidth));
					GridSearch.ColWidth(CNSTGRIDSEARCHPERMITCOLSTNUM, FCConvert.ToInt32(0.08 * GridWidth));
				}
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHINSPECTION)
			{
				GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLDATE, FCConvert.ToInt32(0.12 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLPERMIT, FCConvert.ToInt32(0.11 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLTYPE, FCConvert.ToInt32(0.18 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLSTATUS, FCConvert.ToInt32(0.18 * GridWidth));
				if (cmbtsearchtype.Text == "Location")
				{
					GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLINSPECTOR, FCConvert.ToInt32(0.2 * GridWidth));
					GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLSTNUM, FCConvert.ToInt32(0.08 * GridWidth));
				}
				else if (cmbtsearchtype.Text != "Inspection Date")
				{
					GridSearch.ColWidth(CNSTGRIDSEARCHINSPECTIONCOLINSPECTOR, FCConvert.ToInt32(0.2 * GridWidth));
				}
			}
			else if (FCConvert.ToInt32(GridSearch.Tag) == CNSTGRIDSEARCHVIOLATION)
			{
				if (cmbViolationSearchType.Text == "Date Violation Issued" || cmbViolationSearchType.Text == "Date Violation Occurred" || cmbViolationSearchType.Text == "Date Violation Resolved")
				{
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLSTNUM, true);
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLSTREET, true);
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLISSUED, false);
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLRESOLVED, false);
					GridSearch.ColWidth(CNSTGRIDSEARCHVIOLATIONCOLISSUED, FCConvert.ToInt32(0.12 * GridWidth));
				}
				else
				{
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLSTNUM, false);
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLSTREET, false);
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLISSUED, true);
					GridSearch.ColHidden(CNSTGRIDSEARCHVIOLATIONCOLRESOLVED, true);
					GridSearch.ColWidth(CNSTGRIDSEARCHVIOLATIONCOLSTNUM, FCConvert.ToInt32(0.08 * GridWidth));
				}
				GridSearch.ColWidth(CNSTGRIDSEARCHVIOLATIONCOLIDENTIFIER, FCConvert.ToInt32(0.11 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHVIOLATIONCOLNAME, FCConvert.ToInt32(0.3 * GridWidth));
				GridSearch.ColWidth(CNSTGRIDSEARCHVIOLATIONCOLTYPE, FCConvert.ToInt32(0.25 * GridWidth));
			}
		}

		private void SearchPermit()
		{
			string strSQL;
			string strWhere;
			string strOrderBy = "";
			string strAnd;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			string[] strAry = null;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp = 0;
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			string strTables;
			bool boolContractors;
			bool boolInspections;
			bool boolInspectors;
			bool boolPermitContractors;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolContractors = false;
				boolInspections = false;
				boolInspectors = false;
				boolPermitContractors = false;
				strAnd = "";
				strSQL = "select * from permits ";
				strSQL = "select " + tSQL.GetTableFields("permits");
				strWhere = " where ";
				if (chkShowDel.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					strWhere += strAnd + " permits.deleted = 0 ";
					strAnd = " and ";
				}
				strTables = "Permits";
				if (cmbtsearchtype.Text == "Owner Name")
				{
					// owner
					// strSQL = strSQL & " inner join cemaster on (permits.account = cemaster.ceaccount) "
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " remaster.rscard = 1 "
					// strAnd = " and "
					// End If
					// strTemp = Trim(txtSearch.Text)
					// strTemp = EscapeQuotes(strTemp)
					// If Optcontain(0).Value Then
					// strWhere = strWhere & strAnd & " rsname like '*" & strTemp & "*' "
					// Else
					// strWhere = strWhere & strAnd & " rsname between '" & strTemp & "' and '" & strTemp & "ZZZZZ' "
					// End If
					// strAnd = " and "
					// strOrderBy = " order by rsname,rsaccount "
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " originalowner like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " originalowner between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					strOrderBy = " order by originalowner ";
				}
				else if (cmbtsearchtype.Text == "Location")
				{
					// location
					// strSQL = strSQL & " inner join cemaster on (permits.account = cemaster.ceaccount) "
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " originalstreet like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " originalstreet between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					// strOrderBy = " order by originalstreetname,val(originalstreetnumber & '')"
					strOrderBy = " order by originalstreet,Convert(int,isnull(originalstreetnum,0))";
				}
				else if (cmbtsearchtype.Text == "Map / Lot")
				{
					// maplot
					// strSQL = strSQL & " inner join " & CECustom.MasterTable & " on (permits.account = " & CECustom.MasterTable & ".rsaccount) "
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " remaster.rscard = 1 "
					// strAnd = " and "
					// End If
					// strTemp = Trim(txtSearch.Text)
					// If Optcontain(0).Value Then
					// strWhere = strWhere & strAnd & " rsmaplot like '*" & strTemp & "*' "
					// Else
					// strWhere = strWhere & strAnd & " rsmaplot between '" & strTemp & "' and '" & strTemp & "ZZZZZ' "
					// End If
					// strAnd = " and "
					// strOrderBy = " order by rsmaplot,rsaccount "
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " originalmaplot like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " originalmaplot between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					strOrderBy = " order by originalmaplot ";
				}
				else if (cmbtsearchtype.Text == "Contractor Number")
				{
					// contractor number
					boolContractors = true;
					boolPermitContractors = true;
					strSQL += ", " + tSQL.GetTableFields("permitcontractors") + ", " + tSQL.GetTableFields("contractors");
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strWhere += strAnd + " contractornumber = " + FCConvert.ToString(Conversion.Val(strTemp));
					strAnd = " and ";
				}
				else if (cmbtsearchtype.Text == "Permit")
				{
					// permit
					intTemp = Strings.InStr(1, txtSearch.Text, "-", CompareConstants.vbTextCompare);
					if (intTemp > 0)
					{
						strAry = Strings.Split(fecherFoundation.Strings.Trim(txtSearch.Text), "-", -1, CompareConstants.vbTextCompare);
						if (strAry[0].Length > 0)
						{
							if (strAry[0].Length == 2)
							{
								if (Conversion.Val(strAry[0]) > 85)
								{
									strWhere += strAnd + " permityear = 19" + strAry[0];
								}
								else
								{
									strWhere += strAnd + " permityear = 20" + strAry[0] + " ";
								}
							}
							else
							{
								strWhere += strAnd + " permityear = " + FCConvert.ToString(FCConvert.ToDateTime(strAry[0]).Year) + " ";
							}
							strAnd = " and ";
						}
						if (strAry[1].Length > 0)
						{
							strWhere += strAnd + " permitnumber = " + FCConvert.ToString(Conversion.Val(strAry[1])) + " ";
							strAnd = " and ";
						}
						strOrderBy = " order by permityear desc,permitnumber ";
					}
					else
					{
						MessageBox.Show("Permit numbers must be entered in yy-number format where yy and number can be omitted (Dash must be included)", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbtsearchtype.Text == "Permit Identifier")
				{
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modMain.EscapeQuote(ref strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " permitidentifier like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " permitidentifier between '" + strTemp + "' and '" + strTemp + "zzzzzz' ";
					}
					strAnd = " and ";
				}
				else if (cmbtsearchtype.Text == "Contractor Name")
				{
					// contractor name
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modMain.EscapeQuote(ref strTemp);
					boolContractors = true;
					boolPermitContractors = true;
					strTables += ",permitcontractors";
					strSQL += ", " + tSQL.GetTableFields("permitcontractors");
					// strSQL = strSQL & " inner join permitcontractors on (permits.ID = permitcontractors.permitidnum) "
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " PERMITCONTRACtorname like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " permitcontractorname between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strOrderBy = " Order by permitcontractorname,permityear desc,permitnumber ";
					strAnd = " and ";
				}
				else if (cmbtsearchtype.Text == "Inspection Date")
				{
					// inspection date
					boolInspections = true;
					strTables += ",inspections";
					if (Information.IsDate(t2kSearchDate.Text))
					{
						// strSQL = strSQL & " inner join  inspections on (permits.ID = inspections.permitid) "
						strSQL += ", " + tSQL.GetTableFields("inspections");
						strWhere += strAnd + " inspections.inspectiondate = '" + t2kSearchDate.Text + "' ";
						strAnd = " and ";
						strOrderBy = " order by inspectiondate desc,permityear desc,permitnumber";
					}
					else
					{
						MessageBox.Show("You must enter a valid inspection date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				strSQL += " from ";
				strSQL += tSQL.GetInnerJoin(boolContractors, boolInspections, false, true, boolInspectors, false, boolPermitContractors, false);
				strSQL += strWhere + strOrderBy;
				GridSearch.Rows = 1;
				GridSearch.Tag = (System.Object)(CNSTGRIDSEARCHPERMIT);
				SetupGridSearch();
				clsLoad.OpenRecordset(strSQL, "twce0000.vb1");
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				FillGrid(ref clsLoad);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SearchPermit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SearchViolation()
		{
			string strSQL;
			string strWhere;
			string strOrderBy = "";
			string strAnd;
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				strAnd = "";
				strSQL = "select " + tSQL.GetTableFields("violations");
				strWhere = " where ";
				if (cmbViolationSearchType.Text == "Name")
				{
					// name
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " violationname like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " violationname between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					strOrderBy = " order by violationname ";
				}
				else if (cmbViolationSearchType.Text == "Location")
				{
					// location
					strTemp = fecherFoundation.Strings.Trim(txtSearch2.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (strTemp != string.Empty)
					{
						if (cmbtcontain.Text == "Contain Search Criteria")
						{
							strWhere += strAnd + " violationstreet like '%" + strTemp + "%' ";
						}
						else
						{
							strWhere += strAnd + " violationstreet between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
						}
						strAnd = " and ";
					}
					if (Conversion.Val(txtSearch.Text) > 0)
					{
						strWhere += strAnd + " val(violationstreetnumber & '') = " + FCConvert.ToString(Conversion.Val(txtSearch.Text + "")) + " ";
						strAnd = " and ";
					}
					strOrderBy = " order by violationstreet,violationstreetnumber ";
				}
				else if (cmbViolationSearchType.Text == "Doing Business As")
				{
					// doing business as
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " violationdba like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " violationdba between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					strOrderBy = " order by violationdba ";
				}
				else if (cmbViolationSearchType.Text == "Date Violation Issued")
				{
					// violation issued date
					if (Information.IsDate(t2kSearchDate.Text))
					{
						strWhere += strAnd + " dateviolationissued = '" + t2kSearchDate.Text + "' ";
						strAnd = " and ";
						strOrderBy = " order by dateviolationissued desc";
					}
					else
					{
						MessageBox.Show("You must enter a valid issue date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbViolationSearchType.Text == "Date Violation Occurred")
				{
					// violation occurred date
					if (Information.IsDate(t2kSearchDate.Text))
					{
						strWhere += strAnd + " dateviolationoccurred = '" + t2kSearchDate.Text + "' ";
						strAnd = " and ";
						strOrderBy = " order by dateviolationoccurred desc";
					}
					else
					{
						MessageBox.Show("You must enter a valid date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbViolationSearchType.Text == "Date Violation Resolved")
				{
					// violation resolved date
					if (Information.IsDate(t2kSearchDate.Text))
					{
						strWhere += strAnd + " dateviolationresolved = '" + t2kSearchDate.Text + "' ";
						strAnd = " and ";
						strOrderBy = " order by dateviolationresolved desc";
					}
					else
					{
						MessageBox.Show("You must enter a valid resolution date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				strSQL += " from " + tSQL.GetInnerJoin(false, false, false, false, false, false, false, true);
				strSQL += strWhere + strOrderBy;
				GridSearch.Rows = 1;
				GridSearch.Tag = (System.Object)(CNSTGRIDSEARCHVIOLATION);
				SetupGridSearch();
				rsLoad.OpenRecordset(strSQL, "twce0000.vb1");
				if (rsLoad.EndOfFile())
				{
					MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				FillGrid(ref rsLoad);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SearchViolation", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void SearchInspection()
		{
			string strSQL;
			string strWhere;
			string strOrderBy = "";
			string strAnd;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			string[] strAry = null;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp = 0;
			bool boolContractors;
			bool boolPermits;
			bool boolPermitContractors;
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolContractors = false;
				boolPermits = false;
				boolPermitContractors = false;
				strAnd = "";
				// strSQL = "select * from inspections "
				strSQL = "select " + tSQL.GetTableFields("inspections");
				strWhere = " where ";
				if (chkShowDel.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					strWhere += strAnd + " inspections.deleted = 0 ";
					strAnd = " and ";
				}
				if (cmbtsearchtype.Text == "Owner Name")
				{
					// owner name
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits");
					// strSQL = strSQL & " inner join permits on (permits.ID = inspections.permitid) "
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " originalowner like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " originalowner between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					strOrderBy = " order by originalowner,permits.account ";
				}
				else if (cmbtsearchtype.Text == "Location")
				{
					// location
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits");
					// strSQL = strSQL & " inner join permits  on (permits.ID = inspections.permitid) "
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " remaster.rscard = 1 "
					// strAnd = " and "
					// End If
					strTemp = fecherFoundation.Strings.Trim(txtSearch2.Text);
					strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
					if (strTemp != string.Empty)
					{
						if (cmbtcontain.Text == "Contain Search Criteria")
						{
							strWhere += strAnd + " originalstreet like '%" + strTemp + "%' ";
						}
						else
						{
							strWhere += strAnd + " originalstreet between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
						}
						strAnd = " and ";
					}
					if (Conversion.Val(txtSearch.Text) > 0)
					{
						strWhere += strAnd + " val(originalstreetnum & '') = " + FCConvert.ToString(Conversion.Val(txtSearch.Text + "")) + " ";
						strAnd = " and ";
					}
					strOrderBy = " order by originalstreet,originalstreetnum,permits.account ";
				}
				else if (cmbtsearchtype.Text == "Map / Lot")
				{
					// maplot
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits");
					// strSQL = strSQL & " inner join permits on (permits.ID = inspections.permitid) "
					// If CECustom.GetDataFromRE Then
					// strWhere = strWhere & strAnd & " remaster.rscard = 1 "
					// strAnd = " and "
					// End If
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " originalmaplot like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " originalmaplot between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strAnd = " and ";
					strOrderBy = " order by originalmaplot,permits.account ";
				}
				else if (cmbtsearchtype.Text == "Contractor Number")
				{
					// contractor number
					boolContractors = true;
					boolPermitContractors = true;
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits") + ", " + tSQL.GetTableFields("permitcontractors") + ", " + tSQL.GetTableFields("contractors");
					// strSQL = strSQL & " inner join (permits inner join contractors on (contractors.ID = permits.contractorid)) on (permits.ID = inspections.permitid) "
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strWhere += strAnd + " contractornumber = " + FCConvert.ToString(Conversion.Val(strTemp));
					strAnd = " and ";
				}
				else if (cmbtsearchtype.Text == "Permit")
				{
					// permit
					// search for permit number
					int lngPermitNumber;
					int lngPermitYear;
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits");
					intTemp = Strings.InStr(1, txtSearch.Text, "-", CompareConstants.vbTextCompare);
					if (intTemp > 0)
					{
						strAry = Strings.Split(fecherFoundation.Strings.Trim(txtSearch.Text), "-", -1, CompareConstants.vbTextCompare);
						// strSQL = strSQL & " inner join permits on (permits.ID = inspections.PERMITID) "
						if (strAry[0].Length > 0)
						{
							if (strAry[0].Length == 2)
							{
								if (Conversion.Val(strAry[0]) > 85)
								{
									strWhere += strAnd + " permits.permityear = 19" + strAry[0];
								}
								else
								{
									strWhere += strAnd + " permits.permityear = 20" + strAry[0] + " ";
								}
							}
							else
							{
								strWhere += strAnd + " permits.permityear = " + FCConvert.ToString(FCConvert.ToDateTime(strAry[0]).Year) + " ";
							}
							strAnd = " and ";
						}
						if (strAry[1].Length > 0)
						{
							strWhere += strAnd + " permits.permitnumber = " + FCConvert.ToString(Conversion.Val(strAry[1])) + " ";
							strAnd = " and ";
						}
						strOrderBy = " order by permityear desc,permitnumber ";
					}
					else
					{
						MessageBox.Show("Permit numbers must be entered in yy-number format where yy and number can be omitted (Dash must be included)", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else if (cmbtsearchtype.Text == "Permit Identifier")
				{
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits");
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modMain.EscapeQuote(ref strTemp);
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " permits.permitidentifier like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " permits.permitidentifier between '" + strTemp + "' and '" + strTemp + "zzzzzz' ";
					}
					strOrderBy = " order by permitidentifier ";
					strAnd = " and ";
				}
				else if (cmbtsearchtype.Text == "Contractor Name")
				{
					// contractor name
					boolContractors = true;
					// boolPermitContractors = True
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits") + ", " + tSQL.GetTableFields("permitcontractors");
					strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
					strTemp = modMain.EscapeQuote(ref strTemp);
					// strSQL = strSQL & " inner join permits on (permits.ID = inspections.permitid) "
					if (cmbtcontain.Text == "Contain Search Criteria")
					{
						strWhere += strAnd + " PERMITCONTRACtorname like '%" + strTemp + "%' ";
					}
					else
					{
						strWhere += strAnd + " permitcontractorname between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
					}
					strOrderBy = " Order by permitcontractorname ";
					strAnd = " and ";
				}
				else if (cmbtsearchtype.Text == "Inspection Date")
				{
					// inspection date
					boolPermits = true;
					strSQL += ", " + tSQL.GetTableFields("permits");
					if (Information.IsDate(t2kSearchDate.Text))
					{
						strWhere += strAnd + " inspectiondate = '" + t2kSearchDate.Text + "' ";
						strAnd = " and ";
						strOrderBy = " order by inspectiondate desc";
					}
					else
					{
						MessageBox.Show("You must enter a valid inspection date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				strSQL += " from " + tSQL.GetInnerJoin(boolContractors, true, false, boolPermits, false, false, boolPermitContractors, false);
				strSQL += strWhere + strOrderBy;
				GridSearch.Rows = 1;
				GridSearch.Tag = (System.Object)(CNSTGRIDSEARCHINSPECTION);
				SetupGridSearch();
				clsLoad.OpenRecordset(strSQL, "twce0000.vb1");
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				FillGrid(ref clsLoad);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SearchInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SearchContractor()
		{
			string strSQL;
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strOrderBy;
			string strWhere;
			string strInnerSQL = "";
			string strSearch = "";
			string[] strAry = null;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp = 0;
			string strAnd;
			bool boolPermits;
			bool boolProperty;
			bool boolInspections;
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			boolPermits = false;
			boolProperty = false;
			boolInspections = false;
			strAnd = "";
			// strSQL = "select * from contractors "
			strSQL = "select " + tSQL.GetTableFields("contractors");
			strOrderBy = "";
			strWhere = " where ";
			if (chkShowDel.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				strWhere += strAnd + " not isnull(contractors.deleted,0) = 1 ";
				strAnd = " and ";
			}
			if (cmbtsearchtype.Text == "Owner Name")
			{
				// name
				// strSQL = "Select * from contractors inner join ("
				strSQL += ", " + tSQL.GetTableFields("Permits") + ", " + tSQL.GetTableFields("PermitContractors");
				boolPermits = true;
				// boolProperty = True
				// strWhere = " and "
				// strInnerSQL = "permits inner join cemaster on (permits.account = cemaster.ceaccount)) on (contractors.ID = permits.contractorid) where "
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					// contains
					strSearch = " originalowner like '%" + strTemp + "%' ";
				}
				else
				{
					// begins with
					strSearch = " originalowner between '" + strTemp + "' and '" + strTemp + "zzzz'";
				}
				strWhere += strAnd + strSearch;
				strAnd = " and ";
				// strInnerSQL = strInnerSQL & " originalowner " & strSearch
				// strSQL = strSQL & strInnerSQL
				strOrderBy = " order by originalowner,contractors.ID";
			}
			else if (cmbtsearchtype.Text == "Location")
			{
				// location
				// must search for all permits with that location
				// strSQL = "Select * from contractors inner join ("
				// strWhere = " and "
				// strInnerSQL = "permits inner join cemaster on (permits.account = cemaster.ceaccount)) on (contractors.ID = permits.contractorid) where "
				boolPermits = true;
				strSQL += ", " + tSQL.GetTableFields("Permits") + ", " + tSQL.GetTableFields("PermitContractors");
				strTemp = fecherFoundation.Strings.Trim(txtSearch2.Text);
				strTemp = modGlobalFunctions.EscapeQuotes(strTemp);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					// contains
					strSearch = " like '%" + strTemp + "%' ";
				}
				else
				{
					// begins with
					strSearch = " between '" + strTemp + "' and '" + strTemp + "zzzz'";
				}
				if (Conversion.Val(txtSearch.Text) > 0)
				{
					strWhere += strAnd + " val(originalstreetnum & '') = " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
					strAnd = " and ";
					if (fecherFoundation.Strings.Trim(txtSearch2.Text) != string.Empty)
					{
						strWhere += strAnd + " and originalstreet " + strSearch;
					}
				}
				else
				{
					strWhere += strAnd + " originalstreet " + strSearch;
				}
				strAnd = " and ";
				// strSQL = strSQL & strInnerSQL
				strOrderBy = " order by orIginalstreet,originalstreetnum,contractors.ID";
			}
			else if (cmbtsearchtype.Text == "Map / Lot")
			{
				// maplot
				// strSQL = "Select * from contractors inner join ("
				// strWhere = " and "
				// strInnerSQL = "permits inner join cemaster on (permits.account = cemaster.ceaccount)) on (contractors.ID = permits.contractorid) where "
				boolPermits = true;
				strSQL += ", " + tSQL.GetTableFields("Permits") + ", " + tSQL.GetTableFields("PermitContractors");
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					// contains
					strSearch = " like '%" + strTemp + "%' ";
				}
				else
				{
					// begins with
					strSearch = " between '" + strTemp + "' and '" + strTemp + "zzzz'";
				}
				strWhere += strAnd + " originalmaplot " + strSearch;
				strAnd = " and ";
				// strSQL = strSQL & strInnerSQL
				strOrderBy = " order by originalmaplot,contractors.ID";
			}
			else if (cmbtsearchtype.Text == "Contractor Number")
			{
				// contractor number
				// strSQL = "select * from contractors where contractornumber = " & Val(txtSearch.Text)
				strWhere += strAnd + " contractornumber = " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
				strAnd = " and ";
			}
			else if (cmbtsearchtype.Text == "Permit")
			{
				// permit
				// search for permit number
				int lngPermitNumber;
				int lngPermitYear;
				boolPermits = true;
				strSQL += ", " + tSQL.GetTableFields("Permits") + ", " + tSQL.GetTableFields("PermitContractors");
				intTemp = Strings.InStr(1, txtSearch.Text, "-", CompareConstants.vbTextCompare);
				if (intTemp > 0)
				{
					strAry = Strings.Split(fecherFoundation.Strings.Trim(txtSearch.Text), "-", -1, CompareConstants.vbTextCompare);
					// strSQL = strSQL & " inner join permits on (permits.contractorid = contractors.ID) "
					if (strAry[0].Length > 0)
					{
						if (strAry[0].Length == 2)
						{
							if (Conversion.Val(strAry[0]) > 85)
							{
								strWhere += strAnd + " permits.permityear = 19" + strAry[0];
							}
							else
							{
								strWhere += strAnd + " permits.permityear = 20" + strAry[0] + " ";
							}
						}
						else
						{
							strWhere += strAnd + " permits.permityear = " + FCConvert.ToString(FCConvert.ToDateTime(strAry[0]).Year) + " ";
						}
						strAnd = " and ";
					}
					if (strAry[1].Length > 0)
					{
						strWhere += strAnd + " permits.permitnumber = " + FCConvert.ToString(Conversion.Val(strAry[1])) + " ";
						strAnd = " and ";
					}
					strOrderBy = " order by permityear desc,permitnumber ";
				}
				else
				{
					MessageBox.Show("Permit numbers must be entered in yy-number format where yy and number can be omitted (Dash must be included)", "Invalid Permit Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbtsearchtype.Text == "Permit Identifier")
			{
				boolPermits = true;
				strSQL += ", " + tSQL.GetTableFields("Permits") + ", " + tSQL.GetTableFields("PermitContractors");
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				strTemp = modMain.EscapeQuote(ref strTemp);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					strWhere += strAnd + " permits.permitidentifier like '%" + strTemp + "%' ";
				}
				else
				{
					strWhere += strAnd + " permits.permitidentifier between '" + strTemp + "' and '" + strTemp + "zzzzzz' ";
				}
				strOrderBy = " order by permitidentifier ";
				strAnd = " and ";
			}
			else if (cmbtsearchtype.Text == "Contractor Name")
			{
				// contractor name
				strTemp = fecherFoundation.Strings.Trim(txtSearch.Text);
				strTemp = modMain.EscapeQuote(ref strTemp);
				if (cmbtcontain.Text == "Contain Search Criteria")
				{
					strWhere += strAnd + " (name1 like '%" + strTemp + "%' or name2 like '%" + strTemp + "%') ";
				}
				else
				{
					strWhere += strAnd + "  name1 between '" + strTemp + "' and '" + strTemp + "ZZZZZ' or name2 between '" + strTemp + "' and '" + strTemp + "ZZZZZ' ";
				}
				strOrderBy = " Order by name1,NAME2,contractornumber ";
				strAnd = " and ";
			}
			else if (cmbtsearchtype.Text == "Inspection Date")
			{
				// inspection date
				boolPermits = true;
				boolInspections = true;
				if (Information.IsDate(t2kSearchDate.Text))
				{
					strSQL += ", " + tSQL.GetTableFields("permits") + ", " + tSQL.GetTableFields("permitcontractors") + ", " + tSQL.GetTableFields("inspections");
					// strSQL = strSQL & " inner join (permits inner join inspections on (permits.ID = inspections.permitid)) on (permits.contractorid = contractors.ID) "
					strWhere += strAnd + " inspections.inspectiondate = '" + t2kSearchDate.Text + "' ";
					strAnd = " and ";
					strOrderBy = " order by inspectiondate desc,contractors.ID";
				}
				else
				{
					MessageBox.Show("You must enter a valid inspection date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			strSQL += " from ";
			strSQL += tSQL.GetInnerJoin(true, boolInspections, false, boolPermits, false, false, boolPermits, false);
			strSQL += strWhere;
			strSQL += strOrderBy;
			SetupGridSearch();
			clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (clsLoad.EndOfFile())
			{
				MessageBox.Show("No Records Found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			GridSearch.Rows = 1;
			GridSearch.Tag = (System.Object)(CNSTGRIDSEARCHCONTRACTOR);
			FillGrid(ref clsLoad);
		}

		private void Optsearchtype_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 1:
					{
						// location
						txtSearch2.Visible = true;
						lblBook.Visible = true;
						lblPage.Visible = true;
						lblSearchInfo.Visible = false;
						//FC:FINAL:CHN - i.issue #1670: Redesign to get correct showing.
						// txtSearch.Width = FCConvert.ToInt32(cmdClear.Width / 2.0);
						// lblBook.Width = txtSearch.Width;
						// lblBook.Left = txtSearch.Left;
						// txtSearch2.Left = txtSearch.Left + txtSearch.Width + 60;
						// txtSearch2.Width = cmdSearch.Width + cmdSearch.Left - txtSearch2.Left;
						// lblPage.Width = txtSearch2.Width;
						// lblPage.Left = txtSearch2.Left;
						//lblBook.Left = cmbtcontain.Left;
						//txtSearch.Left = lblBook.Left + lblBook.Width + 15;
						//txtSearch.Width = 103;
						//txtSearch2.Width = 103;
						//lblPage.Left = txtSearch.Left + txtSearch.Width + 15;
						//txtSearch2.Left = lblPage.Left + lblPage.Width + 15;
						t2kSearchDate.Visible = false;
						txtSearch.Visible = true;
						//FC:FINAL:MSH - issue #1909: restore default field width to avoid overlapping
						txtSearch.Width = 103;
						txtSearch.Focus();
						break;
					}
				case 6:
					{
                        //FC:FINAL:AKV:#3703 - Search Field Adjustment
                        //Property IndentiFier

                        txtSearch2.Visible = false;
						lblBook.Visible = false;
						lblPage.Visible = false;
						lblSearchInfo.Visible = true;
						txtSearch.Visible = true;
						lblSearchInfo.Text = "Enter Search Criteria";
						//t2kSearchDate.Visible = true;
						//t2kSearchDate.Focus();
						break;
					}
                case 7:
                    {
                        //FC:FINAL:AKV:#3703 - Search Field Adjustment
                        // inspection date
                        txtSearch2.Visible = false;
                        lblBook.Visible = false;
                        lblPage.Visible = false;
                        lblSearchInfo.Visible = true;
                        txtSearch.Visible = false;
                        lblSearchInfo.Text = "Enter Inspection Date";
                        t2kSearchDate.Visible = true;
                        t2kSearchDate.Focus();
                        break;
                    }
				default:
					{
						txtSearch.Visible = true;
						//FC:FINAL:MSH - issue #1909: increase width of the search field
						txtSearch.Width = 320;
						txtSearch2.Visible = false;
						lblBook.Visible = false;
						lblPage.Visible = false;
						lblSearchInfo.Visible = true;
						t2kSearchDate.Visible = false;
						//FC:FINAL:CHN - i.issue #1670: Redesign to get correct showing.
						// txtSearch.Width = cmdSearch.Left + cmdSearch.Width - cmdClear.Left;
						//txtSearch.WidthOriginal = cmdSearch.LeftOriginal + cmdSearch.WidthOriginal - cmdClear.LeftOriginal;
						//txtSearch.Left = cmbtcontain.Left;
						lblSearchInfo.Text = "Enter Search Criteria";
						txtSearch.Focus();
						break;
					}
			}
			//end switch
		}

		private void Optsearchtype_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbtsearchtype.SelectedIndex;
			Optsearchtype_CheckedChanged(index, sender, e);
		}

		private void optViolationSearchType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 1:
					{
						// location
						txtSearch2.Visible = true;
						lblBook.Visible = true;
						lblPage.Visible = true;
						lblSearchInfo.Visible = false;
						//FC:FINAL:DDU:done by design logic
						//txtSearch.Width = FCConvert.ToInt32(cmdClear.Width / 2.0);
						//lblBook.Width = txtSearch.Width;
						//lblBook.Left = txtSearch.Left;
						//txtSearch2.Left = txtSearch.Left + txtSearch.Width + 60;
						//txtSearch2.Width = cmdSearch.Width + cmdSearch.Left - txtSearch2.Left;
						//lblPage.Width = txtSearch2.Width;
						//lblPage.Left = txtSearch2.Left;
						t2kSearchDate.Visible = false;
						txtSearch.Visible = true;
						//FC:FINAL:MSH - issue #1909: restore default field width to avoid overlapping
						txtSearch.Width = 103;
						txtSearch.Focus();
						break;
					}
				case 3:
				case 4:
				case 5:
					{
						// date
						txtSearch2.Visible = false;
						lblBook.Visible = false;
						lblPage.Visible = false;
						lblSearchInfo.Visible = true;
						txtSearch.Visible = false;
						lblSearchInfo.Text = "Enter Date";
						t2kSearchDate.Visible = true;
						t2kSearchDate.Focus();
						break;
					}
				default:
					{
						txtSearch.Visible = true;
						//FC:FINAL:MSH - issue #1909: increase width of the search field
						txtSearch.Width = 320;
						txtSearch2.Visible = false;
						lblBook.Visible = false;
						lblPage.Visible = false;
						lblSearchInfo.Visible = true;
						t2kSearchDate.Visible = false;
						//FC:FINAL:DDU:done by design logic
						//txtSearch.Width = cmdSearch.Left + cmdSearch.Width - cmdClear.Left;
						lblSearchInfo.Text = "Enter Search Criteria";
						txtSearch.Focus();
						break;
					}
			}
			//end switch
		}

		private void optViolationSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbViolationSearchType.SelectedIndex;
			optViolationSearchType_CheckedChanged(index, sender, e);
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						cmdGetAccountNumber_Click();
						break;
					}
			}
			//end switch
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						cmdSearch_Click();
						break;
					}
			}
			//end switch
		}

		private void txtSearch2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						cmdSearch_Click();
						break;
					}
			}
            //end switch
        }
    }
}
