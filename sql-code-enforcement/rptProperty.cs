﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptProperty.
	/// </summary>
	public partial class rptProperty : BaseSectionReport
	{
		public rptProperty()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptProperty InstancePtr
		{
			get
			{
				return (rptProperty)Sys.GetInstance(typeof(rptProperty));
			}
		}

		protected rptProperty _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptProperty	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref clsSQLStatement clsStatement)
		{
			string strSQL = "";
			if (!(clsStatement == null))
			{
				// strSQL = clsStatement.SQLStatement
				strSQL = "select distinct ceaccount, streetnumber , apt , streetname, last, first, middle, desig  from (" + clsStatement.SelectStatement + " " + clsStatement.WhereStatement + " " + clsStatement.OrderByStatement + ") abc";
			}
			else
			{
				strSQL = "Select * from cemaster where not deleted = 1 order by ceaccount";
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			txtRange.Text = clsStatement.ParameterDescriptionText;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Properties");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtPage.Text = "Page 1";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				clsDRWrapper rsTemp = new clsDRWrapper();
				txtAccount.Text = rsReport.Get_Fields_String("ceaccount");
				string strTemp = "";
				if (Conversion.Val(rsReport.Get_Fields_String("streetnumber")) > 0)
				{
					strTemp = rsReport.Get_Fields_String("streetnumber");
				}
				strTemp += " " + rsReport.Get_Fields_String("apt");
				strTemp = fecherFoundation.Strings.Trim(strTemp);
				strTemp += " " + rsReport.Get_Fields_String("streetname");
				strTemp = fecherFoundation.Strings.Trim(strTemp);
				txtLocation.Text = strTemp;
				strTemp = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("last"));
				if (strTemp != string.Empty)
				{
					if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("first")) != string.Empty || fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("middle")) != string.Empty)
					{
						strTemp = fecherFoundation.Strings.Trim(strTemp + ", " + rsReport.Get_Fields_String("first") + " " + rsReport.Get_Fields_String("middle") + " " + rsReport.Get_Fields_String("desig"));
					}
				}
				else
				{
					strTemp = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("first") + " " + rsReport.Get_Fields_String("middle") + " " + rsReport.Get_Fields_String("desig"));
				}
				txtOwner.Text = strTemp;
				// now figure out how many outstanding permits there are
				rsTemp.OpenRecordset("select count(account) as thecount from permits where account = " + rsReport.Get_Fields_String("ceaccount") + " and appdencode <> " + FCConvert.ToString(modCEConstants.CNSTPERMITCOMPLETE), modGlobalVariables.Statics.strCEDatabase);
				if (!rsTemp.EndOfFile())
				{
					txtPermits.Text = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_String("thecount")));
				}
				else
				{
					txtPermits.Text = "0";
				}
				rsTemp.Dispose();
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
