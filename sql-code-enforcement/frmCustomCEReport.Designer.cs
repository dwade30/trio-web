//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmCustomCEReport.
	/// </summary>
	partial class frmCustomCEReport
	{
		public fecherFoundation.FCComboBox cmbTotal;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCComboBox cmbJust;
		public fecherFoundation.FCLabel lblJust;
		public fecherFoundation.FCTextBox txtTitle;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtOpenCode;
		public fecherFoundation.FCTextBox txtCode;
		public fecherFoundation.FCTextBox txtData;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraFields;
		public FCGrid GridFields;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsLayout;
        public fecherFoundation.FCLine Line1;
        public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCLabel Label2;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuInsertColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnusepar6;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuInsertRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuLoad;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteReports;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveAs;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuOptions;
		public fecherFoundation.FCToolStripMenuItem mnuAddCol;
		public fecherFoundation.FCToolStripMenuItem mnuInsertCol;
		public fecherFoundation.FCToolStripMenuItem mnuDelCol;
		public fecherFoundation.FCToolStripMenuItem mnusepar4;
		public fecherFoundation.FCToolStripMenuItem mnuRowAdd;
		public fecherFoundation.FCToolStripMenuItem mnuRowInsert;
		public fecherFoundation.FCToolStripMenuItem mnuDelRow;
		public fecherFoundation.FCToolStripMenuItem mnuSepar5;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomCEReport));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbTotal = new fecherFoundation.FCComboBox();
            this.lblTotal = new fecherFoundation.FCLabel();
            this.cmbJust = new fecherFoundation.FCComboBox();
            this.lblJust = new fecherFoundation.FCLabel();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtOpenCode = new fecherFoundation.FCTextBox();
            this.txtCode = new fecherFoundation.FCTextBox();
            this.txtData = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraFields = new fecherFoundation.FCFrame();
            this.GridFields = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.Line1 = new fecherFoundation.FCLine();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.Label2 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuInsertColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInsertRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLoad = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteReports = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveAs = new fecherFoundation.FCToolStripMenuItem();
            this.mnuOptions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddCol = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInsertCol = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelCol = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRowAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRowInsert = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddColumn = new fecherFoundation.FCButton();
            this.cmdDeleteColumn = new fecherFoundation.FCButton();
            this.cmdAddRow = new fecherFoundation.FCButton();
            this.cmdDeleteRow = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtTitle);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddColumn);
            this.TopPanel.Controls.Add(this.cmdDeleteColumn);
            this.TopPanel.Controls.Add(this.cmdAddRow);
            this.TopPanel.Controls.Add(this.cmdDeleteRow);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddColumn, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(190, 30);
            this.HeaderText.Text = "Custom Reports";
            // 
            // cmbTotal
            // 
            this.cmbTotal.AutoSize = false;
            this.cmbTotal.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbTotal.FormattingEnabled = true;
            this.cmbTotal.Items.AddRange(new object[] {
            "Count",
            "Total",
            "None"});
            this.cmbTotal.Location = new System.Drawing.Point(148, 150);
            this.cmbTotal.Name = "cmbTotal";
            this.cmbTotal.Size = new System.Drawing.Size(174, 40);
            this.cmbTotal.TabIndex = 19;
            this.cmbTotal.Text = "None";
            this.cmbTotal.SelectedIndexChanged += new System.EventHandler(this.optTotal_CheckedChanged);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(30, 164);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(96, 16);
            this.lblTotal.TabIndex = 20;
            this.lblTotal.Text = "SHOW TOTAL";
            // 
            // cmbJust
            // 
            this.cmbJust.AutoSize = false;
            this.cmbJust.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbJust.FormattingEnabled = true;
            this.cmbJust.Items.AddRange(new object[] {
            "Right",
            "Left",
            "Center"});
            this.cmbJust.Location = new System.Drawing.Point(148, 90);
            this.cmbJust.Name = "cmbJust";
            this.cmbJust.Size = new System.Drawing.Size(174, 40);
            this.cmbJust.TabIndex = 21;
            this.cmbJust.Text = "Left";
            this.cmbJust.SelectedIndexChanged += new System.EventHandler(this.optJust_CheckedChanged);
            // 
            // lblJust
            // 
            this.lblJust.AutoSize = true;
            this.lblJust.Location = new System.Drawing.Point(30, 104);
            this.lblJust.Name = "lblJust";
            this.lblJust.Size = new System.Drawing.Size(108, 16);
            this.lblJust.TabIndex = 22;
            this.lblJust.Text = "JUSTIFICATION";
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.LinkItem = null;
            this.txtTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtTitle.LinkTopic = null;
            this.txtTitle.Location = new System.Drawing.Point(127, 30);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(353, 40);
            this.txtTitle.TabIndex = 15;
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.Controls.Add(this.txtOpenCode);
            this.Frame2.Controls.Add(this.cmbTotal);
            this.Frame2.Controls.Add(this.lblTotal);
            this.Frame2.Controls.Add(this.cmbJust);
            this.Frame2.Controls.Add(this.lblJust);
            this.Frame2.Controls.Add(this.txtCode);
            this.Frame2.Controls.Add(this.txtData);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Location = new System.Drawing.Point(470, 376);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(341, 326);
            this.Frame2.TabIndex = 5;
            // 
            // txtOpenCode
            // 
            this.txtOpenCode.AutoSize = false;
            this.txtOpenCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpenCode.LinkItem = null;
            this.txtOpenCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtOpenCode.LinkTopic = null;
            this.txtOpenCode.Location = new System.Drawing.Point(148, 241);
            this.txtOpenCode.Name = "txtOpenCode";
            this.txtOpenCode.Size = new System.Drawing.Size(80, 40);
            this.txtOpenCode.TabIndex = 18;
            this.txtOpenCode.Visible = false;
            // 
            // txtCode
            // 
            this.txtCode.AutoSize = false;
            this.txtCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode.LinkItem = null;
            this.txtCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCode.LinkTopic = null;
            this.txtCode.Location = new System.Drawing.Point(242, 241);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(80, 40);
            this.txtCode.TabIndex = 17;
            this.txtCode.Visible = false;
            // 
            // txtData
            // 
            this.txtData.AutoSize = false;
            this.txtData.BackColor = System.Drawing.SystemColors.Window;
            this.txtData.Enabled = false;
            this.txtData.LinkItem = null;
            this.txtData.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtData.LinkTopic = null;
            this.txtData.Location = new System.Drawing.Point(148, 30);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(174, 40);
            this.txtData.TabIndex = 13;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(44, 18);
            this.Label1.TabIndex = 14;
            this.Label1.Text = "DATA";
            // 
            // fraFields
            // 
            this.fraFields.AppearanceKey = "groupBoxNoBorders";
            this.fraFields.Controls.Add(this.GridFields);
            this.fraFields.Location = new System.Drawing.Point(30, 376);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(418, 329);
            this.fraFields.TabIndex = 3;
            this.fraFields.Text = "Fields To Display On Report";
            // 
            // GridFields
            // 
            this.GridFields.AllowSelection = false;
            this.GridFields.AllowUserToResizeColumns = false;
            this.GridFields.AllowUserToResizeRows = false;
            this.GridFields.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridFields.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridFields.BackColorBkg = System.Drawing.Color.Empty;
            this.GridFields.BackColorFixed = System.Drawing.Color.Empty;
            this.GridFields.BackColorSel = System.Drawing.Color.Empty;
            this.GridFields.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridFields.Cols = 5;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridFields.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.GridFields.ColumnHeadersHeight = 30;
            this.GridFields.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridFields.ColumnHeadersVisible = false;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridFields.DefaultCellStyle = dataGridViewCellStyle6;
            this.GridFields.DragIcon = null;
            this.GridFields.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridFields.ExtendLastCol = true;
            this.GridFields.FixedRows = 0;
            this.GridFields.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridFields.FrozenCols = 0;
            this.GridFields.GridColor = System.Drawing.Color.Empty;
            this.GridFields.GridColorFixed = System.Drawing.Color.Empty;
            this.GridFields.Location = new System.Drawing.Point(0, 30);
            this.GridFields.Name = "GridFields";
            this.GridFields.OutlineCol = 0;
            this.GridFields.ReadOnly = true;
            this.GridFields.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridFields.RowHeightMin = 0;
            this.GridFields.Rows = 0;
            this.GridFields.ScrollTipText = null;
            this.GridFields.ShowColumnVisibilityMenu = false;
            this.GridFields.ShowFocusCell = false;
            this.GridFields.Size = new System.Drawing.Size(379, 277);
            this.GridFields.StandardTab = true;
            this.GridFields.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridFields.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridFields.TabIndex = 4;
            this.GridFields.DoubleClick += new System.EventHandler(this.GridFields_DblClick);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Controls.Add(this.Image1);
            this.Frame1.Controls.Add(this.Line1);
            this.Frame1.Location = new System.Drawing.Point(0, 90);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1049, 286);
            this.Frame1.TabIndex = 0;
            // 
            // vsLayout
            // 
            this.vsLayout.AllowSelection = false;
            this.vsLayout.AllowUserToResizeColumns = true;
            this.vsLayout.AllowUserToResizeRows = false;
            this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsLayout.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsLayout.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsLayout.BackColorBkg = System.Drawing.Color.Empty;
            this.vsLayout.BackColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.BackColorSel = System.Drawing.Color.Empty;
            this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Both;
            this.vsLayout.Cols = 0;
            dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsLayout.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.vsLayout.ColumnHeadersHeight = 30;
            this.vsLayout.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsLayout.DefaultCellStyle = dataGridViewCellStyle8;
            this.vsLayout.DragIcon = null;
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.FrozenCols = 0;
            this.vsLayout.GridColor = System.Drawing.Color.Empty;
            this.vsLayout.GridColorFixed = System.Drawing.Color.Empty;
            this.vsLayout.Location = new System.Drawing.Point(30, 56);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.OutlineCol = 0;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsLayout.RowHeightMin = 0;
            this.vsLayout.Rows = 1;
            this.vsLayout.ScrollTipText = null;
            this.vsLayout.ShowColumnVisibilityMenu = false;
            this.vsLayout.ShowFocusCell = false;
            this.vsLayout.Size = new System.Drawing.Size(990, 210);
            this.vsLayout.StandardTab = true;
            this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsLayout.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsLayout.TabIndex = 1;
            this.vsLayout.CurrentCellChanged += new System.EventHandler(this.vsLayout_RowColChange);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            this.vsLayout.ColumnWidthChanged += new DataGridViewColumnEventHandler(this.vsLayout_AfterUserResize);
            // 
            // Line1
            // 
            this.Line1.LineColor = System.Drawing.Color.Red;
            this.Line1.LineSize = 2;
            this.Line1.Location = new System.Drawing.Point(750, 56);
            this.Line1.Name = "line1";
            this.Line1.Orientation = Wisej.Web.Orientation.Vertical;
            this.Line1.Size = new System.Drawing.Size(2, 210);
            // 
            // Image1
            // 
            this.Image1.AllowDrop = true;
            this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.DrawStyle = ((short)(0));
            this.Image1.DrawWidth = ((short)(1));
            this.Image1.FillStyle = ((short)(1));
            this.Image1.FontTransparent = true;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(30, 20);
            this.Image1.Name = "Image1";
            this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
            this.Image1.Size = new System.Drawing.Size(990, 22);
            this.Image1.TabIndex = 3;
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry2});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(45, 17);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "TITLE";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuInsertColumn,
            this.mnuInsertRow,
            this.mnuLoad,
            this.mnuDeleteReports,
            this.mnuSaveAs,
            this.mnuOptions});
            this.MainMenu1.Name = null;
            // 
            // mnuInsertColumn
            // 
            this.mnuInsertColumn.Index = 0;
            this.mnuInsertColumn.Name = "mnuInsertColumn";
            this.mnuInsertColumn.Text = "Insert Column (Before Current)";
            this.mnuInsertColumn.Click += new System.EventHandler(this.mnuInsertColumn_Click);
            // 
            // mnuInsertRow
            // 
            this.mnuInsertRow.Index = 1;
            this.mnuInsertRow.Name = "mnuInsertRow";
            this.mnuInsertRow.Text = "Insert Row (Before Current)";
            this.mnuInsertRow.Click += new System.EventHandler(this.mnuInsertRow_Click);
            // 
            // mnuLoad
            // 
            this.mnuLoad.Index = 2;
            this.mnuLoad.Name = "mnuLoad";
            this.mnuLoad.Text = "Load Report";
            this.mnuLoad.Click += new System.EventHandler(this.mnuLoad_Click);
            // 
            // mnuDeleteReports
            // 
            this.mnuDeleteReports.Index = 3;
            this.mnuDeleteReports.Name = "mnuDeleteReports";
            this.mnuDeleteReports.Text = "Delete Report(s)";
            this.mnuDeleteReports.Click += new System.EventHandler(this.mnuDeleteReports_Click);
            // 
            // mnuSaveAs
            // 
            this.mnuSaveAs.Index = 4;
            this.mnuSaveAs.Name = "mnuSaveAs";
            this.mnuSaveAs.Text = "Save As";
            this.mnuSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // mnuOptions
            // 
            this.mnuOptions.Index = 5;
            this.mnuOptions.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddCol,
            this.mnuInsertCol,
            this.mnuDelCol,
            this.mnusepar4,
            this.mnuRowAdd,
            this.mnuRowInsert,
            this.mnuDelRow,
            this.mnuSepar5,
            this.mnuCancel});
            this.mnuOptions.Name = "mnuOptions";
            this.mnuOptions.Text = "Options";
            this.mnuOptions.Visible = false;
            // 
            // mnuAddCol
            // 
            this.mnuAddCol.Index = 0;
            this.mnuAddCol.Name = "mnuAddCol";
            this.mnuAddCol.Text = "Add Column (After Last)";
            this.mnuAddCol.Click += new System.EventHandler(this.mnuAddCol_Click);
            // 
            // mnuInsertCol
            // 
            this.mnuInsertCol.Index = 1;
            this.mnuInsertCol.Name = "mnuInsertCol";
            this.mnuInsertCol.Text = "Insert Column (Before Current)";
            this.mnuInsertCol.Click += new System.EventHandler(this.mnuInsertCol_Click);
            // 
            // mnuDelCol
            // 
            this.mnuDelCol.Index = 2;
            this.mnuDelCol.Name = "mnuDelCol";
            this.mnuDelCol.Text = "Delete Column";
            this.mnuDelCol.Click += new System.EventHandler(this.mnuDelCol_Click);
            // 
            // mnusepar4
            // 
            this.mnusepar4.Index = 3;
            this.mnusepar4.Name = "mnusepar4";
            this.mnusepar4.Text = "-";
            // 
            // mnuRowAdd
            // 
            this.mnuRowAdd.Index = 4;
            this.mnuRowAdd.Name = "mnuRowAdd";
            this.mnuRowAdd.Text = "Add Row (After Last)";
            this.mnuRowAdd.Click += new System.EventHandler(this.mnuRowAdd_Click);
            // 
            // mnuRowInsert
            // 
            this.mnuRowInsert.Index = 5;
            this.mnuRowInsert.Name = "mnuRowInsert";
            this.mnuRowInsert.Text = "Insert Row (Before Current)";
            this.mnuRowInsert.Click += new System.EventHandler(this.mnuRowInsert_Click);
            // 
            // mnuDelRow
            // 
            this.mnuDelRow.Index = 6;
            this.mnuDelRow.Name = "mnuDelRow";
            this.mnuDelRow.Text = "Delete Row";
            this.mnuDelRow.Click += new System.EventHandler(this.mnuDelRow_Click);
            // 
            // mnuSepar5
            // 
            this.mnuSepar5.Index = 7;
            this.mnuSepar5.Name = "mnuSepar5";
            this.mnuSepar5.Text = "-";
            // 
            // mnuCancel
            // 
            this.mnuCancel.Index = 8;
            this.mnuCancel.Name = "mnuCancel";
            this.mnuCancel.Text = "Cancel";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = -1;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row (After Last)";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = -1;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = -1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column (After Last)";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = -1;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // mnusepar6
            // 
            this.mnusepar6.Index = -1;
            this.mnusepar6.Name = "mnusepar6";
            this.mnusepar6.Text = "-";
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(498, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddColumn
            // 
            this.cmdAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddColumn.AppearanceKey = "toolbarButton";
            this.cmdAddColumn.Location = new System.Drawing.Point(530, 29);
            this.cmdAddColumn.Name = "cmdAddColumn";
            this.cmdAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdAddColumn.Size = new System.Drawing.Size(162, 24);
            this.cmdAddColumn.TabIndex = 1;
            this.cmdAddColumn.Text = "Add Column (After Last)";
            this.cmdAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // cmdDeleteColumn
            // 
            this.cmdDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteColumn.AppearanceKey = "toolbarButton";
            this.cmdDeleteColumn.Location = new System.Drawing.Point(698, 29);
            this.cmdDeleteColumn.Name = "cmdDeleteColumn";
            this.cmdDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdDeleteColumn.Size = new System.Drawing.Size(110, 24);
            this.cmdDeleteColumn.TabIndex = 3;
            this.cmdDeleteColumn.Text = "Delete Column";
            this.cmdDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // cmdAddRow
            // 
            this.cmdAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddRow.AppearanceKey = "toolbarButton";
            this.cmdAddRow.Location = new System.Drawing.Point(814, 29);
            this.cmdAddRow.Name = "cmdAddRow";
            this.cmdAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdAddRow.Size = new System.Drawing.Size(142, 24);
            this.cmdAddRow.TabIndex = 4;
            this.cmdAddRow.Text = "Add Row (After Last)";
            this.cmdAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // cmdDeleteRow
            // 
            this.cmdDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteRow.AppearanceKey = "toolbarButton";
            this.cmdDeleteRow.Location = new System.Drawing.Point(962, 29);
            this.cmdDeleteRow.Name = "cmdDeleteRow";
            this.cmdDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdDeleteRow.Size = new System.Drawing.Size(88, 24);
            this.cmdDeleteRow.TabIndex = 5;
            this.cmdDeleteRow.Text = "Delete Row";
            this.cmdDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // frmCustomCEReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmCustomCEReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Custom Reports";
            this.Load += new System.EventHandler(this.frmCustomCEReport_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomCEReport_KeyDown);
            this.Resize += new System.EventHandler(this.frmCustomCEReport_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddColumn;
		private FCButton cmdAddRow;
		private FCButton cmdDeleteColumn;
		private FCButton cmdDeleteRow;
    }
}