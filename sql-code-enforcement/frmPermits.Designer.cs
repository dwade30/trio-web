//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmPermits.
	/// </summary>
	partial class frmPermits
	{
		public fecherFoundation.FCComboBox cmbViolations;
		public fecherFoundation.FCLabel lblViolations;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtContractorNote;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCTextBox txtContractorZip4;
		public fecherFoundation.FCTextBox txtContractorZip;
		public fecherFoundation.FCTextBox txtContractorState;
		public fecherFoundation.FCTextBox txtContractorEmail;
		public fecherFoundation.FCTextBox txtContractCity;
		public fecherFoundation.FCTextBox txtContractorAddr2;
		public fecherFoundation.FCTextBox txtContractorAddr1;
		public fecherFoundation.FCTextBox txtContractorName;
		public fecherFoundation.FCComboBox cmbContractor;
		public FCGrid GridContractorPhone;
		private fecherFoundation.FCButton cmdListContractor;
		private fecherFoundation.FCButton cmdAddContractor;
		private fecherFoundation.FCButton cmdDeleteContractor;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel lblSuspended;
		public fecherFoundation.FCLabel lblContractorName;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCComboBox cmbApprovedDenied;
		public fecherFoundation.FCTextBox txtIdentifier;
		public Global.T2KDateBox t2kOccupancyDate;
		public fecherFoundation.FCCheckBox chkCertificateOfOccupancy;
		public fecherFoundation.FCTextBox txtOMBCode;
		public fecherFoundation.FCTextBox txtHousingUnits;
		public fecherFoundation.FCTextBox txtUnits;
		public fecherFoundation.FCTextBox txtBuildings;
		public fecherFoundation.FCTextBox txtPlan;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridOpens;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid GridInspections;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCRichTextBox rtbDescription;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public FCGrid gridActivity;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public FCGrid gridViolations;
		public fecherFoundation.FCTabPage SSTab1_Page6;
		public FCGrid GridChecklist;
		public FCGrid GridDeletedPhones;
		public FCGrid GridEstimate;
		public fecherFoundation.FCFrame framApplication;
		public fecherFoundation.FCTextBox txtFiledBy;
		public Global.T2KDateBox t2kApplicationDate;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCFrame framContact;
		public fecherFoundation.FCTextBox txtContactName;
		public fecherFoundation.FCTextBox txtEmail;
		public FCGrid GridPhones;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCLabel lblDesig;
		public fecherFoundation.FCLabel lblLastName;
		public fecherFoundation.FCLabel lblMiddleName;
		public fecherFoundation.FCLabel lblFirstName;
		public fecherFoundation.FCLabel lblStreetNum;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel lblOccupancyDate;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblPermitNumber;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label1_0;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddInspection;
		public fecherFoundation.FCToolStripMenuItem mnuAddActivity;
		public fecherFoundation.FCToolStripMenuItem mnuAddViolation;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuMove;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrintActivity;
		public fecherFoundation.FCToolStripMenuItem mnuPrintActivityWithDetail;
		public fecherFoundation.FCToolStripMenuItem mnuPrintViolations;
		public fecherFoundation.FCToolStripMenuItem mnuSepar5;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPermit;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCustomForm;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuDeletePermit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPermits));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("Frame1.Controls"))));
            Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("Frame1.Controls1"))));
            Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("Frame1.Controls2"))));
            this.cmbViolations = new fecherFoundation.FCComboBox();
            this.lblViolations = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtContractorNote = new fecherFoundation.FCTextBox();
            this.Picture1 = new fecherFoundation.FCPictureBox();
            this.txtContractorZip4 = new fecherFoundation.FCTextBox();
            this.txtContractorZip = new fecherFoundation.FCTextBox();
            this.txtContractorState = new fecherFoundation.FCTextBox();
            this.txtContractorEmail = new fecherFoundation.FCTextBox();
            this.txtContractCity = new fecherFoundation.FCTextBox();
            this.txtContractorAddr2 = new fecherFoundation.FCTextBox();
            this.txtContractorAddr1 = new fecherFoundation.FCTextBox();
            this.txtContractorName = new fecherFoundation.FCTextBox();
            this.cmbContractor = new fecherFoundation.FCComboBox();
            this.GridContractorPhone = new fecherFoundation.FCGrid();
            this.cmdListContractor = new fecherFoundation.FCButton();
            this.cmdAddContractor = new fecherFoundation.FCButton();
            this.cmdDeleteContractor = new fecherFoundation.FCButton();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.lblSuspended = new fecherFoundation.FCLabel();
            this.lblContractorName = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.cmbApprovedDenied = new fecherFoundation.FCComboBox();
            this.txtIdentifier = new fecherFoundation.FCTextBox();
            this.t2kOccupancyDate = new Global.T2KDateBox();
            this.chkCertificateOfOccupancy = new fecherFoundation.FCCheckBox();
            this.txtOMBCode = new fecherFoundation.FCTextBox();
            this.txtHousingUnits = new fecherFoundation.FCTextBox();
            this.txtUnits = new fecherFoundation.FCTextBox();
            this.txtBuildings = new fecherFoundation.FCTextBox();
            this.txtPlan = new fecherFoundation.FCTextBox();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.GridOpens = new fecherFoundation.FCGrid();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.GridInspections = new fecherFoundation.FCGrid();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.rtbDescription = new fecherFoundation.FCRichTextBox();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.gridActivity = new fecherFoundation.FCGrid();
            this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
            this.gridViolations = new fecherFoundation.FCGrid();
            this.SSTab1_Page6 = new fecherFoundation.FCTabPage();
            this.GridChecklist = new fecherFoundation.FCGrid();
            this.GridDeletedPhones = new fecherFoundation.FCGrid();
            this.GridEstimate = new fecherFoundation.FCGrid();
            this.framApplication = new fecherFoundation.FCFrame();
            this.txtFiledBy = new fecherFoundation.FCTextBox();
            this.t2kApplicationDate = new Global.T2KDateBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.framContact = new fecherFoundation.FCFrame();
            this.txtContactName = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.GridPhones = new fecherFoundation.FCGrid();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.lblDesig = new fecherFoundation.FCLabel();
            this.lblLastName = new fecherFoundation.FCLabel();
            this.lblMiddleName = new fecherFoundation.FCLabel();
            this.lblFirstName = new fecherFoundation.FCLabel();
            this.lblStreetNum = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.lblOccupancyDate = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblPermitNumber = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblMapLot = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuPrintActivity = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintActivityWithDetail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintViolations = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPermit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintCustomForm = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddInspection = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddActivity = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddViolation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMove = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeletePermit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdAddInspection = new fecherFoundation.FCButton();
            this.cmdAddActivity = new fecherFoundation.FCButton();
            this.cmdAddViolation = new fecherFoundation.FCButton();
            this.cmdComment = new fecherFoundation.FCButton();
            this.cmdMovePermit = new fecherFoundation.FCButton();
            this.cmdDeletePermit = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridContractorPhone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdListContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteContractor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kOccupancyDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCertificateOfOccupancy)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridInspections)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbDescription)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridActivity)).BeginInit();
            this.SSTab1_Page5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViolations)).BeginInit();
            this.SSTab1_Page6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridChecklist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridEstimate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framApplication)).BeginInit();
            this.framApplication.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kApplicationDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framContact)).BeginInit();
            this.framContact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPhones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddInspection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddViolation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMovePermit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeletePermit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 555);
            this.BottomPanel.Size = new System.Drawing.Size(1028, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmbApprovedDenied);
            this.ClientArea.Controls.Add(this.txtIdentifier);
            this.ClientArea.Controls.Add(this.t2kOccupancyDate);
            this.ClientArea.Controls.Add(this.chkCertificateOfOccupancy);
            this.ClientArea.Controls.Add(this.txtOMBCode);
            this.ClientArea.Controls.Add(this.txtHousingUnits);
            this.ClientArea.Controls.Add(this.txtUnits);
            this.ClientArea.Controls.Add(this.txtBuildings);
            this.ClientArea.Controls.Add(this.txtPlan);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.GridDeletedPhones);
            this.ClientArea.Controls.Add(this.GridEstimate);
            this.ClientArea.Controls.Add(this.framApplication);
            this.ClientArea.Controls.Add(this.framContact);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.lblDesig);
            this.ClientArea.Controls.Add(this.lblLastName);
            this.ClientArea.Controls.Add(this.lblMiddleName);
            this.ClientArea.Controls.Add(this.lblFirstName);
            this.ClientArea.Controls.Add(this.lblStreetNum);
            this.ClientArea.Controls.Add(this.Label17);
            this.ClientArea.Controls.Add(this.ImgComment);
            this.ClientArea.Controls.Add(this.lblOccupancyDate);
            this.ClientArea.Controls.Add(this.Label16);
            this.ClientArea.Controls.Add(this.Label15);
            this.ClientArea.Controls.Add(this.Label14);
            this.ClientArea.Controls.Add(this.Label13);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.lblPermitNumber);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.lblYear);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.lblLocation);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblMapLot);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.Label1_0);
            this.ClientArea.Size = new System.Drawing.Size(1028, 495);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddInspection);
            this.TopPanel.Controls.Add(this.cmdAddActivity);
            this.TopPanel.Controls.Add(this.cmdAddViolation);
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Controls.Add(this.cmdMovePermit);
            this.TopPanel.Controls.Add(this.cmdDeletePermit);
            this.TopPanel.Size = new System.Drawing.Size(1028, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeletePermit, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdMovePermit, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddViolation, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddActivity, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddInspection, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(95, 30);
            this.HeaderText.Text = "Permits";
            // 
            // cmbViolations
            // 
            this.cmbViolations.Items.AddRange(new object[] {
            "Show Current",
            "Show All"});
            this.cmbViolations.Location = new System.Drawing.Point(96, 20);
            this.cmbViolations.Name = "cmbViolations";
            this.cmbViolations.Size = new System.Drawing.Size(201, 40);
            this.cmbViolations.TabIndex = 28;
            this.cmbViolations.Text = "Show Current";
            this.cmbViolations.SelectedIndexChanged += new System.EventHandler(this.optViolations_CheckedChanged);
            // 
            // lblViolations
            // 
            this.lblViolations.AutoSize = true;
            this.lblViolations.Location = new System.Drawing.Point(20, 34);
            this.lblViolations.Name = "lblViolations";
            this.lblViolations.Size = new System.Drawing.Size(46, 15);
            this.lblViolations.TabIndex = 29;
            this.lblViolations.Text = "SHOW";
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1,
            imageListEntry2,
            imageListEntry3});
            this.ImageList1.ImageSize = new System.Drawing.Size(32, 32);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(255, 255, 255);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtContractorNote);
            this.Frame1.Controls.Add(this.Picture1);
            this.Frame1.Controls.Add(this.txtContractorZip4);
            this.Frame1.Controls.Add(this.txtContractorZip);
            this.Frame1.Controls.Add(this.txtContractorState);
            this.Frame1.Controls.Add(this.txtContractorEmail);
            this.Frame1.Controls.Add(this.txtContractCity);
            this.Frame1.Controls.Add(this.txtContractorAddr2);
            this.Frame1.Controls.Add(this.txtContractorAddr1);
            this.Frame1.Controls.Add(this.txtContractorName);
            this.Frame1.Controls.Add(this.cmbContractor);
            this.Frame1.Controls.Add(this.GridContractorPhone);
            this.Frame1.Controls.Add(this.cmdListContractor);
            this.Frame1.Controls.Add(this.cmdAddContractor);
            this.Frame1.Controls.Add(this.cmdDeleteContractor);
            this.Frame1.Controls.Add(this.Label19);
            this.Frame1.Controls.Add(this.Label18);
            this.Frame1.Controls.Add(this.lblSuspended);
            this.Frame1.Controls.Add(this.lblContractorName);
            this.Frame1.Controls.Add(this.Label12);
            this.Frame1.Controls.Add(this.Label11);
            this.Frame1.Controls.Add(this.Label10);
            this.Frame1.Location = new System.Drawing.Point(411, 285);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(489, 567);
            this.Frame1.TabIndex = 52;
            this.Frame1.Text = "Contractors";
            // 
            // txtContractorNote
            // 
            this.txtContractorNote.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorNote.Location = new System.Drawing.Point(113, 329);
            this.txtContractorNote.MaxLength = 255;
            this.txtContractorNote.Name = "txtContractorNote";
            this.txtContractorNote.Size = new System.Drawing.Size(357, 40);
            this.txtContractorNote.TabIndex = 16;
            // 
            // Picture1
            // 
            this.Picture1.BackColor = System.Drawing.SystemColors.Window;
            this.Picture1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Picture1.FillColor = -2147483643;
            this.Picture1.Image = ((System.Drawing.Image)(resources.GetObject("Picture1.Image")));
            this.Picture1.Location = new System.Drawing.Point(86, 376);
            this.Picture1.Name = "Picture1";
            this.Picture1.Size = new System.Drawing.Size(16, 16);
            this.ToolTip1.SetToolTip(this.Picture1, "Click to expand or hide contact information");
            this.Picture1.Click += new System.EventHandler(this.Picture1_Click);
            // 
            // txtContractorZip4
            // 
            this.txtContractorZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorZip4.Location = new System.Drawing.Point(406, 279);
            this.txtContractorZip4.Name = "txtContractorZip4";
            this.txtContractorZip4.Size = new System.Drawing.Size(64, 40);
            this.txtContractorZip4.TabIndex = 15;
            // 
            // txtContractorZip
            // 
            this.txtContractorZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorZip.Location = new System.Drawing.Point(319, 279);
            this.txtContractorZip.Name = "txtContractorZip";
            this.txtContractorZip.Size = new System.Drawing.Size(81, 40);
            this.txtContractorZip.TabIndex = 14;
            // 
            // txtContractorState
            // 
            this.txtContractorState.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorState.Location = new System.Drawing.Point(230, 279);
            this.txtContractorState.Name = "txtContractorState";
            this.txtContractorState.Size = new System.Drawing.Size(83, 40);
            this.txtContractorState.TabIndex = 13;
            // 
            // txtContractorEmail
            // 
            this.txtContractorEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorEmail.Location = new System.Drawing.Point(113, 404);
            this.txtContractorEmail.Name = "txtContractorEmail";
            this.txtContractorEmail.Size = new System.Drawing.Size(357, 40);
            this.txtContractorEmail.TabIndex = 17;
            this.txtContractorEmail.TabStop = false;
            // 
            // txtContractCity
            // 
            this.txtContractCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractCity.Location = new System.Drawing.Point(113, 279);
            this.txtContractCity.Name = "txtContractCity";
            this.txtContractCity.Size = new System.Drawing.Size(111, 40);
            this.txtContractCity.TabIndex = 12;
            // 
            // txtContractorAddr2
            // 
            this.txtContractorAddr2.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorAddr2.Location = new System.Drawing.Point(113, 229);
            this.txtContractorAddr2.Name = "txtContractorAddr2";
            this.txtContractorAddr2.Size = new System.Drawing.Size(357, 40);
            this.txtContractorAddr2.TabIndex = 11;
            // 
            // txtContractorAddr1
            // 
            this.txtContractorAddr1.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorAddr1.Location = new System.Drawing.Point(113, 179);
            this.txtContractorAddr1.Name = "txtContractorAddr1";
            this.txtContractorAddr1.Size = new System.Drawing.Size(357, 40);
            this.txtContractorAddr1.TabIndex = 10;
            // 
            // txtContractorName
            // 
            this.txtContractorName.BackColor = System.Drawing.SystemColors.Window;
            this.txtContractorName.Location = new System.Drawing.Point(113, 129);
            this.txtContractorName.Name = "txtContractorName";
            this.txtContractorName.Size = new System.Drawing.Size(357, 40);
            this.txtContractorName.TabIndex = 9;
            // 
            // cmbContractor
            // 
            this.cmbContractor.BackColor = System.Drawing.SystemColors.Window;
            this.cmbContractor.Location = new System.Drawing.Point(113, 79);
            this.cmbContractor.Name = "cmbContractor";
            this.cmbContractor.Size = new System.Drawing.Size(357, 40);
            this.cmbContractor.TabIndex = 8;
            this.cmbContractor.SelectedIndexChanged += new System.EventHandler(this.cmbContractor_SelectedIndexChanged);
            // 
            // GridContractorPhone
            // 
            this.GridContractorPhone.Cols = 4;
            this.GridContractorPhone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridContractorPhone.ExtendLastCol = true;
            this.GridContractorPhone.FixedCols = 0;
            this.GridContractorPhone.Location = new System.Drawing.Point(113, 460);
            this.GridContractorPhone.Name = "GridContractorPhone";
            this.GridContractorPhone.ReadOnly = false;
            this.GridContractorPhone.RowHeadersVisible = false;
            this.GridContractorPhone.Rows = 1;
            this.GridContractorPhone.ShowFocusCell = false;
            this.GridContractorPhone.Size = new System.Drawing.Size(357, 88);
            this.GridContractorPhone.TabIndex = 18;
            this.GridContractorPhone.TabStop = false;
            this.ToolTip1.SetToolTip(this.GridContractorPhone, "Use Insert and Delete to add and delete phone numbers");
            this.GridContractorPhone.CurrentCellChanged += new System.EventHandler(this.GridContractorPhone_RowColChange);
            this.GridContractorPhone.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridContractorPhone_MouseDownEvent);
            this.GridContractorPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.GridContractorPhone_KeyDownEvent);
            // 
            // cmdListContractor
            // 
            this.cmdListContractor.AppearanceKey = "actionButton";
            this.cmdListContractor.BackgroundImage = imageListEntry1.Image;
            this.cmdListContractor.Location = new System.Drawing.Point(30, 30);
            this.cmdListContractor.Name = "cmdListContractor";
            this.cmdListContractor.Size = new System.Drawing.Size(40, 40);
            this.cmdListContractor.TabIndex = 74;
            this.cmdListContractor.ToolTipText = "List of contractors for this permit";
            this.cmdListContractor.Click += new System.EventHandler(this.Toolbar1_ButtonClick_Wrapper);
            // 
            // cmdAddContractor
            // 
            this.cmdAddContractor.AppearanceKey = "actionButton";
            this.cmdAddContractor.BackgroundImage = imageListEntry2.Image;
            this.cmdAddContractor.Location = new System.Drawing.Point(80, 30);
            this.cmdAddContractor.Name = "cmdAddContractor";
            this.cmdAddContractor.Size = new System.Drawing.Size(40, 40);
            this.cmdAddContractor.TabIndex = 75;
            this.cmdAddContractor.ToolTipText = "Add Contractor to Permit";
            this.cmdAddContractor.Click += new System.EventHandler(this.Toolbar1_ButtonClick_Wrapper);
            // 
            // cmdDeleteContractor
            // 
            this.cmdDeleteContractor.AppearanceKey = "actionButton";
            this.cmdDeleteContractor.BackgroundImage = imageListEntry3.Image;
            this.cmdDeleteContractor.Location = new System.Drawing.Point(129, 30);
            this.cmdDeleteContractor.Name = "cmdDeleteContractor";
            this.cmdDeleteContractor.Size = new System.Drawing.Size(40, 40);
            this.cmdDeleteContractor.TabIndex = 76;
            this.cmdDeleteContractor.ToolTipText = "Delete Contractor from Permit";
            this.cmdDeleteContractor.Click += new System.EventHandler(this.Toolbar1_ButtonClick_Wrapper);
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(30, 340);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(63, 19);
            this.Label19.TabIndex = 74;
            this.Label19.Text = "NOTE";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(113, 379);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(148, 15);
            this.Label18.TabIndex = 72;
            this.Label18.Text = "CONTACT INFORMATION";
            this.ToolTip1.SetToolTip(this.Label18, "Click control to left to expand or hide contact information");
            // 
            // lblSuspended
            // 
            this.lblSuspended.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblSuspended.Location = new System.Drawing.Point(115, 134);
            this.lblSuspended.Name = "lblSuspended";
            this.lblSuspended.Size = new System.Drawing.Size(270, 23);
            this.lblSuspended.TabIndex = 65;
            this.lblSuspended.Text = "SUSPENDED";
            this.lblSuspended.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblSuspended.Visible = false;
            // 
            // lblContractorName
            // 
            this.lblContractorName.Location = new System.Drawing.Point(30, 143);
            this.lblContractorName.Name = "lblContractorName";
            this.lblContractorName.Size = new System.Drawing.Size(63, 19);
            this.lblContractorName.TabIndex = 57;
            this.lblContractorName.Text = "NAME";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(30, 418);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(63, 19);
            this.Label12.TabIndex = 56;
            this.Label12.Text = "E-MAIL";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(30, 295);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(80, 17);
            this.Label11.TabIndex = 55;
            this.Label11.Text = "CITY/ST/ZIP";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(30, 193);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(63, 19);
            this.Label10.TabIndex = 54;
            this.Label10.Text = "ADDRESS";
            // 
            // cmbApprovedDenied
            // 
            this.cmbApprovedDenied.BackColor = System.Drawing.SystemColors.Window;
            this.cmbApprovedDenied.Location = new System.Drawing.Point(172, 152);
            this.cmbApprovedDenied.Name = "cmbApprovedDenied";
            this.cmbApprovedDenied.Size = new System.Drawing.Size(355, 40);
            this.cmbApprovedDenied.TabIndex = 1;
            // 
            // txtIdentifier
            // 
            this.txtIdentifier.BackColor = System.Drawing.SystemColors.Window;
            this.txtIdentifier.Location = new System.Drawing.Point(170, 203);
            this.txtIdentifier.Name = "txtIdentifier";
            this.txtIdentifier.Size = new System.Drawing.Size(155, 40);
            this.txtIdentifier.TabIndex = 2;
            // 
            // t2kOccupancyDate
            // 
            this.t2kOccupancyDate.Location = new System.Drawing.Point(795, 1280);
            this.t2kOccupancyDate.Name = "t2kOccupancyDate";
            this.t2kOccupancyDate.Size = new System.Drawing.Size(118, 40);
            this.t2kOccupancyDate.TabIndex = 35;
            this.t2kOccupancyDate.Visible = false;
            // 
            // chkCertificateOfOccupancy
            // 
            this.chkCertificateOfOccupancy.Location = new System.Drawing.Point(695, 1243);
            this.chkCertificateOfOccupancy.Name = "chkCertificateOfOccupancy";
            this.chkCertificateOfOccupancy.Size = new System.Drawing.Size(277, 27);
            this.chkCertificateOfOccupancy.TabIndex = 34;
            this.chkCertificateOfOccupancy.Text = "Certificate of Occupancy Required";
            this.chkCertificateOfOccupancy.CheckedChanged += new System.EventHandler(this.chkCertificateOfOccupancy_CheckedChanged);
            // 
            // txtOMBCode
            // 
            this.txtOMBCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtOMBCode.Location = new System.Drawing.Point(795, 1193);
            this.txtOMBCode.Name = "txtOMBCode";
            this.txtOMBCode.Size = new System.Drawing.Size(105, 40);
            this.txtOMBCode.TabIndex = 33;
            // 
            // txtHousingUnits
            // 
            this.txtHousingUnits.BackColor = System.Drawing.SystemColors.Window;
            this.txtHousingUnits.Location = new System.Drawing.Point(795, 1143);
            this.txtHousingUnits.Name = "txtHousingUnits";
            this.txtHousingUnits.Size = new System.Drawing.Size(105, 40);
            this.txtHousingUnits.TabIndex = 32;
            // 
            // txtUnits
            // 
            this.txtUnits.BackColor = System.Drawing.SystemColors.Window;
            this.txtUnits.Location = new System.Drawing.Point(795, 1093);
            this.txtUnits.Name = "txtUnits";
            this.txtUnits.Size = new System.Drawing.Size(105, 40);
            this.txtUnits.TabIndex = 31;
            // 
            // txtBuildings
            // 
            this.txtBuildings.BackColor = System.Drawing.SystemColors.Window;
            this.txtBuildings.Location = new System.Drawing.Point(795, 1043);
            this.txtBuildings.Name = "txtBuildings";
            this.txtBuildings.Size = new System.Drawing.Size(105, 40);
            this.txtBuildings.TabIndex = 30;
            // 
            // txtPlan
            // 
            this.txtPlan.BackColor = System.Drawing.SystemColors.Window;
            this.txtPlan.Location = new System.Drawing.Point(795, 993);
            this.txtPlan.Name = "txtPlan";
            this.txtPlan.Size = new System.Drawing.Size(105, 40);
            this.txtPlan.TabIndex = 29;
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Controls.Add(this.SSTab1_Page5);
            this.SSTab1.Controls.Add(this.SSTab1_Page6);
            this.SSTab1.Location = new System.Drawing.Point(28, 990);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.SSTab1.Size = new System.Drawing.Size(644, 328);
            this.SSTab1.TabIndex = 20;
            this.SSTab1.TabStop = false;
            this.SSTab1.Text = "Checklist";
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.GridOpens);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Size = new System.Drawing.Size(642, 280);
            this.SSTab1_Page1.Text = "User Defined";
            // 
            // GridOpens
            // 
            this.GridOpens.Cols = 7;
            this.GridOpens.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridOpens.ExtendLastCol = true;
            this.GridOpens.FixedCols = 2;
            this.GridOpens.FrozenCols = 1;
            this.GridOpens.Location = new System.Drawing.Point(20, 20);
            this.GridOpens.Name = "GridOpens";
            this.GridOpens.ReadOnly = false;
            this.GridOpens.Rows = 1;
            this.GridOpens.ShowFocusCell = false;
            this.GridOpens.Size = new System.Drawing.Size(604, 238);
            this.GridOpens.TabIndex = 21;
            this.GridOpens.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridOpens_ValidateEdit);
            this.GridOpens.CurrentCellChanged += new System.EventHandler(this.GridOpens_RowColChange);
            this.GridOpens.Enter += new System.EventHandler(this.GridOpens_Enter);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.GridInspections);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Size = new System.Drawing.Size(642, 280);
            this.SSTab1_Page2.Text = "Inspections";
            // 
            // GridInspections
            // 
            this.GridInspections.Cols = 10;
            this.GridInspections.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridInspections.ExtendLastCol = true;
            this.GridInspections.Location = new System.Drawing.Point(20, 20);
            this.GridInspections.Name = "GridInspections";
            this.GridInspections.Rows = 1;
            this.GridInspections.ShowFocusCell = false;
            this.GridInspections.Size = new System.Drawing.Size(604, 239);
            this.GridInspections.TabIndex = 22;
            this.GridInspections.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridInspections_ValidateEdit);
            this.GridInspections.Enter += new System.EventHandler(this.GridInspections_Enter);
            this.GridInspections.DoubleClick += new System.EventHandler(this.GridInspections_DblClick);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.rtbDescription);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Size = new System.Drawing.Size(642, 280);
            this.SSTab1_Page3.Text = "Description";
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(20, 20);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(604, 239);
            this.rtbDescription.TabIndex = 23;
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.gridActivity);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Size = new System.Drawing.Size(642, 280);
            this.SSTab1_Page4.Text = "Activity";
            // 
            // gridActivity
            // 
            this.gridActivity.Cols = 4;
            this.gridActivity.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.gridActivity.ExtendLastCol = true;
            this.gridActivity.Location = new System.Drawing.Point(20, 20);
            this.gridActivity.Name = "gridActivity";
            this.gridActivity.Rows = 1;
            this.gridActivity.ShowFocusCell = false;
            this.gridActivity.Size = new System.Drawing.Size(604, 239);
            this.gridActivity.TabIndex = 24;
            this.gridActivity.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.gridActivity_MouseDownEvent);
            this.gridActivity.Enter += new System.EventHandler(this.gridActivity_Enter);
            this.gridActivity.DoubleClick += new System.EventHandler(this.gridActivity_DblClick);
            this.gridActivity.KeyDown += new Wisej.Web.KeyEventHandler(this.gridActivity_KeyDownEvent);
            // 
            // SSTab1_Page5
            // 
            this.SSTab1_Page5.Controls.Add(this.gridViolations);
            this.SSTab1_Page5.Controls.Add(this.cmbViolations);
            this.SSTab1_Page5.Controls.Add(this.lblViolations);
            this.SSTab1_Page5.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page5.Name = "SSTab1_Page5";
            this.SSTab1_Page5.Size = new System.Drawing.Size(642, 280);
            this.SSTab1_Page5.Text = "Violations";
            // 
            // gridViolations
            // 
            this.gridViolations.Cols = 5;
            this.gridViolations.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.gridViolations.ExtendLastCol = true;
            this.gridViolations.FixedCols = 0;
            this.gridViolations.Location = new System.Drawing.Point(20, 70);
            this.gridViolations.Name = "gridViolations";
            this.gridViolations.RowHeadersVisible = false;
            this.gridViolations.Rows = 1;
            this.gridViolations.ShowFocusCell = false;
            this.gridViolations.Size = new System.Drawing.Size(604, 191);
            this.gridViolations.TabIndex = 27;
            this.gridViolations.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.gridViolations_MouseDownEvent);
            this.gridViolations.DoubleClick += new System.EventHandler(this.gridViolations_DblClick);
            // 
            // SSTab1_Page6
            // 
            this.SSTab1_Page6.Controls.Add(this.GridChecklist);
            this.SSTab1_Page6.Location = new System.Drawing.Point(1, 47);
            this.SSTab1_Page6.Name = "SSTab1_Page6";
            this.SSTab1_Page6.Size = new System.Drawing.Size(642, 280);
            this.SSTab1_Page6.Text = "Checklist";
            // 
            // GridChecklist
            // 
            this.GridChecklist.AutoSizeColumnsMode = Wisej.Web.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.GridChecklist.Cols = 7;
            this.GridChecklist.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridChecklist.ExtendLastCol = true;
            this.GridChecklist.Location = new System.Drawing.Point(20, 20);
            this.GridChecklist.Name = "GridChecklist";
            this.GridChecklist.ReadOnly = false;
            this.GridChecklist.Rows = 1;
            this.GridChecklist.ShowFocusCell = false;
            this.GridChecklist.Size = new System.Drawing.Size(608, 246);
            this.GridChecklist.StandardTab = false;
            this.GridChecklist.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridChecklist.TabIndex = 28;
            this.GridChecklist.Tag = "-1";
            this.GridChecklist.CurrentCellChanged += new System.EventHandler(this.GridChecklist_RowColChange);
            this.GridChecklist.Enter += new System.EventHandler(this.GridChecklist_Enter);
            // 
            // GridDeletedPhones
            // 
            this.GridDeletedPhones.ColumnHeadersVisible = false;
            this.GridDeletedPhones.FixedRows = 0;
            this.GridDeletedPhones.Location = new System.Drawing.Point(592, 67);
            this.GridDeletedPhones.Name = "GridDeletedPhones";
            this.GridDeletedPhones.Rows = 0;
            this.GridDeletedPhones.ShowFocusCell = false;
            this.GridDeletedPhones.Size = new System.Drawing.Size(43, 19);
            this.GridDeletedPhones.TabIndex = 53;
            this.GridDeletedPhones.Visible = false;
            // 
            // GridEstimate
            // 
            this.GridEstimate.Cols = 7;
            this.GridEstimate.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridEstimate.ExtendLastCol = true;
            this.GridEstimate.FixedCols = 0;
            this.GridEstimate.Location = new System.Drawing.Point(29, 862);
            this.GridEstimate.Name = "GridEstimate";
            this.GridEstimate.ReadOnly = false;
            this.GridEstimate.RowHeadersVisible = false;
            this.GridEstimate.ShowFocusCell = false;
            this.GridEstimate.Size = new System.Drawing.Size(871, 118);
            this.GridEstimate.StandardTab = false;
            this.GridEstimate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridEstimate.TabIndex = 19;
            this.GridEstimate.ComboCloseUp += new System.EventHandler(this.GridEstimate_ComboCloseUp);
            this.GridEstimate.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridEstimate_CellChanged);
            this.GridEstimate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridEstimate_ValidateEdit);
            this.GridEstimate.CurrentCellChanged += new System.EventHandler(this.GridEstimate_RowColChange);
            // 
            // framApplication
            // 
            this.framApplication.Controls.Add(this.txtFiledBy);
            this.framApplication.Controls.Add(this.t2kApplicationDate);
            this.framApplication.Controls.Add(this.Label9);
            this.framApplication.Controls.Add(this.Label8);
            this.framApplication.Location = new System.Drawing.Point(30, 285);
            this.framApplication.Name = "framApplication";
            this.framApplication.Size = new System.Drawing.Size(361, 142);
            this.framApplication.TabIndex = 48;
            this.framApplication.Text = "Application";
            // 
            // txtFiledBy
            // 
            this.txtFiledBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtFiledBy.Location = new System.Drawing.Point(90, 80);
            this.txtFiledBy.Name = "txtFiledBy";
            this.txtFiledBy.Size = new System.Drawing.Size(251, 40);
            this.txtFiledBy.TabIndex = 4;
            // 
            // t2kApplicationDate
            // 
            this.t2kApplicationDate.Location = new System.Drawing.Point(90, 30);
            this.t2kApplicationDate.Mask = "##/##/####";
            this.t2kApplicationDate.Name = "t2kApplicationDate";
            this.t2kApplicationDate.Size = new System.Drawing.Size(117, 40);
            this.t2kApplicationDate.TabIndex = 3;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 94);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(40, 18);
            this.Label9.TabIndex = 50;
            this.Label9.Text = "FILER";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(33, 21);
            this.Label8.TabIndex = 49;
            this.Label8.Text = "DATE";
            // 
            // framContact
            // 
            this.framContact.Controls.Add(this.txtContactName);
            this.framContact.Controls.Add(this.txtEmail);
            this.framContact.Controls.Add(this.GridPhones);
            this.framContact.Controls.Add(this.Label6);
            this.framContact.Controls.Add(this.Label1_10);
            this.framContact.Location = new System.Drawing.Point(30, 447);
            this.framContact.Name = "framContact";
            this.framContact.Size = new System.Drawing.Size(361, 259);
            this.framContact.TabIndex = 46;
            this.framContact.Text = "Contact Information";
            // 
            // txtContactName
            // 
            this.txtContactName.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactName.Location = new System.Drawing.Point(90, 30);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(251, 40);
            this.txtContactName.TabIndex = 5;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(90, 80);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(251, 40);
            this.txtEmail.TabIndex = 6;
            // 
            // GridPhones
            // 
            this.GridPhones.Cols = 4;
            this.GridPhones.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridPhones.ExtendLastCol = true;
            this.GridPhones.FixedCols = 0;
            this.GridPhones.Location = new System.Drawing.Point(20, 130);
            this.GridPhones.Name = "GridPhones";
            this.GridPhones.ReadOnly = false;
            this.GridPhones.RowHeadersVisible = false;
            this.GridPhones.Rows = 1;
            this.GridPhones.ShowFocusCell = false;
            this.GridPhones.Size = new System.Drawing.Size(321, 111);
            this.GridPhones.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.GridPhones, "Use Insert and Delete to add and delete phone numbers");
            this.GridPhones.CurrentCellChanged += new System.EventHandler(this.GridPhones_RowColChange);
            this.GridPhones.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridPhones_MouseDownEvent);
            this.GridPhones.KeyDown += new Wisej.Web.KeyEventHandler(this.GridPhones_KeyDownEvent);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(40, 18);
            this.Label6.TabIndex = 51;
            this.Label6.Text = "NAME";
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(20, 94);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(40, 18);
            this.Label1_10.TabIndex = 47;
            this.Label1_10.Text = "E-MAIL";
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Location = new System.Drawing.Point(173, 102);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(152, 40);
            this.txtYear.TabIndex = 54;
            // 
            // lblDesig
            // 
            this.lblDesig.Location = new System.Drawing.Point(30, 52);
            this.lblDesig.Name = "lblDesig";
            this.lblDesig.Size = new System.Drawing.Size(128, 13);
            this.lblDesig.TabIndex = 71;
            this.lblDesig.Visible = false;
            // 
            // lblLastName
            // 
            this.lblLastName.Location = new System.Drawing.Point(30, 77);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(103, 13);
            this.lblLastName.TabIndex = 70;
            this.lblLastName.Visible = false;
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.Location = new System.Drawing.Point(30, 59);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(103, 13);
            this.lblMiddleName.TabIndex = 69;
            this.lblMiddleName.Visible = false;
            // 
            // lblFirstName
            // 
            this.lblFirstName.Location = new System.Drawing.Point(222, 59);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(133, 13);
            this.lblFirstName.TabIndex = 68;
            this.lblFirstName.Visible = false;
            // 
            // lblStreetNum
            // 
            this.lblStreetNum.Location = new System.Drawing.Point(490, 59);
            this.lblStreetNum.Name = "lblStreetNum";
            this.lblStreetNum.Size = new System.Drawing.Size(43, 15);
            this.lblStreetNum.TabIndex = 67;
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(30, 217);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(112, 15);
            this.Label17.TabIndex = 66;
            this.Label17.Text = "PERMIT IDENTIFIER";
            // 
            // ImgComment
            // 
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(693, 46);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Size = new System.Drawing.Size(17, 18);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ImgComment.Click += new System.EventHandler(this.ImgComment_Click);
            // 
            // lblOccupancyDate
            // 
            this.lblOccupancyDate.Location = new System.Drawing.Point(695, 1294);
            this.lblOccupancyDate.Name = "lblOccupancyDate";
            this.lblOccupancyDate.Size = new System.Drawing.Size(90, 18);
            this.lblOccupancyDate.TabIndex = 64;
            this.lblOccupancyDate.Text = "DATE ISSUED";
            this.lblOccupancyDate.Visible = false;
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(695, 1207);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(64, 15);
            this.Label16.TabIndex = 63;
            this.Label16.Text = "OMB CODE";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(695, 1157);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(53, 15);
            this.Label15.TabIndex = 62;
            this.Label15.Text = "H. UNITS";
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(695, 1107);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(46, 15);
            this.Label14.TabIndex = 61;
            this.Label14.Text = "UNITS";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(695, 1057);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(70, 15);
            this.Label13.TabIndex = 60;
            this.Label13.Text = "BUILDINGS";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(695, 1007);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(46, 14);
            this.Label4.TabIndex = 59;
            this.Label4.Text = "PLAN";
            // 
            // lblPermitNumber
            // 
            this.lblPermitNumber.Location = new System.Drawing.Point(173, 116);
            this.lblPermitNumber.Name = "lblPermitNumber";
            this.lblPermitNumber.Size = new System.Drawing.Size(61, 15);
            this.lblPermitNumber.TabIndex = 58;
            this.lblPermitNumber.Visible = false;
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(30, 166);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(48, 15);
            this.Label7.TabIndex = 45;
            this.Label7.Text = "STATUS";
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(30, 116);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(70, 15);
            this.lblYear.TabIndex = 44;
            this.lblYear.Text = "YEAR";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(490, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(195, 15);
            this.lblName.TabIndex = 43;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(402, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(48, 15);
            this.Label5.TabIndex = 42;
            this.Label5.Text = "NAME";
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(553, 59);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(190, 15);
            this.lblLocation.TabIndex = 41;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(402, 59);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(70, 15);
            this.Label3.TabIndex = 40;
            this.Label3.Text = "LOCATION";
            // 
            // lblMapLot
            // 
            this.lblMapLot.Location = new System.Drawing.Point(248, 30);
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Size = new System.Drawing.Size(133, 15);
            this.lblMapLot.TabIndex = 39;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(170, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(56, 15);
            this.Label2.TabIndex = 38;
            this.Label2.Text = "MAP/LOT";
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(107, 30);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(48, 15);
            this.lblAccount.TabIndex = 37;
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(30, 30);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(70, 15);
            this.Label1_0.TabIndex = 36;
            this.Label1_0.Text = "ACCOUNT";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintActivity,
            this.mnuPrintActivityWithDetail,
            this.mnuPrintViolations,
            this.mnuPrintPermit,
            this.mnuPrintCustomForm});
            this.MainMenu1.Name = null;
            // 
            // mnuPrintActivity
            // 
            this.mnuPrintActivity.Index = 0;
            this.mnuPrintActivity.Name = "mnuPrintActivity";
            this.mnuPrintActivity.Text = "Print Activity";
            this.mnuPrintActivity.Click += new System.EventHandler(this.mnuPrintActivity_Click);
            // 
            // mnuPrintActivityWithDetail
            // 
            this.mnuPrintActivityWithDetail.Index = 1;
            this.mnuPrintActivityWithDetail.Name = "mnuPrintActivityWithDetail";
            this.mnuPrintActivityWithDetail.Text = "Print Activity With Detail";
            this.mnuPrintActivityWithDetail.Click += new System.EventHandler(this.mnuPrintActivityWithDetail_Click);
            // 
            // mnuPrintViolations
            // 
            this.mnuPrintViolations.Index = 2;
            this.mnuPrintViolations.Name = "mnuPrintViolations";
            this.mnuPrintViolations.Text = "Print Violations";
            this.mnuPrintViolations.Click += new System.EventHandler(this.mnuPrintViolations_Click);
            // 
            // mnuPrintPermit
            // 
            this.mnuPrintPermit.Index = 3;
            this.mnuPrintPermit.Name = "mnuPrintPermit";
            this.mnuPrintPermit.Text = "Print Permit";
            this.mnuPrintPermit.Click += new System.EventHandler(this.mnuPrintPermit_Click);
            // 
            // mnuPrintCustomForm
            // 
            this.mnuPrintCustomForm.Index = 4;
            this.mnuPrintCustomForm.Name = "mnuPrintCustomForm";
            this.mnuPrintCustomForm.Text = "Print Custom Form";
            this.mnuPrintCustomForm.Click += new System.EventHandler(this.mnuPrintCustomForm_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSepar5
            // 
            this.mnuSepar5.Index = -1;
            this.mnuSepar5.Name = "mnuSepar5";
            this.mnuSepar5.Text = "-";
            // 
            // mnuAddInspection
            // 
            this.mnuAddInspection.Index = -1;
            this.mnuAddInspection.Name = "mnuAddInspection";
            this.mnuAddInspection.Text = "Add Inspection";
            this.mnuAddInspection.Click += new System.EventHandler(this.mnuAddInspection_Click);
            // 
            // mnuAddActivity
            // 
            this.mnuAddActivity.Index = -1;
            this.mnuAddActivity.Name = "mnuAddActivity";
            this.mnuAddActivity.Text = "Add Activity";
            this.mnuAddActivity.Click += new System.EventHandler(this.mnuAddActivity_Click);
            // 
            // mnuAddViolation
            // 
            this.mnuAddViolation.Index = -1;
            this.mnuAddViolation.Name = "mnuAddViolation";
            this.mnuAddViolation.Text = "Add Violation";
            this.mnuAddViolation.Click += new System.EventHandler(this.mnuAddViolation_Click);
            // 
            // mnuComment
            // 
            this.mnuComment.Index = -1;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comment";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // mnuMove
            // 
            this.mnuMove.Index = -1;
            this.mnuMove.Name = "mnuMove";
            this.mnuMove.Text = "Move Permit to Another Account";
            this.mnuMove.Click += new System.EventHandler(this.mnuMove_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = -1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuDeletePermit
            // 
            this.mnuDeletePermit.Index = -1;
            this.mnuDeletePermit.Name = "mnuDeletePermit";
            this.mnuDeletePermit.Text = "Delete Permit";
            this.mnuDeletePermit.Click += new System.EventHandler(this.mnuDeletePermit_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAddInspection
            // 
            this.cmdAddInspection.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddInspection.Location = new System.Drawing.Point(268, 29);
            this.cmdAddInspection.Name = "cmdAddInspection";
            this.cmdAddInspection.Size = new System.Drawing.Size(111, 24);
            this.cmdAddInspection.TabIndex = 1;
            this.cmdAddInspection.Text = "Add Inspection";
            this.cmdAddInspection.Click += new System.EventHandler(this.mnuAddInspection_Click);
            // 
            // cmdAddActivity
            // 
            this.cmdAddActivity.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddActivity.Location = new System.Drawing.Point(385, 29);
            this.cmdAddActivity.Name = "cmdAddActivity";
            this.cmdAddActivity.Size = new System.Drawing.Size(93, 24);
            this.cmdAddActivity.TabIndex = 2;
            this.cmdAddActivity.Text = "Add Activity";
            this.cmdAddActivity.Click += new System.EventHandler(this.mnuAddActivity_Click);
            // 
            // cmdAddViolation
            // 
            this.cmdAddViolation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddViolation.Location = new System.Drawing.Point(484, 29);
            this.cmdAddViolation.Name = "cmdAddViolation";
            this.cmdAddViolation.Size = new System.Drawing.Size(100, 24);
            this.cmdAddViolation.TabIndex = 3;
            this.cmdAddViolation.Text = "Add Violation";
            this.cmdAddViolation.Click += new System.EventHandler(this.mnuAddViolation_Click);
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.Location = new System.Drawing.Point(590, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComment.Size = new System.Drawing.Size(78, 24);
            this.cmdComment.TabIndex = 4;
            this.cmdComment.Text = "Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // cmdMovePermit
            // 
            this.cmdMovePermit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdMovePermit.Location = new System.Drawing.Point(674, 29);
            this.cmdMovePermit.Name = "cmdMovePermit";
            this.cmdMovePermit.Size = new System.Drawing.Size(218, 24);
            this.cmdMovePermit.TabIndex = 5;
            this.cmdMovePermit.Text = "Move Permit to Another Account";
            this.cmdMovePermit.Click += new System.EventHandler(this.mnuMove_Click);
            // 
            // cmdDeletePermit
            // 
            this.cmdDeletePermit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeletePermit.Location = new System.Drawing.Point(898, 29);
            this.cmdDeletePermit.Name = "cmdDeletePermit";
            this.cmdDeletePermit.Size = new System.Drawing.Size(102, 24);
            this.cmdDeletePermit.TabIndex = 6;
            this.cmdDeletePermit.Text = "Delete Permit";
            this.cmdDeletePermit.Click += new System.EventHandler(this.mnuDeletePermit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(413, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(79, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmPermits
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1028, 663);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmPermits";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Permits";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmPermits_Load);
            this.Resize += new System.EventHandler(this.frmPermits_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPermits_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridContractorPhone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdListContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteContractor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kOccupancyDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCertificateOfOccupancy)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridInspections)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            this.SSTab1_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbDescription)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridActivity)).EndInit();
            this.SSTab1_Page5.ResumeLayout(false);
            this.SSTab1_Page5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViolations)).EndInit();
            this.SSTab1_Page6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridChecklist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridEstimate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framApplication)).EndInit();
            this.framApplication.ResumeLayout(false);
            this.framApplication.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kApplicationDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framContact)).EndInit();
            this.framContact.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridPhones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddInspection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddViolation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMovePermit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeletePermit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdAddViolation;
		private FCButton cmdAddActivity;
		private FCButton cmdAddInspection;
		private FCButton cmdDeletePermit;
		private FCButton cmdMovePermit;
		private FCButton cmdComment;
		private FCButton cmdSave;
	}
}
