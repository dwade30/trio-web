﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsCustomReportItemList
	{
		//=========================================================
		private FCCollection ReportItemList = new FCCollection();
		private int intCurrentIndex;

		public void Clear()
		{
			int x;
			for (x = 0; x <= ReportItemList.Count - 1; x++)
			{
				ReportItemList.Remove(0);
			}
			// x
			intCurrentIndex = -1;
		}

		public void InsertReportItem(clsCustomReportItem pRepItem)
		{
			int x;
			bool boolAdded;
			clsCustomReportItem tRepItem;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolAdded = false;
				for (x = 1; x <= ReportItemList.Count; x++)
				{
					if (!(ReportItemList[x] == null))
					{
						tRepItem = ReportItemList[x];
						if (pRepItem.Row < tRepItem.Row || (pRepItem.Row == tRepItem.Row && pRepItem.Col < tRepItem.Col))
						{
							ReportItemList.Add(pRepItem, null, x);
							boolAdded = true;
							return;
						}
					}
				}
				// x
				if (!boolAdded)
				{
					ReportItemList.Add(pRepItem);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertReportItem", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intCodeType As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngOpenCode As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intTotalOption As int	OnWriteFCConvert.ToDouble(
		public void AddItem(int intRow, int intCol, int intCodeType, int lngOpenCode, int intTotalOption)
		{
			clsCustomReportItem tRep = new clsCustomReportItem();
			tRep.Row = intRow;
			tRep.Col = intCol;
			tRep.OpenCode = lngOpenCode;
			tRep.CodeType = intCodeType;
			tRep.TotalOption = intTotalOption;
			tRep.TypeOfData = FCConvert.ToInt16(tRep.Get_TypeFromCodes(intCodeType, lngOpenCode));
			InsertReportItem(tRep);
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsCustomReportItem GetCurrentReportItem
		{
			get
			{
				clsCustomReportItem GetCurrentReportItem = null;
				clsCustomReportItem tRep;
				tRep = null;
				if (!FCUtils.IsEmpty(ReportItemList))
				{
					if (intCurrentIndex > 0)
					{
						if (!(ReportItemList[intCurrentIndex] == null))
						{
							tRep = ReportItemList[intCurrentIndex];
						}
					}
				}
				GetCurrentReportItem = tRep;
				return GetCurrentReportItem;
			}
		}

		public clsCustomReportItem Get_GetItemByRowCol(int intRow, int intCol)
		{
			clsCustomReportItem GetItemByRowCol = null;
			clsCustomReportItem tRep;
			tRep = null;
			if (!FCUtils.IsEmpty(ReportItemList))
			{
				int x;
				for (x = 0; x <= ReportItemList.Count - 1; x++)
				{
					if (!(ReportItemList[x] == null))
					{
						if (ReportItemList[x].Row == intRow && ReportItemList[x].Col == intCol)
						{
							tRep = ReportItemList[x];
							intCurrentIndex = x;
							break;
						}
					}
				}
				// x
			}
			GetItemByRowCol = tRep;
			return GetItemByRowCol;
		}

		public void MoveFirst()
		{
			if (!(ReportItemList == null))
			{
				if (!FCUtils.IsEmpty(ReportItemList))
				{
					if (ReportItemList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(ReportItemList))
			{
				if (intCurrentIndex > ReportItemList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= ReportItemList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > ReportItemList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (ReportItemList[intCurrentIndex] == null)
						{
							// ElseIf ReportItemList[intCurrentIndex].Unused Then
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}
	}
}
