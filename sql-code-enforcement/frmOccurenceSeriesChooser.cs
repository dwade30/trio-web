﻿//Fecher vbPorter - Version 1.0.0.59
using Global;
using fecherFoundation;

namespace TWCE0000
{
	public partial class frmOccurrenceSeriesChooser : BaseForm
	{
		public frmOccurrenceSeriesChooser()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmOccurrenceSeriesChooser InstancePtr
		{
			get
			{
				return (frmOccurrenceSeriesChooser)Sys.GetInstance(typeof(frmOccurrenceSeriesChooser));
			}
		}

		protected frmOccurrenceSeriesChooser _InstancePtr = null;
		//=========================================================
		public string m_strEventSubject = "";
		public bool m_bOcurrence;
		public bool m_bDeleteRequest;
		public bool m_bOK;

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			m_bOK = false;
			Close();
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			m_bOK = true;
			m_bOcurrence = cmbOccurrence.Text == "Open the Occurence";
			Close();
		}

		private void Form_Initialize()
		{
			m_bDeleteRequest = false;
			m_bOcurrence = true;
		}

		private void frmOccurrenceSeriesChooser_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOccurrenceSeriesChooser properties;
			//frmOccurrenceSeriesChooser.FillStyle	= 0;
			//frmOccurrenceSeriesChooser.ScaleWidth	= 5880;
			//frmOccurrenceSeriesChooser.ScaleHeight	= 3810;
			//frmOccurrenceSeriesChooser.LinkTopic	= "Form2";
			//frmOccurrenceSeriesChooser.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			if (m_bDeleteRequest)
			{
				txtAction.Text = "Do you want to delete only this occurrence or the series?";
				if (cmbOccurrence.Items.Contains("Open the Occurence"))
				{
					cmbOccurrence.Items.Remove("Open the Occurence");
					cmbOccurrence.Items.Add("Delete the Occurrence");
				}
				if (cmbOccurrence.Items.Contains("Open the Series"))
				{
					cmbOccurrence.Items.Remove("Open the Series");
					cmbOccurrence.Items.Add("Delete the Series");
				}
			}
			else
			{
				txtAction.Text = "Do you want to open only this occurrence or the series?";
				if (cmbOccurrence.Items.Contains("Delete the Occurence"))
				{
					cmbOccurrence.Items.Remove("Delete the Occurence");
					cmbOccurrence.Items.Add("Open the Occurrence");
				}
				if (cmbOccurrence.Items.Contains("Delete the Series"))
				{
					cmbOccurrence.Items.Remove("Delete the Series");
					cmbOccurrence.Items.Add("Open the Series");
				}
			}
			m_bOK = false;
			txtDescription.Text = "'" + m_strEventSubject + "'" + " is a recurring event.";
			if (m_bOcurrence)
			{
				cmbOccurrence.SelectedIndex = 0;
			}
			else
			{
				cmbOccurrence.SelectedIndex = 1;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
