//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmViolation.
	/// </summary>
	partial class frmViolation
	{
		public fecherFoundation.FCTextBox txtFee;
		public fecherFoundation.FCFrame framContact;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txtContactName;
		public FCGrid GridPhones;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCTextBox txtDoingBusinessAs;
		public fecherFoundation.FCTextBox txtDesig;
		public fecherFoundation.FCTextBox txtLast;
		public fecherFoundation.FCTextBox txtMiddle;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtStreet;
		public fecherFoundation.FCTextBox txtApt;
		public fecherFoundation.FCTextBox txtStreetNum;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtBusinessLicense;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid GridOpens;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public fecherFoundation.FCRichTextBox rtbDetail;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public FCGrid gridActivity;
		public fecherFoundation.FCTabPage SSTab1_Page5;
		public FCGrid gridDocuments;
		public fecherFoundation.FCTextBox txtIdentifier;
		public fecherFoundation.FCComboBox cmbStatus;
		public fecherFoundation.FCComboBox cmbViolationType;
		public fecherFoundation.FCTextBox txtDescription;
		public Global.T2KDateBox t2kDateIssued;
		public Global.T2KDateBox T2KDateOccurred;
		public Global.T2KDateBox T2KDateResolved;
		public FCGrid GridDeletedPhones;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel lblPermit;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblPermitLabel;
		public fecherFoundation.FCLabel lblAccountLabel;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCButton cmdAttachDoc;
		public fecherFoundation.FCButton cmdRemoveDocument;
		public fecherFoundation.FCButton cmdViewDocuments;
		public fecherFoundation.FCButton cmdAddActivity;
		public fecherFoundation.FCButton cmdDeleteViolation;
		public fecherFoundation.FCButton cmdSave;
        private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuPrintActivity;
        public fecherFoundation.FCToolStripMenuItem mnuPrintActivityWithDetail;
        public fecherFoundation.FCToolStripMenuItem mnuPrintViolation;
        private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
            this.txtFee = new fecherFoundation.FCTextBox();
            this.framContact = new fecherFoundation.FCFrame();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.txtContactName = new fecherFoundation.FCTextBox();
            this.GridPhones = new fecherFoundation.FCGrid();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label1_6 = new fecherFoundation.FCLabel();
            this.Label1_5 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.txtDoingBusinessAs = new fecherFoundation.FCTextBox();
            this.txtDesig = new fecherFoundation.FCTextBox();
            this.txtLast = new fecherFoundation.FCTextBox();
            this.txtMiddle = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.txtStreet = new fecherFoundation.FCTextBox();
            this.txtApt = new fecherFoundation.FCTextBox();
            this.txtStreetNum = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtBusinessLicense = new fecherFoundation.FCTextBox();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.GridOpens = new fecherFoundation.FCGrid();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.rtbDetail = new fecherFoundation.FCRichTextBox();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.gridActivity = new fecherFoundation.FCGrid();
            this.SSTab1_Page5 = new fecherFoundation.FCTabPage();
            this.gridDocuments = new fecherFoundation.FCGrid();
            this.txtIdentifier = new fecherFoundation.FCTextBox();
            this.cmbStatus = new fecherFoundation.FCComboBox();
            this.cmbViolationType = new fecherFoundation.FCComboBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.t2kDateIssued = new Global.T2KDateBox();
            this.T2KDateOccurred = new Global.T2KDateBox();
            this.T2KDateResolved = new Global.T2KDateBox();
            this.GridDeletedPhones = new fecherFoundation.FCGrid();
            this.Label10 = new fecherFoundation.FCLabel();
            this.lblPermit = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblPermitLabel = new fecherFoundation.FCLabel();
            this.lblAccountLabel = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.cmdAttachDoc = new fecherFoundation.FCButton();
            this.cmdRemoveDocument = new fecherFoundation.FCButton();
            this.cmdViewDocuments = new fecherFoundation.FCButton();
            this.cmdAddActivity = new fecherFoundation.FCButton();
            this.cmdDeleteViolation = new fecherFoundation.FCButton();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuPrintActivity = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintActivityWithDetail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintViolation = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framContact)).BeginInit();
            this.framContact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPhones)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbDetail)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridActivity)).BeginInit();
            this.SSTab1_Page5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateIssued)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateOccurred)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateResolved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAttachDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdViewDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteViolation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtFee);
            this.ClientArea.Controls.Add(this.framContact);
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.txtIdentifier);
            this.ClientArea.Controls.Add(this.cmbStatus);
            this.ClientArea.Controls.Add(this.cmbViolationType);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.t2kDateIssued);
            this.ClientArea.Controls.Add(this.T2KDateOccurred);
            this.ClientArea.Controls.Add(this.T2KDateResolved);
            this.ClientArea.Controls.Add(this.GridDeletedPhones);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.lblPermit);
            this.ClientArea.Controls.Add(this.lblAccount);
            this.ClientArea.Controls.Add(this.lblPermitLabel);
            this.ClientArea.Controls.Add(this.lblAccountLabel);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label1_0);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Size = new System.Drawing.Size(1078, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAttachDoc);
            this.TopPanel.Controls.Add(this.cmdRemoveDocument);
            this.TopPanel.Controls.Add(this.cmdViewDocuments);
            this.TopPanel.Controls.Add(this.cmdAddActivity);
            this.TopPanel.Controls.Add(this.cmdDeleteViolation);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteViolation, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddActivity, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdViewDocuments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemoveDocument, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAttachDoc, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(107, 30);
            this.HeaderText.Text = "Violation";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // txtFee
            // 
			this.txtFee.MaxLength = 254;
            this.txtFee.AutoSize = false;
            this.txtFee.BackColor = System.Drawing.SystemColors.Window;
            this.txtFee.LinkItem = null;
            this.txtFee.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFee.LinkTopic = null;
            this.txtFee.Location = new System.Drawing.Point(31, 167);
            this.txtFee.Name = "txtFee";
            this.txtFee.Size = new System.Drawing.Size(143, 40);
            this.txtFee.TabIndex = 3;
            this.txtFee.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtFee, null);
            // 
            // framContact
            // 
            this.framContact.Controls.Add(this.txtEmail);
            this.framContact.Controls.Add(this.txtContactName);
            this.framContact.Controls.Add(this.GridPhones);
            this.framContact.Controls.Add(this.Label1_10);
            this.framContact.Controls.Add(this.Label4);
            this.framContact.Location = new System.Drawing.Point(633, 30);
            this.framContact.Name = "framContact";
            this.framContact.Size = new System.Drawing.Size(399, 254);
            this.framContact.TabIndex = 34;
            this.framContact.Text = "Contact Information";
            this.ToolTip1.SetToolTip(this.framContact, null);
            // 
            // txtEmail
            // 
            this.txtEmail.AutoSize = false;
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.LinkItem = null;
            this.txtEmail.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtEmail.LinkTopic = null;
            this.txtEmail.Location = new System.Drawing.Point(124, 90);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(256, 40);
            this.txtEmail.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtEmail, null);
            // 
            // txtContactName
            // 
            this.txtContactName.AutoSize = false;
            this.txtContactName.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactName.LinkItem = null;
            this.txtContactName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtContactName.LinkTopic = null;
            this.txtContactName.Location = new System.Drawing.Point(124, 30);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(256, 40);
            this.txtContactName.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtContactName, null);
            // 
            // GridPhones
            // 
            this.GridPhones.AllowSelection = false;
            this.GridPhones.AllowUserToResizeColumns = false;
            this.GridPhones.AllowUserToResizeRows = false;
            this.GridPhones.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridPhones.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridPhones.BackColorBkg = System.Drawing.Color.Empty;
            this.GridPhones.BackColorFixed = System.Drawing.Color.Empty;
            this.GridPhones.BackColorSel = System.Drawing.Color.Empty;
            this.GridPhones.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridPhones.Cols = 4;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridPhones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridPhones.ColumnHeadersHeight = 30;
            this.GridPhones.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridPhones.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridPhones.DragIcon = null;
            this.GridPhones.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridPhones.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridPhones.ExtendLastCol = true;
            this.GridPhones.FixedCols = 0;
            this.GridPhones.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridPhones.FrozenCols = 0;
            this.GridPhones.GridColor = System.Drawing.Color.Empty;
            this.GridPhones.GridColorFixed = System.Drawing.Color.Empty;
            this.GridPhones.Location = new System.Drawing.Point(20, 150);
            this.GridPhones.Name = "GridPhones";
            this.GridPhones.OutlineCol = 0;
            this.GridPhones.RowHeadersVisible = false;
            this.GridPhones.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridPhones.RowHeightMin = 0;
            this.GridPhones.Rows = 1;
            this.GridPhones.ScrollTipText = null;
            this.GridPhones.ShowColumnVisibilityMenu = false;
            this.GridPhones.ShowFocusCell = false;
            this.GridPhones.Size = new System.Drawing.Size(360, 85);
            this.GridPhones.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridPhones.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridPhones.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.GridPhones, "Use Insert and Delete to add and delete phone numbers");
            this.GridPhones.CurrentCellChanged += new System.EventHandler(this.GridPhones_RowColChange);
            this.GridPhones.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridPhones_MouseDownEvent);
            this.GridPhones.KeyDown += new Wisej.Web.KeyEventHandler(this.GridPhones_KeyDownEvent);
            // 
            // Label1_10
            // 
            this.Label1_10.Location = new System.Drawing.Point(20, 104);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(38, 18);
            this.Label1_10.TabIndex = 36;
            this.Label1_10.Text = "E-MAIL";
            this.ToolTip1.SetToolTip(this.Label1_10, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(40, 21);
            this.Label4.TabIndex = 35;
            this.Label4.Text = "NAME";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Controls.Add(this.SSTab1_Page5);
            this.SSTab1.Location = new System.Drawing.Point(30, 304);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 50, 1, 1);
            this.SSTab1.ShowFocusRect = false;
            this.SSTab1.Size = new System.Drawing.Size(1003, 551);
            this.SSTab1.TabIndex = 33;
            this.SSTab1.TabsPerRow = 0;
            this.SSTab1.Text = "Legal";
            this.ToolTip1.SetToolTip(this.SSTab1, null);
            this.SSTab1.WordWrap = false;
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.Label12);
            this.SSTab1_Page1.Controls.Add(this.Label1_1);
            this.SSTab1_Page1.Controls.Add(this.Label13);
            this.SSTab1_Page1.Controls.Add(this.Label1_12);
            this.SSTab1_Page1.Controls.Add(this.Label1_6);
            this.SSTab1_Page1.Controls.Add(this.Label1_5);
            this.SSTab1_Page1.Controls.Add(this.Label1_4);
            this.SSTab1_Page1.Controls.Add(this.Label1_3);
            this.SSTab1_Page1.Controls.Add(this.Label1_2);
            this.SSTab1_Page1.Controls.Add(this.Label9);
            this.SSTab1_Page1.Controls.Add(this.txtDoingBusinessAs);
            this.SSTab1_Page1.Controls.Add(this.txtDesig);
            this.SSTab1_Page1.Controls.Add(this.txtLast);
            this.SSTab1_Page1.Controls.Add(this.txtMiddle);
            this.SSTab1_Page1.Controls.Add(this.txtName);
            this.SSTab1_Page1.Controls.Add(this.txtStreet);
            this.SSTab1_Page1.Controls.Add(this.txtApt);
            this.SSTab1_Page1.Controls.Add(this.txtStreetNum);
            this.SSTab1_Page1.Controls.Add(this.txtZip4);
            this.SSTab1_Page1.Controls.Add(this.txtZip);
            this.SSTab1_Page1.Controls.Add(this.txtState);
            this.SSTab1_Page1.Controls.Add(this.txtCity);
            this.SSTab1_Page1.Controls.Add(this.txtAddress2);
            this.SSTab1_Page1.Controls.Add(this.txtAddress1);
            this.SSTab1_Page1.Controls.Add(this.txtBusinessLicense);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Text = "Legal";
            this.ToolTip1.SetToolTip(this.SSTab1_Page1, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 85);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(132, 16);
            this.Label12.TabIndex = 37;
            this.Label12.Text = "DOING BUSINESS AS";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(20, 34);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(50, 18);
            this.Label1_1.TabIndex = 38;
            this.Label1_1.Text = "OWNER";
            this.ToolTip1.SetToolTip(this.Label1_1, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(408, 334);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(15, 13);
            this.Label13.TabIndex = 39;
            this.Label13.Text = "-";
            this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(20, 394);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(74, 18);
            this.Label1_12.TabIndex = 40;
            this.Label1_12.Text = "LOCATION";
            this.ToolTip1.SetToolTip(this.Label1_12, null);
            // 
            // Label1_6
            // 
            this.Label1_6.Location = new System.Drawing.Point(289, 334);
            this.Label1_6.Name = "Label1_6";
            this.Label1_6.Size = new System.Drawing.Size(22, 18);
            this.Label1_6.TabIndex = 41;
            this.Label1_6.Text = "ZIP";
            this.ToolTip1.SetToolTip(this.Label1_6, null);
            // 
            // Label1_5
            // 
            this.Label1_5.Location = new System.Drawing.Point(20, 334);
            this.Label1_5.Name = "Label1_5";
            this.Label1_5.Size = new System.Drawing.Size(50, 18);
            this.Label1_5.TabIndex = 42;
            this.Label1_5.Text = "STATE";
            this.ToolTip1.SetToolTip(this.Label1_5, null);
            // 
            // Label1_4
            // 
            this.Label1_4.Location = new System.Drawing.Point(20, 274);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(41, 18);
            this.Label1_4.TabIndex = 43;
            this.Label1_4.Text = "CITY";
            this.ToolTip1.SetToolTip(this.Label1_4, null);
            // 
            // Label1_3
            // 
            this.Label1_3.Location = new System.Drawing.Point(20, 214);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(74, 18);
            this.Label1_3.TabIndex = 44;
            this.Label1_3.Text = "ADDRESS 2";
            this.ToolTip1.SetToolTip(this.Label1_3, null);
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(20, 154);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(66, 18);
            this.Label1_2.TabIndex = 45;
            this.Label1_2.Text = "ADDRESS 1";
            this.ToolTip1.SetToolTip(this.Label1_2, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 454);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(112, 16);
            this.Label9.TabIndex = 46;
            this.Label9.Text = "BUSINESS LICENSE";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // txtDoingBusinessAs
            // 
			this.txtDoingBusinessAs.MaxLength = 254;
            this.txtDoingBusinessAs.AutoSize = false;
            this.txtDoingBusinessAs.BackColor = System.Drawing.SystemColors.Window;
            this.txtDoingBusinessAs.LinkItem = null;
            this.txtDoingBusinessAs.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDoingBusinessAs.LinkTopic = null;
            this.txtDoingBusinessAs.Location = new System.Drawing.Point(204, 80);
            this.txtDoingBusinessAs.Name = "txtDoingBusinessAs";
            this.txtDoingBusinessAs.Size = new System.Drawing.Size(301, 40);
            this.txtDoingBusinessAs.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.txtDoingBusinessAs, null);
            // 
            // txtDesig
            // 
            this.txtDesig.AutoSize = false;
            this.txtDesig.BackColor = System.Drawing.SystemColors.Window;
            this.txtDesig.LinkItem = null;
            this.txtDesig.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDesig.LinkTopic = null;
            this.txtDesig.Location = new System.Drawing.Point(691, 20);
            this.txtDesig.Name = "txtDesig";
            this.txtDesig.Size = new System.Drawing.Size(86, 40);
            this.txtDesig.TabIndex = 14;
            this.txtDesig.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtDesig, "Designation. Jr,Sr,III etc.");
            // 
            // txtLast
            // 
            this.txtLast.AutoSize = false;
            this.txtLast.BackColor = System.Drawing.SystemColors.Window;
            this.txtLast.LinkItem = null;
            this.txtLast.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLast.LinkTopic = null;
            this.txtLast.Location = new System.Drawing.Point(523, 20);
            this.txtLast.Name = "txtLast";
            this.txtLast.Size = new System.Drawing.Size(148, 40);
            this.txtLast.TabIndex = 13;
            this.txtLast.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtLast, "Last name");
            // 
            // txtMiddle
            // 
            this.txtMiddle.AutoSize = false;
            this.txtMiddle.BackColor = System.Drawing.SystemColors.Window;
            this.txtMiddle.LinkItem = null;
            this.txtMiddle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMiddle.LinkTopic = null;
            this.txtMiddle.Location = new System.Drawing.Point(405, 20);
            this.txtMiddle.Name = "txtMiddle";
            this.txtMiddle.Size = new System.Drawing.Size(98, 40);
            this.txtMiddle.TabIndex = 12;
            this.txtMiddle.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtMiddle, "Middle name or initial");
            // 
            // txtName
            // 
            this.txtName.AutoSize = false;
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.LinkItem = null;
            this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtName.LinkTopic = null;
            this.txtName.Location = new System.Drawing.Point(204, 20);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(181, 40);
            this.txtName.TabIndex = 11;
            this.txtName.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtName, "Organization name or first name of individual");
            // 
            // txtStreet
            // 
            this.txtStreet.AutoSize = false;
            this.txtStreet.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreet.LinkItem = null;
            this.txtStreet.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStreet.LinkTopic = null;
            this.txtStreet.Location = new System.Drawing.Point(372, 380);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(165, 40);
            this.txtStreet.TabIndex = 24;
            this.txtStreet.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtStreet, null);
            // 
            // txtApt
            // 
            this.txtApt.AutoSize = false;
            this.txtApt.BackColor = System.Drawing.SystemColors.Window;
            this.txtApt.LinkItem = null;
            this.txtApt.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtApt.LinkTopic = null;
            this.txtApt.Location = new System.Drawing.Point(289, 380);
            this.txtApt.Name = "txtApt";
            this.txtApt.Size = new System.Drawing.Size(66, 40);
            this.txtApt.TabIndex = 23;
            this.txtApt.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtApt, null);
            // 
            // txtStreetNum
            // 
            this.txtStreetNum.AutoSize = false;
            this.txtStreetNum.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNum.LinkItem = null;
            this.txtStreetNum.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStreetNum.LinkTopic = null;
            this.txtStreetNum.Location = new System.Drawing.Point(204, 380);
            this.txtStreetNum.Name = "txtStreetNum";
            this.txtStreetNum.Size = new System.Drawing.Size(66, 40);
            this.txtStreetNum.TabIndex = 22;
            this.txtStreetNum.Tag = "RE";
            this.txtStreetNum.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtStreetNum, null);
            // 
            // txtZip4
            // 
            this.txtZip4.AutoSize = false;
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.LinkItem = null;
            this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip4.LinkTopic = null;
            this.txtZip4.Location = new System.Drawing.Point(437, 320);
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(76, 40);
            this.txtZip4.TabIndex = 21;
            this.txtZip4.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtZip4, null);
            // 
            // txtZip
            // 
            this.txtZip.AutoSize = false;
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.LinkItem = null;
            this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZip.LinkTopic = null;
            this.txtZip.Location = new System.Drawing.Point(325, 320);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(76, 40);
            this.txtZip.TabIndex = 20;
            this.txtZip.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtZip, null);
            // 
            // txtState
            // 
			this.txtState.MaxLength = 2;
            this.txtState.AutoSize = false;
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.LinkItem = null;
            this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtState.LinkTopic = null;
            this.txtState.Location = new System.Drawing.Point(204, 320);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(66, 40);
            this.txtState.TabIndex = 19;
            this.txtState.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtState, null);
            // 
            // txtCity
            // 
            this.txtCity.AutoSize = false;
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.LinkItem = null;
            this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtCity.LinkTopic = null;
            this.txtCity.Location = new System.Drawing.Point(204, 260);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(301, 40);
            this.txtCity.TabIndex = 18;
            this.txtCity.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtCity, null);
            // 
            // txtAddress2
            // 
            this.txtAddress2.AutoSize = false;
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.LinkItem = null;
            this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress2.LinkTopic = null;
            this.txtAddress2.Location = new System.Drawing.Point(204, 200);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(301, 40);
            this.txtAddress2.TabIndex = 17;
            this.txtAddress2.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtAddress2, null);
            // 
            // txtAddress1
            // 
            this.txtAddress1.AutoSize = false;
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.LinkItem = null;
            this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAddress1.LinkTopic = null;
            this.txtAddress1.Location = new System.Drawing.Point(204, 140);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(301, 40);
            this.txtAddress1.TabIndex = 16;
            this.txtAddress1.Tag = "RE";
            this.ToolTip1.SetToolTip(this.txtAddress1, null);
            // 
            // txtBusinessLicense
            // 
			this.txtBusinessLicense.MaxLength = 254;
            this.txtBusinessLicense.AutoSize = false;
            this.txtBusinessLicense.BackColor = System.Drawing.SystemColors.Window;
            this.txtBusinessLicense.LinkItem = null;
            this.txtBusinessLicense.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBusinessLicense.LinkTopic = null;
            this.txtBusinessLicense.Location = new System.Drawing.Point(204, 440);
            this.txtBusinessLicense.Name = "txtBusinessLicense";
            this.txtBusinessLicense.Size = new System.Drawing.Size(299, 40);
            this.txtBusinessLicense.TabIndex = 25;
            this.ToolTip1.SetToolTip(this.txtBusinessLicense, null);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.GridOpens);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Text = "User Defined";
            this.ToolTip1.SetToolTip(this.SSTab1_Page2, null);
            // 
            // GridOpens
            // 
            this.GridOpens.AllowSelection = false;
            this.GridOpens.AllowUserToResizeColumns = false;
            this.GridOpens.AllowUserToResizeRows = false;
            this.GridOpens.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridOpens.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridOpens.BackColorBkg = System.Drawing.Color.Empty;
            this.GridOpens.BackColorFixed = System.Drawing.Color.Empty;
            this.GridOpens.BackColorSel = System.Drawing.Color.Empty;
            this.GridOpens.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridOpens.Cols = 7;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridOpens.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridOpens.ColumnHeadersHeight = 30;
            this.GridOpens.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridOpens.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridOpens.DragIcon = null;
            this.GridOpens.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridOpens.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridOpens.ExtendLastCol = true;
            this.GridOpens.FixedCols = 2;
            this.GridOpens.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridOpens.FrozenCols = 0;
            this.GridOpens.GridColor = System.Drawing.Color.Empty;
            this.GridOpens.GridColorFixed = System.Drawing.Color.Empty;
            this.GridOpens.Location = new System.Drawing.Point(20, 20);
            this.GridOpens.Name = "GridOpens";
            this.GridOpens.OutlineCol = 0;
            this.GridOpens.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridOpens.RowHeightMin = 0;
            this.GridOpens.Rows = 1;
            this.GridOpens.ScrollTipText = null;
            this.GridOpens.ShowColumnVisibilityMenu = false;
            this.GridOpens.ShowFocusCell = false;
            this.GridOpens.Size = new System.Drawing.Size(963, 462);
            this.GridOpens.StandardTab = true;
            this.GridOpens.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridOpens.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridOpens.TabIndex = 56;
            this.ToolTip1.SetToolTip(this.GridOpens, null);
            this.GridOpens.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridOpens_ValidateEdit);
            this.GridOpens.CurrentCellChanged += new System.EventHandler(this.GridOpens_RowColChange);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.rtbDetail);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Text = "Detail";
            this.ToolTip1.SetToolTip(this.SSTab1_Page3, null);
            // 
            // rtbDetail
            // 
			this.rtbDetail.MaxLength = 63000;
            this.rtbDetail.Location = new System.Drawing.Point(20, 20);
            this.rtbDetail.Multiline = true;
            this.rtbDetail.Name = "rtbDetail";
            this.rtbDetail.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.rtbDetail.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.rtbDetail.SelTabCount = null;
            this.rtbDetail.Size = new System.Drawing.Size(963, 462);
            this.rtbDetail.TabIndex = 55;
            this.ToolTip1.SetToolTip(this.rtbDetail, null);
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.gridActivity);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Text = "Activity";
            this.ToolTip1.SetToolTip(this.SSTab1_Page4, null);
            // 
            // gridActivity
            // 
            this.gridActivity.AllowSelection = false;
            this.gridActivity.AllowUserToResizeColumns = false;
            this.gridActivity.AllowUserToResizeRows = false;
            this.gridActivity.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridActivity.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridActivity.BackColorBkg = System.Drawing.Color.Empty;
            this.gridActivity.BackColorFixed = System.Drawing.Color.Empty;
            this.gridActivity.BackColorSel = System.Drawing.Color.Empty;
            this.gridActivity.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridActivity.Cols = 4;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridActivity.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridActivity.ColumnHeadersHeight = 30;
            this.gridActivity.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridActivity.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridActivity.DragIcon = null;
            this.gridActivity.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridActivity.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMoveRows;
            this.gridActivity.ExtendLastCol = true;
            this.gridActivity.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridActivity.FrozenCols = 0;
            this.gridActivity.GridColor = System.Drawing.Color.Empty;
            this.gridActivity.GridColorFixed = System.Drawing.Color.Empty;
            this.gridActivity.Location = new System.Drawing.Point(20, 20);
            this.gridActivity.Name = "gridActivity";
            this.gridActivity.OutlineCol = 0;
            this.gridActivity.ReadOnly = true;
            this.gridActivity.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridActivity.RowHeightMin = 0;
            this.gridActivity.Rows = 1;
            this.gridActivity.ScrollTipText = null;
            this.gridActivity.ShowColumnVisibilityMenu = false;
            this.gridActivity.ShowFocusCell = false;
            this.gridActivity.Size = new System.Drawing.Size(963, 462);
            this.gridActivity.StandardTab = true;
            this.gridActivity.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridActivity.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridActivity.TabIndex = 54;
            this.ToolTip1.SetToolTip(this.gridActivity, null);
            this.gridActivity.KeyDown += new Wisej.Web.KeyEventHandler(this.gridActivity_KeyDownEvent);
            this.gridActivity.DoubleClick += new System.EventHandler(this.gridActivity_DblClick);
            // 
            // SSTab1_Page5
            // 
            this.SSTab1_Page5.Controls.Add(this.gridDocuments);
            this.SSTab1_Page5.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page5.Name = "SSTab1_Page5";
            this.SSTab1_Page5.Text = "Documents";
            this.ToolTip1.SetToolTip(this.SSTab1_Page5, null);
            // 
            // gridDocuments
            // 
            this.gridDocuments.AllowSelection = false;
            this.gridDocuments.AllowUserToResizeColumns = false;
            this.gridDocuments.AllowUserToResizeRows = false;
            this.gridDocuments.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridDocuments.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridDocuments.BackColorBkg = System.Drawing.Color.Empty;
            this.gridDocuments.BackColorFixed = System.Drawing.Color.Empty;
            this.gridDocuments.BackColorSel = System.Drawing.Color.Empty;
            this.gridDocuments.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridDocuments.Cols = 3;
            dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridDocuments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridDocuments.ColumnHeadersHeight = 30;
            this.gridDocuments.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridDocuments.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridDocuments.DragIcon = null;
            this.gridDocuments.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridDocuments.ExtendLastCol = true;
            this.gridDocuments.FixedCols = 0;
            this.gridDocuments.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridDocuments.FrozenCols = 0;
            this.gridDocuments.GridColor = System.Drawing.Color.Empty;
            this.gridDocuments.GridColorFixed = System.Drawing.Color.Empty;
            this.gridDocuments.Location = new System.Drawing.Point(20, 20);
            this.gridDocuments.Name = "gridDocuments";
            this.gridDocuments.OutlineCol = 0;
            this.gridDocuments.ReadOnly = true;
            this.gridDocuments.RowHeadersVisible = false;
            this.gridDocuments.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDocuments.RowHeightMin = 0;
            this.gridDocuments.Rows = 1;
            this.gridDocuments.ScrollTipText = null;
            this.gridDocuments.ShowColumnVisibilityMenu = false;
            this.gridDocuments.ShowFocusCell = false;
            this.gridDocuments.Size = new System.Drawing.Size(963, 462);
            this.gridDocuments.StandardTab = true;
            this.gridDocuments.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridDocuments.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridDocuments.TabIndex = 53;
            this.ToolTip1.SetToolTip(this.gridDocuments, null);
            this.gridDocuments.KeyDown += new Wisej.Web.KeyEventHandler(this.gridDocuments_KeyDownEvent);
            this.gridDocuments.DoubleClick += new System.EventHandler(this.gridDocuments_DblClick);
            // 
            // txtIdentifier
            // 
			this.txtIdentifier.MaxLength = 254;
            this.txtIdentifier.AutoSize = false;
            this.txtIdentifier.BackColor = System.Drawing.SystemColors.Window;
            this.txtIdentifier.LinkItem = null;
            this.txtIdentifier.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtIdentifier.LinkTopic = null;
            this.txtIdentifier.Location = new System.Drawing.Point(30, 82);
            this.txtIdentifier.Name = "txtIdentifier";
            this.txtIdentifier.Size = new System.Drawing.Size(144, 40);
            this.txtIdentifier.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtIdentifier, null);
            // 
            // cmbStatus
            // 
            this.cmbStatus.AutoSize = false;
            this.cmbStatus.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(415, 81);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(198, 40);
            this.cmbStatus.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbStatus, null);
            // 
            // cmbViolationType
            // 
            this.cmbViolationType.AutoSize = false;
            this.cmbViolationType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbViolationType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbViolationType.FormattingEnabled = true;
            this.cmbViolationType.Location = new System.Drawing.Point(197, 82);
            this.cmbViolationType.Name = "cmbViolationType";
            this.cmbViolationType.Size = new System.Drawing.Size(198, 40);
            this.cmbViolationType.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbViolationType, null);
            this.cmbViolationType.SelectedIndexChanged += new System.EventHandler(this.cmbViolationType_SelectedIndexChanged);
            // 
            // txtDescription
            // 
			this.txtDescription.MaxLength = 254;
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(197, 167);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(416, 40);
            this.txtDescription.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtDescription, null);
            // 
            // t2kDateIssued
            // 
            this.t2kDateIssued.Location = new System.Drawing.Point(30, 244);
            this.t2kDateIssued.Mask = "##/##/####";
            this.t2kDateIssued.Name = "t2kDateIssued";
            this.t2kDateIssued.Size = new System.Drawing.Size(115, 40);
            this.t2kDateIssued.TabIndex = 5;
            this.t2kDateIssued.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.t2kDateIssued, null);
            // 
            // T2KDateOccurred
            // 
            this.T2KDateOccurred.Location = new System.Drawing.Point(163, 244);
            this.T2KDateOccurred.Mask = "##/##/####";
            this.T2KDateOccurred.Name = "T2KDateOccurred";
            this.T2KDateOccurred.Size = new System.Drawing.Size(115, 40);
            this.T2KDateOccurred.TabIndex = 6;
            this.T2KDateOccurred.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.T2KDateOccurred, null);
            // 
            // T2KDateResolved
            // 
            this.T2KDateResolved.Location = new System.Drawing.Point(296, 244);
            this.T2KDateResolved.Mask = "##/##/####";
            this.T2KDateResolved.Name = "T2KDateResolved";
            this.T2KDateResolved.Size = new System.Drawing.Size(115, 40);
            this.T2KDateResolved.TabIndex = 7;
            this.T2KDateResolved.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.T2KDateResolved, null);
            // 
            // GridDeletedPhones
            // 
            this.GridDeletedPhones.AllowSelection = false;
            this.GridDeletedPhones.AllowUserToResizeColumns = false;
            this.GridDeletedPhones.AllowUserToResizeRows = false;
            this.GridDeletedPhones.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridDeletedPhones.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridDeletedPhones.BackColorBkg = System.Drawing.Color.Empty;
            this.GridDeletedPhones.BackColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedPhones.BackColorSel = System.Drawing.Color.Empty;
            this.GridDeletedPhones.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridDeletedPhones.Cols = 2;
            dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridDeletedPhones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.GridDeletedPhones.ColumnHeadersHeight = 30;
            this.GridDeletedPhones.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridDeletedPhones.ColumnHeadersVisible = false;
            dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridDeletedPhones.DefaultCellStyle = dataGridViewCellStyle10;
            this.GridDeletedPhones.DragIcon = null;
            this.GridDeletedPhones.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridDeletedPhones.FixedRows = 0;
            this.GridDeletedPhones.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedPhones.FrozenCols = 0;
            this.GridDeletedPhones.GridColor = System.Drawing.Color.Empty;
            this.GridDeletedPhones.GridColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedPhones.Location = new System.Drawing.Point(30, 30);
            this.GridDeletedPhones.Name = "GridDeletedPhones";
            this.GridDeletedPhones.OutlineCol = 0;
            this.GridDeletedPhones.ReadOnly = true;
            this.GridDeletedPhones.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridDeletedPhones.RowHeightMin = 0;
            this.GridDeletedPhones.Rows = 0;
            this.GridDeletedPhones.ScrollTipText = null;
            this.GridDeletedPhones.ShowColumnVisibilityMenu = false;
            this.GridDeletedPhones.ShowFocusCell = false;
            this.GridDeletedPhones.Size = new System.Drawing.Size(144, 42);
            this.GridDeletedPhones.StandardTab = true;
            this.GridDeletedPhones.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridDeletedPhones.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridDeletedPhones.TabIndex = 52;
            this.ToolTip1.SetToolTip(this.GridDeletedPhones, null);
            this.GridDeletedPhones.Visible = false;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(33, 140);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(128, 16);
            this.Label10.TabIndex = 51;
            this.Label10.Text = "FEE";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // lblPermit
            // 
            this.lblPermit.Location = new System.Drawing.Point(308, 28);
            this.lblPermit.Name = "lblPermit";
            this.lblPermit.Size = new System.Drawing.Size(161, 18);
            this.lblPermit.TabIndex = 50;
            this.ToolTip1.SetToolTip(this.lblPermit, null);
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(106, 28);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(122, 18);
            this.lblAccount.TabIndex = 49;
            this.ToolTip1.SetToolTip(this.lblAccount, null);
            // 
            // lblPermitLabel
            // 
            this.lblPermitLabel.Location = new System.Drawing.Point(248, 28);
            this.lblPermitLabel.Name = "lblPermitLabel";
            this.lblPermitLabel.Size = new System.Drawing.Size(55, 18);
            this.lblPermitLabel.TabIndex = 48;
            this.lblPermitLabel.Text = "PERMIT";
            this.ToolTip1.SetToolTip(this.lblPermitLabel, null);
            // 
            // lblAccountLabel
            // 
            this.lblAccountLabel.Location = new System.Drawing.Point(30, 28);
            this.lblAccountLabel.Name = "lblAccountLabel";
            this.lblAccountLabel.Size = new System.Drawing.Size(67, 18);
            this.lblAccountLabel.TabIndex = 47;
            this.lblAccountLabel.Text = "ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblAccountLabel, null);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(31, 55);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(128, 16);
            this.Label8.TabIndex = 32;
            this.Label8.Text = "IDENTIFIER";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(415, 55);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(198, 16);
            this.Label7.TabIndex = 31;
            this.Label7.Text = "STATUS";
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(296, 222);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(98, 16);
            this.Label6.TabIndex = 30;
            this.Label6.Text = "DATE RESOLVED";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(163, 222);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(108, 16);
            this.Label5.TabIndex = 29;
            this.Label5.Text = "DATE OCCURRED";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(30, 222);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(98, 16);
            this.Label1_0.TabIndex = 28;
            this.Label1_0.Text = "DATE ISSUED";
            this.ToolTip1.SetToolTip(this.Label1_0, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(197, 55);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(198, 16);
            this.Label2.TabIndex = 27;
            this.Label2.Text = "TYPE";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(197, 140);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(416, 16);
            this.Label3.TabIndex = 26;
            this.Label3.Text = "DESCRIPTION";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // cmdAttachDoc
            // 
            this.cmdAttachDoc.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAttachDoc.AppearanceKey = "toolbarButton";
            this.cmdAttachDoc.Location = new System.Drawing.Point(437, 29);
            this.cmdAttachDoc.Name = "cmdAttachDoc";
            this.cmdAttachDoc.Size = new System.Drawing.Size(124, 24);
            this.cmdAttachDoc.TabIndex = 0;
            this.cmdAttachDoc.Text = "Attach Document";
            this.ToolTip1.SetToolTip(this.cmdAttachDoc, null);
            this.cmdAttachDoc.Click += new System.EventHandler(this.mnuAttachDoc_Click);
            // 
            // cmdRemoveDocument
            // 
            this.cmdRemoveDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemoveDocument.AppearanceKey = "toolbarButton";
            this.cmdRemoveDocument.Location = new System.Drawing.Point(567, 29);
            this.cmdRemoveDocument.Name = "cmdRemoveDocument";
            this.cmdRemoveDocument.Size = new System.Drawing.Size(136, 24);
            this.cmdRemoveDocument.TabIndex = 0;
            this.cmdRemoveDocument.Text = "Remove Document";
            this.ToolTip1.SetToolTip(this.cmdRemoveDocument, null);
            this.cmdRemoveDocument.Click += new System.EventHandler(this.mnuRemoveDocument_Click);
            // 
            // cmdViewDocuments
            // 
            this.cmdViewDocuments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdViewDocuments.AppearanceKey = "toolbarButton";
            this.cmdViewDocuments.Location = new System.Drawing.Point(709, 29);
            this.cmdViewDocuments.Name = "cmdViewDocuments";
            this.cmdViewDocuments.Size = new System.Drawing.Size(122, 24);
            this.cmdViewDocuments.TabIndex = 0;
            this.cmdViewDocuments.Text = "View Documents";
            this.ToolTip1.SetToolTip(this.cmdViewDocuments, null);
            this.cmdViewDocuments.Click += new System.EventHandler(this.mnuViewDocuments_Click);
            // 
            // cmdAddActivity
            // 
            this.cmdAddActivity.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddActivity.AppearanceKey = "toolbarButton";
            this.cmdAddActivity.Location = new System.Drawing.Point(837, 29);
            this.cmdAddActivity.Name = "cmdAddActivity";
            this.cmdAddActivity.Size = new System.Drawing.Size(92, 24);
            this.cmdAddActivity.TabIndex = 0;
            this.cmdAddActivity.Text = "Add Activity";
            this.ToolTip1.SetToolTip(this.cmdAddActivity, null);
            this.cmdAddActivity.Click += new System.EventHandler(this.mnuAddActivity_Click);
            // 
            // cmdDeleteViolation
            // 
            this.cmdDeleteViolation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteViolation.AppearanceKey = "toolbarButton";
            this.cmdDeleteViolation.Location = new System.Drawing.Point(935, 29);
            this.cmdDeleteViolation.Name = "cmdDeleteViolation";
            this.cmdDeleteViolation.Size = new System.Drawing.Size(115, 24);
            this.cmdDeleteViolation.TabIndex = 0;
            this.cmdDeleteViolation.Text = "Delete Violation";
            this.ToolTip1.SetToolTip(this.cmdDeleteViolation, null);
            this.cmdDeleteViolation.Click += new System.EventHandler(this.mnuDeleteViolation_Click);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintActivity,
            this.mnuPrintActivityWithDetail,
            this.mnuPrintViolation});
            this.MainMenu1.Name = null;
            // 
            // mnuPrintActivity
            // 
            this.mnuPrintActivity.Index = 0;
            this.mnuPrintActivity.Name = "mnuPrintActivity";
            this.mnuPrintActivity.Text = "Print Activity";
            this.mnuPrintActivity.Click += new System.EventHandler(this.mnuPrintActivity_Click);
            // 
            // mnuPrintActivityWithDetail
            // 
            this.mnuPrintActivityWithDetail.Index = 1;
            this.mnuPrintActivityWithDetail.Name = "mnuPrintActivityWithDetail";
            this.mnuPrintActivityWithDetail.Text = "Print Activity With Detail";
            this.mnuPrintActivityWithDetail.Click += new System.EventHandler(this.mnuPrintActivityWithDetail_Click);
            // 
            // mnuPrintViolation
            // 
            this.mnuPrintViolation.Index = 2;
            this.mnuPrintViolation.Name = "mnuPrintViolation";
            this.mnuPrintViolation.Text = "Print Violation";
            this.mnuPrintViolation.Click += new System.EventHandler(this.mnuPrintViolation_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(481, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.ToolTip1.SetToolTip(this.cmdSave, null);
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmViolation
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmViolation";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Violation";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmViolation_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmViolation_KeyDown);
            this.Resize += new System.EventHandler(this.frmViolation_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framContact)).EndInit();
            this.framContact.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridPhones)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            this.SSTab1_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            this.SSTab1_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbDetail)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridActivity)).EndInit();
            this.SSTab1_Page5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDateIssued)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateOccurred)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateResolved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAttachDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdViewDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteViolation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private System.ComponentModel.IContainer components;
    }
}
