//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Diagnostics;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using Wisej.Web.Ext.FullCalendar;

namespace TWCE0000
{
	public partial class frmEditRecurrence : BaseForm
	{
		public frmEditRecurrence()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmEditRecurrence InstancePtr
		{
			get
			{
				return (frmEditRecurrence)Sys.GetInstance(typeof(frmEditRecurrence));
			}
		}

		protected frmEditRecurrence _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public Event m_pMasterEvent = new Event();
		public bool m_bUpdateFromEvent;
		//private CalendarRecurrencePattern m_pRPattern = new CalendarRecurrencePattern();
		private bool m_bWasNotRecur;
		// Dim xctl As XtremeCommandBars.ICommandBarControl
		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			UpdatePatternFromControls();
			//FC:TODO
			//if (m_pRPattern.Exceptions.Count>0) {
			//	// vbPorter upgrade warning: nRes As object	OnWrite(DialogResult)
			//	DialogResult nRes = 0;
			//	nRes = MessageBox.Show("Any exceptions associated with this recurring appointment will be lost. Is this OK?", "Continue?", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
			//	if (nRes==DialogResult.Cancel) {
			//		return;
			//	}
			//	m_pRPattern.RemoveAllExceptions();
			//}
			//m_pMasterEvent.UpdateRecurrence(m_pRPattern);
			// =====================================================
			Close();
		}

		private void btnRemoveRecurrence_Click(object sender, System.EventArgs e)
		{
			//m_pMasterEvent.RemoveRecurrence();
			Close();
		}
		// Private Sub cmbEventDuration_Change()
		// Dim arTimes
		// arTimes = Split(cmbEventStartTime, ":")
		// m_pRPattern.StartTime = TimeSerial(arTimes(0), arTimes(1), 0)
		//
		// Dim dtEndTime As Date
		// dtEndTime = DateAdd("n", Val(cmbEventDuration), m_pRPattern.StartTime)
		//
		// txtEventEndTime.Text = FormatDateTime(dtEndTime, vbShortTime)
		// End Sub
		private void UpdateDuration()
		{
			//FC:TODO
			//m_pRPattern.StartTime = dtpEventStart.Value;
			//int intDays;
			//int intHours;
			//int intMinutes;
			//intDays = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDays.Text));
			//intHours = FCConvert.ToInt32(Math.Round(Conversion.Val(txtHours.Text));
			//intMinutes = FCConvert.ToInt32(Math.Round(Conversion.Val(txtMinutes.Text));
			//DateTime dtEndTime;
			//int lngMinutes;
			//lngMinutes = (intHours*60)+(intDays*1440)+intMinutes;
			//dtEndTime = fecherFoundation.DateAndTime.DateAdd("n", lngMinutes, m_pRPattern.StartTime);
			//txtEventEndTime.Text = Strings.Format(dtEndTime, "MM/dd/yyyy");
		}
		// Private Sub cmbEventDuration_Click()
		// cmbEventDuration_Change
		// End Sub
		// Private Sub cmbEventStartTime_Change()
		// UpdateEndTimeCombo
		// End Sub
		//
		// Private Sub cmbEventStartTime_Click()
		// UpdateDuration
		// End Sub
		//
		// Private Sub cmbEventStartTime_LostFocus()
		// UpdateDuration
		// End Sub
		private void dtpEventStart_Change(object sender, System.EventArgs e)
		{
			UpdateDuration();
		}

		private void dtpEventStart_Leave(object sender, System.EventArgs e)
		{
			UpdateDuration();
		}

		private void frmEditRecurrence_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditRecurrence properties;
			//frmEditRecurrence.FillStyle	= 0;
			//frmEditRecurrence.ScaleWidth	= 9300;
			//frmEditRecurrence.ScaleHeight	= 7545;
			//frmEditRecurrence.LinkTopic	= "Form2";
			//frmEditRecurrence.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// On Error GoTo skip1
			m_bWasNotRecur = false;
			m_bUpdateFromEvent = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			MonthCalendar nRState;
			//FC:TODO
			//nRState = m_pMasterEvent.RecurrenceState;
			//if (nRState==xtpCalendarRecurrenceMaster) {
			//	m_pRPattern = m_pMasterEvent.RecurrencePattern;
			//} else {
			//	m_pMasterEvent = m_pMasterEvent.CloneEvent;
			//	if (nRState==xtpCalendarRecurrenceNotRecurring) {
			//		DateTime dtStartTime, dtEndTime;
			//		// vbPorter upgrade warning: nDuartionMinutes As int	OnWrite(long)
			//		int nDuartionMinutes = 0;
			//		dtStartTime = m_pMasterEvent.StartTime;
			//		dtEndTime = m_pMasterEvent.EndTime;
			//		 /*? On Error Resume Next  */
			//		nDuartionMinutes = 0;
			//		nDuartionMinutes = Math.Abs(fecherFoundation.DateAndTime.DateDiff("n", dtEndTime, dtStartTime));
			//		m_pMasterEvent.CreateRecurrence();
			//		m_bWasNotRecur = true;
			//		m_pRPattern = m_pMasterEvent.RecurrencePattern;
			//		m_pRPattern.StartTime = DateAndTime.TimeValue(dtStartTime.ToString());
			//		m_pRPattern.DurationMinutes = nDuartionMinutes;
			//		m_pRPattern.StartDate = DateAndTime.DateValue(dtStartTime.ToString());
			//		m_pMasterEvent.UpdateRecurrence(m_pRPattern);
			//	} else {
			//		Debug.Assert(nRState==xtpCalendarRecurrenceOccurrence || nRState==xtpCalendarRecurrenceException);
			//		m_bUpdateFromEvent = true;
			//		m_pRPattern = m_pMasterEvent.RecurrencePattern;
			//		m_pMasterEvent = m_pRPattern.MasterEvent;
			//	}
			//}
			skip1:
			;
			// ================================
			/*- pageDaily.BorderStyle = 0 */
			;
			// 0 - none
			/*- pageWeekly.BorderStyle = 0 */
			;
			// 0 - none
			/*- pageMonthly.BorderStyle = 0 */
			;
			// 0 - none
			/*- pageYearly.BorderStyle = 0 */
			;
			// 0 - none
			frameRecurrencePattern.WidthOriginal = frameApointmentTime.WidthOriginal;
			pageWeekly.MoveTo(pageDaily.LeftOriginal, pageDaily.TopOriginal);
			pageMonthly.MoveTo(pageDaily.LeftOriginal, pageDaily.TopOriginal);
			pageYearly.MoveTo(pageDaily.LeftOriginal, pageDaily.TopOriginal);
			SetActivePage(1);
			cmbPatternNoEnd.Text = "No End Date";
			btnRemoveRecurrence.Enabled = !m_bWasNotRecur;
			// Dim i As Long, strTmp As String
			// For i = 0 To 24 * 60 Step 30
			// strTmp = Format(i / 60, "00") + ":" + Format(i Mod 60, "00")
			// cmbEventStartTime.AddItem strTmp
			// Next
			// cmbEventDuration.AddItem "5 minutes"
			// cmbEventDuration.AddItem "10 minutes"
			// cmbEventDuration.AddItem "15 minutes"
			// cmbEventDuration.AddItem "30 minutes"
			// cmbEventDuration.AddItem "60 minutes (1 hour)"
			// 
			// cmbEventDuration.AddItem "120 minutes (2 hours)"
			// cmbEventDuration.AddItem "180 minutes (3 hours)"
			// cmbEventDuration.AddItem "240 minutes (4 hours)"
			// cmbEventDuration.AddItem 24 * 60 & " minutes (1 day)"
			// cmbEventDuration.AddItem 2 * 24 * 60 & " minutes (2 days)"
			MonthlyInitialization();
			YearlyInitialization();
			// oprPatternNoEnd.OptionValue = xtpCalendarPatternEndNoDate
			// oprPatternEndAfter.OptionValue = xtpCalendarPatternEndAfterOccurrences
			// oprPatternEndByDate.OptionValue = xtpCalendarPatternEndDate
			// ===
			UpdateControlsFromEvent();
		}

		private void MonthlyInitialization()
		{
			/*? On Error Resume Next  */
			if (cmbMonthlyDayOfMonth.Items.Count == 0)
			{
				int cnt;
				for (cnt = 1; cnt <= 31; cnt++)
				{
					cmbMonthlyDayOfMonth.AddItem(cnt.ToString(), cnt - 1);
				}
				// cnt
				//FC:TODO
				//if (frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.IsValid) {
				//	cmbMonthlyDayOfMonth.SelectedIndex = frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.Begin.Day-1;
				//}
			}
			if (cmbMonthlyDay.Items.Count == 0)
			{
				cmbMonthlyDay.AddItem("first", 0);
				cmbMonthlyDay.AddItem("second", 1);
				cmbMonthlyDay.AddItem("third", 2);
				cmbMonthlyDay.AddItem("fourth", 3);
				cmbMonthlyDay.AddItem("last", 4);
				cmbMonthlyDay.SelectedIndex = 0;
			}
			if (cmbMonthlyDayOfWeek.Items.Count == 0)
			{
				cmbMonthlyDayOfWeek.AddItem("Day", 0);
				cmbMonthlyDayOfWeek.AddItem("WeekDay", 1);
				cmbMonthlyDayOfWeek.AddItem("WeekendDay", 2);
				cmbMonthlyDayOfWeek.AddItem("Sunday", 3);
				cmbMonthlyDayOfWeek.AddItem("Monday", 4);
				cmbMonthlyDayOfWeek.AddItem("Tuesday", 5);
				cmbMonthlyDayOfWeek.AddItem("Wednesday", 6);
				cmbMonthlyDayOfWeek.AddItem("Thursday", 7);
				cmbMonthlyDayOfWeek.AddItem("Friday", 8);
				cmbMonthlyDayOfWeek.AddItem("Saturday", 9);
				//FC:TODO
				//if (frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.IsValid) {
				//	cmbMonthlyDayOfWeek.SelectedIndex = fecherFoundation.DateAndTime.Weekday(frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.Begin, DayOfWeek.Sunday)+2;
				//}
			}
			if (txtMonthlyEveryMonth.Text == "")
			{
				txtMonthlyEveryMonth.Text = "1";
			}
			if (txtMonthlyOfEveryTheMonths.Text == "")
			{
				txtMonthlyOfEveryTheMonths.Text = "1";
			}
		}

		private void YearlyInitialization()
		{
			/*? On Error Resume Next  */
			if (cmbYearlyDate.Items.Count == 0)
			{
				int cnt;
				for (cnt = 1; cnt <= 31; cnt++)
				{
					cmbYearlyDate.AddItem(cnt.ToString(), cnt - 1);
				}
				// cnt
				//FC:TODO
				//if (frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.IsValid) {
				//	cmbYearlyDate.SelectedIndex = frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.Begin.Day-1;
				//}
			}
			if (cmdYearlyThePartOfWeek.Items.Count == 0)
			{
				cmdYearlyThePartOfWeek.AddItem("first", 0);
				cmdYearlyThePartOfWeek.AddItem("second", 1);
				cmdYearlyThePartOfWeek.AddItem("third", 2);
				cmdYearlyThePartOfWeek.AddItem("fourth", 3);
				cmdYearlyThePartOfWeek.AddItem("last", 4);
				cmdYearlyThePartOfWeek.SelectedIndex = 0;
			}
			if (cmbYearlyTheDay.Items.Count == 0)
			{
				cmbYearlyTheDay.AddItem("Day", 0);
				cmbYearlyTheDay.AddItem("WeekDay", 1);
				cmbYearlyTheDay.AddItem("WeekendDay", 2);
				cmbYearlyTheDay.AddItem("Sunday", 3);
				cmbYearlyTheDay.AddItem("Monday", 4);
				cmbYearlyTheDay.AddItem("Tuesday", 5);
				cmbYearlyTheDay.AddItem("Wednesday", 6);
				cmbYearlyTheDay.AddItem("Thursday", 7);
				cmbYearlyTheDay.AddItem("Friday", 8);
				cmbYearlyTheDay.AddItem("Saturday", 9);
				//FC:TODO
				//if (frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.IsValid) {
				//	cmbYearlyTheDay.SelectedIndex = fecherFoundation.DateAndTime.Weekday(frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.Begin, DayOfWeek.Sunday)+2;
				//}
			}
			if (cmbYearlyEveryDate.Items.Count == 0)
			{
				cmbYearlyEveryDate.AddItem("January", 0);
				cmbYearlyEveryDate.AddItem("February", 1);
				cmbYearlyEveryDate.AddItem("March", 2);
				cmbYearlyEveryDate.AddItem("April", 3);
				cmbYearlyEveryDate.AddItem("May", 4);
				cmbYearlyEveryDate.AddItem("June", 5);
				cmbYearlyEveryDate.AddItem("July", 6);
				cmbYearlyEveryDate.AddItem("August", 7);
				cmbYearlyEveryDate.AddItem("September", 8);
				cmbYearlyEveryDate.AddItem("October", 9);
				cmbYearlyEveryDate.AddItem("November", 10);
				cmbYearlyEveryDate.AddItem("December", 11);
				int k;
				for (k = 0; k <= cmbYearlyEveryDate.Items.Count - 1; k++)
				{
					cmbYearlyTheMonth.AddItem(cmbYearlyEveryDate.Items[k].ToString(), k);
				}
				//FC:TODO
				//if (frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.IsValid) {
				//	cmbYearlyEveryDate.SelectedIndex = frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.Begin.Month-1;
				//	cmbYearlyTheMonth.SelectedIndex = frmCalendar.InstancePtr.CalendarControl.ActiveView.selection.Begin.Month-1;
				//}
			}
		}

		private void SetActivePage(int nPage)
		{
			if (nPage == 1)
			{
				cmbRecYearly.Text = "Daily";
			}
			else if (nPage == 2)
			{
				cmbRecYearly.Text = "Weekly";
			}
			else if (nPage == 3)
			{
				cmbRecYearly.Text = "Monthly";
			}
			else if (nPage == 4)
			{
				cmbRecYearly.Text = "Yearly";
			}
			pageDaily.Visible = (nPage == 1);
			pageWeekly.Visible = (nPage == 2);
			pageMonthly.Visible = (nPage == 3);
			pageYearly.Visible = (nPage == 4);
		}

		public int WhichDayMask2index(int nWDay)
		{
			int WhichDayMask2index = 0;
			//FC:TODO
			//if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayAllWeek)
			//{
			//	WhichDayMask2index = 0;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayMo_Fr)
			//{
			//	WhichDayMask2index = 1;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDaySaSu)
			//{
			//	WhichDayMask2index = 2;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDaySunday)
			//{
			//	WhichDayMask2index = 3;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayMonday)
			//{
			//	WhichDayMask2index = 4;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayTuesday)
			//{
			//	WhichDayMask2index = 5;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayWednesday)
			//{
			//	WhichDayMask2index = 6;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayThursday)
			//{
			//	WhichDayMask2index = 7;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDayFriday)
			//{
			//	WhichDayMask2index = 8;
			//}
			//else if ((FCConvert.ToBoolean(nWDay) & xtpCalendarDayAllWeek)==xtpCalendarDaySaturday)
			//{
			//	WhichDayMask2index = 9;
			//}
			return WhichDayMask2index;
		}

		public int index2WhichDayMask(int nIndex)
		{
			int index2WhichDayMask = 0;
			switch ((nIndex))
			{
				case 0:
					{
						//index2WhichDayMask = xtpCalendarDayAllWeek;
						break;
					}
				case 1:
					{
						//index2WhichDayMask = xtpCalendarDayMo_Fr;
						break;
					}
				case 2:
					{
						//index2WhichDayMask = xtpCalendarDaySaSu;
						break;
					}
				case 3:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Sunday);
						break;
					}
				case 4:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Monday);
						break;
					}
				case 5:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Tuesday);
						break;
					}
				case 6:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Wednesday);
						break;
					}
				case 7:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Thursday);
						break;
					}
				case 8:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Friday);
						break;
					}
				case 9:
					{
						index2WhichDayMask = FCConvert.ToInt32(Day.Saturday);
						break;
					}
			}
			//end switch
			return index2WhichDayMask;
		}

		private void UpdateControlsFromEvent()
		{
			// cmbEventStartTime.Text = FormatDateTime(m_pRPattern.StartTime, vbShortTime)
			//dtpEventStart.Value = m_pRPattern.StartTime;
			// cmbEventDuration.Text = m_pRPattern.DurationMinutes & " minutes"
			double intDays;
			double intHours;
			// vbPorter upgrade warning: intMinutes As int	OnWriteFCConvert.ToInt32(
			double intMinutes = 0;
			// vbPorter upgrade warning: lngMinutes As int	OnRead
			double lngMinutes = 0;
			//lngMinutes = m_pRPattern.DurationMinutes;
			intDays = lngMinutes / 1440;
			if (intDays > 0)
			{
				lngMinutes -= (intDays * 1440);
			}
			intHours = lngMinutes / 60;
			if (intHours > 0)
			{
				lngMinutes -= (60 * intHours);
			}
			intMinutes = lngMinutes;
			//UpDownDays.Value = intDays;
			//UpDownHours.Value = intHours;
			//UpDownMinutes.Value = intMinutes;
			UpdateDuration();
			SetActivePage(1);
			//FC:TODO
			// 
			//if (m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceDaily) {
			//	SetActivePage(1);
			//	if (m_pRPattern.Options.DailyEveryWeekDayOnly) {
			//		optDailyEveryNDays.Checked = false;
			//		optDailyEveryWorkDay.Checked = true;
			//		txtDailyEveryNdays.Text = FCConvert.ToString(1);
			//	} else {
			//		optDailyEveryWorkDay.Checked = false;
			//		optDailyEveryNDays.Checked = true;
			//		txtDailyEveryNdays.Text = m_pRPattern.Options.DailyIntervalDays;
			//	}
			//} else if (m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceWeekly) {
			//	SetActivePage(2);
			//	txtWeeklyNWeeks.Text = m_pRPattern.Options.WeeklyIntervalWeeks;
			//	int nWDays = 0;
			//	nWDays = m_pRPattern.Options.WeeklyDayOfWeekMask;
			//	chkWeeklyMonday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDayMonday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//	chkWeeklyTusday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDayTuesday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//	chkWeeklyWednesday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDayWednesday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//	chkWeeklyThursday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDayThursday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//	chkWeeklyFriday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDayFriday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//	chkWeeklySaturday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDaySaturday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//	chkWeeklySunday.CheckState = ((FCConvert.ToBoolean(nWDays) & xtpCalendarDaySunday)!=false ? Wisej.Web.CheckState.Checked : Wisej.Web.CheckState.Unchecked);
			//} else if (m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceMonthly || m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceMonthNth) {
			//	SetActivePage(3);
			//	if (m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceMonthly) {
			//		optMonthlyDay.Checked = true;
			//		cmbMonthlyDayOfMonth.SelectedIndex = m_pRPattern.Options.MonthlyDayOfMonth-1;
			//		txtMonthlyEveryMonth.Text = m_pRPattern.Options.MonthlyIntervalMonths;
			//	} else {
			//		optMonthlyThe.Checked = true;
			//		cmbMonthlyDay.SelectedIndex = m_pRPattern.Options.MonthNthWhichDay-1;
			//		cmbMonthlyDayOfWeek.SelectedIndex = WhichDayMask2index(m_pRPattern.Options.MonthNthWhichDayMask);
			//		txtMonthlyOfEveryTheMonths.Text = m_pRPattern.Options.MonthNthIntervalMonths;
			//	}
			//} else if (m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceYearly || m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceYearNth) {
			//	SetActivePage(4);
			//	if (m_pRPattern.Options.RecurrenceType==xtpCalendarRecurrenceYearly) {
			//		optYearlyDay.Checked = true;
			//		cmbYearlyEveryDate.SelectedIndex = m_pRPattern.Options.YearlyMonthOfYear-1;
			//		cmbYearlyDate.SelectedIndex = m_pRPattern.Options.YearlyDayOfMonth-1;
			//	} else {
			//		optYearlyThe.Checked = true;
			//		cmdYearlyThePartOfWeek.SelectedIndex = m_pRPattern.Options.YearNthWhichDay-1;
			//		cmbYearlyTheDay.SelectedIndex = WhichDayMask2index(m_pRPattern.Options.YearNthWhichDayMask);
			//		cmbYearlyTheMonth.SelectedIndex = m_pRPattern.Options.YearNthMonthOfYear-1;
			//	}
			//}
			//// Start-End pattern
			//ddPatternStartDate.Text = m_pRPattern.StartDate;
			//optPatternNoEnd.Checked = m_pRPattern.EndMethod==xtpCalendarPatternEndNoDate;
			//optPatternEndAfter.Checked = m_pRPattern.EndMethod==xtpCalendarPatternEndAfterOccurrences;
			//optPatternEndByDate.Checked = m_pRPattern.EndMethod==xtpCalendarPatternEndDate;
			//txtPatternEndAfter.Text = "10";
			//ddPatternEndDate.Text = DateTime.FromOADate(DateTime.Now.ToOADate()+5).ToShortDateString();
			//if (m_pRPattern.EndMethod==xtpCalendarPatternEndAfterOccurrences) {
			//	txtPatternEndAfter.Text = m_pRPattern.EndAfterOccurrences;
			//} else if (m_pRPattern.EndMethod==xtpCalendarPatternEndDate) {
			//	ddPatternEndDate.Text = m_pRPattern.EndDate;
			//}
		}

		private void UpdatePatternFromControls()
		{
			//FC:TODO
			// Dim arTimes
			// arTimes = Split(cmbEventStartTime.Text, ":")
			// m_pRPattern.StartTime = TimeSerial(arTimes(0), arTimes(1), 0)
			//m_pRPattern.StartTime = dtpEventStart.Value;
			//int intDays;
			//int intHours;
			//int intMinutes;
			//intDays = FCConvert.ToInt32(Math.Round(Conversion.Val(txtDays.Text));
			//intHours = FCConvert.ToInt32(Math.Round(Conversion.Val(txtHours.Text));
			//intMinutes = FCConvert.ToInt32(Math.Round(Conversion.Val(txtMinutes.Text));
			//m_pRPattern.DurationMinutes = (intDays*1440)+(intHours*60)+intMinutes;
			//if (optRecDaily.Checked) {
			//	m_pRPattern.Options.RecurrenceType = xtpCalendarRecurrenceDaily;
			//	if (optDailyEveryWorkDay.Checked) {
			//		m_pRPattern.Options.DailyEveryWeekDayOnly = true;
			//	} else {
			//		Debug.Assert(optDailyEveryNDays.Checked);
			//		m_pRPattern.Options.DailyEveryWeekDayOnly = false;
			//		m_pRPattern.Options.DailyIntervalDays = Conversion.Val(txtDailyEveryNdays.Text);
			//	}
			//} else if (optRecWeekly.Checked) {
			//	m_pRPattern.Options.RecurrenceType = xtpCalendarRecurrenceWeekly;
			//	m_pRPattern.Options.WeeklyIntervalWeeks = Conversion.Val(txtWeeklyNWeeks.Text);
			//	// vbPorter upgrade warning: nWDays As int	OnWrite(int, bool)
			//	int nWDays = 0;
			//	nWDays = 0;
			//	nWDays += (chkWeeklyMonday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDayMonday : false);
			//	nWDays += (chkWeeklyTusday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDayTuesday : false);
			//	nWDays += (chkWeeklyWednesday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDayWednesday : false);
			//	nWDays += (chkWeeklyThursday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDayThursday : false);
			//	nWDays += (chkWeeklyFriday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDayFriday : false);
			//	nWDays += (chkWeeklySaturday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDaySaturday : false);
			//	nWDays += (chkWeeklySunday.CheckState==Wisej.Web.CheckState.Checked ? xtpCalendarDaySunday : false);
			//	m_pRPattern.Options.WeeklyDayOfWeekMask = nWDays;
			//} else if (optRecMonthly.Checked) {
			//	if (optMonthlyDay.Checked) {
			//		m_pRPattern.Options.RecurrenceType = xtpCalendarRecurrenceMonthly;
			//		m_pRPattern.Options.MonthlyDayOfMonth = cmbMonthlyDayOfMonth.SelectedIndex+1;
			//		m_pRPattern.Options.MonthlyIntervalMonths = FCConvert.ToInt32(Conversion.Val(txtMonthlyEveryMonth.Text));
			//		if (m_pRPattern.Options.MonthlyIntervalMonths<1) m_pRPattern.Options.MonthlyIntervalMonths = 1;
			//	} else {
			//		Debug.Assert(optMonthlyThe.Checked);
			//		m_pRPattern.Options.RecurrenceType = xtpCalendarRecurrenceMonthNth;
			//		m_pRPattern.Options.MonthNthWhichDay = cmbMonthlyDay.SelectedIndex+1;
			//		m_pRPattern.Options.MonthNthWhichDayMask = index2WhichDayMask(cmbMonthlyDayOfWeek.SelectedIndex);
			//		m_pRPattern.Options.MonthNthIntervalMonths = FCConvert.ToInt32(Conversion.Val(txtMonthlyOfEveryTheMonths.Text));
			//		if (m_pRPattern.Options.MonthNthIntervalMonths<1) m_pRPattern.Options.MonthNthIntervalMonths = 1;
			//	}
			//} else if (optRecYearly.Checked) {
			//	// 
			//	if (optYearlyDay.Checked) {
			//		m_pRPattern.Options.RecurrenceType = xtpCalendarRecurrenceYearly;
			//		m_pRPattern.Options.YearlyMonthOfYear = cmbYearlyEveryDate.SelectedIndex+1;
			//		m_pRPattern.Options.YearlyDayOfMonth = cmbYearlyDate.SelectedIndex+1;
			//	} else {
			//		Debug.Assert(optYearlyThe.Checked);
			//		m_pRPattern.Options.RecurrenceType = xtpCalendarRecurrenceYearNth;
			//		m_pRPattern.Options.YearNthWhichDay = cmdYearlyThePartOfWeek.SelectedIndex+1;
			//		m_pRPattern.Options.YearNthWhichDayMask = index2WhichDayMask(cmbYearlyTheDay.SelectedIndex);
			//		m_pRPattern.Options.YearNthMonthOfYear = cmbYearlyTheMonth.SelectedIndex+1;
			//	}
			//}
			//// Start-End pattern
			//m_pRPattern.StartDate = fecherFoundation.DateAndTime.DateValue(ddPatternStartDate.Text);
			//if (optPatternNoEnd.Checked) {
			//	m_pRPattern.EndMethod = xtpCalendarPatternEndNoDate;
			//} else if (optPatternEndAfter.Checked) {
			//	m_pRPattern.EndMethod = xtpCalendarPatternEndAfterOccurrences;
			//	m_pRPattern.EndAfterOccurrences = Conversion.Val(txtPatternEndAfter.Text);
			//} else if (optPatternEndByDate.Checked) {
			//	m_pRPattern.EndMethod = xtpCalendarPatternEndDate;
			//	m_pRPattern.EndDate = fecherFoundation.DateAndTime.DateValue(ddPatternEndDate.Text);
			//}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void optRecDaily_CheckedChanged(object sender, System.EventArgs e)
		{
			SetActivePage(1);
		}

		private void optRecMonthly_CheckedChanged(object sender, System.EventArgs e)
		{
			SetActivePage(3);
		}

		private void optRecWeekly_CheckedChanged(object sender, System.EventArgs e)
		{
			SetActivePage(2);
		}

		private void optRecYearly_CheckedChanged(object sender, System.EventArgs e)
		{
			SetActivePage(4);
		}

		private void frmEditRecurrence_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void UpDownDays_Change(object sender, System.EventArgs e)
		{
			UpdateDuration();
		}

		private void UpDownHours_Change(object sender, System.EventArgs e)
		{
			UpdateDuration();
		}

		private void UpDownMinutes_Change(object sender, System.EventArgs e)
		{
			UpdateDuration();
		}

		private void cmbRecYearly_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		}
		//FC:FINAL:CHN - i.issue #1679: Redesign: Fix overlapping (at original app this controls was near RadioButtons).
		private void cmbPatternNoEnd_SelectedIndexChanged(object sender, EventArgs e)
		{
			txtPatternEndAfter.Visible = false;
			Label7.Visible = false;
			ddPatternEndDate.Visible = false;
			if (cmbPatternNoEnd.SelectedIndex == 0)
			{
			}
			else if (cmbPatternNoEnd.SelectedIndex == 1)
			{
				txtPatternEndAfter.Visible = true;
				Label7.Visible = true;
			}
			else if (cmbPatternNoEnd.SelectedIndex == 2)
			{
				ddPatternEndDate.Visible = true;
			}
		}
	}
}
