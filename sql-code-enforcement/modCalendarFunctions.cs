﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class modCalendarFunctions
	{
		//=========================================================
		public static void FillStandardDurations_0m_2w(ref FCComboBox cmbDuration, bool bSnoozeBox)
		{
			if (!bSnoozeBox)
			{
				cmbDuration.AddItem("0 minutes");
				cmbDuration.AddItem("1 minute");
			}
			cmbDuration.AddItem("5 minutes");
			cmbDuration.AddItem("10 minutes");
			cmbDuration.AddItem("15 minutes");
			cmbDuration.AddItem("30 minutes");
			cmbDuration.AddItem("1 hour");
			cmbDuration.AddItem("2 hours");
			cmbDuration.AddItem("4 hours");
			cmbDuration.AddItem("8 hours");
			cmbDuration.AddItem("0.5 day");
			cmbDuration.AddItem("1 day");
			cmbDuration.AddItem("2 days");
			cmbDuration.AddItem("3 days");
			cmbDuration.AddItem("4 days");
			cmbDuration.AddItem("1 week");
			cmbDuration.AddItem("2 weeks");
		}

		public static int CalcStandardDurations_0m_2wLong(string sDuration)
		{
			int CalcStandardDurations_0m_2wLong = 0;
			if (sDuration == "0 minutes")
			{
				CalcStandardDurations_0m_2wLong = 0;
			}
			else if (sDuration == "1 minute")
			{
				CalcStandardDurations_0m_2wLong = 1;
			}
			else if (sDuration == "5 minutes")
			{
				CalcStandardDurations_0m_2wLong = 5;
			}
			else if (sDuration == "10 minutes")
			{
				CalcStandardDurations_0m_2wLong = 10;
			}
			else if (sDuration == "15 minutes")
			{
				CalcStandardDurations_0m_2wLong = 15;
			}
			else if (sDuration == "30 minutes")
			{
				CalcStandardDurations_0m_2wLong = 30;
			}
			else if (sDuration == "1 hour")
			{
				CalcStandardDurations_0m_2wLong = 60;
			}
			else if (sDuration == "2 hours")
			{
				CalcStandardDurations_0m_2wLong = 60 * 2;
			}
			else if (sDuration == "4 hours")
			{
				CalcStandardDurations_0m_2wLong = 60 * 4;
			}
			else if (sDuration == "8 hours")
			{
				CalcStandardDurations_0m_2wLong = 60 * 8;
			}
			else if (sDuration == "0.5 day")
			{
				CalcStandardDurations_0m_2wLong = 60 * 12;
			}
			else if (sDuration == "1 day")
			{
				CalcStandardDurations_0m_2wLong = 60 * 24;
			}
			else if (sDuration == "2 days")
			{
				CalcStandardDurations_0m_2wLong = 60 * 24 * 2;
			}
			else if (sDuration == "3 days")
			{
				CalcStandardDurations_0m_2wLong = 60 * 24 * 3;
			}
			else if (sDuration == "4 days")
			{
				CalcStandardDurations_0m_2wLong = 60 * 24 * 4;
			}
			else if (sDuration == "1 week")
			{
				CalcStandardDurations_0m_2wLong = 60 * 24 * 7;
			}
			else if (sDuration == "2 weeks")
			{
				CalcStandardDurations_0m_2wLong = 60 * 24 * 7 * 2;
			}
			return CalcStandardDurations_0m_2wLong;
		}

		public static string CalcStandardDurations_0m_2wString(int lDuration)
		{
			string CalcStandardDurations_0m_2wString = "";
			switch (lDuration)
			{
				case 0:
					{
						CalcStandardDurations_0m_2wString = "0 minutes";
						break;
					}
				case 1:
					{
						CalcStandardDurations_0m_2wString = "1 minute";
						break;
					}
				case 5:
					{
						CalcStandardDurations_0m_2wString = "5 minutes";
						break;
					}
				case 10:
					{
						CalcStandardDurations_0m_2wString = "10 minutes";
						break;
					}
				case 15:
					{
						CalcStandardDurations_0m_2wString = "15 minutes";
						break;
					}
				case 30:
					{
						CalcStandardDurations_0m_2wString = "30 minutes";
						break;
					}
				case 60:
					{
						CalcStandardDurations_0m_2wString = "1 hour";
						break;
					}
				case (60 * 2):
					{
						CalcStandardDurations_0m_2wString = "2 hours";
						break;
					}
				case (60 * 4):
					{
						CalcStandardDurations_0m_2wString = "4 hours";
						break;
					}
				case (60 * 8):
					{
						CalcStandardDurations_0m_2wString = "8 hours";
						break;
					}
				case (60 * 12):
					{
						CalcStandardDurations_0m_2wString = "0.5 day";
						break;
					}
				case (60 * 24):
					{
						CalcStandardDurations_0m_2wString = "1 day";
						break;
					}
				case (60 * 24 * 2):
					{
						CalcStandardDurations_0m_2wString = "2 days";
						break;
					}
				case (60 * 24 * 3):
					{
						CalcStandardDurations_0m_2wString = "3 days";
						break;
					}
				case (60 * 24 * 4):
					{
						CalcStandardDurations_0m_2wString = "4 days";
						break;
					}
				case (60 * 24 * 7):
					{
						CalcStandardDurations_0m_2wString = "1 week";
						break;
					}
				case (60 * 24 * 7 * 2):
					{
						CalcStandardDurations_0m_2wString = "2 weeks";
						break;
					}
			}
			//end switch
			return CalcStandardDurations_0m_2wString;
		}
		// vbPorter upgrade warning: pnMinutes As int	OnWrite(int, double)
		public static bool ParseTimeDuration(string strTime, ref int pnMinutes)
		{
			bool ParseTimeDuration = false;
			pnMinutes = 0;
			ParseTimeDuration = false;
			int nI, nLen;
			int nMeasureStart, nFIdx = 0;
			string strChI = "";
			strTime = fecherFoundation.Strings.Trim(strTime);
			nLen = strTime.Length;
			if (nLen == 0)
			{
				return ParseTimeDuration;
			}
			// ------------------------------------------
			nMeasureStart = -1;
			for (nI = 1; nI <= nLen; nI++)
			{
				strChI = Strings.Mid(strTime, nI, 1);
				nFIdx = Strings.InStr(1, "-+.,0123456789", strChI, CompareConstants.vbBinaryCompare);
				if (nFIdx <= 0)
				{
					nMeasureStart = nI;
					break;
				}
			}
			string strNumber = "", strMeasure = "";
			int nMultiplier;
			if (nMeasureStart > 0)
			{
				strNumber = Strings.Left(strTime, nMeasureStart - 1);
				strMeasure = Strings.Mid(strTime, nMeasureStart);
				strMeasure = fecherFoundation.Strings.Trim(strMeasure);
			}
			else
			{
				strNumber = strTime;
			}
			if (strNumber.Length == 0)
			{
				return ParseTimeDuration;
			}
			string strM0;
			strM0 = Strings.Left(strMeasure, 1);
			nMultiplier = 1;
			if (strM0 == "m" || strM0 == "M")
			{
				nMultiplier = 1;
			}
			else if (strM0 == "h" || strM0 == "H")
			{
				nMultiplier = 60;
			}
			else if (strM0 == "d" || strM0 == "D")
			{
				nMultiplier = 60 * 24;
			}
			else if (strM0 == "w" || strM0 == "W")
			{
				nMultiplier = 60 * 24 * 7;
			}
			double dblTime;
			dblTime = Conversion.Val(strNumber);
			pnMinutes = FCConvert.ToInt32(dblTime * nMultiplier);
			ParseTimeDuration = true;
			return ParseTimeDuration;
		}

		public static string FormatTimeDuration(int nMinutes, bool bAprox)
		{
			string FormatTimeDuration = "";
			// vbPorter upgrade warning: nWeeks As int	OnWriteFCConvert.ToDouble(
			// vbPorter upgrade warning: nDays As int	OnWriteFCConvert.ToDouble(
			// vbPorter upgrade warning: nHours As int	OnWriteFCConvert.ToDouble(
			int nWeeks, nDays, nHours;
			nWeeks = FCConvert.ToInt32(FCConvert.ToDouble(nMinutes) / (7 * 24 * 60));
			nDays = FCConvert.ToInt32(FCConvert.ToDouble(nMinutes) / (24 * 60));
			nHours = FCConvert.ToInt32(nMinutes / 60.0);
			string strDuration = "";
			if ((bAprox || (nMinutes % (7 * 24 * 60)) == 0) && nWeeks > 0)
			{
				strDuration = FCConvert.ToString(nWeeks) + " week" + (nWeeks > 1 ? "s" : "");
			}
			else if ((bAprox || (nMinutes % (24 * 60)) == 0) && nDays > 0)
			{
				strDuration = FCConvert.ToString(nDays) + " day" + (nDays > 1 ? "s" : "");
			}
			else if ((bAprox || (nMinutes % 60) == 0) && nHours > 0)
			{
				strDuration = FCConvert.ToString(nHours) + " hour" + (nHours > 1 ? "s" : "");
			}
			else
			{
				strDuration = FCConvert.ToString(nMinutes) + " minute" + (nMinutes > 1 ? "s" : "");
			}
			FormatTimeDuration = strDuration;
			return FormatTimeDuration;
		}
	}
}
