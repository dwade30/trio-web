﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmDynamicReport.
	/// </summary>
	partial class frmDynamicReport
	{
		public FCGrid GridProperties;
		public FCGrid Grid;
		public fecherFoundation.FCRichTextBox rtbData;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuLoadReport;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.GridProperties = new fecherFoundation.FCGrid();
			this.Grid = new fecherFoundation.FCGrid();
			this.rtbData = new fecherFoundation.FCRichTextBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLoadReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdLoad = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridProperties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rtbData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 568);
			this.BottomPanel.Size = new System.Drawing.Size(969, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridProperties);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.rtbData);
			this.ClientArea.Size = new System.Drawing.Size(969, 508);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdLoad);
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(969, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoad, 0);
			// 
			// GridProperties
			// 
			this.GridProperties.Cols = 10;
			this.GridProperties.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridProperties.Location = new System.Drawing.Point(610, 381);
			this.GridProperties.Name = "GridProperties";
			this.GridProperties.ReadOnly = false;
			this.GridProperties.Rows = 50;
			this.GridProperties.ShowFocusCell = false;
			this.GridProperties.Size = new System.Drawing.Size(300, 145);
			this.GridProperties.TabIndex = 2;
			this.GridProperties.Visible = false;
			this.GridProperties.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridProperties_ValidateEdit);
			// 
			// Grid
			// 
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid.Cols = 4;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.Location = new System.Drawing.Point(30, 381);
			this.Grid.Name = "Grid";
			this.Grid.RowHeadersVisible = false;
			this.Grid.Rows = 1;
			this.Grid.ShowFocusCell = false;
			this.Grid.Size = new System.Drawing.Size(888, 108);
			this.Grid.TabIndex = 1;
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// rtbData
			// 
			this.rtbData.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.rtbData.Location = new System.Drawing.Point(30, 30);
			this.rtbData.Name = "rtbData";
			this.rtbData.Size = new System.Drawing.Size(888, 331);
			this.rtbData.TabIndex = 3;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuDelete,
            this.mnuClear,
            this.mnuLoadReport,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 0;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete a document";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 1;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuLoadReport
			// 
			this.mnuLoadReport.Index = 2;
			this.mnuLoadReport.Name = "mnuLoadReport";
			this.mnuLoadReport.Text = "Load";
			this.mnuLoadReport.Click += new System.EventHandler(this.mnuLoadReport_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 3;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 4;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 5;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 6;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 7;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(462, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.Location = new System.Drawing.Point(801, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(134, 24);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete a document";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(748, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(50, 24);
			this.cmdClear.TabIndex = 2;
			this.cmdClear.Text = "Clear";
			this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// cmdLoad
			// 
			this.cmdLoad.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoad.Location = new System.Drawing.Point(697, 29);
			this.cmdLoad.Name = "cmdLoad";
			this.cmdLoad.Size = new System.Drawing.Size(47, 24);
			this.cmdLoad.TabIndex = 3;
			this.cmdLoad.Text = "Load";
			this.cmdLoad.Click += new System.EventHandler(this.mnuLoadReport_Click);
			// 
			// frmDynamicReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(969, 676);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmDynamicReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmDynamicReport_Load);
			this.Resize += new System.EventHandler(this.frmDynamicReport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDynamicReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridProperties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rtbData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdDelete;
		private FCButton cmdClear;
		private FCButton cmdLoad;
	}
}
