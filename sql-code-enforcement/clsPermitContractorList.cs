//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsPermitContractorList
	{
		//=========================================================
		private FCCollection ContractorList = new FCCollection();
		private int intCurrentIndex;
		private int lngPermitID;

		public int PermitID
		{
			set
			{
				lngPermitID = value;
			}
			get
			{
				int PermitID = 0;
				PermitID = lngPermitID;
				return PermitID;
			}
		}

		public void AddContractor(string strName, string strAddress1, string strAddress2, string strCity, string strState, string strZip, string strZip4, string strEmail, string strNote, int lngContractorID, int lngAutoID)
		{
			clsPermitContractor tPC = new clsPermitContractor();
			tPC.Deleted = false;
			tPC.Address1 = strAddress1;
			tPC.Address2 = strAddress2;
			tPC.ID = lngAutoID;
			tPC.City = strCity;
			tPC.ContractorID = lngContractorID;
			tPC.ContractorName = strName;
			tPC.Note = strNote;
			tPC.PermitID = lngPermitID;
			tPC.Zip = strZip;
			tPC.Zip4 = strZip4;
			tPC.State = strState;
			tPC.Email = strEmail;
			InsertContractor(ref tPC);
		}

		public void InsertContractor(ref clsPermitContractor tPC)
		{
			ContractorList.Add(tPC);
			intCurrentIndex = ContractorList.Count;
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsPermitContractor GetCurrentContractor
		{
			get
			{
				clsPermitContractor GetCurrentContractor = null;
				clsPermitContractor tPC;
				tPC = null;
				if (!FCUtils.IsEmpty(ContractorList))
				{
					if (intCurrentIndex > 0)
					{
						if (!(ContractorList[intCurrentIndex] == null))
						{
							tPC = ContractorList[intCurrentIndex];
						}
					}
				}
				GetCurrentContractor = tPC;
				return GetCurrentContractor;
			}
		}
		// vbPorter upgrade warning: intValue As int	OnWriteFCConvert.ToDouble(
		public void SetContractorByIndex(int intValue)
		{
			clsPermitContractor tPC;
			tPC = null;
			if (!FCUtils.IsEmpty(ContractorList))
			{
				if (intValue > 0 && intValue <= ContractorList.Count)
				{
					tPC = ContractorList[intValue];
					if (!(tPC == null))
					{
						intCurrentIndex = intValue;
					}
				}
			}
		}

		public clsPermitContractor Get_GetContractorByIndex(int intValue)
		{
			clsPermitContractor GetContractorByIndex = null;
			clsPermitContractor tPC;
			tPC = null;
			if (!FCUtils.IsEmpty(ContractorList))
			{
				if (intValue > 0 && intValue <= ContractorList.Count)
				{
					tPC = ContractorList[intValue];
				}
			}
			GetContractorByIndex = tPC;
			return GetContractorByIndex;
		}

		public int GetContractorCount
		{
			get
			{
				int GetContractorCount = 0;
				int intReturn;
				intReturn = 0;
				if (!FCUtils.IsEmpty(ContractorList))
				{
					intReturn = ContractorList.Count;
				}
				GetContractorCount = intReturn;
				return GetContractorCount;
			}
		}

		public void Clear()
		{
			int x;
			if (!FCUtils.IsEmpty(ContractorList))
			{
				for (x = 1; x <= ContractorList.Count; x++)
				{
					ContractorList.Remove(1);
				}
				// x
			}
			intCurrentIndex = -1;
		}

		public void ClearDeleted()
		{
			int x;
			if (!FCUtils.IsEmpty(ContractorList))
			{
				for (x = ContractorList.Count; x >= 1; x--)
				{
					if (!(ContractorList[x] == null))
					{
						if (ContractorList[x].Deleted)
						{
							ContractorList.Remove(x);
						}
					}
				}
				// x
				if (intCurrentIndex > ContractorList.Count)
				{
					MoveFirst();
				}
			}
		}

		public void MoveFirst()
		{
			if (!(ContractorList == null))
			{
				if (!FCUtils.IsEmpty(ContractorList))
				{
					if (ContractorList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(ContractorList))
			{
				if (intCurrentIndex > ContractorList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= ContractorList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > ContractorList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (ContractorList[intCurrentIndex] == null)
						{
						}
						else if (ContractorList[intCurrentIndex].Deleted)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}

		public void LoadContractors()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsPermitContractor tPC;
			Clear();
			rsLoad.OpenRecordset("select * from PermitContractors where permitidNum = " + FCConvert.ToString(lngPermitID) + " order by ordernumber", modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				tPC = new clsPermitContractor();
				tPC.Address1 = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractoraddress1"));
				tPC.Address2 = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractoraddress2"));
				tPC.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
				tPC.City = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractorcity"));
				tPC.ContractorID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("contractorid"));
				tPC.ContractorName = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractorname"));
				tPC.Deleted = false;
				tPC.Note = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractornote"));
				tPC.OrderNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ordernumber"));
				tPC.PermitID = lngPermitID;
				tPC.State = FCConvert.ToString(rsLoad.Get_Fields_String("PermitContractorState"));
				tPC.Email = FCConvert.ToString(rsLoad.Get_Fields_String("PermitContractorEmail"));
				tPC.Zip = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractorzip"));
				tPC.Zip4 = FCConvert.ToString(rsLoad.Get_Fields_String("permitcontractorzip4"));
				tPC.LoadPhoneNumbers();
				InsertContractor(ref tPC);
				rsLoad.MoveNext();
			}
		}

		public bool SaveContractors()
		{
			bool SaveContractors = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsPermitContractor tPC;
				int intIndex;
				SaveContractors = false;
				if (!FCUtils.IsEmpty(ContractorList))
				{
					for (intIndex = 1; intIndex <= ContractorList.Count; intIndex++)
					{
						tPC = Get_GetContractorByIndex(intIndex);
						if (!(tPC == null))
						{
							if (!tPC.Deleted)
							{
								rsSave.OpenRecordset("select * from permitcontractors where ID = " + FCConvert.ToString(tPC.ID), modGlobalVariables.Statics.strCEDatabase);
								if (!rsSave.EndOfFile())
								{
									rsSave.Edit();
								}
								else
								{
									rsSave.AddNew();
								}
								rsSave.Set_Fields("PermitIDNum", lngPermitID);
								rsSave.Set_Fields("ContractorID", tPC.ContractorID);
								rsSave.Set_Fields("PermitContractorName", tPC.ContractorName);
								rsSave.Set_Fields("PermitContractorAddress1", tPC.Address1);
								rsSave.Set_Fields("PermitContractorAddress2", tPC.Address2);
								rsSave.Set_Fields("PermitContractorCity", tPC.City);
								rsSave.Set_Fields("PermitContractorState", tPC.State);
								rsSave.Set_Fields("Permitcontractorzip", tPC.Zip);
								rsSave.Set_Fields("PermitContractorzip4", tPC.Zip4);
								rsSave.Set_Fields("PermitContractorEmail", tPC.Email);
								rsSave.Set_Fields("PermitContractorNote", tPC.Note);
								rsSave.Set_Fields("OrderNumber", tPC.OrderNumber);
								rsSave.Update();
								tPC.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
								tPC.SavePhoneNumbers();
							}
							else
							{
								if (tPC.ID > 0)
								{
									rsSave.Execute("delete from phonenumbers where parentid = " + FCConvert.ToString(tPC.ID) + " and phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPEPERMITCONTRACTOR), modGlobalVariables.Statics.strCEDatabase);
									rsSave.Execute("delete from permitcontractors where ID = " + FCConvert.ToString(tPC.ID), modGlobalVariables.Statics.strCEDatabase);
								}
							}
						}
					}
					// intIndex
					ClearDeleted();
				}
				SaveContractors = true;
				return SaveContractors;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveContractors", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveContractors;
		}

		public clsPermitContractorList Copy()
		{
			clsPermitContractorList Copy = null;
			clsPermitContractorList tList = new clsPermitContractorList();
			int intIndex;
			clsPermitContractor tPC;
			clsPermitContractor nPC;
			clsPhoneList tpL;
			if (!FCUtils.IsEmpty(ContractorList))
			{
				for (intIndex = 1; intIndex <= ContractorList.Count; intIndex++)
				{
					tPC = Get_GetContractorByIndex(intIndex);
					if (!(tPC == null))
					{
						nPC = new clsPermitContractor();
						nPC.Address1 = tPC.Address1;
						nPC.Address2 = tPC.Address2;
						nPC.ID = tPC.ID;
						nPC.City = tPC.City;
						nPC.ContractorID = tPC.ContractorID;
						nPC.ContractorName = tPC.ContractorName;
						nPC.Deleted = tPC.Deleted;
						nPC.Email = tPC.Email;
						nPC.Note = tPC.Note;
						nPC.OrderNumber = tPC.OrderNumber;
						nPC.PermitID = tPC.PermitID;
						tpL = tPC.PhoneNumberList.Copy();
						if (!(tpL == null))
						{
							nPC.SetPhoneNumberList(ref tpL);
						}
						nPC.State = tPC.State;
						nPC.Zip = tPC.Zip;
						nPC.Zip4 = tPC.Zip4;
						tList.InsertContractor(ref nPC);
					}
				}
				// intIndex
			}
			Copy = tList;
			return Copy;
		}

		public void ReOrder()
		{
			if (!FCUtils.IsEmpty(ContractorList))
			{
				clsPermitContractor tPC;
				clsPermitContractor nPC;
				int intIndex;
				int x;
				if (ContractorList.Count > 1)
				{
					for (x = 1; x <= ContractorList.Count; x++)
					{
						for (intIndex = 1; intIndex <= ContractorList.Count - 1; intIndex++)
						{
							tPC = Get_GetContractorByIndex(intIndex);
							nPC = Get_GetContractorByIndex((intIndex + 1));
							if (!(tPC == null) && !(nPC == null))
							{
								if ((tPC.OrderNumber > nPC.OrderNumber || tPC.Deleted) && !nPC.Deleted)
								{
									// order by ordernumber and by not deleted
									ContractorList.Remove(intIndex + 1);
									ContractorList.Add(nPC, null, intIndex);
								}
							}
						}
						// intIndex
					}
					// x
					MoveFirst();
				}
			}
		}
	}
}
