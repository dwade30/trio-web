//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptScheduledEvents.
	/// </summary>
	public partial class rptScheduledEvents : BaseSectionReport
	{
		public rptScheduledEvents()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Scheduled Events";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptScheduledEvents InstancePtr
		{
			get
			{
				return (rptScheduledEvents)Sys.GetInstance(typeof(rptScheduledEvents));
			}
		}

		protected rptScheduledEvents _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptScheduledEvents	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		// vbPorter upgrade warning: clUsers As Collection	OnRead(string)
		public void Init(ref FCCollection clUsers, string strFromDate, string strToDate, bool boolIncludePrivate, string strOrderBy)
		{
			string strSQL;
			string strUsers;
			// vbPorter upgrade warning: strTemp As string	OnWrite(Collection)
			string strTemp = "";
			string strOr;
			string strAnd;
			int x;
			strSQL = "Select * from NewTasks where ";
			strOr = "";
			strUsers = "";
			strAnd = "";
			for (x = 1; x <= clUsers.Count; x++)
			{
				strTemp = clUsers[x];
				strUsers += strOr + "TaskPerson = '" + strTemp.Replace("'", "''") + "'";
				strOr = " or ";
			}
			// x
			if (strUsers != "")
			{
				strUsers = " (" + strUsers + ")";
				strSQL += strAnd + strUsers;
				strAnd = " and ";
			}
			if (!boolIncludePrivate)
			{
				strSQL += strAnd + " not isnull(isprivate,0) = 1 ";
				strAnd = " and ";
			}
			strSQL += strAnd + " startdatetime >= '" + strFromDate + "' and startdatetime <= '" + strToDate + "'";
			if (strOrderBy != "")
			{
				if (fecherFoundation.Strings.LCase(strOrderBy) == "user")
				{
					strSQL += " order by taskperson,startdatetime";
				}
				else
				{
					strSQL += " order by startdatetime,taskperson";
				}
			}
			else
			{
				strSQL += " order by startdatetime,taskperson";
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strGNDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Scheduled Events");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtPage.Text = "Page 1";
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				double dblHours = 0;
				// vbPorter upgrade warning: lngMinutes As int	OnWriteFCConvert.ToDouble(
				int lngMinutes = 0;
				double dblDays = 0;
				// vbPorter upgrade warning: dblTemp As double	OnWrite(long, double)	OnReadFCConvert.ToInt32(
				double dblTemp = 0;
				txtEventDate.Text = Strings.Format(rsReport.Get_Fields_DateTime("startdatetime"), "MM/dd/yyyy");
				string strTemp = "";
				if (!FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("isalldayevent")))
				{
					txtEventTime.Text = Strings.Format(rsReport.Get_Fields_DateTime("startdatetime"), "hh:mm tt");
					dblTemp = fecherFoundation.DateAndTime.DateDiff("n", (DateTime)rsReport.Get_Fields_DateTime("startdatetime"), (DateTime)rsReport.Get_Fields_DateTime("enddatetime"));
					while (dblTemp >= 1440)
					{
						dblDays += 1;
						dblTemp -= 1440;
					}
					while (dblTemp >= 60)
					{
						dblHours += 1;
						dblTemp -= 60;
					}
					lngMinutes = FCConvert.ToInt32(dblTemp);
					strTemp = "";
					if (dblDays > 0)
					{
						strTemp += FCConvert.ToString(dblDays) + " days ";
					}
					if (dblHours > 0)
					{
						strTemp += FCConvert.ToString(dblHours) + " hrs ";
					}
					if (lngMinutes > 0)
					{
						strTemp += FCConvert.ToString(lngMinutes) + " mins";
					}
					txtEventDuration.Text = strTemp;
				}
				else
				{
					txtEventTime.Text = "";
					dblTemp = fecherFoundation.DateAndTime.DateDiff("d", (DateTime)rsReport.Get_Fields_DateTime("startdatetime"), (DateTime)rsReport.Get_Fields_DateTime("enddatetime"));
					dblTemp += 1;
					if (dblTemp <= 1)
					{
						txtEventDuration.Text = "All Day";
					}
					else
					{
						txtEventDuration.Text = FCConvert.ToString(dblTemp) + " days";
					}
				}
				txtUser.Text = rsReport.Get_Fields_String("taskperson");
				if (!FCConvert.ToBoolean(rsReport.Get_Fields_Boolean("isprivate")) || fecherFoundation.Strings.LCase(FCConvert.ToString(rsReport.Get_Fields_String("taskperson"))) == fecherFoundation.Strings.LCase(FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID())))
				{
					txtLocation.Text = rsReport.Get_Fields_String("location");
					txtDescription.Text = rsReport.Get_Fields_String("subject");
				}
				else
				{
					txtLocation.Text = "";
					txtDescription.Text = "Unavailable";
				}
				rsReport.MoveNext();
			}
		}

		
	}
}
