﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for srptViolation.
	/// </summary>
	public partial class srptViolation : FCSectionReport
	{
		public srptViolation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptViolation InstancePtr
		{
			get
			{
				return (srptViolation)Sys.GetInstance(typeof(srptViolation));
			}
		}

		protected srptViolation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptViolation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsActivityList alList = new clsActivityList();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = alList.GetCurrentIndex() < 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL = "";
			int lngID;
			int lngReturn;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			lngReturn = alList.LoadList(0, modCEConstants.CNSTCODETYPEVIOLATION, lngID);
			if (lngReturn != 0)
			{
				this.Stop();
				return;
			}
			else
			{
				alList.MoveFirst();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsActivity tAct;
			tAct = alList.GetCurrentActivity();
			if (!(tAct == null))
			{
				txtActivityDate.Text = FCConvert.ToString(tAct.ActivityDate);
				txtDescription.Text = tAct.Description;
				txtType.Text = tAct.ActivityType;
			}
			alList.MoveNext();
		}

		
	}
}
