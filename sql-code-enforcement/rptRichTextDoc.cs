﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Collections.ObjectModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Linq;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptRichTextDoc.
	/// </summary>
	public partial class rptRichTextDoc : BaseSectionReport
	{
		public rptRichTextDoc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptRichTextDoc InstancePtr
		{
			get
			{
				return (rptRichTextDoc)Sys.GetInstance(typeof(rptRichTextDoc));
			}
		}

		protected rptRichTextDoc _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRichTextDoc	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolAttachDocument;
		private string strDocName = string.Empty;
		private int lngParentID;
		private int intParentType;
		private int lngAccountNumber;
		private string strDocDescription = string.Empty;

		public void Init(bool boolAttach, string strRichText, string strFileName, int ParentID, int ParentType, int lngAccount, string strDescription)
		{
			//FC:FINAL:DSE WordWrap is not functioning in RichTextBox
			//RichEdit1.Text = strRichText;
			RichEdit1.SetHtmlText(strRichText);
			lngAccountNumber = lngAccount;
			strDocDescription = strDescription;
			lngParentID = ParentID;
			intParentType = ParentType;
			boolAttachDocument = boolAttach;
			strDocName = strFileName;
			frmReportViewer.InstancePtr.Init(this, strFileTitle: "Document");
		}

        public bool CheckForDocs(int referenceId, string referenceType)
        {
            var docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
            var docList = new Collection<CentralDocumentHeader>();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { DataGroup = "Clerk", ReferenceId = referenceId, ReferenceType = referenceType });            
            return docList.Count > 0;
        }
        private void AddAttachment(byte[] documentBytes)
		{
			int lngAutoID = 0;
			clsActivity tAct = new clsActivity();
			tAct.Account = lngAccountNumber;
			tAct.ActivityDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			tAct.ActivityType = "Document";
			tAct.Description = "Created Document";
			tAct.Detail = "";
			tAct.ParentID = lngParentID;
			tAct.ParentType = intParentType;
			tAct.ReferenceID = 0;
			tAct.ReferenceType = 0;
			if (tAct.SaveActivity() == 0)
			{
				lngAutoID = tAct.ID;
				if (lngAutoID > 0)
				{
                    var id = StaticSettings.GlobalCommandDispatcher.Send(new CreateCentralDocument(@"application / pdf", "Document",
                        "Activity", lngAutoID, "", "CodeEnforcement", Guid.NewGuid(), documentBytes, "", DateTime.Now)).Result;
				}
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (boolAttachDocument)
			{
				if (strDocName != "")
				{
                    //FCFileSystem fso = new FCFileSystem();
					FileInfo fl;
                    //FC:FINAL:IPI - #i1706 - on web the file will be downloaded
                    GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
					a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                    string fileName = strDocName;
                   // a.Export(this.Document, fileName);
                    
                    using (MemoryStream stream = new MemoryStream())
                    {
                        a.Export(this.Document, stream);
                        stream.Position = 0;
                        var bytes = stream.ToArray();                        
                       AddAttachment(bytes);
					}					
				}
			}
		}

		
	}
}
