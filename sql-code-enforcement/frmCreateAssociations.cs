//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmCreateAssociations : BaseForm
	{
		public frmCreateAssociations()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCreateAssociations InstancePtr
		{
			get
			{
				return (frmCreateAssociations)Sys.GetInstance(typeof(frmCreateAssociations));
			}
		}

		protected frmCreateAssociations _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLCHECK = 0;
		const int CNSTGRIDCOLCEACCT = 1;
		const int CNSTGRIDCOLMAPLOT = 3;
		const int CNSTGRIDCOLLOCATION = 4;
		const int CNSTGRIDCOLREACCT = 2;
		const int CNSTGRIDCOLCENAME = 5;
		const int CNSTGRIDCOLRENAME = 6;
		const int CNSTGRIDCOLHIDDENLOCATION = 7;
		// not visible but used in sorting
		private bool boolDescend;

		public void Init()
		{
			//FC:FINAL:CHN - i.issue #1662: Add Header text to new design.
			this.HeaderText.Text = this.Text;
			this.Show(App.MainForm);
		}

		private void chkCreateNew_CheckedChanged(object sender, System.EventArgs e)
		{
			ReLoad();
		}

		private void cmdDeselectAll_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCHECK, FCConvert.ToString(false));
			}
			// lngRow
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCHECK, FCConvert.ToString(true));
			}
			// lngRow
		}

		private void frmCreateAssociations_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCreateAssociations_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreateAssociations properties;
			//frmCreateAssociations.FillStyle	= 0;
			//frmCreateAssociations.ScaleWidth	= 9300;
			//frmCreateAssociations.ScaleHeight	= 7800;
			//frmCreateAssociations.LinkTopic	= "Form2";
			//frmCreateAssociations.LockControls	= -1  'True;
			//frmCreateAssociations.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			boolDescend = false;
			ReLoad();
		}

		private void frmCreateAssociations_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterSort(object sender, EventArgs e)
		{
			switch (Grid.Col)
			{
				case CNSTGRIDCOLLOCATION:
					{
						Grid.Col = CNSTGRIDCOLHIDDENLOCATION;
						Grid.Col = CNSTGRIDCOLHIDDENLOCATION;
						if (!boolDescend)
						{
							Grid.Sort = FCGrid.SortSettings.flexSortStringAscending;
						}
						else
						{
							Grid.Sort = FCGrid.SortSettings.flexSortStringDescending;
						}
						boolDescend = !boolDescend;
						break;
					}
			}
			//end switch
		}

		//private void Grid_BeforeRowColChange(object sender, System.EventArgs e)
		private void Grid_RowColChange(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - restore functionality for disabling editing if current cell isn't CNSTGRIDCOLCHECK
			//switch (Grid.newCol) {
			switch (Grid.Col)
			{
				case CNSTGRIDCOLCHECK:
					{
						Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						Grid.Editable = FCGrid.EditableSettings.flexEDNone;
						//e.Cancel = true;
						break;
					}
			} //end switch
		}
		//
		// Private Sub Grid_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
		// Dim lngRow As Long
		// Dim lngCol As Long
		//
		// lngRow = Grid.MouseRow
		// lngCol = Grid.MouseCol
		// If lngRow < 1 Then Exit Sub
		// If lngCol = CNSTGRIDCOLCHECK Then
		// Grid.TextMatrix(lngRow, lngCol) = Not CBool(Grid.TextMatrix(lngRow, lngCol))
		// End If
		// End Sub
		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLHIDDENLOCATION, true);
			Grid.TextMatrix(0, CNSTGRIDCOLCEACCT, "CE");
			Grid.TextMatrix(0, CNSTGRIDCOLREACCT, "RE");
			Grid.ColAlignment(CNSTGRIDCOLCEACCT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			Grid.ColAlignment(CNSTGRIDCOLREACCT, FCGrid.AlignmentSettings.flexAlignRightCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLCEACCT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDCOLREACCT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.TextMatrix(0, CNSTGRIDCOLMAPLOT, "Map / Lot");
			Grid.TextMatrix(0, CNSTGRIDCOLLOCATION, "Location");
			Grid.TextMatrix(0, CNSTGRIDCOLCENAME, "CE Name");
			Grid.TextMatrix(0, CNSTGRIDCOLRENAME, "RE Name");
			Grid.ColAlignment(CNSTGRIDCOLCHECK, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColDataType(CNSTGRIDCOLCHECK, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.Col = 0;
			Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLCHECK, FCConvert.ToInt32(0.03 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCEACCT, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLREACCT, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMAPLOT, FCConvert.ToInt32(0.16 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLLOCATION, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLCENAME, FCConvert.ToInt32(0.22 * GridWidth));
		}

		private void mnuReload_Click(object sender, System.EventArgs e)
		{
			ReLoad();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (MakeAssociations())
			{
				//FC:DDU:REDESIGN - Don't close form
                //FC:FINAL:BSE #3753 form should close after saving
				Close();
			}
		}

		private void optMatch_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			ReLoad();
		}

		private void optMatch_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbMatch.SelectedIndex;
			optMatch_CheckedChanged(index, sender, e);
		}

		private void ReLoad()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				int intMatchBy;
				// 0 is maplot 1 is location
				bool boolMakeNew = false;
				string strSQL = "";
				string strREDBName;
				strREDBName = rsLoad.Get_GetFullDBName("RealEstate");
				string strREMasterJoin;
				strREMasterJoin = modCE.GetREMasterJoinForJoin();
				if (cmbMatch.Text == "Map / Lot")
				{
					intMatchBy = 0;
				}
				else
				{
					intMatchBy = 1;
				}
				if (chkCreateNew.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolMakeNew = true;
				}
				else
				{
					boolMakeNew = false;
				}
				Grid.Rows = 1;
				switch (intMatchBy)
				{
					case 0:
						{
							// maplot
							// strSQL = "select * from " & strREDBName & ".dbo.master left join cemaster on (master.RSmaplot = cemaster.maplot) where not rsdeleted = 1  and rscard = 1 and RSmaplot <> '' and isnull(cemaster.rsaccount,0) = 0 order by rsmaplot,RSNAME"
							strSQL = "select *,remj.rsaccount as RErsaccount from " + strREMasterJoin + " left join cemaster on (remj.RSmaplot = cemaster.maplot) where not rsdeleted = 1  and rscard = 1 and RSmaplot <> '' and isnull(cemaster.rsaccount,0) = 0 order by rsmaplot,RSNAME";
							break;
						}
					case 1:
						{
							// location
							strSQL = "select * from " + strREDBName + ".dbo.master left join cemaster on (isnull(master.rslocnumalph,0) = cemaster.streetnumber) and (master.rslocapt  = cemaster.apt ) and (master.rslocstreet = cemaster.streetname) where not rsdeleted = 1 and rscard = 1 and rslocstreet <> '' and isnull(rslocnumalph ,0) > 0 and isnull(cemaster.rsaccount,0) = 0 order by rslocstreet,isnull(rslocnumalph ,0)";
							break;
						}
				}
				//end switch
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					if ((Conversion.Val(rsLoad.Get_Fields_Int32("ceaccount")) > 0 || boolMakeNew) && rsLoad.Get_Fields_Boolean("deleted") != true && Conversion.Val(rsLoad.Get_Fields("cemaster.rsaccount")) == 0)
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCHECK, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCEACCT, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ceaccount"))));
						if (Conversion.Val(rsLoad.Get_Fields_Int32("CEACCOUNT")) == 0)
							Grid.TextMatrix(lngRow, CNSTGRIDCOLCEACCT, "New");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLREACCT, FCConvert.ToString(rsLoad.Get_Fields("rersaccount")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(rsLoad.Get_Fields_String("rsmaplot")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCATION, fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("rslocnumalph") + " " + rsLoad.Get_Fields_String("rslocstreet")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLHIDDENLOCATION, fecherFoundation.Strings.Trim(rsLoad.Get_Fields_String("rslocstreet") + " " + Strings.Format(FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("rslocnumalph"))), "000000")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLCENAME, FCConvert.ToString(rsLoad.Get_Fields_String("name")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLRENAME, FCConvert.ToString(rsLoad.Get_Fields_String("rsname")));
					}
					rsLoad.MoveNext();
				}
				if (Grid.Rows > 1)
				{
					Grid.Col = 0;
					Grid.Row = 1;
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in ReLoad", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool MakeAssociations()
		{
			bool MakeAssociations = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				int lngCEAcct;
				clsCEMaster MastRec = new clsCEMaster();
				int lngMax;
				int lngRet = 0;
				MakeAssociations = false;
				if (Grid.Rows < 2)
				{
					MessageBox.Show("There are no associations to save", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				lngMax = Grid.Rows;
				frmWait.InstancePtr.Init("Associating Accounts");
				int tInt;
				tInt = 0;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					// frmWait.IncrementProgress (1)
					//App.DoEvents();
					tInt += 1;
					if (tInt > 20)
					{
						tInt = 0;
						//App.DoEvents();
					}
					if (FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLCHECK)))
					{
						lngRet = MastRec.Load(FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREACCT)))), false);
						if (lngRet == 0)
						{
							MastRec.CEAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCEACCT))));
							// MastRec.RSAccount = Val(GRID.TextMatrix(lngRow, CNSTGRIDCOLREACCT))
							MastRec.Save();
						}
						else if (lngRet < 0)
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show("Error associating RE account " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREACCT))));
							return MakeAssociations;
						}
						else
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show("RE Account " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLREACCT))) + " already associated with CE Account " + FCConvert.ToString(lngRet), "Already Associated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return MakeAssociations;
						}
					}
				}
				// lngRow
				MakeAssociations = true;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return MakeAssociations;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In MakeAssociations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeAssociations;
		}
	}
}
