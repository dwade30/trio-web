//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmCustomCEReport : BaseForm
	{
		public frmCustomCEReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomCEReport InstancePtr
		{
			get
			{
				return (frmCustomCEReport)Sys.GetInstance(typeof(frmCustomCEReport));
			}
		}

		protected frmCustomCEReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDFIELDSCOLLEAF = 0;
		const int CNSTGRIDFIELDSCOLCODE = 1;
		const int CNSTGRIDFIELDSCOLDESC = 2;
		const int CNSTGRIDFIELDSCOLOPENCODE = 3;
		const int CNSTGRIDFIELDSCOLDEFAULTJUSTIFICATION = 4;
		// vbPorter upgrade warning: dblSizeRatio As double	OnWrite(int, float)
		private double dblSizeRatio;
		private int lngReportID;

		public void Init(int lngRepID)
		{
			lngReportID = lngRepID;
            //FC:FINAL:AM: show as tab page
            //this.Show(FCForm.FormShowEnum.Modal);
            this.Show(App.MainForm);
		}

		private void frmCustomCEReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: lngRepID As int	OnWriteFCConvert.ToDouble(
		private void LoadReport(int lngRepID)
		{
			lngReportID = lngRepID;
			vsLayout.Rows = 1;
			vsLayout.Cols = 0;
			int lngMaxRow;
			int lngMaxCol;
			lngMaxRow = 0;
			lngMaxCol = -1;
			int lngRow = 0;
			int lngCol = 0;
			string strTemp = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from customreports where ID = " + FCConvert.ToString(lngReportID), modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				txtTitle.Text = FCConvert.ToString(rsLoad.Get_Fields("title"));
			}
			rsLoad.OpenRecordset("select * from CustomReportLayouts where reportid = " + FCConvert.ToString(lngReportID) + " order by rownumber,colnumber", modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				lngRow = FCConvert.ToInt32(rsLoad.Get_Fields("RowNumber"));
				lngCol = FCConvert.ToInt32(rsLoad.Get_Fields("colnumber"));
				if (lngRow > lngMaxRow)
				{
					while ((vsLayout.Rows - 1) < lngRow)
					{
						AddRow();
					}
					lngMaxRow = lngRow;
				}
				if (lngCol > lngMaxCol)
				{
					while ((vsLayout.Cols - 1) < lngCol)
					{
						AddColumn();
					}
					lngMaxCol = lngCol;
					vsLayout.ColWidth(lngCol, FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("colwidth")) * dblSizeRatio));
					vsLayout.ColData(lngCol, FCConvert.ToInt32(FCConvert.ToString(rsLoad.Get_Fields("colwidth"))));
				}
				strTemp = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("CodeType")) + "|" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("Opencode"))) + "|" + rsLoad.Get_Fields_String("Header") + "|" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("Justification"))) + "|" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("TotalOption"))));
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol, strTemp);
				vsLayout.TextMatrix(lngRow, lngCol, FCConvert.ToString(rsLoad.Get_Fields_String("Header")));
				rsLoad.MoveNext();
			}
		}

		private bool SaveReport(int lngSaveID)
		{
			bool SaveReport = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				bool boolNew;
				string strTables;
				boolNew = false;
				SaveReport = false;
				if (fecherFoundation.Strings.Trim(txtTitle.Text) == string.Empty)
				{
					MessageBox.Show("A report layout must have a title", "No Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveReport;
				}
				if (vsLayout.Rows < 2)
				{
					MessageBox.Show("A report layout must have at least one row of data");
					return SaveReport;
				}
				if (vsLayout.Cols < 1)
				{
					MessageBox.Show("A report layout must have at least one column of data");
					return SaveReport;
				}
				strTables = "";
				int lngRow;
				int lngCol;
				bool boolPropertyTable;
				bool boolPropertyOpens;
				bool boolContractor;
				bool boolContractorClass;
				bool boolPermit;
				bool boolPermitOpens;
				bool boolInspections;
				bool boolInspectionsOpens;
				bool boolContractorOpens;
				bool boolInspectors;
				string strTemp = "";
				// vbPorter upgrade warning: lngCode As int	OnWrite(string)
				int lngCode = 0;
				// vbPorter upgrade warning: lngOpenCode As int	OnWrite(string)
				int lngOpenCode = 0;
				// vbPorter upgrade warning: strAry As string()	OnRead(int, string)
				string[] strAry = null;
				boolPropertyTable = false;
				boolPropertyOpens = false;
				boolContractor = false;
				boolContractorClass = false;
				boolPermit = false;
				boolPermitOpens = false;
				boolInspections = false;
				boolInspectionsOpens = false;
				boolContractorOpens = false;
				boolInspectors = false;
				for (lngRow = 1; lngRow <= vsLayout.Rows - 1; lngRow++)
				{
					for (lngCol = 0; lngCol <= vsLayout.Cols - 1; lngCol++)
					{
						strTemp = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol));
						strAry = Strings.Split(strTemp, "|", -1, CompareConstants.vbTextCompare);
						lngCode = FCConvert.ToInt32(strAry[0]);
						lngOpenCode = FCConvert.ToInt32(strAry[1]);
						if (Conversion.Val(strAry[0]) >= 1 && Conversion.Val(strAry[0]) <= 12)
						{
							boolPropertyTable = true;
						}
						else if (Conversion.Val(strAry[0]) == 29)
						{
							boolPropertyOpens = true;
							boolPropertyTable = true;
						}
						else if ((Conversion.Val(strAry[0]) >= 13 && Conversion.Val(strAry[0]) <= 19) || (Conversion.Val(strAry[0]) == 33) || (Conversion.Val(strAry[0]) == 34))
						{
							boolPermit = true;
						}
						else if (Conversion.Val(strAry[0]) >= 20 && Conversion.Val(strAry[0]) <= 22)
						{
							boolInspections = true;
						}
						else if (Conversion.Val(strAry[0]) == 23)
						{
							boolInspections = true;
							boolInspectors = true;
						}
						else if (Conversion.Val(strAry[0]) >= 24 && Conversion.Val(strAry[0]) <= 28)
						{
							boolContractor = true;
							if (Conversion.Val(strAry[0]) == 26 || Conversion.Val(strAry[0]) == 28)
							{
								boolContractorClass = true;
							}
						}
						else if (Conversion.Val(strAry[0]) == 30)
						{
							boolContractorOpens = true;
							boolContractor = true;
						}
						else if (Conversion.Val(strAry[0]) == 31)
						{
							boolPermitOpens = true;
							boolPermit = true;
						}
						else if (Conversion.Val(strAry[0]) == 32)
						{
							boolInspectionsOpens = true;
							boolInspections = true;
						}
					}
					// lngCol
				}
				// lngRow
				strTemp = "";
				if (boolPropertyTable)
				{
					strTemp += "CEMaster,";
				}
				if (boolPermit)
				{
					strTemp += "Permits,";
				}
				if (boolPermit && boolContractor)
				{
					strTemp += "PermitContractors,";
				}
				if (boolContractorClass)
				{
					strTemp += "ClassCodes,";
				}
				if (boolContractor)
				{
					strTemp += "Contractors,";
				}
				if (boolInspections)
				{
					strTemp += "Inspections,";
				}
				if (boolInspectors)
				{
					strTemp += "Inspectors,";
				}
				if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				rsSave.OpenRecordset("select * from customreports where ID = " + FCConvert.ToString(lngSaveID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					boolNew = true;
				}
				lngReportID = lngSaveID;
				rsSave.Set_Fields("Title", fecherFoundation.Strings.Trim(txtTitle.Text));
				// build table list
				rsSave.Set_Fields("tablelist", strTemp);
				rsSave.Update();
				lngReportID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
				lngSaveID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
				int lngFieldID;
				string strDescription = "";
				int intJust = 0;
				int intTotal = 0;
				rsSave.Execute("delete from customreportlayouts where reportid = " + FCConvert.ToString(lngReportID), modGlobalVariables.Statics.strCEDatabase);
				rsSave.OpenRecordset("select * from customreportlayouts where ID = -1", modGlobalVariables.Statics.strCEDatabase);
				for (lngRow = 1; lngRow <= vsLayout.Rows - 1; lngRow++)
				{
					for (lngCol = 0; lngCol <= vsLayout.Cols - 1; lngCol++)
					{
						strTemp = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, lngCol));
						strAry = Strings.Split(strTemp, "|", -1, CompareConstants.vbTextCompare);
						lngCode = FCConvert.ToInt32(strAry[0]);
						lngOpenCode = FCConvert.ToInt32(strAry[1]);
						strDescription = strAry[2];
						intJust = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[3])));
						intTotal = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[4])));
						rsSave.AddNew();
						rsSave.Set_Fields("RowNumber", lngRow);
						rsSave.Set_Fields("ColNumber", lngCol);
						rsSave.Set_Fields("CodeType", lngCode);
						rsSave.Set_Fields("OpenCode", lngOpenCode);
						rsSave.Set_Fields("Header", strDescription);
						rsSave.Set_Fields("Justification", intJust);
						rsSave.Set_Fields("totaloption", intTotal);
						rsSave.Set_Fields("ColWidth", vsLayout.ColData(lngCol));
						rsSave.Set_Fields("ReportID", lngReportID);
						rsSave.Update();
					}
					// lngCol
				}
				// lngRow
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveReport = true;
				return SaveReport;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveReport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveReport;
		}

		private void GridFields_DblClick(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (vsLayout.Row < 1)
				return;
			if (vsLayout.Col < 0)
				return;
			if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLCODE)) > 0)
			{
				if (GridFields.RowOutlineLevel(GridFields.Row) < 1)
					return;
				// If optJust(0).Value Then
				// strTemp = "0"
				// ElseIf optJust(1).Value Then
				// strTemp = "1"
				// ElseIf optJust(2).Value Then
				// strTemp = "2"
				// End If
				strTemp = FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLDEFAULTJUSTIFICATION)));
				if (cmbTotal.Text == "None")
				{
					strTemp += "|0";
				}
				else if (cmbTotal.Text == "Total")
				{
					strTemp += "|1";
				}
				else
				{
					strTemp += "|2";
				}
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLCODE) + "|" + GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLOPENCODE) + "|" + GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLDESC) + "|" + strTemp);
				vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLDESC));
				txtCode.Text = GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLCODE);
				txtOpenCode.Text = GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLOPENCODE);
				txtData.Text = GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLDESC);
				if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLDEFAULTJUSTIFICATION)) == 0)
				{
					cmbJust.Text = "Left";
				}
				else if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLDEFAULTJUSTIFICATION)) == 1)
				{
					cmbJust.Text = "Right";
				}
				else
				{
					cmbJust.Text = "Center";
				}
			}
			else
			{
				strTemp = "0|0||0|0";
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, strTemp);
				vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col, "");
				txtCode.Text = FCConvert.ToString(0);
				txtOpenCode.Text = FCConvert.ToString(0);
				txtData.Text = "";
			}
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			//Image1.LeftOriginal = FCConvert.ToInt32(-HScroll1.Value + (30 * dblSizeRatio));
			// vsLayout.Left = (720 * dblSizeRatio) + (-HScroll1.Value + (30 * dblSizeRatio))
			vsLayout.LeftOriginal = Image1.LeftOriginal;
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			//Image1.LeftOriginal = FCConvert.ToInt32(-HScroll1.Value + (30 * dblSizeRatio));
			// vsLayout.Left = (720 * dblSizeRatio) + (-HScroll1.Value + (30 * dblSizeRatio))
			vsLayout.LeftOriginal = Image1.LeftOriginal;
		}

		private void frmCustomCEReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomCEReport properties;
			//frmCustomCEReport.FillStyle	= 0;
			//frmCustomCEReport.ScaleWidth	= 9300;
			//frmCustomCEReport.ScaleHeight	= 7875;
			//frmCustomCEReport.LinkTopic	= "Form2";
			//frmCustomCEReport.LockControls	= -1  'True;
			//frmCustomCEReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			dblSizeRatio = 1;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridFields();
			FillGridFields();
			vsLayout.Rows = 2;
            vsLayout.Row = 1;
			AddColumn();
			//Image1.SizeMode = PictureBoxSizeMode.StretchImage;
			//Image1.WidthOriginal = 1440 * 14;
			//Image1.Image = ImageList1.Images[0];
            //HScroll1.Minimum = 0;
            //if (Image1.WidthOriginal > Frame1.WidthOriginal)
            //{
            //	HScroll1.Maximum = Image1.WidthOriginal - Frame1.WidthOriginal;
            //}
            //else
            //{
            //	HScroll1.Enabled = false;
            //}
            //vsLayout.WidthOriginal = Image1.WidthOriginal - 720;
            //FC:FINAL:AM:#3664 - don't merge the row
            //vsLayout.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			//vsLayout.MergeRow(1, true);
			vsLayout.LeftOriginal = Image1.LeftOriginal;
            // Line1.X1 = Image1.Left + (1440 * 8) - 900
            //Line1.X1 = FCConvert.ToSingle(1440 * 7.5);
            //Line1.X2 = Line1.X1;
            //Line1.Y2 = vsLayout.TopOriginal + vsLayout.HeightOriginal;

            Line1.BringToFront();
        }

		private void SetupGridFields()
		{
			GridFields.ColHidden(CNSTGRIDFIELDSCOLCODE, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLOPENCODE, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLDEFAULTJUSTIFICATION, true);
			GridFields.Rows = 2;
		}

		private void FillGridFields()
		{
			int lngRow;
			clsDRWrapper rsLoad = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			int intJust = 0;
			lngRow = 0;
			GridFields.Rows = 0;
			AddFieldRow(0, "Clear Item");
			AddParentRow("Property");
			AddFieldRow(1, "Account", 0, 1);
			AddFieldRow(2, "Owner Name");
			AddFieldRow(3, "Name Last, First");
			AddFieldRow(4, "Second Owner");
			AddFieldRow(5, "Sec. Name Last, First");
			AddFieldRow(6, "Map/Lot");
			AddFieldRow(7, "Street Number", 0, 1);
			AddFieldRow(8, "Street Name");
			AddFieldRow(9, "Neighborhood");
			AddFieldRow(10, "Zone");
			AddFieldRow(11, "Acreage", 0, 1);
			AddFieldRow(12, "Frontage", 0, 1);
			// add open fields
			rsLoad.OpenRecordset("SELECT * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				AddParentRow("Property User Data");
				while (!rsLoad.EndOfFile())
				{
					intJust = 0;
					if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL && rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
					{
						intJust = 1;
					}
					AddFieldRow(29, rsLoad.Get_Fields_String("Description"), rsLoad.Get_Fields_Int32("ID"), intJust);
					rsLoad.MoveNext();
				}
			}
			AddParentRow("Permit");
			AddFieldRow(13, "Permit Year", 0, 1);
			AddFieldRow(14, "Permit");
			AddFieldRow(52, "Applicant");
			AddFieldRow(15, "Application Date");
			AddFieldRow(16, "Type");
			AddFieldRow(51, "Category");
			AddFieldRow(17, "Status");
			AddFieldRow(18, "Description");
			AddFieldRow(19, "Plan");
			AddFieldRow(33, "Fee");
			AddFieldRow(34, "Permit Contractor");
			AddFieldRow(53, "Value");
			AddFieldRow(54, "Contact Name");
			AddFieldRow(55, "Email");
			AddFieldRow(56, "Comm / Res");
			AddFieldRow(57, "Cert. of Occ. Date");
			// section for each permit type
			rsTemp.OpenRecordset("select * FROM systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			while (!rsTemp.EndOfFile())
			{
				rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " AND SUBTYPE = " + rsTemp.Get_Fields_Int32("ID") + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					AddParentRow(rsTemp.Get_Fields_String("description") + " Permit User Data");
					while (!rsLoad.EndOfFile())
					{
						intJust = 0;
						if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL && rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
						{
							intJust = 1;
						}
						AddFieldRow(31, rsLoad.Get_Fields_String("description"), rsLoad.Get_Fields_Int32("ID"), intJust);
						rsLoad.MoveNext();
					}
				}
				rsTemp.MoveNext();
			}
			// add open fields
			AddParentRow("Inspection");
			AddFieldRow(20, "Type");
			AddFieldRow(21, "Status");
			AddFieldRow(22, "Date");
			AddFieldRow(23, "Inspector");
			// add open fields
			// section for each permit type
			rsTemp.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " and not isnull(booloption,0) = 1 order by description", modGlobalVariables.Statics.strCEDatabase);
			while (!rsTemp.EndOfFile())
			{
				rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " and subtype = " + rsTemp.Get_Fields_Int32("ID") + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					AddParentRow(rsTemp.Get_Fields_String("description") + " Inspection User Data");
					while (!rsLoad.EndOfFile())
					{
						intJust = 0;
						if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL && rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
						{
							intJust = 1;
						}
						AddFieldRow(32, rsLoad.Get_Fields_String("description"), rsLoad.Get_Fields_Int32("ID"), intJust);
						rsLoad.MoveNext();
					}
				}
				rsTemp.MoveNext();
			}
			AddParentRow("Contractor");
			AddFieldRow(24, "Name");
			AddFieldRow(25, "Name2");
			AddFieldRow(26, "License");
			AddFieldRow(27, "Status");
			// Call AddFieldRow(28, "Class")
			rsLoad.OpenRecordset("SELECT * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				AddParentRow("Contractor User Data");
				while (!rsLoad.EndOfFile())
				{
					intJust = 0;
					if (rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPEDECIMAL && rsLoad.Get_Fields_Int32("valueformat") == modCEConstants.CNSTVALUETYPELONG)
					{
						intJust = 1;
					}
					AddFieldRow(30, rsLoad.Get_Fields_String("Description"), rsLoad.Get_Fields_Int32("ID"), intJust);
					rsLoad.MoveNext();
				}
			}
			for (lngRow = 1; lngRow <= GridFields.Rows - 1; lngRow++)
			{
				GridFields.IsCollapsed(lngRow, FCGrid.CollapsedSettings.flexOutlineCollapsed);
			}
            // lngRow
            modColorScheme.ColorGrid(GridFields);
		}

		private void ResizeGridFields()
		{
			int GridWidth = 0;
			GridWidth = GridFields.WidthOriginal;
			GridFields.ColWidth(CNSTGRIDFIELDSCOLLEAF, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void AddParentRow(string strDesc)
		{
			int lngRow;
			GridFields.Rows += 1;
			lngRow = GridFields.Rows - 1;
			GridFields.RowOutlineLevel(lngRow, 0);
			GridFields.IsSubtotal(lngRow, true);
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESC, strDesc);
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLCODE, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLOPENCODE, FCConvert.ToString(0));
			GridFields.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, GridFields.Cols - 1, Information.RGB(180, 255, 180));
		}

		private int AddFieldRow(int lngCode, string strDesc, int lngOpenCode = 0, int intJust = 0)
		{
			int AddFieldRow = 0;
			int lngRow;
			GridFields.Rows += 1;
			lngRow = GridFields.Rows - 1;
			GridFields.RowOutlineLevel(lngRow, 1);
			GridFields.IsSubtotal(lngRow, true);
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLOPENCODE, FCConvert.ToString(lngOpenCode));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLCODE, FCConvert.ToString(lngCode));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESC, strDesc);
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDEFAULTJUSTIFICATION, FCConvert.ToString(intJust));
			return AddFieldRow;
		}

		private void mnuAddCol_Click(object sender, System.EventArgs e)
		{
			AddColumn(true);
		}

		private void mnuAddColumn_Click(object sender, System.EventArgs e)
		{
			AddColumn(true);
		}

		private void AddColumn(bool startEdit = false)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strTemp = "";
			vsLayout.Cols += 1;
			vsLayout.ColWidth(vsLayout.Cols - 1, FCConvert.ToInt32(1440 * dblSizeRatio));
			vsLayout.ColData(vsLayout.Cols - 1, 1440);
			for (x = 1; x <= (vsLayout.Rows - 1); x++)
			{
				// default the settings
				strTemp = "0|0||0|0";
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, x, vsLayout.Cols - 1, strTemp);
			}
			// x
			vsLayout.Col = vsLayout.Cols - 1;
            //FC:FINAL:AM:#1971 - focus the new column
            if (startEdit)
            {
                vsLayout.Focus();
                vsLayout.EditCell();
            }
		}

		private void AddRow()
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strTemp;
			vsLayout.Rows += 1;
			strTemp = "0|0||0|0";
			for (x = 0; x <= (vsLayout.Cols - 1); x++)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Rows - 1, x, strTemp);
			}
			// x
		}
		// vbPorter upgrade warning: intRow As int	OnWriteFCConvert.ToInt32(
		private void InsertRow(int intRow)
		{
			if (intRow < 1)
				return;
			if (intRow > vsLayout.Rows - 1)
				return;
			AddRow();
			vsLayout.RowPosition(vsLayout.Rows - 1, intRow);
			vsLayout.Row = intRow;
			vsLayout.Col = 0;
		}

		private void mnuAddRow_Click(object sender, System.EventArgs e)
		{
			AddRow();
		}

		private void mnuDelCol_Click(object sender, System.EventArgs e)
		{
			DeleteColumn();
		}

		private void mnuDeleteColumn_Click(object sender, System.EventArgs e)
		{
			DeleteColumn();
		}

		private void mnuDeleteReports_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strReturn = "";
			rsLoad.OpenRecordset("select * from customreports order by title", modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				string strTemp = "";
				strTemp = "";
				while (!rsLoad.EndOfFile())
				{
					strTemp += FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))) + ";" + rsLoad.Get_Fields("title") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strTemp, "Custom Reports", "Report to Delete", true);
					if (fecherFoundation.Strings.Trim(strReturn) != string.Empty)
					{
						DeleteReports(ref strReturn);
					}
				}
			}
			else
			{
				MessageBox.Show("There are no saved reports to delete", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void DeleteReports(ref string strList)
		{
			string[] strAry = null;
			clsDRWrapper rsExec = new clsDRWrapper();
			strAry = Strings.Split(strList, ",", -1, CompareConstants.vbTextCompare);
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
			{
				rsExec.Execute("delete from customreportlayouts where reportid = " + FCConvert.ToString(Conversion.Val(strAry[x])), modGlobalVariables.Statics.strCEDatabase);
				rsExec.Execute("delete from customreports where ID = " + FCConvert.ToString(Conversion.Val(strAry[x])), modGlobalVariables.Statics.strCEDatabase);
			}
			// x
			MessageBox.Show("Report(s) deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuDeleteRow_Click(object sender, System.EventArgs e)
		{
			DeleteRow();
		}

		private void mnuDelRow_Click(object sender, System.EventArgs e)
		{
			DeleteRow();
		}

		private void mnuInsertCol_Click(object sender, System.EventArgs e)
		{
			InsertColumn(vsLayout.Col);
		}

		private void mnuInsertColumn_Click(object sender, System.EventArgs e)
		{
			InsertColumn(vsLayout.Col);
		}
		// vbPorter upgrade warning: intCol As int	OnWriteFCConvert.ToInt32(
		private void InsertColumn(int intCol)
		{
			if (intCol < 0)
				return;
			if (intCol > vsLayout.Cols - 1)
				return;
			AddColumn(true);
			vsLayout.ColPosition(vsLayout.Cols - 1, intCol);
			vsLayout.Col = intCol;
		}

		private void mnuInsertRow_Click(object sender, System.EventArgs e)
		{
			InsertRow(vsLayout.Row);
		}

		private void mnuLoad_Click(object sender, System.EventArgs e)
		{
			int lngID;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strReturn = "";
			rsLoad.OpenRecordset("select * from customreports order by title", modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				string strTemp = "";
				strTemp = "";
				while (!rsLoad.EndOfFile())
				{
					strTemp += FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))) + ";" + rsLoad.Get_Fields("title") + "|";
					rsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					strReturn = frmListChoiceWithIDs.InstancePtr.Init(ref strTemp, "Custom Reports", "Report to Load");
					if (fecherFoundation.Strings.Trim(strReturn) != string.Empty)
					{
						LoadReport(FCConvert.ToInt32(Conversion.Val(strReturn)));
					}
				}
			}
			else
			{
				MessageBox.Show("There are no saved reports to load", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuRowAdd_Click(object sender, System.EventArgs e)
		{
			AddRow();
		}

		private void mnuRowInsert_Click(object sender, System.EventArgs e)
		{
			InsertRow(vsLayout.Row);
		}

		private void DeleteRow()
		{
			if (vsLayout.Row < 1)
				return;
			if (vsLayout.Rows > 2)
			{
				vsLayout.RemoveItem(vsLayout.Row);
			}
			else
			{
				MessageBox.Show("You must have at least one row in a report", "Insufficient Rows", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void DeleteColumn()
		{
			if (vsLayout.Col < 0)
				return;
			if (vsLayout.Cols > 1)
			{
				vsLayout.ColPosition(vsLayout.Col, vsLayout.Cols - 1);
				vsLayout.Cols -= 1;
			}
			else
			{
				MessageBox.Show("You must have at least one column in a report", "Insufficient Columns", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveReport(lngReportID);
		}

		private void mnuSaveAs_Click(object sender, System.EventArgs e)
		{
			object strReturn = "";
			if (frmInput.InstancePtr.Init(ref strReturn, "Title", "New Title", 2880))
			{
				if (fecherFoundation.Strings.Trim(strReturn.ToString()) != string.Empty)
				{
					txtTitle.Text = fecherFoundation.Strings.Trim(strReturn.ToString());
					SaveReport(0);
				}
				else
				{
					MessageBox.Show("A report layout must have a title", "No Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveReport(lngReportID))
			{
				Close();
			}
		}

        //FC:FINAL:AM:#1973 - trigger code on ColumnWidthChanged
        private void vsLayout_AfterUserResize(object sender, DataGridViewColumnEventArgs e)
		{
            //FC:FINAL:AM: AfterUserResize is fired in VB6 only when there are already rows/columns to resize
            if (vsLayout.Cols > 0)
            {
                int col = vsLayout.GetFlexColIndex(e.Column.Index);
                vsLayout.ColData(col, Conversion.Int(vsLayout.ColWidth(col) / dblSizeRatio));
            }
		}

		private void frmCustomCEReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGridFields();
			//dblSizeRatio = lnRatio.X2 / 1440;
			//Line1.X1 = FCConvert.ToSingle(1440 * 7.5 * dblSizeRatio);
			ResizeVSLayout();
			//Image1.LeftOriginal = FCConvert.ToInt32(-HScroll1.Value + (30 * dblSizeRatio));
			vsLayout.LeftOriginal = Image1.LeftOriginal;
		}

		private void ResizeVSLayout()
		{
			// have to resize it to stay relative to the ruler
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 0; x <= (vsLayout.Cols - 1); x++)
			{
                //FC:FINAL:MSH - i.issue #1722: invalid cast operation
				//vsLayout.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(vsLayout.ColData(x) * dblSizeRatio));
				vsLayout.ColWidth(x, FCConvert.ToInt32(FCConvert.ToDouble(vsLayout.ColData(x)) * dblSizeRatio));
			}
			// x
			// Line1.X1 = Int(Image1.Left + (1440 * 8 * dblSizeRatio) - (900 * dblSizeRatio))
			//Line1.X1 = FCConvert.ToSingle(1440 * 7.5 * dblSizeRatio);
			//Line1.X2 = Line1.X1;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				if (vsLayout.MouseRow != 0)
				{
					vsLayout.Row = vsLayout.MouseRow;
					vsLayout.Col = vsLayout.MouseCol;
					PopupMenu(mnuOptions);
				}
			}
		}

		private void vsLayout_RowColChange(object sender, System.EventArgs e)
		{
			string strTemp = "";
			int intTemp;
			string[] strAry = null;
			if ((vsLayout.Row > 0) && (vsLayout.Col >= 0))
			{
				if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col)).Length > 1)
				{
					strAry = Strings.Split(FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col)), "|", -1, CompareConstants.vbTextCompare);
					txtCode.Text = strAry[0];
					txtOpenCode.Text = strAry[1];
					txtData.Text = strAry[2];
					if (strAry[3] == "1")
					{
						cmbJust.Text = "Right";
					}
					else if (strAry[3] == "2")
					{
						cmbJust.Text = "Center";
					}
					else
					{
						cmbJust.Text = "Left";
					}
					if (Conversion.Val(strAry[4]) == 0)
					{
						// none
						cmbTotal.Text = "None";
					}
					else if (Conversion.Val(strAry[4]) == 1)
					{
						// total
						cmbTotal.Text = "Total";
					}
					else if (Conversion.Val(strAry[4]) == 2)
					{
						// count
						cmbTotal.Text = "Count";
					}
				}
				else
				{
					txtCode.Text = "";
					txtOpenCode.Text = FCConvert.ToString(0);
					txtData.Text = "";
					cmbJust.Text = "Left";
					cmbTotal.Text = "None";
				}
			}
		}

		private void optJust_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			string strTemp;
			string[] strAry = null;
			if (vsLayout.Row < 1)
				return;
			if (vsLayout.Col < 0)
				return;
			strTemp = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col));
			strAry = Strings.Split(strTemp, "|", -1, CompareConstants.vbTextCompare);
			if (strTemp.Length > 0)
			{
				if (cmbJust.Text == "Left")
				{
					strAry[3] = "0";
				}
				else if (cmbJust.Text == "Right")
				{
					strAry[3] = "1";
				}
				else
				{
					strAry[3] = "2";
				}
				strTemp = strAry[0] + "|" + strAry[1] + "|" + strAry[2] + "|" + strAry[3] + "|" + strAry[4];
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, strTemp);
			}
		}

		private void optJust_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbJust.SelectedIndex;
			optJust_CheckedChanged(index, sender, e);
		}

		private void optTotal_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			string strTemp;
			// vbPorter upgrade warning: strAry As string()	OnWrite(string(), int())
			string[] strAry = null;
			if (vsLayout.Row < 1)
				return;
			if (vsLayout.Col < 0)
				return;
			strTemp = FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col));
			strAry = Strings.Split(strTemp, "|", -1, CompareConstants.vbTextCompare);
			if (strTemp.Length > 0)
			{
				if (cmbTotal.Text == "None")
				{
					strAry[4] = FCConvert.ToString(0);
				}
				else if (cmbTotal.Text == "Total")
				{
					strAry[4] = FCConvert.ToString(1);
				}
				else
				{
					strAry[4] = FCConvert.ToString(2);
				}
				strTemp = strAry[0] + "|" + strAry[1] + "|" + strAry[2] + "|" + strAry[3] + "|" + strAry[4];
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, strTemp);
			}
			else
			{
				strTemp = "0|0||0";
				if (cmbTotal.Text == "None")
				{
					strTemp += "|0";
				}
				else if (cmbTotal.Text == "Total")
				{
					strTemp += "|1";
				}
				else
				{
					strTemp += "|2";
				}
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, strTemp);
			}
		}

		private void optTotal_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbTotal.SelectedIndex;
			optTotal_CheckedChanged(index, sender, e);
		}
	}
}
