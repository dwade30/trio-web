//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmSystemDefined.
	/// </summary>
	partial class frmSystemDefined
	{
		public fecherFoundation.FCComboBox cmbCodeType;
		public FCGrid GridDelete;
		public FCGrid Grid;
		public FCGrid GridDeleteSub;
		public fecherFoundation.FCLabel lblSubType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCButton cmdAddCode;
		public fecherFoundation.FCButton cmdDeleteCode;
		public fecherFoundation.FCButton cmdPrintCurrent;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSave;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbCodeType = new fecherFoundation.FCComboBox();
			this.GridDelete = new fecherFoundation.FCGrid();
			this.Grid = new fecherFoundation.FCGrid();
			this.GridDeleteSub = new fecherFoundation.FCGrid();
			this.lblSubType = new fecherFoundation.FCLabel();
			this.lblType = new fecherFoundation.FCLabel();
			this.cmdAddCode = new fecherFoundation.FCButton();
			this.cmdDeleteCode = new fecherFoundation.FCButton();
			this.cmdPrintCurrent = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleteSub)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 498);
			this.BottomPanel.Size = new System.Drawing.Size(799, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbCodeType);
			this.ClientArea.Controls.Add(this.GridDelete);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Controls.Add(this.GridDeleteSub);
			this.ClientArea.Controls.Add(this.lblSubType);
			this.ClientArea.Controls.Add(this.lblType);
			this.ClientArea.Size = new System.Drawing.Size(799, 438);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAddCode);
			this.TopPanel.Controls.Add(this.cmdDeleteCode);
			this.TopPanel.Controls.Add(this.cmdPrintCurrent);
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Size = new System.Drawing.Size(799, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintCurrent, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCode, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddCode, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "System Defined";
			// 
			// cmbCodeType
			// 
			this.cmbCodeType.AutoSize = false;
			this.cmbCodeType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCodeType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCodeType.FormattingEnabled = true;
			this.cmbCodeType.Location = new System.Drawing.Point(30, 30);
			this.cmbCodeType.Name = "cmbCodeType";
			this.cmbCodeType.Size = new System.Drawing.Size(213, 40);
			this.cmbCodeType.TabIndex = 2;
			this.cmbCodeType.SelectedIndexChanged += new System.EventHandler(this.cmbCodeType_SelectedIndexChanged);
			// 
			// GridDelete
			// 
			this.GridDelete.AllowSelection = false;
			this.GridDelete.AllowUserToResizeColumns = false;
			this.GridDelete.AllowUserToResizeRows = false;
			this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.GridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDelete.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDelete.ColumnHeadersHeight = 30;
			this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDelete.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDelete.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDelete.DragIcon = null;
			this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDelete.FixedCols = 0;
			this.GridDelete.FixedRows = 0;
			this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.FrozenCols = 0;
			this.GridDelete.GridColor = System.Drawing.Color.Empty;
			this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.Location = new System.Drawing.Point(32, 302);
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.OutlineCol = 0;
			this.GridDelete.ReadOnly = true;
			this.GridDelete.RowHeadersVisible = false;
			this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDelete.RowHeightMin = 0;
			this.GridDelete.Rows = 0;
			this.GridDelete.ScrollTipText = null;
			this.GridDelete.ShowColumnVisibilityMenu = false;
			this.GridDelete.ShowFocusCell = false;
			this.GridDelete.Size = new System.Drawing.Size(17, 26);
			this.GridDelete.StandardTab = true;
			this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDelete.TabIndex = 1;
			this.GridDelete.Visible = false;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.Grid.Cols = 7;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
			this.Grid.DragIcon = null;
			this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.ExtendLastCol = true;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 121);
			this.Grid.Name = "Grid";
			this.Grid.OutlineCol = 0;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.ShowFocusCell = false;
			this.Grid.Size = new System.Drawing.Size(741, 291);
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.Grid.TabIndex = 0;
			this.Grid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEdit);
			this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
			this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
			this.Grid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.Grid_MouseDownEvent);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
			// 
			// GridDeleteSub
			// 
			this.GridDeleteSub.AllowSelection = false;
			this.GridDeleteSub.AllowUserToResizeColumns = false;
			this.GridDeleteSub.AllowUserToResizeRows = false;
			this.GridDeleteSub.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDeleteSub.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDeleteSub.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDeleteSub.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteSub.BackColorSel = System.Drawing.Color.Empty;
			this.GridDeleteSub.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDeleteSub.Cols = 1;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDeleteSub.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.GridDeleteSub.ColumnHeadersHeight = 30;
			this.GridDeleteSub.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDeleteSub.ColumnHeadersVisible = false;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDeleteSub.DefaultCellStyle = dataGridViewCellStyle6;
			this.GridDeleteSub.DragIcon = null;
			this.GridDeleteSub.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDeleteSub.FixedCols = 0;
			this.GridDeleteSub.FixedRows = 0;
			this.GridDeleteSub.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteSub.FrozenCols = 0;
			this.GridDeleteSub.GridColor = System.Drawing.Color.Empty;
			this.GridDeleteSub.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDeleteSub.Location = new System.Drawing.Point(0, 24);
			this.GridDeleteSub.Name = "GridDeleteSub";
			this.GridDeleteSub.OutlineCol = 0;
			this.GridDeleteSub.ReadOnly = true;
			this.GridDeleteSub.RowHeadersVisible = false;
			this.GridDeleteSub.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDeleteSub.RowHeightMin = 0;
			this.GridDeleteSub.Rows = 0;
			this.GridDeleteSub.ScrollTipText = null;
			this.GridDeleteSub.ShowColumnVisibilityMenu = false;
			this.GridDeleteSub.ShowFocusCell = false;
			this.GridDeleteSub.Size = new System.Drawing.Size(17, 26);
			this.GridDeleteSub.StandardTab = true;
			this.GridDeleteSub.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDeleteSub.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDeleteSub.TabIndex = 3;
			this.GridDeleteSub.Visible = false;
			// 
			// lblSubType
			// 
			this.lblSubType.Location = new System.Drawing.Point(158, 92);
			this.lblSubType.Name = "lblSubType";
			this.lblSubType.Size = new System.Drawing.Size(108, 16);
			this.lblSubType.TabIndex = 5;
			this.lblSubType.Text = "SUB TYPE";
			this.lblSubType.Visible = false;
			// 
			// lblType
			// 
			this.lblType.Location = new System.Drawing.Point(34, 92);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(108, 16);
			this.lblType.TabIndex = 4;
			this.lblType.Text = "TYPE";
			this.lblType.Visible = false;
			// 
			// cmdAddCode
			// 
			this.cmdAddCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddCode.AppearanceKey = "toolbarButton";
			this.cmdAddCode.Location = new System.Drawing.Point(358, 26);
			this.cmdAddCode.Name = "cmdAddCode";
			this.cmdAddCode.Size = new System.Drawing.Size(78, 24);
			this.cmdAddCode.TabIndex = 1;
			this.cmdAddCode.Text = "Add Code";
			this.cmdAddCode.Click += new System.EventHandler(this.mnuAddCode_Click);
			// 
			// cmdDeleteCode
			// 
			this.cmdDeleteCode.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteCode.AppearanceKey = "toolbarButton";
			this.cmdDeleteCode.Location = new System.Drawing.Point(439, 26);
			this.cmdDeleteCode.Name = "cmdDeleteCode";
			this.cmdDeleteCode.Size = new System.Drawing.Size(93, 24);
			this.cmdDeleteCode.TabIndex = 2;
			this.cmdDeleteCode.Text = "Delete Code";
			this.cmdDeleteCode.Click += new System.EventHandler(this.mnuDeleteCode_Click);
			// 
			// cmdPrintCurrent
			// 
			this.cmdPrintCurrent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintCurrent.AppearanceKey = "toolbarButton";
			this.cmdPrintCurrent.Location = new System.Drawing.Point(536, 26);
			this.cmdPrintCurrent.Name = "cmdPrintCurrent";
			this.cmdPrintCurrent.Size = new System.Drawing.Size(129, 24);
			this.cmdPrintCurrent.TabIndex = 4;
			this.cmdPrintCurrent.Text = "Print Current Type";
			this.cmdPrintCurrent.Click += new System.EventHandler(this.mnuPrintCurrent_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.AppearanceKey = "toolbarButton";
			this.cmdPrint.Location = new System.Drawing.Point(668, 26);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(103, 24);
			this.cmdPrint.TabIndex = 3;
			this.cmdPrint.Text = "Print All Types";
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(317, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmSystemDefined
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(799, 606);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSystemDefined";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "System Defined";
			this.Load += new System.EventHandler(this.frmSystemDefined_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSystemDefined_KeyDown);
			this.Resize += new System.EventHandler(this.frmSystemDefined_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeleteSub)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}