﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWCE0000
{
	public class clsReportSQLCreator
	{
		//=========================================================
		private clsReportParameters ParametersList;
		private clsReportOrderClause OrderList;
		// Private cpf As clsCustomPrintForm
		const int CNSTGRIDPROPERTYROWOWNERLAST = 0;
		const int CNSTGRIDPROPERTYROWOWNERFIRST = 1;
		const int CNSTGRIDPROPERTYROWMAPLOT = 2;
		const int CNSTGRIDPROPERTYROWACCOUNT = 3;
		const int CNSTGRIDPROPERTYROWLOCATION = 4;
		const int CNSTGRIDPROPERTYROWNEIGHBORHOOD = 5;
		const int CNSTGRIDPROPERTYROWZONE = 6;
		const int CNSTGRIDPROPERTYROWDELETED = -2;
		const int CNSTGRIDINSPECTIONROWTYPE = 0;
		const int CNSTGRIDINSPECTIONROWSTATUS = 1;
		const int CNSTGRIDINSPECTIONROWDATE = 2;
		const int CNSTGRIDINSPECTIONROWINSPECTOR = 3;
		const int CNSTGRIDCONTRACTORROWNAME = 0;
		const int CNSTGRIDCONTRACTORROWNAME2 = 1;
		const int CNSTGRIDCONTRACTORROWLICENSE = 2;
		const int CNSTGRIDCONTRACTORROWSTATUS = 3;
		const int CNSTGRIDCONTRACTORROWCLASS = 4;
		const int CNSTGRIDPERMITROWYEAR = 0;
		const int CNSTGRIDPERMITROWIDENTIFIER = 1;
		const int CNSTGRIDPERMITROWAPPLYDATE = 2;
		const int CNSTGRIDPERMITROWFILER = 5;
		const int CNSTGRIDPERMITROWCATEGORY = 4;
		const int CNSTGRIDPERMITROWTYPE = 3;
		const int CNSTGRIDPERMITROWSTATUS = 6;
		const int CNSTGRIDPERMITROWFEE = 7;

		public clsReportParameters ListOfParameters
		{
			set
			{
				ParametersList = value;
			}
			get
			{
				clsReportParameters ListOfParameters = null;
				ListOfParameters = ParametersList;
				return ListOfParameters;
			}
		}

		public clsReportOrderClause ListOfOrders
		{
			set
			{
				OrderList = value;
			}
			get
			{
				clsReportOrderClause ListOfOrders = null;
				ListOfOrders = OrderList;
				return ListOfOrders;
			}
		}

		public string Get_SQLStatement(clsSQLStatement tSQLStatement, string strDefaultTable = "")
		{
			string SQLStatement = "";
			string strReturn;
			string strWhere;
			bool boolContractTable;
			bool boolInspectionTable;
			bool boolMasterTable;
			bool boolPermitTable;
			bool boolInspectorsTable;
			bool boolClassCodes;
			string strTable = "";
			clsReportParameter tParm;
			clsReportOrder tOrd;
			string strSQL;
			int intTables;
			string strLastTable;
			clsReportParameter lastParm;
			bool boolInAnOr;
			string strFrom;
			string[] strAry = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strOrderBy;
			bool boolPermitContractors;
			string strPropOpens;
			string strPermOpens;
			string strInspOpens;
			string strContOpens;
			strPropOpens = "";
			strPermOpens = "";
			strInspOpens = "";
			strContOpens = "";
			strOrderBy = "";
			strFrom = "";
			boolContractTable = false;
			boolInspectionTable = false;
			boolMasterTable = false;
			boolPermitTable = false;
			boolInspectorsTable = false;
			boolClassCodes = false;
			boolPermitContractors = false;
			if (strDefaultTable != string.Empty)
			{
				strAry = Strings.Split(strDefaultTable, ",", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					if (fecherFoundation.Strings.LCase(strAry[x]) == "permitcontractors")
					{
						boolPermitContractors = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
					else if (fecherFoundation.Strings.LCase(strAry[x]) == "classcodes")
					{
						boolClassCodes = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
					else if (fecherFoundation.Strings.LCase(strAry[x]) == "contractors")
					{
						boolContractTable = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
					else if (fecherFoundation.Strings.LCase(strAry[x]) == "permits")
					{
						boolPermitTable = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
					else if (fecherFoundation.Strings.LCase(strAry[x]) == "inspections")
					{
						boolInspectionTable = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
					else if (fecherFoundation.Strings.LCase(strAry[x]) == "inspectors")
					{
						boolInspectorsTable = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
					else if (fecherFoundation.Strings.LCase(strAry[x]) == "cemaster")
					{
						boolMasterTable = true;
						if (strFrom == string.Empty)
						{
							strFrom = "select ";
						}
						else
						{
							strFrom += ", ";
						}
						strFrom += GetTableFields(strAry[x]);
					}
				}
				// x
			}
			tParm = null;
			lastParm = null;
			boolInAnOr = false;
			strReturn = "";
			strSQL = "select * from ";
			strLastTable = "";
			intTables = 0;
			strWhere = "";
			ParametersList.MoveFirst();
			while (ParametersList.GetCurrentIndex >= 0)
			{
				tParm = ParametersList.GetCurrentParameter;
				if (!(tParm == null))
				{
					strTable = TableFromCode(tParm.CodeType, tParm.Code);
					if (strTable != strLastTable)
					{
						strLastTable = strTable;
						if (fecherFoundation.Strings.UCase(strTable) == "CLASSCODES")
						{
							if (!boolClassCodes)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields(strTable);
							}
							boolClassCodes = true;
							intTables += 1;
							if (!boolContractTable)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields("contractors");
							}
							boolContractTable = true;
							intTables += 1;
						}
						else if (fecherFoundation.Strings.UCase(strTable) == "CONTRACTORS")
						{
							if (!boolContractTable)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields(strTable);
								boolContractTable = true;
								intTables += 1;
							}
						}
						else if (fecherFoundation.Strings.UCase(strTable) == "INSPECTIONS")
						{
							if (!boolInspectionTable)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields(strTable);
							}
							boolInspectionTable = true;
							intTables += 1;
						}
						else if (fecherFoundation.Strings.UCase(strTable) == "CEMASTER")
						{
							if (!boolMasterTable)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields(strTable);
							}
							boolMasterTable = true;
							intTables += 1;
						}
						else if (fecherFoundation.Strings.UCase(strTable) == "PERMITCONTRACTORS")
						{
							if (!boolPermitContractors)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields(strTable);
							}
							boolPermitContractors = true;
							intTables += 1;
							if (!boolPermitTable)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields("permits");
							}
							boolPermitTable = true;
							intTables += 1;
						}
						else if (fecherFoundation.Strings.UCase(strTable) == "PERMITS")
						{
							if (!boolPermitTable)
							{
								if (strFrom == string.Empty)
								{
									strFrom = "select ";
								}
								else
								{
									strFrom += ", ";
								}
								strFrom += GetTableFields(strTable);
							}
							boolPermitTable = true;
							intTables += 1;
						}
					}
					if (!(lastParm == null))
					{
						// If lastParm.CodeType = tParm.CodeType And lastParm.Code = tParm.Code And lastParm.ID = tParm.ID Then
						if (lastParm.ParameterGroup == tParm.ParameterGroup)
						{
							// need to make an or statement
							strWhere += " or ";
						}
						else
						{
							if (lastParm.InGroup)
							{
								strWhere += ") ";
								if (lastParm.ID > 0)
								{
									strWhere += ") ";
								}
							}
							strWhere += " and ";
							if (tParm.InGroup)
							{
								strWhere += "(";
								if (tParm.ID > 0)
								{
									strWhere += " ucv" + FCConvert.ToString(tParm.ID) + ".fieldid = " + FCConvert.ToString(tParm.ID) + " and (";
								}
							}
							else if (tParm.ID > 0)
							{
								strWhere += "( ucv" + FCConvert.ToString(tParm.ID) + ".fieldid = " + FCConvert.ToString(tParm.ID) + " and ";
							}
							if (tParm.ID > 0 && (tParm.ID != lastParm.ID || tParm.CodeType != lastParm.CodeType))
							{
								switch (tParm.CodeType)
								{
									case modCEConstants.CNSTCODETYPEPERMIT:
										{
											strPermOpens += FCConvert.ToString(tParm.ID) + ",";
											break;
										}
									case modCEConstants.CNSTCODETYPECONTRACTOR:
										{
											strContOpens += FCConvert.ToString(tParm.ID) + ",";
											break;
										}
									case modCEConstants.CNSTCODETYPEPROPERTY:
										{
											strPropOpens += FCConvert.ToString(tParm.ID) + ",";
											break;
										}
									case modCEConstants.CNSTCODETYPEINSPECTION:
										{
											strInspOpens += FCConvert.ToString(tParm.ID) + ",";
											break;
										}
								}
								//end switch
							}
						}
					}
					else
					{
						if (tParm.InGroup)
						{
							strWhere += "(";
							if (tParm.ID > 0)
							{
								strWhere += " ucv" + FCConvert.ToString(tParm.ID) + ".fieldid = " + FCConvert.ToString(tParm.ID) + " and (";
							}
						}
						else if (tParm.ID > 0)
						{
							strWhere += "( ucv" + FCConvert.ToString(tParm.ID) + ".fieldid = " + FCConvert.ToString(tParm.ID) + " and ";
						}
						if (tParm.ID > 0)
						{
							switch (tParm.CodeType)
							{
								case modCEConstants.CNSTCODETYPEPERMIT:
									{
										strPermOpens += FCConvert.ToString(tParm.ID) + ",";
										break;
									}
								case modCEConstants.CNSTCODETYPECONTRACTOR:
									{
										strContOpens += FCConvert.ToString(tParm.ID) + ",";
										break;
									}
								case modCEConstants.CNSTCODETYPEPROPERTY:
									{
										strPropOpens += FCConvert.ToString(tParm.ID) + ",";
										break;
									}
								case modCEConstants.CNSTCODETYPEINSPECTION:
									{
										strInspOpens += FCConvert.ToString(tParm.ID) + ",";
										break;
									}
							}
							//end switch
						}
					}
					if (tParm.IsTrue)
					{
						strWhere += GetParameterValue(tParm.ID, tParm.Code, tParm.CodeType, tParm.FromParameter, tParm.ToParameter, tParm.DBType);
					}
					else
					{
						strWhere += " not " + GetParameterValue(tParm.ID, tParm.Code, tParm.CodeType, tParm.FromParameter, tParm.ToParameter, tParm.DBType);
					}
					if (tParm.ID > 0 && !tParm.InGroup)
					{
						strWhere += ") ";
					}
					lastParm = tParm;
				}
				ParametersList.MoveNext();
			}
			if (!(tParm == null))
			{
				if (tParm.ID > 0 && tParm.InGroup)
				{
					strWhere += ") ";
				}
			}
			if (fecherFoundation.Strings.Trim(strPermOpens) != "")
			{
				strPermOpens = Strings.Mid(strPermOpens, 1, strPermOpens.Length - 1);
			}
			if (fecherFoundation.Strings.Trim(strPropOpens) != "")
			{
				strPropOpens = Strings.Mid(strPropOpens, 1, strPropOpens.Length - 1);
			}
			if (fecherFoundation.Strings.Trim(strInspOpens) != "")
			{
				strInspOpens = Strings.Mid(strInspOpens, 1, strInspOpens.Length - 1);
			}
			if (fecherFoundation.Strings.Trim(strContOpens) != "")
			{
				strContOpens = Strings.Mid(strContOpens, 1, strContOpens.Length - 1);
			}
			// If intTables = 0 Then
			// strFrom = "select " & GetTableFields(strDefaultTable) & " from " & strDefaultTable
			// strSQL = strFrom
			// ElseIf intTables = 1 Then
			strSQL = strFrom + " from " + GetInnerJoin(boolContractTable, boolInspectionTable, boolMasterTable, boolPermitTable, boolInspectorsTable, boolClassCodes, boolPermitContractors, false, strPropOpens, strPermOpens, strInspOpens, strContOpens) + " ";
			// End If
			// strSQL = strFrom
			if (!(tParm == null))
			{
				if (tParm.InGroup)
					strWhere += ") ";
			}
			if (fecherFoundation.Strings.Trim(strWhere) != string.Empty)
			{
				strWhere = " where " + strWhere;
			}
			OrderList.MoveFirst();
			while (OrderList.GetCurrentIndex >= 0)
			{
				tOrd = OrderList.GetCurrentOrderItem;
				if (!(tOrd == null))
				{
					strOrderBy += tOrd.OrderByString + ",";
				}
				OrderList.MoveNext();
			}
			if (fecherFoundation.Strings.Trim(strOrderBy) != string.Empty)
			{
				strOrderBy = Strings.Mid(strOrderBy, 1, strOrderBy.Length - 1);
				strOrderBy = " order by " + strOrderBy;
			}
			tSQLStatement.SelectStatement = strSQL;
			strSQL += strWhere + strOrderBy;
			tSQLStatement.OrderByStatement = strOrderBy;
			tSQLStatement.SQLStatement = strSQL;
			tSQLStatement.WhereStatement = strWhere;
			SQLStatement = strSQL;
			return SQLStatement;
		}

		public string GetTableFields(string strTable)
		{
			string GetTableFields = "";
			string strReturn;
			string strMasterJoin = "";
			string strMasterJoinJoin = "";
			strReturn = "";
			if (fecherFoundation.Strings.LCase(strTable) == "permitcontractors")
			{
				strReturn = " permitcontractors.ID as PermitContractorAutoID,permitcontractors.permitIDNum as permitIDNum,permitcontractors.ContractorID as PermitContractorAutoID,PermitContractorName,PermitContractorAddress1,PermitContractorAddress2,PermitContractorCity,PermitContractorState,PermitContractorZip,PermitContractorZip4,PermitContractorEmail,ORDERNUMBER AS PermitContractorOrderNumber ";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "classcodes")
			{
				strReturn = " classcodes.ID as ClassCodeAutoID,classcodes.code as ClassCode,License, classcodes.ContractorID as ClassCodeContractor ";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "contractors")
			{
				strReturn = " contractors.ID as ContractorAutoID,ContractorNumber,contractors.name1 as ContractorName1,contractors.name2 as ContractorName2,Contractors.address1 as ContractorAddress1,";
				strReturn += "contractors.address2 as ContractorAddress2,contractors.city as ContractorCity,contractors.state as ContractorState,contractors.zip as ContractorZip,contractors.zip4 as ContractorZip4,";
				strReturn += "contractors.email as ContractorEmail,contractorstatus,contractors.deleted as ContractorDeleted,contractors.comment as ContractorComment";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "cemaster")
			{
				// strReturn = "cemaster.ID as PropertyAutoID,CEAccount,RSAccount,cemaster.First as First,cemaster.Middle as Middle,Last,Desig,SecFirst,SecLast,SecMiddle,SecDesig,cemaster.Name as name,SecOwner,cemaster.Address1 as Address1,cemaster.Address2 as Address2,cemaster.City as City,cemaster.State as State,cemaster.Zip as Zip,cemaster.Zip4 as Zip4,Maplot,"
				// strReturn = strReturn & "streetnumber,apt,streetname,ref1,ref2,acres,frontage,cemaster.email as Email,cemaster.deleted as PropertyDeleted,zone,neighborhood"
				strReturn = "mj.ID as PropertyAutoID,CEAccount,RSAccount,mj.First as First,mj.Middle as Middle,Last,Desig,SecFirst,SecLast,SecMiddle,SecDesig,mj.Name as name,SecOwner,mj.Address1 as Address1,mj.Address2 as Address2,mj.City as City,mj.State as State,mj.Zip as Zip,mj.Zip4 as Zip4,Maplot,";
				strReturn += "streetnumber,apt,streetname,ref1,ref2,acres,frontage,mj.email as Email,mj.deleted as PropertyDeleted,zone,neighborhood";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "permits")
			{
				// strReturn = " permits.ID as PermitAutoid,account,PermitIdentifier,PermitYear,AppDenCode,ApplicationDate,Plan,FiledBy,ContactName,ContactEmail,StartDate,CompletionDate,EstimatedValue,Fee,PermitType,"
				// strReturn = strReturn & "PermitSubType,permits.ContractorID as PermitContractorID,CertificateOfOCCRequired,CertificateOfOCCDate,PermitNumber,Units,DwellUnits,NumBuildings,CommResCode,Description,OMBCode,PermitContractorName,"
				// strReturn = strReturn & "PermitContractorAddress1,PermitContractorAddress2,PermitContractorCity,PermitContractorState,PermitContractorZip,PermitContractorZip4,PermitContractorEmail,Completed,"
				// strReturn = strReturn & "permits.deleted as PermitDeleted,permits.comment as PermitComment,OriginalOwner,OriginalMapLot,OriginalStreetNum,OriginalStreet,OriginalOwnerFirst,OriginalOwnerLast,OriginalOwnerMiddle,OriginalOwnerDesig"
				strReturn = " permits.ID as PermitAutoid,permits.account as account,PermitIdentifier,PermitYear,AppDenCode,ApplicationDate,[Plan],FiledBy,ContactName,ContactEmail,StartDate,CompletionDate,EstimatedValue,Fee,PermitType,";
				strReturn += "PermitSubType,CertificateOfOCCRequired,CertificateOfOCCDate,PermitNumber,Units,DwellUnits,NumBuildings,CommResCode,[Description],OMBCode,Completed,";
				strReturn += "permits.deleted as PermitDeleted,permits.comment as PermitComment,OriginalOwner,OriginalMapLot,OriginalStreetNum,OriginalStreet,OriginalOwnerFirst,OriginalOwnerLast,OriginalOwnerMiddle,OriginalOwnerDesig";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "inspections")
			{
				strReturn += " inspections.ID as InspectionAutoID,InspectorID,InspectionDate,InspectionType,InspectionStatus,inspections.Comment as InspectionComment,PermitID,inspections.deleted as InspectionDeleted";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "inspectors")
			{
				strReturn += " inspectors.ID as InspectorsAutoID,Inspectors.name as InspectorName,inspectors.shortname as InspectorShortName ";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "activitylog")
			{
				strReturn += "activitylog.ID as ActivityAutoID,activitylog.account as ActivityAccount,activitylog.parentid as ActivityParentID,activitylog.parenttype as ActivityParentType,activitylog.description as ActivityDescription,ActivityDate,ActivityType,activitylog.detail as ActivityDetail,activitylog.referenceID as ActivityReferenceID, activitylog.referencetype as ActivityReferenceCode";
			}
			else if (fecherFoundation.Strings.LCase(strTable) == "violations")
			{
				strReturn += "violations.id as ViolationAutoID,violationtype,violationaccount,violationstatus,dateviolationoccurred,dateviolationissued,dateviolationresolved,violationdescription,violationdetail,violationfee,violationidentifier,violationcontactname,violationcontactemail,violationpermit,violationfirstname,violationmiddlename,violationlastname,violationdesig,violationdba,violationaddress1,violationaddress2,violationcity,violationstate,violationzip,violationzip4,violationstreetnumber,violationapt,violationstreet,violationbusinesslicense,violationname ";
			}
			GetTableFields = strReturn;
			return GetTableFields;
		}

		private string GetParameterValue(int lngAutoID, int lngCode, int lngCodeType, string strFrom, string strTo, int intDBType)
		{
			string GetParameterValue = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn;
				string strField = "";
				// vbPorter upgrade warning: lngDBType As int	OnWrite(DAO.DataTypeEnum)
				int lngDBType = 0;
				bool boolCodeType;
				strReturn = "";
				boolCodeType = false;
				if (lngAutoID == 0)
				{
					switch (lngCodeType)
					{
						case modCEConstants.CNSTCODETYPECONTRACTOR:
							{
								switch (lngCode)
								{
									case CNSTGRIDCONTRACTORROWCLASS:
										{
											strField = "Code";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
									case CNSTGRIDCONTRACTORROWLICENSE:
										{
											strField = "License";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
											break;
										}
									case CNSTGRIDCONTRACTORROWNAME:
										{
											strField = "Name1";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
											break;
										}
									case CNSTGRIDCONTRACTORROWNAME2:
										{
											strField = "Name2";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
											break;
										}
									case CNSTGRIDCONTRACTORROWSTATUS:
										{
											strField = "contractorstatus";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
								}
								//end switch
								break;
							}
						case modCEConstants.CNSTCODETYPEPERMIT:
							{
								switch (lngCode)
								{
									case CNSTGRIDPERMITROWAPPLYDATE:
										{
											strField = "ApplicationDate";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbDate);
											break;
										}
									case CNSTGRIDPERMITROWFILER:
										{
											strField = "FiledBy";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
											break;
										}
									case CNSTGRIDPERMITROWIDENTIFIER:
										{
											strField = "PermitIdentifier";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
											break;
										}
									case CNSTGRIDPERMITROWSTATUS:
										{
											strField = "AppDenCode";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
									case CNSTGRIDPERMITROWTYPE:
										{
											strField = "PermitType";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
									case CNSTGRIDPERMITROWCATEGORY:
										{
											strField = "PermitSubType";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
									case CNSTGRIDPERMITROWYEAR:
										{
											strField = "PermitYear";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											break;
										}
									case CNSTGRIDPERMITROWFEE:
										{
											strField = "Fee";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbDouble);
											break;
										}
								}
								//end switch
								break;
							}
						case modCEConstants.CNSTCODETYPEINSPECTION:
							{
								switch (lngCode)
								{
									case CNSTGRIDINSPECTIONROWTYPE:
										{
											strField = "InspectionType";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
									case CNSTGRIDINSPECTIONROWSTATUS:
										{
											strField = "InspectionStatus";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
									case CNSTGRIDINSPECTIONROWDATE:
										{
											strField = "InspectionDate";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbDate);
											break;
										}
									case CNSTGRIDINSPECTIONROWINSPECTOR:
										{
											strField = "InspectorID";
											lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
											boolCodeType = true;
											break;
										}
								}
								//end switch
								break;
							}
						case modCEConstants.CNSTCODETYPEPROPERTY:
							{
								if (lngCode == CNSTGRIDPROPERTYROWACCOUNT)
								{
									strField = "CEAccount";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWLOCATION)
								{
									strField = "StreetName";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWMAPLOT)
								{
									strField = "MapLot";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWNEIGHBORHOOD)
								{
									strField = "Neighborhood";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWOWNERLAST)
								{
									strField = "Last";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWOWNERFIRST)
								{
									strField = "First";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWZONE)
								{
									strField = "Zone";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								}
								else if (lngCode == CNSTGRIDPROPERTYROWDELETED)
								{
									strField = "Deleted";
									lngDBType = FCConvert.ToInt32(DataTypeEnum.dbBoolean);
								}
								break;
							}
					}
					//end switch
				}
				else
				{
					switch (intDBType)
					{
						case modCEConstants.CNSTVALUETYPELONG:
							{
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".numericvalue";
								break;
							}
						case modCEConstants.CNSTVALUETYPEDECIMAL:
							{
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".numericvalue";
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbDouble);
								break;
							}
						case modCEConstants.CNSTVALUETYPETEXT:
							{
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".textvalue";
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbText);
								break;
							}
						case modCEConstants.CNSTVALUETYPEDATE:
							{
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbDate);
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".datevalue";
								break;
							}
						case modCEConstants.CNSTVALUETYPEDROPDOWN:
							{
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".dropdownvalue";
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
								break;
							}
						case modCEConstants.CNSTVALUETYPEBOOLEAN:
							{
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".booleanvalue";
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbBoolean);
								break;
							}
						default:
							{
								strField = "ucv" + FCConvert.ToString(lngAutoID) + ".numericvalue";
								lngDBType = FCConvert.ToInt32(DataTypeEnum.dbLong);
								break;
							}
					}
					//end switch
					// If cpf Is Nothing Then
					// Set cpf = New clsCustomPrintForm
					// cpf.LoadControlTypes
					// End If
					// Dim tOb As clsCustomControlParameters
					// Set tOb = cpf.GetControlByCode()
					// If Not tOb Is Nothing Then
					// Select Case tOb.TypeOfData
					// Case CNSTVALUETYPELONG
					// lngDBType = dbLong
					// Case CNSTVALUETYPEDECIMAL
					// lngDBType = dbDouble
					// Case CNSTVALUETYPETEXT
					// lngDBType = dbText
					// Case CNSTVALUETYPEDATE
					// lngDBType = dbDate
					// Case CNSTVALUETYPEDROPDOWN
					// lngDBType = dbLong
					// Case CNSTVALUETYPEBOOLEAN
					// lngDBType = dbBoolean
					// Case Else
					// lngDBType = dbLong
					// End Select
					// lngDBType = tOb.TypeOfData
					// Select Case tOb
					// End Select
					// End If
				}
				if (strFrom != strTo)
				{
					switch ((DataTypeEnum)lngDBType)
					{
						case DataTypeEnum.dbLong:
							{
								if (!boolCodeType || (Conversion.Val(strTo) != 0))
								{
									if (Conversion.Val(strTo) != 0)
									{
										strReturn = strField + " between " + FCConvert.ToString(Conversion.Val(strFrom)) + " and " + FCConvert.ToString(Conversion.Val(strTo)) + " ";
									}
									else
									{
										strReturn = strField + " >= " + FCConvert.ToString(Conversion.Val(strFrom)) + " ";
									}
								}
								else
								{
									strReturn = strField + " = " + FCConvert.ToString(Conversion.Val(strFrom));
								}
								break;
							}
						case DataTypeEnum.dbText:
							{
								if (strTo != string.Empty)
								{
									strReturn = strField + " between '" + strFrom + "' and '" + strTo + "' ";
								}
								else
								{
									strReturn = strField + " >= '" + strFrom + "' ";
								}
								break;
							}
						case DataTypeEnum.dbDate:
							{
								if (strTo != string.Empty)
								{
									strReturn = strField + " between '" + strFrom + "' and '" + strTo + "' ";
								}
								else
								{
									strReturn = strField + " >= '" + strFrom + "' ";
								}
								break;
							}
						case DataTypeEnum.dbBoolean:
							{
								strReturn = strField + " = " + strFrom;
								break;
							}
						case DataTypeEnum.dbDouble:
							{
								if (!boolCodeType || (Conversion.Val(strTo) != 0))
								{
									if (Conversion.Val(strTo) != 0)
									{
										strReturn = strField + " between " + FCConvert.ToString(Conversion.Val(strFrom)) + " and " + FCConvert.ToString(Conversion.Val(strTo)) + " ";
									}
									else
									{
										strReturn = strField + " >= " + FCConvert.ToString(Conversion.Val(strFrom)) + " ";
									}
								}
								else
								{
									strReturn = strField + " = " + FCConvert.ToString(Conversion.Val(strFrom));
								}
								break;
							}
					}
					//end switch
				}
				else
				{
					switch ((DataTypeEnum)lngDBType)
					{
						case DataTypeEnum.dbLong:
							{
								strReturn = strField + " = " + FCConvert.ToString(Conversion.Val(strFrom));
								break;
							}
						case DataTypeEnum.dbText:
							{
								strReturn = strField + " = '" + strFrom + "' ";
								break;
							}
						case DataTypeEnum.dbDate:
							{
								strReturn = strField + " = '" + strFrom + "'";
								break;
							}
						case DataTypeEnum.dbBoolean:
							{
								strReturn = strField + " = " + strFrom;
								break;
							}
						case DataTypeEnum.dbDouble:
							{
								strReturn = strField + " = " + FCConvert.ToString(Conversion.Val(strFrom));
								break;
							}
					}
					//end switch
				}
				GetParameterValue = strReturn;
				return GetParameterValue;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetParameterValue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetParameterValue;
		}

		private string TableFromCode(int lngCodeType, int longCode)
		{
			string TableFromCode = "";
			string strReturn = "";
			switch (lngCodeType)
			{
				case modCEConstants.CNSTCODETYPECONTRACTOR:
					{
						if (longCode == CNSTGRIDCONTRACTORROWCLASS || longCode == CNSTGRIDCONTRACTORROWLICENSE)
						{
							strReturn = "ClassCodes";
						}
						else
						{
							strReturn = "Contractors";
						}
						break;
					}
				case modCEConstants.CNSTCODETYPEINSPECTION:
					{
						strReturn = "Inspections";
						break;
					}
				case modCEConstants.CNSTCODETYPEPROPERTY:
					{
						strReturn = "CEMaster";
						break;
					}
				case modCEConstants.CNSTCODETYPEPERMIT:
					{
						strReturn = "Permits";
						break;
					}
				case modCEConstants.CNSTCODETYPEVIOLATION:
					{
						strReturn = "Violations";
						break;
					}
			}
			//end switch
			TableFromCode = strReturn;
			return TableFromCode;
		}

		public string GetInnerJoin(bool boolContractors, bool boolInspections, bool boolMaster, bool boolPermits, bool boolInspectors, bool boolContractorClass, bool boolPermitContractors, bool boolViolations, string strOpenProps = "", string strOpenPermits = "", string strOpenInspections = "", string strOpenContractors = "")
		{
			string GetInnerJoin = "";
			string strReturn;
			int intCount;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			string strMasterJoin;
			string strMasterJoinJoin;
			strMasterJoin = modCE.GetCEMasterJoin();
			strMasterJoinJoin = modCE.GetCEMasterJoinForJoin();
			intCount = 0;
			strReturn = "";
			if (fecherFoundation.Strings.Trim(strOpenContractors) != "")
			{
				boolContractors = true;
			}
			if (fecherFoundation.Strings.Trim(strOpenInspections) != "")
			{
				boolInspections = true;
			}
			if (fecherFoundation.Strings.Trim(strOpenProps) != "")
			{
				boolMaster = true;
			}
			if (fecherFoundation.Strings.Trim(strOpenPermits) != "")
			{
				boolPermits = true;
			}
			if (boolContractorClass)
			{
				intCount += 2;
				// must inner join with contractors
			}
			else
			{
				if (boolContractors)
					intCount += 1;
			}
			if (boolInspections)
				intCount += 1;
			if (boolViolations)
				intCount += 1;
			if (boolMaster)
				intCount += 1;
			if (boolPermitContractors)
			{
				intCount += 2;
				// must inner join with permits
			}
			else
			{
				if (boolPermits)
					intCount += 1;
			}
			if (boolInspectors)
				intCount += 1;
			if (boolInspectors && !boolInspections && intCount > 1)
			{
				boolInspections = true;
				intCount += 1;
			}
			if (intCount > 1 && boolViolations)
			{
				if (!boolMaster)
				{
					boolMaster = true;
					intCount += 1;
				}
			}
			if (intCount == 1)
			{
				if (boolContractors)
				{
					strReturn = " contractors ";
				}
				else if (boolInspections)
				{
					strReturn = " inspections ";
				}
				else if (boolMaster)
				{
					// strReturn = " cemaster "
					strReturn = " " + strMasterJoinJoin + " ";
				}
				else if (boolInspectors)
				{
					strReturn = " inspectors ";
				}
				else if (boolPermits)
				{
					strReturn = " permits ";
				}
				else if (boolViolations)
				{
					strReturn = " violations ";
				}
			}
			else if (intCount == 2 && boolInspectors && boolInspections)
			{
				strReturn = " inspectors inner join inspections on (inspectors.ID = inspections.inspectorid) ";
			}
			else if (intCount == 2 && boolContractors && boolContractorClass)
			{
				strReturn = " contractors inner join classcodes on (contractors.ID = classcodes.contractorid) ";
			}
			else if (intCount == 2 && boolMaster && boolViolations)
			{
				// strReturn = " cemaster inner join violations on (cemaster.ceaccount = violations.violationaccount) "
				strReturn = " " + strMasterJoinJoin + " inner join violations on (mj.ceaccount = violations.violationaccount) ";
			}
			else
			{
				// must inner join with the permits table even if boolpermits is false
				strReturn = " permits inner join ";
				strReturn = "";
				for (x = 1; x <= intCount; x++)
				{
					if (boolMaster)
					{
						if (strReturn == "")
						{
							// strReturn = " permits inner join cemaster on (permits.account = cemaster.ceaccount) "
							strReturn = " permits inner join " + strMasterJoinJoin + " on (permits.account = mj.ceaccount) ";
						}
						else
						{
							// strReturn = " cemaster inner join (" & strReturn & ") on (cemaster.ceaccount = permits.account) "
							strReturn = " " + strMasterJoinJoin + " inner join (" + strReturn + ") on (mj.ceaccount = permits.account) ";
						}
						boolMaster = false;
					}
					else if (boolViolations)
					{
						if (strReturn == "")
						{
							strReturn = " permits inner join violations on (violations.violationpermit = permits.ID) ";
						}
						else
						{
							strReturn = " violations inner join (" + strReturn + ") on (permits.ID = violations.violationpermit) ";
						}
						boolViolations = false;
					}
					else if (boolPermits)
					{
						boolPermits = false;
					}
					else if (boolInspections)
					{
						if (strReturn == "")
						{
							strReturn = " permits inner join inspections on (inspections.permitid = permits.ID) ";
						}
						else
						{
							// strReturn = " inspections inner join (" & strReturn & ") on (inspections.permitid = permits.ID) "
							strReturn = " inspections Right join (" + strReturn + ") on (inspections.permitid = permits.ID) ";
						}
						boolInspections = false;
					}
					else if (boolInspectors)
					{
						// strReturn = " inspectors inner join (" & strReturn & ") on (inspections.inspectorid = inspectors.ID) "
						strReturn = " inspectors right join (" + strReturn + ") on (inspections.inspectorid = inspectors.ID) ";
						boolInspectors = false;
						// ElseIf boolPermitContractors Then
						// If strReturn = "" Then
						// strReturn = " permits inner join permitcontractors on (permits.ID = permitcontractors.permitidNUM) "
						// Else
						// strReturn = " permitcontractors inner join (" & strReturn & ") on (permitcontractors.permitidNUM = permits.ID) "
						// End If
						// boolPermitContractors = False
					}
					else if (boolContractors)
					{
						// If strReturn = "" Then
						// strReturn = " permits inner join contractors on (permits.contractorid = contractors.ID) "
						// Else
						// strReturn = " contractors inner join (" & strReturn & ") on (contractors.ID = permits.contractorid) "
						// End If
						if (strReturn == "")
						{
							strReturn = " permits inner join permitcontractors on (permits.ID = permitcontractors.permitidNUM) ";
						}
						else
						{
							strReturn = " permitcontractors inner join (" + strReturn + ") on (permitcontractors.permitidNUM = permits.ID) ";
						}
						if (strReturn == "")
						{
							strReturn = " permitcontractors left join contractors on (permitcontractors.contractorid = contractors.ID) ";
						}
						else
						{
							strReturn = " contractors right join (" + strReturn + ") on (permitcontractors.contractorid = contractors.ID) ";
						}
						boolContractors = false;
						boolPermitContractors = false;
					}
					else if (boolContractorClass)
					{
						strReturn = " classcodes right join (" + strReturn + ") on (classcodes.contractorid = contractors.ID) ";
						boolContractorClass = false;
					}
					else if (boolPermitContractors)
					{
						if (strReturn == "")
						{
							strReturn = " permits inner join permitcontractors on (permits.ID = permitcontractors.permitidnum) ";
						}
						else
						{
							strReturn = " permitcontractors inner join (" + strReturn + ") on (permitcontractors.permitidnum = permits.ID) ";
						}
						boolPermitContractors = false;
					}
				}
				// x
			}
			string[] strAry = null;
			if (strOpenProps != "")
			{
				strAry = Strings.Split(strOpenProps, ",", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					if (intCount == 1)
					{
						// strReturn = " usercodevalues as ucv" & strAry(x) & " inner join " & strReturn & " on (cemaster.ceaccount = ucv" & strAry(x) & ".account) "
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join " + strReturn + " on (mj.ceaccount = ucv" + strAry[x] + ".account) ";
					}
					else
					{
						// strReturn = " usercodevalues as ucv" & strAry(x) & " inner join (" & strReturn & ") on (cemaster.ceaccount = ucv" & strAry(x) & ".account) "
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join (" + strReturn + ") on (mj.ceaccount = ucv" + strAry[x] + ".account) ";
					}
					intCount += 1;
				}
				// x
			}
			if (strOpenPermits != "")
			{
				strAry = Strings.Split(strOpenPermits, ",", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					if (intCount == 1)
					{
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join " + strReturn + " on (permits.ID = ucv" + strAry[x] + ".account) ";
					}
					else
					{
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join (" + strReturn + ") on (permits.ID = ucv" + strAry[x] + ".account) ";
					}
					intCount += 1;
				}
				// x
			}
			if (strOpenInspections != "")
			{
				strAry = Strings.Split(strOpenInspections, ",", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					if (intCount == 1)
					{
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join " + strReturn + " on (inspections.ID = ucv" + strAry[x] + ".account) ";
					}
					else
					{
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join (" + strReturn + ") on (inpsections.ID = ucv" + strAry[x] + ".account) ";
					}
					intCount += 1;
				}
				// x
			}
			if (strOpenContractors != "")
			{
				strAry = Strings.Split(strOpenContractors, ",", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
				{
					if (intCount == 1)
					{
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join " + strReturn + " on (contractors.ID = ucv" + strAry[x] + ".account) ";
					}
					else
					{
						strReturn = " usercodevalues as ucv" + strAry[x] + " inner join (" + strReturn + ") on (contractors.ID = ucv" + strAry[x] + ".account) ";
					}
					intCount += 1;
				}
				// x
			}
			GetInnerJoin = strReturn;
			return GetInnerJoin;
		}
	}
}
