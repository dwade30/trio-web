//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	public partial class frmInspection : BaseForm
	{
		public frmInspection()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmInspection InstancePtr
		{
			get
			{
				return (frmInspection)Sys.GetInstance(typeof(frmInspection));
			}
		}

		protected frmInspection _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int lngCurrentInspection;
		int lngPermitID;
		int lngCurrentAccount;
		bool boolDataChanged;
		private bool boolChangeStatus;
		private int lngOriginalStatus;
		private int lngPermitType;
		const int CNSTGRIDOPENSCOLAUTOID = 0;
		const int CNSTGRIDOPENSCOLDESC = 1;
		const int CNSTGRIDOPENSCOLVALUE = 2;
		// Private Const CNSTGRIDOPENSCOLDATE = 3
		// Private Const CNSTGRIDOPENSCOLUSEVALUE = 4
		// Private Const CNSTGRIDOPENSCOLUSEDATE = 5
		const int CNSTGRIDOPENSCOLVALUEFORMAT = 3;
		const int CNSTGRIDOPENSCOLMANDATORY = 4;
		const int CNSTGRIDOPENSCOLMIN = 5;
		const int CNSTGRIDOPENSCOLMAX = 6;
		const int CNSTGRIDVIOLATIONSCOLAUTOID = 0;
		const int CNSTGRIDVIOLATIONSCOLIDENTIFIER = 1;
		const int CNSTGRIDVIOLATIONSCOLTYPE = 2;
		const int CNSTGRIDVIOLATIONSCOLSTATUS = 4;
		const int CNSTGRIDVIOLATIONSCOLISSUED = 3;
		const int CNSTGRIDCHECKLISTCOLAUTOID = 1;
		const int CNSTGRIDCHECKLISTCOLCHECKLISTITEMID = 2;
		const int CNSTGRIDCHECKLISTCOLCOMPLETED = 3;
		const int CNSTGRIDCHECKLISTCOLDATECOMPLETED = 4;
		const int CNSTGRIDCHECKLISTCOLDESCRIPTION = 5;
		const int CNSTGRIDCHECKLISTCOLMANDATORY = 6;
		const int CNSTGRIDCHECKLISTCOLHEADER = 0;
		private bool boolCantEdit;
		// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
		// vbPorter upgrade warning: lngPType As int	OnWriteFCConvert.ToDouble(
		public bool Init(int lngID, int lngPermit = 0, int lngPType = 0)
		{
			bool Init = false;
			Init = false;
			boolChangeStatus = false;
			lngCurrentInspection = lngID;
			lngPermitID = lngPermit;
			lngPermitType = lngPType;
			if (lngID < 1 && lngPermit < 1)
			{
				MessageBox.Show("Error loading inspection" + "\r\n" + "Missing permit number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return Init;
			}
			SetupScreen();
			LoadInspection(lngCurrentInspection);
			boolDataChanged = false;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = boolChangeStatus;
			return Init;
		}

		private void frmInspection_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmInspection_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInspection properties;
			//frmInspection.FillStyle	= 0;
			//frmInspection.ScaleWidth	= 9300;
			//frmInspection.ScaleHeight	= 7905;
			//frmInspection.LinkTopic	= "Form2";
			//frmInspection.LockControls	= -1  'True;
			//frmInspection.PaletteMode	= 1  'UseZOrder;
			//rtbComments properties;
			//rtbComments.AutoVerbMenu	= -1  'True;
			//rtbComments.TextRTF	= $"frmInspection.frx":05C2;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SetTRIOColors Me
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITINSPECTIONSSECURITY, false))
			{
				DisableControls();
				boolCantEdit = true;
			}
		}

		private void SetupScreen()
		{
			FillcmbInspector();
			FillcmbType();
			FillcmbStatus();
			// FillcmbViolations
			SetupGridOpens(0);
			SetupGridViolations();
			// SetupGridChecklist
			// If GridChecklist.Rows > 1 Then
			// SSTab1.Tab = 2
			// End If
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveInspection())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmInspection_Resize(object sender, System.EventArgs e)
		{
			ResizeGridOpens();
			ResizeGridViolations();
			// ResizeGridCheckList
		}

		private void mnuDeleteInspection_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentInspection > 0)
			{
				if (MessageBox.Show("Are you sure you want to delete this inspection?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (modMain.DeleteInspection(lngCurrentInspection))
					{
						MessageBox.Show("Inpsection Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
						mnuExit_Click();
					}
				}
			}
			else
			{
				mnuExit_Click();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintViolations_Click(object sender, System.EventArgs e)
		{
			rptViolations.InstancePtr.Init(lngCurrentAccount, lngPermitID, lngCurrentInspection, this.Modal);
		}

		private void FillcmbInspector()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from inspectors order by NAME", modGlobalVariables.Statics.strCEDatabase);
			cmbInspector.Clear();
			cmbInspector.AddItem("Unknown");
			cmbInspector.ItemData(cmbInspector.NewIndex, 0);
			while (!clsLoad.EndOfFile())
			{
				cmbInspector.AddItem(clsLoad.Get_Fields_String("name"));
				cmbInspector.ItemData(cmbInspector.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")));
				clsLoad.MoveNext();
			}
			cmbInspector.SelectedIndex = 0;
		}

		private void FillcmbType()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONTYPE), modGlobalVariables.Statics.strCEDatabase);
			cmbType.Clear();
			while (!clsLoad.EndOfFile())
			{
				cmbType.AddItem(clsLoad.Get_Fields_String("description"));
				cmbType.ItemData(cmbType.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("code")));
				clsLoad.MoveNext();
			}
			if (cmbType.Items.Count > 0)
			{
				cmbType.SelectedIndex = 0;
			}
		}

		private void FillcmbStatus()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONSTATUS), modGlobalVariables.Statics.strCEDatabase);
			cmbStatus.Clear();
			cmbStatus.AddItem("Scheduled");
			cmbStatus.ItemData(cmbStatus.NewIndex, modCEConstants.CNSTINSPECTIONPENDING);
			cmbStatus.AddItem("Completed (Last Inspection)");
			cmbStatus.ItemData(cmbStatus.NewIndex, modCEConstants.CNSTINSPECTIONCOMPLETE);
			// .AddItem ("Violation")
			// .ItemData(.NewIndex) = CNSTINSPECTIONVIOLATION
			while (!clsLoad.EndOfFile())
			{
				cmbStatus.AddItem(clsLoad.Get_Fields_String("description"));
				cmbStatus.ItemData(cmbStatus.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields("code")));
				clsLoad.MoveNext();
			}
			if (cmbStatus.Items.Count > 0)
			{
				cmbStatus.SelectedIndex = 0;
			}
		}
		// Private Sub FillcmbViolations()
		// Dim clsLoad As New clsdrwrapper
		//
		// Call clsLoad.OpenRecordset("select * from systemcodes where codetype = " & CNSTCODETYPEINSPECTIONVIOLATIONS, strCEDatabase)
		// With cmbViolations
		// .Clear
		// .AddItem ("None")
		// .ItemData(.NewIndex) = CNSTINSPECTIONVIOLATIONSNONE
		// Do While Not clsLoad.EndOfFile
		// .AddItem (clsLoad.Fields("description"))
		// .ItemData(.NewIndex) = clsLoad.Fields("code")
		// clsLoad.MoveNext
		// Loop
		// If .ListCount > 0 Then
		// .ListIndex = 0
		// End If
		// End With
		// End Sub
		private void LoadInspection(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int x;
				clsLoad.OpenRecordset("select * from inspections where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				if (!clsLoad.EndOfFile())
				{
					if (lngPermitID == 0)
					{
						lngPermitID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("permitid"))));
					}
					if (Information.IsDate(clsLoad.Get_Fields("InspectionDate")))
					{
						if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("InspectionDate")).ToOADate() != 0)
						{
							t2kDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("inspectiondate"), "MM/dd/yyyy");
						}
						else
						{
							t2kDate.Text = "";
						}
					}
					else
					{
						t2kDate.Text = "";
					}
					cmbInspector.SelectedIndex = 0;
					if (Conversion.Val(clsLoad.Get_Fields_Int32("inspectorid")) > 0)
					{
						for (x = 0; x <= cmbInspector.Items.Count - 1; x++)
						{
							if (cmbInspector.ItemData(x) == Conversion.Val(clsLoad.Get_Fields_Int32("inspectorid")))
							{
								cmbInspector.SelectedIndex = x;
								break;
							}
						}
						// x
					}
					if (cmbType.Items.Count > 0)
					{
						cmbType.SelectedIndex = 0;
					}
					if (Conversion.Val(clsLoad.Get_Fields_Int32("inspectiontype")) > 0)
					{
						for (x = 0; x <= cmbType.Items.Count - 1; x++)
						{
							if (cmbType.ItemData(x) == Conversion.Val(clsLoad.Get_Fields_Int32("Inspectiontype")))
							{
								cmbType.SelectedIndex = x;
								break;
							}
						}
						// x
					}
					if (cmbStatus.Items.Count > 0)
					{
						cmbStatus.SelectedIndex = 0;
					}
					lngOriginalStatus = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("inspectionstatus"))));
					// If Val(clsLoad.Fields("inspectionstatus")) > 0 Then
					for (x = 0; x <= cmbStatus.Items.Count - 1; x++)
					{
						if (cmbStatus.ItemData(x) == Conversion.Val(clsLoad.Get_Fields_Int32("Inspectionstatus")))
						{
							cmbStatus.SelectedIndex = x;
							break;
						}
					}
					// x
					// End If
					// If Val(clsLoad.Fields("inspectionstatus")) = CNSTINSPECTIONVIOLATION Then
					// cmbViolations.Enabled = True
					// For x = 0 To cmbViolations.ListCount - 1
					// If cmbViolations.ItemData(x) = Val(clsLoad.Fields("violationtype")) Then
					// cmbViolations.ListIndex = x
					// Exit For
					// End If
					// Next x
					// Else
					// cmbViolations.Enabled = False
					// cmbViolations.ListIndex = 0
					// End If
					rtbComments.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Comment"));
					LoadGridOpens();
					// LoadGridChecklist
				}
				else
				{
					if (cmbStatus.Items.Count > 0)
					{
						cmbStatus.SelectedIndex = 0;
					}
					if (cmbType.Items.Count > 0)
					{
						cmbType.SelectedIndex = 0;
					}
					t2kDate.Text = "";
					cmbInspector.SelectedIndex = 0;
					rtbComments.Text = "";
					lngOriginalStatus = 0;
				}
				FillPermitInfo();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckGridOpens()
		{
			bool CheckGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				string strTemp = "";
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				int lngTemp = 0;
				for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
				{
					// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
					if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
					{
						if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
						{
							MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CheckGridOpens;
						}
					}
					dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
					dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
					if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
					{
						strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
						dblTemp = Conversion.Val(strTemp);
						if (dblTemp != 0)
						{
							if (dblMin != 0 && dblMin > dblTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
							if (dblMax != 0 && dblMax < dblTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
						}
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (dblTemp == 0)
							{
								if (dblMin > dblTemp || dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckGridOpens;
								}
							}
						}
					}
					else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
					{
						strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
						if (lngTemp != 0)
						{
							if (dblMin != 0 && dblMin > lngTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
							if (dblMax != 0 && dblMax < lngTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
						}
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (lngTemp == 0)
							{
								if (dblMin > lngTemp || dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckGridOpens;
								}
							}
						}
					}
				}
				// lngRow
				CheckGridOpens = true;
				return CheckGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return CheckGridOpens;
		}

		private bool SaveInspection()
		{
			bool SaveInspection = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				bool boolOK;
				int lngTemp = 0;
				DateTime dtTemp;
				// vbPorter upgrade warning: dtDate As DateTime	OnWrite(string)
				DateTime dtDate;
				bool boolNew;
				int lngTempInspection = 0;
				string strTemp = "";
				boolNew = false;
				SaveInspection = false;
				boolOK = true;
				if (!CheckGridOpens())
				{
					return SaveInspection;
				}
				clsSave.OpenRecordset("select * from inspections where ID = " + FCConvert.ToString(lngCurrentInspection), modGlobalVariables.Statics.strCEDatabase);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
					boolNew = true;
				}
				if (cmbStatus.SelectedIndex >= 0)
				{
					if (cmbStatus.ItemData(cmbStatus.SelectedIndex) != modCEConstants.CNSTINSPECTIONPENDING)
					{
						if (Information.IsDate(t2kDate.Text))
						{
							// check to see if it hasn't been added yet
							clsTemp.OpenRecordset("select * from activitylog where parenttype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " and parentid = " + FCConvert.ToString(lngPermitID) + " and referencetype = " + FCConvert.ToString(modCEConstants.CNSTACTIVITYTYPEINSPECTION) + " and referenceid = " + FCConvert.ToString(lngCurrentInspection), modGlobalVariables.Statics.strCEDatabase);
							if (clsTemp.EndOfFile())
							{
								clsActivity tAct = new clsActivity();
								tAct.Account = lngCurrentAccount;
								tAct.ActivityDate = FCConvert.ToDateTime(t2kDate.Text);
								tAct.ActivityType = "Inspection";
								tAct.Description = cmbStatus.Text;
								tAct.Detail = rtbComments.Text;
								tAct.ParentID = lngPermitID;
								tAct.ParentType = modCEConstants.CNSTCODETYPEPERMIT;
								tAct.ReferenceID = lngCurrentInspection;
								tAct.ReferenceType = modCEConstants.CNSTACTIVITYTYPEINSPECTION;
								tAct.SaveActivity();
							}
						}
					}
					clsSave.Set_Fields("InspectionStATUS", cmbStatus.ItemData(cmbStatus.SelectedIndex));
					if (cmbStatus.ItemData(cmbStatus.SelectedIndex) != modCEConstants.CNSTINSPECTIONCOMPLETE)
					{
						if (lngOriginalStatus == modCEConstants.CNSTINSPECTIONCOMPLETE)
						{
							boolChangeStatus = true;
						}
						else
						{
							boolChangeStatus = false;
						}
					}
					else
					{
						boolChangeStatus = false;
					}
				}
				else
				{
					clsSave.Set_Fields("inspectionstatus", 0);
					boolChangeStatus = false;
				}
				if (cmbType.SelectedIndex >= 0)
				{
					clsSave.Set_Fields("InspectionType", cmbType.ItemData(cmbType.SelectedIndex));
				}
				else
				{
					clsSave.Set_Fields("InspectionType", 0);
				}
				// 
				// If cmbViolations.ListIndex >= 0 Then
				// clsSave.Fields("violationtype") = cmbViolations.ItemData(cmbViolations.ListIndex)
				// Else
				// clsSave.Fields("ViolationType") = 0
				// End If
				clsSave.Set_Fields("Comment", fecherFoundation.Strings.Trim(rtbComments.Text));
				if (Information.IsDate(t2kDate.Text))
				{
					clsSave.Set_Fields("InspectionDate", t2kDate.Text);
				}
				else
				{
					clsSave.Set_Fields("InspectionDate", 0);
				}
				if (cmbInspector.SelectedIndex >= 0)
				{
					clsSave.Set_Fields("InspectorID", cmbInspector.ItemData(cmbInspector.SelectedIndex));
				}
				else
				{
					clsSave.Set_Fields("InspectorID", 0);
				}
				clsSave.Set_Fields("PermitID", lngPermitID);
				clsSave.Update();
				lngCurrentInspection = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				boolOK = boolOK && SaveGridOpens();
				if (!boolOK)
					return SaveInspection;
				// boolOK = boolOK And SaveGridChecklist
				if (cmbStatus.Items.Count > 0)
				{
					if (cmbStatus.ItemData(cmbStatus.SelectedIndex) == modCEConstants.CNSTINSPECTIONCOMPLETE)
					{
						clsSave.Execute("update permits set appdencode = " + FCConvert.ToString(modCEConstants.CNSTPERMITCOMPLETE) + " where ID = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
					}
				}
				// if the permit is not set to complete or scheduled then check if there are any scheduled inspections after this
				if (Information.IsDate(t2kDate.Text))
				{
					// need a valid date to compare to
					dtDate = FCConvert.ToDateTime(t2kDate.Text);
					if (boolNew && modGlobalVariables.Statics.CECustom.AutoAddInspectionsToCalendar)
					{
						modMain.AddInspectionToCalendar(lngCurrentInspection, fecherFoundation.Strings.Trim(rtbComments.Text), fecherFoundation.Strings.Trim(lblLocation.Text), fecherFoundation.Strings.Trim(lblPermitType.Text) + " Inspection", fecherFoundation.DateAndTime.DateAdd("h", 8, dtDate));
					}
					if (cmbStatus.SelectedIndex >= 0)
					{
						lngTemp = cmbStatus.ItemData(cmbStatus.SelectedIndex);
						if (lngTemp != modCEConstants.CNSTINSPECTIONCOMPLETE && lngTemp != modCEConstants.CNSTINSPECTIONPENDING)
						{
							clsSave.OpenRecordset("select * from inspections where permitid = " + FCConvert.ToString(lngPermitID) + " and inspectiondate > '" + FCConvert.ToString(dtDate) + "'", modGlobalVariables.Statics.strCEDatabase);
							if (clsSave.EndOfFile())
							{
								if (MessageBox.Show("Do you want to assign an inspection date for the next inspection?", "Schedule Inspection?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									bool boolChose = false;
									boolChose = false;
									while (!boolChose)
									{
										dtTemp = fecherFoundation.DateAndTime.DateAdd("d", 1, DateTime.Today);
										if (fecherFoundation.DateAndTime.DateDiff("d", dtTemp, dtDate) > 0)
										{
											dtTemp = fecherFoundation.DateAndTime.DateAdd("d", 1, dtDate);
										}
										object temp = dtTemp;
										if (frmInput.InstancePtr.Init(ref temp, "Inspection Date", "", 1700, false, modGlobalConstants.InputDTypes.idtDate))
										{
											dtTemp = FCConvert.ToDateTime(temp);
											if (dtTemp.Year > 2004)
											{
												// valid date
												if (fecherFoundation.DateAndTime.DateDiff("d", dtDate, dtTemp) >= 0)
												{
													// create an inspection
													clsSave.OpenRecordset("select * from INSPECTIONS where ID = -1", modGlobalVariables.Statics.strCEDatabase);
													clsSave.AddNew();
													clsSave.Set_Fields("inspectorid", 0);
													clsSave.Set_Fields("inspectiondate", dtTemp);
													clsSave.Set_Fields("inspectionstatus", modCEConstants.CNSTINSPECTIONPENDING);
													clsSave.Set_Fields("permitid", lngPermitID);
													clsSave.Set_Fields("inspectiontype", modCEConstants.CNSTINSPECTIONTYPEUNKOWN);
													clsSave.Update();
													lngTempInspection = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
													boolChose = true;
													if (lngTempInspection > 0 && modGlobalVariables.Statics.CECustom.AutoAddInspectionsToCalendar)
													{
														modMain.AddInspectionToCalendar(lngTempInspection, fecherFoundation.Strings.Trim(rtbComments.Text), fecherFoundation.Strings.Trim(lblLocation.Text), fecherFoundation.Strings.Trim(lblPermitType.Text) + " inspection", fecherFoundation.DateAndTime.DateAdd("h", 8, dtTemp));
													}
												}
												else
												{
													// bad date
													MessageBox.Show("The next inspection cannot be before the current inspection", "Bad Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
												}
											}
										}
										else
										{
											boolChose = true;
										}
									}
								}
							}
						}
					}
				}
				SaveInspection = boolOK;
				boolDataChanged = false;
				if (boolOK)
				{
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return SaveInspection;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInspection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInspection;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridOpens.Row = 0;
            //FC:FINAL:AKV:#3685 - Should close out after save
            //SaveInspection();
            if (SaveInspection())
            {
                mnuExit_Click();
            }
        }

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridOpens.Row = 0;
			if (SaveInspection())
			{
				mnuExit_Click();
			}
		}

		private void FillPermitInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsTemp = new clsDRWrapper();
			string strType = "";
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsLoad.OpenRecordset("select * from permits where ID = " + FCConvert.ToString(lngPermitID), modGlobalVariables.Statics.strCEDatabase);
				if (!clsLoad.EndOfFile())
				{
					lblPermit.Text = Strings.Right(FCConvert.ToString(clsLoad.Get_Fields_Int32("permityear")), 2) + "-" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("permitnumber")));
					lngCurrentAccount = FCConvert.ToInt32(clsLoad.Get_Fields("account"));
					lblName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("originalowner")));
					lblIdentifier.Text = FCConvert.ToString(clsLoad.Get_Fields_String("permitidentifier"));
					lblMapLot.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("originalmaplot")));
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("originalstreetnum"))) != "" && fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("originalstreetnum"))) != "0")
					{
						lblLocation.Text = fecherFoundation.Strings.Trim(clsLoad.Get_Fields_String("originalstreetnum") + " " + clsLoad.Get_Fields_String("originalstreet"));
					}
					else
					{
						lblLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("originalstreet")));
					}
					if (Conversion.Val(clsLoad.Get_Fields_Int32("COMMRESCODE")) == modCEConstants.CNSTPERMITCODECOMMERCIAL)
					{
						lblCommRes.Text = "Commercial";
					}
					else
					{
						lblCommRes.Text = "Residential";
					}
					if (lngPermitType == 0)
					{
						lngPermitType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("permittype"))));
					}
					clsTemp.OpenRecordset("select * from systemcodes where ID = " + FCConvert.ToString(lngPermitType) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE), modGlobalVariables.Statics.strCEDatabase);
					strType = "";
					if (!clsTemp.EndOfFile())
					{
						strType = FCConvert.ToString(clsTemp.Get_Fields_String("Description"));
						// & " - "
						// Call clsTemp.OpenRecordset("select * from systemcodes where code = " & Val(clsLoad.Fields("permitsubtype")) & " and codetype = " & CNSTCODETYPEPERMITSUBTYPE & " and parentcode = " & lngTemp, strCEDatabase)
						// If Not clsTemp.EndOfFile Then
						// strType = strType & clsTemp.Fields("description")
						// End If
					}
					lblPermitType.Text = strType;
					lblPlan.Text = FCConvert.ToString(clsLoad.Get_Fields_String("plan"));
					if (Information.IsDate(clsLoad.Get_Fields("applicationdate")))
					{
						if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("applicationdate")).ToOADate() != 0)
						{
							lblApplicationDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("applicationdate"), "MM/dd/yyyy");
						}
					}
					CheckCategory(lngPermitType);
					FillGridViolations();
					// FillAccountInformation
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "in FillPermitInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Private Sub FillAccountInformation()
		// Dim clsLoad As New clsdrwrapper
		//
		// Call clsLoad.OpenRecordset("select * from cemaster where ceaccount = " & lngCurrentAccount, strCEDatabase)
		//
		// If Not clsLoad.EndOfFile Then
		// lblAccount.Text = lngCurrentAccount
		// lblLocation.Text = Trim(clsLoad.Fields("rslocnumalph") & " " & clsLoad.Fields("rslocstreet"))
		// lblName.Text = Trim(clsLoad.Fields("rsname"))
		// lblMapLot.Text = clsLoad.Fields("rsmaplot")
		// Else
		// lblAccount.Text = ""
		// lblLocation.Text = ""
		// lblName.Text = ""
		// lblMapLot.Text = ""
		// End If
		// End Sub
		private void GridOpens_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			Row = GridOpens.MouseRow;
			Col = GridOpens.MouseCol;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDOPENSCOLVALUE:
						{
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEVALUE)) Then
							if (!(FCConvert.ToString(GridOpens.Tag) == "UNAUTHORIZED"))
							{
								GridOpens.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(Row));
							if (Conversion.Val(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
							{
								GridOpens.EditMask = "00/00/0000";
							}
							else
							{
								GridOpens.EditMask = "";
							}
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							// Case CNSTGRIDOPENSCOLDATE
							// GridOpens.ComboList = ""
							// GridOpens.EditMask = "00/00/0000"
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEDATE)) Then
							// GridOpens.Editable = flexEDKbdMouse
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							break;
						}
				}
				//end switch
			}
		}

		private void GridOpens_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridOpens.GetFlexRowIndex(e.RowIndex);
            int col = GridOpens.GetFlexColIndex(e.ColumnIndex);

            if (row < 1)
				return;
			string strTemp = "";
			string[] strAry = null;
            //FC:FINAL:AM:#3741 - don't reset the EditMask or the date will have an incorrect format
			//GridOpens.EditMask = "";
			if (col == CNSTGRIDOPENSCOLVALUE)
			{
				if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					strTemp = GridOpens.EditText;
					if (Information.IsDate(strTemp))
					{
						strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					}
					else
					{
						strTemp = "";
					}
					GridOpens.EditText = strTemp;
					// Case CNSTVALUETYPEBOOLEAN
					// strTemp = GridOpens.EditText
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strAry = Strings.Split(strTemp, ".", -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Format(strAry[0], "#,###,###,##0");
					if (Information.UBound(strAry, 1) > 0)
					{
						strTemp += "." + strAry[1];
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					strTemp = GridOpens.ComboData();
					GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, Conversion.Val(strTemp));
				}
				// Case CNSTGRIDOPENSCOLDATE
			}
		}

		private void ResizeGridOpens()
		{
			int GridWidth = 0;
			GridWidth = GridOpens.WidthOriginal;
			GridOpens.ColWidth(CNSTGRIDOPENSCOLDESC, FCConvert.ToInt32(0.4 * GridWidth));
			GridOpens.ColWidth(CNSTGRIDOPENSCOLVALUE, FCConvert.ToInt32(0.35 * GridWidth));
		}
		// Private Sub SetupGridChecklist()
		// On Error GoTo ErrorHandler
		// Dim rsLoad As New clsdrwrapper
		// Call rsLoad.OpenRecordset("select * from inspectionchecklistsetup where permittype = " & lngPermitType & " order by orderno", strCEDatabase)
		// With GridChecklist
		// .Rows = 1
		// .TextMatrix(0, CNSTGRIDCHECKLISTCOLDATECOMPLETED) = "Date"
		// .TextMatrix(0, CNSTGRIDCHECKLISTCOLDESCRIPTION) = "Description"
		// .TextMatrix(0, CNSTGRIDCHECKLISTCOLCOMPLETED) = ""
		// .ColDataType(CNSTGRIDCHECKLISTCOLCOMPLETED) = flexDTBoolean
		// .ColDataType(CNSTGRIDCHECKLISTCOLDATECOMPLETED) = flexDTDate
		// .ColDataType(CNSTGRIDCHECKLISTCOLMANDATORY) = flexDTBoolean
		// .ColHidden(CNSTGRIDCHECKLISTCOLAUTOID) = True
		// .ColHidden(CNSTGRIDCHECKLISTCOLCHECKLISTITEMID) = True
		// .ColHidden(CNSTGRIDCHECKLISTCOLMANDATORY) = True
		// .ColAlignment(CNSTGRIDCHECKLISTCOLDATECOMPLETED) = flexAlignLeftCenter
		// .ColAlignment(CNSTGRIDCHECKLISTCOLDESCRIPTION) = flexAlignLeftCenter
		// .ColEditMask(CNSTGRIDCHECKLISTCOLDATECOMPLETED) = "00/00/0000"
		// Dim lngRow As Long
		// Do While Not rsLoad.EndOfFile
		// GridChecklist.Rows = GridChecklist.Rows + 1
		// lngRow = GridChecklist.Rows - 1
		// .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCHECKLISTITEMID) = rsLoad.Fields("ID")
		// .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID) = 0
		// .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCOMPLETED) = False
		// .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLMANDATORY) = rsLoad.Fields("mandatory")
		// .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLHEADER) = rsLoad.Fields("Description")
		// If rsLoad.Fields("mandatory") Then
		// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, .Cols - 1) = TRIOCOLORHIGHLIGHT
		// End If
		// rsLoad.MoveNext
		// Loop
		// End With
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In SetupGridChecklist", vbCritical, "Error"
		// End Sub
		// Private Sub ResizeGridCheckList()
		// Dim lngWidth As Long
		// lngWidth = GridChecklist.Width
		// GridChecklist.ColWidth(CNSTGRIDCHECKLISTCOLHEADER) = 0.33 * lngWidth
		// GridChecklist.ColWidth(CNSTGRIDCHECKLISTCOLCOMPLETED) = 0.06 * lngWidth
		// GridChecklist.ColWidth(CNSTGRIDCHECKLISTCOLDATECOMPLETED) = 0.13 * lngWidth
		//
		// End Sub
		private void SetupGridOpens(int lngSubType)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsCodes = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			GridOpens.Cols = 7;
			GridOpens.Rows = 1;
			GridOpens.ColHidden(CNSTGRIDOPENSCOLAUTOID, true);
			// .ColHidden(CNSTGRIDOPENSCOLUSEVALUE) = True
			// .ColHidden(CNSTGRIDOPENSCOLUSEDATE) = True
			GridOpens.ColHidden(CNSTGRIDOPENSCOLVALUEFORMAT, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMANDATORY, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMAX, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMIN, true);
			GridOpens.ColDataType(CNSTGRIDOPENSCOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLDESC, "Item");
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLVALUE, "Value");
			// .TextMatrix(0, CNSTGRIDOPENSCOLDATE) = "Date"
			//GridOpens.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLVALUE, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLDATE) = flexAlignCenterCenter
			clsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " AND subtype = " + FCConvert.ToString(lngSubType) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				GridOpens.Rows += 1;
				lngRow = GridOpens.Rows - 1;
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(false));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("MINVALUE"))));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("maxvalue"))));
				// If Not clsLoad.Fields("usevalue") Then
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLVALUE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = False
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = True
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("valueformat"))));
				if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					// get the list of drop downs
					strTemp = "";
					clsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + clsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " order by code", modGlobalVariables.Statics.strCEDatabase);
					while (!clsCodes.EndOfFile())
					{
						strTemp += "#" + clsCodes.Get_Fields("code") + ";" + clsCodes.Get_Fields_String("description") + "|";
						clsCodes.MoveNext();
					}
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					}
					GridOpens.RowData(lngRow, strTemp);
				}
				else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
					GridOpens.RowData(lngRow, "Yes|No");
				}
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("mandatory")))
				{
					GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(true));
				}
				// End If
				// If Not clsLoad.Fields("usedate") Then
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = False
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLDATE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = True
				// End If
				clsLoad.MoveNext();
			}
		}
		// Private Sub LoadGridChecklist()
		// On Error GoTo ErrorHandler
		// Dim rsLoad As New clsdrwrapper
		// If lngCurrentInspection > 0 Then
		// Dim lngRow As Long
		// Call rsLoad.OpenRecordset("select * from inspectionchecklist where inspectionid = " & lngCurrentInspection, strCEDatabase)
		// For lngRow = 1 To GridChecklist.Rows - 1
		// If rsLoad.FindFirstRecord("checklistitemid", Val(GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCHECKLISTITEMID))) Then
		// GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID) = rsLoad.Fields("ID")
		// GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCOMPLETED) = rsLoad.Fields("completed")
		// If IsDate(rsLoad.Fields("datecompleted")) Then
		// If rsLoad.Fields("datecompleted") <> 0 Then
		// GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED) = rsLoad.Fields("datecompleted")
		// End If
		// End If
		// GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDESCRIPTION) = rsLoad.Fields("comment")
		// End If
		// Next lngRow
		// End If
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In LoadGridCheckList", vbCritical, "Error"
		// End Sub
		private void LoadGridOpens()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				string[] strAry = null;
				string[] strAry2 = null;
                if (lngCurrentInspection > 0)
                {
                    clsLoad.OpenRecordset("select * from USERcodevalues where account = " + FCConvert.ToString(lngCurrentInspection) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
                        {
                            if (clsLoad.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
                            {
                                // If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
                                GridOpens.ComboList = "";
                                if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
                                {
                                    // not allowing type date at this time
                                    if (Information.IsDate(clsLoad.Get_Fields("datevalue")))
                                    {
                                        if (Convert.ToDateTime(clsLoad.Get_Fields_DateTime("datevalue")).ToOADate() != 0)
                                        {
                                            GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(clsLoad.Get_Fields_DateTime("datevalue"), "MM/dd/yyyy"));
                                        }
                                    }
                                }

                                else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
                                {
                                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("booleanvalue")))
                                    {
                                        GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "Yes");
                                    }
                                    else
                                    {
                                        GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "No");
                                    }
                                }
                                else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
                                {
                                    GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))));
                                }
                                else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
                                {
                                    GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(lngRow));
                                    // .TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) = Val(clsLoad.Fields("dropdownvalue"))
                                    GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE, Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")));
                                    strAry = Strings.Split(Strings.Replace(FCConvert.ToString(GridOpens.RowData(lngRow)), "#", "", 1, -1, CompareConstants.vbTextCompare), "|", -1, CompareConstants.vbTextCompare);
                                    for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
                                    {
                                        strAry2 = Strings.Split(strAry[x], ";", -1, CompareConstants.vbTextCompare);
                                        if (Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")) == Conversion.Val(strAry2[0]))
                                        {
                                            GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, strAry2[1]);
                                        }
                                    }
                                    // x
                                }
                                else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
                                {
                                    GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))), "#,###,###,##0"));
                                }
                                else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
                                {
                                    GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(clsLoad.Get_Fields_String("textvalue")));
                                }
                                // End If
                                // If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
                                // If IsDate(clsLoad.Fields("DateValue")) Then
                                // If clsLoad.Fields("datevalue") <> 0 Then
                                // .TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE) = Format(clsLoad.Fields("datevalue"), "MM/dd/yyyy")
                                // End If
                                // End If
                                // End If
                            }
                            // lngRow
                        }
                    }
                }
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Private Function SaveGridChecklist() As Boolean
		// On Error GoTo ErrorHandler
		// Dim rsSave As New clsdrwrapper
		// Dim lngRow As Long
		// Dim boolMatch As Boolean
		//
		// SaveGridChecklist = False
		//
		// If lngCurrentInspection > 0 Then
		// Call rsSave.OpenRecordset("select * from inspectionchecklist where inspectionid = " & lngCurrentInspection, strCEDatabase)
		// For lngRow = 1 To GridChecklist.Rows - 1
		// boolMatch = False
		// If Not rsSave.EndOfFile Then
		// If rsSave.FindFirstRecord("ID", GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID)) Then
		// boolMatch = True
		// End If
		// End If
		// If boolMatch Then
		// rsSave.Edit
		// Else
		// rsSave.AddNew
		// End If
		// With GridChecklist
		// rsSave.Fields("InspectionID") = lngCurrentInspection
		// rsSave.Fields("checklistitemid") = Val(.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCHECKLISTITEMID))
		// rsSave.Fields("completed") = .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCOMPLETED)
		// rsSave.Fields("Comment") = Trim(.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDESCRIPTION))
		// If IsDate(.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED)) Then
		// rsSave.Fields("datecompleted") = .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED)
		// Else
		// rsSave.Fields("DateCOmpleted") = 0
		// End If
		// .TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID) = rsSave.Fields("ID")
		// End With
		// rsSave.Update
		// Next lngRow
		// End If
		//
		// SaveGridChecklist = True
		// Exit Function
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In SaveGridChecklist", vbCritical, "Error"
		// End Function
		private bool SaveGridOpens()
		{
			bool SaveGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				bool boolMatch = false;
				string strTemp = "";
				int lngTemp = 0;
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				// shouldn't be able to get to this point without a permit number
				SaveGridOpens = false;
				if (lngCurrentInspection > 0)
				{
					clsSave.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(lngCurrentInspection) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
					{
						boolMatch = false;
						if (!clsSave.EndOfFile())
						{
							if (clsSave.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								boolMatch = true;
							}
						}
						if (boolMatch)
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
						clsSave.Set_Fields("ACCOUNT", lngCurrentInspection);
						clsSave.Set_Fields("codetype", modCEConstants.CNSTCODETYPEINSPECTION);
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))));
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveGridOpens;
							}
						}
						dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
						dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
						if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
						{
							// not allowing type date at this time
							strTemp = GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE);
							if (Information.IsDate(strTemp))
							{
								clsSave.Set_Fields("datevalue", Strings.Format(strTemp, "MM/dd/yyyy"));
							}
							else
							{
								clsSave.Set_Fields("datevalue", 0);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == "Yes")
							{
								clsSave.Set_Fields("booleanvalue", true);
							}
							else
							{
								clsSave.Set_Fields("booleanvalue", false);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							dblTemp = Conversion.Val(strTemp);
							if (dblTemp != 0)
							{
								if (dblMin != 0 && dblMin > dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (dblTemp == 0)
								{
									if (dblMin > dblTemp || dblMax < dblTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", dblTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							clsSave.Set_Fields("dropdownvalue", FCConvert.ToString(Conversion.Val(GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE))));
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							if (lngTemp != 0)
							{
								if (dblMin != 0 && dblMin > lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (lngTemp == 0)
								{
									if (dblMin > lngTemp || dblMax < lngTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", lngTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
						{
							clsSave.Set_Fields("textvalue", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE));
						}
						// End If
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
						// If IsDate(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE)) Then
						// clsSave.Fields("datevalue") = Format(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE), "MM/dd/yyyy")
						// End If
						// Else
						// clsSave.Fields("datevalue") = 0
						// End If
						clsSave.Update();
					}
					// lngRow
				}
				SaveGridOpens = true;
				return SaveGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Eror Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridOpens;
		}

		private void CheckCategory(int lngType)
		{
			// Dim clsLoad As New clsdrwrapper
			// Dim lngCat As Long
			// Dim lngRow As Long
			// 
			// On Error GoTo ErrorHandler
			// 
			// Call clsLoad.OpenRecordset("select * from systemcodes where ID = " & lngType & " and codetype = " & CNSTCODETYPEPERMITTYPE, strCEDatabase)
			// If Not clsLoad.EndOfFile Then
			// lngCat = Val(clsLoad.Fields("codespecificid"))
			// If lngCat = CNSTCATEGORYDEFAULT Then
			// For lngRow = 1 To GridOpens.Rows - 1
			// GridOpens.RowHidden(lngRow) = False
			// Next lngRow
			// Else
			// Call clsLoad.OpenRecordset("select * from catcodeoptions where categoryid = " & lngCat & " and codetype = " & CNSTCODETYPEINSPECTION & " order by usercodeid", strCEDatabase)
			// For lngRow = 1 To GridOpens.Rows - 1
			// If clsLoad.FindFirstRecord("usercodeid", Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))) Then
			// GridOpens.RowHidden(lngRow) = Not (clsLoad.Fields("boolshow"))
			// Else
			// GridOpens.RowHidden(lngRow) = False
			// End If
			// Next lngRow
			// End If
			// End If
			try
			{
				SetupGridOpens(lngPermitType);
				LoadGridOpens();
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckCategory", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
		}

		private void DisableControls()
		{
			int x;
			for (x = 0; x <= this.Controls.Count - 1; x++)
			{
				if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "    ", 4)) != "GRID" && !(this.Controls[x] is FCToolStripMenuItem) && !(this.Controls[x] is FCLabel) && !(this.Controls[x] is FCTabControl))
				{
					this.Controls[x].Enabled = false;
				}
				else if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "    ", 4)) == "GRID")
				{
					this.Controls[x].Tag = "UNAUTHORIZED";
					(this.Controls[x] as FCGrid).Editable = FCGrid.EditableSettings.flexEDNone;
					this.Controls[x].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
					if ((this.Controls[x] as FCGrid).Rows > 0 && (this.Controls[x] as FCGrid).Cols > 0)
					{
						(this.Controls[x] as FCGrid).Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, (this.Controls[x] as FCGrid).Rows - 1, (this.Controls[x] as FCGrid).Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
					}
				}
			}
			// x
			cmdDeleteInspection.Enabled = false;
			cmdSave.Enabled = false;
			mnuSaveExit.Enabled = false;
			cmdAddViolation.Enabled = false;
		}

		private void FillGridViolations()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				int x;
				gridViolations.Rows = 1;
				rsLoad.OpenRecordset("select * from violations where violationaccount = " + FCConvert.ToString(lngCurrentAccount) + " and violationaccount > 0 and violationpermit = " + FCConvert.ToString(lngPermitID) + " and violationpermit > 0 and violationinspection = " + FCConvert.ToString(lngCurrentInspection) + " and violationinspection > 0 order by dateviolationissued desc,violationidentifier", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					gridViolations.Rows += 1;
					lngRow = gridViolations.Rows - 1;
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLIDENTIFIER, FCConvert.ToString(rsLoad.Get_Fields_String("violationidentifier")));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus"))));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLTYPE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("violationtype"))));
					if (Information.IsDate(rsLoad.Get_Fields("dateviolationissued")))
					{
						if (Convert.ToDateTime(rsLoad.Get_Fields_DateTime("dateviolationissued")).ToOADate() != 0)
						{
							gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLISSUED, FCConvert.ToString(rsLoad.Get_Fields_DateTime("dateviolationissued")));
						}
					}
					if (cmbViolations.Text == "Show Current")
					{
						if ((Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus")) == modCEConstants.CNSTVIOLATIONSTATUSCLOSED) || (Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus")) == modCEConstants.CNSTVIOLATIONSTATUSRESOLVED))
						{
							gridViolations.RowHidden(lngRow, true);
						}
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridViolations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridViolations()
		{
			int GridWidth = 0;
			GridWidth = gridViolations.WidthOriginal;
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLIDENTIFIER, FCConvert.ToInt32(0.15 * GridWidth));
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLTYPE, FCConvert.ToInt32(0.32 * GridWidth));
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLISSUED, FCConvert.ToInt32(0.17 * GridWidth));
		}

		private void mnuAddViolation_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentInspection == 0)
			{
				MessageBox.Show("This inspection must be saved before you can add a violation", "Cannot Add", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmViolation.InstancePtr.Init(0, false, lngPermitID, lngCurrentAccount, lngCurrentInspection);
			FillGridViolations();
		}

		private void gridViolations_DblClick(object sender, System.EventArgs e)
		{
			if (gridViolations.MouseRow > 0)
			{
				if (!(fecherFoundation.Strings.LCase(FCConvert.ToString(gridViolations.Tag)) == "unauthorized"))
				{
					bool boolRonly = false;
					boolRonly = !cmdAddViolation.Enabled;
					frmViolation.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(gridViolations.TextMatrix(gridViolations.MouseRow, CNSTGRIDVIOLATIONSCOLAUTOID))), boolRonly);
					FillGridViolations();
				}
			}
		}

		private void SetupGridViolations()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLIDENTIFIER, "Identifier");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLISSUED, "Issued");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLSTATUS, "Status");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLTYPE, "Type");
			gridViolations.ColHidden(CNSTGRIDVIOLATIONSCOLAUTOID, true);
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONSTATUS) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			strTemp = "";
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTVIOLATIONSTATUSCLOSED) + ";Closed";
			gridViolations.ColComboList(CNSTGRIDVIOLATIONSCOLSTATUS, strTemp);
			strTemp = "";
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != "")
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			gridViolations.ColComboList(CNSTGRIDVIOLATIONSCOLTYPE, strTemp);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTVIOLATIONSSECURITY, false))
			{
				gridViolations.Tag = (System.Object)("UNAUTHORIZED");
				cmdAddViolation.Enabled = false;
			}
			else
			{
				gridViolations.Tag = (System.Object)("");
				if (modSecurity.ValidPermissions_6(this, modCEConstants.CNSTVIOLATIONSEDITSECURITY, false))
				{
					cmdAddViolation.Enabled = true;
				}
				else
				{
					cmdAddViolation.Enabled = false;
				}
			}
		}

		private void optViolations_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			int lngRow;
			switch (Index)
			{
				case 0:
					{
						// current
						for (lngRow = 1; lngRow <= gridViolations.Rows - 1; lngRow++)
						{
							if (Conversion.Val(gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS)) == modCEConstants.CNSTVIOLATIONSTATUSCLOSED || Conversion.Val(gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS)) == modCEConstants.CNSTVIOLATIONSTATUSRESOLVED)
							{
								gridViolations.RowHidden(lngRow, true);
							}
						}
						// lngRow
						break;
					}
				case 1:
					{
						// all
						for (lngRow = 1; lngRow <= gridViolations.Rows - 1; lngRow++)
						{
							gridViolations.RowHidden(lngRow, false);
						}
						// lngRow
						break;
					}
			}
			//end switch
		}

		private void optViolations_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbViolations.SelectedIndex;
			optViolations_CheckedChanged(index, sender, e);
		}
	}
}
