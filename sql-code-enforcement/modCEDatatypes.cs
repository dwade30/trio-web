﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCE0000
{
	public class modCEDatatypes
	{
		//=========================================================
		public struct PhoneRecord
		{
			public string PhoneNumber;
			public string Description;
			public int ID;
		};

		public struct Abutter
		{
			public int Account;
			public string Name;
			public int ID;
		};

		public struct UserDData
		{
			public int ID;
			public string Description;
			public string strValue;
			public double dblValue;
			public bool boolUseValue;
			public bool boolUseDate;
			public int intValType;
			public DateTime ItemDate;
		};
	}
}
