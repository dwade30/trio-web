//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmChooseEnvelope.
	/// </summary>
	partial class frmChooseEnvelope
	{
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCComboBox cmbEnvelopeType;
		public fecherFoundation.FCCheckBox chkReturnAddress;
		public fecherFoundation.FCLabel lblInfo;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdOK = new fecherFoundation.FCButton();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmbEnvelopeType = new fecherFoundation.FCComboBox();
            this.chkReturnAddress = new fecherFoundation.FCCheckBox();
            this.lblInfo = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReturnAddress)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 295);
            this.BottomPanel.Size = new System.Drawing.Size(440, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.cmdCancel);
            this.ClientArea.Controls.Add(this.cmbEnvelopeType);
            this.ClientArea.Controls.Add(this.chkReturnAddress);
            this.ClientArea.Controls.Add(this.lblInfo);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(440, 235);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(440, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(206, 30);
            this.HeaderText.Text = "Choose Envelope";
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(30, 170);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(64, 40);
            this.cmdOK.TabIndex = 4;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(114, 170);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(93, 40);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmbEnvelopeType
            // 
            this.cmbEnvelopeType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEnvelopeType.Location = new System.Drawing.Point(155, 77);
            this.cmbEnvelopeType.Name = "cmbEnvelopeType";
            this.cmbEnvelopeType.Size = new System.Drawing.Size(191, 40);
            this.cmbEnvelopeType.TabIndex = 1;
            this.cmbEnvelopeType.SelectedIndexChanged += new System.EventHandler(this.cmbEnvelopeType_SelectedIndexChanged);
            // 
            // chkReturnAddress
            // 
            this.chkReturnAddress.Checked = true;
            this.chkReturnAddress.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkReturnAddress.Location = new System.Drawing.Point(30, 30);
            this.chkReturnAddress.Name = "chkReturnAddress";
            this.chkReturnAddress.Size = new System.Drawing.Size(150, 26);
            this.chkReturnAddress.TabIndex = 5;
            this.chkReturnAddress.Text = "Print Return Address";
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(24, 123);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(212, 27);
            this.lblInfo.TabIndex = 5;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 91);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(109, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "ENVELOPE TYPE";
            // 
            // frmChooseEnvelope
            // 
            this.AcceptButton = this.cmdOK;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(440, 295);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmChooseEnvelope";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Choose Envelope";
            this.Load += new System.EventHandler(this.frmChooseEnvelope_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseEnvelope_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReturnAddress)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}