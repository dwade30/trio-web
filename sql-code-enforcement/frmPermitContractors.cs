﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmPermitContractors : BaseForm
	{
		public frmPermitContractors()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPermitContractors InstancePtr
		{
			get
			{
				return (frmPermitContractors)Sys.GetInstance(typeof(frmPermitContractors));
			}
		}

		protected frmPermitContractors _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private clsPermitContractorList clsContList;
		private clsPermitContractorList clsTempList;
		const int CNSTGRIDCOLMOVE = 0;
		const int CNSTGRIDCOLDESCRIPTION = 1;
		const int CNSTGRIDCOLINDEX = 2;
		private bool boolChangedOrder;
		private bool boolReadOnly;

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLINDEX, true);
			Grid.ColHidden(3, true);
			Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Contractor");
		}

		public void Init(ref clsPermitContractorList clsConts, bool boolCantEdit = false)
		{
			if (clsConts == null)
			{
				MessageBox.Show("No Contractors Found", "No Contractors", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			boolReadOnly = boolCantEdit;
			boolChangedOrder = false;
			clsContList = clsConts;
			clsTempList = clsContList.Copy();
			this.Show(FCForm.FormShowEnum.Modal);
			if (boolReadOnly)
			{
				Grid.ExplorerBar = FCGrid.ExplorerBarSettings.flexExNone;
			}
			clsConts = clsContList;
		}

		private void frmPermitContractors_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPermitContractors_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPermitContractors properties;
			//frmPermitContractors.FillStyle	= 0;
			//frmPermitContractors.ScaleWidth	= 5880;
			//frmPermitContractors.ScaleHeight	= 4230;
			//frmPermitContractors.LinkTopic	= "Form2";
			//frmPermitContractors.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			// SetTRIOColors Me
			SetupGrid();
			LoadContractors();
		}

		private void LoadContractors()
		{
			int lngRow;
			int intIndex;
			clsPermitContractor tCN;
			Grid.Rows = 1;
			clsTempList.MoveFirst();
			intIndex = clsTempList.GetCurrentIndex;
			while (intIndex >= 0)
			{
				tCN = clsTempList.GetCurrentContractor;
				if (!(tCN == null))
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, tCN.ContractorName);
					Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX, FCConvert.ToString(intIndex));
				}
				intIndex = clsTempList.MoveNext();
			}
		}

		private void frmPermitContractors_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterMoveRow(object sender, EventArgs e)
		{
			boolChangedOrder = true;
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLMOVE, FCConvert.ToInt32(0.05 * GridWidth));
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow > 0)
			{
				// view that record, but save changes first
				int lngRowToUse = 0;
				lngRowToUse = Grid.MouseRow;
				SaveChanges();
				clsContList.SetContractorByIndex(FCConvert.ToInt16(Conversion.Val(Grid.TextMatrix(lngRowToUse, CNSTGRIDCOLINDEX))));
				Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SaveChanges()
		{
			// check to see if order changed
			if (boolChangedOrder)
			{
				// copy list over
				boolChangedOrder = false;
				clsPermitContractorList tList = new clsPermitContractorList();
				int lngRow;
				clsPermitContractor tCont;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					tCont = clsTempList.Get_GetContractorByIndex(FCConvert.ToInt32(Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX)));
					if (!(tCont == null))
					{
						tCont.OrderNumber = lngRow;
					}
				}
				// lngRow
				clsTempList.ReOrder();
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					Grid.TextMatrix(lngRow, CNSTGRIDCOLINDEX, FCConvert.ToString(lngRow));
				}
				// lngRow
				clsContList.Clear();
				clsContList = clsTempList.Copy();
				clsContList.MoveFirst();
			}
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				// view that record, but save changes first
				int lngRowToUse = 0;
				lngRowToUse = Grid.Row;
				SaveChanges();
				clsContList.SetContractorByIndex(FCConvert.ToInt16(Conversion.Val(Grid.TextMatrix(lngRowToUse, CNSTGRIDCOLINDEX))));
				Close();
			}
		}
	}
}
