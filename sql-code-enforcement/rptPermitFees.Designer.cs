﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptPermitFees.
	/// </summary>
	partial class rptPermitFees
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPermitFees));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.txtTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtGroupCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtGroupFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtCount,
            this.txtValue,
            this.txtFee,
            this.txtCategory});
			this.Detail.Height = 0.21875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtCount
			// 
			this.txtCount.Height = 0.1770833F;
			this.txtCount.Left = 2.791667F;
			this.txtCount.Name = "txtCount";
			this.txtCount.Style = "text-align: right";
			this.txtCount.Text = null;
			this.txtCount.Top = 0F;
			this.txtCount.Width = 0.6458333F;
			// 
			// txtValue
			// 
			this.txtValue.Height = 0.1770833F;
			this.txtValue.Left = 3.53125F;
			this.txtValue.Name = "txtValue";
			this.txtValue.Style = "text-align: right";
			this.txtValue.Text = null;
			this.txtValue.Top = 0F;
			this.txtValue.Width = 0.8333333F;
			// 
			// txtFee
			// 
			this.txtFee.Height = 0.1770833F;
			this.txtFee.Left = 4.4375F;
			this.txtFee.Name = "txtFee";
			this.txtFee.Style = "text-align: right";
			this.txtFee.Text = null;
			this.txtFee.Top = 0F;
			this.txtFee.Width = 0.9583333F;
			// 
			// txtCategory
			// 
			this.txtCategory.Height = 0.1770833F;
			this.txtCategory.Left = 0.8125F;
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Text = null;
			this.txtCategory.Top = 0F;
			this.txtCategory.Width = 1.895833F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalCount,
            this.txtTotalValue,
            this.txtTotalFee,
            this.Label14,
            this.Line1});
			this.ReportFooter.Height = 0.2395833F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// txtTotalCount
			// 
			this.txtTotalCount.Height = 0.1770833F;
			this.txtTotalCount.Left = 2.791667F;
			this.txtTotalCount.Name = "txtTotalCount";
			this.txtTotalCount.Style = "text-align: right";
			this.txtTotalCount.Text = null;
			this.txtTotalCount.Top = 0.0625F;
			this.txtTotalCount.Width = 0.6458333F;
			// 
			// txtTotalValue
			// 
			this.txtTotalValue.Height = 0.1770833F;
			this.txtTotalValue.Left = 3.53125F;
			this.txtTotalValue.Name = "txtTotalValue";
			this.txtTotalValue.Style = "text-align: right";
			this.txtTotalValue.Text = null;
			this.txtTotalValue.Top = 0.0625F;
			this.txtTotalValue.Width = 0.8333333F;
			// 
			// txtTotalFee
			// 
			this.txtTotalFee.Height = 0.1770833F;
			this.txtTotalFee.Left = 4.4375F;
			this.txtTotalFee.Name = "txtTotalFee";
			this.txtTotalFee.Style = "text-align: right";
			this.txtTotalFee.Text = null;
			this.txtTotalFee.Top = 0.0625F;
			this.txtTotalFee.Width = 0.9583333F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1770833F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 2.0625F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold; text-align: left";
			this.Label14.Text = "Total";
			this.Label14.Top = 0.0625F;
			this.Label14.Width = 0.6145833F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 2.677083F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.02083333F;
			this.Line1.Width = 2.864584F;
			this.Line1.X1 = 2.677083F;
			this.Line1.X2 = 5.541667F;
			this.Line1.Y1 = 0.02083333F;
			this.Line1.Y2 = 0.02083333F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage,
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label6,
            this.Label7,
            this.txtRange});
			this.PageHeader.Height = 0.6770833F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 1.25F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.21875F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.15625F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = null;
			this.txtPage.Top = 0.21875F;
			this.txtPage.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.25F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Permit Application Summary";
			this.Label1.Top = 0.03125F;
			this.Label1.Width = 4.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1770833F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 2.822917F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold; text-align: right";
			this.Label2.Text = "Count";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 0.6145833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1770833F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 3.552083F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold; text-align: right";
			this.Label3.Text = "Value";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 0.8125F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1770833F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.02083333F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold; text-align: left";
			this.Label6.Text = "Permit Type";
			this.Label6.Top = 0.5F;
			this.Label6.Width = 1.9375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1770833F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.583333F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold; text-align: right";
			this.Label7.Text = "Fee";
			this.Label7.Top = 0.5F;
			this.Label7.Width = 0.8125F;
			// 
			// txtRange
			// 
			this.txtRange.Height = 0.2083333F;
			this.txtRange.Left = 1.25F;
			this.txtRange.Name = "txtRange";
			this.txtRange.Text = null;
			this.txtRange.Top = 0.25F;
			this.txtRange.Width = 4.9375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtType});
			this.GroupHeader1.DataField = "permittype";
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// txtType
			// 
			this.txtType.Height = 0.1770833F;
			this.txtType.Left = 0F;
			this.txtType.Name = "txtType";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 2.708333F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroupCount,
            this.txtGroupValue,
            this.txtGroupFee,
            this.Label15});
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtGroupCount
			// 
			this.txtGroupCount.DataField = "PermitCount";
			this.txtGroupCount.Height = 0.1770833F;
			this.txtGroupCount.Left = 2.791667F;
			this.txtGroupCount.Name = "txtGroupCount";
			this.txtGroupCount.OutputFormat = resources.GetString("txtGroupCount.OutputFormat");
			this.txtGroupCount.Style = "text-align: right";
			this.txtGroupCount.SummaryGroup = "GroupHeader1";
			this.txtGroupCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtGroupCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtGroupCount.Text = null;
			this.txtGroupCount.Top = 0F;
			this.txtGroupCount.Width = 0.6458333F;
			// 
			// txtGroupValue
			// 
			this.txtGroupValue.DataField = "PermitValue";
			this.txtGroupValue.Height = 0.1770833F;
			this.txtGroupValue.Left = 3.53125F;
			this.txtGroupValue.Name = "txtGroupValue";
			this.txtGroupValue.OutputFormat = resources.GetString("txtGroupValue.OutputFormat");
			this.txtGroupValue.Style = "text-align: right";
			this.txtGroupValue.SummaryGroup = "GroupHeader1";
			this.txtGroupValue.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtGroupValue.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtGroupValue.Text = null;
			this.txtGroupValue.Top = 0F;
			this.txtGroupValue.Width = 0.8333333F;
			// 
			// txtGroupFee
			// 
			this.txtGroupFee.DataField = "PermitFee";
			this.txtGroupFee.Height = 0.1770833F;
			this.txtGroupFee.Left = 4.4375F;
			this.txtGroupFee.Name = "txtGroupFee";
			this.txtGroupFee.OutputFormat = resources.GetString("txtGroupFee.OutputFormat");
			this.txtGroupFee.Style = "text-align: right";
			this.txtGroupFee.SummaryGroup = "GroupHeader1";
			this.txtGroupFee.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
			this.txtGroupFee.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
			this.txtGroupFee.Text = null;
			this.txtGroupFee.Top = 0F;
			this.txtGroupFee.Width = 0.9583333F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.177F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 2.0625F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold; text-align: left";
			this.Label15.Text = "Subtotal";
			this.Label15.Top = 0F;
			this.Label15.Width = 0.625F;
			// 
			// rptPermitFees
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtGroupFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCategory;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
	}
}
