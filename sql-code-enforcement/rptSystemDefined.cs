﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
using Global;
using System;
using TWSharedLibrary;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptSystemDefined.
	/// </summary>
	public partial class rptSystemDefined : BaseSectionReport
	{
		public rptSystemDefined()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "System Defined Types";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptSystemDefined InstancePtr
		{
			get
			{
				return (rptSystemDefined)Sys.GetInstance(typeof(rptSystemDefined));
			}
		}

		protected rptSystemDefined _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptSystemDefined	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngIndex;
		int lngRow;
		int lngSubRow;
		bool boolAll;
		int lngCurType;
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLCODE = 1;
		const int CNSTGRIDCOLDESC = 2;
		const int CNSTGRIDCOLSHORTDESC = 3;
		const int CNSTGRIDCOLCODETYPE = 4;
		const int CNSTGRIDSUBCOLAUTOID = 0;
		const int CNSTGRIDSUBCOLCODE = 1;
		const int CNSTGRIDSUBCOLDESC = 2;
		const int CNSTGRIDSUBCOLSHORTDESC = 3;
		const int CNSTGRIDSUBCOLCODETYPE = 4;
		const int CNSTGRIDSUBCOLPARENTCODETYPE = 5;
		const int CNSTGRIDSUBCOLPARENTCODE = 6;
		const int CNSTGRIDSUBCOLPARENTID = 7;

		public void Init(int lngType)
		{
			lngSubRow = 0;
			switch (lngType)
			{
				case -1:
					{
						lngIndex = 0;
						boolAll = true;
						// Case CNSTCODETYPEPERMITSUBTYPE
						break;
					}
				default:
					{
						lngIndex = frmSystemDefined.InstancePtr.cmbCodeType.SelectedIndex;
						lngCurType = lngType;
						boolAll = false;
						break;
					}
			}
			//end switch
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "SystemDefined");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckAgain:
			;
			if (lngRow >= frmSystemDefined.InstancePtr.Grid.Rows)
			{
				if (!boolAll)
				{
					eArgs.EOF = true;
					return;
				}
				else
				{
					lngIndex += 1;
					if (lngIndex >= frmSystemDefined.InstancePtr.cmbCodeType.Items.Count)
					{
						eArgs.EOF = true;
						return;
					}
					lngCurType = frmSystemDefined.InstancePtr.cmbCodeType.ItemData(lngIndex);
                    // If lngCurType = CNSTCODETYPEPERMITSUBTYPE Then
                    // lngIndex = lngIndex + 1
                    // GoTo CheckAgain
                    // End If
                    //GroupHeader1.DataField = lngIndex.ToString();
                    this.Fields["grpHeader"].Value = lngIndex.ToString();
                    lngRow = 1;
				}
			}
			if (Conversion.Val(frmSystemDefined.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == frmSystemDefined.InstancePtr.cmbCodeType.ItemData(lngIndex))
			{
				eArgs.EOF = false;
				// If lngCurType = CNSTCODETYPEPERMITTYPE Then
				// GroupHeader2.DataField = lngRow
				// If lngSubRow >= frmSystemDefined.GridSub.Rows Then
				// lngRow = lngRow + 1
				// lngSubRow = 1
				// Else
				// lngSubRow = lngSubRow + 1
				// End If
				// 
				// End If
			}
			else
			{
				lngRow += 1;
				goto CheckAgain;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRow < frmSystemDefined.InstancePtr.Grid.Rows)
			{
				// If lngCurType = CNSTCODETYPEPERMITTYPE Then
				// txtDesc.Text = frmSystemDefined.GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLDESC)
				// txtShort.Text = frmSystemDefined.GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLSHORTDESC)
				// lngSubRow = lngSubRow + 1
				// If lngSubRow >= frmSystemDefined.GridSub.Rows Then
				// lngRow = lngRow + 1
				// lngSubRow = 0
				// Else
				// lngSubRow = lngSubRow + 1
				// End If
				// Else
				txtDesc.Text = frmSystemDefined.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC);
				txtShort.Text = frmSystemDefined.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTDESC);
				lngRow += 1;
				// End If
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtGroup.Text = frmSystemDefined.InstancePtr.cmbCodeType.Items[lngIndex].ToString();
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			txtPermitDesc.Text = frmSystemDefined.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC);
			txtPermitShort.Text = frmSystemDefined.InstancePtr.Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTDESC);
		}

		

        private void rptSystemDefined_DataInitialize(object sender, EventArgs e)
        {
            this.Fields.Add("grpHeader");
        }
    }
}
