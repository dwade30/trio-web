//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmSearchAccounts : BaseForm
	{
		public frmSearchAccounts()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSearchAccounts InstancePtr
		{
			get
			{
				return (frmSearchAccounts)Sys.GetInstance(typeof(frmSearchAccounts));
			}
		}

		protected frmSearchAccounts _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLACCOUNT = 0;
		const int CNSTGRIDCOLMAPLOT = 2;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLLOCNUM = 3;
		const int CNSTGRIDCOLLOCSTREET = 4;
		clsDRWrapper clsLoad = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		int lngReturnAccount;
		bool boolAddBlank;
		bool boolFromRE;
		int lngCEAccount;

		public int Init(string strCaption = "", bool boolUseRE = false, bool boolBlankAccount = false, int CEAccount = 0)
		{
			try
			{
				int Init = 0;
				string strSQL = "";
				string strMasterJoin;
				strMasterJoin = modCE.GetCEMasterJoin();
				Init = 0;
				lngReturnAccount = 0;
				boolAddBlank = boolBlankAccount;
				boolFromRE = boolUseRE;
				lngCEAccount = CEAccount;
				if (!boolUseRE)
				{
					// strSQL = "Select ceaccount,maplot,name,streetnumber,streetname from cemaster where not deleted = 1 and ceaccount <> " & lngCEAccount
					strSQL = strMasterJoin + " where not isnull(deleted,0) = 1 and ceaccount <> " + FCConvert.ToString(lngCEAccount);
					strSQL += " order by ceaccount";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				}
				else
				{
					lngReturnAccount = -1;
					strSQL = "select rsaccount from cemaster where not isnull(deleted,0) = 1 and ceaccount <> " + FCConvert.ToString(lngCEAccount) + " order by ceaccount ";
					// strSQL = "select * from remaster left join cemaster on (cemaster.rsaccount = remaster.rsaccount) where not rsdeleted and (isnull (ceaccount)  or ceaccount = " & CEAccount & ")order by remaster.rsaccount "
					rsRE.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
					strSQL = "select * from master where not rsdeleted = 1 and rscard = 1 order by rsaccount";
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
				}
				if (clsLoad.EndOfFile())
				{
					MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return Init;
				}
				this.Text = strCaption;
				this.Show(FCForm.FormShowEnum.Modal);
				Init = lngReturnAccount;
				return Init;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return 0;
			}
		}

		private void frmSearchAccounts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSearchAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSearchAccounts properties;
			//frmSearchAccounts.FillStyle	= 0;
			//frmSearchAccounts.ScaleWidth	= 9300;
			//frmSearchAccounts.ScaleHeight	= 7800;
			//frmSearchAccounts.LinkTopic	= "Form2";
			//frmSearchAccounts.LockControls	= -1  'True;
			//frmSearchAccounts.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			int lngRow;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			if (boolAddBlank)
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(0));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, "No Account");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCNUM, "");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCSTREET, "");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, "");
			}
			while (!clsLoad.EndOfFile())
			{
				if (!boolFromRE)
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ceaccount"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("name")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCNUM, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("streetnumber"))));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCSTREET, FCConvert.ToString(clsLoad.Get_Fields_String("streetname")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(clsLoad.Get_Fields_String("maplot")));
				}
				else
				{
					if (!rsRE.FindFirstRecord("rsaccount", Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount"))))
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("rsaccount"))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("rsname")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCNUM, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("rslocnumalph"))));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLLOCSTREET, FCConvert.ToString(clsLoad.Get_Fields_String("rslocstreet")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLMAPLOT, FCConvert.ToString(clsLoad.Get_Fields_String("rsmaplot")));
					}
				}
				clsLoad.MoveNext();
			}
		}

		private void frmSearchAccounts_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow > 0)
			{
				lngReturnAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.MouseRow, CNSTGRIDCOLACCOUNT))));
				Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.TextMatrix(0, CNSTGRIDCOLACCOUNT, "Acct");
			Grid.TextMatrix(0, CNSTGRIDCOLMAPLOT, "Map Lot");
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLLOCNUM, "St Num");
			Grid.TextMatrix(0, CNSTGRIDCOLLOCSTREET, "Street Name");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLACCOUNT, FCConvert.ToInt32(0.07 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(0.28 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLMAPLOT, FCConvert.ToInt32(0.15 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLLOCNUM, FCConvert.ToInt32(0.10 * GridWidth));
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				lngReturnAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLACCOUNT))));
				Close();
				return;
			}
			else
			{
				MessageBox.Show("No records were selected", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}
	}
}
