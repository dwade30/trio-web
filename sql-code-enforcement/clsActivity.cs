//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWCE0000
{
	public class clsActivity
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strDetail = string.Empty;
		private string strType = string.Empty;
		private int lngParentID;
		private int lngAccount;
		private int lngParentType;
		// vbPorter upgrade warning: dtActivityDate As DateTime	OnWrite(DateTime, int)
		private DateTime dtActivityDate;
		private int lngReferenceID;
		private int lngRefType;
		private int lngAutoID;

		public int ID
		{
			set
			{
				lngAutoID = value;
			}
			get
			{
				int ID = 0;
				ID = lngAutoID;
				return ID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Detail
		{
			set
			{
				strDetail = value;
			}
			get
			{
				string Detail = "";
				Detail = strDetail;
				return Detail;
			}
		}

		public string ActivityType
		{
			set
			{
				strType = value;
			}
			get
			{
				string ActivityType = "";
				ActivityType = strType;
				return ActivityType;
			}
		}

		public int ParentID
		{
			set
			{
				lngParentID = value;
			}
			get
			{
				int ParentID = 0;
				ParentID = lngParentID;
				return ParentID;
			}
		}

		public int Account
		{
			set
			{
				lngAccount = value;
			}
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}

		public int ParentType
		{
			set
			{
				lngParentType = value;
			}
			get
			{
				int ParentType = 0;
				ParentType = lngParentType;
				return ParentType;
			}
		}

		public DateTime ActivityDate
		{
			set
			{
				dtActivityDate = value;
			}
			get
			{
				DateTime ActivityDate = System.DateTime.Now;
				ActivityDate = dtActivityDate;
				return ActivityDate;
			}
		}

		public int ReferenceID
		{
			set
			{
				lngReferenceID = value;
			}
			get
			{
				int ReferenceID = 0;
				ReferenceID = lngReferenceID;
				return ReferenceID;
			}
		}

		public int ReferenceType
		{
			set
			{
				lngRefType = value;
			}
			get
			{
				int ReferenceType = 0;
				ReferenceType = lngRefType;
				return ReferenceType;
			}
		}

		public int LoadActivity(int lngID)
		{
			int LoadActivity = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			LoadActivity = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				rsLoad.OpenRecordset("select * from activitylog where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					strDescription = rsLoad.Get_Fields_String("description");
					strDetail = rsLoad.Get_Fields_String("detail");
					strType = rsLoad.Get_Fields_String("activitytype");
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("account"))));
					lngParentID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("parentid"))));
					lngParentType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("parenttype"))));
					lngReferenceID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("referenceid"))));
					lngRefType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("referencetype"))));
					if (Information.IsDate(rsLoad.Get_Fields("activitydate")))
					{
						dtActivityDate = (DateTime)rsLoad.Get_Fields_DateTime("activitydate");
					}
					else
					{
						dtActivityDate = DateTime.FromOADate(0);
					}
				}
				else
				{
					Clear();
				}
				LoadActivity = 0;
				return LoadActivity;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				LoadActivity = fecherFoundation.Information.Err(ex).Number;
			}
			return LoadActivity;
		}

		public int DeleteActivity()
		{
			int DeleteActivity = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				DeleteActivity = 0;
				if (lngAutoID > 0)
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.Execute("delete from activitylog where ID = " + FCConvert.ToString(lngAutoID), modGlobalVariables.Statics.strCEDatabase);
				}
				Clear();
				return DeleteActivity;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				DeleteActivity = fecherFoundation.Information.Err(ex).Number;
			}
			return DeleteActivity;
		}

		public int SaveActivity()
		{
			int SaveActivity = 0;
			SaveActivity = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from activitylog where ID = " + FCConvert.ToString(lngAutoID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				rsSave.Set_Fields("account", lngAccount);
				rsSave.Set_Fields("parentid", lngParentID);
				rsSave.Set_Fields("parenttype", lngParentType);
				rsSave.Set_Fields("activitydate", dtActivityDate);
				rsSave.Set_Fields("activitytype", strType);
				rsSave.Set_Fields("description", strDescription);
				rsSave.Set_Fields("detail", strDetail);
				rsSave.Set_Fields("referenceid", lngReferenceID);
				rsSave.Set_Fields("referencetype", lngRefType);
				rsSave.Update();
				lngAutoID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
				return SaveActivity;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SaveActivity = fecherFoundation.Information.Err(ex).Number;
			}
			return SaveActivity;
		}

		public void Clear()
		{
			lngAccount = 0;
			lngAutoID = 0;
			lngParentID = 0;
			lngParentType = 0;
			lngReferenceID = 0;
			lngRefType = 0;
			strType = "";
			strDescription = "";
			strDetail = "";
			dtActivityDate = DateTime.FromOADate(0);
		}
	}
}
