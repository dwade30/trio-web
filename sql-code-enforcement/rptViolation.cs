﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptViolation.
	/// </summary>
	public partial class rptViolation : BaseSectionReport
	{
		public rptViolation()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Violation";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptViolation InstancePtr
		{
			get
			{
				return (rptViolation)Sys.GetInstance(typeof(rptViolation));
			}
		}

		protected rptViolation _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptViolation	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsViolation vViolation = new clsViolation();

		public void Init(int lngViolationAutoID, clsViolation tViol = null)
		{
			int lngReturn = 0;
			if (tViol == null)
			{
				lngReturn = vViolation.LoadViolation(lngViolationAutoID);
				if (lngReturn != 0)
				{
					return;
				}
			}
			else
			{
				vViolation = tViol;
			}
			//FC:FINAL:IPI - #i1705 - show the report viewer on web
			//this.Show(FCForm.FormShowEnum.Modal);
			frmReportViewer.InstancePtr.Init(this, showModal: true);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtPageDate.Text = txtDate.Text;
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtPageTime.Text = txtTime.Text;
			txtPageMuni.Text = txtMuni.Text;
			txtIdentifier.Text = vViolation.Identifier;
			if (Information.IsDate(vViolation.DateIssued))
			{
				if (vViolation.DateIssued.ToOADate() != 0)
				{
					txtDateIssued.Text = Strings.Format(vViolation.DateIssued, "MM/dd/yyyy");
				}
				else
				{
					txtDateIssued.Text = "'";
				}
			}
			else
			{
				txtDateIssued.Text = "";
			}
			if (Information.IsDate(vViolation.DateOccurred))
			{
				if (vViolation.DateOccurred.ToOADate() != 0)
				{
					txtDateOccurred.Text = Strings.Format(vViolation.DateOccurred, "MM/dd/yyyy");
				}
				else
				{
					txtDateOccurred.Text = "";
				}
			}
			else
			{
				txtDateOccurred.Text = "";
			}
			if (Information.IsDate(vViolation.DateResolved))
			{
				if (vViolation.DateResolved.ToOADate() != 0)
				{
					txtDateClosed.Text = Strings.Format(vViolation.DateResolved, "MM/dd/yyyy");
				}
				else
				{
					txtDateClosed.Text = "";
				}
			}
			else
			{
				txtDateClosed.Text = "";
			}
			txtFee.Text = Strings.Format(vViolation.Fee, "#,###,###,##0.00");
			txtDescription.Text = vViolation.Description;
			txtDoingBusinessAs.Text = vViolation.DBA;
			txtBusinessLicense.Text = vViolation.BusinessLicense;
			if (vViolation.StreetNumber > 0)
			{
				txtLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(vViolation.StreetNumber) + " " + vViolation.Apt) + " " + vViolation.Street;
			}
			else
			{
				txtLocation.Text = vViolation.Street;
			}
			txtDetail.Text = vViolation.Detail;
			txtStatus.Text = vViolation.Get_StatusLabel(vViolation.Status);
			txtType.Text = vViolation.Get_TypeLabel(vViolation.ViolationType);
			string strTemp;
			strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(vViolation.FirstName + " " + vViolation.MiddleName) + " " + vViolation.LastName + " " + vViolation.Desig);
			txtName.Text = strTemp;
			strTemp = "";
			if (fecherFoundation.Strings.Trim(vViolation.Address1) != "")
			{
				if (fecherFoundation.Strings.Trim(vViolation.Address2) != "")
				{
					strTemp = vViolation.Address1 + "\r\n" + vViolation.Address2;
				}
				else
				{
					strTemp = vViolation.Address1;
				}
			}
			else
			{
				if (fecherFoundation.Strings.Trim(vViolation.Address2) != "")
				{
					strTemp = vViolation.Address2;
				}
			}
			if (fecherFoundation.Strings.Trim(vViolation.City) != "" || fecherFoundation.Strings.Trim(vViolation.State) != "")
			{
				if (strTemp != "")
				{
					strTemp += "\r\n" + fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(vViolation.City) + " " + fecherFoundation.Strings.Trim(vViolation.State)) + "  " + vViolation.Zip + " " + vViolation.Zip4;
				}
			}
			txtAddress.Text = strTemp;
			txtContact.Text = vViolation.ContactName;
			txtEmail.Text = vViolation.ContactEmail;
			clsPhoneList tPhone;
			tPhone = vViolation.PhoneNumberList;
			clsPhoneNumber tNum;
			strTemp = "";
			if (!(tPhone == null))
			{
				if (!(tPhone.GetCount < 1))
				{
					tPhone.MoveFirst();
					tPhone.MovePrevious();
					while (tPhone.MoveNext() > 0)
					{
						tNum = tPhone.GetCurrentPhoneNumber;
						if (!(tNum == null))
						{
							if (strTemp != "")
							{
								strTemp += "\r\n" + tNum.FormattedPhoneNumber + "  " + tNum.Description;
							}
							else
							{
								strTemp = tNum.FormattedPhoneNumber + "  " + tNum.Description;
							}
						}
					}
				}
			}
			txtContactPhone.Text = strTemp;
			SubReport1.Report = new srptViolation();
			SubReport1.Report.UserData = vViolation.ID;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			if (PageNumber > 1)
			{
				this.PageHeader.Visible = true;
				txtPagePage.Text = "Page " + this.PageNumber;
			}
		}

		
	}
}
