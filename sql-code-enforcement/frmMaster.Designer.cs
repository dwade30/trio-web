//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmMaster.
	/// </summary>
	partial class frmMaster
	{
		public fecherFoundation.FCComboBox cmbPermits;
		public fecherFoundation.FCLabel lblPermits;
		public fecherFoundation.FCComboBox cmbViolations;
		public fecherFoundation.FCLabel lblViolations;
		public fecherFoundation.FCTabControl SSTab1;
		public fecherFoundation.FCTabPage SSTab1_Page1;
		public FCGrid GridOpens;
		public fecherFoundation.FCTabPage SSTab1_Page2;
		public FCGrid GridPermits;
		public fecherFoundation.FCTabPage SSTab1_Page3;
		public FCGrid gridViolations;
		public fecherFoundation.FCTabPage SSTab1_Page4;
		public FCGrid gridActivity;
		public fecherFoundation.FCFrame framREInfo;
		public fecherFoundation.FCButton cmdSearchOwner;
		public fecherFoundation.FCButton cmdEditOwner;
		public fecherFoundation.FCTextBox txtOwnerID;
		public fecherFoundation.FCButton cmdSearchSecOwner;
		public fecherFoundation.FCButton cmdEditSecOwner;
		public fecherFoundation.FCTextBox txt2ndOwnerID;
		public fecherFoundation.FCButton cmdRemoveSecOwner;
		public fecherFoundation.FCButton cmdRemoveOwner;
		public FCGrid gridBPDelete;
		public fecherFoundation.FCTextBox txtZone;
		public fecherFoundation.FCTextBox txtNeighborhood;
		public fecherFoundation.FCTextBox txtMapLot;
		public fecherFoundation.FCTextBox txtStreet;
		public fecherFoundation.FCTextBox txtApt;
		public fecherFoundation.FCTextBox txtStreetNum;
		public fecherFoundation.FCTextBox txtFrontage;
		public fecherFoundation.FCTextBox txtAcres;
		public fecherFoundation.FCLabel lbl2ndOwner;
		public fecherFoundation.FCLabel lblOwner1;
		public fecherFoundation.FCLabel lblAddress;
		public fecherFoundation.FCLabel lblRE;
		public fecherFoundation.FCLabel lblREAccount;
		public fecherFoundation.FCLabel Label_2;
		public fecherFoundation.FCLabel Label_1;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCLabel Label_0;
		public fecherFoundation.FCLabel Label_12;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		public FCGrid GridAbutters;
		public FCGrid GridPreviousMaps;
		public FCGrid GridDeletedPhones;
		public fecherFoundation.FCGrid BPGrid;
		public fecherFoundation.FCLabel lblViewOnly;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddActivity;
		public fecherFoundation.FCToolStripMenuItem mnuAddPermit;
		public fecherFoundation.FCToolStripMenuItem mnuAddViolation;
		public fecherFoundation.FCToolStripMenuItem mnuSepar5;
		public fecherFoundation.FCToolStripMenuItem mnuCreateDocument;
		public fecherFoundation.FCToolStripMenuItem mnuPrintAbutters;
		public fecherFoundation.FCToolStripMenuItem mnuPrintActivity;
		public fecherFoundation.FCToolStripMenuItem mnuPrintActivityWithDetail;
		public fecherFoundation.FCToolStripMenuItem mnuPrintHistory;
		public fecherFoundation.FCToolStripMenuItem mnuPrintViolations;
		public fecherFoundation.FCToolStripMenuItem mnuPrintEnvelope;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuChangeREAssoc;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteAccount;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMaster));
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle9 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle10 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle13 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle14 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle15 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle16 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle17 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle18 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbPermits = new fecherFoundation.FCComboBox();
            this.lblPermits = new fecherFoundation.FCLabel();
            this.cmbViolations = new fecherFoundation.FCComboBox();
            this.lblViolations = new fecherFoundation.FCLabel();
            this.SSTab1 = new fecherFoundation.FCTabControl();
            this.SSTab1_Page1 = new fecherFoundation.FCTabPage();
            this.GridOpens = new fecherFoundation.FCGrid();
            this.SSTab1_Page2 = new fecherFoundation.FCTabPage();
            this.GridPermits = new fecherFoundation.FCGrid();
            this.SSTab1_Page3 = new fecherFoundation.FCTabPage();
            this.gridViolations = new fecherFoundation.FCGrid();
            this.SSTab1_Page4 = new fecherFoundation.FCTabPage();
            this.gridActivity = new fecherFoundation.FCGrid();
            this.framREInfo = new fecherFoundation.FCFrame();
            this.cmdSearchOwner = new fecherFoundation.FCButton();
            this.cmdEditOwner = new fecherFoundation.FCButton();
            this.txtOwnerID = new fecherFoundation.FCTextBox();
            this.cmdSearchSecOwner = new fecherFoundation.FCButton();
            this.cmdEditSecOwner = new fecherFoundation.FCButton();
            this.txt2ndOwnerID = new fecherFoundation.FCTextBox();
            this.cmdRemoveSecOwner = new fecherFoundation.FCButton();
            this.cmdRemoveOwner = new fecherFoundation.FCButton();
            this.gridBPDelete = new fecherFoundation.FCGrid();
            this.txtZone = new fecherFoundation.FCTextBox();
            this.txtNeighborhood = new fecherFoundation.FCTextBox();
            this.txtMapLot = new fecherFoundation.FCTextBox();
            this.txtStreet = new fecherFoundation.FCTextBox();
            this.txtApt = new fecherFoundation.FCTextBox();
            this.txtStreetNum = new fecherFoundation.FCTextBox();
            this.txtFrontage = new fecherFoundation.FCTextBox();
            this.txtAcres = new fecherFoundation.FCTextBox();
            this.lbl2ndOwner = new fecherFoundation.FCLabel();
            this.lblOwner1 = new fecherFoundation.FCLabel();
            this.lblAddress = new fecherFoundation.FCLabel();
            this.lblRE = new fecherFoundation.FCLabel();
            this.lblREAccount = new fecherFoundation.FCLabel();
            this.Label_2 = new fecherFoundation.FCLabel();
            this.Label_1 = new fecherFoundation.FCLabel();
            this.ImgComment = new fecherFoundation.FCPictureBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.Label_0 = new fecherFoundation.FCLabel();
            this.Label_12 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.GridAbutters = new fecherFoundation.FCGrid();
            this.GridPreviousMaps = new fecherFoundation.FCGrid();
            this.GridDeletedPhones = new fecherFoundation.FCGrid();
            this.BPGrid = new fecherFoundation.FCGrid();
            this.lblViewOnly = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuPrintAbutters = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintActivity = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintActivityWithDetail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintHistory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintViolations = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintEnvelope = new fecherFoundation.FCToolStripMenuItem();
            this.mnuChangeREAssoc = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddActivity = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddPermit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddViolation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCreateDocument = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddActivity = new fecherFoundation.FCButton();
            this.cmdAddPermit = new fecherFoundation.FCButton();
            this.cmdAddViolation = new fecherFoundation.FCButton();
            this.cmdCreateDocument = new fecherFoundation.FCButton();
            this.cmdChangeREAssociation = new fecherFoundation.FCButton();
            this.cmdComment = new fecherFoundation.FCButton();
            this.cmdDeleteAccount = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SSTab1.SuspendLayout();
            this.SSTab1_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).BeginInit();
            this.SSTab1_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPermits)).BeginInit();
            this.SSTab1_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViolations)).BeginInit();
            this.SSTab1_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framREInfo)).BeginInit();
            this.framREInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBPDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAbutters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPreviousMaps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddActivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPermit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddViolation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeREAssociation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(1068, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.SSTab1);
            this.ClientArea.Controls.Add(this.framREInfo);
            this.ClientArea.Controls.Add(this.GridAbutters);
            this.ClientArea.Controls.Add(this.GridPreviousMaps);
            this.ClientArea.Controls.Add(this.GridDeletedPhones);
            this.ClientArea.Controls.Add(this.BPGrid);
            this.ClientArea.Controls.Add(this.lblViewOnly);
            this.ClientArea.Size = new System.Drawing.Size(1068, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddActivity);
            this.TopPanel.Controls.Add(this.cmdAddPermit);
            this.TopPanel.Controls.Add(this.cmdAddViolation);
            this.TopPanel.Controls.Add(this.cmdCreateDocument);
            this.TopPanel.Controls.Add(this.cmdChangeREAssociation);
            this.TopPanel.Controls.Add(this.cmdComment);
            this.TopPanel.Controls.Add(this.cmdDeleteAccount);
            this.TopPanel.Size = new System.Drawing.Size(1068, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdChangeREAssociation, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdCreateDocument, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddViolation, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddPermit, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddActivity, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(105, 30);
            this.HeaderText.Text = "Property";
            // 
            // cmbPermits
            // 
            this.cmbPermits.AutoSize = false;
            this.cmbPermits.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPermits.FormattingEnabled = true;
            this.cmbPermits.Items.AddRange(new object[] {
            "Show Current",
            "Show All"});
            this.cmbPermits.Location = new System.Drawing.Point(103, 20);
            this.cmbPermits.Name = "cmbPermits";
            this.cmbPermits.Size = new System.Drawing.Size(183, 40);
            this.cmbPermits.TabIndex = 12;
            this.cmbPermits.Text = "Show Current";
            this.cmbPermits.SelectedIndexChanged += new System.EventHandler(this.optPermits_CheckedChanged);
            // 
            // lblPermits
            // 
            this.lblPermits.AutoSize = true;
            this.lblPermits.Location = new System.Drawing.Point(20, 34);
            this.lblPermits.Name = "lblPermits";
            this.lblPermits.Size = new System.Drawing.Size(46, 15);
            this.lblPermits.TabIndex = 13;
            this.lblPermits.Text = "SHOW";
            // 
            // cmbViolations
            // 
            this.cmbViolations.AutoSize = false;
            this.cmbViolations.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbViolations.FormattingEnabled = true;
            this.cmbViolations.Items.AddRange(new object[] {
            "Show Current",
            "Show All"});
            this.cmbViolations.Location = new System.Drawing.Point(103, 20);
            this.cmbViolations.Name = "cmbViolations";
            this.cmbViolations.Size = new System.Drawing.Size(183, 40);
            this.cmbViolations.TabIndex = 34;
            this.cmbViolations.Text = "Show Current";
            this.cmbViolations.SelectedIndexChanged += new System.EventHandler(this.optViolations_CheckedChanged);
            // 
            // lblViolations
            // 
            this.lblViolations.AutoSize = true;
            this.lblViolations.Location = new System.Drawing.Point(20, 34);
            this.lblViolations.Name = "lblViolations";
            this.lblViolations.Size = new System.Drawing.Size(46, 15);
            this.lblViolations.TabIndex = 35;
            this.lblViolations.Text = "SHOW";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.SSTab1_Page1);
            this.SSTab1.Controls.Add(this.SSTab1_Page2);
            this.SSTab1.Controls.Add(this.SSTab1_Page3);
            this.SSTab1.Controls.Add(this.SSTab1_Page4);
            this.SSTab1.Location = new System.Drawing.Point(30, 440);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.PageInsets = new Wisej.Web.Padding(1, 50, 1, 1);
            this.SSTab1.ShowFocusRect = false;
            this.SSTab1.Size = new System.Drawing.Size(634, 327);
            this.SSTab1.TabIndex = 26;
            this.SSTab1.TabsPerRow = 0;
            this.SSTab1.Text = "User Defined";
            this.SSTab1.WordWrap = false;
            // 
            // SSTab1_Page1
            // 
            this.SSTab1_Page1.Controls.Add(this.GridOpens);
            this.SSTab1_Page1.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page1.Name = "SSTab1_Page1";
            this.SSTab1_Page1.Text = "User Defined";
            // 
            // GridOpens
            // 
            this.GridOpens.AllowSelection = false;
            this.GridOpens.AllowUserToResizeColumns = false;
            this.GridOpens.AllowUserToResizeRows = false;
            //this.GridOpens.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridOpens.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridOpens.BackColorBkg = System.Drawing.Color.Empty;
            this.GridOpens.BackColorFixed = System.Drawing.Color.Empty;
            this.GridOpens.BackColorSel = System.Drawing.Color.Empty;
            this.GridOpens.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridOpens.Cols = 6;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridOpens.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridOpens.ColumnHeadersHeight = 30;
            this.GridOpens.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridOpens.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridOpens.DragIcon = null;
            this.GridOpens.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridOpens.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridOpens.ExtendLastCol = true;
            this.GridOpens.FixedCols = 2;
            this.GridOpens.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridOpens.FrozenCols = 0;
            this.GridOpens.GridColor = System.Drawing.Color.Empty;
            this.GridOpens.GridColorFixed = System.Drawing.Color.Empty;
            this.GridOpens.Location = new System.Drawing.Point(20, 20);
            this.GridOpens.Name = "GridOpens";
            this.GridOpens.OutlineCol = 0;
            this.GridOpens.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridOpens.RowHeightMin = 0;
            this.GridOpens.Rows = 50;
            this.GridOpens.ScrollTipText = null;
            this.GridOpens.ShowColumnVisibilityMenu = false;
            this.GridOpens.ShowFocusCell = false;
            this.GridOpens.Size = new System.Drawing.Size(593, 240);
            this.GridOpens.StandardTab = true;
            this.GridOpens.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridOpens.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridOpens.TabIndex = 10;
            this.GridOpens.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridOpens_ValidateEdit);
            this.GridOpens.CurrentCellChanged += new System.EventHandler(this.GridOpens_RowColChange);
            // 
            // SSTab1_Page2
            // 
            this.SSTab1_Page2.Controls.Add(this.GridPermits);
            this.SSTab1_Page2.Controls.Add(this.cmbPermits);
            this.SSTab1_Page2.Controls.Add(this.lblPermits);
            this.SSTab1_Page2.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page2.Name = "SSTab1_Page2";
            this.SSTab1_Page2.Text = "Permits";
            // 
            // GridPermits
            // 
            this.GridPermits.AllowSelection = false;
            this.GridPermits.AllowUserToResizeColumns = false;
            this.GridPermits.AllowUserToResizeRows = false;
            this.GridPermits.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridPermits.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridPermits.BackColorBkg = System.Drawing.Color.Empty;
            this.GridPermits.BackColorFixed = System.Drawing.Color.Empty;
            this.GridPermits.BackColorSel = System.Drawing.Color.Empty;
            this.GridPermits.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridPermits.Cols = 11;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridPermits.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.GridPermits.ColumnHeadersHeight = 30;
            this.GridPermits.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridPermits.DefaultCellStyle = dataGridViewCellStyle4;
            this.GridPermits.DragIcon = null;
            this.GridPermits.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridPermits.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridPermits.ExtendLastCol = true;
            this.GridPermits.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridPermits.FrozenCols = 0;
            this.GridPermits.GridColor = System.Drawing.Color.Empty;
            this.GridPermits.GridColorFixed = System.Drawing.Color.Empty;
            this.GridPermits.Location = new System.Drawing.Point(20, 78);
            this.GridPermits.Name = "GridPermits";
            this.GridPermits.OutlineCol = 0;
            this.GridPermits.ReadOnly = true;
            this.GridPermits.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridPermits.RowHeightMin = 0;
            this.GridPermits.Rows = 50;
            this.GridPermits.ScrollTipText = null;
            this.GridPermits.ShowColumnVisibilityMenu = false;
            this.GridPermits.ShowFocusCell = false;
            this.GridPermits.Size = new System.Drawing.Size(593, 177);
            this.GridPermits.StandardTab = true;
            this.GridPermits.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridPermits.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridPermits.TabIndex = 11;
            this.GridPermits.DoubleClick += new System.EventHandler(this.GridPermits_DblClick);
            // 
            // SSTab1_Page3
            // 
            this.SSTab1_Page3.Controls.Add(this.gridViolations);
            this.SSTab1_Page3.Controls.Add(this.cmbViolations);
            this.SSTab1_Page3.Controls.Add(this.lblViolations);
            this.SSTab1_Page3.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page3.Name = "SSTab1_Page3";
            this.SSTab1_Page3.Text = "Violations";
            // 
            // gridViolations
            // 
            this.gridViolations.AllowSelection = false;
            this.gridViolations.AllowUserToResizeColumns = false;
            this.gridViolations.AllowUserToResizeRows = false;
            this.gridViolations.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridViolations.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridViolations.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridViolations.BackColorBkg = System.Drawing.Color.Empty;
            this.gridViolations.BackColorFixed = System.Drawing.Color.Empty;
            this.gridViolations.BackColorSel = System.Drawing.Color.Empty;
            this.gridViolations.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridViolations.Cols = 5;
            dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridViolations.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.gridViolations.ColumnHeadersHeight = 30;
            this.gridViolations.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridViolations.DefaultCellStyle = dataGridViewCellStyle6;
            this.gridViolations.DragIcon = null;
            this.gridViolations.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridViolations.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.gridViolations.ExtendLastCol = true;
            this.gridViolations.FixedCols = 0;
            this.gridViolations.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridViolations.FrozenCols = 0;
            this.gridViolations.GridColor = System.Drawing.Color.Empty;
            this.gridViolations.GridColorFixed = System.Drawing.Color.Empty;
            this.gridViolations.Location = new System.Drawing.Point(20, 78);
            this.gridViolations.Name = "gridViolations";
            this.gridViolations.OutlineCol = 0;
            this.gridViolations.ReadOnly = true;
            this.gridViolations.RowHeadersVisible = false;
            this.gridViolations.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridViolations.RowHeightMin = 0;
            this.gridViolations.Rows = 1;
            this.gridViolations.ScrollTipText = null;
            this.gridViolations.ShowColumnVisibilityMenu = false;
            this.gridViolations.ShowFocusCell = false;
            this.gridViolations.Size = new System.Drawing.Size(593, 177);
            this.gridViolations.StandardTab = true;
            this.gridViolations.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridViolations.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridViolations.TabIndex = 33;
            this.gridViolations.DoubleClick += new System.EventHandler(this.gridViolations_DblClick);
            // 
            // SSTab1_Page4
            // 
            this.SSTab1_Page4.Controls.Add(this.gridActivity);
            this.SSTab1_Page4.Location = new System.Drawing.Point(1, 50);
            this.SSTab1_Page4.Name = "SSTab1_Page4";
            this.SSTab1_Page4.Text = "Activity";
            // 
            // gridActivity
            // 
            this.gridActivity.AllowSelection = false;
            this.gridActivity.AllowUserToResizeColumns = false;
            this.gridActivity.AllowUserToResizeRows = false;
            this.gridActivity.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.gridActivity.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridActivity.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridActivity.BackColorBkg = System.Drawing.Color.Empty;
            this.gridActivity.BackColorFixed = System.Drawing.Color.Empty;
            this.gridActivity.BackColorSel = System.Drawing.Color.Empty;
            this.gridActivity.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridActivity.Cols = 4;
            dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridActivity.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.gridActivity.ColumnHeadersHeight = 30;
            this.gridActivity.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridActivity.DefaultCellStyle = dataGridViewCellStyle8;
            this.gridActivity.DragIcon = null;
            this.gridActivity.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridActivity.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.gridActivity.ExtendLastCol = true;
            this.gridActivity.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridActivity.FrozenCols = 0;
            this.gridActivity.GridColor = System.Drawing.Color.Empty;
            this.gridActivity.GridColorFixed = System.Drawing.Color.Empty;
            this.gridActivity.Location = new System.Drawing.Point(20, 20);
            this.gridActivity.Name = "gridActivity";
            this.gridActivity.OutlineCol = 0;
            this.gridActivity.ReadOnly = true;
            this.gridActivity.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridActivity.RowHeightMin = 0;
            this.gridActivity.Rows = 1;
            this.gridActivity.ScrollTipText = null;
            this.gridActivity.ShowColumnVisibilityMenu = false;
            this.gridActivity.ShowFocusCell = false;
            this.gridActivity.Size = new System.Drawing.Size(593, 240);
            this.gridActivity.StandardTab = true;
            this.gridActivity.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridActivity.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridActivity.TabIndex = 40;
            this.gridActivity.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.gridActivity_MouseDownEvent);
            this.gridActivity.KeyDown += new Wisej.Web.KeyEventHandler(this.gridActivity_KeyDownEvent);
            this.gridActivity.DoubleClick += new System.EventHandler(this.gridActivity_DblClick);
            // 
            // framREInfo
            // 
            this.framREInfo.Controls.Add(this.cmdSearchOwner);
            this.framREInfo.Controls.Add(this.cmdEditOwner);
            this.framREInfo.Controls.Add(this.txtOwnerID);
            this.framREInfo.Controls.Add(this.cmdSearchSecOwner);
            this.framREInfo.Controls.Add(this.cmdEditSecOwner);
            this.framREInfo.Controls.Add(this.txt2ndOwnerID);
            this.framREInfo.Controls.Add(this.cmdRemoveSecOwner);
            this.framREInfo.Controls.Add(this.cmdRemoveOwner);
            this.framREInfo.Controls.Add(this.gridBPDelete);
            this.framREInfo.Controls.Add(this.txtZone);
            this.framREInfo.Controls.Add(this.txtNeighborhood);
            this.framREInfo.Controls.Add(this.txtMapLot);
            this.framREInfo.Controls.Add(this.txtStreet);
            this.framREInfo.Controls.Add(this.txtApt);
            this.framREInfo.Controls.Add(this.txtStreetNum);
            this.framREInfo.Controls.Add(this.txtFrontage);
            this.framREInfo.Controls.Add(this.txtAcres);
            this.framREInfo.Controls.Add(this.lbl2ndOwner);
            this.framREInfo.Controls.Add(this.lblOwner1);
            this.framREInfo.Controls.Add(this.lblAddress);
            this.framREInfo.Controls.Add(this.lblRE);
            this.framREInfo.Controls.Add(this.lblREAccount);
            this.framREInfo.Controls.Add(this.Label_2);
            this.framREInfo.Controls.Add(this.Label_1);
            this.framREInfo.Controls.Add(this.ImgComment);
            this.framREInfo.Controls.Add(this.Label3);
            this.framREInfo.Controls.Add(this.lblAccount);
            this.framREInfo.Controls.Add(this.Label2);
            this.framREInfo.Controls.Add(this.Label1_12);
            this.framREInfo.Controls.Add(this.Label_0);
            this.framREInfo.Controls.Add(this.Label_12);
            this.framREInfo.Controls.Add(this.Label1_2);
            this.framREInfo.Controls.Add(this.Label1_1);
            this.framREInfo.Controls.Add(this.Label1_0);
            this.framREInfo.Location = new System.Drawing.Point(30, 30);
            this.framREInfo.Name = "framREInfo";
            this.framREInfo.Size = new System.Drawing.Size(987, 400);
            this.framREInfo.TabIndex = 15;
            // 
            // cmdSearchOwner
            // 
            this.cmdSearchOwner.AppearanceKey = "actionButton";
            this.cmdSearchOwner.ImageSource = "icon - search";
            this.cmdSearchOwner.Location = new System.Drawing.Point(234, 94);
            this.cmdSearchOwner.Name = "cmdSearchOwner";
            this.cmdSearchOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchOwner.TabIndex = 46;
            this.cmdSearchOwner.TabStop = false;
            this.cmdSearchOwner.Click += new System.EventHandler(this.cmdSearchOwner_Click);
            // 
            // cmdEditOwner
            // 
            this.cmdEditOwner.AppearanceKey = "actionButton";
            this.cmdEditOwner.ImageSource = "icon - edit";
            this.cmdEditOwner.Location = new System.Drawing.Point(284, 94);
            this.cmdEditOwner.Name = "cmdEditOwner";
            this.cmdEditOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdEditOwner.TabIndex = 45;
            this.cmdEditOwner.TabStop = false;
            this.cmdEditOwner.Click += new System.EventHandler(this.cmdEditOwner_Click);
            // 
            // txtOwnerID
            // 
            //this.txtOwnerID.Appearance = 0;
            this.txtOwnerID.AutoSize = false;
            this.txtOwnerID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            //this.txtOwnerID.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtOwnerID.LinkItem = null;
            this.txtOwnerID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtOwnerID.LinkTopic = null;
            this.txtOwnerID.Location = new System.Drawing.Point(133, 94);
            this.txtOwnerID.Name = "txtOwnerID";
            this.txtOwnerID.Size = new System.Drawing.Size(90, 40);
            this.txtOwnerID.TabIndex = 1;
            this.txtOwnerID.Tag = "address";
            this.txtOwnerID.Text = "0";
            this.txtOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txtOwnerID_Validating);
            // 
            // cmdSearchSecOwner
            // 
            this.cmdSearchSecOwner.AppearanceKey = "actionButton";
            this.cmdSearchSecOwner.ImageSource = "icon - search";
            this.cmdSearchSecOwner.Location = new System.Drawing.Point(234, 177);
            this.cmdSearchSecOwner.Name = "cmdSearchSecOwner";
            this.cmdSearchSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdSearchSecOwner.TabIndex = 44;
            this.cmdSearchSecOwner.TabStop = false;
            this.cmdSearchSecOwner.Click += new System.EventHandler(this.cmdSearchSecOwner_Click);
            // 
            // cmdEditSecOwner
            // 
            this.cmdEditSecOwner.AppearanceKey = "actionButton";
            this.cmdEditSecOwner.ImageSource = "icon - edit";
            this.cmdEditSecOwner.Location = new System.Drawing.Point(284, 177);
            this.cmdEditSecOwner.Name = "cmdEditSecOwner";
            this.cmdEditSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdEditSecOwner.TabIndex = 43;
            this.cmdEditSecOwner.TabStop = false;
            this.cmdEditSecOwner.Click += new System.EventHandler(this.cmdEditSecOwner_Click);
            // 
            // txt2ndOwnerID
            // 
            //this.txt2ndOwnerID.Appearance = 0;
            this.txt2ndOwnerID.AutoSize = false;
            this.txt2ndOwnerID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            //this.txt2ndOwnerID.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txt2ndOwnerID.LinkItem = null;
            this.txt2ndOwnerID.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txt2ndOwnerID.LinkTopic = null;
            this.txt2ndOwnerID.Location = new System.Drawing.Point(133, 177);
            this.txt2ndOwnerID.Name = "txt2ndOwnerID";
            this.txt2ndOwnerID.Size = new System.Drawing.Size(90, 40);
            this.txt2ndOwnerID.TabIndex = 2;
            this.txt2ndOwnerID.Tag = "address";
            this.txt2ndOwnerID.Text = "0";
            this.txt2ndOwnerID.Validating += new System.ComponentModel.CancelEventHandler(this.txt2ndOwnerID_Validating);
            // 
            // cmdRemoveSecOwner
            // 
            this.cmdRemoveSecOwner.AppearanceKey = "actionButton";
            this.cmdRemoveSecOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveSecOwner.Location = new System.Drawing.Point(334, 177);
            this.cmdRemoveSecOwner.Name = "cmdRemoveSecOwner";
            this.cmdRemoveSecOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveSecOwner.TabIndex = 42;
            this.cmdRemoveSecOwner.TabStop = false;
            this.cmdRemoveSecOwner.Click += new System.EventHandler(this.cmdRemoveSecOwner_Click);
            // 
            // cmdRemoveOwner
            // 
            this.cmdRemoveOwner.AppearanceKey = "actionButton";
            this.cmdRemoveOwner.ImageSource = "icon-close-menu";
            this.cmdRemoveOwner.Location = new System.Drawing.Point(334, 94);
            this.cmdRemoveOwner.Name = "cmdRemoveOwner";
            this.cmdRemoveOwner.Size = new System.Drawing.Size(40, 40);
            this.cmdRemoveOwner.TabIndex = 41;
            this.cmdRemoveOwner.TabStop = false;
            this.cmdRemoveOwner.Click += new System.EventHandler(this.cmdRemoveOwner_Click);
            // 
            // gridBPDelete
            // 
            this.gridBPDelete.AllowSelection = false;
            this.gridBPDelete.AllowUserToResizeColumns = false;
            this.gridBPDelete.AllowUserToResizeRows = false;
            this.gridBPDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridBPDelete.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridBPDelete.BackColorBkg = System.Drawing.Color.Empty;
            this.gridBPDelete.BackColorFixed = System.Drawing.Color.Empty;
            this.gridBPDelete.BackColorSel = System.Drawing.Color.Empty;
            this.gridBPDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridBPDelete.Cols = 1;
            dataGridViewCellStyle9.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridBPDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.gridBPDelete.ColumnHeadersHeight = 30;
            this.gridBPDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridBPDelete.ColumnHeadersVisible = false;
            dataGridViewCellStyle10.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridBPDelete.DefaultCellStyle = dataGridViewCellStyle10;
            this.gridBPDelete.DragIcon = null;
            this.gridBPDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridBPDelete.FixedCols = 0;
            this.gridBPDelete.FixedRows = 0;
            this.gridBPDelete.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridBPDelete.FrozenCols = 0;
            this.gridBPDelete.GridColor = System.Drawing.Color.Empty;
            this.gridBPDelete.GridColorFixed = System.Drawing.Color.Empty;
            this.gridBPDelete.Location = new System.Drawing.Point(396, 138);
            this.gridBPDelete.Name = "gridBPDelete";
            this.gridBPDelete.OutlineCol = 0;
            this.gridBPDelete.ReadOnly = true;
            this.gridBPDelete.RowHeadersVisible = false;
            this.gridBPDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridBPDelete.RowHeightMin = 0;
            this.gridBPDelete.Rows = 0;
            this.gridBPDelete.ScrollTipText = null;
            this.gridBPDelete.ShowColumnVisibilityMenu = false;
            this.gridBPDelete.ShowFocusCell = false;
            this.gridBPDelete.Size = new System.Drawing.Size(10, 30);
            this.gridBPDelete.StandardTab = true;
            this.gridBPDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridBPDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridBPDelete.TabIndex = 32;
            this.gridBPDelete.Visible = false;
            // 
            // txtZone
            // 
            this.txtZone.AutoSize = false;
            this.txtZone.BackColor = System.Drawing.SystemColors.Window;
            this.txtZone.LinkItem = null;
            this.txtZone.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtZone.LinkTopic = null;
            this.txtZone.Location = new System.Drawing.Point(558, 245);
            this.txtZone.Name = "txtZone";
            this.txtZone.Size = new System.Drawing.Size(190, 40);
            this.txtZone.TabIndex = 9;
            this.txtZone.Tag = "RE";
            // 
            // txtNeighborhood
            // 
            this.txtNeighborhood.AutoSize = false;
            this.txtNeighborhood.BackColor = System.Drawing.SystemColors.Window;
            this.txtNeighborhood.LinkItem = null;
            this.txtNeighborhood.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtNeighborhood.LinkTopic = null;
            this.txtNeighborhood.Location = new System.Drawing.Point(558, 194);
            this.txtNeighborhood.Name = "txtNeighborhood";
            this.txtNeighborhood.Size = new System.Drawing.Size(190, 40);
            this.txtNeighborhood.TabIndex = 8;
            this.txtNeighborhood.Tag = "RE";
            // 
            // txtMapLot
            // 
            this.txtMapLot.AutoSize = false;
            this.txtMapLot.BackColor = System.Drawing.SystemColors.Window;
            this.txtMapLot.LinkItem = null;
            this.txtMapLot.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtMapLot.LinkTopic = null;
            this.txtMapLot.Location = new System.Drawing.Point(285, 30);
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Size = new System.Drawing.Size(158, 40);
            this.txtMapLot.TabIndex = 0;
            this.txtMapLot.Tag = "RE";
            // 
            // txtStreet
            // 
            this.txtStreet.AutoSize = false;
            this.txtStreet.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreet.LinkItem = null;
            this.txtStreet.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStreet.LinkTopic = null;
            this.txtStreet.Location = new System.Drawing.Point(274, 347);
            this.txtStreet.Name = "txtStreet";
            this.txtStreet.Size = new System.Drawing.Size(200, 40);
            this.txtStreet.TabIndex = 5;
            this.txtStreet.Tag = "RE";
            // 
            // txtApt
            // 
            this.txtApt.AutoSize = false;
            this.txtApt.BackColor = System.Drawing.SystemColors.Window;
            this.txtApt.LinkItem = null;
            this.txtApt.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtApt.LinkTopic = null;
            this.txtApt.Location = new System.Drawing.Point(204, 347);
            this.txtApt.Name = "txtApt";
            this.txtApt.Size = new System.Drawing.Size(60, 40);
            this.txtApt.TabIndex = 4;
            this.txtApt.Tag = "RE";
            // 
            // txtStreetNum
            // 
            this.txtStreetNum.AutoSize = false;
            this.txtStreetNum.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNum.LinkItem = null;
            this.txtStreetNum.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtStreetNum.LinkTopic = null;
            this.txtStreetNum.Location = new System.Drawing.Point(133, 347);
            this.txtStreetNum.Name = "txtStreetNum";
            this.txtStreetNum.Size = new System.Drawing.Size(60, 40);
            this.txtStreetNum.TabIndex = 3;
            this.txtStreetNum.Tag = "RE";
            this.txtStreetNum.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtFrontage
            // 
            this.txtFrontage.AutoSize = false;
            this.txtFrontage.BackColor = System.Drawing.SystemColors.Window;
            this.txtFrontage.LinkItem = null;
            this.txtFrontage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtFrontage.LinkTopic = null;
            this.txtFrontage.Location = new System.Drawing.Point(558, 144);
            this.txtFrontage.Name = "txtFrontage";
            this.txtFrontage.Size = new System.Drawing.Size(190, 40);
            this.txtFrontage.TabIndex = 7;
            this.txtFrontage.Tag = "RE";
            this.txtFrontage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtAcres
            // 
            this.txtAcres.AutoSize = false;
            this.txtAcres.BackColor = System.Drawing.SystemColors.Window;
            this.txtAcres.LinkItem = null;
            this.txtAcres.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAcres.LinkTopic = null;
            this.txtAcres.Location = new System.Drawing.Point(558, 94);
            this.txtAcres.Name = "txtAcres";
            this.txtAcres.Size = new System.Drawing.Size(190, 40);
            this.txtAcres.TabIndex = 6;
            this.txtAcres.Tag = "RE";
            this.txtAcres.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // lbl2ndOwner
            // 
            this.lbl2ndOwner.Location = new System.Drawing.Point(133, 229);
            this.lbl2ndOwner.Name = "lbl2ndOwner";
            this.lbl2ndOwner.Size = new System.Drawing.Size(256, 17);
            this.lbl2ndOwner.TabIndex = 49;
            // 
            // lblOwner1
            // 
            this.lblOwner1.Location = new System.Drawing.Point(133, 147);
            this.lblOwner1.Name = "lblOwner1";
            this.lblOwner1.Size = new System.Drawing.Size(258, 17);
            this.lblOwner1.TabIndex = 48;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(133, 260);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(257, 76);
            this.lblAddress.TabIndex = 47;
            // 
            // lblRE
            // 
            this.lblRE.Location = new System.Drawing.Point(478, 44);
            this.lblRE.Name = "lblRE";
            this.lblRE.Size = new System.Drawing.Size(80, 18);
            this.lblRE.TabIndex = 31;
            this.lblRE.Text = "RE ACCOUNT";
            // 
            // lblREAccount
            // 
            this.lblREAccount.Location = new System.Drawing.Point(569, 44);
            this.lblREAccount.Name = "lblREAccount";
            this.lblREAccount.Size = new System.Drawing.Size(90, 18);
            this.lblREAccount.TabIndex = 30;
            // 
            // Label_2
            // 
            this.Label_2.Location = new System.Drawing.Point(429, 259);
            this.Label_2.Name = "Label_2";
            this.Label_2.Size = new System.Drawing.Size(58, 18);
            this.Label_2.TabIndex = 29;
            this.Label_2.Text = "ZONE";
            // 
            // Label_1
            // 
            this.Label_1.Location = new System.Drawing.Point(429, 208);
            this.Label_1.Name = "Label_1";
            this.Label_1.Size = new System.Drawing.Size(100, 18);
            this.Label_1.TabIndex = 28;
            this.Label_1.Text = "NEIGHBORHOOD";
            // 
            // ImgComment
            // 
            this.ImgComment.AllowDrop = true;
            this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
            this.ImgComment.DrawStyle = ((short)(0));
            this.ImgComment.DrawWidth = ((short)(1));
            this.ImgComment.FillStyle = ((short)(1));
            this.ImgComment.FontTransparent = true;
            this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
            this.ImgComment.Location = new System.Drawing.Point(5, 13);
            this.ImgComment.Name = "ImgComment";
            this.ImgComment.Picture = ((System.Drawing.Image)(resources.GetObject("ImgComment.Picture")));
            this.ImgComment.Size = new System.Drawing.Size(17, 18);
            this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ImgComment.TabIndex = 50;
            this.ImgComment.Click += new System.EventHandler(this.ImgComment_Click);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(197, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 18);
            this.Label3.TabIndex = 25;
            this.Label3.Text = "MAP/LOT";
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(104, 44);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(77, 18);
            this.lblAccount.TabIndex = 24;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(70, 18);
            this.Label2.TabIndex = 23;
            this.Label2.Text = "ACCOUNT";
            // 
            // Label1_12
            // 
            this.Label1_12.Location = new System.Drawing.Point(30, 361);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(74, 18);
            this.Label1_12.TabIndex = 21;
            this.Label1_12.Text = "LOCATION";
            // 
            // Label_0
            // 
            this.Label_0.Location = new System.Drawing.Point(429, 158);
            this.Label_0.Name = "Label_0";
            this.Label_0.Size = new System.Drawing.Size(70, 18);
            this.Label_0.TabIndex = 20;
            this.Label_0.Text = "FRONTAGE";
            // 
            // Label_12
            // 
            this.Label_12.Location = new System.Drawing.Point(429, 112);
            this.Label_12.Name = "Label_12";
            this.Label_12.Size = new System.Drawing.Size(41, 18);
            this.Label_12.TabIndex = 19;
            this.Label_12.Text = "ACRES";
            // 
            // Label1_2
            // 
            this.Label1_2.Location = new System.Drawing.Point(30, 260);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(66, 18);
            this.Label1_2.TabIndex = 18;
            this.Label1_2.Text = "ADDRESS";
            // 
            // Label1_1
            // 
            this.Label1_1.Location = new System.Drawing.Point(30, 191);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(74, 18);
            this.Label1_1.TabIndex = 17;
            this.Label1_1.Text = "2ND OWNER";
            // 
            // Label1_0
            // 
            this.Label1_0.Location = new System.Drawing.Point(30, 112);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(50, 18);
            this.Label1_0.TabIndex = 16;
            this.Label1_0.Text = "OWNER";
            // 
            // GridAbutters
            // 
            this.GridAbutters.AllowSelection = false;
            this.GridAbutters.AllowUserToResizeColumns = false;
            this.GridAbutters.AllowUserToResizeRows = false;
            this.GridAbutters.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridAbutters.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridAbutters.BackColorBkg = System.Drawing.Color.Empty;
            this.GridAbutters.BackColorFixed = System.Drawing.Color.Empty;
            this.GridAbutters.BackColorSel = System.Drawing.Color.Empty;
            this.GridAbutters.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridAbutters.Cols = 3;
            dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridAbutters.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.GridAbutters.ColumnHeadersHeight = 30;
            this.GridAbutters.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridAbutters.DefaultCellStyle = dataGridViewCellStyle12;
            this.GridAbutters.DragIcon = null;
            this.GridAbutters.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridAbutters.ExtendLastCol = true;
            this.GridAbutters.FixedCols = 0;
            this.GridAbutters.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridAbutters.FrozenCols = 0;
            this.GridAbutters.GridColor = System.Drawing.Color.Empty;
            this.GridAbutters.GridColorFixed = System.Drawing.Color.Empty;
            this.GridAbutters.Location = new System.Drawing.Point(684, 440);
            this.GridAbutters.Name = "GridAbutters";
            this.GridAbutters.OutlineCol = 0;
            this.GridAbutters.ReadOnly = true;
            this.GridAbutters.RowHeadersVisible = false;
            this.GridAbutters.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridAbutters.RowHeightMin = 0;
            this.GridAbutters.Rows = 1;
            this.GridAbutters.ScrollTipText = null;
            this.GridAbutters.ShowColumnVisibilityMenu = false;
            this.GridAbutters.ShowFocusCell = false;
            this.GridAbutters.Size = new System.Drawing.Size(333, 90);
            this.GridAbutters.StandardTab = true;
            this.GridAbutters.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridAbutters.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridAbutters.TabIndex = 12;
            this.GridAbutters.KeyDown += new Wisej.Web.KeyEventHandler(this.GridAbutters_KeyDownEvent);
            this.GridAbutters.Click += new System.EventHandler(this.GridAbutters_ClickEvent);
            // 
            // GridPreviousMaps
            // 
            this.GridPreviousMaps.AllowSelection = false;
            this.GridPreviousMaps.AllowUserToResizeColumns = false;
            this.GridPreviousMaps.AllowUserToResizeRows = false;
            this.GridPreviousMaps.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridPreviousMaps.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridPreviousMaps.BackColorBkg = System.Drawing.Color.Empty;
            this.GridPreviousMaps.BackColorFixed = System.Drawing.Color.Empty;
            this.GridPreviousMaps.BackColorSel = System.Drawing.Color.Empty;
            this.GridPreviousMaps.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridPreviousMaps.Cols = 2;
            dataGridViewCellStyle13.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridPreviousMaps.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.GridPreviousMaps.ColumnHeadersHeight = 30;
            this.GridPreviousMaps.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle14.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridPreviousMaps.DefaultCellStyle = dataGridViewCellStyle14;
            this.GridPreviousMaps.DragIcon = null;
            this.GridPreviousMaps.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridPreviousMaps.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridPreviousMaps.ExtendLastCol = true;
            this.GridPreviousMaps.FixedCols = 0;
            this.GridPreviousMaps.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridPreviousMaps.FrozenCols = 0;
            this.GridPreviousMaps.GridColor = System.Drawing.Color.Empty;
            this.GridPreviousMaps.GridColorFixed = System.Drawing.Color.Empty;
            this.GridPreviousMaps.Location = new System.Drawing.Point(684, 640);
            this.GridPreviousMaps.Name = "GridPreviousMaps";
            this.GridPreviousMaps.OutlineCol = 0;
            this.GridPreviousMaps.RowHeadersVisible = false;
            this.GridPreviousMaps.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridPreviousMaps.RowHeightMin = 0;
            this.GridPreviousMaps.Rows = 1;
            this.GridPreviousMaps.ScrollTipText = null;
            this.GridPreviousMaps.ShowColumnVisibilityMenu = false;
            this.GridPreviousMaps.ShowFocusCell = false;
            this.GridPreviousMaps.Size = new System.Drawing.Size(333, 90);
            this.GridPreviousMaps.StandardTab = true;
            this.GridPreviousMaps.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridPreviousMaps.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridPreviousMaps.TabIndex = 14;
            this.GridPreviousMaps.Tag = "REGrid";
            this.GridPreviousMaps.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridPreviousMaps_AfterEdit);
            this.GridPreviousMaps.KeyDown += new Wisej.Web.KeyEventHandler(this.GridPreviousMaps_KeyDownEvent);
            // 
            // GridDeletedPhones
            // 
            this.GridDeletedPhones.AllowSelection = false;
            this.GridDeletedPhones.AllowUserToResizeColumns = false;
            this.GridDeletedPhones.AllowUserToResizeRows = false;
            this.GridDeletedPhones.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridDeletedPhones.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridDeletedPhones.BackColorBkg = System.Drawing.Color.Empty;
            this.GridDeletedPhones.BackColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedPhones.BackColorSel = System.Drawing.Color.Empty;
            this.GridDeletedPhones.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridDeletedPhones.Cols = 2;
            dataGridViewCellStyle15.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridDeletedPhones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.GridDeletedPhones.ColumnHeadersHeight = 30;
            this.GridDeletedPhones.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridDeletedPhones.ColumnHeadersVisible = false;
            dataGridViewCellStyle16.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridDeletedPhones.DefaultCellStyle = dataGridViewCellStyle16;
            this.GridDeletedPhones.DragIcon = null;
            this.GridDeletedPhones.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridDeletedPhones.FixedRows = 0;
            this.GridDeletedPhones.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedPhones.FrozenCols = 0;
            this.GridDeletedPhones.GridColor = System.Drawing.Color.Empty;
            this.GridDeletedPhones.GridColorFixed = System.Drawing.Color.Empty;
            this.GridDeletedPhones.Location = new System.Drawing.Point(0, 24);
            this.GridDeletedPhones.Name = "GridDeletedPhones";
            this.GridDeletedPhones.OutlineCol = 0;
            this.GridDeletedPhones.ReadOnly = true;
            this.GridDeletedPhones.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridDeletedPhones.RowHeightMin = 0;
            this.GridDeletedPhones.Rows = 0;
            this.GridDeletedPhones.ScrollTipText = null;
            this.GridDeletedPhones.ShowColumnVisibilityMenu = false;
            this.GridDeletedPhones.ShowFocusCell = false;
            this.GridDeletedPhones.Size = new System.Drawing.Size(43, 19);
            this.GridDeletedPhones.StandardTab = true;
            this.GridDeletedPhones.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridDeletedPhones.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridDeletedPhones.TabIndex = 27;
            this.GridDeletedPhones.Visible = false;
            // 
            // BPGrid
            // 
            this.BPGrid.AllowSelection = false;
            this.BPGrid.AllowUserToResizeColumns = false;
            this.BPGrid.AllowUserToResizeRows = false;
            this.BPGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.BPGrid.BackColorAlternate = System.Drawing.Color.Empty;
            this.BPGrid.BackColorBkg = System.Drawing.Color.Empty;
            this.BPGrid.BackColorFixed = System.Drawing.Color.Empty;
            this.BPGrid.BackColorSel = System.Drawing.Color.Empty;
            this.BPGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.BPGrid.Cols = 4;
            dataGridViewCellStyle17.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.BPGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.BPGrid.ColumnHeadersHeight = 30;
            this.BPGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle18.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.BPGrid.DefaultCellStyle = dataGridViewCellStyle18;
            this.BPGrid.DragIcon = null;
            this.BPGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.BPGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.BPGrid.ExtendLastCol = true;
            this.BPGrid.FixedCols = 0;
            this.BPGrid.ForeColorFixed = System.Drawing.Color.Empty;
            this.BPGrid.FrozenCols = 0;
            this.BPGrid.GridColor = System.Drawing.Color.Empty;
            this.BPGrid.GridColorFixed = System.Drawing.Color.Empty;
            this.BPGrid.Location = new System.Drawing.Point(684, 540);
            this.BPGrid.Name = "BPGrid";
            this.BPGrid.OutlineCol = 0;
            this.BPGrid.RowHeadersVisible = false;
            this.BPGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.BPGrid.RowHeightMin = 0;
            this.BPGrid.Rows = 2;
            this.BPGrid.ScrollTipText = null;
            this.BPGrid.ShowColumnVisibilityMenu = false;
            this.BPGrid.ShowFocusCell = false;
            this.BPGrid.Size = new System.Drawing.Size(333, 90);
            this.BPGrid.StandardTab = true;
            this.BPGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.BPGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.BPGrid.TabIndex = 13;
            this.BPGrid.Tag = "REGrid";
            this.BPGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.BPGrid_KeyPressEdit);
            this.BPGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.BPGrid_AfterEdit);
            this.BPGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.BPGrid_ValidateEdit);
            this.BPGrid.CurrentCellChanged += new System.EventHandler(this.BPGrid_RowColChange);
            this.BPGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.BPGrid_KeyDownEvent);
            // 
            // lblViewOnly
            // 
            this.lblViewOnly.Location = new System.Drawing.Point(684, 743);
            this.lblViewOnly.Name = "lblViewOnly";
            this.lblViewOnly.Size = new System.Drawing.Size(333, 18);
            this.lblViewOnly.TabIndex = 22;
            this.lblViewOnly.Text = "VIEW ONLY";
            this.lblViewOnly.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblViewOnly.Visible = false;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintAbutters,
            this.mnuPrintActivity,
            this.mnuPrintActivityWithDetail,
            this.mnuPrintHistory,
            this.mnuPrintViolations,
            this.mnuPrintEnvelope});
            this.MainMenu1.Name = null;
            // 
            // mnuPrintAbutters
            // 
            this.mnuPrintAbutters.Index = 0;
            this.mnuPrintAbutters.Name = "mnuPrintAbutters";
            this.mnuPrintAbutters.Text = "Print Abutters";
            this.mnuPrintAbutters.Click += new System.EventHandler(this.mnuPrintAbutters_Click);
            // 
            // mnuPrintActivity
            // 
            this.mnuPrintActivity.Index = 1;
            this.mnuPrintActivity.Name = "mnuPrintActivity";
            this.mnuPrintActivity.Text = "Print Activity";
            this.mnuPrintActivity.Click += new System.EventHandler(this.mnuPrintActivity_Click);
            // 
            // mnuPrintActivityWithDetail
            // 
            this.mnuPrintActivityWithDetail.Index = 2;
            this.mnuPrintActivityWithDetail.Name = "mnuPrintActivityWithDetail";
            this.mnuPrintActivityWithDetail.Text = "Print Activity with Detail";
            this.mnuPrintActivityWithDetail.Click += new System.EventHandler(this.mnuPrintActivityWithDetail_Click);
            // 
            // mnuPrintHistory
            // 
            this.mnuPrintHistory.Index = 3;
            this.mnuPrintHistory.Name = "mnuPrintHistory";
            this.mnuPrintHistory.Text = "Print Account Permit History";
            this.mnuPrintHistory.Click += new System.EventHandler(this.mnuPrintHistory_Click);
            // 
            // mnuPrintViolations
            // 
            this.mnuPrintViolations.Index = 4;
            this.mnuPrintViolations.Name = "mnuPrintViolations";
            this.mnuPrintViolations.Text = "Print Violations";
            this.mnuPrintViolations.Click += new System.EventHandler(this.mnuPrintViolations_Click);
            // 
            // mnuPrintEnvelope
            // 
            this.mnuPrintEnvelope.Index = 5;
            this.mnuPrintEnvelope.Name = "mnuPrintEnvelope";
            this.mnuPrintEnvelope.Text = "Print Envelope";
            this.mnuPrintEnvelope.Click += new System.EventHandler(this.mnuPrintEnvelope_Click);
            // 
            // mnuChangeREAssoc
            // 
            this.mnuChangeREAssoc.Index = -1;
            this.mnuChangeREAssoc.Name = "mnuChangeREAssoc";
            this.mnuChangeREAssoc.Shortcut = Wisej.Web.Shortcut.F9;
            this.mnuChangeREAssoc.Text = "Change Real Estate Association";
            this.mnuChangeREAssoc.Click += new System.EventHandler(this.mnuChangeREAssoc_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddActivity
            // 
            this.mnuAddActivity.Index = -1;
            this.mnuAddActivity.Name = "mnuAddActivity";
            this.mnuAddActivity.Text = "Add Activity";
            this.mnuAddActivity.Click += new System.EventHandler(this.mnuAddActivity_Click);
            // 
            // mnuAddPermit
            // 
            this.mnuAddPermit.Index = -1;
            this.mnuAddPermit.Name = "mnuAddPermit";
            this.mnuAddPermit.Text = "Add Permit";
            this.mnuAddPermit.Click += new System.EventHandler(this.mnuAddPermit_Click);
            // 
            // mnuAddViolation
            // 
            this.mnuAddViolation.Index = -1;
            this.mnuAddViolation.Name = "mnuAddViolation";
            this.mnuAddViolation.Text = "Add Violation";
            this.mnuAddViolation.Click += new System.EventHandler(this.mnuAddViolation_Click);
            // 
            // mnuSepar5
            // 
            this.mnuSepar5.Index = -1;
            this.mnuSepar5.Name = "mnuSepar5";
            this.mnuSepar5.Text = "-";
            // 
            // mnuCreateDocument
            // 
            this.mnuCreateDocument.Index = -1;
            this.mnuCreateDocument.Name = "mnuCreateDocument";
            this.mnuCreateDocument.Text = "Create Document";
            this.mnuCreateDocument.Click += new System.EventHandler(this.mnuCreateDocument_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuComment
            // 
            this.mnuComment.Index = -1;
            this.mnuComment.Name = "mnuComment";
            this.mnuComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuComment.Text = "Comment";
            this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = -1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuDeleteAccount
            // 
            this.mnuDeleteAccount.Index = -1;
            this.mnuDeleteAccount.Name = "mnuDeleteAccount";
            this.mnuDeleteAccount.Text = "Delete Account";
            this.mnuDeleteAccount.Click += new System.EventHandler(this.mnuDeleteAccount_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(478, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(76, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddActivity
            // 
            this.cmdAddActivity.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddActivity.AppearanceKey = "toolbarButton";
            this.cmdAddActivity.Location = new System.Drawing.Point(197, 29);
            this.cmdAddActivity.Name = "cmdAddActivity";
            this.cmdAddActivity.Size = new System.Drawing.Size(92, 24);
            this.cmdAddActivity.TabIndex = 1;
            this.cmdAddActivity.Text = "Add Activity";
            this.cmdAddActivity.Click += new System.EventHandler(this.mnuAddActivity_Click);
            // 
            // cmdAddPermit
            // 
            this.cmdAddPermit.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddPermit.AppearanceKey = "toolbarButton";
            this.cmdAddPermit.Location = new System.Drawing.Point(295, 29);
            this.cmdAddPermit.Name = "cmdAddPermit";
            this.cmdAddPermit.Size = new System.Drawing.Size(86, 24);
            this.cmdAddPermit.TabIndex = 2;
            this.cmdAddPermit.Text = "Add Permit";
            this.cmdAddPermit.Click += new System.EventHandler(this.mnuAddPermit_Click);
            // 
            // cmdAddViolation
            // 
            this.cmdAddViolation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddViolation.AppearanceKey = "toolbarButton";
            this.cmdAddViolation.Location = new System.Drawing.Point(387, 29);
            this.cmdAddViolation.Name = "cmdAddViolation";
            this.cmdAddViolation.Size = new System.Drawing.Size(100, 24);
            this.cmdAddViolation.TabIndex = 3;
            this.cmdAddViolation.Text = "Add Violation";
            this.cmdAddViolation.Click += new System.EventHandler(this.mnuAddViolation_Click);
            // 
            // cmdCreateDocument
            // 
            this.cmdCreateDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdCreateDocument.AppearanceKey = "toolbarButton";
            this.cmdCreateDocument.Location = new System.Drawing.Point(493, 29);
            this.cmdCreateDocument.Name = "cmdCreateDocument";
            this.cmdCreateDocument.Size = new System.Drawing.Size(126, 24);
            this.cmdCreateDocument.TabIndex = 4;
            this.cmdCreateDocument.Text = "Create Document";
            this.cmdCreateDocument.Click += new System.EventHandler(this.mnuCreateDocument_Click);
            // 
            // cmdChangeREAssociation
            // 
            this.cmdChangeREAssociation.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdChangeREAssociation.AppearanceKey = "toolbarButton";
            this.cmdChangeREAssociation.Location = new System.Drawing.Point(625, 29);
            this.cmdChangeREAssociation.Name = "cmdChangeREAssociation";
            this.cmdChangeREAssociation.Shortcut = Wisej.Web.Shortcut.F9;
            this.cmdChangeREAssociation.Size = new System.Drawing.Size(213, 24);
            this.cmdChangeREAssociation.TabIndex = 5;
            this.cmdChangeREAssociation.Text = "Change Real Estate Association";
            this.cmdChangeREAssociation.Click += new System.EventHandler(this.mnuChangeREAssoc_Click);
            // 
            // cmdComment
            // 
            this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdComment.AppearanceKey = "toolbarButton";
            this.cmdComment.Location = new System.Drawing.Point(844, 29);
            this.cmdComment.Name = "cmdComment";
            this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdComment.Size = new System.Drawing.Size(78, 24);
            this.cmdComment.TabIndex = 6;
            this.cmdComment.Text = "Comment";
            this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
            // 
            // cmdDeleteAccount
            // 
            this.cmdDeleteAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteAccount.AppearanceKey = "toolbarButton";
            this.cmdDeleteAccount.Location = new System.Drawing.Point(928, 29);
            this.cmdDeleteAccount.Name = "cmdDeleteAccount";
            this.cmdDeleteAccount.Size = new System.Drawing.Size(112, 24);
            this.cmdDeleteAccount.TabIndex = 7;
            this.cmdDeleteAccount.Text = "Delete Account";
            this.cmdDeleteAccount.Click += new System.EventHandler(this.mnuDeleteAccount_Click);
            // 
            // frmMaster
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1068, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmMaster";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Property";
            this.Load += new System.EventHandler(this.frmMaster_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMaster_KeyDown);
            this.Resize += new System.EventHandler(this.frmMaster_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.SSTab1.ResumeLayout(false);
            this.SSTab1_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridOpens)).EndInit();
            this.SSTab1_Page2.ResumeLayout(false);
            this.SSTab1_Page2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridPermits)).EndInit();
            this.SSTab1_Page3.ResumeLayout(false);
            this.SSTab1_Page3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridViolations)).EndInit();
            this.SSTab1_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framREInfo)).EndInit();
            this.framREInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearchSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEditSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveSecOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridBPDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridAbutters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridPreviousMaps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BPGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddActivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddPermit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddViolation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCreateDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChangeREAssociation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteAccount)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdAddPermit;
		private FCButton cmdAddActivity;
		private FCButton cmdAddViolation;
		private FCButton cmdCreateDocument;
		private FCButton cmdComment;
		private FCButton cmdChangeREAssociation;
		private FCButton cmdDeleteAccount;
	}
}