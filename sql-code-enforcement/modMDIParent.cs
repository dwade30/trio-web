﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	public class modMDIParent
	{
		//=========================================================
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmComment, frmAddDocument, frmScanDocument, frmViewDocuments, frmCalendar, frmCentralPartySearch, frmEditCentralParties)
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public static bool FormExist(FCForm FormName)
		{
			bool FormExist = false;
			foreach (FCForm frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		public static void EnableDisableMenuOption(bool boolState, int intRow)
		{
			//if (!boolState)
			//{
			//	MDIParent.InstancePtr.GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//	MDIParent.InstancePtr.GRID.RowData(intRow, false);
			//}
			//else
			//{
			//	MDIParent.InstancePtr.GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1, Color.Black);
			//	MDIParent.InstancePtr.GRID.RowData(intRow, true);
			//}
		}
	}
}
