//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmInspectors : BaseForm
	{
		public frmInspectors()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmInspectors InstancePtr
		{
			get
			{
				return (frmInspectors)Sys.GetInstance(typeof(frmInspectors));
			}
		}

		protected frmInspectors _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Corey
		// Date           03/30/2005
		// add or delete inspectors
		// ********************************************************
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLSHORTNAME = 2;
		bool boolEdited;
		private bool boolCantEdit;

		private void frmInspectors_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmInspectors_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInspectors properties;
			//frmInspectors.FillStyle	= 0;
			//frmInspectors.ScaleWidth	= 9300;
			//frmInspectors.ScaleHeight	= 7725;
			//frmInspectors.LinkTopic	= "Form2";
			//frmInspectors.LockControls	= -1  'True;
			//frmInspectors.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITDESCRIPTIONSECURITY, false))
			{
				boolCantEdit = true;
				cmdAddInspector.Enabled = false;
				cmdDeleteInspector.Enabled = false;
				cmdSave.Enabled = false;
				mnuSaveExit.Enabled = false;
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			SetupGrid();
			LoadInspectors();
			boolEdited = false;
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 3;
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLSHORTNAME, "Short Name");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(0.75 * GridWidth));
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolEdited && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
				{
					if (!SaveInspectors())
					{
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void frmInspectors_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void LoadInspectors()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			Grid.Rows = 1;
			GridDelete.Rows = 0;
			clsLoad.OpenRecordset("select * from Inspectors order by ID", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(clsLoad.Get_Fields_String("name")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTNAME, FCConvert.ToString(clsLoad.Get_Fields_String("shortname")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				clsLoad.MoveNext();
			}
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (Grid.Row > 0)
			{
				Grid.RowData(Grid.Row, true);
				boolEdited = true;
			}
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteInspector(Grid.Row);
						break;
					}
				case Keys.Insert:
					{
						AddInspector(Grid.Row);
						break;
					}
			}
			//end switch
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = Grid.GetFlexRowIndex(e.RowIndex);
            int col = Grid.GetFlexColIndex(e.ColumnIndex);

            if (row > 0)
			{
				Grid.RowData(row, true);
				boolEdited = true;
			}
		}

		private void mnuAddInspector_Click(object sender, System.EventArgs e)
		{
			AddInspector();
		}

		private void AddInspector(int lngRow = -1)
		{
			boolEdited = true;
			if (lngRow <= 0)
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
			}
			else
			{
				Grid.AddItem(FCConvert.ToString(0), lngRow);
			}
			Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(0));
			Grid.RowData(lngRow, true);
			Grid.Row = lngRow;
			Grid.Col = CNSTGRIDCOLNAME;
			Grid.EditCell();
		}

		private void mnuDeleteInspector_Click(object sender, System.EventArgs e)
		{
			DeleteInspector(Grid.Row);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//App.DoEvents();
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void DeleteInspector(int lngRow)
		{
			if (lngRow < 1)
			{
				MessageBox.Show("You must select an inspector first", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) > 0)
			{
				boolEdited = true;
				GridDelete.Rows += 1;
				GridDelete.TextMatrix(GridDelete.Rows - 1, 0, Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID));
			}
			Grid.RemoveItem(lngRow);
		}

		private bool SaveInspectors()
		{
			bool SaveInspectors = false;
			// first delete all of the deleted ones
			// then save all of the changed ones
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngRow;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Grid.Row = 0;
				//App.DoEvents();
				SaveInspectors = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				for (x = 0; x <= (GridDelete.Rows - 1); x++)
				{
					clsSave.Execute("delete from inspectors where ID = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(x, 0))), modGlobalVariables.Statics.strCEDatabase);
				}
				// x
				GridDelete.Rows = 0;
				lngRow = Grid.FindRow(true, 1);
				while (lngRow > 0)
				{
					clsSave.OpenRecordset("select * from inspectors where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))), modGlobalVariables.Statics.strCEDatabase);
					if (FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) > 0)
					{
						if (!clsSave.EndOfFile())
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
					}
					else
					{
						clsSave.AddNew();
					}
					clsSave.Set_Fields("name", Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME));
					clsSave.Set_Fields("shortname", Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTNAME));
					clsSave.Update();
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
					Grid.RowData(lngRow, false);
					if (lngRow < Grid.Rows - 1)
					{
						lngRow = Grid.FindRow(true, lngRow + 1);
					}
					else
					{
						lngRow = -1;
					}
				}
				boolEdited = false;
				SaveInspectors = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInspectors;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInspectors", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInspectors;
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			if (Grid.Rows > 1)
			{
				rptInspectors.InstancePtr.Init();
			}
			else
			{
				MessageBox.Show("There are no inspectors to print", "No Inspectors", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInspectors();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInspectors())
			{
				mnuExit_Click();
			}
		}
	}
}
