//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmGetRecords.
	/// </summary>
	partial class frmGetRecords
	{
		public fecherFoundation.FCComboBox cmbtrecordtype;
		public fecherFoundation.FCLabel lbltrecordtype;
		public fecherFoundation.FCComboBox cmbViolationSearchType;
		public fecherFoundation.FCLabel lblViolationSearchType;
		public fecherFoundation.FCComboBox cmbtcontain;
		public fecherFoundation.FCLabel lbltcontain;
		public fecherFoundation.FCComboBox cmbtsearchtype;
		public fecherFoundation.FCLabel lbltsearchtype;
		public fecherFoundation.FCFrame Frame2;
		public Global.T2KDateBox t2kSearchDate;
		public fecherFoundation.FCCheckBox chkShowDel;
		public fecherFoundation.FCButton cmdClear;
		//public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtSearch2;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCLabel lblPage;
		public fecherFoundation.FCLabel lblBook;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCFrame framSearch;
		public FCGrid GridSearch;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuNewSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbtrecordtype = new fecherFoundation.FCComboBox();
            this.lbltrecordtype = new fecherFoundation.FCLabel();
            this.cmbViolationSearchType = new fecherFoundation.FCComboBox();
            this.lblViolationSearchType = new fecherFoundation.FCLabel();
            this.cmbtcontain = new fecherFoundation.FCComboBox();
            this.lbltcontain = new fecherFoundation.FCLabel();
            this.cmbtsearchtype = new fecherFoundation.FCComboBox();
            this.lbltsearchtype = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkShowDel = new fecherFoundation.FCCheckBox();
            this.t2kSearchDate = new Global.T2KDateBox();
            this.cmdClear = new fecherFoundation.FCButton();
            this.txtSearch2 = new fecherFoundation.FCTextBox();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.lblSearchInfo = new fecherFoundation.FCLabel();
            this.lblPage = new fecherFoundation.FCLabel();
            this.lblBook = new fecherFoundation.FCLabel();
            this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
            this.cmdGetAccountNumber = new fecherFoundation.FCButton();
            this.framSearch = new fecherFoundation.FCFrame();
            this.GridSearch = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNewSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnSearch = new fecherFoundation.FCButton();
            this.cmdAddNew = new fecherFoundation.FCButton();
            this.cmdNewSearch = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kSearchDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framSearch)).BeginInit();
            this.framSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdGetAccountNumber);
            this.BottomPanel.Location = new System.Drawing.Point(0, 567);
            this.BottomPanel.Size = new System.Drawing.Size(961, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.cmbtrecordtype);
            this.ClientArea.Controls.Add(this.lbltrecordtype);
            this.ClientArea.Controls.Add(this.txtGetAccountNumber);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.framSearch);
            this.ClientArea.Size = new System.Drawing.Size(961, 507);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.btnSearch);
            this.TopPanel.Controls.Add(this.cmdNewSearch);
            this.TopPanel.Controls.Add(this.cmdAddNew);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(961, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.btnSearch, 0);
            // 
            // HeaderText
            // 
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbtrecordtype
            // 
            this.cmbtrecordtype.Appearance = fecherFoundation.FCComboBox.AppearanceConstants.dbl3D;
            this.cmbtrecordtype.Items.AddRange(new object[] {
            "Property Records",
            "Contractors",
            "Inspections",
            "Permits",
            "Violations"});
            this.cmbtrecordtype.Location = new System.Drawing.Point(236, 105);
            this.cmbtrecordtype.Name = "cmbtrecordtype";
            this.cmbtrecordtype.Size = new System.Drawing.Size(330, 40);
            this.cmbtrecordtype.TabIndex = 17;
            this.cmbtrecordtype.Text = "Property Records";
            this.ToolTip1.SetToolTip(this.cmbtrecordtype, null);
            this.cmbtrecordtype.Visible = false;
            this.cmbtrecordtype.SelectedIndexChanged += new System.EventHandler(this.Optrecordtype_CheckedChanged);
            // 
            // lbltrecordtype
            // 
            this.lbltrecordtype.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lbltrecordtype.AutoSize = true;
            this.lbltrecordtype.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lbltrecordtype.Location = new System.Drawing.Point(30, 119);
            this.lbltrecordtype.Name = "lbltrecordtype";
            this.lbltrecordtype.Size = new System.Drawing.Size(200, 15);
            this.lbltrecordtype.TabIndex = 1;
            this.lbltrecordtype.Text = "TYPE OF RECORDS TO ACCESS";
            this.ToolTip1.SetToolTip(this.lbltrecordtype, null);
            this.lbltrecordtype.Visible = false;
            // 
            // cmbViolationSearchType
            // 
            this.cmbViolationSearchType.Appearance = fecherFoundation.FCComboBox.AppearanceConstants.dbl3D;
            this.cmbViolationSearchType.Items.AddRange(new object[] {
            "Name",
            "Location",
            "Doing Business As",
            "Date Violation Issued",
            "Date Violation Occurred",
            "Date Violation Resolved"});
            this.cmbViolationSearchType.Location = new System.Drawing.Point(196, 67);
            this.cmbViolationSearchType.Name = "cmbViolationSearchType";
            this.cmbViolationSearchType.Size = new System.Drawing.Size(320, 40);
            this.cmbViolationSearchType.TabIndex = 2;
            this.cmbViolationSearchType.Text = "Name";
            this.ToolTip1.SetToolTip(this.cmbViolationSearchType, null);
            this.cmbViolationSearchType.SelectedIndexChanged += new System.EventHandler(this.optViolationSearchType_CheckedChanged);
            // 
            // lblViolationSearchType
            // 
            this.lblViolationSearchType.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblViolationSearchType.AutoSize = true;
            this.lblViolationSearchType.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblViolationSearchType.Location = new System.Drawing.Point(20, 81);
            this.lblViolationSearchType.Name = "lblViolationSearchType";
            this.lblViolationSearchType.Size = new System.Drawing.Size(79, 15);
            this.lblViolationSearchType.TabIndex = 3;
            this.lblViolationSearchType.Text = "SEARCH BY";
            this.ToolTip1.SetToolTip(this.lblViolationSearchType, null);
            // 
            // cmbtcontain
            // 
            this.cmbtcontain.Appearance = fecherFoundation.FCComboBox.AppearanceConstants.dbl3D;
            this.cmbtcontain.Items.AddRange(new object[] {
            "Contain Search Criteria",
            "Start With Search Criteria"});
            this.cmbtcontain.Location = new System.Drawing.Point(196, 117);
            this.cmbtcontain.Name = "cmbtcontain";
            this.cmbtcontain.Size = new System.Drawing.Size(320, 40);
            this.cmbtcontain.TabIndex = 3;
            this.cmbtcontain.Text = "Start With Search Criteria";
            this.ToolTip1.SetToolTip(this.cmbtcontain, null);
            // 
            // lbltcontain
            // 
            this.lbltcontain.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lbltcontain.AutoSize = true;
            this.lbltcontain.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lbltcontain.Location = new System.Drawing.Point(20, 131);
            this.lbltcontain.Name = "lbltcontain";
            this.lbltcontain.Size = new System.Drawing.Size(160, 15);
            this.lbltcontain.TabIndex = 1;
            this.lbltcontain.Text = "DISPLAY RECORDS THAT";
            this.ToolTip1.SetToolTip(this.lbltcontain, null);
            // 
            // cmbtsearchtype
            // 
            this.cmbtsearchtype.Appearance = fecherFoundation.FCComboBox.AppearanceConstants.dbl3D;
            this.cmbtsearchtype.Items.AddRange(new object[] {
            "Owner Name",
            "Location",
            "Map / Lot",
            "Contractor Name",
            "Contractor Number",
            "Permit",
            "Permit Identifier",
            "Inspection Date"});
            this.cmbtsearchtype.Location = new System.Drawing.Point(196, 67);
            this.cmbtsearchtype.Name = "cmbtsearchtype";
            this.cmbtsearchtype.Size = new System.Drawing.Size(320, 40);
            this.cmbtsearchtype.TabIndex = 4;
            this.cmbtsearchtype.Text = "Owner Name";
            this.ToolTip1.SetToolTip(this.cmbtsearchtype, null);
            this.cmbtsearchtype.SelectedItemChanged += new System.EventHandler(this.Optsearchtype_CheckedChanged);
            // 
            // lbltsearchtype
            // 
            this.lbltsearchtype.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lbltsearchtype.AutoSize = true;
            this.lbltsearchtype.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lbltsearchtype.Location = new System.Drawing.Point(30, 81);
            this.lbltsearchtype.Name = "lbltsearchtype";
            this.lbltsearchtype.Size = new System.Drawing.Size(79, 15);
            this.lbltsearchtype.TabIndex = 5;
            this.lbltsearchtype.Text = "SEARCH BY";
            this.ToolTip1.SetToolTip(this.lbltsearchtype, null);
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkShowDel);
            this.Frame2.Controls.Add(this.cmbtcontain);
            this.Frame2.Controls.Add(this.lbltcontain);
            this.Frame2.Controls.Add(this.cmbViolationSearchType);
            this.Frame2.Controls.Add(this.lblViolationSearchType);
            this.Frame2.Controls.Add(this.cmbtsearchtype);
            this.Frame2.Controls.Add(this.lbltsearchtype);
            this.Frame2.Controls.Add(this.t2kSearchDate);
            this.Frame2.Controls.Add(this.txtSearch2);
            this.Frame2.Controls.Add(this.txtSearch);
            this.Frame2.Controls.Add(this.lblSearchInfo);
            this.Frame2.Controls.Add(this.lblPage);
            this.Frame2.Controls.Add(this.lblBook);
            this.Frame2.Location = new System.Drawing.Point(30, 100);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(536, 234);
            this.Frame2.TabIndex = 16;
            this.Frame2.Text = "Search";
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // chkShowDel
            // 
            this.chkShowDel.Location = new System.Drawing.Point(20, 30);
            this.chkShowDel.Name = "chkShowDel";
            this.chkShowDel.Size = new System.Drawing.Size(221, 27);
            this.chkShowDel.TabIndex = 2;
            this.chkShowDel.Text = "Include Deleted Properties";
            this.ToolTip1.SetToolTip(this.chkShowDel, null);
            this.chkShowDel.Value = fecherFoundation.FCCheckBox.ValueSettings.Unchecked;
            // 
            // t2kSearchDate
            // 
            this.t2kSearchDate.Location = new System.Drawing.Point(196, 167);
            this.t2kSearchDate.Mask = "##/##/####";
            this.t2kSearchDate.Name = "t2kSearchDate";
            this.t2kSearchDate.Size = new System.Drawing.Size(120, 40);
            this.t2kSearchDate.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.t2kSearchDate, null);
            this.t2kSearchDate.Visible = false;
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdClear.Location = new System.Drawing.Point(594, 29);
            this.cmdClear.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(93, 24);
            this.cmdClear.TabIndex = 14;
            this.cmdClear.Text = "Clear Search";
            this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // txtSearch2
            // 
            this.txtSearch2.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch2.Location = new System.Drawing.Point(417, 167);
            this.txtSearch2.Name = "txtSearch2";
            this.txtSearch2.Size = new System.Drawing.Size(99, 40);
            this.txtSearch2.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtSearch2, null);
            this.txtSearch2.Visible = false;
            this.txtSearch2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch2_KeyDown);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(196, 167);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(320, 40);
            this.txtSearch.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.txtSearch, null);
            this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // lblSearchInfo
            // 
            this.lblSearchInfo.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblSearchInfo.AutoSize = true;
            this.lblSearchInfo.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblSearchInfo.Location = new System.Drawing.Point(20, 181);
            this.lblSearchInfo.Name = "lblSearchInfo";
            this.lblSearchInfo.Size = new System.Drawing.Size(165, 15);
            this.lblSearchInfo.TabIndex = 24;
            this.lblSearchInfo.Text = "ENTER SEARCH CRITERIA";
            this.ToolTip1.SetToolTip(this.lblSearchInfo, null);
            // 
            // lblPage
            // 
            this.lblPage.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblPage.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblPage.Location = new System.Drawing.Point(334, 181);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(54, 18);
            this.lblPage.TabIndex = 23;
            this.lblPage.Text = "STREET";
            this.ToolTip1.SetToolTip(this.lblPage, null);
            this.lblPage.Visible = false;
            // 
            // lblBook
            // 
            this.lblBook.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.lblBook.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.lblBook.Location = new System.Drawing.Point(136, 181);
            this.lblBook.Name = "lblBook";
            this.lblBook.Size = new System.Drawing.Size(31, 18);
            this.lblBook.TabIndex = 22;
            this.lblBook.Text = "ST #";
            this.ToolTip1.SetToolTip(this.lblBook, null);
            this.lblBook.Visible = false;
            // 
            // txtGetAccountNumber
            // 
            this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGetAccountNumber.Location = new System.Drawing.Point(236, 55);
            this.txtGetAccountNumber.Name = "txtGetAccountNumber";
            this.txtGetAccountNumber.Size = new System.Drawing.Size(170, 40);
            this.txtGetAccountNumber.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtGetAccountNumber, null);
            this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
            // 
            // cmdGetAccountNumber
            // 
            this.cmdGetAccountNumber.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.cmdGetAccountNumber.AppearanceKey = "acceptButton";
            this.cmdGetAccountNumber.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdGetAccountNumber.Location = new System.Drawing.Point(340, 30);
            this.cmdGetAccountNumber.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
            this.cmdGetAccountNumber.Size = new System.Drawing.Size(140, 48);
            this.cmdGetAccountNumber.TabIndex = 2;
            this.cmdGetAccountNumber.Text = "Get Account";
            this.ToolTip1.SetToolTip(this.cmdGetAccountNumber, null);
            this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // framSearch
            // 
            this.framSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framSearch.AppearanceKey = "groupBoxNoBorders";
            this.framSearch.Controls.Add(this.GridSearch);
            this.framSearch.Location = new System.Drawing.Point(30, 30);
            this.framSearch.Name = "framSearch";
            this.framSearch.Size = new System.Drawing.Size(917, 476);
            this.framSearch.TabIndex = 30;
            this.ToolTip1.SetToolTip(this.framSearch, null);
            this.framSearch.Visible = false;
            // 
            // GridSearch
            // 
            this.GridSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridSearch.Cols = 10;
            this.GridSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.GridSearch.ExtendLastCol = true;
            this.GridSearch.FixedCols = 0;
            this.GridSearch.Name = "GridSearch";
            this.GridSearch.RowHeadersVisible = false;
            this.GridSearch.Rows = 1;
            this.GridSearch.ShowFocusCell = false;
            this.GridSearch.Size = new System.Drawing.Size(912, 414);
            this.GridSearch.StandardTab = false;
            this.GridSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.GridSearch.TabIndex = 31;
            this.ToolTip1.SetToolTip(this.GridSearch, null);
            this.GridSearch.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridSearch_BeforeSort);
            this.GridSearch.DoubleClick += new System.EventHandler(this.GridSearch_DblClick);
            // 
            // Label2
            // 
            this.Label2.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.Label2.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.Label2.Location = new System.Drawing.Point(30, 69);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(200, 15);
            this.Label2.TabIndex = 29;
            this.Label2.Text = "LAST ACCOUNT ACCESSED";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label1
            // 
            this.Label1.Appearance = fecherFoundation.FCLabel.AppearanceConstants.cc3D;
            this.Label1.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(450, 15);
            this.Label1.TabIndex = 28;
            this.Label1.Text = "ENTER AN ACCOUNT NUMBER OR 0 TO ADD A NEW ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddNew,
            this.mnuSepar2,
            this.mnuNewSearch,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddNew
            // 
            this.mnuAddNew.Index = 0;
            this.mnuAddNew.Name = "mnuAddNew";
            this.mnuAddNew.Text = "Add New";
            this.mnuAddNew.Click += new System.EventHandler(this.mnuAddNew_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuNewSearch
            // 
            this.mnuNewSearch.Index = 2;
            this.mnuNewSearch.Name = "mnuNewSearch";
            this.mnuNewSearch.Text = "New Search";
            this.mnuNewSearch.Visible = false;
            this.mnuNewSearch.Click += new System.EventHandler(this.mnuNewSearch_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 3;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Search";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.btnSearch.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.btnSearch.Location = new System.Drawing.Point(693, 29);
            this.btnSearch.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSearch.Size = new System.Drawing.Size(61, 24);
            this.btnSearch.Text = "Search";
            this.btnSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdAddNew
            // 
            this.cmdAddNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddNew.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdAddNew.Location = new System.Drawing.Point(856, 29);
            this.cmdAddNew.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdAddNew.Name = "cmdAddNew";
            this.cmdAddNew.Size = new System.Drawing.Size(80, 24);
            this.cmdAddNew.TabIndex = 1;
            this.cmdAddNew.Text = "Add New";
            this.cmdAddNew.Click += new System.EventHandler(this.mnuAddNew_Click);
            // 
            // cmdNewSearch
            // 
            this.cmdNewSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewSearch.DragMode = fecherFoundation.DragModeConstants.vbManual;
            this.cmdNewSearch.Location = new System.Drawing.Point(760, 29);
            this.cmdNewSearch.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.cmdNewSearch.Name = "cmdNewSearch";
            this.cmdNewSearch.Size = new System.Drawing.Size(90, 24);
            this.cmdNewSearch.TabIndex = 2;
            this.cmdNewSearch.Text = "New Search";
            this.cmdNewSearch.Visible = false;
            this.cmdNewSearch.Click += new System.EventHandler(this.mnuNewSearch_Click);
            // 
            // frmGetRecords
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BorderStyleOriginal = fecherFoundation.FCForm.BorderStyleConstants.vbSizable;
            this.ClientSize = new System.Drawing.Size(961, 675);
            this.FillColor = 0;
            this.FillStyle = fecherFoundation.FillStyleConstants.vbFSTransparent;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.MousePointer = fecherFoundation.MousePointerConstants.vbDefault;
            this.Name = "frmGetRecords";
            this.PaletteMode = fecherFoundation.FCForm.PalleteModeConstants.vbPaletteModeHalfTone;
            this.ScaleMode = fecherFoundation.ScaleModeConstants.vbTwips;
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmGetRecords_Load);
            this.Resize += new System.EventHandler(this.frmGetRecords_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetRecords_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kSearchDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framSearch)).EndInit();
            this.framSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnSearch;
		private FCButton cmdAddNew;
		private FCButton cmdNewSearch;
	}
}