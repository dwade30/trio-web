//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmSignature.
	/// </summary>
	partial class frmSignature
	{
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCPictureBox imgSignature;
		public fecherFoundation.FCLabel Shape1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSignature));
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdBrowse = new fecherFoundation.FCButton();
            this.imgSignature = new fecherFoundation.FCPictureBox();
            this.Shape1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSignature)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 268);
            this.BottomPanel.Size = new System.Drawing.Size(428, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdClear);
            this.ClientArea.Controls.Add(this.cmdBrowse);
            this.ClientArea.Controls.Add(this.imgSignature);
            this.ClientArea.Controls.Add(this.Shape1);
            this.ClientArea.Size = new System.Drawing.Size(428, 208);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(428, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(117, 30);
            this.HeaderText.Text = "Signature";
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.Location = new System.Drawing.Point(165, 166);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(84, 40);
            this.cmdClear.TabIndex = 1;
            this.cmdClear.Text = "Clear";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.AppearanceKey = "actionButton";
            this.cmdBrowse.Location = new System.Drawing.Point(30, 166);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(105, 40);
            this.cmdBrowse.TabIndex = 0;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // imgSignature
            // 
            this.imgSignature.AllowDrop = true;
            this.imgSignature.DrawStyle = ((short)(0));
            this.imgSignature.DrawWidth = ((short)(1));
            this.imgSignature.FillStyle = ((short)(1));
            this.imgSignature.FontTransparent = true;
            this.imgSignature.Image = ((System.Drawing.Image)(resources.GetObject("imgSignature.Image")));
            this.imgSignature.Location = new System.Drawing.Point(30, 30);
            this.imgSignature.Name = "imgSignature";
            this.imgSignature.Picture = ((System.Drawing.Image)(resources.GetObject("imgSignature.Picture")));
            this.imgSignature.Size = new System.Drawing.Size(374, 114);
            this.imgSignature.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgSignature.TabIndex = 2;
            // 
            // Shape1
            // 
            this.Shape1.BorderStyle = 1;
            this.Shape1.Location = new System.Drawing.Point(30, 30);
            this.Shape1.Name = "Shape1";
            this.Shape1.Size = new System.Drawing.Size(374, 114);
            this.Shape1.TabIndex = 3;
            this.Shape1.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 0;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 1;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Exit";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(171, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(79, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmSignature
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(428, 376);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSignature";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Signature";
            this.Load += new System.EventHandler(this.frmSignature_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSignature_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSignature)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSave;
    }
}