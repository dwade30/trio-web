//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmRTBDocument.
	/// </summary>
	partial class frmRTBDocument
	{
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkBulleted;
		public fecherFoundation.FCComboBox cmbIndent;
		public fecherFoundation.FCComboBox cmbAlignment;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCCheckBox chkUnderline;
		public fecherFoundation.FCCheckBox chkItalic;
		public fecherFoundation.FCCheckBox chkBold;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbFontColor;
		public fecherFoundation.FCComboBox cmbFontSize;
		public fecherFoundation.FCComboBox cmbFont;
		public fecherFoundation.FCRichTextBox rtb;
		public fecherFoundation.FCLabel Label4;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuAttach;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkBulleted = new fecherFoundation.FCCheckBox();
            this.cmbIndent = new fecherFoundation.FCComboBox();
            this.cmbAlignment = new fecherFoundation.FCComboBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.chkUnderline = new fecherFoundation.FCCheckBox();
            this.chkItalic = new fecherFoundation.FCCheckBox();
            this.chkBold = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmbFontColor = new fecherFoundation.FCComboBox();
            this.cmbFontSize = new fecherFoundation.FCComboBox();
            this.cmbFont = new fecherFoundation.FCComboBox();
            this.rtb = new fecherFoundation.FCRichTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAttach = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.cmdAttach = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBulleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnderline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkItalic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAttach)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 707);
            this.BottomPanel.Size = new System.Drawing.Size(818, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.chkUnderline);
            this.ClientArea.Controls.Add(this.chkItalic);
            this.ClientArea.Controls.Add(this.chkBold);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.rtb);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Size = new System.Drawing.Size(838, 606);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.rtb, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkBold, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkItalic, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkUnderline, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame2, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAttach);
            this.TopPanel.Size = new System.Drawing.Size(838, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAttach, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(124, 30);
            this.HeaderText.Text = "Document";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(175, 261);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(172, 40);
            this.txtDescription.TabIndex = 15;
            this.txtDescription.TabStop = false;
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkBulleted);
            this.Frame2.Controls.Add(this.cmbIndent);
            this.Frame2.Controls.Add(this.cmbAlignment);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Controls.Add(this.Label2);
            this.Frame2.Controls.Add(this.Label1);
            this.Frame2.Location = new System.Drawing.Point(509, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(278, 196);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Paragraph";
            // 
            // chkBulleted
            // 
            this.chkBulleted.Location = new System.Drawing.Point(109, 30);
            this.chkBulleted.Name = "chkBulleted";
            this.chkBulleted.Size = new System.Drawing.Size(22, 23);
            this.chkBulleted.TabIndex = 11;
            this.chkBulleted.TabStop = false;
            this.chkBulleted.CheckedChanged += new System.EventHandler(this.chkBulleted_CheckedChanged);
            // 
            // cmbIndent
            // 
            this.cmbIndent.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIndent.Location = new System.Drawing.Point(109, 136);
            this.cmbIndent.Name = "cmbIndent";
            this.cmbIndent.Size = new System.Drawing.Size(149, 40);
            this.cmbIndent.TabIndex = 10;
            this.cmbIndent.TabStop = false;
            this.cmbIndent.SelectedIndexChanged += new System.EventHandler(this.cmbIndent_SelectedIndexChanged);
            // 
            // cmbAlignment
            // 
            this.cmbAlignment.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAlignment.Location = new System.Drawing.Point(109, 76);
            this.cmbAlignment.Name = "cmbAlignment";
            this.cmbAlignment.Size = new System.Drawing.Size(149, 40);
            this.cmbAlignment.TabIndex = 9;
            this.cmbAlignment.TabStop = false;
            this.cmbAlignment.SelectedIndexChanged += new System.EventHandler(this.cmbAlignment_SelectedIndexChanged);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(20, 37);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(79, 17);
            this.Label3.TabIndex = 14;
            this.Label3.Text = "BULLETED";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 150);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(55, 15);
            this.Label2.TabIndex = 13;
            this.Label2.Text = "INDENT";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(20, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(87, 17);
            this.Label1.TabIndex = 12;
            this.Label1.Text = "ALIGNMENT";
            // 
            // chkUnderline
            // 
            this.chkUnderline.Location = new System.Drawing.Point(32, 214);
            this.chkUnderline.Name = "chkUnderline";
            this.chkUnderline.Size = new System.Drawing.Size(88, 24);
            this.chkUnderline.TabIndex = 7;
            this.chkUnderline.TabStop = false;
            this.chkUnderline.Text = "Underline";
            this.chkUnderline.CheckedChanged += new System.EventHandler(this.chkUnderline_CheckedChanged);
            // 
            // chkItalic
            // 
            this.chkItalic.Location = new System.Drawing.Point(32, 177);
            this.chkItalic.Name = "chkItalic";
            this.chkItalic.Size = new System.Drawing.Size(56, 24);
            this.chkItalic.TabIndex = 6;
            this.chkItalic.TabStop = false;
            this.chkItalic.Text = "Italic";
            this.chkItalic.CheckedChanged += new System.EventHandler(this.chkItalic_CheckedChanged);
            // 
            // chkBold
            // 
            this.chkBold.Location = new System.Drawing.Point(30, 140);
            this.chkBold.Name = "chkBold";
            this.chkBold.Size = new System.Drawing.Size(56, 24);
            this.chkBold.TabIndex = 5;
            this.chkBold.TabStop = false;
            this.chkBold.Text = "Bold";
            this.chkBold.CheckedChanged += new System.EventHandler(this.chkBold_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmbFontColor);
            this.Frame1.Controls.Add(this.cmbFontSize);
            this.Frame1.Controls.Add(this.cmbFont);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(449, 90);
            this.Frame1.TabIndex = 1;
            this.Frame1.Text = "Font";
            // 
            // cmbFontColor
            // 
            this.cmbFontColor.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFontColor.Location = new System.Drawing.Point(281, 30);
            this.cmbFontColor.Name = "cmbFontColor";
            this.cmbFontColor.Size = new System.Drawing.Size(148, 40);
            this.cmbFontColor.TabIndex = 4;
            this.cmbFontColor.TabStop = false;
            this.cmbFontColor.SelectedIndexChanged += new System.EventHandler(this.cmbFontColor_SelectedIndexChanged);
            // 
            // cmbFontSize
            // 
            this.cmbFontSize.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFontSize.Location = new System.Drawing.Point(178, 30);
            this.cmbFontSize.Name = "cmbFontSize";
            this.cmbFontSize.Size = new System.Drawing.Size(93, 40);
            this.cmbFontSize.TabIndex = 3;
            this.cmbFontSize.TabStop = false;
            this.cmbFontSize.SelectedIndexChanged += new System.EventHandler(this.cmbFontSize_SelectedIndexChanged);
            // 
            // cmbFont
            // 
            this.cmbFont.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFont.Location = new System.Drawing.Point(20, 30);
            this.cmbFont.Name = "cmbFont";
            this.cmbFont.Size = new System.Drawing.Size(148, 40);
            this.cmbFont.Sorted = true;
            this.cmbFont.TabIndex = 2;
            this.cmbFont.TabStop = false;
            this.cmbFont.SelectedIndexChanged += new System.EventHandler(this.cmbFont_SelectedIndexChanged);
            // 
            // rtb
            // 
            this.rtb.Location = new System.Drawing.Point(30, 321);
            this.rtb.Name = "rtb";
            this.rtb.Size = new System.Drawing.Size(757, 386);
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(30, 275);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(99, 17);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "DESCRIPTION";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintPreview,
            this.mnuAttach,
            this.Seperator,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 0;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintPreview.Text = "Print Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuAttach
            // 
            this.mnuAttach.Index = 1;
            this.mnuAttach.Name = "mnuAttach";
            this.mnuAttach.Text = "Preview and Attach Document";
            this.mnuAttach.Click += new System.EventHandler(this.mnuAttach_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 3;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(334, 30);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(143, 48);
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // cmdAttach
            // 
            this.cmdAttach.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAttach.Location = new System.Drawing.Point(607, 29);
            this.cmdAttach.Name = "cmdAttach";
            this.cmdAttach.Size = new System.Drawing.Size(203, 24);
            this.cmdAttach.TabIndex = 1;
            this.cmdAttach.Text = "Preview and Attach Document";
            this.cmdAttach.Click += new System.EventHandler(this.mnuAttach_Click);
            // 
            // frmRTBDocument
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(838, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmRTBDocument";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Document";
            this.Load += new System.EventHandler(this.frmRTBDocument_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRTBDocument_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkBulleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUnderline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkItalic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rtb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAttach)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdPrintPreview;
        private FCButton cmdAttach;
    }
}