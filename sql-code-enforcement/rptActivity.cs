﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptActivity.
	/// </summary>
	public partial class rptActivity : BaseSectionReport
	{
		public rptActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Activity";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptActivity InstancePtr
		{
			get
			{
				return (rptActivity)Sys.GetInstance(typeof(rptActivity));
			}
		}

		protected rptActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptActivity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsActivityList alList = new clsActivityList();
		private bool boolDetail;
		// vbPorter upgrade warning: intModal As int	OnWrite(FCForm.FormShowEnum)
		public void Init(int lngID, int lngType, bool boolShowDetail, string strTitle, int lngRefID = 0, int lngRefType = 0, int intModal = 0, clsActivity tAct = null)
		{
			int lngReturn = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				lblParent.Text = strTitle;
				boolDetail = boolShowDetail;
				if (boolDetail)
				{
					txtDetail.Visible = true;
				}
				else
				{
					txtDetail.Visible = false;
				}
				if (tAct == null)
				{
					lngReturn = alList.LoadList(0, lngType, lngID, lngRefType);
				}
				else
				{
					// Dim tAct As New clsActivity
					// lngReturn = tAct.LoadActivity(lngActivityID)
					lngReturn = 0;
					if (lngReturn == 0)
					{
						alList.Clear();
						alList.AddActivity(tAct.ID, tAct.Account, tAct.ParentID, tAct.ParentType, tAct.ActivityDate, tAct.ActivityType, tAct.Description, tAct.Detail, tAct.ReferenceID, tAct.ReferenceType);
					}
				}
				if (lngReturn == 0)
				{
					alList.MoveFirst();
                    //FC:FINAL:MSH - i.issue #1716: restore functionality for showing report as modal window (as in original)
                    //frmReportViewer.InstancePtr.Init(this, "", intModal, strAttachmentName: "Activity");
                    if (intModal == 0)
                    {
                        frmReportViewer.InstancePtr.Init(this, "", intModal, strAttachmentName: "Activity", showModal: false);
                    }
                    else
                    {
                        frmReportViewer.InstancePtr.Init(this, "", intModal, strAttachmentName: "Activity", showModal: true);
                    }
                }
				else
				{
					fecherFoundation.Information.Err().Raise(lngReturn, null, null, null, null);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = alList.GetCurrentIndex() < 1;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsActivity tAct;
			tAct = alList.GetCurrentActivity();
			if (!(tAct == null))
			{
				txtActivityDate.Text = Strings.Format(tAct.ActivityDate, "MM/dd/yyyy");
				txtDescription.Text = tAct.Description;
				txtType.Text = tAct.ActivityType;
				if (boolDetail)
				{
					txtDetail.Text = tAct.Detail;
				}
			}
			alList.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
