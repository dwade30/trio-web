﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class modCE
	{
		//=========================================================
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public static object GetCERegistryKey(string KeyName)
		{
			object GetCERegistryKey = null;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modCEConstants.CEREGISTRYKEY, KeyName, ref modReplaceWorkFiles.Statics.gstrReturn);
				if (modReplaceWorkFiles.Statics.gstrReturn == string.Empty)
				{
					modReplaceWorkFiles.Statics.gstrReturn = string.Empty;
				}
				else
				{
					GetCERegistryKey = modReplaceWorkFiles.Statics.gstrReturn;
				}
				return GetCERegistryKey;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "GetCERegistryKey", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCERegistryKey;
		}
		// vbPorter upgrade warning: KeyValue As string	OnWrite(int, double, string)
		public static bool SaveCERegistryKey(string KeyName, string KeyValue)
		{
			bool SaveCERegistryKey = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveCERegistryKey = modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modCEConstants.CEREGISTRYKEY, KeyName, KeyValue);
				return SaveCERegistryKey;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "SaveCERegistryKey", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCERegistryKey;
		}

		public static string GetMasterJoin(bool boolNameOnly = false)
		{
			string GetMasterJoin = "";
			string strReturn = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strFullDBName;
			strFullDBName = rsTemp.Get_GetFullDBName("CentralParties");
			if (!boolNameOnly)
			{
				strReturn = "SELECT *, FullNameLF as Name,FirstName as First, MiddleName as Middle, LastName as last, Designation as Desig, PartyState as State, '' as Zip4 FROM ceMaster CROSS APPLY " + strFullDBName + ".dbo.GetCentralPartyNameAndAddress(PartyID,NULL,'CE',NULL) ";
			}
			else
			{
				strReturn = "SELECT *, FullNameLF as Name FROM ppMaster CROSS APPLY " + strFullDBName + ".dbo.GetCentralPartyName(PartyID) ";
			}
			GetMasterJoin = strReturn;
			return GetMasterJoin;
		}

		public static string GetMasterJoinForJoin(bool boolNameOnly = false)
		{
			string GetMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetMasterJoin(boolNameOnly) + ") mj ";
			GetMasterJoinForJoin = strReturn;
			return GetMasterJoinForJoin;
		}

		public static string GetCEMasterJoin()
		{
			string GetCEMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
			string strReturn;
			strReturn = "select cemaster.*, cpo.FullNameLF as Name,cpso.FullNameLF as SecOwner,cpo.firstname as First, cpo.MiddleName as Middle, cpo.lastname as Last,cpo.designation as desig,cpso.firstname as SecFirst,cpso.MiddleName as SecMiddle,cpso.LastName as SecLast,cpso.designation as SecDesig,cpo.address1 as address1,cpo.address2 as address2,cpo.city as city, cpo.state as state,cpo.zip as zip, '' as zip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("CodeEnforcement");
			strReturn += strTemp + ".dbo.cemaster left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (cemaster.owner1id = cpo.PartyID) left join ";
			strReturn += strTemp + " as cpso on (cemaster.owner2ID = cpso.PartyID)";
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetCEMasterJoin = strReturn;
			return GetCEMasterJoin;
		}

		public static string GetCEMasterJoinForJoin()
		{
			string GetCEMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetCEMasterJoin() + ") mj ";
			GetCEMasterJoinForJoin = strReturn;
			return GetCEMasterJoinForJoin;
		}

		public static string GetREMasterJoin()
		{
			string GetREMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
			string strReturn;
			strReturn = "select master.*, cpo.FullNameLF as RSName,cpso.FullNameLF as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.PartyID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.PartyID)";
			strReturn = " select * from (" + strReturn + ") REmparty ";
			GetREMasterJoin = strReturn;
			return GetREMasterJoin;
		}

		public static string GetREMasterJoinForJoin()
		{
			string GetREMasterJoinForJoin = "";
			string strReturn;
			strReturn = "(" + GetREMasterJoin() + ") REmj ";
			GetREMasterJoinForJoin = strReturn;
			return GetREMasterJoinForJoin;
		}
	}
}
