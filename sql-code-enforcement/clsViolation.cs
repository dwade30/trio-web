//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsViolation
	{
		//=========================================================
		private int lngAutoID;
		private int lngType;
		private int lngAccount;
		private int lngStatus;
		// vbPorter upgrade warning: dtOccurred As DateTime	OnWrite(DateTime, int, string)
		private DateTime dtOccurred;
		// vbPorter upgrade warning: dtIssued As DateTime	OnWrite(DateTime, int, string)
		private DateTime dtIssued;
		// vbPorter upgrade warning: dtResolved As DateTime	OnWrite(DateTime, int, string)
		private DateTime dtResolved;
		private string strDescription = string.Empty;
		private string strDetail = string.Empty;
		private double dblFee;
		// vbPorter upgrade warning: strIdentifier As string	OnWrite(string, double)
		private string strIdentifier = string.Empty;
		private clsPhoneList PhoneList = new clsPhoneList();
		private int lngPermit;
		private string strFirst = string.Empty;
		private string strMiddle = string.Empty;
		private string strLast = string.Empty;
		private string strDesig = string.Empty;
		private string strDBA = string.Empty;
		private string strAddress1 = string.Empty;
		private string strAddress2 = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZip4 = string.Empty;
		private int lngStreetNumber;
		private string strApt = string.Empty;
		private string strStreet = string.Empty;
		private string strBusinessLicense = string.Empty;
		private string strName = string.Empty;
		private int lnginspection;
		private FCCollection vStatus = new FCCollection();
		private string strContactEMail = string.Empty;
		private string strContactName = string.Empty;

		public string ContactEmail
		{
			set
			{
				strContactEMail = value;
			}
			get
			{
				string ContactEmail = "";
				ContactEmail = strContactEMail;
				return ContactEmail;
			}
		}

		public string ContactName
		{
			set
			{
				strContactName = value;
			}
			get
			{
				string ContactName = "";
				ContactName = strContactName;
				return ContactName;
			}
		}

		public string Get_StatusLabel(int lngValue)
		{
			string StatusLabel = "";
			string strReturn;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (Statics.clStatus.Count < 1)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONSTATUS), modGlobalVariables.Statics.strCEDatabase);
					while (!rsLoad.EndOfFile())
					{
						Statics.clStatus.Add(rsLoad.Get_Fields_String("description"), "ID" + rsLoad.Get_Fields_Int32("ID"));
						rsLoad.MoveNext();
					}
					Statics.clStatus.Add("Closed", "ID" + FCConvert.ToString(modCEConstants.CNSTVIOLATIONSTATUSCLOSED));
					Statics.clStatus.Add("Closed", "ID" + FCConvert.ToString(modCEConstants.CNSTVIOLATIONSTATUSRESOLVED));
				}
				strReturn = Statics.clStatus["ID" + FCConvert.ToString(lngValue)].ToString();
				StatusLabel = strReturn;
				return StatusLabel;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				StatusLabel = "";
			}
			return StatusLabel;
		}

		public string Get_TypeLabel(int lngValue)
		{
			string TypeLabel = "";
			string strReturn;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (Statics.clType.Count < 1)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE), modGlobalVariables.Statics.strCEDatabase);
					while (!rsLoad.EndOfFile())
					{
						Statics.clType.Add(rsLoad.Get_Fields_String("description"), "ID" + rsLoad.Get_Fields_Int32("ID"));
						rsLoad.MoveNext();
					}
					Statics.clType.Add("", "ID0");
				}
				strReturn = Statics.clType["ID" + FCConvert.ToString(lngValue)].ToString();
				TypeLabel = strReturn;
				return TypeLabel;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				TypeLabel = "";
			}
			return TypeLabel;
		}

		public int Inspection
		{
			set
			{
				lnginspection = value;
			}
			get
			{
				int Inspection = 0;
				Inspection = lnginspection;
				return Inspection;
			}
		}

		public string SearchName
		{
			set
			{
				strName = value;
			}
			get
			{
				string SearchName = "";
				SearchName = strName;
				return SearchName;
			}
		}

		public int Permit
		{
			set
			{
				lngPermit = value;
			}
			get
			{
				int Permit = 0;
				Permit = lngPermit;
				return Permit;
			}
		}

		public string FirstName
		{
			set
			{
				strFirst = value;
				ReFigureNameString();
			}
			get
			{
				string FirstName = "";
				FirstName = strFirst;
				return FirstName;
			}
		}

		public string MiddleName
		{
			set
			{
				strMiddle = value;
				ReFigureNameString();
			}
			get
			{
				string MiddleName = "";
				MiddleName = strMiddle;
				return MiddleName;
			}
		}

		public string LastName
		{
			set
			{
				strLast = value;
				ReFigureNameString();
			}
			get
			{
				string LastName = "";
				LastName = strLast;
				return LastName;
			}
		}

		public string Desig
		{
			set
			{
				strDesig = value;
				ReFigureNameString();
			}
			get
			{
				string Desig = "";
				Desig = strDesig;
				return Desig;
			}
		}

		public string DBA
		{
			set
			{
				strDBA = value;
			}
			get
			{
				string DBA = "";
				DBA = strDBA;
				return DBA;
			}
		}

		public string Address1
		{
			set
			{
				strAddress1 = value;
			}
			get
			{
				string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}

		public string Address2
		{
			set
			{
				strAddress2 = value;
			}
			get
			{
				string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string Zip4
		{
			set
			{
				strZip4 = value;
			}
			get
			{
				string Zip4 = "";
				Zip4 = strZip4;
				return Zip4;
			}
		}

		public int StreetNumber
		{
			set
			{
				lngStreetNumber = value;
			}
			get
			{
				int StreetNumber = 0;
				StreetNumber = lngStreetNumber;
				return StreetNumber;
			}
		}

		public string Apt
		{
			set
			{
				strApt = value;
			}
			get
			{
				string Apt = "";
				Apt = strApt;
				return Apt;
			}
		}

		public string Street
		{
			set
			{
				strStreet = value;
			}
			get
			{
				string Street = "";
				Street = strStreet;
				return Street;
			}
		}

		public string BusinessLicense
		{
			set
			{
				strBusinessLicense = value;
			}
			get
			{
				string BusinessLicense = "";
				BusinessLicense = strBusinessLicense;
				return BusinessLicense;
			}
		}

		public string Identifier
		{
			set
			{
				strIdentifier = value;
			}
			get
			{
				string Identifier = "";
				Identifier = strIdentifier;
				return Identifier;
			}
		}

		public int ID
		{
			set
			{
				lngAutoID = value;
				PhoneList.ParentID = lngAutoID;
			}
			get
			{
				int ID = 0;
				ID = lngAutoID;
				return ID;
			}
		}

		public int ViolationType
		{
			set
			{
				lngType = value;
			}
			get
			{
				int ViolationType = 0;
				ViolationType = lngType;
				return ViolationType;
			}
		}

		public int Account
		{
			set
			{
				lngAccount = value;
			}
			get
			{
				int Account = 0;
				Account = lngAccount;
				return Account;
			}
		}

		public int Status
		{
			set
			{
				lngStatus = value;
			}
			get
			{
				int Status = 0;
				Status = lngStatus;
				return Status;
			}
		}

		public DateTime DateOccurred
		{
			set
			{
				dtOccurred = value;
			}
			get
			{
				DateTime DateOccurred = System.DateTime.Now;
				DateOccurred = dtOccurred;
				return DateOccurred;
			}
		}

		public DateTime DateIssued
		{
			set
			{
				dtIssued = value;
			}
			get
			{
				DateTime DateIssued = System.DateTime.Now;
				DateIssued = dtIssued;
				return DateIssued;
			}
		}

		public DateTime DateResolved
		{
			set
			{
				dtResolved = value;
			}
			get
			{
				DateTime DateResolved = System.DateTime.Now;
				DateResolved = dtResolved;
				return DateResolved;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Detail
		{
			set
			{
				strDetail = value;
			}
			get
			{
				string Detail = "";
				Detail = strDetail;
				return Detail;
			}
		}

		public double Fee
		{
			set
			{
				dblFee = value;
			}
			get
			{
				double Fee = 0;
				Fee = dblFee;
				return Fee;
			}
		}

		public void Clear()
		{
			strDescription = "";
			strDetail = "";
			strContactEMail = "";
			strContactName = "";
			dtResolved = DateTime.FromOADate(0);
			dtIssued = DateTime.FromOADate(0);
			dtOccurred = DateTime.FromOADate(0);
			dblFee = 0;
			lngStatus = 0;
			lngType = 0;
			lngAccount = 0;
			lngAutoID = 0;
			PhoneList.Clear();
			strAddress1 = "";
			strAddress2 = "";
			strApt = "";
			strBusinessLicense = "";
			strCity = "";
			strState = "";
			strZip = "";
			strZip4 = "";
			strDBA = "";
			strStreet = "";
			lngPermit = 0;
			lngStreetNumber = 0;
			strFirst = "";
			strMiddle = "";
			strLast = "";
			strDesig = "";
			strIdentifier = "";
			strName = "";
			lnginspection = 0;
		}

		public int SaveViolation()
		{
			int SaveViolation = 0;
			bool boolLocked = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveViolation = 0;
				clsDRWrapper rsSave = new clsDRWrapper();
				boolLocked = false;
				rsSave.OpenRecordset("select * from violations where id = " + FCConvert.ToString(lngAutoID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					if (modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers)
					{
						string strTemp = "";
						if (modMain.AttemptLock(ref strTemp, modCEConstants.CNSTVIOLATIONLOCK))
						{
							clsDRWrapper rsTemp = new clsDRWrapper();
							string strSQL = "";
							boolLocked = true;
							if (modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberDashYear) || modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberOnly))
							{
								strSQL = "select max(val(violationidentifier & '')) as maxid from violations ";
							}
							else
							{
								strSQL = "select max(val(mid(violationidentifier & '      ',6))) as maxid from violations ";
							}
							if (modGlobalVariables.Statics.CECustom.PermitIdentifierValidation == FCConvert.ToInt32(clsCustomize.IdentifierValidation.TypeUnique))
							{
								strSQL += " where violationtype = " + FCConvert.ToString(lngType);
							}
							rsTemp.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
							if (modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberDashYear))
							{
								strIdentifier = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("maxid")) + 1) + "-" + FCConvert.ToString(DateTime.Today.Year);
							}
							else if (modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberOnly))
							{
								strIdentifier = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("maxid")) + 1);
							}
							else if (modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.yeardashnumber))
							{
								strIdentifier = FCConvert.ToString(DateTime.Today.Year) + "-" + FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("maxid")) + 1);
							}
							rsSave.Set_Fields("violationtype", lngType);
							rsSave.Set_Fields("violationidentifier", strIdentifier);
							rsSave.Update();
							lngAutoID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
							modMain.ReleaseLock(modCEConstants.CNSTVIOLATIONLOCK);
							rsSave.OpenRecordset("select * from violations where id = " + FCConvert.ToString(lngAutoID), modGlobalVariables.Statics.strCEDatabase);
							rsSave.Edit();
						}
						else
						{
						}
					}
				}
				rsSave.Set_Fields("violationtype", lngType);
				rsSave.Set_Fields("violationaccount", lngAccount);
				rsSave.Set_Fields("violationstatus", lngStatus);
				if (dtOccurred.ToOADate() != 0)
				{
					rsSave.Set_Fields("dateviolationoccurred", dtOccurred);
				}
				else
				{
					rsSave.Set_Fields("dateviolationoccurred", 0);
				}
				if (dtIssued.ToOADate() != 0)
				{
					rsSave.Set_Fields("dateviolationissued", dtIssued);
				}
				else
				{
					rsSave.Set_Fields("dateviolationissued", 0);
				}
				if (dtResolved.ToOADate() != 0)
				{
					rsSave.Set_Fields("dateviolationresolved", dtResolved);
				}
				else
				{
					rsSave.Set_Fields("dateviolationresolved", 0);
				}
				rsSave.Set_Fields("violationdescription", strDescription);
				rsSave.Set_Fields("violationdetail", strDetail);
				rsSave.Set_Fields("violationfee", dblFee);
				rsSave.Set_Fields("violationIdentifier", strIdentifier);
				rsSave.Set_Fields("violationpermit", lngPermit);
				rsSave.Set_Fields("violationfirstname", strFirst);
				rsSave.Set_Fields("violationmiddlename", strMiddle);
				rsSave.Set_Fields("violationlastname", strLast);
				rsSave.Set_Fields("violationdesig", strDesig);
				rsSave.Set_Fields("violationname", strName);
				rsSave.Set_Fields("violationdba", strDBA);
				rsSave.Set_Fields("violationaddress1", strAddress1);
				rsSave.Set_Fields("violationaddress2", strAddress2);
				rsSave.Set_Fields("violationcity", strCity);
				rsSave.Set_Fields("violationstate", strState);
				rsSave.Set_Fields("violationzip", strZip);
				rsSave.Set_Fields("violationzip4", strZip4);
				rsSave.Set_Fields("violationstreetnumber", lngStreetNumber);
				rsSave.Set_Fields("violationapt", strApt);
				rsSave.Set_Fields("violationstreet", strStreet);
				rsSave.Set_Fields("violationbusinesslicense", strBusinessLicense);
				rsSave.Set_Fields("violationinspection", lnginspection);
				rsSave.Set_Fields("ViolationContactName", strContactName);
				rsSave.Set_Fields("ViolationContactEMail", strContactEMail);
				rsSave.Update();
				SavePhoneNumbers();
				return SaveViolation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolLocked)
				{
					modMain.ReleaseLock(modCEConstants.CNSTVIOLATIONLOCK);
					boolLocked = false;
				}
				SaveViolation = fecherFoundation.Information.Err(ex).Number;
			}
			return SaveViolation;
		}

		public int DeleteViolation()
		{
			int DeleteViolation = 0;
			DeleteViolation = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("delete from activitylog where parentid = " + FCConvert.ToString(lngAutoID) + " and  parenttype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATION), modGlobalVariables.Statics.strCEDatabase);
				rsSave.Execute("delete from violations where id = " + FCConvert.ToString(lngAutoID), modGlobalVariables.Statics.strCEDatabase);
				Clear();
				return DeleteViolation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				DeleteViolation = fecherFoundation.Information.Err(ex).Number;
			}
			return DeleteViolation;
		}

		public int LoadViolation(int lngID = 0)
		{
			int LoadViolation = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadViolation = 0;
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngLoadID = 0;
				if (lngID == 0)
				{
					lngLoadID = lngAutoID;
				}
				else
				{
					lngLoadID = lngID;
				}
				rsLoad.OpenRecordset("select * from violations where id = " + FCConvert.ToString(lngLoadID), modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("id"))));
					strDescription = FCConvert.ToString(rsLoad.Get_Fields_String("violationdescription"));
					strDetail = FCConvert.ToString(rsLoad.Get_Fields_String("violationdetail"));
					if (Information.IsDate(rsLoad.Get_Fields("dateviolationresolved")))
					{
						if (Convert.ToDateTime(rsLoad.Get_Fields_DateTime("dateviolationresolved")).ToOADate() != 0)
						{
							dtResolved = FCConvert.ToDateTime(Strings.Format(rsLoad.Get_Fields_DateTime("dateviolationresolved"), "MM/dd/yyyy"));
						}
						else
						{
							dtResolved = DateTime.FromOADate(0);
						}
					}
					else
					{
						dtResolved = DateTime.FromOADate(0);
					}
					if (Information.IsDate(rsLoad.Get_Fields("dateviolationissued")))
					{
						if (Convert.ToDateTime(rsLoad.Get_Fields_DateTime("dateviolationissued")).ToOADate() != 0)
						{
							dtIssued = FCConvert.ToDateTime(Strings.Format(rsLoad.Get_Fields_DateTime("dateviolationissued"), "MM/dd/yyyy"));
						}
						else
						{
							dtIssued = DateTime.FromOADate(0);
						}
					}
					else
					{
						dtIssued = DateTime.FromOADate(0);
					}
					if (Information.IsDate(rsLoad.Get_Fields("dateviolationoccurred")))
					{
						if (Convert.ToDateTime(rsLoad.Get_Fields_DateTime("dateviolationoccurred")).ToOADate() != 0)
						{
							dtOccurred = FCConvert.ToDateTime(Strings.Format(rsLoad.Get_Fields_DateTime("dateviolationoccurred"), "MM/dd/yyyy"));
						}
						else
						{
							dtOccurred = DateTime.FromOADate(0);
						}
					}
					else
					{
						dtOccurred = DateTime.FromOADate(0);
					}
					dblFee = Conversion.Val(rsLoad.Get_Fields_Double("violationfee"));
					lngStatus = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus"))));
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("violationaccount"))));
					lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("violationtype"))));
					strIdentifier = FCConvert.ToString(rsLoad.Get_Fields_String("violationidentifier"));
					lngPermit = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("violationpermit"))));
					strFirst = FCConvert.ToString(rsLoad.Get_Fields_String("violationfirstname"));
					strMiddle = FCConvert.ToString(rsLoad.Get_Fields_String("violationmiddlename"));
					strLast = FCConvert.ToString(rsLoad.Get_Fields_String("violationlastname"));
					strDesig = FCConvert.ToString(rsLoad.Get_Fields_String("violationdesig"));
					strDBA = FCConvert.ToString(rsLoad.Get_Fields_String("violationdba"));
					strAddress1 = FCConvert.ToString(rsLoad.Get_Fields_String("violationaddress1"));
					strAddress2 = FCConvert.ToString(rsLoad.Get_Fields_String("violationaddress2"));
					strCity = FCConvert.ToString(rsLoad.Get_Fields_String("violationcity"));
					strState = FCConvert.ToString(rsLoad.Get_Fields_String("violationstate"));
					strZip = FCConvert.ToString(rsLoad.Get_Fields_String("violationzip"));
					strZip4 = FCConvert.ToString(rsLoad.Get_Fields_String("violationzip4"));
					lngStreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_String("violationstreetnumber"))));
					strApt = FCConvert.ToString(rsLoad.Get_Fields_String("violationapt"));
					strStreet = FCConvert.ToString(rsLoad.Get_Fields_String("violationstreet"));
					strBusinessLicense = FCConvert.ToString(rsLoad.Get_Fields_String("violationbusinesslicense"));
					lnginspection = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ViolationInspection"))));
					strContactEMail = FCConvert.ToString(rsLoad.Get_Fields_String("ViolationContactEMail"));
					strContactName = FCConvert.ToString(rsLoad.Get_Fields_String("ViolationContactName"));
					LoadPhoneNumbers();
				}
				else
				{
					int lngAcct = 0;
					int lngPerm = 0;
					int lngInsp = 0;
					lngInsp = lnginspection;
					lngAcct = lngAccount;
					lngPerm = lngPermit;
					Clear();
					lngAccount = lngAcct;
					lngPermit = lngPerm;
					lnginspection = lngInsp;
					if (lngAccount > 0)
					{
						rsLoad.OpenRecordset("select * from cemaster where ceaccount = " + FCConvert.ToString(lngAccount), modGlobalVariables.Statics.strCEDatabase);
						if (!rsLoad.EndOfFile())
						{
							strFirst = FCConvert.ToString(rsLoad.Get_Fields("First"));
							strMiddle = FCConvert.ToString(rsLoad.Get_Fields("middle"));
							strLast = FCConvert.ToString(rsLoad.Get_Fields("last"));
							strDesig = FCConvert.ToString(rsLoad.Get_Fields_String("desig"));
							strCity = FCConvert.ToString(rsLoad.Get_Fields_String("city"));
							strState = FCConvert.ToString(rsLoad.Get_Fields("state"));
							strZip = FCConvert.ToString(rsLoad.Get_Fields_String("zip"));
							strZip4 = FCConvert.ToString(rsLoad.Get_Fields_String("zip4"));
							lngStreetNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("streetnumber"))));
							strStreet = FCConvert.ToString(rsLoad.Get_Fields_String("streetname"));
							strApt = FCConvert.ToString(rsLoad.Get_Fields_String("apt"));
							strDBA = FCConvert.ToString(rsLoad.Get_Fields_String("doingbusinessas"));
							strBusinessLicense = FCConvert.ToString(rsLoad.Get_Fields_String("businesslicense"));
						}
						else
						{
							lngAccount = 0;
							lngPermit = 0;
							lnginspection = 0;
						}
					}
				}
				ReFigureNameString();
				return LoadViolation;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				LoadViolation = fecherFoundation.Information.Err(ex).Number;
			}
			return LoadViolation;
		}

		public void LoadPhoneNumbers()
		{
			PhoneList.PhoneCode = modCEConstants.CNSTPHONETYPEVIOLATIONCONTACT;
			PhoneList.ParentID = lngAutoID;
			PhoneList.LoadNumbers();
		}

		public bool SavePhoneNumbers()
		{
			bool SavePhoneNumbers = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SavePhoneNumbers = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				int intIndex;
				if (!(PhoneList == null))
				{
					PhoneList.PhoneCode = modCEConstants.CNSTPHONETYPEVIOLATIONCONTACT;
					PhoneList.ParentID = lngAutoID;
					PhoneList.SaveNumbers();
				}
				SavePhoneNumbers = true;
				return SavePhoneNumbers;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SavePhoneNumbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SavePhoneNumbers;
		}

		public void SetPhoneNumberList(ref clsPhoneList pList)
		{
			PhoneList = null;
			PhoneList = pList;
		}

		public clsPhoneList PhoneNumberList
		{
			get
			{
				clsPhoneList PhoneNumberList = null;
				PhoneNumberList = PhoneList;
				return PhoneNumberList;
			}
		}

		public clsViolation() : base()
		{
			PhoneList.PhoneCode = modCEConstants.CNSTPHONETYPEVIOLATIONCONTACT;
		}

		private void ReFigureNameString()
		{
			if (fecherFoundation.Strings.Trim(strMiddle + strLast + strDesig) == string.Empty)
			{
				strName = strFirst;
			}
			else
			{
				if (fecherFoundation.Strings.Trim(strLast) != string.Empty)
				{
					if (fecherFoundation.Strings.Trim(strFirst + strMiddle) != string.Empty)
					{
						strName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strLast) + ", " + strFirst) + " " + strMiddle) + " " + strDesig);
					}
					else
					{
						strName = strLast;
					}
				}
				else
				{
					strName = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(strFirst) + " " + strMiddle) + " " + strDesig);
				}
			}
		}

		public class StaticVariables
		{
			public FCCollection clStatus = new FCCollection();
			public FCCollection clType = new FCCollection();
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
