//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmActivity.
	/// </summary>
	partial class frmActivity
	{
		public FCGrid gridDelete;
		public fecherFoundation.FCComboBox cmbActivityType;
		public FCGrid gridDocuments;
		public Global.T2KDateBox t2kDate;
		public fecherFoundation.FCRichTextBox rtbDetail;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAttachDoc;
		public fecherFoundation.FCToolStripMenuItem mnuRemoveDocument;
		public fecherFoundation.FCToolStripMenuItem mnuViewDocuments;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.gridDelete = new fecherFoundation.FCGrid();
            this.cmbActivityType = new fecherFoundation.FCComboBox();
            this.gridDocuments = new fecherFoundation.FCGrid();
            this.t2kDate = new Global.T2KDateBox();
            this.rtbDetail = new fecherFoundation.FCRichTextBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.txtType = new fecherFoundation.FCTextBox();
            this.lblType = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAttachDoc = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRemoveDocument = new fecherFoundation.FCToolStripMenuItem();
            this.mnuViewDocuments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdAttachDocument = new fecherFoundation.FCButton();
            this.cmdRemoveDocument = new fecherFoundation.FCButton();
            this.cmdViewDocuments = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAttachDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdViewDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 534);
            this.BottomPanel.Size = new System.Drawing.Size(947, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.gridDelete);
            this.ClientArea.Controls.Add(this.cmbActivityType);
            this.ClientArea.Controls.Add(this.gridDocuments);
            this.ClientArea.Controls.Add(this.t2kDate);
            this.ClientArea.Controls.Add(this.rtbDetail);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtType);
            this.ClientArea.Controls.Add(this.lblType);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(947, 474);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdViewDocuments);
            this.TopPanel.Controls.Add(this.cmdRemoveDocument);
            this.TopPanel.Controls.Add(this.cmdAttachDocument);
            this.TopPanel.Size = new System.Drawing.Size(947, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAttachDocument, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemoveDocument, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdViewDocuments, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(90, 30);
            this.HeaderText.Text = "Activity";
            // 
            // gridDelete
            // 
            this.gridDelete.AllowSelection = false;
            this.gridDelete.AllowUserToResizeColumns = false;
            this.gridDelete.AllowUserToResizeRows = false;
            this.gridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridDelete.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridDelete.BackColorBkg = System.Drawing.Color.Empty;
            this.gridDelete.BackColorFixed = System.Drawing.Color.Empty;
            this.gridDelete.BackColorSel = System.Drawing.Color.Empty;
            this.gridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridDelete.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDelete.ColumnHeadersHeight = 30;
            this.gridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gridDelete.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridDelete.DefaultCellStyle = dataGridViewCellStyle2;
            this.gridDelete.DragIcon = null;
            this.gridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridDelete.FixedRows = 0;
            this.gridDelete.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridDelete.FrozenCols = 0;
            this.gridDelete.GridColor = System.Drawing.Color.Empty;
            this.gridDelete.GridColorFixed = System.Drawing.Color.Empty;
            this.gridDelete.Location = new System.Drawing.Point(572, 600);
            this.gridDelete.Name = "gridDelete";
            this.gridDelete.OutlineCol = 0;
            this.gridDelete.ReadOnly = true;
            this.gridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDelete.RowHeightMin = 0;
            this.gridDelete.Rows = 0;
            this.gridDelete.ScrollTipText = null;
            this.gridDelete.ShowColumnVisibilityMenu = false;
            this.gridDelete.ShowFocusCell = false;
            this.gridDelete.Size = new System.Drawing.Size(23, 32);
            this.gridDelete.StandardTab = true;
            this.gridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridDelete.TabIndex = 12;
            this.gridDelete.Visible = false;
            // 
            // cmbActivityType
            // 
            this.cmbActivityType.AutoSize = false;
            this.cmbActivityType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbActivityType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbActivityType.FormattingEnabled = true;
            this.cmbActivityType.Location = new System.Drawing.Point(146, 30);
            this.cmbActivityType.Name = "cmbActivityType";
            this.cmbActivityType.Size = new System.Drawing.Size(165, 40);
            this.cmbActivityType.TabIndex = 10;
            this.cmbActivityType.Text = "Combo1";
            this.cmbActivityType.SelectedIndexChanged += new System.EventHandler(this.cmbActivityType_SelectedIndexChanged);
            // 
            // gridDocuments
            // 
            this.gridDocuments.AllowSelection = false;
            this.gridDocuments.AllowUserToResizeColumns = false;
            this.gridDocuments.AllowUserToResizeRows = false;
            this.gridDocuments.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.gridDocuments.BackColorAlternate = System.Drawing.Color.Empty;
            this.gridDocuments.BackColorBkg = System.Drawing.Color.Empty;
            this.gridDocuments.BackColorFixed = System.Drawing.Color.Empty;
            this.gridDocuments.BackColorSel = System.Drawing.Color.Empty;
            this.gridDocuments.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.gridDocuments.Cols = 3;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.gridDocuments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridDocuments.ColumnHeadersHeight = 30;
            this.gridDocuments.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.gridDocuments.DefaultCellStyle = dataGridViewCellStyle4;
            this.gridDocuments.DragIcon = null;
            this.gridDocuments.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.gridDocuments.ExtendLastCol = true;
            this.gridDocuments.FixedCols = 0;
            this.gridDocuments.ForeColorFixed = System.Drawing.Color.Empty;
            this.gridDocuments.FrozenCols = 0;
            this.gridDocuments.GridColor = System.Drawing.Color.Empty;
            this.gridDocuments.GridColorFixed = System.Drawing.Color.Empty;
            this.gridDocuments.Location = new System.Drawing.Point(146, 510);
            this.gridDocuments.Name = "gridDocuments";
            this.gridDocuments.OutlineCol = 0;
            this.gridDocuments.ReadOnly = true;
            this.gridDocuments.RowHeadersVisible = false;
            this.gridDocuments.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gridDocuments.RowHeightMin = 0;
            this.gridDocuments.Rows = 1;
            this.gridDocuments.ScrollTipText = null;
            this.gridDocuments.ShowColumnVisibilityMenu = false;
            this.gridDocuments.ShowFocusCell = false;
            this.gridDocuments.Size = new System.Drawing.Size(557, 167);
            this.gridDocuments.StandardTab = true;
            this.gridDocuments.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.gridDocuments.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.gridDocuments.TabIndex = 4;
            this.gridDocuments.KeyDown += new Wisej.Web.KeyEventHandler(this.gridDocuments_KeyDownEvent);
            this.gridDocuments.DoubleClick += new System.EventHandler(this.gridDocuments_DblClick);
            // 
            // t2kDate
            // 
            this.t2kDate.Location = new System.Drawing.Point(146, 150);
            this.t2kDate.Mask = "##/##/####";
            this.t2kDate.Name = "t2kDate";
            this.t2kDate.Size = new System.Drawing.Size(165, 40);
            this.t2kDate.TabIndex = 1;
            this.t2kDate.Text = "  /  /";
            // 
            // rtbDetail
            // 
			this.rtbDetail.MaxLength = 63000;
            this.rtbDetail.Location = new System.Drawing.Point(146, 270);
            this.rtbDetail.Multiline = true;
            this.rtbDetail.Name = "rtbDetail";
            this.rtbDetail.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
            this.rtbDetail.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
            this.rtbDetail.SelTabCount = null;
            this.rtbDetail.Size = new System.Drawing.Size(557, 220);
            this.rtbDetail.TabIndex = 3;
            // 
            // txtDescription
            // 
			this.txtDescription.MaxLength = 254;
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(146, 210);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(310, 40);
            this.txtDescription.TabIndex = 2;
            // 
            // txtType
            // 
            this.txtType.AutoSize = false;
            this.txtType.BackColor = System.Drawing.SystemColors.Window;
            this.txtType.LinkItem = null;
            this.txtType.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtType.LinkTopic = null;
            this.txtType.Location = new System.Drawing.Point(146, 90);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(165, 40);
            this.txtType.TabIndex = 0;
            // 
            // lblType
            // 
            this.lblType.Location = new System.Drawing.Point(30, 104);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(63, 16);
            this.lblType.TabIndex = 11;
            this.lblType.Text = "TYPE";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(35, 524);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(93, 16);
            this.Label5.TabIndex = 9;
            this.Label5.Text = "DOCUMENTS";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(35, 284);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(58, 16);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "DETAIL";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(35, 224);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(93, 16);
            this.Label3.TabIndex = 7;
            this.Label3.Text = "DESCRIPTION";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(45, 16);
            this.Label2.TabIndex = 6;
            this.Label2.Text = "TYPE";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 164);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(45, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "DATE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAttachDoc,
            this.mnuRemoveDocument,
            this.mnuViewDocuments,
            this.mnuSepar3,
            this.mnuPrint,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAttachDoc
            // 
            this.mnuAttachDoc.Index = 0;
            this.mnuAttachDoc.Name = "mnuAttachDoc";
            this.mnuAttachDoc.Text = "Attach Document";
            this.mnuAttachDoc.Click += new System.EventHandler(this.mnuAttachDoc_Click);
            // 
            // mnuRemoveDocument
            // 
            this.mnuRemoveDocument.Index = 1;
            this.mnuRemoveDocument.Name = "mnuRemoveDocument";
            this.mnuRemoveDocument.Text = "Remove Document";
            this.mnuRemoveDocument.Click += new System.EventHandler(this.mnuRemoveDocument_Click);
            // 
            // mnuViewDocuments
            // 
            this.mnuViewDocuments.Index = 2;
            this.mnuViewDocuments.Name = "mnuViewDocuments";
            this.mnuViewDocuments.Text = "View Documents";
            this.mnuViewDocuments.Click += new System.EventHandler(this.mnuViewDocuments_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 3;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 4;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 5;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 6;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 7;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 8;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 9;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdAttachDocument
            // 
            this.cmdAttachDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAttachDocument.AppearanceKey = "toolbarButton";
            this.cmdAttachDocument.Location = new System.Drawing.Point(396, 29);
            this.cmdAttachDocument.Name = "cmdAttachDocument";
            this.cmdAttachDocument.Size = new System.Drawing.Size(122, 24);
            this.cmdAttachDocument.TabIndex = 1;
            this.cmdAttachDocument.Text = "Attach Document";
            this.cmdAttachDocument.Click += new System.EventHandler(this.mnuAttachDoc_Click);
            // 
            // cmdRemoveDocument
            // 
            this.cmdRemoveDocument.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemoveDocument.AppearanceKey = "toolbarButton";
            this.cmdRemoveDocument.Location = new System.Drawing.Point(522, 29);
            this.cmdRemoveDocument.Name = "cmdRemoveDocument";
            this.cmdRemoveDocument.Size = new System.Drawing.Size(136, 24);
            this.cmdRemoveDocument.TabIndex = 2;
            this.cmdRemoveDocument.Text = "Remove Document";
            this.cmdRemoveDocument.Click += new System.EventHandler(this.mnuRemoveDocument_Click);
            // 
            // cmdViewDocuments
            // 
            this.cmdViewDocuments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdViewDocuments.AppearanceKey = "toolbarButton";
            this.cmdViewDocuments.Location = new System.Drawing.Point(664, 29);
            this.cmdViewDocuments.Name = "cmdViewDocuments";
            this.cmdViewDocuments.Size = new System.Drawing.Size(121, 24);
            this.cmdViewDocuments.TabIndex = 3;
            this.cmdViewDocuments.Text = "View Documents";
            this.cmdViewDocuments.Click += new System.EventHandler(this.mnuViewDocuments_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(791, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(48, 24);
            this.cmdPrint.TabIndex = 4;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(445, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // frmActivity
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(947, 642);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmActivity";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Activity";
            this.Load += new System.EventHandler(this.frmActivity_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmActivity_KeyDown);
            this.Resize += new System.EventHandler(this.frmActivity_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.t2kDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAttachDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdViewDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdRemoveDocument;
		private FCButton cmdAttachDocument;
		private FCButton cmdViewDocuments;
		private FCButton cmdSave;
		private FCButton cmdPrint;
	}
}
