﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmInspectors.
	/// </summary>
	partial class frmInspectors
	{
		public FCGrid GridDelete;
		public FCGrid Grid;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddInspector;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteInspector;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            this.GridDelete = new fecherFoundation.FCGrid();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddInspector = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteInspector = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddInspector = new fecherFoundation.FCButton();
            this.cmdDeleteInspector = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddInspector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteInspector)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 545);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.GridDelete);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Size = new System.Drawing.Size(1078, 485);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAddInspector);
            this.TopPanel.Controls.Add(this.cmdDeleteInspector);
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteInspector, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddInspector, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(126, 30);
            this.HeaderText.Text = "Inspectors";
            // 
            // GridDelete
            // 
            this.GridDelete.AllowSelection = false;
            this.GridDelete.AllowUserToResizeColumns = false;
            this.GridDelete.AllowUserToResizeRows = false;
            this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
            this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
            this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
            this.GridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.GridDelete.Cols = 1;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GridDelete.ColumnHeadersHeight = 30;
            this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GridDelete.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.GridDelete.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridDelete.DragIcon = null;
            this.GridDelete.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.GridDelete.FixedCols = 0;
            this.GridDelete.FixedRows = 0;
            this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.FrozenCols = 0;
            this.GridDelete.GridColor = System.Drawing.Color.Empty;
            this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
            this.GridDelete.Location = new System.Drawing.Point(32, 302);
            this.GridDelete.Name = "GridDelete";
            this.GridDelete.OutlineCol = 0;
            this.GridDelete.RowHeadersVisible = false;
            this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GridDelete.RowHeightMin = 0;
            this.GridDelete.Rows = 0;
            this.GridDelete.ScrollTipText = null;
            this.GridDelete.ShowColumnVisibilityMenu = false;
            this.GridDelete.ShowFocusCell = false;
            this.GridDelete.Size = new System.Drawing.Size(17, 26);
            this.GridDelete.StandardTab = true;
            this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.GridDelete.TabIndex = 1;
            this.GridDelete.Visible = false;
            // 
            // Grid
            // 
            this.Grid.AllowSelection = false;
            this.Grid.AllowUserToResizeColumns = false;
            this.Grid.AllowUserToResizeRows = false;
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
            this.Grid.BackColorBkg = System.Drawing.Color.Empty;
            this.Grid.BackColorFixed = System.Drawing.Color.Empty;
            this.Grid.BackColorSel = System.Drawing.Color.Empty;
            this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 3;
            dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.Grid.ColumnHeadersHeight = 30;
            this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
            this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
            this.Grid.FrozenCols = 0;
            this.Grid.GridColor = System.Drawing.Color.Empty;
            this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(30, 30);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
            this.Grid.RowHeadersVisible = false;
            this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
            this.Grid.ScrollTipText = null;
            this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(1014, 454);
            this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.Grid.TabIndex = 0;
            this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.Grid_KeyDownEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddInspector,
            this.mnuDeleteInspector,
            this.mnuSepar3,
            this.mnuPrint,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAddInspector
            // 
            this.mnuAddInspector.Index = 0;
            this.mnuAddInspector.Name = "mnuAddInspector";
            this.mnuAddInspector.Text = "Add Inspector";
            this.mnuAddInspector.Click += new System.EventHandler(this.mnuAddInspector_Click);
            // 
            // mnuDeleteInspector
            // 
            this.mnuDeleteInspector.Index = 1;
            this.mnuDeleteInspector.Name = "mnuDeleteInspector";
            this.mnuDeleteInspector.Text = "Delete Inspector";
            this.mnuDeleteInspector.Click += new System.EventHandler(this.mnuDeleteInspector_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 2;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 3;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print Inspectors";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 4;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 5;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 6;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 7;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 8;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(498, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(78, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddInspector
            // 
            this.cmdAddInspector.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddInspector.AppearanceKey = "toolbarButton";
            this.cmdAddInspector.Location = new System.Drawing.Point(704, 29);
            this.cmdAddInspector.Name = "cmdAddInspector";
            this.cmdAddInspector.Size = new System.Drawing.Size(104, 24);
            this.cmdAddInspector.TabIndex = 1;
            this.cmdAddInspector.Text = "Add Inspector";
            this.cmdAddInspector.Click += new System.EventHandler(this.mnuAddInspector_Click);
            // 
            // cmdDeleteInspector
            // 
            this.cmdDeleteInspector.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteInspector.AppearanceKey = "toolbarButton";
            this.cmdDeleteInspector.Location = new System.Drawing.Point(814, 29);
            this.cmdDeleteInspector.Name = "cmdDeleteInspector";
            this.cmdDeleteInspector.Size = new System.Drawing.Size(120, 24);
            this.cmdDeleteInspector.TabIndex = 2;
            this.cmdDeleteInspector.Text = "Delete Inspector";
            this.cmdDeleteInspector.Click += new System.EventHandler(this.mnuDeleteInspector_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.AppearanceKey = "toolbarButton";
            this.cmdPrint.Location = new System.Drawing.Point(940, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(110, 24);
            this.cmdPrint.TabIndex = 3;
            this.cmdPrint.Text = "Print Inspectors";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmInspectors
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1078, 653);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmInspectors";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Inspectors";
            this.Load += new System.EventHandler(this.frmInspectors_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmInspectors_KeyDown);
            this.Resize += new System.EventHandler(this.frmInspectors_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddInspector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteInspector)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdPrint;
		private FCButton cmdDeleteInspector;
		private FCButton cmdAddInspector;
	}
}
