//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsCustomBillUtilities
	{
		//=========================================================
		public string GetTitleFromCustomForm(ref int lngID)
		{
			string GetTitleFromCustomForm = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select formatname from custombills where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				GetTitleFromCustomForm = FCConvert.ToString(rsLoad.Get_Fields_String("FormatName"));
			}
			else
			{
				GetTitleFromCustomForm = "";
			}
			return GetTitleFromCustomForm;
		}

		public string GetDescriptionFromCustomForm(ref int lngID)
		{
			string GetDescriptionFromCustomForm = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select description from custombills where ID = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				GetDescriptionFromCustomForm = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
			}
			else
			{
				GetDescriptionFromCustomForm = "";
			}
			return GetDescriptionFromCustomForm;
		}

		public string GetTableListFromCustomForm(ref int lngID)
		{
			string GetTableListFromCustomForm = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				bool boolPropTable = false;
				bool boolPermitTable = false;
				bool boolInspection = false;
				bool boolInspector = false;
				bool boolContractor = false;
				bool boolPermitContractors = false;
				bool boolPermitOpens;
				string strTemp;
				int x;
				strTemp = "";
				// Call rsLoad.OpenRecordset("select * from custombillfields where fieldid > 0 and  formatid = " & lngID & " order by fieldid", strCEDatabase)
				// Do While Not rsLoad.EndOfFile
				// Select Case rsLoad.Fields("fieldid")
				// Case 1 To 25, 41, 42, 43
				// boolPropTable = True
				// Case 26 To 33, 44
				// boolPermitTable = True
				// Case 40
				// boolPermitTable = True
				// boolPermitContractors = True
				// Case 34 To 36, 45
				// boolInspection = True
				// Case 37
				// boolInspection = True
				// boolInspector = True
				// Case 38, 39, 46
				// boolContractor = True
				// End Select
				// rsLoad.MoveNext
				// Loop
				FCCollection cdList = new FCCollection();
				clsDynamicDocumentTag tTag;
				GetCodeListFromCustomForm(ref lngID, ref cdList);
				if (!FCUtils.IsEmpty(cdList))
				{
					if (cdList.Count > 0)
					{
						for (x = 1; x <= cdList.Count; x++)
						{
							tTag = cdList[x];
							if (!(tTag == null))
							{
								int vbPorterVar = tTag.Code;
								if ((vbPorterVar >= 1 && vbPorterVar <= 25) || (vbPorterVar == 41) || (vbPorterVar == 42) || (vbPorterVar == 43))
								{
									boolPropTable = true;
								}
								else if (vbPorterVar >= 26 && vbPorterVar <= 33)
								{
									boolPermitTable = true;
								}
								else if (vbPorterVar == 44)
								{
									boolPermitOpens = true;
									boolPermitTable = true;
								}
								else if (vbPorterVar == 40)
								{
									boolPermitTable = true;
									boolPermitContractors = true;
								}
								else if ((vbPorterVar >= 34 && vbPorterVar <= 36) || (vbPorterVar == 45))
								{
									boolInspection = true;
								}
								else if (vbPorterVar == 37)
								{
									boolInspection = true;
									boolInspector = true;
								}
								else if ((vbPorterVar == 38) || (vbPorterVar == 39) || (vbPorterVar == 46))
								{
									boolContractor = true;
								}
							}
						}
						// x
					}
				}
				for (x = 1; x <= cdList.Count; x++)
				{
					cdList.Remove(1);
				}
				// x
				cdList = null;
				if (boolPropTable)
				{
					strTemp += "CEMaster,";
				}
				if (boolPermitTable)
				{
					strTemp += "Permits,";
				}
				if ((boolPermitTable && boolContractor) || boolPermitContractors)
				{
					strTemp += "PermitContractors,";
				}
				if (boolContractor)
				{
					strTemp += "Contractors,";
				}
				if (boolInspection)
				{
					strTemp += "Inspections,";
				}
				if (boolInspector)
				{
					strTemp += "Inspectors,";
				}
				if (fecherFoundation.Strings.Trim(strTemp) != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				GetTableListFromCustomForm = strTemp;
				return GetTableListFromCustomForm;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetTableListFromCustomForm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetTableListFromCustomForm;
		}

		private void GetCodeListFromCustomForm(ref int lngID, ref FCCollection cdList)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int x;
				clsDynamicDocumentTag tTag;
				string strTemp = "";
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from custombillfields where (fieldid = -4 or fieldid > 0) and formatid = " + FCConvert.ToString(lngID), modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					if (rsLoad.Get_Fields("fieldid") > 0)
					{
						if (cdList.Count > 0)
						{
							tTag = GetCodeByCode("Code" + Conversion.Val(rsLoad.Get_Fields("fieldid")) + "ID" + Conversion.Val(rsLoad.Get_Fields_Int32("openid")), ref cdList);
							if (tTag == null)
							{
								tTag = new clsDynamicDocumentTag();
								tTag.Code = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("fieldid"))));
								tTag.MiscID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
								cdList.Add(tTag, "Code" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fieldid"))) + "ID" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
							}
						}
						else
						{
							tTag = new clsDynamicDocumentTag();
							tTag.Code = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("fieldid"))));
							tTag.MiscID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
							cdList.Add(tTag, "Code" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields("fieldid"))) + "ID" + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("openid"))));
						}
					}
					else
					{
						rsTemp.OpenRecordset("select * from dynamicreports where ID = " + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("usertext"))), modGlobalVariables.Statics.strCEDatabase);
						if (!rsTemp.EndOfFile())
						{
							strTemp = FCConvert.ToString(rsTemp.Get_Fields_String("text"));
							if (strTemp != "")
							{
								ParseDDTextForCodes(ref strTemp, ref cdList);
							}
						}
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In GetCodeListFromCustomForm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public clsDynamicDocumentTag GetCodeByCode(string strTag, ref FCCollection cdList)
		{
			clsDynamicDocumentTag GetCodeByCode = null;
			clsDynamicDocumentTag tDDT;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				tDDT = null;
				if (!FCUtils.IsEmpty(cdList))
				{
					if (cdList.Count > 0)
					{
						tDDT = (clsDynamicDocumentTag)cdList[strTag];
					}
				}
				GetCodeByCode = tDDT;
				return GetCodeByCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				tDDT = null;
				GetCodeByCode = tDDT;
			}
			return GetCodeByCode;
		}

		private void ParseDDTextForCodes(ref string strText, ref FCCollection cdList)
		{
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			string strTag = "";
			int lngMiscID;
			int lngCode = 0;
			clsDynamicDocumentTag tTag;
			clsDynamicDocument ddDefault;
			ddDefault = new clsDynamicDocument();
			ddDefault.SetupTags();
			try
			{
				lngMiscID = 0;
				// ParseDynamicDocumentString = ""
				lngEndOfLastVariable = 0;
				lngNextVariable = 1;
				if (strText.Length < 1)
					return;
				// priming read
				if (Strings.InStr(1, strText, "<", CompareConstants.vbBinaryCompare) > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strText, "<", CompareConstants.vbBinaryCompare);
					// do until there are no more variables left
					do
					{
						// set the end pointer
						if (Strings.InStr(lngNextVariable, strText, ">", CompareConstants.vbBinaryCompare) > 0)
						{
							lngEndOfLastVariable = Strings.InStr(lngNextVariable, strText, ">", CompareConstants.vbBinaryCompare);
							// replace the variable
							strTag = Strings.Mid(strText, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1);
							if (fecherFoundation.Strings.UCase(strTag) == "DATE")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "TIME")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "YEAR")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "BUSNAME")
							{
							}
							else if (fecherFoundation.Strings.UCase(strTag) == "CRLF")
							{
							}
							else
							{
								tTag = ddDefault.GetTagByTag(ref strTag);
								if (!(tTag == null))
								{
									lngCode = tTag.Code;
									lngMiscID = tTag.MiscID;
									// If intReportType = CNSTDYNAMICREPORTTYPECUSTOMBILL Then
									// lngCode = GetCodeFromDynamicTag(strTag, intReportType, lngMiscID)
									tTag = GetCodeByCode("Code" + lngCode + "ID" + lngMiscID, ref cdList);
									if (tTag == null)
									{
										tTag = new clsDynamicDocumentTag();
										tTag.Code = lngCode;
										tTag.MiscID = lngMiscID;
										cdList.Add(tTag, "Code" + FCConvert.ToString(lngCode) + "ID" + FCConvert.ToString(lngMiscID));
									}
									// Else
									// lngCode = GetCodeFromDynamicTag(strTag, intReportType, lngMiscID)
									// strBuildString = strBuildString & GetDataByCode(lngCode, lngMiscID)
									// End If
								}
							}
							// check for another variable
							lngNextVariable = Strings.InStr(lngEndOfLastVariable, strText, "<", CompareConstants.vbBinaryCompare);
						}
						else
						{
							lngNextVariable = 0;
						}
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				// If lngEndOfLastVariable < Len(strText) Then
				// strBuildString = strBuildString & Mid(strOriginal, lngEndOfLastVariable + 1)
				// End If
				return;
			}
			catch (Exception ex)
			{
				ErrorHandler:
				;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ParseDDTextForCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
