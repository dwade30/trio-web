﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmCustomize : BaseForm
	{
		public frmCustomize()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCustomize InstancePtr
		{
			get
			{
				return (frmCustomize)Sys.GetInstance(typeof(frmCustomize));
			}
		}

		protected frmCustomize _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDPERMITSCOLAUTOID = 0;
		const int CNSTGRIDPERMITSCOLPLAN = 2;
		const int CNSTGRIDPERMITSCOLPERMITNUMBER = 1;
		const int CNSTGRIDPERMITSCOLSTARTDATE = 3;
		const int CNSTGRIDPERMITSCOLENDDATE = 4;
		const int CNSTGRIDPERMITSCOLSTATUS = 5;
		const int CNSTGRIDPERMITSCOLPERMITIDENTIFIER = 6;
		const int CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE = 7;
		const int CNSTGRIDPERMITSCOLPERMITVALUE = 8;
		const int CNSTGRIDPERMITSCOLPERMITFEE = 9;
		const int CNSTGRIDPERMITSCOLTYPE = 10;
		// Private aryColWidths(11) As Double
		clsGridPermitSetup gps = new clsGridPermitSetup();

		private void chkApplicationDate_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkApplicationDate.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, gps.Get_ColWidth(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkComplete_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkComplete.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLENDDATE, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLENDDATE, gps.Get_ColWidth(CNSTGRIDPERMITSCOLENDDATE));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLENDDATE, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPermitFee_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPermitFee.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITFEE, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLPERMITFEE, gps.Get_ColWidth(CNSTGRIDPERMITSCOLPERMITFEE));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITFEE, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPermitIdentifier_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPermitIdentifier.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITIDENTIFIER, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLPERMITIDENTIFIER, gps.Get_ColWidth(CNSTGRIDPERMITSCOLPERMITIDENTIFIER));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITIDENTIFIER, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPermitNumber_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPermitNumber.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITNUMBER, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLPERMITNUMBER, gps.Get_ColWidth(CNSTGRIDPERMITSCOLPERMITNUMBER));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITNUMBER, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPermitStatus_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPermitStatus.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLSTATUS, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLSTATUS, gps.Get_ColWidth(CNSTGRIDPERMITSCOLSTATUS));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLSTATUS, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPermitType_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPermitType.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLTYPE, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLTYPE, gps.Get_ColWidth(CNSTGRIDPERMITSCOLTYPE));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLTYPE, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPermitValue_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPermitValue.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITVALUE, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLPERMITVALUE, gps.Get_ColWidth(CNSTGRIDPERMITSCOLPERMITVALUE));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPERMITVALUE, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkPlan_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkPlan.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPLAN, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLPLAN, gps.Get_ColWidth(CNSTGRIDPERMITSCOLPLAN));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLPLAN, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
            {
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
                //GridPermits.ExtendLastCol = true;
            }
        }

		private void chkStart_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkStart.CheckState == Wisej.Web.CheckState.Checked)
			{
				HidePermitCol(CNSTGRIDPERMITSCOLSTARTDATE, false);
				ResizePermitCol(CNSTGRIDPERMITSCOLSTARTDATE, gps.Get_ColWidth(CNSTGRIDPERMITSCOLSTARTDATE));
			}
			else
			{
				HidePermitCol(CNSTGRIDPERMITSCOLSTARTDATE, true);
			}
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
                //FC:FINAL:BSE:#4364 - extend last column not working similar with vb6 version
				//GridPermits.ExtendLastCol = true;
			}
		}

		private void frmCustomize_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomize_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomize properties;
			//frmCustomize.FillStyle	= 0;
			//frmCustomize.ScaleWidth	= 9300;
			//frmCustomize.ScaleHeight	= 7950;
			//frmCustomize.LinkTopic	= "Form2";
			//frmCustomize.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FillcmbYearCalc();
			FillcmbStartMonth();
			FillcmbPermitFormat();
			LoadInfo();
			SetupGridPermit();
		}

		private void frmCustomize_Resize(object sender, System.EventArgs e)
		{
			ResizeGridPermit();
		}

		private void FillcmbYearCalc()
		{
			cmbYearCalc.Clear();
			cmbYearCalc.AddItem("Calendar Year");
			cmbYearCalc.ItemData(cmbYearCalc.NewIndex, 0);
			cmbYearCalc.AddItem("Calendar Year + 1");
			cmbYearCalc.ItemData(cmbYearCalc.NewIndex, 1);
		}

		private void FillcmbStartMonth()
		{
			cmbFirstMonth.Clear();
			cmbFirstMonth.AddItem("January");
			cmbFirstMonth.AddItem("February");
			cmbFirstMonth.AddItem("March");
			cmbFirstMonth.AddItem("April");
			cmbFirstMonth.AddItem("May");
			cmbFirstMonth.AddItem("June");
			cmbFirstMonth.AddItem("July");
			cmbFirstMonth.AddItem("August");
			cmbFirstMonth.AddItem("September");
			cmbFirstMonth.AddItem("October");
			cmbFirstMonth.AddItem("November");
			cmbFirstMonth.AddItem("December");
		}

		private void FillcmbPermitFormat()
		{
			cmbFormatPermitNumber.Clear();
			cmbFormatViolationNumber.Clear();
			int x;
			for (x = 0; x <= 20; x++)
			{
				cmbFormatPermitNumber.AddItem(x.ToString());
				cmbFormatPermitNumber.ItemData(cmbFormatPermitNumber.NewIndex, x);
				cmbFormatViolationNumber.AddItem(x.ToString());
				cmbFormatViolationNumber.ItemData(cmbFormatViolationNumber.NewIndex, x);
			}
			// x
		}

		private void GridPermits_AfterMoveColumn(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				gps.Set_ColOrder(FCConvert.ToInt32(GridPermits.ColData(x)), x);
			}
			// x
		}

		private void GridPermits_AfterUserResize(object sender, EventArgs e)
		{
			int intType;
			int GridWidth;
			double dblSize;
			intType = FCConvert.ToInt16(GridPermits.ColData(GridPermits.Col));
			GridWidth = GridPermits.WidthOriginal;
			dblSize = FCConvert.ToDouble(GridPermits.ColWidth(GridPermits.Col)) / GridWidth;
			gps.Set_ColWidth(intType, dblSize);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
                //FC:FINAL:DSE:#1894 Save button should not close the form
                //mnuExit_Click();
                FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			SaveInfo = false;
			if (chkUseRE.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.UseREData = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.UseREData = false;
			}
			if (chkGeneratePermitIdentifier.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.GeneratePermitIdentifiers = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.GeneratePermitIdentifiers = false;
			}
			if (chkGenerateViolationIdentifier.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers = false;
			}
			if (chkInspectionPrompt.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.PromptInspectionSchedule = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.PromptInspectionSchedule = false;
			}
			if (chkAddInspectionsToCalendar.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.AutoAddInspectionsToCalendar = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.AutoAddInspectionsToCalendar = false;
			}
			if (chkResetNumber.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.PermitNumberReset = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.PermitNumberReset = false;
			}
			if (chk2DigitYear.CheckState == Wisej.Web.CheckState.Checked)
			{
				modGlobalVariables.Statics.CECustom.UseTwoDigitYear = true;
			}
			else
			{
				modGlobalVariables.Statics.CECustom.UseTwoDigitYear = false;
			}
			modGlobalVariables.Statics.CECustom.StartMonth = (cmbFirstMonth.SelectedIndex + 1);
			modGlobalVariables.Statics.CECustom.AddToYear = cmbYearCalc.ItemData(cmbYearCalc.SelectedIndex);
			modGlobalVariables.Statics.CECustom.PermitFormatPlaces = cmbFormatPermitNumber.ItemData(cmbFormatPermitNumber.SelectedIndex);
			modGlobalVariables.Statics.CECustom.ViolationFormatPlaces = cmbFormatViolationNumber.ItemData(cmbFormatViolationNumber.SelectedIndex);
			int x;
			for (x = 0; x <= 2; x++)
			{
				if (cmbPermitIdentFormat.SelectedIndex == x)
				{
					modGlobalVariables.Statics.CECustom.PermitIdentifierFormat = x;
				}
				if (cmbIdentifierType.SelectedIndex == x)
				{
					modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat = x;
				}
			}
			// x
			for (x = 0; x <= 1; x++)
			{
				if (cmbPermitIdentifier.SelectedIndex == x)
				{
					modGlobalVariables.Statics.CECustom.PermitIdentifierValidation = x;
				}
				if (cmbIdentifier.SelectedIndex == x)
				{
					modGlobalVariables.Statics.CECustom.ViolationIdentifierValidation = x;
				}
			}
			// x
			modGlobalVariables.Statics.CECustom.Save();
			gps.SaveCols();
			SaveInfo = true;
            //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
            App.MainForm.ReloadNavigationMenu();
            return SaveInfo;
		}

		private object LoadInfo()
		{
			object LoadInfo = null;
			if (modGlobalVariables.Statics.CECustom.UseREData)
			{
				chkUseRE.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkUseRE.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobalVariables.Statics.CECustom.GeneratePermitIdentifiers)
			{
				chkGeneratePermitIdentifier.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkGeneratePermitIdentifier.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobalVariables.Statics.CECustom.GenerateViolationIdentifiers)
			{
				chkGenerateViolationIdentifier.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkGenerateViolationIdentifier.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobalVariables.Statics.CECustom.PromptInspectionSchedule)
			{
				chkInspectionPrompt.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkInspectionPrompt.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobalVariables.Statics.CECustom.AutoAddInspectionsToCalendar)
			{
				chkAddInspectionsToCalendar.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkAddInspectionsToCalendar.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobalVariables.Statics.CECustom.PermitNumberReset)
			{
				chkResetNumber.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkResetNumber.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (modGlobalVariables.Statics.CECustom.UseTwoDigitYear)
			{
				chk2DigitYear.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chk2DigitYear.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			cmbFirstMonth.SelectedIndex = modGlobalVariables.Statics.CECustom.StartMonth - 1;
			int x;
			for (x = 0; x <= cmbYearCalc.Items.Count - 1; x++)
			{
				if (cmbYearCalc.ItemData(x) == modGlobalVariables.Statics.CECustom.AddToYear)
				{
					cmbYearCalc.SelectedIndex = x;
					break;
				}
			}
			// x
			for (x = 0; x <= cmbFormatPermitNumber.Items.Count - 1; x++)
			{
				if (cmbFormatPermitNumber.ItemData(x) == modGlobalVariables.Statics.CECustom.PermitFormatPlaces)
				{
					cmbFormatPermitNumber.SelectedIndex = x;
					break;
				}
			}
			// x
			for (x = 0; x <= cmbFormatViolationNumber.Items.Count - 1; x++)
			{
				if (cmbFormatViolationNumber.ItemData(x) == modGlobalVariables.Statics.CECustom.ViolationFormatPlaces)
				{
					cmbFormatViolationNumber.SelectedIndex = x;
					break;
				}
			}
			// x
			cmbIdentifier.SelectedIndex = modGlobalVariables.Statics.CECustom.ViolationIdentifierValidation;
			cmbIdentifierType.SelectedIndex = modGlobalVariables.Statics.CECustom.ViolationIdentifierFormat;
			cmbPermitIdentFormat.SelectedIndex = modGlobalVariables.Statics.CECustom.PermitIdentifierFormat;
			cmbPermitIdentifier.SelectedIndex = modGlobalVariables.Statics.CECustom.PermitIdentifierValidation;
			gps.LoadCols();
			SetupGridPermit();
			return LoadInfo;
		}

		private void ResizeGridPermit()
		{
			int GridWidth;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			GridWidth = GridPermits.WidthOriginal;
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				if (!GridPermits.ColHidden(x))
				{
					GridPermits.ColWidth(x, FCConvert.ToInt32(gps.Get_ColWidth(FCConvert.ToInt32(GridPermits.ColData(x))) * GridWidth));
				}
			}
			// x
			if (gps.VisiblePercentage() >= 1)
			{
				GridPermits.ExtendLastCol = false;
			}
			else
			{
				GridPermits.ExtendLastCol = true;
			}
		}

		private void SetupGridPermit()
		{
			int GridWidth = 0;
			GridWidth = GridPermits.WidthOriginal;
			GridPermits.Cols = 11;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			GridPermits.ColHidden(CNSTGRIDPERMITSCOLAUTOID, true);
			GridPermits.ColData(CNSTGRIDPERMITSCOLAUTOID, CNSTGRIDPERMITSCOLAUTOID);
			GridPermits.ColData(CNSTGRIDPERMITSCOLENDDATE, CNSTGRIDPERMITSCOLENDDATE);
			GridPermits.ColData(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE);
			GridPermits.ColData(CNSTGRIDPERMITSCOLPERMITFEE, CNSTGRIDPERMITSCOLPERMITFEE);
			GridPermits.ColData(CNSTGRIDPERMITSCOLPERMITIDENTIFIER, CNSTGRIDPERMITSCOLPERMITIDENTIFIER);
			GridPermits.ColData(CNSTGRIDPERMITSCOLPERMITNUMBER, CNSTGRIDPERMITSCOLPERMITNUMBER);
			GridPermits.ColData(CNSTGRIDPERMITSCOLPERMITVALUE, CNSTGRIDPERMITSCOLPERMITVALUE);
			GridPermits.ColData(CNSTGRIDPERMITSCOLPLAN, CNSTGRIDPERMITSCOLPLAN);
			GridPermits.ColData(CNSTGRIDPERMITSCOLSTARTDATE, CNSTGRIDPERMITSCOLSTARTDATE);
			GridPermits.ColData(CNSTGRIDPERMITSCOLSTATUS, CNSTGRIDPERMITSCOLSTATUS);
			GridPermits.ColData(CNSTGRIDPERMITSCOLTYPE, CNSTGRIDPERMITSCOLTYPE);
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLENDDATE, "Complete");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLENDDATE, "01/02/" + FCConvert.ToString(DateTime.Today.Year));
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLENDDATE, "07/23/" + FCConvert.ToString(DateTime.Today.Year));
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, "App Date");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, "01/01/" + FCConvert.ToString(DateTime.Today.Year));
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE, "4/11/" + FCConvert.ToString(DateTime.Today.Year));
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITFEE, "Fee");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLPERMITFEE, "10.50");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLPERMITFEE, "25.00");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITIDENTIFIER, "Identifier");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLPERMITIDENTIFIER, "P17989");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLPERMITIDENTIFIER, "P18995");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITNUMBER, "Permit");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLPERMITNUMBER, Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2) + "-99");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLPERMITNUMBER, Strings.Right(FCConvert.ToString(DateTime.Today.Year), 2) + "-215");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPERMITVALUE, "Value");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLPERMITVALUE, "25,300");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLPERMITVALUE, "519");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLPLAN, "Plan");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLPLAN, "519");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLPLAN, "");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLSTARTDATE, "Start");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLSTARTDATE, "01/01/" + FCConvert.ToString(DateTime.Today.Year));
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLSTARTDATE, "05/18/" + FCConvert.ToString(DateTime.Today.Year));
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLSTATUS, "Status");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLSTATUS, "Completed");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLSTATUS, "Completed");
			GridPermits.TextMatrix(0, CNSTGRIDPERMITSCOLTYPE, "Type");
			GridPermits.TextMatrix(1, CNSTGRIDPERMITSCOLTYPE, "Building");
			GridPermits.TextMatrix(2, CNSTGRIDPERMITSCOLTYPE, "Plumbing");
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLENDDATE))
			{
				chkComplete.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE))
			{
				chkApplicationDate.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLPERMITFEE))
			{
				chkPermitFee.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLPERMITIDENTIFIER))
			{
				chkPermitIdentifier.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLPERMITNUMBER))
			{
				chkPermitNumber.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLPERMITVALUE))
			{
				chkPermitValue.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLPLAN))
			{
				chkPlan.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLSTARTDATE))
			{
				chkStart.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLSTATUS))
			{
				chkPermitStatus.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (gps.Get_ColShow(CNSTGRIDPERMITSCOLTYPE))
			{
				chkPermitType.CheckState = Wisej.Web.CheckState.Checked;
			}
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				// .ColWidth(x) = 0.18 * gridWidth
				GridPermits.ColWidth(x, FCConvert.ToInt32(gps.Get_ColWidth(x) * GridWidth));
				GridPermits.ColHidden(x, !gps.Get_ColShow(x));
				// aryColWidths(x) = 0.18
			}
			// x
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				MoveCol(ref x, gps.Get_ColOrder(x));
			}
			// x
		}

		private void MoveCol(ref int intType, int intCol)
		{
			int x;
			for (x = 1; x <= 10; x++)
			{
				if (FCConvert.ToInt32(GridPermits.ColData(x)) == intType)
				{
					GridPermits.ColPosition(x, intCol);
					break;
				}
			}
			// x
		}

		private void ResizePermitCol(int intColType, double dblWidth)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int GridWidth;
			GridWidth = GridPermits.WidthOriginal;
			// aryColWidths(intColType) = dblWidth
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				if (FCConvert.ToInt32(GridPermits.ColData(x)) == intColType)
				{
					GridPermits.ColWidth(x, FCConvert.ToInt32(dblWidth * GridWidth));
					break;
				}
			}
			// x
		}

		private void HidePermitCol(int intColType, bool boolHide)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			for (x = 1; x <= (GridPermits.Cols - 1); x++)
			{
				if (FCConvert.ToInt32(GridPermits.ColData(x)) == intColType)
				{
					GridPermits.ColHidden(x, boolHide);
					gps.Set_ColShow(intColType, !boolHide);
					break;
				}
			}
			// x
		}
	}
}
