//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmInspectionChecklists : BaseForm
	{
		public frmInspectionChecklists()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmInspectionChecklists InstancePtr
		{
			get
			{
				return (frmInspectionChecklists)Sys.GetInstance(typeof(frmInspectionChecklists));
			}
		}

		protected frmInspectionChecklists _InstancePtr = null;
		//=========================================================
		const int CNSTGRIDCOLAUTOID = 3;
		const int CNSTGRIDCOLDESCRIPTION = 1;
		const int CNSTGRIDCOLMANDATORY = 2;
		const int CNSTGRIDCOLPERMITTYPE = 4;
		const int CNSTGRIDCOLROWHEADER = 0;
		private bool boolDataChanged;
		private bool boolCantEdit;

		private void cmbPermitType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbPermitType.SelectedIndex >= 0)
			{
				ReShowList(cmbPermitType.ItemData(cmbPermitType.SelectedIndex));
			}
		}

		private void ReShowList(int lngPermitType)
		{
			int lngRow;
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLPERMITTYPE)) == lngPermitType)
				{
					Grid.RowHidden(lngRow, false);
				}
				else
				{
					Grid.RowHidden(lngRow, true);
				}
			}
			// lngRow
		}

		private void frmInspectionChecklists_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmInspectionChecklists_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInspectionChecklists properties;
			//frmInspectionChecklists.FillStyle	= 0;
			//frmInspectionChecklists.ScaleWidth	= 9300;
			//frmInspectionChecklists.ScaleHeight	= 7710;
			//frmInspectionChecklists.LinkTopic	= "Form2";
			//frmInspectionChecklists.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITDESCRIPTIONSECURITY, false))
			{
				boolCantEdit = true;
				cmdAddItem.Enabled = false;
				cmdDeleteItem.Enabled = false;
				cmdSave.Enabled = false;
				mnuSaveExit.Enabled = false;
			}
			SetupGrid();
			LoadInfo();
			SetupcmbPermitTypes();
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteItem(Grid.Row);
						break;
					}
				case Keys.Insert:
					{
						AddItem();
						break;
					}
			}
			//end switch
		}

		private void Grid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (boolCantEdit)
				return;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						AddItem();
						break;
					}
			}
			//end switch
		}

		private void SetupGrid()
		{
			Grid.Cols = 5;
			Grid.TextMatrix(0, CNSTGRIDCOLMANDATORY, "Mandatory");
			Grid.ColHidden(CNSTGRIDCOLMANDATORY, true);
			Grid.ColHidden(CNSTGRIDCOLPERMITTYPE, true);
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			Grid.ColDataType(CNSTGRIDCOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
			Grid.ColWidth(CNSTGRIDCOLROWHEADER, 300);
		}

		private void SetupcmbPermitTypes()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			int lngFirst;
			lngFirst = 0;
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description ", modGlobalVariables.Statics.strCEDatabase);
			cmbPermitType.Clear();
			while (!rsLoad.EndOfFile())
			{
				cmbPermitType.AddItem(rsLoad.Get_Fields_String("Description"));
				cmbPermitType.ItemData(cmbPermitType.NewIndex, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID")));
				rsLoad.MoveNext();
			}
			if (cmbPermitType.Items.Count > 0)
			{
				cmbPermitType.SelectedIndex = 0;
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
				{
					if (!SaveInfo())
					{
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void LoadInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				GridDelete.Rows = 0;
				rsLoad.OpenRecordset("select * from inspectionchecklistsetup order by permittype,orderno", modGlobalVariables.Statics.strCEDatabase);
				Grid.Visible = false;
				int lngRow;
				Grid.Rows = 1;
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLMANDATORY, FCConvert.ToString(rsLoad.Get_Fields_Boolean("mandatory")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLPERMITTYPE, FCConvert.ToString(rsLoad.Get_Fields("permittype")));
					Grid.RowHidden(lngRow, true);
					rsLoad.MoveNext();
				}
				Grid.Visible = true;
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			Grid.Row = -1;
			//App.DoEvents();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				clsDRWrapper rsSave = new clsDRWrapper();
				for (x = 0; x <= (GridDelete.Rows - 1); x++)
				{
					if (Conversion.Val(GridDelete.TextMatrix(x, 0)) > 0)
					{
						rsSave.Execute("delete from inspectionchecklist where checklistitemid = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(x, 0))), modGlobalVariables.Statics.strCEDatabase);
						rsSave.Execute("delete from inspectionchecklistsetup where ID = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(x, 0))), modGlobalVariables.Statics.strCEDatabase);
					}
				}
				// x
				GridDelete.Rows = 0;
				int intCount;
				int lngLastPermitType;
				lngLastPermitType = 0;
				intCount = 0;
				rsSave.OpenRecordset("select * from inspectionchecklistsetup order by ID", modGlobalVariables.Statics.strCEDatabase);
				for (x = 1; x <= (Grid.Rows - 1); x++)
				{
					if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLPERMITTYPE)) != lngLastPermitType)
					{
						intCount = 0;
					}
					lngLastPermitType = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLPERMITTYPE))));
					intCount += 1;
					if (Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLAUTOID)) > 0)
					{
						if (rsSave.FindFirstRecord("ID", Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLAUTOID))))
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
					}
					else
					{
						rsSave.AddNew();
					}
					rsSave.Set_Fields("permittype", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(x, CNSTGRIDCOLPERMITTYPE))));
					rsSave.Set_Fields("mandatory", FCConvert.CBool(Grid.TextMatrix(x, CNSTGRIDCOLMANDATORY)));
					rsSave.Set_Fields("description", Grid.TextMatrix(x, CNSTGRIDCOLDESCRIPTION));
					rsSave.Set_Fields("orderno", intCount);
					rsSave.Update();
					Grid.TextMatrix(x, CNSTGRIDCOLAUTOID, FCConvert.ToString(rsSave.Get_Fields_Int32("ID")));
				}
				// x
				boolDataChanged = false;
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void Grid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (!boolCantEdit && Grid.Enabled && Grid.Visible)
			{
				Grid.Focus();
			}
		}

		private void mnuAddItem_Click(object sender, System.EventArgs e)
		{
			AddItem();
		}

		private void AddItem(int lngRow = -1)
		{
			boolDataChanged = true;
			// If GridSub.Visible Then
			// AddSubCode
			// Exit Sub
			// End If
			if (lngRow <= 0)
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
			}
			else
			{
				Grid.AddItem(FCConvert.ToString(0), lngRow);
			}
			// lngNextAutoID = lngNextAutoID - 1
			Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(0));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLPERMITTYPE, FCConvert.ToString(cmbPermitType.ItemData(cmbPermitType.SelectedIndex)));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLMANDATORY, FCConvert.ToString(false));
			Grid.RowData(lngRow, true);
			Grid.TopRow = lngRow;
			Grid.Row = lngRow;
		}

		private void DeleteItem(int lngRow)
		{
			boolDataChanged = true;
			if (lngRow > 0)
			{
				if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) > 0)
				{
					GridDelete.AddItem(FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))));
				}
				Grid.RemoveItem(lngRow);
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row > 0)
			{
				DeleteItem(Grid.Row);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//App.DoEvents();
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
            FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				Close();
			}
		}
	}
}
