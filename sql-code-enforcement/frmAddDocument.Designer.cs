//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmAddDocument.
	/// </summary>
	partial class frmAddDocument
	{
		//public AxSCRIBBLELib.AxImageViewer imgPreview;
		public fecherFoundation.FCButton cmdScan;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCLabel lblFile;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.cmdScan = new fecherFoundation.FCButton();
            this.cmdBrowse = new fecherFoundation.FCButton();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.lblFile = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.fcViewerPanel1 = new fecherFoundation.FCViewerPanel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 560);
            this.BottomPanel.Size = new System.Drawing.Size(576, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcViewerPanel1);
            this.ClientArea.Controls.Add(this.cmdScan);
            this.ClientArea.Controls.Add(this.cmdBrowse);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.lblFile);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(576, 500);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(576, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(175, 30);
            this.HeaderText.Text = "Add Document";
            // 
            // cmdScan
            // 
            this.cmdScan.AppearanceKey = "actionButton";
            this.cmdScan.Location = new System.Drawing.Point(152, 122);
            this.cmdScan.Name = "cmdScan";
            this.cmdScan.Size = new System.Drawing.Size(168, 40);
            this.cmdScan.TabIndex = 2;
            this.cmdScan.Text = "Scan Document";
            this.cmdScan.Click += new System.EventHandler(this.cmdScan_Click);
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.AppearanceKey = "actionButton";
            this.cmdBrowse.Location = new System.Drawing.Point(30, 122);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(102, 40);
            this.cmdBrowse.TabIndex = 1;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(211, 30);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(337, 40);
            this.txtDescription.TabIndex = 0;
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(30, 90);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(513, 15);
            this.lblFile.TabIndex = 4;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(170, 18);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "DOCUMENT DESCRIPTION";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(230, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Process";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // fcViewerPanel1
            // 
            this.fcViewerPanel1.Location = new System.Drawing.Point(30, 182);
            this.fcViewerPanel1.Name = "fcViewerPanel1";
            this.fcViewerPanel1.Size = new System.Drawing.Size(518, 301);
            this.fcViewerPanel1.TabIndex = 5;
            // 
            // frmAddDocument
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(576, 668);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmAddDocument";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Add Document";
            this.Load += new System.EventHandler(this.frmAddDocument_Load);
            this.Activated += new System.EventHandler(this.frmAddDocument_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAddDocument_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
        private FCViewerPanel fcViewerPanel1;
    }
}