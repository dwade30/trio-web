﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for srptViolation.
	/// </summary>
	partial class srptViolation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptViolation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtActivityDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActivityDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtActivityDate,
				this.txtType,
				this.txtDescription
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label22,
				this.Label21,
				this.Label3,
				this.Label4
			});
			this.ReportHeader.Height = 0.5208333F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// Label22
			// 
			this.Label22.Height = 0.2395833F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.322917F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-weight: bold";
			this.Label22.Text = "Activity";
			this.Label22.Top = 0F;
			this.Label22.Width = 0.8333333F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1770833F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.04166667F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold";
			this.Label21.Text = "Date";
			this.Label21.Top = 0.2916667F;
			this.Label21.Width = 0.8125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1770833F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-weight: bold";
			this.Label3.Text = "Type";
			this.Label3.Top = 0.2916667F;
			this.Label3.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1770833F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.8125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-weight: bold";
			this.Label4.Text = "Description";
			this.Label4.Top = 0.2916667F;
			this.Label4.Width = 1.34375F;
			// 
			// txtActivityDate
			// 
			this.txtActivityDate.Height = 0.1666667F;
			this.txtActivityDate.Left = 0.04166667F;
			this.txtActivityDate.Name = "txtActivityDate";
			this.txtActivityDate.Style = "text-align: left";
			this.txtActivityDate.Text = null;
			this.txtActivityDate.Top = 0F;
			this.txtActivityDate.Width = 0.8645833F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1666667F;
			this.txtType.Left = 1F;
			this.txtType.Name = "txtType";
			this.txtType.Text = null;
			this.txtType.Top = 0F;
			this.txtType.Width = 1.739583F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1666667F;
			this.txtDescription.Left = 2.8125F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0F;
			this.txtDescription.Width = 4.614583F;
			// 
			// srptViolation
			//
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtActivityDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtActivityDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
	}
}
