﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptOpenPermits.
	/// </summary>
	partial class rptOpenPermits
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptOpenPermits));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.txtID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDateFiled = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.txtTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtRange = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtGroupCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtID,
            this.txtDateFiled,
            this.txtAccount,
            this.txtLocation,
            this.txtStatus});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // txtID
            // 
            this.txtID.Height = 0.1770833F;
            this.txtID.Left = 0.1875F;
            this.txtID.Name = "txtID";
            this.txtID.Text = null;
            this.txtID.Top = 0F;
            this.txtID.Width = 0.96875F;
            // 
            // txtDateFiled
            // 
            this.txtDateFiled.Height = 0.1770833F;
            this.txtDateFiled.Left = 1.1875F;
            this.txtDateFiled.Name = "txtDateFiled";
            this.txtDateFiled.Text = null;
            this.txtDateFiled.Top = 0F;
            this.txtDateFiled.Width = 0.8125F;
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1770833F;
            this.txtAccount.Left = 2.0625F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "text-align: right";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 0F;
            this.txtAccount.Width = 0.6354167F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.1770833F;
            this.txtLocation.Left = 4.1875F;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 0F;
            this.txtLocation.Width = 3.229167F;
            // 
            // txtStatus
            // 
            this.txtStatus.Height = 0.1770833F;
            this.txtStatus.Left = 2.75F;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Text = null;
            this.txtStatus.Top = 0F;
            this.txtStatus.Width = 1.416667F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTotalCount,
            this.Label14,
            this.Line1});
            this.ReportFooter.Height = 0.2395833F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // txtTotalCount
            // 
            this.txtTotalCount.DataField = "TheCount";
            this.txtTotalCount.Height = 0.1770833F;
            this.txtTotalCount.Left = 3.9375F;
            this.txtTotalCount.Name = "txtTotalCount";
            this.txtTotalCount.Style = "text-align: right";
            this.txtTotalCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtTotalCount.SummaryGroup = "GroupHeader1";
            this.txtTotalCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtTotalCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.GrandTotal;
            this.txtTotalCount.Text = null;
            this.txtTotalCount.Top = 0.0625F;
            this.txtTotalCount.Width = 0.8333333F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1770833F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 3.25F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-weight: bold; text-align: left";
            this.Label14.Text = "Total";
            this.Label14.Top = 0.0625F;
            this.Label14.Width = 0.6145833F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 3.177083F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.02083333F;
            this.Line1.Width = 2.864584F;
            this.Line1.X1 = 3.177083F;
            this.Line1.X2 = 6.041667F;
            this.Line1.Y1 = 0.02083333F;
            this.Line1.Y2 = 0.02083333F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage,
            this.Label1,
            this.txtRange});
            this.PageHeader.Height = 0.5104167F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // txtMuni
            // 
            this.txtMuni.Height = 0.1875F;
            this.txtMuni.HyperLink = null;
            this.txtMuni.Left = 0F;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Style = "";
            this.txtMuni.Text = null;
            this.txtMuni.Top = 0.03125F;
            this.txtMuni.Width = 1.25F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.HyperLink = null;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.21875F;
            this.txtTime.Width = 1.25F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.HyperLink = null;
            this.txtDate.Left = 6.1875F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0.03125F;
            this.txtDate.Width = 1.25F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.HyperLink = null;
            this.txtPage.Left = 6.1875F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.21875F;
            this.txtPage.Width = 1.25F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.21875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 1.25F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.Label1.Text = "Open Permits";
            this.Label1.Top = 0.03125F;
            this.Label1.Width = 4.9375F;
            // 
            // txtRange
            // 
            this.txtRange.Height = 0.2083333F;
            this.txtRange.Left = 1.25F;
            this.txtRange.Name = "txtRange";
            this.txtRange.Text = null;
            this.txtRange.Top = 0.25F;
            this.txtRange.Width = 4.9375F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.txtType,
            this.Label5});
            this.GroupHeader1.DataField = "GroupHeader1";
            this.GroupHeader1.Height = 0.5104167F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
            // 
            // Label8
            // 
            this.Label8.Height = 0.1770833F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.1875F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-weight: bold; text-align: left";
            this.Label8.Text = "Permit";
            this.Label8.Top = 0.3125F;
            this.Label8.Width = 0.96875F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1770833F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 1.1875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-weight: bold";
            this.Label9.Text = "Date Filed";
            this.Label9.Top = 0.3125F;
            this.Label9.Width = 0.8125F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1770833F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-weight: bold";
            this.Label10.Text = "Type";
            this.Label10.Top = 0.0625F;
            this.Label10.Width = 0.4791667F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1979167F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 2.0625F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-weight: bold; text-align: right";
            this.Label11.Text = "Account";
            this.Label11.Top = 0.3125F;
            this.Label11.Width = 0.6354167F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1979167F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 4.1875F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-weight: bold";
            this.Label12.Text = "Location";
            this.Label12.Top = 0.3125F;
            this.Label12.Width = 0.7604167F;
            // 
            // txtType
            // 
            this.txtType.Height = 0.1770833F;
            this.txtType.Left = 0.5625F;
            this.txtType.Name = "txtType";
            this.txtType.Text = null;
            this.txtType.Top = 0.0625F;
            this.txtType.Width = 1.729167F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1770833F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 2.75F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-weight: bold";
            this.Label5.Text = "Status";
            this.Label5.Top = 0.3125F;
            this.Label5.Width = 1.416667F;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroupCount,
            this.Label13});
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtGroupCount
            // 
            this.txtGroupCount.DataField = "TheCount";
            this.txtGroupCount.Height = 0.1770833F;
            this.txtGroupCount.Left = 3.9375F;
            this.txtGroupCount.Name = "txtGroupCount";
            this.txtGroupCount.Style = "text-align: right";
            this.txtGroupCount.SummaryFunc = GrapeCity.ActiveReports.SectionReportModel.SummaryFunc.Count;
            this.txtGroupCount.SummaryGroup = "GroupHeader1";
            this.txtGroupCount.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.Group;
            this.txtGroupCount.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.SubTotal;
            this.txtGroupCount.Text = null;
            this.txtGroupCount.Top = 0.04166667F;
            this.txtGroupCount.Width = 0.8333333F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1770833F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 3.25F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-weight: bold; text-align: left";
            this.Label13.Text = "Count";
            this.Label13.Top = 0.04166667F;
            this.Label13.Width = 0.6666667F;
            // 
            // rptOpenPermits
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.489583F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
            ((System.ComponentModel.ISupportInitialize)(this.txtID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFiled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtID;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateFiled;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtRange;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
	}
}
