//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmUserCodes : BaseForm
	{
		public frmUserCodes()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmUserCodes InstancePtr
		{
			get
			{
				return (frmUserCodes)Sys.GetInstance(typeof(frmUserCodes));
			}
		}

		protected frmUserCodes _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By Corey
		// Date   03/31/2005
		// ********************************************************
		const int CNSTGRIDCODECOLORDERNO = 0;
		const int CNSTGRIDCODECOLDESCRIPTION = 1;
		const int CNSTGRIDCODECOLSHORTDESCRIPTION = 2;
		const int CNSTGRIDCODECOLUSEVALUE = 3;
		const int CNSTGRIDCODECOLVALUEFORMAT = 4;
		const int CNSTGRIDCODECOLMINVALUE = 5;
		const int CNSTGRIDCODECOLMAXVALUE = 6;
		const int CNSTGRIDCODECOLUSEDATE = 7;
		const int CNSTGRIDCODECOLMANDATORY = 8;
		const int CNSTGRIDCODECOLDROPDOWNTYPE = 9;
		const int CNSTGRIDCODECOLCODETYPE = 10;
		const int CNSTGRIDCODECOLAUTOID = 11;
		const int CNSTGRIDCODECOLCODEID = 12;
		const int CNSTGRIDPARAMETERSCOLDESC = 0;
		const int CNSTGRIDPARAMETERSCOLVALUE = 1;
		// Private Const CNSTGRIDPARAMETERSROWUSEVALUE = 0
		// Private Const CNSTGRIDPARAMETERSROWUSEDATE = 1
		const int CNSTGRIDPARAMETERSROWVALUEFORMAT = 0;
		const int CNSTGRIDPARAMETERSROWMANDATORY = 1;
		const int CNSTGRIDPARAMETERSROWMIN = 2;
		const int CNSTGRIDPARAMETERSROWMAX = 3;
		const int CNSTGRIDCODEDESCSCOLAUTOID = 0;
		const int CNSTGRIDCODEDESCSCOLCODE = 1;
		const int CNSTGRIDCODEDESCSCOLDESCRIPTION = 2;
		const int CNSTGRIDCODEDESCSCOLSHORTDESCRIPTION = 3;
		const int CNSTGRIDCODEDESCSCOLCODETYPE = 4;
		const int CNSTGRIDCODEDESCSCOLFIELDID = 5;
		private int lngNextAutoID;
		private bool boolDataChanged;
		private bool boolCantEdit;

		private void cmbCodeGroup_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbCodeGroup.SelectedIndex >= 0)
			{
				ShowCodeType(cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex));
			}
			else
			{
				GridPermitType.Visible = false;
				lblPermitType.Visible = false;
				gridViolationType.Visible = false;
				lblViolationType.Visible = false;
			}
		}

		private void frmUserCodes_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmUserCodes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUserCodes properties;
			//frmUserCodes.FillStyle	= 0;
			//frmUserCodes.ScaleWidth	= 9300;
			//frmUserCodes.ScaleHeight	= 7860;
			//frmUserCodes.LinkTopic	= "Form2";
			//frmUserCodes.LockControls	= -1  'True;
			//frmUserCodes.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngNextAutoID = -1;
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITDESCRIPTIONSECURITY, false))
			{
				boolCantEdit = true;
				cmdAddCode.Enabled = false;
				cmdAddCodeDescription.Enabled = false;
				cmdDeleteCode.Enabled = false;
				cmdDeleteCodeDescription.Enabled = false;
				mnuSave.Enabled = false;
				mnuSaveExit.Enabled = false;
				GridCodeDescs.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			SetupGridCodes();
			SetupcmbCodeGroup();
			SetupGridParameters();
			LoadGridCodes();
			LoadGridCodeDescs();
			if (cmbCodeGroup.Items.Count > 0)
			{
				cmbCodeGroup.SelectedIndex = 0;
			}
			boolDataChanged = false;
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveInfo())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmUserCodes_Resize(object sender, System.EventArgs e)
		{
			ResizeGridCodes();
			ResizeGridParameters();
			ResizeGridCodeDescs();
		}

		private void GridCodeDescs_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void GridCodeDescs_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridCodeDescs.Row > 0)
						{
							DeleteCodeDesc(GridCodeDescs.Row);
						}
						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						AddCodeDesc();
						break;
					}
			}
			//end switch
		}

		private void GridCodeDescs_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridCodeDescs.MouseRow;
			if (mRow < 0)
			{
				GridCodeDescs.Row = 0;
				GridCodeDescs.Focus();
			}
		}

		private void GridCodes_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
		}

		private void GridCodes_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridCodes.Row > 0)
						{
							DeleteCode(GridCodes.Row);
						}
						break;
					}
				case Keys.Insert:
					{
						KeyCode = 0;
						AddCode();
						break;
					}
			}
			//end switch
		}

		private void AddCodeDesc()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				if (cmbCodeGroup.SelectedIndex < 0)
					return;
				if (GridCodes.Row < 0)
					return;
				boolDataChanged = true;
				GridCodeDescs.Rows += 1;
				lngRow = GridCodeDescs.Rows - 1;
				GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLAUTOID, FCConvert.ToString(0));
				GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLFIELDID, FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(GridCodes.Row, CNSTGRIDCODECOLAUTOID))));
				GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODETYPE, FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(GridCodes.Row, CNSTGRIDCODECOLCODETYPE))));
				GridCodeDescs.TopRow = lngRow;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddCodeDesc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AddCode()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				int lngCategory;
				if (cmbCodeGroup.SelectedIndex < 0)
					return;
				boolDataChanged = true;
				lngCategory = cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex);
				lngNextAutoID -= 1;
				GridCodes.Rows += 1;
				lngRow = GridCodes.Rows - 1;
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLAUTOID, FCConvert.ToString(lngNextAutoID));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE, FCConvert.ToString(lngCategory));
				if (lngCategory != modCEConstants.CNSTCODETYPEPERMIT && lngCategory != modCEConstants.CNSTCODETYPEINSPECTION && lngCategory != modCEConstants.CNSTCODETYPEVIOLATION)
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID, FCConvert.ToString(0));
				}
				else if (lngCategory == modCEConstants.CNSTCODETYPEVIOLATION)
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID, FCConvert.ToString(Conversion.Val(gridViolationType.TextMatrix(0, 0))));
				}
				else
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID, FCConvert.ToString(Conversion.Val(GridPermitType.TextMatrix(0, 0))));
				}
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLDROPDOWNTYPE, FCConvert.ToString(0));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMANDATORY, FCConvert.ToString(false));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMAXVALUE, FCConvert.ToString(0));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMINVALUE, FCConvert.ToString(0));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEDATE, FCConvert.ToString(false));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEVALUE, FCConvert.ToString(true));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPELONG));
				GridCodes.Row = lngRow;
				GridCodes.TopRow = lngRow;
				ShowParameters(lngRow);
				ShowCodeDescs(lngNextAutoID);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In AddCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void DeleteCodeDesc(int lngRow)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngAutoID = 0;
				if (lngRow > 0)
				{
					boolDataChanged = true;
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLAUTOID))));
					GridDeletedCodeDescs.AddItem(FCConvert.ToString(lngAutoID) + "\t" + GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLDESCRIPTION) + "\t" + GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODE));
					GridCodeDescs.RemoveItem(lngRow);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteCodeDesc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void DeleteCode(int lngRow)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngAutoID = 0;
				clsDRWrapper clsLoad = new clsDRWrapper();
				int x;
				if (lngRow > 0)
				{
					boolDataChanged = true;
					lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLAUTOID))));
					if (lngAutoID > 0)
					{
						// if it already exists
						clsLoad.OpenRecordset("select top 1 account from usercodevalues where fieldid = " + FCConvert.ToString(lngAutoID), modGlobalVariables.Statics.strCEDatabase);
						if (!clsLoad.EndOfFile())
						{
							if (MessageBox.Show("This code is being used by at least one account" + "\r\n" + "Are you sure you want to delete it?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
							}
							else
							{
								return;
							}
						}
					}
					gridDelete.AddItem(FCConvert.ToString(lngAutoID) + "\t" + GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLDESCRIPTION) + "\t" + GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE));
					GridCodes.RemoveItem(lngRow);
					// now get rid of the rows in the grids
					for (x = GridCodeDescs.Rows - 1; x >= 1; x--)
					{
						if (Conversion.Val(GridCodeDescs.TextMatrix(x, CNSTGRIDCODEDESCSCOLFIELDID)) == lngAutoID)
						{
							GridCodeDescs.RemoveItem(x);
						}
					}
					// x
					// refresh the screen
					if (cmbCodeGroup.SelectedIndex >= 0)
					{
						ShowCodeType(cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex));
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DeleteCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridCodes_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridCodes.MouseRow;
			if (mRow < 0)
			{
				if (GridCodes.Row < 1)
				{
					GridCodes.Row = 0;
				}
				GridCodes.Focus();
			}
		}

		private void GridCodes_RowColChange(object sender, System.EventArgs e)
		{
            //FC:FINAL:AM:#1659 - when deleting a row CurrentCell is null and the Row property is not updated
            if (GridCodes.CurrentCell != null)
            {
                ShowParameters(GridCodes.Row);
                ShowCodeDescs(FCConvert.ToInt32(Conversion.Val(GridCodes.TextMatrix(GridCodes.Row, CNSTGRIDCODECOLAUTOID))));
            }
		}

		private void ShowParameters(int lngRow)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngRow <= 0)
					return;
				// If CBool(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEVALUE)) Then
				// .TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE) = "Yes"
				// Else
				// .TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE) = "No"
				// End If
				// If CBool(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEDATE)) Then
				// .TextMatrix(CNSTGRIDPARAMETERSROWUSEDATE, CNSTGRIDPARAMETERSCOLVALUE) = "Yes"
				// Else
				// .TextMatrix(CNSTGRIDPARAMETERSROWUSEDATE, CNSTGRIDPARAMETERSCOLVALUE) = "No"
				// End If
				GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMIN, CNSTGRIDPARAMETERSCOLVALUE, FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMINVALUE))));
				GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMAX, CNSTGRIDPARAMETERSCOLVALUE, FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMAXVALUE))));
				if (FCConvert.CBool(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMANDATORY)))
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMANDATORY, CNSTGRIDPARAMETERSCOLVALUE, "Yes");
				}
				else
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMANDATORY, CNSTGRIDPARAMETERSCOLVALUE, "No");
				}
				GridCodeDescs.Visible = false;
				cmdAddCodeDescription.Visible = false;
				if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					// shouldn't be used at this time
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Date");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
				}
				else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Decimal Number");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, false);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, false);
				}
				else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Code");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
					GridCodeDescs.Visible = true;
					cmdAddCodeDescription.Visible = true;
				}
				else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Whole Number");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, false);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, false);
				}
				else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Text");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
				}
				else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Yes/No");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
				}
				else
				{
					GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Text");
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
					GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ShowParameters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void UpdateParameters(int lngRow)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (lngRow < 1)
					return;
				// If UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE)) = "YES" Then
				// .TextMatrix(lngRow, CNSTGRIDCODECOLUSEVALUE) = True
				// Else
				// .TextMatrix(lngRow, CNSTGRIDCODECOLUSEVALUE) = False
				// End If
				// If UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWUSEDATE, CNSTGRIDPARAMETERSCOLVALUE)) = "YES" Then
				// .TextMatrix(lngRow, CNSTGRIDCODECOLUSEDATE) = True
				// Else
				// .TextMatrix(lngRow, CNSTGRIDCODECOLUSEDATE) = False
				// End If
				if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMANDATORY, CNSTGRIDPARAMETERSCOLVALUE)) == "YES")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMANDATORY, FCConvert.ToString(true));
				}
				else
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMANDATORY, FCConvert.ToString(false));
				}
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMAXVALUE, FCConvert.ToString(Conversion.Val(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMAX, CNSTGRIDPARAMETERSCOLVALUE))));
				GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMINVALUE, FCConvert.ToString(Conversion.Val(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMIN, CNSTGRIDPARAMETERSCOLVALUE))));
				if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "DECIMAL NUMBER")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPEDECIMAL));
				}
				else if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "WHOLE NUMBER")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPELONG));
				}
				else if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "TEXT")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPETEXT));
				}
				else if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "CODE")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPEDROPDOWN));
				}
				else if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "DATE")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPEDATE));
				}
				else if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "YES/NO")
				{
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(modCEConstants.CNSTVALUETYPEBOOLEAN));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateParameters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridParameters_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDataChanged = true;
			UpdateParameters(GridCodes.Row);
		}

		private void GridParameters_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			string strTemp = "";
			switch (GridParameters.Row)
			{
				case CNSTGRIDPARAMETERSROWMANDATORY:
					{
						// , CNSTGRIDPARAMETERSROWUSEVALUE , CNSTGRIDPARAMETERSROWUSEDATE
						GridParameters.ComboList = "Yes|No";
						break;
					}
				case CNSTGRIDPARAMETERSROWVALUEFORMAT:
					{
						strTemp = "#" + FCConvert.ToString(modCEConstants.CNSTVALUETYPELONG) + ";Whole Number|#" + FCConvert.ToString(modCEConstants.CNSTVALUETYPEDECIMAL) + ";Decimal Number|#" + FCConvert.ToString(modCEConstants.CNSTVALUETYPETEXT) + ";Text|#" + FCConvert.ToString(modCEConstants.CNSTVALUETYPEDATE) + ";Date|#" + FCConvert.ToString(modCEConstants.CNSTVALUETYPEBOOLEAN) + ";Yes/No";
						strTemp += "|#" + FCConvert.ToString(modCEConstants.CNSTVALUETYPEDROPDOWN) + ";Code";
						GridParameters.ComboList = strTemp;
						break;
					}
				case CNSTGRIDPARAMETERSROWMIN:
				case CNSTGRIDPARAMETERSROWMAX:
					{
						GridParameters.ComboList = strTemp;
						break;
					}
			}
			//end switch
		}

		private void GridParameters_ComboCloseUp(object sender, EventArgs e)
		{
			switch (GridParameters.Row)
			{
			// Case CNSTGRIDPARAMETERSROWUSEVALUE
			// comboindex 0 = yes,1 = no
			// If .ComboIndex >= 0 Then
			// If .ComboIndex = 0 Then
			// .TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE) = "Yes"
			// If UCase(.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) <> "CODE" And UCase(.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) <> "TEXT" Then
			// .RowHidden(CNSTGRIDPARAMETERSROWMAX) = False
			// .RowHidden(CNSTGRIDPARAMETERSROWMIN) = False
			// End If
			// .RowHidden(CNSTGRIDPARAMETERSROWVALUEFORMAT) = False
			// Else
			// .TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE) = "No"
			// .RowHidden(CNSTGRIDPARAMETERSROWMAX) = True
			// .RowHidden(CNSTGRIDPARAMETERSROWMIN) = True
			// .RowHidden(CNSTGRIDPARAMETERSROWVALUEFORMAT) = True
			// End If
			// If GridCodes.Row > 0 Then
			// DoEvents
			// ShowCodeDescs (GridCodes.TextMatrix(GridCodes.Row, CNSTGRIDCODECOLAUTOID))
			// End If
			// Else
			// .RowHidden(CNSTGRIDPARAMETERSROWMAX) = True
			// .RowHidden(CNSTGRIDPARAMETERSROWMIN) = True
			// .RowHidden(CNSTGRIDPARAMETERSROWVALUEFORMAT) = True
			// End If
				case CNSTGRIDPARAMETERSROWVALUEFORMAT:
					{
						GridCodeDescs.Visible = false;
						cmdAddCodeDescription.Visible = false;
						switch (GridParameters.ComboIndex)
						{
							case modCEConstants.CNSTVALUETYPELONG:
								{
									// long
									GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Whole Number");
									// If UCase(.TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE)) = "YES" Then
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, false);
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, false);
									// End If
									break;
								}
							case modCEConstants.CNSTVALUETYPEDECIMAL:
								{
									// double
									GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Decimal Number");
									// If UCase(.TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE)) = "YES" Then
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, false);
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, false);
									// End If
									break;
								}
							case modCEConstants.CNSTVALUETYPETEXT:
								{
									// text
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
									GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Text");
									break;
								}
							case modCEConstants.CNSTVALUETYPEDROPDOWN:
								{
									// dropdown
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
									GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Code");
									GridCodeDescs.Visible = true;
									cmdAddCodeDescription.Visible = true;
									break;
								}
							case modCEConstants.CNSTVALUETYPEDATE:
								{
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
									GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Date");
									break;
								}
							case modCEConstants.CNSTVALUETYPEBOOLEAN:
								{
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMAX, true);
									GridParameters.RowHidden(CNSTGRIDPARAMETERSROWMIN, true);
									GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE, "Yes/No");
									break;
								}
						}
						//end switch
						if (GridCodes.Row > 0)
						{
							//App.DoEvents();
							ShowCodeDescs(FCConvert.ToInt32(GridCodes.TextMatrix(GridCodes.Row, CNSTGRIDCODECOLAUTOID)));
						}
						break;
					}
			}
			//end switch
		}

		private void GridParameters_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// boolDataChanged = True
			// UpdateParameters (GridCodes.Row)
		}

		private void GridPermitType_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (cmbCodeGroup.SelectedIndex >= 0)
			{
				//App.DoEvents();
				ShowCodeType(cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex));
			}
		}

		private void GridPermitType_ComboCloseUp(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string)
			int lngID;
			lngID = FCConvert.ToInt32(GridPermitType.ComboData(GridPermitType.ComboIndex));
			if (lngID > 0)
			{
				if (cmbCodeGroup.SelectedIndex >= 0)
				{
					GridPermitType.EndEdit();
					//App.DoEvents();
					ShowCodeType(cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex));
				}
			}
		}

		private void gridViolationType_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (cmbCodeGroup.SelectedIndex >= 0)
			{
				//App.DoEvents();
				ShowCodeType(cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex));
			}
		}

		private void gridViolationType_ComboCloseUp(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string)
			int lngID;
			lngID = FCConvert.ToInt32(gridViolationType.ComboData(gridViolationType.ComboIndex));
			if (lngID > 0)
			{
				if (cmbCodeGroup.SelectedIndex >= 0)
				{
					gridViolationType.EndEdit();
					//App.DoEvents();
					ShowCodeType(cmbCodeGroup.ItemData(cmbCodeGroup.SelectedIndex));
				}
			}
		}

		private void mnuAddCode_Click(object sender, System.EventArgs e)
		{
			AddCode();
		}

		private void mnuAddCodeDescription_Click(object sender, System.EventArgs e)
		{
			AddCodeDesc();
		}

		private void mnuDeleteCode_Click(object sender, System.EventArgs e)
		{
			if (GridCodes.Row > 0)
			{
				DeleteCode(GridCodes.Row);
			}
		}

		private void mnuDeleteCodeDescription_Click(object sender, System.EventArgs e)
		{
			if (GridCodeDescs.Row > 0)
			{
				DeleteCodeDesc(GridCodeDescs.Row);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGridCodes()
		{
			// vbPorter upgrade warning: GridWidth As object	OnWriteFCConvert.ToInt32(
			int GridWidth = GridCodes.WidthOriginal;
			GridCodes.ColWidth(CNSTGRIDCODECOLORDERNO, FCConvert.ToInt32(0.08 * GridWidth));
			GridCodes.ColWidth(CNSTGRIDCODECOLDESCRIPTION, FCConvert.ToInt32(0.5 * GridWidth));
		}

		private void SetupcmbCodeGroup()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp;
			int lngFirst;
			lngFirst = 0;
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description ", modGlobalVariables.Statics.strCEDatabase);
			strTemp = "";
			while (!rsLoad.EndOfFile())
			{
				if (lngFirst == 0)
					lngFirst = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))));
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			GridPermitType.ColComboList(0, strTemp);
			if (lngFirst > 0)
			{
				GridPermitType.TextMatrix(0, 0, FCConvert.ToString(lngFirst));
			}
			cmbCodeGroup.Clear();
			cmbCodeGroup.AddItem("Contractor");
			cmbCodeGroup.ItemData(cmbCodeGroup.NewIndex, modCEConstants.CNSTCODETYPECONTRACTOR);
			if (strTemp != string.Empty)
			{
				cmbCodeGroup.AddItem("Inspection");
				cmbCodeGroup.ItemData(cmbCodeGroup.NewIndex, modCEConstants.CNSTCODETYPEINSPECTION);
				cmbCodeGroup.AddItem("Permit");
				cmbCodeGroup.ItemData(cmbCodeGroup.NewIndex, modCEConstants.CNSTCODETYPEPERMIT);
			}
			cmbCodeGroup.AddItem("Property");
			cmbCodeGroup.ItemData(cmbCodeGroup.NewIndex, modCEConstants.CNSTCODETYPEPROPERTY);
			lngFirst = 0;
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE) + " order by Description ", modGlobalVariables.Statics.strCEDatabase);
			strTemp = "";
			while (!rsLoad.EndOfFile())
			{
				if (lngFirst == 0)
					lngFirst = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ID"))));
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				gridViolationType.ColComboList(0, strTemp);
				if (lngFirst > 0)
				{
					gridViolationType.TextMatrix(0, 0, FCConvert.ToString(lngFirst));
				}
				cmbCodeGroup.AddItem("Violation");
				cmbCodeGroup.ItemData(cmbCodeGroup.NewIndex, modCEConstants.CNSTCODETYPEVIOLATION);
			}
		}

		private void SetupGridCodes()
		{
			GridCodes.Rows = 1;
			GridCodes.ColHidden(CNSTGRIDCODECOLAUTOID, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLCODETYPE, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLDROPDOWNTYPE, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLMANDATORY, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLMAXVALUE, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLMINVALUE, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLUSEDATE, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLUSEVALUE, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLVALUEFORMAT, true);
			GridCodes.ColHidden(CNSTGRIDCODECOLCODEID, true);
			GridCodes.ColDataType(CNSTGRIDCODECOLUSEDATE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridCodes.ColDataType(CNSTGRIDCODECOLUSEVALUE, FCGrid.DataTypeSettings.flexDTBoolean);
			GridCodes.ColDataType(CNSTGRIDCODECOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
			GridCodes.TextMatrix(0, CNSTGRIDCODECOLDESCRIPTION, "Description");
			GridCodes.TextMatrix(0, CNSTGRIDCODECOLSHORTDESCRIPTION, "Short Description");
			GridCodes.TextMatrix(0, CNSTGRIDCODECOLORDERNO, "");
			if (boolCantEdit)
			{
				GridCodes.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void LoadGridCodes()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SetupGridCodes();
				strSQL = "Select * from usercodes where editable = 1 ORDER by  codetype,orderno ";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCECostFilesDatabase);
				while (!clsLoad.EndOfFile())
				{
					GridCodes.Rows += 1;
					lngRow = GridCodes.Rows - 1;
					GridCodes.RowData(lngRow, false);
					// edited
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("codetype"))));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLSHORTDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields("shortdescription")));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLDROPDOWNTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("dropdowntype"))));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMANDATORY, FCConvert.ToString(clsLoad.Get_Fields_Boolean("mandatory")));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMAXVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("maxvalue"))));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMINVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("minvalue"))));
					// .TextMatrix(lngRow, CNSTGRIDCODECOLORDERNO) = Val(clsLoad.Fields("orderno"))
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEDATE, FCConvert.ToString(clsLoad.Get_Fields_Boolean("usedate")));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEVALUE, FCConvert.ToString(clsLoad.Get_Fields_Boolean("usevalue")));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("valueformat"))));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("subtype"))));
					GridCodes.RowHidden(lngRow, true);
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadGridCodeDescs()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				SetupGridCodeDescs();
				strSQL = "Select * from codedescriptions order by type,fieldid,code";
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					GridCodeDescs.Rows += 1;
					lngRow = GridCodeDescs.Rows - 1;
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLAUTOID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))));
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))));
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODETYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("type"))));
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLFIELDID, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fieldid"))));
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLSHORTDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields("shortdescription")));
					GridCodeDescs.RowHidden(lngRow, true);
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridCodeDescs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowCodeType(int lngFieldID)
		{
			int lngRow;
			int lngRowToShow;
			lngRowToShow = 0;
			if (lngFieldID != modCEConstants.CNSTCODETYPEINSPECTION && lngFieldID != modCEConstants.CNSTCODETYPEPERMIT)
			{
				GridPermitType.Visible = false;
				lblPermitType.Visible = false;
			}
			else
			{
				GridPermitType.Visible = true;
				lblPermitType.Visible = true;
			}
			if (lngFieldID != modCEConstants.CNSTCODETYPEVIOLATION)
			{
				gridViolationType.Visible = false;
				lblViolationType.Visible = false;
			}
			else
			{
				gridViolationType.Visible = true;
				lblViolationType.Visible = true;
			}
			for (lngRow = 1; lngRow <= GridCodes.Rows - 1; lngRow++)
			{
				if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) != lngFieldID)
				{
					GridCodes.RowHidden(lngRow, true);
				}
				else
				{
					if (lngFieldID != modCEConstants.CNSTCODETYPEINSPECTION && lngFieldID != modCEConstants.CNSTCODETYPEPERMIT && lngFieldID != modCEConstants.CNSTCODETYPEVIOLATION)
					{
						GridCodes.RowHidden(lngRow, false);
						if (lngRowToShow < 1)
							lngRowToShow = lngRow;
					}
					else if (lngFieldID == modCEConstants.CNSTCODETYPEVIOLATION)
					{
						if (Conversion.Val(gridViolationType.TextMatrix(0, 0)) != Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID)))
						{
							GridCodes.RowHidden(lngRow, true);
						}
						else
						{
							GridCodes.RowHidden(lngRow, false);
							if (lngRowToShow < 1)
								lngRowToShow = lngRow;
						}
					}
					else
					{
						if (Conversion.Val(GridPermitType.TextMatrix(0, 0)) != Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID)))
						{
							GridCodes.RowHidden(lngRow, true);
						}
						else
						{
							GridCodes.RowHidden(lngRow, false);
							if (lngRowToShow < 1)
								lngRowToShow = lngRow;
						}
					}
				}
			}
			// lngRow
			if (lngRowToShow > 0)
			{
				GridCodes.Row = lngRowToShow;
			}
			else
			{
				ShowCodeDescs(0);
				// will blank that form
			}
		}
		// vbPorter upgrade warning: lngFieldID As int	OnWrite(int, double, string)
		private void ShowCodeDescs(int lngFieldID)
		{
			int lngRow;
			//App.DoEvents();
			for (lngRow = 1; lngRow <= GridCodeDescs.Rows - 1; lngRow++)
			{
				if (Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLFIELDID)) != lngFieldID)
				{
					GridCodeDescs.RowHidden(lngRow, true);
				}
				else
				{
					if (fecherFoundation.Strings.UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLVALUE)) == "CODE")
					{
						// And UCase(GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLVALUE)) = "YES" Then
						GridCodeDescs.RowHidden(lngRow, false);
					}
					else
					{
						GridCodeDescs.RowHidden(lngRow, true);
					}
				}
			}
			// lngRow
		}

		private void SetupGridParameters()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridParameters.Rows = 4;
				GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMANDATORY, CNSTGRIDPARAMETERSCOLDESC, "Mandatory");
				GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMAX, CNSTGRIDPARAMETERSCOLDESC, "Max");
				GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWMIN, CNSTGRIDPARAMETERSCOLDESC, "Min");
				// .TextMatrix(CNSTGRIDPARAMETERSROWUSEDATE, CNSTGRIDPARAMETERSCOLDESC) = "Use Date"
				// .TextMatrix(CNSTGRIDPARAMETERSROWUSEVALUE, CNSTGRIDPARAMETERSCOLDESC) = "Use Value"
				GridParameters.TextMatrix(CNSTGRIDPARAMETERSROWVALUEFORMAT, CNSTGRIDPARAMETERSCOLDESC, "Value Type");
				if (boolCantEdit)
				{
					GridParameters.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridParameters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridParameters()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int GridWidth = 0;
				GridWidth = GridParameters.WidthOriginal;
				GridParameters.ColWidth(CNSTGRIDPARAMETERSCOLDESC, FCConvert.ToInt32(0.5 * GridWidth));
				//FC:FINAL:DDU: Resolved at design part
				//GridParameters.Height = GridParameters.RowHeight(0) * 6 + 50;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ResizeGridParameters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridCodeDescs()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				GridCodeDescs.Rows = 1;
				GridCodeDescs.Cols = 6;
				GridCodeDescs.ColHidden(CNSTGRIDCODEDESCSCOLAUTOID, true);
				GridCodeDescs.ColHidden(CNSTGRIDCODEDESCSCOLCODETYPE, true);
				GridCodeDescs.ColHidden(CNSTGRIDCODEDESCSCOLFIELDID, true);
				GridCodeDescs.TextMatrix(0, CNSTGRIDCODEDESCSCOLCODE, "Code");
				GridCodeDescs.TextMatrix(0, CNSTGRIDCODEDESCSCOLDESCRIPTION, "Description");
				GridCodeDescs.TextMatrix(0, CNSTGRIDCODEDESCSCOLSHORTDESCRIPTION, "Short Desc");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridCodeDescs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ResizeGridCodeDescs()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int GridWidth = 0;
				GridWidth = GridCodeDescs.WidthOriginal;
				GridCodeDescs.ColWidth(CNSTGRIDCODEDESCSCOLCODE, FCConvert.ToInt32(0.15 * GridWidth));
				GridCodeDescs.ColWidth(CNSTGRIDCODEDESCSCOLDESCRIPTION, FCConvert.ToInt32(0.55 * GridWidth));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In ResizeGridCodeDescs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			// vbPorter upgrade warning: lngAutoID As object	OnWrite(double, object)
			int lngAutoID = 0;
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngRow;
			int lngNewID = 0;
			int lngOrder = 0;
			int lngLastCategory;
			int lngLastOrder;
			string strCategory = "";
			int lngLastCode;
			SaveInfo = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				for (x = 0; x <= gridDelete.Rows - 1; x++)
				{
					lngAutoID = FCConvert.ToInt32(gridDelete.TextMatrix(x, 0));
					if (lngAutoID > 0)
					{
						modGlobalFunctions.AddCYAEntry_6("CE", "Deleted Code " + gridDelete.TextMatrix(x, 1), "Category " + gridDelete.TextMatrix(x, 2));
						clsSave.Execute("delete from usercodes where ID = " + lngAutoID, modGlobalVariables.Statics.strCEDatabase);
						clsSave.Execute("delete from usercodevalues where fieldid = " + lngAutoID, modGlobalVariables.Statics.strCEDatabase);
						clsSave.Execute("delete from codedescriptions where fieldid = " + lngAutoID, modGlobalVariables.Statics.strCEDatabase);
					}
				}
				// x
				gridDelete.Rows = 0;
				for (x = 0; x <= GridDeletedCodeDescs.Rows - 1; x++)
				{
					lngAutoID = FCConvert.ToInt32(GridDeletedCodeDescs.TextMatrix(x, 0));
					if (lngAutoID > 0)
					{
						clsSave.Execute("delete from codedescriptions where ID = " + lngAutoID, modGlobalVariables.Statics.strCEDatabase);
					}
				}
				// x
				GridDeletedCodeDescs.Rows = 0;
				lngLastCategory = -1;
				lngLastOrder = -1;
				// GridCodes.Col = CNSTGRIDCODECOLORDERNO
				// GridCodes.Sort = flexSortNumericAscending
                //FC:FINAL:CHN - i.issue #1660: Grid not need in sorting.
				// GridCodes.Col = CNSTGRIDCODECOLCODETYPE;
				// GridCodes.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				// should now be in code type,order order
				for (lngRow = 1; lngRow <= GridCodes.Rows - 1; lngRow++)
				{
					strCategory = "";
					if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) == modCEConstants.CNSTCODETYPECONTRACTOR)
					{
						strCategory = "Contractor";
					}
					else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) == modCEConstants.CNSTCODETYPEINSPECTION)
					{
						strCategory = "Inspection";
					}
					else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) == modCEConstants.CNSTCODETYPEPERMIT)
					{
						strCategory = "Permit";
					}
					else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) == modCEConstants.CNSTCODETYPEPROPERTY)
					{
						strCategory = "Property";
					}
					else if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) == modCEConstants.CNSTCODETYPEVIOLATION)
					{
						strCategory = "Violation";
					}
					// If Val(.TextMatrix(lngRow, CNSTGRIDCODECOLORDERNO)) < 1 Then
					// MsgBox "Fields cannot have an order number < 1" & vbNewLine & strCategory & " Field " & .TextMatrix(lngRow, CNSTGRIDCODECOLDESCRIPTION) & " has an invalid order number", vbExclamation, "Bad Order Number"
					// Exit Function
					// End If
					if (Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE)) != lngLastCategory)
					{
						lngLastCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE))));
						lngLastOrder = -1;
						lngOrder = 1;
					}
					// lngOrder = Val(.TextMatrix(lngRow, CNSTGRIDCODECOLORDERNO))
					// If lngOrder = lngLastOrder Then
					// MsgBox "Fields cannot have the same order number" & vbNewLine & strCategory & " Field " & .TextMatrix(lngRow, CNSTGRIDCODECOLDESCRIPTION) & " has a duplicate order number", vbExclamation, "Bad Order Number"
					// Exit Function
					// End If
					// lngLastOrder = lngOrder
					lngAutoID = FCConvert.ToInt32(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLAUTOID));
					clsSave.OpenRecordset("select * from usercodes where ID = " + lngAutoID, modGlobalVariables.Statics.strCEDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
						lngNewID = 0;
					}
					else
					{
						clsSave.AddNew();
					}
					clsSave.Set_Fields("description", fecherFoundation.Strings.Trim(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLDESCRIPTION)));
					clsSave.Set_Fields("shortdescription", fecherFoundation.Strings.Trim(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLSHORTDESCRIPTION)));
					clsSave.Set_Fields("usevalue", FCConvert.CBool(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEVALUE)));
					clsSave.Set_Fields("usedate", FCConvert.CBool(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLUSEDATE)));
					clsSave.Set_Fields("mandatory", FCConvert.CBool(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMANDATORY)));
					clsSave.Set_Fields("codetype", FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODETYPE))));
					clsSave.Set_Fields("minvalue", FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMINVALUE))));
					clsSave.Set_Fields("maxvalue", FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLMAXVALUE))));
					clsSave.Set_Fields("subtype", FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLCODEID))));
					clsSave.Set_Fields("orderno", lngOrder);
					lngOrder += 1;
					clsSave.Set_Fields("valueformat", FCConvert.ToString(Conversion.Val(GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLVALUEFORMAT))));
					clsSave.Set_Fields("editable", true);
					clsSave.Update();
					lngNewID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
					GridCodes.TextMatrix(lngRow, CNSTGRIDCODECOLAUTOID, FCConvert.ToString(lngNewID));
					// update grid code descs with the new id
					if (lngNewID > 0)
					{
						for (x = 1; x <= GridCodeDescs.Rows - 1; x++)
						{
							if (Conversion.Val(GridCodeDescs.TextMatrix(x, CNSTGRIDCODEDESCSCOLFIELDID)) == lngAutoID)
							{
								GridCodeDescs.TextMatrix(x, CNSTGRIDCODEDESCSCOLFIELDID, FCConvert.ToString(lngNewID));
							}
						}
						// x
					}
				}
				// lngRow
				GridCodeDescs.Col = CNSTGRIDCODEDESCSCOLCODE;
				GridCodeDescs.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				GridCodeDescs.Col = CNSTGRIDCODEDESCSCOLFIELDID;
				GridCodeDescs.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				// should now be in field,code order
				lngLastOrder = -1;
				lngLastCode = 0;
				for (lngRow = 1; lngRow <= GridCodeDescs.Rows - 1; lngRow++)
				{
					if (lngLastCode != Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLFIELDID)))
					{
						lngLastCode = FCConvert.ToInt32(Math.Round(Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLFIELDID))));
						lngLastOrder = -1;
					}
					if (Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODE)) < 1)
					{
						MessageBox.Show("Description Codes must have a code number" + "\r\n" + "Code " + GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLDESCRIPTION) + " has no code number", "Bad Code Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
					lngOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODE))));
					if (lngOrder == lngLastOrder)
					{
						MessageBox.Show("Description Codes cannot have the same code number for any one field" + "\r\n" + "Code " + GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLDESCRIPTION) + " has a duplicate code number", "Duplicate Code Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
					lngLastOrder = lngOrder;
					lngAutoID = FCConvert.ToInt32(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLAUTOID));
					clsSave.OpenRecordset("select * from CODEdescriptions where ID = " + lngAutoID, modGlobalVariables.Statics.strCEDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
					}
					clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLFIELDID))));
					clsSave.Set_Fields("type", FCConvert.ToString(Conversion.Val(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLCODETYPE))));
					clsSave.Set_Fields("description", fecherFoundation.Strings.Trim(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLDESCRIPTION)));
					clsSave.Set_Fields("shortdescription", fecherFoundation.Strings.Trim(GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLSHORTDESCRIPTION)));
					clsSave.Set_Fields("Code", lngOrder);
					clsSave.Update();
					lngAutoID = clsSave.Get_Fields_Int32("ID");
					GridCodeDescs.TextMatrix(lngRow, CNSTGRIDCODEDESCSCOLAUTOID, FCConvert.ToString(lngAutoID));
				}
				// lngRow
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveInfo = true;
				boolDataChanged = false;
				modGlobalFunctions.AddCYAEntry_6("CE", "Saved User Codes");
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridCodeDescs.Row = 0;
			GridParameters.Col = 0;
			GridCodes.Col = CNSTGRIDCODECOLAUTOID;
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridCodeDescs.Row = 0;
			GridParameters.Col = 0;
			GridCodes.Col = CNSTGRIDCODECOLAUTOID;
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}
	}
}
