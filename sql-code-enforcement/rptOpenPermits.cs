//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptOpenPermits.
	/// </summary>
	public partial class rptOpenPermits : BaseSectionReport
	{
		public rptOpenPermits()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptOpenPermits InstancePtr
		{
			get
			{
				return (rptOpenPermits)Sys.GetInstance(typeof(rptOpenPermits));
			}
		}

		protected rptOpenPermits _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
				rsStatus.Dispose();
				rsType.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOpenPermits	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsReport = new clsDRWrapper();
		clsDRWrapper rsType = new clsDRWrapper();
		private clsDRWrapper rsStatus = new clsDRWrapper();

		public void Init(ref clsSQLStatement clsStatement)
		{
			string strSQL = "";
			string strWhere;
			strWhere = "appdencode <> " + FCConvert.ToString(modCEConstants.CNSTPERMITCOMPLETE) + " and not permits.deleted = 1";
			if (!(clsStatement == null))
			{
				// strSQL = clsStatement.SQLStatement
				// If boolDistinct Then
				if (clsStatement.WhereStatement != "")
				{
					strWhere = clsStatement.WhereStatement + " and " + strWhere;
				}
				else
				{
					strWhere = " where " + strWhere;
				}
				strSQL = "select distinct applicationdate,filedby,permitidentifier,permityear,permitnumber,originalstreetnum,originalstreet,permittype,appdencode,account from (" + clsStatement.SelectStatement + " " + strWhere + ") abc " + " order by permittype,applicationdate,permitidentifier";
				// End If
			}
			else
			{
				strSQL = "select permits.ID as PermitAutoid,account,PermitIdentifier,PermitYear,AppDenCode,ApplicationDate,Plan,FiledBy,ContactName,ContactEmail,StartDate,CompletionDate,EstimatedValue,Fee,PermitType,";
				strSQL += "PermitSubType,PERMITS.ContractorID AS PermitContractorID,CertificateOfOCCRequired,CertificateOfOCCDate,PermitNumber,Units,DwellUnits,NumBuildings,CommResCode,Description,OMBCode,PermitContractorName,";
				strSQL += "PermitContractorAddress1,PermitContractorAddress2,PermitContractorCity,PermitContractorState,PermitContractorZip,PermitContractorZip4,PermitContractorEmail,Completed,";
				strSQL += "permits.deleted as PermitDeleted,permits.comment as PermitComment,OriginalOwner,OriginalMapLot,OriginalStreetNum,OriginalStreet,OriginalOwnerFirst,OriginalOwnerLast,OriginalOwnerMiddle,OriginalOwnerDesig from permits where " + strWhere + " order by permittype,applicationdate,permitidentifier";
			}
			txtRange.Text = clsStatement.ParameterDescriptionText;
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
                //FC:FINAL:SBE - #i1749 - set correct groupping for group header
                //GroupHeader1.DataField = rsReport.Get_Fields_String("permittype");
                //this.Fields["GroupHeader1"].Value = rsReport.Get_Fields_String("permittype");
            }
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "Open Permits");
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("TheCount");
            Fields.Add("GroupHeader1");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				//FC:FINAL:SBE - #i1749 - set correct groupping for group header
                //GroupHeader1.DataField = rsReport.Get_Fields_String("permittype");
                this.Fields["GroupHeader1"].Value = rsReport.Get_Fields_String("permittype");
                this.Fields["TheCount"].Value = 1;
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			rsType.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
			rsStatus.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by code", modGlobalVariables.Statics.strCEDatabase);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				if (Information.IsDate(rsReport.Get_Fields("applicationdate")))
				{
                    //FC:FINAL:MSH - i.issue #1693: replace Convert by Get_Fields_DateTime to avoid conversion errors
                    //if (Convert.ToDateTime(rsReport.Get_Fields("applicationdate")).ToOADate() != 0)
                    if (rsReport.Get_Fields_DateTime("applicationdate").ToOADate() != 0)
					{
                        //FC:FINAL:MSH - i.issue #1693: get date in a short format (as in original)
						//txtDateFiled.Text = rsReport.Get_Fields("applicationdate");
						txtDateFiled.Text = rsReport.Get_Fields_DateTime("applicationdate").ToShortDateString();
					}
					else
					{
						txtDateFiled.Text = "";
					}
				}
				else
				{
					txtDateFiled.Text = "";
				}
				// txtFiler.Text = rsReport.Fields("filedby")
				if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("permitidentifier")) != string.Empty)
				{
					txtID.Text = rsReport.Get_Fields_String("permitidentifier");
				}
				else
				{
					txtID.Text = rsReport.Get_Fields_String("permityear") + "-" + rsReport.Get_Fields_String("permitnumber");
				}
				txtLocation.Text = fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("originalstreetnum") + " " + rsReport.Get_Fields_String("originalstreet"));
				if (Conversion.Val(rsReport.Get_Fields_String("account")) > 0)
				{
					txtAccount.Text = rsReport.Get_Fields_String("account");
				}
				else
				{
					txtAccount.Text = "";
				}
				// If rsType.FindFirstRecord("ID", Val(rsReport.Fields("permittype"))) Then
				// txtType.Text = rsType.Fields("description")
				// Else
				// txtType.Text = ""
				// End If
				if (rsStatus.FindFirstRecord("Code", Conversion.Val(rsReport.Get_Fields_String("appdencode"))))
				{
					txtStatus.Text = rsStatus.Get_Fields_String("description");
				}
				else
				{
					txtStatus.Text = "";
				}
				rsReport.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
            //FC:FINAL:SBE - #i1749 - set correct groupping for group header
            //if (rsType.FindFirstRecord("ID", GroupHeader1.DataField))
            if (rsType.FindFirstRecord("ID", this.Fields["GroupHeader1"].Value))
            {
                txtType.Text = rsType.Get_Fields_String("description");
            }
            else
            {
                txtType.Text = "";
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
