﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWCE0000
{
	public partial class frmRTBDocument : BaseForm
	{
		public frmRTBDocument()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmRTBDocument InstancePtr
		{
			get
			{
				return (frmRTBDocument)Sys.GetInstance(typeof(frmRTBDocument));
			}
		}

		protected frmRTBDocument _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngAccountNumber;
		private int lngParentID;
		private int intParentType;

		public void Init(int lngAccount, int ParentID, int ParentType)
		{
			lngParentID = ParentID;
			intParentType = ParentType;
			lngAccountNumber = lngAccount;
			this.Show(App.MainForm);
		}

		private void chkBold_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkBold.CheckState == Wisej.Web.CheckState.Checked)
			{
				rtb.SetFontBold(true);
			}
			else
			{
				rtb.SetFontBold(false);
			}
			rtb.Focus();
		}

		private void SetupIndentCombo()
		{
			cmbIndent.Clear();
			double x;
			for (x = 0; x <= 6; x += 0.5)
			{
				cmbIndent.AddItem(x.ToString());
			}
			// x
			cmbIndent.SelectedIndex = 0;
		}

		private void chkBulleted_CheckedChanged(object sender, System.EventArgs e)
		{
			//if (chkBulleted.CheckState==Wisej.Web.CheckState.Checked) {
			//	rtb.SelBullet = true;
			//} else {
			//	rtb.SelBullet = false;
			//}
			//if (rtb.Visible) rtb.Focus();
		}

		private void chkItalic_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkItalic.CheckState == Wisej.Web.CheckState.Checked)
			{
				rtb.SetFontItalic(true);
			}
			else
			{
				rtb.SetFontItalic(false);
			}
			if (rtb.Visible)
				rtb.Focus();
		}

		private void chkUnderline_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkUnderline.CheckState == Wisej.Web.CheckState.Checked)
			{
				rtb.SetFontUnderline(true);
			}
			else
			{
				rtb.SetFontUnderline(false);
			}
			if (rtb.Visible)
				rtb.Focus();
		}

		private void cmbAlignment_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAlignment.SelectedIndex >= 0)
			{
				switch (cmbAlignment.SelectedIndex)
				{
					case 0:
						{
							rtb.TextAlign = HorizontalAlignment.Left;
							break;
						}
					case 1:
						{
							rtb.TextAlign = HorizontalAlignment.Right;
							break;
						}
					case 2:
						{
							rtb.TextAlign = HorizontalAlignment.Center;
							break;
						}
					default:
						{
							rtb.TextAlign = HorizontalAlignment.Left;
							break;
						}
				}
				//end switch
			}
			if (rtb.Visible)
				rtb.Focus();
		}

		private void cmbFont_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFont.SelectedIndex >= 0)
			{
				rtb.SetFontName(cmbFont.Text);
				if (rtb.Visible == true)
					rtb.Focus();
			}
		}

		private void cmbFontColor_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFontColor.SelectedIndex >= 0)
			{
				if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "black")
				{
					rtb.ForeColor = Color.Black;
				}
				else if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "blue")
				{
					rtb.ForeColor = Color.Blue;
				}
				else if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "red")
				{
					rtb.ForeColor = Color.Red;
				}
				else if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "green")
				{
					rtb.ForeColor = Color.Lime;
				}
				else if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "yellow")
				{
					rtb.ForeColor = Color.Yellow;
				}
				else if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "cyan")
				{
					rtb.ForeColor = Color.Cyan;
				}
				else if (fecherFoundation.Strings.LCase(cmbFontColor.Text) == "magenta")
				{
					rtb.ForeColor = Color.Magenta;
				}
				else
				{
					rtb.ForeColor = Color.Black;
				}
				if (rtb.Visible)
					rtb.Focus();
			}
		}

		private void cmbFontSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFontSize.SelectedIndex >= 0)
			{
				rtb.SetFontSize(FCConvert.ToSingle(cmbFontSize.Text));
				if (rtb.Visible)
					rtb.Focus();
			}
		}

		private void cmbIndent_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbIndent.SelectedIndex >= 0)
			{
				rtb.SelHangingIndent = FCConvert.ToInt32(cmbIndent.Text);
				if (rtb.Visible)
					rtb.Focus();
			}
		}

		private void frmRTBDocument_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmRTBDocument_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRTBDocument properties;
			//frmRTBDocument.FillStyle	= 0;
			//frmRTBDocument.ScaleMode	= 5;
			//frmRTBDocument.ScaleWidth	= 6.45800018310547;
			//frmRTBDocument.ScaleHeight	= 5.44799995422363;
			//frmRTBDocument.LinkTopic	= "Form2";
			//frmRTBDocument.PaletteMode	= 1  'UseZOrder;
			//rtb properties;
			//rtb.ScrollBars	= 2;
			//rtb.BulletIndent	= 0.5;
			//rtb.AutoVerbMenu	= -1  'True;
			//rtb.TextRTF	= $"frmRTBDocument.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// rtb.Font.Size = 12
			SetupFontCombo();
			SetupFontSizeCombo();
			SetupFontColorCombo();
			SetupAlignmentCombo();
			SetupIndentCombo();
		}

		private void mnuAttach_Click(object sender, System.EventArgs e)
		{
			if (fecherFoundation.Strings.Trim(txtDescription.Text) == "")
			{
				MessageBox.Show("You must enter a description when attaching documents", "Invalid Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
            //FC:FINAL:IPI - #i1706 - on web the file will be downloaded
            string strFile;
            // get filename
            //strFile = PickFileName();
            //if (strFile != "")
            //  {
            //	   rptRichTextDoc.InstancePtr.Init(true, rtb.TextRTF, strFile, lngParentID, intParentType, lngAccountNumber, txtDescription.Text);
            //}  
            string folder = FCFileSystem.Statics.UserDataFolder + "\\RTBDocs\\";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            strFile = Path.Combine(folder, Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + ".pdf");

            rptRichTextDoc.InstancePtr.Init(true, rtb.TextRTF, strFile, lngParentID, intParentType, lngAccountNumber, txtDescription.Text);
        }

		private string PickFileName()
		{
			string PickFileName = "";
			string strFile = "";
			// Dim strCurDir As String
			// 
			// strCurDir = CurDir           'save the current directory so we can set it back
			fecherFoundation.Information.Err().Clear();
			// clear any errors
			// MDIParent.InstancePtr.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNPathMustExist	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
			/*? On Error Resume Next  */
			MDIParent.InstancePtr.CommonDialog1.Filter = "PDF Files |*.pdf";
			// set filter for pdf files
			MDIParent.InstancePtr.CommonDialog1.DefaultExt = "pdf";
			// .CommonDialog1.FileName = "*.pdf"
			// .CommonDialog1.ShowOpen
			MDIParent.InstancePtr.CommonDialog1.ShowSave();
			if (fecherFoundation.Information.Err().Number == 0)
			{
				strFile = MDIParent.InstancePtr.CommonDialog1.FileName;
				// if a file is selecte show it in the file label
				// imgPreview.View = 9                   'set the view of the image viewer control to force the image to fit in the window
				// If UCase(Right(.CommonDialog1.FileName, 3)) <> "PDF" Then
				// imgPreview.FileName = .CommonDialog1.FileName
				// Else
				// imgPreview.LoadMultiPage .CommonDialog1.FileName, 1   'load the first page of the file
				// End If
				// imgPreview.View = 9                   'set the view of the image viewer control to force the image to fit in the window
				PickFileName = strFile;
			}
			else
			{
				// ChDrive strCurDir   'if nothign is selected reset the drive and directory to what they were
				// ChDir strCurDir
				return PickFileName;
			}
			// ChDrive strCurDir    'reset the drive and directory to what they were
			// ChDir strCurDir
			return PickFileName;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupAlignmentCombo()
		{
			cmbAlignment.Clear();
			cmbAlignment.AddItem("Left Justify");
			cmbAlignment.AddItem("Right Justify");
			cmbAlignment.AddItem("Center");
			cmbAlignment.SelectedIndex = 0;
		}

		private void SetupFontColorCombo()
		{
			cmbFontColor.Clear();
			cmbFontColor.AddItem("Black");
			cmbFontColor.AddItem("Blue");
			cmbFontColor.AddItem("Green");
			cmbFontColor.AddItem("Red");
			cmbFontColor.AddItem("Yellow");
			cmbFontColor.AddItem("Cyan");
			cmbFontColor.AddItem("Magenta");
			cmbFontColor.SelectedIndex = 0;
		}

		private void SetupFontSizeCombo()
		{
			cmbFontSize.Clear();
			int x;
			cmbFontSize.Clear();
			cmbFontSize.AddItem("8");
			cmbFontSize.AddItem("9");
			for (x = 10; x <= 72; x += 2)
			{
				cmbFontSize.AddItem(x.ToString());
			}
			// x
			cmbFontSize.SelectedIndex = 3;
		}

		private void SetupFontCombo()
		{
			cmbFont.Clear();
			int x;
			int intFont = 0;
			intFont = 0;
			for (x = 0; x <= FCGlobal.Printer.FontCount - 1; x++)
			{
				cmbFont.AddItem(FCGlobal.Printer.Fonts[x]);
			}
			// x
			for (x = 0; x <= cmbFont.Items.Count - 1; x++)
			{
				if (fecherFoundation.Strings.LCase(cmbFont.Items[x].ToString()) == "tahoma")
				{
					intFont = x;
					break;
				}
				else
				{
					if (intFont == 0)
					{
						if (fecherFoundation.Strings.LCase(cmbFont.Items[x].ToString()) == "times new roman")
						{
							intFont = x;
						}
					}
				}
			}
			// x
			cmbFont.SelectedIndex = intFont;
		}

		private void SetFontSizeCombo(int intFontSize)
		{
			int x;
			for (x = 0; x <= cmbFontSize.Items.Count - 1; x++)
			{
				if (Conversion.Val(cmbFontSize.Items[x].ToString()) == intFontSize)
				{
					cmbFontSize.SelectedIndex = x;
					break;
				}
			}
			// x
		}

		private void SetFontCombo(string strFont)
		{
			int x;
			for (x = 0; x <= cmbFont.Items.Count - 1; x++)
			{
				if (fecherFoundation.Strings.LCase(cmbFont.Items[x].ToString()) == fecherFoundation.Strings.LCase(strFont))
				{
					cmbFont.SelectedIndex = x;
					break;
				}
			}
			// x
		}

		private void SetIndentCombo(double dblIndent)
		{
			int x;
			for (x = 0; x <= cmbIndent.Items.Count - 1; x++)
			{
				if (Conversion.Val(cmbIndent.Items[x].ToString()) == dblIndent)
				{
					cmbIndent.SelectedIndex = x;
					break;
				}
			}
			// x
		}

		private void mnuPrintPreview_Click(object sender, System.EventArgs e)
		{
			rptRichTextDoc.InstancePtr.Init(false, rtb.TextRTF, "", lngParentID, intParentType, lngAccountNumber, txtDescription.Text);
		}

		private void rtb_SelChange()
		{
			if (!fecherFoundation.FCUtils.IsNull(rtb.Font.Size))
			{
				SetFontSizeCombo(FCConvert.ToInt32(rtb.Font.Size));
			}
			if (!fecherFoundation.FCUtils.IsNull(rtb.Font.Name))
			{
				if (FCConvert.ToString(rtb.Font.Name) != "")
				{
					SetFontCombo(rtb.Font.Name);
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rtb.ForeColor))
			{
				if (rtb.ForeColor == Color.Black)
				{
					cmbFontColor.SelectedIndex = 0;
				}
				else if (rtb.ForeColor == Color.Blue)
				{
					cmbFontColor.SelectedIndex = 1;
				}
				else if (rtb.ForeColor == Color.Red)
				{
					cmbFontColor.SelectedIndex = 3;
				}
				else if (rtb.ForeColor == Color.Lime)
				{
					cmbFontColor.SelectedIndex = 2;
				}
				else if (rtb.ForeColor == Color.Yellow)
				{
					cmbFontColor.SelectedIndex = 4;
				}
				else if (rtb.ForeColor == Color.Cyan)
				{
					cmbFontColor.SelectedIndex = 5;
				}
				else if (rtb.ForeColor == Color.Magenta)
				{
					cmbFontColor.SelectedIndex = 6;
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rtb.Font.Bold))
			{
				bool vbPorterVar = rtb.Font.Bold;
				if (vbPorterVar == true)
				{
					chkBold.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkBold.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			if (!fecherFoundation.FCUtils.IsNull(rtb.Font.Italic))
			{
				switch (rtb.Font.Italic)
				{
					case true:
						{
							chkItalic.CheckState = Wisej.Web.CheckState.Checked;
							break;
						}
					default:
						{
							chkItalic.CheckState = Wisej.Web.CheckState.Unchecked;
							break;
						}
				}
				//end switch
			}
			if (!fecherFoundation.FCUtils.IsNull(rtb.Font.Underline))
			{
				switch (rtb.Font.Underline)
				{
					case true:
						{
							chkUnderline.CheckState = Wisej.Web.CheckState.Checked;
							break;
						}
					default:
						{
							chkUnderline.CheckState = Wisej.Web.CheckState.Unchecked;
							break;
						}
				}
				//end switch
			}
			//FC:TODO
			//if (!fecherFoundation.FCUtils.IsNull(rtb.SelBullet)) {
			//	switch (rtb.SelBullet) {
			//		case true:
			//		{
			//			chkBulleted.CheckState = Wisej.Web.CheckState.Checked;
			//			break;
			//		}
			//		default: {
			//			chkBulleted.CheckState = Wisej.Web.CheckState.Unchecked;
			//			break;
			//		}
			//	} //end switch
			//}
			if (!fecherFoundation.FCUtils.IsNull(rtb.TextAlign))
			{
				switch (rtb.TextAlign)
				{
					case HorizontalAlignment.Left:
						{
							cmbAlignment.SelectedIndex = 0;
							break;
						}
					case HorizontalAlignment.Right:
						{
							cmbAlignment.SelectedIndex = 1;
							break;
						}
					case HorizontalAlignment.Center:
						{
							cmbAlignment.SelectedIndex = 2;
							break;
						}
					default:
						{
							cmbAlignment.SelectedIndex = 0;
							break;
						}
				}
				//end switch
			}
			if (!fecherFoundation.FCUtils.IsNull(rtb.SelHangingIndent))
			{
				SetIndentCombo(rtb.SelHangingIndent);
			}
		}
	}
}
