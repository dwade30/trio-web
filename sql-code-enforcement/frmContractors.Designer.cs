//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmContractors.
	/// </summary>
	partial class frmContractors
	{
		public FCGrid GridClasses;
		public fecherFoundation.FCFrame Frame1;
		public FCGrid GridPhones;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCComboBox cmbStatus;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtName2;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtContractorNumber;
		public FCGrid GridOpens;
		public FCGrid GridDeletedPhones;
		public fecherFoundation.FCPictureBox ImgComment;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_7;
		public fecherFoundation.FCLabel Label1_6;
		public fecherFoundation.FCLabel Label1_5;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_0;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuComment;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteContractor;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmContractors));
			this.GridClasses = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.GridPhones = new fecherFoundation.FCGrid();
			this.txtEmail = new fecherFoundation.FCTextBox();
			this.Label1_10 = new fecherFoundation.FCLabel();
			this.cmbStatus = new fecherFoundation.FCComboBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.txtName2 = new fecherFoundation.FCTextBox();
			this.txtName = new fecherFoundation.FCTextBox();
			this.txtContractorNumber = new fecherFoundation.FCTextBox();
			this.GridOpens = new fecherFoundation.FCGrid();
			this.GridDeletedPhones = new fecherFoundation.FCGrid();
			this.ImgComment = new fecherFoundation.FCPictureBox();
			this.Label1_11 = new fecherFoundation.FCLabel();
			this.Label1_7 = new fecherFoundation.FCLabel();
			this.Label1_6 = new fecherFoundation.FCLabel();
			this.Label1_5 = new fecherFoundation.FCLabel();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteContractor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdComment = new fecherFoundation.FCButton();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridClasses)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridPhones)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridOpens)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ImgComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 647);
			this.BottomPanel.Size = new System.Drawing.Size(1405, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridClasses);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cmbStatus);
			this.ClientArea.Controls.Add(this.txtState);
			this.ClientArea.Controls.Add(this.txtZip);
			this.ClientArea.Controls.Add(this.txtZip4);
			this.ClientArea.Controls.Add(this.txtCity);
			this.ClientArea.Controls.Add(this.txtAddress2);
			this.ClientArea.Controls.Add(this.txtAddress1);
			this.ClientArea.Controls.Add(this.txtName2);
			this.ClientArea.Controls.Add(this.txtName);
			this.ClientArea.Controls.Add(this.txtContractorNumber);
			this.ClientArea.Controls.Add(this.GridOpens);
			this.ClientArea.Controls.Add(this.GridDeletedPhones);
			this.ClientArea.Controls.Add(this.ImgComment);
			this.ClientArea.Controls.Add(this.Label1_11);
			this.ClientArea.Controls.Add(this.Label1_7);
			this.ClientArea.Controls.Add(this.Label1_6);
			this.ClientArea.Controls.Add(this.Label1_5);
			this.ClientArea.Controls.Add(this.Label1_4);
			this.ClientArea.Controls.Add(this.Label1_3);
			this.ClientArea.Controls.Add(this.Label1_2);
			this.ClientArea.Controls.Add(this.Label1_1);
			this.ClientArea.Controls.Add(this.Label1_0);
			this.ClientArea.Size = new System.Drawing.Size(1405, 587);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdComment);
			this.TopPanel.Size = new System.Drawing.Size(1405, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdComment, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(139, 30);
			this.HeaderText.Text = "Contractors";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// GridClasses
			// 
			this.GridClasses.AllowSelection = false;
			this.GridClasses.AllowUserToResizeColumns = false;
			this.GridClasses.AllowUserToResizeRows = false;
			this.GridClasses.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridClasses.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridClasses.BackColorBkg = System.Drawing.Color.Empty;
			this.GridClasses.BackColorFixed = System.Drawing.Color.Empty;
			this.GridClasses.BackColorSel = System.Drawing.Color.Empty;
			this.GridClasses.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridClasses.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridClasses.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridClasses.ColumnHeadersHeight = 30;
			this.GridClasses.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridClasses.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridClasses.DragIcon = null;
			this.GridClasses.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridClasses.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridClasses.ExtendLastCol = true;
			this.GridClasses.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridClasses.FrozenCols = 0;
			this.GridClasses.GridColor = System.Drawing.Color.Empty;
			this.GridClasses.GridColorFixed = System.Drawing.Color.Empty;
			this.GridClasses.Location = new System.Drawing.Point(701, 430);
			this.GridClasses.Name = "GridClasses";
			this.GridClasses.OutlineCol = 0;
			this.GridClasses.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridClasses.RowHeightMin = 0;
			this.GridClasses.Rows = 1;
			this.GridClasses.ScrollTipText = null;
			this.GridClasses.ShowColumnVisibilityMenu = false;
			this.GridClasses.ShowFocusCell = false;
			this.GridClasses.Size = new System.Drawing.Size(366, 179);
			this.GridClasses.StandardTab = true;
			this.GridClasses.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridClasses.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridClasses.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.GridClasses, null);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.GridPhones);
			this.Frame1.Controls.Add(this.txtEmail);
			this.Frame1.Controls.Add(this.Label1_10);
			this.Frame1.Location = new System.Drawing.Point(701, 24);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(366, 230);
			this.Frame1.TabIndex = 23;
			this.Frame1.Text = "Contact Information";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// GridPhones
			// 
			this.GridPhones.AllowSelection = false;
			this.GridPhones.AllowUserToResizeColumns = false;
			this.GridPhones.AllowUserToResizeRows = false;
			this.GridPhones.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridPhones.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridPhones.BackColorBkg = System.Drawing.Color.Empty;
			this.GridPhones.BackColorFixed = System.Drawing.Color.Empty;
			this.GridPhones.BackColorSel = System.Drawing.Color.Empty;
			this.GridPhones.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridPhones.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridPhones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridPhones.ColumnHeadersHeight = 30;
			this.GridPhones.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridPhones.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridPhones.DragIcon = null;
			this.GridPhones.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridPhones.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridPhones.ExtendLastCol = true;
			this.GridPhones.FixedCols = 0;
			this.GridPhones.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridPhones.FrozenCols = 0;
			this.GridPhones.GridColor = System.Drawing.Color.Empty;
			this.GridPhones.GridColorFixed = System.Drawing.Color.Empty;
			this.GridPhones.Location = new System.Drawing.Point(20, 90);
			this.GridPhones.Name = "GridPhones";
			this.GridPhones.OutlineCol = 0;
			this.GridPhones.RowHeadersVisible = false;
			this.GridPhones.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridPhones.RowHeightMin = 0;
			this.GridPhones.Rows = 1;
			this.GridPhones.ScrollTipText = null;
			this.GridPhones.ShowColumnVisibilityMenu = false;
			this.GridPhones.ShowFocusCell = false;
			this.GridPhones.Size = new System.Drawing.Size(326, 122);
			this.GridPhones.StandardTab = true;
			this.GridPhones.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridPhones.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridPhones.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.GridPhones, "Use Insert and Delete to add and delete phone numbers");
			this.GridPhones.CurrentCellChanged += new System.EventHandler(this.GridPhones_RowColChange);
			this.GridPhones.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GridPhones_MouseDownEvent);
			this.GridPhones.KeyDown += new Wisej.Web.KeyEventHandler(this.GridPhones_KeyDownEvent);
			// 
			// txtEmail
			// 
			this.txtEmail.AutoSize = false;
			this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
			this.txtEmail.LinkItem = null;
			this.txtEmail.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEmail.LinkTopic = null;
			this.txtEmail.Location = new System.Drawing.Point(90, 30);
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(256, 40);
			this.txtEmail.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtEmail, null);
			this.txtEmail.Validating += new System.ComponentModel.CancelEventHandler(this.txtEmail_Validating);
			// 
			// Label1_10
			// 
			this.Label1_10.Location = new System.Drawing.Point(20, 44);
			this.Label1_10.Name = "Label1_10";
			this.Label1_10.Size = new System.Drawing.Size(59, 18);
			this.Label1_10.TabIndex = 24;
			this.Label1_10.Text = "E-MAIL";
			this.ToolTip1.SetToolTip(this.Label1_10, null);
			// 
			// cmbStatus
			// 
			this.cmbStatus.AutoSize = false;
			this.cmbStatus.BackColor = System.Drawing.SystemColors.Window;
			this.cmbStatus.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbStatus.FormattingEnabled = true;
			this.cmbStatus.Location = new System.Drawing.Point(125, 380);
			this.cmbStatus.Name = "cmbStatus";
			this.cmbStatus.Size = new System.Drawing.Size(558, 40);
			this.cmbStatus.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.cmbStatus, null);
			this.cmbStatus.TextChanged += new System.EventHandler(this.cmbStatus_TextChanged);
			// 
			// txtState
			// 
			this.txtState.MaxLength = 2;
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
			this.txtState.Location = new System.Drawing.Point(125, 330);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(294, 40);
			this.txtState.TabIndex = 5;
			this.txtState.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtState, null);
			this.txtState.Validating += new System.ComponentModel.CancelEventHandler(this.txtState_Validating);
			// 
			// txtZip
			// 
			this.txtZip.MaxLength = 5;
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
			this.txtZip.Location = new System.Drawing.Point(495, 330);
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(102, 40);
			this.txtZip.TabIndex = 6;
			this.txtZip.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtZip, null);
			this.txtZip.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip_Validating);
			// 
			// txtZip4
			// 
			this.txtZip4.MaxLength = 4;
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.LinkItem = null;
			this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip4.LinkTopic = null;
			this.txtZip4.Location = new System.Drawing.Point(606, 330);
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(77, 40);
			this.txtZip4.TabIndex = 7;
			this.txtZip4.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtZip4, null);
			this.txtZip4.Validating += new System.ComponentModel.CancelEventHandler(this.txtZip4_Validating);
			// 
			// txtCity
			// 
			this.txtCity.MaxLength = 255;
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
			this.txtCity.Location = new System.Drawing.Point(125, 280);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(558, 40);
			this.txtCity.TabIndex = 4;
			this.txtCity.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtCity, null);
			this.txtCity.Validating += new System.ComponentModel.CancelEventHandler(this.txtCity_Validating);
			// 
			// txtAddress2
			// 
			this.txtAddress2.MaxLength = 255;
			this.txtAddress2.AutoSize = false;
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.LinkItem = null;
			this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress2.LinkTopic = null;
			this.txtAddress2.Location = new System.Drawing.Point(125, 230);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(558, 40);
			this.txtAddress2.TabIndex = 3;
			this.txtAddress2.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtAddress2, null);
			this.txtAddress2.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress2_Validating);
			// 
			// txtAddress1
			// 
			this.txtAddress1.MaxLength = 255;
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.LinkItem = null;
			this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress1.LinkTopic = null;
			this.txtAddress1.Location = new System.Drawing.Point(125, 180);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(558, 40);
			this.txtAddress1.TabIndex = 2;
			this.txtAddress1.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtAddress1, null);
			this.txtAddress1.Validating += new System.ComponentModel.CancelEventHandler(this.txtAddress1_Validating);
			// 
			// txtName2
			// 
			this.txtName2.MaxLength = 255;
			this.txtName2.AutoSize = false;
			this.txtName2.BackColor = System.Drawing.SystemColors.Window;
			this.txtName2.LinkItem = null;
			this.txtName2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName2.LinkTopic = null;
			this.txtName2.Location = new System.Drawing.Point(125, 130);
			this.txtName2.Name = "txtName2";
			this.txtName2.Size = new System.Drawing.Size(558, 40);
			this.txtName2.TabIndex = 1;
			this.txtName2.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtName2, null);
			this.txtName2.Validating += new System.ComponentModel.CancelEventHandler(this.txtName2_Validating);
			// 
			// txtName
			// 
			this.txtName.MaxLength = 255;
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.LinkItem = null;
			this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName.LinkTopic = null;
			this.txtName.Location = new System.Drawing.Point(125, 80);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(558, 40);
			this.txtName.TabIndex = 0;
			this.txtName.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtName, null);
			this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtName_Validating);
			// 
			// txtContractorNumber
			// 
			this.txtContractorNumber.Appearance = 0;
			this.txtContractorNumber.AutoSize = false;
			this.txtContractorNumber.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtContractorNumber.LinkItem = null;
			this.txtContractorNumber.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtContractorNumber.LinkTopic = null;
			this.txtContractorNumber.Location = new System.Drawing.Point(125, 30);
			this.txtContractorNumber.LockedOriginal = true;
			this.txtContractorNumber.Name = "txtContractorNumber";
			this.txtContractorNumber.ReadOnly = true;
			this.txtContractorNumber.Size = new System.Drawing.Size(247, 40);
			this.txtContractorNumber.TabIndex = 14;
			this.txtContractorNumber.Text = "Text1";
			this.ToolTip1.SetToolTip(this.txtContractorNumber, null);
			// 
			// GridOpens
			// 
			this.GridOpens.AllowSelection = false;
			this.GridOpens.AllowUserToResizeColumns = false;
			this.GridOpens.AllowUserToResizeRows = false;
			this.GridOpens.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridOpens.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridOpens.BackColorBkg = System.Drawing.Color.Empty;
			this.GridOpens.BackColorFixed = System.Drawing.Color.Empty;
			this.GridOpens.BackColorSel = System.Drawing.Color.Empty;
			this.GridOpens.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridOpens.Cols = 7;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridOpens.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.GridOpens.ColumnHeadersHeight = 30;
			this.GridOpens.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridOpens.DefaultCellStyle = dataGridViewCellStyle6;
			this.GridOpens.DragIcon = null;
			this.GridOpens.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridOpens.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridOpens.ExtendLastCol = true;
			this.GridOpens.FixedCols = 2;
			this.GridOpens.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridOpens.FrozenCols = 0;
			this.GridOpens.GridColor = System.Drawing.Color.Empty;
			this.GridOpens.GridColorFixed = System.Drawing.Color.Empty;
			this.GridOpens.Location = new System.Drawing.Point(125, 430);
			this.GridOpens.Name = "GridOpens";
			this.GridOpens.OutlineCol = 0;
			this.GridOpens.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridOpens.RowHeightMin = 0;
			this.GridOpens.Rows = 50;
			this.GridOpens.ScrollTipText = null;
			this.GridOpens.ShowColumnVisibilityMenu = false;
			this.GridOpens.ShowFocusCell = false;
			this.GridOpens.Size = new System.Drawing.Size(558, 178);
			this.GridOpens.StandardTab = true;
			this.GridOpens.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridOpens.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridOpens.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.GridOpens, null);
			this.GridOpens.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridOpens_ValidateEdit);
			this.GridOpens.CurrentCellChanged += new System.EventHandler(this.GridOpens_RowColChange);
			// 
			// GridDeletedPhones
			// 
			this.GridDeletedPhones.AllowSelection = false;
			this.GridDeletedPhones.AllowUserToResizeColumns = false;
			this.GridDeletedPhones.AllowUserToResizeRows = false;
			this.GridDeletedPhones.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDeletedPhones.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDeletedPhones.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDeletedPhones.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDeletedPhones.BackColorSel = System.Drawing.Color.Empty;
			this.GridDeletedPhones.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDeletedPhones.Cols = 2;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDeletedPhones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.GridDeletedPhones.ColumnHeadersHeight = 30;
			this.GridDeletedPhones.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDeletedPhones.ColumnHeadersVisible = false;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDeletedPhones.DefaultCellStyle = dataGridViewCellStyle8;
			this.GridDeletedPhones.DragIcon = null;
			this.GridDeletedPhones.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDeletedPhones.FixedRows = 0;
			this.GridDeletedPhones.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDeletedPhones.FrozenCols = 0;
			this.GridDeletedPhones.GridColor = System.Drawing.Color.Empty;
			this.GridDeletedPhones.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDeletedPhones.Location = new System.Drawing.Point(209, 7);
			this.GridDeletedPhones.Name = "GridDeletedPhones";
			this.GridDeletedPhones.OutlineCol = 0;
			this.GridDeletedPhones.ReadOnly = true;
			this.GridDeletedPhones.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDeletedPhones.RowHeightMin = 0;
			this.GridDeletedPhones.Rows = 0;
			this.GridDeletedPhones.ScrollTipText = null;
			this.GridDeletedPhones.ShowColumnVisibilityMenu = false;
			this.GridDeletedPhones.ShowFocusCell = false;
			this.GridDeletedPhones.Size = new System.Drawing.Size(43, 19);
			this.GridDeletedPhones.StandardTab = true;
			this.GridDeletedPhones.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDeletedPhones.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDeletedPhones.TabIndex = 25;
			this.ToolTip1.SetToolTip(this.GridDeletedPhones, null);
			this.GridDeletedPhones.Visible = false;
			// 
			// ImgComment
			// 
			this.ImgComment.AllowDrop = true;
			this.ImgComment.BorderStyle = Wisej.Web.BorderStyle.None;
			this.ImgComment.DrawStyle = ((short)(0));
			this.ImgComment.DrawWidth = ((short)(1));
			this.ImgComment.FillStyle = ((short)(1));
			this.ImgComment.FontTransparent = true;
			this.ImgComment.Image = ((System.Drawing.Image)(resources.GetObject("ImgComment.Image")));
			this.ImgComment.Location = new System.Drawing.Point(256, 9);
			this.ImgComment.Name = "ImgComment";
			this.ImgComment.Picture = ((System.Drawing.Image)(resources.GetObject("ImgComment.Picture")));
			this.ImgComment.Size = new System.Drawing.Size(17, 18);
			this.ImgComment.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.ImgComment.TabIndex = 26;
			this.ToolTip1.SetToolTip(this.ImgComment, null);
			this.ImgComment.Click += new System.EventHandler(this.ImgComment_Click);
			// 
			// Label1_11
			// 
			this.Label1_11.Location = new System.Drawing.Point(30, 394);
			this.Label1_11.Name = "Label1_11";
			this.Label1_11.Size = new System.Drawing.Size(66, 18);
			this.Label1_11.TabIndex = 22;
			this.Label1_11.Text = "STATUS";
			this.ToolTip1.SetToolTip(this.Label1_11, null);
			// 
			// Label1_7
			// 
			this.Label1_7.Location = new System.Drawing.Point(30, 344);
			this.Label1_7.Name = "Label1_7";
			this.Label1_7.Size = new System.Drawing.Size(58, 18);
			this.Label1_7.TabIndex = 21;
			this.Label1_7.Text = "STATE";
			this.ToolTip1.SetToolTip(this.Label1_7, null);
			// 
			// Label1_6
			// 
			this.Label1_6.Location = new System.Drawing.Point(447, 344);
			this.Label1_6.Name = "Label1_6";
			this.Label1_6.Size = new System.Drawing.Size(34, 18);
			this.Label1_6.TabIndex = 20;
			this.Label1_6.Text = "ZIP";
			this.ToolTip1.SetToolTip(this.Label1_6, null);
			// 
			// Label1_5
			// 
			this.Label1_5.Location = new System.Drawing.Point(30, 294);
			this.Label1_5.Name = "Label1_5";
			this.Label1_5.Size = new System.Drawing.Size(66, 18);
			this.Label1_5.TabIndex = 19;
			this.Label1_5.Text = "CITY";
			this.ToolTip1.SetToolTip(this.Label1_5, null);
			// 
			// Label1_4
			// 
			this.Label1_4.Location = new System.Drawing.Point(30, 244);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(66, 18);
			this.Label1_4.TabIndex = 18;
			this.Label1_4.Text = "ADDRESS 2";
			this.ToolTip1.SetToolTip(this.Label1_4, null);
			// 
			// Label1_3
			// 
			this.Label1_3.Location = new System.Drawing.Point(30, 194);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(66, 18);
			this.Label1_3.TabIndex = 17;
			this.Label1_3.Text = "ADDRESS 1";
			this.ToolTip1.SetToolTip(this.Label1_3, null);
			// 
			// Label1_2
			// 
			this.Label1_2.Location = new System.Drawing.Point(30, 144);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(66, 18);
			this.Label1_2.TabIndex = 16;
			this.Label1_2.Text = "NAME 2";
			this.ToolTip1.SetToolTip(this.Label1_2, null);
			// 
			// Label1_1
			// 
			this.Label1_1.Location = new System.Drawing.Point(30, 94);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(66, 18);
			this.Label1_1.TabIndex = 15;
			this.Label1_1.Text = "NAME";
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(30, 44);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(66, 18);
			this.Label1_0.TabIndex = 13;
			this.Label1_0.Text = "NUMBER";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuComment,
            this.mnuSepar3,
            this.mnuDeleteContractor,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.Seperator,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuComment
			// 
			this.mnuComment.Index = 0;
			this.mnuComment.Name = "mnuComment";
			this.mnuComment.Text = "Comment";
			this.mnuComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = 1;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuDeleteContractor
			// 
			this.mnuDeleteContractor.Index = 2;
			this.mnuDeleteContractor.Name = "mnuDeleteContractor";
			this.mnuDeleteContractor.Text = "Delete Contractor";
			this.mnuDeleteContractor.Click += new System.EventHandler(this.mnuDeleteContractor_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 3;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 4;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 5;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 6;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 7;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdComment
			// 
			this.cmdComment.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdComment.AppearanceKey = "toolbarButton";
			this.cmdComment.Location = new System.Drawing.Point(1206, 28);
			this.cmdComment.Name = "cmdComment";
			this.cmdComment.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdComment.Size = new System.Drawing.Size(78, 24);
			this.cmdComment.TabIndex = 1;
			this.cmdComment.Text = "Comment";
			this.ToolTip1.SetToolTip(this.cmdComment, null);
			this.cmdComment.Click += new System.EventHandler(this.mnuComment_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(630, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(80, 48);
			this.cmdSave.TabIndex = 2;
			this.cmdSave.Text = "Save";
			this.ToolTip1.SetToolTip(this.cmdSave, null);
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(1071, 28);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(129, 24);
			this.cmdDelete.TabIndex = 2;
			this.cmdDelete.Text = "Delete Contractor";
			this.ToolTip1.SetToolTip(this.cmdDelete, null);
			this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteContractor_Click);
			// 
			// frmContractors
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(1405, 755);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmContractors";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Contractors";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmContractors_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmContractors_KeyDown);
			this.Resize += new System.EventHandler(this.frmContractors_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridClasses)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridPhones)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridOpens)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridDeletedPhones)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ImgComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdComment;
		private FCButton cmdSave;
		private FCButton cmdDelete;
	}
}
