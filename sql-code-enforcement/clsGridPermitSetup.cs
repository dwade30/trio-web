﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsGridPermitSetup
	{
		//=========================================================
		const int CNSTGRIDPERMITSCOLAUTOID = 0;
		const int CNSTGRIDPERMITSCOLPLAN = 2;
		const int CNSTGRIDPERMITSCOLPERMITNUMBER = 1;
		const int CNSTGRIDPERMITSCOLSTARTDATE = 3;
		const int CNSTGRIDPERMITSCOLENDDATE = 4;
		const int CNSTGRIDPERMITSCOLSTATUS = 5;
		const int CNSTGRIDPERMITSCOLPERMITIDENTIFIER = 6;
		const int CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE = 7;
		const int CNSTGRIDPERMITSCOLPERMITVALUE = 8;
		const int CNSTGRIDPERMITSCOLPERMITFEE = 9;
		const int CNSTGRIDPERMITSCOLTYPE = 10;
		private double[] aryColWidths = new double[11 + 1];
		private bool[] aryColShow = new bool[11 + 1];
		private int[] arycolOrder = new int[11 + 1];

		public void Set_ColWidth(int intType, double dblWidth)
		{
			aryColWidths[intType] = dblWidth;
		}

		public double Get_ColWidth(int intType)
		{
			double ColWidth = 0;
			ColWidth = aryColWidths[intType];
			return ColWidth;
		}

		public void Set_ColShow(int intType, bool boolShow)
		{
			aryColShow[intType] = boolShow;
		}

		public bool Get_ColShow(int intType)
		{
			bool ColShow = false;
			ColShow = aryColShow[intType];
			return ColShow;
		}

		public void Set_ColOrder(int intType, int intOrder)
		{
			arycolOrder[intType] = intOrder;
		}

		public int Get_ColOrder(int intType)
		{
			int ColOrder = 0;
			ColOrder = arycolOrder[intType];
			return ColOrder;
		}

		private string GetFieldName(ref int intType)
		{
			string GetFieldName = "";
			string strReturn = "";
			switch (intType)
			{
				case CNSTGRIDPERMITSCOLPLAN:
					{
						strReturn = "permitplan";
						break;
					}
				case CNSTGRIDPERMITSCOLPERMITNUMBER:
					{
						strReturn = "permitnumber";
						break;
					}
				case CNSTGRIDPERMITSCOLSTARTDATE:
					{
						strReturn = "permitstart";
						break;
					}
				case CNSTGRIDPERMITSCOLENDDATE:
					{
						strReturn = "permitcomplete";
						break;
					}
				case CNSTGRIDPERMITSCOLSTATUS:
					{
						strReturn = "permitstatus";
						break;
					}
				case CNSTGRIDPERMITSCOLPERMITIDENTIFIER:
					{
						strReturn = "permitidentifier";
						break;
					}
				case CNSTGRIDPERMITSCOLPERMITAPPLICATIONDATE:
					{
						strReturn = "permitapplication";
						break;
					}
				case CNSTGRIDPERMITSCOLPERMITVALUE:
					{
						strReturn = "permitvalue";
						break;
					}
				case CNSTGRIDPERMITSCOLPERMITFEE:
					{
						strReturn = "permitfee";
						break;
					}
				case CNSTGRIDPERMITSCOLTYPE:
					{
						strReturn = "permittype";
						break;
					}
			}
			//end switch
			GetFieldName = strReturn;
			return GetFieldName;
		}

		public void LoadCols()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int x;
				DefaultSetup();
				rsLoad.OpenRecordset("select * from permitgridsetup", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					for (x = 1; x <= 10; x++)
					{
						if (Conversion.Val(rsLoad.Get_Fields(GetFieldName(ref x) + "width")) > 0)
						{
							aryColWidths[x] = rsLoad.Get_Fields(GetFieldName(ref x) + "width");
						}
						aryColShow[x] = FCConvert.ToBoolean(rsLoad.Get_Fields(GetFieldName(ref x) + "show"));
						arycolOrder[x] = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields(GetFieldName(ref x) + "order"))));
					}
					// x
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadCols", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public double VisiblePercentage()
		{
			double VisiblePercentage = 0;
			int x;
			double dblTotal;
			dblTotal = 0;
			for (x = 1; x <= 10; x++)
			{
				if (aryColShow[x])
				{
					dblTotal += aryColWidths[x];
				}
			}
			// x
			VisiblePercentage = dblTotal;
			return VisiblePercentage;
		}

		public bool SaveCols()
		{
			bool SaveCols = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				int x;
				SaveCols = false;
				rsSave.OpenRecordset("select * from permitgridsetup", modGlobalVariables.Statics.strCEDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				for (x = 1; x <= 10; x++)
				{
					rsSave.Set_Fields(GetFieldName(ref x) + "width", aryColWidths[x]);
					rsSave.Set_Fields(GetFieldName(ref x) + "show", aryColShow[x]);
					rsSave.Set_Fields(GetFieldName(ref x) + "order", arycolOrder[x]);
				}
				// x
				rsSave.Update();
				SaveCols = true;
				return SaveCols;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveCols", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCols;
		}

		private void DefaultSetup()
		{
			int x;
			for (x = 1; x <= 10; x++)
			{
				aryColWidths[x] = 0.18;
				aryColShow[x] = true;
				arycolOrder[x] = x;
			}
			// x
		}
	}
}
