﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWCE0000
{
	public class clsCustomControlParameters
	{
		//=========================================================
		private struct CustomControlParameters
		{
			public string strName;
			public bool IsSubTotal;
			public string Category;
			public int OutlineLevel;
			public int Code;
			public string Help;
			public double DefaultHeight;
			public double DefaultWidth;
			public string DefaultAlign;
			public string ExtraParameters;
			public string strToolTip;
			public string ParamsToolTip;
		};

		private int lngFieldID;
		private bool boolSpecialCase;
		private string strFieldName = string.Empty;
		private string strTableName = string.Empty;
		private string strDBName = string.Empty;
		private int intDataType;
		private bool boolUserDefined;
		private string strFormatString = string.Empty;
		private int intOrderNumber;
		private int intCategoryNumber;
		private CustomControlParameters ccParam = new CustomControlParameters();

		public int OrderNumber
		{
			set
			{
				intOrderNumber = value;
			}
			get
			{
				int OrderNumber = 0;
				OrderNumber = intOrderNumber;
				return OrderNumber;
			}
		}

		public int CategoryNumber
		{
			set
			{
				intCategoryNumber = value;
			}
			get
			{
				int CategoryNumber = 0;
				CategoryNumber = intCategoryNumber;
				return CategoryNumber;
			}
		}

		public string FormatString
		{
			set
			{
				strFormatString = value;
			}
			get
			{
				string FormatString = "";
				FormatString = strFormatString;
				return FormatString;
			}
		}

		public bool UserDefined
		{
			set
			{
				boolUserDefined = value;
			}
			get
			{
				bool UserDefined = false;
				UserDefined = boolUserDefined;
				return UserDefined;
			}
		}

		public int TypeOfData
		{
			set
			{
				intDataType = value;
			}
			get
			{
				int TypeOfData = 0;
				TypeOfData = intDataType;
				return TypeOfData;
			}
		}

		public string DBName
		{
			set
			{
				strDBName = value;
			}
			get
			{
				string DBName = "";
				DBName = strDBName;
				return DBName;
			}
		}

		public string FieldName
		{
			set
			{
				strFieldName = value;
			}
			get
			{
				string FieldName = "";
				FieldName = strFieldName;
				return FieldName;
			}
		}

		public string TableName
		{
			set
			{
				strTableName = value;
			}
			get
			{
				string TableName = "";
				TableName = strTableName;
				return TableName;
			}
		}

		public bool SpecialCase
		{
			set
			{
				boolSpecialCase = value;
			}
			get
			{
				bool SpecialCase = false;
				SpecialCase = boolSpecialCase;
				return SpecialCase;
			}
		}

		public int FieldID
		{
			set
			{
				lngFieldID = value;
			}
			get
			{
				int FieldID = 0;
				FieldID = lngFieldID;
				return FieldID;
			}
		}

		public string ControlName
		{
			set
			{
				ccParam.strName = value;
			}
			get
			{
				string ControlName = "";
				ControlName = ccParam.strName;
				return ControlName;
			}
		}

		public bool IsSubTotal
		{
			set
			{
				ccParam.IsSubTotal = value;
			}
			get
			{
				bool IsSubTotal = false;
				IsSubTotal = ccParam.IsSubTotal;
				return IsSubTotal;
			}
		}

		public string Category
		{
			set
			{
				ccParam.Category = value;
			}
			get
			{
				string Category = "";
				Category = ccParam.Category;
				return Category;
			}
		}

		public int OutlineLevel
		{
			set
			{
				ccParam.OutlineLevel = value;
			}
			get
			{
				int OutlineLevel = 0;
				OutlineLevel = ccParam.OutlineLevel;
				return OutlineLevel;
			}
		}

		public int Code
		{
			set
			{
				ccParam.Code = value;
			}
			get
			{
				int Code = 0;
				Code = ccParam.Code;
				return Code;
			}
		}

		public string HelpString
		{
			set
			{
				ccParam.Help = value;
			}
			get
			{
				string HelpString = "";
				HelpString = ccParam.Help;
				return HelpString;
			}
		}

		public double DefaultHeight
		{
			set
			{
				ccParam.DefaultHeight = value;
			}
			get
			{
				double DefaultHeight = 0;
				DefaultHeight = ccParam.DefaultHeight;
				return DefaultHeight;
			}
		}

		public double DefaultWidth
		{
			set
			{
				ccParam.DefaultWidth = value;
			}
			get
			{
				double DefaultWidth = 0;
				DefaultWidth = ccParam.DefaultWidth;
				return DefaultWidth;
			}
		}

		public string DefaultAlign
		{
			set
			{
				ccParam.DefaultAlign = value;
			}
			get
			{
				string DefaultAlign = "";
				DefaultAlign = ccParam.DefaultAlign;
				return DefaultAlign;
			}
		}

		public string ExtraParameters
		{
			set
			{
				ccParam.ExtraParameters = value;
			}
			get
			{
				string ExtraParameters = "";
				ExtraParameters = ccParam.ExtraParameters;
				return ExtraParameters;
			}
		}

		public string ToolTipString
		{
			set
			{
				ccParam.strToolTip = value;
			}
			get
			{
				string ToolTipString = "";
				ToolTipString = ccParam.strToolTip;
				return ToolTipString;
			}
		}

		public string ParametersToolTip
		{
			set
			{
				ccParam.ParamsToolTip = value;
			}
			get
			{
				string ParametersToolTip = "";
				ParametersToolTip = ccParam.ParamsToolTip;
				return ParametersToolTip;
			}
		}
	}
}
