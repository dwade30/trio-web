﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptCustomBill.
	/// </summary>
	public partial class rptCustomBill : BaseSectionReport
	{
		public rptCustomBill()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Bills";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptCustomBill InstancePtr
		{
			get
			{
				return (rptCustomBill)Sys.GetInstance(typeof(rptCustomBill));
			}
		}

		protected rptCustomBill _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomBill	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ****************** THIS IS GLOBAL. DO NOT MESS WITH IT *******************
		string strModuleDB;
		// database to load custom format from
		string strThisModule;
		// Two character module name
		clsCustomBill CustomBillClass;
		// class that handles all module specific parts of the report
		int intUnitsType;
		// inches or centimeters
		bool boolChoosePrinter;
		// is the printer already chosen or do we need to prompt
		int intTwipsPerUnit;
		// 1440 if in inches, 567 if in centimeters
		private CustomFieldInfo[] aryFieldsToUpdate = null;
		// An array of names of controls that aren't static
		int lngFieldsIndex;
		// Dim aryExtraParameters() As String  'an array of parameters
		// vbPorter upgrade warning: aryAutoPopIDs As string()	OnWrite(string(), int())
		string[] aryAutoPopIDs = null;
		string[] aryAutoPopText = null;
		private bool boolIsSub;
		const int CNSTCUSTOMBILLUNITSINCHES = 0;
		const int CNSTCUSTOMBILLUNITSCENTIMETERS = 1;
		const int CNSTCUSTOMBILLFONTSTYLEREGULAR = 0;
		const int CNSTCUSTOMBILLFONTSTYLEBOLD = 1;
		const int CNSTCUSTOMBILLFONTSTYLEITALIC = 2;
		const int CNSTCUSTOMBILLFONTSTYLEBOLDITALIC = 3;
		// define a type to make it easier to pass info to functions
		private struct CustomBillFieldType
		{
			public int ID;
			public int FieldNumber;
			public int FormatID;
			public int FieldID;
			public float lngTop;
			public float lngLeft;
			public float lngHeight;
			public float lngWidth;
			public int BillType;
			public string Description;
			public int intAlignment;
			public string UserText;
			public string strFont;
			public double dblFontSize;
			public int intFontStyle;
			public string ExtraParameters;
			public int OpenID;
		};

		private CustomBillFieldType CurrentFieldType = new CustomBillFieldType();

		private struct PrinterFontType
		{
			public string strfont10CPI;
			public string strFont12CPI;
			public string strFont17CPI;
		};

		private PrinterFontType CurrentPrinterFont = new PrinterFontType();
		private bool boolPrintXsForAlignment;
		int woogy;
		bool blnPrintCancel;
		// MAL@20080110: Store if user pressed Cancel at the Print dialog
		int lngRecNum;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!boolIsSub)
			{
				eArgs.EOF = CustomBillClass.EndOfFile;
			}
			else
			{
				lngRecNum += 1;
				if (lngRecNum > 1)
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}
		// Public Sub AssignClass(ByRef CustomBillObject As Object)
		// Set CustomBillClass = CustomBillObject
		// boolIsSub = True
		//
		// End Sub
		// vbPorter upgrade warning: CustomBillObject As object	OnWrite(clsCustomBill)
		// vbPorter upgrade warning: intModal As int	OnWrite(FCForm.FormShowEnum)
		public void Init(clsCustomBill CustomBillObject, bool boolPreview = true, bool boolDontUseReportViewer = false, bool boolShowXsForAlignment = false, bool boolSubReport = false, int lngFormatToUse = 0, int intModal = 0)
		{
			if (CustomBillObject.ReportTitle != "")
			{
				this.Name = CustomBillObject.ReportTitle;
			}
			string[] aryTemp = null;
			string strTemp = "";
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int intTemp = 0;
			boolIsSub = boolSubReport;
			lngRecNum = 0;
			boolPrintXsForAlignment = boolShowXsForAlignment;
			boolChoosePrinter = true;
            //FC:FINAL:MSH - i.issue #1703: creating temp object will remove data after Init() work
			//clsCustomBill CustomBillClass = CustomBillObject;
            CustomBillClass = CustomBillObject;
			strThisModule = CustomBillClass.Module;
			strModuleDB = CustomBillClass.DBFile;
			if (!boolIsSub)
			{
				strTemp = frmCustomBillAutoPop.InstancePtr.Init(CustomBillClass.FormatID, strModuleDB);
			}
			else
			{
				strTemp = frmCustomBillAutoPop.InstancePtr.Init(lngFormatToUse, strModuleDB);
			}
			if (strTemp != string.Empty)
			{
				if (strTemp == "QUIT")
				{
					this.Close();
					return;
				}
				aryTemp = Strings.Split(strTemp, "|", -1, CompareConstants.vbTextCompare);
				intTemp = 0;
				for (x = 0; x <= (Information.UBound(aryTemp, 1)); x += 2)
				{
					intTemp += 1;
					Array.Resize(ref aryAutoPopIDs, intTemp + 1);
					Array.Resize(ref aryAutoPopText, intTemp + 1);
					aryAutoPopIDs[intTemp] = aryTemp[x];
					aryAutoPopText[intTemp] = aryTemp[x + 1];
				}
				// x
			}
			else
			{
				// just so ubound won't give errors
				aryAutoPopIDs = new string[1 + 1];
				aryAutoPopText = new string[1 + 1];
				aryAutoPopIDs[1] = FCConvert.ToString(0);
			}
			if (!boolIsSub)
			{
				if (fecherFoundation.Strings.Trim(CustomBillClass.PrinterName) != string.Empty)
				{
					this.Document.Printer.PrinterName = CustomBillClass.PrinterName;
					boolChoosePrinter = false;
				}
				if (CustomBillClass.UsePrinterFonts)
				{
				}
			}
			lngFieldsIndex = 0;
			if (!boolIsSub)
			{
				SetupFormat();
			}
			else
			{
				SetupFields(lngFormatToUse);
			}
			// MAL@20080110: Add check for user pressing Cancel at choose printer dialog
			// Tracker Reference: 11882
			if (!blnPrintCancel)
			{
				if (!boolIsSub)
				{
					SetupFields(CustomBillClass.FormatID);
					CustomBillClass.LoadData();
					if (CustomBillClass.EndOfFile)
					{
						MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        this.Unload();
						return;
					}
					if (boolPreview)
					{
						if (boolDontUseReportViewer)
						{
							// presumably the other viewer or method is handling everything
						}
						else
						{
							if (boolChoosePrinter)
							{
                                frmReportViewer.InstancePtr.Init(this, "", intModal, showModal: FCConvert.CBool(intModal));
							}
							else
							{
                                frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName, intModal, showModal: FCConvert.CBool(intModal));
							}
						}
					}
					else
					{
						this.PrintReport(boolChoosePrinter);
					}
				}
			}
		}

		private bool SetupFormat()
		{
			bool SetupFormat = false;
			// makes the page size and margins and might have to prompt for the printer
			clsDRWrapper clsLoad = new clsDRWrapper();
            //FC:FINAL:MSH - i.issue #1703: remove reinitializing to avoid missing data
            //CustomBillClass = new clsCustomBill();
			int intReturn = 0;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                SetupFormat = false;
                clsLoad.OpenRecordset("select * from custombills where ID = " + CustomBillClass.FormatID, strModuleDB);
                if (!clsLoad.EndOfFile())
                {
                    if (clsLoad.Get_Fields_Boolean("islaser"))
                    {
                        CustomBillClass.DotMatrixFormat = false;
                    }
                    else
                    {
                        CustomBillClass.DotMatrixFormat = true;
                    }

                    intUnitsType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("units"))));
                    //FC:FINAL:MSH - i.issue #1703: wrong calculations
                    //if ((ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("PageWidth")), intUnitsType) != 8.5 * 1440) || (ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("PageHeight")), intUnitsType) != 15840))
                    if ((ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("PageWidth")), intUnitsType) != 8.5) ||
                        (ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("PageHeight")), intUnitsType) != 11))
                    {
                        // not the standard 8.5 x 11 so change it
                        this.PageSettings.PaperWidth =
                            ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("PageWidth")), intUnitsType);
                        this.PageSettings.PaperHeight =
                            ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("PageHeight")), intUnitsType);
                        if (fecherFoundation.Strings.Trim(CustomBillClass.PrinterName) == string.Empty)
                        {
                            boolChoosePrinter = false;
                        }

                        // vbPorter upgrade warning: lngWid As int	OnWriteFCConvert.ToDouble(
                        float lngWid = 0;
                        // vbPorter upgrade warning: lngHit As int	OnWriteFCConvert.ToDouble(
                        float lngHit = 0;
                        lngWid = FCConvert.ToSingle(this.PageSettings.PaperWidth * 2.54 * 10000) / 1440F;
                        lngHit = FCConvert.ToSingle(this.PageSettings.PaperHeight * 2.54 * 10000) / 1440F;
                        this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize(
                            "CustomFormat" + strThisModule + "42",
                            FCConvert.ToInt32(this.PageSettings.PaperWidth * 100),
                            FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
                    }

                    this.PageSettings.Margins.Left =
                        ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("leftmargin")), intUnitsType);
                    this.PageSettings.Margins.Right =
                        ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("RightMargin")), intUnitsType);
                    this.PageSettings.Margins.Top = ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("TopMargin")),
                        intUnitsType);
                    this.PageSettings.Margins.Bottom =
                        ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("BottomMargin")), intUnitsType);
                    this.PrintWidth = this.PageSettings.PaperWidth - this.PageSettings.Margins.Left -
                                      this.PageSettings.Margins.Right;
                    this.Detail.Height = this.PageSettings.PaperHeight - this.PageSettings.Margins.Top -
                                         this.PageSettings.Margins.Bottom;
                    this.Detail.KeepTogether = true;
                    if (CustomBillClass.DotMatrixFormat && CustomBillClass.UsePrinterFonts)
                    {
                        // load the printer fonts from the printer
                        clsReportPrinterFunctions prtObj = new clsReportPrinterFunctions();
                        CurrentPrinterFont.strfont10CPI = prtObj.GetFont(this.Document.Printer.PrinterName, 10);
                        CurrentPrinterFont.strFont12CPI = prtObj.GetFont(this.Document.Printer.PrinterName, 12);
                        CurrentPrinterFont.strFont17CPI = prtObj.GetFont(this.Document.Printer.PrinterName, 17);
                    }
                }
                else
                {
                    return SetupFormat;
                }

                SetupFormat = true;
                return SetupFormat;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupFormat", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }
			return SetupFormat;
		}

		private bool SetupFields(int lngFormatToUse)
		{
			bool SetupFields = false;
			// dynamically creates the fields
			clsDRWrapper clsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                SetupFields = false;
                // Call clsLoad.OpenRecordset("select * from CustomBillFields where formatid = " & CustomBillClass.FormatID & " order by fieldnumber", strModuleDB)
                clsLoad.OpenRecordset(
                    "select * from CustomBillFields where formatid = " + FCConvert.ToString(lngFormatToUse) +
                    " order by fieldnumber", strModuleDB);
                if (clsLoad.EndOfFile())
                    return SetupFields;
                while (!clsLoad.EndOfFile())
                {
                    // create a field
                    // fill the type to be passed to the function that actually creates the field
                    CurrentFieldType.ID = clsLoad.Get_Fields_Int32("ID");
                    CurrentFieldType.FieldNumber = clsLoad.Get_Fields_Int32("FieldNumber");
                    CurrentFieldType.FormatID = clsLoad.Get_Fields_Int32("FormatID");
                    CurrentFieldType.FieldID = clsLoad.Get_Fields_Int32("FieldID");
                    if (!boolIsSub)
                    {
                        CurrentFieldType.lngTop =
                            ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("Top")), intUnitsType) +
                            CustomBillClass.VerticalAlignment;
                    }
                    else
                    {
                        //FC:FINAL:MSH - i.issue #1703: remove Rounding to avoid wrong calculations
                        //CurrentFieldType.lngTop = ConvertTwips(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("Top"))), intUnitsType);
                        CurrentFieldType.lngTop =
                            ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("Top")), intUnitsType);
                    }

                    //FC:FINAL:MSH - i.issue #1703: remove Rounding to avoid wrong calculations
                    //CurrentFieldType.lngLeft = ConvertTwips(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("Left"))), intUnitsType);
                    //CurrentFieldType.lngHeight = ConvertTwips(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("Height"))), intUnitsType);
                    //CurrentFieldType.lngWidth = ConvertTwips(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("Width"))), intUnitsType);
                    CurrentFieldType.lngLeft =
                        ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("Left")), intUnitsType);
                    CurrentFieldType.lngHeight =
                        ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("Height")), intUnitsType) < 0.2F
                            ? 0.2F
                            : ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("Height")), intUnitsType);
                    CurrentFieldType.lngWidth =
                        ConvertTwips(Conversion.Val(clsLoad.Get_Fields_String("Width")), intUnitsType);
                    CurrentFieldType.BillType =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("BillType"))));
                    CurrentFieldType.Description = clsLoad.Get_Fields_String("Description");
                    CurrentFieldType.intAlignment = clsLoad.Get_Fields_Int32("Alignment");
                    //FC:FINAL:AM:#3651 - fix alignment - in VB6 1 is Right and 2 is Centered
                    if (CurrentFieldType.intAlignment == 1)
                    {
                        CurrentFieldType.intAlignment = 2;
                    }
                    else if (CurrentFieldType.intAlignment == 2)
                    {
                        CurrentFieldType.intAlignment = 1;
                    }

                    CurrentFieldType.UserText = clsLoad.Get_Fields_String("UserText");
                    CurrentFieldType.strFont = clsLoad.Get_Fields_String("Font");
                    CurrentFieldType.dblFontSize = clsLoad.Get_Fields_Double("FontSize");
                    CurrentFieldType.intFontStyle =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("FontStyle"))));
                    CurrentFieldType.ExtraParameters = clsLoad.Get_Fields_String("ExtraParameters");
                    CurrentFieldType.OpenID =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_String("openid"))));
                    CreateAField(CurrentFieldType.FieldNumber, CurrentFieldType.FieldID, CurrentFieldType.OpenID);
                    clsLoad.MoveNext();
                }

                SetupFields = true;
                return SetupFields;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupFields", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.Dispose();
            }

			return SetupFields;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		private float ConvertTwips(double dblUnits, int intTypeUnits)
		{
			float ConvertTwips = 0;
			// converts to twips from whatever units the format is in
			switch (intTypeUnits)
			{
				case CNSTCUSTOMBILLUNITSCENTIMETERS:
					{
						ConvertTwips = FCConvert.ToInt32(dblUnits * 567) / 1440F;
						break;
					}
				default:
					{
						ConvertTwips = FCConvert.ToInt32(dblUnits * 1440) / 1440F;
						break;
					}
			}
			//end switch
			return ConvertTwips;
		}

		private void CreateAField(int lngFldNum, int lngFldType, int lngFldID = 0)
		{
			// creates a field based on the info in lngfldtype
			GrapeCity.ActiveReports.SectionReportModel.ARControl ctl = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			string strTemp = "";
			int lngWidth = 0;
			int lngHeight = 0;
			double dblRatio = 0;
			bool boolStatic;
			bool boolUsesFont;
            Font ctlFont = null;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			clsDRWrapper clsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                boolUsesFont = false;
                boolStatic = true;
                if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Picture();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).SizeMode =
                        GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    if (FCConvert.CBool(
                        fecherFoundation.Strings.Trim(FCConvert.ToString(CurrentFieldType.UserText != string.Empty))))
                    {
                        if (FCFileSystem.FileExists(CurrentFieldType.UserText))
                        {
                            (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image =
                                FCUtils.LoadPicture(CurrentFieldType.UserText);
                            lngWidth = (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image.Width;
                            lngHeight = (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image.Height;
                            dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;
                            if (dblRatio * ctl.Height > ctl.Width)
                            {
                                // keep width, change height
                                ctl.Height = FCConvert.ToSingle(ctl.Width / dblRatio);
                            }
                            else
                            {
                                // keep height, change width
                                ctl.Width = FCConvert.ToSingle(ctl.Height * dblRatio);
                            }
                        }
                    }
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLSIGNATURE)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Picture();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).SizeMode =
                        GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    clsSignature tSig = new clsSignature();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image = tSig.GetCurrentSignature();
                    if (!((ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image == null))
                    {
                        lngWidth = (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image.Width;
                        lngHeight = (ctl as GrapeCity.ActiveReports.SectionReportModel.Picture).Image.Height;
                        dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;
                        if (dblRatio * ctl.Height > ctl.Width)
                        {
                            ctl.Height = FCConvert.ToSingle(ctl.Width / dblRatio);
                        }
                        else
                        {
                            ctl.Width = FCConvert.ToSingle(ctl.Height * dblRatio);
                        }
                    }
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
                {
                    // a static field
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Label();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Alignment =
                        (GrapeCity.ActiveReports.Document.Section.TextAlignment) CurrentFieldType.intAlignment;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Text = CurrentFieldType.UserText;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).MultiLine = true;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).WordWrap = true;
                    //FC:FINAL:MSH - i.issue #1703: save font for using in next step to avoid exceptions on field type converting
                    ctlFont = (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font;
                    boolUsesFont = true;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT)
                {
                    // not implemented yet.  doesn't seem to be a need for it yet
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.SubReport).CanGrow = true;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.SubReport).CanShrink = true;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.SubReport).Report = new rptCustomBill();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.SubReport).Report.UserData = "subreport";
                    ((ctl as GrapeCity.ActiveReports.SectionReportModel.SubReport).Report as rptCustomBill).Init(
                        CustomBillClass, boolSubReport: true,
                        lngFormatToUse: FCConvert.ToInt32(CurrentFieldType.UserText));
                    // add to the list so we know to update it in the detail section
                    // lngFieldsIndex = lngFieldsIndex + 1
                    // ReDim Preserve aryFieldsToUpdate(lngFieldsIndex) As CustomFieldInfo
                    // If aryFieldsToUpdate(lngFieldsIndex) Is Nothing Then
                    // Set aryFieldsToUpdate(lngFieldsIndex) = New CustomFieldInfo
                    // End If
                    // aryFieldsToUpdate(lngFieldsIndex).Title = ctl.Name
                    // aryFieldsToUpdate(lngFieldsIndex).ExtraParameters = CurrentFieldType.ExtraParameters
                    // aryFieldsToUpdate(lngFieldsIndex).Code = lngFldNum
                    // aryFieldsToUpdate(lngFieldsIndex).FieldID = lngFldID
                    Detail.Controls.Add(ctl);
                    return;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
                {
                    // not implemented yet.
                    boolStatic = false;
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
                    // ctl.Alignment = CurrentFieldType.intAlignment
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).Text = "";
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    //(ctl as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).MultiLine = true;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).CanGrow = false;
                    //FC:FINAL:MSH - i.issue #1703: save font for using in next step to avoid exceptions on field type converting
                    ctlFont = (ctl as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).Font;
                    boolUsesFont = true;
                    this.Fields.Add("DynamicReport" + FCConvert.ToString(lngFldNum));
                    strTemp = "";
                    clsTemp.OpenRecordset(
                        "select * from dynamicreports where ID = " +
                        FCConvert.ToString(Conversion.Val(CurrentFieldType.UserText)), strModuleDB);
                    if (!clsTemp.EndOfFile())
                    {
                        strTemp = clsTemp.Get_Fields_String("text");
                    }

                    this.Fields["DynamicReport" + lngFldNum].Value = strTemp;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Line();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).X1 = CurrentFieldType.lngLeft;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 = CurrentFieldType.lngTop;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).X2 =
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).X1 + CurrentFieldType.lngWidth;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 =
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).Y1;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Line();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).X1 = CurrentFieldType.lngLeft;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 = CurrentFieldType.lngTop;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).X2 =
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).X1;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).Y2 =
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Line).Y1 + CurrentFieldType.lngHeight;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Shape();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Shape).Style =
                        GrapeCity.ActiveReports.SectionReportModel.ShapeType.Rectangle;
                    // rectangle
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Shape).LineStyle =
                        GrapeCity.ActiveReports.SectionReportModel.LineStyle.Transparent;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
                {
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Label();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Alignment =
                        (GrapeCity.ActiveReports.Document.Section.TextAlignment) CurrentFieldType.intAlignment;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Text = CurrentFieldType.UserText;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    ;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).BackColor = Color.Black;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).ForeColor = Color.White;
                    //FC:FINAL:MSH - i.issue #1703: save font for using in next step to avoid exceptions on field type converting
                    ctlFont = (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font;
                    boolUsesFont = true;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE)
                {
                    boolStatic = false;
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Alignment =
                        (StringAlignment) CurrentFieldType.intAlignment;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).CheckSumEnabled = false;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Style =
                        GrapeCity.ActiveReports.SectionReportModel.BarCodeStyle.Code39x;
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Font =
                        new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Font.Name, 8);
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Font = new Font("Tahoma",
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Font.Size);
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).CheckSumEnabled = false;
                    if (fecherFoundation.Strings.UCase(CurrentFieldType.ExtraParameters) == "SHOW CAPTION")
                    {
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).CaptionPosition =
                            GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below;
                    }
                    else
                    {
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).CaptionPosition =
                            GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.None;
                    }

                    //FC:FINAL:MSH - i.issue #1703: save font for using in next step to avoid exceptions on field type converting
                    ctlFont = (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Font;
                }
                else if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
                {
                    boolStatic = true;
                    // will fill this in a different way
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Label();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Alignment =
                        (GrapeCity.ActiveReports.Document.Section.TextAlignment) CurrentFieldType.intAlignment;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    //FC:FINAL:MSH - i.issue #1703: save font for using in next step to avoid exceptions on field type converting
                    ctlFont = (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font;
                    boolUsesFont = true;
                }
                else
                {
                    // default to non static label
                    boolStatic = false;
                    ctl = new GrapeCity.ActiveReports.SectionReportModel.Label();
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Alignment =
                        (GrapeCity.ActiveReports.Document.Section.TextAlignment) CurrentFieldType.intAlignment;
                    ctl.Height = CurrentFieldType.lngHeight;
                    ctl.Width = CurrentFieldType.lngWidth;
                    ctl.Top = CurrentFieldType.lngTop;
                    ctl.Left = CurrentFieldType.lngLeft;
                    //FC:FINAL:MSH - i.issue #1703: save font for using in next step to avoid exceptions on field type converting
                    ctlFont = (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font;
                    boolUsesFont = true;
                }

                //if (boolUsesFont)
                //{
                //	(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font(CurrentFieldType.strFont, (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Size);
                //	if (CustomBillClass.DotMatrixFormat && CustomBillClass.UsePrinterFonts)
                //	{
                //		if (CurrentFieldType.dblFontSize >= 12)
                //		{
                //			// 10 cpi
                //			if (fecherFoundation.Strings.Trim(CurrentPrinterFont.strfont10CPI) != string.Empty)
                //			{
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font(CurrentPrinterFont.strfont10CPI, (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Size);
                //			}
                //		}
                //		else if (CurrentFieldType.dblFontSize < 10)
                //		{
                //			// 17 cpi
                //			if (fecherFoundation.Strings.Trim(CurrentPrinterFont.strFont17CPI) != string.Empty)
                //			{
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font(CurrentPrinterFont.strFont17CPI, (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Size);
                //			}
                //		}
                //		else
                //		{
                //			// 12 cpi
                //			if (fecherFoundation.Strings.Trim(CurrentPrinterFont.strFont12CPI) != string.Empty)
                //			{
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font(CurrentPrinterFont.strFont12CPI, (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Size);
                //			}
                //		}
                //		(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Name, 10);
                //		// must always be 10 when using printer fonts
                //	}
                //	else
                //	{
                //		(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Name, FCConvert.ToInt32((CurrentFieldType.dblFontSize));
                //	}
                //	switch (CurrentFieldType.intFontStyle)
                //	{
                //		case CNSTCUSTOMBILLFONTSTYLEBOLD:
                //			{
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font, FontStyle.Bold);
                //				//(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Italic = false;
                //				break;
                //			}
                //		case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
                //			{
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font, FontStyle.Bold);
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font, FontStyle.Italic);
                //				break;
                //			}
                //		case CNSTCUSTOMBILLFONTSTYLEITALIC:
                //			{
                //				//(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Bold = false;
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font, FontStyle.Italic);
                //				break;
                //			}
                //		default:
                //			{
                //				// regular
                //				//(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Bold = false;
                //				//(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Italic = false;
                //				(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font, FontStyle.Regular);
                //				break;
                //			}
                //	}
                //	//end switch
                //}
                //FC:FINAL:MSH - i.issue #1703: ctl can be not only label, so use saved Font for applying changes
                if (boolUsesFont && ctlFont != null)
                {
                    ctlFont = new Font(CurrentFieldType.strFont, ctlFont.Size);
                    if (CustomBillClass.DotMatrixFormat && CustomBillClass.UsePrinterFonts)
                    {
                        if (CurrentFieldType.dblFontSize >= 12)
                        {
                            // 10 cpi
                            if (fecherFoundation.Strings.Trim(CurrentPrinterFont.strfont10CPI) != string.Empty)
                            {
                                ctlFont = new Font(CurrentPrinterFont.strfont10CPI, ctlFont.Size);
                            }
                        }
                        else if (CurrentFieldType.dblFontSize < 10)
                        {
                            // 17 cpi
                            if (fecherFoundation.Strings.Trim(CurrentPrinterFont.strFont17CPI) != string.Empty)
                            {
                                ctlFont = new Font(CurrentPrinterFont.strFont17CPI, ctlFont.Size);
                            }
                        }
                        else
                        {
                            // 12 cpi
                            if (fecherFoundation.Strings.Trim(CurrentPrinterFont.strFont12CPI) != string.Empty)
                            {
                                ctlFont = new Font(CurrentPrinterFont.strFont12CPI, ctlFont.Size);
                            }
                        }

                        ctlFont = new Font(ctlFont.Name, 10);
                        // must always be 10 when using printer fonts
                    }
                    else
                    {
                        ctlFont = new Font(ctlFont.Name, FCConvert.ToInt32(CurrentFieldType.dblFontSize));
                    }

                    switch (CurrentFieldType.intFontStyle)
                    {
                        case CNSTCUSTOMBILLFONTSTYLEBOLD:
                        {
                            ctlFont = new Font(ctlFont, FontStyle.Bold);
                            //(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Italic = false;
                            break;
                        }
                        case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
                        {
                            ctlFont = new Font(ctlFont, FontStyle.Bold);
                            ctlFont = new Font(ctlFont, FontStyle.Italic);
                            break;
                        }
                        case CNSTCUSTOMBILLFONTSTYLEITALIC:
                        {
                            //(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Bold = false;
                            ctlFont = new Font(ctlFont, FontStyle.Italic);
                            break;
                        }
                        default:
                        {
                            // regular
                            //(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Bold = false;
                            //(ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font.Italic = false;
                            ctlFont = new Font(ctlFont, FontStyle.Regular);
                            break;
                        }
                    }

                    //end switch
                    //FC:FINAL:MSH - i.issue #1703: set new Font to the control
                    if (ctl as GrapeCity.ActiveReports.SectionReportModel.Label != null)
                    {
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Font = ctlFont;
                    }
                    else if (ctl as GrapeCity.ActiveReports.SectionReportModel.RichTextBox != null)
                    {
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).Font = ctlFont;
                    }
                    else if (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode != null)
                    {
                        (ctl as GrapeCity.ActiveReports.SectionReportModel.Barcode).Font = ctlFont;
                    }
                }

                strTemp = "CustomLabel" + FCConvert.ToString(lngFldNum);
                ctl.Name = strTemp;
                ctl.Tag = lngFldType;
                // so we can tell from the control name what code to use from the custombillcode table
                if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
                {
                    (ctl as GrapeCity.ActiveReports.SectionReportModel.Label).Text = "";
                    for (x = 1; x <= (Information.UBound(aryAutoPopIDs, 1)); x++)
                    {
                        if (Conversion.Val(aryAutoPopIDs[x]) == CurrentFieldType.ID)
                        {
                            // its a match
                            aryAutoPopIDs[x] = strTemp;
                            // the array will now have the control names not ids
                        }
                    }

                    // x
                }

                if (!boolStatic)
                {
                    // add to the list so we know to update it in the detail section
                    lngFieldsIndex += 1;
                    Array.Resize(ref aryFieldsToUpdate, lngFieldsIndex + 1);
                    if (aryFieldsToUpdate[lngFieldsIndex] == null)
                    {
                        aryFieldsToUpdate[lngFieldsIndex] = new CustomFieldInfo();
                    }

                    aryFieldsToUpdate[lngFieldsIndex].Title = ctl.Name;
                    aryFieldsToUpdate[lngFieldsIndex].ExtraParameters = CurrentFieldType.ExtraParameters;
                    aryFieldsToUpdate[lngFieldsIndex].Code = lngFldNum;
                    aryFieldsToUpdate[lngFieldsIndex].FieldID = lngFldID;
                    // ReDim Preserve aryExtraParameters(lngFieldsIndex)
                    // aryExtraParameters(lngFieldsIndex) = CurrentFieldType.ExtraParameters
                }

                Detail.Controls.Add(ctl);
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " +
                    fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CreateAField", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsTemp.Dispose();
            }
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (!boolIsSub)
			{
				if (boolPrintXsForAlignment)
				{
					if (CustomBillClass.DotMatrixFormat)
					{
						if (modPrinterFunctions.PrintXsForAlignment("The top of the X should be flush with the top of the form.", 5, 1, this.Document.Printer.PrinterName) == DialogResult.Cancel)
						{
							this.Close();
							return;
						}
					}
				}
			}
			// CustomBillClass.LoadData
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{

		}

		private void Detail_BeforePrint(object sender, EventArgs e)
		{
			if (!CustomBillClass.EndOfFile)
			{
				if (!boolIsSub)
				{
					CustomBillClass.MoveNext();
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			int lngCode = 0;
			if (!CustomBillClass.EndOfFile)
			{
				if (lngFieldsIndex > 0)
				{
					// if there are any to update.  Otherwise the array will be empty and will cause an error
					// for each control that needs to be updated
					for (x = 1; x <= lngFieldsIndex; x++)
					{
						if (!(FCConvert.ToString(Detail.Controls[aryFieldsToUpdate[x].Title].Tag) == "subreport"))
						{
							lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Detail.Controls[aryFieldsToUpdate[x].Title].Tag)));
							if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT)
							{
								// not actually used at this time
								//FC:FINAL:DSE WordWrap is not functioning in RichTextBox
								//(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).Text = CustomBillClass.GetDataByCode(lngCode, aryFieldsToUpdate[x].FieldID, aryFieldsToUpdate[x].ExtraParameters);
								(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).SetHtmlText(CustomBillClass.GetDataByCode(lngCode, aryFieldsToUpdate[x].FieldID, aryFieldsToUpdate[x].ExtraParameters));
							}
							else if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
							{
								// dynamic images are not supported at this time
								// should never get here
							}
							else if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
							{
                                //FC:FINAL:MSH - i.issue #1703: wrong field type
								//FC:FINAL:DSE WordWrapping not working in RichTextBox
                                //(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = CustomBillClass.ParseDynamicDocumentString(strModuleDB, this.Fields["DynamicReport" + Strings.Mid(aryFieldsToUpdate[x].Title, 12)].Value.ToString(), modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL);
                                (Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.RichTextBox).SetHtmlText(CustomBillClass.ParseDynamicDocumentString(strModuleDB, this.Fields["DynamicReport" + Strings.Mid(aryFieldsToUpdate[x].Title, 12)].Value.ToString(), modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL));
							}
							else if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMDATE)
							{
								(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
							}
							else if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
							{
							}
							else if (lngCode == modCustomBill.CNSTCUSTOMBILLSIGNATURE)
							{
							}
							//FC:FINAL:DDU:#i1754 - added barcode control
							else if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE)
							{
								(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.Barcode).Text = CustomBillClass.GetDataByCode(lngCode, aryFieldsToUpdate[x].FieldID);
							}
							else
							{
                                //FC:FINAL:MSH - i.issue #1703: wrong field type
								//(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = CustomBillClass.GetDataByCode(lngCode, aryFieldsToUpdate[x].FieldID);
								(Detail.Controls[aryFieldsToUpdate[x].Title] as GrapeCity.ActiveReports.SectionReportModel.Label).Text = CustomBillClass.GetDataByCode(lngCode, aryFieldsToUpdate[x].FieldID);
							}
						}
						else
						{
						}
					}
					// x
				}
				// now do all of the autopops
				for (x = 1; x <= (Information.UBound(aryAutoPopIDs, 1)); x++)
				{
					if (aryAutoPopIDs[x] != string.Empty && aryAutoPopIDs[x] != "0")
					{
						(Detail.Controls[aryAutoPopIDs[x]] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = aryAutoPopText[x];
					}
				}
				// x
			}
		}

		
	}
}
