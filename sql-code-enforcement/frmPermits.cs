//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWCE0000
{
	public partial class frmPermits : BaseForm
	{
		public frmPermits()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmPermits InstancePtr
		{
			get
			{
				return (frmPermits)Sys.GetInstance(typeof(frmPermits));
			}
		}

		protected frmPermits _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDVIOLATIONSCOLAUTOID = 0;
		const int CNSTGRIDVIOLATIONSCOLIDENTIFIER = 1;
		const int CNSTGRIDVIOLATIONSCOLTYPE = 2;
		const int CNSTGRIDVIOLATIONSCOLSTATUS = 4;
		const int CNSTGRIDVIOLATIONSCOLISSUED = 3;
		const int CNSTGRIDESTIMATECOLTYPE = 0;
		const int CNSTGRIDESTIMATECOLCategory = 1;
		const int CNSTGRIDESTIMATECOLCOMMRES = 2;
		const int CNSTGRIDESTIMATECOLSTART = 3;
		const int CNSTGRIDESTIMATECOLEND = 4;
		const int CNSTGRIDESTIMATECOLVALUE = 5;
		const int CNSTGRIDESTIMATECOLFEE = 6;
		const int CNSTGRIDINSPECTIONCOLAUTOID = 0;
		const int CNSTGRIDINSPECTIONCOLSTATUS = 3;
		const int CNSTGRIDINSPECTIONCOLDATE = 1;
		const int CNSTGRIDINSPECTIONCOLTYPE = 2;
		const int CNSTGRIDINSPECTIONCOLINSPECTOR = 4;
		const int CNSTGRIDSUBTYPECOLPARENTCODE = 0;
		const int CNSTGRIDSUBTYPECOLCOMBOLIST = 1;
		const int CNSTGRIDOPENSCOLAUTOID = 0;
		const int CNSTGRIDOPENSCOLDESC = 1;
		const int CNSTGRIDOPENSCOLVALUE = 2;
		// Private Const CNSTGRIDOPENSCOLDATE = 3
		// Private Const CNSTGRIDOPENSCOLUSEVALUE = 4
		// Private Const CNSTGRIDOPENSCOLUSEDATE = 5
		const int CNSTGRIDOPENSCOLVALUEFORMAT = 3;
		const int CNSTGRIDOPENSCOLMANDATORY = 4;
		const int CNSTGRIDOPENSCOLMIN = 5;
		const int CNSTGRIDOPENSCOLMAX = 6;
		const int CNSTGRIDACTIVITYCOLAUTOID = 0;
		const int CNSTGRIDACTIVITYCOLDATE = 1;
		const int CNSTGRIDACTIVITYCOLTYPE = 2;
		const int CNSTGRIDACTIVITYCOLDESC = 3;
		const int CNSTGRIDCHECKLISTCOLAUTOID = 1;
		const int CNSTGRIDCHECKLISTCOLCHECKLISTITEMID = 2;
		const int CNSTGRIDCHECKLISTCOLCOMPLETED = 3;
		const int CNSTGRIDCHECKLISTCOLDATECOMPLETED = 4;
		const int CNSTGRIDCHECKLISTCOLDESCRIPTION = 5;
		const int CNSTGRIDCHECKLISTCOLMANDATORY = 6;
		const int CNSTGRIDCHECKLISTCOLHEADER = 0;
		private int lngCurrentPermit;
		private int lngCurrentAccount;
		private int lngPermitNumber;
		private bool boolPermitComplete;
		private bool boolUseInspections;
		private bool boolDataChanged;
		private bool boolCantEdit;
		private clsPermitContractorList ContList = new clsPermitContractorList();
		private clsActivityList alList = new clsActivityList();
		// vbPorter upgrade warning: boolDontUpdateContractorCmb As object	OnWrite(bool)
		private object boolDontUpdateContractorCmb;
		// vbPorter upgrade warning: lngPermit As int	OnWrite(int, string)
		public int Init(int lngPermit, int lngAccount = 0)
		{
			int Init = 0;
			lngCurrentPermit = lngPermit;
			lngCurrentAccount = lngAccount;
			if (lngAccount == 0 && lngPermit <= 0)
			{
				MessageBox.Show("Error loading permit" + "\r\n" + "Missing account number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return Init;
			}
			boolPermitComplete = false;
			Init = 0;
			SetupForm();
			LoadPermit();
			SetupGridChecklist();
			FillAccountInformation();
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngCurrentPermit;
			boolDataChanged = false;
			return Init;
		}

		private void LoadGridChecklist()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				if (lngCurrentPermit > 0)
				{
					int lngRow;
					rsLoad.OpenRecordset("select * from inspectionchecklist where inspectionid = " + FCConvert.ToString(lngCurrentPermit), modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridChecklist.Rows - 1; lngRow++)
					{
						if (rsLoad.FindFirstRecord("checklistitemid", Conversion.Val(GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCHECKLISTITEMID))))
						{
							GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
							GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCOMPLETED, FCConvert.ToString(rsLoad.Get_Fields_Boolean("completed")));
							if (Information.IsDate(rsLoad.Get_Fields("datecompleted")))
							{
                                //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                                //if (Convert.ToDateTime(rsLoad.Get_Fields("datecompleted")).ToOADate() != 0)
                                if (rsLoad.Get_Fields_DateTime("datecompleted").ToOADate() != 0)
								{
                                    //FC:FINAL:MSH - i.issue #1728: get date in a short format
                                    //GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED, FCConvert.ToString(rsLoad.Get_Fields("datecompleted")));
                                    GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED, rsLoad.Get_Fields_DateTime("datecompleted").ToShortDateString());
								}
							}
							GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDESCRIPTION, FCConvert.ToString(rsLoad.Get_Fields_String("comment")));
						}
					}
					// lngRow
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridCheckList", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveGridChecklist()
		{
			bool SaveGridChecklist = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				bool boolMatch = false;
				SaveGridChecklist = false;
				if (lngCurrentPermit > 0)
				{
					rsSave.OpenRecordset("select * from inspectionchecklist where inspectionid = " + FCConvert.ToString(lngCurrentPermit), modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridChecklist.Rows - 1; lngRow++)
					{
						boolMatch = false;
						if (!rsSave.EndOfFile())
						{
							if (rsSave.FindFirstRecord("ID", GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID)))
							{
								boolMatch = true;
							}
						}
						if (boolMatch)
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
						rsSave.Set_Fields("InspectionID", lngCurrentPermit);
						rsSave.Set_Fields("checklistitemid", FCConvert.ToString(Conversion.Val(GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCHECKLISTITEMID))));
						rsSave.Set_Fields("completed", GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCOMPLETED));
						rsSave.Set_Fields("Comment", fecherFoundation.Strings.Trim(GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDESCRIPTION)));
						if (Information.IsDate(GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED)))
						{
							rsSave.Set_Fields("datecompleted", GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLDATECOMPLETED));
						}
						else
						{
							rsSave.Set_Fields("DateCOmpleted", 0);
						}
						rsSave.Update();
						GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID, FCConvert.ToString(rsSave.Get_Fields_Int32("ID")));
					}
					// lngRow
				}
				SaveGridChecklist = true;
				return SaveGridChecklist;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridChecklist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridChecklist;
		}

		private void chkCertificateOfOccupancy_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkCertificateOfOccupancy.CheckState == Wisej.Web.CheckState.Checked)
			{
				lblOccupancyDate.Visible = true;
				t2kOccupancyDate.Visible = true;
			}
			else
			{
				lblOccupancyDate.Visible = false;
				t2kOccupancyDate.Visible = false;
			}
		}

		private void cmbContractor_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			//App.DoEvents();
			if (!FCConvert.ToBoolean(boolDontUpdateContractorCmb))
			{
				if (cmbContractor.SelectedIndex > 0)
				{
					// contractor
					lblSuspended.Visible = false;
					lblContractorName.Visible = false;
					txtContractorName.Visible = false;
					txtContractorAddr1.Enabled = false;
					txtContractorAddr2.Enabled = false;
					txtContractCity.Enabled = false;
					txtContractorState.Enabled = false;
					txtContractorZip.Enabled = false;
					txtContractorZip4.Enabled = false;
					txtContractorEmail.Enabled = false;
					GridContractorPhone.Enabled = false;
					GridContractorPhone.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
					clsLoad.OpenRecordset("select * from contractors where ID = " + FCConvert.ToString(cmbContractor.ItemData(cmbContractor.SelectedIndex)), modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						txtContractorName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Name1"));
						txtContractorAddr1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address1"));
						txtContractorAddr2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address2"));
						txtContractCity.Text = FCConvert.ToString(clsLoad.Get_Fields_String("city"));
						txtContractorState.Text = FCConvert.ToString(clsLoad.Get_Fields("state"));
						txtContractorZip.Text = FCConvert.ToString(clsLoad.Get_Fields_String("zip"));
						txtContractorZip4.Text = FCConvert.ToString(clsLoad.Get_Fields_String("zip4"));
						txtContractorEmail.Text = FCConvert.ToString(clsLoad.Get_Fields_String("email"));
						if (Conversion.Val(clsLoad.Get_Fields_Int32("CONTRACTORSTATUS")) == modCEConstants.CNSTCONTRACTORSTATUSWARNING)
						{
							lblSuspended.Visible = true;
						}
						else
						{
							lblSuspended.Visible = false;
						}
					}
					clsLoad.OpenRecordset("select * from phonenumbers where phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPECONTRACTOR) + " and parentid = " + FCConvert.ToString(cmbContractor.ItemData(cmbContractor.SelectedIndex)) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
					GridContractorPhone.Rows = 1;
					while (!clsLoad.EndOfFile())
					{
						GridContractorPhone.Rows += 1;
						lngRow = GridContractorPhone.Rows - 1;
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(0));
						// want it so save as new
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("PHONEdescription")));
						strTemp = Strings.Right(Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("Phonenumber"), 10);
						strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER, strTemp);
						clsLoad.MoveNext();
					}
				}
				else
				{
					// no contractor
					lblSuspended.Visible = false;
					GridContractorPhone.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
					lblContractorName.Visible = true;
					txtContractorName.Visible = true;
					if (!boolCantEdit)
					{
						txtContractorAddr1.Enabled = true;
						txtContractorAddr2.Enabled = true;
						txtContractCity.Enabled = true;
						txtContractorState.Enabled = true;
						txtContractorZip.Enabled = true;
						txtContractorZip4.Enabled = true;
						txtContractorEmail.Enabled = true;
						GridContractorPhone.Enabled = true;
					}
					else
					{
						txtContractorAddr1.Enabled = false;
						txtContractorAddr2.Enabled = false;
						txtContractCity.Enabled = false;
						txtContractorState.Enabled = false;
						txtContractorZip.Enabled = false;
						txtContractorZip4.Enabled = false;
						txtContractorEmail.Enabled = false;
						GridContractorPhone.Enabled = false;
					}
					clsLoad.OpenRecordset("select * from phonenumbers where phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPEPERMITCONTRACTOR) + " and parentid = " + FCConvert.ToString(lngCurrentPermit) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
					GridContractorPhone.Rows = 1;
					while (!clsLoad.EndOfFile())
					{
						GridContractorPhone.Rows += 1;
						lngRow = GridContractorPhone.Rows - 1;
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
						// want it so save as new
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("PHONEdescription")));
						strTemp = Strings.Right(Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("Phonenumber"), 10);
						strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER, strTemp);
						clsLoad.MoveNext();
					}
				}
			}
		}

		private void frmPermits_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPermits_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPermits properties;
			//frmPermits.FillStyle	= 0;
			//frmPermits.ScaleWidth	= 9300;
			//frmPermits.ScaleHeight	= 7860;
			//frmPermits.LinkTopic	= "Form2";
			//frmPermits.LockControls	= -1  'True;
			//frmPermits.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// SetTRIOColors Me
			lblSuspended.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			lblSuspended.Font = FCUtils.FontChangeSize(lblSuspended.Font, 12);
			// Set ImgComment.Picture = MDIParent.ImageList2.ListImages(1).Picture
			boolCantEdit = false;
			Picture1.BackColor = Color.White;
			boolDontUpdateContractorCmb = false;
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITPERMITSECURITY))
			{
				boolCantEdit = true;
				DisableControls();
			}

            Frame1.Height = Label18.Location.Y + Label18.Height + 10;
            this.Picture1.ImageSource = "node-plus";
        }

		private void SetupForm()
		{
			if (lngCurrentPermit > 0)
			{
				txtYear.Visible = false;
				lblYear.Text = "Permit";
				lblPermitNumber.Visible = true;
			}
			else
			{
				txtYear.Visible = true;
				lblYear.Text = "Year";
				// vbPorter upgrade warning: intyeartouse As int	OnWriteFCConvert.ToInt32(
				int intyeartouse = 0;
				intyeartouse = DateTime.Today.Year;
				intyeartouse += modGlobalVariables.Statics.CECustom.AddToYear;
				if (DateTime.Today.Month < modGlobalVariables.Statics.CECustom.StartMonth)
				{
					intyeartouse -= 1;
				}
				txtYear.Text = FCConvert.ToString(intyeartouse);
				lblPermitNumber.Visible = false;
			}
			FillCmbApprovedDenied();
			FillCmbContractor();
			SetupGridEstimate();
			SetupGridInspections();
			SetupGridPhones();
			SetupGridOpens(0);
			SetupGridActivity();
			SetupGridViolations();
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDataChanged && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save now?", "Save First?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SavePermit())
					{
						e.Cancel = true;
					}
					else
					{
						ContList = null;
					}
				}
				else
				{
					ContList = null;
				}
			}
			else
			{
				ContList = null;
			}
		}

		private void frmPermits_Resize(object sender, System.EventArgs e)
		{
			ResizeGridEstimate();
			ResizeGridInspections();
			ResizeGridPhones();
			ResizeGridOpens();
			ResizeGridActivity();
			ResizeGridViolations();
			ResizeGridCheckList();
			//Toolbar1.Width = 3 * Toolbar1.ButtonWidth;
			//Toolbar1.Left = FCConvert.ToInt32((Frame1.WidthOriginal - Toolbar1.Width) / 2.0);
			//if (Toolbar1.Height < (cmbContractor.TopOriginal))
			//{
			//	Toolbar1.Top = FCConvert.ToInt32((cmbContractor.TopOriginal - Toolbar1.ButtonHeight) / 2.0);
			//}
		}

		private void SetupGridChecklist()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngPermitType;
				lngPermitType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE))));
				if (lngPermitType != Conversion.Val(GridInspections.Tag) || lngPermitType == 0)
				{
					rsLoad.OpenRecordset("select * from inspectionchecklistsetup where permittype = " + FCConvert.ToString(lngPermitType) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
					GridChecklist.Rows = 1;
					GridChecklist.TextMatrix(0, CNSTGRIDCHECKLISTCOLDATECOMPLETED, "Date");
					GridChecklist.TextMatrix(0, CNSTGRIDCHECKLISTCOLDESCRIPTION, "Description");
					GridChecklist.TextMatrix(0, CNSTGRIDCHECKLISTCOLCOMPLETED, "");
					GridChecklist.ColDataType(CNSTGRIDCHECKLISTCOLCOMPLETED, FCGrid.DataTypeSettings.flexDTBoolean);
					// .ColDataType(CNSTGRIDCHECKLISTCOLDATECOMPLETED) = flexDTDate
					GridChecklist.ColDataType(CNSTGRIDCHECKLISTCOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
					GridChecklist.ColHidden(CNSTGRIDCHECKLISTCOLAUTOID, true);
					GridChecklist.ColHidden(CNSTGRIDCHECKLISTCOLCHECKLISTITEMID, true);
					GridChecklist.ColHidden(CNSTGRIDCHECKLISTCOLMANDATORY, true);
					//GridChecklist.ColAlignment(CNSTGRIDCHECKLISTCOLDATECOMPLETED, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					//GridChecklist.ColAlignment(CNSTGRIDCHECKLISTCOLDESCRIPTION, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					GridChecklist.ColEditMask(CNSTGRIDCHECKLISTCOLDATECOMPLETED, "00/00/0000");
					int lngRow;
					while (!rsLoad.EndOfFile())
					{
						GridChecklist.Rows += 1;
						lngRow = GridChecklist.Rows - 1;
						GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCHECKLISTITEMID, FCConvert.ToString(rsLoad.Get_Fields_Int32("ID")));
						GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLAUTOID, FCConvert.ToString(0));
						GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLCOMPLETED, FCConvert.ToString(false));
						GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLMANDATORY, FCConvert.ToString(rsLoad.Get_Fields_Boolean("mandatory")));
						GridChecklist.TextMatrix(lngRow, CNSTGRIDCHECKLISTCOLHEADER, FCConvert.ToString(rsLoad.Get_Fields_String("Description")));
						if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("mandatory")))
						{
							GridChecklist.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, GridChecklist.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
						}
						rsLoad.MoveNext();
					}
					LoadGridChecklist();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridChecklist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillCmbContractor()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select ID,name1 from contractors order by name1", modGlobalVariables.Statics.strCEDatabase);
			cmbContractor.Clear();
			cmbContractor.AddItem("Other");
			cmbContractor.ItemData(cmbContractor.NewIndex, 0);
			while (!clsLoad.EndOfFile())
			{
				cmbContractor.AddItem(clsLoad.Get_Fields_String("name1"));
				cmbContractor.ItemData(cmbContractor.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")));
				clsLoad.MoveNext();
			}
			cmbContractor.SelectedIndex = 0;
		}

		private void gridActivity_DblClick(object sender, System.EventArgs e)
		{
			if (gridActivity.MouseRow > 0)
			{
				frmActivity.InstancePtr.Init(ref boolCantEdit, FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.MouseRow, CNSTGRIDACTIVITYCOLAUTOID))), lngCurrentPermit, modCEConstants.CNSTCODETYPEPERMIT);
				if (!boolCantEdit)
				{
					alList.LoadList(lngCurrentAccount, modCEConstants.CNSTCODETYPEPERMIT, lngCurrentPermit);
					FillActivityGrid();
				}
			}
		}

		private void gridActivity_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 3;
		}

		private void gridActivity_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (!boolCantEdit)
			{
				switch (KeyCode)
				{
					case Keys.Delete:
						{
							KeyCode = 0;
							if (gridActivity.Rows > 0)
							{
								if (MessageBox.Show("This will delete this activity entry" + "\r\n" + "Do you want to continue?", "Delete Activity?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
								{
									clsActivity tAct;
									tAct = alList.GetActivityByID(FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.Row, CNSTGRIDACTIVITYCOLAUTOID))));
									if (!(tAct == null))
									{
										tAct.DeleteActivity();
									}
									alList.RemovebyID(FCConvert.ToInt32(Conversion.Val(gridActivity.TextMatrix(gridActivity.Row, CNSTGRIDACTIVITYCOLAUTOID))));
									gridActivity.RemoveItem(gridActivity.Row);
								}
							}
							break;
						}
					case Keys.Insert:
						{
							if (!boolCantEdit)
							{
								AddActivity();
							}
							break;
						}
				}
				//end switch
			}
		}

		private void AddActivity()
		{
			if (frmActivity.InstancePtr.Init(ref boolCantEdit, 0, lngCurrentPermit, modCEConstants.CNSTCODETYPEPERMIT, 0, 0, lngCurrentAccount) > 0)
			{
				alList.LoadList(lngCurrentAccount, modCEConstants.CNSTCODETYPEPERMIT, lngCurrentPermit);
				FillActivityGrid();
			}
		}

		private void FillActivityGrid()
		{
			gridActivity.Rows = 1;
			int lngRow;
			alList.MoveFirst();
			alList.MovePrevious();
			clsAttachedDocuments dlAttachedDocs = new clsAttachedDocuments();
			clsActivity tAct;
			while (alList.MoveNext() > 0)
			{
				tAct = alList.GetCurrentActivity();
				if (!(tAct == null))
				{
					gridActivity.Rows += 1;
					lngRow = gridActivity.Rows - 1;
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLAUTOID, FCConvert.ToString(tAct.ID));
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLDATE, Strings.Format(tAct.ActivityDate, "MM/dd/yyyy"));
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLDESC, tAct.Description);
					gridActivity.TextMatrix(lngRow, CNSTGRIDACTIVITYCOLTYPE, tAct.ActivityType);
					dlAttachedDocs.LoadDocuments(tAct.ID, modCEConstants.CNSTCODETYPEACTIVITY.ToString(), modGlobalVariables.Statics.strCEDatabase);
					if (dlAttachedDocs.Count > 0)
					{
                        //FC:FINAL:IPI - index correction: ImageList starts with index 1 in VB6
                        //gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDACTIVITYCOLDESC, MDIParent.InstancePtr.CEImageList.Images[2]);
                        gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDACTIVITYCOLDESC, MDIParent.InstancePtr.CEImageList.Images[1]);
                    }
					gridActivity.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDACTIVITYCOLDESC, FCGrid.PictureAlignmentSettings.flexPicAlignRightCenter);
				}
			}
		}

		private void gridActivity_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = gridActivity.MouseRow;
			if (mRow < 0)
			{
				gridActivity.Row = 0;
				gridActivity.Focus();
			}
		}

		private void GridChecklist_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 5;
		}

		private void GridChecklist_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			Row = GridChecklist.Row;
			Col = GridChecklist.Col;
			if (Row < 0)
				return;
			if (Col < 0)
				return;
			GridEstimate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (Row >= GridChecklist.Rows - 1)
			{
				if (Col == CNSTGRIDCHECKLISTCOLDESCRIPTION)
				{
					GridChecklist.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					GridChecklist.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
		}

		private void GridContractorPhone_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						GridContractorPhone.Rows += 1;
						GridContractorPhone.TopRow = GridContractorPhone.Rows - 1;
						GridContractorPhone.TextMatrix(GridContractorPhone.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
						GridContractorPhone.Select(GridContractorPhone.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER);
						GridContractorPhone.EditCell();
						GridContractorPhone.EditSelLength = 0;
						GridContractorPhone.EditSelStart = 1;
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridContractorPhone.Row < 1)
							return;
						DeleteContractorPhone(GridContractorPhone.Row);
						break;
					}
			}
			//end switch
		}

		private void GridContractorPhone_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridContractorPhone.MouseRow;
			if (mRow < 0)
			{
				GridContractorPhone.Row = 0;
				GridContractorPhone.Focus();
			}
		}

		private void GridContractorPhone_RowColChange(object sender, System.EventArgs e)
		{
			if (GridContractorPhone.Col == modCEConstants.CNSTGRIDPHONESCOLDESC && GridContractorPhone.Row == GridContractorPhone.Rows - 1)
			{
				GridContractorPhone.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				GridContractorPhone.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void GridEstimate_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
            //FC:FINAL:MSH - i.issue #1687: save and use correct indexes of the cell
            int row = GridEstimate.GetFlexRowIndex(e.RowIndex);
            int col = GridEstimate.GetFlexColIndex(e.ColumnIndex);

            switch (col)
			{
				case CNSTGRIDESTIMATECOLTYPE:
					{
						if (row > 0)
						{
							int lngType = 0;
							lngType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridEstimate.TextMatrix(row, CNSTGRIDESTIMATECOLTYPE))));
							// Call CheckCategory(lngType)
							CheckType(lngType);
							SetupGridChecklist();
							// SetupSubTypes (Val(GridEstimate.TextMatrix(Row, CNSTGRIDESTIMATECOLTYPE)))
						}
						break;
					}
			}
			//end switch
		}

		private void CheckType(int lngType)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				SetupGridOpens(lngType);
				LoadGridOpens();
				rsLoad.OpenRecordset("select * from systemcodes where ID = " + FCConvert.ToString(lngType) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE), modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("booloption")))
					{
						// inspectionless
						boolUseInspections = false;
						SSTab1.TabPages[1].Visible = false;
						cmdAddInspection.Visible = false;
					}
					else
					{
						// not inspectionless
						boolUseInspections = true;
						SSTab1.TabPages[1].Visible = true;
						cmdAddInspection.Visible = true;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In CheckType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Private Sub GridEstimate_ChangeEdit()
		// If GridEstimate.Col = CNSTGRIDESTIMATECOLSUBTYPE And Trim(GridEstimate.ColComboList(CNSTGRIDESTIMATECOLSUBTYPE)) = vbNullString Then
		// If GridEstimate.EditText <> "" Then GridEstimate.EditText = ""
		// If GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSUBTYPE) <> "" Then
		// GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSUBTYPE) = ""
		// End If
		// End If
		// End Sub
		private void GridEstimate_ComboCloseUp(object sender, EventArgs e)
		{
			int lngCol;
			lngCol = GridEstimate.Col;
			if (GridEstimate.Row < 1)
				return;
			switch (lngCol)
			{
				case CNSTGRIDESTIMATECOLTYPE:
					{
                        //FC:FINAL:MSH - i.issue #1687: force trigger EndEdit to execute validating for current cell, because in FCGrid won't be changed CurrentCell
                        // (DGVRowIndex will be equal -1) and validating won't be executed
                        GridEstimate.EndEdit();
						GridEstimate.Row = 0;
                        break;
					}
			}
			//end switch
		}
		// Private Sub CheckCategory(lngType As Long)
		// Dim clsLoad As New clsdrwrapper
		// Dim lngCat As Long
		// Dim lngRow As Long
		//
		// On Error GoTo ErrorHandler
		//
		// Call clsLoad.OpenRecordset("select * from systemcodes where code = " & lngType & " and codetype = " & CNSTCODETYPEPERMITTYPE, strCEDatabase)
		// If Not clsLoad.EndOfFile Then
		// lngCat = Val(clsLoad.Fields("codespecificid"))
		// If lngCat = CNSTCATEGORYDEFAULT Then
		// boolUseInspections = True
		// SSTab1.TabVisible(1) = True
		// mnuAddInspection.Visible = True
		// For lngRow = 1 To GridOpens.Rows - 1
		// GridOpens.RowHidden(lngRow) = False
		// Next lngRow
		// Else
		// Call clsLoad.OpenRecordset("select * from categories where ID = " & lngCat, strCEDatabase)
		// If Not clsLoad.EndOfFile Then
		// boolUseInspections = clsLoad.Fields("REQUIREinspections")
		// Else
		// boolUseInspections = True
		// End If
		// If boolUseInspections Then
		// SSTab1.TabVisible(1) = True
		// mnuAddInspection.Visible = True
		// Else
		// SSTab1.TabVisible(1) = False
		// mnuAddInspection.Visible = False
		// End If
		// Call clsLoad.OpenRecordset("select * from catcodeoptions where categoryid = " & lngCat & " and codetype = " & CNSTCODETYPEPERMIT & " order by usercodeid", strCEDatabase)
		// For lngRow = 1 To GridOpens.Rows - 1
		// If clsLoad.FindFirstRecord("usercodeid", Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))) Then
		// GridOpens.RowHidden(lngRow) = Not (clsLoad.Fields("boolshow"))
		// Else
		// GridOpens.RowHidden(lngRow) = False
		// End If
		// Next lngRow
		// End If
		// End If
		//
		// Exit Sub
		// ErrorHandler:
		// MsgBox "Error Number " & Err.Number & " " & Err.Description & vbNewLine & "In CheckCategory", vbCritical
		// End Sub
		private void GridEstimate_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			// Row = GridEstimate.MouseRow
			// Col = GridEstimate.MouseCol
			Row = GridEstimate.Row;
			Col = GridEstimate.Col;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDESTIMATECOLEND:
					case CNSTGRIDESTIMATECOLSTART:
						{
							GridEstimate.EditMask = "00/00/0000";
							break;
						}
					case CNSTGRIDESTIMATECOLVALUE:
						{
							GridEstimate.EditMask = "";
							break;
						}
				}
				//end switch
			}
			if (GridEstimate.Col == CNSTGRIDESTIMATECOLFEE)
			{
				GridEstimate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				GridEstimate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void GridEstimate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - i.issue #1687: save and use correct indexes of the cell
            int row = GridEstimate.GetFlexRowIndex(e.RowIndex);
            int col = GridEstimate.GetFlexColIndex(e.ColumnIndex);

			string strTemp = "";
			if (row < 1)
				return;
			GridEstimate.EditMask = "";
			switch (col)
			{
				case CNSTGRIDESTIMATECOLTYPE:
					{
						break;
					}
				case CNSTGRIDESTIMATECOLEND:
				case CNSTGRIDESTIMATECOLSTART:
					{
						// strTemp = GridEstimate.EditText
						// If IsDate(strTemp) Then
						// strTemp = Format(strTemp, "MM/dd/yyyy")
						// Else
						// strTemp = ""
						// End If
						// GridEstimate.EditText = strTemp
						break;
					}
				case CNSTGRIDESTIMATECOLVALUE:
					{
						strTemp = GridEstimate.EditText;
						strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
						strTemp = FCConvert.ToString(Conversion.Val(strTemp));
						strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
						GridEstimate.EditText = strTemp;
						break;
					}
			}
			//end switch
		}

		private void GridInspections_DblClick(object sender, System.EventArgs e)
		{
			int lngID;
			if (GridInspections.MouseRow < 1)
				return;
			if (fecherFoundation.Strings.UCase(FCConvert.ToString(GridInspections.Tag)) == "UNAUTHORIZED")
				return;
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridInspections.TextMatrix(GridInspections.MouseRow, CNSTGRIDINSPECTIONCOLAUTOID))));
			if (frmInspection.InstancePtr.Init(lngID, lngCurrentPermit, FCConvert.ToInt32(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE)))))
			{
				// details might have changed so update the grid
				FillGridInspections();
			}
			else
			{
				FillGridInspections(true);
			}
			FillGridViolations();
		}

		private void GridInspections_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 1;
		}

        private void GridInspections_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridInspections.GetFlexRowIndex(e.RowIndex);
            int col = GridInspections.GetFlexColIndex(e.ColumnIndex);

            if (row < 1)
				return;
			string strTemp = "";
			string[] strAry = null;
			GridOpens.EditMask = "";
			if (col == CNSTGRIDOPENSCOLVALUE)
			{
				if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					strTemp = GridOpens.EditText;
					if (Information.IsDate(strTemp))
					{
						strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					}
					else
					{
						strTemp = "";
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strAry = Strings.Split(strTemp, ".", -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Format(strAry[0], "#,###,###,##0");
					if (Information.UBound(strAry, 1) > 0)
					{
						strTemp += "." + strAry[1];
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					strTemp = GridOpens.ComboData();
					GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, Conversion.Val(strTemp));
				}
				// Case CNSTGRIDOPENSCOLDATE
			}
		}

		private void GridOpens_Enter(object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 0;
		}

		private void GridOpens_RowColChange(object sender, System.EventArgs e)
		{
			int Row;
			int Col;
			Row = GridOpens.MouseRow;
			Col = GridOpens.MouseCol;
			if (Row > 0)
			{
				switch (Col)
				{
					case CNSTGRIDOPENSCOLVALUE:
						{
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEVALUE)) Then
							if (!(FCConvert.ToString(GridOpens.Tag) == "UNAUTHORIZED") && !boolCantEdit)
							{
								GridOpens.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(Row));
							if (Conversion.Val(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
							{
								GridOpens.EditMask = "00/00/0000";
							}
							else
							{
								GridOpens.EditMask = "";
							}
							if (Row >= GridOpens.Rows - 1)
							{
								GridEstimate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							}
							else
							{
								GridEstimate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
							}
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							// Case CNSTGRIDOPENSCOLDATE
							// GridOpens.ComboList = ""
							// GridOpens.EditMask = "00/00/0000"
							// If CBool(GridOpens.TextMatrix(Row, CNSTGRIDOPENSCOLUSEDATE)) Then
							// GridOpens.Editable = flexEDKbdMouse
							// Else
							// GridOpens.Editable = flexEDNone
							// End If
							break;
						}
					default:
						{
							GridEstimate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
							break;
						}
				}
				//end switch
			}
		}

		private void GridPhones_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						GridPhones.Rows += 1;
						GridPhones.TopRow = GridPhones.Rows - 1;
						GridPhones.TextMatrix(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
						GridPhones.TextMatrix(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(0));
						GridPhones.Select(GridPhones.Rows - 1, modCEConstants.CNSTGRIDPHONESCOLNUMBER);
						GridPhones.EditCell();
						GridPhones.EditSelLength = 0;
						GridPhones.EditSelStart = 1;
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (GridPhones.Row < 1)
							return;
						DeletePhone(GridPhones.Row);
						break;
					}
			}
			//end switch
		}

		private void DeletePhone(int lngRow)
		{
			if (boolCantEdit)
				return;
			GridDeletedPhones.Rows += 1;
			GridDeletedPhones.TextMatrix(GridDeletedPhones.Rows - 1, 0, FCConvert.ToString(Conversion.Val(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID))));
			GridPhones.RemoveItem(lngRow);
		}

		private void DeleteContractorPhone(int lngRow)
		{
			if (boolCantEdit)
				return;
			GridDeletedPhones.Rows += 1;
			GridDeletedPhones.TextMatrix(GridDeletedPhones.Rows - 1, 0, FCConvert.ToString(Conversion.Val(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID))));
			GridContractorPhone.RemoveItem(lngRow);
		}

		private void GridPhones_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = GridPhones.MouseRow;
			if (mRow < 0)
			{
				GridPhones.Row = 0;
				GridPhones.Focus();
			}
		}

		private void GridPhones_RowColChange(object sender, System.EventArgs e)
		{
			if (GridPhones.Col == modCEConstants.CNSTGRIDPHONESCOLDESC && GridPhones.Row == GridPhones.Rows - 1)
			{
				GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				GridPhones.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void gridViolations_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = gridViolations.MouseRow;
			if (mRow < 0)
			{
				gridViolations.Row = 0;
				gridViolations.Focus();
			}
		}

		private void ImgComment_Click(object sender, System.EventArgs e)
		{
			mnuComment_Click();
		}

		private void mnuAddActivity_Click(object sender, System.EventArgs e)
		{
			if (!boolCantEdit)
			{
				AddActivity();
			}
			else
			{
				MessageBox.Show("You cannot add activity until this permit has been created", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}

		private void mnuAddInspection_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentPermit < 1)
			{
				MessageBox.Show("You cannot add an inspection until this permit has been created", "Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (cmbApprovedDenied.SelectedIndex >= 0)
			{
				if (cmbApprovedDenied.ItemData(cmbApprovedDenied.SelectedIndex) == modCEConstants.CNSTPERMITCOMPLETE)
				{
					MessageBox.Show("You cannot add an inspection to a completed permit", "Invalid Option", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			frmInspection.InstancePtr.Init(0, lngCurrentPermit, FCConvert.ToInt32(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE))));
			FillGridInspections();
			FillGridViolations();
		}

		private void mnuComment_Click(object sender, System.EventArgs e)
		{
			bool boolNonEditable = false;
			if (modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITPERMITSECURITY, false))
			{
				boolNonEditable = false;
			}
			else
			{
				boolNonEditable = true;
			}
			if (lngCurrentPermit > 0)
			{
				if (!frmGlobalComment.InstancePtr.Init("CE", "Permits", "Comment", "ID", lngCurrentPermit, "Comment", "", 0, boolNonEditable, true))
				{
					ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
				}
				else
				{
					ImgComment.Image = null;
				}
			}
			else
			{
				MessageBox.Show("You must save and create the permit before you can assign it a comment", "Save First", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void mnuComment_Click()
		{
			mnuComment_Click(mnuComment, new System.EventArgs());
		}

		private void mnuDeletePermit_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentPermit > 0)
			{
				if (MessageBox.Show("Are you sure you want to delete this permit?", "Delete?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (modMain.DeletePermit(lngCurrentPermit))
					{
						mnuExit_Click();
					}
				}
			}
			else
			{
				mnuExit_Click();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillCmbApprovedDenied()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEAPPROVALDENIAL) + " order by code", modGlobalVariables.Statics.strCEDatabase);
			cmbApprovedDenied.Clear();
			while (!clsLoad.EndOfFile())
			{
				cmbApprovedDenied.AddItem(clsLoad.Get_Fields_String("description"));
				cmbApprovedDenied.ItemData(cmbApprovedDenied.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("code"))));
				clsLoad.MoveNext();
			}
			cmbApprovedDenied.AddItem("Complete");
			cmbApprovedDenied.ItemData(cmbApprovedDenied.NewIndex, modCEConstants.CNSTPERMITCOMPLETE);
		}

		private void SetupGridEstimate()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				string strTemp = "";
				int lngLastParent;
				int lngRow;
				GridEstimate.Cols = 7;
				GridEstimate.Rows = 2;
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLSTART, "Start");
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLEND, "Complete");
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLVALUE, "Value");
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLTYPE, "Type");
				// .TextMatrix(0, CNSTGRIDESTIMATECOLSUBTYPE) = "Sub Type"
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLCategory, "Category");
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLCOMMRES, "Comm/Res");
				GridEstimate.TextMatrix(0, CNSTGRIDESTIMATECOLFEE, "Fee");
				//GridEstimate.ColAlignment(CNSTGRIDESTIMATECOLVALUE, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//GridEstimate.ColAlignment(CNSTGRIDESTIMATECOLSTART, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//GridEstimate.ColAlignment(CNSTGRIDESTIMATECOLEND, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				GridEstimate.ColAlignment(CNSTGRIDESTIMATECOLFEE, FCGrid.AlignmentSettings.flexAlignRightCenter);
				GridEstimate.ColFormat(CNSTGRIDESTIMATECOLFEE, "0.00");
				strTemp = "";
				clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))) + ";" + clsLoad.Get_Fields_String("description") + "|";
					clsLoad.MoveNext();
				}
				if (!(strTemp == string.Empty))
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					GridEstimate.ColComboList(CNSTGRIDESTIMATECOLTYPE, strTemp);
				}
				strTemp = "#0;None|";
				clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITCATEGORIES) + " order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))) + ";" + clsLoad.Get_Fields_String("Description") + "|";
					clsLoad.MoveNext();
				}
				if (!(strTemp == string.Empty))
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					GridEstimate.ColComboList(CNSTGRIDESTIMATECOLCategory, strTemp);
				}
				// strTemp = ""
				// lngLastParent = -1
				// lngRow = -1
				// gridSubTypes.Rows = 0
				// Call clsLoad.OpenRecordset("select * from systemcodes where codetype = " & CNSTCODETYPEPERMITSUBTYPE & " order by PARENTCODE,description", strCEDatabase)
				// Do While Not clsLoad.EndOfFile
				// If lngLastParent <> Val(clsLoad.Fields("parentcode")) Then
				// If lngRow >= 0 Then
				// If strTemp <> vbNullString Then
				// strTemp = Mid(strTemp, 1, Len(strTemp) - 1)
				// gridSubTypes.Rows = gridSubTypes.Rows + 1
				// lngRow = gridSubTypes.Rows - 1
				// gridSubTypes.TextMatrix(lngRow, CNSTGRIDSUBTYPECOLCOMBOLIST) = strTemp
				// gridSubTypes.TextMatrix(lngRow, CNSTGRIDSUBTYPECOLPARENTCODE) = lngLastParent
				// End If
				// Else
				// lngRow = 0
				// End If
				// lngLastParent = Val(clsLoad.Fields("parentcode"))
				// strTemp = ""
				// 
				// End If
				// strTemp = strTemp & "#" & Val(clsLoad.Fields("code")) & ";" & clsLoad.Fields("description") & "|"
				// clsLoad.MoveNext
				// Loop
				// If Not strTemp = vbNullString Then
				// strTemp = Mid(strTemp, 1, Len(strTemp) - 1)
				// gridSubTypes.Rows = gridSubTypes.Rows + 1
				// lngRow = gridSubTypes.Rows - 1
				// gridSubTypes.TextMatrix(lngRow, CNSTGRIDSUBTYPECOLCOMBOLIST) = strTemp
				// gridSubTypes.TextMatrix(lngRow, CNSTGRIDSUBTYPECOLPARENTCODE) = lngLastParent
				// .ColComboList(CNSTGRIDESTIMATECOLSUBTYPE) = strTemp
				// End If
				GridEstimate.ColComboList(CNSTGRIDESTIMATECOLCOMMRES, "#" + FCConvert.ToString(modCEConstants.CNSTPERMITCODERESIDENTIAL) + ";Residential|#" + FCConvert.ToString(modCEConstants.CNSTPERMITCODECOMMERCIAL) + ";Commercial");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupGridEstimate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Private Sub SetupSubTypes(lngCode As Long)
		// Dim lngRow As Long
		// Dim lngSubCode As Long
		// Dim strTemp As String
		// Dim boolFound As Boolean
		//
		// strTemp = ""
		// lngSubCode = Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSUBTYPE))
		// For lngRow = 0 To gridSubTypes.Rows - 1
		// If Val(gridSubTypes.TextMatrix(lngRow, CNSTGRIDSUBTYPECOLPARENTCODE)) = lngCode Then
		// strTemp = gridSubTypes.TextMatrix(lngRow, CNSTGRIDSUBTYPECOLCOMBOLIST)
		// Exit For
		// End If
		// Next lngRow
		// GridEstimate.ColComboList(CNSTGRIDESTIMATECOLSUBTYPE) = strTemp
		// If Trim(strTemp) = vbNullString Then
		// GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSUBTYPE) = ""
		// ElseIf lngSubCode = Trim(GridEstimate.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, CNSTGRIDESTIMATECOLSUBTYPE)) Then
		// GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSUBTYPE) = ""
		// End If
		// End Sub
		private void SetupGridPhones()
		{
			GridPhones.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "Telephone");
			GridPhones.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLDESC, "Description");
			GridPhones.ColHidden(modCEConstants.CNSTGRIDPHONESCOLAUTOID, true);
			GridPhones.ColHidden(modCEConstants.CNSTGRIDPHONESCOLINDEX, true);
			GridPhones.ColEditMask(modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
			//GridPhones.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridContractorPhone.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, "Telephone");
			GridContractorPhone.TextMatrix(0, modCEConstants.CNSTGRIDPHONESCOLDESC, "Description");
			GridContractorPhone.ColHidden(modCEConstants.CNSTGRIDPHONESCOLAUTOID, true);
			GridContractorPhone.ColHidden(modCEConstants.CNSTGRIDPHONESCOLINDEX, true);
			GridContractorPhone.ColEditMask(modCEConstants.CNSTGRIDPHONESCOLNUMBER, "(000)000-0000");
			//GridContractorPhone.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridPhones()
		{
			int GridWidth = 0;
			GridWidth = GridPhones.WidthOriginal;
			GridPhones.ColWidth(modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCConvert.ToInt32(0.4 * GridWidth));
			GridWidth = GridContractorPhone.WidthOriginal;
			GridContractorPhone.ColWidth(modCEConstants.CNSTGRIDPHONESCOLNUMBER, FCConvert.ToInt32(0.4 * GridWidth));
		}

		private void ResizeGridEstimate()
		{
			int GridWidth = 0;
			GridWidth = GridEstimate.WidthOriginal;
			GridEstimate.ColWidth(CNSTGRIDESTIMATECOLCOMMRES, FCConvert.ToInt32(0.12 * GridWidth));
			GridEstimate.ColWidth(CNSTGRIDESTIMATECOLCategory, FCConvert.ToInt32(0.19 * GridWidth));
			GridEstimate.ColWidth(CNSTGRIDESTIMATECOLTYPE, FCConvert.ToInt32(0.27 * GridWidth));
			GridEstimate.ColWidth(CNSTGRIDESTIMATECOLSTART, FCConvert.ToInt32(0.11 * GridWidth));
			GridEstimate.ColWidth(CNSTGRIDESTIMATECOLEND, FCConvert.ToInt32(0.11 * GridWidth));
			GridEstimate.ColWidth(CNSTGRIDESTIMATECOLVALUE, FCConvert.ToInt32(0.11 * GridWidth));
			//GridEstimate.Height = GridEstimate.RowHeight(0) * 2 + 50;
		}

		private void SetupGridInspections()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			GridInspections.Cols = 5;
			GridInspections.Rows = 1;
			GridInspections.ExtendLastCol = true;
			GridInspections.ColHidden(CNSTGRIDINSPECTIONCOLAUTOID, true);
			GridInspections.ColDataType(CNSTGRIDINSPECTIONCOLDATE, FCGrid.DataTypeSettings.flexDTDate);
			GridInspections.TextMatrix(0, CNSTGRIDINSPECTIONCOLDATE, "Date");
			GridInspections.TextMatrix(0, CNSTGRIDINSPECTIONCOLINSPECTOR, "Inspector");
			GridInspections.TextMatrix(0, CNSTGRIDINSPECTIONCOLSTATUS, "Status");
			GridInspections.TextMatrix(0, CNSTGRIDINSPECTIONCOLTYPE, "Type");
			strTemp = "0#;Unknown|#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONPENDING) + ";Scheduled|#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONCOMPLETE) + ";Completed (Last)|#" + FCConvert.ToString(modCEConstants.CNSTINSPECTIONVIOLATION) + ";Violation|";
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONSTATUS), modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Code"))) + ";" + clsLoad.Get_Fields_String("description") + "|";
				clsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			GridInspections.ColComboList(CNSTGRIDINSPECTIONCOLSTATUS, strTemp);
			strTemp = "";
			clsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTIONTYPE), modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("CODE"))) + ";" + clsLoad.Get_Fields_String("description") + "|";
				clsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			GridInspections.ColComboList(CNSTGRIDINSPECTIONCOLTYPE, strTemp);
			strTemp = "#0;Unknown|";
			clsLoad.OpenRecordset("select * from inspectors order by NAME", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				strTemp += "#" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ID"))) + ";" + clsLoad.Get_Fields_String("name") + "|";
				clsLoad.MoveNext();
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			GridInspections.ColComboList(CNSTGRIDINSPECTIONCOLINSPECTOR, strTemp);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTPERMITSECURITY, false))
			{
				GridInspections.Tag = (System.Object)("UNAUTHORIZED");
			}
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITINSPECTIONSSECURITY, false))
			{
				cmdAddInspection.Enabled = false;
			}
		}

		private void ResizeGridInspections()
		{
			// vbPorter upgrade warning: GridWidth As object	OnWriteFCConvert.ToInt32(
			float GridWidth = GridInspections.WidthOriginal;
			GridInspections.ColWidth(CNSTGRIDINSPECTIONCOLDATE, FCConvert.ToInt32(0.18 * GridWidth));
			GridInspections.ColWidth(CNSTGRIDINSPECTIONCOLSTATUS, FCConvert.ToInt32(0.25 * GridWidth));
			GridInspections.ColWidth(CNSTGRIDINSPECTIONCOLTYPE, FCConvert.ToInt32(0.23 * GridWidth));
		}

		private void FillGridInspections(bool boolDontUpdateStatus = false)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				bool boolTemp = false;
				GridInspections.Rows = 1;
				clsLoad.OpenRecordset("select * from inspections where permitid = " + FCConvert.ToString(lngCurrentPermit) + " and not isnull(deleted,0) = 1 ORDER BY inspectionDATE", modGlobalVariables.Statics.strCEDatabase);
				boolTemp = false;
				while (!clsLoad.EndOfFile())
				{
					GridInspections.Rows += 1;
					lngRow = GridInspections.Rows - 1;
					GridInspections.TextMatrix(lngRow, CNSTGRIDINSPECTIONCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					GridInspections.TextMatrix(lngRow, CNSTGRIDINSPECTIONCOLINSPECTOR, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectorid"))));
					GridInspections.TextMatrix(lngRow, CNSTGRIDINSPECTIONCOLTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectiontype"))));
					GridInspections.TextMatrix(lngRow, CNSTGRIDINSPECTIONCOLSTATUS, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("inspectionstatus"))));
					if (Conversion.Val(clsLoad.Get_Fields_Int32("inspectionstatus")) == modCEConstants.CNSTINSPECTIONCOMPLETE)
					{
						boolTemp = true;
					}
					if (Information.IsDate(clsLoad.Get_Fields("inspectiondate")))
					{
                        //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                        //if (Convert.ToDateTime(clsLoad.Get_Fields("inspectiondate")).ToOADate() != 0)
                        if (clsLoad.Get_Fields_DateTime("inspectiondate").ToOADate() != 0)
						{
							GridInspections.TextMatrix(lngRow, CNSTGRIDINSPECTIONCOLDATE, Strings.Format(clsLoad.Get_Fields_DateTime("InspectionDate"), "MM/dd/yyyy"));
						}
					}
					clsLoad.MoveNext();
				}
				boolPermitComplete = boolTemp;
				if (boolTemp)
				{
					cmbApprovedDenied.SelectedIndex = cmbApprovedDenied.Items.Count - 1;
					// completed is always the last
					cmbApprovedDenied.Enabled = false;
				}
				else
				{
					if (!boolCantEdit)
					{
						cmbApprovedDenied.Enabled = true;
						if (!boolDontUpdateStatus)
						{
							if (cmbApprovedDenied.SelectedIndex == cmbApprovedDenied.Items.Count - 1)
							{
								cmbApprovedDenied.SelectedIndex = 0;
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridInspections", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadPermit()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int x;
				string strTemp = "";
				ImgComment.Image = null;
				clsLoad.OpenRecordset("select * from permits where ID = " + FCConvert.ToString(lngCurrentPermit), modGlobalVariables.Statics.strCEDatabase);
				ContList.PermitID = lngCurrentPermit;
				ContList.LoadContractors();
				ContList.MoveFirst();
				if (!clsLoad.EndOfFile())
				{
					if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("comment"))) != string.Empty)
					{
						ImgComment.Image = MDIParent.InstancePtr.ImageList2.Images[0];
					}
					if (lngCurrentAccount == 0)
						lngCurrentAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("account"))));
					rtbDescription.Text = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
					GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLVALUE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("estimatedvalue"))), "#,###,###,##0"));
					GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLCOMMRES, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("commrescode"))));
					GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("permittype"))));
					GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLFEE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fee"))), "0.00"));
					GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLCategory, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("permitsubtype"))));
					if (Information.IsDate(clsLoad.Get_Fields("startdate")))
					{
                        //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                        //if (Convert.ToDateTime(clsLoad.Get_Fields("startdate")).ToOADate() != 0)
                        if (clsLoad.Get_Fields_DateTime("startdate").ToOADate() != 0)
						{
							GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSTART, Strings.Format(clsLoad.Get_Fields_DateTime("startdate"), "MM/dd/yyyy"));
						}
					}
					if (Information.IsDate(clsLoad.Get_Fields("completiondate")))
					{
                        //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                        //if (Convert.ToDateTime(clsLoad.Get_Fields("completiondate")).ToOADate() != 0)
                        if (clsLoad.Get_Fields_DateTime("completiondate").ToOADate() != 0)
						{
							GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLEND, Strings.Format(clsLoad.Get_Fields_DateTime("completiondate"), "MM/dd/yyyy"));
						}
					}
					txtYear.Text = FCConvert.ToString(clsLoad.Get_Fields_Int32("permityear"));
					lblPermitNumber.Text = Strings.Right(txtYear.Text, 2) + "-" + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("permitnumber")));
					txtIdentifier.Text = FCConvert.ToString(clsLoad.Get_Fields_String("PERMITidentifier"));
					if (!FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("CERTIFICATEOFOCCrequired")))
					{
						chkCertificateOfOccupancy.CheckState = Wisej.Web.CheckState.Unchecked;
						t2kOccupancyDate.Text = "";
					}
					else
					{
						chkCertificateOfOccupancy.CheckState = Wisej.Web.CheckState.Checked;
						t2kOccupancyDate.Text = "";
						if (Information.IsDate(clsLoad.Get_Fields("certificateofoccdate")))
						{
                            //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                            //if (Convert.ToDateTime(clsLoad.Get_Fields("certificateofoccdate")).ToOADate() != 0)
                            if (clsLoad.Get_Fields_DateTime("certificateofoccdate").ToOADate() != 0)
							{
								t2kOccupancyDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("certificateofoccdate"), "MM/dd/yyyy");
							}
						}
					}
					for (x = 0; x <= cmbApprovedDenied.Items.Count - 1; x++)
					{
						if (cmbApprovedDenied.ItemData(x) == Conversion.Val(clsLoad.Get_Fields_Int32("appdencode")))
						{
							cmbApprovedDenied.SelectedIndex = x;
							break;
						}
					}
					// x
					// For x = 0 To cmbContractor.ListCount - 1
					// If cmbContractor.ItemData(x) = Val(clsLoad.Fields("contractorid")) Then
					// cmbContractor.ListIndex = x
					// Exit For
					// End If
					// Next x
					if (Information.IsDate(clsLoad.Get_Fields("applicationdate")))
					{
                        //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                        //if (Convert.ToDateTime(clsLoad.Get_Fields("applicationdate")).ToOADate() != 0)
                        if (clsLoad.Get_Fields_DateTime("applicationdate").ToOADate() != 0)
						{
							t2kApplicationDate.Text = Strings.Format(clsLoad.Get_Fields_DateTime("applicationdate"), "MM/dd/yyyy");
						}
					}
					lblName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("OriginalOwner"));
					lblMapLot.Text = FCConvert.ToString(clsLoad.Get_Fields_String("originalmaplot"));
					lblFirstName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("OriginalOwnerfirst"));
					lblMiddleName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("originalownermiddle"));
					lblLastName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("originalownerlast"));
					lblDesig.Text = FCConvert.ToString(clsLoad.Get_Fields_String("originalownerdesig"));
					lblStreetNum.Text = FCConvert.ToString(clsLoad.Get_Fields_String("originalstreetnum"));
					lblLocation.Text = FCConvert.ToString(clsLoad.Get_Fields_String("originalstreet"));
					txtFiledBy.Text = FCConvert.ToString(clsLoad.Get_Fields_String("filedby"));
					txtContactName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("contactname"));
					txtEmail.Text = FCConvert.ToString(clsLoad.Get_Fields_String("contactemail"));
					txtUnits.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("units")));
					txtHousingUnits.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("dwellunits")));
					txtBuildings.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("numbuildings")));
					txtOMBCode.Text = FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("ombcode")));
					txtPlan.Text = FCConvert.ToString(clsLoad.Get_Fields_String("plan"));
					// If Val(clsLoad.Fields("contractorid")) <= 0 Then
					// txtContractorName.Text = clsLoad.Fields("permitcontractorname")
					// txtContractorAddr1.Text = clsLoad.Fields("permitcontractoraddress1")
					// txtContractorAddr2.Text = clsLoad.Fields("permitcontractoraddress2")
					// txtContractCity.Text = clsLoad.Fields("permitcontractorcity")
					// txtContractorZip.Text = clsLoad.Fields("permitcontractorzip")
					// txtContractorZip4.Text = clsLoad.Fields("permitcontractorzip4")
					// txtContractorEmail.Text = clsLoad.Fields("permitcontractoremail")
					// End If
					int lngRow;
					clsLoad.OpenRecordset("select * from phonenumbers where phonecode = " + FCConvert.ToString(modCEConstants.CNSTPHONETYPEPERMIT) + " and parentid = " + FCConvert.ToString(lngCurrentPermit), modGlobalVariables.Statics.strCEDatabase);
					GridPhones.Rows = 1;
					while (!clsLoad.EndOfFile())
					{
						GridPhones.Rows += 1;
						lngRow = GridPhones.Rows - 1;
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("PHONEdescription")));
						strTemp = Strings.Right(Strings.StrDup(10, "0") + clsLoad.Get_Fields_String("Phonenumber"), 10);
						strTemp = "(" + Strings.Mid(strTemp, 1, 3) + ")" + Strings.Mid(strTemp, 4, 3) + "-" + Strings.Mid(strTemp, 7, 4);
						GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER, strTemp);
						clsLoad.MoveNext();
					}
					if (!boolCantEdit)
					{
						cmdAddActivity.Enabled = true;
					}
					else
					{
						cmdAddActivity.Enabled = false;
					}
					gridActivity.Enabled = true;
					mnuPrintActivity.Enabled = true;
					mnuPrintActivityWithDetail.Enabled = true;
					alList.Clear();
					alList.LoadList(lngCurrentAccount, modCEConstants.CNSTCODETYPEPERMIT, lngCurrentPermit);
					FillActivityGrid();
					FillGridViolations();
				}
				else
				{
					gridActivity.Enabled = false;
					cmdAddActivity.Enabled = false;
					mnuPrintActivity.Enabled = false;
					mnuPrintActivityWithDetail.Enabled = false;
					alList.Clear();
					FillActivityGrid();
					FillGridViolations();
				}
				LoadGridOpens();
				FillGridInspections(true);
				ShowCurrentContractor();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadPermit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void UpdateCurrentContractor()
		{
			clsPermitContractor tPC;
			tPC = ContList.GetCurrentContractor;
			if (!(tPC == null))
			{
				tPC.ContractorID = cmbContractor.ItemData(cmbContractor.SelectedIndex);
				tPC.Address1 = fecherFoundation.Strings.Trim(txtContractorAddr1.Text);
				tPC.Address2 = fecherFoundation.Strings.Trim(txtContractorAddr2.Text);
				tPC.City = fecherFoundation.Strings.Trim(txtContractCity.Text);
				tPC.ContractorName = fecherFoundation.Strings.Trim(txtContractorName.Text);
				tPC.Email = fecherFoundation.Strings.Trim(txtContractorEmail.Text);
				tPC.Note = fecherFoundation.Strings.Trim(txtContractorNote.Text);
				tPC.State = fecherFoundation.Strings.Trim(txtContractorState.Text);
				tPC.Zip = txtContractorZip.Text;
				tPC.Zip4 = txtContractorZip4.Text;
				clsPhoneList tPList;
				tPList = tPC.PhoneNumberList;
				clsPhoneNumber tPN;
				int lngRow;
				for (lngRow = 1; lngRow <= GridContractorPhone.Rows - 1; lngRow++)
				{
					if (Conversion.Val(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX)) > 0)
					{
						tPN = tPList.Get_GetPhoneNumberByIndex(FCConvert.ToInt16(Conversion.Val(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX))));
						if (!(tPN == null))
						{
							tPN.Description = fecherFoundation.Strings.Trim(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC));
							tPN.PhoneNumber = fecherFoundation.Strings.Trim(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
						}
						else
						{
							tPN = new clsPhoneNumber();
							tPN.ID = 0;
							tPN.Deleted = false;
							tPN.Description = fecherFoundation.Strings.Trim(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC));
							tPN.PhoneNumber = fecherFoundation.Strings.Trim(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
							GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(tPList.InsertPhoneNumber(ref tPN)));
						}
					}
					else
					{
						tPN = new clsPhoneNumber();
						tPN.ID = 0;
						tPN.Deleted = false;
						tPN.Description = fecherFoundation.Strings.Trim(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC));
						tPN.PhoneNumber = fecherFoundation.Strings.Trim(GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
						GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(tPList.InsertPhoneNumber(ref tPN)));
					}
				}
				// lngRow
			}
		}

		private void ShowCurrentContractor()
		{
			clsPermitContractor tPC;
			int lngRow;
			int x;
			// UpdateCurrentContractor
			tPC = ContList.GetCurrentContractor;
			if (!(tPC == null))
			{
				if (tPC.ContractorID > 0)
				{
					for (x = 0; x <= cmbContractor.Items.Count - 1; x++)
					{
						if (cmbContractor.ItemData(x) == tPC.ContractorID)
						{
							cmbContractor.SelectedIndex = x;
							//App.DoEvents();
							break;
						}
					}
					// x
				}
				else
				{
					clsPhoneList PhoneList;
					PhoneList = null;
					boolDontUpdateContractorCmb = true;
					cmbContractor.SelectedIndex = 0;
					//App.DoEvents();
					boolDontUpdateContractorCmb = false;
					txtContractorName.Text = tPC.ContractorName;
					txtContractorName.Visible = true;
					txtContractorAddr1.Text = tPC.Address1;
					txtContractorAddr2.Text = tPC.Address2;
					txtContractCity.Text = tPC.City;
					txtContractorZip.Text = tPC.Zip;
					txtContractorZip4.Text = tPC.Zip4;
					txtContractorState.Text = tPC.State;
					txtContractorEmail.Text = tPC.Email;
					txtContractorNote.Text = tPC.Note;
					PhoneList = tPC.PhoneNumberList;
					GridContractorPhone.Rows = 1;
					if (!(PhoneList == null))
					{
						clsPhoneNumber tPN;
						PhoneList.MoveFirst();
						while (PhoneList.GetCurrentIndex >= 0)
						{
							tPN = PhoneList.GetCurrentPhoneNumber;
							if (!(tPN == null))
							{
								GridContractorPhone.Rows += 1;
								lngRow = GridContractorPhone.Rows - 1;
								GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(tPN.ID));
								GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC, tPN.Description);
								GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER, tPN.FormattedPhoneNumber);
								GridContractorPhone.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLINDEX, FCConvert.ToString(PhoneList.GetCurrentIndex));
							}
							PhoneList.MoveNext();
						}
					}
					else
					{
						GridContractorPhone.Rows = 1;
					}
				}
			}
			else
			{
				txtContractorName.Text = "";
				txtContractorAddr1.Text = "";
				txtContractorAddr2.Text = "";
				txtContractCity.Text = "";
				txtContractorZip.Text = "";
				txtContractorZip4.Text = "";
				txtContractorState.Text = "";
				txtContractorEmail.Text = "";
				txtContractorNote.Text = "";
				cmbContractor.SelectedIndex = 0;
				GridContractorPhone.Rows = 1;
				ContList.MoveFirst();
				if (ContList.GetCurrentIndex < 0)
				{
					ContList.AddContractor("", "", "", "", "", "", "", "", "", 0, 0);
				}
			}
		}

		private bool SavePermit(bool boolOverwriteOwnerInfo = false)
		{
			bool SavePermit = false;
			// VB6 Bad Scope Dim:
			clsDRWrapper rsTemp = new clsDRWrapper();
			bool boolLocked = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				string strTemp = "";
				int lngRow;
				bool boolOK;
				// vbPorter upgrade warning: lngTemp As int	OnWrite(double, int)
				int lngTemp = 0;
				bool boolNew;
				bool boolAddActivity;
				string strSQL = "";
				string strIdentifier = "";
				string strWhere = "";
				string strAnd = "";
				GridEstimate.Row = 0;
				SavePermit = false;
				boolOK = true;
				int intPermYear;
				// vbPorter upgrade warning: intyeartouse As int	OnWriteFCConvert.ToInt32(
				int intyeartouse;
				intyeartouse = FCConvert.ToInt32(Math.Round(Conversion.Val(txtYear.Text)));
				if (intyeartouse == 0)
				{
					intyeartouse = DateTime.Today.Year;
					intyeartouse += modGlobalVariables.Statics.CECustom.AddToYear;
					if (DateTime.Today.Month < modGlobalVariables.Statics.CECustom.StartMonth)
					{
						intyeartouse -= 1;
					}
				}
				intPermYear = intyeartouse;
				if (modGlobalVariables.Statics.CECustom.UseTwoDigitYear)
				{
					intPermYear = intyeartouse - 2000;
				}
				if (!modGlobalVariables.Statics.CECustom.GeneratePermitIdentifiers)
				{
					if (fecherFoundation.Strings.Trim(txtIdentifier.Text) == "")
					{
						MessageBox.Show("You have not provided an identifier and have chosen not to auto-generate identifiers" + "\r\n" + "An identifier must be provided before saving", "Identifier Empty", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SavePermit;
					}
				}
				if (!(modGlobalVariables.Statics.CECustom.PermitIdentifierValidation == FCConvert.ToInt32(clsCustomize.IdentifierValidation.NotUnique)))
				{
					if (!modGlobalVariables.Statics.CECustom.GeneratePermitIdentifiers || lngCurrentPermit > 0)
					{
						// if auto generate then don't warn, just fix it when you save
						strSQL = "select ID from permits where ID <> " + FCConvert.ToString(lngCurrentPermit) + " and permitidentifier = '" + txtIdentifier.Text + "' ";
						if (modGlobalVariables.Statics.CECustom.PermitIdentifierValidation == FCConvert.ToInt32(clsCustomize.IdentifierValidation.TypeUnique))
						{
							strSQL += " and permittype = " + FCConvert.ToString(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE)));
						}
						rsTemp.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
						if (!rsTemp.EndOfFile())
						{
							MessageBox.Show("There is already another permit with this identifier", "Identifier Already Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return SavePermit;
						}
					}
				}
				if (cmbApprovedDenied.SelectedIndex < 0)
				{
					MessageBox.Show("You must select a status before the permit can be saved", "No Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SavePermit;
				}
				if (!CheckGridOpens())
				{
					return SavePermit;
				}
				for (lngRow = GridDeletedPhones.Rows - 1; lngRow >= 0; lngRow--)
				{
					clsSave.Execute("delete from phonenumbers where ID = " + GridDeletedPhones.TextMatrix(lngRow, 0), modGlobalVariables.Statics.strCEDatabase);
					GridDeletedPhones.RemoveItem(lngRow);
				}
				// lngRow
				boolNew = false;
				boolAddActivity = false;
				clsSave.OpenRecordset("select * from permits where ID = " + FCConvert.ToString(lngCurrentPermit), modGlobalVariables.Statics.strCEDatabase);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					if (Conversion.Val(txtYear.Text) < 1980)
					{
						MessageBox.Show("You must enter a valid year for this permit", "Bad Year", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SavePermit;
					}
					if (modMain.AttemptLock(ref strTemp, modCEConstants.CNSTPERMITLOCK))
					{
						// use the lock and close it as quickly as possible
						// don't keep the lock while saving everything
						boolLocked = true;
						if (modGlobalVariables.Statics.CECustom.PermitNumberReset)
						{
							clsSave.OpenRecordset("select max(PERMITnumber) as maxnum from permits where PERMITYEAR = " + FCConvert.ToString(Conversion.Val(txtYear.Text)), modGlobalVariables.Statics.strCEDatabase);
						}
						else
						{
							clsSave.OpenRecordset("select max(PERMITnumber) as maxnum from permits", modGlobalVariables.Statics.strCEDatabase);
						}
						if (!clsSave.EndOfFile())
						{
							lngTemp = FCConvert.ToInt32(Conversion.Val(clsSave.Get_Fields("maxnum")) + 1);
						}
						else
						{
							lngTemp = 1;
						}
						lngPermitNumber = lngTemp;
						clsSave.OpenRecordset("select * from permits where ID = 0", modGlobalVariables.Statics.strCEDatabase);
						clsSave.AddNew();
						clsSave.Set_Fields("Account", lngCurrentAccount);
						clsSave.Set_Fields("PermitNumber", lngPermitNumber);
						lblPermitNumber.Text = Strings.Right(txtYear.Text, 2) + "-" + FCConvert.ToString(lngPermitNumber);
						lblPermitNumber.Visible = true;
						lblYear.Text = "Permit";
						txtYear.Visible = false;
						clsSave.Set_Fields("deleted", false);
						clsSave.Set_Fields("originalowner", fecherFoundation.Strings.Trim(lblName.Text));
						clsSave.Set_Fields("originalmaplot", fecherFoundation.Strings.Trim(lblMapLot.Text));
						clsSave.Set_Fields("originalownerfirst", fecherFoundation.Strings.Trim(lblFirstName.Text));
						clsSave.Set_Fields("originalownermiddle", fecherFoundation.Strings.Trim(lblMiddleName.Text));
						clsSave.Set_Fields("originalownerlast", fecherFoundation.Strings.Trim(lblLastName.Text));
						clsSave.Set_Fields("originalownerdesig", fecherFoundation.Strings.Trim(lblDesig.Text));
						clsSave.Set_Fields("originalstreet", fecherFoundation.Strings.Trim(lblLocation.Text));
						clsSave.Set_Fields("originalstreetnum", fecherFoundation.Strings.Trim(lblStreetNum.Text));
						// If CECustom.GeneratePermitIdentifiers Then
						// If CECustom.PermitIdentifierFormat = IdentifierFormat.NumberDashYear Or CECustom.PermitIdentifierFormat = IdentifierFormat.NumberOnly Then
						// strSQL = "select max(CAST(LEFT(permitidentifier, Patindex('%[^-.0-9]%', permitidentifier + 'x') - 1) AS Float)) as maxid from permits "
						// Else
						// strSQL = "select max(val(mid(permitidentifier & '      ',6))) as maxid from permits "
						// CAST(LEFT(atext, Patindex('%[^-.0-9]%', atext + 'x') - 1) AS Float) AS Val
						// strSQL = "select max(val(substring(permitidentifier & '      ',6,50))) as maxid from permits "
						// strSQL = "select max(CAST(LEFT(substring(permitidentifier + '      ',6,50), Patindex('%[^-.0-9]%', substring(permitidentifier + '      ',6,50) + 'x') - 1) AS Float)) as maxid from permits "
						// End If
						// If CECustom.PermitIdentifierValidation = IdentifierValidation.TypeUnique Then
						// strSQL = strSQL & " where permittype = " & Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE))
						// End If
						// Call rsTemp.OpenRecordset(strSQL, strCEDatabase)
						// Select Case CECustom.PermitIdentifierFormat
						// Case IdentifierFormat.NumberDashYear
						// strIdentifier = Val(rsTemp.Fields("maxid")) + 1 & "-" & Year(Date)
						// Case IdentifierFormat.NumberOnly
						// strIdentifier = Val(rsTemp.Fields("maxid")) + 1
						// Case IdentifierFormat.yeardashnumber
						// strIdentifier = Year(Date) & "-" & Val(rsTemp.Fields("maxid")) + 1
						// End Select
						// txtIdentifier.Text = strIdentifier
						// End If
						if (modGlobalVariables.Statics.CECustom.GeneratePermitIdentifiers)
						{
							strAnd = "";
							if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberDashYear) || (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberOnly) && modGlobalVariables.Statics.CECustom.PermitNumberReset))
							{
								// strSQL = "select max(val(permitidentifier & '')) as maxid from permits "
								strSQL = "select max(CAST(LEFT(permitidentifier, Patindex('%[^-.0-9]%', permitidentifier + 'x') - 1) AS Float)) as maxid from permits ";
							}
							else if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberOnly))
							{
								// strSQL = "select max(val(permitidentifier & '')) as maxid from permits "
								strSQL = "select max(CAST(LEFT(permitidentifier, Patindex('%[^-.0-9]%', permitidentifier + 'x') - 1) AS Float)) as maxid from permits ";
							}
							else
							{
								if (!modGlobalVariables.Statics.CECustom.UseTwoDigitYear)
								{
									// strSQL = "select max(val(mid(permitidentifier & '      ',6))) as maxid from permits "
									strSQL = "select max(CAST(LEFT(substring(permitidentifier + '      ',6,50), Patindex('%[^-.0-9]%', substring(permitidentifier + '      ',6,50) + 'x') - 1) AS Float)) as maxid from permits ";
								}
								else
								{
									// strSQL = "select max(val(mid(permitidentifier & '      ',4))) as maxid from permits "
									strSQL = "select max(CAST(LEFT(substring(permitidentifier + '      ',4,50), Patindex('%[^-.0-9]%', substring(permitidentifier + '      ',4,50) + 'x') - 1) AS Float)) as maxid from permits ";
								}
							}
							if (modGlobalVariables.Statics.CECustom.PermitIdentifierValidation == FCConvert.ToInt32(clsCustomize.IdentifierValidation.TypeUnique))
							{
								// strSQL = strSQL & " where "
								strWhere = " permittype = " + FCConvert.ToString(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE))) + " ";
								strAnd = " and ";
							}
							if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat != FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberOnly))
							{
								strWhere += strAnd + "  ";
								if (modGlobalVariables.Statics.CECustom.UseTwoDigitYear)
								{
									if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.yeardashnumber))
									{
										// strWhere = strWhere & " mid(permitidentifier & '  ',1,2) = '" & intPermYear & "' "
										strWhere += " substring(permitidentifier + '  ',1,2) = '" + FCConvert.ToString(intPermYear) + "' ";
									}
									else
									{
										// strWhere = strWhere & " right('  ' & permitidentifier,2) = '" & intPermYear & "' "
										strWhere += " right('  ' + permitidentifier,2) = '" + FCConvert.ToString(intPermYear) + "' ";
									}
								}
								else
								{
									if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.yeardashnumber))
									{
										// strWhere = strWhere & " mid(permitidentifier & '  ',1,4) = '" & intPermYear & "' "
										strWhere += " substring(permitidentifier + '  ',1,4) = '" + FCConvert.ToString(intPermYear) + "' ";
									}
									else
									{
										// strWhere = strWhere & " right('    ' & permitidentifier,4) = '" & intPermYear & "' "
										strWhere += " right('    ' + permitidentifier,4) = '" + FCConvert.ToString(intPermYear) + "' ";
									}
								}
								strAnd = " and ";
							}
							if (strWhere != "")
							{
								strSQL += " where " + strWhere;
							}
							rsTemp.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
							// vbPorter upgrade warning: strNumberToUse As string	OnWrite(double, string)
							string strNumberToUse = "";
							strNumberToUse = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("maxid")) + 1);
							if (modGlobalVariables.Statics.CECustom.PermitFormatPlaces > 0)
							{
								strNumberToUse = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("maxid")))) + 1, Strings.StrDup(modGlobalVariables.Statics.CECustom.PermitFormatPlaces, "0"));
							}
							if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberDashYear))
							{
								strIdentifier = strNumberToUse + "-" + FCConvert.ToString(intPermYear);
							}
							else if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.NumberOnly))
							{
								strIdentifier = strNumberToUse;
							}
							else if (modGlobalVariables.Statics.CECustom.PermitIdentifierFormat == FCConvert.ToInt32(clsCustomize.IdentifierFormat.yeardashnumber))
							{
								strIdentifier = FCConvert.ToString(intPermYear) + "-" + strNumberToUse;
							}
							txtIdentifier.Text = strIdentifier;
						}
						clsSave.Update();
						lngCurrentPermit = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
						modMain.ReleaseLock(modCEConstants.CNSTPERMITLOCK);
						boolLocked = false;
						boolNew = true;
						boolAddActivity = true;
					}
					else
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Couldn't obtain lock to assign new permit number" + "\r\n" + "Locked out by " + strTemp, "Locked Out", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SavePermit;
					}
				}
				if (boolAddActivity)
				{
					clsActivity tAct = new clsActivity();
					tAct.Account = lngCurrentAccount;
					tAct.ActivityDate = FCConvert.ToDateTime(Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					tAct.ActivityType = "Initial";
					tAct.Description = "Created Permit";
					tAct.Detail = "";
					tAct.ParentID = lngCurrentPermit;
					tAct.ParentType = modCEConstants.CNSTCODETYPEPERMIT;
					tAct.ReferenceID = 0;
					tAct.ReferenceType = modCEConstants.CNSTACTIVITYTYPEPERMITCREATION;
					tAct.SaveActivity();
				}
				clsSave.OpenRecordset("select * from permits where ID = " + FCConvert.ToString(lngCurrentPermit), modGlobalVariables.Statics.strCEDatabase);
				clsSave.Edit();
				if (boolOverwriteOwnerInfo)
				{
					clsSave.Set_Fields("originalowner", fecherFoundation.Strings.Trim(lblName.Text));
					clsSave.Set_Fields("originalmaplot", fecherFoundation.Strings.Trim(lblMapLot.Text));
					clsSave.Set_Fields("originalownerfirst", fecherFoundation.Strings.Trim(lblFirstName.Text));
					clsSave.Set_Fields("originalownermiddle", fecherFoundation.Strings.Trim(lblMiddleName.Text));
					clsSave.Set_Fields("originalownerlast", fecherFoundation.Strings.Trim(lblLastName.Text));
					clsSave.Set_Fields("originalownerdesig", fecherFoundation.Strings.Trim(lblDesig.Text));
					clsSave.Set_Fields("originalstreet", fecherFoundation.Strings.Trim(lblLocation.Text));
					clsSave.Set_Fields("originalstreetnum", fecherFoundation.Strings.Trim(lblStreetNum.Text));
				}
				clsSave.Set_Fields("originalstreet", fecherFoundation.Strings.Trim(lblLocation.Text));
				clsSave.Set_Fields("originalstreetnum", fecherFoundation.Strings.Trim(lblStreetNum.Text));
				clsSave.Set_Fields("permitidentifier", fecherFoundation.Strings.Trim(txtIdentifier.Text));
				clsSave.Set_Fields("Account", lngCurrentAccount);
				clsSave.Set_Fields("filedby", fecherFoundation.Strings.Trim(txtFiledBy.Text));
				clsSave.Set_Fields("contactname", txtContactName.Text);
				clsSave.Set_Fields("contactemail", txtEmail.Text);
				clsSave.Set_Fields("Plan", fecherFoundation.Strings.Trim(txtPlan.Text));
				clsSave.Set_Fields("units", FCConvert.ToString(Conversion.Val(txtUnits.Text)));
				clsSave.Set_Fields("dwellunits", FCConvert.ToString(Conversion.Val(txtHousingUnits.Text)));
				clsSave.Set_Fields("NumBuildings", FCConvert.ToString(Conversion.Val(txtBuildings.Text)));
				clsSave.Set_Fields("ombcode", FCConvert.ToString(Conversion.Val(txtOMBCode.Text)));
				clsSave.Set_Fields("permityear", FCConvert.ToString(Conversion.Val(txtYear.Text)));
				clsSave.Set_Fields("description", rtbDescription.Text);
				clsSave.Set_Fields("estimatedvalue", FCConvert.ToString(Conversion.Val(Strings.Replace(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare))));
				clsSave.Set_Fields("commrescode", FCConvert.ToString(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLCOMMRES))));
				clsSave.Set_Fields("permittype", FCConvert.ToString(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE))));
				clsSave.Set_Fields("fee", FCConvert.ToString(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLFEE))));
				clsSave.Set_Fields("permitsubtype", FCConvert.ToString(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLCategory))));
				if (Information.IsDate(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSTART)))
				{
					clsSave.Set_Fields("StartDate", Strings.Format(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLSTART), "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("startdate", 0);
				}
				if (Information.IsDate(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLEND)))
				{
					clsSave.Set_Fields("completiondate", Strings.Format(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLEND), "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("completiondate", 0);
				}
				if (Information.IsDate(t2kApplicationDate.Text))
				{
					clsSave.Set_Fields("applicationdate", Strings.Format(t2kApplicationDate.Text, "MM/dd/yyyy"));
				}
				else
				{
					clsSave.Set_Fields("applicationdate", 0);
				}
				// If cmbContractor.ListIndex >= 0 Then
				// clsSave.Fields("contractorid") = Val(cmbContractor.ItemData(cmbContractor.ListIndex))
				// Else
				// clsSave.Fields("contractorid") = 0
				// End If
				if (cmbApprovedDenied.SelectedIndex >= 0)
				{
					clsSave.Set_Fields("appdencode", cmbApprovedDenied.ItemData(cmbApprovedDenied.SelectedIndex));
				}
				else
				{
					clsSave.Set_Fields("appdencode", 0);
				}
				if (boolPermitComplete)
				{
					clsSave.Set_Fields("appdencode", modCEConstants.CNSTPERMITCOMPLETE);
					// don't let them override it if the inspections are all done
				}
				if (chkCertificateOfOccupancy.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("certificateofoccrequired", true);
					if (Information.IsDate(t2kOccupancyDate.Text))
					{
						clsSave.Set_Fields("certificateofoccdate", Strings.Format(t2kOccupancyDate.Text, "MM/dd/yyyy"));
					}
					else
					{
						clsSave.Set_Fields("certificateofoccdate", 0);
					}
				}
				else
				{
					clsSave.Set_Fields("certificateofoccdate", 0);
					clsSave.Set_Fields("certificateofoccRequired", false);
				}
				// clsSave.Fields("permitcontractorname") = Trim(txtContractorName.Text)
				// clsSave.Fields("permitcontractoraddress1") = Trim(txtContractorAddr1.Text)
				// clsSave.Fields("permitcontractoraddress2") = Trim(txtContractorAddr2.Text)
				// clsSave.Fields("permitcontractorcity") = Trim(txtContractCity.Text)
				// clsSave.Fields("permitcontractorstate") = Trim(txtContractorState.Text)
				// clsSave.Fields("permitcontractorzip") = Trim(txtContractorZip.Text)
				// clsSave.Fields("permitcontractorzip4") = Trim(txtContractorZip4.Text)
				// clsSave.Fields("permitcontractoremail") = Trim(txtContractorEmail.Text)
				clsSave.Update();
				SaveGridChecklist();
				ContList.PermitID = lngCurrentPermit;
				for (lngRow = 1; lngRow <= GridPhones.Rows - 1; lngRow++)
				{
					strTemp = fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLNUMBER));
					strTemp = Strings.Replace(strTemp, "-", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, "(", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ")", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Right(Strings.StrDup(10, "0") + strTemp, 10);
					clsSave.OpenRecordset("select * from PHONENUMBERS where ID = " + FCConvert.ToString(Conversion.Val(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID))), modGlobalVariables.Statics.strCEDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
					}
					clsSave.Set_Fields("phonenumber", strTemp);
					clsSave.Set_Fields("phonecode", modCEConstants.CNSTPHONETYPEPERMIT);
					clsSave.Set_Fields("parentid", lngCurrentPermit);
					clsSave.Set_Fields("phonedescription", fecherFoundation.Strings.Trim(GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLDESC)));
					clsSave.Update();
					GridPhones.TextMatrix(lngRow, modCEConstants.CNSTGRIDPHONESCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
				}
				// lngRow
				// If cmbContractor.ListIndex = 0 Then
				// other
				// For lngRow = 1 To GridContractorPhone.Rows - 1
				// strTemp = Trim(GridContractorPhone.TextMatrix(lngRow, CNSTGRIDPHONESCOLNUMBER))
				// strTemp = Replace(strTemp, "-", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, "(", "", , , vbTextCompare)
				// strTemp = Replace(strTemp, ")", "", , , vbTextCompare)
				// strTemp = Right(String(10, "0") & strTemp, 10)
				// Call clsSave.OpenRecordset("select * from PHONENUMBERS where ID = " & Val(GridContractorPhone.TextMatrix(lngRow, CNSTGRIDPHONESCOLAUTOID)), strCEDatabase)
				// If Not clsSave.EndOfFile Then
				// clsSave.Edit
				// Else
				// clsSave.AddNew
				// GridContractorPhone.TextMatrix(lngRow, CNSTGRIDPHONESCOLAUTOID) = clsSave.Fields("ID")
				// End If
				// clsSave.Fields("phonenumber") = strTemp
				// clsSave.Fields("phonecode") = CNSTPHONETYPEPERMITCONTRACTOR
				// clsSave.Fields("parentid") = lngCurrentPermit
				// clsSave.Fields("phonedescription") = Trim(GridContractorPhone.TextMatrix(lngRow, CNSTGRIDPHONESCOLDESC))
				// clsSave.Update
				// Next lngRow
				// End If
				// update current contractor then save all
				UpdateCurrentContractor();
				ContList.SaveContractors();
				boolOK = boolOK && SaveGridOpens();
				if (boolNew)
				{
					// create a new inspection
					DateTime dtTemp = DateTime.FromOADate(0);
					bool boolChose = false;
					if (boolUseInspections && modGlobalVariables.Statics.CECustom.PromptInspectionSchedule)
					{
						if (MessageBox.Show("Do you want to assign an inspection date for the first inspection?", "Schedule Inspection?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							boolChose = false;
							while (!boolChose)
							{
								dtTemp = fecherFoundation.DateAndTime.DateAdd("d", 1, DateTime.Today);
								object temp = dtTemp;
								if (frmInput.InstancePtr.Init(ref temp, "Inspection Date", "", 1440+200, false, modGlobalConstants.InputDTypes.idtDate))
								{
									dtTemp = Convert.ToDateTime(temp);
									if (dtTemp.Year > 2004)
									{
										// valid date
										if (fecherFoundation.DateAndTime.DateDiff("d", DateTime.Today, dtTemp) >= 0)
										{
											// create an inspection
											boolChose = true;
										}
										else
										{
											// bad date
											MessageBox.Show("The inspection cannot be before the current date", "Bad Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											return SavePermit;
										}
									}
								}
								else
								{
									break;
								}
							}
						}
						strTemp = "insert into inspections (INSPEctorid,inspectiondate,inspectiontype,inspectionstatus,permitid) values (";
						if (boolChose)
						{
							strTemp += "0,'" + FCConvert.ToString(dtTemp) + "'," + FCConvert.ToString(modCEConstants.CNSTINSPECTIONTYPEUNKOWN) + "," + FCConvert.ToString(modCEConstants.CNSTINSPECTIONPENDING) + "," + FCConvert.ToString(lngCurrentPermit) + ")";
							clsSave.Execute(strTemp, modGlobalVariables.Statics.strCEDatabase);
							if (modGlobalVariables.Statics.CECustom.AutoAddInspectionsToCalendar)
							{
								strTemp = fecherFoundation.Strings.Trim(lblStreetNum.Text);
								if (strTemp != "" && strTemp != "0")
								{
									strTemp += " " + fecherFoundation.Strings.Trim(lblLocation.Text);
								}
								else
								{
									strTemp = fecherFoundation.Strings.Trim(lblLocation.Text);
								}
								modMain.AddInspectionToCalendar(0, "", strTemp, GridEstimate.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, 1, CNSTGRIDESTIMATECOLTYPE) + " Inspection", fecherFoundation.DateAndTime.DateAdd("h", 8, dtTemp));
							}
						}
						else
						{
							// strTemp = strTemp & "0,0," & CNSTINSPECTIONTYPEUNKOWN & "," & CNSTINSPECTIONPENDING & "," & lngCurrentPermit & ")"
						}
						// Call clsSave.Execute(strTemp, strCEDatabase)
						FillGridInspections(true);
					}
				}
				SavePermit = boolOK;
				if (!boolCantEdit)
				{
					cmdAddActivity.Enabled = true;
					gridActivity.Enabled = true;
					mnuPrintActivity.Enabled = true;
					mnuPrintActivityWithDetail.Enabled = true;
				}
				if (boolOK)
				{
					MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return SavePermit;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (boolLocked)
				{
					modMain.ReleaseLock(modCEConstants.CNSTPERMITLOCK);
					boolLocked = false;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SavePermit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SavePermit;
		}

		private void mnuMove_Click(object sender, System.EventArgs e)
		{
			GridPhones.Row = 0;
			GridOpens.Row = 0;
			GridContractorPhone.Row = 0;
			if (MessageBox.Show("This will remove this permit and assign it to another account." + "\r\n" + "Are you sure you want to continue?", "Move Permit?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
			{
				int lngAcct = 0;
				lngAcct = frmSearchAccounts.InstancePtr.Init("Choose Abutter", false, false, lngCurrentAccount);
				if (lngAcct > 0)
				{
					lngCurrentAccount = lngAcct;
					RefillAccountInformation();
					SavePermit(true);
				}
			}
		}

		private void mnuPrintActivity_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (fecherFoundation.Strings.Trim(txtIdentifier.Text) != "")
			{
				strTemp = txtIdentifier.Text;
			}
			else
			{
				strTemp = lblPermitNumber.Text;
			}
			strTemp = "Permit " + strTemp;
			rptActivity.InstancePtr.Init(lngCurrentPermit, modCEConstants.CNSTCODETYPEPERMIT, false, strTemp, 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void mnuPrintActivityWithDetail_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (fecherFoundation.Strings.Trim(txtIdentifier.Text) != "")
			{
				strTemp = txtIdentifier.Text;
			}
			else
			{
				strTemp = lblPermitNumber.Text;
			}
			strTemp = "Permit " + strTemp;
			rptActivity.InstancePtr.Init(lngCurrentPermit, modCEConstants.CNSTCODETYPEPERMIT, true, strTemp, 0, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void mnuPrintCustomForm_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmChooseCustomBillType.InstancePtr.Init("CE", modGlobalVariables.Statics.strCEDatabase);
			if (lngID <= 0)
			{
				return;
			}
			clsSQLStatement SQLStatement;
			clsCustomBill clsbill = new clsCustomBill();
			string strTemp;
			clsCustomBillUtilities tHelp = new clsCustomBillUtilities();
			// Dim tSQLStatement As New clsSQLStatement
			// Dim tSQL As New clsReportSQLCreator
			// Dim tList As New clsReportParameters
			clsbill.DBFile = "TWCE0000.vb1";
			clsbill.DotMatrixFormat = false;
			clsbill.FormatID = lngID;
			strTemp = tHelp.GetTableListFromCustomForm(ref lngID);
			// strTemp = tSQL.SQLStatement(tSQLStatement, strTemp)
			// tSQLStatement.WhereStatement = "Where permit.ID = " & lngCurrentPermit
			SQLStatement = GetPermitSQL(ref strTemp);
			// Call tList.AddParameter("Permits", lngCode, True, lngCurrentPermit, strFrom, lngAutoID, CNSTVALUETYPELONG)
			clsbill.SQL = SQLStatement.SelectStatement + " " + SQLStatement.WhereStatement + " " + SQLStatement.OrderByStatement;
			if (fecherFoundation.Strings.Trim(clsbill.SQL) == "" || fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(clsbill.SQL)) == "from")
			{
				clsbill.SQL = "select * from cemaster";
			}
			clsbill.ReportTitle = tHelp.GetTitleFromCustomForm(ref lngID);
			rptCustomBill.InstancePtr.Init(clsbill, true, false, false, false, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void ResizeGridCheckList()
		{
			int lngWidth;
			lngWidth = GridChecklist.WidthOriginal;
			GridChecklist.ColWidth(CNSTGRIDCHECKLISTCOLHEADER, FCConvert.ToInt32(0.33 * lngWidth));
			GridChecklist.ColWidth(CNSTGRIDCHECKLISTCOLCOMPLETED, FCConvert.ToInt32(0.06 * lngWidth));
			GridChecklist.ColWidth(CNSTGRIDCHECKLISTCOLDATECOMPLETED, FCConvert.ToInt32(0.13 * lngWidth));
		}

		private void mnuPrintPermit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngID = 0;
			clsSQLStatement SQLStatement;
			int lngPermitType = 0;
			lngPermitType = FCConvert.ToInt32(Math.Round(Conversion.Val(GridEstimate.TextMatrix(1, CNSTGRIDESTIMATECOLTYPE))));
			if (lngPermitType <= 0)
			{
				MessageBox.Show("You must set a permit type before printing", "Invalid Permit Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (lngCurrentPermit <= 0)
			{
				MessageBox.Show("You must save the permit before printing it", "Permit Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			rsLoad.OpenRecordset("select * from systemcodes where  CODETYPE = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " and ID = " + FCConvert.ToString(lngPermitType) + " and documentid > 0", modGlobalVariables.Statics.strCEDatabase);
			if (!rsLoad.EndOfFile())
			{
				lngID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("documentid"));
			}
			else
			{
				MessageBox.Show("No document type was found for this permit type", "No document type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			clsCustomBill clsbill = new clsCustomBill();
			string strTemp;
			clsCustomBillUtilities tHelp = new clsCustomBillUtilities();
			// Dim tSQLStatement As New clsSQLStatement
			// Dim tSQL As New clsReportSQLCreator
			// Dim tList As New clsReportParameters
			clsbill.DBFile = "TWCE0000.vb1";
			clsbill.DotMatrixFormat = false;
			clsbill.FormatID = lngID;
			strTemp = tHelp.GetTableListFromCustomForm(ref lngID);
			// strTemp = tSQL.SQLStatement(tSQLStatement, strTemp)
			// tSQLStatement.WhereStatement = "Where permit.ID = " & lngCurrentPermit
			SQLStatement = GetPermitSQL(ref strTemp);
			// Call tList.AddParameter("Permits", lngCode, True, lngCurrentPermit, strFrom, lngAutoID, CNSTVALUETYPELONG)
			clsbill.SQL = SQLStatement.SelectStatement + " " + SQLStatement.WhereStatement + " " + SQLStatement.OrderByStatement;
			if (fecherFoundation.Strings.Trim(clsbill.SQL) == "" || fecherFoundation.Strings.Trim(fecherFoundation.Strings.LCase(clsbill.SQL)) == "from")
			{
				clsbill.SQL = "select * from cemaster";
			}
			clsbill.ReportTitle = tHelp.GetTitleFromCustomForm(ref lngID);
			rptCustomBill.InstancePtr.Init(clsbill, true, false, false, false, 0, FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private clsSQLStatement GetPermitSQL(ref string strDefaultTables)
		{
			clsSQLStatement GetPermitSQL = null;
			clsReportSQLCreator tSQL = new clsReportSQLCreator();
			clsReportParameters tList = new clsReportParameters();
			clsReportOrderClause tOrdList = new clsReportOrderClause();
			bool boolTemp;
			int lngCode;
			int lngAutoID;
			int lngSetupRow;
			int lngRow;
			clsSQLStatement tSQLStatement = new clsSQLStatement();
			string strTo = "";
			string strFrom = "";
			// For lngRow = 1 To GridParameters.Rows - 1
			// If Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLNOT)) = 1 Then
			// boolTemp = False
			// Else
			// boolTemp = True
			// End If
			// lngSetupRow = Val(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLDESC))
			// Select Case Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE))
			// Case CNSTCODETYPECONTRACTOR
			// lngAutoID = Val(GridContractorSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))
			// Case CNSTCODETYPEINSPECTION
			// lngAutoID = Val(GridInspectionSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))
			// Case CNSTCODETYPEPERMIT
			// lngAutoID = Val(GridPermitSetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))
			// Case CNSTCODETYPEPROPERTY
			// lngAutoID = Val(GridPropertySetup.TextMatrix(lngSetupRow, CNSTGRIDSETUPCOLAUTOID))
			// End Select
			// If lngAutoID = 0 Then
			// lngCode = lngSetupRow
			// Else
			// lngCode = 0
			// End If
			// strTo = ""
			// strFrom = ""
			// If Not IsEmpty(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)) Then
			// If Not GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE) = "" Then
			// strTo = GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)
			// Else
			// strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)
			// End If
			// Else
			// strTo = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTOVALUE)
			// End If
			// If Not IsEmpty(GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)) Then
			// If Not GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE) = "" Then
			// strFrom = GridParameters.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)
			// Else
			// strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)
			// End If
			// Else
			// strFrom = GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLFROMVALUE)
			// End If
			// 
			// Call tList.AddParameter(Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLTABLE)), lngCode, boolTemp, strTo, strFrom, lngAutoID, Val(GridParameters.TextMatrix(lngRow, CNSTGRIDPARAMETERSCOLVALUEFORMAT)))
			// Next lngRow
			// Call tList.AddParameter(CNSTCODETYPEPROPERTY, CNSTGRIDPROPERTYROWDELETED, False, "true", "true", 0)
			tSQL.ListOfParameters = tList;
			// now figure order by clause
			// For lngRow = 1 To GridOrderBy.Rows - 1
			// If GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLUSE) Then
			// Call tOrdList.AddOrder(0, Val(GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDIRECTION)) = 1, GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLDESC), GridOrderBy.TextMatrix(lngRow, CNSTGRIDORDERBYCOLFIELDNAME), lngRow)
			// End If
			// Next lngRow
			tSQL.ListOfOrders = tOrdList;
			string strTemp;
			strTemp = tSQL.Get_SQLStatement(tSQLStatement, strDefaultTables);
			tSQLStatement.WhereStatement = "Where permits.ID = " + FCConvert.ToString(lngCurrentPermit);
			GetPermitSQL = tSQLStatement;
			return GetPermitSQL;
		}

		private void mnuPrintViolations_Click(object sender, System.EventArgs e)
		{
			rptViolations.InstancePtr.Init(lngCurrentAccount, lngCurrentPermit, 0, this.Modal);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			GridPhones.Row = 0;
			GridOpens.Row = 0;
			GridContractorPhone.Row = 0;
			SavePermit();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			GridPhones.Row = 0;
			GridOpens.Row = 0;
			GridContractorPhone.Row = 0;
			if (SavePermit())
			{
				mnuExit_Click();
			}
		}

		private void ResizeGridOpens()
		{
			int GridWidth = 0;
			GridWidth = GridOpens.WidthOriginal;
            //FC:FINAL:BSE #3713 increase column size
            GridOpens.ColWidth(CNSTGRIDOPENSCOLDESC, FCConvert.ToInt32(0.35 * GridWidth));
            GridOpens.ColWidth(CNSTGRIDOPENSCOLVALUE, FCConvert.ToInt32(0.3 * GridWidth));
			// .ColWidth(CNSTGRIDOPENSCOLVALUE) = 0.35 * GridWidth
		}

		private void SetupGridOpens(int lngSubType)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsCodes = new clsDRWrapper();
			string strTemp = "";
			int lngRow;
			GridOpens.Cols = 7;
			GridOpens.Rows = 1;
			GridOpens.ColHidden(CNSTGRIDOPENSCOLAUTOID, true);
			// .ColHidden(CNSTGRIDOPENSCOLUSEVALUE) = True
			// .ColHidden(CNSTGRIDOPENSCOLUSEDATE) = True
			GridOpens.ColHidden(CNSTGRIDOPENSCOLVALUEFORMAT, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMANDATORY, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMAX, true);
			GridOpens.ColHidden(CNSTGRIDOPENSCOLMIN, true);
			GridOpens.ColDataType(CNSTGRIDOPENSCOLMANDATORY, FCGrid.DataTypeSettings.flexDTBoolean);
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLDESC, "Item");
			GridOpens.TextMatrix(0, CNSTGRIDOPENSCOLVALUE, "Value");
			// .TextMatrix(0, CNSTGRIDOPENSCOLDATE) = "Date"
			//GridOpens.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLVALUE, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// .ColAlignment(CNSTGRIDOPENSCOLDATE) = flexAlignLeftCenter
			// .Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDOPENSCOLDATE) = flexAlignCenterCenter
			clsLoad.OpenRecordset("select * FROM usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " AND SUBTYPE = " + FCConvert.ToString(lngSubType) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				GridOpens.Rows += 1;
				lngRow = GridOpens.Rows - 1;
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(false));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("MINVALUE"))));
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("maxvalue"))));
				// If Not clsLoad.Fields("usevalue") Then
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLVALUE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = False
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) = True
				GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("valueformat"))));
				if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("valueformat")) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					// get the list of drop downs
					strTemp = "";
					clsCodes.OpenRecordset("select * from codedescriptions where fieldid = " + clsLoad.Get_Fields_Int32("ID") + " and type = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " order by code", modGlobalVariables.Statics.strCEDatabase);
					while (!clsCodes.EndOfFile())
					{
						strTemp += "#" + clsCodes.Get_Fields("code") + ";" + clsCodes.Get_Fields_String("description") + "|";
						clsCodes.MoveNext();
					}
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					}
					GridOpens.RowData(lngRow, strTemp);
				}
				else if (FCConvert.ToInt32(clsLoad.Get_Fields_Int32("VALUEFORMAT")) == modCEConstants.CNSTVALUETYPEBOOLEAN)
				{
					GridOpens.RowData(lngRow, "Yes|No");
				}
				// End If
				// If Not clsLoad.Fields("usedate") Then
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = False
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, CNSTGRIDOPENSCOLDATE) = TRIOCOLORGRAYEDOUTTEXTBOX
				// Else
				// .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) = True
				// End If
				if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("mandatory")))
				{
					GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY, FCConvert.ToString(true));
				}
				clsLoad.MoveNext();
			}
		}

		private void LoadGridOpens()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				int lngRow;
				// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
				int x;
				string[] strAry = null;
				string[] strAry2 = null;
				if (lngCurrentPermit > 0)
				{
					clsLoad.OpenRecordset("select * from USERcodevalues where account = " + FCConvert.ToString(lngCurrentPermit) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					if (!clsLoad.EndOfFile())
					{
						for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
						{
							if (clsLoad.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
								GridOpens.ComboList = "";
								if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
								{
									// not allowing type date at this time
									if (Information.IsDate(clsLoad.Get_Fields("datevalue")))
									{
                                        //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                                        //if (Convert.ToDateTime(clsLoad.Get_Fields("datevalue")).ToOADate() != 0)
                                        if (clsLoad.Get_Fields_DateTime("datevalue").ToOADate() != 0)
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(clsLoad.Get_Fields_DateTime("datevalue"), "MM/dd/yyyy"));
										}
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
								{
									if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("booleanvalue")))
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "Yes");
									}
									else
									{
										GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, "No");
									}
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
								{
									GridOpens.ComboList = FCConvert.ToString(GridOpens.RowData(lngRow));
									// .TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) = Val(clsLoad.Fields("dropdownvalue"))
									GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE, Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")));
									strAry = Strings.Split(Strings.Replace(FCConvert.ToString(GridOpens.RowData(lngRow)), "#", "", 1, -1, CompareConstants.vbTextCompare), "|", -1, CompareConstants.vbTextCompare);
									for (x = 0; x <= (Information.UBound(strAry, 1)); x++)
									{
										strAry2 = Strings.Split(strAry[x], ";", -1, CompareConstants.vbTextCompare);
										if (Conversion.Val(clsLoad.Get_Fields_Int32("dropdownvalue")) == Conversion.Val(strAry2[0]))
										{
											GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, strAry2[1]);
										}
									}
									// x
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, Strings.Format(FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("numericvalue"))), "#,###,###,##0"));
								}
								else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
								{
									GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE, FCConvert.ToString(clsLoad.Get_Fields_String("textvalue")));
								}
								// End If
								// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
								// If IsDate(clsLoad.Fields("DateValue")) Then
								// If clsLoad.Fields("datevalue") <> 0 Then
								// .TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE) = Format(clsLoad.Fields("datevalue"), "MM/dd/yyyy")
								// End If
								// End If
								// End If
							}
						}
						// lngRow
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckGridOpens()
		{
			bool CheckGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				int lngRow;
				string strTemp = "";
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				int lngTemp = 0;
				for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
				{
					// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
					if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
					{
						if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
						{
							MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return CheckGridOpens;
						}
					}
					dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
					dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
					if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
					{
						strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
						dblTemp = Conversion.Val(strTemp);
						if (dblTemp != 0)
						{
							if (dblMin != 0 && dblMin > dblTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
							if (dblMax != 0 && dblMax < dblTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
						}
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (dblTemp == 0)
							{
								if (dblMin > dblTemp || dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckGridOpens;
								}
							}
						}
					}
					else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
					{
						strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
						if (lngTemp != 0)
						{
							if (dblMin != 0 && dblMin > lngTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
							if (dblMax != 0 && dblMax < lngTemp)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return CheckGridOpens;
							}
						}
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (lngTemp == 0)
							{
								if (dblMin > lngTemp || dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return CheckGridOpens;
								}
							}
						}
					}
				}
				// lngRow
				CheckGridOpens = true;
				return CheckGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return CheckGridOpens;
		}

		private bool SaveGridOpens()
		{
			bool SaveGridOpens = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngRow;
				bool boolMatch = false;
				string strTemp = "";
				int lngTemp = 0;
				double dblMin = 0;
				double dblMax = 0;
				double dblTemp = 0;
				// shouldn't be able to get to this point without a permit number
				SaveGridOpens = false;
				if (lngCurrentPermit > 0)
				{
					clsSave.OpenRecordset("select * from usercodevalues where account = " + FCConvert.ToString(lngCurrentPermit) + " and codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " order by fieldid", modGlobalVariables.Statics.strCEDatabase);
					for (lngRow = 1; lngRow <= GridOpens.Rows - 1; lngRow++)
					{
						boolMatch = false;
						if (!clsSave.EndOfFile())
						{
							if (clsSave.FindFirstRecord("fieldid", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID)))
							{
								boolMatch = true;
							}
						}
						if (boolMatch)
						{
							clsSave.Edit();
						}
						else
						{
							clsSave.AddNew();
						}
						clsSave.Set_Fields("ACCOUNT", lngCurrentPermit);
						clsSave.Set_Fields("codetype", modCEConstants.CNSTCODETYPEPERMIT);
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLAUTOID))));
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEVALUE) Then
						if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == string.Empty)
							{
								MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter data in this field to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return SaveGridOpens;
							}
						}
						dblMin = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMIN));
						dblMax = Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMAX));
						if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
						{
							// not allowing type date at this time
							if (Information.IsDate(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE)))
							{
								clsSave.Set_Fields("datevalue", Strings.Format(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), "MM/dd/yyyy"));
							}
							else
							{
								clsSave.Set_Fields("datevalue", 0);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEBOOLEAN)
						{
							if (GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE) == "Yes")
							{
								clsSave.Set_Fields("booleanvalue", true);
							}
							else
							{
								clsSave.Set_Fields("booleanvalue", false);
							}
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							dblTemp = Conversion.Val(strTemp);
							if (dblTemp != 0)
							{
								if (dblMin != 0 && dblMin > dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < dblTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (dblTemp == 0)
								{
									if (dblMin > dblTemp || dblMax < dblTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", dblTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
						{
							clsSave.Set_Fields("dropdownvalue", FCConvert.ToString(Conversion.Val(GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, lngRow, CNSTGRIDOPENSCOLVALUE))));
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
						{
							strTemp = Strings.Replace(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE), ",", "", 1, -1, CompareConstants.vbTextCompare);
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							if (lngTemp != 0)
							{
								if (dblMin != 0 && dblMin > lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is less than the minimum of " + FCConvert.ToString(dblMin), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
								if (dblMax != 0 && dblMax < lngTemp)
								{
									MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is greater than the maximum of " + FCConvert.ToString(dblMax), "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return SaveGridOpens;
								}
							}
							if (FCConvert.CBool(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLMANDATORY)))
							{
								if (lngTemp == 0)
								{
									if (dblMin > lngTemp || dblMax < lngTemp)
									{
										MessageBox.Show("Item " + GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLDESC) + " is a mandatory field" + "\r\n" + "You must enter a valid number between " + FCConvert.ToString(dblMin) + " and " + FCConvert.ToString(dblMax) + " to save", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										return SaveGridOpens;
									}
								}
							}
							clsSave.Set_Fields("numericvalue", lngTemp);
						}
						else if (Conversion.Val(GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPETEXT)
						{
							clsSave.Set_Fields("textvalue", GridOpens.TextMatrix(lngRow, CNSTGRIDOPENSCOLVALUE));
						}
						// End If
						// If .TextMatrix(lngRow, CNSTGRIDOPENSCOLUSEDATE) Then
						// If IsDate(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE)) Then
						// clsSave.Fields("datevalue") = Format(.TextMatrix(lngRow, CNSTGRIDOPENSCOLDATE), "MM/dd/yyyy")
						// End If
						// Else
						// clsSave.Fields("datevalue") = 0
						// End If
						clsSave.Update();
					}
					// lngRow
				}
				SaveGridOpens = true;
				return SaveGridOpens;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Eror Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveGridOpens", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveGridOpens;
		}

		private void GridOpens_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridOpens.GetFlexRowIndex(e.RowIndex);
            int col = GridOpens.GetFlexColIndex(e.ColumnIndex);

            if (row < 1)
				return;
			string strTemp = "";
			string[] strAry = null;
			GridOpens.EditMask = "";
			if (col == CNSTGRIDOPENSCOLVALUE)
			{
				if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDATE)
				{
					strTemp = GridOpens.EditText;
					if (Information.IsDate(strTemp))
					{
						strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
					}
					else
					{
						strTemp = "";
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDECIMAL)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strAry = Strings.Split(strTemp, ".", -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Format(strAry[0], "#,###,###,##0");
					if (Information.UBound(strAry, 1) > 0)
					{
						strTemp += "." + strAry[1];
					}
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPELONG)
				{
					strTemp = GridOpens.EditText;
					strTemp = fecherFoundation.Strings.Trim(Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare));
					strTemp = FCConvert.ToString(Conversion.Val(strTemp));
					strTemp = Strings.Format(strTemp, "#,###,###,###,##0");
					GridOpens.EditText = strTemp;
				}
				else if (Conversion.Val(GridOpens.TextMatrix(row, CNSTGRIDOPENSCOLVALUEFORMAT)) == modCEConstants.CNSTVALUETYPEDROPDOWN)
				{
					strTemp = GridOpens.ComboData();
					GridOpens.Cell(FCGrid.CellPropertySettings.flexcpData, row, col, Conversion.Val(strTemp));
				}
				// Case CNSTGRIDOPENSCOLDATE
			}
		}

		private void FillAccountInformation()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from cemaster where ceaccount = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
			if (!clsLoad.EndOfFile())
			{
				lblAccount.Text = FCConvert.ToString(lngCurrentAccount);
				lblStreetNum.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("streetnumber")));
				lblLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("streetname")));
				if (lngCurrentPermit < 1)
				{
					lblName.Text = modMain.NameFromParts(clsLoad.Get_Fields("first"), clsLoad.Get_Fields("middle"), clsLoad.Get_Fields("last"), clsLoad.Get_Fields_String("desig"));
					lblMapLot.Text = FCConvert.ToString(clsLoad.Get_Fields_String("maplot"));
					lblFirstName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("first")));
					lblMiddleName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("middle")));
					lblLastName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("last")));
					lblDesig.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("desig")));
				}
				if (lngCurrentPermit == 0)
				{
					t2kApplicationDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
					txtFiledBy.Text = modMain.NameFromParts(clsLoad.Get_Fields("first"), clsLoad.Get_Fields("middle"), clsLoad.Get_Fields("last"), clsLoad.Get_Fields_String("desig"));
				}
			}
			else
			{
				lblAccount.Text = "";
				lblLocation.Text = "";
				lblStreetNum.Text = "";
				lblName.Text = "";
				lblMapLot.Text = "";
				lblFirstName.Text = "";
				lblMiddleName.Text = "";
				lblLastName.Text = "";
				lblDesig.Text = "";
			}
		}

		private void RefillAccountInformation()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsLoad.OpenRecordset("select * from cemaster where ceaccount = " + FCConvert.ToString(lngCurrentAccount), modGlobalVariables.Statics.strCEDatabase);
			if (!clsLoad.EndOfFile())
			{
				lblAccount.Text = FCConvert.ToString(lngCurrentAccount);
				lblStreetNum.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("streetnumber")));
				lblLocation.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("streetname")));
				lblName.Text = modMain.NameFromParts(clsLoad.Get_Fields("first"), clsLoad.Get_Fields("middle"), clsLoad.Get_Fields("last"), clsLoad.Get_Fields_String("desig"));
				lblMapLot.Text = FCConvert.ToString(clsLoad.Get_Fields_String("maplot"));
				lblFirstName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("first")));
				lblMiddleName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("middle")));
				lblLastName.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("last")));
				lblDesig.Text = fecherFoundation.Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("desig")));
			}
			else
			{
				lblAccount.Text = "";
				lblLocation.Text = "";
				lblStreetNum.Text = "";
				lblName.Text = "";
				lblMapLot.Text = "";
				lblFirstName.Text = "";
				lblMiddleName.Text = "";
				lblLastName.Text = "";
				lblDesig.Text = "";
			}
		}

		private void DisableControls()
		{
			int x;
			for (x = 0; x <= this.Controls.Count - 1; x++)
			{
				if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "    ", 4)) != "GRID" && fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "        ", 8)) != "TOOLBAR1" && fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "      ", 6)) != "FRAME1" && fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "        ", 8)) != "PICTURE1" && !(this.Controls[x] is FCLine) && !(this.Controls[x] is ImageList) && !(this.Controls[x] is FCToolStripMenuItem) && !(this.Controls[x] is FCLabel) && !(this.Controls[x] is FCTabControl))
				{
					this.Controls[x].Enabled = false;
				}
				else if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "    ", 4)) == "GRID")
				{
					if (fecherFoundation.Strings.UCase(this.Controls[x].GetName()) != "GRIDACTIVITY")
					{
						(this.Controls[x] as FCGrid).Editable = FCGrid.EditableSettings.flexEDNone;
						this.Controls[x].ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
						if ((this.Controls[x] as FCGrid).Rows > 0 && (this.Controls[x] as FCGrid).Cols > 0)
						{
							(this.Controls[x] as FCGrid).Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, (this.Controls[x] as FCGrid).Rows - 1, (this.Controls[x] as FCGrid).Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
						}
					}
					else
					{
						gridActivity.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
						if (gridActivity.Rows > 0)
						{
							gridActivity.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, gridActivity.Rows - 1, gridActivity.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
						}
					}
				}
				else if (fecherFoundation.Strings.UCase(Strings.Left(this.Controls[x].GetName() + "        ", 8)) == "TOOLBAR1")
				{
					cmdAddContractor.Enabled = false;
					cmdDeleteContractor.Enabled = false;
				}
			}
			// x
			cmdAddActivity.Enabled = false;
			cmdAddInspection.Enabled = false;
			cmdDeleteContractor.Enabled = false;
			cmdMovePermit.Enabled = false;
			cmdSave.Enabled = false;
			mnuSaveExit.Enabled = false;
		}

		private void optViolations_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			int lngRow;
			switch (Index)
			{
				case 0:
					{
						// current
						for (lngRow = 1; lngRow <= gridViolations.Rows - 1; lngRow++)
						{
							if (Conversion.Val(gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS)) == modCEConstants.CNSTVIOLATIONSTATUSCLOSED || Conversion.Val(gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS)) == modCEConstants.CNSTVIOLATIONSTATUSRESOLVED)
							{
								gridViolations.RowHidden(lngRow, true);
							}
						}
						// lngRow
						break;
					}
				case 1:
					{
						// all
						for (lngRow = 1; lngRow <= gridViolations.Rows - 1; lngRow++)
						{
							gridViolations.RowHidden(lngRow, false);
						}
						// lngRow
						break;
					}
			}
			//end switch
		}

		private void optViolations_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbViolations.SelectedIndex;
			optViolations_CheckedChanged(index, sender, e);
		}

		private void optViolations_Enter(int Index, object sender, System.EventArgs e)
		{
			SSTab1.SelectedIndex = 4;
		}

		private void optViolations_Enter(object sender, System.EventArgs e)
		{
			int index = cmbViolations.SelectedIndex;
			optViolations_Enter(index, sender, e);
		}

		private void Picture1_Click(object sender, System.EventArgs e)
		{
			if (Frame1.Height > GridContractorPhone.Location.Y)
			{
                // it is hidden
                //Frame1.HeightOriginal = GridContractorPhone.TopOriginal + GridContractorPhone.HeightOriginal+80;
                Frame1.Height = Label18.Location.Y + Label18.Height + 10;
                this.Picture1.ImageSource = "node-plus";
            }
            else
			{
                // it is showing               
                //Frame1.HeightOriginal = (framContact.TopOriginal + framContact.HeightOriginal) - Frame1.TopOriginal;
                Frame1.Height = GridContractorPhone.Location.Y + GridContractorPhone.Height + 20;
                this.Picture1.ImageSource = "node-minus";
            }
		}

		private void rtbDescription_GotFocus()
		{
			SSTab1.SelectedIndex = 2;
		}
        //FC:FINAL:CHN - i.issue #1697: Add handling click on buttons (at original App this buttons was into Toolbar1 and handled by 1 method).
        private void Toolbar1_ButtonClick_Wrapper(object sender, EventArgs e)
        {
            this.Toolbar1_ButtonClick(sender as FCButton);
        }
        //FC:FINAL:CHN - i.issue #1697: Add handling click on buttons (at original App this buttons was into Toolbar1 and handled by 1 method). Changed parameters of method. Change button names at conditions on new.
        private void Toolbar1_ButtonClick(Wisej.Web.Button Button)
		{
			string vbPorterVar = Button.Name;
			if (vbPorterVar == "cmdListContractor")
			{
				UpdateCurrentContractor();
				frmPermitContractors.InstancePtr.Init(ref ContList, boolCantEdit);
				ShowCurrentContractor();
			}
			else if (vbPorterVar == "cmdAddContractor")
			{
				AddContractor();
				boolDataChanged = true;
			}
			else if (vbPorterVar == "cmdDeleteContractor")
			{
				clsPermitContractor tCont;
				tCont = ContList.GetCurrentContractor;
				if (!(tCont == null))
				{
					tCont.Deleted = true;
					boolDataChanged = true;
				}
				ContList.MoveFirst();
				ShowCurrentContractor();
			}
		}

		private void AddContractor()
		{
			int intCID;
			if (fecherFoundation.Strings.Trim(txtContractorName.Text) == "" && cmbContractor.ItemData(cmbContractor.SelectedIndex) == 0 && fecherFoundation.Strings.Trim(txtContractorAddr1.Text) == "")
			{
				MessageBox.Show("There is no need to add a contractor" + "\r\n" + "The current one appears unused", "Unused Contractor", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (!(ContList == null))
			{
				UpdateCurrentContractor();
				GridContractorPhone.Rows = 1;
				txtContractorAddr1.Text = "";
				txtContractorAddr2.Text = "";
				txtContractCity.Text = "";
				txtContractorState.Text = "";
				txtContractorZip.Text = "";
				txtContractorZip4.Text = "";
				txtContractorEmail.Text = "";
				txtContractorNote.Text = "";
				txtContractorName.Text = "";
				cmbContractor.SelectedIndex = 0;
				ContList.AddContractor("", "", "", "", "", "", "", "", "", 0, 0);
			}
		}

		private void SetupGridActivity()
		{
			gridActivity.ColHidden(CNSTGRIDACTIVITYCOLAUTOID, true);
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLDATE, "Date");
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLDESC, "Description");
			gridActivity.TextMatrix(0, CNSTGRIDACTIVITYCOLTYPE, "Type");
		}

		private void ResizeGridActivity()
		{
			int GridWidth = 0;
			GridWidth = gridActivity.WidthOriginal;
			gridActivity.ColWidth(CNSTGRIDACTIVITYCOLDATE, FCConvert.ToInt32(0.17 * GridWidth));
			gridActivity.ColWidth(CNSTGRIDACTIVITYCOLTYPE, FCConvert.ToInt32(0.25 * GridWidth));
		}

		private void SetupGridViolations()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLIDENTIFIER, "Identifier");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLISSUED, "Issued");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLSTATUS, "Status");
			gridViolations.TextMatrix(0, CNSTGRIDVIOLATIONSCOLTYPE, "Type");
			gridViolations.ColHidden(CNSTGRIDVIOLATIONSCOLAUTOID, true);
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONSTATUS) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			strTemp = "";
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			strTemp += "#" + FCConvert.ToString(modCEConstants.CNSTVIOLATIONSTATUSCLOSED) + ";Closed";
			gridViolations.ColComboList(CNSTGRIDVIOLATIONSCOLSTATUS, strTemp);
			strTemp = "";
			rsLoad.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEVIOLATIONTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
			while (!rsLoad.EndOfFile())
			{
				strTemp += "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("description") + "|";
				rsLoad.MoveNext();
			}
			if (strTemp != "")
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			gridViolations.ColComboList(CNSTGRIDVIOLATIONSCOLTYPE, strTemp);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTVIOLATIONSSECURITY, false))
			{
				gridViolations.Tag = (System.Object)("UNAUTHORIZED");
				cmdAddViolation.Enabled = false;
			}
			else
			{
				gridViolations.Tag = (System.Object)("");
				if (modSecurity.ValidPermissions_6(this, modCEConstants.CNSTVIOLATIONSEDITSECURITY, false))
				{
					cmdAddViolation.Enabled = true;
				}
				else
				{
					cmdAddViolation.Enabled = false;
				}
			}
		}

		private void ResizeGridViolations()
		{
			int GridWidth = 0;
			GridWidth = gridViolations.WidthOriginal;
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLIDENTIFIER, FCConvert.ToInt32(0.15 * GridWidth));
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLTYPE, FCConvert.ToInt32(0.32 * GridWidth));
			gridViolations.ColWidth(CNSTGRIDVIOLATIONSCOLISSUED, FCConvert.ToInt32(0.17 * GridWidth));
		}

		private void FillGridViolations()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				int x;
				gridViolations.Rows = 1;
				rsLoad.OpenRecordset("select * from violations where violationaccount = " + FCConvert.ToString(lngCurrentAccount) + " and violationaccount > 0 and violationpermit = " + FCConvert.ToString(lngCurrentPermit) + " and violationpermit > 0 order by dateviolationissued desc,violationidentifier", modGlobalVariables.Statics.strCEDatabase);
				while (!rsLoad.EndOfFile())
				{
					gridViolations.Rows += 1;
					lngRow = gridViolations.Rows - 1;
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLAUTOID, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLIDENTIFIER, FCConvert.ToString(rsLoad.Get_Fields_String("violationidentifier")));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLSTATUS, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus"))));
					gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLTYPE, FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("violationtype"))));
					if (Information.IsDate(rsLoad.Get_Fields("dateviolationissued")))
					{
                        //FC:FINAL:MSH - i.issue #1728: Convert.ToDateTime replaced by Get_Fields_DateTime to avoid wrong conversions
                        //if (Convert.ToDateTime(rsLoad.Get_Fields("dateviolationissued")).ToOADate() != 0)
                        if (rsLoad.Get_Fields_DateTime("dateviolationissued").ToOADate() != 0)
						{
                            //FC:FINAL:MSH - i.issue #1728: get date in a short format
                            //gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLISSUED, FCConvert.ToString(rsLoad.Get_Fields("dateviolationissued")));
                            gridViolations.TextMatrix(lngRow, CNSTGRIDVIOLATIONSCOLISSUED, rsLoad.Get_Fields_DateTime("dateviolationissued").ToShortDateString());
						}
					}
					if (cmbViolations.Text == "Show Current")
					{
						if ((Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus")) == modCEConstants.CNSTVIOLATIONSTATUSCLOSED) || (Conversion.Val(rsLoad.Get_Fields_Int32("violationstatus")) == modCEConstants.CNSTVIOLATIONSTATUSRESOLVED))
						{
							gridViolations.RowHidden(lngRow, true);
						}
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillGridViolations", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuAddViolation_Click(object sender, System.EventArgs e)
		{
			if (lngCurrentPermit == 0)
			{
				MessageBox.Show("This permit must be saved before you can add a violation", "Cannot Add", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			frmViolation.InstancePtr.Init(0, false, lngCurrentPermit, lngCurrentAccount);
			FillGridViolations();
		}

		private void gridViolations_DblClick(object sender, System.EventArgs e)
		{
			if (gridViolations.MouseRow > 0)
			{
				if (!(fecherFoundation.Strings.LCase(FCConvert.ToString(gridViolations.Tag)) == "unauthorized"))
				{
					bool boolRonly = false;
					boolRonly = !cmdAddViolation.Enabled;
					frmViolation.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(gridViolations.TextMatrix(gridViolations.MouseRow, CNSTGRIDVIOLATIONSCOLAUTOID))), boolRonly);
					FillGridViolations();
				}
			}
		}
    }
}
