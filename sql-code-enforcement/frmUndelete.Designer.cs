//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmUndelete.
	/// </summary>
	partial class frmUndelete
	{
		public fecherFoundation.FCFrame framRecords;
		public FCGrid Grid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAccounts;
		public fecherFoundation.FCToolStripMenuItem mnuPermits;
		public fecherFoundation.FCToolStripMenuItem mnuInspections;
		public fecherFoundation.FCToolStripMenuItem mnuContractors;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuUndelete;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.framRecords = new fecherFoundation.FCFrame();
            this.Grid = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAccounts = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPermits = new fecherFoundation.FCToolStripMenuItem();
            this.mnuInspections = new fecherFoundation.FCToolStripMenuItem();
            this.mnuContractors = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuUndelete = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdUndelete = new fecherFoundation.FCButton();
            this.cmdContractors = new fecherFoundation.FCButton();
            this.cmdInspections = new fecherFoundation.FCButton();
            this.cmdPermits = new fecherFoundation.FCButton();
            this.cmdAccounts = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRecords)).BeginInit();
            this.framRecords.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUndelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContractors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdInspections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPermits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccounts)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdUndelete);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(642, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framRecords);
            this.ClientArea.Size = new System.Drawing.Size(642, 498);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAccounts);
            this.TopPanel.Controls.Add(this.cmdPermits);
            this.TopPanel.Controls.Add(this.cmdInspections);
            this.TopPanel.Controls.Add(this.cmdContractors);
            this.TopPanel.Size = new System.Drawing.Size(642, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdContractors, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdInspections, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPermits, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAccounts, 0);
            // 
            // HeaderText
            // 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(209, 30);
            this.HeaderText.Text = "Undelete Records";
            // 
            // framRecords
            // 
            this.framRecords.Controls.Add(this.Grid);
            this.framRecords.Location = new System.Drawing.Point(30, 30);
            this.framRecords.Name = "framRecords";
            this.framRecords.Size = new System.Drawing.Size(769, 492);
            this.framRecords.Text = "Accounts";
            // 
            // Grid
            // 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.Grid.Cols = 13;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.DragIcon = null;
            this.Grid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.Grid.ExtendLastCol = true;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.GridColorFixed = System.Drawing.Color.Empty;
            this.Grid.Location = new System.Drawing.Point(20, 30);
            this.Grid.Name = "Grid";
            this.Grid.OutlineCol = 0;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
            this.Grid.Rows = 1;
			this.Grid.ScrollTipText = null;
			this.Grid.ShowColumnVisibilityMenu = false;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(731, 446);
			this.Grid.StandardTab = true;
			this.Grid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.Grid.TabIndex = 1;
            this.Grid.Tag = "0";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAccounts,
            this.mnuPermits,
            this.mnuInspections,
            this.mnuContractors,
            this.mnuSepar2,
            this.mnuUndelete,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuAccounts
            // 
            this.mnuAccounts.Index = 0;
            this.mnuAccounts.Name = "mnuAccounts";
            this.mnuAccounts.Text = "Accounts";
            this.mnuAccounts.Click += new System.EventHandler(this.mnuAccounts_Click);
            // 
            // mnuPermits
            // 
            this.mnuPermits.Index = 1;
            this.mnuPermits.Name = "mnuPermits";
            this.mnuPermits.Text = "Permits";
            this.mnuPermits.Click += new System.EventHandler(this.mnuPermits_Click);
            // 
            // mnuInspections
            // 
            this.mnuInspections.Index = 2;
            this.mnuInspections.Name = "mnuInspections";
            this.mnuInspections.Text = "Inspections";
            this.mnuInspections.Click += new System.EventHandler(this.mnuInspections_Click);
            // 
            // mnuContractors
            // 
            this.mnuContractors.Index = 3;
            this.mnuContractors.Name = "mnuContractors";
            this.mnuContractors.Text = "Contractors";
            this.mnuContractors.Click += new System.EventHandler(this.mnuContractors_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 4;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuUndelete
            // 
            this.mnuUndelete.Index = 5;
            this.mnuUndelete.Name = "mnuUndelete";
            this.mnuUndelete.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuUndelete.Text = "Undelete Records";
            this.mnuUndelete.Click += new System.EventHandler(this.mnuUndelete_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 6;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 7;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdUndelete
            // 
            this.cmdUndelete.AppearanceKey = "acceptButton";
            this.cmdUndelete.Location = new System.Drawing.Point(224, 30);
            this.cmdUndelete.Name = "cmdUndelete";
            this.cmdUndelete.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdUndelete.Size = new System.Drawing.Size(182, 48);
            this.cmdUndelete.Text = "Undelete Records";
            this.cmdUndelete.Click += new System.EventHandler(this.mnuUndelete_Click);
            // 
            // cmdContractors
            // 
            this.cmdContractors.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdContractors.AppearanceKey = "toolbarButton";
            this.cmdContractors.Location = new System.Drawing.Point(526, 29);
            this.cmdContractors.Name = "cmdContractors";
            this.cmdContractors.Size = new System.Drawing.Size(88, 24);
            this.cmdContractors.TabIndex = 1;
            this.cmdContractors.Text = "Contractors";
            this.cmdContractors.Click += new System.EventHandler(this.mnuContractors_Click);
            // 
            // cmdInspections
            // 
            this.cmdInspections.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdInspections.AppearanceKey = "toolbarButton";
            this.cmdInspections.Location = new System.Drawing.Point(432, 29);
            this.cmdInspections.Name = "cmdInspections";
            this.cmdInspections.Size = new System.Drawing.Size(88, 24);
            this.cmdInspections.TabIndex = 2;
            this.cmdInspections.Text = "Inspections";
            this.cmdInspections.Click += new System.EventHandler(this.mnuInspections_Click);
            // 
            // cmdPermits
            // 
            this.cmdPermits.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPermits.AppearanceKey = "toolbarButton";
            this.cmdPermits.Location = new System.Drawing.Point(362, 29);
            this.cmdPermits.Name = "cmdPermits";
            this.cmdPermits.Size = new System.Drawing.Size(64, 24);
            this.cmdPermits.TabIndex = 3;
            this.cmdPermits.Text = "Permits";
            this.cmdPermits.Click += new System.EventHandler(this.mnuPermits_Click);
            // 
            // cmdAccounts
            // 
            this.cmdAccounts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAccounts.AppearanceKey = "toolbarButton";
            this.cmdAccounts.Location = new System.Drawing.Point(282, 29);
            this.cmdAccounts.Name = "cmdAccounts";
            this.cmdAccounts.Size = new System.Drawing.Size(74, 24);
            this.cmdAccounts.TabIndex = 4;
            this.cmdAccounts.Text = "Accounts";
            this.cmdAccounts.Click += new System.EventHandler(this.mnuAccounts_Click);
            // 
            // frmUndelete
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(642, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmUndelete";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Undelete Records";
            this.Load += new System.EventHandler(this.frmUndelete_Load);
            this.Resize += new System.EventHandler(this.frmUndelete_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUndelete_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framRecords)).EndInit();
            this.framRecords.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUndelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdContractors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdInspections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPermits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAccounts)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdUndelete;
        private FCButton cmdInspections;
        private FCButton cmdContractors;
        private FCButton cmdPermits;
        private FCButton cmdAccounts;
    }
}