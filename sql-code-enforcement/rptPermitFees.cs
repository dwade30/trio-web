﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptPermitFees.
	/// </summary>
	public partial class rptPermitFees : BaseSectionReport
	{
		public rptPermitFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "ActiveReport1";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptPermitFees InstancePtr
		{
			get
			{
				return (rptPermitFees)Sys.GetInstance(typeof(rptPermitFees));
			}
		}

		protected rptPermitFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsType.Dispose();
				rsReport.Dispose();
				rsCategory.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPermitFees	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private clsDRWrapper rsReport = new clsDRWrapper();
		private clsDRWrapper rsType = new clsDRWrapper();
		private clsDRWrapper rsCategory = new clsDRWrapper();
		private double dblTotValue;
		private double dblTotFee;
		private double dblTotCount;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("PermitCount");
			Fields.Add("PermitFee");
			Fields.Add("PermitValue");
			Fields.Add("permittype");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
			if (!rsReport.EndOfFile())
			{
				this.Fields["permittype"].Value = rsReport.Get_Fields_String("permittype");
				this.Fields["PermitFee"].Value = Conversion.Val(rsReport.Get_Fields_String("sumfee"));
				this.Fields["PermitCount"].Value = Conversion.Val(rsReport.Get_Fields_String("thecount"));
				this.Fields["PermitValue"].Value = Conversion.Val(rsReport.Get_Fields_String("sumvalue"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			rsType.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
			rsCategory.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITCATEGORIES) + " order by ID", modGlobalVariables.Statics.strCEDatabase);
			dblTotValue = 0;
			dblTotFee = 0;
			dblTotCount = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				// If rsType.FindFirstRecord("ID", Val(rsReport.Fields("permittype"))) Then
				// txtType.Text = rsType.Fields("description")
				// Else
				// If Val(rsReport.Fields("permittype")) <> 0 Then
				// txtType.Text = "Unknown"
				// Else
				// txtType.Text = "None"
				// End If
				// End If
				if (rsCategory.FindFirstRecord("ID", Conversion.Val(rsReport.Get_Fields_String("permitsubtype"))))
				{
					txtCategory.Text = rsCategory.Get_Fields_String("description");
				}
				else
				{
					if (Conversion.Val(rsReport.Get_Fields_String("Permitsubtype")) != 0)
					{
						txtCategory.Text = "Unknown";
					}
					else
					{
						txtCategory.Text = "None";
					}
				}
				double dblValue = 0;
				double dblFee = 0;
				double dblCount = 0;
				dblValue = rsReport.Get_Fields_Double("Sumvalue");
				dblCount = rsReport.Get_Fields_Double("thecount");
				dblFee = rsReport.Get_Fields_Double("sumfee");
				txtCount.Text = Strings.Format(dblCount, "#,###,###,##0");
				txtFee.Text = Strings.Format(dblFee, "#,###,###,##0.00");
				txtValue.Text = Strings.Format(dblValue, "#,###,###,##0");
				dblTotValue += dblValue;
				dblTotFee += dblFee;
				dblTotCount += dblCount;
				rsReport.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (rsType.FindFirstRecord("ID", Conversion.Val(this.Fields["permittype"].Value)))
			{
				txtType.Text = rsType.Get_Fields_String("description");
			}
			else
			{
				if (Conversion.Val(this.Fields["permittype"].Value) != 0)
				{
					txtType.Text = "Unknown";
				}
				else
				{
					txtType.Text = "None";
				}
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		public void Init(ref clsSQLStatement clsStatement, bool boolDistinct = false)
		{
			string strSQL = "";
			string strWhere;
			strWhere = "not permitS.deleted = 1";
			if (!(clsStatement == null))
			{
				// strSQL = clsStatement.SQLStatement
				// If boolDistinct Then
				if (clsStatement.WhereStatement != "")
				{
					strWhere = clsStatement.WhereStatement + " and " + strWhere;
				}
				else
				{
					strWhere = " where " + strWhere;
				}
				strSQL = "select distinct Fee,estimatedvalue,permitidentifier,permityear,permitnumber,originalstreetnum,originalstreet,permittype,permitsubtype,appdencode from (" + clsStatement.SelectStatement + " " + strWhere + ") def " + clsStatement.OrderByStatement;
				strSQL = "select sum(Fee) as sumfee,sum(estimatedValue) as SumValue, permittype,permitsubtype, count(permittype) as TheCount from (" + strSQL + ") abc group by permittype,permitsubtype order by permittype,permitsubtype";
				// End If
			}
			else
			{
				strSQL = "select permits.ID as PermitAutoid,account,PermitIdentifier,PermitYear,AppDenCode,ApplicationDate,[Plan],FiledBy,ContactName,ContactEmail,StartDate,CompletionDate,EstimatedValue,Fee,PermitType,";
				strSQL += "PermitSubType,PERMITS.ContractorID AS PermitContractorID,CertificateOfOCCRequired,CertificateOfOCCDate,PermitNumber,Units,DwellUnits,NumBuildings,CommResCode,[Description],OMBCode,PermitContractorName,";
				strSQL += "PermitContractorAddress1,PermitContractorAddress2,PermitContractorCity,PermitContractorState,PermitContractorZip,PermitContractorZip4,PermitContractorEmail,Completed,";
				strSQL += "permits.deleted as PermitDeleted,permits.comment as PermitComment,OriginalOwner,OriginalMapLot,OriginalStreetNum,OriginalStreet,OriginalOwnerFirst,OriginalOwnerLast,OriginalOwnerMiddle,OriginalOwnerDesig from permits  where " + strWhere + " ";
				// & order by applicationdate,permitidentifier"
				strSQL = "select sum(Fee) as sumfee,sum(estimatedValue) as SumValue, permittype,permitsubtype, count(permittype) as TheCount from (" + strSQL + ") abc group by permittype,permitsubtype order by permittype,permitsubtype";
			}
			txtRange.Text = clsStatement.ParameterDescriptionText;
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			frmReportViewer.InstancePtr.Init(this, strAttachmentName: "Permit Fees");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtTotalCount.Text = Strings.Format(dblTotCount, "#,###,###,##0");
			txtTotalFee.Text = Strings.Format(dblTotFee, "#,###,###,##0.00");
			txtTotalValue.Text = Strings.Format(dblTotValue, "#,###,###,##0");
		}

		
	}
}
