//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmDynamicReport : BaseForm
	{
		public frmDynamicReport()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDynamicReport InstancePtr
		{
			get
			{
				return (frmDynamicReport)Sys.GetInstance(typeof(frmDynamicReport));
			}
		}

		protected frmDynamicReport _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey
		// Date
		// 03/24/2005
		// you will need:
		// modDynamicReports
		// frmChooseDynamicReport
		// modDynamicReports contains functions to create the tables in your database
		// ********************************************************
		const int CNSTGRIDCOLTAG = 2;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLCODE = 0;
		const int CNSTGRIDCOLHELP = 3;
		bool boolDontCheck;
		int intReportType;
		string strModule;
		string strModuleDB;
		int lngCurrentID;

		public void Init(string strMod, int intTypeOfReport, bool boolModal = false, int lngID = 0, string strCaption = "Dynamic Documents")
		{
			// strmod is the two char module name
			// set lngID if you want to start with a specific report
			intReportType = intTypeOfReport;
			strModule = strMod;
			lngCurrentID = lngID;
			strModuleDB = "tw" + strModule + "0000.vb1";
			this.Text = strCaption;
			if (boolModal)
			{
				this.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
			LoadReport();
		}

		private void frmDynamicReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmDynamicReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDynamicReport properties;
			//frmDynamicReport.FillStyle	= 0;
			//frmDynamicReport.ScaleWidth	= 9045;
			//frmDynamicReport.ScaleHeight	= 7650;
			//frmDynamicReport.LinkTopic	= "Form2";
			//frmDynamicReport.PaletteMode	= 1  'UseZOrder;
			//rtbData properties;
			//rtbData.ScrollBars	= 2;
			//rtbData.AutoVerbMenu	= -1  'True;
			//rtbData.TextRTF	= $"frmDynamicReport.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
			if (lngCurrentID > 0)
			{
			}
		}

		private void frmDynamicReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			if (Grid.IsSubtotal(Grid.MouseRow))
			{
				// don't want the categories
				return;
			}
			else
			{
				rtbData.SelText = Grid.TextMatrix(Grid.MouseRow, CNSTGRIDCOLTAG);
			}
		}

		private void GridProperties_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = GridProperties.GetFlexRowIndex(e.RowIndex);
            int col = GridProperties.GetFlexColIndex(e.ColumnIndex);

            string strTemp;
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			strTemp = GridProperties.TextMatrix(0, 1);
			strTemp = "<" + strTemp;
			for (x = 1; x <= (GridProperties.Rows - 1); x++)
			{
				if (row != x)
				{
					strTemp += " " + GridProperties.TextMatrix(x, 0) + " = " + FCConvert.ToString(Convert.ToChar(34)) + GridProperties.TextMatrix(x, 1) + FCConvert.ToString(Convert.ToChar(34));
				}
				else
				{
					strTemp += " " + GridProperties.TextMatrix(x, 0) + " = " + FCConvert.ToString(Convert.ToChar(34)) + GridProperties.EditText + FCConvert.ToString(Convert.ToChar(34));
				}
			}
			// x
			strTemp += ">";
			if (rtbData.SelLength > 2)
			{
				rtbData.SelText = strTemp;
			}
			else
			{
				string strStart = "";
				string strEnd = "";
				// strAry = Split(GridProperties.Tag, ",", , vbTextCompare)
				int intStart = 0;
				// vbPorter upgrade warning: intEnd As int	OnWriteFCConvert.ToInt32(
				int intEnd = 0;
				intStart = FCConvert.ToInt16(GridProperties.Tag);
				if (intStart > 1)
				{
					strStart = Strings.Mid(rtbData.Text, 1, intStart - 1);
				}
				else
				{
					strStart = "";
				}
				intEnd = Strings.InStr(intStart, rtbData.Text, ">", CompareConstants.vbTextCompare);
				if (intEnd < rtbData.Text.Length)
				{
					strEnd = Strings.Mid(rtbData.Text, intEnd + 1);
				}
				else
				{
					strEnd = "";
				}
				rtbData.Text = strStart + strTemp + strEnd;
			}
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			rtbData.Text = "";
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// delete the dynamic document of the users choosing
			int lngIDToLoad;
			clsDRWrapper clsSave = new clsDRWrapper();
			lngIDToLoad = frmChooseDynamicReport.InstancePtr.Init("CE", intReportType, true);
			if (lngIDToLoad == 0)
				return;
			if (lngIDToLoad == lngCurrentID)
			{
				lngCurrentID = 0;
			}
			clsSave.Execute("delete from dynamicREPORTS where ID = " + FCConvert.ToString(lngIDToLoad), strModuleDB);
			MessageBox.Show("Document Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuLoadReport_Click(object sender, System.EventArgs e)
		{
			// load the text from the document the user chooses
			int lngIDToLoad;
			lngIDToLoad = frmChooseDynamicReport.InstancePtr.Init("CE", intReportType);
			if (lngIDToLoad == 0)
				return;
			if (lngIDToLoad < 0)
			{
				MessageBox.Show("There are no reports to load", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			lngCurrentID = lngIDToLoad;
			LoadReport();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveReport();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveReport())
			{
				mnuExit_Click();
			}
		}
		//FC:FINAL:CHN - i.issue #1690: Add Missing handler by wrapper.
        private void rtbData_KeyDown_Wrapper(object sender, KeyEventArgs e)
        {
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            rtbData_KeyDown(e.KeyCode, Shift);
        }

        private void rtbData_KeyDown(Keys KeyCode, int Shift)
		{
			string strText = "";
			int lngStart = 0;
			int lngLen = 0;
			// make sure they don't type in a < or > and cant edit any part of a tag other than deleting it
			boolDontCheck = true;
			switch (KeyCode)
			{
				case Keys.Back:
					{
						KeyCode = 0;
						// backspace - need to make sure that there is not a tag where the user is trying to delete
						// if it is a tag, then delete the whole thing
						NewRTB_BackSpace();
						break;
					}
				case Keys.Delete:
					{
						boolDontCheck = false;
						KeyCode = 0;
						NewRTB_Delete();
						break;
					}
				case Keys.Right:
					{
						// right arrow key
						strText = rtbData.Text;
						lngStart = rtbData.SelStart;
						lngLen = rtbData.SelLength;
						lngStart += 1;
						if (lngLen > 0)
						{
							if (Shift > 0)
							{
								if (Strings.Mid(strText, lngStart + 1, 1) == "<")
								{
									KeyCode = 0;
									boolDontCheck = false;
									rtbData.SelLength = 0;
									rtbData.SelStart = lngStart + lngLen - 1;
								}
								else
								{
									rtbData.SelStart = lngStart + lngLen - 1;
								}
							}
							else
							{
								rtbData.SelStart = lngStart + lngLen - 2;
							}
						}
						else
						{
							if (Strings.Mid(strText, lngStart, 1) == "<")
							{
								KeyCode = 0;
								lngLen = Strings.InStr(lngStart, strText, ">", CompareConstants.vbBinaryCompare) - lngStart;
								rtbData.SelLength = lngLen + 1;
								CheckTagProperties(rtbData.SelText);
							}
						}
						break;
					}
				case Keys.Left:
					{
						// left arrow key
						strText = rtbData.Text;
						lngStart = rtbData.SelStart;
						lngLen = rtbData.SelLength;
						if (lngStart != 0)
						{
							if (lngLen > 0)
							{
								if (Shift > 0)
								{
									KeyCode = 0;
									boolDontCheck = false;
									rtbData.SelLength = 0;
									rtbData.SelStart = lngStart;
								}
								else
								{
								}
							}
							else
							{
								if (Strings.Mid(strText, lngStart, 1) == ">")
								{
									KeyCode = 0;
									lngLen += lngStart - Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
									lngStart = Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
									rtbData.SelStart = lngStart - 1;
									rtbData.SelLength = lngLen + 1;
									CheckTagProperties(rtbData.SelText);
									boolDontCheck = false;
								}
							}
						}
						break;
					}
				case Keys.Up:
					{
						rtbData.SelLength = 0;
						boolDontCheck = false;
						break;
					}
				case Keys.Down:
					{
						rtbData.SelLength = 0;
						boolDontCheck = false;
						break;
					}
				case Keys.Return:
					{
                        //FC:FINAL:CHN - i.issue #1690: Add Missing handler by wrapper.
                        var olSelectionStart = rtbData.SelectionStart;
                        rtbData.SelectionStart -= 1;
						rtbData.SelText = "<CRLF>";
                        rtbData.SelectionStart = olSelectionStart + 6;
                        break;
					}
			}
			//end switch
			boolDontCheck = false;
		}

		private void NewRTB_BackSpace()
		{
			// this will actually take over for the real backspace key
			string strText;
			string strTemp = "";
			int lngStart = 0;
			int lngDelNum = 0;
			strText = rtbData.Text;
			if (rtbData.SelLength > 0)
			{
				// if there is a selection, then just delete it
				rtbData.SelText = "";
			}
			else
			{
				lngStart = rtbData.SelStart;
				if (lngStart > 1)
				{
					boolDontCheck = true;
					// turn off the selction checking
					if (Strings.Mid(strText, lngStart, 1) == ">")
					{
						// I need to select and delete the whole tag
						lngDelNum = lngStart - Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/) + 1;
						rtbData.SelStart = lngStart - lngDelNum;
						// start at the beginning
						rtbData.SelLength = lngDelNum;
						// the length of the tag to delete
						rtbData.SelText = "";
						// delete the text selected
					}
					else
					{
						rtbData.SelStart = lngStart - 1;
						rtbData.SelLength = 1;
						rtbData.SelText = "";
					}
					boolDontCheck = false;
				}
				else
				{
					// if at the beginning, then don't do anything
					// do nothing
				}
			}
		}

		private void NewRTB_Delete()
		{
			// this will do the delete key's job when inside a tag
			int lngStart = 0;
			int lngLen = 0;
			string strText;
			strText = rtbData.Text;
			boolDontCheck = true;
			if (rtbData.SelLength > 0)
			{
				// if there is a selection, then just delete it
				rtbData.SelText = "";
			}
			else
			{
				// if there is no selection made, then check to see if there is a tag
				lngStart = rtbData.SelStart;
				if (Strings.Mid(strText, lngStart + 1, 1) == "<")
				{
					lngLen = Strings.InStr(lngStart + 1, strText, ">", CompareConstants.vbBinaryCompare) - lngStart;
					if (lngLen > 0)
					{
						rtbData.SelLength = lngLen;
						rtbData.SelText = "";
					}
				}
				else
				{
					// not a tag, just delete one character
					rtbData.SelLength = 1;
					rtbData.SelText = "";
				}
			}
			boolDontCheck = false;
		}

		private void rtbData_KeyPress(ref int KeyAscii)
		{
			switch (KeyAscii)
			{
				case 60:
				case 62:
					{
						KeyAscii = 0;
						break;
					}
			}
			//end switch
		}

		private void rtbData_SelChange()
		{
			int lngStart = rtbData.SelStart;
			int lngLen = rtbData.SelLength;
			CheckInsideTag(lngStart, lngLen);
		}

		private void CheckInsideTag(int lngStart, int lngLen)
		{
			// this will check to make sure that neither the beginning or the end of the selection is in the middle of a tag
			int lngEnd = 0;
			int lngGT = 0;
			int lngLT = 0;
			string strText = "";
			string strTemp = "";
			bool boolInTag = false;
			if (!boolDontCheck)
			{
				boolInTag = false;
				if (lngStart == 0 && lngLen == 0)
				{
					GridProperties.Visible = false;
					return;
				}
				lngStart += 1;
				// adjust for the 1 based array the text functions use
				strText = rtbData.Text;
				// check the beginning of the selection
				if (lngStart == 1)
				{
					lngGT = Strings.InStrRev(strText, ">", lngStart, CompareConstants.vbTextCompare/*?*/);
				}
				else
				{
					lngGT = Strings.InStrRev(strText, ">", lngStart - 1, CompareConstants.vbTextCompare/*?*/);
				}
				lngLT = Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
				if (lngLT > lngGT || (lngLT != 0 && lngGT == 0))
				{
					// this means that we are in a tag on the front side of the selection
					lngLen += (lngStart - lngLT);
					lngStart = lngLT;
					boolInTag = true;
				}
				else
				{
					// we are not in a tag from the front of the selection
				}
				// check the end of the selection
				lngEnd = lngStart + lngLen;
				lngGT = Strings.InStr(lngEnd, strText, ">", CompareConstants.vbBinaryCompare);
				lngLT = Strings.InStr(lngEnd, strText, "<", CompareConstants.vbBinaryCompare);
				if (lngGT < lngLT || (lngGT != 0 && lngLT == 0))
				{
					// this means that the end of the selection is inside of a tag
					lngLen += (lngGT - lngEnd + 1);
					boolInTag = true;
				}
				boolDontCheck = true;
				rtbData.SelStart = lngStart - 1;
				// adjust for the 0 based array the text boxes use
				if (lngStart > 0)
				{
					rtbData.SelLength = lngLen;
				}
				boolDontCheck = false;
				if (boolInTag && rtbData.SelLength > 0)
				{
					CheckTagProperties(rtbData.SelText);
				}
				else if (boolInTag)
				{
					lngLen = lngLen;
				}
				else
				{
					GridProperties.Visible = false;
				}
			}
		}

		private void CheckTagProperties(string strText)
		{
			// vbPorter upgrade warning: intStart As int	OnWriteFCConvert.ToInt32(
			int intStart;
			// vbPorter upgrade warning: intTemp As int	OnWriteFCConvert.ToInt32(
			int intTemp = 0;
			string strProperty = "";
			string strValue = "";
			string strTemp = "";
			string strTag;
			int lngRow;
			GridProperties.Visible = false;
			strTag = Strings.Mid(strText, 2, strText.Length - 1);
			GridProperties.TextMatrix(0, 1, strTag);
			intStart = Strings.InStr(1, strTag, " ", CompareConstants.vbTextCompare);
			if (intStart > 0)
			{
				GridProperties.TextMatrix(0, 1, Strings.Mid(strTag, 1, intStart - 1));
			}
			GridProperties.Rows = 1;
			GridProperties.Tag = (System.Object)(rtbData.SelStart);
			while (intStart > 0 && intStart < strTag.Length)
			{
				intStart += 1;
				if (intStart > strTag.Length)
					break;
				intTemp = Strings.InStr(intStart, strTag, "=", CompareConstants.vbTextCompare);
				if (intTemp > 0)
				{
					strProperty = fecherFoundation.Strings.Trim(Strings.Mid(strTag, intStart, intTemp - intStart));
				}
				else
				{
					break;
				}
				if (intTemp + 1 > strTag.Length)
					break;
				intStart = intTemp + 1;
				intTemp = Strings.InStr(intStart, strTag, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
				if (intTemp < 1 || intTemp + 1 > strTag.Length)
					break;
				intStart = intTemp + 1;
				intTemp = Strings.InStr(intStart, strTag, FCConvert.ToString(Convert.ToChar(34)), CompareConstants.vbTextCompare);
				if (intTemp < 1)
					break;
				if (intStart == intTemp)
				{
					strValue = "";
				}
				else
				{
					strValue = Strings.Mid(strTag, intStart, intTemp - intStart);
				}
				GridProperties.Rows += 1;
				lngRow = GridProperties.Rows - 1;
				GridProperties.TextMatrix(lngRow, 0, strProperty);
				GridProperties.TextMatrix(lngRow, 1, strValue);
				intStart = intTemp;
			}
			if (GridProperties.Rows > 1)
			{
				GridProperties.Visible = true;
			}
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 4;
			Grid.ColHidden(CNSTGRIDCOLCODE, true);
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			Grid.TextMatrix(0, CNSTGRIDCOLTAG, "Tag");
			Grid.TextMatrix(0, CNSTGRIDCOLHELP, "Description");
			Grid.ExtendLastCol = true;
		}

		private void ResizeGrid()
		{
            //FC:FINAl:BSE #1975 grid size is done with anchoring 
            //Grid.AutoSize(CNSTGRIDCOLNAME);
            //Grid.AutoSize(CNSTGRIDCOLTAG);
            //FC:FINAL:BSE #1976 increase columns width 
            Grid.ColWidth(CNSTGRIDCOLNAME, (int)(Grid.WidthOriginal * 0.33));
            Grid.ColWidth(CNSTGRIDCOLTAG, (int)(Grid.WidthOriginal * 0.33));
            Grid.ColWidth(CNSTGRIDCOLHELP, (int)(Grid.WidthOriginal * 0.34));
        }

		private void LoadGrid()
		{
			// Dim clsLoad As New clsdrwrapper
			string strSQL = "";
			string strCategory = "";
			int lngRow;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				// strSQL = "select * from DynamicReportCodes where reporttype = " & intReportType & " order by categoryno,code"
				// Call clsLoad.OpenRecordset(strSQL, strModuleDB)
				Grid.OutlineBar = FCGrid.OutlineBarSettings.flexOutlineBarSimple;
				Grid.OutlineCol = CNSTGRIDCOLNAME;
				Grid.Rows = 1;
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, "Misc");
				Grid.RowOutlineLevel(lngRow, 0);
				Grid.IsSubtotal(lngRow, true);
				Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, Grid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, "Date");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(-1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, "<DATE>");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHELP, "Current Date");
                //FC:FINAL:MSH - i.issue #1689: change row Outline Level
				Grid.RowOutlineLevel(lngRow, 1);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, "Time");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(-1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, "<TIME>");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHELP, "Current Time");
                //FC:FINAL:MSH - i.issue #1689: change row Outline Level
				Grid.RowOutlineLevel(lngRow, 1);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, "Year");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(-1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, "<YEAR>");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHELP, "Current Year");
                //FC:FINAL:MSH - i.issue #1689: change row Outline Level
				Grid.RowOutlineLevel(lngRow, 1);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, ">");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(-1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, "<GreaterThan>");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHELP, "Greater than symbol");
                //FC:FINAL:MSH - i.issue #1689: change row Outline Level
				Grid.RowOutlineLevel(lngRow, 1);
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, "<");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(-1));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLTAG, "<LessThan>");
				Grid.TextMatrix(lngRow, CNSTGRIDCOLHELP, "Less than symbol");
                //FC:FINAL:MSH - i.issue #1689: change row Outline Level
				Grid.RowOutlineLevel(lngRow, 1);
                strCategory = "Misc";
				clsDynamicDocument ddSetup = new clsDynamicDocument();
				clsDynamicDocumentTag tTag;
				ddSetup.SetupTags();
				ddSetup.MoveFirst();
				ddSetup.MovePrevious();
				while (ddSetup.MoveNext() > 0)
				{
					tTag = ddSetup.GetCurrentTag();
					if (!(tTag == null))
					{
						if (strCategory != tTag.Category)
						{
							Grid.Rows += 1;
							lngRow = Grid.Rows - 1;
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, tTag.Category);
							Grid.RowOutlineLevel(lngRow, 0);
							Grid.IsSubtotal(lngRow, true);
							Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, Grid.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							strCategory = tTag.Category;
						}
						Grid.Rows += 1;
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLCODE, FCConvert.ToString(tTag.Code));
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLNAME, tTag.TagName);
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTAG, tTag.Tag);
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLHELP, tTag.Description);
                        //FC:FINAL:MSH - i.issue #1689: change row Outline Level
				        Grid.RowOutlineLevel(Grid.Rows - 1, 1);
                    }
                }
                // Do While Not clsLoad.EndOfFile
                // If strCategory <> clsLoad.Fields("category") Then
                // .Rows = .Rows + 1
                // lngRow = .Rows - 1
                // .TextMatrix(lngRow, CNSTGRIDCOLNAME) = clsLoad.Fields("category")
                // .RowOutlineLevel(lngRow) = 0
                // .IsSubTotal(lngRow) = True
                // .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 0, lngRow, .Cols - 1) = TRIOCOLORHIGHLIGHT
                // strCategory = clsLoad.Fields("category")
                // End If
                // .Rows = .Rows + 1
                // .TextMatrix(.Rows - 1, CNSTGRIDCOLCODE) = clsLoad.Fields("code")
                // .TextMatrix(.Rows - 1, CNSTGRIDCOLNAME) = clsLoad.Fields("Name")
                // .TextMatrix(.Rows - 1, CNSTGRIDCOLTAG) = clsLoad.Fields("Tag")
                // .TextMatrix(.Rows - 1, CNSTGRIDCOLHELP) = clsLoad.Fields("description")
                // 
                // clsLoad.MoveNext
                // Loop
                modColorScheme.ColorGrid(Grid);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadReport()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsLoad = new clsDRWrapper();
				rtbData.Text = "";
				if (lngCurrentID == 0)
				{
					return;
				}
				clsLoad.OpenRecordset("select * from dynamicreports where ID = " + FCConvert.ToString(lngCurrentID), strModuleDB);
				if (!clsLoad.EndOfFile())
				{
					rtbData.Text = FCConvert.ToString(clsLoad.Get_Fields_String("text"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In LoadReport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveReport()
		{
			bool SaveReport = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper clsSave = new clsDRWrapper();
				object strTitle;
				object strDescription;
				string strSQL;
				SaveReport = false;
				strTitle = "";
				strDescription = "";
				if (lngCurrentID == 0)
				{
					// get info
					if (frmInput.InstancePtr.Init(ref strTitle, "Enter a report title", "Title", 4320, false, modGlobalConstants.InputDTypes.idtString))
					{
						if (!frmInput.InstancePtr.Init(ref strDescription, "Enter a report description", "Description", 4320, false, modGlobalConstants.InputDTypes.idtString))
						{
							return SaveReport;
						}
					}
					else
					{
						return SaveReport;
					}
				}
				strSQL = "Select * from DynamicReports where ID = " + FCConvert.ToString(lngCurrentID);
				clsSave.OpenRecordset(strSQL, strModuleDB);
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
				}
				if (FCConvert.ToString(strTitle) != string.Empty)
				{
					clsSave.Set_Fields("Title", strTitle);
				}
				if (FCConvert.ToString(strDescription) != string.Empty)
				{
					clsSave.Set_Fields("Description", strDescription);
				}
				clsSave.Set_Fields("text", rtbData.Text);
				clsSave.Set_Fields("reporttype", intReportType);
				clsSave.Update();
				lngCurrentID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				MessageBox.Show("Save complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveReport = true;
				return SaveReport;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveReport", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveReport;
		}
	}
}
