﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class modCEDynamicDocument
	{
		//=========================================================
		public static string DynamicDocumentTextByCode(ref int lngCode, int intReportType)
		{
			string DynamicDocumentTextByCode = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strReturn;
				DynamicDocumentTextByCode = "";
				strReturn = "";
				//switch (intReportType) {
				//	case modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL:
				//	{
				//		switch (lngCode) {
				//				break;
				//			}
				//		} //end switch
				//		break;
				//	}
				//} //end switch
				DynamicDocumentTextByCode = strReturn;
				return DynamicDocumentTextByCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In DynamicDocumentTextByCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DynamicDocumentTextByCode;
		}
	}
}
