﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptInspectorsList.
	/// </summary>
	public partial class rptInspectorsList : BaseSectionReport
	{
		public rptInspectorsList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Inspectors";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptInspectorsList InstancePtr
		{
			get
			{
				return (rptInspectorsList)Sys.GetInstance(typeof(rptInspectorsList));
			}
		}

		protected rptInspectorsList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptInspectorsList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLNAME = 1;
		const int CNSTGRIDCOLSHORTNAME = 2;
		clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref clsSQLStatement clsStatement)
		{
			string strSQL = "";
			if (!(clsStatement == null))
			{
				strSQL = "select distinct inspectorname, inspectorshortname from (" + clsStatement.SelectStatement + " " + clsStatement.WhereStatement + " " + clsStatement.OrderByStatement + ") abc";
				// strSQL = clsStatement.SQLStatement
				// If Not clsStatement.WhereStatement = vbNullString Then
				// strSQL = "select * from inspectors inner join inspections on (inspections.inspectorid = inspectors.ID) " & clsStatement.WhereStatement
				// 
				// Else
				// strSQL = "select * from inspectors "
				// End If
				// If clsStatement.OrderByStatement <> vbNullString Then
				// strSQL = strSQL & " " & clsStatement.OrderByStatement
				// Else
				// strSQL = strSQL & " order by inspectors.name"
				// End If
			}
			else
			{
				strSQL = "select * from inspectors order by name";
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			txtRange.Text = clsStatement.ParameterDescriptionText;
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Inspectors");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsReport.EndOfFile())
			{
				txtName.Text = rsReport.Get_Fields_String("inspectorname");
				txtShort.Text = rsReport.Get_Fields_String("inspectorshortname");
				rsReport.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + PageNumber;
		}

		
	}
}
