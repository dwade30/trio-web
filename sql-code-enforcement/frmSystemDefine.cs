//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public partial class frmSystemDefined : BaseForm
	{
		public frmSystemDefined()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmSystemDefined InstancePtr
		{
			get
			{
				return (frmSystemDefined)Sys.GetInstance(typeof(frmSystemDefined));
			}
		}

		protected frmSystemDefined _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By     Corey
		// Date           03/30/2005
		// add or delete inspectors
		// ********************************************************
		const int CNSTGRIDCOLAUTOID = 0;
		const int CNSTGRIDCOLCODE = 1;
		const int CNSTGRIDCOLDESC = 2;
		const int CNSTGRIDCOLSHORTDESC = 3;
		const int CNSTGRIDCOLCODETYPE = 4;
		const int CNSTGRIDCOLBOOLOPTION = 5;
		const int CNSTGRIDCOLFORMTYPE = 6;
		// Private Const CNSTGRIDSUBCOLAUTOID = 0
		// Private Const CNSTGRIDSUBCOLCODE = 1
		// Private Const CNSTGRIDSUBCOLDESC = 2
		// Private Const CNSTGRIDSUBCOLSHORTDESC = 3
		// Private Const CNSTGRIDSUBCOLCODETYPE = 4
		// Private Const CNSTGRIDSUBCOLPARENTCODETYPE = 5
		// Private Const CNSTGRIDSUBCOLPARENTCODE = 6
		// Private Const CNSTGRIDSUBCOLPARENTID = 7
		bool boolEdited;
		bool boolFormLoaded;
		int lngNextAutoID;
		private bool boolCantEdit;

		private void FillCmbCodeType()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				cmbCodeType.Clear();
				cmbCodeType.AddItem("Activity Type");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEACTIVITYTYPE);
				cmbCodeType.AddItem("Contractor Class");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPECONTRACTORCLASS);
				cmbCodeType.AddItem("Contractor Status");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPECONTRACTORSTATUS);
				cmbCodeType.AddItem("Inspection Status");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEINSPECTIONSTATUS);
				cmbCodeType.AddItem("Inspection Type");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEINSPECTIONTYPE);
				// .AddItem ("Inspection Violations")
				// .ItemData(.NewIndex) = CNSTCODETYPEINSPECTIONVIOLATIONS
				cmbCodeType.AddItem("Permit Approval Status");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEAPPROVALDENIAL);
				cmbCodeType.AddItem("Permit Type");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEPERMITTYPE);
				cmbCodeType.AddItem("Permit Categories");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEPERMITCATEGORIES);
				// .AddItem ("Permit Sub Type")
				// .ItemData(.NewIndex) = CNSTCODETYPEPERMITSUBTYPE
				cmbCodeType.AddItem("Violation Status");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEVIOLATIONSTATUS);
				cmbCodeType.AddItem("Violation Type");
				cmbCodeType.ItemData(cmbCodeType.NewIndex, modCEConstants.CNSTCODETYPEVIOLATIONTYPE);
				cmbCodeType.SelectedIndex = 0;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In FillCmbCodeType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowCodeType(int lngCodeType)
		{
			int lngRow;
			int lngPickRow;
			// Select Case lngCodeType
			// Case CNSTCODETYPEPERMITSUBTYPE
			// GridSub.Visible = True
			// lblType.Visible = True
			// lblSubType.Visible = True
			// Grid.Left = 30
			// Grid.Editable = flexEDNone
			// lblType.Left = Grid.Left
			// lblType.Width = Grid.Width
			// lblSubType.Left = GridSub.Left
			// lblSubType.Width = GridSub.Width
			// lngPickRow = 0
			// For lngRow = 1 To Grid.Rows - 1
			// If Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) = CNSTCODETYPEPERMITTYPE Then
			// Grid.RowHidden(lngRow) = False
			// If lngPickRow = 0 Then
			// lngPickRow = lngRow
			// End If
			// Else
			// Grid.RowHidden(lngRow) = True
			// End If
			// Next lngRow
			// Grid.Row = lngPickRow
			// Case Else
			if (!boolCantEdit)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			// GridSub.Visible = False
			lblType.Visible = false;
			lblSubType.Visible = false;
			if (boolFormLoaded)
			{
				//Grid.Left = FCConvert.ToInt32((this.WidthOriginal - Grid.WidthOriginal) / 2.0);
			}
			for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
			{
				if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)) == lngCodeType)
				{
					Grid.RowHidden(lngRow, false);
				}
				else
				{
					Grid.RowHidden(lngRow, true);
				}
			}
			// lngRow
			ResizeGrid();
			// End Select
		}
		// Private Sub ShowSubType(lngParentID As Long)
		// Dim lngRow As Long
		// Dim lngParentRow As Long
		//
		// For lngRow = 1 To GridSub.Rows - 1
		// If Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTID)) = lngParentID Then
		// GridSub.RowHidden(lngRow) = False
		// Else
		// GridSub.RowHidden(lngRow) = True
		// End If
		// Next lngRow
		// End Sub
		private void cmbCodeType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbCodeType.SelectedIndex >= 0)
			{
				ShowCodeType(cmbCodeType.ItemData(cmbCodeType.SelectedIndex));
			}
		}

		private void frmSystemDefined_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmSystemDefined_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSystemDefined properties;
			//frmSystemDefined.FillStyle	= 0;
			//frmSystemDefined.ScaleWidth	= 9300;
			//frmSystemDefined.ScaleHeight	= 7890;
			//frmSystemDefined.LinkTopic	= "Form2";
			//frmSystemDefined.LockControls	= -1  'True;
			//frmSystemDefined.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			boolFormLoaded = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (!modSecurity.ValidPermissions_6(this, modCEConstants.CNSTEDITDESCRIPTIONSECURITY, false))
			{
				boolCantEdit = true;
				cmdAddCode.Enabled = false;
				cmdDeleteCode.Enabled = false;
				cmdSave.Enabled = false;
				//mnuSaveExit.Enabled = false;
			}
			SetupGrid();
			// SetupGridSub
			lngNextAutoID = -1;
			LoadGrid();
			FillCmbCodeType();
			boolEdited = false;
			boolFormLoaded = true;
			ResizeGrid();
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.Cols = 7;
			Grid.ColHidden(CNSTGRIDCOLAUTOID, true);
			Grid.ColHidden(CNSTGRIDCOLCODETYPE, true);
			Grid.TextMatrix(0, CNSTGRIDCOLCODE, "No.");
			Grid.TextMatrix(0, CNSTGRIDCOLDESC, "Description");
			Grid.TextMatrix(0, CNSTGRIDCOLSHORTDESC, "Short Desc");
			Grid.TextMatrix(0, CNSTGRIDCOLBOOLOPTION, "No Insp.");
			Grid.TextMatrix(0, CNSTGRIDCOLFORMTYPE, "Form");
			Grid.ColHidden(CNSTGRIDCOLBOOLOPTION, true);
			Grid.ColHidden(CNSTGRIDCOLFORMTYPE, true);
			Grid.ColDataType(CNSTGRIDCOLBOOLOPTION, FCGrid.DataTypeSettings.flexDTBoolean);
			//Grid.FixedAlignment(CNSTGRIDCOLBOOLOPTION, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			if (boolCantEdit)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			string strList = "";
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select * from custombills order by formatname", modGlobalVariables.Statics.strCEDatabase);
			strList = "#0;None";
			while (!rsLoad.EndOfFile())
			{
				strList += "|" + "#" + rsLoad.Get_Fields_Int32("ID") + ";" + rsLoad.Get_Fields_String("formatname");
				rsLoad.MoveNext();
			}
			Grid.ColComboList(CNSTGRIDCOLFORMTYPE, strList);
		}
		// Private Sub SetupGridSub()
		// With GridSub
		// .Rows = 1
		// .Cols = 8
		// .ColHidden(CNSTGRIDSUBCOLAUTOID) = True
		// .ColHidden(CNSTGRIDSUBCOLCODETYPE) = True
		// .ColHidden(CNSTGRIDSUBCOLPARENTCODE) = True
		// .ColHidden(CNSTGRIDSUBCOLPARENTCODETYPE) = True
		// .ColHidden(CNSTGRIDSUBCOLPARENTID) = True
		// .TextMatrix(0, CNSTGRIDSUBCOLCODE) = "No."
		// .TextMatrix(0, CNSTGRIDSUBCOLDESC) = "Description"
		// .TextMatrix(0, CNSTGRIDSUBCOLSHORTDESC) = "Short Desc"
		// End With
		// End Sub
		// Private Sub ResizeGridSub()
		// Dim GridWidth As Long
		// With GridSub
		// GridWidth = .Width
		// .ColWidth(CNSTGRIDSUBCOLCODE) = 0.09 * GridWidth
		// .ColWidth(CNSTGRIDSUBCOLDESC) = 0.65 * GridWidth
		// End With
		// End Sub
		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLCODE, FCConvert.ToInt32(0.07 * GridWidth));
			if (cmbCodeType.SelectedIndex >= 0)
			{
				switch (cmbCodeType.ItemData(cmbCodeType.SelectedIndex))
				{
					case modCEConstants.CNSTCODETYPEPERMITTYPE:
						{
							Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(0.37 * GridWidth));
							Grid.ColWidth(CNSTGRIDCOLSHORTDESC, FCConvert.ToInt32(0.16 * GridWidth));
							Grid.ColHidden(CNSTGRIDCOLBOOLOPTION, false);
							Grid.ColHidden(CNSTGRIDCOLFORMTYPE, false);
							Grid.ColWidth(CNSTGRIDCOLBOOLOPTION, FCConvert.ToInt32(0.1 * GridWidth));
							break;
						}
					case modCEConstants.CNSTCODETYPEACTIVITYTYPE:
					case modCEConstants.CNSTCODETYPEVIOLATIONTYPE:
					case modCEConstants.CNSTCODETYPEINSPECTIONTYPE:
						{
							Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(0.45 * GridWidth));
							Grid.ColHidden(CNSTGRIDCOLBOOLOPTION, true);
							Grid.ColHidden(CNSTGRIDCOLFORMTYPE, false);
							Grid.ColWidth(CNSTGRIDCOLSHORTDESC, FCConvert.ToInt32(0.16 * GridWidth));
							break;
						}
					default:
						{
							Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(0.65 * GridWidth));
							Grid.ColHidden(CNSTGRIDCOLBOOLOPTION, true);
							Grid.ColHidden(CNSTGRIDCOLFORMTYPE, true);
							// .ColWidth(CNSTGRIDCOLSHORTDESC) = 0.25 * GridWidth
							break;
						}
				}
				//end switch
			}
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolEdited && !boolCantEdit)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save changes?", "Save Changes?", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
				{
					if (!SaveInfo())
					{
						e.Cancel = true;
						return;
					}
				}
			}
		}

		private void frmSystemDefined_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
			// ResizeGridSub
		}

		private void LoadGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			Grid.Rows = 1;
			// GridSub.Rows = 1
			GridDeleteSub.Rows = 0;
			GridDelete.Rows = 0;
			clsLoad.OpenRecordset("select * from SystemCodes where editable = 1 and parentid = 0 order by codetype,Code", modGlobalVariables.Statics.strCEDatabase);
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.RowData(lngRow, false);
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTDESC, FCConvert.ToString(clsLoad.Get_Fields("shortdescription")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("code"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("codetype"))));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLBOOLOPTION, FCConvert.ToString(clsLoad.Get_Fields_Boolean("booloption")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLFORMTYPE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("DocumentID"))));
				clsLoad.MoveNext();
			}
			// Call clsLoad.OpenRecordset("select * from systemcodes where editable and parentid <> 0 order by parentcodetype,parentid,code", strCEDatabase)
			// Do While Not clsLoad.EndOfFile
			// GridSub.Rows = GridSub.Rows + 1
			// lngRow = GridSub.Rows - 1
			// GridSub.RowData(lngRow) = False
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLDESC) = clsLoad.Fields("description")
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLSHORTDESC) = clsLoad.Fields("shortdescription")
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLAUTOID) = clsLoad.Fields("ID")
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODE) = Val(clsLoad.Fields("code"))
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODETYPE) = Val(clsLoad.Fields("codetype"))
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODE) = Val(clsLoad.Fields("parentcode"))
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODETYPE) = Val(clsLoad.Fields("parentcodetype"))
			// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTID) = Val(clsLoad.Fields("Parentid"))
			// GridSub.RowHidden(lngRow) = True
			// clsLoad.MoveNext
			// Loop
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (Grid.Row > 0)
			{
				Grid.RowData(Grid.Row, true);
				boolEdited = true;
			}
		}

		private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (boolCantEdit)
				return;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						DeleteCode(Grid.Row);
						break;
					}
				case Keys.Insert:
					{
						AddCode();
						break;
					}
			}
			//end switch
		}

		private void Grid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (boolCantEdit)
				return;
			switch (e.KeyCode)
			{
				case Keys.Insert:
					{
						AddCode();
						break;
					}
			}
			//end switch
		}

		private void Grid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int mRow;
			mRow = Grid.MouseRow;
			if (mRow < 0)
			{
				if (Grid.Row < 1)
				{
					Grid.Row = 0;
				}
				Grid.Focus();
			}
		}
		// Private Sub Grid_RowColChange()
		// If Grid.Row > 0 Then
		// ShowSubType (Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLAUTOID)))
		// End If
		// End Sub
		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:MSH - save and use correct indexes of the cell
            int row = Grid.GetFlexRowIndex(e.RowIndex);
            int col = Grid.GetFlexColIndex(e.ColumnIndex);

            if (row > 0)
			{
				Grid.RowData(row, true);
				boolEdited = true;
			}
		}
		// Private Sub GridSub_AfterEdit(ByVal Row As Long, ByVal Col As Long)
		// If Row > 0 Then
		// Grid.RowData(Row) = True
		// boolEdited = True
		// End If
		// End Sub
		//
		// Private Sub GridSub_KeyDown(KeyCode As Integer, Shift As Integer)
		// Select Case KeyCode
		// Case vbKeyDelete
		// KeyCode = 0
		// DeleteSubCode (GridSub.Row)
		// Case vbKeyInsert
		// AddCode
		// End Select
		// End Sub
		//
		// Private Sub GridSub_ValidateEdit(ByVal Row As Long, ByVal Col As Long, Cancel As Boolean)
		// If Row > 0 Then
		// GridSub.RowData(Row) = True
		// boolEdited = False
		// End If
		// End Sub
		private void mnuAddCode_Click(object sender, System.EventArgs e)
		{
			AddCode();
		}

		private void AddCode(int lngRow = -1)
		{
			boolEdited = true;
			// If GridSub.Visible Then
			// AddSubCode
			// Exit Sub
			// End If
			if (lngRow <= 0)
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
			}
			else
			{
				Grid.AddItem(FCConvert.ToString(0), lngRow);
			}
			lngNextAutoID -= 1;
			Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(lngNextAutoID));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE, FCConvert.ToString(cmbCodeType.ItemData(cmbCodeType.SelectedIndex)));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLBOOLOPTION, FCConvert.ToString(false));
			Grid.TextMatrix(lngRow, CNSTGRIDCOLFORMTYPE, FCConvert.ToString(0));
			Grid.RowData(lngRow, true);
			Grid.TopRow = lngRow;
			Grid.Row = lngRow;
			Grid.Col = CNSTGRIDCOLCODE;
		}
		// Private Sub AddSubCode()
		// Dim lngRow As Long
		// boolEdited = True
		// If Grid.Row < 1 Then
		// MsgBox "Cannot add a sub code without picking a parent code", vbExclamation, "No Code"
		// Exit Sub
		// End If
		// If Grid.RowHidden(Grid.Row) Then
		// probably can't happen
		// Exit Sub
		// End If
		// GridSub.Rows = GridSub.Rows + 1
		// lngRow = GridSub.Rows - 1
		// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLAUTOID) = 0
		// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODETYPE) = Val(cmbCodeType.ItemData(cmbCodeType.ListIndex))
		// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODETYPE) = Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCODETYPE))
		// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTID) = Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLAUTOID))
		// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODE) = Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLCODE))
		// GridSub.RowData(lngRow) = True
		// GridSub.TopRow = lngRow
		// End Sub
		private void mnuDeleteCode_Click(object sender, System.EventArgs e)
		{
			// If Not GridSub.Visible Then
			DeleteCode(Grid.Row);
			// Else
			// DeleteSubCode (GridSub.Row)
			// End If
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Grid.Row = 0;
			//App.DoEvents();
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void DeleteCode(int lngRow)
		{
			int x;
			int lngAutoID;
			if (lngRow < 1)
			{
				MessageBox.Show("You must select a code first", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) > 0)
			{
				boolEdited = true;
				GridDelete.Rows += 1;
				GridDelete.TextMatrix(GridDelete.Rows - 1, 0, Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID));
			}
			lngAutoID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))));
			Grid.RemoveItem(lngRow);
			// For x = GridSub.Rows - 1 To 1 Step -1
			// If lngAutoID = Val(GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTID)) Then
			// DeleteSubCode (x)
			// End If
			// Next x
		}
		// Private Sub DeleteSubCode(lngRow As Long)
		// If lngRow < 1 Then
		// MsgBox "You must select a sub code first", vbExclamation, "No Selection"
		// Exit Sub
		// End If
		//
		// If Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLAUTOID)) > 0 Then
		// boolEdited = True
		// GridDeleteSub.Rows = GridDeleteSub.Rows + 1
		// GridDeleteSub.TextMatrix(GridDeleteSub.Rows - 1, 0) = GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLAUTOID)
		// End If
		// GridSub.RemoveItem (lngRow)
		// End Sub
		private bool SaveInfo()
		{
			bool SaveInfo = false;
			// first delete all of the deleted ones
			// then save all of the changed ones
			// vbPorter upgrade warning: x As int	OnWriteFCConvert.ToInt32(
			int x;
			clsDRWrapper clsSave = new clsDRWrapper();
			int lngRow;
			string strTemp = "";
			string strCategory = "";
			int lngLastCategory;
			int lngLastOrder;
			int lngLastCode;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				Grid.Row = 0;
				//App.DoEvents();
				SaveInfo = false;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				for (x = 0; x <= (GridDelete.Rows - 1); x++)
				{
					clsSave.Execute("delete from systemcodes where ID = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(x, 0))), modGlobalVariables.Statics.strCEDatabase);
				}
				// x
				GridDelete.Rows = 0;
				for (x = 0; x <= (GridDeleteSub.Rows - 1); x++)
				{
					clsSave.Execute("delete from systemcodes where ID = " + FCConvert.ToString(Conversion.Val(GridDeleteSub.TextMatrix(x, 0))), modGlobalVariables.Statics.strCEDatabase);
				}
				// x
				GridDeleteSub.Rows = 0;
				Grid.Col = CNSTGRIDCOLCODETYPE;
				Grid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				Grid.Col = CNSTGRIDCOLCODE;
				Grid.Sort = FCGrid.SortSettings.flexSortNumericAscending;
				lngLastCategory = -1;
				lngLastOrder = -1;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (lngLastCategory != Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE)))
					{
						lngLastCategory = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE))));
						lngLastOrder = -1;
					}
					strCategory = "";
					switch (lngLastCategory)
					{
						case modCEConstants.CNSTCODETYPECONTRACTORCLASS:
							{
								strCategory = "Contractor Class";
								break;
							}
						case modCEConstants.CNSTCODETYPECONTRACTORSTATUS:
							{
								strCategory = "Contractor Status";
								// Case CNSTCODETYPEPERMITSUBTYPE
								// strCategory = "Permit Sub Type"
								break;
							}
						case modCEConstants.CNSTCODETYPEPERMITTYPE:
							{
								strCategory = "Permit Type";
								break;
							}
						case modCEConstants.CNSTCODETYPEAPPROVALDENIAL:
							{
								strCategory = "Permit Status";
								break;
							}
						case modCEConstants.CNSTCODETYPEINSPECTIONTYPE:
							{
								strCategory = "Inspection Type";
								break;
							}
						case modCEConstants.CNSTCODETYPEINSPECTIONSTATUS:
							{
								strCategory = "Inspection Status";
								break;
							}
						case modCEConstants.CNSTCODETYPEINSPECTIONVIOLATIONS:
							{
								strCategory = "Inspection Violations";
								break;
							}
					}
					//end switch
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE)) <= 0)
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("Codes cannot have an order number < 1" + "\r\n" + strCategory + " code " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC), "Bad Order Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveInfo;
					}
					if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE)) == lngLastOrder)
					{
						MessageBox.Show("Codes cannot have duplicate order numbers within the same code type" + "\r\n" + strCategory + " code " + Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC) + " has a duplicate order number", "Bad Order Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						return SaveInfo;
					}
					lngLastOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
					clsSave.OpenRecordset("select * from systemcodes where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID))), modGlobalVariables.Statics.strCEDatabase);
					if (FCConvert.ToDouble(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) > 0)
					{
						if (!clsSave.EndOfFile())
						{
							clsSave.Edit();
							// For x = 1 To GridSub.Rows - 1
							// If Val(GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTID)) = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) Then
							// GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTCODE) = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))
							// GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTCODETYPE) = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE))
							// End If
							// Next x
						}
						else
						{
							clsSave.AddNew();
							// For x = 1 To GridSub.Rows - 1
							// If Val(GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTID)) = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)) Then
							// GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTID) = clsSave.Fields("ID")
							// GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTCODE) = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))
							// GridSub.TextMatrix(x, CNSTGRIDSUBCOLPARENTCODETYPE) = Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE))
							// End If
							// Next x
						}
					}
					else
					{
						clsSave.AddNew();
					}
					// Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID) = clsSave.Fields("ID")
					clsSave.Set_Fields("description", Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC));
					clsSave.Set_Fields("shortdescription", Grid.TextMatrix(lngRow, CNSTGRIDCOLSHORTDESC));
					clsSave.Set_Fields("codetype", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODETYPE))));
					clsSave.Set_Fields("code", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLCODE))));
					clsSave.Set_Fields("editable", true);
					clsSave.Set_Fields("parentid", 0);
					clsSave.Set_Fields("parentcodetype", 0);
					clsSave.Set_Fields("parentcode", 0);
					clsSave.Set_Fields("booloption", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLBOOLOPTION)));
					clsSave.Set_Fields("DocumentID", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLFORMTYPE))));
					clsSave.Update();
					Grid.TextMatrix(lngRow, CNSTGRIDCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
					Grid.RowData(lngRow, false);
				}
				// lngRow
				// 
				// GridSub.Col = CNSTGRIDSUBCOLPARENTID
				// GridSub.Sort = flexSortNumericAscending
				// GridSub.Col = CNSTGRIDSUBCOLCODE
				// GridSub.Sort = flexSortNumericAscending
				lngLastCategory = -1;
				lngLastOrder = -1;
				lngLastCode = -1;
				// For lngRow = 1 To GridSub.Rows - 1
				// If lngLastCode <> Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTID)) Then
				// lngLastCode = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTID))
				// lngLastOrder = -1
				// End If
				// lngLastCategory = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODETYPE))
				// strCategory = ""
				// Select Case lngLastCategory
				// Case CNSTCODETYPECONTRACTORCLASS
				// strCategory = "Contractor Class"
				// Case CNSTCODETYPECONTRACTORSTATUS
				// strCategory = "Contractor Status"
				// Case CNSTCODETYPEPERMITSUBTYPE
				// strCategory = "Permit Sub Type"
				// Case CNSTCODETYPEPERMITTYPE
				// strCategory = "Permit Type"
				// Case CNSTCODETYPEAPPROVALDENIAL
				// strCategory = "Permit Approval Status"
				// Case CNSTCODETYPEINSPECTIONTYPE
				// strCategory = "Inspection Type"
				// Case CNSTCODETYPEINSPECTIONSTATUS
				// strCategory = "Inspection Status"
				// Case CNSTCODETYPEINSPECTIONVIOLATIONS
				// strCategory = "Inspection Violations"
				// End Select
				// If Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODE)) <= 0 Then
				// Screen.MousePointer = vbDefault
				// MsgBox "Sub codes cannot have an order number < 1" & vbNewLine & strCategory & " Code " & GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODE) & " sub code " & GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLDESC), vbExclamation, "Bad Order Number"
				// Exit Function
				// End If
				// If Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODE)) = lngLastOrder Then
				// MsgBox "Sub codes cannot have duplicate order numbers within the same parent code" & vbNewLine & strCategory & " Code " & GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODE) & " sub code " & GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLDESC) & " has a duplicate order number", vbExclamation, "Bad Order Number"
				// Screen.MousePointer = vbDefault
				// Exit Function
				// End If
				// lngLastOrder = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODE))
				// Call clsSave.OpenRecordset("select * from systemcodes where ID = " & Val(GridSub.TextMatrix(lngRow, CNSTGRIDCOLAUTOID)), strCEDatabase)
				// If Not clsSave.EndOfFile Then
				// clsSave.Edit
				// Else
				// clsSave.AddNew
				// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLAUTOID) = clsSave.Fields("ID")
				// End If
				// GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLAUTOID) = clsSave.Fields("ID")
				// clsSave.Fields("description") = GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLDESC)
				// clsSave.Fields("shortdescription") = GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLSHORTDESC)
				// clsSave.Fields("codetype") = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODETYPE))
				// clsSave.Fields("code") = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLCODE))
				// clsSave.Fields("parentid") = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTID))
				// clsSave.Fields("Parentcode") = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODE))
				// clsSave.Fields("Parentcodetype") = Val(GridSub.TextMatrix(lngRow, CNSTGRIDSUBCOLPARENTCODETYPE))
				// clsSave.Fields("editable") = True
				// clsSave.Update
				// GridSub.RowData(lngRow) = False
				// Next lngRow
				// 
				boolEdited = false;
				SaveInfo = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			rptSystemDefined.InstancePtr.Init(-1);
		}

		private void mnuPrintCurrent_Click(object sender, System.EventArgs e)
		{
			rptSystemDefined.InstancePtr.Init(cmbCodeType.ItemData(cmbCodeType.SelectedIndex));
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo())
			{
				mnuExit_Click();
			}
		}
	}
}
