﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptEnvelope10.
	/// </summary>
	public partial class rptEnvelope10 : BaseSectionReport
	{
		public rptEnvelope10()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Name = "Envelope";
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptEnvelope10 InstancePtr
		{
			get
			{
				return (rptEnvelope10)Sys.GetInstance(typeof(rptEnvelope10));
			}
		}

		protected rptEnvelope10 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsReport.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEnvelope10	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private bool boolShowReturnAddress;
		private clsDRWrapper rsReport = new clsDRWrapper();

		public void Init(ref clsSQLStatement clsStatement, bool boolReturnAddress, int lngEnvelopeType)
		{
			string strSQL = "";
			string strWhere;
			string strCEMasterJoin;
			strCEMasterJoin = modCE.GetCEMasterJoinForJoin();
			strWhere = " not isnull(mj.deleted,0) = 1";
			boolShowReturnAddress = boolReturnAddress;
			if (!(clsStatement == null))
			{
				// strSQL = clsStatement.SQLStatement
				if (clsStatement.WhereStatement != "")
				{
					strWhere = clsStatement.WhereStatement + " and " + strWhere;
				}
				else
				{
					strWhere = " where " + strWhere;
				}
				strSQL = "select distinct last, first, middle, desig,address1,address2,city,state,zip,zip4  from (" + clsStatement.SelectStatement + " " + strWhere + " " + clsStatement.OrderByStatement + ") abc order by last,middle,first ";
			}
			else
			{
				// strSQL = "select distinct last, first, middle, desig,address1,address2,city,state,zip,zip4  from  cemaster where not deleted = 1 order by last,middle,first"
				strSQL = "select distinct last, first, middle, desig,address1,address2,city,state,zip,zip4  from  " + strCEMasterJoin + " where not deleted = 1 order by last,middle,first";
			}
			rsReport.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
			if (rsReport.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			//PageSettings.PaperSize = 256;
			switch (lngEnvelopeType)
			{
				case modCEConstants.CNSTEnvelope10:
					{
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = 4.13F;
						PageSettings.PaperWidth = 9.5F;
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						Detail.KeepTogether = true;
						break;
					}
				case modCEConstants.CNSTEnvelopeB5:
					{
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = 6.93F;
						PageSettings.PaperWidth = 9.84F;
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						Detail.KeepTogether = true;
						break;
					}
				case modCEConstants.CNSTEnvelopeC5:
					{
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = 6.38F;
						PageSettings.PaperWidth = 9.02F;
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						Detail.KeepTogether = true;
						break;
					}
				case modCEConstants.CNSTEnvelopeDL:
					{
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = 4.33F;
						PageSettings.PaperWidth = 8.66F;
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						Detail.KeepTogether = true;
						break;
					}
				case modCEConstants.CNSTEnvelopeMonarch:
					{
						PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
						;
						Detail.ColumnCount = 1;
						PageSettings.PaperHeight = 3.88F;
						PageSettings.PaperWidth = 7.5F;
						Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
						Detail.KeepTogether = true;
						break;
					}
			}
			//end switch
			int PrintWidth = FCConvert.ToInt32(PageSettings.PaperWidth - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right - 1);
			Detail.Height = PageSettings.PaperHeight - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 1;
			if (txtMailingAddress.Left + txtMailingAddress.Width > PrintWidth)
			{
				txtMailingAddress.Width = FCConvert.ToInt32(PrintWidth) - txtMailingAddress.Left - 10 / 1440F;
			}
			frmReportViewer.InstancePtr.Init(this, boolAllowEmail: true, strAttachmentName: "Envelope");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsReport.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			if (!boolShowReturnAddress)
			{
				txtReturnAddress.Visible = false;
			}
			else
			{
				txtReturnAddress.Visible = true;
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strTemp = "";
				string strTemp2 = "";
				int intTemp;
				strTemp = "";
				rsTemp.OpenRecordset("select * from customize", modGlobalVariables.Statics.strCEDatabase);
				if (!rsTemp.EndOfFile())
				{
					strTemp2 = "";
					for (intTemp = 1; intTemp <= 4; intTemp++)
					{
						if (fecherFoundation.Strings.Trim(rsTemp.Get_Fields_String("returnAddress" + FCConvert.ToString(intTemp))) != string.Empty)
						{
							strTemp += strTemp2 + rsTemp.Get_Fields_String("returnaddress" + FCConvert.ToString(intTemp));
							strTemp2 = "\r\n";
						}
					}
					// intTemp
				}
				rsTemp.Dispose();
				txtReturnAddress.Text = strTemp;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			if (!rsReport.EndOfFile())
			{
				strTemp = "";
				strTemp = fecherFoundation.Strings.Trim(fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("First") + " " + rsReport.Get_Fields_String("Last")) + " " + rsReport.Get_Fields_String("Desig")) + "\r\n";
				if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("address1")) != string.Empty)
					strTemp += fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("address1")) + "\r\n";
				if (fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("address2")) != string.Empty)
					strTemp += fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("address2")) + "\r\n";
				strTemp += fecherFoundation.Strings.Trim(rsReport.Get_Fields_String("city") + " " + rsReport.Get_Fields_String("state") + " " + rsReport.Get_Fields_String("zip") + " " + rsReport.Get_Fields_String("zip4")) + "\r\n";
				txtMailingAddress.Text = strTemp;
				rsReport.MoveNext();
			}
		}

		
	}
}
