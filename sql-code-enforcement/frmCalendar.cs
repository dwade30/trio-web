//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using Wisej.Web.Ext.FullCalendar;
using System.Linq;

namespace TWCE0000
{
	public partial class frmCalendar : BaseForm
	{
		public frmCalendar()
		{
			//
			// required for windows form designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            //TODO - move commented code from designer.cs
            //this.CalendarControl._StockProps = 64;
            //this.CalendarControl.ViewType = 3;
            //this.CalendarControl.VisualTheme = 1;
            //this.CalendarControl.ShowCaptionBar = -1;//  'True;
            //this.CalendarControl.ShowTimelineButton = 0;//   'False;
            //this.CalendarControl.ShowMultiColumnsButton = 0;//   'False;
            //this.CalendarControl.ContextMenuEvent += new AxXtremeCalendarControl._DCalendarControlEvents_ContextMenuEventHandler(this.CalendarControl_ContextMenuEvent);
            //this.CalendarControl.DoubleClick += new System.EventHandler(this.CalendarControl_DblClick);
            //this.CalendarControl.GetItemText += new AxXtremeCalendarControl._DCalendarControlEvents_GetItemTextEventHandler(this.CalendarControl_GetItemText);
            //this.CalendarControl.OnReminders += new AxXtremeCalendarControl._DCalendarControlEvents_OnRemindersEventHandler(this.CalendarControl_OnReminders);
            //
            // CommandBarsFrame
            //
            //this.CommandBarsFrame.Name = "CommandBarsFrame";
            //this.CommandBarsFrame.TabIndex = 1;
            //this.CommandBarsFrame.Location = new System.Drawing.Point(0, 24);
            //this.CommandBarsFrame.Size = new System.Drawing.Size(627, 21);
            //this.CommandBarsFrame._StockProps = 0;
            //this.CommandBarsFrame.VisualTheme = 3;
            //this.CommandBarsFrame.Execute += new AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEventHandler(this.CommandBarsFrame_Execute);

            //
            // todo: add any constructor code after initializecomponent call
            //
            if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmCalendar InstancePtr
		{
			get
			{
				return (frmCalendar)Sys.GetInstance(typeof(frmCalendar));
			}
		}

		protected frmCalendar _InstancePtr = null;
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		//CalendarEvent ContextEvent = new CalendarEvent();
		public providerAccessCalendar m_pCustomDataHandler;
		private int ModalFormsRunningCounter;
		public bool DisableDragging_ForRecurrenceEvents;
		public bool DisableInPlaceCreateEvents_ForSaSu;
		public bool EnableScrollV_DayView;
		public bool EnableScrollH_DayView;
		public bool EnableScrollV_WeekView;
		public bool EnableScrollV_MonthView;
		//public XtremeCalendarControl.CalendarResourcesManager g_DataResourcesMan = new XtremeCalendarControl.CalendarResourcesManager();
		public bool g_bUseBuiltInCalendarDialogs;
        private Event currentEvent;
		//public XtremeCalendarControl.CalendarDialogs g_dlgCalendarReminders = new XtremeCalendarControl.CalendarDialogs();
		// Dim xctl As XtremeCommandBars.ICommandBarControl
		public int ModalFormsRunning
		{
			get
			{
				int ModalFormsRunning = 0;
				ModalFormsRunning = ModalFormsRunningCounter;
				return ModalFormsRunning;
			}
		}

		public void DecrementModalFormsRunning()
		{
			ModalFormsRunningCounter -= 1;
			if (ModalFormsRunningCounter < 0)
			{
				ModalFormsRunningCounter = 0;
			}
			if (ModalFormsRunningCounter == 0)
			{
				// TimerRMDForm.Enabled = True
			}
		}

		public void IncrementModalFormsRunning()
		{
			ModalFormsRunningCounter += 1;
		}

		public void OpenProvider()
		{
			m_pCustomDataHandler = new providerAccessCalendar();
			m_pCustomDataHandler.SetCalendar(CalendarControl);
			//CalendarControl.SetDataProvider("Provider=custom");
			////CalendarControl.DataProvider.CacheMode = XtremeCalendarControl.CalendarDataProviderCacheMode.xtpCalendarDPCacheModeOnRepeat;
			//if (!CalendarControl.DataProvider.Open) {
			//	CalendarControl.DataProvider.Create();
			//}
		}
		//FC:TODO
		//private void CalendarControl_ContextMenuEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_ContextMenuEvent e)
		//{
		//	CalendarHitTestInfo HitTest = new CalendarHitTestInfo();
		//	HitTest = CalendarControl.ActiveView.HitTest;
		//	if (!(HitTest.ViewEvent==null)) {
		//		ContextEvent = HitTest.ViewEvent.Event;
		//		this.PopupMenu(mnuContextEditEvent);
		//		ContextEvent = null;
		//	} else {
		//		this.PopupMenu(mnuContextNewEvent);
		//	}
		//}
		//private void CalendarControl_DblClick(object sender, System.EventArgs e)
		//{
		//	CalendarHitTestInfo HitTest = new CalendarHitTestInfo();
		//	HitTest = CalendarControl.ActiveView.HitTest;
		//	CalendarEvents Events = new CalendarEvents();
		//	if (!(HitTest.HitCode==XtremeCalendarControl.CalendarHitTestCode.xtpCalendarHitTestUnknown)) {
		//		// Set Events = CalendarControl.DataProvider.RetrieveDayEvents(HitTest.ViewDay.Date)
		//	}
		//	if (HitTest.ViewEvent==null) {
		//		mnuNewEvent_Click();
		//	} else {
		//		ModifyEvent(ref HitTest.ViewEvent.Event);
		//	}
		//}
		//private void CalendarControl_GetItemText(object sender, AxXtremeCalendarControl._DCalendarControlEvents_GetItemTextEvent e)
		//{
		//	vbPorterVar = e.ItemParams.Item;
		//		// Case xtpCalendarItemText_EventToolTipText
		//		// x = 5
		//	if (vbPorterVar==XtremeCalendarControl.XTPCalendarGetItemText.xtpCalendarItemText_EventSubject)
		//	{
		//		if (e.ItemParams.Text!="") {
		//			if (e.ItemParams.ViewEvent.Event.CustomProperties.Property("user")!="") {
		//				e.ItemParams.Text = fecherFoundation.Strings.Trim(e.ItemParams.ViewEvent.Event.CustomProperties.Property("user")+": "+e.ItemParams.Text);
		//			} else {
		//			}
		//		}
		//	}
		//	else
		//	{
		//	}
		//}
		//private void CalendarControl_OnReminders(object sender, AxXtremeCalendarControl._DCalendarControlEvents_OnRemindersEvent e)
		//{
		//	if (g_bUseBuiltInCalendarDialogs) {
		//		g_dlgCalendarReminders.ShowRemindersWindow();
		//		return;
		//	}
		//	frmCalendarReminders.InstancePtr.OnReminders(e.action, e.reminder);
		//	if (e.action==XtremeCalendarControl.CalendarRemindersAction.xtpCalendarRemindersFire) {
		//		if (ModalFormsRunningCounter==0) {
		//			if (frmCalendarReminders.InstancePtr.ctrlReminders.Items.Count>0) {
		//				IncrementModalFormsRunning();
		//				frmCalendarReminders.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		//				DecrementModalFormsRunning();
		//			}
		//		} else {
		//			TimerRMDForm.Enabled = true;
		//		}
		//	}
		//}
		// Private Sub chkShowAll_Click()
		// If chkShowAll.Value = vbChecked Then
		// m_pCustomDataHandler.ShowAllEvents = True
		// CalendarControl.Populate
		// Else
		// m_pCustomDataHandler.ShowAllEvents = False
		// CalendarControl.Populate
		// End If
		// End Sub
		private void frmCalendar_Activated(object sender, System.EventArgs e)
		{
			if (FCConvert.ToBoolean(modMDIParent.FormExist(this)))
			{
				return;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			this.Refresh();
		}

		private void frmCalendar_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCalendar properties;
			//frmCalendar.FillStyle	= 0;
			//frmCalendar.ScaleWidth	= 9300;
			//frmCalendar.ScaleHeight	= 7425;
			//frmCalendar.LinkTopic	= "Form2";
			//frmCalendar.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			CreateFormMenuBar();
			//CommandBarsFrame.ActiveMenuBar.EnableDocking(XtremeCommandBars.XTPToolBarFlags.xtpFlagStretched);
			DisableDragging_ForRecurrenceEvents = false;
			DisableInPlaceCreateEvents_ForSaSu = false;
			EnableScrollV_DayView = true;
			EnableScrollH_DayView = true;
			EnableScrollV_WeekView = true;
			EnableScrollV_MonthView = true;
			g_bUseBuiltInCalendarDialogs = false;
			//FC:FINAL:DSE Set provider after populating the calendar (this way, events will not be saved twice in the database)
			//OpenProvider();
            // CalendarControl.VisualTheme = xtpCalendarThemeOffice2007
            // CalendarGlobalSettings.Office2007Images = CurDir & "\Themes\Office2007Black.dll"
            //FC:TODO
            CalendarControl.LabelList.Add(new CalendarLabel() { LabelID = 11, Color = Color.Red, Name = "Inspection" });
            //DateTime Today;
            //Today = DateTime.Now;
            //bool bReminders;
            //bReminders = true;
            //CalendarControl.ViewType = XtremeCalendarControl.CalendarViewType.xtpCalendarDayView;
            //CalendarControl.DayView.ShowDays(Today.ToOADate()-2, Today.ToOADate()+2);
            //CalendarControl.ViewType = XtremeCalendarControl.CalendarViewType.xtpCalendarWorkWeekView;
            //CalendarControl.Populate();
            //CalendarControl.DayView.ScrollToWorkDayBegin();
            //// bReminders = GetSetting("Codejock Calendar VB Sample", "AdvancedOptions", "RemindersManEnabled", True)
            //CalendarControl.EnableReminders(bReminders);
            //if (g_bUseBuiltInCalendarDialogs) {
            //	g_dlgCalendarReminders.Calendar = CalendarControl;
            //	g_dlgCalendarReminders.ParentHWND = this.Handle.ToInt32();
            //	g_dlgCalendarReminders.RemindersWindowShowInTaskBar = false;
            //	g_dlgCalendarReminders.CreateRemindersWindow();
            //}
            // Set g_dlgCalendarReminders.Calendar = CalendarControl
            // g_dlgCalendarReminders.ParentHwnd = Me.hwnd
            // g_dlgCalendarReminders.RemindersWindowShowInTaskBar = False
            // g_dlgCalendarReminders.CreateRemindersWindow
            // CalendarControl.DayView.TimeScaleMinTime = #4:00:00 AM#
            // CalendarControl.DayView.TimeScaleMaxTime = #11:00:00 PM#
            // With CalendarControl.MonthView
            // .DayHeaderFormatShort = "MMM dd "
            // .DayHeaderFormatMiddle = "MMM dd"
            // .DayHeaderFormatLong = "MMM dd "
            // .DayHeaderFormatShortest = "d"
            // End With
            //CalendarControl.DayView.TimeScale = 30; // if you go 15 then you can only show 8 to 5
            //CalendarControl.Options.WorkDayStartTime = "12:00:00 AM";
            //CalendarControl.Options.WorkDayEndTime = "12:00:00 AM";
            //CalendarControl.DayView.TimeScaleMinTime = "7:00:00 AM";
            //CalendarControl.DayView.TimeScaleMaxTime = "7:00:00 PM";
            //CalendarControl.MonthView.DayHeaderFormatShort = "d";
            //CalendarControl.MonthView.DayHeaderFormatMiddle = "MMM d";
            //CalendarControl.MonthView.DayHeaderFormatLong = "MMMM d";
            //CalendarControl.MonthView.DayHeaderFormatShortest = "d";
            //CalendarControl.SetLongDayHeaderFormat("ddd, dd MMMM");
            //CalendarControl.WeekView.DayHeaderFormatLong = "ddd, dd MMMM";
            // CalendarControl.ShowMultiColumnsButton = False
            // CalendarControl.ShowTimelineButton = False
            //CalendarControl.HideCaptionBar = false;
            //// mnuEnableTheme2007_Click
            //CalendarControl.ViewType = XtremeCalendarControl.CalendarViewType.xtpCalendarMonthView;
            //// CalendarControl.ViewType = xtpCalendarTimeLineView
            //CalendarControl.TimeLineView.backgroundColor = Information.RGB(128, 128, 128);
            //CalendarControl.TimeLineView.SeparateGroup = true;
            //CalendarControl.TimeLineView.MaxPixelsForEvent = 300;
            //// example of AdditionalOptionsFlags settings:
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptWeekViewShowStartTimeAlways);
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptWeekViewShowEndTimeAlways);
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptDayViewShowStartTimeAlways);
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptWorkWeekViewShowEndTimeAlways);
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptDayViewShowEndTimeAlways);
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptMonthViewShowStartTimeAlways);
            //CalendarControl.Options.AdditionalOptionsFlags.SetFlag(XtremeCalendarControl.XTPCalendarAdditionalOptions.xtpCalendarOptMonthViewShowEndTimeAlways);
            //// CalendarControl.EnableToolTips (True)
            //CalendarControl.AskItemTextFlags.SetFlag(XtremeCalendarControl.XTPCalendarGetItemText.xtpCalendarItemText_EventSubject);
            LoadSchedule();
			OpenProvider();
		}

		private void LoadSchedule()
		{
			//FC:TODO
			clsDRWrapper rsTaskInfo = new clsDRWrapper();
            //CalendarEvent calEvent = new CalendarEvent();
            Event calEvent = new Event();
			clsDRWrapper rsRecurrenceInfo = new clsDRWrapper();
			int counter;
			SQLDataHelper m_pHelper = new SQLDataHelper();
			m_pHelper.SetCalendar(CalendarControl);
			rsTaskInfo.OpenRecordset("SELECT * FROM NewTasks WHERE TaskPerson = '"+modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID()+"' OR TaskPerson = ''", modGlobalVariables.Statics.strGNDatabase);
            if (rsTaskInfo.EndOfFile() != true && rsTaskInfo.BeginningOfFile() != true)
            {
                do
                {
                    //calEvent = CalendarControl.DataProvider.createEvent;
                    calEvent = m_pHelper.CreateEventFromRS(ref rsTaskInfo, false);
                    //if (FCConvert.ToInt32(rsTaskInfo.Get_Fields("RecurrencePatternID")) != 0)
                    if (!rsTaskInfo.IsFieldNull("RecurrencePatternID") && FCConvert.ToString(rsTaskInfo.Get_Fields_Int32("RecurrencePatternID")) != "0")
                    {
                        rsRecurrenceInfo.OpenRecordset("SELECT * FROM RecurrencePattern WHERE RecurrencePatternID = " + rsTaskInfo.Get_Fields_Int32("RecurrencePatternID"), modGlobalVariables.Statics.strGNDatabase);
                        if (rsRecurrenceInfo.EndOfFile() != true && rsRecurrenceInfo.BeginningOfFile() != true)
                        {
                            //calEvent.CreateRecurrence();
                            //calEvent.UpdateRecurrence(m_pHelper.CreateRPatternFromRS(ref rsRecurrenceInfo));
                        }
                    }
                    CalendarLabel pLabel = CalendarControl.LabelList.Where(label => label.LabelID == calEvent.UserData.Label).FirstOrDefault();
                    if (pLabel != null)
                    {
                        calEvent.BackgroundColor = pLabel.Color;
                    }
                    //CalendarControl.DataProvider.AddEvent(calEvent);
                    CalendarControl.Events.Add(calEvent);
                    rsTaskInfo.MoveNext();
                } while (rsTaskInfo.EndOfFile() != true);
                FCUtils.ApplicationUpdate(CalendarControl);
            }
		}

		private void frmCalendar_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click()
		{
			Close();
		}
		// vbPorter upgrade warning: Cancel As int	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			//FC:FINAL:AM: close the form
            //if (!MDIParent.InstancePtr.blnProgramEnding)
			//{
			//	e.Cancel = true;
			//	this.Hide();
			//}
		}

		private void frmCalendar_Resize(object sender, System.EventArgs e)
		{
			//CommandBarsFrame.Item.HeightOriginal = 375;
			// On Error Resume Next
			// Dim nHeight As Long
			// 
			// nHeight = Height - CalendarControl.Top - 12 * Screen.TwipsPerPixelY
			// 
			// If nHeight < 0 Then Height = 0
			// 
			// CalendarControl.Move 0, CalendarControl.Top, Me.ScaleWidth, nHeight
			/*? On Error Resume Next  *///FC:TODO
			//int lBottom;
			//lBottom = CalendarControl.TopOriginal+CalendarControl.HeightOriginal;
			//int lTop;
			//lTop = CommandBarsFrame.HeightOriginal+CommandBarsFrame.TopOriginal;
			//CalendarControl.TopOriginal = lTop;
			//CalendarControl.HeightOriginal = lBottom-lTop;
		}

		private void CreateFormMenuBar()
		{
			//FC:TODO
			//XtremeCommandBars.CommandBarControl Control = new XtremeCommandBars.CommandBarControl();
			//XtremeCommandBars.CommandBarPopup ControlLevel1 = new XtremeCommandBars.CommandBarPopup();
			//XtremeCommandBars.CommandBarPopup ControlLevel2 = new XtremeCommandBars.CommandBarPopup();
			//XtremeCommandBars.CommandBarPopup ControlLevel3 = new XtremeCommandBars.CommandBarPopup();
			//XtremeCommandBars.CommandBarPopup ControlLevel4 = new XtremeCommandBars.CommandBarPopup();
			//CommandBarsGlobalSettings.App = App;
			//ControlLevel1 = CommandBarsFrame.ActiveMenuBar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlPopup, 0, "File");
			//Control = ControlLevel1.CommandBar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlButton, 1, "Page Setup");
			//Control = ControlLevel1.CommandBar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlButton, 2, "Print");
			//CommandBarsFrame.KeyBindings.AddShortcut(2, "F11");
			//Control = ControlLevel1.CommandBar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlButton, 3, "Print Preview");
			//CommandBarsFrame.KeyBindings.AddShortcut(3, "F12");
			//Control = ControlLevel1.CommandBar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlButton, 6, "Print Event Listing");
			//Control.BeginGroup = true;
			//Control = ControlLevel1.CommandBar.Controls.Add(XtremeCommandBars.XTPControlType.xtpControlButton, 5, "Exit");
			//CommandBarsFrame.KeyBindings.AddShortcut(5, "Esc");
			//Control.BeginGroup = true;
		}

		private void PrintCalendar()
		{
            // Header ---------------------------------------------------
            // CalendarControl.PrintOptions.Header.Font.Name = "Courier New"
            // CalendarControl.PrintOptions.Header.Font.Bold = True
            // CalendarControl.PrintOptions.Header.Font.SIZE = 10
            // CalendarControl.PrintOptions.Header.TextLeft = "(c)1998-2009 Codejock Software, All Rights Reserved."
            // CalendarControl.PrintOptions.Header.TextCenter = "Calendar"
            //FC:TODO
            //CalendarControl.PrintOptions.header.TextRight = "Page 1 of 1 ";
            //// Footer ---------------------------------------------------
            //// CalendarControl.PrintOptions.Header.Font.Italic = True
            //CalendarControl.PrintOptions.footer.TextLeft = "Date: "+ DateTime.Now.ToShortDateString();
            //// CalendarControl.PrintOptions.Footer.TextCenter = "Codejock Software, " & vbLf & " Print calendar example "
            //CalendarControl.PrintOptions.footer.TextRight = "Time: "+DateTime.Now.ToLongTimeString();
            //// Other fonts
            //// CalendarControl.PrintOptions.DateHeaderFont.SIZE = 12
            //// CalendarControl.PrintOptions.DateHeaderCalendarFont.SIZE = 10
            //// If CalendarControl.ShowPrintPageSetup() Then
            //CalendarControl.PrintCalendar(0);
            // End If
            CalendarControl.PrintCalendar();
		}

		private void PreviewCalendar()
		{
            // CalendarControl.PrintPreviewOptions.Title = "Calendar Control VB 6.0 Sample application"
            // CalendarControl.PrintPreview True
            //FC:TODO
            //CalendarControl.PrintPreviewExt(true, 200, 200, 800, 600);
            CalendarControl.PrintCalendar();
        }
		//FC:TODO
		//private void CommandBarsFrame_Execute(object sender, AxXtremeCommandBars._DCommandBarsFrameEvents_ExecuteEvent e)
		//{
		//	XtremeCommandBars.ICommandBarControl xctlTest = new XtremeCommandBars.ICommandBarControl();
		//	 /*? On Error Resume Next  */
		//	xctlTest = CommandBarsFrame.FindControl(, e.Control.ID, , true);
		//	if (xctlTest.Enabled && xctlTest.Visible) {
		//		switch (e.Control.ID) {
		//			case 1:
		//			{
		//				// mnuFilePreview     Print Preview
		//				CalendarControl.ShowPrintPageSetup();
		//				break;
		//			}
		//			case 2:
		//			{
		//				// mnuProcessQuit     Exit
		//				PrintCalendar();
		//				break;
		//			}
		//			case 3:
		//			{
		//				PreviewCalendar();
		//				break;
		//			}
		//			case 5:
		//			{
		//				Close();
		//				break;
		//			}
		//			case 6:
		//			{
		//				// print event listing
		//				frmPrintScheduledEvents.InstancePtr.Show(App.MainForm);
		//				break;
		//			}
		//		} //end switch
		//	}
		//}
		private void mnuDeleteEvent_Click(object sender, System.EventArgs e)
		{
			bool bDeleted;
			bDeleted = false;
			//FC:TODO
			//if (ContextEvent.RecurrenceState==XtremeCalendarControl.CalendarEventRecurrenceState.xtpCalendarRecurrenceOccurrence || ContextEvent.RecurrenceState==XtremeCalendarControl.CalendarEventRecurrenceState.xtpCalendarRecurrenceException)
			//{
			//	frmOccurrenceSeriesChooser.InstancePtr.m_bOcurrence = true;
			//	frmOccurrenceSeriesChooser.InstancePtr.m_bDeleteRequest = true;
			//	frmOccurrenceSeriesChooser.InstancePtr.m_strEventSubject = ContextEvent.Subject;
			//	frmOccurrenceSeriesChooser.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			//	if (frmOccurrenceSeriesChooser.InstancePtr.m_bOK==false) {
			//		return;
			//	} else if (!frmOccurrenceSeriesChooser.InstancePtr.m_bOcurrence) {
			//		// Series
			//		CalendarControl.DataProvider.DeleteEvent(ContextEvent.RecurrencePattern.MasterEvent);
			//		bDeleted = true;
			//	}
			//}
			//if (!bDeleted) {
			//	CalendarControl.DataProvider.DeleteEvent(ContextEvent);
			//}
			//CalendarControl.Populate();
			// CalendarControl.RedrawControl
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuNewEvent_Click(object sender, System.EventArgs e)
		{
			if (g_bUseBuiltInCalendarDialogs)
			{
				//XtremeCalendarControl.CalendarDialogs dlgCalendar = new XtremeCalendarControl.CalendarDialogs();
				//dlgCalendar.ParentHWND = this.Handle.ToInt32();
				//dlgCalendar.Calendar = CalendarControl;
				//dlgCalendar.ShowNewEvent();
				return;
			}
			frmEditEvent.InstancePtr.NewEvent();
			frmEditEvent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}

		public void mnuNewEvent_Click()
		{
			mnuNewEvent_Click(mnuNewEvent, new System.EventArgs());
		}

		private void mnuOpenEvent_Click(object sender, System.EventArgs e)
		{
			//ModifyEvent(ref ContextEvent);
		}
		private void ModifyEvent(Event ModEvent)
		{
		//	if (g_bUseBuiltInCalendarDialogs) {
		//		XtremeCalendarControl.CalendarDialogs dlgCalendar = new XtremeCalendarControl.CalendarDialogs();
		//		dlgCalendar.ParentHWND = this.Handle.ToInt32();
		//		dlgCalendar.Calendar = CalendarControl;
		//		dlgCalendar.ShowEditEvent(ModEvent);
		//		return;
		//	}
		//if (ModEvent.RecurrenceState!=XtremeCalendarControl.CalendarEventRecurrenceState.xtpCalendarRecurrenceNotRecurring)
		//	{
		//		frmOccurrenceSeriesChooser.InstancePtr.m_bOcurrence = true;
		//		frmOccurrenceSeriesChooser.InstancePtr.m_bDeleteRequest = false;
		//		frmOccurrenceSeriesChooser.InstancePtr.m_strEventSubject = ModEvent.Subject;
		//		frmOccurrenceSeriesChooser.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		//		if (frmOccurrenceSeriesChooser.InstancePtr.m_bOK==false) {
		//			return;
		//		} else if (frmOccurrenceSeriesChooser.InstancePtr.m_bOcurrence) {
		//			if (ModEvent.RecurrenceState==XtremeCalendarControl.CalendarEventRecurrenceState.xtpCalendarRecurrenceOccurrence) {
		//				ModEvent = ModEvent.CloneEvent;
		//				ModEvent.MakeAsRException();
		//			}
		//		} else {
		//			// Series
		//			ModEvent = ModEvent.RecurrencePattern.MasterEvent;
		//		}
		//	}
		frmEditEvent.InstancePtr.ModifyEvent(ref ModEvent);
		frmEditEvent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
		}
		private void TimerRMDForm_Tick(object sender, System.EventArgs e)
		{
			if (ModalFormsRunningCounter == 0)
			{
				TimerRMDForm.Enabled = false;
				// frmcalendarreminders.CheckReminders
				// IncrementModalFormsRunning
				frmCalendarReminders.InstancePtr.Show(FCForm.FormShowEnum.Modal);
				// DecrementModalFormsRunning
			}
		}

        private void CalendarControl_EventDoubleClick(object sender, Wisej.Web.Ext.FullCalendar.EventClickEventArgs e)
        {
            ModifyEvent(e.Event);
        }

        private void CalendarControl_EventClick(object sender, Wisej.Web.Ext.FullCalendar.EventClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                currentEvent = e.Event;
                contextMenuDelete.Show(CalendarControl.PointToScreen(e.Location));
            }
            
        }

        private void CalendarControl_DayDoubleClick(object sender, Wisej.Web.Ext.FullCalendar.DayClickEventArgs e)
        {
            //force load of form, to initialize comboboxes
			//FC:FINAL:DSE:#1901 Load event should come later
            //frmEditEvent.InstancePtr.LoadForm();
            frmEditEvent.InstancePtr.NewEvent();
            frmEditEvent.InstancePtr.Show(FCForm.FormShowEnum.Modal);
        }

		private void cmdPageSetup_Click(object sender, EventArgs e)
		{
			//CalendarControl.ShowPrintPageSetup();
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			PrintCalendar();
		}

		private void cmdPrintPreview_Click(object sender, EventArgs e)
		{
			PreviewCalendar();
		}

		private void cmdPrintEventListing_Click(object sender, EventArgs e)
		{
			frmPrintScheduledEvents.InstancePtr.Show(App.MainForm);
		}

        private void menuDeleteEvent_Click(object sender, EventArgs e)
        {
            CalendarControl.Events.Remove(currentEvent);
        }
    }
}
