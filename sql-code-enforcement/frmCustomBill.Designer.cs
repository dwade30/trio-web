//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Collections.Generic;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmCustomBill.
	/// </summary>
	partial class frmCustomBill
	{
		public FCGrid GridCopy;
		public FCGrid GridHighlighted;
		public fecherFoundation.FCFrame framControlInfo;
		public fecherFoundation.FCButton cmdOKControlInfoNew;
		public FCGrid GridControlInfo;
		public fecherFoundation.FCButton cmdOKControlInfo;
		public FCGrid GridControlTypes;
		public fecherFoundation.FCPictureBox imgControlImage;
		public FCGrid GridFields;
		public FCGrid GridBillSize;
		public FCGrid GridDeleted;
		public fecherFoundation.FCPanel framPage;
		public fecherFoundation.FCPictureBox Picture1;
		public fecherFoundation.FCLabel ctlGroupControl;
		public fecherFoundation.FCPictureBox BillImage;
		public fecherFoundation.FCLabel CustomLabel_0;
		public fecherFoundation.FCFrame framUserText;
		public fecherFoundation.FCButton cmdUserDefinedTextOK;
		public fecherFoundation.FCRichTextBox rtbUserText;
		public fecherFoundation.FCFrame framLoadDelete;
		public fecherFoundation.FCButton cmdLoadDeleteCancel;
		public FCGrid GridLoadDelete;
        public fecherFoundation.FCLine RatioLine;
		public fecherFoundation.FCMenuStrip MainMenu;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddField;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteField;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuZoomIn;
		public fecherFoundation.FCToolStripMenuItem mnuZoomOut;
		public fecherFoundation.FCToolStripMenuItem mnuSepar7;
		public fecherFoundation.FCToolStripMenuItem mnuPrintSample;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuImportFormat;
		public fecherFoundation.FCToolStripMenuItem mnuExportFormat;
		public fecherFoundation.FCToolStripMenuItem mnuSepar6;
		public fecherFoundation.FCToolStripMenuItem mnuPreferences;
		public fecherFoundation.FCToolStripMenuItem mnuFullCharFullLine;
		public fecherFoundation.FCToolStripMenuItem mnuMoveHalfCharHalfLine;
		public fecherFoundation.FCToolStripMenuItem mnuMoveQuarterCharQuarterLine;
		public fecherFoundation.FCToolStripMenuItem mnusepar5;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteFormat;
		public fecherFoundation.FCToolStripMenuItem mnuLoadFormat;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveAs;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuPop;
		public fecherFoundation.FCToolStripMenuItem mnuAlignLefts;
		public fecherFoundation.FCToolStripMenuItem mnuAlignRights;
		public fecherFoundation.FCToolStripMenuItem mnuAlignTops;
		public fecherFoundation.FCToolStripMenuItem mnuAlignBottoms;
		public fecherFoundation.FCToolStripMenuItem mnuPopSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuBringToFront;
		public fecherFoundation.FCToolStripMenuItem mnuSendToBack;
		public fecherFoundation.FCToolStripMenuItem mnuPopSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuCopyControls;
		public fecherFoundation.FCToolStripMenuItem mnuPasteControls;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomBill));
            this.GridCopy = new fecherFoundation.FCGrid();
            this.GridHighlighted = new fecherFoundation.FCGrid();
            this.framControlInfo = new fecherFoundation.FCFrame();
            this.GridControlInfo = new fecherFoundation.FCGrid();
            this.GridControlTypes = new fecherFoundation.FCGrid();
            this.imgControlImage = new fecherFoundation.FCPictureBox();
            this.cmdOKControlInfoNew = new fecherFoundation.FCButton();
            this.cmdOKControlInfo = new fecherFoundation.FCButton();
            this.MainMenu = new fecherFoundation.FCMenuStrip();
            this.mnuPrintSample = new fecherFoundation.FCToolStripMenuItem();
            this.mnuZoomIn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuZoomOut = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPreferences = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFullCharFullLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMoveHalfCharHalfLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMoveQuarterCharQuarterLine = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExportFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLoadFormat = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveAs = new fecherFoundation.FCToolStripMenuItem();
            this.GridFields = new fecherFoundation.FCGrid();
            this.GridBillSize = new fecherFoundation.FCGrid();
            this.framUserText = new fecherFoundation.FCFrame();
            this.cmdUserDefinedTextOK = new fecherFoundation.FCButton();
            this.rtbUserText = new fecherFoundation.FCRichTextBox();
            this.GridDeleted = new fecherFoundation.FCGrid();
            this.framPage = new fecherFoundation.FCPanel();
            this.Picture1 = new fecherFoundation.FCPictureBox();
            this.ctlGroupControl = new fecherFoundation.FCLabel();
            this.BillImage = new fecherFoundation.FCPictureBox();
            this.CustomLabel_0 = new fecherFoundation.FCLabel();
            this.framLoadDelete = new fecherFoundation.FCFrame();
            this.cmdLoadDeleteCancel = new fecherFoundation.FCButton();
            this.GridLoadDelete = new fecherFoundation.FCGrid();
            this.RatioLine = new fecherFoundation.FCLine();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPop = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignLefts = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignRights = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignTops = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAlignBottoms = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPopSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBringToFront = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSendToBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPopSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCopyControls = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPasteControls = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar6 = new fecherFoundation.FCToolStripMenuItem();
            this.mnusepar5 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddField = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteField = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar7 = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdAddField = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSaveAs = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCopy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridHighlighted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framControlInfo)).BeginInit();
            this.framControlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridControlInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControlTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgControlImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfoNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFields)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBillSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framUserText)).BeginInit();
            this.framUserText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUserDefinedTextOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbUserText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPage)).BeginInit();
            this.framPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).BeginInit();
            this.Picture1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BillImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLoadDelete)).BeginInit();
            this.framLoadDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadDeleteCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLoadDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 661);
            this.BottomPanel.Size = new System.Drawing.Size(1138, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framUserText);
            this.ClientArea.Controls.Add(this.GridCopy);
            this.ClientArea.Controls.Add(this.GridHighlighted);
            this.ClientArea.Controls.Add(this.GridFields);
            this.ClientArea.Controls.Add(this.GridBillSize);
            this.ClientArea.Controls.Add(this.GridDeleted);
            this.ClientArea.Controls.Add(this.RatioLine);
            this.ClientArea.Controls.Add(this.framPage);
            this.ClientArea.Controls.Add(this.framLoadDelete);
            this.ClientArea.Controls.Add(this.framControlInfo);
            this.ClientArea.Size = new System.Drawing.Size(1138, 601);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSaveAs);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdAddField);
            this.TopPanel.Size = new System.Drawing.Size(1138, 60);
            this.TopPanel.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddField, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSaveAs, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(173, 30);
            this.HeaderText.Text = "Custom Forms";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // GridCopy
            // 
            this.GridCopy.Cols = 17;
            this.GridCopy.ColumnHeadersVisible = false;
            this.GridCopy.FixedCols = 0;
            this.GridCopy.FixedRows = 0;
            this.GridCopy.Location = new System.Drawing.Point(0, 66);
            this.GridCopy.Name = "GridCopy";
            this.GridCopy.RowHeadersVisible = false;
            this.GridCopy.Rows = 0;
            this.GridCopy.ShowFocusCell = false;
            this.GridCopy.Size = new System.Drawing.Size(9, 18);
            this.GridCopy.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.GridCopy, null);
            this.GridCopy.Visible = false;
            // 
            // GridHighlighted
            // 
            this.GridHighlighted.Cols = 3;
            this.GridHighlighted.ColumnHeadersVisible = false;
            this.GridHighlighted.FixedRows = 0;
            this.GridHighlighted.Location = new System.Drawing.Point(0, 49);
            this.GridHighlighted.Name = "GridHighlighted";
            this.GridHighlighted.Rows = 0;
            this.GridHighlighted.ShowFocusCell = false;
            this.GridHighlighted.Size = new System.Drawing.Size(2, 10);
            this.GridHighlighted.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.GridHighlighted, null);
            this.GridHighlighted.Visible = false;
            // 
            // framControlInfo
            // 
            this.framControlInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framControlInfo.AppearanceKey = "groupBoxNoBorders";
            this.framControlInfo.BackColor = System.Drawing.Color.White;
            this.framControlInfo.Controls.Add(this.GridControlInfo);
            this.framControlInfo.Controls.Add(this.GridControlTypes);
            this.framControlInfo.Controls.Add(this.imgControlImage);
            this.framControlInfo.Controls.Add(this.cmdOKControlInfoNew);
            this.framControlInfo.Controls.Add(this.cmdOKControlInfo);
            this.framControlInfo.Location = new System.Drawing.Point(10, 10);
            this.framControlInfo.Name = "framControlInfo";
            this.framControlInfo.Size = new System.Drawing.Size(1108, 573);
            this.framControlInfo.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.framControlInfo, null);
            this.framControlInfo.Visible = false;
            // 
            // GridControlInfo
            // 
            this.GridControlInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridControlInfo.ColumnHeadersVisible = false;
            this.GridControlInfo.ExtendLastCol = true;
            this.GridControlInfo.FixedRows = 0;
            this.GridControlInfo.Location = new System.Drawing.Point(20, 20);
            this.GridControlInfo.Name = "GridControlInfo";
            this.GridControlInfo.Rows = 6;
            this.GridControlInfo.ShowFocusCell = false;
            this.GridControlInfo.Size = new System.Drawing.Size(1080, 453);
            this.GridControlInfo.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.GridControlInfo, null);
            this.GridControlInfo.CellButtonClick += new System.EventHandler(this.GridControlInfo_CellButtonClick);
            this.GridControlInfo.ComboCloseUp += new System.EventHandler(this.GridControlInfo_ComboCloseUp);
            this.GridControlInfo.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridControlInfo_BeforeEdit);
            this.GridControlInfo.CurrentCellChanged += new System.EventHandler(this.GridControlInfo_RowColChange);
            this.GridControlInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridControlInfo_MouseMoveEvent);
            // 
            // GridControlTypes
            // 
            this.GridControlTypes.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridControlTypes.Cols = 10;
            this.GridControlTypes.ExtendLastCol = true;
            this.GridControlTypes.Location = new System.Drawing.Point(20, 20);
            this.GridControlTypes.Name = "GridControlTypes";
            this.GridControlTypes.Rows = 1;
            this.GridControlTypes.ShowFocusCell = false;
            this.GridControlTypes.Size = new System.Drawing.Size(1070, 453);
            this.GridControlTypes.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.GridControlTypes, null);
            this.GridControlTypes.Visible = false;
            this.GridControlTypes.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridControlTypes_MouseMoveEvent);
            this.GridControlTypes.Click += new System.EventHandler(this.GridControlTypes_ClickEvent);
            // 
            // imgControlImage
            // 
            this.imgControlImage.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgControlImage.FillColor = 16777215;
            this.imgControlImage.Image = ((System.Drawing.Image)(resources.GetObject("imgControlImage.Image")));
            this.imgControlImage.Location = new System.Drawing.Point(20, 150);
            this.imgControlImage.Name = "imgControlImage";
            this.imgControlImage.Picture = ((System.Drawing.Image)(resources.GetObject("imgControlImage.Picture")));
            this.imgControlImage.Size = new System.Drawing.Size(336, 177);
            this.imgControlImage.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgControlImage, null);
            this.imgControlImage.Visible = false;
            // 
            // cmdOKControlInfoNew
            // 
            this.cmdOKControlInfoNew.AppearanceKey = "actionButton";
            this.cmdOKControlInfoNew.Location = new System.Drawing.Point(20, 462);
            this.cmdOKControlInfoNew.Name = "cmdOKControlInfoNew";
            this.cmdOKControlInfoNew.Size = new System.Drawing.Size(63, 40);
            this.cmdOKControlInfoNew.TabIndex = 18;
            this.cmdOKControlInfoNew.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdOKControlInfoNew, null);
            this.cmdOKControlInfoNew.Visible = false;
            this.cmdOKControlInfoNew.Click += new System.EventHandler(this.cmdOKControlInfoNew_Click);
            // 
            // cmdOKControlInfo
            // 
            this.cmdOKControlInfo.AppearanceKey = "actionButton";
            this.cmdOKControlInfo.Location = new System.Drawing.Point(20, 462);
            this.cmdOKControlInfo.Name = "cmdOKControlInfo";
            this.cmdOKControlInfo.Size = new System.Drawing.Size(63, 40);
            this.cmdOKControlInfo.TabIndex = 9;
            this.cmdOKControlInfo.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdOKControlInfo, null);
            this.cmdOKControlInfo.Click += new System.EventHandler(this.cmdOKControlInfo_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrintSample,
            this.mnuZoomIn,
            this.mnuZoomOut,
            this.mnuPreferences,
            this.mnuImportFormat,
            this.mnuExportFormat,
            this.mnuDeleteFormat,
            this.mnuLoadFormat,
            this.mnuNew});
            this.MainMenu.Name = null;
            // 
            // mnuPrintSample
            // 
            this.mnuPrintSample.Index = 0;
            this.mnuPrintSample.Name = "mnuPrintSample";
            this.mnuPrintSample.Text = "Print Sample";
            this.mnuPrintSample.Click += new System.EventHandler(this.mnuPrintSample_Click);
            // 
            // mnuZoomIn
            // 
            this.mnuZoomIn.Index = 1;
            this.mnuZoomIn.Name = "mnuZoomIn";
            this.mnuZoomIn.Shortcut = Wisej.Web.Shortcut.CtrlI;
            this.mnuZoomIn.Text = "Zoom In";
            this.mnuZoomIn.Click += new System.EventHandler(this.mnuZoomIn_Click);
            // 
            // mnuZoomOut
            // 
            this.mnuZoomOut.Index = 2;
            this.mnuZoomOut.Name = "mnuZoomOut";
            this.mnuZoomOut.Shortcut = Wisej.Web.Shortcut.CtrlO;
            this.mnuZoomOut.Text = "Zoom Out";
            this.mnuZoomOut.Click += new System.EventHandler(this.mnuZoomOut_Click);
            // 
            // mnuPreferences
            // 
            this.mnuPreferences.Index = 3;
            this.mnuPreferences.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFullCharFullLine,
            this.mnuMoveHalfCharHalfLine,
            this.mnuMoveQuarterCharQuarterLine});
            this.mnuPreferences.Name = "mnuPreferences";
            this.mnuPreferences.Text = "Preferences";
            // 
            // mnuFullCharFullLine
            // 
            this.mnuFullCharFullLine.Checked = true;
            this.mnuFullCharFullLine.Index = 0;
            this.mnuFullCharFullLine.Name = "mnuFullCharFullLine";
            this.mnuFullCharFullLine.Text = "Move Full Character & Full Line";
            this.mnuFullCharFullLine.Click += new System.EventHandler(this.mnuFullCharFullLine_Click);
            // 
            // mnuMoveHalfCharHalfLine
            // 
            this.mnuMoveHalfCharHalfLine.Index = 1;
            this.mnuMoveHalfCharHalfLine.Name = "mnuMoveHalfCharHalfLine";
            this.mnuMoveHalfCharHalfLine.Text = "Move Half Character & Half Line";
            this.mnuMoveHalfCharHalfLine.Click += new System.EventHandler(this.mnuMoveHalfCharHalfLine_Click);
            // 
            // mnuMoveQuarterCharQuarterLine
            // 
            this.mnuMoveQuarterCharQuarterLine.Index = 2;
            this.mnuMoveQuarterCharQuarterLine.Name = "mnuMoveQuarterCharQuarterLine";
            this.mnuMoveQuarterCharQuarterLine.Text = "Move Quarter Character & Quarter Line";
            this.mnuMoveQuarterCharQuarterLine.Click += new System.EventHandler(this.mnuMoveQuarterCharQuarterLine_Click);
            // 
            // mnuImportFormat
            // 
            this.mnuImportFormat.Index = 4;
            this.mnuImportFormat.Name = "mnuImportFormat";
            this.mnuImportFormat.Text = "Import Format";
            this.mnuImportFormat.Click += new System.EventHandler(this.mnuImportFormat_Click);
            // 
            // mnuExportFormat
            // 
            this.mnuExportFormat.Index = 5;
            this.mnuExportFormat.Name = "mnuExportFormat";
            this.mnuExportFormat.Text = "Export Format";
            this.mnuExportFormat.Click += new System.EventHandler(this.mnuExportFormat_Click);
            // 
            // mnuDeleteFormat
            // 
            this.mnuDeleteFormat.Index = 6;
            this.mnuDeleteFormat.Name = "mnuDeleteFormat";
            this.mnuDeleteFormat.Text = "Delete Format";
            this.mnuDeleteFormat.Click += new System.EventHandler(this.mnuDeleteFormat_Click);
            // 
            // mnuLoadFormat
            // 
            this.mnuLoadFormat.Index = 7;
            this.mnuLoadFormat.Name = "mnuLoadFormat";
            this.mnuLoadFormat.Text = "Load Format";
            this.mnuLoadFormat.Click += new System.EventHandler(this.mnuLoadFormat_Click);
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 8;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New Format";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSaveAs
            // 
            this.mnuSaveAs.Index = -1;
            this.mnuSaveAs.Name = "mnuSaveAs";
            this.mnuSaveAs.Text = "Save As";
            this.mnuSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // GridFields
            // 
            this.GridFields.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridFields.Cols = 10;
            this.GridFields.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridFields.FixedCols = 2;
            this.GridFields.FrozenCols = 1;
            this.GridFields.Location = new System.Drawing.Point(453, 30);
            this.GridFields.Name = "GridFields";
            this.GridFields.ReadOnly = false;
            this.GridFields.Rows = 1;
            this.GridFields.ShowFocusCell = false;
            this.GridFields.Size = new System.Drawing.Size(669, 272);
            this.GridFields.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.GridFields, null);
            this.GridFields.CellButtonClick += new System.EventHandler(this.GridFields_CellButtonClick);
            this.GridFields.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridFields_ValidateEdit);
            this.GridFields.CurrentCellChanged += new System.EventHandler(this.GridFields_RowColChange);
            this.GridFields.Click += new System.EventHandler(this.GridFields_ClickEvent);
            this.GridFields.KeyDown += new Wisej.Web.KeyEventHandler(this.GridFields_KeyDownEvent);
            this.GridFields.KeyUp += new Wisej.Web.KeyEventHandler(this.GridFields_KeyUpEvent);
            // 
            // GridBillSize
            // 
            this.GridBillSize.ColumnHeadersVisible = false;
            this.GridBillSize.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.GridBillSize.ExtendLastCol = true;
            this.GridBillSize.FixedRows = 0;
            this.GridBillSize.Location = new System.Drawing.Point(30, 30);
            this.GridBillSize.Name = "GridBillSize";
            this.GridBillSize.ReadOnly = false;
            this.GridBillSize.Rows = 9;
            this.GridBillSize.ShowFocusCell = false;
            this.GridBillSize.Size = new System.Drawing.Size(399, 272);
            this.GridBillSize.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.GridBillSize, null);
            this.GridBillSize.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.GridBillSize_ChangeEdit);
            this.GridBillSize.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.GridBillSize_BeforeEdit);
            this.GridBillSize.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridBillSize_ValidateEdit);
            // 
            // framUserText
            // 
            this.framUserText.BackColor = System.Drawing.Color.White;
            this.framUserText.Controls.Add(this.cmdUserDefinedTextOK);
            this.framUserText.Controls.Add(this.rtbUserText);
            this.framUserText.Location = new System.Drawing.Point(20, 20);
            this.framUserText.Name = "framUserText";
            this.framUserText.Size = new System.Drawing.Size(590, 516);
            this.framUserText.TabIndex = 5;
            this.framUserText.Text = "User Defined Text";
            this.ToolTip1.SetToolTip(this.framUserText, null);
            this.framUserText.Visible = false;
            // 
            // cmdUserDefinedTextOK
            // 
            this.cmdUserDefinedTextOK.AppearanceKey = "actionButton";
            this.cmdUserDefinedTextOK.Location = new System.Drawing.Point(251, 455);
            this.cmdUserDefinedTextOK.Name = "cmdUserDefinedTextOK";
            this.cmdUserDefinedTextOK.Size = new System.Drawing.Size(80, 40);
            this.cmdUserDefinedTextOK.TabIndex = 7;
            this.cmdUserDefinedTextOK.Text = "OK";
            this.ToolTip1.SetToolTip(this.cmdUserDefinedTextOK, null);
            this.cmdUserDefinedTextOK.Click += new System.EventHandler(this.cmdUserDefinedTextOK_Click);
            // 
            // rtbUserText
            // 
            this.rtbUserText.Location = new System.Drawing.Point(20, 20);
            this.rtbUserText.Name = "rtbUserText";
            this.rtbUserText.Size = new System.Drawing.Size(551, 414);
            this.rtbUserText.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.rtbUserText, null);
            // 
            // GridDeleted
            // 
            this.GridDeleted.Cols = 1;
            this.GridDeleted.ColumnHeadersVisible = false;
            this.GridDeleted.FixedCols = 0;
            this.GridDeleted.FixedRows = 0;
            this.GridDeleted.Location = new System.Drawing.Point(0, 91);
            this.GridDeleted.Name = "GridDeleted";
            this.GridDeleted.RowHeadersVisible = false;
            this.GridDeleted.Rows = 0;
            this.GridDeleted.ShowFocusCell = false;
            this.GridDeleted.Size = new System.Drawing.Size(17, 10);
            this.GridDeleted.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.GridDeleted, null);
            this.GridDeleted.Visible = false;
            // 
            // framPage
            // 
            this.framPage.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framPage.AutoScroll = true;
            this.framPage.Controls.Add(this.Picture1);
            this.framPage.Location = new System.Drawing.Point(10, 331);
            this.framPage.Name = "framPage";
            this.framPage.Size = new System.Drawing.Size(1096, 252);
            this.framPage.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.framPage, null);
            // 
            // Picture1
            // 
            this.Picture1.BackColor = System.Drawing.SystemColors.Window;
            this.Picture1.Controls.Add(this.ctlGroupControl);
            this.Picture1.Controls.Add(this.BillImage);
            this.Picture1.Controls.Add(this.CustomLabel_0);
            this.Picture1.FillColor = -2147483643;
            this.Picture1.Image = ((System.Drawing.Image)(resources.GetObject("Picture1.Image")));
            this.Picture1.Location = new System.Drawing.Point(20, 0);
            this.Picture1.Name = "Picture1";
            this.Picture1.Picture = ((System.Drawing.Image)(resources.GetObject("Picture1.Picture")));
            this.Picture1.Size = new System.Drawing.Size(1058, 220);
            this.ToolTip1.SetToolTip(this.Picture1, null);
            // 
            // ctlGroupControl
            // 
            this.ctlGroupControl.BorderStyle = 1;
            this.ctlGroupControl.Name = "ctlGroupControl";
            this.ctlGroupControl.Size = new System.Drawing.Size(1, 10);
            this.ToolTip1.SetToolTip(this.ctlGroupControl, null);
            this.ctlGroupControl.Visible = false;
            // 
            // BillImage
            // 
            this.BillImage.BorderStyle = Wisej.Web.BorderStyle.None;
            this.BillImage.FillColor = -2147483643;
            this.BillImage.Image = ((System.Drawing.Image)(resources.GetObject("BillImage.Image")));
            this.BillImage.Name = "BillImage";
            this.BillImage.Picture = ((System.Drawing.Image)(resources.GetObject("BillImage.Picture")));
            this.BillImage.Size = new System.Drawing.Size(33, 43);
            this.BillImage.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.BillImage, null);
            this.BillImage.MouseDown += new Wisej.Web.MouseEventHandler(this.BillImage_MouseDown);
            this.BillImage.MouseMove += new Wisej.Web.MouseEventHandler(this.BillImage_MouseMove);
            this.BillImage.MouseUp += new Wisej.Web.MouseEventHandler(this.BillImage_MouseUp);
            this.BillImage.DragDrop += new Wisej.Web.DragEventHandler(this.BillImage_DragDrop);
            this.BillImage.DragOver += new Wisej.Web.DragEventHandler(this.BillImage_DragOver);
            // 
            // CustomLabel_0
            // 
            this.CustomLabel_0.AllowDrag = true;
            this.CustomLabel_0.AllowDrop = true;
            this.CustomLabel_0.BackColor = System.Drawing.Color.Transparent;
            this.CustomLabel_0.BorderStyle = 1;
            this.CustomLabel_0.Location = new System.Drawing.Point(-7, -24);
            this.CustomLabel_0.Name = "CustomLabel_0";
            this.CustomLabel_0.Size = new System.Drawing.Size(58, 18);
            this.CustomLabel_0.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.CustomLabel_0, null);
            this.CustomLabel_0.Visible = false;
            this.CustomLabel_0.MouseDown += new Wisej.Web.MouseEventHandler(this.CustomLabel_MouseDown);
            this.CustomLabel_0.MouseUp += new Wisej.Web.MouseEventHandler(this.CustomLabel_0_MouseUp);
            // 
            // framLoadDelete
            // 
            this.framLoadDelete.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framLoadDelete.AppearanceKey = "groupBoxNoBorders";
            this.framLoadDelete.BackColor = System.Drawing.Color.White;
            this.framLoadDelete.Controls.Add(this.cmdLoadDeleteCancel);
            this.framLoadDelete.Controls.Add(this.GridLoadDelete);
            this.framLoadDelete.Location = new System.Drawing.Point(30, 30);
            this.framLoadDelete.Name = "framLoadDelete";
            this.framLoadDelete.Size = new System.Drawing.Size(1000, 278);
            this.framLoadDelete.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.framLoadDelete, null);
            this.framLoadDelete.Visible = false;
            // 
            // cmdLoadDeleteCancel
            // 
            this.cmdLoadDeleteCancel.AppearanceKey = "actionButton";
            this.cmdLoadDeleteCancel.Location = new System.Drawing.Point(20, 219);
            this.cmdLoadDeleteCancel.Name = "cmdLoadDeleteCancel";
            this.cmdLoadDeleteCancel.Size = new System.Drawing.Size(80, 40);
            this.cmdLoadDeleteCancel.TabIndex = 16;
            this.cmdLoadDeleteCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdLoadDeleteCancel, null);
            this.cmdLoadDeleteCancel.Click += new System.EventHandler(this.cmdLoadDeleteCancel_Click);
            // 
            // GridLoadDelete
            // 
            this.GridLoadDelete.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridLoadDelete.Cols = 3;
            this.GridLoadDelete.ExtendLastCol = true;
            this.GridLoadDelete.FixedCols = 0;
            this.GridLoadDelete.Location = new System.Drawing.Point(20, 20);
            this.GridLoadDelete.Name = "GridLoadDelete";
            this.GridLoadDelete.RowHeadersVisible = false;
            this.GridLoadDelete.Rows = 1;
            this.GridLoadDelete.ShowFocusCell = false;
            this.GridLoadDelete.Size = new System.Drawing.Size(870, 178);
            this.GridLoadDelete.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.GridLoadDelete, null);
            this.GridLoadDelete.CurrentCellChanged += new System.EventHandler(this.GridLoadDelete_RowColChange);
            this.GridLoadDelete.DoubleClick += new System.EventHandler(this.GridLoadDelete_DblClick);
            // 
            // RatioLine
            // 
            this.RatioLine.Location = new System.Drawing.Point(0, -29);
            this.RatioLine.Name = "RatioLine";
            this.RatioLine.Size = new System.Drawing.Size(1018, 1);
            this.ToolTip1.SetToolTip(this.RatioLine, null);
            this.RatioLine.Visible = false;
            this.RatioLine.X2 = 1440F;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
            // 
            // mnuPop
            // 
            this.mnuPop.Index = -1;
            this.mnuPop.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAlignLefts,
            this.mnuAlignRights,
            this.mnuAlignTops,
            this.mnuAlignBottoms,
            this.mnuPopSepar1,
            this.mnuBringToFront,
            this.mnuSendToBack,
            this.mnuPopSepar2,
            this.mnuCopyControls,
            this.mnuPasteControls});
            this.mnuPop.Name = "mnuPop";
            this.mnuPop.Text = "mnuPop";
            this.mnuPop.Visible = false;
            // 
            // mnuAlignLefts
            // 
            this.mnuAlignLefts.Index = 0;
            this.mnuAlignLefts.Name = "mnuAlignLefts";
            this.mnuAlignLefts.Text = "Align Lefts";
            this.mnuAlignLefts.Click += new System.EventHandler(this.mnuAlignLefts_Click);
            // 
            // mnuAlignRights
            // 
            this.mnuAlignRights.Index = 1;
            this.mnuAlignRights.Name = "mnuAlignRights";
            this.mnuAlignRights.Text = "Align Rights";
            this.mnuAlignRights.Click += new System.EventHandler(this.mnuAlignRights_Click);
            // 
            // mnuAlignTops
            // 
            this.mnuAlignTops.Index = 2;
            this.mnuAlignTops.Name = "mnuAlignTops";
            this.mnuAlignTops.Text = "Align Tops";
            this.mnuAlignTops.Click += new System.EventHandler(this.mnuAlignTops_Click);
            // 
            // mnuAlignBottoms
            // 
            this.mnuAlignBottoms.Index = 3;
            this.mnuAlignBottoms.Name = "mnuAlignBottoms";
            this.mnuAlignBottoms.Text = "Align Bottoms";
            this.mnuAlignBottoms.Click += new System.EventHandler(this.mnuAlignBottoms_Click);
            // 
            // mnuPopSepar1
            // 
            this.mnuPopSepar1.Index = 4;
            this.mnuPopSepar1.Name = "mnuPopSepar1";
            this.mnuPopSepar1.Text = "-";
            // 
            // mnuBringToFront
            // 
            this.mnuBringToFront.Index = 5;
            this.mnuBringToFront.Name = "mnuBringToFront";
            this.mnuBringToFront.Text = "Bring to Front";
            this.mnuBringToFront.Click += new System.EventHandler(this.mnuBringToFront_Click);
            // 
            // mnuSendToBack
            // 
            this.mnuSendToBack.Index = 6;
            this.mnuSendToBack.Name = "mnuSendToBack";
            this.mnuSendToBack.Text = "Send to Back";
            this.mnuSendToBack.Click += new System.EventHandler(this.mnuSendToBack_Click);
            // 
            // mnuPopSepar2
            // 
            this.mnuPopSepar2.Index = 7;
            this.mnuPopSepar2.Name = "mnuPopSepar2";
            this.mnuPopSepar2.Text = "-";
            // 
            // mnuCopyControls
            // 
            this.mnuCopyControls.Index = 8;
            this.mnuCopyControls.Name = "mnuCopyControls";
            this.mnuCopyControls.Text = "Copy";
            this.mnuCopyControls.Click += new System.EventHandler(this.mnuCopyControls_Click);
            // 
            // mnuPasteControls
            // 
            this.mnuPasteControls.Enabled = false;
            this.mnuPasteControls.Index = 9;
            this.mnuPasteControls.Name = "mnuPasteControls";
            this.mnuPasteControls.Text = "Paste";
            this.mnuPasteControls.Click += new System.EventHandler(this.mnuPasteControls_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = -1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuSepar6
            // 
            this.mnuSepar6.Index = -1;
            this.mnuSepar6.Name = "mnuSepar6";
            this.mnuSepar6.Text = "-";
            // 
            // mnusepar5
            // 
            this.mnusepar5.Index = -1;
            this.mnusepar5.Name = "mnusepar5";
            this.mnusepar5.Text = "-";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuAddField
            // 
            this.mnuAddField.Index = -1;
            this.mnuAddField.Name = "mnuAddField";
            this.mnuAddField.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
            this.mnuAddField.Text = "Add Field";
            this.mnuAddField.Click += new System.EventHandler(this.mnuAddField_Click);
            // 
            // mnuDeleteField
            // 
            this.mnuDeleteField.Index = -1;
            this.mnuDeleteField.Name = "mnuDeleteField";
            this.mnuDeleteField.Shortcut = Wisej.Web.Shortcut.ShiftDelete;
            this.mnuDeleteField.Text = "Delete Field";
            this.mnuDeleteField.Click += new System.EventHandler(this.mnuDeleteField_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuSepar7
            // 
            this.mnuSepar7.Index = -1;
            this.mnuSepar7.Name = "mnuSepar7";
            this.mnuSepar7.Text = "-";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(548, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdAddField
            // 
            this.cmdAddField.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddField.Location = new System.Drawing.Point(948, 29);
            this.cmdAddField.Name = "cmdAddField";
            this.cmdAddField.Shortcut = Wisej.Web.Shortcut.ShiftInsert;
            this.cmdAddField.Size = new System.Drawing.Size(76, 24);
            this.cmdAddField.TabIndex = 1;
            this.cmdAddField.Text = "Add Field";
            this.cmdAddField.Click += new System.EventHandler(this.mnuAddField_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(1029, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Shortcut = Wisej.Web.Shortcut.ShiftDelete;
            this.cmdDelete.Size = new System.Drawing.Size(89, 24);
            this.cmdDelete.TabIndex = 2;
            this.cmdDelete.Text = "Delete Field";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDeleteField_Click);
            // 
            // cmdSaveAs
            // 
            this.cmdSaveAs.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSaveAs.Location = new System.Drawing.Point(878, 29);
            this.cmdSaveAs.Name = "cmdSaveAs";
            this.cmdSaveAs.Size = new System.Drawing.Size(64, 24);
            this.cmdSaveAs.TabIndex = 3;
            this.cmdSaveAs.Text = "Save As";
            this.cmdSaveAs.Click += new System.EventHandler(this.mnuSaveAs_Click);
            // 
            // frmCustomBill
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(1138, 769);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu;
            this.Name = "frmCustomBill";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmCustomBill_Load);
            this.Resize += new System.EventHandler(this.frmCustomBill_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomBill_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCopy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridHighlighted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framControlInfo)).EndInit();
            this.framControlInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridControlInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControlTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgControlImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfoNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOKControlInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridFields)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridBillSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framUserText)).EndInit();
            this.framUserText.ResumeLayout(false);
            this.framUserText.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUserDefinedTextOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbUserText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framPage)).EndInit();
            this.framPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Picture1)).EndInit();
            this.Picture1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BillImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framLoadDelete)).EndInit();
            this.framLoadDelete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdLoadDeleteCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridLoadDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveAs)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdAddField;
		private FCButton cmdDelete;
		private FCButton cmdSaveAs;
	}
}