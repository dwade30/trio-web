//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;

namespace TWCE0000
{
	/// <summary>
	/// Summary description for frmEditEvent.
	/// </summary>
	partial class frmEditEvent
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCTextBox txtBody;
		public fecherFoundation.FCButton btnRecurrence;
		public fecherFoundation.FCComboBox cboUser;
		public fecherFoundation.FCGrid vsInstallers;
		public fecherFoundation.FCCheckBox chkMeeting;
		public fecherFoundation.FCCheckBox chkPublic;
		public fecherFoundation.FCComboBox cmbStartDate;
		public fecherFoundation.FCComboBox cmbEndDate;
		public fecherFoundation.FCComboBox cmbReminder;
		public fecherFoundation.FCCheckBox chkReminder;
		public fecherFoundation.FCCheckBox chkAllDayEvent;
		public fecherFoundation.FCTextBox txtColor;
		public fecherFoundation.FCTextBox txtSubject;
		public fecherFoundation.FCTextBox txtLocation;
		public fecherFoundation.FCComboBox cmbLabel;
		public FCDateTimePicker dtpStart;
		public FCDateTimePicker dtpEnd;
		public fecherFoundation.FCLabel lblStartTime;
		public fecherFoundation.FCLabel lblEndTime;
		public fecherFoundation.FCLabel lblSubject;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCLabel lblLabel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.txtBody = new fecherFoundation.FCTextBox();
            this.btnRecurrence = new fecherFoundation.FCButton();
            this.cboUser = new fecherFoundation.FCComboBox();
            this.vsInstallers = new fecherFoundation.FCGrid();
            this.chkMeeting = new fecherFoundation.FCCheckBox();
            this.chkPublic = new fecherFoundation.FCCheckBox();
            this.cmbStartDate = new fecherFoundation.FCComboBox();
            this.cmbEndDate = new fecherFoundation.FCComboBox();
            this.cmbReminder = new fecherFoundation.FCComboBox();
            this.chkReminder = new fecherFoundation.FCCheckBox();
            this.chkAllDayEvent = new fecherFoundation.FCCheckBox();
            this.txtColor = new fecherFoundation.FCTextBox();
            this.txtSubject = new fecherFoundation.FCTextBox();
            this.txtLocation = new fecherFoundation.FCTextBox();
            this.cmbLabel = new fecherFoundation.FCComboBox();
            this.dtpStart = new fecherFoundation.FCDateTimePicker();
            this.dtpEnd = new fecherFoundation.FCDateTimePicker();
            this.lblStartTime = new fecherFoundation.FCLabel();
            this.lblEndTime = new fecherFoundation.FCLabel();
            this.lblSubject = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.lblLabel = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRecurrence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsInstallers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMeeting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPublic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReminder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllDayEvent)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 653);
            this.BottomPanel.Size = new System.Drawing.Size(989, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cboUser);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.vsInstallers);
            this.ClientArea.Controls.Add(this.cmdCancel);
            this.ClientArea.Controls.Add(this.cmdOk);
            this.ClientArea.Controls.Add(this.txtBody);
            this.ClientArea.Controls.Add(this.btnRecurrence);
            this.ClientArea.Controls.Add(this.chkMeeting);
            this.ClientArea.Controls.Add(this.chkPublic);
            this.ClientArea.Controls.Add(this.cmbStartDate);
            this.ClientArea.Controls.Add(this.cmbEndDate);
            this.ClientArea.Controls.Add(this.cmbReminder);
            this.ClientArea.Controls.Add(this.chkReminder);
            this.ClientArea.Controls.Add(this.chkAllDayEvent);
            this.ClientArea.Controls.Add(this.txtColor);
            this.ClientArea.Controls.Add(this.txtSubject);
            this.ClientArea.Controls.Add(this.txtLocation);
            this.ClientArea.Controls.Add(this.cmbLabel);
            this.ClientArea.Controls.Add(this.dtpStart);
            this.ClientArea.Controls.Add(this.dtpEnd);
            this.ClientArea.Controls.Add(this.lblStartTime);
            this.ClientArea.Controls.Add(this.lblEndTime);
            this.ClientArea.Controls.Add(this.lblSubject);
            this.ClientArea.Controls.Add(this.lblLocation);
            this.ClientArea.Controls.Add(this.lblLabel);
            this.ClientArea.Size = new System.Drawing.Size(989, 593);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(989, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(180, 30);
            this.HeaderText.Text = "Untitled - Event";
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(111, 510);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(92, 40);
            this.cmdCancel.TabIndex = 23;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.AppearanceKey = "actionButton";
            this.cmdOk.Location = new System.Drawing.Point(30, 510);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(61, 40);
            this.cmdOk.TabIndex = 22;
            this.cmdOk.Text = "Ok";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // txtBody
            // 
            this.txtBody.AutoSize = false;
            this.txtBody.BackColor = System.Drawing.SystemColors.Window;
            this.txtBody.LinkItem = null;
            this.txtBody.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtBody.LinkTopic = null;
            this.txtBody.Location = new System.Drawing.Point(30, 385);
            this.txtBody.Multiline = true;
            this.txtBody.Name = "txtBody";
            this.txtBody.Size = new System.Drawing.Size(624, 106);
            this.txtBody.TabIndex = 21;
            // 
            // btnRecurrence
            // 
            this.btnRecurrence.AppearanceKey = "actionButton";
            this.btnRecurrence.Location = new System.Drawing.Point(800, 326);
            this.btnRecurrence.Name = "btnRecurrence";
            this.btnRecurrence.Size = new System.Drawing.Size(132, 40);
            this.btnRecurrence.TabIndex = 20;
            this.btnRecurrence.Text = "Recurrence";
            this.btnRecurrence.Visible = true;
            this.btnRecurrence.Click += new System.EventHandler(this.btnRecurrence_Click);
            // 
            // cboUser
            // 
            this.cboUser.AutoSize = false;
            this.cboUser.BackColor = System.Drawing.SystemColors.Window;
            this.cboUser.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cboUser.FormattingEnabled = true;
            this.cboUser.Location = new System.Drawing.Point(491, 217);
            this.cboUser.Name = "cboUser";
            this.cboUser.Size = new System.Drawing.Size(191, 40);
            this.cboUser.TabIndex = 19;
            this.cboUser.Text = "Combo1";
            // 
            // vsInstallers
            // 
            this.vsInstallers.AllowSelection = false;
            this.vsInstallers.AllowUserToResizeColumns = false;
            this.vsInstallers.AllowUserToResizeRows = false;
            this.vsInstallers.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsInstallers.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsInstallers.BackColorBkg = System.Drawing.Color.Empty;
            this.vsInstallers.BackColorFixed = System.Drawing.Color.Empty;
            this.vsInstallers.BackColorSel = System.Drawing.Color.Empty;
            this.vsInstallers.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
            this.vsInstallers.Cols = 2;
            dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
            this.vsInstallers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsInstallers.ColumnHeadersHeight = 30;
            this.vsInstallers.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.vsInstallers.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsInstallers.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsInstallers.DragIcon = null;
            this.vsInstallers.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsInstallers.ExtendLastCol = true;
            this.vsInstallers.FixedCols = 0;
            this.vsInstallers.FixedRows = 0;
            this.vsInstallers.ForeColorFixed = System.Drawing.Color.Empty;
            this.vsInstallers.FrozenCols = 0;
            this.vsInstallers.GridColor = System.Drawing.Color.Empty;
            this.vsInstallers.GridColorFixed = System.Drawing.Color.Empty;
            this.vsInstallers.Location = new System.Drawing.Point(491, 217);
            this.vsInstallers.Name = "vsInstallers";
            this.vsInstallers.OutlineCol = 0;
            this.vsInstallers.ReadOnly = true;
            this.vsInstallers.RowHeadersVisible = false;
            this.vsInstallers.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsInstallers.RowHeightMin = 0;
            this.vsInstallers.Rows = 0;
            this.vsInstallers.ScrollTipText = null;
            this.vsInstallers.ShowColumnVisibilityMenu = false;
            this.vsInstallers.ShowFocusCell = false;
            this.vsInstallers.Size = new System.Drawing.Size(191, 150);
            this.vsInstallers.StandardTab = true;
            this.vsInstallers.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
            this.vsInstallers.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsInstallers.TabIndex = 17;
            this.vsInstallers.Visible = false;
            this.vsInstallers.KeyDown += new Wisej.Web.KeyEventHandler(this.vsInstallers_KeyDownEvent);
            this.vsInstallers.Click += new System.EventHandler(this.vsInstallers_ClickEvent);
            // 
            // chkMeeting
            // 
            this.chkMeeting.Location = new System.Drawing.Point(703, 275);
            this.chkMeeting.Name = "chkMeeting";
            this.chkMeeting.Size = new System.Drawing.Size(86, 27);
            this.chkMeeting.TabIndex = 15;
            this.chkMeeting.Text = "Meeting";
            // 
            // chkPublic
            // 
            this.chkPublic.Location = new System.Drawing.Point(703, 225);
            this.chkPublic.Name = "chkPublic";
            this.chkPublic.Size = new System.Drawing.Size(72, 27);
            this.chkPublic.TabIndex = 14;
            this.chkPublic.Text = "Public";
            this.chkPublic.CheckedChanged += new System.EventHandler(this.chkPublic_CheckedChanged);
            // 
            // cmbStartDate
            // 
            this.cmbStartDate.AutoSize = false;
            this.cmbStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStartDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.cmbStartDate.FormattingEnabled = true;
            this.cmbStartDate.Location = new System.Drawing.Point(126, 217);
            this.cmbStartDate.Name = "cmbStartDate";
            this.cmbStartDate.Size = new System.Drawing.Size(124, 40);
            this.cmbStartDate.TabIndex = 11;
            // 
            // cmbEndDate
            // 
            this.cmbEndDate.AutoSize = false;
            this.cmbEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEndDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDown;
            this.cmbEndDate.FormattingEnabled = true;
            this.cmbEndDate.Location = new System.Drawing.Point(126, 267);
            this.cmbEndDate.Name = "cmbEndDate";
            this.cmbEndDate.Size = new System.Drawing.Size(124, 40);
            this.cmbEndDate.TabIndex = 10;
            // 
            // cmbReminder
            // 
            this.cmbReminder.AutoSize = false;
            this.cmbReminder.BackColor = System.Drawing.SystemColors.Window;
            this.cmbReminder.FormattingEnabled = true;
            this.cmbReminder.Location = new System.Drawing.Point(140, 326);
            this.cmbReminder.Name = "cmbReminder";
            this.cmbReminder.Size = new System.Drawing.Size(141, 40);
            this.cmbReminder.TabIndex = 9;
            this.cmbReminder.Text = "15 minutes";
            // 
            // chkReminder
            // 
            this.chkReminder.Location = new System.Drawing.Point(30, 335);
            this.chkReminder.Name = "chkReminder";
            this.chkReminder.Size = new System.Drawing.Size(98, 27);
            this.chkReminder.TabIndex = 8;
            this.chkReminder.Text = "Reminder";
            this.chkReminder.CheckedChanged += new System.EventHandler(this.chkReminder_CheckedChanged);
            // 
            // chkAllDayEvent
            // 
            this.chkAllDayEvent.Location = new System.Drawing.Point(30, 180);
            this.chkAllDayEvent.Name = "chkAllDayEvent";
            this.chkAllDayEvent.Size = new System.Drawing.Size(126, 27);
            this.chkAllDayEvent.TabIndex = 7;
            this.chkAllDayEvent.Text = "All Day Event";
            this.chkAllDayEvent.CheckedChanged += new System.EventHandler(this.chkAllDayEvent_CheckedChanged);
            // 
            // txtColor
            // 
            this.txtColor.AutoSize = false;
            this.txtColor.BackColor = System.Drawing.SystemColors.Window;
            this.txtColor.LinkItem = null;
            this.txtColor.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtColor.LinkTopic = null;
            this.txtColor.Location = new System.Drawing.Point(424, 130);
            this.txtColor.LockedOriginal = true;
            this.txtColor.Name = "txtColor";
            this.txtColor.ReadOnly = true;
            this.txtColor.Size = new System.Drawing.Size(55, 40);
            this.txtColor.TabIndex = 3;
            // 
            // txtSubject
            // 
            this.txtSubject.AutoSize = false;
            this.txtSubject.BackColor = System.Drawing.SystemColors.Window;
            this.txtSubject.LinkItem = null;
            this.txtSubject.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSubject.LinkTopic = null;
            this.txtSubject.Location = new System.Drawing.Point(126, 30);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(353, 40);
            this.txtSubject.TabIndex = 2;
            // 
            // txtLocation
            // 
            this.txtLocation.AutoSize = false;
            this.txtLocation.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation.LinkItem = null;
            this.txtLocation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtLocation.LinkTopic = null;
            this.txtLocation.Location = new System.Drawing.Point(126, 80);
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(353, 40);
            this.txtLocation.TabIndex = 1;
            // 
            // cmbLabel
            // 
            this.cmbLabel.AutoSize = false;
            this.cmbLabel.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLabel.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbLabel.FormattingEnabled = true;
            this.cmbLabel.Location = new System.Drawing.Point(126, 130);
            this.cmbLabel.Name = "cmbLabel";
            this.cmbLabel.Size = new System.Drawing.Size(281, 40);
            this.cmbLabel.TabIndex = 0;
            this.cmbLabel.SelectedIndexChanged += new System.EventHandler(this.cmbLabel_SelectedIndexChanged);
            // 
            // dtpStart
            // 
            this.dtpStart.AutoSize = false;
            this.dtpStart.Format = Wisej.Web.DateTimePickerFormat.Time;
            this.dtpStart.Location = new System.Drawing.Point(272, 215);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.ShowCalendar = false;
            this.dtpStart.ShowUpDown = true;
            this.dtpStart.Size = new System.Drawing.Size(135, 40);
            this.dtpStart.TabIndex = 24;
            this.dtpStart.Value = new System.DateTime(2018, 9, 12, 12, 30, 26, 338);
            this.dtpStart.Leave += new System.EventHandler(this.dtpStart_Leave);
            // 
            // dtpEnd
            // 
            this.dtpEnd.AutoSize = false;
            this.dtpEnd.Format = Wisej.Web.DateTimePickerFormat.Time;
            this.dtpEnd.Location = new System.Drawing.Point(272, 265);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.ShowCalendar = false;
            this.dtpEnd.ShowUpDown = true;
            this.dtpEnd.Size = new System.Drawing.Size(135, 40);
            this.dtpEnd.TabIndex = 25;
            this.dtpEnd.Value = new System.DateTime(2018, 9, 12, 12, 30, 26, 342);
            // 
            // lblStartTime
            // 
            this.lblStartTime.Location = new System.Drawing.Point(30, 231);
            this.lblStartTime.Name = "lblStartTime";
            this.lblStartTime.Size = new System.Drawing.Size(75, 17);
            this.lblStartTime.TabIndex = 13;
            this.lblStartTime.Text = "START TIME";
            // 
            // lblEndTime
            // 
            this.lblEndTime.Location = new System.Drawing.Point(30, 281);
            this.lblEndTime.Name = "lblEndTime";
            this.lblEndTime.Size = new System.Drawing.Size(58, 17);
            this.lblEndTime.TabIndex = 12;
            this.lblEndTime.Text = "END TIME";
            // 
            // lblSubject
            // 
            this.lblSubject.Location = new System.Drawing.Point(30, 44);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(58, 17);
            this.lblSubject.TabIndex = 6;
            this.lblSubject.Text = "SUBJECT";
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(30, 94);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(65, 17);
            this.lblLocation.TabIndex = 5;
            this.lblLocation.Text = "LOCATION";
            // 
            // lblLabel
            // 
            this.lblLabel.Location = new System.Drawing.Point(30, 144);
            this.lblLabel.Name = "lblLabel";
            this.lblLabel.Size = new System.Drawing.Size(41, 17);
            this.lblLabel.TabIndex = 4;
            this.lblLabel.Text = "LABEL";
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(424, 231);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(41, 17);
            this.fcLabel1.TabIndex = 26;
            this.fcLabel1.Text = "USER";
            // 
            // frmEditEvent
            // 
            this.AcceptButton = this.cmdOk;
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(989, 653);
            this.FillColor = 0;
            this.Name = "frmEditEvent";
            this.Text = "Untitled - Event";
            this.Load += new System.EventHandler(this.frmEditEvent_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRecurrence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsInstallers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMeeting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPublic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReminder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllDayEvent)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		public FCLabel fcLabel1;
	}
}