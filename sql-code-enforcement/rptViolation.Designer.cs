﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptViolation.
	/// </summary>
	partial class rptViolation
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptViolation));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPageMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPageTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPageDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtPagePage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtIdentifier = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateIssued = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateOccurred = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDateClosed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDoingBusinessAs = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtFee = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBusinessLicense = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtDetail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.SubReport1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtContact = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtContactPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPagePage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIdentifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOccurred)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateClosed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDoingBusinessAs)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessLicense)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContact)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8,
				this.txtIdentifier,
				this.Label2,
				this.txtType,
				this.Label13,
				this.txtStatus,
				this.Label10,
				this.txtDateIssued,
				this.Label14,
				this.txtDateOccurred,
				this.Label15,
				this.txtDateClosed,
				this.Label5,
				this.txtName,
				this.Label6,
				this.txtDoingBusinessAs,
				this.Label7,
				this.txtLocation,
				this.Label16,
				this.txtFee,
				this.Label17,
				this.txtDescription,
				this.Label18,
				this.txtBusinessLicense,
				this.Label19,
				this.txtAddress,
				this.Label20,
				this.txtDetail,
				this.SubReport1,
				this.Label31,
				this.txtContact,
				this.Label32,
				this.txtEmail,
				this.Label33,
				this.txtContactPhone
			});
			this.Detail.Height = 3.229167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuni,
				this.txtTime,
				this.txtDate,
				this.txtPage,
				this.Label12
			});
			this.ReportHeader.Height = 0.59375F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			//
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label23,
				this.Label24,
				this.Label25,
				this.txtPageMuni,
				this.txtPageTime,
				this.txtPageDate,
				this.txtPagePage,
				this.Label30
			});
			this.PageHeader.Height = 0.78125F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Visible = false;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// txtMuni
			// 
			this.txtMuni.Height = 0.1875F;
			this.txtMuni.HyperLink = null;
			this.txtMuni.Left = 0F;
			this.txtMuni.Name = "txtMuni";
			this.txtMuni.Style = "";
			this.txtMuni.Text = null;
			this.txtMuni.Top = 0.03125F;
			this.txtMuni.Width = 2.625F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.HyperLink = null;
			this.txtTime.Left = 0F;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.HyperLink = null;
			this.txtDate.Left = 6.1875F;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0.03125F;
			this.txtDate.Width = 1.25F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.HyperLink = null;
			this.txtPage.Left = 6.1875F;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "text-align: right";
			this.txtPage.Text = "Page 1";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.25F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.21875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 1.25F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label12.Text = "Violation";
			this.Label12.Top = 0.03125F;
			this.Label12.Width = 4.9375F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1770833F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.04166667F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-weight: bold";
			this.Label23.Text = "Date";
			this.Label23.Top = 0.6041667F;
			this.Label23.Width = 0.8125F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1770833F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-weight: bold";
			this.Label24.Text = "Type";
			this.Label24.Top = 0.6041667F;
			this.Label24.Width = 0.8125F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1770833F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 2.8125F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold";
			this.Label25.Text = "Description";
			this.Label25.Top = 0.6041667F;
			this.Label25.Width = 1.34375F;
			// 
			// txtPageMuni
			// 
			this.txtPageMuni.Height = 0.1875F;
			this.txtPageMuni.HyperLink = null;
			this.txtPageMuni.Left = 0F;
			this.txtPageMuni.Name = "txtPageMuni";
			this.txtPageMuni.Style = "";
			this.txtPageMuni.Text = null;
			this.txtPageMuni.Top = 0.03125F;
			this.txtPageMuni.Width = 2.625F;
			// 
			// txtPageTime
			// 
			this.txtPageTime.Height = 0.1875F;
			this.txtPageTime.HyperLink = null;
			this.txtPageTime.Left = 0F;
			this.txtPageTime.Name = "txtPageTime";
			this.txtPageTime.Style = "";
			this.txtPageTime.Text = null;
			this.txtPageTime.Top = 0.25F;
			this.txtPageTime.Width = 1.25F;
			// 
			// txtPageDate
			// 
			this.txtPageDate.Height = 0.1875F;
			this.txtPageDate.HyperLink = null;
			this.txtPageDate.Left = 6.1875F;
			this.txtPageDate.Name = "txtPageDate";
			this.txtPageDate.Style = "text-align: right";
			this.txtPageDate.Text = null;
			this.txtPageDate.Top = 0.03125F;
			this.txtPageDate.Width = 1.25F;
			// 
			// txtPagePage
			// 
			this.txtPagePage.Height = 0.1875F;
			this.txtPagePage.HyperLink = null;
			this.txtPagePage.Left = 6.1875F;
			this.txtPagePage.Name = "txtPagePage";
			this.txtPagePage.Style = "text-align: right";
			this.txtPagePage.Text = null;
			this.txtPagePage.Top = 0.25F;
			this.txtPagePage.Width = 1.25F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.21875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 1.25F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label30.Text = "Violation";
			this.Label30.Top = 0.03125F;
			this.Label30.Width = 4.9375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1770833F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-weight: bold";
			this.Label8.Text = "Identifier";
			this.Label8.Top = 0F;
			this.Label8.Width = 0.8333333F;
			// 
			// txtIdentifier
			// 
			this.txtIdentifier.Height = 0.1875F;
			this.txtIdentifier.Left = 1F;
			this.txtIdentifier.Name = "txtIdentifier";
			this.txtIdentifier.Text = null;
			this.txtIdentifier.Top = 0F;
			this.txtIdentifier.Width = 1.333333F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1770833F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-weight: bold";
			this.Label2.Text = "Type";
			this.Label2.Top = 0.1875F;
			this.Label2.Width = 0.5104167F;
			// 
			// txtType
			// 
			this.txtType.Height = 0.1875F;
			this.txtType.Left = 1F;
			this.txtType.Name = "txtType";
			this.txtType.Text = null;
			this.txtType.Top = 0.1875F;
			this.txtType.Width = 2.322917F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1770833F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-weight: bold";
			this.Label13.Text = "Status";
			this.Label13.Top = 0.375F;
			this.Label13.Width = 0.7708333F;
			// 
			// txtStatus
			// 
			this.txtStatus.Height = 0.1875F;
			this.txtStatus.Left = 1F;
			this.txtStatus.Name = "txtStatus";
			this.txtStatus.Text = null;
			this.txtStatus.Top = 0.375F;
			this.txtStatus.Width = 2.583333F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1770833F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-weight: bold";
			this.Label10.Text = "Date Issued";
			this.Label10.Top = 0F;
			this.Label10.Width = 0.9583333F;
			// 
			// txtDateIssued
			// 
			this.txtDateIssued.Height = 0.1875F;
			this.txtDateIssued.Left = 6.375F;
			this.txtDateIssued.Name = "txtDateIssued";
			this.txtDateIssued.Style = "text-align: right";
			this.txtDateIssued.Text = null;
			this.txtDateIssued.Top = 0F;
			this.txtDateIssued.Width = 1.083333F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1770833F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-weight: bold";
			this.Label14.Text = "Date Occurred";
			this.Label14.Top = 0.1875F;
			this.Label14.Width = 1.083333F;
			// 
			// txtDateOccurred
			// 
			this.txtDateOccurred.Height = 0.1875F;
			this.txtDateOccurred.Left = 6.375F;
			this.txtDateOccurred.Name = "txtDateOccurred";
			this.txtDateOccurred.Style = "text-align: right";
			this.txtDateOccurred.Text = null;
			this.txtDateOccurred.Top = 0.1875F;
			this.txtDateOccurred.Width = 1.083333F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1770833F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5.125F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-weight: bold";
			this.Label15.Text = "Date Closed";
			this.Label15.Top = 0.375F;
			this.Label15.Width = 0.9583333F;
			// 
			// txtDateClosed
			// 
			this.txtDateClosed.Height = 0.1875F;
			this.txtDateClosed.Left = 6.375F;
			this.txtDateClosed.Name = "txtDateClosed";
			this.txtDateClosed.Style = "text-align: right";
			this.txtDateClosed.Text = null;
			this.txtDateClosed.Top = 0.375F;
			this.txtDateClosed.Width = 1.083333F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-weight: bold";
			this.Label5.Text = "Name";
			this.Label5.Top = 1F;
			this.Label5.Width = 0.75F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 1.40625F;
			this.txtName.Name = "txtName";
			this.txtName.Text = null;
			this.txtName.Top = 1F;
			this.txtName.Width = 4.697917F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-weight: bold";
			this.Label6.Text = "Doing Business As";
			this.Label6.Top = 1.1875F;
			this.Label6.Width = 1.333333F;
			// 
			// txtDoingBusinessAs
			// 
			this.txtDoingBusinessAs.Height = 0.1875F;
			this.txtDoingBusinessAs.Left = 1.40625F;
			this.txtDoingBusinessAs.Name = "txtDoingBusinessAs";
			this.txtDoingBusinessAs.Text = null;
			this.txtDoingBusinessAs.Top = 1.1875F;
			this.txtDoingBusinessAs.Width = 4.697917F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-weight: bold";
			this.Label7.Text = "Location";
			this.Label7.Top = 1.5625F;
			this.Label7.Width = 1.333333F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1.40625F;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 1.5625F;
			this.txtLocation.Width = 4.697917F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1770833F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 5.125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold";
			this.Label16.Text = "Fee";
			this.Label16.Top = 0.5625F;
			this.Label16.Width = 0.8333333F;
			// 
			// txtFee
			// 
			this.txtFee.Height = 0.1875F;
			this.txtFee.Left = 6.375F;
			this.txtFee.Name = "txtFee";
			this.txtFee.Style = "text-align: right";
			this.txtFee.Text = null;
			this.txtFee.Top = 0.5625F;
			this.txtFee.Width = 1.083333F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1770833F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold";
			this.Label17.Text = "Description";
			this.Label17.Top = 0.5625F;
			this.Label17.Width = 0.9583333F;
			// 
			// txtDescription
			// 
			this.txtDescription.Height = 0.1875F;
			this.txtDescription.Left = 1F;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Text = null;
			this.txtDescription.Top = 0.5625F;
			this.txtDescription.Width = 4.020833F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold";
			this.Label18.Text = "Business License";
			this.Label18.Top = 1.75F;
			this.Label18.Width = 1.3125F;
			// 
			// txtBusinessLicense
			// 
			this.txtBusinessLicense.Height = 0.1875F;
			this.txtBusinessLicense.Left = 1.40625F;
			this.txtBusinessLicense.Name = "txtBusinessLicense";
			this.txtBusinessLicense.Text = null;
			this.txtBusinessLicense.Top = 1.75F;
			this.txtBusinessLicense.Width = 4.697917F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold";
			this.Label19.Text = "Address";
			this.Label19.Top = 1.375F;
			this.Label19.Width = 0.75F;
			// 
			// txtAddress
			// 
			this.txtAddress.Height = 0.1875F;
			this.txtAddress.Left = 1.40625F;
			this.txtAddress.Name = "txtAddress";
			this.txtAddress.Text = null;
			this.txtAddress.Top = 1.375F;
			this.txtAddress.Width = 4.697917F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold";
			this.Label20.Text = "Detail";
			this.Label20.Top = 1.9375F;
			this.Label20.Width = 0.75F;
			// 
			// txtDetail
			// 
			this.txtDetail.Height = 0.1875F;
			this.txtDetail.Left = 1.40625F;
			this.txtDetail.Name = "txtDetail";
			this.txtDetail.Text = null;
			this.txtDetail.Top = 1.9375F;
			this.txtDetail.Width = 5.96875F;
			// 
			// SubReport1
			// 
			this.SubReport1.CloseBorder = false;
			this.SubReport1.Height = 0.04166667F;
			this.SubReport1.Left = 0F;
			this.SubReport1.Name = "SubReport1";
			this.SubReport1.Report = null;
			this.SubReport1.Top = 3.1875F;
			this.SubReport1.Width = 7.46875F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-weight: bold";
			this.Label31.Text = "Contact";
			this.Label31.Top = 2.3125F;
			this.Label31.Width = 0.75F;
			// 
			// txtContact
			// 
			this.txtContact.Height = 0.1875F;
			this.txtContact.Left = 1.40625F;
			this.txtContact.Name = "txtContact";
			this.txtContact.Text = null;
			this.txtContact.Top = 2.3125F;
			this.txtContact.Width = 4.697917F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-weight: bold";
			this.Label32.Text = "E-Mail";
			this.Label32.Top = 2.5F;
			this.Label32.Width = 1.333333F;
			// 
			// txtEmail
			// 
			this.txtEmail.Height = 0.1875F;
			this.txtEmail.Left = 1.40625F;
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Text = null;
			this.txtEmail.Top = 2.5F;
			this.txtEmail.Width = 4.697917F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-weight: bold";
			this.Label33.Text = "Telephone";
			this.Label33.Top = 2.6875F;
			this.Label33.Width = 1.333333F;
			// 
			// txtContactPhone
			// 
			this.txtContactPhone.Height = 0.1875F;
			this.txtContactPhone.Left = 1.40625F;
			this.txtContactPhone.Name = "txtContactPhone";
			this.txtContactPhone.Text = null;
			this.txtContactPhone.Top = 2.6875F;
			this.txtContactPhone.Width = 4.697917F;
			// 
			// rptViolation
			//
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.479167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPageDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPagePage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtIdentifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateIssued)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateOccurred)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDateClosed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDoingBusinessAs)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBusinessLicense)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContact)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtContactPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtIdentifier;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStatus;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateIssued;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateOccurred;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDateClosed;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDoingBusinessAs;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtFee;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBusinessLicense;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDetail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport SubReport1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContact;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtContactPhone;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPageMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPageTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPageDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtPagePage;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
