//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsDynamicDocument
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION
		//
		// WRITTEN BY     : Corey
		// Date           : 03/28/2005
		//
		// MODIFIED BY    :
		// Last Updated   :
		// ********************************************************
		private FCCollection TagList = new FCCollection();
		private int intCurrentIndex;

		public string GetDataByCode(ref int lngCode, int intReportType = 0)
		{
			string GetDataByCode = "";
			// calls a function which is different for each module
			GetDataByCode = modCEDynamicDocument.DynamicDocumentTextByCode(ref lngCode, intReportType);
			return GetDataByCode;
		}

		public int MoveFirst()
		{
			int MoveFirst = 0;
			int intReturn;
			intCurrentIndex = 0;
			intReturn = MoveNext();
			MoveFirst = intReturn;
			return MoveFirst;
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(TagList))
			{
				if (TagList.Count >= intCurrentIndex + 1)
				{
					while (intCurrentIndex <= TagList.Count)
					{
						intCurrentIndex += 1;
						if (!(TagList[intCurrentIndex] == null))
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
					if (intCurrentIndex > TagList.Count)
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MoveNext = intReturn;
			return MoveNext;
		}

		public int MovePrevious()
		{
			int MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			if (!FCUtils.IsEmpty(TagList))
			{
				if (TagList.Count > 0)
				{
					while (intCurrentIndex > 0)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex > 0)
						{
							if (!(TagList[intCurrentIndex] == null))
							{
								intReturn = intCurrentIndex;
							}
						}
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			MovePrevious = intReturn;
			return MovePrevious;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			if (!FCUtils.IsEmpty(TagList))
			{
				if (intCurrentIndex > TagList.Count)
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
			GetCurrentIndex = intCurrentIndex;
			return GetCurrentIndex;
		}

		public clsDynamicDocumentTag GetCurrentTag()
		{
			clsDynamicDocumentTag GetCurrentTag = null;
			clsDynamicDocumentTag tDDT;
			tDDT = null;
			if (!FCUtils.IsEmpty(TagList))
			{
				if (intCurrentIndex > 0 && intCurrentIndex <= TagList.Count)
				{
					tDDT = TagList[intCurrentIndex];
				}
			}
			GetCurrentTag = tDDT;
			return GetCurrentTag;
		}

		public clsDynamicDocumentTag GetTagByIndex(ref int intIndex)
		{
			clsDynamicDocumentTag GetTagByIndex = null;
			clsDynamicDocumentTag tDDT;
			tDDT = null;
			if (!FCUtils.IsEmpty(TagList))
			{
				if (TagList.Count >= intIndex)
				{
					tDDT = TagList[intIndex];
				}
			}
			GetTagByIndex = tDDT;
			return GetTagByIndex;
		}

		public clsDynamicDocument() : base()
		{
			intCurrentIndex = -1;
		}
		// Public Function GetTagByCode(lngCode As Long, lngMiscID As Long) As clsDynamicDocumentTag
		// Dim tDDT As clsDynamicDocumentTag
		// On Error GoTo ErrorHandler
		// Set tDDT = Nothing
		// If Not IsEmpty(TagList) Then
		// If TagList.Count > 0 Then
		// Set tDDT = ControlList["Code" & lngCode & "ID" & lngMiscID]
		// End If
		// End If
		// Set GetTagByCode = tDDT
		// Exit Function
		// ErrorHandler:
		// Set tDDT = Nothing
		// Set TagByCode = tDDT
		// End Function
		public clsDynamicDocumentTag GetTagByTag(ref string strTag)
		{
			clsDynamicDocumentTag GetTagByTag = null;
			clsDynamicDocumentTag tDDT;
			string strTemp = "";
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				tDDT = null;
				if (!FCUtils.IsEmpty(TagList))
				{
					strTemp = Strings.Replace(strTag, "<", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ">", "", 1, -1, CompareConstants.vbTextCompare);
					// vbPorter upgrade warning: intpos As int	OnWriteFCConvert.ToInt32(
					int intpos = 0;
					intpos = Strings.InStr(1, strTemp, " ", CompareConstants.vbTextCompare);
					if (intpos > 0)
					{
						strTemp = Strings.Mid(strTemp, 1, intpos - 1);
					}
					if (TagList.Count > 0)
					{
						tDDT = (clsDynamicDocumentTag)TagList[strTag];
					}
				}
				GetTagByTag = tDDT;
				return GetTagByTag;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				tDDT = null;
				GetTagByTag = tDDT;
			}
			return GetTagByTag;
		}

		private void InsertTag(ref clsDynamicDocumentTag tTag)
		{
			TagList.Add(tTag, tTag.BaseTag);
		}

		public void AddTag(string strTag, int lngCode, int lngMiscID, string strName, string strDescription, string strCategory, int lngCategoryNO, int lngReportType)
		{
			clsDynamicDocumentTag tTag = new clsDynamicDocumentTag();
			if (Strings.InStr(1, strTag, "<", CompareConstants.vbTextCompare) < 1)
			{
				strTag = "<" + strTag + ">";
			}
			strTag = fecherFoundation.Strings.UCase(strTag);
			tTag.Category = strCategory;
			tTag.CategoryNO = lngCategoryNO;
			tTag.Code = lngCode;
			tTag.Description = strDescription;
			tTag.MiscID = lngMiscID;
			tTag.Tag = strTag;
			tTag.TagName = strName;
			InsertTag(ref tTag);
		}

		public void SetupTags()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsLoad = new clsDRWrapper();
				AddTag("FullName", 1, 0, "Full Name", "Owner's Full Name", "Owner", 1, 0);
				AddTag("2ndFullName", 2, 0, "2nd Full Name", "Second Owner's Full Name", "Owner", 1, 0);
				AddTag("LastFirst", 41, 0, "Last, First", "Owner's Last, First", "Owner", 1, 0);
				AddTag("2ndLastFirst", 42, 0, "2nd Last, First", "Second Owner's Last, First", "Owner", 1, 0);
				AddTag("FirstName", 3, 0, "First Name", "Owner's First Name", "Owner", 1, 0);
				AddTag("MiddleName", 4, 0, "Middle Name", "Owner's Middle Name", "Owner", 1, 0);
				AddTag("LastName", 5, 0, "Last Name", "Owner's Last Name", "Owner", 1, 0);
				AddTag("Designation", 6, 0, "Designation", "Owner's Designation", "Owner", 1, 0);
				AddTag("2ndFirstName", 7, 0, "2nd First Name", "Second Owner's First Name", "Owner", 1, 0);
				AddTag("2ndMiddleName", 8, 0, "2nd Middle Name", "Second Owner's Middle Name", "Owner", 1, 0);
				AddTag("2ndLastName", 9, 0, "2nd Last Name", "Second Owner's Last Name", "Owner", 1, 0);
				AddTag("2ndDesignation", 10, 0, "2nd Designation", "Second Owner's Designation", "Owner", 1, 0);
				AddTag("MailingAddress", 11, 0, "Mailing Address", "Mailing Address", "Owner", 1, 0);
				AddTag("Address1", 12, 0, "Address 1", "Address Line 1", "Owner", 1, 0);
				AddTag("Address2", 13, 0, "Address 2", "Address Line 2", "Owner", 1, 0);
				AddTag("City", 14, 0, "City", "City", "Owner", 1, 0);
				AddTag("State", 15, 0, "State", "State", "Owner", 1, 0);
				AddTag("Zip", 16, 0, "Zip Code", "Zip Code", "Owner", 1, 0);
				AddTag("Account", 17, 0, "Account", "Account Number", "Property", 2, 0);
				AddTag("MapLot", 18, 0, "Map/Lot", "Map and Lot", "Property", 2, 0);
				AddTag("StreetNumber", 19, 0, "Street Number", "Street Number", "Property", 2, 0);
				AddTag("Apartment", 20, 0, "Apartment", "Apartment", "Property", 2, 0);
				AddTag("StreetName", 21, 0, "Street Name", "Street Name", "Property", 2, 0);
				AddTag("Acreage", 22, 0, "Acreage", "Acreage", "Property", 2, 0);
				AddTag("Frontage", 23, 0, "Frontage", "Frontage", "Property", 2, 0);
				AddTag("Neighborhood", 24, 0, "Neighborhood", "Neighborhood", "Property", 2, 0);
				AddTag("Zone", 25, 0, "Zone", "Zone", "Property", 2, 0);
				rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPROPERTY) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					while (!rsLoad.EndOfFile())
					{
						AddTag("PropertyUD" + rsLoad.Get_Fields_Int32("ID"), 43, rsLoad.Get_Fields_Int32("ID"), rsLoad.Get_Fields_String("description"), "", "Property User Data", 6, 0);
						rsLoad.MoveNext();
					}
				}
				AddTag("PermitYear", 26, 0, "Permit Year", "Permit Year", "Permit", 3, 0);
				AddTag("Permit", 27, 0, "Permit", "Permit", "Permit", 3, 0);
				AddTag("ApplicantName", 52, 0, "Applicant Name", "Applicant Name", "Permit", 3, 0);
				AddTag("ApplicationDate", 28, 0, "Application Date", "Application Date", "Permit", 3, 0);
				AddTag("PermitType", 29, 0, "Type", "Type of Permit", "Permit", 3, 0);
				AddTag("PermitCategory", 51, 0, "Category", "Permit Category", "Permit", 3, 0);
				AddTag("PermitStatus", 30, 0, "Status", "Status of Permit", "Permit", 3, 0);
				AddTag("PermitDescription", 31, 0, "Description", "Permit Description", "Permit", 3, 0);
				AddTag("PermitPlan", 32, 0, "Plan", "Plan", "Permit", 3, 0);
				AddTag("PermitFee", 33, 0, "Fee", "Fee", "Permit", 3, 0);
				// Call AddTag("PermitContractor", 40, 0, "Permit Contractor", "Permit Contractor", "Permit", 3, 0)
				AddTag("PermitStartDate", 47, 0, "Permit Start Date", "Permit Start Date", "Permit", 3, 0);
				AddTag("PermitEndDate", 48, 0, "Permit End Date", "Permit End Date", "Permit", 3, 0);
				AddTag("PermitValue", 53, 0, "Value", "Value", "Permit", 3, 0);
				AddTag("PermitContact", 54, 0, "Contact Name", "Contact Name", "Permit", 3, 0);
				AddTag("PermitContactEmail", 55, 0, "Contact Email", "Contact Email", "Permit", 3, 0);
				AddTag("PermitCommResCode", 56, 0, "Commercial / Residential", "Commercial / Residential", "Permit", 3, 0);
				AddTag("PermitOCCDate", 57, 0, "Certificate of Occupancy Date", "Certificate of Occupancy Date", "Permit", 3, 0);
				clsDRWrapper rsTemp = new clsDRWrapper();
				rsTemp.OpenRecordset("select * FROM systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!rsTemp.EndOfFile())
				{
					rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMIT) + " AND SUBTYPE = " + rsTemp.Get_Fields_Int32("ID") + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
					while (!rsLoad.EndOfFile())
					{
						AddTag("PermitUD" + rsLoad.Get_Fields_Int32("ID"), 44, rsLoad.Get_Fields_Int32("ID"), rsLoad.Get_Fields_String("description"), "", rsTemp.Get_Fields_String("description") + " Permit User Data", 7, 0);
						rsLoad.MoveNext();
					}
					rsTemp.MoveNext();
				}
				AddTag("InspectionType", 34, 0, "Type", "Inspection Type", "Inspection", 4, 0);
				AddTag("InspectionStatus", 35, 0, "Status", "Inspection Status", "Inspection", 4, 0);
				AddTag("InspectionDate", 36, 0, "Date", "Inspection Date", "Inspection", 4, 0);
				AddTag("Inspector", 37, 0, "Inspector", "Inspector", "Inspection", 4, 0);
				// Call rsLoad.OpenRecordset("select * from usercodes where codetype = " & CNSTCODETYPEINSPECTION & " order by orderno", strCEDatabase)
				// If Not rsLoad.EndOfFile Then
				// Do While Not rsLoad.EndOfFile
				// Call AddTag("InspectionUD" & rsLoad.Fields("ID"), 45, rsLoad.Fields("ID"), rsLoad.Fields("description"), "", "Inspection User Data", 8, 0)
				// rsLoad.MoveNext
				// Loop
				// End If
				rsTemp.OpenRecordset("select * from systemcodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEPERMITTYPE) + " and not booloption = 1 order by description", modGlobalVariables.Statics.strCEDatabase);
				while (!rsTemp.EndOfFile())
				{
					rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPEINSPECTION) + " and subtype = " + rsTemp.Get_Fields_Int32("ID") + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
					if (!rsLoad.EndOfFile())
					{
						while (!rsLoad.EndOfFile())
						{
							AddTag("InspectionUD" + rsLoad.Get_Fields_Int32("ID"), 45, rsLoad.Get_Fields_Int32("ID"), rsLoad.Get_Fields_String("description"), "", rsTemp.Get_Fields_String("description") + " Inspection User Data", 8, 0);
							rsLoad.MoveNext();
						}
					}
					rsTemp.MoveNext();
				}
				AddTag("ContractorName", 38, 0, "Name", "Contractor Name", "Contractor", 5, 0);
				AddTag("ContractorName2", 39, 0, "Name 2", "Contractor Name 2", "Contractor", 5, 0);
				rsLoad.OpenRecordset("select * from usercodes where codetype = " + FCConvert.ToString(modCEConstants.CNSTCODETYPECONTRACTOR) + " order by orderno", modGlobalVariables.Statics.strCEDatabase);
				if (!rsLoad.EndOfFile())
				{
					while (!rsLoad.EndOfFile())
					{
						AddTag("ContractorUD" + rsLoad.Get_Fields_Int32("ID"), 46, rsLoad.Get_Fields_Int32("ID"), rsLoad.Get_Fields_String("description"), "", "Contractor User Data", 9, 0);
						rsLoad.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SetupTags", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
