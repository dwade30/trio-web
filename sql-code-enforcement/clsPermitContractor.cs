﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsPermitContractor
	{
		//=========================================================
		private int lngPermitID;
		private string strName = string.Empty;
		private string strAddress1 = string.Empty;
		private string strAddress2 = string.Empty;
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZip4 = string.Empty;
		private int lngContractorID;
		private string strEmail = string.Empty;
		private int lngID;
		private string strNote = string.Empty;
		private bool boolDeleted;
		private clsPhoneList PhoneList = new clsPhoneList();
		private int lngOrderNumber;

		public int OrderNumber
		{
			set
			{
				lngOrderNumber = value;
			}
			get
			{
				int OrderNumber = 0;
				OrderNumber = lngOrderNumber;
				return OrderNumber;
			}
		}

		public bool Deleted
		{
			set
			{
				boolDeleted = value;
			}
			get
			{
				bool Deleted = false;
				Deleted = boolDeleted;
				return Deleted;
			}
		}

		public int PermitID
		{
			set
			{
				lngPermitID = PermitID;
			}
			get
			{
				int PermitID = 0;
				PermitID = lngPermitID;
				return PermitID;
			}
		}

		public string ContractorName
		{
			set
			{
				strName = value;
			}
			get
			{
				string ContractorName = "";
				ContractorName = strName;
				return ContractorName;
			}
		}

		public string Address1
		{
			set
			{
				strAddress1 = value;
			}
			get
			{
				string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}

		public string Address2
		{
			set
			{
				strAddress2 = value;
			}
			get
			{
				string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string Zip4
		{
			set
			{
				strZip4 = value;
			}
			get
			{
				string Zip4 = "";
				Zip4 = strZip4;
				return Zip4;
			}
		}

		public string Email
		{
			set
			{
				strEmail = value;
			}
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
		}

		public int ContractorID
		{
			set
			{
				lngContractorID = value;
			}
			get
			{
				int ContractorID = 0;
				ContractorID = lngContractorID;
				return ContractorID;
			}
		}

		public int ID
		{
			set
			{
				lngID = value;
				PhoneList.ParentID = lngID;
			}
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
		}

		public string Note
		{
			set
			{
				strNote = value;
			}
			get
			{
				string Note = "";
				Note = strNote;
				return Note;
			}
		}

		public void LoadPhoneNumbers()
		{
			PhoneList.PhoneCode = modCEConstants.CNSTPHONETYPEPERMITCONTRACTOR;
			PhoneList.ParentID = lngID;
			PhoneList.LoadNumbers();
		}

		public bool SavePhoneNumbers()
		{
			bool SavePhoneNumbers = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SavePhoneNumbers = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				int intIndex;
				if (!(PhoneList == null))
				{
					PhoneList.PhoneCode = modCEConstants.CNSTPHONETYPEPERMITCONTRACTOR;
					PhoneList.ParentID = lngID;
					PhoneList.SaveNumbers();
					// Dim tPN As clsPhoneNumber
					// For intIndex = 1 To PhoneList.GetCount
					// Set tPN = PhoneList.GetPhoneNumberByIndex(intIndex)
					// If Not tPN Is Nothing Then
					// If Not tPN.Deleted Then
					// Call rsSave.OpenRecordset("select * from phonenumbers where ID = " & tPN.ID, strCEDatabase)
					// If Not rsSave.EndOfFile Then
					// rsSave.Edit
					// Else
					// rsSave.AddNew
					// tPN.ID = rsSave.Fields("ID")
					// End If
					// rsSave.Fields("PhoneCode") = CNSTPHONETYPEPERMITCONTRACTOR
					// rsSave.Fields("ParentID") = lngID
					// rsSave.Fields("PhoneNumber") = tPN.PhoneNumber
					// rsSave.Fields("phonedescription") = tPN.Description
					// rsSave.Update
					// Else
					// If tPN.ID > 0 Then
					// Call rsSave.Execute("delete from phonenumbers where ID = " & tPN.ID, strCEDatabase)
					// End If
					// End If
					// End If
					// Next intIndex
					// PhoneList.ClearDeleted
				}
				SavePhoneNumbers = true;
				return SavePhoneNumbers;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In SavePhoneNumbers", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SavePhoneNumbers;
		}

		public void SetPhoneNumberList(ref clsPhoneList pList)
		{
			PhoneList = null;
			PhoneList = pList;
		}

		public clsPhoneList PhoneNumberList
		{
			get
			{
				clsPhoneList PhoneNumberList = null;
				PhoneNumberList = PhoneList;
				return PhoneNumberList;
			}
		}

		public clsPermitContractor() : base()
		{
			PhoneList.PhoneCode = modCEConstants.CNSTPHONETYPEPERMITCONTRACTOR;
		}
	}
}
