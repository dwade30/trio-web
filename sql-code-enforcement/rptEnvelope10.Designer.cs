﻿namespace TWCE0000
{
	/// <summary>
	/// Summary description for rptEnvelope10.
	/// </summary>
	partial class rptEnvelope10
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEnvelope10));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtReturnAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMailingAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtReturnAddress,
            this.txtMailingAddress});
			this.Detail.Height = 3.125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtReturnAddress
			// 
			this.txtReturnAddress.Height = 0.9791667F;
			this.txtReturnAddress.Left = 0.0625F;
			this.txtReturnAddress.Name = "txtReturnAddress";
			this.txtReturnAddress.Text = null;
			this.txtReturnAddress.Top = 0.07291666F;
			this.txtReturnAddress.Width = 2.90625F;
			// 
			// txtMailingAddress
			// 
			this.txtMailingAddress.Height = 1.354167F;
			this.txtMailingAddress.Left = 3.0625F;
			this.txtMailingAddress.Name = "txtMailingAddress";
			this.txtMailingAddress.Text = null;
			this.txtMailingAddress.Top = 1.5F;
			this.txtMailingAddress.Width = 4.96875F;
			// 
			// rptEnvelope10
			// 
			this.MasterReport = false;
			this.PageSettings.DefaultPaperSource = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperSource = System.Drawing.Printing.PaperSourceKind.FormSource;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.416667F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMailingAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReturnAddress;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMailingAddress;
	}
}
