//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWCE0000
{
	public class MDIParent //: BaseForm
	{
        public FCCommonDialog CommonDialog1_Open;
        public FCCommonDialog CommonDialog1;
        public Wisej.Web.ImageList ImageList2;
        public Wisej.Web.ImageList CEImageList;
        const string strTrio = "TRIO Software";

        public MDIParent()
		{
			//
			// required for windows form designer support
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// todo: add any constructor code after initializecomponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;

            this.CommonDialog1_Open = new FCCommonDialog();
            this.CommonDialog1 = new FCCommonDialog();
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(MDIParent));
            this.ImageList2 = new Wisej.Web.ImageList();
            this.CEImageList = new Wisej.Web.ImageList();
            this.ImageList2.ImageSize = new System.Drawing.Size(32, 32);
            //this.ImageList2.ColorDepth = Wisej.Web.ColorDepth.Depth8Bit;
            this.ImageList2.TransparentColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
            this.ImageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList2.ImageStream")));
            this.CEImageList.ImageSize = new System.Drawing.Size(16, 16);
            //this.CEImageList.ColorDepth = Wisej.Web.ColorDepth.Depth8Bit;
            this.CEImageList.TransparentColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(192)));
            this.CEImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("CEImageList.ImageStream")));
            this.CEImageList.Images.SetKeyName(0, "Document");
            this.CEImageList.Images.SetKeyName(1, "Attachment");
        }
		/// <summary>
		/// default instance for form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;
		//=========================================================
		const int CNSTMAINMENUID = 0;
		const int CNSTFILEMAINTENANCEMENUID = 1;
		const int CNSTPRINTMENUID = 2;
		const int CNSTDESCRIPTIONRECORDSMENUID = 3;
		const int CNSTACCOUNTMENUID = 4;
		private int intExit;
		public bool blnProgramEnding;

		private void SetMenuOptions(int intMenu)
		{
			//GRID.ColData(1, string.Empty);
			//// clears the whole col
            //FC:FINAL:AM:#1959 - don't close the opened forms
			//CloseChildForms();
			//GRID.Tag = (System.Object)(intMenu);
			//switch (intMenu)
			//{
			//	case CNSTMAINMENUID:
			//		{
			//			MainCaptions();
			//			break;
			//		}
			//	case CNSTFILEMAINTENANCEMENUID:
			//		{
			//			//FileMaintCaptions();
			//			break;
			//		}
			//	case CNSTPRINTMENUID:
			//		{
			//			//PrintCaptions();
			//			break;
			//		}
			//	case CNSTDESCRIPTIONRECORDSMENUID:
			//		{
			//			DescriptionRecordCaptions();
			//			break;
			//		}
			//	case CNSTACCOUNTMENUID:
			//		{
			//			AccountFunctionCaptions();
			//			break;
			//		}
			//}
			//end switch
		}

		public void CloseChildForms()
		{
			//FC:FINAL:DDU:cannot close a form from foreach enumeration
			//foreach (FCForm frm in FCGlobal.Statics.Forms)
			//{
			//	if (frm.Name != "MDIParent")
			//	{
			//		frm.Close();
			//	}
			//}
			FCUtils.CloseFormsOnProject();
		}

		//private void MDIParent_Activated(object sender, System.EventArgs e)
		//{
		//	this.StatusBar1.Panels[0].Width = GRID.WidthOriginal;
		//	//modGlobalFunctions.SetHelpMenuOptions();
		//	AutoSizeMenu();
		//	if (!(this.WindowState == FormWindowState.Minimized))
		//	{
		//		picArchive.WidthOriginal = this.WidthOriginal - GRID.WidthOriginal - 135;
		//		modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
		//	}
		//}

		//private void MDIParent_Load(object sender, System.EventArgs e)
        public void Init()
        {
			//Begin Unmaped Properties
			//MDIParent properties;
			//MDIParent.LinkMode	= 1  'Source;
			//MDIParent.LinkTopic	= "mdiparent";
			//MDIParent.LockControls	= -1  'True;
			//End Unmaped Properties
			string strReturn = "";
			int intUserID;
			modGlobalFunctions.LoadTRIOColors();
            //this.Text = strTrio + " - Code Enforcement";
            App.MainForm.Caption = "CODE ENFORCEMENT";
            App.MainForm.ApplicationIcon = "icon-code-enforcement";
            App.MainForm.menuTree.ImageList = CreateImageList();
            string assemblyName = this.GetType().Assembly.GetName().Name;
            if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
            {
                App.MainForm.ApplicationIcons.Add(assemblyName, "icon-code-enforcement");
            }
            App.MainForm.NavigationMenu.Owner = this;
            //modGlobalFunctions.SetMenuBackColor();
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, GRID.Cols - 1, GRID.BackColor);
			modGlobalConstants.Statics.TRIOCOLORBLUE = Information.RGB(0, 0, 200);
			//this.GRID.ColWidth(0, 200);
			//this.GRID.ColWidth(1, 2400);
			//GRID.ColHidden(2, true);
			//GRID.ColHidden(3, true);
			//GRID.Width = 2700;
			//GRID.Select(0, 1);
			//FC:FINAL:MSH - issue #1886: change status bar text
			//this.StatusBar1.Panels[0].Text = modGlobalConstants.Statics.MuniName;
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			if (strReturn == string.Empty)
				strReturn = "False";
			modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
			//mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
			//SetMenuOptions(CNSTMAINMENUID);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);
			modRegistry.GetKeyValues(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "securityid", ref strReturn);
			intUserID = FCConvert.ToInt32(Math.Round(Conversion.Val(strReturn + "")));
			modRegistry.GetKeyValues(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", ref strReturn);
			if (strReturn == string.Empty)
				strReturn = "False";
			modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
			//mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
            App.MainForm.NavigationMenu.OriginName = "Code Enforcement";
			MainCaptions();
            frmCalendar.InstancePtr.LoadForm();
            //SetMenuOptions(CNSTMAINMENUID);
            //! Load frmCalendar;
        }

        public ImageList CreateImageList()
        {
            ImageList imageList = new ImageList();
            imageList.ImageSize = new System.Drawing.Size(18, 18);
            imageList.Images.AddRange(new ImageListEntry[] {
                new ImageListEntry("menutree-property", "property"),
                new ImageListEntry("menutree-permits", "permits"),
                new ImageListEntry("menutree-inspections", "inspections"),
                new ImageListEntry("menutree-printing", "printing"),
                new ImageListEntry("menutree-violations", "violations"),
                new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
                new ImageListEntry("menutree-contractors", "contractors"),
                new ImageListEntry("menutree-property-active", "account-property-active"),
                new ImageListEntry("menutree-permits-active", "permits-active"),
                new ImageListEntry("menutree-inspections-active", "inspections-active"),
                new ImageListEntry("menutree-printing-active", "printing-active"),
                new ImageListEntry("menutree-violations-active", "violations-active"),
                new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
                new ImageListEntry("menutree-contractors-active", "contractors-active"),
            });
            return imageList;
        }
        // vbPorter upgrade warning: intMenu As int	OnWriteFCConvert.ToInt32(
        public void MainActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmGetRecords.InstancePtr.Init(modCEConstants.CNSTSEARCHPROPERTY);
						break;
					}
				case 2:
					{
						frmGetRecords.InstancePtr.Init(modCEConstants.CNSTSEARCHPERMIT);
						break;
					}
				case 3:
					{
						frmGetRecords.InstancePtr.Init(modCEConstants.CNSTSEARCHINSPECTION);
						break;
					}
				case 4:
					{
						frmGetRecords.InstancePtr.Init(modCEConstants.CNSTSEARCHCONTRACTOR);
						break;
					}
				case 6:
					{
						SetMenuOptions(CNSTPRINTMENUID);
						break;
					}
				case 7:
					{
						SetMenuOptions(CNSTFILEMAINTENANCEMENUID);
						break;
					}
				case 8:
					{
						mnuFExit_Click();
						break;
					}
				case 5:
					{
						// violations
						frmGetRecords.InstancePtr.Init(modCEConstants.CNSTSEARCHVIOLATION);
						break;
					}
			}
			//end switch
		}

		private object MainCaptions()
		{
			object MainCaptions = null;
			int CurRow;
			string strTemp = "";
			int lngFCode;
			//MDIParent.InstancePtr.Text = "TRIO Software - Code Enforcement       [Main]";
			ClearLabels();
			ClearMenuItems();
			CurRow = 1;
			AddMenuItem(ref CurRow, 1, "Property", modCEConstants.CNSTPROPERTYSCREEN, imageKey: "property");
			AddMenuItem(ref CurRow, 2, "Permits", modCEConstants.CNSTPERMITSECURITY, imageKey: "permits");
			AddMenuItem(ref CurRow, 3, "Inspections", modCEConstants.CNSTINSPECTIONSECURITY, imageKey: "inspections");
			AddMenuItem(ref CurRow, 4, "Contractors", modCEConstants.CNSTCONTRACTORSECURITY, imageKey: "contractors");
			AddMenuItem(ref CurRow, 8, "Violations", modCEConstants.CNSTVIOLATIONSSECURITY, imageKey: "violations");
			var navigationItem = AddMenuItem(ref CurRow, 5, "Printing", modCEConstants.CNSTPRINTINGSECURITY, imageKey: "printing");
            PrintCaptions(navigationItem);
			navigationItem = AddMenuItem(ref CurRow, 6, "File Maintenance", modCEConstants.CNSTFILEMAINTSECURITY, "M", imageKey: "file-maintenance");
            FileMaintCaptions(navigationItem);
			//AddMenuItem(ref CurRow, 7, "Exit Code Enforcement", 0, "X");
			AutoSizeMenu();
			return MainCaptions;
		}

		private object PrintCaptions(FCMenuItem parentMenu)
		{
            string menu = "PRINT";
			object PrintCaptions = null;
			int CurRow;
			string strTemp = "";
			int lngFCode;
			//MDIParent.InstancePtr.Text = "TRIO Software - Code Enforcement       [Printing]";
			ClearLabels();
			ClearMenuItems();
			CurRow = 1;
			AddMenuItem(ref CurRow, 1, "Reports", 0, "", "Printing and options for all reports, forms etc.", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 6, "Custom Reports", modCEConstants.CNSTPRINTINGEDITSECURITY, "", "Reports whose data and format are user defined", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 2, "Custom Forms", modCEConstants.CNSTPRINTINGEDITSECURITY, "", "Forms whose data, format, size etc. are user defined", menu: menu, parentMenu: parentMenu);
			// Call AddMenuItem(CurRow, 3, "Print Custom Forms")
			AddMenuItem(ref CurRow, 4, "Dynamic Documents", modCEConstants.CNSTPRINTINGEDITSECURITY, "", "Text documents that can have variable content", menu: menu, parentMenu: parentMenu);
			//AddMenuItem(ref CurRow, 5, "Return to Main", 0, "X");
			AutoSizeMenu();
			return PrintCaptions;
		}

		private void ClearMenuItems()
		{
			//int CurRow;
			//for (CurRow = 0; CurRow <= 20; CurRow++)
			//{
			//	GRID.RowData(CurRow, false);
			//	GRID.TextMatrix(CurRow, 1, "");
			//	GRID.TextMatrix(CurRow, 2, FCConvert.ToString(0));
			//	GRID.TextMatrix(CurRow, 3, "");
			//}
			//// CurRow
		}
		// vbPorter upgrade warning: intRow As int	OnRead(string)
		private FCMenuItem AddMenuItem(ref int intRow, int intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", string menu = "MAIN", FCMenuItem parentMenu = null, string imageKey = "")
		{
			// vbPorter upgrade warning: strOption As string	OnWrite(string, int)
			string strOption;
            //string menu = "MAIN";
            strOption = strForceOption;
			if (strOption == string.Empty)
			{
				if (intRow < 10)
				{
					strOption = FCConvert.ToString(intRow);
				}
				else
				{
					switch (intRow)
					{
						case 10:
							{
								strOption = "A";
								break;
							}
						case 11:
							{
								strOption = "B";
								break;
							}
						case 12:
							{
								strOption = "C";
								break;
							}
						case 13:
							{
								strOption = "D";
								break;
							}
						case 14:
							{
								strOption = "E";
								break;
							}
						case 15:
							{
								strOption = "F";
								break;
							}
						case 16:
							{
								strOption = "G";
								break;
							}
						case 17:
							{
								strOption = "H";
								break;
							}
						case 18:
							{
								strOption = "I";
								break;
							}
						case 19:
							{
								strOption = "J";
								break;
							}
						case 20:
							{
								strOption = "K";
								break;
							}
						case 21:
							{
								strOption = "L";
								break;
							}
						case 22:
							{
								strOption = "M";
								break;
							}
					}
					//end switch
				}
			}
			strOption += ". " + strCaption;
			//GRID.TextMatrix(intRow, 1, strOption);
			//GRID.TextMatrix(intRow, 2, FCConvert.ToString(intCode));
			//GRID.TextMatrix(intRow, 3, strToolTip);
			//GRID.RowData(intRow, true);
            bool boolDisable = false;
            if (lngFCode != 0)
			{
                boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
                modMDIParent.EnableDisableMenuOption(!boolDisable, intRow);
			}
            FCMenuItem newItem = null;
            if (parentMenu == null)
            {
                newItem = App.MainForm.NavigationMenu.Add(strCaption, "MenuActions:" + intRow, menu, !boolDisable, 1, imageKey);
            }
            else
            {
                newItem = parentMenu.SubItems.Add(strCaption, "MenuActions:" + intRow, menu, !boolDisable, parentMenu.Level + 1, imageKey);
            }
            intRow += 1;

            return newItem;
		}

		private void DescriptionRecordCaptions(FCMenuItem parentMenu)
		{
			int CurRow;
			string strTemp = "";
			int lngFCode;
            string menu = "DESCRIPTION RECORD";
			//MDIParent.InstancePtr.Text = "TRIO Software - Code Enforcement        [Description Records]";
			ClearLabels();
			ClearMenuItems();
			CurRow = 1;
			AddMenuItem(ref CurRow, 1, "User Defined", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 2, "System Defined", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 3, "Inspectors", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 5, "Permit Checklists", menu: menu, parentMenu: parentMenu);
			//AddMenuItem(ref CurRow, 4, "Return to File Maintenance", 0, "X");
			AutoSizeMenu();
		}
		// vbPorter upgrade warning: intMenu As int	OnWriteFCConvert.ToInt32(
		private void AccountFunctionActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						UpdateCEFromRE();
						break;
					}
				case 2:
					{
						frmCreateAssociations.InstancePtr.Init();
						break;
					}
				case 4:
					{
						SetMenuOptions(CNSTFILEMAINTENANCEMENUID);
						break;
					}
				case 3:
					{
						frmUndelete.InstancePtr.Show(App.MainForm);
						break;
					}
			}
			//end switch
		}

		private void AccountFunctionCaptions(FCMenuItem parentMenu)
		{
			int CurRow;
			string strTemp = "";
			int lngFCode;
            string menu = "ACCOUNT FUNCTION";
			//MDIParent.InstancePtr.Text = "TRIO Software - Code Enforcement        [Property Functions]";
			ClearLabels();
			ClearMenuItems();
			CurRow = 1;
			AddMenuItem(ref CurRow, 1, "Update From Real Estate", 0, "", "Copy information from real estate to associated CE records", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 2, "Create Associations from RE", 0, "", "Associate CE records to real estate records based on location or map and lot", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 4, "Undelete Records", menu: menu, parentMenu: parentMenu);
			//AddMenuItem(ref CurRow, 3, "Return to File Maintenance", 0, "X");
			AutoSizeMenu();
		}

		private void FileMaintCaptions(FCMenuItem parentMenu)
		{
            string menu = "FILE MAINTENANCE";
			int CurRow;
			string strTemp = "";
			int lngFCode;
			//MDIParent.InstancePtr.Text = "TRIO Software - Code Enforcement        [File Maintenance]";
			ClearLabels();
			ClearMenuItems();
			CurRow = 1;
			AddMenuItem(ref CurRow, 1, "Customize", modCEConstants.CNSTCUSTOMIZESECURITY, menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 2, "Check Database Structure", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 9, "Calendar", menu: menu, parentMenu: parentMenu);
			var navigationItem = AddMenuItem(ref CurRow, 3, "Description Records", modCEConstants.CNSTDESCRIPTIONRECORDS, menu: menu, parentMenu: parentMenu);
            DescriptionRecordCaptions(navigationItem);
			navigationItem = AddMenuItem(ref CurRow, 5, "Property Functions", modCEConstants.CNSTACCOUNTFUNCTIONSSECURITY, menu: menu, parentMenu: parentMenu);
            AccountFunctionCaptions(navigationItem);
            AddMenuItem(ref CurRow, 10, "Return Address", modCEConstants.CNSTCUSTOMIZESECURITY, menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 8, "Signature File", menu: menu, parentMenu: parentMenu);
			AddMenuItem(ref CurRow, 7, "Clear Database Locks", 0, "", "Used to clear " + FCConvert.ToString(Convert.ToChar(34)) + "stuck" + FCConvert.ToString(Convert.ToChar(34)) + " locks.  Clear them if locks are perpetually locked and records can't be created", menu: menu, parentMenu: parentMenu);
			//AddMenuItem(ref CurRow, 6, "Return to Main", 0, "X");
			AutoSizeMenu();
		}

		private void MDIForm_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			blnProgramEnding = true;
		}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			blnProgramEnding = true;
			modReplaceWorkFiles.EntryFlagFile(true, "CE");
            //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
            //Interaction.Shell(App.Path + "\\TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Normal, false, -1);
            App.MainForm.OpenModule("TWGNENTY");
            // Unload MDIParent
            frmCalendar.InstancePtr.Unload();
			//FC:FINAL:IPI
            //Application.Exit();
		}

		private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twbd0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twcr0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuClerkHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twck0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twce0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twe90000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuFExit_Click(object sender, System.EventArgs e)
		{
			this.blnProgramEnding = true;
			//Close();
		}

		public void mnuFExit_Click()
		{
			//mnuFExit_Click(mnuFExit, new System.EventArgs());
		}

		private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twgn0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuHAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout.InstancePtr.Show();
		}

		private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twmv0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		//private void mnuOMaxForms_Click(object sender, System.EventArgs e)
		//{
		//	modRegistry.RegCreateKeyWrp(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, 0);
		//	if (mnuOMaxForms.Checked)
		//	{
		//		modRegistry.SaveKeyValue(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "False");
		//	}
		//	else
		//	{
		//		modRegistry.SaveKeyValue(modGlobalVariables.HKEY_LOCAL_MACHINE, modGlobalConstants.REGISTRYKEY, "Maximize Forms", "True");
		//	}
		//	modGlobalConstants.Statics.boolMaxForms = !modGlobalConstants.Statics.boolMaxForms;
		//	//mnuOMaxForms.Checked = !mnuOMaxForms.Checked;
		//}

		private void AutoSizeMenu()
		{
			int intCounter;
			// vbPorter upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
			int FullRowHeight;
			// this will store the height of the labels with text
			int intLabels;
			intLabels = 20;
			// Grid.RowHeight(0) = 0       'the first row in the grid is height = 0
			//FullRowHeight = FCConvert.ToInt32(GRID.HeightOriginal / (intLabels + (((21 - intLabels) / 3.0) / 2)));
			for (intCounter = 1; intCounter <= 20; intCounter++)
			{
				// this just assigns the row height
				// If Grid.TextMatrix(intCounter, 1) = vbNullString Then
				// Grid.RowHeight(intCounter) = (FullRowHeight / 3) - 55
				// -50 is just a small adjustment
				// Else
				//GRID.RowHeight(intCounter, FullRowHeight + 15);
				// +20 minor adjustment to help with two row labels
				// End If
			}
		}

		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twpy0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twpp0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuPrintForms_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal);
			//Support.ZOrder(this, 1);
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twre0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuRedbookHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twrb0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twbl0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twcl0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twut0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("twvr0000.hlp", AppWinStyle.MaximizedFocus, App.Path + "\\");
		}

		//private void GRID_KeyDownEvent(object sender, KeyEventArgs e)
		//{
		//	Keys KeyCode = e.KeyCode;
		//	// vbPorter upgrade warning: intCounter As int	OnWriteFCConvert.ToInt32(
		//	int intCounter;
		//	// vbPorter upgrade warning: intPlaceHolder As int	OnWriteFCConvert.ToInt32(
		//	int intPlaceHolder = 0;
		//	if (KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9)
		//	{
		//		KeyCode -= 48;
		//	}
		//	if ((e.KeyCode == Keys.Down) || (e.KeyCode == Keys.Up))
		//	{
		//		intPlaceHolder = GRID.Row;
		//		if (e.KeyCode == Keys.Down)
		//		{
		//			KeepGoing:
		//			if ((GRID.Row + 1) <= (GRID.Rows - 1))
		//			{
		//				GRID.Row += 1;
		//				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//				{
		//					GRID.Row -= 1;
		//				}
		//				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				{
		//					goto KeepGoing;
		//				}
		//			}
		//		}
		//		else
		//		{
		//			KeepGoingUP:
		//			if ((GRID.Row - 1) >= 1)
		//			{
		//				GRID.Row -= 1;
		//				if (GRID.Row < 1)
		//					GRID.Row = 1;
		//				if (GRID.TextMatrix(GRID.Row, 1) == string.Empty)
		//				{
		//				}
		//				else if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, GRID.Row, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				{
		//					goto KeepGoingUP;
		//				}
		//			}
		//			else
		//			{
		//				GRID.Row = intPlaceHolder;
		//			}
		//		}
		//		// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1) = ImageList1.ListImages("Clear").Picture
		//		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, null);
		//		GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, GRID.Row, 0, ImageList1.Images["Left"]);
		//		KeyCode = 0;
		//		return;
		//	}
		//	for (intCounter = 1; intCounter <= (GRID.Rows - 1); intCounter++)
		//	{
		//		if (Strings.Left(FCConvert.ToString(GRID.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == FCConvert.ToString(Convert.ToChar(e.KeyCode)))
		//		{
		//			if (!FCConvert.ToBoolean(GRID.RowData(intCounter)))
		//				return;
		//			// MenuActions (intCounter)
		//			MenuActions(FCConvert.ToInt32(Conversion.Val(GRID.TextMatrix(intCounter, 2))));
		//			KeyCode = 0;
		//			break;
		//		}
		//	}
		//}

		//private void GRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		//{
		//	int KeyAscii = Strings.Asc(e.KeyChar);
		//	if (KeyAscii != 13)
		//		return;
		//	if (!FCConvert.ToBoolean(GRID.RowData(GRID.Row)))
		//		return;
		//	// MenuActions (Grid.Row)
		//	MenuActions(FCConvert.ToInt32(Conversion.Val(GRID.TextMatrix(GRID.Row, 2))));
		//	e.KeyChar = Strings.Chr(KeyAscii);
		//}

		//private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	int lngRow;
		//	lngRow = GRID.MouseRow;
		//	if (lngRow < 0)
		//		return;
		//	if (!FCConvert.ToBoolean(GRID.RowData(lngRow)))
		//		return;
		//	// MenuActions lngRow
		//	MenuActions(FCConvert.ToInt32(Conversion.Val(GRID.TextMatrix(lngRow, 2))));
		//}
		// vbPorter upgrade warning: lngRow As int	OnWriteFCConvert.ToDouble(
		public void MenuActions(int lngRow)
		{
            string menu = App.MainForm.NavigationMenu.CurrentItem.Menu;
            // accepts the menu row and calls the appropriate routine
            switch (Strings.UCase(menu))
			{
				case "MAIN":
					{
						MainActions(lngRow);
						break;
					}
				case "FILE MAINTENANCE":
					{
						FileMaintActions(lngRow);
						break;
					}
				case "PRINT":
					{
						PrintActions(lngRow);
						break;
					}
				case "DESCRIPTION RECORD":
					{
						DescriptionRecordActions(lngRow);
						break;
					}
				case "ACCOUNT FUNCTION":
					{
						AccountFunctionActions(lngRow);
						break;
					}
			}
			//end switch
		}

		static int R = 0, c;

		//private void GRID_MouseMoveEvent(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	double i;
		//	// avoid extra work
		//	if (GRID.MouseRow < 1)
		//		return;
		//	ToolTip1.SetToolTip(GRID, GRID.TextMatrix(GRID.MouseRow, 3));
		//	if (GRID.MouseRow == R)
		//		return;
		//	R = GRID.MouseRow;
		//	if (R < 1)
		//		return;
		//	// move selection (even with no click)
		//	GRID.Select(R, 1);
		//	// Grid.Select R, 0
		//	// remove cursor image
		//	// Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1, 0) = ImageList1.ListImages("Clear").Picture
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, GRID.Rows - 1, 0, null);
		//	// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, 1, GRID.BackColor);
		//	// show cursor image if there is some text in column 5
		//	if (R == 0)
		//		return;
		//	if (FCConvert.ToInt32(GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, R, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION || GRID.TextMatrix(R, 1) == string.Empty)
		//	{
		//		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
		//		return;
		//	}
		//	else
		//	{
		//		GRID.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
		//	}
		//	// Call Grid.Select(R, 0, R, 1)
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpPicture, R, 0, ImageList1.Images["Left"]);
		//	GRID.Select(R, 1);
		//}

		private void ClearLabels()
		{
			//// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, Grid.Rows - 1, 1) = TRIOCOLORGRAYBACKGROUND
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, GRID.Rows - 1, 1, GRID.BackColor);
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 20, 1, modGlobalConstants.Statics.TRIOCOLORBLACK);
			//GRID.ColData(1, string.Empty);
		}

		public void HandlePartialPermission(ref int lngFuncID)
		{
			// takes a function number as a parameter
			// Then gets all the child functions that belong to it and
			// disables the appropriate controls
			// mdiparent shouldnt need this function.  mdi functions should always send false
			// to validpermissions
			// Dim clsChildList As clsdrwrapper
			// Dim dcTemp As New clsdrwrapper
			// Dim strPerm As String
			// Dim lngChild As Long
			// 
			// 
			// Set clsChildList = New clsdrwrapper
			// 
			// Call tscRESecurity.Get_Children(clsChildList, lngFuncID)
			// 
			// Do While Not clsChildList.EndOfFile
			// lngChild = clsChildList.GetData("childid")
			// strPerm = tscRESecurity.Check_Permissions(lngChild)
			// 
			// Select Case lngFuncID
			// Case COSTUPDATE
			// Select Case lngChild
			// 
			// End Select
			// Case UPDATEMENU
			// End Select
			// 
			// Call clsChildList.MoveNext
			// Loop
		}
		// vbPorter upgrade warning: intMenu As int	OnWriteFCConvert.ToInt32(
		private void PrintActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmReports.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						frmCustomCEReport.InstancePtr.Init(0);
						break;
					}
				case 3:
					{
						frmCustomBill.InstancePtr.Init("CE", "Custom Forms");
						break;
					}
				case 4:
					{
						frmDynamicReport.InstancePtr.Init("CE", modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL);
						break;
					}
				case 5:
					{
						SetMenuOptions(CNSTMAINMENUID);
						break;
					}
				case 6:
					{
						clsCustomBill clsbill = new clsCustomBill();
						clsbill.DBFile = "TWCE0000.vb1";
						clsbill.DotMatrixFormat = false;
						clsbill.FormatID = frmChooseCustomBillType.InstancePtr.Init("CE", "TWCE0000.vb1");
						clsbill.SQL = "Select * from cemaster";
						rptCustomBill.InstancePtr.Init(clsbill, true);
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: intMenu As int	OnWriteFCConvert.ToInt32(
		private void FileMaintActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmCustomize.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						modMain.CheckDatabaseStructure(StaticSettings.GlobalCommandDispatcher);
						break;
					}
				case 4:
					{
						SetMenuOptions(CNSTDESCRIPTIONRECORDSMENUID);
						break;
					}
				case 5:
					{
						SetMenuOptions(CNSTACCOUNTMENUID);
						break;
					}
				case 9:
					{
						SetMenuOptions(CNSTMAINMENUID);
						break;
					}
				case 8:
					{
						modMain.ClearLocks();
						MessageBox.Show("Record locks have been cleared", "Locks Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					}
				case 7:
					{
						frmSignature.InstancePtr.Show(App.MainForm);
						break;
					}
				case 3:
					{
						frmCalendar.InstancePtr.Show(App.MainForm);
						break;
					}
				case 6:
					{
						frmReturnAddress.InstancePtr.Show(App.MainForm);
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: intMenu As int	OnWriteFCConvert.ToInt32(
		private void DescriptionRecordActions(int intMenu)
		{
			switch (intMenu)
			{
				case 1:
					{
						frmUserCodes.InstancePtr.Show(App.MainForm);
						break;
					}
				case 2:
					{
						frmSystemDefined.InstancePtr.Show(App.MainForm);
						break;
					}
				case 3:
					{
						frmInspectors.InstancePtr.Show(App.MainForm);
						break;
					}
				case 5:
					{
						SetMenuOptions(CNSTFILEMAINTENANCEMENUID);
						break;
					}
				case 4:
					{
						frmInspectionChecklists.InstancePtr.Show(App.MainForm);
						break;
					}
			}
			//end switch
		}

		private void UpdateCEFromRE()
		{
			// copy master info from re to records that are associated in ce
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsPhone = new clsDRWrapper();
				clsDRWrapper rsBookPage = new clsDRWrapper();
				clsCEMaster mRec = new clsCEMaster();
				if (MessageBox.Show("This will replace all saved Code Enforcement master data with master data from associated Real Estate accounts" + "\r\n" + "Are you sure you want to continue?", "Update?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				{
					return;
				}
				modGlobalFunctions.AddCYAEntry_6("CE", "Updated CE from RE");
				frmWait.InstancePtr.Init("Updating associated records");
				strSQL = "select * from master where not rsdeleted = 1 order by rsaccount";
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
				strSQL = "select * from cemaster where rsaccount > 0 and not deleted = 1 order by ceaccount";
				rsSave.OpenRecordset(strSQL, modGlobalVariables.Statics.strCEDatabase);
				clsDRWrapper rsZone = new clsDRWrapper();
				clsDRWrapper rsNeighborhood = new clsDRWrapper();
				strSQL = "select * from zones order by code";
				rsZone.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
				strSQL = "select * from neighborhood order by code";
				rsNeighborhood.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
				int tInt;
				tInt = 0;
				while (!rsSave.EndOfFile())
				{
					//App.DoEvents();
					if (rsLoad.FindFirstRecord("rsaccount", rsSave.Get_Fields_Int32("rsaccount")))
					{
						frmWait.InstancePtr.lblMessage.Text = "Updating " + rsSave.Get_Fields_Int32("ceaccount");
                        //FC:FINAL:MSH - i.issue #1664: update 'Wait' window for displaying progress
                        FCUtils.ApplicationUpdate(frmWait.InstancePtr.lblMessage);
						// frmWait.lblMessage.Refresh
						tInt += 1;
						if (tInt > 20)
						{
							// need doevents but don't need the slow down of doing every time
							tInt = 0;
							//App.DoEvents();
						}
						rsSave.Edit();
						rsSave.Set_Fields("Owner1ID", rsLoad.Get_Fields_Int32("OwnerPartyID"));
						rsSave.Set_Fields("Owner2ID", rsLoad.Get_Fields_Int32("SecOwnerPartyID"));
						// rsSave.Fields("name") = rsLoad.Fields("rsname")
						// rsSave.Fields("secowner") = rsLoad.Fields("rssecowner")
						// Call mRec.ParseName(rsLoad.Fields("rssecowner"), True)
						// mRec.ParseName (rsLoad.Fields("rsname"))
						rsSave.Set_Fields("maplot", rsLoad.Get_Fields_String("rsmaplot"));
						rsSave.Set_Fields("streetnumber", FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_String("rslocnumalph"))));
						rsSave.Set_Fields("apt", rsLoad.Get_Fields_String("rslocapt"));
						rsSave.Set_Fields("streetname", rsLoad.Get_Fields_String("rslocstreet"));
						rsSave.Set_Fields("ref1", rsLoad.Get_Fields_String("rsref1"));
						rsSave.Set_Fields("ref2", rsLoad.Get_Fields_String("rsref2"));
						rsSave.Set_Fields("acres", rsLoad.Get_Fields_Double("piacres"));
						if (rsZone.FindFirstRecord("code", Conversion.Val(rsLoad.Get_Fields_Int32("pizone"))))
						{
							rsSave.Set_Fields("zone", rsZone.Get_Fields_String("description"));
						}
						else
						{
							rsSave.Set_Fields("zone", "");
						}
						if (rsNeighborhood.FindFirstRecord("code", Conversion.Val(rsLoad.Get_Fields_Int32("pineighborhood"))))
						{
							rsSave.Set_Fields("neighborhood", rsNeighborhood.Get_Fields_String("description"));
						}
						else
						{
							rsSave.Set_Fields("neighborhood", "");
						}
						rsSave.Update();
					}
					rsSave.MoveNext();
				}
				rsSave.MoveFirst();
				// now update maplot and phone records
				// strSQL = "select ceaccount,rsaccount from cemaster where rsaccount > 0 order by ceaccount"
				// Call rsLoad.OpenRecordset(strSQL, strCEDatabase)
				strSQL = "select * from maplot order by account,line";
				rsLoad.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
				strSQL = "select * from bookpage order by account,line";
				rsBookPage.OpenRecordset(strSQL, modGlobalVariables.Statics.strREDatabase);
				while (!rsSave.EndOfFile())
				{
					frmWait.InstancePtr.lblMessage.Text = "Checking Account " + rsSave.Get_Fields_Int32("ceaccount");
                    //FC:FINAL:MSH - i.issue #1664: update 'Wait' window for displaying progress
                    FCUtils.ApplicationUpdate(frmWait.InstancePtr.lblMessage);
                    // frmWait.lblMessage.Refresh
                    //App.DoEvents();
                    rsZone.Execute("delete from cemaplot where account = " + rsSave.Get_Fields_Int32("ceaccount"), modGlobalVariables.Statics.strCEDatabase);
					rsZone.Execute("delete from bookpage where account = " + rsSave.Get_Fields_Int32("ceaccount"), modGlobalVariables.Statics.strCEDatabase);
					if (rsLoad.FindFirstRecord("account", rsSave.Get_Fields_Int32("rsaccount")))
					{
						strSQL = "insert into cemaplot (account,maplot,mpdate,line) values (";
						strSQL += rsSave.Get_Fields_Int32("ceaccount");
						strSQL += ",'" + rsLoad.Get_Fields_String("maplot") + "'";
						strSQL += ",'" + rsLoad.Get_Fields_String("mpdate") + "'";
						strSQL += "," + rsLoad.Get_Fields("line");
						strSQL += ")";
						rsZone.Execute(strSQL, modGlobalVariables.Statics.strCEDatabase);
						while (rsLoad.FindNextRecord("account", rsSave.Get_Fields_Int32("rsaccount")))
						{
							//App.DoEvents();
							strSQL = "insert into cemaplot (account,maplot,mpdate,line) values (";
							strSQL += rsSave.Get_Fields_Int32("ceaccount");
							strSQL += ",'" + rsLoad.Get_Fields_String("maplot") + "'";
							strSQL += ",'" + rsLoad.Get_Fields_String("mpdate") + "'";
							strSQL += "," + rsLoad.Get_Fields("line");
							strSQL += ")";
							rsZone.Execute(strSQL, modGlobalVariables.Statics.strCEDatabase);
						}
					}
					if (rsBookPage.FindFirstRecord("account", rsSave.Get_Fields_Int32("rsaccount")))
					{
						strSQL = "insert into bookpage (account,book,page,[current]) values (";
						strSQL += rsSave.Get_Fields_Int32("ceaccount");
						strSQL += "," + FCConvert.ToString(Conversion.Val(rsBookPage.Get_Fields("book")));
						strSQL += "," + FCConvert.ToString(Conversion.Val(rsBookPage.Get_Fields("page")));
						if (FCConvert.ToBoolean(rsBookPage.Get_Fields_Boolean("Current")))
						{
							strSQL += ",1";
						}
						else
						{
							strSQL += ",0";
						}
						strSQL += ")";
						rsZone.Execute(strSQL, modGlobalVariables.Statics.strCEDatabase);
						while (rsBookPage.FindNextRecord("account", rsSave.Get_Fields_Int32("rsaccount")))
						{
							//App.DoEvents();
							strSQL = "insert into bookpage (account,book,page,[current]) values (";
							strSQL += rsSave.Get_Fields_Int32("ceaccount");
							strSQL += "," + FCConvert.ToString(Conversion.Val(rsBookPage.Get_Fields("book")));
							strSQL += "," + FCConvert.ToString(Conversion.Val(rsBookPage.Get_Fields("page")));
							if (FCConvert.ToBoolean(rsBookPage.Get_Fields_Boolean("Current")))
							{
								strSQL += ",1";
							}
							else
							{
								strSQL += ",0";
							}
							strSQL += ")";
							rsZone.Execute(strSQL, modGlobalVariables.Statics.strCEDatabase);
						}
					}
					rsSave.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Records updated", "Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In UpdateCEFromRE", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
