﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWCE0000
{
	public class clsReportOrderClause
	{
		//=========================================================
		private FCCollection OrderList = new FCCollection();
		private int intCurrentIndex;

		public void Clear()
		{
			int x;
			for (x = 0; x <= OrderList.Count - 1; x++)
			{
				OrderList.Remove(0);
			}
			// x
			intCurrentIndex = -1;
		}

		public void InsertOrderItem(ref clsReportOrder pOrd)
		{
			int x;
			bool boolAdded;
			clsReportOrder tOrd;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				boolAdded = false;
				for (x = 1; x <= OrderList.Count; x++)
				{
					if (!(OrderList[x] == null))
					{
						tOrd = OrderList[x];
						if (pOrd.Order < tOrd.Order)
						{
							OrderList.Add(pOrd, null, x);
							boolAdded = true;
							return;
						}
					}
				}
				// x
				if (!boolAdded)
				{
					OrderList.Add(pOrd);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " " + fecherFoundation.Information.Err(ex).Description + "\r\n" + "In InsertOrderItem", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void AddOrder(int lngAutoID, bool boolIsDescending, string strDesc, string strFields, ref int lngOrder)
		{
			clsReportOrder tOrd = new clsReportOrder();
			tOrd.ID = lngAutoID;
			tOrd.Descending = boolIsDescending;
			tOrd.Description = strDesc;
			tOrd.Fields = strFields;
			tOrd.Order = lngOrder;
			InsertOrderItem(ref tOrd);
		}

		public int GetCurrentIndex
		{
			get
			{
				int GetCurrentIndex = 0;
				GetCurrentIndex = intCurrentIndex;
				return GetCurrentIndex;
			}
		}

		public clsReportOrder GetCurrentOrderItem
		{
			get
			{
				clsReportOrder GetCurrentOrderItem = null;
				clsReportOrder tOrd;
				tOrd = null;
				if (!FCUtils.IsEmpty(OrderList))
				{
					if (intCurrentIndex > 0)
					{
						if (!(OrderList[intCurrentIndex] == null))
						{
							tOrd = OrderList[intCurrentIndex];
						}
					}
				}
				GetCurrentOrderItem = tOrd;
				return GetCurrentOrderItem;
			}
		}

		public void MoveFirst()
		{
			if (!(OrderList == null))
			{
				if (!FCUtils.IsEmpty(OrderList))
				{
					if (OrderList.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(OrderList))
			{
				if (intCurrentIndex > OrderList.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= OrderList.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > OrderList.Count)
						{
							intReturn = -1;
							break;
						}
						else if (OrderList[intCurrentIndex] == null)
						{
						}
						else if (OrderList[intCurrentIndex].Unused)
						{
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}
	}
}
