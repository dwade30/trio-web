﻿using System;
using System.IO;
using SharedApplication;
using SharedApplication.Extensions;
using Wisej.Core;

namespace SharedFileAccess
{
    public class WiseJTrioFileSystem : ITrioFileSystem
    {
        private IFileSystemProvider fileSystemProvider;
        private cGlobalSettings globalSettings;
        public WiseJTrioFileSystem(cGlobalSettings globalSettings)
        {
            this.globalSettings = globalSettings;
            fileSystemProvider = new FileSystemProvider(globalSettings.GlobalDataDirectory,"Root");

        }
        public string[] GetDirectories(string path, string pattern, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return fileSystemProvider.GetDirectories(path, pattern, searchOption);
        }

        public string[] GetFiles(string path, string pattern, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return fileSystemProvider.GetFiles(path, pattern, searchOption);
        }

        public FileAttributes GetAttributes(string path)
        {
            return fileSystemProvider.GetAttributes(path);
        }

        public void CreateDirectory(string path)
        {
            fileSystemProvider.CreateDirectory(path);
        }

        public void DeleteDirectory(string path, bool recursive)
        {
            fileSystemProvider.DeleteDirectory(path,recursive);
        }

        public DateTime GetCreationTime(string path)
        {
            return fileSystemProvider.GetCreationTime(path);
        }

        public DateTime GetLastWriteTime(string path)
        {
            return fileSystemProvider.GetLastWriteTime(path);
        }

        public bool Exists(string path)
        {
            return fileSystemProvider.Exists(path);
        }

        public long GetFileSize(string path)
        {
            return fileSystemProvider.GetFileSize(path);
        }

        public void DeleteFile(string path)
        {
            fileSystemProvider.DeleteFile(path);
        }

        public void RenameFile(string path, string newName)
        {
            fileSystemProvider.RenameFile(path,newName);
        }

        public void RenameDirectory(string path, string newName)
        {
            fileSystemProvider.RenameDirectory(path,newName);
        }

        public bool Contains(string path)
        {
            return fileSystemProvider.Contains(path);
        }

        public string MapPath(string path)
        {
            return fileSystemProvider.MapPath(path);
        }

        public Stream OpenFileStream(string path, FileMode mode, FileAccess access)
        {
            return fileSystemProvider.OpenFileStream(path, mode, access);
        }

        public string Root
        {
            get => fileSystemProvider.Root;
            set => fileSystemProvider.Root = value;
        }
        public string Name
        {
            get => fileSystemProvider.Name;
            set => fileSystemProvider.Name = value;
        }

        
    }
}