﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using SharedApplication;
using SharedFileAccess;

namespace SharedFileAccessWisej.configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WiseJTrioFileSystem>().As<ITrioFileSystem>();
        }
    }
}
