namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class arLienPartialPayment
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arLienPartialPayment));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblTitleBar = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLegalDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTownHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldMainText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWitnessLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWitnessName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxPayerLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxPayerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDateLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTownHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMainText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWitnessLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWitnessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxPayerLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxPayerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDateLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldMainText});
            this.Detail.Height = 2.864583F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTitleBar,
            this.lblLegalDescription,
            this.lblMapLot,
            this.fldAccount,
            this.lblTownHeader});
            this.PageHeader.Height = 0.9375F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldWitnessLine,
            this.fldWitnessName,
            this.fldTaxPayerLine,
            this.fldTaxPayerName,
            this.fldDateLine});
            this.PageFooter.Height = 1.65625F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblTitleBar
            // 
            this.lblTitleBar.Height = 0.25F;
            this.lblTitleBar.HyperLink = null;
            this.lblTitleBar.Left = 0F;
            this.lblTitleBar.Name = "lblTitleBar";
            this.lblTitleBar.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblTitleBar.Text = "WAIVER FORM FOR USE WITH PARTIAL PAYMENTS";
            this.lblTitleBar.Top = 0.25F;
            this.lblTitleBar.Width = 7F;
            // 
            // lblLegalDescription
            // 
            this.lblLegalDescription.Height = 0.25F;
            this.lblLegalDescription.HyperLink = null;
            this.lblLegalDescription.Left = 0F;
            this.lblLegalDescription.Name = "lblLegalDescription";
            this.lblLegalDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblLegalDescription.Text = "30-DAY NOTICES & PROPERTY TAX LIENS";
            this.lblLegalDescription.Top = 0.5F;
            this.lblLegalDescription.Width = 7F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 0F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblMapLot.Text = null;
            this.lblMapLot.Top = 0.75F;
            this.lblMapLot.Width = 7F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.125F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0.5625F;
            this.fldAccount.Width = 0.8125F;
            // 
            // lblTownHeader
            // 
            this.lblTownHeader.Height = 0.25F;
            this.lblTownHeader.HyperLink = null;
            this.lblTownHeader.Left = 0F;
            this.lblTownHeader.Name = "lblTownHeader";
            this.lblTownHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblTownHeader.Text = null;
            this.lblTownHeader.Top = 0F;
            this.lblTownHeader.Width = 7F;
            // 
            // fldMainText
            // 
            this.fldMainText.Height = 2.225F;
            this.fldMainText.Left = 0F;
            this.fldMainText.Name = "fldMainText";
            this.fldMainText.Style = "font-family: \'Tahoma\'";
            this.fldMainText.Text = null;
            this.fldMainText.Top = 0.4F;
            this.fldMainText.Width = 7F;
            // 
            // fldWitnessLine
            // 
            this.fldWitnessLine.Height = 0.1875F;
            this.fldWitnessLine.Left = 0.25F;
            this.fldWitnessLine.Name = "fldWitnessLine";
            this.fldWitnessLine.Style = "font-family: \'Tahoma\'";
            this.fldWitnessLine.Text = null;
            this.fldWitnessLine.Top = 0.5F;
            this.fldWitnessLine.Width = 3F;
            // 
            // fldWitnessName
            // 
            this.fldWitnessName.Height = 0.1875F;
            this.fldWitnessName.Left = 0.25F;
            this.fldWitnessName.Name = "fldWitnessName";
            this.fldWitnessName.Style = "font-family: \'Tahoma\'";
            this.fldWitnessName.Text = null;
            this.fldWitnessName.Top = 0.8125F;
            this.fldWitnessName.Width = 3F;
            // 
            // fldTaxPayerLine
            // 
            this.fldTaxPayerLine.Height = 0.1875F;
            this.fldTaxPayerLine.Left = 4F;
            this.fldTaxPayerLine.Name = "fldTaxPayerLine";
            this.fldTaxPayerLine.Style = "font-family: \'Tahoma\'";
            this.fldTaxPayerLine.Text = null;
            this.fldTaxPayerLine.Top = 0.5F;
            this.fldTaxPayerLine.Width = 3F;
            // 
            // fldTaxPayerName
            // 
            this.fldTaxPayerName.Height = 0.1875F;
            this.fldTaxPayerName.Left = 4F;
            this.fldTaxPayerName.Name = "fldTaxPayerName";
            this.fldTaxPayerName.Style = "font-family: \'Tahoma\'";
            this.fldTaxPayerName.Text = null;
            this.fldTaxPayerName.Top = 0.8125F;
            this.fldTaxPayerName.Width = 3F;
            // 
            // fldDateLine
            // 
            this.fldDateLine.Height = 0.1875F;
            this.fldDateLine.Left = 0.25F;
            this.fldDateLine.Name = "fldDateLine";
            this.fldDateLine.Style = "font-family: \'Tahoma\'";
            this.fldDateLine.Text = null;
            this.fldDateLine.Top = 0.125F;
            this.fldDateLine.Width = 6.625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 2F;
            this.PageSettings.Margins.Left = 0.75F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.010417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTownHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMainText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWitnessLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWitnessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxPayerLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxPayerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDateLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMainText;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitleBar;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalDescription;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTownHeader;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWitnessLine;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWitnessName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxPayerLine;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxPayerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateLine;
    }
}
