namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptReminderSummary
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderSummary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSumTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnFooter = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldYear,
            this.fldAccount,
            this.fldTotal,
            this.fldName});
            this.Detail.Height = 0.1770833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblReportType,
            this.lblYear,
            this.lblName,
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lblAccount,
            this.lblTotal,
            this.Line1});
            this.PageHeader.Height = 0.8333333F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Height = 0F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalCount,
            this.fldSumTotal,
            this.lnFooter});
            this.GroupFooter1.Height = 0.3541667F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.25F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = null;
            this.lblReportType.Top = 0.1875F;
            this.lblReportType.Width = 7F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0.75F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblYear.Text = "Year";
            this.lblYear.Top = 0.625F;
            this.lblYear.Width = 0.5625F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 1.3125F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.625F;
            this.lblName.Width = 2.6875F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1875F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Reminder Notice Summary";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.875F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.875F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.4375F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblAccount.Text = "Account";
            this.lblAccount.Top = 0.625F;
            this.lblAccount.Width = 0.75F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 5.8125F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 0.625F;
            this.lblTotal.Width = 1.1875F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.8125F;
            this.Line1.Width = 7.03125F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.03125F;
            this.Line1.Y1 = 0.8125F;
            this.Line1.Y2 = 0.8125F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 0.75F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 0.5625F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.75F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 5.8125F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.1875F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 1.3125F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 4.5F;
            // 
            // fldTotalCount
            // 
            this.fldTotalCount.Height = 0.1875F;
            this.fldTotalCount.Left = 3.125F;
            this.fldTotalCount.Name = "fldTotalCount";
            this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.fldTotalCount.Text = "Total:";
            this.fldTotalCount.Top = 0.125F;
            this.fldTotalCount.Width = 2.6875F;
            // 
            // fldSumTotal
            // 
            this.fldSumTotal.Height = 0.1875F;
            this.fldSumTotal.Left = 5.8125F;
            this.fldSumTotal.Name = "fldSumTotal";
            this.fldSumTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldSumTotal.Text = null;
            this.fldSumTotal.Top = 0.125F;
            this.fldSumTotal.Width = 1.1875F;
            // 
            // lnFooter
            // 
            this.lnFooter.Height = 0F;
            this.lnFooter.Left = 5.8125F;
            this.lnFooter.LineWeight = 1F;
            this.lnFooter.Name = "lnFooter";
            this.lnFooter.Top = 0.0625F;
            this.lnFooter.Width = 1.1875F;
            this.lnFooter.X1 = 5.8125F;
            this.lnFooter.X2 = 7F;
            this.lnFooter.Y1 = 0.0625F;
            this.lnFooter.Y2 = 0.0625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnFooter;
    }
}
