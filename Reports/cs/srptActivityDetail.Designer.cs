namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class srptActivityDetail
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptActivityDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldOwed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalOwed = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalOwed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDate,
            this.fldCode,
            this.fldRef,
            this.fldReceipt,
            this.fldPayment,
            this.fldOwed});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalOwed,
            this.fldTotalPayment,
            this.lblFooter,
            this.Line1});
            this.ReportFooter.Height = 0.2395833F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // fldDate
            // 
            this.fldDate.CanGrow = false;
            this.fldDate.Height = 0.2F;
            this.fldDate.Left = 2.45F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'";
            this.fldDate.Text = null;
            this.fldDate.Top = 0F;
            this.fldDate.Width = 1F;
            // 
            // fldCode
            // 
            this.fldCode.CanGrow = false;
            this.fldCode.Height = 0.2F;
            this.fldCode.Left = 3.45F;
            this.fldCode.Name = "fldCode";
            this.fldCode.Style = "font-family: \'Tahoma\'";
            this.fldCode.Text = null;
            this.fldCode.Top = 0F;
            this.fldCode.Width = 0.55F;
            // 
            // fldRef
            // 
            this.fldRef.CanGrow = false;
            this.fldRef.Height = 0.1875F;
            this.fldRef.Left = 4F;
            this.fldRef.Name = "fldRef";
            this.fldRef.Style = "font-family: \'Tahoma\'";
            this.fldRef.Text = null;
            this.fldRef.Top = 0F;
            this.fldRef.Width = 0.8125F;
            // 
            // fldReceipt
            // 
            this.fldReceipt.CanGrow = false;
            this.fldReceipt.Height = 0.1875F;
            this.fldReceipt.Left = 4.8125F;
            this.fldReceipt.Name = "fldReceipt";
            this.fldReceipt.Style = "font-family: \'Tahoma\'";
            this.fldReceipt.Text = null;
            this.fldReceipt.Top = 0F;
            this.fldReceipt.Width = 0.5625F;
            // 
            // fldPayment
            // 
            this.fldPayment.CanGrow = false;
            this.fldPayment.Height = 0.1875F;
            this.fldPayment.Left = 5.375F;
            this.fldPayment.Name = "fldPayment";
            this.fldPayment.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPayment.Text = null;
            this.fldPayment.Top = 0F;
            this.fldPayment.Width = 1.0625F;
            // 
            // fldOwed
            // 
            this.fldOwed.CanGrow = false;
            this.fldOwed.Height = 0.2F;
            this.fldOwed.Left = 6.45F;
            this.fldOwed.Name = "fldOwed";
            this.fldOwed.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldOwed.Text = null;
            this.fldOwed.Top = 0F;
            this.fldOwed.Width = 1.05F;
            // 
            // fldTotalOwed
            // 
            this.fldTotalOwed.Height = 0.2F;
            this.fldTotalOwed.Left = 6.45F;
            this.fldTotalOwed.Name = "fldTotalOwed";
            this.fldTotalOwed.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalOwed.Text = null;
            this.fldTotalOwed.Top = 0F;
            this.fldTotalOwed.Width = 1.05F;
            // 
            // fldTotalPayment
            // 
            this.fldTotalPayment.Height = 0.2F;
            this.fldTotalPayment.Left = 5.4F;
            this.fldTotalPayment.Name = "fldTotalPayment";
            this.fldTotalPayment.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPayment.Text = null;
            this.fldTotalPayment.Top = 0F;
            this.fldTotalPayment.Width = 1.05F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.2F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 4.85F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Tahoma\'";
            this.lblFooter.Text = "Total";
            this.lblFooter.Top = 0F;
            this.lblFooter.Width = 0.55F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 5F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 2.5F;
            this.Line1.X1 = 5F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.489583F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOwed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalOwed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPayment;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwed;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalOwed;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPayment;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
    }
}
