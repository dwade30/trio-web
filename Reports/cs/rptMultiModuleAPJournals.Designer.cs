namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptMultiModuleAPJournals
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMultiModuleAPJournals));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.GroupHeader3 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter3 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEncTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.rptSubTotals = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.fldCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.srptEncLiquid = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEncTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Field11,
            this.Field12,
            this.Field13,
            this.Field14,
            this.Line2});
            this.Detail.Height = 0.4375F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label6,
            this.lblDescription});
            this.GroupHeader1.Height = 0.3958333F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.srptEncLiquid});
            this.GroupFooter1.Height = 0.2916667F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Height = 0F;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label21,
            this.fldDebitTotal,
            this.fldEncTotal,
            this.rptSubTotals,
            this.fldCreditTotal,
            this.Line3});
            this.GroupFooter2.Height = 0.6145833F;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label12,
            this.Label14,
            this.Label15,
            this.Label16,
            this.Label17,
            this.Label18,
            this.Label19,
            this.Label20,
            this.Line1,
            this.Label22});
            this.GroupHeader3.Height = 0.4375F;
            this.GroupHeader3.Name = "GroupHeader3";
            this.GroupHeader3.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.Height = 0F;
            this.GroupFooter3.Name = "GroupFooter3";
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label6.Text = "Label6";
            this.Label6.Top = 0.1875F;
            this.Label6.Width = 7.5F;
            // 
            // lblDescription
            // 
            this.lblDescription.Height = 0.1875F;
            this.lblDescription.HyperLink = null;
            this.lblDescription.Left = 0F;
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.lblDescription.Text = "Label23";
            this.lblDescription.Top = 0F;
            this.lblDescription.Width = 7.5F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0.0625F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label7.Text = "Per";
            this.Label7.Top = 0.0625F;
            this.Label7.Width = 0.375F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.46875F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
            this.Label8.Text = "Date";
            this.Label8.Top = 0.0625F;
            this.Label8.Width = 0.6875F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 1.21875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label9.Text = "Description";
            this.Label9.Top = 0.0625F;
            this.Label9.Width = 1.71875F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 3F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label12.Text = "Account";
            this.Label12.Top = 0.0625F;
            this.Label12.Width = 1.8125F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 4.875F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label14.Text = "Debit";
            this.Label14.Top = 0.0625F;
            this.Label14.Width = 0.8125F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 6.625F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label15.Text = "Enc";
            this.Label15.Top = 0.0625F;
            this.Label15.Width = 0.875F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 0.625F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label16.Text = "Proj";
            this.Label16.Top = 0.25F;
            this.Label16.Width = 0.6875F;
            // 
            // Label17
            // 
            this.Label17.Height = 0.1875F;
            this.Label17.HyperLink = null;
            this.Label17.Left = 1.4375F;
            this.Label17.Name = "Label17";
            this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label17.Text = "Wrnt #";
            this.Label17.Top = 0.25F;
            this.Label17.Width = 0.75F;
            // 
            // Label18
            // 
            this.Label18.Height = 0.1875F;
            this.Label18.HyperLink = null;
            this.Label18.Left = 3.0625F;
            this.Label18.Name = "Label18";
            this.Label18.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label18.Text = "Check";
            this.Label18.Top = 0.25F;
            this.Label18.Width = 0.875F;
            // 
            // Label19
            // 
            this.Label19.Height = 0.1875F;
            this.Label19.HyperLink = null;
            this.Label19.Left = 2.3125F;
            this.Label19.Name = "Label19";
            this.Label19.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label19.Text = "Vendor";
            this.Label19.Top = 0.25F;
            this.Label19.Width = 0.625F;
            // 
            // Label20
            // 
            this.Label20.Height = 0.1875F;
            this.Label20.HyperLink = null;
            this.Label20.Left = 4.0625F;
            this.Label20.Name = "Label20";
            this.Label20.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 1";
            this.Label20.Text = "Reference";
            this.Label20.Top = 0.25F;
            this.Label20.Width = 1.4375F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.4375F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 0.4375F;
            this.Line1.Y2 = 0.4375F;
            // 
            // Label22
            // 
            this.Label22.Height = 0.1875F;
            this.Label22.HyperLink = null;
            this.Label22.Left = 5.8125F;
            this.Label22.Name = "Label22";
            this.Label22.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label22.Text = "Credit";
            this.Label22.Top = 0.0625F;
            this.Label22.Width = 0.6875F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 0.0625F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field1.Text = "Field1";
            this.Field1.Top = 0.0625F;
            this.Field1.Width = 0.375F;
            // 
            // Field2
            // 
            this.Field2.Height = 0.1875F;
            this.Field2.Left = 0.46875F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Field2.Text = "Field2";
            this.Field2.Top = 0.0625F;
            this.Field2.Width = 0.6875F;
            // 
            // Field3
            // 
            this.Field3.Height = 0.1875F;
            this.Field3.Left = 1.21875F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field3.Text = "Field3";
            this.Field3.Top = 0.0625F;
            this.Field3.Width = 1.71875F;
            // 
            // Field6
            // 
            this.Field6.Height = 0.1875F;
            this.Field6.Left = 3F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field6.Text = null;
            this.Field6.Top = 0.0625F;
            this.Field6.Width = 1.8125F;
            // 
            // Field7
            // 
            this.Field7.Height = 0.1875F;
            this.Field7.Left = 4.875F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Field7.Text = "Field7";
            this.Field7.Top = 0.0625F;
            this.Field7.Width = 0.8125F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.1875F;
            this.Field8.Left = 6.625F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Field8.Text = "Field8";
            this.Field8.Top = 0.0625F;
            this.Field8.Width = 0.875F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.1875F;
            this.Field9.Left = 0.625F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field9.Text = "Field9";
            this.Field9.Top = 0.25F;
            this.Field9.Width = 0.6875F;
            // 
            // Field10
            // 
            this.Field10.Height = 0.1875F;
            this.Field10.Left = 1.4375F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field10.Text = "Field10";
            this.Field10.Top = 0.25F;
            this.Field10.Width = 0.75F;
            // 
            // Field11
            // 
            this.Field11.Height = 0.1875F;
            this.Field11.Left = 2.3125F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field11.Text = "Field11";
            this.Field11.Top = 0.25F;
            this.Field11.Width = 0.625F;
            // 
            // Field12
            // 
            this.Field12.Height = 0.1875F;
            this.Field12.Left = 3.0625F;
            this.Field12.Name = "Field12";
            this.Field12.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field12.Text = "Field12";
            this.Field12.Top = 0.25F;
            this.Field12.Width = 0.875F;
            // 
            // Field13
            // 
            this.Field13.Height = 0.1875F;
            this.Field13.Left = 4.0625F;
            this.Field13.Name = "Field13";
            this.Field13.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field13.Text = "Field13";
            this.Field13.Top = 0.25F;
            this.Field13.Width = 1.4375F;
            // 
            // Field14
            // 
            this.Field14.Height = 0.1875F;
            this.Field14.Left = 5.8125F;
            this.Field14.Name = "Field14";
            this.Field14.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Field14.Text = "Field14";
            this.Field14.Top = 0.0625F;
            this.Field14.Width = 0.6875F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Visible = false;
            this.Line2.Width = 7.5F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // Label21
            // 
            this.Label21.Height = 0.1875F;
            this.Label21.HyperLink = null;
            this.Label21.Left = 4.1875F;
            this.Label21.Name = "Label21";
            this.Label21.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
            this.Label21.Text = "Totals";
            this.Label21.Top = 0.125F;
            this.Label21.Width = 0.5625F;
            // 
            // fldDebitTotal
            // 
            this.fldDebitTotal.Height = 0.1875F;
            this.fldDebitTotal.Left = 4.875F;
            this.fldDebitTotal.Name = "fldDebitTotal";
            this.fldDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldDebitTotal.Text = null;
            this.fldDebitTotal.Top = 0.125F;
            this.fldDebitTotal.Width = 0.8125F;
            // 
            // fldEncTotal
            // 
            this.fldEncTotal.Height = 0.1875F;
            this.fldEncTotal.Left = 6.625F;
            this.fldEncTotal.Name = "fldEncTotal";
            this.fldEncTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldEncTotal.Text = null;
            this.fldEncTotal.Top = 0.125F;
            this.fldEncTotal.Width = 0.875F;
            // 
            // rptSubTotals
            // 
            this.rptSubTotals.CloseBorder = false;
            this.rptSubTotals.Height = 0.125F;
            this.rptSubTotals.Left = 0F;
            this.rptSubTotals.Name = "rptSubTotals";
            this.rptSubTotals.Report = null;
            this.rptSubTotals.Top = 0.46875F;
            this.rptSubTotals.Width = 7.5F;
            // 
            // fldCreditTotal
            // 
            this.fldCreditTotal.Height = 0.1875F;
            this.fldCreditTotal.Left = 5.8125F;
            this.fldCreditTotal.Name = "fldCreditTotal";
            this.fldCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCreditTotal.Text = null;
            this.fldCreditTotal.Top = 0.125F;
            this.fldCreditTotal.Width = 0.6875F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 4.1875F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.0625F;
            this.Line3.Width = 3.3125F;
            this.Line3.X1 = 4.1875F;
            this.Line3.X2 = 7.5F;
            this.Line3.Y1 = 0.0625F;
            this.Line3.Y2 = 0.0625F;
            // 
            // srptEncLiquid
            // 
            this.srptEncLiquid.CloseBorder = false;
            this.srptEncLiquid.Height = 0.125F;
            this.srptEncLiquid.Left = 0F;
            this.srptEncLiquid.Name = "srptEncLiquid";
            this.srptEncLiquid.Report = null;
            this.srptEncLiquid.Top = 0.15625F;
            this.srptEncLiquid.Width = 7.5F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.GroupHeader2);
            this.Sections.Add(this.GroupHeader3);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter3);
            this.Sections.Add(this.GroupFooter2);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEncTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport srptEncLiquid;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEncTotal;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport rptSubTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter3;
    }
}
