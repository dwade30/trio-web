namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptMultiModuleJournalsTotals
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMultiModuleJournalsTotals));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Line2,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Field11});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.Label2,
            this.Label3,
            this.Label4,
            this.Line1,
            this.Label6,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10});
            this.GroupHeader1.GroupKeepTogether = GrapeCity.ActiveReports.SectionReportModel.GroupKeepTogether.All;
            this.GroupHeader1.Height = 0.8333333F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label5,
            this.Field5,
            this.Field6,
            this.Field7,
            this.Line3,
            this.Line4,
            this.Field12,
            this.Field13,
            this.Field14,
            this.Field15});
            this.GroupFooter1.Height = 0.2604167F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0.1875F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
            this.Label1.Text = "Funds";
            this.Label1.Top = 0.5625F;
            this.Label1.Width = 0.625F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.1875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 1F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label2.Text = "Exp";
            this.Label2.Top = 0.5625F;
            this.Label2.Width = 0.75F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.1875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 1.875F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label3.Text = "Rev";
            this.Label3.Top = 0.5625F;
            this.Label3.Width = 0.6875F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.1875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 2.6875F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label4.Text = "GL";
            this.Label4.Top = 0.5625F;
            this.Label4.Width = 0.75F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.1875F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.8125F;
            this.Line1.Width = 7.0625F;
            this.Line1.X1 = 0.1875F;
            this.Line1.X2 = 7.25F;
            this.Line1.Y1 = 0.8125F;
            this.Line1.Y2 = 0.8125F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 3.625F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label6.Text = "Cash";
            this.Label6.Top = 0.5625F;
            this.Label6.Width = 0.625F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 4.375F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label7.Text = "Due To";
            this.Label7.Top = 0.5625F;
            this.Label7.Width = 0.75F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 5.25F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label8.Text = "Due From";
            this.Label8.Top = 0.5625F;
            this.Label8.Width = 0.8125F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 6.1875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
            this.Label9.Text = "Enc Control";
            this.Label9.Top = 0.5625F;
            this.Label9.Width = 0.9375F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.25F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 0F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-family: \'Tahoma\'; font-size: 11pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label10.Text = "Summary";
            this.Label10.Top = 0.1875F;
            this.Label10.Width = 7.5F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 0.25F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Field1.Text = "Field1";
            this.Field1.Top = 0F;
            this.Field1.Width = 0.5F;
            // 
            // Field2
            // 
            this.Field2.Height = 0.1875F;
            this.Field2.Left = 0.9375F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field2.Text = "Field2";
            this.Field2.Top = 0F;
            this.Field2.Width = 0.8125F;
            // 
            // Field3
            // 
            this.Field3.Height = 0.1875F;
            this.Field3.Left = 1.8125F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field3.Text = "Field3";
            this.Field3.Top = 0F;
            this.Field3.Width = 0.75F;
            // 
            // Field4
            // 
            this.Field4.Height = 0.1875F;
            this.Field4.Left = 2.625F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field4.Text = "Field4";
            this.Field4.Top = 0F;
            this.Field4.Width = 0.8125F;
            // 
            // Line2
            // 
            this.Line2.Height = 0.1875F;
            this.Line2.Left = 0.9375F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 0F;
            this.Line2.X1 = 0.9375F;
            this.Line2.X2 = 0.9375F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0.1875F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.1875F;
            this.Field8.Left = 3.5F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field8.Text = "Field8";
            this.Field8.Top = 0F;
            this.Field8.Width = 0.75F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.1875F;
            this.Field9.Left = 4.3125F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field9.Text = "Field9";
            this.Field9.Top = 0F;
            this.Field9.Width = 0.8125F;
            // 
            // Field10
            // 
            this.Field10.Height = 0.1875F;
            this.Field10.Left = 5.1875F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field10.Text = "Field10";
            this.Field10.Top = 0F;
            this.Field10.Width = 0.875F;
            // 
            // Field11
            // 
            this.Field11.Height = 0.1875F;
            this.Field11.Left = 6.125F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
            this.Field11.Text = "Field11";
            this.Field11.Top = 0F;
            this.Field11.Width = 1F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.1875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 0.25F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
            this.Label5.Text = "Totals";
            this.Label5.Top = 0.0625F;
            this.Label5.Width = 0.5F;
            // 
            // Field5
            // 
            this.Field5.Height = 0.1875F;
            this.Field5.Left = 0.9375F;
            this.Field5.Name = "Field5";
            this.Field5.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field5.Text = "Field5";
            this.Field5.Top = 0.0625F;
            this.Field5.Width = 0.8125F;
            // 
            // Field6
            // 
            this.Field6.Height = 0.1875F;
            this.Field6.Left = 1.8125F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field6.Text = "Field6";
            this.Field6.Top = 0.0625F;
            this.Field6.Width = 0.75F;
            // 
            // Field7
            // 
            this.Field7.Height = 0.1875F;
            this.Field7.Left = 2.625F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field7.Text = "Field7";
            this.Field7.Top = 0.0625F;
            this.Field7.Width = 0.8125F;
            // 
            // Line3
            // 
            this.Line3.Height = 0.25F;
            this.Line3.Left = 0.9375F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 0F;
            this.Line3.X1 = 0.9375F;
            this.Line3.X2 = 0.9375F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0.25F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 0.1875F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0F;
            this.Line4.Width = 7.0625F;
            this.Line4.X1 = 0.1875F;
            this.Line4.X2 = 7.25F;
            this.Line4.Y1 = 0F;
            this.Line4.Y2 = 0F;
            // 
            // Field12
            // 
            this.Field12.Height = 0.1875F;
            this.Field12.Left = 3.5F;
            this.Field12.Name = "Field12";
            this.Field12.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field12.Text = "Field12";
            this.Field12.Top = 0.0625F;
            this.Field12.Width = 0.75F;
            // 
            // Field13
            // 
            this.Field13.Height = 0.1875F;
            this.Field13.Left = 4.3125F;
            this.Field13.Name = "Field13";
            this.Field13.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field13.Text = "Field13";
            this.Field13.Top = 0.0625F;
            this.Field13.Width = 0.8125F;
            // 
            // Field14
            // 
            this.Field14.Height = 0.1875F;
            this.Field14.Left = 5.1875F;
            this.Field14.Name = "Field14";
            this.Field14.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field14.Text = "Field14";
            this.Field14.Top = 0.0625F;
            this.Field14.Width = 0.875F;
            // 
            // Field15
            // 
            this.Field15.Height = 0.1875F;
            this.Field15.Left = 6.125F;
            this.Field15.Name = "Field15";
            this.Field15.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" +
    "char-set: 1";
            this.Field15.Text = "Field15";
            this.Field15.Top = 0.0625F;
            this.Field15.Width = 1F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
    }
}
