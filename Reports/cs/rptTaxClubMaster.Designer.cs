namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptTaxClubMaster
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxClubMaster));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblHeaderPaymentAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAgreementDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblStartingDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNumberOfPayments = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAgreementDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldStartingDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNumberOfPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPaymentAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblLastPaid = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldLastPaid = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderPaymentAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAgreementDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStartingDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAgreementDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStartingDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNumberOfPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLastPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldYear,
            this.lblYear,
            this.lblAgreementDate,
            this.lblStartingDate,
            this.lblNumberOfPayments,
            this.lblPaymentAmount,
            this.lblTotalAmount,
            this.lblMapLot,
            this.lblName,
            this.fldAgreementDate,
            this.fldStartingDate,
            this.fldNumberOfPayments,
            this.fldPaymentAmount,
            this.fldTotalAmount,
            this.fldMapLot,
            this.fldName,
            this.lblLocation,
            this.fldLocation,
            this.lblAccount,
            this.fldAccount,
            this.lblTotalPaid,
            this.fldTotalPaid,
            this.lblLastPaid,
            this.fldLastPaid});
            this.Detail.Height = 1.395833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblHeaderAccount,
            this.lblHeaderYear,
            this.lblHeaderName,
            this.Line1,
            this.lblHeaderPaymentAmount});
            this.PageHeader.Height = 0.59375F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.4F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0.05F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblHeader.Text = "Tax Club Information";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.4375F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.625F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.25F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.25F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.25F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.25F;
            // 
            // lblHeaderAccount
            // 
            this.lblHeaderAccount.Height = 0.1875F;
            this.lblHeaderAccount.HyperLink = null;
            this.lblHeaderAccount.Left = 0.1875F;
            this.lblHeaderAccount.Name = "lblHeaderAccount";
            this.lblHeaderAccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderAccount.Text = "Account";
            this.lblHeaderAccount.Top = 0.375F;
            this.lblHeaderAccount.Visible = false;
            this.lblHeaderAccount.Width = 1F;
            // 
            // lblHeaderYear
            // 
            this.lblHeaderYear.Height = 0.1875F;
            this.lblHeaderYear.HyperLink = null;
            this.lblHeaderYear.Left = 1.1875F;
            this.lblHeaderYear.Name = "lblHeaderYear";
            this.lblHeaderYear.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderYear.Text = "Year";
            this.lblHeaderYear.Top = 0.375F;
            this.lblHeaderYear.Visible = false;
            this.lblHeaderYear.Width = 1F;
            // 
            // lblHeaderName
            // 
            this.lblHeaderName.Height = 0.1875F;
            this.lblHeaderName.HyperLink = null;
            this.lblHeaderName.Left = 3F;
            this.lblHeaderName.Name = "lblHeaderName";
            this.lblHeaderName.Style = "font-family: \'Tahoma\'";
            this.lblHeaderName.Text = "Name";
            this.lblHeaderName.Top = 0.375F;
            this.lblHeaderName.Visible = false;
            this.lblHeaderName.Width = 1.125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.02083333F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.5625F;
            this.Line1.Visible = false;
            this.Line1.Width = 7.479167F;
            this.Line1.X1 = 0.02083333F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 0.5625F;
            this.Line1.Y2 = 0.5625F;
            // 
            // lblHeaderPaymentAmount
            // 
            this.lblHeaderPaymentAmount.Height = 0.1875F;
            this.lblHeaderPaymentAmount.HyperLink = null;
            this.lblHeaderPaymentAmount.Left = 6.5F;
            this.lblHeaderPaymentAmount.Name = "lblHeaderPaymentAmount";
            this.lblHeaderPaymentAmount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderPaymentAmount.Text = "Payment";
            this.lblHeaderPaymentAmount.Top = 0.375F;
            this.lblHeaderPaymentAmount.Visible = false;
            this.lblHeaderPaymentAmount.Width = 1F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 5.25F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldYear.Text = null;
            this.fldYear.Top = 0.0625F;
            this.fldYear.Width = 1.5625F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 3.625F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'";
            this.lblYear.Text = null;
            this.lblYear.Top = 0.0625F;
            this.lblYear.Width = 1.5F;
            // 
            // lblAgreementDate
            // 
            this.lblAgreementDate.Height = 0.1875F;
            this.lblAgreementDate.HyperLink = null;
            this.lblAgreementDate.Left = 3.625F;
            this.lblAgreementDate.Name = "lblAgreementDate";
            this.lblAgreementDate.Style = "font-family: \'Tahoma\'";
            this.lblAgreementDate.Text = null;
            this.lblAgreementDate.Top = 0.625F;
            this.lblAgreementDate.Width = 1.5F;
            // 
            // lblStartingDate
            // 
            this.lblStartingDate.Height = 0.1875F;
            this.lblStartingDate.HyperLink = null;
            this.lblStartingDate.Left = 3.625F;
            this.lblStartingDate.Name = "lblStartingDate";
            this.lblStartingDate.Style = "font-family: \'Tahoma\'";
            this.lblStartingDate.Text = null;
            this.lblStartingDate.Top = 0.8125F;
            this.lblStartingDate.Width = 1.5F;
            // 
            // lblNumberOfPayments
            // 
            this.lblNumberOfPayments.Height = 0.1875F;
            this.lblNumberOfPayments.HyperLink = null;
            this.lblNumberOfPayments.Left = 0.1875F;
            this.lblNumberOfPayments.Name = "lblNumberOfPayments";
            this.lblNumberOfPayments.Style = "font-family: \'Tahoma\'";
            this.lblNumberOfPayments.Text = null;
            this.lblNumberOfPayments.Top = 0.4375F;
            this.lblNumberOfPayments.Width = 1.625F;
            // 
            // lblPaymentAmount
            // 
            this.lblPaymentAmount.Height = 0.1875F;
            this.lblPaymentAmount.HyperLink = null;
            this.lblPaymentAmount.Left = 0.1875F;
            this.lblPaymentAmount.Name = "lblPaymentAmount";
            this.lblPaymentAmount.Style = "font-family: \'Tahoma\'";
            this.lblPaymentAmount.Text = null;
            this.lblPaymentAmount.Top = 0.625F;
            this.lblPaymentAmount.Width = 1.625F;
            // 
            // lblTotalAmount
            // 
            this.lblTotalAmount.Height = 0.1875F;
            this.lblTotalAmount.HyperLink = null;
            this.lblTotalAmount.Left = 0.1875F;
            this.lblTotalAmount.Name = "lblTotalAmount";
            this.lblTotalAmount.Style = "font-family: \'Tahoma\'";
            this.lblTotalAmount.Text = null;
            this.lblTotalAmount.Top = 0.8125F;
            this.lblTotalAmount.Width = 1.625F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 3.625F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'";
            this.lblMapLot.Text = null;
            this.lblMapLot.Top = 0.4375F;
            this.lblMapLot.Width = 1.5F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.1875F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'";
            this.lblName.Text = null;
            this.lblName.Top = 0.25F;
            this.lblName.Width = 1.625F;
            // 
            // fldAgreementDate
            // 
            this.fldAgreementDate.Height = 0.1875F;
            this.fldAgreementDate.Left = 5.25F;
            this.fldAgreementDate.Name = "fldAgreementDate";
            this.fldAgreementDate.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldAgreementDate.Text = null;
            this.fldAgreementDate.Top = 0.625F;
            this.fldAgreementDate.Width = 1.5625F;
            // 
            // fldStartingDate
            // 
            this.fldStartingDate.Height = 0.1875F;
            this.fldStartingDate.Left = 5.25F;
            this.fldStartingDate.Name = "fldStartingDate";
            this.fldStartingDate.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldStartingDate.Text = null;
            this.fldStartingDate.Top = 0.8125F;
            this.fldStartingDate.Width = 1.5625F;
            // 
            // fldNumberOfPayments
            // 
            this.fldNumberOfPayments.Height = 0.1875F;
            this.fldNumberOfPayments.Left = 2F;
            this.fldNumberOfPayments.Name = "fldNumberOfPayments";
            this.fldNumberOfPayments.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldNumberOfPayments.Text = null;
            this.fldNumberOfPayments.Top = 0.4375F;
            this.fldNumberOfPayments.Width = 1F;
            // 
            // fldPaymentAmount
            // 
            this.fldPaymentAmount.Height = 0.1875F;
            this.fldPaymentAmount.Left = 2F;
            this.fldPaymentAmount.Name = "fldPaymentAmount";
            this.fldPaymentAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPaymentAmount.Text = null;
            this.fldPaymentAmount.Top = 0.625F;
            this.fldPaymentAmount.Width = 1F;
            // 
            // fldTotalAmount
            // 
            this.fldTotalAmount.Height = 0.1875F;
            this.fldTotalAmount.Left = 2F;
            this.fldTotalAmount.Name = "fldTotalAmount";
            this.fldTotalAmount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalAmount.Text = null;
            this.fldTotalAmount.Top = 0.8125F;
            this.fldTotalAmount.Width = 1F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 5.25F;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 0.4375F;
            this.fldMapLot.Width = 1.5625F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 2F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0.25F;
            this.fldName.Width = 4.5625F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 3.625F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'";
            this.lblLocation.Text = null;
            this.lblLocation.Top = 1F;
            this.lblLocation.Width = 1.5F;
            // 
            // fldLocation
            // 
            this.fldLocation.Height = 0.375F;
            this.fldLocation.Left = 5.25F;
            this.fldLocation.Name = "fldLocation";
            this.fldLocation.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldLocation.Text = null;
            this.fldLocation.Top = 1F;
            this.fldLocation.Width = 2.25F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0.1875F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'";
            this.lblAccount.Text = null;
            this.lblAccount.Top = 0.0625F;
            this.lblAccount.Width = 1.125F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 2F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0.0625F;
            this.fldAccount.Width = 1F;
            // 
            // lblTotalPaid
            // 
            this.lblTotalPaid.Height = 0.1875F;
            this.lblTotalPaid.HyperLink = null;
            this.lblTotalPaid.Left = 0.1875F;
            this.lblTotalPaid.Name = "lblTotalPaid";
            this.lblTotalPaid.Style = "font-family: \'Tahoma\'";
            this.lblTotalPaid.Text = null;
            this.lblTotalPaid.Top = 1F;
            this.lblTotalPaid.Width = 1.625F;
            // 
            // fldTotalPaid
            // 
            this.fldTotalPaid.Height = 0.1875F;
            this.fldTotalPaid.Left = 2F;
            this.fldTotalPaid.Name = "fldTotalPaid";
            this.fldTotalPaid.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalPaid.Text = null;
            this.fldTotalPaid.Top = 1F;
            this.fldTotalPaid.Width = 1F;
            // 
            // lblLastPaid
            // 
            this.lblLastPaid.Height = 0.1875F;
            this.lblLastPaid.HyperLink = null;
            this.lblLastPaid.Left = 0.1875F;
            this.lblLastPaid.Name = "lblLastPaid";
            this.lblLastPaid.Style = "font-family: \'Tahoma\'";
            this.lblLastPaid.Text = null;
            this.lblLastPaid.Top = 1.1875F;
            this.lblLastPaid.Width = 1.625F;
            // 
            // fldLastPaid
            // 
            this.fldLastPaid.Height = 0.1875F;
            this.fldLastPaid.Left = 2F;
            this.fldLastPaid.Name = "fldLastPaid";
            this.fldLastPaid.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldLastPaid.Text = null;
            this.fldLastPaid.Top = 1.1875F;
            this.fldLastPaid.Width = 1F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderPaymentAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAgreementDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblStartingDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAgreementDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStartingDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNumberOfPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLastPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAgreementDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblStartingDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNumberOfPayments;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAgreementDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStartingDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNumberOfPayments;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentAmount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPaid;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaid;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLastPaid;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLastPaid;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderName;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderPaymentAmount;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
