namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class arLienProcessEditReport
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arLienProcessEditReport));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPLInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.sarLienEditMortHolders = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.fldCO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblExtraLines = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBankruptDisclaimer = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankruptDisclaimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldPrincipal,
            this.fldName,
            this.fldPLInt,
            this.fldCosts,
            this.fldCurrentInt,
            this.fldTotal,
            this.sarLienEditMortHolders,
            this.fldCO,
            this.fldMapLot,
            this.fldBookPage,
            this.fldLocation,
            this.fldAddress,
            this.lblExtraLines});
            this.Detail.Height = 0.2916667F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTotals,
            this.fldTotalPrincipal,
            this.fldTotalPLInt,
            this.fldTotalCosts,
            this.fldTotalCurrentInt,
            this.fldTotalTotal,
            this.Line2});
            this.ReportFooter.Height = 0.1666667F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lblAccount,
            this.lblPrincipal,
            this.lblName,
            this.lblPLInt,
            this.lblCosts,
            this.lblCurrentInt,
            this.lblTotal,
            this.lblReportType,
            this.Line1});
            this.PageHeader.Height = 1.05F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFooter,
            this.lblBankruptDisclaimer});
            this.PageFooter.Height = 0.3541667F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.3F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Lien Process Edit List";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.2F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.35F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.15F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.2F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.35F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.2F;
            this.lblPage.Width = 1.15F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.6875F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.2F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAccount.Text = "Acct";
            this.lblAccount.Top = 0.85F;
            this.lblAccount.Width = 0.55F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.2F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 2.7F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 0.85F;
            this.lblPrincipal.Width = 0.95F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.2F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.55F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.85F;
            this.lblName.Width = 2.15F;
            // 
            // lblPLInt
            // 
            this.lblPLInt.Height = 0.2F;
            this.lblPLInt.HyperLink = null;
            this.lblPLInt.Left = 3.65F;
            this.lblPLInt.Name = "lblPLInt";
            this.lblPLInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPLInt.Text = "Pre Lien Int";
            this.lblPLInt.Top = 0.85F;
            this.lblPLInt.Width = 0.9F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.2F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 4.55F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 0.85F;
            this.lblCosts.Width = 0.95F;
            // 
            // lblCurrentInt
            // 
            this.lblCurrentInt.Height = 0.2F;
            this.lblCurrentInt.HyperLink = null;
            this.lblCurrentInt.Left = 5.5F;
            this.lblCurrentInt.Name = "lblCurrentInt";
            this.lblCurrentInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblCurrentInt.Text = "Current Int";
            this.lblCurrentInt.Top = 0.85F;
            this.lblCurrentInt.Width = 0.95F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.2F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6.45F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 0.85F;
            this.lblTotal.Width = 1.05F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.5375F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = null;
            this.lblReportType.Top = 0.3125F;
            this.lblReportType.Width = 7.5F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.05F;
            this.Line1.Width = 7.5F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 1.05F;
            this.Line1.Y2 = 1.05F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.2F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.55F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.2F;
            this.fldPrincipal.Left = 2.7F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 0.9F;
            // 
            // fldName
            // 
            this.fldName.CanGrow = false;
            this.fldName.Height = 0.2F;
            this.fldName.Left = 0.55F;
            this.fldName.MultiLine = false;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 2.15F;
            // 
            // fldPLInt
            // 
            this.fldPLInt.Height = 0.2F;
            this.fldPLInt.Left = 3.6F;
            this.fldPLInt.Name = "fldPLInt";
            this.fldPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPLInt.Text = null;
            this.fldPLInt.Top = 0F;
            this.fldPLInt.Width = 0.95F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.2F;
            this.fldCosts.Left = 4.55F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCosts.Text = null;
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 0.95F;
            // 
            // fldCurrentInt
            // 
            this.fldCurrentInt.Height = 0.2F;
            this.fldCurrentInt.Left = 5.5F;
            this.fldCurrentInt.Name = "fldCurrentInt";
            this.fldCurrentInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCurrentInt.Text = null;
            this.fldCurrentInt.Top = 0F;
            this.fldCurrentInt.Width = 0.95F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.2F;
            this.fldTotal.Left = 6.45F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.05F;
            // 
            // sarLienEditMortHolders
            // 
            this.sarLienEditMortHolders.CanShrink = false;
            this.sarLienEditMortHolders.CloseBorder = false;
            this.sarLienEditMortHolders.Height = 0.0625F;
            this.sarLienEditMortHolders.Left = 0.4375F;
            this.sarLienEditMortHolders.Name = "sarLienEditMortHolders";
            this.sarLienEditMortHolders.Report = null;
            this.sarLienEditMortHolders.Top = 0.1875F;
            this.sarLienEditMortHolders.Width = 7.0625F;
            // 
            // fldCO
            // 
            this.fldCO.CanShrink = true;
            this.fldCO.Height = 0.1875F;
            this.fldCO.Left = 0.5625F;
            this.fldCO.Name = "fldCO";
            this.fldCO.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldCO.Text = null;
            this.fldCO.Top = 0.125F;
            this.fldCO.Width = 6.625F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.CanGrow = false;
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 0.5625F;
            this.fldMapLot.MultiLine = false;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 0.25F;
            this.fldMapLot.Visible = false;
            this.fldMapLot.Width = 3.3125F;
            // 
            // fldBookPage
            // 
            this.fldBookPage.Height = 0.1875F;
            this.fldBookPage.Left = 0.5625F;
            this.fldBookPage.MultiLine = false;
            this.fldBookPage.Name = "fldBookPage";
            this.fldBookPage.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldBookPage.Text = null;
            this.fldBookPage.Top = 0.625F;
            this.fldBookPage.Visible = false;
            this.fldBookPage.Width = 6.625F;
            // 
            // fldLocation
            // 
            this.fldLocation.CanGrow = false;
            this.fldLocation.Height = 0.1875F;
            this.fldLocation.Left = 0.5625F;
            this.fldLocation.MultiLine = false;
            this.fldLocation.Name = "fldLocation";
            this.fldLocation.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldLocation.Text = null;
            this.fldLocation.Top = 0.4375F;
            this.fldLocation.Visible = false;
            this.fldLocation.Width = 3.3125F;
            // 
            // fldAddress
            // 
            this.fldAddress.CanGrow = false;
            this.fldAddress.Height = 0.5625F;
            this.fldAddress.Left = 3.875F;
            this.fldAddress.Name = "fldAddress";
            this.fldAddress.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldAddress.Text = null;
            this.fldAddress.Top = 0.25F;
            this.fldAddress.Visible = false;
            this.fldAddress.Width = 3.3125F;
            // 
            // lblExtraLines
            // 
            this.lblExtraLines.Height = 0.125F;
            this.lblExtraLines.HyperLink = null;
            this.lblExtraLines.Left = 0.5625F;
            this.lblExtraLines.Name = "lblExtraLines";
            this.lblExtraLines.Style = "";
            this.lblExtraLines.Text = "                  ";
            this.lblExtraLines.Top = 0.8125F;
            this.lblExtraLines.Width = 3.3125F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.2F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 0F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Tahoma\'";
            this.lblFooter.Text = null;
            this.lblFooter.Top = 0F;
            this.lblFooter.Width = 7.5F;
            // 
            // lblBankruptDisclaimer
            // 
            this.lblBankruptDisclaimer.Height = 0.1875F;
            this.lblBankruptDisclaimer.HyperLink = null;
            this.lblBankruptDisclaimer.Left = 0F;
            this.lblBankruptDisclaimer.Name = "lblBankruptDisclaimer";
            this.lblBankruptDisclaimer.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblBankruptDisclaimer.Text = "Account numbers in parentheses are flagged as being in bankruptcy.";
            this.lblBankruptDisclaimer.Top = 0.1875F;
            this.lblBankruptDisclaimer.Visible = false;
            this.lblBankruptDisclaimer.Width = 7.5F;
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.2F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 0F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotals.Text = "Total:";
            this.lblTotals.Top = 0F;
            this.lblTotals.Width = 2.7F;
            // 
            // fldTotalPrincipal
            // 
            this.fldTotalPrincipal.Height = 0.2F;
            this.fldTotalPrincipal.Left = 2.7F;
            this.fldTotalPrincipal.Name = "fldTotalPrincipal";
            this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPrincipal.Text = "0.00";
            this.fldTotalPrincipal.Top = 0F;
            this.fldTotalPrincipal.Width = 0.9F;
            // 
            // fldTotalPLInt
            // 
            this.fldTotalPLInt.Height = 0.2F;
            this.fldTotalPLInt.Left = 3.6F;
            this.fldTotalPLInt.Name = "fldTotalPLInt";
            this.fldTotalPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPLInt.Text = "0.00";
            this.fldTotalPLInt.Top = 0F;
            this.fldTotalPLInt.Width = 0.95F;
            // 
            // fldTotalCosts
            // 
            this.fldTotalCosts.Height = 0.2F;
            this.fldTotalCosts.Left = 4.55F;
            this.fldTotalCosts.Name = "fldTotalCosts";
            this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCosts.Text = "0.00";
            this.fldTotalCosts.Top = 0F;
            this.fldTotalCosts.Width = 0.95F;
            // 
            // fldTotalCurrentInt
            // 
            this.fldTotalCurrentInt.Height = 0.2F;
            this.fldTotalCurrentInt.Left = 5.5F;
            this.fldTotalCurrentInt.Name = "fldTotalCurrentInt";
            this.fldTotalCurrentInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCurrentInt.Text = "0.00";
            this.fldTotalCurrentInt.Top = 0F;
            this.fldTotalCurrentInt.Width = 0.95F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.2F;
            this.fldTotalTotal.Left = 6.45F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTotal.Text = "0.00";
            this.fldTotalTotal.Top = 0F;
            this.fldTotalTotal.Width = 1.05F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 2.1875F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Width = 5.3125F;
            this.Line2.X1 = 2.1875F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCurrentInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExtraLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankruptDisclaimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport sarLienEditMortHolders;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCO;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblExtraLines;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurrentInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPLInt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrentInt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBankruptDisclaimer;
    }
}
