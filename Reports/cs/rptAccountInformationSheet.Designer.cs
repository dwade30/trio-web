namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptAccountInformationSheet
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAccountInformationSheet));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnTaxes = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAcreage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBuilding = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblExempt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblValuationTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLandValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBuildingValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblExemptValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblValualtionValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnAssessment = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblBookPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNameTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocationTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccountTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAcreageTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLotTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDeedTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblOutstandingTaxes = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldEx1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblEx = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldEx2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEx3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTreeGrowthTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTreeGrowth = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldExVal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldExVal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldExVal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMill = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMillTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFarmTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFarm = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLastTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLastTaxTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldExAmountTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnExempt = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblOpenTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblOpen = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPD = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZoningTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblZoning = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSaleDateTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSaleDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSalePriceTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSalePrice = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSFLATitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSFLA = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrevYearTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrevYearTaxTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrevMill = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblInfoGiven = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSignDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnTitle = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lnSignerName = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldTotalPD = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblDateDisclaimer = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldPeriodDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodDate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodDate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodDate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPeriodTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldPeriodAmount1Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount1Int = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount1Cost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount2Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount3Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmount4Total = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPeriodAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnPeriodTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTaxInfoMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcreage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValuationTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLandValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExemptValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValualtionValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNameTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocationTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcreageTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLotTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeedTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOutstandingTaxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTreeGrowthTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTreeGrowth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExVal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExVal2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExVal3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMillTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFarmTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFarm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastTaxTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExAmountTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpenTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZoningTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZoning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaleDateTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaleDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSalePriceTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSalePrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSFLATitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSFLA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrevYearTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrevYearTaxTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrevMill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfoGiven)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSignDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateDisclaimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriodTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1Int)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1Cost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount2Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount3Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount4Total)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmountTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxInfoMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldYear,
            this.fldPrincipal,
            this.fldTotal,
            this.fldInterest,
            this.fldCosts,
            this.fldPD,
            this.lnTotals});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalPrin,
            this.fldTotalTotal,
            this.fldTotalInt,
            this.fldTotalCost,
            this.lnTotal,
            this.lblInfoGiven,
            this.lblTitle,
            this.lblSignDate,
            this.lnTitle,
            this.lnSignerName,
            this.fldTotalPD,
            this.lblDateDisclaimer,
            this.fldPeriodDate1,
            this.fldPeriodDate2,
            this.fldPeriodDate3,
            this.fldPeriodDate4,
            this.fldPeriodAmount1,
            this.fldPeriodAmount2,
            this.fldPeriodAmount3,
            this.fldPeriodAmount4,
            this.lblPeriodTitle,
            this.fldPeriodAmount1Total,
            this.fldPeriodAmount1Int,
            this.fldPeriodAmount1Cost,
            this.fldPeriodAmount2Total,
            this.fldPeriodAmount3Total,
            this.fldPeriodAmount4Total,
            this.fldPeriodAmountTotal,
            this.lnPeriodTotal,
            this.lblTaxInfoMessage});
            this.ReportFooter.Height = 2.458333F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblYear,
            this.lblPrincipal,
            this.lblInterest,
            this.lblCosts,
            this.lblTotal,
            this.lnTaxes,
            this.lblName,
            this.lblLocation,
            this.lblMapLot,
            this.lblAcreage,
            this.lblLand,
            this.lblBuilding,
            this.lblExempt,
            this.lblValuationTotal,
            this.lblLandValue,
            this.lblBuildingValue,
            this.lblExemptValue,
            this.lblValualtionValue,
            this.lnAssessment,
            this.lblBookPage,
            this.lblNameTitle,
            this.lblLocationTitle,
            this.lblAccountTitle,
            this.lblAcreageTitle,
            this.lblMapLotTitle,
            this.lblDeedTitle,
            this.lblAccountNumber,
            this.lblOutstandingTaxes,
            this.fldEx1,
            this.lblEx,
            this.fldEx2,
            this.fldEx3,
            this.lblTreeGrowthTitle,
            this.lblTreeGrowth,
            this.fldExVal1,
            this.fldExVal2,
            this.fldExVal3,
            this.lblMill,
            this.lblMillTitle,
            this.lblFarmTitle,
            this.lblFarm,
            this.lblLastTax,
            this.lblLastTaxTitle,
            this.fldExAmountTitle,
            this.lnExempt,
            this.lblOpenTotal,
            this.lblOpen,
            this.lblPD,
            this.lblZoningTitle,
            this.lblZoning,
            this.lblSaleDateTitle,
            this.lblSaleDate,
            this.lblSalePriceTitle,
            this.lblSalePrice,
            this.lblSFLATitle,
            this.lblSFLA,
            this.lblPrevYearTax,
            this.lblPrevYearTaxTitle,
            this.lblPrevMill,
            this.Label1});
            this.PageHeader.Height = 4.197917F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.5625F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblHeader.Text = "Account Detail";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.75F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.8125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.6875F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.8125F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.6875F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblYear.Text = "Year";
            this.lblYear.Top = 4F;
            this.lblYear.Width = 0.625F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 1.875F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 4F;
            this.lblPrincipal.Width = 1.3125F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1875F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 3.375F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 4F;
            this.lblInterest.Width = 1.125F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 4.6875F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 4F;
            this.lblCosts.Width = 1.125F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 4F;
            this.lblTotal.Width = 1.4375F;
            // 
            // lnTaxes
            // 
            this.lnTaxes.Height = 0F;
            this.lnTaxes.Left = 0.8125F;
            this.lnTaxes.LineWeight = 1F;
            this.lnTaxes.Name = "lnTaxes";
            this.lnTaxes.Top = 4.1875F;
            this.lnTaxes.Width = 6.625F;
            this.lnTaxes.X1 = 0.8125F;
            this.lnTaxes.X2 = 7.4375F;
            this.lnTaxes.Y1 = 4.1875F;
            this.lnTaxes.Y2 = 4.1875F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.375F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 3.5625F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblName.Text = null;
            this.lblName.Top = 0.75F;
            this.lblName.Width = 3.9375F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 1.5F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblLocation.Text = null;
            this.lblLocation.Top = 1.125F;
            this.lblLocation.Width = 2.5F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 1.5F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMapLot.Text = null;
            this.lblMapLot.Top = 1.375F;
            this.lblMapLot.Width = 2.5F;
            // 
            // lblAcreage
            // 
            this.lblAcreage.Height = 0.1875F;
            this.lblAcreage.HyperLink = null;
            this.lblAcreage.Left = 4.75F;
            this.lblAcreage.Name = "lblAcreage";
            this.lblAcreage.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblAcreage.Text = null;
            this.lblAcreage.Top = 2.0625F;
            this.lblAcreage.Width = 1F;
            // 
            // lblLand
            // 
            this.lblLand.Height = 0.1875F;
            this.lblLand.HyperLink = null;
            this.lblLand.Left = 0.125F;
            this.lblLand.Name = "lblLand";
            this.lblLand.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblLand.Text = "Land:";
            this.lblLand.Top = 2.0625F;
            this.lblLand.Width = 1.375F;
            // 
            // lblBuilding
            // 
            this.lblBuilding.Height = 0.1875F;
            this.lblBuilding.HyperLink = null;
            this.lblBuilding.Left = 0.125F;
            this.lblBuilding.Name = "lblBuilding";
            this.lblBuilding.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblBuilding.Text = "Building:";
            this.lblBuilding.Top = 2.25F;
            this.lblBuilding.Width = 1.375F;
            // 
            // lblExempt
            // 
            this.lblExempt.Height = 0.1875F;
            this.lblExempt.HyperLink = null;
            this.lblExempt.Left = 0.125F;
            this.lblExempt.Name = "lblExempt";
            this.lblExempt.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblExempt.Text = "Exempt";
            this.lblExempt.Top = 2.4375F;
            this.lblExempt.Width = 1.375F;
            // 
            // lblValuationTotal
            // 
            this.lblValuationTotal.Height = 0.1875F;
            this.lblValuationTotal.HyperLink = null;
            this.lblValuationTotal.Left = 0.125F;
            this.lblValuationTotal.Name = "lblValuationTotal";
            this.lblValuationTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblValuationTotal.Text = "Total:";
            this.lblValuationTotal.Top = 2.625F;
            this.lblValuationTotal.Width = 1.375F;
            // 
            // lblLandValue
            // 
            this.lblLandValue.Height = 0.1875F;
            this.lblLandValue.HyperLink = null;
            this.lblLandValue.Left = 1.9375F;
            this.lblLandValue.Name = "lblLandValue";
            this.lblLandValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblLandValue.Text = null;
            this.lblLandValue.Top = 2.0625F;
            this.lblLandValue.Width = 1.25F;
            // 
            // lblBuildingValue
            // 
            this.lblBuildingValue.Height = 0.1875F;
            this.lblBuildingValue.HyperLink = null;
            this.lblBuildingValue.Left = 1.9375F;
            this.lblBuildingValue.Name = "lblBuildingValue";
            this.lblBuildingValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblBuildingValue.Text = null;
            this.lblBuildingValue.Top = 2.25F;
            this.lblBuildingValue.Width = 1.25F;
            // 
            // lblExemptValue
            // 
            this.lblExemptValue.Height = 0.1875F;
            this.lblExemptValue.HyperLink = null;
            this.lblExemptValue.Left = 1.9375F;
            this.lblExemptValue.Name = "lblExemptValue";
            this.lblExemptValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblExemptValue.Text = null;
            this.lblExemptValue.Top = 2.4375F;
            this.lblExemptValue.Width = 1.25F;
            // 
            // lblValualtionValue
            // 
            this.lblValualtionValue.Height = 0.1875F;
            this.lblValualtionValue.HyperLink = null;
            this.lblValualtionValue.Left = 1.9375F;
            this.lblValualtionValue.Name = "lblValualtionValue";
            this.lblValualtionValue.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblValualtionValue.Text = null;
            this.lblValualtionValue.Top = 2.625F;
            this.lblValualtionValue.Width = 1.25F;
            // 
            // lnAssessment
            // 
            this.lnAssessment.Height = 0F;
            this.lnAssessment.Left = 2.1875F;
            this.lnAssessment.LineWeight = 1F;
            this.lnAssessment.Name = "lnAssessment";
            this.lnAssessment.Top = 2.625F;
            this.lnAssessment.Width = 1F;
            this.lnAssessment.X1 = 2.1875F;
            this.lnAssessment.X2 = 3.1875F;
            this.lnAssessment.Y1 = 2.625F;
            this.lnAssessment.Y2 = 2.625F;
            // 
            // lblBookPage
            // 
            this.lblBookPage.Height = 0.375F;
            this.lblBookPage.HyperLink = null;
            this.lblBookPage.Left = 1.5F;
            this.lblBookPage.Name = "lblBookPage";
            this.lblBookPage.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblBookPage.Text = null;
            this.lblBookPage.Top = 1.625F;
            this.lblBookPage.Width = 2.5F;
            // 
            // lblNameTitle
            // 
            this.lblNameTitle.Height = 0.1875F;
            this.lblNameTitle.HyperLink = null;
            this.lblNameTitle.Left = 2.4375F;
            this.lblNameTitle.Name = "lblNameTitle";
            this.lblNameTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblNameTitle.Text = "Name:";
            this.lblNameTitle.Top = 0.75F;
            this.lblNameTitle.Width = 1F;
            // 
            // lblLocationTitle
            // 
            this.lblLocationTitle.Height = 0.1875F;
            this.lblLocationTitle.HyperLink = null;
            this.lblLocationTitle.Left = 0.125F;
            this.lblLocationTitle.Name = "lblLocationTitle";
            this.lblLocationTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblLocationTitle.Text = "Location:";
            this.lblLocationTitle.Top = 1.125F;
            this.lblLocationTitle.Width = 1.375F;
            // 
            // lblAccountTitle
            // 
            this.lblAccountTitle.Height = 0.1875F;
            this.lblAccountTitle.HyperLink = null;
            this.lblAccountTitle.Left = 0.125F;
            this.lblAccountTitle.Name = "lblAccountTitle";
            this.lblAccountTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblAccountTitle.Text = "Account:";
            this.lblAccountTitle.Top = 0.75F;
            this.lblAccountTitle.Width = 0.875F;
            // 
            // lblAcreageTitle
            // 
            this.lblAcreageTitle.Height = 0.1875F;
            this.lblAcreageTitle.HyperLink = null;
            this.lblAcreageTitle.Left = 3.5F;
            this.lblAcreageTitle.Name = "lblAcreageTitle";
            this.lblAcreageTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblAcreageTitle.Text = "Total Acres:";
            this.lblAcreageTitle.Top = 2.0625F;
            this.lblAcreageTitle.Width = 1.25F;
            // 
            // lblMapLotTitle
            // 
            this.lblMapLotTitle.Height = 0.1875F;
            this.lblMapLotTitle.HyperLink = null;
            this.lblMapLotTitle.Left = 0.125F;
            this.lblMapLotTitle.Name = "lblMapLotTitle";
            this.lblMapLotTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMapLotTitle.Text = "Map and Lot:";
            this.lblMapLotTitle.Top = 1.375F;
            this.lblMapLotTitle.Width = 1.375F;
            // 
            // lblDeedTitle
            // 
            this.lblDeedTitle.Height = 0.1875F;
            this.lblDeedTitle.HyperLink = null;
            this.lblDeedTitle.Left = 0.125F;
            this.lblDeedTitle.Name = "lblDeedTitle";
            this.lblDeedTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblDeedTitle.Text = "Deed Reference:";
            this.lblDeedTitle.Top = 1.625F;
            this.lblDeedTitle.Width = 1.375F;
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.Height = 0.1875F;
            this.lblAccountNumber.HyperLink = null;
            this.lblAccountNumber.Left = 1.0625F;
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblAccountNumber.Text = null;
            this.lblAccountNumber.Top = 0.75F;
            this.lblAccountNumber.Width = 0.625F;
            // 
            // lblOutstandingTaxes
            // 
            this.lblOutstandingTaxes.Height = 0.1875F;
            this.lblOutstandingTaxes.HyperLink = null;
            this.lblOutstandingTaxes.Left = 1F;
            this.lblOutstandingTaxes.Name = "lblOutstandingTaxes";
            this.lblOutstandingTaxes.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblOutstandingTaxes.Text = "Outstanding Taxes";
            this.lblOutstandingTaxes.Top = 3.8125F;
            this.lblOutstandingTaxes.Width = 5F;
            // 
            // fldEx1
            // 
            this.fldEx1.Height = 0.1875F;
            this.fldEx1.Left = 0.125F;
            this.fldEx1.Name = "fldEx1";
            this.fldEx1.Style = "font-family: \'Tahoma\'";
            this.fldEx1.Text = null;
            this.fldEx1.Top = 3.0625F;
            this.fldEx1.Width = 2F;
            // 
            // lblEx
            // 
            this.lblEx.Height = 0.1875F;
            this.lblEx.HyperLink = null;
            this.lblEx.Left = 0.125F;
            this.lblEx.Name = "lblEx";
            this.lblEx.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblEx.Text = "Exempt Codes:";
            this.lblEx.Top = 2.875F;
            this.lblEx.Width = 1.125F;
            // 
            // fldEx2
            // 
            this.fldEx2.Height = 0.1875F;
            this.fldEx2.Left = 0.125F;
            this.fldEx2.Name = "fldEx2";
            this.fldEx2.Style = "font-family: \'Tahoma\'";
            this.fldEx2.Text = null;
            this.fldEx2.Top = 3.25F;
            this.fldEx2.Width = 2F;
            // 
            // fldEx3
            // 
            this.fldEx3.Height = 0.1875F;
            this.fldEx3.Left = 0.125F;
            this.fldEx3.Name = "fldEx3";
            this.fldEx3.Style = "font-family: \'Tahoma\'";
            this.fldEx3.Text = null;
            this.fldEx3.Top = 3.4375F;
            this.fldEx3.Width = 2F;
            // 
            // lblTreeGrowthTitle
            // 
            this.lblTreeGrowthTitle.Height = 0.1875F;
            this.lblTreeGrowthTitle.HyperLink = null;
            this.lblTreeGrowthTitle.Left = 3.5F;
            this.lblTreeGrowthTitle.Name = "lblTreeGrowthTitle";
            this.lblTreeGrowthTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTreeGrowthTitle.Text = "Tree Growth:";
            this.lblTreeGrowthTitle.Top = 2.25F;
            this.lblTreeGrowthTitle.Width = 1.25F;
            // 
            // lblTreeGrowth
            // 
            this.lblTreeGrowth.Height = 0.1875F;
            this.lblTreeGrowth.HyperLink = null;
            this.lblTreeGrowth.Left = 4.75F;
            this.lblTreeGrowth.Name = "lblTreeGrowth";
            this.lblTreeGrowth.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblTreeGrowth.Text = null;
            this.lblTreeGrowth.Top = 2.25F;
            this.lblTreeGrowth.Width = 2.375F;
            // 
            // fldExVal1
            // 
            this.fldExVal1.Height = 0.1875F;
            this.fldExVal1.Left = 2.125F;
            this.fldExVal1.Name = "fldExVal1";
            this.fldExVal1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldExVal1.Text = null;
            this.fldExVal1.Top = 3.0625F;
            this.fldExVal1.Width = 1.0625F;
            // 
            // fldExVal2
            // 
            this.fldExVal2.Height = 0.1875F;
            this.fldExVal2.Left = 2.125F;
            this.fldExVal2.Name = "fldExVal2";
            this.fldExVal2.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldExVal2.Text = null;
            this.fldExVal2.Top = 3.25F;
            this.fldExVal2.Width = 1.0625F;
            // 
            // fldExVal3
            // 
            this.fldExVal3.Height = 0.1875F;
            this.fldExVal3.Left = 2.125F;
            this.fldExVal3.Name = "fldExVal3";
            this.fldExVal3.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldExVal3.Text = null;
            this.fldExVal3.Top = 3.4375F;
            this.fldExVal3.Width = 1.0625F;
            // 
            // lblMill
            // 
            this.lblMill.Height = 0.1875F;
            this.lblMill.HyperLink = null;
            this.lblMill.Left = 6.6875F;
            this.lblMill.Name = "lblMill";
            this.lblMill.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblMill.Text = null;
            this.lblMill.Top = 3.375F;
            this.lblMill.Width = 0.8125F;
            // 
            // lblMillTitle
            // 
            this.lblMillTitle.Height = 0.1875F;
            this.lblMillTitle.HyperLink = null;
            this.lblMillTitle.Left = 6.6875F;
            this.lblMillTitle.Name = "lblMillTitle";
            this.lblMillTitle.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblMillTitle.Text = "Mill Rate";
            this.lblMillTitle.Top = 3.1875F;
            this.lblMillTitle.Width = 0.8125F;
            // 
            // lblFarmTitle
            // 
            this.lblFarmTitle.Height = 0.1875F;
            this.lblFarmTitle.HyperLink = null;
            this.lblFarmTitle.Left = 3.5F;
            this.lblFarmTitle.Name = "lblFarmTitle";
            this.lblFarmTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblFarmTitle.Text = "Farmland:";
            this.lblFarmTitle.Top = 2.4375F;
            this.lblFarmTitle.Width = 1.25F;
            // 
            // lblFarm
            // 
            this.lblFarm.Height = 0.1875F;
            this.lblFarm.HyperLink = null;
            this.lblFarm.Left = 4.75F;
            this.lblFarm.Name = "lblFarm";
            this.lblFarm.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblFarm.Text = null;
            this.lblFarm.Top = 2.4375F;
            this.lblFarm.Width = 2.375F;
            // 
            // lblLastTax
            // 
            this.lblLastTax.Height = 0.1875F;
            this.lblLastTax.HyperLink = null;
            this.lblLastTax.Left = 5.625F;
            this.lblLastTax.Name = "lblLastTax";
            this.lblLastTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblLastTax.Text = null;
            this.lblLastTax.Top = 3.375F;
            this.lblLastTax.Width = 1.0625F;
            // 
            // lblLastTaxTitle
            // 
            this.lblLastTaxTitle.Height = 0.1875F;
            this.lblLastTaxTitle.HyperLink = null;
            this.lblLastTaxTitle.Left = 3.5F;
            this.lblLastTaxTitle.Name = "lblLastTaxTitle";
            this.lblLastTaxTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblLastTaxTitle.Text = "Last Billed :";
            this.lblLastTaxTitle.Top = 3.375F;
            this.lblLastTaxTitle.Width = 2.125F;
            // 
            // fldExAmountTitle
            // 
            this.fldExAmountTitle.Height = 0.1875F;
            this.fldExAmountTitle.Left = 2.125F;
            this.fldExAmountTitle.Name = "fldExAmountTitle";
            this.fldExAmountTitle.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.fldExAmountTitle.Text = "Amount";
            this.fldExAmountTitle.Top = 2.875F;
            this.fldExAmountTitle.Width = 1.0625F;
            // 
            // lnExempt
            // 
            this.lnExempt.Height = 0F;
            this.lnExempt.Left = 2.1875F;
            this.lnExempt.LineWeight = 1F;
            this.lnExempt.Name = "lnExempt";
            this.lnExempt.Top = 3.0625F;
            this.lnExempt.Width = 1F;
            this.lnExempt.X1 = 2.1875F;
            this.lnExempt.X2 = 3.1875F;
            this.lnExempt.Y1 = 3.0625F;
            this.lnExempt.Y2 = 3.0625F;
            // 
            // lblOpenTotal
            // 
            this.lblOpenTotal.Height = 0.1875F;
            this.lblOpenTotal.HyperLink = null;
            this.lblOpenTotal.Left = 3.5F;
            this.lblOpenTotal.Name = "lblOpenTotal";
            this.lblOpenTotal.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblOpenTotal.Text = "Open Space:";
            this.lblOpenTotal.Top = 2.625F;
            this.lblOpenTotal.Width = 1.25F;
            // 
            // lblOpen
            // 
            this.lblOpen.Height = 0.1875F;
            this.lblOpen.HyperLink = null;
            this.lblOpen.Left = 4.75F;
            this.lblOpen.Name = "lblOpen";
            this.lblOpen.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblOpen.Text = null;
            this.lblOpen.Top = 2.625F;
            this.lblOpen.Width = 2.375F;
            // 
            // lblPD
            // 
            this.lblPD.Height = 0.1875F;
            this.lblPD.HyperLink = null;
            this.lblPD.Left = 0.75F;
            this.lblPD.Name = "lblPD";
            this.lblPD.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPD.Text = "Per Diem";
            this.lblPD.Top = 4F;
            this.lblPD.Width = 0.8125F;
            // 
            // lblZoningTitle
            // 
            this.lblZoningTitle.Height = 0.1875F;
            this.lblZoningTitle.HyperLink = null;
            this.lblZoningTitle.Left = 3.5F;
            this.lblZoningTitle.Name = "lblZoningTitle";
            this.lblZoningTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblZoningTitle.Text = "Zoning:";
            this.lblZoningTitle.Top = 2.8125F;
            this.lblZoningTitle.Width = 1.25F;
            // 
            // lblZoning
            // 
            this.lblZoning.Height = 0.1875F;
            this.lblZoning.HyperLink = null;
            this.lblZoning.Left = 4.75F;
            this.lblZoning.Name = "lblZoning";
            this.lblZoning.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblZoning.Text = null;
            this.lblZoning.Top = 2.8125F;
            this.lblZoning.Width = 2.375F;
            // 
            // lblSaleDateTitle
            // 
            this.lblSaleDateTitle.Height = 0.1875F;
            this.lblSaleDateTitle.HyperLink = null;
            this.lblSaleDateTitle.Left = 4.1875F;
            this.lblSaleDateTitle.Name = "lblSaleDateTitle";
            this.lblSaleDateTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblSaleDateTitle.Text = "Sale Date:";
            this.lblSaleDateTitle.Top = 1.375F;
            this.lblSaleDateTitle.Width = 0.875F;
            // 
            // lblSaleDate
            // 
            this.lblSaleDate.Height = 0.1875F;
            this.lblSaleDate.HyperLink = null;
            this.lblSaleDate.Left = 5.0625F;
            this.lblSaleDate.Name = "lblSaleDate";
            this.lblSaleDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblSaleDate.Text = null;
            this.lblSaleDate.Top = 1.375F;
            this.lblSaleDate.Width = 2.4375F;
            // 
            // lblSalePriceTitle
            // 
            this.lblSalePriceTitle.Height = 0.1875F;
            this.lblSalePriceTitle.HyperLink = null;
            this.lblSalePriceTitle.Left = 4.1875F;
            this.lblSalePriceTitle.Name = "lblSalePriceTitle";
            this.lblSalePriceTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblSalePriceTitle.Text = "Sale Price:";
            this.lblSalePriceTitle.Top = 1.625F;
            this.lblSalePriceTitle.Width = 0.875F;
            // 
            // lblSalePrice
            // 
            this.lblSalePrice.Height = 0.1875F;
            this.lblSalePrice.HyperLink = null;
            this.lblSalePrice.Left = 5.0625F;
            this.lblSalePrice.Name = "lblSalePrice";
            this.lblSalePrice.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblSalePrice.Text = null;
            this.lblSalePrice.Top = 1.625F;
            this.lblSalePrice.Width = 2.4375F;
            // 
            // lblSFLATitle
            // 
            this.lblSFLATitle.Height = 0.1875F;
            this.lblSFLATitle.HyperLink = null;
            this.lblSFLATitle.Left = 3.5F;
            this.lblSFLATitle.Name = "lblSFLATitle";
            this.lblSFLATitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblSFLATitle.Text = "SFLA:";
            this.lblSFLATitle.Top = 3F;
            this.lblSFLATitle.Width = 1.25F;
            // 
            // lblSFLA
            // 
            this.lblSFLA.Height = 0.1875F;
            this.lblSFLA.HyperLink = null;
            this.lblSFLA.Left = 4.75F;
            this.lblSFLA.Name = "lblSFLA";
            this.lblSFLA.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblSFLA.Text = null;
            this.lblSFLA.Top = 3F;
            this.lblSFLA.Width = 2.375F;
            // 
            // lblPrevYearTax
            // 
            this.lblPrevYearTax.Height = 0.1875F;
            this.lblPrevYearTax.HyperLink = null;
            this.lblPrevYearTax.Left = 5.625F;
            this.lblPrevYearTax.Name = "lblPrevYearTax";
            this.lblPrevYearTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblPrevYearTax.Text = null;
            this.lblPrevYearTax.Top = 3.5625F;
            this.lblPrevYearTax.Width = 1.0625F;
            // 
            // lblPrevYearTaxTitle
            // 
            this.lblPrevYearTaxTitle.Height = 0.1875F;
            this.lblPrevYearTaxTitle.HyperLink = null;
            this.lblPrevYearTaxTitle.Left = 3.5F;
            this.lblPrevYearTaxTitle.Name = "lblPrevYearTaxTitle";
            this.lblPrevYearTaxTitle.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblPrevYearTaxTitle.Text = "Previous Billed :";
            this.lblPrevYearTaxTitle.Top = 3.5625F;
            this.lblPrevYearTaxTitle.Width = 2.125F;
            // 
            // lblPrevMill
            // 
            this.lblPrevMill.Height = 0.1875F;
            this.lblPrevMill.HyperLink = null;
            this.lblPrevMill.Left = 6.6875F;
            this.lblPrevMill.Name = "lblPrevMill";
            this.lblPrevMill.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblPrevMill.Text = null;
            this.lblPrevMill.Top = 3.5625F;
            this.lblPrevMill.Width = 0.8125F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 5.875F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.Label1.Text = "Amount";
            this.Label1.Top = 3.1875F;
            this.Label1.Width = 0.8125F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 0F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 0.625F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 1.875F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 1.3125F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.4375F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 3.375F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 1.125F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.1875F;
            this.fldCosts.Left = 4.6875F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldCosts.Text = null;
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 1.125F;
            // 
            // fldPD
            // 
            this.fldPD.Height = 0.1875F;
            this.fldPD.Left = 0.75F;
            this.fldPD.Name = "fldPD";
            this.fldPD.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPD.Text = null;
            this.fldPD.Top = 0F;
            this.fldPD.Width = 0.8125F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 0.8125F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Visible = false;
            this.lnTotals.Width = 6.625F;
            this.lnTotals.X1 = 0.8125F;
            this.lnTotals.X2 = 7.4375F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // fldTotalPrin
            // 
            this.fldTotalPrin.Height = 0.1875F;
            this.fldTotalPrin.Left = 1.8125F;
            this.fldTotalPrin.Name = "fldTotalPrin";
            this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalPrin.Text = null;
            this.fldTotalPrin.Top = 0.125F;
            this.fldTotalPrin.Width = 1.375F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 6F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalTotal.Text = null;
            this.fldTotalTotal.Top = 0.125F;
            this.fldTotalTotal.Width = 1.4375F;
            // 
            // fldTotalInt
            // 
            this.fldTotalInt.Height = 0.1875F;
            this.fldTotalInt.Left = 3.375F;
            this.fldTotalInt.Name = "fldTotalInt";
            this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalInt.Text = null;
            this.fldTotalInt.Top = 0.125F;
            this.fldTotalInt.Width = 1.125F;
            // 
            // fldTotalCost
            // 
            this.fldTotalCost.Height = 0.1875F;
            this.fldTotalCost.Left = 4.6875F;
            this.fldTotalCost.Name = "fldTotalCost";
            this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalCost.Text = null;
            this.fldTotalCost.Top = 0.125F;
            this.fldTotalCost.Width = 1.125F;
            // 
            // lnTotal
            // 
            this.lnTotal.Height = 0F;
            this.lnTotal.Left = 0.8125F;
            this.lnTotal.LineWeight = 1F;
            this.lnTotal.Name = "lnTotal";
            this.lnTotal.Top = 0.0625F;
            this.lnTotal.Width = 6.625F;
            this.lnTotal.X1 = 0.8125F;
            this.lnTotal.X2 = 7.4375F;
            this.lnTotal.Y1 = 0.0625F;
            this.lnTotal.Y2 = 0.0625F;
            // 
            // lblInfoGiven
            // 
            this.lblInfoGiven.Height = 0.1875F;
            this.lblInfoGiven.HyperLink = null;
            this.lblInfoGiven.Left = 1.25F;
            this.lblInfoGiven.Name = "lblInfoGiven";
            this.lblInfoGiven.Style = "font-family: \'Tahoma\'";
            this.lblInfoGiven.Text = "Information Given By:";
            this.lblInfoGiven.Top = 1.5F;
            this.lblInfoGiven.Width = 1.625F;
            // 
            // lblTitle
            // 
            this.lblTitle.Height = 0.1875F;
            this.lblTitle.HyperLink = null;
            this.lblTitle.Left = 1.25F;
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Style = "font-family: \'Tahoma\'";
            this.lblTitle.Text = "Title:";
            this.lblTitle.Top = 1.75F;
            this.lblTitle.Width = 0.625F;
            // 
            // lblSignDate
            // 
            this.lblSignDate.Height = 0.1875F;
            this.lblSignDate.HyperLink = null;
            this.lblSignDate.Left = 4.25F;
            this.lblSignDate.Name = "lblSignDate";
            this.lblSignDate.Style = "font-family: \'Tahoma\'";
            this.lblSignDate.Text = "Date:";
            this.lblSignDate.Top = 1.75F;
            this.lblSignDate.Width = 1.625F;
            // 
            // lnTitle
            // 
            this.lnTitle.Height = 0F;
            this.lnTitle.Left = 1.875F;
            this.lnTitle.LineWeight = 1F;
            this.lnTitle.Name = "lnTitle";
            this.lnTitle.Top = 1.9375F;
            this.lnTitle.Width = 2.3125F;
            this.lnTitle.X1 = 1.875F;
            this.lnTitle.X2 = 4.1875F;
            this.lnTitle.Y1 = 1.9375F;
            this.lnTitle.Y2 = 1.9375F;
            // 
            // lnSignerName
            // 
            this.lnSignerName.Height = 0F;
            this.lnSignerName.Left = 2.8125F;
            this.lnSignerName.LineWeight = 1F;
            this.lnSignerName.Name = "lnSignerName";
            this.lnSignerName.Top = 1.6875F;
            this.lnSignerName.Width = 3.0625F;
            this.lnSignerName.X1 = 2.8125F;
            this.lnSignerName.X2 = 5.875F;
            this.lnSignerName.Y1 = 1.6875F;
            this.lnSignerName.Y2 = 1.6875F;
            // 
            // fldTotalPD
            // 
            this.fldTotalPD.Height = 0.1875F;
            this.fldTotalPD.Left = 0.75F;
            this.fldTotalPD.Name = "fldTotalPD";
            this.fldTotalPD.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalPD.Text = null;
            this.fldTotalPD.Top = 0.125F;
            this.fldTotalPD.Width = 0.8125F;
            // 
            // lblDateDisclaimer
            // 
            this.lblDateDisclaimer.Height = 0.1875F;
            this.lblDateDisclaimer.HyperLink = null;
            this.lblDateDisclaimer.Left = 1.1875F;
            this.lblDateDisclaimer.Name = "lblDateDisclaimer";
            this.lblDateDisclaimer.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center";
            this.lblDateDisclaimer.Text = null;
            this.lblDateDisclaimer.Top = 2.25F;
            this.lblDateDisclaimer.Width = 4.8125F;
            // 
            // fldPeriodDate1
            // 
            this.fldPeriodDate1.Height = 0.1875F;
            this.fldPeriodDate1.Left = 0.9375F;
            this.fldPeriodDate1.Name = "fldPeriodDate1";
            this.fldPeriodDate1.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldPeriodDate1.Text = null;
            this.fldPeriodDate1.Top = 0.625F;
            this.fldPeriodDate1.Width = 1.1875F;
            // 
            // fldPeriodDate2
            // 
            this.fldPeriodDate2.Height = 0.1875F;
            this.fldPeriodDate2.Left = 0.9375F;
            this.fldPeriodDate2.Name = "fldPeriodDate2";
            this.fldPeriodDate2.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldPeriodDate2.Text = null;
            this.fldPeriodDate2.Top = 0.8125F;
            this.fldPeriodDate2.Width = 1.1875F;
            // 
            // fldPeriodDate3
            // 
            this.fldPeriodDate3.Height = 0.1875F;
            this.fldPeriodDate3.Left = 0.9375F;
            this.fldPeriodDate3.Name = "fldPeriodDate3";
            this.fldPeriodDate3.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldPeriodDate3.Text = null;
            this.fldPeriodDate3.Top = 1F;
            this.fldPeriodDate3.Width = 1.1875F;
            // 
            // fldPeriodDate4
            // 
            this.fldPeriodDate4.Height = 0.1875F;
            this.fldPeriodDate4.Left = 0.9375F;
            this.fldPeriodDate4.Name = "fldPeriodDate4";
            this.fldPeriodDate4.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.fldPeriodDate4.Text = null;
            this.fldPeriodDate4.Top = 1.1875F;
            this.fldPeriodDate4.Width = 1.1875F;
            // 
            // fldPeriodAmount1
            // 
            this.fldPeriodAmount1.Height = 0.1875F;
            this.fldPeriodAmount1.Left = 2.125F;
            this.fldPeriodAmount1.Name = "fldPeriodAmount1";
            this.fldPeriodAmount1.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount1.Text = null;
            this.fldPeriodAmount1.Top = 0.625F;
            this.fldPeriodAmount1.Width = 0.875F;
            // 
            // fldPeriodAmount2
            // 
            this.fldPeriodAmount2.Height = 0.1875F;
            this.fldPeriodAmount2.Left = 2.125F;
            this.fldPeriodAmount2.Name = "fldPeriodAmount2";
            this.fldPeriodAmount2.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount2.Text = null;
            this.fldPeriodAmount2.Top = 0.8125F;
            this.fldPeriodAmount2.Width = 0.875F;
            // 
            // fldPeriodAmount3
            // 
            this.fldPeriodAmount3.Height = 0.1875F;
            this.fldPeriodAmount3.Left = 2.125F;
            this.fldPeriodAmount3.Name = "fldPeriodAmount3";
            this.fldPeriodAmount3.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount3.Text = null;
            this.fldPeriodAmount3.Top = 1F;
            this.fldPeriodAmount3.Width = 0.875F;
            // 
            // fldPeriodAmount4
            // 
            this.fldPeriodAmount4.Height = 0.1875F;
            this.fldPeriodAmount4.Left = 2.125F;
            this.fldPeriodAmount4.Name = "fldPeriodAmount4";
            this.fldPeriodAmount4.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount4.Text = null;
            this.fldPeriodAmount4.Top = 1.1875F;
            this.fldPeriodAmount4.Width = 0.875F;
            // 
            // lblPeriodTitle
            // 
            this.lblPeriodTitle.Height = 0.1875F;
            this.lblPeriodTitle.HyperLink = null;
            this.lblPeriodTitle.Left = 0.9375F;
            this.lblPeriodTitle.Name = "lblPeriodTitle";
            this.lblPeriodTitle.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblPeriodTitle.Text = "Period Due";
            this.lblPeriodTitle.Top = 0.4375F;
            this.lblPeriodTitle.Visible = false;
            this.lblPeriodTitle.Width = 2.0625F;
            // 
            // fldPeriodAmount1Total
            // 
            this.fldPeriodAmount1Total.Height = 0.1875F;
            this.fldPeriodAmount1Total.Left = 6F;
            this.fldPeriodAmount1Total.Name = "fldPeriodAmount1Total";
            this.fldPeriodAmount1Total.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount1Total.Text = null;
            this.fldPeriodAmount1Total.Top = 0.625F;
            this.fldPeriodAmount1Total.Width = 1.4375F;
            // 
            // fldPeriodAmount1Int
            // 
            this.fldPeriodAmount1Int.Height = 0.1875F;
            this.fldPeriodAmount1Int.Left = 3.375F;
            this.fldPeriodAmount1Int.Name = "fldPeriodAmount1Int";
            this.fldPeriodAmount1Int.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount1Int.Text = null;
            this.fldPeriodAmount1Int.Top = 0.625F;
            this.fldPeriodAmount1Int.Width = 1.125F;
            // 
            // fldPeriodAmount1Cost
            // 
            this.fldPeriodAmount1Cost.Height = 0.1875F;
            this.fldPeriodAmount1Cost.Left = 4.6875F;
            this.fldPeriodAmount1Cost.Name = "fldPeriodAmount1Cost";
            this.fldPeriodAmount1Cost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount1Cost.Text = null;
            this.fldPeriodAmount1Cost.Top = 0.625F;
            this.fldPeriodAmount1Cost.Width = 1.125F;
            // 
            // fldPeriodAmount2Total
            // 
            this.fldPeriodAmount2Total.Height = 0.1875F;
            this.fldPeriodAmount2Total.Left = 6F;
            this.fldPeriodAmount2Total.Name = "fldPeriodAmount2Total";
            this.fldPeriodAmount2Total.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount2Total.Text = null;
            this.fldPeriodAmount2Total.Top = 0.8125F;
            this.fldPeriodAmount2Total.Width = 1.4375F;
            // 
            // fldPeriodAmount3Total
            // 
            this.fldPeriodAmount3Total.Height = 0.1875F;
            this.fldPeriodAmount3Total.Left = 6F;
            this.fldPeriodAmount3Total.Name = "fldPeriodAmount3Total";
            this.fldPeriodAmount3Total.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount3Total.Text = null;
            this.fldPeriodAmount3Total.Top = 1F;
            this.fldPeriodAmount3Total.Width = 1.4375F;
            // 
            // fldPeriodAmount4Total
            // 
            this.fldPeriodAmount4Total.Height = 0.1875F;
            this.fldPeriodAmount4Total.Left = 6F;
            this.fldPeriodAmount4Total.Name = "fldPeriodAmount4Total";
            this.fldPeriodAmount4Total.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmount4Total.Text = null;
            this.fldPeriodAmount4Total.Top = 1.1875F;
            this.fldPeriodAmount4Total.Width = 1.4375F;
            // 
            // fldPeriodAmountTotal
            // 
            this.fldPeriodAmountTotal.Height = 0.1875F;
            this.fldPeriodAmountTotal.Left = 6F;
            this.fldPeriodAmountTotal.Name = "fldPeriodAmountTotal";
            this.fldPeriodAmountTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPeriodAmountTotal.Text = null;
            this.fldPeriodAmountTotal.Top = 1.375F;
            this.fldPeriodAmountTotal.Width = 1.4375F;
            // 
            // lnPeriodTotal
            // 
            this.lnPeriodTotal.Height = 0F;
            this.lnPeriodTotal.Left = 6F;
            this.lnPeriodTotal.LineWeight = 1F;
            this.lnPeriodTotal.Name = "lnPeriodTotal";
            this.lnPeriodTotal.Top = 1.375F;
            this.lnPeriodTotal.Width = 1.4375F;
            this.lnPeriodTotal.X1 = 6F;
            this.lnPeriodTotal.X2 = 7.4375F;
            this.lnPeriodTotal.Y1 = 1.375F;
            this.lnPeriodTotal.Y2 = 1.375F;
            // 
            // lblTaxInfoMessage
            // 
            this.lblTaxInfoMessage.Height = 0.1875F;
            this.lblTaxInfoMessage.HyperLink = null;
            this.lblTaxInfoMessage.Left = 0.0625F;
            this.lblTaxInfoMessage.MultiLine = false;
            this.lblTaxInfoMessage.Name = "lblTaxInfoMessage";
            this.lblTaxInfoMessage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: center; white-space: nowrap";
            this.lblTaxInfoMessage.Text = null;
            this.lblTaxInfoMessage.Top = 2.0625F;
            this.lblTaxInfoMessage.Width = 7.375F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcreage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValuationTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLandValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuildingValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExemptValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblValualtionValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNameTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocationTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcreageTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLotTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDeedTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOutstandingTaxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEx3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTreeGrowthTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTreeGrowth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExVal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExVal2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExVal3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMillTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFarmTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFarm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastTaxTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExAmountTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpenTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZoningTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblZoning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaleDateTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSaleDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSalePriceTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSalePrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSFLATitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSFLA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrevYearTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrevYearTaxTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrevMill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInfoGiven)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSignDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateDisclaimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodDate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPeriodTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1Int)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount1Cost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount2Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount3Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmount4Total)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPeriodAmountTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxInfoMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPD;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInfoGiven;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSignDate;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSignerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDateDisclaimer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodDate1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodDate2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodDate3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodDate4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPeriodTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount1Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount1Int;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount1Cost;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount2Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount3Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmount4Total;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPeriodAmountTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnPeriodTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxInfoMessage;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTaxes;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAcreage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLand;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBuilding;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblValuationTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLandValue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBuildingValue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblExemptValue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblValualtionValue;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnAssessment;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBookPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNameTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocationTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAcreageTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLotTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDeedTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOutstandingTaxes;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEx1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblEx;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEx2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEx3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTreeGrowthTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTreeGrowth;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExVal1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExVal2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExVal3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMill;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMillTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFarmTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFarm;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLastTax;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLastTaxTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExAmountTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnExempt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOpenTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOpen;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPD;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZoningTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblZoning;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSaleDateTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSaleDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSalePriceTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSalePrice;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSFLATitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSFLA;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrevYearTax;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrevYearTaxTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrevMill;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
