namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptLienDischargeBP
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLienDischargeBP));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTopText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLienDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLienBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLienPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDischargeDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLDNBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLDNPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLienPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDischargeDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLDNBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLDNPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBottomText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldEnding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeading)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTopText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDischargeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLDNBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLDNPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDischargeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLDNBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLDNPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBottomText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEnding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldYear,
            this.fldMapLot,
            this.fldLienDate,
            this.fldLienBook,
            this.fldLienPage,
            this.fldDischargeDate,
            this.fldLDNBook,
            this.fldLDNPage});
            this.Detail.Height = 0.2083333F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDate,
            this.fldHeading,
            this.fldTopText,
            this.lblYear,
            this.lblMapLot,
            this.lblLienDate,
            this.lblLienBook,
            this.lblLienPage,
            this.lblDischargeDate,
            this.lblLDNBook,
            this.lblLDNPage,
            this.Line1});
            this.ReportHeader.Height = 2.364583F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.CanShrink = true;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldBottomText,
            this.fldEnding});
            this.ReportFooter.Height = 2.0625F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // fldDate
            // 
            this.fldDate.Height = 0.1875F;
            this.fldDate.Left = 0.0625F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'";
            this.fldDate.Text = null;
            this.fldDate.Top = 0.0625F;
            this.fldDate.Width = 1.4375F;
            // 
            // fldHeading
            // 
            this.fldHeading.Height = 0.5625F;
            this.fldHeading.Left = 0.0625F;
            this.fldHeading.Name = "fldHeading";
            this.fldHeading.Style = "font-family: \'Tahoma\'";
            this.fldHeading.Text = null;
            this.fldHeading.Top = 0.6875F;
            this.fldHeading.Width = 7.25F;
            // 
            // fldTopText
            // 
            this.fldTopText.Height = 0.75F;
            this.fldTopText.Left = 0.0625F;
            this.fldTopText.Name = "fldTopText";
            this.fldTopText.Style = "font-family: \'Tahoma\'";
            this.fldTopText.Text = null;
            this.fldTopText.Top = 1.25F;
            this.fldTopText.Width = 7.25F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'";
            this.lblYear.Text = null;
            this.lblYear.Top = 2.125F;
            this.lblYear.Width = 0.6875F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 0.6875F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'";
            this.lblMapLot.Text = null;
            this.lblMapLot.Top = 2.125F;
            this.lblMapLot.Width = 0.875F;
            // 
            // lblLienDate
            // 
            this.lblLienDate.Height = 0.1875F;
            this.lblLienDate.HyperLink = null;
            this.lblLienDate.Left = 1.5625F;
            this.lblLienDate.Name = "lblLienDate";
            this.lblLienDate.Style = "font-family: \'Tahoma\'";
            this.lblLienDate.Text = null;
            this.lblLienDate.Top = 2.125F;
            this.lblLienDate.Width = 1.1875F;
            // 
            // lblLienBook
            // 
            this.lblLienBook.Height = 0.1875F;
            this.lblLienBook.HyperLink = null;
            this.lblLienBook.Left = 2.75F;
            this.lblLienBook.Name = "lblLienBook";
            this.lblLienBook.Style = "font-family: \'Tahoma\'";
            this.lblLienBook.Text = null;
            this.lblLienBook.Top = 2.125F;
            this.lblLienBook.Width = 0.8125F;
            // 
            // lblLienPage
            // 
            this.lblLienPage.Height = 0.1875F;
            this.lblLienPage.HyperLink = null;
            this.lblLienPage.Left = 3.5625F;
            this.lblLienPage.Name = "lblLienPage";
            this.lblLienPage.Style = "font-family: \'Tahoma\'";
            this.lblLienPage.Text = null;
            this.lblLienPage.Top = 2.125F;
            this.lblLienPage.Width = 0.8125F;
            // 
            // lblDischargeDate
            // 
            this.lblDischargeDate.Height = 0.1875F;
            this.lblDischargeDate.HyperLink = null;
            this.lblDischargeDate.Left = 4.375F;
            this.lblDischargeDate.Name = "lblDischargeDate";
            this.lblDischargeDate.Style = "font-family: \'Tahoma\'";
            this.lblDischargeDate.Text = null;
            this.lblDischargeDate.Top = 2.125F;
            this.lblDischargeDate.Width = 1.3125F;
            // 
            // lblLDNBook
            // 
            this.lblLDNBook.Height = 0.1875F;
            this.lblLDNBook.HyperLink = null;
            this.lblLDNBook.Left = 5.6875F;
            this.lblLDNBook.Name = "lblLDNBook";
            this.lblLDNBook.Style = "font-family: \'Tahoma\'";
            this.lblLDNBook.Text = null;
            this.lblLDNBook.Top = 2.125F;
            this.lblLDNBook.Width = 0.8125F;
            // 
            // lblLDNPage
            // 
            this.lblLDNPage.Height = 0.1875F;
            this.lblLDNPage.HyperLink = null;
            this.lblLDNPage.Left = 6.5F;
            this.lblLDNPage.Name = "lblLDNPage";
            this.lblLDNPage.Style = "font-family: \'Tahoma\'";
            this.lblLDNPage.Text = null;
            this.lblLDNPage.Top = 2.125F;
            this.lblLDNPage.Width = 0.8125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 2.3125F;
            this.Line1.Width = 7.3125F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.3125F;
            this.Line1.Y1 = 2.3125F;
            this.Line1.Y2 = 2.3125F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 0F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 0.6875F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 0.6875F;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 0F;
            this.fldMapLot.Width = 0.875F;
            // 
            // fldLienDate
            // 
            this.fldLienDate.Height = 0.1875F;
            this.fldLienDate.Left = 1.5625F;
            this.fldLienDate.Name = "fldLienDate";
            this.fldLienDate.Style = "font-family: \'Tahoma\'";
            this.fldLienDate.Text = null;
            this.fldLienDate.Top = 0F;
            this.fldLienDate.Width = 1.1875F;
            // 
            // fldLienBook
            // 
            this.fldLienBook.Height = 0.1875F;
            this.fldLienBook.Left = 2.75F;
            this.fldLienBook.Name = "fldLienBook";
            this.fldLienBook.Style = "font-family: \'Tahoma\'";
            this.fldLienBook.Text = null;
            this.fldLienBook.Top = 0F;
            this.fldLienBook.Width = 0.8125F;
            // 
            // fldLienPage
            // 
            this.fldLienPage.Height = 0.1875F;
            this.fldLienPage.Left = 3.5625F;
            this.fldLienPage.Name = "fldLienPage";
            this.fldLienPage.Style = "font-family: \'Tahoma\'";
            this.fldLienPage.Text = null;
            this.fldLienPage.Top = 0F;
            this.fldLienPage.Width = 0.8125F;
            // 
            // fldDischargeDate
            // 
            this.fldDischargeDate.Height = 0.1875F;
            this.fldDischargeDate.Left = 4.375F;
            this.fldDischargeDate.Name = "fldDischargeDate";
            this.fldDischargeDate.Style = "font-family: \'Tahoma\'";
            this.fldDischargeDate.Text = null;
            this.fldDischargeDate.Top = 0F;
            this.fldDischargeDate.Width = 1.3125F;
            // 
            // fldLDNBook
            // 
            this.fldLDNBook.Height = 0.1875F;
            this.fldLDNBook.Left = 5.6875F;
            this.fldLDNBook.Name = "fldLDNBook";
            this.fldLDNBook.Style = "font-family: \'Tahoma\'";
            this.fldLDNBook.Text = null;
            this.fldLDNBook.Top = 0F;
            this.fldLDNBook.Width = 0.8125F;
            // 
            // fldLDNPage
            // 
            this.fldLDNPage.Height = 0.1875F;
            this.fldLDNPage.Left = 6.5F;
            this.fldLDNPage.Name = "fldLDNPage";
            this.fldLDNPage.Style = "font-family: \'Tahoma\'";
            this.fldLDNPage.Text = null;
            this.fldLDNPage.Top = 0F;
            this.fldLDNPage.Width = 0.8125F;
            // 
            // fldBottomText
            // 
            this.fldBottomText.Height = 0.8125F;
            this.fldBottomText.Left = 0.0625F;
            this.fldBottomText.Name = "fldBottomText";
            this.fldBottomText.Style = "font-family: \'Tahoma\'";
            this.fldBottomText.Text = null;
            this.fldBottomText.Top = 0.125F;
            this.fldBottomText.Width = 7.25F;
            // 
            // fldEnding
            // 
            this.fldEnding.Height = 1F;
            this.fldEnding.Left = 0.0625F;
            this.fldEnding.Name = "fldEnding";
            this.fldEnding.Style = "font-family: \'Tahoma\'";
            this.fldEnding.Text = null;
            this.fldEnding.Top = 0.9375F;
            this.fldEnding.Width = 7.25F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.385417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeading)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTopText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLienPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDischargeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLDNBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLDNPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLienPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDischargeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLDNBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLDNPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBottomText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldEnding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienBook;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDischargeDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLDNBook;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLDNPage;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeading;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTopText;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLienDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLienBook;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLienPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDischargeDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLDNBook;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLDNPage;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomText;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEnding;
    }
}
