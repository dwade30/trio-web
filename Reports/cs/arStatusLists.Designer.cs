namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class arStatusLists
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arStatusLists));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTaxDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAbatement = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAbatementPayments = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAbatement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAbatementPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAddress = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBuilding = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBuilding = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblExempt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldExempt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblLand = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldLand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReference2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldRef2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblReference = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldRef1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.srptSLAllActivityDetailOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.fldTADate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSpecialTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldSpecialTotals = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNonInterestDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalAbatement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalAbatementPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRTError = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSpecialTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSpecialText = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblSummaryPaymentType1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblSumPrin1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumHeaderType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummary1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldSummary1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSummaryPaymentType12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryPaymentType13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSummaryTotal13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumPrin13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumInt13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSumCost13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnSubtotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldTotalNonInterestDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblNonSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldnonsummary1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblNonSummaryTotal1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSumHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblNonSummaryTotal13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldSumCount1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAcctCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAbatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAbatementPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAbatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAbatementPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBuilding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExempt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTADate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpecialTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSpecialTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNonInterestDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAbatement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAbatementPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRTError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpecialTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpecialText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalNonInterestDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldnonsummary1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSumHeaderTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcctCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldName,
            this.fldYear,
            this.fldTaxDue,
            this.fldPaymentReceived,
            this.fldAbatement,
            this.fldDue,
            this.fldAbatementPayments,
            this.lblAddress,
            this.lblLocation,
            this.fldAddress,
            this.fldLocation,
            this.lblMapLot,
            this.lblBuilding,
            this.fldMapLot,
            this.fldBuilding,
            this.lblExempt,
            this.fldExempt,
            this.lblLand,
            this.fldLand,
            this.lblReference2,
            this.fldRef2,
            this.lblReference,
            this.fldRef1,
            this.srptSLAllActivityDetailOB,
            this.fldTADate,
            this.lblSpecialTotals,
            this.fldSpecialTotals,
            this.fldNonInterestDue});
            this.Detail.Height = 1.78125F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalTaxDue,
            this.fldTotalPaymentReceived,
            this.fldTotalAbatement,
            this.fldTotalDue,
            this.fldTotalAbatementPayments,
            this.lnTotals,
            this.lblTotals,
            this.lblRTError,
            this.lblSpecialTotal,
            this.lblSpecialText,
            this.lblSummaryHeader,
            this.line2,
            this.lblSummaryPaymentType1,
            this.lblSummaryPaymentType2,
            this.lblSummaryPaymentType3,
            this.lblSummaryPaymentType4,
            this.lblSummaryPaymentType5,
            this.lblSummaryPaymentType6,
            this.lblSummaryPaymentType7,
            this.lblSummaryPaymentType8,
            this.lblSummaryPaymentType9,
            this.lblSummaryPaymentType10,
            this.lblSummaryPaymentType11,
            this.lblSummaryTotal1,
            this.lblSummaryTotal2,
            this.lblSummaryTotal3,
            this.lblSummaryTotal4,
            this.lblSummaryTotal5,
            this.lblSummaryTotal6,
            this.lblSummaryTotal7,
            this.lblSummaryTotal8,
            this.lblSummaryTotal9,
            this.lblSummaryTotal10,
            this.lblSummaryTotal11,
            this.lnSummaryTotal,
            this.lblSumPrin1,
            this.lblSumPrin2,
            this.lblSumPrin3,
            this.lblSumPrin4,
            this.lblSumPrin5,
            this.lblSumPrin6,
            this.lblSumPrin7,
            this.lblSumPrin8,
            this.lblSumPrin9,
            this.lblSumPrin10,
            this.lblSumPrin11,
            this.lblSumInt1,
            this.lblSumInt2,
            this.lblSumInt3,
            this.lblSumInt4,
            this.lblSumInt5,
            this.lblSumInt6,
            this.lblSumInt7,
            this.lblSumInt8,
            this.lblSumInt9,
            this.lblSumInt10,
            this.lblSumInt11,
            this.lblSumCost1,
            this.lblSumCost2,
            this.lblSumCost3,
            this.lblSumCost4,
            this.lblSumCost5,
            this.lblSumCost6,
            this.lblSumCost7,
            this.lblSumCost8,
            this.lblSumCost9,
            this.lblSumCost10,
            this.lblSumCost11,
            this.lblSumHeaderType,
            this.lblSumHeaderTotal,
            this.lblSumHeaderPrin,
            this.lblSumHeaderInt,
            this.lblSumHeaderCost,
            this.lblSummary,
            this.lblSummary1,
            this.fldSummary1,
            this.lblSummaryPaymentType12,
            this.lblSummaryTotal12,
            this.lblSumPrin12,
            this.lblSumInt12,
            this.lblSumCost12,
            this.lblSummaryPaymentType13,
            this.lblSummaryTotal13,
            this.lblSumPrin13,
            this.lblSumInt13,
            this.lblSumCost13,
            this.lnSubtotal,
            this.fldTotalNonInterestDue,
            this.lblNonSummary,
            this.fldnonsummary1,
            this.Line1,
            this.lblNonSummaryTotal1,
            this.lblNonSummaryTotal2,
            this.lblNonSummaryTotal3,
            this.lblNonSummaryTotal4,
            this.lblNonSummaryTotal5,
            this.lblNonSummaryTotal6,
            this.lblNonSummaryTotal7,
            this.lblNonSummaryTotal8,
            this.lblNonSummaryTotal9,
            this.lblNonSummaryTotal10,
            this.lblNonSummaryTotal11,
            this.lblNonSumHeaderTotal,
            this.lblNonSummaryTotal12,
            this.lblNonSummaryTotal13,
            this.Line3,
            this.fldSumCount1,
            this.fldAcctCount});
            this.ReportFooter.Height = 7F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblAccount,
            this.lblYear,
            this.lblTaxDue,
            this.lblPaymentReceived,
            this.lblAbatement,
            this.lblDue,
            this.lblAbatementPayments,
            this.lblReportType,
            this.Label1});
            this.PageHeader.Height = 1.125F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1875F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Collection Status List";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.875F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.875F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.5625F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 1.125F;
            this.lnHeader.Width = 6.9375F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 6.9375F;
            this.lnHeader.Y1 = 1.125F;
            this.lnHeader.Y2 = 1.125F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0.1979167F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAccount.Text = "Acct";
            this.lblAccount.Top = 0.9375F;
            this.lblAccount.Width = 0.5F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0.8125F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblYear.Text = "Year";
            this.lblYear.Top = 0.9375F;
            this.lblYear.Width = 0.625F;
            // 
            // lblTaxDue
            // 
            this.lblTaxDue.Height = 0.4375F;
            this.lblTaxDue.HyperLink = null;
            this.lblTaxDue.Left = 1.875F;
            this.lblTaxDue.Name = "lblTaxDue";
            this.lblTaxDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblTaxDue.Text = "Property Tax Due";
            this.lblTaxDue.Top = 0.6875F;
            this.lblTaxDue.Width = 0.75F;
            // 
            // lblPaymentReceived
            // 
            this.lblPaymentReceived.Height = 0.4375F;
            this.lblPaymentReceived.HyperLink = null;
            this.lblPaymentReceived.Left = 3F;
            this.lblPaymentReceived.Name = "lblPaymentReceived";
            this.lblPaymentReceived.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblPaymentReceived.Text = "Payment Received";
            this.lblPaymentReceived.Top = 0.6875F;
            this.lblPaymentReceived.Width = 0.75F;
            // 
            // lblAbatement
            // 
            this.lblAbatement.Height = 0.4375F;
            this.lblAbatement.HyperLink = null;
            this.lblAbatement.Left = 4.125F;
            this.lblAbatement.Name = "lblAbatement";
            this.lblAbatement.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblAbatement.Text = "Abate - Adjust";
            this.lblAbatement.Top = 0.6875F;
            this.lblAbatement.Width = 0.6875F;
            // 
            // lblDue
            // 
            this.lblDue.Height = 0.4375F;
            this.lblDue.HyperLink = null;
            this.lblDue.Left = 5.9375F;
            this.lblDue.Name = "lblDue";
            this.lblDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblDue.Text = "Balance Due w/ Interest";
            this.lblDue.Top = 0.6875F;
            this.lblDue.Width = 0.9375F;
            // 
            // lblAbatementPayments
            // 
            this.lblAbatementPayments.Height = 0.4375F;
            this.lblAbatementPayments.HyperLink = null;
            this.lblAbatementPayments.Left = 6.902778F;
            this.lblAbatementPayments.Name = "lblAbatementPayments";
            this.lblAbatementPayments.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblAbatementPayments.Text = "Refund Abate";
            this.lblAbatementPayments.Top = 0.6875F;
            this.lblAbatementPayments.Width = 0.5902778F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.625F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0.8125F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = "Report Type";
            this.lblReportType.Top = 0.1875F;
            this.lblReportType.Width = 5.5F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.4375F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 5.125F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label1.Text = "Balance Due";
            this.Label1.Top = 0.6875F;
            this.Label1.Width = 0.75F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.2F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.7F;
            // 
            // fldName
            // 
            this.fldName.CanGrow = false;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.75F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 6.125F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 0.8125F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldYear.Text = null;
            this.fldYear.Top = 0.1875F;
            this.fldYear.Width = 0.625F;
            // 
            // fldTaxDue
            // 
            this.fldTaxDue.Height = 0.1875F;
            this.fldTaxDue.Left = 1.5625F;
            this.fldTaxDue.Name = "fldTaxDue";
            this.fldTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxDue.Text = "0.00";
            this.fldTaxDue.Top = 0.1875F;
            this.fldTaxDue.Width = 1.0625F;
            // 
            // fldPaymentReceived
            // 
            this.fldPaymentReceived.Height = 0.1875F;
            this.fldPaymentReceived.Left = 2.625F;
            this.fldPaymentReceived.Name = "fldPaymentReceived";
            this.fldPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPaymentReceived.Text = "0.00";
            this.fldPaymentReceived.Top = 0.1875F;
            this.fldPaymentReceived.Width = 1.0625F;
            // 
            // fldAbatement
            // 
            this.fldAbatement.Height = 0.1875F;
            this.fldAbatement.Left = 3.6875F;
            this.fldAbatement.Name = "fldAbatement";
            this.fldAbatement.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAbatement.Text = "0.00";
            this.fldAbatement.Top = 0.1875F;
            this.fldAbatement.Width = 1.0625F;
            // 
            // fldDue
            // 
            this.fldDue.Height = 0.1875F;
            this.fldDue.Left = 5.8125F;
            this.fldDue.Name = "fldDue";
            this.fldDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldDue.Text = "0.00";
            this.fldDue.Top = 0.1875F;
            this.fldDue.Width = 1.0625F;
            // 
            // fldAbatementPayments
            // 
            this.fldAbatementPayments.Height = 0.1875F;
            this.fldAbatementPayments.Left = 6.875F;
            this.fldAbatementPayments.Name = "fldAbatementPayments";
            this.fldAbatementPayments.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAbatementPayments.Text = "0.00";
            this.fldAbatementPayments.Top = 0.1875F;
            this.fldAbatementPayments.Width = 0.625F;
            // 
            // lblAddress
            // 
            this.lblAddress.Height = 0.1875F;
            this.lblAddress.HyperLink = null;
            this.lblAddress.Left = 0F;
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Style = "font-family: \'Tahoma\'";
            this.lblAddress.Text = "Address:";
            this.lblAddress.Top = 0.625F;
            this.lblAddress.Width = 0.75F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 0F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'";
            this.lblLocation.Text = "Location:";
            this.lblLocation.Top = 0.8125F;
            this.lblLocation.Width = 0.75F;
            // 
            // fldAddress
            // 
            this.fldAddress.CanGrow = false;
            this.fldAddress.Height = 0.1875F;
            this.fldAddress.Left = 0.75F;
            this.fldAddress.Name = "fldAddress";
            this.fldAddress.Style = "font-family: \'Tahoma\'";
            this.fldAddress.Text = null;
            this.fldAddress.Top = 0.625F;
            this.fldAddress.Width = 6.1875F;
            // 
            // fldLocation
            // 
            this.fldLocation.Height = 0.1875F;
            this.fldLocation.Left = 0.75F;
            this.fldLocation.Name = "fldLocation";
            this.fldLocation.Style = "font-family: \'Tahoma\'";
            this.fldLocation.Text = null;
            this.fldLocation.Top = 0.8125F;
            this.fldLocation.Width = 6.1875F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 0F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'";
            this.lblMapLot.Text = "Map Lot:";
            this.lblMapLot.Top = 1F;
            this.lblMapLot.Width = 0.75F;
            // 
            // lblBuilding
            // 
            this.lblBuilding.Height = 0.1875F;
            this.lblBuilding.HyperLink = null;
            this.lblBuilding.Left = 1.625F;
            this.lblBuilding.Name = "lblBuilding";
            this.lblBuilding.Style = "font-family: \'Tahoma\'";
            this.lblBuilding.Text = "Building:";
            this.lblBuilding.Top = 1F;
            this.lblBuilding.Width = 0.625F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.CanGrow = false;
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 0.75F;
            this.fldMapLot.MultiLine = false;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 1F;
            this.fldMapLot.Width = 0.875F;
            // 
            // fldBuilding
            // 
            this.fldBuilding.CanGrow = false;
            this.fldBuilding.Height = 0.1875F;
            this.fldBuilding.Left = 2.1875F;
            this.fldBuilding.MultiLine = false;
            this.fldBuilding.Name = "fldBuilding";
            this.fldBuilding.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldBuilding.Text = null;
            this.fldBuilding.Top = 1F;
            this.fldBuilding.Width = 1F;
            // 
            // lblExempt
            // 
            this.lblExempt.Height = 0.1875F;
            this.lblExempt.HyperLink = null;
            this.lblExempt.Left = 1.625F;
            this.lblExempt.Name = "lblExempt";
            this.lblExempt.Style = "font-family: \'Tahoma\'";
            this.lblExempt.Text = "Exempt:";
            this.lblExempt.Top = 1.375F;
            this.lblExempt.Width = 0.625F;
            // 
            // fldExempt
            // 
            this.fldExempt.CanGrow = false;
            this.fldExempt.Height = 0.1875F;
            this.fldExempt.Left = 2.1875F;
            this.fldExempt.MultiLine = false;
            this.fldExempt.Name = "fldExempt";
            this.fldExempt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldExempt.Text = null;
            this.fldExempt.Top = 1.375F;
            this.fldExempt.Width = 1F;
            // 
            // lblLand
            // 
            this.lblLand.Height = 0.1875F;
            this.lblLand.HyperLink = null;
            this.lblLand.Left = 1.625F;
            this.lblLand.Name = "lblLand";
            this.lblLand.Style = "font-family: \'Tahoma\'";
            this.lblLand.Text = "Land";
            this.lblLand.Top = 1.1875F;
            this.lblLand.Width = 0.625F;
            // 
            // fldLand
            // 
            this.fldLand.CanGrow = false;
            this.fldLand.Height = 0.1875F;
            this.fldLand.Left = 2.1875F;
            this.fldLand.MultiLine = false;
            this.fldLand.Name = "fldLand";
            this.fldLand.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldLand.Text = null;
            this.fldLand.Top = 1.1875F;
            this.fldLand.Width = 1F;
            // 
            // lblReference2
            // 
            this.lblReference2.Height = 0.1875F;
            this.lblReference2.HyperLink = null;
            this.lblReference2.Left = 0F;
            this.lblReference2.Name = "lblReference2";
            this.lblReference2.Style = "font-family: \'Tahoma\'";
            this.lblReference2.Text = "Reference:";
            this.lblReference2.Top = 1.375F;
            this.lblReference2.Width = 0.75F;
            // 
            // fldRef2
            // 
            this.fldRef2.CanGrow = false;
            this.fldRef2.Height = 0.1875F;
            this.fldRef2.Left = 0.75F;
            this.fldRef2.MultiLine = false;
            this.fldRef2.Name = "fldRef2";
            this.fldRef2.Style = "font-family: \'Tahoma\'";
            this.fldRef2.Text = null;
            this.fldRef2.Top = 1.375F;
            this.fldRef2.Width = 0.875F;
            // 
            // lblReference
            // 
            this.lblReference.Height = 0.1875F;
            this.lblReference.HyperLink = null;
            this.lblReference.Left = 0F;
            this.lblReference.Name = "lblReference";
            this.lblReference.Style = "font-family: \'Tahoma\'";
            this.lblReference.Text = "Reference:";
            this.lblReference.Top = 1.1875F;
            this.lblReference.Width = 0.75F;
            // 
            // fldRef1
            // 
            this.fldRef1.CanGrow = false;
            this.fldRef1.Height = 0.1875F;
            this.fldRef1.Left = 0.75F;
            this.fldRef1.MultiLine = false;
            this.fldRef1.Name = "fldRef1";
            this.fldRef1.Style = "font-family: \'Tahoma\'";
            this.fldRef1.Text = null;
            this.fldRef1.Top = 1.1875F;
            this.fldRef1.Width = 0.875F;
            // 
            // srptSLAllActivityDetailOB
            // 
            this.srptSLAllActivityDetailOB.CloseBorder = false;
            this.srptSLAllActivityDetailOB.Height = 0.125F;
            this.srptSLAllActivityDetailOB.Left = 0F;
            this.srptSLAllActivityDetailOB.Name = "srptSLAllActivityDetailOB";
            this.srptSLAllActivityDetailOB.Report = null;
            this.srptSLAllActivityDetailOB.Top = 1.625F;
            this.srptSLAllActivityDetailOB.Visible = false;
            this.srptSLAllActivityDetailOB.Width = 7.5F;
            // 
            // fldTADate
            // 
            this.fldTADate.Height = 0.1875F;
            this.fldTADate.Left = 5.75F;
            this.fldTADate.Name = "fldTADate";
            this.fldTADate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTADate.Text = null;
            this.fldTADate.Top = 0F;
            this.fldTADate.Visible = false;
            this.fldTADate.Width = 1.25F;
            // 
            // lblSpecialTotals
            // 
            this.lblSpecialTotals.Height = 0.1875F;
            this.lblSpecialTotals.HyperLink = null;
            this.lblSpecialTotals.Left = 0F;
            this.lblSpecialTotals.Name = "lblSpecialTotals";
            this.lblSpecialTotals.Style = "font-family: \'Tahoma\'";
            this.lblSpecialTotals.Text = "Abatements:";
            this.lblSpecialTotals.Top = 0.4375F;
            this.lblSpecialTotals.Width = 0.75F;
            // 
            // fldSpecialTotals
            // 
            this.fldSpecialTotals.CanGrow = false;
            this.fldSpecialTotals.CanShrink = true;
            this.fldSpecialTotals.Height = 0.1875F;
            this.fldSpecialTotals.Left = 0.75F;
            this.fldSpecialTotals.Name = "fldSpecialTotals";
            this.fldSpecialTotals.Style = "font-family: \'Tahoma\'";
            this.fldSpecialTotals.Text = null;
            this.fldSpecialTotals.Top = 0.4375F;
            this.fldSpecialTotals.Width = 6.1875F;
            // 
            // fldNonInterestDue
            // 
            this.fldNonInterestDue.Height = 0.1875F;
            this.fldNonInterestDue.Left = 4.75F;
            this.fldNonInterestDue.Name = "fldNonInterestDue";
            this.fldNonInterestDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldNonInterestDue.Text = "0.00";
            this.fldNonInterestDue.Top = 0.1875F;
            this.fldNonInterestDue.Width = 1.0625F;
            // 
            // fldTotalTaxDue
            // 
            this.fldTotalTaxDue.Height = 0.1875F;
            this.fldTotalTaxDue.Left = 1.375F;
            this.fldTotalTaxDue.Name = "fldTotalTaxDue";
            this.fldTotalTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTaxDue.Text = "0,000,000,000.00";
            this.fldTotalTaxDue.Top = 0.0625F;
            this.fldTotalTaxDue.Width = 1.25F;
            // 
            // fldTotalPaymentReceived
            // 
            this.fldTotalPaymentReceived.Height = 0.1875F;
            this.fldTotalPaymentReceived.Left = 2.375F;
            this.fldTotalPaymentReceived.Name = "fldTotalPaymentReceived";
            this.fldTotalPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPaymentReceived.Text = "0.00";
            this.fldTotalPaymentReceived.Top = 0.25F;
            this.fldTotalPaymentReceived.Width = 1.3125F;
            // 
            // fldTotalAbatement
            // 
            this.fldTotalAbatement.Height = 0.1875F;
            this.fldTotalAbatement.Left = 3.5F;
            this.fldTotalAbatement.Name = "fldTotalAbatement";
            this.fldTotalAbatement.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalAbatement.Text = "0.00";
            this.fldTotalAbatement.Top = 0.0625F;
            this.fldTotalAbatement.Width = 1.25F;
            // 
            // fldTotalDue
            // 
            this.fldTotalDue.Height = 0.1875F;
            this.fldTotalDue.Left = 5.5625F;
            this.fldTotalDue.Name = "fldTotalDue";
            this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalDue.Text = "0.00";
            this.fldTotalDue.Top = 0.0625F;
            this.fldTotalDue.Width = 1.3125F;
            // 
            // fldTotalAbatementPayments
            // 
            this.fldTotalAbatementPayments.Height = 0.1875F;
            this.fldTotalAbatementPayments.Left = 6.375F;
            this.fldTotalAbatementPayments.Name = "fldTotalAbatementPayments";
            this.fldTotalAbatementPayments.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalAbatementPayments.Text = "0.00";
            this.fldTotalAbatementPayments.Top = 0.25F;
            this.fldTotalAbatementPayments.Width = 1.125F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 1.25F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Width = 6.25F;
            this.lnTotals.X1 = 1.25F;
            this.lnTotals.X2 = 7.5F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.1875F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 0.0625F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTotals.Text = "Total for XXXX Bills:";
            this.lblTotals.Top = 0.0625F;
            this.lblTotals.Width = 1.4375F;
            // 
            // lblRTError
            // 
            this.lblRTError.Height = 0.3125F;
            this.lblRTError.HyperLink = null;
            this.lblRTError.Left = 0F;
            this.lblRTError.Name = "lblRTError";
            this.lblRTError.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblRTError.Text = "  ";
            this.lblRTError.Top = 6.6875F;
            this.lblRTError.Width = 7F;
            // 
            // lblSpecialTotal
            // 
            this.lblSpecialTotal.Height = 0.1875F;
            this.lblSpecialTotal.HyperLink = null;
            this.lblSpecialTotal.Left = 3.3125F;
            this.lblSpecialTotal.Name = "lblSpecialTotal";
            this.lblSpecialTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSpecialTotal.Text = null;
            this.lblSpecialTotal.Top = 0.4375F;
            this.lblSpecialTotal.Width = 2.8125F;
            // 
            // lblSpecialText
            // 
            this.lblSpecialText.Height = 0.1875F;
            this.lblSpecialText.HyperLink = null;
            this.lblSpecialText.Left = 0.3125F;
            this.lblSpecialText.Name = "lblSpecialText";
            this.lblSpecialText.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
            this.lblSpecialText.Text = null;
            this.lblSpecialText.Top = 0.4375F;
            this.lblSpecialText.Width = 3F;
            // 
            // lblSummaryHeader
            // 
            this.lblSummaryHeader.Height = 0.1875F;
            this.lblSummaryHeader.HyperLink = null;
            this.lblSummaryHeader.Left = 0.3125F;
            this.lblSummaryHeader.Name = "lblSummaryHeader";
            this.lblSummaryHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblSummaryHeader.Text = "Payment Summary";
            this.lblSummaryHeader.Top = 0.625F;
            this.lblSummaryHeader.Width = 7.1875F;
            // 
            // line2
            // 
            this.line2.Height = 0F;
            this.line2.Left = 0.3125F;
            this.line2.LineWeight = 1F;
            this.line2.Name = "line2";
            this.line2.Top = 0.8125F;
            this.line2.Width = 6.25F;
            this.line2.X1 = 0.3125F;
            this.line2.X2 = 6.5625F;
            this.line2.Y1 = 0.8125F;
            this.line2.Y2 = 0.8125F;
            // 
            // lblSummaryPaymentType1
            // 
            this.lblSummaryPaymentType1.Height = 0.1875F;
            this.lblSummaryPaymentType1.HyperLink = null;
            this.lblSummaryPaymentType1.Left = 0.3125F;
            this.lblSummaryPaymentType1.MultiLine = false;
            this.lblSummaryPaymentType1.Name = "lblSummaryPaymentType1";
            this.lblSummaryPaymentType1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType1.Text = null;
            this.lblSummaryPaymentType1.Top = 1F;
            this.lblSummaryPaymentType1.Width = 2.5F;
            // 
            // lblSummaryPaymentType2
            // 
            this.lblSummaryPaymentType2.Height = 0.1875F;
            this.lblSummaryPaymentType2.HyperLink = null;
            this.lblSummaryPaymentType2.Left = 0.3125F;
            this.lblSummaryPaymentType2.MultiLine = false;
            this.lblSummaryPaymentType2.Name = "lblSummaryPaymentType2";
            this.lblSummaryPaymentType2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType2.Text = null;
            this.lblSummaryPaymentType2.Top = 1.1875F;
            this.lblSummaryPaymentType2.Width = 2.5F;
            // 
            // lblSummaryPaymentType3
            // 
            this.lblSummaryPaymentType3.Height = 0.1875F;
            this.lblSummaryPaymentType3.HyperLink = null;
            this.lblSummaryPaymentType3.Left = 0.3125F;
            this.lblSummaryPaymentType3.MultiLine = false;
            this.lblSummaryPaymentType3.Name = "lblSummaryPaymentType3";
            this.lblSummaryPaymentType3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType3.Text = null;
            this.lblSummaryPaymentType3.Top = 1.375F;
            this.lblSummaryPaymentType3.Width = 2.5F;
            // 
            // lblSummaryPaymentType4
            // 
            this.lblSummaryPaymentType4.Height = 0.1875F;
            this.lblSummaryPaymentType4.HyperLink = null;
            this.lblSummaryPaymentType4.Left = 0.3125F;
            this.lblSummaryPaymentType4.MultiLine = false;
            this.lblSummaryPaymentType4.Name = "lblSummaryPaymentType4";
            this.lblSummaryPaymentType4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType4.Text = null;
            this.lblSummaryPaymentType4.Top = 1.5625F;
            this.lblSummaryPaymentType4.Width = 2.5F;
            // 
            // lblSummaryPaymentType5
            // 
            this.lblSummaryPaymentType5.Height = 0.1875F;
            this.lblSummaryPaymentType5.HyperLink = null;
            this.lblSummaryPaymentType5.Left = 0.3125F;
            this.lblSummaryPaymentType5.MultiLine = false;
            this.lblSummaryPaymentType5.Name = "lblSummaryPaymentType5";
            this.lblSummaryPaymentType5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType5.Text = null;
            this.lblSummaryPaymentType5.Top = 1.75F;
            this.lblSummaryPaymentType5.Width = 2.5F;
            // 
            // lblSummaryPaymentType6
            // 
            this.lblSummaryPaymentType6.Height = 0.1875F;
            this.lblSummaryPaymentType6.HyperLink = null;
            this.lblSummaryPaymentType6.Left = 0.3125F;
            this.lblSummaryPaymentType6.MultiLine = false;
            this.lblSummaryPaymentType6.Name = "lblSummaryPaymentType6";
            this.lblSummaryPaymentType6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType6.Text = null;
            this.lblSummaryPaymentType6.Top = 1.9375F;
            this.lblSummaryPaymentType6.Width = 2.5F;
            // 
            // lblSummaryPaymentType7
            // 
            this.lblSummaryPaymentType7.Height = 0.1875F;
            this.lblSummaryPaymentType7.HyperLink = null;
            this.lblSummaryPaymentType7.Left = 0.3125F;
            this.lblSummaryPaymentType7.MultiLine = false;
            this.lblSummaryPaymentType7.Name = "lblSummaryPaymentType7";
            this.lblSummaryPaymentType7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType7.Text = null;
            this.lblSummaryPaymentType7.Top = 2.125F;
            this.lblSummaryPaymentType7.Width = 2.5F;
            // 
            // lblSummaryPaymentType8
            // 
            this.lblSummaryPaymentType8.Height = 0.1875F;
            this.lblSummaryPaymentType8.HyperLink = null;
            this.lblSummaryPaymentType8.Left = 0.3125F;
            this.lblSummaryPaymentType8.MultiLine = false;
            this.lblSummaryPaymentType8.Name = "lblSummaryPaymentType8";
            this.lblSummaryPaymentType8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType8.Text = null;
            this.lblSummaryPaymentType8.Top = 2.3125F;
            this.lblSummaryPaymentType8.Width = 2.5F;
            // 
            // lblSummaryPaymentType9
            // 
            this.lblSummaryPaymentType9.Height = 0.1875F;
            this.lblSummaryPaymentType9.HyperLink = null;
            this.lblSummaryPaymentType9.Left = 0.3125F;
            this.lblSummaryPaymentType9.MultiLine = false;
            this.lblSummaryPaymentType9.Name = "lblSummaryPaymentType9";
            this.lblSummaryPaymentType9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType9.Text = null;
            this.lblSummaryPaymentType9.Top = 2.5F;
            this.lblSummaryPaymentType9.Width = 2.5F;
            // 
            // lblSummaryPaymentType10
            // 
            this.lblSummaryPaymentType10.Height = 0.1875F;
            this.lblSummaryPaymentType10.HyperLink = null;
            this.lblSummaryPaymentType10.Left = 0.3125F;
            this.lblSummaryPaymentType10.MultiLine = false;
            this.lblSummaryPaymentType10.Name = "lblSummaryPaymentType10";
            this.lblSummaryPaymentType10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType10.Text = null;
            this.lblSummaryPaymentType10.Top = 2.6875F;
            this.lblSummaryPaymentType10.Width = 2.5F;
            // 
            // lblSummaryPaymentType11
            // 
            this.lblSummaryPaymentType11.Height = 0.1875F;
            this.lblSummaryPaymentType11.HyperLink = null;
            this.lblSummaryPaymentType11.Left = 0.3125F;
            this.lblSummaryPaymentType11.MultiLine = false;
            this.lblSummaryPaymentType11.Name = "lblSummaryPaymentType11";
            this.lblSummaryPaymentType11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType11.Text = null;
            this.lblSummaryPaymentType11.Top = 2.875F;
            this.lblSummaryPaymentType11.Width = 2.5F;
            // 
            // lblSummaryTotal1
            // 
            this.lblSummaryTotal1.Height = 0.1875F;
            this.lblSummaryTotal1.HyperLink = null;
            this.lblSummaryTotal1.Left = 6.5F;
            this.lblSummaryTotal1.Name = "lblSummaryTotal1";
            this.lblSummaryTotal1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal1.Text = null;
            this.lblSummaryTotal1.Top = 1F;
            this.lblSummaryTotal1.Width = 1F;
            // 
            // lblSummaryTotal2
            // 
            this.lblSummaryTotal2.Height = 0.1875F;
            this.lblSummaryTotal2.HyperLink = null;
            this.lblSummaryTotal2.Left = 6.5F;
            this.lblSummaryTotal2.Name = "lblSummaryTotal2";
            this.lblSummaryTotal2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal2.Text = null;
            this.lblSummaryTotal2.Top = 1.1875F;
            this.lblSummaryTotal2.Width = 1F;
            // 
            // lblSummaryTotal3
            // 
            this.lblSummaryTotal3.Height = 0.1875F;
            this.lblSummaryTotal3.HyperLink = null;
            this.lblSummaryTotal3.Left = 6.5F;
            this.lblSummaryTotal3.Name = "lblSummaryTotal3";
            this.lblSummaryTotal3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal3.Text = null;
            this.lblSummaryTotal3.Top = 1.375F;
            this.lblSummaryTotal3.Width = 1F;
            // 
            // lblSummaryTotal4
            // 
            this.lblSummaryTotal4.Height = 0.1875F;
            this.lblSummaryTotal4.HyperLink = null;
            this.lblSummaryTotal4.Left = 6.5F;
            this.lblSummaryTotal4.Name = "lblSummaryTotal4";
            this.lblSummaryTotal4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal4.Text = null;
            this.lblSummaryTotal4.Top = 1.5625F;
            this.lblSummaryTotal4.Width = 1F;
            // 
            // lblSummaryTotal5
            // 
            this.lblSummaryTotal5.Height = 0.1875F;
            this.lblSummaryTotal5.HyperLink = null;
            this.lblSummaryTotal5.Left = 6.5F;
            this.lblSummaryTotal5.Name = "lblSummaryTotal5";
            this.lblSummaryTotal5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal5.Text = null;
            this.lblSummaryTotal5.Top = 1.75F;
            this.lblSummaryTotal5.Width = 1F;
            // 
            // lblSummaryTotal6
            // 
            this.lblSummaryTotal6.Height = 0.1875F;
            this.lblSummaryTotal6.HyperLink = null;
            this.lblSummaryTotal6.Left = 6.5F;
            this.lblSummaryTotal6.Name = "lblSummaryTotal6";
            this.lblSummaryTotal6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal6.Text = null;
            this.lblSummaryTotal6.Top = 1.9375F;
            this.lblSummaryTotal6.Width = 1F;
            // 
            // lblSummaryTotal7
            // 
            this.lblSummaryTotal7.Height = 0.1875F;
            this.lblSummaryTotal7.HyperLink = null;
            this.lblSummaryTotal7.Left = 6.5F;
            this.lblSummaryTotal7.Name = "lblSummaryTotal7";
            this.lblSummaryTotal7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal7.Text = null;
            this.lblSummaryTotal7.Top = 2.125F;
            this.lblSummaryTotal7.Width = 1F;
            // 
            // lblSummaryTotal8
            // 
            this.lblSummaryTotal8.Height = 0.1875F;
            this.lblSummaryTotal8.HyperLink = null;
            this.lblSummaryTotal8.Left = 6.5F;
            this.lblSummaryTotal8.Name = "lblSummaryTotal8";
            this.lblSummaryTotal8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal8.Text = null;
            this.lblSummaryTotal8.Top = 2.3125F;
            this.lblSummaryTotal8.Width = 1F;
            // 
            // lblSummaryTotal9
            // 
            this.lblSummaryTotal9.Height = 0.1875F;
            this.lblSummaryTotal9.HyperLink = null;
            this.lblSummaryTotal9.Left = 6.5F;
            this.lblSummaryTotal9.Name = "lblSummaryTotal9";
            this.lblSummaryTotal9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal9.Text = null;
            this.lblSummaryTotal9.Top = 2.5F;
            this.lblSummaryTotal9.Width = 1F;
            // 
            // lblSummaryTotal10
            // 
            this.lblSummaryTotal10.Height = 0.1875F;
            this.lblSummaryTotal10.HyperLink = null;
            this.lblSummaryTotal10.Left = 6.5F;
            this.lblSummaryTotal10.Name = "lblSummaryTotal10";
            this.lblSummaryTotal10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal10.Text = null;
            this.lblSummaryTotal10.Top = 2.6875F;
            this.lblSummaryTotal10.Width = 1F;
            // 
            // lblSummaryTotal11
            // 
            this.lblSummaryTotal11.Height = 0.1875F;
            this.lblSummaryTotal11.HyperLink = null;
            this.lblSummaryTotal11.Left = 6.5F;
            this.lblSummaryTotal11.Name = "lblSummaryTotal11";
            this.lblSummaryTotal11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal11.Text = null;
            this.lblSummaryTotal11.Top = 2.875F;
            this.lblSummaryTotal11.Width = 1F;
            // 
            // lnSummaryTotal
            // 
            this.lnSummaryTotal.Height = 0F;
            this.lnSummaryTotal.Left = 3.75F;
            this.lnSummaryTotal.LineWeight = 1F;
            this.lnSummaryTotal.Name = "lnSummaryTotal";
            this.lnSummaryTotal.Top = 3.25F;
            this.lnSummaryTotal.Width = 3.75F;
            this.lnSummaryTotal.X1 = 3.75F;
            this.lnSummaryTotal.X2 = 7.5F;
            this.lnSummaryTotal.Y1 = 3.25F;
            this.lnSummaryTotal.Y2 = 3.25F;
            // 
            // lblSumPrin1
            // 
            this.lblSumPrin1.Height = 0.1875F;
            this.lblSumPrin1.HyperLink = null;
            this.lblSumPrin1.Left = 2.8125F;
            this.lblSumPrin1.Name = "lblSumPrin1";
            this.lblSumPrin1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin1.Text = null;
            this.lblSumPrin1.Top = 1F;
            this.lblSumPrin1.Width = 1F;
            // 
            // lblSumPrin2
            // 
            this.lblSumPrin2.Height = 0.1875F;
            this.lblSumPrin2.HyperLink = null;
            this.lblSumPrin2.Left = 2.8125F;
            this.lblSumPrin2.Name = "lblSumPrin2";
            this.lblSumPrin2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin2.Text = null;
            this.lblSumPrin2.Top = 1.1875F;
            this.lblSumPrin2.Width = 1F;
            // 
            // lblSumPrin3
            // 
            this.lblSumPrin3.Height = 0.1875F;
            this.lblSumPrin3.HyperLink = null;
            this.lblSumPrin3.Left = 2.8125F;
            this.lblSumPrin3.Name = "lblSumPrin3";
            this.lblSumPrin3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin3.Text = null;
            this.lblSumPrin3.Top = 1.375F;
            this.lblSumPrin3.Width = 1F;
            // 
            // lblSumPrin4
            // 
            this.lblSumPrin4.Height = 0.1875F;
            this.lblSumPrin4.HyperLink = null;
            this.lblSumPrin4.Left = 2.8125F;
            this.lblSumPrin4.Name = "lblSumPrin4";
            this.lblSumPrin4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin4.Text = null;
            this.lblSumPrin4.Top = 1.5625F;
            this.lblSumPrin4.Width = 1F;
            // 
            // lblSumPrin5
            // 
            this.lblSumPrin5.Height = 0.1875F;
            this.lblSumPrin5.HyperLink = null;
            this.lblSumPrin5.Left = 2.8125F;
            this.lblSumPrin5.Name = "lblSumPrin5";
            this.lblSumPrin5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin5.Text = null;
            this.lblSumPrin5.Top = 1.75F;
            this.lblSumPrin5.Width = 1F;
            // 
            // lblSumPrin6
            // 
            this.lblSumPrin6.Height = 0.1875F;
            this.lblSumPrin6.HyperLink = null;
            this.lblSumPrin6.Left = 2.8125F;
            this.lblSumPrin6.Name = "lblSumPrin6";
            this.lblSumPrin6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin6.Text = null;
            this.lblSumPrin6.Top = 1.9375F;
            this.lblSumPrin6.Width = 1F;
            // 
            // lblSumPrin7
            // 
            this.lblSumPrin7.Height = 0.1875F;
            this.lblSumPrin7.HyperLink = null;
            this.lblSumPrin7.Left = 2.8125F;
            this.lblSumPrin7.Name = "lblSumPrin7";
            this.lblSumPrin7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin7.Text = null;
            this.lblSumPrin7.Top = 2.125F;
            this.lblSumPrin7.Width = 1F;
            // 
            // lblSumPrin8
            // 
            this.lblSumPrin8.Height = 0.1875F;
            this.lblSumPrin8.HyperLink = null;
            this.lblSumPrin8.Left = 2.8125F;
            this.lblSumPrin8.Name = "lblSumPrin8";
            this.lblSumPrin8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin8.Text = null;
            this.lblSumPrin8.Top = 2.3125F;
            this.lblSumPrin8.Width = 1F;
            // 
            // lblSumPrin9
            // 
            this.lblSumPrin9.Height = 0.1875F;
            this.lblSumPrin9.HyperLink = null;
            this.lblSumPrin9.Left = 2.8125F;
            this.lblSumPrin9.Name = "lblSumPrin9";
            this.lblSumPrin9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin9.Text = null;
            this.lblSumPrin9.Top = 2.5F;
            this.lblSumPrin9.Width = 1F;
            // 
            // lblSumPrin10
            // 
            this.lblSumPrin10.Height = 0.1875F;
            this.lblSumPrin10.HyperLink = null;
            this.lblSumPrin10.Left = 2.8125F;
            this.lblSumPrin10.Name = "lblSumPrin10";
            this.lblSumPrin10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin10.Text = null;
            this.lblSumPrin10.Top = 2.6875F;
            this.lblSumPrin10.Width = 1F;
            // 
            // lblSumPrin11
            // 
            this.lblSumPrin11.Height = 0.1875F;
            this.lblSumPrin11.HyperLink = null;
            this.lblSumPrin11.Left = 2.8125F;
            this.lblSumPrin11.Name = "lblSumPrin11";
            this.lblSumPrin11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin11.Text = null;
            this.lblSumPrin11.Top = 2.875F;
            this.lblSumPrin11.Width = 1F;
            // 
            // lblSumInt1
            // 
            this.lblSumInt1.Height = 0.1875F;
            this.lblSumInt1.HyperLink = null;
            this.lblSumInt1.Left = 3.8125F;
            this.lblSumInt1.Name = "lblSumInt1";
            this.lblSumInt1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt1.Text = null;
            this.lblSumInt1.Top = 1F;
            this.lblSumInt1.Width = 1F;
            // 
            // lblSumInt2
            // 
            this.lblSumInt2.Height = 0.1875F;
            this.lblSumInt2.HyperLink = null;
            this.lblSumInt2.Left = 3.8125F;
            this.lblSumInt2.Name = "lblSumInt2";
            this.lblSumInt2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt2.Text = null;
            this.lblSumInt2.Top = 1.1875F;
            this.lblSumInt2.Width = 1F;
            // 
            // lblSumInt3
            // 
            this.lblSumInt3.Height = 0.1875F;
            this.lblSumInt3.HyperLink = null;
            this.lblSumInt3.Left = 3.8125F;
            this.lblSumInt3.Name = "lblSumInt3";
            this.lblSumInt3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt3.Text = null;
            this.lblSumInt3.Top = 1.375F;
            this.lblSumInt3.Width = 1F;
            // 
            // lblSumInt4
            // 
            this.lblSumInt4.Height = 0.1875F;
            this.lblSumInt4.HyperLink = null;
            this.lblSumInt4.Left = 3.8125F;
            this.lblSumInt4.Name = "lblSumInt4";
            this.lblSumInt4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt4.Text = null;
            this.lblSumInt4.Top = 1.5625F;
            this.lblSumInt4.Width = 1F;
            // 
            // lblSumInt5
            // 
            this.lblSumInt5.Height = 0.1875F;
            this.lblSumInt5.HyperLink = null;
            this.lblSumInt5.Left = 3.8125F;
            this.lblSumInt5.Name = "lblSumInt5";
            this.lblSumInt5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt5.Text = null;
            this.lblSumInt5.Top = 1.75F;
            this.lblSumInt5.Width = 1F;
            // 
            // lblSumInt6
            // 
            this.lblSumInt6.Height = 0.1875F;
            this.lblSumInt6.HyperLink = null;
            this.lblSumInt6.Left = 3.8125F;
            this.lblSumInt6.Name = "lblSumInt6";
            this.lblSumInt6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt6.Text = null;
            this.lblSumInt6.Top = 1.9375F;
            this.lblSumInt6.Width = 1F;
            // 
            // lblSumInt7
            // 
            this.lblSumInt7.Height = 0.1875F;
            this.lblSumInt7.HyperLink = null;
            this.lblSumInt7.Left = 3.8125F;
            this.lblSumInt7.Name = "lblSumInt7";
            this.lblSumInt7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt7.Text = null;
            this.lblSumInt7.Top = 2.125F;
            this.lblSumInt7.Width = 1F;
            // 
            // lblSumInt8
            // 
            this.lblSumInt8.Height = 0.1875F;
            this.lblSumInt8.HyperLink = null;
            this.lblSumInt8.Left = 3.8125F;
            this.lblSumInt8.Name = "lblSumInt8";
            this.lblSumInt8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt8.Text = null;
            this.lblSumInt8.Top = 2.3125F;
            this.lblSumInt8.Width = 1F;
            // 
            // lblSumInt9
            // 
            this.lblSumInt9.Height = 0.1875F;
            this.lblSumInt9.HyperLink = null;
            this.lblSumInt9.Left = 3.8125F;
            this.lblSumInt9.Name = "lblSumInt9";
            this.lblSumInt9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt9.Text = null;
            this.lblSumInt9.Top = 2.5F;
            this.lblSumInt9.Width = 1F;
            // 
            // lblSumInt10
            // 
            this.lblSumInt10.Height = 0.1875F;
            this.lblSumInt10.HyperLink = null;
            this.lblSumInt10.Left = 3.8125F;
            this.lblSumInt10.Name = "lblSumInt10";
            this.lblSumInt10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt10.Text = null;
            this.lblSumInt10.Top = 2.6875F;
            this.lblSumInt10.Width = 1F;
            // 
            // lblSumInt11
            // 
            this.lblSumInt11.Height = 0.1875F;
            this.lblSumInt11.HyperLink = null;
            this.lblSumInt11.Left = 3.8125F;
            this.lblSumInt11.Name = "lblSumInt11";
            this.lblSumInt11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt11.Text = null;
            this.lblSumInt11.Top = 2.875F;
            this.lblSumInt11.Width = 1F;
            // 
            // lblSumCost1
            // 
            this.lblSumCost1.Height = 0.1875F;
            this.lblSumCost1.HyperLink = null;
            this.lblSumCost1.Left = 4.8125F;
            this.lblSumCost1.Name = "lblSumCost1";
            this.lblSumCost1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost1.Text = null;
            this.lblSumCost1.Top = 1F;
            this.lblSumCost1.Width = 0.75F;
            // 
            // lblSumCost2
            // 
            this.lblSumCost2.Height = 0.1875F;
            this.lblSumCost2.HyperLink = null;
            this.lblSumCost2.Left = 4.8125F;
            this.lblSumCost2.Name = "lblSumCost2";
            this.lblSumCost2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost2.Text = null;
            this.lblSumCost2.Top = 1.1875F;
            this.lblSumCost2.Width = 0.75F;
            // 
            // lblSumCost3
            // 
            this.lblSumCost3.Height = 0.1875F;
            this.lblSumCost3.HyperLink = null;
            this.lblSumCost3.Left = 4.8125F;
            this.lblSumCost3.Name = "lblSumCost3";
            this.lblSumCost3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost3.Text = null;
            this.lblSumCost3.Top = 1.375F;
            this.lblSumCost3.Width = 0.75F;
            // 
            // lblSumCost4
            // 
            this.lblSumCost4.Height = 0.1875F;
            this.lblSumCost4.HyperLink = null;
            this.lblSumCost4.Left = 4.8125F;
            this.lblSumCost4.Name = "lblSumCost4";
            this.lblSumCost4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost4.Text = null;
            this.lblSumCost4.Top = 1.5625F;
            this.lblSumCost4.Width = 0.75F;
            // 
            // lblSumCost5
            // 
            this.lblSumCost5.Height = 0.1875F;
            this.lblSumCost5.HyperLink = null;
            this.lblSumCost5.Left = 4.8125F;
            this.lblSumCost5.Name = "lblSumCost5";
            this.lblSumCost5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost5.Text = null;
            this.lblSumCost5.Top = 1.75F;
            this.lblSumCost5.Width = 0.75F;
            // 
            // lblSumCost6
            // 
            this.lblSumCost6.Height = 0.1875F;
            this.lblSumCost6.HyperLink = null;
            this.lblSumCost6.Left = 4.8125F;
            this.lblSumCost6.Name = "lblSumCost6";
            this.lblSumCost6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost6.Text = null;
            this.lblSumCost6.Top = 1.9375F;
            this.lblSumCost6.Width = 0.75F;
            // 
            // lblSumCost7
            // 
            this.lblSumCost7.Height = 0.1875F;
            this.lblSumCost7.HyperLink = null;
            this.lblSumCost7.Left = 4.8125F;
            this.lblSumCost7.Name = "lblSumCost7";
            this.lblSumCost7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost7.Text = null;
            this.lblSumCost7.Top = 2.125F;
            this.lblSumCost7.Width = 0.75F;
            // 
            // lblSumCost8
            // 
            this.lblSumCost8.Height = 0.1875F;
            this.lblSumCost8.HyperLink = null;
            this.lblSumCost8.Left = 4.8125F;
            this.lblSumCost8.Name = "lblSumCost8";
            this.lblSumCost8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost8.Text = null;
            this.lblSumCost8.Top = 2.3125F;
            this.lblSumCost8.Width = 0.75F;
            // 
            // lblSumCost9
            // 
            this.lblSumCost9.Height = 0.1875F;
            this.lblSumCost9.HyperLink = null;
            this.lblSumCost9.Left = 4.8125F;
            this.lblSumCost9.Name = "lblSumCost9";
            this.lblSumCost9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost9.Text = null;
            this.lblSumCost9.Top = 2.5F;
            this.lblSumCost9.Width = 0.75F;
            // 
            // lblSumCost10
            // 
            this.lblSumCost10.Height = 0.1875F;
            this.lblSumCost10.HyperLink = null;
            this.lblSumCost10.Left = 4.8125F;
            this.lblSumCost10.Name = "lblSumCost10";
            this.lblSumCost10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost10.Text = null;
            this.lblSumCost10.Top = 2.6875F;
            this.lblSumCost10.Width = 0.75F;
            // 
            // lblSumCost11
            // 
            this.lblSumCost11.Height = 0.1875F;
            this.lblSumCost11.HyperLink = null;
            this.lblSumCost11.Left = 4.8125F;
            this.lblSumCost11.Name = "lblSumCost11";
            this.lblSumCost11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost11.Text = null;
            this.lblSumCost11.Top = 2.875F;
            this.lblSumCost11.Width = 0.75F;
            // 
            // lblSumHeaderType
            // 
            this.lblSumHeaderType.Height = 0.1875F;
            this.lblSumHeaderType.HyperLink = null;
            this.lblSumHeaderType.Left = 0.3125F;
            this.lblSumHeaderType.MultiLine = false;
            this.lblSumHeaderType.Name = "lblSumHeaderType";
            this.lblSumHeaderType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSumHeaderType.Text = null;
            this.lblSumHeaderType.Top = 0.8125F;
            this.lblSumHeaderType.Width = 2.5F;
            // 
            // lblSumHeaderTotal
            // 
            this.lblSumHeaderTotal.Height = 0.1875F;
            this.lblSumHeaderTotal.HyperLink = null;
            this.lblSumHeaderTotal.Left = 6.5F;
            this.lblSumHeaderTotal.Name = "lblSumHeaderTotal";
            this.lblSumHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumHeaderTotal.Text = null;
            this.lblSumHeaderTotal.Top = 0.8125F;
            this.lblSumHeaderTotal.Width = 1F;
            // 
            // lblSumHeaderPrin
            // 
            this.lblSumHeaderPrin.Height = 0.1875F;
            this.lblSumHeaderPrin.HyperLink = null;
            this.lblSumHeaderPrin.Left = 2.8125F;
            this.lblSumHeaderPrin.Name = "lblSumHeaderPrin";
            this.lblSumHeaderPrin.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumHeaderPrin.Text = null;
            this.lblSumHeaderPrin.Top = 0.8125F;
            this.lblSumHeaderPrin.Width = 1F;
            // 
            // lblSumHeaderInt
            // 
            this.lblSumHeaderInt.Height = 0.1875F;
            this.lblSumHeaderInt.HyperLink = null;
            this.lblSumHeaderInt.Left = 3.8125F;
            this.lblSumHeaderInt.Name = "lblSumHeaderInt";
            this.lblSumHeaderInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumHeaderInt.Text = null;
            this.lblSumHeaderInt.Top = 0.8125F;
            this.lblSumHeaderInt.Width = 1F;
            // 
            // lblSumHeaderCost
            // 
            this.lblSumHeaderCost.Height = 0.1875F;
            this.lblSumHeaderCost.HyperLink = null;
            this.lblSumHeaderCost.Left = 4.8125F;
            this.lblSumHeaderCost.Name = "lblSumHeaderCost";
            this.lblSumHeaderCost.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumHeaderCost.Text = null;
            this.lblSumHeaderCost.Top = 0.8125F;
            this.lblSumHeaderCost.Width = 0.75F;
            // 
            // lblSummary
            // 
            this.lblSummary.Height = 0.1875F;
            this.lblSummary.HyperLink = null;
            this.lblSummary.Left = 2.75F;
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblSummary.Text = "Balance Due";
            this.lblSummary.Top = 3.5F;
            this.lblSummary.Width = 1F;
            // 
            // lblSummary1
            // 
            this.lblSummary1.Height = 0.1875F;
            this.lblSummary1.HyperLink = null;
            this.lblSummary1.Left = 0.3125F;
            this.lblSummary1.Name = "lblSummary1";
            this.lblSummary1.Style = "font-family: \'Tahoma\'; text-align: left";
            this.lblSummary1.Text = null;
            this.lblSummary1.Top = 3.6875F;
            this.lblSummary1.Width = 0.625F;
            // 
            // fldSummary1
            // 
            this.fldSummary1.Height = 0.1875F;
            this.fldSummary1.Left = 2.75F;
            this.fldSummary1.Name = "fldSummary1";
            this.fldSummary1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSummary1.Text = "0.00";
            this.fldSummary1.Top = 3.6875F;
            this.fldSummary1.Width = 1F;
            // 
            // lblSummaryPaymentType12
            // 
            this.lblSummaryPaymentType12.Height = 0.1875F;
            this.lblSummaryPaymentType12.HyperLink = null;
            this.lblSummaryPaymentType12.Left = 0.3125F;
            this.lblSummaryPaymentType12.MultiLine = false;
            this.lblSummaryPaymentType12.Name = "lblSummaryPaymentType12";
            this.lblSummaryPaymentType12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType12.Text = null;
            this.lblSummaryPaymentType12.Top = 3.0625F;
            this.lblSummaryPaymentType12.Width = 2.5F;
            // 
            // lblSummaryTotal12
            // 
            this.lblSummaryTotal12.Height = 0.1875F;
            this.lblSummaryTotal12.HyperLink = null;
            this.lblSummaryTotal12.Left = 6.5F;
            this.lblSummaryTotal12.Name = "lblSummaryTotal12";
            this.lblSummaryTotal12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal12.Text = null;
            this.lblSummaryTotal12.Top = 3.0625F;
            this.lblSummaryTotal12.Width = 1F;
            // 
            // lblSumPrin12
            // 
            this.lblSumPrin12.Height = 0.1875F;
            this.lblSumPrin12.HyperLink = null;
            this.lblSumPrin12.Left = 2.8125F;
            this.lblSumPrin12.Name = "lblSumPrin12";
            this.lblSumPrin12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin12.Text = null;
            this.lblSumPrin12.Top = 3.0625F;
            this.lblSumPrin12.Width = 1F;
            // 
            // lblSumInt12
            // 
            this.lblSumInt12.Height = 0.1875F;
            this.lblSumInt12.HyperLink = null;
            this.lblSumInt12.Left = 3.8125F;
            this.lblSumInt12.Name = "lblSumInt12";
            this.lblSumInt12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt12.Text = null;
            this.lblSumInt12.Top = 3.0625F;
            this.lblSumInt12.Width = 1F;
            // 
            // lblSumCost12
            // 
            this.lblSumCost12.Height = 0.1875F;
            this.lblSumCost12.HyperLink = null;
            this.lblSumCost12.Left = 4.8125F;
            this.lblSumCost12.Name = "lblSumCost12";
            this.lblSumCost12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost12.Text = null;
            this.lblSumCost12.Top = 3.0625F;
            this.lblSumCost12.Width = 0.75F;
            // 
            // lblSummaryPaymentType13
            // 
            this.lblSummaryPaymentType13.Height = 0.1875F;
            this.lblSummaryPaymentType13.HyperLink = null;
            this.lblSummaryPaymentType13.Left = 0.3125F;
            this.lblSummaryPaymentType13.MultiLine = false;
            this.lblSummaryPaymentType13.Name = "lblSummaryPaymentType13";
            this.lblSummaryPaymentType13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
            this.lblSummaryPaymentType13.Text = null;
            this.lblSummaryPaymentType13.Top = 3.25F;
            this.lblSummaryPaymentType13.Width = 2.5F;
            // 
            // lblSummaryTotal13
            // 
            this.lblSummaryTotal13.Height = 0.1875F;
            this.lblSummaryTotal13.HyperLink = null;
            this.lblSummaryTotal13.Left = 6.5F;
            this.lblSummaryTotal13.Name = "lblSummaryTotal13";
            this.lblSummaryTotal13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSummaryTotal13.Text = null;
            this.lblSummaryTotal13.Top = 3.25F;
            this.lblSummaryTotal13.Width = 1F;
            // 
            // lblSumPrin13
            // 
            this.lblSumPrin13.Height = 0.1875F;
            this.lblSumPrin13.HyperLink = null;
            this.lblSumPrin13.Left = 2.8125F;
            this.lblSumPrin13.Name = "lblSumPrin13";
            this.lblSumPrin13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumPrin13.Text = null;
            this.lblSumPrin13.Top = 3.25F;
            this.lblSumPrin13.Width = 1F;
            // 
            // lblSumInt13
            // 
            this.lblSumInt13.Height = 0.1875F;
            this.lblSumInt13.HyperLink = null;
            this.lblSumInt13.Left = 3.8125F;
            this.lblSumInt13.Name = "lblSumInt13";
            this.lblSumInt13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumInt13.Text = null;
            this.lblSumInt13.Top = 3.25F;
            this.lblSumInt13.Width = 1F;
            // 
            // lblSumCost13
            // 
            this.lblSumCost13.Height = 0.1875F;
            this.lblSumCost13.HyperLink = null;
            this.lblSumCost13.Left = 4.8125F;
            this.lblSumCost13.Name = "lblSumCost13";
            this.lblSumCost13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblSumCost13.Text = null;
            this.lblSumCost13.Top = 3.25F;
            this.lblSumCost13.Width = 0.75F;
            // 
            // lnSubtotal
            // 
            this.lnSubtotal.Height = 0F;
            this.lnSubtotal.Left = 5.5625F;
            this.lnSubtotal.LineWeight = 1F;
            this.lnSubtotal.Name = "lnSubtotal";
            this.lnSubtotal.Top = 3.5625F;
            this.lnSubtotal.Width = 1.9375F;
            this.lnSubtotal.X1 = 5.5625F;
            this.lnSubtotal.X2 = 7.5F;
            this.lnSubtotal.Y1 = 3.5625F;
            this.lnSubtotal.Y2 = 3.5625F;
            // 
            // fldTotalNonInterestDue
            // 
            this.fldTotalNonInterestDue.Height = 0.1875F;
            this.fldTotalNonInterestDue.Left = 4.5F;
            this.fldTotalNonInterestDue.Name = "fldTotalNonInterestDue";
            this.fldTotalNonInterestDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalNonInterestDue.Text = "0.00";
            this.fldTotalNonInterestDue.Top = 0.25F;
            this.fldTotalNonInterestDue.Width = 1.3125F;
            // 
            // lblNonSummary
            // 
            this.lblNonSummary.Height = 0.1875F;
            this.lblNonSummary.HyperLink = null;
            this.lblNonSummary.Left = 1.3125F;
            this.lblNonSummary.Name = "lblNonSummary";
            this.lblNonSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblNonSummary.Text = "Non-Interest Due";
            this.lblNonSummary.Top = 3.5F;
            this.lblNonSummary.Width = 1.375F;
            // 
            // fldnonsummary1
            // 
            this.fldnonsummary1.Height = 0.1875F;
            this.fldnonsummary1.Left = 1.6875F;
            this.fldnonsummary1.Name = "fldnonsummary1";
            this.fldnonsummary1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldnonsummary1.Text = "0.00";
            this.fldnonsummary1.Top = 3.6875F;
            this.fldnonsummary1.Width = 1F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0.3125F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 3.6875F;
            this.Line1.Width = 3.4375F;
            this.Line1.X1 = 0.3125F;
            this.Line1.X2 = 3.75F;
            this.Line1.Y1 = 3.6875F;
            this.Line1.Y2 = 3.6875F;
            // 
            // lblNonSummaryTotal1
            // 
            this.lblNonSummaryTotal1.Height = 0.1875F;
            this.lblNonSummaryTotal1.HyperLink = null;
            this.lblNonSummaryTotal1.Left = 5.5625F;
            this.lblNonSummaryTotal1.Name = "lblNonSummaryTotal1";
            this.lblNonSummaryTotal1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal1.Text = null;
            this.lblNonSummaryTotal1.Top = 1F;
            this.lblNonSummaryTotal1.Width = 1F;
            // 
            // lblNonSummaryTotal2
            // 
            this.lblNonSummaryTotal2.Height = 0.1875F;
            this.lblNonSummaryTotal2.HyperLink = null;
            this.lblNonSummaryTotal2.Left = 5.5625F;
            this.lblNonSummaryTotal2.Name = "lblNonSummaryTotal2";
            this.lblNonSummaryTotal2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal2.Text = null;
            this.lblNonSummaryTotal2.Top = 1.1875F;
            this.lblNonSummaryTotal2.Width = 1F;
            // 
            // lblNonSummaryTotal3
            // 
            this.lblNonSummaryTotal3.Height = 0.1875F;
            this.lblNonSummaryTotal3.HyperLink = null;
            this.lblNonSummaryTotal3.Left = 5.5625F;
            this.lblNonSummaryTotal3.Name = "lblNonSummaryTotal3";
            this.lblNonSummaryTotal3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal3.Text = null;
            this.lblNonSummaryTotal3.Top = 1.375F;
            this.lblNonSummaryTotal3.Width = 1F;
            // 
            // lblNonSummaryTotal4
            // 
            this.lblNonSummaryTotal4.Height = 0.1875F;
            this.lblNonSummaryTotal4.HyperLink = null;
            this.lblNonSummaryTotal4.Left = 5.5625F;
            this.lblNonSummaryTotal4.Name = "lblNonSummaryTotal4";
            this.lblNonSummaryTotal4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal4.Text = null;
            this.lblNonSummaryTotal4.Top = 1.5625F;
            this.lblNonSummaryTotal4.Width = 1F;
            // 
            // lblNonSummaryTotal5
            // 
            this.lblNonSummaryTotal5.Height = 0.1875F;
            this.lblNonSummaryTotal5.HyperLink = null;
            this.lblNonSummaryTotal5.Left = 5.5625F;
            this.lblNonSummaryTotal5.Name = "lblNonSummaryTotal5";
            this.lblNonSummaryTotal5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal5.Text = null;
            this.lblNonSummaryTotal5.Top = 1.75F;
            this.lblNonSummaryTotal5.Width = 1F;
            // 
            // lblNonSummaryTotal6
            // 
            this.lblNonSummaryTotal6.Height = 0.1875F;
            this.lblNonSummaryTotal6.HyperLink = null;
            this.lblNonSummaryTotal6.Left = 5.5625F;
            this.lblNonSummaryTotal6.Name = "lblNonSummaryTotal6";
            this.lblNonSummaryTotal6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal6.Text = null;
            this.lblNonSummaryTotal6.Top = 1.9375F;
            this.lblNonSummaryTotal6.Width = 1F;
            // 
            // lblNonSummaryTotal7
            // 
            this.lblNonSummaryTotal7.Height = 0.1875F;
            this.lblNonSummaryTotal7.HyperLink = null;
            this.lblNonSummaryTotal7.Left = 5.5625F;
            this.lblNonSummaryTotal7.Name = "lblNonSummaryTotal7";
            this.lblNonSummaryTotal7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal7.Text = null;
            this.lblNonSummaryTotal7.Top = 2.125F;
            this.lblNonSummaryTotal7.Width = 1F;
            // 
            // lblNonSummaryTotal8
            // 
            this.lblNonSummaryTotal8.Height = 0.1875F;
            this.lblNonSummaryTotal8.HyperLink = null;
            this.lblNonSummaryTotal8.Left = 5.5625F;
            this.lblNonSummaryTotal8.Name = "lblNonSummaryTotal8";
            this.lblNonSummaryTotal8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal8.Text = null;
            this.lblNonSummaryTotal8.Top = 2.3125F;
            this.lblNonSummaryTotal8.Width = 1F;
            // 
            // lblNonSummaryTotal9
            // 
            this.lblNonSummaryTotal9.Height = 0.1875F;
            this.lblNonSummaryTotal9.HyperLink = null;
            this.lblNonSummaryTotal9.Left = 5.5625F;
            this.lblNonSummaryTotal9.Name = "lblNonSummaryTotal9";
            this.lblNonSummaryTotal9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal9.Text = null;
            this.lblNonSummaryTotal9.Top = 2.5F;
            this.lblNonSummaryTotal9.Width = 1F;
            // 
            // lblNonSummaryTotal10
            // 
            this.lblNonSummaryTotal10.Height = 0.1875F;
            this.lblNonSummaryTotal10.HyperLink = null;
            this.lblNonSummaryTotal10.Left = 5.5625F;
            this.lblNonSummaryTotal10.Name = "lblNonSummaryTotal10";
            this.lblNonSummaryTotal10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal10.Text = null;
            this.lblNonSummaryTotal10.Top = 2.6875F;
            this.lblNonSummaryTotal10.Width = 1F;
            // 
            // lblNonSummaryTotal11
            // 
            this.lblNonSummaryTotal11.Height = 0.1875F;
            this.lblNonSummaryTotal11.HyperLink = null;
            this.lblNonSummaryTotal11.Left = 5.5625F;
            this.lblNonSummaryTotal11.Name = "lblNonSummaryTotal11";
            this.lblNonSummaryTotal11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal11.Text = null;
            this.lblNonSummaryTotal11.Top = 2.875F;
            this.lblNonSummaryTotal11.Width = 1F;
            // 
            // lblNonSumHeaderTotal
            // 
            this.lblNonSumHeaderTotal.Height = 0.1875F;
            this.lblNonSumHeaderTotal.HyperLink = null;
            this.lblNonSumHeaderTotal.Left = 5.5625F;
            this.lblNonSumHeaderTotal.Name = "lblNonSumHeaderTotal";
            this.lblNonSumHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSumHeaderTotal.Text = null;
            this.lblNonSumHeaderTotal.Top = 0.8125F;
            this.lblNonSumHeaderTotal.Width = 1F;
            // 
            // lblNonSummaryTotal12
            // 
            this.lblNonSummaryTotal12.Height = 0.1875F;
            this.lblNonSummaryTotal12.HyperLink = null;
            this.lblNonSummaryTotal12.Left = 5.5625F;
            this.lblNonSummaryTotal12.Name = "lblNonSummaryTotal12";
            this.lblNonSummaryTotal12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal12.Text = null;
            this.lblNonSummaryTotal12.Top = 3.0625F;
            this.lblNonSummaryTotal12.Width = 1F;
            // 
            // lblNonSummaryTotal13
            // 
            this.lblNonSummaryTotal13.Height = 0.1875F;
            this.lblNonSummaryTotal13.HyperLink = null;
            this.lblNonSummaryTotal13.Left = 5.5625F;
            this.lblNonSummaryTotal13.Name = "lblNonSummaryTotal13";
            this.lblNonSummaryTotal13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblNonSummaryTotal13.Text = null;
            this.lblNonSummaryTotal13.Top = 3.25F;
            this.lblNonSummaryTotal13.Width = 1F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 2.8125F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 3.25F;
            this.Line3.Visible = false;
            this.Line3.Width = 4.6875F;
            this.Line3.X1 = 2.8125F;
            this.Line3.X2 = 7.5F;
            this.Line3.Y1 = 3.25F;
            this.Line3.Y2 = 3.25F;
            // 
            // fldSumCount1
            // 
            this.fldSumCount1.Height = 0.1875F;
            this.fldSumCount1.Left = 1.0625F;
            this.fldSumCount1.Name = "fldSumCount1";
            this.fldSumCount1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSumCount1.Text = null;
            this.fldSumCount1.Top = 3.6875F;
            this.fldSumCount1.Width = 0.375F;
            // 
            // fldAcctCount
            // 
            this.fldAcctCount.Height = 0.1875F;
            this.fldAcctCount.Left = 0.75F;
            this.fldAcctCount.Name = "fldAcctCount";
            this.fldAcctCount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAcctCount.Text = "Accounts";
            this.fldAcctCount.Top = 0.25F;
            this.fldAcctCount.Width = 1.25F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAbatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAbatementPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAbatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAbatementPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBuilding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldExempt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTADate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpecialTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSpecialTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNonInterestDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAbatement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalAbatementPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRTError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpecialTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSpecialText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumPrin13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumInt13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSumCost13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalNonInterestDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldnonsummary1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSumHeaderTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNonSummaryTotal13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSumCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcctCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentReceived;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAbatement;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAbatementPayments;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBuilding;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBuilding;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblExempt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldExempt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLand;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLand;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReference2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReference;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef1;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSLAllActivityDetailOB;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTADate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSpecialTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSpecialTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNonInterestDue;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentReceived;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAbatement;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAbatementPayments;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblRTError;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSpecialTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSpecialText;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Line line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType9;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType11;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal9;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal11;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSummaryTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin9;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin11;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt9;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt11;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost9;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost11;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderType;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderPrin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderInt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderCost;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummary1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType12;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal12;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin12;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt12;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost12;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType13;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal13;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin13;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt13;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost13;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnSubtotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalNonInterestDue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummary;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldnonsummary1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal4;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal5;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal6;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal7;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal8;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal9;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal10;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal11;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSumHeaderTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal12;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNonSummaryTotal13;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSumCount1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctCount;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxDue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentReceived;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAbatement;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAbatementPayments;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
