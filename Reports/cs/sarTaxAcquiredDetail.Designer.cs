namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class sarTaxAcquiredDetail
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarTaxAcquiredDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblPreviousOwners = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblOutStandingTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnOutStandingTaxes = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblLastBillingAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLastBillingAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTaxes = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldPrevOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldTotalTaxes = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblPreviousOwners)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOutStandingTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastBillingAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastBillingAmountTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrevOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTaxes,
            this.lblTaxes,
            this.fldPrevOwner});
            this.Detail.Height = 0.1979167F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.CanGrow = false;
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblPreviousOwners,
            this.lblOutStandingTax,
            this.lnOutStandingTaxes,
            this.lblLastBillingAmount,
            this.lblLastBillingAmountTotal});
            this.GroupHeader1.Height = 0.4583333F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line1,
            this.fldTotalTaxes});
            this.GroupFooter1.Height = 0.3125F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lblPreviousOwners
            // 
            this.lblPreviousOwners.Height = 0.1875F;
            this.lblPreviousOwners.HyperLink = null;
            this.lblPreviousOwners.Left = 0.75F;
            this.lblPreviousOwners.Name = "lblPreviousOwners";
            this.lblPreviousOwners.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.lblPreviousOwners.Text = "Previous Owners";
            this.lblPreviousOwners.Top = 0.25F;
            this.lblPreviousOwners.Width = 1.4375F;
            // 
            // lblOutStandingTax
            // 
            this.lblOutStandingTax.Height = 0.1875F;
            this.lblOutStandingTax.HyperLink = null;
            this.lblOutStandingTax.Left = 4.8125F;
            this.lblOutStandingTax.Name = "lblOutStandingTax";
            this.lblOutStandingTax.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
            this.lblOutStandingTax.Text = "Outstanding Taxes";
            this.lblOutStandingTax.Top = 0.25F;
            this.lblOutStandingTax.Width = 1.875F;
            // 
            // lnOutStandingTaxes
            // 
            this.lnOutStandingTaxes.Height = 0F;
            this.lnOutStandingTaxes.Left = 4.8125F;
            this.lnOutStandingTaxes.LineWeight = 1F;
            this.lnOutStandingTaxes.Name = "lnOutStandingTaxes";
            this.lnOutStandingTaxes.Top = 0.4375F;
            this.lnOutStandingTaxes.Width = 1.875F;
            this.lnOutStandingTaxes.X1 = 6.6875F;
            this.lnOutStandingTaxes.X2 = 4.8125F;
            this.lnOutStandingTaxes.Y1 = 0.4375F;
            this.lnOutStandingTaxes.Y2 = 0.4375F;
            // 
            // lblLastBillingAmount
            // 
            this.lblLastBillingAmount.Height = 0.1875F;
            this.lblLastBillingAmount.HyperLink = null;
            this.lblLastBillingAmount.Left = 3.6875F;
            this.lblLastBillingAmount.Name = "lblLastBillingAmount";
            this.lblLastBillingAmount.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
            this.lblLastBillingAmount.Text = "Last Billing Amount:";
            this.lblLastBillingAmount.Top = 0F;
            this.lblLastBillingAmount.Width = 1.6875F;
            // 
            // lblLastBillingAmountTotal
            // 
            this.lblLastBillingAmountTotal.Height = 0.1875F;
            this.lblLastBillingAmountTotal.HyperLink = null;
            this.lblLastBillingAmountTotal.Left = 5.375F;
            this.lblLastBillingAmountTotal.Name = "lblLastBillingAmountTotal";
            this.lblLastBillingAmountTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblLastBillingAmountTotal.Text = null;
            this.lblLastBillingAmountTotal.Top = 0F;
            this.lblLastBillingAmountTotal.Width = 1.3125F;
            // 
            // fldTaxes
            // 
            this.fldTaxes.Height = 0.1875F;
            this.fldTaxes.Left = 5.5625F;
            this.fldTaxes.MultiLine = false;
            this.fldTaxes.Name = "fldTaxes";
            this.fldTaxes.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxes.Text = null;
            this.fldTaxes.Top = 0F;
            this.fldTaxes.Width = 1.125F;
            // 
            // lblTaxes
            // 
            this.lblTaxes.Height = 0.1875F;
            this.lblTaxes.HyperLink = null;
            this.lblTaxes.Left = 4.8125F;
            this.lblTaxes.MultiLine = false;
            this.lblTaxes.Name = "lblTaxes";
            this.lblTaxes.Style = "font-family: \'Tahoma\'; text-align: left; white-space: nowrap; ddo-char-set: 0";
            this.lblTaxes.Text = null;
            this.lblTaxes.Top = 0F;
            this.lblTaxes.Width = 0.75F;
            // 
            // fldPrevOwner
            // 
            this.fldPrevOwner.Height = 0.1875F;
            this.fldPrevOwner.Left = 0.75F;
            this.fldPrevOwner.MultiLine = false;
            this.fldPrevOwner.Name = "fldPrevOwner";
            this.fldPrevOwner.Style = "font-family: \'Tahoma\'";
            this.fldPrevOwner.Text = null;
            this.fldPrevOwner.Top = 0F;
            this.fldPrevOwner.Width = 3.8125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 5.5F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 1.25F;
            this.Line1.X1 = 5.5F;
            this.Line1.X2 = 6.75F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0F;
            // 
            // fldTotalTaxes
            // 
            this.fldTotalTaxes.Height = 0.1875F;
            this.fldTotalTaxes.Left = 5.4375F;
            this.fldTotalTaxes.MultiLine = false;
            this.fldTotalTaxes.Name = "fldTotalTaxes";
            this.fldTotalTaxes.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTaxes.Text = null;
            this.fldTotalTaxes.Top = 0F;
            this.fldTotalTaxes.Width = 1.25F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblPreviousOwners)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOutStandingTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastBillingAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLastBillingAmountTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrevOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxes;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxes;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrevOwner;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPreviousOwners;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOutStandingTax;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnOutStandingTaxes;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLastBillingAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLastBillingAmountTotal;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxes;
    }
}
