namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptLienRemovalPayments
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLienRemovalPayments));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDateHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPer = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDate,
            this.fldPer,
            this.fldCode,
            this.fldRef,
            this.fldPrincipal,
            this.fldTotal,
            this.fldInterest,
            this.fldCosts,
            this.lnTotals});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalPrin,
            this.fldTotalTotal,
            this.fldTotalInt,
            this.fldTotalCost,
            this.Line2});
            this.ReportFooter.Height = 0.3958333F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblDateHeader,
            this.lblRef,
            this.lblPer,
            this.lblCode,
            this.lblPrincipal,
            this.lblInterest,
            this.lblCosts,
            this.lblTotal,
            this.Line1,
            this.lblName,
            this.lblLocation,
            this.lblAccount});
            this.PageHeader.Height = 1.21875F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.375F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.lblHeader.Text = "Removed From Lien";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.0625F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.75F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 5.8125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.25F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 5.8125F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.25F;
            // 
            // lblDateHeader
            // 
            this.lblDateHeader.Height = 0.1875F;
            this.lblDateHeader.HyperLink = null;
            this.lblDateHeader.Left = 0.1875F;
            this.lblDateHeader.Name = "lblDateHeader";
            this.lblDateHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblDateHeader.Text = "Date";
            this.lblDateHeader.Top = 1F;
            this.lblDateHeader.Width = 0.8125F;
            // 
            // lblRef
            // 
            this.lblRef.Height = 0.1875F;
            this.lblRef.HyperLink = null;
            this.lblRef.Left = 1.1875F;
            this.lblRef.Name = "lblRef";
            this.lblRef.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblRef.Text = "Reference";
            this.lblRef.Top = 1F;
            this.lblRef.Width = 1.0625F;
            // 
            // lblPer
            // 
            this.lblPer.Height = 0.1875F;
            this.lblPer.HyperLink = null;
            this.lblPer.Left = 2.4375F;
            this.lblPer.Name = "lblPer";
            this.lblPer.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblPer.Text = "P";
            this.lblPer.Top = 1F;
            this.lblPer.Width = 0.1875F;
            // 
            // lblCode
            // 
            this.lblCode.Height = 0.1875F;
            this.lblCode.HyperLink = null;
            this.lblCode.Left = 2.625F;
            this.lblCode.Name = "lblCode";
            this.lblCode.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
            this.lblCode.Text = "C";
            this.lblCode.Top = 1F;
            this.lblCode.Width = 0.1875F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 3F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 1F;
            this.lblPrincipal.Width = 1F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1875F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 4F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 1F;
            this.lblInterest.Width = 1F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 5F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 1F;
            this.lblCosts.Width = 1F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 6F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 1F;
            this.lblTotal.Width = 1.0625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.1875F;
            this.Line1.Width = 7F;
            this.Line1.X1 = 7F;
            this.Line1.X2 = 0F;
            this.Line1.Y1 = 1.1875F;
            this.Line1.Y2 = 1.1875F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.3125F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.4375F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblName.Text = null;
            this.lblName.Top = 0.625F;
            this.lblName.Width = 5.75F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 1.5625F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblLocation.Text = null;
            this.lblLocation.Top = 0.4375F;
            this.lblLocation.Width = 4.625F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0.4375F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.lblAccount.Text = null;
            this.lblAccount.Top = 0.4375F;
            this.lblAccount.Width = 1.125F;
            // 
            // fldDate
            // 
            this.fldDate.Height = 0.1875F;
            this.fldDate.Left = 0.1875F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldDate.Text = null;
            this.fldDate.Top = 0F;
            this.fldDate.Width = 0.8125F;
            // 
            // fldPer
            // 
            this.fldPer.Height = 0.1875F;
            this.fldPer.Left = 2.4375F;
            this.fldPer.Name = "fldPer";
            this.fldPer.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldPer.Text = null;
            this.fldPer.Top = 0F;
            this.fldPer.Width = 0.1875F;
            // 
            // fldCode
            // 
            this.fldCode.Height = 0.1875F;
            this.fldCode.Left = 2.625F;
            this.fldCode.Name = "fldCode";
            this.fldCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldCode.Text = null;
            this.fldCode.Top = 0F;
            this.fldCode.Width = 0.1875F;
            // 
            // fldRef
            // 
            this.fldRef.Height = 0.1875F;
            this.fldRef.Left = 1.1875F;
            this.fldRef.Name = "fldRef";
            this.fldRef.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
            this.fldRef.Text = null;
            this.fldRef.Top = 0F;
            this.fldRef.Width = 1.0625F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 3F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 1F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.0625F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 4F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 1F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.1875F;
            this.fldCosts.Left = 5F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldCosts.Text = null;
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 1F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 3F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Visible = false;
            this.lnTotals.Width = 4F;
            this.lnTotals.X1 = 3F;
            this.lnTotals.X2 = 7F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // fldTotalPrin
            // 
            this.fldTotalPrin.Height = 0.1875F;
            this.fldTotalPrin.Left = 3F;
            this.fldTotalPrin.Name = "fldTotalPrin";
            this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalPrin.Text = null;
            this.fldTotalPrin.Top = 0.125F;
            this.fldTotalPrin.Width = 1F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 6F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalTotal.Text = null;
            this.fldTotalTotal.Top = 0.125F;
            this.fldTotalTotal.Width = 1.0625F;
            // 
            // fldTotalInt
            // 
            this.fldTotalInt.Height = 0.1875F;
            this.fldTotalInt.Left = 4F;
            this.fldTotalInt.Name = "fldTotalInt";
            this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalInt.Text = null;
            this.fldTotalInt.Top = 0.125F;
            this.fldTotalInt.Width = 1F;
            // 
            // fldTotalCost
            // 
            this.fldTotalCost.Height = 0.1875F;
            this.fldTotalCost.Left = 5F;
            this.fldTotalCost.Name = "fldTotalCost";
            this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
            this.fldTotalCost.Text = null;
            this.fldTotalCost.Top = 0.125F;
            this.fldTotalCost.Width = 1F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 3F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.0625F;
            this.Line2.Width = 4.0625F;
            this.Line2.X1 = 3F;
            this.Line2.X2 = 7.0625F;
            this.Line2.Y1 = 0.0625F;
            this.Line2.Y2 = 0.0625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.0625F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPer;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDateHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblRef;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPer;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
