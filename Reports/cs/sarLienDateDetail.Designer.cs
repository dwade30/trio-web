namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class sarLienDateDetail
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarLienDateDetail));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblYearHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldMonth1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ln1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ln3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ln2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.ln4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldYear1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblYearHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMonth1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDate1,
            this.lblDate2,
            this.lblDate3,
            this.lblDate4,
            this.fldMonth1,
            this.ln1,
            this.ln3,
            this.ln2,
            this.ln4,
            this.fldYear1,
            this.fldYear2,
            this.fldYear3,
            this.fldYear4});
            this.Detail.Height = 1.84375F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblYearHeader,
            this.lblDescription});
            this.ReportHeader.Height = 0.34375F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Height = 0F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lblYearHeader
            // 
            this.lblYearHeader.Height = 0.1875F;
            this.lblYearHeader.HyperLink = null;
            this.lblYearHeader.Left = 0.0625F;
            this.lblYearHeader.Name = "lblYearHeader";
            this.lblYearHeader.Style = "font-family: \'Tahoma\'";
            this.lblYearHeader.Text = null;
            this.lblYearHeader.Top = 0.0625F;
            this.lblYearHeader.Width = 0.9375F;
            // 
            // lblDescription
            // 
            this.lblDescription.Height = 0.1875F;
            this.lblDescription.HyperLink = null;
            this.lblDescription.Left = 1.25F;
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Style = "font-family: \'Tahoma\'";
            this.lblDescription.Text = null;
            this.lblDescription.Top = 0.0625F;
            this.lblDescription.Width = 4.375F;
            // 
            // lblDate1
            // 
            this.lblDate1.Height = 0.375F;
            this.lblDate1.HyperLink = null;
            this.lblDate1.Left = 0.5F;
            this.lblDate1.Name = "lblDate1";
            this.lblDate1.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblDate1.Text = null;
            this.lblDate1.Top = 0F;
            this.lblDate1.Width = 1.5F;
            // 
            // lblDate2
            // 
            this.lblDate2.Height = 0.375F;
            this.lblDate2.HyperLink = null;
            this.lblDate2.Left = 2.25F;
            this.lblDate2.Name = "lblDate2";
            this.lblDate2.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblDate2.Text = null;
            this.lblDate2.Top = 0.625F;
            this.lblDate2.Width = 1.5F;
            // 
            // lblDate3
            // 
            this.lblDate3.Height = 0.375F;
            this.lblDate3.HyperLink = null;
            this.lblDate3.Left = 4F;
            this.lblDate3.Name = "lblDate3";
            this.lblDate3.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblDate3.Text = null;
            this.lblDate3.Top = 0.3125F;
            this.lblDate3.Width = 1.5F;
            // 
            // lblDate4
            // 
            this.lblDate4.Height = 0.375F;
            this.lblDate4.HyperLink = null;
            this.lblDate4.Left = 5.5625F;
            this.lblDate4.Name = "lblDate4";
            this.lblDate4.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblDate4.Text = null;
            this.lblDate4.Top = 0.625F;
            this.lblDate4.Width = 1.5F;
            // 
            // fldMonth1
            // 
            this.fldMonth1.Border.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldMonth1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldMonth1.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldMonth1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldMonth1.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldMonth1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldMonth1.Border.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldMonth1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldMonth1.Height = 0.1875F;
            this.fldMonth1.Left = 0.3472222F;
            this.fldMonth1.Name = "fldMonth1";
            this.fldMonth1.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldMonth1.Text = "J";
            this.fldMonth1.Top = 1.1875F;
            this.fldMonth1.Width = 0.1875F;
            // 
            // ln1
            // 
            this.ln1.Height = 0.8125F;
            this.ln1.Left = 0.5F;
            this.ln1.LineWeight = 1F;
            this.ln1.Name = "ln1";
            this.ln1.Top = 0.375F;
            this.ln1.Width = 0F;
            this.ln1.X1 = 0.5F;
            this.ln1.X2 = 0.5F;
            this.ln1.Y1 = 0.375F;
            this.ln1.Y2 = 1.1875F;
            // 
            // ln3
            // 
            this.ln3.Height = 0.5F;
            this.ln3.Left = 4F;
            this.ln3.LineWeight = 1F;
            this.ln3.Name = "ln3";
            this.ln3.Top = 0.6875F;
            this.ln3.Width = 0F;
            this.ln3.X1 = 4F;
            this.ln3.X2 = 4F;
            this.ln3.Y1 = 0.6875F;
            this.ln3.Y2 = 1.1875F;
            // 
            // ln2
            // 
            this.ln2.Height = 0.1875F;
            this.ln2.Left = 2.25F;
            this.ln2.LineWeight = 1F;
            this.ln2.Name = "ln2";
            this.ln2.Top = 1F;
            this.ln2.Width = 0F;
            this.ln2.X1 = 2.25F;
            this.ln2.X2 = 2.25F;
            this.ln2.Y1 = 1F;
            this.ln2.Y2 = 1.1875F;
            // 
            // ln4
            // 
            this.ln4.Height = 0.1875F;
            this.ln4.Left = 5.5625F;
            this.ln4.LineWeight = 1F;
            this.ln4.Name = "ln4";
            this.ln4.Top = 1F;
            this.ln4.Width = 0F;
            this.ln4.X1 = 5.5625F;
            this.ln4.X2 = 5.5625F;
            this.ln4.Y1 = 1F;
            this.ln4.Y2 = 1.1875F;
            // 
            // fldYear1
            // 
            this.fldYear1.Border.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear1.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear1.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear1.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear1.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear1.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear1.Border.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear1.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear1.Height = 0.1875F;
            this.fldYear1.Left = 0.3472222F;
            this.fldYear1.Name = "fldYear1";
            this.fldYear1.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldYear1.Text = null;
            this.fldYear1.Top = 1.4375F;
            this.fldYear1.Width = 0.4375F;
            // 
            // fldYear2
            // 
            this.fldYear2.Border.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear2.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear2.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear2.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear2.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear2.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear2.Border.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear2.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear2.Height = 0.1875F;
            this.fldYear2.Left = 2.25F;
            this.fldYear2.Name = "fldYear2";
            this.fldYear2.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldYear2.Text = null;
            this.fldYear2.Top = 1.4375F;
            this.fldYear2.Width = 0.4375F;
            // 
            // fldYear3
            // 
            this.fldYear3.Border.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear3.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear3.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear3.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear3.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear3.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear3.Border.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear3.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear3.Height = 0.1875F;
            this.fldYear3.Left = 4.75F;
            this.fldYear3.Name = "fldYear3";
            this.fldYear3.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldYear3.Text = null;
            this.fldYear3.Top = 1.4375F;
            this.fldYear3.Width = 0.4375F;
            // 
            // fldYear4
            // 
            this.fldYear4.Border.BottomColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear4.Border.BottomStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear4.Border.LeftColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear4.Border.LeftStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear4.Border.RightColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear4.Border.RightStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear4.Border.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldYear4.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldYear4.Height = 0.1875F;
            this.fldYear4.Left = 7F;
            this.fldYear4.Name = "fldYear4";
            this.fldYear4.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldYear4.Text = null;
            this.fldYear4.Top = 1.4375F;
            this.fldYear4.Width = 0.4375F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.9375F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblYearHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMonth1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMonth1;
        private GrapeCity.ActiveReports.SectionReportModel.Line ln1;
        private GrapeCity.ActiveReports.SectionReportModel.Line ln3;
        private GrapeCity.ActiveReports.SectionReportModel.Line ln2;
        private GrapeCity.ActiveReports.SectionReportModel.Line ln4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear4;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYearHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
    }
}
