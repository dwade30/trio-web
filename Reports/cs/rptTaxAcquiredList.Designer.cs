namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptTaxAcquiredList
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxAcquiredList));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblNumberOfAccounts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLAmount1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLAmount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAcct,
            this.fldName,
            this.fldTotal});
            this.Detail.Height = 0.1979167F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTotalTotal,
            this.Line1,
            this.lblNumberOfAccounts,
            this.lblYear,
            this.lblAmount,
            this.Line2,
            this.lblYear1,
            this.lblAmount1,
            this.lblLAmount,
            this.lblLAmount1,
            this.Label1});
            this.ReportFooter.Height = 1.875F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblAcct,
            this.lblName,
            this.lblHeaderTotal});
            this.PageHeader.Height = 0.6979167F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.3125F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Tax Acquired List";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.375F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.375F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.9375F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 0.7F;
            this.lnHeader.Width = 6.95F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 6.95F;
            this.lnHeader.Y1 = 0.7F;
            this.lnHeader.Y2 = 0.7F;
            // 
            // lblAcct
            // 
            this.lblAcct.Height = 0.1875F;
            this.lblAcct.HyperLink = null;
            this.lblAcct.Left = 0F;
            this.lblAcct.Name = "lblAcct";
            this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAcct.Text = "Acct";
            this.lblAcct.Top = 0.5F;
            this.lblAcct.Width = 0.5625F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.625F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.5F;
            this.lblName.Width = 1.375F;
            // 
            // lblHeaderTotal
            // 
            this.lblHeaderTotal.Height = 0.1875F;
            this.lblHeaderTotal.HyperLink = null;
            this.lblHeaderTotal.Left = 6.0625F;
            this.lblHeaderTotal.Name = "lblHeaderTotal";
            this.lblHeaderTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblHeaderTotal.Text = "Total";
            this.lblHeaderTotal.Top = 0.5F;
            this.lblHeaderTotal.Width = 1.125F;
            // 
            // fldAcct
            // 
            this.fldAcct.Height = 0.1875F;
            this.fldAcct.Left = 0F;
            this.fldAcct.Name = "fldAcct";
            this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAcct.Text = null;
            this.fldAcct.Top = 0F;
            this.fldAcct.Width = 0.5625F;
            // 
            // fldName
            // 
            this.fldName.CanGrow = false;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.5625F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 5.5625F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6.0625F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1.125F;
            // 
            // lblTotalTotal
            // 
            this.lblTotalTotal.Height = 0.1875F;
            this.lblTotalTotal.HyperLink = null;
            this.lblTotalTotal.Left = 6.0625F;
            this.lblTotalTotal.Name = "lblTotalTotal";
            this.lblTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblTotalTotal.Text = null;
            this.lblTotalTotal.Top = 0F;
            this.lblTotalTotal.Width = 1.125F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 6.125F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0F;
            this.Line1.Width = 1.375F;
            this.Line1.X1 = 6.125F;
            this.Line1.X2 = 7.5F;
            this.Line1.Y1 = 0F;
            this.Line1.Y2 = 0F;
            // 
            // lblNumberOfAccounts
            // 
            this.lblNumberOfAccounts.Height = 0.1875F;
            this.lblNumberOfAccounts.HyperLink = null;
            this.lblNumberOfAccounts.Left = 1.375F;
            this.lblNumberOfAccounts.Name = "lblNumberOfAccounts";
            this.lblNumberOfAccounts.Style = "font-family: \'Tahoma\'";
            this.lblNumberOfAccounts.Text = null;
            this.lblNumberOfAccounts.Top = 0F;
            this.lblNumberOfAccounts.Width = 4.75F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 2F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblYear.Text = "Year";
            this.lblYear.Top = 0.5625F;
            this.lblYear.Width = 1F;
            // 
            // lblAmount
            // 
            this.lblAmount.Height = 0.1875F;
            this.lblAmount.HyperLink = null;
            this.lblAmount.Left = 3F;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAmount.Text = "Regular";
            this.lblAmount.Top = 0.5625F;
            this.lblAmount.Width = 1.3125F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 2F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.75F;
            this.Line2.Width = 3.4375F;
            this.Line2.X1 = 2F;
            this.Line2.X2 = 5.4375F;
            this.Line2.Y1 = 0.75F;
            this.Line2.Y2 = 0.75F;
            // 
            // lblYear1
            // 
            this.lblYear1.Height = 0.1875F;
            this.lblYear1.HyperLink = null;
            this.lblYear1.Left = 2F;
            this.lblYear1.Name = "lblYear1";
            this.lblYear1.Style = "font-family: \'Tahoma\'";
            this.lblYear1.Text = null;
            this.lblYear1.Top = 0.75F;
            this.lblYear1.Width = 1F;
            // 
            // lblAmount1
            // 
            this.lblAmount1.Height = 0.1875F;
            this.lblAmount1.HyperLink = null;
            this.lblAmount1.Left = 3F;
            this.lblAmount1.Name = "lblAmount1";
            this.lblAmount1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblAmount1.Text = null;
            this.lblAmount1.Top = 0.75F;
            this.lblAmount1.Width = 1.3125F;
            // 
            // lblLAmount
            // 
            this.lblLAmount.Height = 0.1875F;
            this.lblLAmount.HyperLink = null;
            this.lblLAmount.Left = 4.3125F;
            this.lblLAmount.Name = "lblLAmount";
            this.lblLAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblLAmount.Text = "Lien";
            this.lblLAmount.Top = 0.5625F;
            this.lblLAmount.Width = 1.125F;
            // 
            // lblLAmount1
            // 
            this.lblLAmount1.Height = 0.1875F;
            this.lblLAmount1.HyperLink = null;
            this.lblLAmount1.Left = 4.3125F;
            this.lblLAmount1.Name = "lblLAmount1";
            this.lblLAmount1.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblLAmount1.Text = null;
            this.lblLAmount1.Top = 0.75F;
            this.lblLAmount1.Width = 1.125F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.1875F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label1.Text = "Year Summary";
            this.Label1.Top = 0.375F;
            this.Label1.Width = 3.4375F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblNumberOfAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLAmount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblNumberOfAccounts;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLAmount1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTotal;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
