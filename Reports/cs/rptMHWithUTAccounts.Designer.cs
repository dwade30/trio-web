namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptMHWithUTAccounts
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMHWithUTAccounts));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldReportName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMortgageName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnAccountHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblBookPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldReportName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMortgageName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtaccount,
            this.txtName,
            this.txtMapLot,
            this.txtLocation,
            this.fldBookPage});
            this.Detail.Height = 0.2291667F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtMuniname,
            this.txtDate,
            this.txtTitle,
            this.lblPage,
            this.txtTime,
            this.fldReportName});
            this.PageHeader.Height = 0.5208333F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.CanShrink = true;
            this.GroupHeader1.ColumnLayout = false;
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblAccount,
            this.lblName,
            this.lblLocation,
            this.lblMapLot,
            this.txtNumber,
            this.txtMortgageName,
            this.txtAddress1,
            this.txtAddress2,
            this.txtAddress3,
            this.txtAddress4,
            this.lnAccountHeader,
            this.lblBookPage});
            this.GroupHeader1.DataField = "TheBinder";
            this.GroupHeader1.Height = 1.25F;
            this.GroupHeader1.KeepTogether = true;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line2});
            this.GroupFooter1.Height = 0.08333334F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // txtMuniname
            // 
            this.txtMuniname.Height = 0.1875F;
            this.txtMuniname.Left = 0F;
            this.txtMuniname.MultiLine = false;
            this.txtMuniname.Name = "txtMuniname";
            this.txtMuniname.Style = "font-family: \'Tahoma\'";
            this.txtMuniname.Text = null;
            this.txtMuniname.Top = 0F;
            this.txtMuniname.Width = 2.430556F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 6.4375F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.0625F;
            // 
            // txtTitle
            // 
            this.txtTitle.Height = 0.25F;
            this.txtTitle.Left = 0F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTitle.Text = "Real Estate List by Mortgage Holder";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 7.5F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.4375F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; text-align: left";
            this.lblPage.Text = "Page";
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.0625F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0F;
            this.txtTime.MultiLine = false;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.1875F;
            this.txtTime.Width = 1.5F;
            // 
            // fldReportName
            // 
            this.fldReportName.Height = 0.1875F;
            this.fldReportName.Left = 0F;
            this.fldReportName.Name = "fldReportName";
            this.fldReportName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.fldReportName.Text = null;
            this.fldReportName.Top = 0.25F;
            this.fldReportName.Width = 7.5F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.MultiLine = false;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAccount.Text = "Acct";
            this.lblAccount.Top = 0.9791667F;
            this.lblAccount.Width = 0.5F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.625F;
            this.lblName.MultiLine = false;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.9791667F;
            this.lblName.Width = 1.1875F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 4.25F;
            this.lblLocation.MultiLine = false;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblLocation.Text = "Location";
            this.lblLocation.Top = 0.9791667F;
            this.lblLocation.Width = 1.5F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 3F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblMapLot.Text = "MapLot";
            this.lblMapLot.Top = 0.9791667F;
            this.lblMapLot.Width = 1F;
            // 
            // txtNumber
            // 
            this.txtNumber.Height = 0.1666667F;
            this.txtNumber.Left = 0F;
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.txtNumber.Text = null;
            this.txtNumber.Top = 0.04166667F;
            this.txtNumber.Width = 0.5625F;
            // 
            // txtMortgageName
            // 
            this.txtMortgageName.Height = 0.1666667F;
            this.txtMortgageName.Left = 0.875F;
            this.txtMortgageName.Name = "txtMortgageName";
            this.txtMortgageName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold";
            this.txtMortgageName.Text = null;
            this.txtMortgageName.Top = 0.04166667F;
            this.txtMortgageName.Width = 4.125F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Height = 0.1666667F;
            this.txtAddress1.Left = 0.875F;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Style = "font-family: \'Tahoma\'";
            this.txtAddress1.Text = null;
            this.txtAddress1.Top = 0.2083333F;
            this.txtAddress1.Width = 4.4375F;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Height = 0.1666667F;
            this.txtAddress2.Left = 0.875F;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Style = "font-family: \'Tahoma\'";
            this.txtAddress2.Text = null;
            this.txtAddress2.Top = 0.375F;
            this.txtAddress2.Width = 4.4375F;
            // 
            // txtAddress3
            // 
            this.txtAddress3.Height = 0.1666667F;
            this.txtAddress3.Left = 0.875F;
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Style = "font-family: \'Tahoma\'";
            this.txtAddress3.Text = null;
            this.txtAddress3.Top = 0.5416667F;
            this.txtAddress3.Width = 4.4375F;
            // 
            // txtAddress4
            // 
            this.txtAddress4.Height = 0.1666667F;
            this.txtAddress4.Left = 0.875F;
            this.txtAddress4.Name = "txtAddress4";
            this.txtAddress4.Style = "font-family: \'Tahoma\'";
            this.txtAddress4.Text = null;
            this.txtAddress4.Top = 0.7083333F;
            this.txtAddress4.Width = 4.4375F;
            // 
            // lnAccountHeader
            // 
            this.lnAccountHeader.Height = 0F;
            this.lnAccountHeader.Left = 0F;
            this.lnAccountHeader.LineWeight = 1F;
            this.lnAccountHeader.Name = "lnAccountHeader";
            this.lnAccountHeader.Top = 1.197917F;
            this.lnAccountHeader.Width = 7.5F;
            this.lnAccountHeader.X1 = 0F;
            this.lnAccountHeader.X2 = 7.5F;
            this.lnAccountHeader.Y1 = 1.197917F;
            this.lnAccountHeader.Y2 = 1.197917F;
            // 
            // lblBookPage
            // 
            this.lblBookPage.Height = 0.1875F;
            this.lblBookPage.HyperLink = null;
            this.lblBookPage.Left = 6.427083F;
            this.lblBookPage.Name = "lblBookPage";
            this.lblBookPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblBookPage.Text = "BookPage";
            this.lblBookPage.Top = 1F;
            this.lblBookPage.Width = 1F;
            // 
            // txtaccount
            // 
            this.txtaccount.Height = 0.1875F;
            this.txtaccount.Left = 0F;
            this.txtaccount.MultiLine = false;
            this.txtaccount.Name = "txtaccount";
            this.txtaccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtaccount.Text = null;
            this.txtaccount.Top = 0.02083333F;
            this.txtaccount.Width = 0.5F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1875F;
            this.txtName.Left = 0.625F;
            this.txtName.MultiLine = false;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \'Tahoma\'";
            this.txtName.Text = null;
            this.txtName.Top = 0.02083333F;
            this.txtName.Width = 2.322917F;
            // 
            // txtMapLot
            // 
            this.txtMapLot.Height = 0.1875F;
            this.txtMapLot.Left = 3F;
            this.txtMapLot.MultiLine = false;
            this.txtMapLot.Name = "txtMapLot";
            this.txtMapLot.Style = "font-family: \'Tahoma\'; text-align: left";
            this.txtMapLot.Text = null;
            this.txtMapLot.Top = 0.02083333F;
            this.txtMapLot.Width = 1.25F;
            // 
            // txtLocation
            // 
            this.txtLocation.Height = 0.1875F;
            this.txtLocation.Left = 4.25F;
            this.txtLocation.MultiLine = false;
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Style = "font-family: \'Tahoma\'";
            this.txtLocation.Text = null;
            this.txtLocation.Top = 0.02083333F;
            this.txtLocation.Width = 2.1875F;
            // 
            // fldBookPage
            // 
            this.fldBookPage.Height = 0.1875F;
            this.fldBookPage.Left = 6.4375F;
            this.fldBookPage.MultiLine = false;
            this.fldBookPage.Name = "fldBookPage";
            this.fldBookPage.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldBookPage.Text = null;
            this.fldBookPage.Top = 0.02083333F;
            this.fldBookPage.Width = 1.0625F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0.01041667F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.04166667F;
            this.Line2.Width = 7.5F;
            this.Line2.X1 = 0.01041667F;
            this.Line2.X2 = 7.510417F;
            this.Line2.Y1 = 0.04166667F;
            this.Line2.Y2 = 0.04166667F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldReportName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMortgageName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookPage;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReportName;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumber;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMortgageName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnAccountHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBookPage;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
    }
}
