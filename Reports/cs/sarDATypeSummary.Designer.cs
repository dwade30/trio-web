namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class sarDATypeSummary
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarDATypeSummary));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPLI = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDesc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldPrincipal,
            this.fldInterest,
            this.fldCosts,
            this.fldTotal,
            this.fldType,
            this.fldDesc,
            this.fldPLI});
            this.Detail.Height = 0.40625F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblPrincipal,
            this.lblInterest,
            this.lblCosts,
            this.lblTotal,
            this.lnHeader,
            this.lblType,
            this.lblDesc,
            this.lblHeader,
            this.lblPLI});
            this.GroupHeader1.Height = 0.5104167F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalPLI,
            this.lblTotals,
            this.fldTotalPrin,
            this.fldTotalInt,
            this.fldTotalCosts,
            this.fldTotalTotal,
            this.lnTotals});
            this.GroupFooter1.Height = 0.46875F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1875F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 2.28125F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPrincipal.Text = "Principal";
            this.lblPrincipal.Top = 0.3125F;
            this.lblPrincipal.Width = 1F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1875F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 5.3125F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblInterest.Text = "Interest";
            this.lblInterest.Top = 0.3125F;
            this.lblInterest.Width = 1F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 6.6875F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblCosts.Text = "Costs";
            this.lblCosts.Top = 0.3125F;
            this.lblCosts.Width = 1F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 8.0625F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotal.Text = "Total";
            this.lblTotal.Top = 0.3125F;
            this.lblTotal.Width = 1F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 0.5F;
            this.lnHeader.Width = 6.9375F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 6.9375F;
            this.lnHeader.Y1 = 0.5F;
            this.lnHeader.Y2 = 0.5F;
            // 
            // lblType
            // 
            this.lblType.Height = 0.1875F;
            this.lblType.HyperLink = null;
            this.lblType.Left = 0F;
            this.lblType.Name = "lblType";
            this.lblType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblType.Text = "Type";
            this.lblType.Top = 0.3125F;
            this.lblType.Width = 0.5F;
            // 
            // lblDesc
            // 
            this.lblDesc.Height = 0.1875F;
            this.lblDesc.HyperLink = null;
            this.lblDesc.Left = 0.625F;
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblDesc.Text = "Description";
            this.lblDesc.Top = 0.3125F;
            this.lblDesc.Width = 1.46875F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.3125F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "- - - Type Summary - - -";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7F;
            // 
            // lblPLI
            // 
            this.lblPLI.Height = 0.1875F;
            this.lblPLI.HyperLink = null;
            this.lblPLI.Left = 5.0625F;
            this.lblPLI.Name = "lblPLI";
            this.lblPLI.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPLI.Text = "PLI";
            this.lblPLI.Top = 0.3125F;
            this.lblPLI.Width = 1F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 2.625F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 1F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 4.625F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 1F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.1875F;
            this.fldCosts.Left = 5.625F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCosts.Text = null;
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 1F;
            // 
            // fldTotal
            // 
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 6.625F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1F;
            // 
            // fldType
            // 
            this.fldType.Height = 0.1875F;
            this.fldType.Left = 0F;
            this.fldType.Name = "fldType";
            this.fldType.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldType.Text = null;
            this.fldType.Top = 0F;
            this.fldType.Width = 0.5F;
            // 
            // fldDesc
            // 
            this.fldDesc.Height = 0.1875F;
            this.fldDesc.Left = 0.625F;
            this.fldDesc.Name = "fldDesc";
            this.fldDesc.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldDesc.Text = "Refunded Abatement";
            this.fldDesc.Top = 0F;
            this.fldDesc.Width = 1.46875F;
            // 
            // fldPLI
            // 
            this.fldPLI.Height = 0.1875F;
            this.fldPLI.Left = 3.625F;
            this.fldPLI.Name = "fldPLI";
            this.fldPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPLI.Text = null;
            this.fldPLI.Top = 0F;
            this.fldPLI.Width = 1F;
            // 
            // fldTotalPLI
            // 
            this.fldTotalPLI.Height = 0.1875F;
            this.fldTotalPLI.Left = 3.6875F;
            this.fldTotalPLI.Name = "fldTotalPLI";
            this.fldTotalPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPLI.Text = "0.00";
            this.fldTotalPLI.Top = 0.0625F;
            this.fldTotalPLI.Width = 1F;
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.1875F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 0.59375F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblTotals.Text = "Total:";
            this.lblTotals.Top = 0.0625F;
            this.lblTotals.Width = 1.5F;
            // 
            // fldTotalPrin
            // 
            this.fldTotalPrin.Height = 0.1875F;
            this.fldTotalPrin.Left = 2.6875F;
            this.fldTotalPrin.Name = "fldTotalPrin";
            this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPrin.Text = "0.00";
            this.fldTotalPrin.Top = 0.0625F;
            this.fldTotalPrin.Width = 1F;
            // 
            // fldTotalInt
            // 
            this.fldTotalInt.Height = 0.1875F;
            this.fldTotalInt.Left = 4.6875F;
            this.fldTotalInt.Name = "fldTotalInt";
            this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalInt.Text = "0.00";
            this.fldTotalInt.Top = 0.0625F;
            this.fldTotalInt.Width = 1F;
            // 
            // fldTotalCosts
            // 
            this.fldTotalCosts.Height = 0.1875F;
            this.fldTotalCosts.Left = 5.6875F;
            this.fldTotalCosts.Name = "fldTotalCosts";
            this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCosts.Text = "0.00";
            this.fldTotalCosts.Top = 0.0625F;
            this.fldTotalCosts.Width = 1F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 6.6875F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTotal.Text = "0.00";
            this.fldTotalTotal.Top = 0.0625F;
            this.fldTotalTotal.Width = 1F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 2.6875F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0.0625F;
            this.lnTotals.Width = 4F;
            this.lnTotals.X1 = 2.6875F;
            this.lnTotals.X2 = 6.6875F;
            this.lnTotals.Y1 = 0.0625F;
            this.lnTotals.Y2 = 0.0625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.375F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDesc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDesc;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLI;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDesc;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPLI;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLI;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
    }
}
