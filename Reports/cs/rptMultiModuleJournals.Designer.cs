namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptMultiModuleJournals
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMultiModuleJournals));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldDebitTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.rptSubTotals = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.fldCreditTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBalance = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.rptSchoolSubTotals = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Field1,
            this.Field2,
            this.Field3,
            this.Field4,
            this.Field5,
            this.Field6,
            this.Field7,
            this.Field8,
            this.Field9,
            this.Line2});
            this.Detail.Height = 0.25F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label6,
            this.Line1,
            this.Label7,
            this.Label8,
            this.Label9,
            this.Label10,
            this.Label11,
            this.Label12,
            this.Label13,
            this.Label14,
            this.Label16,
            this.lblDescription});
            this.GroupHeader1.Height = 0.65625F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label15,
            this.fldDebitTotal,
            this.rptSubTotals,
            this.fldCreditTotal,
            this.lblBalance,
            this.Line3,
            this.rptSchoolSubTotals});
            this.GroupFooter1.Height = 0.75F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label6
            // 
            this.Label6.Height = 0.1875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 0F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.Label6.Text = "Label6";
            this.Label6.Top = 0.1875F;
            this.Label6.Width = 7.625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.625F;
            this.Line1.Width = 7.625F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 7.625F;
            this.Line1.Y1 = 0.625F;
            this.Line1.Y2 = 0.625F;
            // 
            // Label7
            // 
            this.Label7.Height = 0.1875F;
            this.Label7.HyperLink = null;
            this.Label7.Left = 0F;
            this.Label7.Name = "Label7";
            this.Label7.Style = "font-size: 10pt";
            this.Label7.Text = "Per";
            this.Label7.Top = 0.4375F;
            this.Label7.Width = 0.375F;
            // 
            // Label8
            // 
            this.Label8.Height = 0.1875F;
            this.Label8.HyperLink = null;
            this.Label8.Left = 0.4375F;
            this.Label8.Name = "Label8";
            this.Label8.Style = "font-size: 10pt; text-align: center";
            this.Label8.Text = "Date";
            this.Label8.Top = 0.4375F;
            this.Label8.Width = 0.6875F;
            // 
            // Label9
            // 
            this.Label9.Height = 0.1875F;
            this.Label9.HyperLink = null;
            this.Label9.Left = 1.1875F;
            this.Label9.Name = "Label9";
            this.Label9.Style = "font-size: 10pt";
            this.Label9.Text = "Description";
            this.Label9.Top = 0.4375F;
            this.Label9.Width = 1.625F;
            // 
            // Label10
            // 
            this.Label10.Height = 0.1875F;
            this.Label10.HyperLink = null;
            this.Label10.Left = 2.84375F;
            this.Label10.Name = "Label10";
            this.Label10.Style = "font-size: 10pt";
            this.Label10.Text = "RCB";
            this.Label10.Top = 0.4375F;
            this.Label10.Width = 0.375F;
            // 
            // Label11
            // 
            this.Label11.Height = 0.1875F;
            this.Label11.HyperLink = null;
            this.Label11.Left = 3.25F;
            this.Label11.Name = "Label11";
            this.Label11.Style = "font-size: 10pt";
            this.Label11.Text = "Type";
            this.Label11.Top = 0.4375F;
            this.Label11.Width = 0.375F;
            // 
            // Label12
            // 
            this.Label12.Height = 0.1875F;
            this.Label12.HyperLink = null;
            this.Label12.Left = 3.6875F;
            this.Label12.Name = "Label12";
            this.Label12.Style = "font-size: 10pt";
            this.Label12.Text = "Account";
            this.Label12.Top = 0.4375F;
            this.Label12.Width = 1.0625F;
            // 
            // Label13
            // 
            this.Label13.Height = 0.1875F;
            this.Label13.HyperLink = null;
            this.Label13.Left = 5.34375F;
            this.Label13.Name = "Label13";
            this.Label13.Style = "font-size: 10pt";
            this.Label13.Text = "Proj";
            this.Label13.Top = 0.4375F;
            this.Label13.Width = 0.375F;
            // 
            // Label14
            // 
            this.Label14.Height = 0.1875F;
            this.Label14.HyperLink = null;
            this.Label14.Left = 6.75F;
            this.Label14.Name = "Label14";
            this.Label14.Style = "font-size: 10pt; text-align: right";
            this.Label14.Text = "Credit";
            this.Label14.Top = 0.4375F;
            this.Label14.Width = 0.84375F;
            // 
            // Label16
            // 
            this.Label16.Height = 0.1875F;
            this.Label16.HyperLink = null;
            this.Label16.Left = 5.75F;
            this.Label16.Name = "Label16";
            this.Label16.Style = "font-size: 10pt; text-align: right";
            this.Label16.Text = "Debit";
            this.Label16.Top = 0.4375F;
            this.Label16.Width = 0.875F;
            // 
            // lblDescription
            // 
            this.lblDescription.Height = 0.1875F;
            this.lblDescription.HyperLink = null;
            this.lblDescription.Left = 0F;
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.lblDescription.Text = "Label17";
            this.lblDescription.Top = 0F;
            this.lblDescription.Width = 7.625F;
            // 
            // Field1
            // 
            this.Field1.Height = 0.1875F;
            this.Field1.Left = 0F;
            this.Field1.Name = "Field1";
            this.Field1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field1.Text = "Field1";
            this.Field1.Top = 0.0625F;
            this.Field1.Width = 0.375F;
            // 
            // Field2
            // 
            this.Field2.Height = 0.1875F;
            this.Field2.Left = 0.4375F;
            this.Field2.Name = "Field2";
            this.Field2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Field2.Text = "Field2";
            this.Field2.Top = 0.0625F;
            this.Field2.Width = 0.6875F;
            // 
            // Field3
            // 
            this.Field3.Height = 0.1875F;
            this.Field3.Left = 1.1875F;
            this.Field3.Name = "Field3";
            this.Field3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field3.Text = "Field3";
            this.Field3.Top = 0.0625F;
            this.Field3.Width = 1.625F;
            // 
            // Field4
            // 
            this.Field4.Height = 0.1875F;
            this.Field4.Left = 2.84375F;
            this.Field4.Name = "Field4";
            this.Field4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Field4.Text = "Field4";
            this.Field4.Top = 0.0625F;
            this.Field4.Width = 0.375F;
            // 
            // Field5
            // 
            this.Field5.Height = 0.1875F;
            this.Field5.Left = 3.3125F;
            this.Field5.Name = "Field5";
            this.Field5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
            this.Field5.Text = "Field5";
            this.Field5.Top = 0.0625F;
            this.Field5.Width = 0.25F;
            // 
            // Field6
            // 
            this.Field6.Height = 0.1875F;
            this.Field6.Left = 3.6875F;
            this.Field6.Name = "Field6";
            this.Field6.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field6.Text = "p 001-0001-0001-0001-01";
            this.Field6.Top = 0.0625F;
            this.Field6.Width = 1.5625F;
            // 
            // Field7
            // 
            this.Field7.Height = 0.1875F;
            this.Field7.Left = 5.3125F;
            this.Field7.Name = "Field7";
            this.Field7.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
            this.Field7.Text = "Field7";
            this.Field7.Top = 0.0625F;
            this.Field7.Width = 0.40625F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.1875F;
            this.Field8.Left = 6.6875F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Field8.Text = "Field8";
            this.Field8.Top = 0.0625F;
            this.Field8.Width = 0.90625F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.1875F;
            this.Field9.Left = 5.75F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.Field9.Text = "Field9";
            this.Field9.Top = 0.0625F;
            this.Field9.Width = 0.875F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0F;
            this.Line2.Visible = false;
            this.Line2.Width = 7.5F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 0F;
            this.Line2.Y2 = 0F;
            // 
            // Label15
            // 
            this.Label15.Height = 0.1875F;
            this.Label15.HyperLink = null;
            this.Label15.Left = 5.1875F;
            this.Label15.Name = "Label15";
            this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 1";
            this.Label15.Text = "Total";
            this.Label15.Top = 0.125F;
            this.Label15.Width = 0.5F;
            // 
            // fldDebitTotal
            // 
            this.fldDebitTotal.Height = 0.1875F;
            this.fldDebitTotal.Left = 5.75F;
            this.fldDebitTotal.Name = "fldDebitTotal";
            this.fldDebitTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldDebitTotal.Text = null;
            this.fldDebitTotal.Top = 0.125F;
            this.fldDebitTotal.Width = 0.875F;
            // 
            // rptSubTotals
            // 
            this.rptSubTotals.CloseBorder = false;
            this.rptSubTotals.Height = 0.125F;
            this.rptSubTotals.Left = 0F;
            this.rptSubTotals.Name = "rptSubTotals";
            this.rptSubTotals.Report = null;
            this.rptSubTotals.Top = 0.46875F;
            this.rptSubTotals.Width = 7.625F;
            // 
            // fldCreditTotal
            // 
            this.fldCreditTotal.Height = 0.1875F;
            this.fldCreditTotal.Left = 6.6875F;
            this.fldCreditTotal.Name = "fldCreditTotal";
            this.fldCreditTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
            this.fldCreditTotal.Text = null;
            this.fldCreditTotal.Top = 0.125F;
            this.fldCreditTotal.Width = 0.90625F;
            // 
            // lblBalance
            // 
            this.lblBalance.Height = 0.3125F;
            this.lblBalance.HyperLink = null;
            this.lblBalance.Left = 0.375F;
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Style = "font-family: \'Tahoma\'; font-size: 18pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 1";
            this.lblBalance.Text = "OUT OF BALANCE!!";
            this.lblBalance.Top = 0.125F;
            this.lblBalance.Visible = false;
            this.lblBalance.Width = 3.125F;
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 5.34375F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0.0625F;
            this.Line3.Width = 2.28125F;
            this.Line3.X1 = 5.34375F;
            this.Line3.X2 = 7.625F;
            this.Line3.Y1 = 0.0625F;
            this.Line3.Y2 = 0.0625F;
            // 
            // rptSchoolSubTotals
            // 
            this.rptSchoolSubTotals.CloseBorder = false;
            this.rptSchoolSubTotals.Height = 0.125F;
            this.rptSchoolSubTotals.Left = 0F;
            this.rptSchoolSubTotals.Name = "rptSchoolSubTotals";
            this.rptSchoolSubTotals.Report = null;
            this.rptSchoolSubTotals.Top = 0.625F;
            this.rptSchoolSubTotals.Width = 7.625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.635417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDebitTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCreditTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDebitTotal;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport rptSubTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBalance;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport rptSchoolSubTotals;
    }
}
