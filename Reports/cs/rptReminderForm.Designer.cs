namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptReminderForm
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderForm));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPreMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.srptReminderFormDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.fldMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.imgLogo = new GrapeCity.ActiveReports.SectionReportModel.Picture();
            this.fldSignOut = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSignerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSignerTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPreMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSignOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSignerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSignerTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblName,
            this.lblMailingAddress1,
            this.lblMailingAddress2,
            this.lblMailingAddress3,
            this.lblAccount,
            this.lblHeader,
            this.lblPreMessage,
            this.lblMailingAddress4,
            this.srptReminderFormDetail,
            this.lblDate,
            this.lblLocation,
            this.lblMapLot,
            this.imgSig,
            this.fldMessage,
            this.imgLogo,
            this.fldSignOut,
            this.fldSignerName,
            this.fldSignerTitle});
            this.Detail.Height = 7.3125F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblName
            // 
            this.lblName.Height = 0.125F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Courier New\'";
            this.lblName.Tag = "TEXT";
            this.lblName.Text = " ";
            this.lblName.Top = 1.5F;
            this.lblName.Width = 4F;
            // 
            // lblMailingAddress1
            // 
            this.lblMailingAddress1.Height = 0.125F;
            this.lblMailingAddress1.HyperLink = null;
            this.lblMailingAddress1.Left = 0F;
            this.lblMailingAddress1.Name = "lblMailingAddress1";
            this.lblMailingAddress1.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress1.Tag = "TEXT";
            this.lblMailingAddress1.Text = " ";
            this.lblMailingAddress1.Top = 1.625F;
            this.lblMailingAddress1.Width = 4F;
            // 
            // lblMailingAddress2
            // 
            this.lblMailingAddress2.Height = 0.125F;
            this.lblMailingAddress2.HyperLink = null;
            this.lblMailingAddress2.Left = 0F;
            this.lblMailingAddress2.Name = "lblMailingAddress2";
            this.lblMailingAddress2.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress2.Tag = "TEXT";
            this.lblMailingAddress2.Text = " ";
            this.lblMailingAddress2.Top = 1.75F;
            this.lblMailingAddress2.Width = 4F;
            // 
            // lblMailingAddress3
            // 
            this.lblMailingAddress3.Height = 0.125F;
            this.lblMailingAddress3.HyperLink = null;
            this.lblMailingAddress3.Left = 0F;
            this.lblMailingAddress3.Name = "lblMailingAddress3";
            this.lblMailingAddress3.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress3.Tag = "TEXT";
            this.lblMailingAddress3.Text = " ";
            this.lblMailingAddress3.Top = 1.875F;
            this.lblMailingAddress3.Width = 4F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.15F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Courier New\'";
            this.lblAccount.Tag = "TEXT";
            this.lblAccount.Text = " ";
            this.lblAccount.Top = 0.6F;
            this.lblAccount.Width = 4.75F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1875F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Courier New\'; text-align: center";
            this.lblHeader.Tag = "TEXT";
            this.lblHeader.Text = " ";
            this.lblHeader.Top = 2.5F;
            this.lblHeader.Width = 6F;
            // 
            // lblPreMessage
            // 
            this.lblPreMessage.Height = 0.375F;
            this.lblPreMessage.HyperLink = null;
            this.lblPreMessage.Left = 0F;
            this.lblPreMessage.Name = "lblPreMessage";
            this.lblPreMessage.Style = "font-family: \'Courier New\'";
            this.lblPreMessage.Tag = "TEXT";
            this.lblPreMessage.Text = " ";
            this.lblPreMessage.Top = 2.875F;
            this.lblPreMessage.Width = 6F;
            // 
            // lblMailingAddress4
            // 
            this.lblMailingAddress4.Height = 0.125F;
            this.lblMailingAddress4.HyperLink = null;
            this.lblMailingAddress4.Left = 0F;
            this.lblMailingAddress4.Name = "lblMailingAddress4";
            this.lblMailingAddress4.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress4.Tag = "TEXT";
            this.lblMailingAddress4.Text = "  ";
            this.lblMailingAddress4.Top = 2F;
            this.lblMailingAddress4.Width = 4F;
            // 
            // srptReminderFormDetail
            // 
            this.srptReminderFormDetail.CloseBorder = false;
            this.srptReminderFormDetail.Height = 0.125F;
            this.srptReminderFormDetail.Left = 0F;
            this.srptReminderFormDetail.Name = "srptReminderFormDetail";
            this.srptReminderFormDetail.Report = null;
            this.srptReminderFormDetail.Top = 3.3125F;
            this.srptReminderFormDetail.Width = 6F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.125F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 0F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Courier New\'";
            this.lblDate.Tag = "TEXT";
            this.lblDate.Text = " ";
            this.lblDate.Top = 0.375F;
            this.lblDate.Width = 4.75F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.125F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 0F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Courier New\'";
            this.lblLocation.Tag = "TEXT";
            this.lblLocation.Text = "  ";
            this.lblLocation.Top = 2.25F;
            this.lblLocation.Width = 4F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.125F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 0F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Courier New\'";
            this.lblMapLot.Tag = "TEXT";
            this.lblMapLot.Text = "  ";
            this.lblMapLot.Top = 2.375F;
            this.lblMapLot.Width = 4F;
            // 
            // imgSig
            // 
            this.imgSig.Height = 0.5625F;
            this.imgSig.HyperLink = null;
            this.imgSig.ImageData = null;
            this.imgSig.Left = 2.8125F;
            this.imgSig.LineWeight = 1F;
            this.imgSig.Name = "imgSig";
            this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
            this.imgSig.Top = 6.25F;
            this.imgSig.Width = 2.375F;
            // 
            // fldMessage
            // 
            this.fldMessage.Height = 2.5625F;
            this.fldMessage.Left = 0F;
            this.fldMessage.Name = "fldMessage";
            this.fldMessage.Style = "font-family: \'Courier New\'";
            this.fldMessage.Text = null;
            this.fldMessage.Top = 3.5F;
            this.fldMessage.Width = 5.9375F;
            // 
            // imgLogo
            // 
            this.imgLogo.Height = 1F;
            this.imgLogo.HyperLink = null;
            this.imgLogo.ImageData = null;
            this.imgLogo.Left = 4.75F;
            this.imgLogo.LineWeight = 1F;
            this.imgLogo.Name = "imgLogo";
            this.imgLogo.Top = 0F;
            this.imgLogo.Visible = false;
            this.imgLogo.Width = 1F;
            // 
            // fldSignOut
            // 
            this.fldSignOut.Height = 0.1875F;
            this.fldSignOut.Left = 2.8125F;
            this.fldSignOut.Name = "fldSignOut";
            this.fldSignOut.Style = "font-family: \'Courier New\'";
            this.fldSignOut.Text = null;
            this.fldSignOut.Top = 6.0625F;
            this.fldSignOut.Width = 2.375F;
            // 
            // fldSignerName
            // 
            this.fldSignerName.Height = 0.1875F;
            this.fldSignerName.Left = 2.8125F;
            this.fldSignerName.Name = "fldSignerName";
            this.fldSignerName.Style = "font-family: \'Courier New\'";
            this.fldSignerName.Text = null;
            this.fldSignerName.Top = 6.8125F;
            this.fldSignerName.Width = 2.375F;
            // 
            // fldSignerTitle
            // 
            this.fldSignerTitle.Height = 0.1875F;
            this.fldSignerTitle.Left = 2.8125F;
            this.fldSignerTitle.Name = "fldSignerTitle";
            this.fldSignerTitle.Style = "font-family: \'Courier New\'";
            this.fldSignerTitle.Text = null;
            this.fldSignerTitle.Top = 7F;
            this.fldSignerTitle.Width = 2.375F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPreMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSignOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSignerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSignerTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPreMessage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress4;
        private GrapeCity.ActiveReports.SectionReportModel.SubReport srptReminderFormDetail;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMessage;
        private GrapeCity.ActiveReports.SectionReportModel.Picture imgLogo;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSignOut;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSignerName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSignerTitle;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
