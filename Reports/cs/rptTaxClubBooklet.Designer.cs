namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptTaxClubBooklet
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTaxClubBooklet));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblReceiptFor = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMonth = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCheck = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccountPayment = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccountLine = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmountPaid = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblText = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceiptFor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheck)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountPayment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblYear,
            this.Line1,
            this.lblReceiptFor,
            this.lblMonth,
            this.lblDate,
            this.lblAmount,
            this.lblCheck,
            this.lblAccountPayment,
            this.lblHeader,
            this.lblAccountLine,
            this.lblName,
            this.lblAmountPaid,
            this.lblText,
            this.lblAddress1,
            this.lblAddress2,
            this.lblAddress3,
            this.lblAddress4,
            this.Line2,
            this.Line4,
            this.Line5,
            this.Line6,
            this.Line7});
            this.Detail.Height = 1.5625F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Line3});
            this.PageHeader.Height = 0.0006944445F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // Line3
            // 
            this.Line3.Height = 0F;
            this.Line3.Left = 0F;
            this.Line3.LineWeight = 1F;
            this.Line3.Name = "Line3";
            this.Line3.Top = 0F;
            this.Line3.Width = 6F;
            this.Line3.X1 = 0F;
            this.Line3.X2 = 6F;
            this.Line3.Y1 = 0F;
            this.Line3.Y2 = 0F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1736111F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0.1388889F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'";
            this.lblYear.Text = null;
            this.lblYear.Top = 0F;
            this.lblYear.Width = 2.011111F;
            // 
            // Line1
            // 
            this.Line1.Height = 1.5F;
            this.Line1.Left = 2.25F;
            this.Line1.LineStyle = GrapeCity.ActiveReports.SectionReportModel.LineStyle.Dash;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 0.05F;
            this.Line1.Width = 0F;
            this.Line1.X1 = 2.25F;
            this.Line1.X2 = 2.25F;
            this.Line1.Y1 = 0.05F;
            this.Line1.Y2 = 1.55F;
            // 
            // lblReceiptFor
            // 
            this.lblReceiptFor.Height = 0.1736111F;
            this.lblReceiptFor.HyperLink = null;
            this.lblReceiptFor.Left = 0.3F;
            this.lblReceiptFor.Name = "lblReceiptFor";
            this.lblReceiptFor.Style = "font-family: \'Tahoma\'";
            this.lblReceiptFor.Text = "Receipt For:";
            this.lblReceiptFor.Top = 0.1736111F;
            this.lblReceiptFor.Width = 1.3F;
            // 
            // lblMonth
            // 
            this.lblMonth.Height = 0.1736111F;
            this.lblMonth.HyperLink = null;
            this.lblMonth.Left = 0.375F;
            this.lblMonth.Name = "lblMonth";
            this.lblMonth.Style = "font-family: \'Tahoma\'";
            this.lblMonth.Text = null;
            this.lblMonth.Top = 0.3472222F;
            this.lblMonth.Width = 1.8125F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1736111F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 0.75F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 0.5208333F;
            this.lblDate.Width = 0.8F;
            // 
            // lblAmount
            // 
            this.lblAmount.Height = 0.1736111F;
            this.lblAmount.HyperLink = null;
            this.lblAmount.Left = 0.75F;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Style = "font-family: \'Tahoma\'";
            this.lblAmount.Text = "Amount";
            this.lblAmount.Top = 0.8680556F;
            this.lblAmount.Width = 0.8F;
            // 
            // lblCheck
            // 
            this.lblCheck.Height = 0.1736111F;
            this.lblCheck.HyperLink = null;
            this.lblCheck.Left = 0.75F;
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Style = "font-family: \'Tahoma\'";
            this.lblCheck.Text = "Check #";
            this.lblCheck.Top = 1.215278F;
            this.lblCheck.Width = 0.8F;
            // 
            // lblAccountPayment
            // 
            this.lblAccountPayment.Height = 0.1736111F;
            this.lblAccountPayment.HyperLink = null;
            this.lblAccountPayment.Left = 0.75F;
            this.lblAccountPayment.Name = "lblAccountPayment";
            this.lblAccountPayment.Style = "font-family: \'Tahoma\'";
            this.lblAccountPayment.Text = null;
            this.lblAccountPayment.Top = 1.388889F;
            this.lblAccountPayment.Width = 1.4F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1736111F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 3.3125F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'";
            this.lblHeader.Text = null;
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 2.5F;
            // 
            // lblAccountLine
            // 
            this.lblAccountLine.Height = 0.1736111F;
            this.lblAccountLine.HyperLink = null;
            this.lblAccountLine.Left = 2.9375F;
            this.lblAccountLine.Name = "lblAccountLine";
            this.lblAccountLine.Style = "font-family: \'Tahoma\'; vertical-align: top; white-space: nowrap";
            this.lblAccountLine.Text = null;
            this.lblAccountLine.Top = 0.1736111F;
            this.lblAccountLine.Width = 3F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1736111F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 2.9375F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'";
            this.lblName.Text = null;
            this.lblName.Top = 0.3472222F;
            this.lblName.Width = 2.5F;
            // 
            // lblAmountPaid
            // 
            this.lblAmountPaid.Height = 0.1736111F;
            this.lblAmountPaid.HyperLink = null;
            this.lblAmountPaid.Left = 3.3125F;
            this.lblAmountPaid.Name = "lblAmountPaid";
            this.lblAmountPaid.Style = "font-family: \'Tahoma\'";
            this.lblAmountPaid.Text = "Amount Paid:";
            this.lblAmountPaid.Top = 0.5208333F;
            this.lblAmountPaid.Width = 1F;
            // 
            // lblText
            // 
            this.lblText.Height = 0.1736111F;
            this.lblText.HyperLink = null;
            this.lblText.Left = 2.9375F;
            this.lblText.Name = "lblText";
            this.lblText.Style = "font-family: \'Tahoma\'; vertical-align: top";
            this.lblText.Text = "Mail this stub with payment to:";
            this.lblText.Top = 0.6944444F;
            this.lblText.Width = 2.5F;
            // 
            // lblAddress1
            // 
            this.lblAddress1.Height = 0.1736111F;
            this.lblAddress1.HyperLink = null;
            this.lblAddress1.Left = 3.25F;
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Style = "font-family: \'Tahoma\'";
            this.lblAddress1.Text = null;
            this.lblAddress1.Top = 0.8680556F;
            this.lblAddress1.Width = 2.5F;
            // 
            // lblAddress2
            // 
            this.lblAddress2.Height = 0.1736111F;
            this.lblAddress2.HyperLink = null;
            this.lblAddress2.Left = 3.25F;
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Style = "font-family: \'Tahoma\'; vertical-align: top";
            this.lblAddress2.Text = null;
            this.lblAddress2.Top = 1.041667F;
            this.lblAddress2.Width = 2.5F;
            // 
            // lblAddress3
            // 
            this.lblAddress3.Height = 0.1736111F;
            this.lblAddress3.HyperLink = null;
            this.lblAddress3.Left = 3.25F;
            this.lblAddress3.Name = "lblAddress3";
            this.lblAddress3.Style = "font-family: \'Tahoma\'";
            this.lblAddress3.Text = null;
            this.lblAddress3.Top = 1.215278F;
            this.lblAddress3.Width = 2.5F;
            // 
            // lblAddress4
            // 
            this.lblAddress4.Height = 0.1736111F;
            this.lblAddress4.HyperLink = null;
            this.lblAddress4.Left = 3.25F;
            this.lblAddress4.Name = "lblAddress4";
            this.lblAddress4.Style = "font-family: \'Tahoma\'";
            this.lblAddress4.Text = null;
            this.lblAddress4.Top = 1.388889F;
            this.lblAddress4.Width = 2.5F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 1.55F;
            this.Line2.Width = 6F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 6F;
            this.Line2.Y1 = 1.55F;
            this.Line2.Y2 = 1.55F;
            // 
            // Line4
            // 
            this.Line4.Height = 0F;
            this.Line4.Left = 1.375F;
            this.Line4.LineWeight = 1F;
            this.Line4.Name = "Line4";
            this.Line4.Top = 0.6875F;
            this.Line4.Width = 0.75F;
            this.Line4.X1 = 1.375F;
            this.Line4.X2 = 2.125F;
            this.Line4.Y1 = 0.6875F;
            this.Line4.Y2 = 0.6875F;
            // 
            // Line5
            // 
            this.Line5.Height = 0F;
            this.Line5.Left = 1.375F;
            this.Line5.LineWeight = 1F;
            this.Line5.Name = "Line5";
            this.Line5.Top = 1.041667F;
            this.Line5.Width = 0.75F;
            this.Line5.X1 = 1.375F;
            this.Line5.X2 = 2.125F;
            this.Line5.Y1 = 1.041667F;
            this.Line5.Y2 = 1.041667F;
            // 
            // Line6
            // 
            this.Line6.Height = 0F;
            this.Line6.Left = 1.4F;
            this.Line6.LineWeight = 1F;
            this.Line6.Name = "Line6";
            this.Line6.Top = 1.354167F;
            this.Line6.Width = 0.7500001F;
            this.Line6.X1 = 1.4F;
            this.Line6.X2 = 2.15F;
            this.Line6.Y1 = 1.354167F;
            this.Line6.Y2 = 1.354167F;
            // 
            // Line7
            // 
            this.Line7.Height = 0F;
            this.Line7.Left = 4.3125F;
            this.Line7.LineWeight = 1F;
            this.Line7.Name = "Line7";
            this.Line7.Top = 0.6875F;
            this.Line7.Width = 1.125F;
            this.Line7.X1 = 4.3125F;
            this.Line7.X2 = 5.4375F;
            this.Line7.Y1 = 0.6875F;
            this.Line7.Y2 = 0.6875F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceiptFor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCheck)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountPayment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccountLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmountPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptFor;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMonth;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCheck;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountPayment;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountLine;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAmountPaid;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblText;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress2;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress3;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
