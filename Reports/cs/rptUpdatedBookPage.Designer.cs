namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptUpdatedBookPage
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUpdatedBookPage));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBookPage1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBookPage2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldOld = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOld)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAcct,
            this.fldName,
            this.fldBookPage1,
            this.fldBookPage2,
            this.fldLocation,
            this.fldMapLot,
            this.fldOld,
            this.fldNew});
            this.Detail.Height = 0.625F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanGrow = false;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFooter});
            this.ReportFooter.Height = 0.4375F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblAcct,
            this.lblName,
            this.lblMapLot,
            this.lblLocation});
            this.PageHeader.Height = 0.625F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.3125F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Updated Book and Page Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 7.5F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.375F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.375F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.5625F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 0.75F;
            this.lnHeader.Width = 6.9375F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 6.9375F;
            this.lnHeader.Y1 = 0.75F;
            this.lnHeader.Y2 = 0.75F;
            // 
            // lblAcct
            // 
            this.lblAcct.Height = 0.1875F;
            this.lblAcct.HyperLink = null;
            this.lblAcct.Left = 0F;
            this.lblAcct.Name = "lblAcct";
            this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAcct.Text = "Acct";
            this.lblAcct.Top = 0.4375F;
            this.lblAcct.Width = 0.5625F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.625F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.4375F;
            this.lblName.Width = 1.4375F;
            // 
            // lblMapLot
            // 
            this.lblMapLot.Height = 0.1875F;
            this.lblMapLot.HyperLink = null;
            this.lblMapLot.Left = 3.625F;
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMapLot.Text = "Map Lot";
            this.lblMapLot.Top = 0.4375F;
            this.lblMapLot.Width = 1.0625F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1875F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 4.75F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblLocation.Text = "Location";
            this.lblLocation.Top = 0.4375F;
            this.lblLocation.Width = 1.0625F;
            // 
            // fldAcct
            // 
            this.fldAcct.Height = 0.1875F;
            this.fldAcct.Left = 0F;
            this.fldAcct.Name = "fldAcct";
            this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAcct.Text = null;
            this.fldAcct.Top = 0F;
            this.fldAcct.Width = 0.5625F;
            // 
            // fldName
            // 
            this.fldName.CanGrow = false;
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.625F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 2.9375F;
            // 
            // fldBookPage1
            // 
            this.fldBookPage1.Height = 0.1875F;
            this.fldBookPage1.Left = 1.0625F;
            this.fldBookPage1.Name = "fldBookPage1";
            this.fldBookPage1.Style = "font-family: \'Tahoma\'";
            this.fldBookPage1.Text = null;
            this.fldBookPage1.Top = 0.1875F;
            this.fldBookPage1.Width = 6.3125F;
            // 
            // fldBookPage2
            // 
            this.fldBookPage2.Height = 0.1875F;
            this.fldBookPage2.Left = 1.0625F;
            this.fldBookPage2.Name = "fldBookPage2";
            this.fldBookPage2.Style = "font-family: \'Tahoma\'";
            this.fldBookPage2.Text = null;
            this.fldBookPage2.Top = 0.375F;
            this.fldBookPage2.Width = 6.3125F;
            // 
            // fldLocation
            // 
            this.fldLocation.CanGrow = false;
            this.fldLocation.Height = 0.1875F;
            this.fldLocation.Left = 4.75F;
            this.fldLocation.Name = "fldLocation";
            this.fldLocation.Style = "font-family: \'Tahoma\'; white-space: nowrap";
            this.fldLocation.Text = null;
            this.fldLocation.Top = 0F;
            this.fldLocation.Width = 2.75F;
            // 
            // fldMapLot
            // 
            this.fldMapLot.Height = 0.1875F;
            this.fldMapLot.Left = 3.625F;
            this.fldMapLot.Name = "fldMapLot";
            this.fldMapLot.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldMapLot.Text = null;
            this.fldMapLot.Top = 0F;
            this.fldMapLot.Width = 1.0625F;
            // 
            // fldOld
            // 
            this.fldOld.Height = 0.1875F;
            this.fldOld.Left = 0.625F;
            this.fldOld.Name = "fldOld";
            this.fldOld.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldOld.Text = "Old:";
            this.fldOld.Top = 0.1875F;
            this.fldOld.Width = 0.4375F;
            // 
            // fldNew
            // 
            this.fldNew.Height = 0.1875F;
            this.fldNew.Left = 0.625F;
            this.fldNew.Name = "fldNew";
            this.fldNew.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldNew.Text = "New:";
            this.fldNew.Top = 0.375F;
            this.fldNew.Width = 0.4375F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.3125F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 1F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: center";
            this.lblFooter.Text = null;
            this.lblFooter.Top = 0.125F;
            this.lblFooter.Width = 5F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldOld)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookPage1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookPage2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOld;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNew;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
