namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class srptReminderForm
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptReminderForm));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblHeaderYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooterPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooterInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblFooterCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblYear,
            this.lblPrincipal,
            this.lblInterest,
            this.lblTotal,
            this.lblCost});
            this.Detail.Height = 0.1770833F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeaderYear,
            this.lblHeaderPrin,
            this.lblHeaderInt,
            this.lblHeaderTotal,
            this.lblHeaderCost});
            this.ReportHeader.Height = 0.1770833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblFooter,
            this.lblFooterPrincipal,
            this.lblFooterInterest,
            this.lblFooterTotal,
            this.lnTotal,
            this.lblFooterCost});
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lblHeaderYear
            // 
            this.lblHeaderYear.Height = 0.125F;
            this.lblHeaderYear.HyperLink = null;
            this.lblHeaderYear.Left = 0.07291666F;
            this.lblHeaderYear.Name = "lblHeaderYear";
            this.lblHeaderYear.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
            this.lblHeaderYear.Text = "Year";
            this.lblHeaderYear.Top = 0F;
            this.lblHeaderYear.Width = 0.9270833F;
            // 
            // lblHeaderPrin
            // 
            this.lblHeaderPrin.Height = 0.125F;
            this.lblHeaderPrin.HyperLink = null;
            this.lblHeaderPrin.Left = 1.125F;
            this.lblHeaderPrin.Name = "lblHeaderPrin";
            this.lblHeaderPrin.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
            this.lblHeaderPrin.Text = "Principal";
            this.lblHeaderPrin.Top = 0F;
            this.lblHeaderPrin.Width = 1.25F;
            // 
            // lblHeaderInt
            // 
            this.lblHeaderInt.Height = 0.125F;
            this.lblHeaderInt.HyperLink = null;
            this.lblHeaderInt.Left = 2.5F;
            this.lblHeaderInt.Name = "lblHeaderInt";
            this.lblHeaderInt.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
            this.lblHeaderInt.Text = "Interest";
            this.lblHeaderInt.Top = 0F;
            this.lblHeaderInt.Width = 1.0625F;
            // 
            // lblHeaderTotal
            // 
            this.lblHeaderTotal.Height = 0.125F;
            this.lblHeaderTotal.HyperLink = null;
            this.lblHeaderTotal.Left = 4.75F;
            this.lblHeaderTotal.Name = "lblHeaderTotal";
            this.lblHeaderTotal.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
            this.lblHeaderTotal.Text = "Total";
            this.lblHeaderTotal.Top = 0F;
            this.lblHeaderTotal.Width = 1.25F;
            // 
            // lblHeaderCost
            // 
            this.lblHeaderCost.Height = 0.125F;
            this.lblHeaderCost.HyperLink = null;
            this.lblHeaderCost.Left = 3.6875F;
            this.lblHeaderCost.Name = "lblHeaderCost";
            this.lblHeaderCost.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
            this.lblHeaderCost.Text = "Cost";
            this.lblHeaderCost.Top = 0F;
            this.lblHeaderCost.Width = 1F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.125F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 0.06944445F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblYear.Text = "0000";
            this.lblYear.Top = 0F;
            this.lblYear.Width = 0.9305556F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.125F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 1.125F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblPrincipal.Text = "0.00";
            this.lblPrincipal.Top = 0F;
            this.lblPrincipal.Width = 1.25F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.125F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 2.5F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblInterest.Text = "0.00";
            this.lblInterest.Top = 0F;
            this.lblInterest.Width = 1.0625F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.125F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 4.75F;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblTotal.Text = "0.00";
            this.lblTotal.Top = 0F;
            this.lblTotal.Width = 1.25F;
            // 
            // lblCost
            // 
            this.lblCost.Height = 0.125F;
            this.lblCost.HyperLink = null;
            this.lblCost.Left = 3.6875F;
            this.lblCost.Name = "lblCost";
            this.lblCost.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblCost.Text = "0.00";
            this.lblCost.Top = 0F;
            this.lblCost.Width = 1F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.1375F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 0.06944445F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
            this.lblFooter.Text = "Total:";
            this.lblFooter.Top = 0.05F;
            this.lblFooter.Width = 0.9305556F;
            // 
            // lblFooterPrincipal
            // 
            this.lblFooterPrincipal.Height = 0.125F;
            this.lblFooterPrincipal.HyperLink = null;
            this.lblFooterPrincipal.Left = 1.125F;
            this.lblFooterPrincipal.Name = "lblFooterPrincipal";
            this.lblFooterPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblFooterPrincipal.Text = "0.00";
            this.lblFooterPrincipal.Top = 0.0625F;
            this.lblFooterPrincipal.Width = 1.25F;
            // 
            // lblFooterInterest
            // 
            this.lblFooterInterest.Height = 0.125F;
            this.lblFooterInterest.HyperLink = null;
            this.lblFooterInterest.Left = 2.5F;
            this.lblFooterInterest.Name = "lblFooterInterest";
            this.lblFooterInterest.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblFooterInterest.Text = "0.00";
            this.lblFooterInterest.Top = 0.0625F;
            this.lblFooterInterest.Width = 1.0625F;
            // 
            // lblFooterTotal
            // 
            this.lblFooterTotal.Height = 0.125F;
            this.lblFooterTotal.HyperLink = null;
            this.lblFooterTotal.Left = 4.75F;
            this.lblFooterTotal.Name = "lblFooterTotal";
            this.lblFooterTotal.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblFooterTotal.Text = "0.00";
            this.lblFooterTotal.Top = 0.0625F;
            this.lblFooterTotal.Width = 1.25F;
            // 
            // lnTotal
            // 
            this.lnTotal.Height = 0F;
            this.lnTotal.Left = 0F;
            this.lnTotal.LineWeight = 1F;
            this.lnTotal.Name = "lnTotal";
            this.lnTotal.Top = 0F;
            this.lnTotal.Width = 5.95F;
            this.lnTotal.X1 = 0F;
            this.lnTotal.X2 = 5.95F;
            this.lnTotal.Y1 = 0F;
            this.lnTotal.Y2 = 0F;
            // 
            // lblFooterCost
            // 
            this.lblFooterCost.Height = 0.125F;
            this.lblFooterCost.HyperLink = null;
            this.lblFooterCost.Left = 3.6875F;
            this.lblFooterCost.Name = "lblFooterCost";
            this.lblFooterCost.Style = "font-family: \'Courier New\'; text-align: right";
            this.lblFooterCost.Text = "0.00";
            this.lblFooterCost.Top = 0.0625F;
            this.lblFooterCost.Width = 1F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 6F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooterCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderPrin;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderInt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderCost;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterCost;
    }
}
