namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class arCLReceipt
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arCLReceipt));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblHeaderAmt = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPLI = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPrincipalAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPLIAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterestAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCostAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblOtherAccounts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblComment = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReprint = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReceiptEnd = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAmt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipalAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLIAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterestAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCostAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReprint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceiptEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblName,
            this.lblPrincipal,
            this.lblPLI,
            this.lblInterest,
            this.lblCost,
            this.lblPrincipalAmount,
            this.lblPLIAmount,
            this.lblInterestAmount,
            this.lblCostAmount,
            this.lblLocation});
            this.Detail.Height = 1.125F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.CanGrow = false;
            this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeaderAmt,
            this.lblHeader,
            this.lblDate,
            this.lblTime,
            this.lblAccount,
            this.lnHeader,
            this.lblMuniName});
            this.ReportHeader.Height = 0.6875F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblOtherAccounts,
            this.fldTotal,
            this.lblTotal,
            this.lblComment,
            this.lblReprint,
            this.lblReceiptEnd});
            this.ReportFooter.Height = 1.0625F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // lblHeaderAmt
            // 
            this.lblHeaderAmt.Height = 0.125F;
            this.lblHeaderAmt.HyperLink = null;
            this.lblHeaderAmt.Left = 3.4375F;
            this.lblHeaderAmt.MultiLine = false;
            this.lblHeaderAmt.Name = "lblHeaderAmt";
            this.lblHeaderAmt.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; white-space: nowr" +
    "ap";
            this.lblHeaderAmt.Text = "Amount";
            this.lblHeaderAmt.Top = 0.5625F;
            this.lblHeaderAmt.Width = 1F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1666667F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.MultiLine = false;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Courier New\'; text-align: center; white-space: nowrap";
            this.lblHeader.Text = "Header";
            this.lblHeader.Top = 0.1666667F;
            this.lblHeader.Width = 2.4375F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.125F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 0F;
            this.lblDate.MultiLine = false;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; white-space: nowra" +
    "p";
            this.lblDate.Text = "Date";
            this.lblDate.Top = 0.375F;
            this.lblDate.Width = 1F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.125F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 1.0625F;
            this.lblTime.MultiLine = false;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; white-space: nowra" +
    "p";
            this.lblTime.Text = "Time";
            this.lblTime.Top = 0.375F;
            this.lblTime.Width = 1F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.125F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 2.125F;
            this.lblAccount.MultiLine = false;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left; white-space: nowra" +
    "p";
            this.lblAccount.Text = "Account";
            this.lblAccount.Top = 0.375F;
            this.lblAccount.Width = 1.125F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 0.6875F;
            this.lnHeader.Width = 4.4375F;
            this.lnHeader.X1 = 0F;
            this.lnHeader.X2 = 4.4375F;
            this.lnHeader.Y1 = 0.6875F;
            this.lnHeader.Y2 = 0.6875F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1666667F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.MultiLine = false;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center; white-space: now" +
    "rap";
            this.lblMuniName.Text = "MuniName";
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.4375F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1666667F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Courier New\'; font-size: 10pt";
            this.lblName.Text = null;
            this.lblName.Top = 0F;
            this.lblName.Width = 4.4375F;
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.Height = 0.1666667F;
            this.lblPrincipal.HyperLink = null;
            this.lblPrincipal.Left = 0F;
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
            this.lblPrincipal.Text = null;
            this.lblPrincipal.Top = 0.375F;
            this.lblPrincipal.Width = 2.4375F;
            // 
            // lblPLI
            // 
            this.lblPLI.Height = 0.1666667F;
            this.lblPLI.HyperLink = null;
            this.lblPLI.Left = 0F;
            this.lblPLI.Name = "lblPLI";
            this.lblPLI.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
            this.lblPLI.Text = null;
            this.lblPLI.Top = 0.5625F;
            this.lblPLI.Width = 2.4375F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.1666667F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 0F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
            this.lblInterest.Text = null;
            this.lblInterest.Top = 0.75F;
            this.lblInterest.Width = 2.4375F;
            // 
            // lblCost
            // 
            this.lblCost.Height = 0.1666667F;
            this.lblCost.HyperLink = null;
            this.lblCost.Left = 0F;
            this.lblCost.Name = "lblCost";
            this.lblCost.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: left";
            this.lblCost.Text = null;
            this.lblCost.Top = 0.9375F;
            this.lblCost.Width = 2.4375F;
            // 
            // lblPrincipalAmount
            // 
            this.lblPrincipalAmount.Height = 0.1666667F;
            this.lblPrincipalAmount.HyperLink = null;
            this.lblPrincipalAmount.Left = 3.4375F;
            this.lblPrincipalAmount.Name = "lblPrincipalAmount";
            this.lblPrincipalAmount.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
            this.lblPrincipalAmount.Text = null;
            this.lblPrincipalAmount.Top = 0.375F;
            this.lblPrincipalAmount.Width = 1F;
            // 
            // lblPLIAmount
            // 
            this.lblPLIAmount.Height = 0.1666667F;
            this.lblPLIAmount.HyperLink = null;
            this.lblPLIAmount.Left = 3.4375F;
            this.lblPLIAmount.Name = "lblPLIAmount";
            this.lblPLIAmount.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
            this.lblPLIAmount.Text = null;
            this.lblPLIAmount.Top = 0.5625F;
            this.lblPLIAmount.Width = 1F;
            // 
            // lblInterestAmount
            // 
            this.lblInterestAmount.Height = 0.1666667F;
            this.lblInterestAmount.HyperLink = null;
            this.lblInterestAmount.Left = 3.4375F;
            this.lblInterestAmount.Name = "lblInterestAmount";
            this.lblInterestAmount.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
            this.lblInterestAmount.Text = null;
            this.lblInterestAmount.Top = 0.75F;
            this.lblInterestAmount.Width = 1F;
            // 
            // lblCostAmount
            // 
            this.lblCostAmount.Height = 0.1666667F;
            this.lblCostAmount.HyperLink = null;
            this.lblCostAmount.Left = 3.4375F;
            this.lblCostAmount.Name = "lblCostAmount";
            this.lblCostAmount.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right";
            this.lblCostAmount.Text = null;
            this.lblCostAmount.Top = 0.9375F;
            this.lblCostAmount.Width = 1F;
            // 
            // lblLocation
            // 
            this.lblLocation.Height = 0.1666667F;
            this.lblLocation.HyperLink = null;
            this.lblLocation.Left = 0F;
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Style = "font-family: \'Courier New\'; font-size: 10pt";
            this.lblLocation.Text = null;
            this.lblLocation.Top = 0.1875F;
            this.lblLocation.Width = 4.4375F;
            // 
            // lblOtherAccounts
            // 
            this.lblOtherAccounts.Height = 0.1875F;
            this.lblOtherAccounts.HyperLink = null;
            this.lblOtherAccounts.Left = 0F;
            this.lblOtherAccounts.MultiLine = false;
            this.lblOtherAccounts.Name = "lblOtherAccounts";
            this.lblOtherAccounts.Style = "font-family: \'Courier New\'; font-size: 10pt; white-space: nowrap";
            this.lblOtherAccounts.Text = " ";
            this.lblOtherAccounts.Top = 0.375F;
            this.lblOtherAccounts.Width = 3.1875F;
            // 
            // fldTotal
            // 
            this.fldTotal.Border.TopColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.fldTotal.Border.TopStyle = GrapeCity.ActiveReports.BorderLineStyle.Solid;
            this.fldTotal.CanGrow = false;
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 3.4375F;
            this.fldTotal.MultiLine = false;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: right; white-space: nowr" +
    "ap";
            this.fldTotal.Text = "Total";
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1F;
            // 
            // lblTotal
            // 
            this.lblTotal.Height = 0.1875F;
            this.lblTotal.HyperLink = null;
            this.lblTotal.Left = 2.375F;
            this.lblTotal.MultiLine = false;
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Style = "font-family: \'Courier New\'; font-size: 10pt; white-space: nowrap";
            this.lblTotal.Text = "Total:";
            this.lblTotal.Top = 0F;
            this.lblTotal.Width = 0.625F;
            // 
            // lblComment
            // 
            this.lblComment.Height = 0.1875F;
            this.lblComment.HyperLink = null;
            this.lblComment.Left = 0F;
            this.lblComment.MultiLine = false;
            this.lblComment.Name = "lblComment";
            this.lblComment.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center; white-space: now" +
    "rap";
            this.lblComment.Text = null;
            this.lblComment.Top = 0.1875F;
            this.lblComment.Width = 3.1875F;
            // 
            // lblReprint
            // 
            this.lblReprint.Height = 0.1875F;
            this.lblReprint.HyperLink = null;
            this.lblReprint.Left = 0F;
            this.lblReprint.MultiLine = false;
            this.lblReprint.Name = "lblReprint";
            this.lblReprint.Style = "font-family: \'Courier New\'; font-size: 10pt; text-align: center; white-space: now" +
    "rap";
            this.lblReprint.Text = "  *  *  *  R E P R I N T  *  *  *";
            this.lblReprint.Top = 0.375F;
            this.lblReprint.Visible = false;
            this.lblReprint.Width = 3.1875F;
            // 
            // lblReceiptEnd
            // 
            this.lblReceiptEnd.Height = 0.1875F;
            this.lblReceiptEnd.HyperLink = null;
            this.lblReceiptEnd.Left = 0F;
            this.lblReceiptEnd.MultiLine = false;
            this.lblReceiptEnd.Name = "lblReceiptEnd";
            this.lblReceiptEnd.Style = "font-family: \'Courier New\'; font-size: 10pt; white-space: nowrap";
            this.lblReceiptEnd.Text = " .";
            this.lblReceiptEnd.Top = 0.875F;
            this.lblReceiptEnd.Width = 0.25F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.25F;
            this.PageSettings.Margins.Right = 0F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 4.979167F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAmt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPrincipalAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPLIAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterestAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCostAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblOtherAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReprint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReceiptEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPLI;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipalAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPLIAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterestAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCostAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderAmt;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblOtherAccounts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblComment;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReprint;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReceiptEnd;
    }
}
