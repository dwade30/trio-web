namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptBankruptAccounts
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBankruptAccounts));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTaxDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCurrentInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCurInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCurInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanShrink = true;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldType,
            this.fldName,
            this.fldYear,
            this.fldTaxDue,
            this.fldPaymentReceived,
            this.fldDue,
            this.fldCosts,
            this.fldInterest,
            this.fldCurrentInterest});
            this.Detail.Height = 0.2083333F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalTaxDue,
            this.fldTotalPaymentReceived,
            this.fldTotalDue,
            this.lnTotals,
            this.lblTotals,
            this.fldTotalCosts,
            this.fldTotalInterest,
            this.fldTotalCurInterest});
            this.ReportFooter.Height = 0.2291667F;
            this.ReportFooter.KeepTogether = true;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.CanGrow = false;
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblDate,
            this.lblPage,
            this.lblTime,
            this.lblMuniName,
            this.lnHeader,
            this.lblAccount,
            this.lblYear,
            this.lblTaxDue,
            this.lblPaymentReceived,
            this.lblDue,
            this.lblName,
            this.lblReportType,
            this.lblCosts,
            this.lblInterest,
            this.Label1});
            this.PageHeader.Height = 1.21875F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1875F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Bankrupt Accounts";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 9.4375F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 8.3125F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1.125F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.3125F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1.125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.125F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.625F;
            // 
            // lnHeader
            // 
            this.lnHeader.Height = 0F;
            this.lnHeader.Left = 0.125F;
            this.lnHeader.LineWeight = 1F;
            this.lnHeader.Name = "lnHeader";
            this.lnHeader.Top = 1.1875F;
            this.lnHeader.Width = 9.4375F;
            this.lnHeader.X1 = 0.125F;
            this.lnHeader.X2 = 9.5625F;
            this.lnHeader.Y1 = 1.1875F;
            this.lnHeader.Y2 = 1.1875F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblAccount.Text = "Acct";
            this.lblAccount.Top = 1F;
            this.lblAccount.Width = 0.5F;
            // 
            // lblYear
            // 
            this.lblYear.Height = 0.1875F;
            this.lblYear.HyperLink = null;
            this.lblYear.Left = 2.875F;
            this.lblYear.Name = "lblYear";
            this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblYear.Text = "Year";
            this.lblYear.Top = 1F;
            this.lblYear.Width = 0.4375F;
            // 
            // lblTaxDue
            // 
            this.lblTaxDue.Height = 0.1875F;
            this.lblTaxDue.HyperLink = null;
            this.lblTaxDue.Left = 3.375F;
            this.lblTaxDue.Name = "lblTaxDue";
            this.lblTaxDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblTaxDue.Text = "Original Tax";
            this.lblTaxDue.Top = 1F;
            this.lblTaxDue.Width = 0.9375F;
            // 
            // lblPaymentReceived
            // 
            this.lblPaymentReceived.Height = 0.375F;
            this.lblPaymentReceived.HyperLink = null;
            this.lblPaymentReceived.Left = 7.375F;
            this.lblPaymentReceived.Name = "lblPaymentReceived";
            this.lblPaymentReceived.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblPaymentReceived.Text = "Payment / Adjustments";
            this.lblPaymentReceived.Top = 0.8125F;
            this.lblPaymentReceived.Width = 1.0625F;
            // 
            // lblDue
            // 
            this.lblDue.Height = 0.1875F;
            this.lblDue.HyperLink = null;
            this.lblDue.Left = 8.5F;
            this.lblDue.Name = "lblDue";
            this.lblDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblDue.Text = "Amount Due";
            this.lblDue.Top = 1F;
            this.lblDue.Width = 1F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 0.75F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblName.Text = "Name ----";
            this.lblName.Top = 1F;
            this.lblName.Width = 1F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.625F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = "Report Type";
            this.lblReportType.Top = 0.1875F;
            this.lblReportType.Width = 9.4375F;
            // 
            // lblCosts
            // 
            this.lblCosts.Height = 0.1875F;
            this.lblCosts.HyperLink = null;
            this.lblCosts.Left = 4.375F;
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblCosts.Text = "Costs Added";
            this.lblCosts.Top = 1F;
            this.lblCosts.Width = 0.9375F;
            // 
            // lblInterest
            // 
            this.lblInterest.Height = 0.375F;
            this.lblInterest.HyperLink = null;
            this.lblInterest.Left = 5.375F;
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.lblInterest.Text = "Interest Added";
            this.lblInterest.Top = 0.8125F;
            this.lblInterest.Width = 0.9375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.375F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 6.375F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
            this.Label1.Text = "Current Interest";
            this.Label1.Top = 0.8125F;
            this.Label1.Width = 0.9375F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.5F;
            // 
            // fldType
            // 
            this.fldType.Height = 0.1875F;
            this.fldType.Left = 0.5F;
            this.fldType.Name = "fldType";
            this.fldType.Style = "font-family: \'Tahoma\'; text-align: center";
            this.fldType.Text = null;
            this.fldType.Top = 0F;
            this.fldType.Width = 0.25F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 0.75F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 2.0625F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 2.875F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 0.4375F;
            // 
            // fldTaxDue
            // 
            this.fldTaxDue.Height = 0.1875F;
            this.fldTaxDue.Left = 3.375F;
            this.fldTaxDue.Name = "fldTaxDue";
            this.fldTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTaxDue.Text = "0.00";
            this.fldTaxDue.Top = 0F;
            this.fldTaxDue.Width = 0.9375F;
            // 
            // fldPaymentReceived
            // 
            this.fldPaymentReceived.Height = 0.1875F;
            this.fldPaymentReceived.Left = 7.375F;
            this.fldPaymentReceived.Name = "fldPaymentReceived";
            this.fldPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPaymentReceived.Text = "0.00";
            this.fldPaymentReceived.Top = 0F;
            this.fldPaymentReceived.Width = 1.0625F;
            // 
            // fldDue
            // 
            this.fldDue.Height = 0.1875F;
            this.fldDue.Left = 8.5F;
            this.fldDue.Name = "fldDue";
            this.fldDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldDue.Text = "0.00";
            this.fldDue.Top = 0F;
            this.fldDue.Width = 1F;
            // 
            // fldCosts
            // 
            this.fldCosts.Height = 0.1875F;
            this.fldCosts.Left = 4.375F;
            this.fldCosts.Name = "fldCosts";
            this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCosts.Text = "0.00";
            this.fldCosts.Top = 0F;
            this.fldCosts.Width = 0.9375F;
            // 
            // fldInterest
            // 
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 5.375F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest.Text = "0.00";
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 0.9375F;
            // 
            // fldCurrentInterest
            // 
            this.fldCurrentInterest.Height = 0.1875F;
            this.fldCurrentInterest.Left = 6.375F;
            this.fldCurrentInterest.Name = "fldCurrentInterest";
            this.fldCurrentInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCurrentInterest.Text = "0.00";
            this.fldCurrentInterest.Top = 0F;
            this.fldCurrentInterest.Width = 0.9375F;
            // 
            // fldTotalTaxDue
            // 
            this.fldTotalTaxDue.Height = 0.1875F;
            this.fldTotalTaxDue.Left = 3.375F;
            this.fldTotalTaxDue.Name = "fldTotalTaxDue";
            this.fldTotalTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTaxDue.Text = "0.00";
            this.fldTotalTaxDue.Top = 0F;
            this.fldTotalTaxDue.Width = 0.9375F;
            // 
            // fldTotalPaymentReceived
            // 
            this.fldTotalPaymentReceived.Height = 0.1875F;
            this.fldTotalPaymentReceived.Left = 7.375F;
            this.fldTotalPaymentReceived.Name = "fldTotalPaymentReceived";
            this.fldTotalPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPaymentReceived.Text = "0.00";
            this.fldTotalPaymentReceived.Top = 0F;
            this.fldTotalPaymentReceived.Width = 1.0625F;
            // 
            // fldTotalDue
            // 
            this.fldTotalDue.Height = 0.1875F;
            this.fldTotalDue.Left = 8.5F;
            this.fldTotalDue.Name = "fldTotalDue";
            this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalDue.Text = "0.00";
            this.fldTotalDue.Top = 0F;
            this.fldTotalDue.Width = 1F;
            // 
            // lnTotals
            // 
            this.lnTotals.Height = 0F;
            this.lnTotals.Left = 3.375F;
            this.lnTotals.LineWeight = 1F;
            this.lnTotals.Name = "lnTotals";
            this.lnTotals.Top = 0F;
            this.lnTotals.Width = 6.125F;
            this.lnTotals.X1 = 3.375F;
            this.lnTotals.X2 = 9.5F;
            this.lnTotals.Y1 = 0F;
            this.lnTotals.Y2 = 0F;
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.1875F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 1F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTotals.Text = "Total:";
            this.lblTotals.Top = 0F;
            this.lblTotals.Width = 2.125F;
            // 
            // fldTotalCosts
            // 
            this.fldTotalCosts.Height = 0.1875F;
            this.fldTotalCosts.Left = 4.375F;
            this.fldTotalCosts.Name = "fldTotalCosts";
            this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCosts.Text = "0.00";
            this.fldTotalCosts.Top = 0F;
            this.fldTotalCosts.Width = 0.9375F;
            // 
            // fldTotalInterest
            // 
            this.fldTotalInterest.Height = 0.1875F;
            this.fldTotalInterest.Left = 5.375F;
            this.fldTotalInterest.Name = "fldTotalInterest";
            this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalInterest.Text = "0.00";
            this.fldTotalInterest.Top = 0F;
            this.fldTotalInterest.Width = 0.9375F;
            // 
            // fldTotalCurInterest
            // 
            this.fldTotalCurInterest.Height = 0.1875F;
            this.fldTotalCurInterest.Left = 6.375F;
            this.fldTotalCurInterest.Name = "fldTotalCurInterest";
            this.fldTotalCurInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCurInterest.Text = "0.00";
            this.fldTotalCurInterest.Top = 0F;
            this.fldTotalCurInterest.Width = 0.9375F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.5625F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCurrentInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCurInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentReceived;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentInterest;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxDue;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentReceived;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurInterest;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxDue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentReceived;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDue;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
