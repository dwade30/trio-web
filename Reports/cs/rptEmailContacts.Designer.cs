namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptEmailContacts
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptEmailContacts));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtGroupName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtNickname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.txtEmail = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNickname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtEmail,
            this.txtName,
            this.txtDescription});
            this.Detail.Height = 0.2083333F;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label1,
            this.txtDate,
            this.txtMuniname,
            this.txtPage,
            this.txtTime});
            this.PageHeader.Height = 0.375F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.Label2,
            this.txtGroupName,
            this.Label3,
            this.txtNickname,
            this.Label4,
            this.Label5,
            this.Label6});
            this.GroupHeader1.Height = 0.65625F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // Label1
            // 
            this.Label1.Height = 0.25F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2.208333F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.Label1.Text = "E-mail Groups";
            this.Label1.Top = 0F;
            this.Label1.Width = 3.5625F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 6.302083F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.125F;
            // 
            // txtMuniname
            // 
            this.txtMuniname.Height = 0.1875F;
            this.txtMuniname.Left = 0F;
            this.txtMuniname.Name = "txtMuniname";
            this.txtMuniname.Style = "font-family: \'Tahoma\'";
            this.txtMuniname.Text = null;
            this.txtMuniname.Top = 0F;
            this.txtMuniname.Width = 2.15625F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 6.302083F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.1875F;
            this.txtPage.Width = 1.125F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.1875F;
            this.txtTime.Width = 1.3125F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.21875F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 0.05208333F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label2.Text = "Group";
            this.Label2.Top = 0.04166667F;
            this.Label2.Width = 0.53125F;
            // 
            // txtGroupName
            // 
            this.txtGroupName.Height = 0.21875F;
            this.txtGroupName.Left = 0.625F;
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Style = "font-family: \'Tahoma\'";
            this.txtGroupName.Text = null;
            this.txtGroupName.Top = 0.04166667F;
            this.txtGroupName.Width = 2.114583F;
            // 
            // Label3
            // 
            this.Label3.Height = 0.21875F;
            this.Label3.HyperLink = null;
            this.Label3.Left = 2.927083F;
            this.Label3.Name = "Label3";
            this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label3.Text = "Quickname";
            this.Label3.Top = 0.04166667F;
            this.Label3.Width = 0.9270833F;
            // 
            // txtNickname
            // 
            this.txtNickname.Height = 0.21875F;
            this.txtNickname.Left = 3.9375F;
            this.txtNickname.Name = "txtNickname";
            this.txtNickname.Style = "font-family: \'Tahoma\'";
            this.txtNickname.Text = null;
            this.txtNickname.Top = 0.04166667F;
            this.txtNickname.Width = 2.114583F;
            // 
            // Label4
            // 
            this.Label4.Height = 0.21875F;
            this.Label4.HyperLink = null;
            this.Label4.Left = 0.0625F;
            this.Label4.Name = "Label4";
            this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label4.Text = "E-Mail";
            this.Label4.Top = 0.40625F;
            this.Label4.Width = 2.40625F;
            // 
            // Label5
            // 
            this.Label5.Height = 0.21875F;
            this.Label5.HyperLink = null;
            this.Label5.Left = 3.010417F;
            this.Label5.Name = "Label5";
            this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label5.Text = "Name";
            this.Label5.Top = 0.40625F;
            this.Label5.Width = 2.46875F;
            // 
            // Label6
            // 
            this.Label6.Height = 0.21875F;
            this.Label6.HyperLink = null;
            this.Label6.Left = 5.5F;
            this.Label6.Name = "Label6";
            this.Label6.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.Label6.Text = "Description";
            this.Label6.Top = 0.40625F;
            this.Label6.Width = 1.90625F;
            // 
            // txtEmail
            // 
            this.txtEmail.Height = 0.1666667F;
            this.txtEmail.Left = 0.0625F;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Style = "font-family: \'Tahoma\'";
            this.txtEmail.Text = null;
            this.txtEmail.Top = 0.04166667F;
            this.txtEmail.Width = 2.854167F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1666667F;
            this.txtName.Left = 3.010417F;
            this.txtName.Name = "txtName";
            this.txtName.Style = "font-family: \'Tahoma\'";
            this.txtName.Text = null;
            this.txtName.Top = 0.04166667F;
            this.txtName.Width = 2.458333F;
            // 
            // txtDescription
            // 
            this.txtDescription.Height = 0.1666667F;
            this.txtDescription.Left = 5.5F;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Style = "font-family: \'Tahoma\'";
            this.txtDescription.Text = null;
            this.txtDescription.Top = 0.04166667F;
            this.txtDescription.Width = 1.916667F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.46875F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNickname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEmail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDescription;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupName;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNickname;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
    }
}
