namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class rptGroupInformation
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptGroupInformation));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.fldHeader = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtCity = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtState = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtZip = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.txtModule = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtGroupBalance = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtModule,
            this.txtAccount,
            this.txtName,
            this.txtBalance});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroupBalance,
            this.Field12,
            this.Line2});
            this.ReportFooter.Height = 0.3541667F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldHeader,
            this.txtMuni,
            this.txtTime,
            this.txtDate,
            this.txtPage});
            this.PageHeader.Height = 0.3958333F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtGroupName,
            this.txtAddress1,
            this.txtAddress2,
            this.txtCity,
            this.txtState,
            this.txtZip,
            this.Field8,
            this.Field9,
            this.Field10,
            this.Field11,
            this.Line1});
            this.GroupHeader1.Height = 1.072917F;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // fldHeader
            // 
            this.fldHeader.Height = 0.375F;
            this.fldHeader.Left = 0F;
            this.fldHeader.Name = "fldHeader";
            this.fldHeader.Style = "font-size: 12pt; font-weight: bold; text-align: center";
            this.fldHeader.Text = "Group Balance";
            this.fldHeader.Top = 0F;
            this.fldHeader.Width = 7.4375F;
            // 
            // txtMuni
            // 
            this.txtMuni.Height = 0.1875F;
            this.txtMuni.Left = 0F;
            this.txtMuni.Name = "txtMuni";
            this.txtMuni.Text = null;
            this.txtMuni.Top = 0F;
            this.txtMuni.Width = 3F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.1875F;
            this.txtTime.Left = 0F;
            this.txtTime.Name = "txtTime";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.1875F;
            this.txtTime.Width = 1.4375F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 6F;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.4375F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.1875F;
            this.txtPage.Left = 6F;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "text-align: right";
            this.txtPage.Text = null;
            this.txtPage.Top = 0.1875F;
            this.txtPage.Width = 1.4375F;
            // 
            // txtGroupName
            // 
            this.txtGroupName.Height = 0.1875F;
            this.txtGroupName.Left = 1F;
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Text = null;
            this.txtGroupName.Top = 0F;
            this.txtGroupName.Width = 5F;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Height = 0.1875F;
            this.txtAddress1.Left = 1F;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Text = null;
            this.txtAddress1.Top = 0.1875F;
            this.txtAddress1.Width = 5F;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Height = 0.1875F;
            this.txtAddress2.Left = 1F;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Text = null;
            this.txtAddress2.Top = 0.375F;
            this.txtAddress2.Width = 5F;
            // 
            // txtCity
            // 
            this.txtCity.Height = 0.1875F;
            this.txtCity.Left = 1F;
            this.txtCity.Name = "txtCity";
            this.txtCity.Text = null;
            this.txtCity.Top = 0.5625F;
            this.txtCity.Width = 1.6875F;
            // 
            // txtState
            // 
            this.txtState.Height = 0.1875F;
            this.txtState.Left = 3.3125F;
            this.txtState.Name = "txtState";
            this.txtState.Text = null;
            this.txtState.Top = 0.5625F;
            this.txtState.Width = 0.375F;
            // 
            // txtZip
            // 
            this.txtZip.Height = 0.1875F;
            this.txtZip.Left = 4.1875F;
            this.txtZip.Name = "txtZip";
            this.txtZip.Text = null;
            this.txtZip.Top = 0.5625F;
            this.txtZip.Width = 1.8125F;
            // 
            // Field8
            // 
            this.Field8.Height = 0.1875F;
            this.Field8.Left = 0F;
            this.Field8.Name = "Field8";
            this.Field8.Style = "font-weight: bold";
            this.Field8.Text = "Module";
            this.Field8.Top = 0.90625F;
            this.Field8.Width = 0.5625F;
            // 
            // Field9
            // 
            this.Field9.Height = 0.1875F;
            this.Field9.Left = 0.5625F;
            this.Field9.Name = "Field9";
            this.Field9.Style = "font-weight: bold; text-align: right";
            this.Field9.Text = "Account";
            this.Field9.Top = 0.90625F;
            this.Field9.Width = 0.75F;
            // 
            // Field10
            // 
            this.Field10.Height = 0.1875F;
            this.Field10.Left = 1.4375F;
            this.Field10.Name = "Field10";
            this.Field10.Style = "font-weight: bold";
            this.Field10.Text = "Name";
            this.Field10.Top = 0.90625F;
            this.Field10.Width = 0.625F;
            // 
            // Field11
            // 
            this.Field11.Height = 0.1875F;
            this.Field11.Left = 5F;
            this.Field11.Name = "Field11";
            this.Field11.Style = "font-weight: bold; text-align: right";
            this.Field11.Text = "Balance";
            this.Field11.Top = 0.90625F;
            this.Field11.Width = 1.0625F;
            // 
            // Line1
            // 
            this.Line1.Height = 0F;
            this.Line1.Left = 0F;
            this.Line1.LineWeight = 1F;
            this.Line1.Name = "Line1";
            this.Line1.Top = 1.0625F;
            this.Line1.Width = 6.125F;
            this.Line1.X1 = 0F;
            this.Line1.X2 = 6.125F;
            this.Line1.Y1 = 1.0625F;
            this.Line1.Y2 = 1.0625F;
            // 
            // txtModule
            // 
            this.txtModule.Height = 0.1875F;
            this.txtModule.Left = 0F;
            this.txtModule.Name = "txtModule";
            this.txtModule.Text = null;
            this.txtModule.Top = 0F;
            this.txtModule.Width = 0.5625F;
            // 
            // txtAccount
            // 
            this.txtAccount.Height = 0.1875F;
            this.txtAccount.Left = 0.625F;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Style = "text-align: right";
            this.txtAccount.Text = null;
            this.txtAccount.Top = 0F;
            this.txtAccount.Width = 0.6875F;
            // 
            // txtName
            // 
            this.txtName.Height = 0.1875F;
            this.txtName.Left = 1.4375F;
            this.txtName.Name = "txtName";
            this.txtName.Text = null;
            this.txtName.Top = 0F;
            this.txtName.Width = 3.5F;
            // 
            // txtBalance
            // 
            this.txtBalance.Height = 0.1875F;
            this.txtBalance.Left = 5.0625F;
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.Style = "text-align: right";
            this.txtBalance.Text = null;
            this.txtBalance.Top = 0F;
            this.txtBalance.Width = 1F;
            // 
            // txtGroupBalance
            // 
            this.txtGroupBalance.Height = 0.1875F;
            this.txtGroupBalance.Left = 5.0625F;
            this.txtGroupBalance.Name = "txtGroupBalance";
            this.txtGroupBalance.Style = "text-align: right";
            this.txtGroupBalance.Text = null;
            this.txtGroupBalance.Top = 0.15625F;
            this.txtGroupBalance.Width = 1F;
            // 
            // Field12
            // 
            this.Field12.Height = 0.1875F;
            this.Field12.Left = 3.6875F;
            this.Field12.Name = "Field12";
            this.Field12.Style = "font-weight: bold; text-align: right";
            this.Field12.Text = "Group Balance";
            this.Field12.Top = 0.15625F;
            this.Field12.Width = 1.3125F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 4.6875F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.0625F;
            this.Line2.Width = 1.4375F;
            this.Line2.X1 = 4.6875F;
            this.Line2.X2 = 6.125F;
            this.Line2.Y1 = 0.0625F;
            this.Line2.Y2 = 0.0625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.4375F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.fldHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuni)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtModule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGroupBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtModule;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBalance;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupBalance;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeader;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuni;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtGroupName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCity;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtState;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtZip;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
    }
}
