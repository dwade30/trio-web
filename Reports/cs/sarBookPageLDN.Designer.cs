namespace Reports
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    partial class sarBookPageLDN
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarBookPageLDN));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
            this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldBookPage,
            this.fldYear});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader});
            this.GroupHeader1.Height = 0.2916667F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Height = 0F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.1875F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0.375F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
            this.lblHeader.Text = "Lien Discharge Notices";
            this.lblHeader.Top = 0.0625F;
            this.lblHeader.Width = 2.75F;
            // 
            // fldBookPage
            // 
            this.fldBookPage.Height = 0.1875F;
            this.fldBookPage.Left = 0.375F;
            this.fldBookPage.Name = "fldBookPage";
            this.fldBookPage.Style = "font-family: \'Tahoma\'";
            this.fldBookPage.Text = null;
            this.fldBookPage.Top = 0F;
            this.fldBookPage.Width = 2.375F;
            // 
            // fldYear
            // 
            this.fldYear.Height = 0.1875F;
            this.fldYear.Left = 2.75F;
            this.fldYear.Name = "fldYear";
            this.fldYear.Style = "font-family: \'Tahoma\'";
            this.fldYear.Text = null;
            this.fldYear.Top = 0F;
            this.fldYear.Width = 0.625F;
            // 
            // SectionReport1
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 3.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.GroupHeader1);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.GroupFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
        private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
    }
}
